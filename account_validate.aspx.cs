﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : oct 1, 2009
/// </summary>

public partial class account_validate : LoginBasePage2
{
    protected void Page_Load(object sender, EventArgs e)
    {

       // reg_schema_activation_code.ValidationExpression = RegEx.getText();

        if (!Page.IsPostBack)
        {
            link_login.Visible = false;

            if (!string.IsNullOrEmpty(Request.QueryString["usr"].ToString()) &&
                !string.IsNullOrEmpty(Request.QueryString["id"].ToString()))
            {
                if ((Request.QueryString["usr"].ToString().Trim().Length > 0) && (Request.QueryString["usr"].ToString().Trim().Length <= 75))
                {
                    usr.Text = Convert.ToString(Request.QueryString["usr"].Trim());
                    schema_activation_code.Text = Convert.ToString(Request.QueryString["id"].Trim()).Replace(" ","+");
                }

            }
            else
            {

                btn_activate.Enabled = false;
                schema_activation_code.Enabled = false;

                txt.Text = "Error occurred";

            }
        }

    }
    

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_activate_Click(object sender, EventArgs e)
    {
       
        if (activation_code.Text == "1906")
        {
            tiger.registration.Account a = new tiger.registration.Account(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString());

            if (a.activateAccount(Request.QueryString["usr"].Trim().ToString(), schema_activation_code.Text, Request.UserHostAddress.ToString()) == 0)
            {
                message.Text = Resources.Resource.lbl_validation_succeed;
                link_login.Visible = true;
            }
            else
            {
                message.Text = Resources.Resource.lbl_validation_failed;
            }
        }
    }
}
