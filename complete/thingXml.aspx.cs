﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

public partial class auto_suggest_thingXml : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string q = Request.QueryString["q"] ?? string.Empty;
        string path = Server.MapPath(".") + "\\things.xml";
        IEnumerable<string> things = from p in XDocument.Load(path).Descendants("thing") 
                                     where p.Value.Contains(q) select p.Value;
        foreach(string s in things) Response.Write(s + Environment.NewLine);
    }
}