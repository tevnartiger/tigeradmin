﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="auto_suggest_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AutoSuggest with jQuery</title>
    <script type="text/javascript" language="javascript" src="jquery.js"></script>
    <script type="text/javascript" language="javascript" src="jquery.autocomplete.js"></script>
    <script type="text/javascript">
        $().ready(function() {
            var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

            $("#txtNumbers").autocomplete(Spin());
            $("#txtThings").autocomplete("thingXml.aspx");
        });
        function Spin() {

            var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

            return months
        
        }
        
    </script>
    <style type="text/css">
        .ac_odd {background:#dddddd;}
        .ac_results { padding: 0px; border: 1px solid black; background-color: white; overflow: hidden; z-index: 99999; }
        .ac_results ul { width: 100%; list-style-position: outside; list-style: none; padding: 0; margin: 0; }
        .ac_results li { margin: 0px; padding: 2px 5px; cursor: default; display: block; font: menu; font-size:12px; line-height: 16px; overflow: hidden; }
        .ac_loading { background:url(loading.jpg) right no-repeat; }
        .ac_over { background-color: #0A246A; color: white; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>This is using the <a href="http://plugins.jquery.com/project/autocompletex">jquery.autocomplete.js</a> plugin</div>
        <br />
        <h4>Numbers 0-1000</h4>
        <asp:TextBox ID="txtNumbers" runat="server" />
        <h4>Random Things from 'things.xml'</h4>
        <asp:TextBox ID="txtThings" runat="server" />
    </div>
    </form>
</body>
</html>
