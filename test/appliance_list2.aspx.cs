﻿using System;
using System.Xml;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class test_appliance_list2 : BasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!(Page.IsPostBack))
        {

            SetDefaultView();
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            //get the list of warehouse 
            tiger.Warehouse warehouse = new tiger.Warehouse(strconn);


            DataSet dataSet = new DataSet();
            dataSet.ReadXml("F:/www/web/dev/sinfoca/tiger/App_Data/WarehouseAppliance_List.xml");
            

            ddl_warehouse_list.DataSource = dataSet;
            ddl_warehouse_list.DataBind();
            ddl_warehouse_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
            ddl_warehouse_list.SelectedIndex = 0;

            // dropdownlist of summary of the appliances in the warehouses ( summary section )
            ddl_warehouse_appliance_summary_list.DataSource = ddl_warehouse_list.DataSource;
            ddl_warehouse_appliance_summary_list.DataBind();
            ddl_warehouse_appliance_summary_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
            ddl_warehouse_appliance_summary_list.SelectedIndex = 0;






                DataSet dataSet2 = new DataSet();
                dataSet2.ReadXml("F:/www/web/dev/sinfoca/tiger/App_Data/Appliance_Categ.xml");

                ddl_appliance_categ.DataSource = dataSet2;
                ddl_warehouse_appliance_categ.DataSource = dataSet2;

                ddl_warehouse_appliance_summary_categ.DataSource = dataSet2;
                ddl_home_appliance_summary_categ.DataSource = dataSet2;

               
                if (Session["_lastCulture"].ToString() == "fr-FR")
                {
                    ddl_appliance_categ.DataTextField = "ac_name_fr";
                    ddl_warehouse_appliance_categ.DataTextField = "ac_name_fr";

                    ddl_warehouse_appliance_summary_categ.DataTextField = "ac_name_fr";
                    ddl_home_appliance_summary_categ.DataTextField = "ac_name_fr";
                }
                if (Session["_lastCulture"].ToString() == "en-US")
                {
                    ddl_appliance_categ.DataTextField = "ac_name_en";
                    ddl_warehouse_appliance_categ.DataTextField = "ac_name_en";

                    ddl_warehouse_appliance_summary_categ.DataTextField = "ac_name_en";
                    ddl_home_appliance_summary_categ.DataTextField = "ac_name_en";
                }

                ddl_appliance_categ.DataBind();
                ddl_warehouse_appliance_categ.DataBind();
                ddl_warehouse_appliance_summary_categ.DataBind();
                ddl_home_appliance_summary_categ.DataBind();

          



            DateTime right_now = new DateTime();
            right_now = DateTime.Now;

            tiger.Date d = new tiger.Date();

            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


            tiger.Home l = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getHomeCount(Convert.ToInt32(Session["schema_id"]));

            int home_id = l.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));

            // first we check if there's any property available
            if (home_count > 0)
            {



                tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

                DataSet dataSet3 = new DataSet();
                dataSet3.ReadXml("F:/www/web/dev/sinfoca/tiger/App_Data/Home_list.xml");


                ddl_home_list.DataSource = dataSet3;
                ddl_home_list.DataBind();
                ddl_home_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
                // ddl_home_list.SelectedValue = home_id.ToString();

                ddl_home_appliance_summary_list.DataSource = ddl_home_list.DataSource;
                ddl_home_appliance_summary_list.DataBind();
                ddl_home_appliance_summary_list.Items.Insert(0, Resources.Resource.lbl_all);



                // DropDownList pour les Unit
                //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

                if (unit_count > 0)
                {

                    int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), home_id);

                    // ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
                    // ddl_unit_id.SelectedValue = Convert.ToString(unit_id);
                    // ddl_unit_id.DataBind();
                    ddl_unit_id.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
                    ddl_unit_id.Items.Add(new ListItem(Resources.Resource.lbl_storage_unit, "-1"));
                    ddl_unit_id.SelectedIndex = 0;
                }

            }

            else
            {
                // panel_home_unit_add.Visible = false;
                txt_message.InnerHtml = "<b><a href='home/home_add.aspx'>Please add a property</a></b>";
            }


            tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            DataSet dataSet4 = new DataSet();
            dataSet4.ReadXml("F:/www/web/dev/sinfoca/tiger/App_Data/Appliance_list.xml");


            gv_appliance_list.DataSource = dataSet4;
            gv_appliance_list.DataBind();



           

            //-----------


            DataSet dataSet5 = new DataSet();
            dataSet5.ReadXml("F:/www/web/dev/sinfoca/tiger/App_Data/ApplianceSummary_list.xml");

            gv_home_appliance_summary_list.DataSource = dataSet5;
            gv_home_appliance_summary_list.DataBind();



            //-------------

            DataSet dataSet6 = new DataSet();
            dataSet6.ReadXml("F:/www/web/dev/sinfoca/tiger/App_Data/WarehouseAppliance_list.xml");


            gv_warehouse_appliance_list.DataSource = dataSet6;
            gv_warehouse_appliance_list.DataBind();




            //-----------------------------

            DataSet dataSet7 = new DataSet();
            dataSet7.ReadXml("F:/www/web/dev/sinfoca/tiger/App_Data/WarehouseApplianceSummary_list.xml");



            gv_warehouse_appliance_summary_list.DataSource = dataSet7;
            gv_warehouse_appliance_summary_list.DataBind();



            //-----------------------------


        }


    }





    private void SetDefaultView()
    {
        TabContainer1.ActiveTabIndex = 0;

    }












    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_list_SelectedIndexChanged(object sender, EventArgs e)
    {

        gv_appliance_list.Visible = true;
        gv_appliance_search_list.Visible = false;

        int home_id = 0;
        // int unit_id = 0;

        if (ddl_home_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        //    if (ddl_unit_id.SelectedIndex > 0)
        //    unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        ddl_appliance_categ.SelectedIndex = 0;

        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
        ddl_unit_id.DataBind();
        ddl_unit_id.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
        ddl_unit_id.Items.Add(new ListItem(Resources.Resource.lbl_storage_unit, "-1"));
        ddl_unit_id.SelectedIndex = 0;

        tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        gv_appliance_list.DataSource = app.getApplianceList(Convert.ToInt32(Session["schema_id"]), home_id, 0, 1);
        gv_appliance_list.DataBind();

        TabContainer1.ActiveTabIndex = 0;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_unit_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        int home_id = 0;
        int unit_id = 0;

        gv_appliance_list.Visible = true;
        gv_appliance_search_list.Visible = false;


        ddl_appliance_categ.SelectedIndex = 0;

        //  if (ddl_home_list.SelectedIndex > 0)
        home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        // if (ddl_unit_id.SelectedIndex > 0)
        unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        // if the unit selectid is not the storage unit
        if (ddl_unit_id.SelectedValue != "-1")
        {
            tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            gv_appliance_list.DataSource = app.getApplianceList(Convert.ToInt32(Session["schema_id"]), home_id, unit_id, Convert.ToInt32(ddl_appliance_categ.SelectedValue));
            gv_appliance_list.DataBind();
        }

        else
        {

            tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            gv_appliance_list.DataSource = app.getApplianceStorageUnitList(Convert.ToInt32(Session["schema_id"]), home_id, Convert.ToInt32(ddl_appliance_categ.SelectedValue));
            gv_appliance_list.DataBind();
        }

        TabContainer1.ActiveTabIndex = 0;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_appliance_categ_SelectedIndexChanged(object sender, EventArgs e)
    {
        int home_id = 0;
        int unit_id = 0;

        gv_appliance_list.Visible = true;
        gv_appliance_search_list.Visible = false;

        if (ddl_home_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        if (ddl_unit_id.SelectedIndex > 0)
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        // if the unit selectid is not the storage unit
        if (ddl_unit_id.SelectedValue != "-1")
        {
            tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            gv_appliance_list.DataSource = app.getApplianceList(Convert.ToInt32(Session["schema_id"]), home_id, unit_id, Convert.ToInt32(ddl_appliance_categ.SelectedValue));
            gv_appliance_list.DataBind();
        }

        else
        {

            tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            gv_appliance_list.DataSource = app.getApplianceStorageUnitList(Convert.ToInt32(Session["schema_id"]), home_id, Convert.ToInt32(ddl_appliance_categ.SelectedValue));
            gv_appliance_list.DataBind();
        }
        TabContainer1.ActiveTabIndex = 0;




    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_warehouse_appliance_categ_SelectedIndexChanged(object sender, EventArgs e)
    {
        /* int home_id = 0;
         int unit_id = 0;

         gv_appliance_list.Visible = true;
         gv_appliance_search_list.Visible = false;

         if (ddl_home_list.SelectedIndex > 0)
             home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

         if (ddl_unit_id.SelectedIndex > 0)
             unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);
  */

        gv_warehouse_appliance_list.Visible = true;
        gv_warehouse_appliance_search_list.Visible = false;

        tiger.Appliance app = new tiger.Appliance(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_warehouse_appliance_list.DataSource = app.getWarehouseApplianceList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_warehouse_list.SelectedValue), Convert.ToInt32(ddl_warehouse_appliance_categ.SelectedValue));
        gv_warehouse_appliance_list.DataBind();

        TabContainer1.ActiveTabIndex = 1;

    }


    protected void gv_appliance_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

      /*
             // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
             tiger.Lease paid = new tiger.Lease(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
             gv_archive_lease_list.PageIndex = e.NewPageIndex;
             gv_archive_lease_list.DataSource = paid.getLeaseArchiveList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_list.SelectedValue), 0, right_now);
             gv_archive_lease_list.DataBind();
        */
        int home_id = 0;
        int unit_id = 0;

        if (ddl_home_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        if (ddl_unit_id.SelectedIndex > 0)
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        gv_appliance_list.PageIndex = e.NewPageIndex;
        gv_appliance_list.DataSource = app.getApplianceList(Convert.ToInt32(Session["schema_id"]), home_id, unit_id, Convert.ToInt32(ddl_appliance_categ.SelectedValue));
        gv_appliance_list.DataBind();

        TabContainer1.ActiveTabIndex = 0;

    }


    protected void btn_submit_Click(object sender, EventArgs e)
    {
        gv_appliance_list.Visible = false;
        gv_appliance_search_list.Visible = true;


        string name = "";
        tiger.Appliance appliance = new tiger.Appliance(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));



        gv_appliance_search_list.DataSource = appliance.getApplianceSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(radio_appliance_search.SelectedValue), tbx_appliance_search.Text);
        gv_appliance_search_list.DataBind();

        TabContainer1.ActiveTabIndex = 0;
    }


    protected void gv_appliance_search_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {


        int home_id = 0;
        int unit_id = 0;

        if (ddl_home_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        if (ddl_unit_id.SelectedIndex > 0)
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        tiger.Appliance appliance = new tiger.Appliance(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_appliance_search_list.PageIndex = e.NewPageIndex;
        gv_appliance_search_list.DataSource = appliance.getApplianceSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(radio_appliance_search.SelectedValue), tbx_appliance_search.Text);
        gv_appliance_search_list.DataBind();

        TabContainer1.ActiveTabIndex = 0;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_warehouse_list_SelectedIndexChanged(object sender, EventArgs e)
    {

        gv_warehouse_appliance_list.Visible = true;
        gv_warehouse_appliance_search_list.Visible = false;

        tiger.Appliance app = new tiger.Appliance(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_warehouse_appliance_list.DataSource = app.getWarehouseApplianceList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_warehouse_list.SelectedValue), Convert.ToInt32(ddl_warehouse_appliance_categ.SelectedValue));
        gv_warehouse_appliance_list.DataBind();

        TabContainer1.ActiveTabIndex = 1;

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_warehouse_submit_Click(object sender, EventArgs e)
    {
        gv_warehouse_appliance_list.Visible = false;
        gv_warehouse_appliance_search_list.Visible = true;



        tiger.Appliance appliance = new tiger.Appliance(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_warehouse_appliance_search_list.DataSource = appliance.getWarehouseApplianceSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(radio_warehouse_search.SelectedValue), tbx_warehouse_search.Text);
        gv_warehouse_appliance_search_list.DataBind();

        TabContainer1.ActiveTabIndex = 1;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_warehouse_appliance_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        gv_warehouse_appliance_list.Visible = true;
        gv_warehouse_appliance_search_list.Visible = false;

        tiger.Appliance app = new tiger.Appliance(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_warehouse_appliance_list.PageIndex = e.NewPageIndex;
        gv_warehouse_appliance_list.DataSource = app.getWarehouseApplianceList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_warehouse_list.SelectedValue), Convert.ToInt32(ddl_warehouse_appliance_categ.SelectedValue));
        gv_warehouse_appliance_list.DataBind();

        TabContainer1.ActiveTabIndex = 1;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_warehouse_appliance_search_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_warehouse_appliance_list.Visible = false;
        gv_warehouse_appliance_search_list.Visible = true;



        tiger.Appliance appliance = new tiger.Appliance(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_warehouse_appliance_search_list.PageIndex = e.NewPageIndex;
        gv_warehouse_appliance_search_list.DataSource = appliance.getWarehouseApplianceSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(radio_warehouse_search.SelectedValue), tbx_warehouse_search.Text);
        gv_warehouse_appliance_search_list.DataBind();

        TabContainer1.ActiveTabIndex = 1;

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_appliance_summary_list_SelectedIndexChanged(object sender, EventArgs e)
    {

        int home_id = 0;


        if (ddl_home_appliance_summary_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_appliance_summary_list.SelectedValue);

        tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        gv_home_appliance_summary_list.DataSource = app.getApplianceSummaryList(Convert.ToInt32(Session["schema_id"]), home_id, Convert.ToInt32(ddl_home_appliance_summary_categ.SelectedValue));
        gv_home_appliance_summary_list.DataBind();

        TabContainer1.ActiveTabIndex = 2;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_appliance_summary_categ_SelectedIndexChanged(object sender, EventArgs e)
    {
        int home_id = 0;


        if (ddl_home_appliance_summary_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_appliance_summary_list.SelectedValue);

        tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        gv_home_appliance_summary_list.DataSource = app.getApplianceSummaryList(Convert.ToInt32(Session["schema_id"]), home_id, Convert.ToInt32(ddl_home_appliance_summary_categ.SelectedValue));
        gv_home_appliance_summary_list.DataBind();

        TabContainer1.ActiveTabIndex = 2;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_warehouse_appliance_summary_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        int home_id = 0;


        if (ddl_home_appliance_summary_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_appliance_summary_list.SelectedValue);

        tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        gv_warehouse_appliance_summary_list.DataSource = app.getWarehouseApplianceSummaryList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_warehouse_appliance_summary_list.SelectedValue), Convert.ToInt32(ddl_warehouse_appliance_summary_categ.SelectedValue));
        gv_warehouse_appliance_summary_list.DataBind();

        TabContainer1.ActiveTabIndex = 2;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_warehouse_appliance_summary_categ_SelectedIndexChanged(object sender, EventArgs e)
    {
        int home_id = 0;


        if (ddl_home_appliance_summary_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_appliance_summary_list.SelectedValue);

        tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        gv_warehouse_appliance_summary_list.DataSource = app.getWarehouseApplianceSummaryList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_warehouse_appliance_summary_list.SelectedValue), Convert.ToInt32(ddl_warehouse_appliance_summary_categ.SelectedValue));
        gv_warehouse_appliance_summary_list.DataBind();

        TabContainer1.ActiveTabIndex = 2;
    }

}
