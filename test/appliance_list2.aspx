﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="appliance_list2.aspx.cs" Inherits="test_appliance_list2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="~/uc/xsrf.ascx" TagName="xsrf" TagPrefix="xsrf" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager> 
    <br />
 <b> <asp:Label ID="Label13" runat="server" 
                                   Text="Items" /></b><br /><br />   
   
   <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" Width="99%"  >
    <cc1:TabPanel ID="tab1" runat="server" HeaderText="<%$ Resources:Resource,lbl_property %>"  >
     <ContentTemplate  >
      
       <table style="width:90%">
           <tr>
               <td bgcolor="AliceBlue" style="font-size: small">
                   <b><asp:Label ID="Label1" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_property %>" /></b></td>
           </tr>
       </table>
       </span>
       <div ID="txt_message" runat="server">
       </div>
       <table cellspacing="1" style="width: 83%">
           <tr>
               <td>
                   <table >
                       <tr>
                           <td>
                               <b><asp:Label ID="Label2" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_search_by %>" /></b></td>
                           <td>
                               &nbsp;</td>
                           <td>
                               &nbsp;</td>
                           <td>
                               &nbsp;</td>
                       </tr>
                       <tr>
                           <td>
                               <asp:Label ID="lbl_property" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_property %>" />
                           </td>
                           <td>
                               :
                               <asp:DropDownList ID="ddl_home_list" runat="server" AutoPostBack="True" 
                                   DataTextField="home_name" DataValueField="home_id" 
                                   OnSelectedIndexChanged="ddl_home_list_SelectedIndexChanged">
                               </asp:DropDownList>
                               &nbsp;</td>
                           <td>
                               &nbsp;</td>
                           <td>
                               &nbsp;</td>
                       </tr>
                       <tr>
                           <td>
                               <asp:Label ID="lbl_unit" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_unit %>" />
                           </td>
                           <td colspan="3" valign="top">
                               :
                               <asp:DropDownList ID="ddl_unit_id" runat="server" AutoPostBack="True" 
                                   DataTextField="unit_door_no" DataValueField="unit_id" 
                                   OnSelectedIndexChanged="ddl_unit_id_SelectedIndexChanged" />
                               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                       </tr>
                       <tr>
                           <td>
                               <asp:Label ID="Label16" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_category %>" />
                           </td>
                           <td colspan="3" valign="top">
                               :
                               <asp:DropDownList ID="ddl_appliance_categ" runat="server" AutoPostBack="True" 
                                   DataValueField="ac_id" 
                                   onselectedindexchanged="ddl_appliance_categ_SelectedIndexChanged">
                               </asp:DropDownList>
                           </td>
                       </tr>
                   </table>
               </td>
               <td valign="top">
                   <table  cellpadding="0" cellspacing="1" style="width: 83%">
                       <tr>
                           <td>
                             
                               <asp:Label ID="Label116" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_quick_search_inv_serial %>"  /></td>
                       </tr>
                       <tr>
                           <td>
                               <b>
                               <asp:RadioButtonList ID="radio_appliance_search" runat="server" 
                                   RepeatDirection="Horizontal">
                                  <asp:ListItem  Text="<%$ Resources:Resource,lbl_starts_with %>" Selected="True" Value="1"></asp:ListItem>
                                        <asp:ListItem  Text="<%$ Resources:Resource,lbl_contains %>" Value="2"></asp:ListItem>
                                    
                               </asp:RadioButtonList>
                               </b>
                           </td>
                       </tr>
                       <tr>
                           <td style="width: 316px" valign="top">
                               <asp:TextBox ID="tbx_appliance_search" runat="server"></asp:TextBox>
                               &nbsp;&nbsp;&nbsp;&nbsp;
                               <asp:Button ID="Button1" runat="server" onclick="btn_submit_Click" 
                                   Text="Search" />
                           </td>
                       </tr>
                   </table>
               </td>
           </tr>
       </table>
       <table width="90%">
           <tr>
               <td style="color: #D3D3D3">
                   _________________________________________________________________________________________</td>
           </tr>
       </table>
       <br />
       
         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
         <ContentTemplate>       
        <div>
           <asp:GridView ID="gv_appliance_list" runat="server" AllowPaging="True" AutoGenerateColumns="False" 
               EmptyDataText="<%$ Resources:Resource, lbl_no_data %>" 
               OnPageIndexChanging="gv_appliance_list_PageIndexChanging" 
               Width="100%">
               <Columns>
                  
                   <asp:BoundField DataField="appliance_name" HeaderText="<%$ Resources:Resource, lbl_appliance_name %>"
                       SortExpression="ac_name_en" />
                   <asp:BoundField DataField="ac_name_en" HeaderText="<%$ Resources:Resource, lbl_category %>" 
                       SortExpression="ac_name_en" />
                   <asp:BoundField DataField="appliance_serial_no" HeaderText="<%$ Resources:Resource, lbl_serial_no %>" />
                    <asp:BoundField DataField="home_name" 
                       HeaderText="<%$ Resources:Resource, lbl_property %>"  />
                   <asp:BoundField DataField="unit_door_no" HeaderText="<%$ Resources:Resource, lbl_unit %>"/>
                   <asp:BoundField DataField="ua_dateadd" DataFormatString="{0:M-dd-yyyy}"
                                     HeaderText="<%$ Resources:Resource, lbl_since %>" />
                   <asp:HyperLinkField DataNavigateUrlFields="appliance_id" 
                       DataNavigateUrlFormatString="~/manager/appliance/appliance_view.aspx?appliance_id={0}" 
                       HeaderText="<%$ Resources:Resource, lbl_view %>" 
                       Text="<%$ Resources:Resource, lbl_view %>" />
                       
                       
                        <asp:HyperLinkField DataNavigateUrlFields="appliance_id" 
                       DataNavigateUrlFormatString="~/manager/appliance/appliance_update.aspx?appliance_id={0}" 
                       HeaderText="Edit" 
                       Text="Edit" />
                      
               </Columns>
           </asp:GridView>
           <br />
           <asp:GridView ID="gv_appliance_search_list" runat="server" 
                AllowPaging="True" AutoGenerateColumns="False" 
               EmptyDataText="No Data" 
               OnPageIndexChanging="gv_appliance_search_list_PageIndexChanging" 
               Width="100%">
               <Columns>
                    
                   <asp:BoundField DataField="appliance_name" HeaderText="<%$ Resources:Resource, lbl_appliance_name %>"
                       SortExpression="ac_name_en" />
                   <asp:BoundField DataField="ac_name_en" HeaderText="<%$ Resources:Resource, lbl_category %>" 
                       SortExpression="ac_name_en" />
                   <asp:BoundField DataField="appliance_serial_no" HeaderText="<%$ Resources:Resource, lbl_serial_no %>" />
                   <asp:BoundField DataField="home_name" HeaderText="<%$ Resources:Resource, lbl_property %>" />
                   <asp:BoundField DataField="unit_door_no" HeaderText="<%$ Resources:Resource, lbl_unit %>"/>
                   <asp:BoundField DataField="ua_dateadd" DataFormatString="{0:M-dd-yyyy}"
                      HeaderText="<%$ Resources:Resource, lbl_since %>" />
                   <asp:HyperLinkField DataNavigateUrlFields="appliance_id" 
                       DataNavigateUrlFormatString="~/manager/appliance/appliance_view.aspx?appliance_id={0}" 
                       HeaderText="<%$ Resources:Resource, lbl_view %>" 
                       Text="<%$ Resources:Resource, lbl_view %>" />
                       
                       
                        <asp:HyperLinkField DataNavigateUrlFields="appliance_id" 
                       DataNavigateUrlFormatString="~/manager/appliance/appliance_update.aspx?appliance_id={0}" 
                       HeaderText="Edit" 
                       Text="Edit" />
                      
               </Columns>
           </asp:GridView>
       </div>
      
      </ContentTemplate>
    </asp:UpdatePanel>
   </ContentTemplate  >
</cc1:TabPanel>
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
     
  <cc1:TabPanel ID="tab2" runat="server" HeaderText="<%$ Resources:Resource,lbl_warehousing %>"  >
     <ContentTemplate  >
        <div>
            <table style="width: 100%">
                <tr>
                    <td bgcolor="AliceBlue" style="font-size: small">
                        <b><asp:Label ID="Label3" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_external_storage %>"  /></b></td>
                </tr>
            </table>
            <table cellspacing="1" style="width: 100%">
                <tr>
                    <td valign="top">
                        <table >
                            <tr>
                                <td>
                                    <b><asp:Label ID="Label4" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_search_by %>"  /></b></td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                  <asp:Label ID="Label5" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_external_storage %>"  /></td>
                                <td valign="top">
                                    :
                                    <asp:DropDownList ID="ddl_warehouse_list" runat="server" AutoPostBack="True" 
                                        DataTextField="warehouse_name" DataValueField="warehouse_id" 
                                        OnSelectedIndexChanged="ddl_warehouse_list_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    &nbsp;&nbsp; &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label6" runat="server" 
                                        Text="<%$ Resources:Resource, lbl_category %>" />
                                </td>
                                <td valign="top">
                                    :
                                    <asp:DropDownList ID="ddl_warehouse_appliance_categ" runat="server" 
                                        AutoPostBack="True" DataValueField="ac_id" 
                                        
                                        onselectedindexchanged="ddl_warehouse_appliance_categ_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table  style="width: 100%">
                            <tr>
                                <td>
                                    <b><asp:Label ID="Label7" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_quick_search_inv_serial %>"  /></b></td>
                            </tr>
                            <tr>
                                <td style="height: 25px">
                                    <b>
                                    <asp:RadioButtonList ID="radio_warehouse_search" runat="server" 
                                        RepeatDirection="Horizontal">
                                       <asp:ListItem  Text="<%$ Resources:Resource,lbl_starts_with %>" Selected="True" Value="1"></asp:ListItem>
                                        <asp:ListItem  Text="<%$ Resources:Resource,lbl_contains %>" Value="2"></asp:ListItem>
                                    </asp:RadioButtonList>
                                    </b>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <asp:TextBox ID="tbx_warehouse_search" runat="server"></asp:TextBox>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Button ID="btn_warehouse" runat="server" 
                                        onclick="btn_warehouse_submit_Click" Text="Search" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <asp:GridView ID="gv_warehouse_appliance_list" runat="server" 
                AllowPaging="True" AutoGenerateColumns="false"
                EmptyDataText="<%$ Resources:Resource, lbl_no_data %>" 
                OnPageIndexChanging="gv_warehouse_appliance_list_PageIndexChanging" 
                Width="100%">
                <Columns>
                    <asp:BoundField DataField="warehouse_name" HeaderText="<%$ Resources:Resource, lbl_external_storage %>"/>
                    <asp:BoundField DataField="appliance_name" HeaderText="<%$ Resources:Resource, lbl_appliance_name %>"
                        SortExpression="ac_name_en" />
                    <asp:BoundField DataField="ac_name_en" HeaderText="<%$ Resources:Resource, lbl_category %>"
                        SortExpression="ac_name_en" />
                    <asp:BoundField DataField="appliance_serial_no" HeaderText="<%$ Resources:Resource, lbl_serial_no %>" />
                    <asp:BoundField DataField="ua_dateadd" DataFormatString="{0:M-dd-yyyy}"
                       HeaderText="<%$ Resources:Resource, lbl_since %>" />
                    <asp:HyperLinkField DataNavigateUrlFields="appliance_id" 
                        DataNavigateUrlFormatString="~/manager/appliance/appliance_view.aspx?appliance_id={0}" 
                        HeaderText="<%$ Resources:Resource, lbl_view %>" 
                        Text="<%$ Resources:Resource, lbl_view %>" />
                        
                        
                        <asp:HyperLinkField DataNavigateUrlFields="appliance_id" 
                       DataNavigateUrlFormatString="~/manager/appliance/appliance_update.aspx?appliance_id={0}" 
                       HeaderText="Edit" 
                       Text="Edit" />
                     
                </Columns>
            </asp:GridView>
            <asp:GridView ID="gv_warehouse_appliance_search_list" runat="server" 
                AllowPaging="True" 
                AutoGenerateColumns="False"
                EmptyDataText="<%$ Resources:Resource, lbl_no_data %>" 
                OnPageIndexChanging="gv_warehouse_appliance_search_list_PageIndexChanging" 
                Width="100%">
                <Columns>
                     <asp:BoundField DataField="warehouse_name" HeaderText="<%$ Resources:Resource, lbl_external_storage %>"/>
                    <asp:BoundField DataField="appliance_name" HeaderText="<%$ Resources:Resource, lbl_appliance_name %>"
                        SortExpression="ac_name_en" />
                    <asp:BoundField DataField="ac_name_en" HeaderText="<%$ Resources:Resource, lbl_category %>"
                        SortExpression="ac_name_en" />
                    <asp:BoundField DataField="appliance_serial_no" HeaderText="<%$ Resources:Resource, lbl_serial_no %>" />
                    <asp:BoundField DataField="ua_dateadd" DataFormatString="{0:M-dd-yyyy}"
                            HeaderText="<%$ Resources:Resource, lbl_since %>" />
                    <asp:HyperLinkField DataNavigateUrlFields="appliance_id" 
                        DataNavigateUrlFormatString="~/manager/appliance/appliance_view.aspx?appliance_id={0}" 
                        HeaderText="<%$ Resources:Resource, lbl_view %>" 
                        Text="<%$ Resources:Resource, lbl_view %>" />
                        
                        
                        
                        <asp:HyperLinkField DataNavigateUrlFields="appliance_id" 
                       DataNavigateUrlFormatString="~/manager/appliance/appliance_update.aspx?appliance_id={0}" 
                       HeaderText="Edit" 
                       Text="Edit" />
                        
                </Columns>
            </asp:GridView>
        </div>
        
    </ContentTemplate  >
</cc1:TabPanel>
    
    
    
    
  
    
   <cc1:TabPanel ID="tab3" runat="server" HeaderText="<%$ Resources:Resource,lbl_summary %>"  >
     <ContentTemplate  >
        <div>
        
        <table style="width: 100%">
            <tr>
                <td bgcolor="AliceBlue" style="font-size: small">
                    <b><asp:Label ID="Label8" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_summary%>"  /></b></td>
            </tr>
            </table>
            
    </div>
    <table style="width: 100%">
        <tr>
            <td valign="top">
    <table width="90%" >
        <tr>
            <td  >
      
                <asp:Label ID="Label9" runat="server" 
                    Text="<%$ Resources:Resource, lbl_property%>" />
      
                </td>
            <td valign="top"  >
                :
                <asp:DropDownList ID="ddl_home_appliance_summary_list" runat="server" 
                    AutoPostBack="true" DataTextField="home_name" DataValueField="home_id" 
                    OnSelectedIndexChanged="ddl_home_appliance_summary_list_SelectedIndexChanged">
                </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
            <td  >
                &nbsp;</td>
        </tr>
        <tr>
            <td>
      
                <asp:Label ID="Label10" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_category%>"  /></td>
            <td valign="top"  >
                :                 <asp:DropDownList AutoPostBack="true" DataValueField="ac_id" 
                    ID="ddl_home_appliance_summary_categ" runat="server" 
                    
                    OnSelectedIndexChanged="ddl_home_appliance_summary_categ_SelectedIndexChanged">
        </asp:DropDownList>
                </td>
            <td  >
                &nbsp;</td>
        </tr>
                
        </table>
                              </td>
            <td valign="top">
    <table width="90%" >
        <tr>
            <td>
      
               
                <asp:Label ID="Label11" runat="server" 
                    Text="<%$ Resources:Resource, lbl_external_storage%>" />
               </td>
            <td>
                <asp:DropDownList ID="ddl_warehouse_appliance_summary_list" runat="server" 
                    AutoPostBack="true" DataTextField="warehouse_name" 
                    DataValueField="warehouse_id" 
                    OnSelectedIndexChanged="ddl_warehouse_appliance_summary_list_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
      
                <asp:Label ID="Label12" runat="server" 
                    Text="<%$ Resources:Resource, lbl_category%>" />
            </td>
            <td valign="top">
                &nbsp;<asp:DropDownList 
                    AutoPostBack="true" ID="ddl_warehouse_appliance_summary_categ" runat="server"   
                        DataValueField="ac_id" 
                        
                    onselectedindexchanged="ddl_warehouse_appliance_summary_categ_SelectedIndexChanged">
                        
        </asp:DropDownList>
    
    
    
    
                </td>
        </tr>
                
        </table>

                              </td>
        </tr>
        <tr>
            <td valign="top">
    <asp:GridView ID="gv_home_appliance_summary_list"  runat="server" AutoGenerateColumns="false"
          Width="90%"
         EmptyDataText="<%$ Resources:Resource, lbl_no_data%>" GridLines="Both"  >
        <Columns>
            <asp:BoundField DataField="ac_name_en" HeaderText="<%$ Resources:Resource, lbl_category%>" 
                SortExpression="ac_name_en" />
            
            <asp:BoundField DataField="quantity" HeaderText="<%$ Resources:Resource, lbl_quantity%>" 
                SortExpression="quantity" />
            <asp:BoundField DataField="total_cost" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, lbl_total_cost%>"   
                SortExpression="<%$ Resources:Resource, lbl_total_cost%>" />
        </Columns>
    </asp:GridView>
                         
                              </td>
            <td valign="top">
    <asp:GridView ID="gv_warehouse_appliance_summary_list" runat="server" AutoGenerateColumns="false"
            
         EmptyDataText="<%$ Resources:Resource, lbl_no_data%>" GridLines="Both" > 
        <Columns>
            <asp:BoundField DataField="ac_name_en" HeaderText="<%$ Resources:Resource, lbl_category%>" 
                SortExpression="ac_name_en" />
            
            <asp:BoundField DataField="quantity" HeaderText="<%$ Resources:Resource, lbl_quantity%>" 
                SortExpression="quantity" />
            <asp:BoundField DataField="total_cost" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, lbl_total_cost%>"   
                SortExpression="<%$ Resources:Resource, lbl_total_cost%>" />
        </Columns>
    </asp:GridView>
    
            </td>
        </tr>
    </table>
  </ContentTemplate  >
</cc1:TabPanel>
    
    
</cc1:TabContainer>
    <xsrf:xsrf ID="Xsrf1" runat="server" />
</asp:Content>