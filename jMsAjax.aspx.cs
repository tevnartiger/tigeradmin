﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Script.Services;
using System.Collections.Generic;



public partial class jMsAjax : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
      //  Response.Redirect("login.aspx");
    }


    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    [System.Web.Services.WebMethod(CacheDuration = 0, EnableSession = true)]
    public static DateTime getTime(DateTime date_in)
    {
        if (HttpContext.Current.Session["name_id"] == null)
        {
            HttpContext.Current.Session.Abandon();
            HttpContext.Current.Response.Redirect("~/login.aspx", true);
        }

         return date_in.AddDays(5);
    }


    [System.Web.Script.Services.ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    [System.Web.Services.WebMethod]
    public static Dictionary<string, object>  getAppliance()
    {   
        DataTable dt = new DataTable();
        if (HttpContext.Current.Session["name_id"] == null)
        {
            HttpContext.Current.Session.Abandon();
            return sinfoca.tiger.utils.JsonMethods.ToJson(dt);
        }

       // HttpContext.Current.Session["schema_id"] = 1;
        tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        return sinfoca.tiger.utils.JsonMethods.ToJson(app.getApplianceList(Convert.ToInt32(HttpContext.Current.Session["schema_id"]), 0, 0, 1));
       
    }





}
