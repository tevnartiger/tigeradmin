﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="create_account_30.aspx.cs" Inherits="create_account_30" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css"  >
    div.row {float: left; margin:  0px 0px 8px 0px; padding: 0; width: 98%;}
    div.col500px {float: left; width: 500px; margin: 0 3px 0 0; padding: 0;}
    div.col35em {float: left; width:35em; margin: 0 3px 0 0; padding: 0;}
    div.col50em {float: left; width:50em; margin: 0 3px 0 0; padding: 0;}
    </style>
   
    <link  href="App_Themes/SinfoTiger/myform.css" rel="Stylesheet" />
</head>
<body>
  <br /><br />
    <div id="stylized" class="myform600">
    <form id="form"  runat="server">
    
    <h1>
        <asp:Image ID="Image1" runat="server" ImageUrl="~/login2_files/logo.png" />
    </h1>
    <h1>&nbsp;Create an account&nbsp; ( Dev. )</h1>
    <p>&nbsp;</p>
        <div id="Div3" class="row">
        <div class="col35em">
        <label >

First name<span class="small"><asp:RequiredFieldValidator Display="Dynamic"  
                ControlToValidate="name_fname" runat="server" 
                ErrorMessage="<%$Resources:resource, iv_required %>" 
                ID="RequiredFieldValidator1" />
<asp:RegularExpressionValidator Display="Dynamic" ID="reg_name_fname" ControlToValidate="name_fname" runat="server" ErrorMessage="<%$Resources:resource, iv_invalid_char %>" />
            </span></label>&nbsp;
        <asp:TextBox MaxLength="50"  Width="150" ID="name_fname" runat="server" />
        </div>  
        </div> 

    <label>
    Last name<span class="small">
<asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="name_lname" 
        runat="server" ErrorMessage="Field cannot be empty" 
        ID="RequiredFieldValidator2" />
<asp:RegularExpressionValidator Display="dynamic" ID="reg_name_lname" ControlToValidate="name_lname" runat="server" />
    </span>
    </label>
    <asp:TextBox MaxLength="50" Width="150"  ID="name_lname" runat="server" />
  
   
   
       <div id="Div1" class="row">
        <div class="col35em">
            <label >&nbsp;E-mail&nbsp;         <span class="small">
<asp:RequiredFieldValidator ID="RequiredFieldValidator3" Display="Dynamic"  
                ControlToValidate="name_email" runat="server" 
                ErrorMessage="<%$Resources:resource, iv_required %>" />
<asp:RegularExpressionValidator Display="Dynamic" ID="reg_name_email" ControlToValidate="name_email" runat="server" ErrorMessage="<%$Resources:resource, iv_invalid_email %>" />
            </span></label>&nbsp;
         <asp:TextBox MaxLength="50" width="150"  ID="name_email" runat="server" />&nbsp;
            </div>  
        </div> 
   
    
    
       <div id="Div5" class="row">
        <div class="col35em">
        <label >User name
        <span class="small">
<asp:RequiredFieldValidator ID="RequiredFieldValidator4" Display="Dynamic" 
                ControlToValidate="login_user" runat="server" 
                ErrorMessage="Field cannot be empty" />
<asp:RegularExpressionValidator Display="dynamic" ID="reg_login_user" ControlToValidate="login_user" runat="server" />
        </span>
        </label>&nbsp;
               <asp:TextBox MaxLength="50" Width="150"  ID="login_user" runat="server" />
        </div>  
        </div> 
    
       <div id="Div2" class="row">
        <div class="col35em">
        <label >Password
        <span class="small">
<asp:RequiredFieldValidator Display="Dynamic"  ControlToValidate="login_pwd" runat="server" 
                ErrorMessage="Field cannot be empty" ID="RequiredFieldValidator5" />
<asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="login_pwd" ID="reg_login_pwd" runat="server" ErrorMessage="<%$Resources:resource, iv_invalid_char %>" />
        </span>
        </label>&nbsp;
            <asp:TextBox MaxLength="50" Width="150" TextMode="password" ID="login_pwd" runat="server" />
      
        </div>  
        </div> 
        
         <div id="Div4" class="row">
        <div class="col35em">
        <label >Re-type password
        <span class="small">
 <asp:CompareValidator ControlToCompare="login_pwd" Display="Dynamic" ControlToValidate="retype_login_pwd" ID="CompareValidator1" runat="server" ErrorMessage="CompareValidator" />
<asp:RequiredFieldValidator  ControlToValidate="retype_login_pwd" runat="server" 
                ErrorMessage="Field cannot be empty" ID="RequiredFieldValidator6" /> 
<asp:RegularExpressionValidator ControlToValidate="retype_login_pwd" Display="Dynamic" ID="reg_retype_login_pwd" runat="server" ErrorMessage="<%$Resources:resource, iv_invalid_char %>" />
        </span>
        </label>&nbsp;
        <asp:TextBox MaxLength="50" Width="150" TextMode="password" ID="retype_login_pwd" runat="server" /> 
        </div>  
        </div> 
        
        
        
        <div id="Div6" class="row">
        <div class="col35em">
        <label >&nbsp;&nbsp;&nbsp;&nbsp;
        </label>
            &nbsp;
            <asp:Button ID="bn_create" runat="server" Text="Create Account" 
                OnClick="bn_create_Click" Width="145px" /> 
      
        </div>  
        </div> 
        
        
        
        <div id="Div7" class="row">
        <div class="col50em">
        <label >&nbsp;&nbsp;&nbsp;&nbsp;
        </label>
            &nbsp;
            <asp:Label ID="txt" runat="server" /> 

        </div>  
        </div> 
 <div class="spacer"></div>
    </form>
    </div>
</body>
</html>
