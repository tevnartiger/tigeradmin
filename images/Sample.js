function IconChanger(){
	event.srcElement.src=event.srcElement.src.substr(0,event.srcElement.src.lastIndexOf("_"))+(event.type=='mouseover'?"_active.gif":"_inactive.gif");
}
function collapseSection(oImage, id) {
	if (document.getElementById) { // DOM3 = IE5, NS6
		var oDiv =document.getElementById(id)
		oDiv.style.display = oDiv.style.display == "none" ? "block" : "none";
		
		oImage.title = oDiv.style.display == "none" ? "Expand" : "Collapse";
		if(id.indexOf("div_100")>-1)
			oImage.src = oDiv.style.display == "none" ? "images/headline_maximize_inactive.gif" : "images/headline_minimize_inactive.gif";
		else
			oImage.src = oDiv.style.display == "none" ? "../../images/headline_maximize_inactive.gif" : "../../images/headline_minimize_inactive.gif";
		
		var str2 = "span_" + id.substr(4);
		var oSpan = this.document.getElementById(str2);
		if(oSpan)
		{
			oSpan.title = oDiv.style.display == "none" ? "Expand": "Collapse";
		}
	}
	else
	{
	if (document.layers) {	
			if (document.id.display == "none"){
				document.id.display = 'block';
				oImage.title ="Collapse";
				
			} else {
				
				document.id.display = 'none';
				oImage.title ="Expand";
			}
		} 
		else 
		{
			if (document.all.id.style.visibility == "none"){
				document.all.id.style.display = 'block';
				oImage.title ="Collapse";
			} else {
				
				document.all.id.style.display = 'none';
				oImage.title ="Expand";
			}
		}
	}

}
function collapseSectionTitle(id) {
	if (document.getElementById) { // DOM3 = IE5, NS6
		var oDiv = document.getElementById(id);
		oDiv.style.display = oDiv.style.display == "none" ? "block" : "none";
		var str = "img_" + id.substr(4);
		var oImg = this.document.getElementById(str);
		if(oImg)
		{
			oImg.title = oDiv.style.display == "none" ? "Expand": "Collapse";
			if(id.indexOf("div_100")>-1)
				oImg.src = oDiv.style.display == "none" ? "images/headline_maximize_inactive.gif" : "images/headline_minimize_inactive.gif";
			else
				oImg.src = oDiv.style.display == "none" ? "../../images/headline_maximize_inactive.gif" : "../../images/headline_minimize_inactive.gif";
				
		}
		var str2 = "span_" + id.substr(4);
		var oSpan = this.document.getElementById(str2);
		if(oSpan)
		{
			oSpan.title = oDiv.style.display == "none" ? "Expand": "Collapse";
		}
		
	}
}
function collapseAll(oImage) 
{
	if (document.getElementById) 
	{ // DOM3 = IE5, NS6
		var status = "Expand All";
		if(oImage.title=="Collapse All")
			status ="Collapse All";
		
		oImage.title = status == "Collapse All" ? "Expand All": "Collapse All";
		oImage.src = status == "Collapse All" ? "../../images/headline_maximize_inactive.gif" : "../../images/headline_minimize_inactive.gif";
		
		var spanAll = this.document.getElementById("span_All");
		if(spanAll)
		{
			spanAll.title = status == "Collapse All" ? "Expand All": "Collapse All";
		}

		for(var j=2;j< 31;j++)
		{
			var str = "div_" + j; 
			var oDiv = document.getElementById(str);
			if(oDiv)
			{
				oDiv.style.display = status == "Expand All" ? "block" : "none";
			}
			var str2 = "img_" + j;
			var oImg = this.document.getElementById(str2);
			if(oImg)
			{
				oImg.title = status == "Collapse All" ? "Expand": "Collapse";
				oImg.src = status == "Collapse All" ? "../../images/headline_maximize_inactive.gif" : "../../images/headline_minimize_inactive.gif";
				
			}
			var str3 = "span_" + j;
			var oSpan = this.document.getElementById(str3);
			if(oSpan)
			{
				oSpan.title = status == "Collapse All" ? "Expand": "Collapse";
				
			}
			
		}
	}
	else
	{
		if (document.layers) 
		{	
			if (document.id.display == "none"){
				document.id.display = 'block';
				oImage.title = "Collapse";
				oImage.src = "../../images/headline_minimize_inactive.gif";
			} else {
					
				document.id.display = 'none';
				oImage.title = "Expand";
				oImage.src = "../../images/headline_maximize_inactive.gif";
			}
		} 
		else 
		{
			if (document.all.id.style.visibility == "none"){
				document.all.id.style.display = 'block';
				oImage.title = "Collapse";
				oImage.src = "../../images/headline_minimize_inactive.gif";
			} 
			else 
			{
				oImage.title = "Expand";
				oImage.src = "../../images/headline_maximize_inactive.gif";
				document.all.id.style.display = 'none';
			}
		}
	}

}
function toggleAllTitle(span) 
{
	if (document.getElementById) 
	{ // DOM3 = IE5, NS6
		var status = "Expand All";
		if(span.title=="Collapse All")
			status ="Collapse All";
		
		span.title = status == "Collapse All" ? "Expand All": "Collapse All";
		var imgAll = this.document.getElementById("img_All");
		if(imgAll)
		{
			imgAll.title = status == "Collapse All" ? "Expand All": "Collapse All";
			imgAll.src = status == "Collapse All" ? "../../images/headline_maximize_inactive.gif" : "../../images/headline_minimize_inactive.gif";
				
		}
		
		for(var j=2;j< 31;j++)
		{
			var str = "div_" + j; 
			var oDiv = document.all(str);
			if(oDiv)
			{
				oDiv.style.display = status == "Expand All" ? "block" : "none";
			}
			var str2 = "img_" + j;
			var oImg = this.document.getElementById(str2);
			if(oImg)
			{
				oImg.title = status == "Collapse All" ? "Expand": "Collapse";
				oImg.src = status == "Collapse All" ? "../../images/headline_maximize_inactive.gif" : "../../images/headline_minimize_inactive.gif";
				
			}
			var str3 = "span_" + j;
			var oSpan = this.document.getElementById(str3);
			if(oSpan)
			{
				oSpan.title = status == "Collapse All" ? "Expand": "Collapse";
				
			}
						
		}
	}
	/*else
	{
		if (document.layers) 
		{	
			if (document.id.display == "none"){
				document.id.display = 'block';
				oImage.title = "Collapse";
				oImage.src = "../../images/headline_minimize_inactive.gif";
			} else {
					
				document.id.display = 'none';
				oImage.title = "Expand";
				oImage.src = "../../images/headline_maximize_inactive.gif";
			}
		} 
		else 
		{
			if (document.all.id.style.visibility == "none"){
				document.all.id.style.display = 'block';
				oImage.title = "Collapse";
				oImage.src = "../../images/headline_minimize_inactive.gif";
			} 
			else 
			{
				oImage.title = "Expand";
				oImage.src = "../../images/headline_maximize_inactive.gif";
				document.all.id.style.display = 'none';
			}
		}
	}*/

}


