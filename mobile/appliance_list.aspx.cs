using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class mobile_appliance_list : System.Web.UI.MobileControls.MobilePage
{
    
    void Page_Init (object sender, EventArgs e) {
    ViewStateUserKey = Session.SessionID;
   
   }
    protected void Page_Load(object sender, EventArgs e)
    {
        tiger.Appliance h = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        ol_appliance_list.DataSource = h.getApplianceList(Convert.ToInt32(Session["schema_id"]));
        ol_appliance_list.DataBind();

    }
}
