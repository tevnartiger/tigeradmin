<%@ Page Language="C#" AutoEventWireup="true" CodeFile="appliance_list.aspx.cs" Inherits="mobile_appliance_list" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="Form1" runat="server" >
        <mobile:ObjectList ID="ol_appliance_list" Runat="server" CommandStyle-StyleReference="subcommand"
                TableFields="supplier_name;ac_name_en;appliance_serial_no;home_name;unit_door_no;ua_dateadd" AutoGenerateFields=true>
            <Field DataField="supplier_name" Title="Supplier" />
             <Field DataField="ac_name_en" Title="Categ" />
             <Field  DataField="appliance_serial_no" Title="Serial number" />
          <Field  DataField="home_name" Title="Property" />
    <Field  DataField="unit_door_no" Title="Unit" />
    <Field  DataField="ua_dateadd" Title="Since" />
            
        </mobile:ObjectList> 
        
      </mobile:Form>
</body>
</html>
