﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.NameObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : jun 5 , 2007
/// </summary>
public partial class manager_tenant_tenant_update : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
      
        if (!Page.IsPostBack)
        {
            reg_name_addr.ValidationExpression = RegEx.getText();
            reg_name_addr_city.ValidationExpression = RegEx.getText();
            reg_name_addr_pc.ValidationExpression = RegEx.getText();
            reg_name_fname.ValidationExpression = RegEx.getText();
            reg_name_lname.ValidationExpression = RegEx.getText();
            reg_name_tel.ValidationExpression = RegEx.getText();
            reg_tenantapplication_current_employer.ValidationExpression = RegEx.getText();
            reg_tenantapplication_landlord_name.ValidationExpression = RegEx.getText();
            reg_tenantapplication_landlord_tel.ValidationExpression = RegEx.getText();
            reg_tenantapplication_monthly_income.ValidationExpression = RegEx.getMoney();


            tiger.Country ic = new tiger.Country(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_country_list.DataSource = ic.getCountryList();
            ddl_country_list.DataBind();

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prTenantView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            //  try
            {
                conn.Open();
                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {

                   /* DateTime date_begin = new DateTime();
                    if (dr["pt_date_begin"] != System.DBNull.Value)
                    {
                        date_begin = Convert.ToDateTime(dr["pt_date_begin"]);
                        ddl_pt_date_begin_m.SelectedValue = date_begin.Month.ToString();
                        ddl_pt_date_begin_d.SelectedValue = date_begin.Day.ToString();
                        ddl_pt_date_begin_y.SelectedValue = date_begin.Year.ToString();
                    }*/


                    DateTime name_dob = new DateTime();
                    if (dr["name_dob"] != System.DBNull.Value)
                    {
                        name_dob = Convert.ToDateTime(dr["name_dob"]);
                        ddl_name_dob_m.SelectedValue = name_dob.Month.ToString();
                        ddl_name_dob_d.SelectedValue = name_dob.Day.ToString();
                        ddl_name_dob_y.SelectedValue = name_dob.Year.ToString();
                    }

                  //name_ssn.Text = dr["name_ssn"].ToString();
                    tenantapplication_current_employer.Text = dr["tenantapplication_current_employer"].ToString();





                    name_lname.Text = dr["name_lname"].ToString();
                    name_fname.Text = dr["name_fname"].ToString();


                    name_addr.Text = dr["name_addr"].ToString();
                    name_addr_city.Text = dr["name_addr_city"].ToString();
                    name_addr_pc.Text = dr["name_addr_pc"].ToString();
                    name_addr_state.Text = dr["name_addr_state"].ToString();

                    if (dr["country_id"] != System.DBNull.Value)
                    {
                        ddl_country_list.SelectedValue = dr["country_id"].ToString();
                    }


                    name_tel.Text = dr["name_tel"].ToString();
                    name_tel_work.Text = dr["name_tel_work"].ToString();
                    name_tel_work_ext.Text = dr["name_tel_work_ext"].ToString();

                    name_cell.Text = dr["name_cell"].ToString();
                    name_fax.Text = dr["name_fax"].ToString();
                    name_email.Text = dr["name_email"].ToString();
                    name_com.Text = dr["name_com"].ToString();



                    tenantapplication_landlord_name.Text = dr["tenantapplication_landlord_name"].ToString();
                    tenantapplication_current_employer.Text = dr["tenantapplication_current_employer"].ToString();
                    tenantapplication_landlord_tel.Text = dr["tenantapplication_landlord_tel"].ToString();
                    tenantapplication_monthly_income.Text = dr["tenantapplication_monthly_income"].ToString();
                    tenantapplication_people_with_prospect.Text = dr["tenantapplication_people_with_prospect"].ToString();
                    tenantapplication_reason_depart.Text = dr["tenantapplication_reason_depart"].ToString();
                    name_ssn.Text = dr["name_ssn"].ToString();

                    DateTime date_begin_ta = new DateTime();


                    if (dr["tenantapplication_employer_since"] != System.DBNull.Value)
                    {
                        date_begin_ta = Convert.ToDateTime(dr["tenantapplication_employer_since"]);
                        ddl_tenantapplication_employer_since_m.SelectedValue = date_begin_ta.Month.ToString();
                        ddl_tenantapplication_employer_since_d.SelectedValue = date_begin_ta.Day.ToString();
                        ddl_tenantapplication_employer_since_y.SelectedValue = date_begin_ta.Year.ToString();
                    }
                  //  pt_com.Text = dr["pt_com"].ToString();



                }

                conn.Close();


            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_Click(object sender, EventArgs e)
    {

        tiger.Date d = new tiger.Date();

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prTenantUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        // try
        {
            conn.Open();

            // new tenant parameter - tenant from the database
            //cmd.Parameters.Add("@return_success", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.NVarChar, 15).Value = Request.UserHostAddress.ToString();


            cmd.Parameters.Add("@name_lname", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_lname.Text);
            cmd.Parameters.Add("@name_fname", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_fname.Text);


            cmd.Parameters.Add("@name_addr", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr.Text);
            cmd.Parameters.Add("@name_addr_city", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_city.Text);
            cmd.Parameters.Add("@name_addr_pc", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_pc.Text);
            cmd.Parameters.Add("@name_addr_state", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_state.Text);
            cmd.Parameters.Add("@country_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_country_list.SelectedValue);
            cmd.Parameters.Add("@name_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_tel.Text);
            cmd.Parameters.Add("@name_tel_work", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_tel_work.Text);
            cmd.Parameters.Add("@name_tel_work_ext", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_tel_work_ext.Text);

            cmd.Parameters.Add("@name_cell", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_cell.Text);
            cmd.Parameters.Add("@name_fax", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_fax.Text);
            cmd.Parameters.Add("@name_email", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_email.Text);
            cmd.Parameters.Add("@name_com", SqlDbType.Text).Value = RegEx.getText(name_com.Text);
            cmd.Parameters.Add("@name_dob", SqlDbType.SmallDateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_name_dob_m.SelectedValue, ddl_name_dob_d.SelectedValue, ddl_name_dob_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

            cmd.Parameters.Add("@tenantapplication_landlord_name", SqlDbType.NVarChar, 50).Value = RegEx.getText(tenantapplication_landlord_name.Text);
            cmd.Parameters.Add("@tenantapplication_current_employer", SqlDbType.NVarChar, 50).Value = RegEx.getText(tenantapplication_current_employer.Text);
            cmd.Parameters.Add("@tenantapplication_landlord_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(tenantapplication_landlord_tel.Text);
            cmd.Parameters.Add("@tenantapplication_monthly_income", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tenantapplication_monthly_income.Text));

            cmd.Parameters.Add("@tenantapplication_people_with_prospect", SqlDbType.Text).Value = RegEx.getText(tenantapplication_people_with_prospect.Text);
            cmd.Parameters.Add("@tenantapplication_reason_depart", SqlDbType.Text).Value = RegEx.getText(tenantapplication_reason_depart.Text);
            cmd.Parameters.Add("@tenantapplication_ssn", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_ssn.Text);

            cmd.Parameters.Add("@tenantapplication_employer_since", SqlDbType.SmallDateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_tenantapplication_employer_since_m.SelectedValue, ddl_tenantapplication_employer_since_d.SelectedValue, ddl_tenantapplication_employer_since_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            cmd.ExecuteReader();
            // if (Convert.ToInt32(cmd.Parameters["@return_success"].Value) == 0)
            //   result.InnerHtml = "add successful";
        }
        // finally
        {
            conn.Close();
        }
    }
}
