﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/mp_tenant.master" AutoEventWireup="true" CodeFile="incident_view.aspx.cs" Inherits="manager_incident_incident_view" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


 <p style="font-size: small">
        <b>
          <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_incident_dammage_report %>"></asp:Label></b></p>
  
  <br />
    <table cellpadding="0" cellspacing="0" style="width: 70%">
        <tr>
            <td valign="top">
              <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_title %>"></asp:Label></td>
     
            <td >
                <asp:Label ID="lbl_incident_title" runat="server" Height="16px" Width="228px"></asp:Label>
              <br /><hr />      
            </td>
        </tr>
        <tr>
            <td valign="top">
                  <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lbl_incident_datetime %>"></asp:Label>
           
            </td>
            <td >
                   <asp:Label ID="lbl_incident_date" runat="server" Height="16px" Width="229px"></asp:Label><br />
               <br /><hr />
            </td>
        </tr>
        <tr>
            <td valign="top" >
                <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lbl_property %>"></asp:Label></td>
            <td >
              <asp:Label ID="lbl_home_name" runat="server" Height="16px" Width="229px"></asp:Label><br />
                 <br /><hr />
            </td>
        </tr>
        <tr>
            <td valign="top" >
                <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lbl_unit_if_applicable %>"></asp:Label></td>
            <td >
                <asp:Label ID="lbl_unit_door_no" runat="server" Height="16px" Width="229px"></asp:Label><br />            
                <br /><hr />
            </td>
        </tr>
        <tr>
            <td valign="top" >
                <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lbl_exact_location %>"></asp:Label></td>
            <td >
                <asp:Label ID="lbl_incident_location" runat="server" Height="16px" Width="229px"></asp:Label><br />
                <hr />
            </td>
        </tr>
        <tr>
            <td valign="top" >
                <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, lbl_person_involved %>"/></td>
            <td >
                <asp:Label ID="lbl_incident_person_involved" runat="server" Height="75px" TextMode="MultiLine" 
                    Width="231px"></asp:Label>
                    <br /><hr />
            </td>
        </tr>
        <tr>
            <td valign="top" >
             <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lbl_witnesses %>"/><br />
                </td>
            <td >
                <asp:Label ID="lbl_incident_witnesses" runat="server" Height="76px" TextMode="MultiLine" 
                    Width="234px"></asp:Label>
                <hr />
            </td>
        </tr>
        <tr>
            <td valign="top" >
                <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, lbl_description %>"/></td>
            <td >
                <asp:Label ID="lbl_incident_description" runat="server" Height="141px" TextMode="MultiLine" 
                    Width="360px"></asp:Label>
                    <br /><hr />
            </td>
        </tr>
        <tr>
            <td valign="top" >
                <asp:Label ID="Label10" runat="server" Text="<%$ Resources:Resource, lbl_action_taken %>"/></td>
            <td >
                <asp:Label ID="lbl_incident_action_taken" runat="server" Height="140px" TextMode="MultiLine" 
                    Width="360px"></asp:Label>
                    <br /><hr />
            </td>
        </tr>
       
    </table>



<br />
                <asp:Label ID="lbl_confirmation" runat="server" ForeColor="Red"></asp:Label>
                <br />
                <asp:Label ID="Label12" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label13" runat="server"></asp:Label>
&nbsp;<asp:Label ID="lbl_title" runat="server"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label14" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label15" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label16" runat="server"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label17" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label18" runat="server"></asp:Label>
&nbsp;<asp:Label ID="lbl_date" runat="server"></asp:Label>
&nbsp;<br />
  
<asp:GridView HeaderStyle-BackColor="AliceBlue" Width="83%" BorderColor="White" BorderWidth="3"  ID="gv_wo_list" runat="server" AutoGenerateColumns="false"
       AllowSorting="true" GridLines="Both"   AllowPaging="true" PageSize="10"
      OnPageIndexChanging="gv_wo_list_PageIndexChanging"   AlternatingRowStyle-BackColor="Beige">
        
     <Columns>
   <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_work_order %>" DataField="wo_title"   />
   <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_property %>" DataField="home_name"   />
   <asp:BoundField HeaderText="<%$ Resources:Resource,gv_unit %>" DataField="unit_door_no"   />
   <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_date_begin %>"  DataField="wo_date_begin" DataFormatString="{0:MMM-dd-yyyy}"  
     HtmlEncode="false" />
      
      
  <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_due_date %>"  DataField="wo_exp_date_end" DataFormatString="{0:MMM-dd-yyyy}"  
     HtmlEncode="false" />
      
     <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_status%>"  >
       <ItemTemplate  >
       <asp:Label ID="lbl_wo_status"  
                  Text='<%#GetStatus(Convert.ToInt32(Eval("wo_status")))%>' 
                  runat="server" />   
       </ItemTemplate  >
    </asp:TemplateField>
       
       
       <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_priority%>"  >
       <ItemTemplate  >
       <asp:Label ID="lbl_task_priority"  
                  Text='<%#GetPriority(Convert.ToInt32(Eval("wo_priority")))%>' 
                  runat="server" />   
       </ItemTemplate  >
    </asp:TemplateField>
      
      </Columns>   
        
        
    </asp:GridView>
    <asp:Label ID="Label11" runat="server" ></asp:Label>
     


</asp:Content>

