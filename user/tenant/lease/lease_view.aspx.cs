﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.NameObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : may 13 , 2008
/// </summary>
public partial class manager_lease_lease_view : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!RegEx.IsInteger(Request.QueryString["tu_id"]) ||
            !RegEx.IsInteger(Request.QueryString["unit_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        NameObjectAuthorization tuAuthorization = new NameObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


        // CHECK IF TENANT UNIT IS AUTHORIZED
        
            ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!tuAuthorization.TenantUnit(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["tu_id"]), Convert.ToInt32(Session["name_id"]), 3))
            {
                Session.Abandon();
                Response.Redirect("~/login.aspx");
            }


        if (!tuAuthorization.Unit(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["unit_id"]), Convert.ToInt32(Request.QueryString["tu_id"]), Convert.ToInt32(Session["name_id"]), 3))
            {
                Session.Abandon();
                Response.Redirect("~/login.aspx");
            }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////              
    

        tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                                
        int temp_tenant_id = unit.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["unit_id"])); // unit_id
                           
        txt_current_tenant_name.InnerHtml = unit.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);


        /// get the rent amount
        ///////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prRentView", conn);
        SqlCommand cmd2 = new SqlCommand("prAccommodationView", conn);
        SqlCommand cmd3 = new SqlCommand("prTermsAndConditionsView", conn);
        SqlCommand cmd4 = new SqlCommand("prRentlogList", conn);
        SqlCommand cmd5 = new SqlCommand("prAccommodationList", conn);
        SqlCommand cmd6 = new SqlCommand("prTermsAndConditionsList", conn);
        SqlCommand cmd7 = new SqlCommand("prHomeUnitView", conn);

        cmd.CommandType = CommandType.StoredProcedure;


        DateTime the_date = new DateTime();
        the_date = DateTime.Now; // the date in the to drop downlist

        tiger.Date d = new tiger.Date();
        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
          

        //Add the params
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);
        cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date ;
        try
        {
            conn.Open();

            SqlDataReader dr = null;
            dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

            while (dr.Read() == true)
            {
                lbl_rent_amount.Text = Convert.ToString(Convert.ToInt32(dr["rl_rent_amount"]));
              //  ddl_rl_rent_paid_every.SelectedValue = dr["re_id"].ToString();

                DateTime la_date = new DateTime();
                la_date = Convert.ToDateTime(dr["rl_date_begin"]);

                lbl_current_rl_date_begin.Text = la_date.Month.ToString()+"-"+ la_date.Day.ToString()+"-"+ la_date.Year.ToString();

                if (dr["unit_type"].ToString() == "C" && Convert.ToInt32(dr["company_id"]) > 0)
                {
                    lbl_company.Text = dr["company_name"].ToString();
                    lbl_lease_type.Text = "Commercial";
                }
                else
                {
                    tr_company.Visible = false;
                    lbl_lease_type.Text = "Residential";
                }

                
            }
        }

        finally
        {

          //  conn.Close();
        }


        cmd2.CommandType = CommandType.StoredProcedure;
        //Add the params
        cmd2.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd2.Parameters.Add("tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);
        cmd2.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;


        try
        {
           // conn.Open();

            SqlDataReader dr2 = null;
            dr2 = cmd2.ExecuteReader(CommandBehavior.SingleRow);

            while (dr2.Read() == true)
            {

               // lbl_current_ta_date_begin.Text = String.Format("{0:d}", dr2["ta_date_begin"]);




                DateTime la_date = new DateTime();
                la_date = Convert.ToDateTime(dr2["ta_date_begin"]);

                lbl_current_ta_date_begin.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();






                if (Convert.ToInt32(dr2["ta_electricity_inc"]) == 1)
                    ta_electricity_inc.Checked = true;
                else
                    ta_electricity_inc.Checked = false;

                if (Convert.ToInt32(dr2["ta_heat_inc"]) == 1)
                    ta_heat_inc.Checked = true;
                else
                    ta_heat_inc.Checked = false;

                if (Convert.ToInt32(dr2["ta_water_inc"]) == 1)
                    ta_water_inc.Checked = true;
                else
                    ta_water_inc.Checked = false;

                if (Convert.ToInt32(dr2["ta_water_tax_inc"]) == 1)
                    ta_water_tax_inc.Checked = true;
                else
                    ta_water_tax_inc.Checked = false;

                if (Convert.ToInt32(dr2["ta_garage_inc"]) == 1)
                    ta_garage_inc.Checked = true;
                else
                    ta_garage_inc.Checked = false;

                if (Convert.ToInt32(dr2["ta_parking_inc"]) == 1)
                    ta_parking_inc.Checked = true;
                else
                    ta_parking_inc.Checked = false;

                if (Convert.ToInt32(dr2["ta_semi_furnished_inc"]) == 1)
                    ta_semi_furnished_inc.Checked = true;
                else
                    ta_semi_furnished_inc.Checked = false;

                if (Convert.ToInt32(dr2["ta_furnished_inc"]) == 1)
                    ta_furnished_inc.Checked = true;
                else
                    ta_furnished_inc.Checked = false;


                lbl_ta_com.Text = Convert.ToString(dr2["ta_com"]);





            }



        }

        finally
        {

          //  conn.Close();
        }















        cmd3.CommandType = CommandType.StoredProcedure;

        //Add the params
        // cmd.Parameters.Add("@return_guarantor_name_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd3.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);
        cmd3.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;


        try
        {
            //conn.Open();

            SqlDataReader dr3 = null;
            dr3 = cmd3.ExecuteReader(CommandBehavior.SingleRow);

            int guarantor_name_id;
            /*
            tiger.Name n = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_guarantor_name_id.DataSource = n.getNameList(Convert.ToInt32(Session["schema_id"]), 0);
            ddl_guarantor_name_id.DataBind();
            ddl_guarantor_name_id.Items.Insert(0, "select a person");
            */
            while (dr3.Read() == true)
            {

              //  lbl_current_tt_date_begin.Text = String.Format("{0:d}", dr3["tt_date_begin"]);




                DateTime la_date = new DateTime();
                la_date = Convert.ToDateTime(dr3["tt_date_begin"]);

                lbl_current_tt_date_begin.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();





                if (dr3["tt_guarantor"].ToString() == "1")
                    panel_guarantor.Visible = true;
                else
                    panel_guarantor.Visible = false;

                tt_guarantor.SelectedValue = dr3["tt_guarantor"].ToString();

                guarantor_name.Text = dr3["tt_guarantor_name"].ToString();

                if (guarantor_name.Text == "")
                    guarantor_name.Text = Resources.Resource.lbl_none;

                if (Convert.ToInt32(dr3["tt_guarantor_name_id"]) > 0)
                {
                   // ddl_guarantor_name_id.SelectedValue = Convert.ToString(dr3["tt_guarantor_name_id"]);
                  /*  guarantor_name_fname.Enabled = false;
                    guarantor_name_lname.Enabled = false;
                    guarantor_name_addr.Enabled = false;
                    guarantor_name_addr_city.Enabled = false;
                    guarantor_name_addr_pc.Enabled = false;
                    guarantor_name_addr_state.Enabled = false;
                    ddl_guarantor_country_id.Enabled = false;
                    guarantor_name_tel.Enabled = false;
                    guarantor_name_tel_work.Enabled = false;
                    guarantor_name_tel_work_ext.Enabled = false;
                    guarantor_name_cell.Enabled = false;
                    guarantor_name_fax.Enabled = false;
                    guarantor_name_email.Enabled = false;
                    guarantor_name_com.Enabled = false;
                    txt.Visible = true; */
                }
                else
                {
                  /*  guarantor_name_fname.Enabled = true;
                    guarantor_name_lname.Enabled = true;
                    guarantor_name_addr.Enabled = true;
                    guarantor_name_addr_city.Enabled = true;
                    guarantor_name_addr_pc.Enabled = true;
                    guarantor_name_addr_state.Enabled = true;
                    ddl_guarantor_country_id.Enabled = true;
                    guarantor_name_tel.Enabled = true;
                    guarantor_name_tel_work.Enabled = true;
                    guarantor_name_tel_work_ext.Enabled = true;
                    guarantor_name_cell.Enabled = true;
                    guarantor_name_fax.Enabled = true;
                    guarantor_name_email.Enabled = true;
                    guarantor_name_com.Enabled = true;
                    txt.Visible = false; */

                }



                //  dr3["tt_form_of_payment"].ToString();

                  switch ( Convert.ToInt32(dr3["tt_form_of_payment"]))
                  {
                      case 0: tt_form_of_payment.Text = Resources.Resource.lbl_not_specified;
                          break;
                      case 1: tt_form_of_payment.Text = Resources.Resource.lbl_personal_check;
                          break;
                      case 2: tt_form_of_payment.Text = Resources.Resource.lbl_cashier_check;
                          break;
                      case 3: tt_form_of_payment.Text = Resources.Resource.lbl_cash ;
                          break;
                      case 4: tt_form_of_payment.Text = Resources.Resource.lbl_money_order;
                          break;
                      case 5: tt_form_of_payment.Text = Resources.Resource.lbl_credit_card;
                          break;
                      case 6: tt_form_of_payment.Text = Resources.Resource.lbl_other;
                          break;
                  
                  }


                  decimal nsf, late_fee, security_deposit_amount;

                  if (dr3["tt_nsf"].ToString() != "")
                      nsf = Convert.ToDecimal(dr3["tt_nsf"]);
                  else
                      nsf = 0;

                  if (dr3["tt_late_fee"].ToString() != "")
                      late_fee = Convert.ToDecimal(dr3["tt_late_fee"]);
                  else
                      late_fee = 0;

                  if (dr3["tt_security_deposit_amount"].ToString() != "")
                      security_deposit_amount = Convert.ToDecimal(dr3["tt_security_deposit_amount"]);
                  else
                      security_deposit_amount = 0;


                  tt_nsf.Text = String.Format("{0:0.00}", nsf);
                  tt_late_fee.Text = String.Format("{0:0.00}", late_fee);


                  tt_security_deposit.SelectedValue = dr3["tt_security_deposit"].ToString();

                  tt_security_deposit_amount.Text = String.Format("{0:0.00}", security_deposit_amount);
                


                tt_pets.SelectedValue = dr3["tt_pets"].ToString();
                tt_maintenance.SelectedValue = dr3["tt_maintenance"].ToString();

                
                tt_specify_maintenance.Text = dr3["tt_specify_maintenance"].ToString();
                if (tt_specify_maintenance.Text == "")
                    tt_specify_maintenance.Text = Resources.Resource.lbl_none;

                tt_improvement.SelectedValue = dr3["tt_improvement"].ToString();

                tt_specify_improvement.Text = dr3["tt_specify_improvement"].ToString();
                if (tt_specify_improvement.Text == "")
                    tt_specify_improvement.Text = Resources.Resource.lbl_none;


                tt_notice_to_enter.SelectedValue = dr3["tt_notice_to_enter"].ToString();



                if (dr3["tt_specify_number_of_hours"].ToString() == "0")
                    tt_specify_number_of_hours.Text = "";
                else
                    tt_specify_number_of_hours.Text = dr3["tt_specify_number_of_hours"].ToString();




                switch (Convert.ToInt32(dr3["tt_tenant_content_ins"]))
                {

                    case 0: tt_tenant_content_ins.Text = Resources.Resource.lbl_not_specified;
                        break;
                    case 1: tt_tenant_content_ins.Text = Resources.Resource.lbl_landlord;
                        break;
                    case 2: tt_tenant_content_ins.Text = Resources.Resource.lbl_tenant;
                        break;
                    
                }


                switch (Convert.ToInt32(dr3["tt_landlord_content_ins"]))
                {

                    case 0: tt_landlord_content_ins.Text = Resources.Resource.lbl_not_specified;
                        break;
                    case 1: tt_landlord_content_ins.Text = Resources.Resource.lbl_landlord;
                        break;
                    case 2: tt_landlord_content_ins.Text = Resources.Resource.lbl_tenant;
                        break;

                }


                switch (Convert.ToInt32(dr3["tt_injury_ins"]))
                {

                    case 0: tt_injury_ins.Text = Resources.Resource.lbl_not_specified;
                        break;
                    case 1: tt_injury_ins.Text = Resources.Resource.lbl_landlord;
                        break;
                    case 2: tt_injury_ins.Text = Resources.Resource.lbl_tenant;
                        break;

                }


                switch (Convert.ToInt32(dr3["tt_premises_ins"]))
                {

                    case 0: tt_premises_ins.Text = Resources.Resource.lbl_not_specified;
                        break;
                    case 1: tt_premises_ins.Text = Resources.Resource.lbl_landlord;
                        break;
                    case 2: tt_premises_ins.Text = Resources.Resource.lbl_tenant;
                        break;

                }

                

                tt_additional_terms.Text = dr3["tt_additional_terms"].ToString();
                if (tt_additional_terms.Text  == "")
                    tt_additional_terms.Text  = Resources.Resource.lbl_none;


            }



        }

        finally
        {

            //conn.Close();
        }



       // SqlConnection conn = new SqlConnection(str_conn);
        
        cmd4.CommandType = CommandType.StoredProcedure;

        try
        {
           // conn.Open();
            cmd4.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd4.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);


            SqlDataAdapter da = new SqlDataAdapter(cmd4);
            DataSet ds = new DataSet();
            da.Fill(ds);
           
            gv_rent_list.DataSource = ds;
            gv_rent_list.DataBind();
        }
        finally
        {
           // conn.Close();
        }



        cmd5.CommandType = CommandType.StoredProcedure;

        try
        {
            // conn.Open();
            cmd5.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd5.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);


            SqlDataAdapter da = new SqlDataAdapter(cmd5);
            DataSet ds = new DataSet();
            da.Fill(ds);

            gv_accommodation_list.DataSource = ds;
            gv_accommodation_list.DataBind();
        }
        finally
        {
            // conn.Close();
        }


        cmd6.CommandType = CommandType.StoredProcedure;

        try
        {
            // conn.Open();
            cmd6.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd6.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);


            SqlDataAdapter da = new SqlDataAdapter(cmd6);
            DataSet ds = new DataSet();
            da.Fill(ds);

            gv_terms_list.DataSource = ds;
            gv_terms_list.DataBind();
        }
        finally
        {
            // conn.Close();
        }


        cmd7.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd7.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd7.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);


            SqlDataAdapter da = new SqlDataAdapter(cmd7);
            DataSet ds = new DataSet();
            da.Fill(ds);

            r_HomeUnitView.DataSource = ds;
            r_HomeUnitView.DataBind();
        }
        finally
        {
            conn.Close();
        }


    }



}
