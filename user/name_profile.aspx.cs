﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging; 
/// <summary>
/// done by : Stanley Jocelyn
/// date    : may 17 , 2009
/// </summary>
public partial class manager_name_name_profile_view : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if name id exist and is aloud to be viewed
        
        //if good format
        try
        {
            int name_id = Convert.ToInt32(Session["name_id"]);
            //verify if this id belongs to user schema
            if (sinfoca.tiger.security.ObjectSecurity.checkNameProfile(Convert.ToInt32(Session["schema_id"]), name_id))
            {
                // txt.Text = "GOOD";
                SqlConnection myConnection = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                myConnection.Open();
                string sql = "select n.name_id,n.name_lname,n.name_fname ,n.name_tel,n.name_tel_work,n.name_tel_work_ext ,n.name_cell,n.name_fax ";
                sql += " ,n.name_email,n.name_com from tname n where n.name_id = " + name_id;
                
                SqlCommand cmd = new SqlCommand(sql, myConnection);

                // cmd.Parameters.Add("@ImageId", SqlDbType.Int).Value = context.Request.QueryString["id"]; 
                cmd.Prepare();

                SqlDataReader dr = cmd.ExecuteReader();
                string str_name = "" ;
                while (dr.Read())
                {
                      str_name += Convert.ToString(dr["name_lname"])+" "+Convert.ToString(dr["name_fname"]);
                      lbl_tel.Text = Convert.ToString(dr["name_tel"]);
                      lbl_tel_work.Text = Convert.ToString(dr["name_tel_work"])+" "+Convert.ToString(dr["name_tel_work_ext"]);
                      lbl_fax.Text = Convert.ToString(dr["name_fax"]);
                      lbl_email.Text = Convert.ToString(dr["name_email"]);
                    lbl_com.Text = Convert.ToString(dr["name_com"]);
                    
                }
                dr.Close();
                lbl_name.Text = str_name;
                //GET PICTURE IF POSSIBLE
                /*
                string sql = "select n.name_lname,n.name_fname ,n.name_tel,n.name_tel_work,n.name_tel_work_ext ,n.name_cell,n.name_fax ";
                sql += " ,n.name_email,n.name_com, o.object_media from tname where n.name_id = " + name_id;
                SqlCommand cmd = new SqlCommand(sql, myConnection);

                // cmd.Parameters.Add("@ImageId", SqlDbType.Int).Value = context.Request.QueryString["id"]; 
                cmd.Prepare();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Byte[] bytes = null;
                    bytes = (Byte[])dr["object_media"];
                    Response.OutputStream.Write(bytes, 0, bytes.Length);

                    //context.Response.ContentType = dr["Image_Type"].ToString(); 
                    //context.Response.BinaryWrite((byte[])dr["Image_Content"]); 
                }
                dr.Close();
                */


                myConnection.Close();



                //----------------  STANLEY JOCELYN .. BEGIN ---------------------------
               

                 
               SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

               SqlCommand cmd_name = new SqlCommand("prNameView", conn);
               cmd_name.CommandType = CommandType.StoredProcedure;
        

                 conn.Open();

                //Add the params
                 cmd_name.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd_name.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
           
                SqlDataAdapter da = new SqlDataAdapter(cmd_name);
                DataSet ds = new DataSet();
                da.Fill(ds);


                rNameImage.DataSource = ds;
                rNameImage.DataBind();
                conn.Close();


                tiger.Name n = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                gv_person_role.DataSource = n.getNameRoleList(Convert.ToInt32(Session["schema_id"]), name_id);
                gv_person_role.DataBind();


                if (gv_name_lease_list.Rows.Count == 0)
                    lbl_user_leases.Visible = false;
                //---------------- STANLEY JOCELYN.. END ----------------------------

                

            }
            else
            { 
           
            }
        
        
        }
        catch (Exception ex)
        {
            //txt.Text = "NOT GOOD"+"<br/><br/><br/>"+ex.ToString();
        
        
        }
        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="role_id"></param>
    /// <returns></returns>
    protected DataTable GetPropertyForNameRole(int role_id)
    {
        tiger.Name n = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        return n.getPropertyForNameRole(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), role_id);

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="role_id"></param>
    /// <returns></returns>
    protected string MakeUrl(int home_id)
    {
        string url = "/manager/property/property_view.aspx?h_id=" + home_id.ToString();
        return url;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="module_id"></param>
    /// <returns></returns>
    protected string GetRoleName(int module_id)
    {
        string Module = "";

        switch (module_id)
        {
            case 3: Module = Resources.Resource.lbl_tenant;
                break;
            case 4: Module = Resources.Resource.lbl_owner;
                break;
            case 5: Module = Resources.Resource.lbl_maintenance_worker;
                break;
            case 6: Module = Resources.Resource.lbl_property_manager;
                break;
        }
        return Module;
    }



    protected void bn_upload_OnCLick(object source, EventArgs args)
    {
     // string filename = Path.GetFileName(fu_upload.PostedFile.FileName);
       // int filesize = 0;
        
        string filename = Path.GetFileName(FileUpload1.PostedFile.FileName);
        int filesize = 0;
        if (FileUpload1.HasFile)
        {

            Stream stream = FileUpload1.PostedFile.InputStream;
            filesize = FileUpload1.PostedFile.ContentLength;
            byte[] filedata = new byte[filesize];
            stream.Read(filedata, 0, filesize);

            int success = 0;
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            tiger.Media upload = new tiger.Media(strconn);

            success = upload.NameImageUpdate(filedata, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]),
                                        Convert.ToInt32(Session["name_id"]), Request.UserHostAddress.ToString());
        }


    }

    protected void  lb_upload_OnClick(object sender, EventArgs e)
{
    panel_lb.Visible = false;
    panel_upload.Visible = true;
}
    
   
   
}
   
 
/*   protected string getImage(string name_id)
    {
        string temp_return = " ";
           SqlConnection myConnection = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"])); 
  myConnection.Open(); 
  string sql = "select object_media from tobject where object_categ_id = "+name_id ; 
  SqlCommand cmd = new SqlCommand(sql, myConnection); 
 
 // cmd.Parameters.Add("@ImageId", SqlDbType.Int).Value = context.Request.QueryString["id"]; 
    cmd.Prepare(); 
  SqlDataReader dr = cmd.ExecuteReader();
  if (dr.FieldCount > 0)
  {
      while (dr.Read())
      {
          Byte[] bytes = null;
          bytes = (Byte[])dr["object_media"];
          // Response.OutputStream.Write(bytes, 0, bytes.Length);

          //context.Response.ContentType = dr["Image_Type"].ToString(); 
          //context.Response.BinaryWrite((byte[])dr["Image_Content"]); 
      }
  }
  else
  { return temp_return; }
  dr.Close();
  myConnection.Close(); 
        
    }
    */

    
