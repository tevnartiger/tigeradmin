﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.DataVisualization;
using System.Web.UI.WebControls.WebParts;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : may 29, 2009
/// </summary>

public partial class user_janitor_janitor_main : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prJanitor_Stats_Home", conn);
            SqlCommand cmd3 = new SqlCommand("prJanitor_AlertView", conn);
            SqlCommand cmd6 = new SqlCommand("prJanitor_Stats_DailyTask", conn);
            SqlCommand cmd8 = new SqlCommand("prJanitor_Stats_Users", conn);
            SqlCommand cmd9 = new SqlCommand("prJanitor_Stats_Inventory", conn);
           


            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);

            try
            {

                conn.Open();
                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                    lbl_home_count.Text = dr["home_count"].ToString();
                    lbl_unit_count.Text = dr["unit_count"].ToString();
                    lbl_occ_unit_count.Text = dr["occ_unit_count"].ToString();
                    lbl_vacant_unit_count.Text = dr["vacant_unit_count"].ToString();

                }
            }
            finally
            {
                //  conn.Close();
            }



            cmd3.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd3.Parameters.Add("@home_id", SqlDbType.Int).Value = 0;
            cmd3.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd3.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);

            try
            {
                SqlDataReader dr3 = null;
                dr3 = cmd3.ExecuteReader(CommandBehavior.SingleRow);

                while (dr3.Read() == true)
                {
                    // amount of days for lease pending and expiration
                    lbl_as_lease_expiration.Text = dr3["as_lease_expiration"].ToString();
                    lbl_as_lease_pending.Text = dr3["as_lease_pending"].ToString();

                    lbl_lease_expiration.Text = dr3["lease_expiration"].ToString();
                    if (Convert.ToInt32(dr3["lease_expiration"]) < 1)
                        tr_lease_expiration.Visible = false;

                    lbl_lease_pending.Text = dr3["lease_pending"].ToString();
                    if (Convert.ToInt32(dr3["lease_pending"]) < 1)
                        tr_lease_pending.Visible = false;


                    lbl_rent_delequency.Text = dr3["rent_delequency"].ToString();
                    if (Convert.ToInt32(dr3["rent_delequency"]) < 1)
                        tr_rent_delequency.Visible = false;

                    lbl_wo_overdue.Text = dr3["as_wo_overdue"].ToString();
                    if (Convert.ToInt32(dr3["as_wo_overdue"]) < 1)
                        tr_wo_overdue.Visible = false;

                    lbl_wo_pending.Text = dr3["as_wo_pending"].ToString();
                    if (Convert.ToInt32(dr3["as_wo_pending"]) < 1)
                        tr_wo_pending.Visible = false;

                }

            }
            finally
            {
                // conn.Close();
            }



            cmd6.CommandType = CommandType.StoredProcedure;
            cmd6.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd6.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);

            try
            {
                SqlDataReader dr6 = null;
                dr6 = cmd6.ExecuteReader(CommandBehavior.SingleRow);

                while (dr6.Read() == true)
                {
                    lbl_incident_last30days_count.Text = dr6["incident_last30days_count"].ToString();
                    lbl_incident_last48hrs_count.Text = dr6["incident_last48hrs_count"].ToString();
                    lbl_wo_count.Text = dr6["wo_count"].ToString();
                }
            }
            finally
            {
                //   conn.Close();
            }

            cmd8.CommandType = CommandType.StoredProcedure;
            cmd8.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd8.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);

            try
            {
                SqlDataReader dr8 = null;
                dr8 = cmd8.ExecuteReader(CommandBehavior.SingleRow);

                while (dr8.Read() == true)
                {
                    lbl_pm_count.Text = dr8["pm_count"].ToString();
                    lbl_tenant_count.Text = dr8["tenant_count"].ToString();
                    lbl_owner_count.Text = dr8["owner_count"].ToString();
                    lbl_janitor_count.Text = dr8["janitor_count"].ToString();
                }
            }
            finally
            {
                //   conn.Close();
            }



            cmd9.CommandType = CommandType.StoredProcedure;
            cmd9.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd9.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);

            try
            {
                SqlDataReader dr9 = null;
                dr9 = cmd9.ExecuteReader(CommandBehavior.SingleRow);

                while (dr9.Read() == true)
                {
                    lbl_home_item_count.Text = dr9["home_item_count"].ToString();
                    lbl_warehouse_item_count.Text = dr9["warehouse_item_count"].ToString();
                }
            }
            finally
            {
                   conn.Close();
            }


        }
    }
}
