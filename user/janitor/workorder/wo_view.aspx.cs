﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.NameObjectAuthorization;

// <summary>
/// Done by : Stanley Jocelyn
/// date    : may 23, 2009
/// </summary>
/// 

public partial class manager_workorder_wo_view : BasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RegEx.IsInteger(Request.QueryString["wo_id"]) ||
            !RegEx.IsInteger(Request.QueryString["h_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        if (!Page.IsPostBack)
        {

            NameObjectAuthorization workorderAuthorization = new NameObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
            if (!workorderAuthorization.HomeWorkOrder(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["h_id"]), Convert.ToInt32(Request.QueryString["wo_id"]), Convert.ToInt32(Session["name_id"]),5))
            {
                Session.Abandon();
                Response.Redirect("~/login.aspx");
            }
            ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
        
            MultiView1.ActiveViewIndex = 0;

 
            //-----------  get contactor list and a work order  ----------------------

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd2 = new SqlCommand("prWorkOrderView", conn);
            SqlCommand cmd3 = new SqlCommand("prTaskList", conn);
            SqlCommand cmd4 = new SqlCommand("prIncidentView", conn);
            

            cmd2.CommandType = CommandType.StoredProcedure;
            cmd3.CommandType = CommandType.StoredProcedure;
            cmd4.CommandType = CommandType.StoredProcedure;
            

                conn.Open();
                
                cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd2.Parameters.Add("@wo_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["wo_id"]); ;

                SqlDataReader dr2 = null;
                dr2 = cmd2.ExecuteReader();

                while (dr2.Read() == true)
                {
                    lbl_home_name2.Text = dr2["home_name"].ToString();
                    lbl_unit_door_no2.Text = dr2["unit_door_no"].ToString();
                    lbl_wo_title2.Text = dr2["wo_title"].ToString();
                    lbl_wo_location.Text = dr2["wo_location"].ToString();

                    lbl_wo_priority2.Text = GetPriority(Convert.ToInt32(dr2["wo_priority"]));



                    lbl_wo_status2.Text = GetStatus(Convert.ToInt32(dr2["wo_status"]));                   
                    lbl_wo_date_begin2.Text = String.Format("{0:MMM dd , yyyy}", Convert.ToDateTime(dr2["wo_date_begin"]));
                    

                    lbl_wo_exp_date_end2.Text =  String.Format("{0:MMM dd , yyyy}", Convert.ToDateTime(dr2["wo_exp_date_end"]));
                    

                    if (DBNull.Value == dr2["wo_date_end"])
                    { }
                    else
                    {
                        lbl_wo_date_end2.Text = String.Format("{0:MMM dd , yyyy}", Convert.ToDateTime(dr2["wo_date_end"]));
                    }
                }


                //Add the params
                cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd3.Parameters.Add("@wo_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["wo_id"]);


                SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
                DataTable ds3 = new DataTable();
                da3.Fill(ds3);

                gv_task.DataSource = ds3;
                gv_task.DataBind();


                cmd4.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd4.Parameters.Add("@incident_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["inc"]);
        
                SqlDataReader dr4 = null;
                dr4 = cmd4.ExecuteReader(CommandBehavior.SingleRow);

                while (dr4.Read() == true)
                {
                    lbl_incident_title.Text = dr4["incident_title"].ToString();

                }
           

            // finally
            {
                conn.Close();
            }


            if (Request.QueryString["inc"] == "0")
            {
                lbl_wo_incident_related.Visible = false;
                lbl_wo_incident_related0.Visible = false;
                br_incident1.Visible = false;
                br_incident2.Visible = false;
                br_incident3.Visible = false;
                br_incident4.Visible = false;
                br_incident5.Visible = false;
                br_incident6.Visible = false;
                tb_incident.Visible = false;
                tb_incident0.Visible = false;


            }
        }

    }


    

   
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetPriority(int priority_id)
    {
        string priority = "";

        switch (priority_id)
        {
            case 1:
                priority = Resources.Resource.lbl_urgent;
                break;
            case 2:
                priority = Resources.Resource.lbl_high;
                break;
            case 3:
                priority = Resources.Resource.lbl_medium;
                break;

            case 4:
                priority = Resources.Resource.lbl_low;
                break;
        }

        return priority;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetStatus(int status_id)
    {
        string status = "";

        switch (status_id)
        {
            case 1:
                status = Resources.Resource.lbl_pending;
                break;
            case 2:
                status = Resources.Resource.lbl_in_progress;
                break;
            case 3:
                status = Resources.Resource.lbl_completed;
                break;

            case 4:
                status = Resources.Resource.lbl_closed;
                break;
        }

        return status;
    }



    protected void link_btn_view_task_Click(object sender, EventArgs e)
    {

        MultiView1.ActiveViewIndex = 1;

        tr_task_final_date_end.Visible = true;
        tr_task_final_cost.Visible = true;

        lbl_wo_title.Text = lbl_wo_title2.Text;
        lbl_home_name.Text = lbl_home_name2.Text;
        lbl_unit_door_no.Text = lbl_unit_door_no2.Text;

        lbl_exact_location.Text = lbl_wo_location.Text;

        lbl_wo_priority.Text = lbl_wo_priority2.Text;
        lbl_wo_status.Text = lbl_wo_status2.Text;

        lbl_wo_date_begin.Text = lbl_wo_date_begin2.Text;
        lbl_wo_exp_date_end.Text = "";

        
            lbl_wo_exp_date_end.Text = lbl_wo_exp_date_end2.Text; 

        //*********************************************************************
        LinkButton link_btn_view_task = (LinkButton)sender;
        GridViewRow grdRow = (GridViewRow)link_btn_view_task.Parent.Parent;

        //  string strField1 = grdRow.Cells[6].Text;

        HiddenField h_task_id = (HiddenField)grdRow.Cells[7].FindControl("h_task_id");

        int task_id = Convert.ToInt32(h_task_id.Value);


        // this is the hiddenfield at the bottom of the markup page , we will use it with the update button
        //    protected void btn_update_Click(object sender, EventArgs e)

        hd_task_id.Value = task_id.ToString();

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prTaskView", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        conn.Open();
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@task_id", SqlDbType.Int).Value = task_id;

        SqlDataReader dr = null;
        dr = cmd.ExecuteReader();

        while (dr.Read() == true)
        {
            lbl_task_title.Text = dr["task_title"].ToString();

            
            lbl_task_date_begin.Text = String.Format("{0:MMM dd , yyyy}", Convert.ToDateTime(dr["task_date_begin"]));
            

          
            if (dr["task_date_end"] == DBNull.Value)
            { }
            else
            {
                lbl_task_date_end.Text = String.Format("{0:MMM dd , yyyy}", Convert.ToDateTime(dr["task_date_end"]));

            }

            lbl_task_exp_date_end.Text = String.Format("{0:MMM dd , yyyy}", Convert.ToDateTime(dr["task_exp_date_end"]));



            lbl_task_status.Text = GetStatus( Convert.ToInt32(dr["task_status"]));
            lbl_priority.Text = GetPriority( Convert.ToInt32(dr["task_priority"]));

            lbl_task_contractor.Text = dr["company_name"].ToString();

            lbl_task_notes.Text = dr["task_notes"].ToString();
            lbl_task_description.Text = dr["task_description"].ToString();
            lbl_task_cost_estimation.Text = dr["task_cost_estimation"].ToString();

            if (dr["task_cost"] == DBNull.Value)
                lbl_task_cost.Text = "";
            else
                lbl_task_cost.Text = dr["task_cost"].ToString();


            // this is the category of task being done by a company
            string[] task_type_work = dr["task_type_work"].ToString().Split(',');


            if (task_type_work[0] == "1")
                company_cleaning.Checked = true;
            else
                company_cleaning.Checked = false;


            if (task_type_work[1] == "1")
                company_painting.Checked = true;
            else
                company_painting.Checked = false;


            if (task_type_work[2] == "1")
                company_paving.Checked = true;
            else
                company_paving.Checked = false;




            if (task_type_work[3] == "1")
                company_plumbing.Checked = true;
            else
                company_plumbing.Checked = false;


            if (task_type_work[4] == "1")
                company_decoration.Checked = true;
            else
                company_decoration.Checked = false;


            if (task_type_work[5] == "1")
                company_doors_windows.Checked = true;
            else
                company_doors_windows.Checked = false;


            if (task_type_work[6] == "1")
                company_bricks.Checked = true;
            else
                company_bricks.Checked = false;


            if (task_type_work[7] == "1")
                company_foundation.Checked = true;
            else
                company_foundation.Checked = false;



            if (task_type_work[8] == "1")
                company_alarms_security_systems.Checked = true;
            else
                company_alarms_security_systems.Checked = false;




            if (task_type_work[9] == "1")
                company_cable_satellite_dish.Checked = true;
            else
                company_cable_satellite_dish.Checked = false;


            if (task_type_work[10] == "1")
                company_ciment.Checked = true;
            else
                company_ciment.Checked = false;



            if (task_type_work[11] == "1")
                company_other.Checked = true;
            else
                company_other.Checked = false;


            if (task_type_work[12] == "1")
                company_hvac.Checked = true;
            else
                company_hvac.Checked = false;


            if (task_type_work[13] == "1")
                company_engineer.Checked = true;
            else
                company_engineer.Checked = false;




            if (task_type_work[14] == "1")
                company_gypse_installation.Checked = true;
            else
                company_gypse_installation.Checked = false;


            if (task_type_work[15] == "1")
                company_architech.Checked = true;
            else
                company_architech.Checked = false;





            if (task_type_work[16] == "1")
                company_gardening.Checked = true;
            else
                company_gardening.Checked = false;



            if (task_type_work[17] == "1")
                company_roofs.Checked = true;
            else
                company_roofs.Checked = false;




            if (task_type_work[18] == "1")
                company_flooring.Checked = true;
            else
                company_flooring.Checked = false;


            if (task_type_work[19] == "1")
                company_basement.Checked = true;
            else
                company_basement.Checked = false;





            if (task_type_work[20] == "1")
                company_inspection.Checked = true;
            else
                company_inspection.Checked = false;



            if (task_type_work[21] == "1")
                company_kitchen.Checked = true;
            else
                company_kitchen.Checked = false;


            if (task_type_work[22] == "1")
                company_electrical.Checked = true;
            else
                company_electrical.Checked = false;


            if (task_type_work[23] == "1")
                company_locksmith.Checked = true;
            else
                company_locksmith.Checked = false;


        }

        conn.Close();





    }

}
