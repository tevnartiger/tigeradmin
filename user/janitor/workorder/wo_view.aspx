﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/mp_janitor.master" AutoEventWireup="true" CodeFile="wo_view.aspx.cs" Inherits="manager_workorder_wo_view" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<asp:MultiView ID="MultiView1" runat="server">
    <asp:View ID="View1" runat="server">
    
        <asp:Label ID="lbl_wo_incident_related" runat="server" 
            Text="<%$ Resources:Resource, lbl_wo_incident_related %>" 
            style="font-size: small; font-weight: 700"></asp:Label>
        <br id="br_incident1" runat="server" />
        <br id="br_incident2" runat="server" />
       <table id="tb_incident" runat="server" >
       <tr>
       <td valign="top" style="width: 248px">
           <asp:Label ID="Label36" runat="server" 
               Text="<%$ Resources:Resource, lbl_incident_title %>"></asp:Label>
           </td> 
           <td>
               <asp:Label ID="lbl_incident_title" runat="server" Height="16px" Width="228px"></asp:Label>
           </td>
       </tr>
       </table>
       <br id="br_incident3" runat="server" />
       
       
       
       
       
    
        <b>
          <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_work_order %>" style="font-size: small"></asp:Label></b>
     
    <br /><br /><br />
    <table width="83%" >
        <tr>
            <td valign="top" style="width: 248px">
              <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_title %>"></asp:Label></td>
            <td >
                <asp:Label ID="lbl_wo_title2" runat="server" Height="16px" Width="228px"></asp:Label>
              <br /><hr />      
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 248px" >
                <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lbl_property %>"></asp:Label></td>
            <td >
                <asp:Label ID="lbl_home_name2" runat="server" Text="Label"></asp:Label>
               
                <br /><hr />
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 248px" >
                <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lbl_unit_if_applicable %>"></asp:Label></td>
            <td >
                <asp:Label ID="lbl_unit_door_no2" runat="server" Text="Label"></asp:Label>
                <br /><hr />
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 248px" >
                <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lbl_exact_location %>"></asp:Label></td>
            <td >
                <asp:Label ID="lbl_wo_location" runat="server" Height="16px" Width="229px"></asp:Label><br />
                <hr />
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 248px" >
                <asp:Label ID="Label29" runat="server" 
                    Text="<%$ Resources:Resource, lbl_priority%>"></asp:Label></td>
            <td >
                <asp:Label ID="lbl_wo_priority2" runat="server" Text="Label"></asp:Label>
              
                    <hr />
                    </td>
        </tr>
              
        <tr>
            <td valign="top" style="width: 248px" >
                <asp:Label ID="Label31" runat="server" Text="<%$ Resources:Resource, lbl_status%>"></asp:Label>
            </td>
            <td >
                <asp:Label ID="lbl_wo_status2" runat="server" Text="Label"></asp:Label>
                    <hr />
                    </td>
        </tr>
        
         <tr>
            <td style="width: 248px" valign="top">
                <asp:Label ID="Label34" runat="server" 
                    Text="<%$ Resources:Resource, lbl_date_end %>" style="font-weight: 700"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl_wo_date_end2" runat="server" Text="Label"></asp:Label>
                
                <hr />
            </td>
        </tr>
              
        <tr>
            <td valign="top" style="width: 248px" >
                <asp:Label ID="Label12" runat="server" 
                    Text="<%$ Resources:Resource, lbl_date_begin %>"></asp:Label>&nbsp;</td>
            <td >
            <asp:Label ID="lbl_wo_date_begin2" runat="server" Text="Label"></asp:Label>
                <hr />
                    </td>
        </tr>
        
        <tr>
            <td valign="top" style="width: 248px">
                <asp:Label ID="Label3" runat="server" 
                    Text="<%$ Resources:Resource, lbl_expected_date_end %>"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl_wo_exp_date_end2" runat="server" Text="Label"></asp:Label>
                &nbsp;
              
                <hr />
              
            </td>
        </tr>
                
       
                
        </table>
        
       
        <table  width="83%">
        <tr>
        <td>
            &nbsp;</td>
        </tr>
        </table>
        <br />
        <asp:GridView ID="gv_task" runat="server" AutoGenerateColumns="false" 
            Width="83%">
            <Columns>
                <asp:BoundField DataField="task_title" 
                    HeaderText="<%$ Resources:Resource, lbl_tasks %>" />
                <asp:BoundField DataField="company_name" 
                    HeaderText="<%$ Resources:Resource, lbl_company %>" />
                <asp:BoundField DataField="task_date_begin" 
                    DataFormatString="{0:MMM dd , yyyy}" 
                    HeaderText="<%$ Resources:Resource, lbl_date_begin %>" HtmlEncode="false" />
                <asp:BoundField DataField="task_exp_date_end" 
                    DataFormatString="{0:MMM dd , yyyy}" 
                    HeaderText="<%$ Resources:Resource, lbl_due_date %>" HtmlEncode="false" />
                <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_status%>">
                    <ItemTemplate>
                        <asp:Label ID="lbl_task_status" runat="server" 
                            Text='<%#GetStatus(Convert.ToInt32(Eval("task_status")))%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_priority%>">
                    <ItemTemplate>
                        <asp:Label ID="lbl_task_priority" runat="server" 
                            Text='<%#GetPriority(Convert.ToInt32(Eval("task_priority")))%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="task_cost_estimation" DataFormatString="{0:0.00}" 
                    HeaderText="<%$ Resources:Resource, lbl_cost_estimation %>" />
                <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_view %>">
                    <ItemTemplate>
                    <asp:HiddenField ID="h_task_id" runat="server" Value='<%#Bind("task_id")%>' />
                          <asp:LinkButton ID="link_btn_view_task" runat="server" 
                            OnClick="link_btn_view_task_Click" 
                            Text="<%$ Resources:Resource,lbl_view %>" />
                    </ItemTemplate>
                </asp:TemplateField>
                
            </Columns>
        </asp:GridView>
        <br />
       <br />
       
       
       
       
          
        
            </asp:View>
    
    
    
    
    
    
    <asp:View ID="View2" runat="server">
    
    
    
        <asp:Label ID="lbl_wo_incident_related0" runat="server" 
            style="font-size: small; font-weight: 700" 
            Text="<%$ Resources:Resource, lbl_wo_incident_related %>"></asp:Label>
    <br ID="br_incident4" runat="server" />
    
        <br ID="br_incident5" runat="server" />
        <table ID="tb_incident0" runat="server">
            <tr>
                <td style="width: 248px" valign="top">
                    <asp:Label ID="Label37" runat="server" 
                        Text="<%$ Resources:Resource, lbl_incident_title %>"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lbl_incident_title0" runat="server" Height="16px" Width="228px"></asp:Label>
                </td>
            </tr>
        </table>
        <b>
        <br id="br_incident6" runat="server" />
        <asp:Label ID="Label30" runat="server" style="font-size: small" 
            Text="<%$ Resources:Resource, lbl_work_order %>"></asp:Label>
        </b>
        <br />
    
        <br />
        <table width="83%">
            <tr>
                <td style="width: 248px" valign="top">
                    <asp:Label ID="Label21" runat="server" 
                        Text="<%$ Resources:Resource, lbl_title %>"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lbl_wo_title" runat="server" Height="16px" 
                        style="font-weight: 700" Width="228px"></asp:Label>
                    <br />
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 248px" valign="top">
                    <asp:Label ID="Label22" runat="server" 
                        Text="<%$ Resources:Resource, lbl_property %>"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lbl_home_name" runat="server" style="font-weight: 700"></asp:Label>
                    <br />
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 248px" valign="top">
                    <asp:Label ID="Label23" runat="server" 
                        Text="<%$ Resources:Resource, lbl_unit_if_applicable %>"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lbl_unit_door_no" runat="server" style="font-weight: 700"></asp:Label>
                    <br />
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 248px" valign="top">
                    <asp:Label ID="Label32" runat="server" 
                        Text="<%$ Resources:Resource, lbl_priority%>"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lbl_wo_priority" runat="server" style="font-weight: 700"></asp:Label>
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 248px" valign="top">
                    <asp:Label ID="Label33" runat="server" 
                        Text="<%$ Resources:Resource, lbl_status%>"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lbl_wo_status" runat="server" style="font-weight: 700"></asp:Label>
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 248px" valign="top">
                    <asp:Label ID="Label24" runat="server" 
                        Text="<%$ Resources:Resource, lbl_exact_location %>"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lbl_exact_location" runat="server" Height="16px" 
                        style="font-weight: 700" Width="229px"></asp:Label>
                    <br />
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 248px" valign="top">
                    <asp:Label ID="Label25" runat="server" 
                        Text="<%$ Resources:Resource, lbl_date_begin %>"></asp:Label>
                    &nbsp;</td>
                <td>
                    <asp:Label ID="lbl_wo_date_begin" runat="server" style="font-weight: 700"></asp:Label>
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 248px" valign="top">
                    <asp:Label ID="Label26" runat="server" 
                        Text="<%$ Resources:Resource, lbl_expected_date_end %>"></asp:Label>
                </td>
                <td>
                    &nbsp;<asp:Label ID="lbl_wo_exp_date_end" runat="server" style="font-weight: 700"></asp:Label>
                    <hr />
                </td>
            </tr>
        </table>
        <table width="83%">
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label17" runat="server" 
                        style="font-size: small; font-weight: 700" 
                        Text="<%$ Resources:Resource, lbl_tasks %>"></asp:Label>
                    <br />
                    <br />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label18" runat="server" 
                        Text="<%$ Resources:Resource, lbl_title %>"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lbl_task_title" runat="server" Height="16px" Width="228px"></asp:Label>
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label19" runat="server" 
                        Text="<%$ Resources:Resource, lbl_task_date_begin %>"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lbl_task_date_begin" runat="server" T></asp:Label>
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label20" runat="server" 
                        Text="<%$ Resources:Resource, lbl_task_exp_date_end %>"></asp:Label>
                </td>
                <td> <asp:Label ID="lbl_task_exp_date_end" runat="server" ></asp:Label>
                    &nbsp;<hr />
                </td>
            </tr>
            <tr id="tr_task_final_date_end" runat="server">
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label35" runat="server" style="font-weight: 700" 
                        Text="<%$ Resources:Resource, lbl_date_end %>"></asp:Label>
                </td>
                <td><asp:Label ID="lbl_task_date_end" runat="server" ></asp:Label>
                    &nbsp;<hr />
                </td>
            </tr>
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label7" runat="server" 
                        Text="<%$ Resources:Resource, lbl_type_of_work %>" />
                </td>
                <td>
                    <table ID="tb_contractor" runat="server" bgcolor="#ffffcc" 
                        language="javascript" onclick="return TABLE1_onclick()">
                        <tr>
                            <td>
                                <asp:CheckBox ID="company_hvac" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_hvac %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_decoration" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_decoration %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_inspection" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_inspection %>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="company_cleaning" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_cleaning %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_doors_windows" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_doors %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_kitchen" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_kitchen %>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="company_alarms_security_systems" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_alarm_security %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_electrical" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_electrical %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_locksmith" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_locksmith %>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="company_architech" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_architech %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_engineer" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_engineer %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_painting" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_painting %>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="company_basement" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_basement %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_flooring" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_flooring %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_paving" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_paving %>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="company_bricks" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_bricks %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_foundation" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_foundation %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_plumbing" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_plumbing %>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="company_cable_satellite_dish" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_cable %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_gardening" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_gardening %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_roofs" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_roofs %>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="company_ciment" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_ciment %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_gypse_installation" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_gypse_installation %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_other" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_other %>" />
                            </td>
                        </tr>
                    </table>
                    <br />
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label8" runat="server" 
                        Text="<%$ Resources:Resource, lbl_priority%>"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lbl_priority" runat="server" Text="Label"></asp:Label>
                    
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 248px" valign="top">
                    <asp:Label ID="Label16" runat="server" 
                        Text="<%$ Resources:Resource, lbl_status%>"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lbl_task_status" runat="server" Text="Label"></asp:Label>
                    
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label9" runat="server" 
                        Text="<%$ Resources:Resource, lbl_description %>" />
                </td>
                <td>
                    <asp:Label ID="lbl_task_description" runat="server"></asp:Label>
                    <br />
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label10" runat="server" 
                        Text="<%$ Resources:Resource, lbl_notes %>" />
                </td>
                <td>
                    <asp:Label ID="lbl_task_notes" runat="server" ></asp:Label>
                    <br />
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label13" runat="server" 
                        Text="<%$ Resources:Resource, lbl_contractor %>"></asp:Label>
                    &nbsp;</td>
                <td>
                    <asp:Label ID="lbl_task_contractor" runat="server" Text="Label"></asp:Label>
                    
                    <br />
                      <br />
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label14" runat="server" 
                        Text="<%$ Resources:Resource, lbl_cost_estimation %>"></asp:Label>
                    &nbsp;</td>
                <td>
                    <asp:Label ID="lbl_task_cost_estimation" runat="server"></asp:Label>
                    <hr />
                </td>
            </tr>
            <tr id="tr_task_final_cost" runat="server" >
                <td style="width: 250px" valign="top">
                    <b><asp:Label ID="Label27" runat="server" 
                        Text="<%$ Resources:Resource, lbl_final_cost %>"></asp:Label></b></td>
                <td>
                    <asp:Label ID="lbl_task_cost" runat="server"></asp:Label>
                    <hr />
                </td>
            </tr>
        </table>

    <asp:HiddenField ID="hd_task_id" runat="server" />
    <asp:HiddenField ID="hd_wo_id" runat="server" />
 
    
     <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label>
    </asp:View> 
    </asp:MultiView>
   
<br />
    





























</asp:Content>

