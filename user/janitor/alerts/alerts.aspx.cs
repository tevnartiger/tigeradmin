﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : july 17 , 2008
/// </summary>
public partial class manager_alerts_alerts : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {

            
            tiger.Janitor l = new tiger.Janitor(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getJanitorHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

            int home_id = l.getJanitorHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

            tiger.Janitor hp = new tiger.Janitor(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_rent_delequency.DataSource = hp.getJanitorRentDelequencyList(Convert.ToInt32(Session["schema_id"]), 0, Convert.ToInt32(Session["name_id"]));
            gv_rent_delequency.DataBind();

           
        
        }
    }
  
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetPriority(int priority_id)
    {
        string priority = "";

        switch (priority_id)
        {
            case 1:
                priority = Resources.Resource.lbl_urgent;
                break;
            case 2:
                priority = Resources.Resource.lbl_high;
                break;
            case 3:
                priority = Resources.Resource.lbl_medium;
                break;

            case 4:
                priority = Resources.Resource.lbl_low;
                break;
        }

        return priority;
    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetStatus(int status_id)
    {
        string status = "";

        switch (status_id)
        {
            case 1:
                status = Resources.Resource.lbl_pending;
                break;
            case 2:
                status = Resources.Resource.lbl_in_progress;
                break;
            case 3:
                status = Resources.Resource.lbl_completed;
                break;

            case 4:
                status = Resources.Resource.lbl_closed;
                break;
        }

        return status;
    }

   
    /// <summary>
    /// 
    /// </summary>
    /// <param name="amount_of_warning_sent"></param>
    /// <returns></returns>
    protected string Get_AmountOfWarningSent(int amount_of_warning_sent)
    {

        string warning_sent = "";


        if (amount_of_warning_sent == 0)
        {
            warning_sent = Resources.Resource.lbl_none;
        }

        if( amount_of_warning_sent == 1)
        {
            warning_sent = Resources.Resource.lbl_first_notice;
        }

        if (amount_of_warning_sent == 2)
        {
            warning_sent = Resources.Resource.lbl_second_notice;
        }

        if (amount_of_warning_sent == 3)
        {
            warning_sent = Resources.Resource.lbl_third_notice;
        }

        return warning_sent;
    }
}
