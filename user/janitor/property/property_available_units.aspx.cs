﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;


/// <summary>
/// Done by : Stanley Jocelyn
/// date    : june 12 , 2008
/// </summary>

public partial class manager_property_property_available_units : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!Page.IsPostBack)
        {

            // First we check if there's home available
            tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            gv_HomeNumberOfUnitAvailable.DataSource = h.getHomeNumberOfUnitAvailable(Convert.ToInt32(Session["schema_id"]));
            gv_HomeNumberOfUnitAvailable.DataBind();
            
            
            
            
            int home_count = h.getHomeCount(Convert.ToInt32(Session["schema_id"]));
            string link_to_unit = "";
            if (home_count > 0)
            {
                int home_id = h.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));
                // link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                //Session["schema_id"]));


                ddl_home_id.Visible = true;


                ddl_home_id.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                ddl_home_id.SelectedValue = Convert.ToString(home_id);
                ddl_home_id.DataBind();

                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                gv_unit_available_list.DataSource = u.getUnitAvailableList(Convert.ToInt32(Session["schema_id"]),home_id);
                gv_unit_available_list.DataBind();






            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_unit_available_list.DataSource = u.getUnitAvailableList(Convert.ToInt32(Session["schema_id"]),Convert.ToInt32(ddl_home_id.SelectedValue));
        gv_unit_available_list.DataBind();
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_unit_available_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

            tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_unit_available_list.PageIndex = e.NewPageIndex;
            gv_unit_available_list.DataSource = u.getUnitAvailableList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
            gv_unit_available_list.DataBind();
    }





    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string Get_NumberOfDays(int number_of_days)
    {

        if (number_of_days == 0 )
            return Resources.Resource.lbl_na;
        else
            return number_of_days.ToString();

    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string Since(DateTime since)
    {

        if (since.Year == 1900)
            return Resources.Resource.lbl_na;
        else
            return since.Month.ToString()+"-"+since.Day.ToString()+"-"+since.Year.ToString();
         

    }
    
}
