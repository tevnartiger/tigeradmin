﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/mp_janitor.master" AutoEventWireup="true" CodeFile="property_profile.aspx.cs" Inherits="user_owner_property_property_profile" %>

 <%@ Register Src="~/user/uc/owner/uc_content_menu_group.ascx" TagName="uc_content_menu_group" TagPrefix="uc_content_menu_group" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table>
<tr><td><h2>Property Profile</h2></td></tr>
<tr><td align="center"><uc_content_menu_group:uc_content_menu_group ID="uc_group" runat="server" /></td></tr>
<tr><td>
 <asp:GridView ID="gv_home" runat="server" AutoGenerateColumns="false" DataKeyNames="home_id" Width="800">
 <Columns>
 
  <asp:TemplateField HeaderText="Image">
 <ItemTemplate>
 <asp:Image ID="Image1" Width="100" Height="90"  runat="server" ImageUrl='<%# "~/mediahandler/HomeImage.ashx?h_id="+ Eval("home_id") %>'/>
</ItemTemplate>

</asp:TemplateField> 
 
       <asp:HyperLinkField runat="server"  DataNavigateUrlFields="home_id" DataNavigateUrlFormatString="/manager/home/home_profile?home_id={0}"  HeaderText="" DataTextField="home_name" />

   <asp:TemplateField HeaderText="Owner">
 <ItemTemplate>
 <asp:Label ID="lbl_b" runat="server" Text='<%# getOwnerProfile(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "home_id"))) %>' />
 </ItemTemplate>
 </asp:TemplateField>
 <asp:TemplateField HeaderText="Property Manager">
 <ItemTemplate>     
 <asp:Label ID="lbl_b" runat="server" Text='<%# getManagerProfile(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "home_id"))) %>' />
 </ItemTemplate>
 </asp:TemplateField>
 <asp:TemplateField HeaderText="Janitor">
 <ItemTemplate>
 <asp:Label ID="lbl_b" runat="server" Text='<%# getJanitorProfile(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "home_id"))) %>' />
 </ItemTemplate>
 </asp:TemplateField>
 </Columns>
 </asp:GridView>

</td></tr>
</table> 
<br />
<br />    
 
</asp:Content>

