using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date   : may 21 , 2009
/// </summary>

public partial class property_property_list : BasePage
{
   

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            tiger.Janitor h = new tiger.Janitor(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            dghome_list.DataSource = h.getJanitorHomeList(Convert.ToInt32(Session["schema_id"]),Convert.ToInt32(Session["name_id"]));
            dghome_list.DataBind();

            
        }
        

    }


    protected void dghome_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
        tiger.Janitor h = new tiger.Janitor(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        dghome_list.PageIndex = e.NewPageIndex;
        dghome_list.DataSource = h.getJanitorHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
        dghome_list.DataBind();
    }
    protected void dghome_list_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
