﻿<%@ Page Title="" Language="C#"  MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" CodeFile="tools_default.aspx.cs" Inherits="manager_tools_tools_default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<br />


<table>

<tr><td> <asp:HyperLink ID="HyperLink5" NavigateUrl="~/user/pm/tools/tools_amortization.aspx" runat="server">
<h2>  <asp:Literal ID="Literal5" Text="Mortage Calculator" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to calculate your mortage payments and generate charts and grahps of payments.</td></tr>


<tr><td> <asp:HyperLink ID="HyperLink1" NavigateUrl="~/user/pm/financial/financial_analysis_period_list.aspx" runat="server">
<h2>  <asp:Literal ID="Literal1" Text="Time range Financial scenario" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to view the list of saved time range financial scenario </td></tr>


<tr><td> <asp:HyperLink ID="HyperLink4" NavigateUrl="~/user/pm/financial/financial_scenario_list.aspx" runat="server">
<h2>  <asp:Literal ID="Literal4" Text="Financial scenario" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to view the list of saved financial scenario.</td></tr>

</table>


</asp:Content>

