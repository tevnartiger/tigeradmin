﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.DataVisualization;
using System.Drawing;

/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : march 5 , 2008
/// </summary>
public partial class tools_tools_amortization : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {

            DateTime ladate = new DateTime();
            ladate = DateTime.Now;

            // the default selected month and year is now
            ddl_mortgage_begin_m.SelectedValue = ladate.Month.ToString();
            ddl_mortgage_begin_d.SelectedValue = ladate.Day.ToString();
            ddl_mortgage_begin_y.SelectedValue = ladate.Year.ToString();

            r_amortizationExtraCalculation.Visible = false;

        }

    }
    protected void btn_calculate_Click(object sender, EventArgs e)
    {


        r_amortizationCalculation.Visible = true;
        r_amortizationExtraCalculation.Visible = true;

        string lang_session = Session["_lastCulture"].ToString();
        //frequency at wich the mortgage is paid : Monthly, bi-weekly or weekly
        double frequency = Convert.ToDouble(ddl_mortgage_paid_frequency.SelectedValue);
        //Duration in year of the mortgage ( years of amortization)
        double duration = Convert.ToDouble(RegEx.getMoney(tbx_duration_years.Text));
        double interest = Convert.ToDouble(RegEx.getMoney(tbx_interest.Text));
        double balance = Convert.ToDouble(RegEx.getMoney(tbx_balance.Text));


        DateTime from = new DateTime();

        tiger.Date d = new tiger.Date();
        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        // from = Convert.ToDateTime(d.DateCulture(ddl_mortgage_begin_m.SelectedValue, "1", ddl_mortgage_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        from = Convert.ToDateTime(d.DateCulture(ddl_mortgage_begin_m.SelectedValue, ddl_mortgage_begin_d.SelectedValue, ddl_mortgage_begin_y.SelectedValue, lang_session));


        Label1.Text = lang_session;// ddl_mortgage_begin_m.SelectedValue + "/" + ddl_mortgage_begin_y.SelectedValue;

        tiger.Amortization hv = new tiger.Amortization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


        // if the interest is compounded each year we use The America Amortization ( also Euro )
        if (ddl_interest_compounded.SelectedValue == "12")
        {

            // All the calculation of payment, interest, 50/50 point, end point, total payments, total interest etc...
            r_amortizationCalculation.DataSource = hv.getAmortizationCalculation(from, balance, duration, 12.00, interest, 0.00,
                                                   0.00, Convert.ToInt32(ddl_eachYear.SelectedValue));
            r_amortizationCalculation.DataBind();

            // if the mortgage is paid by-weekly or weekly show the values and get the savigs repeater visible or of we add an additional payment
            if (ddl_mortgage_paid_frequency.SelectedValue == "241" || ddl_mortgage_paid_frequency.SelectedValue == "521" || ddl_mortgage_paid_frequency.SelectedValue == "24" || ddl_mortgage_paid_frequency.SelectedValue == "52" || Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)) > 0.00 || Convert.ToInt32(RegEx.getMoney(tbx_additionnal_payment.Text)) > 0.00)
            {
                //All the calculation of payment, interest, 50/50 point, end point,total payments, total interest etc...
                r_amortizationExtraCalculation.DataSource = hv.getAmortizationCalculation(from, balance, duration, frequency, interest, Convert.ToDouble(RegEx.getMoney(tbx_additionnal_payment.Text)),
                                                       Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)), Convert.ToInt32(ddl_eachYear.SelectedValue));
                r_amortizationExtraCalculation.DataBind();
            }
        }

        // if the interest is compounded twice year we use The Canadian Amortization
        if (ddl_interest_compounded.SelectedValue == "6")
        {

            // All the calculation of Canadian payment, interest, 50/50 point, end point, total payments, total interest etc...
            r_amortizationCalculation.DataSource = hv.getAmortizationCanCalculation(from, balance, duration, 12.00, interest, 0.00,
                                                   0.00, Convert.ToInt32(ddl_eachYear.SelectedValue));
            r_amortizationCalculation.DataBind();

            // if the mortgage is paid by-weekly or weekly  show the values and get the savigs repeater visible or additional payments
            if (ddl_mortgage_paid_frequency.SelectedValue == "241" || ddl_mortgage_paid_frequency.SelectedValue == "521" || ddl_mortgage_paid_frequency.SelectedValue == "24" || ddl_mortgage_paid_frequency.SelectedValue == "52" || Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)) > 0.00 || Convert.ToInt32(RegEx.getMoney(tbx_additionnal_payment.Text)) > 0.00)
            {


                // All the calculation of payment, interest, 50/50 point, end point, total payments, total interest etc...
                r_amortizationExtraCalculation.DataSource = hv.getAmortizationCanCalculation(from, balance, duration, frequency, interest, Convert.ToDouble(RegEx.getMoney(tbx_additionnal_payment.Text)),
                                                         Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)), Convert.ToInt32(ddl_eachYear.SelectedValue));
                r_amortizationExtraCalculation.DataBind();
            }
        }

        gv_amortization.Visible = false;


        if (ddl_mortgage_paid_frequency.SelectedIndex > 0 || Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)) > Convert.ToDouble(0.00) || Convert.ToDouble(RegEx.getMoney(tbx_additionnal_payment.Text)) > Convert.ToDouble(0.00))
            r_amortizationExtraCalculation.Visible = true;
        else
            r_amortizationExtraCalculation.Visible = false;


    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_calculate_extra_Click(object sender, EventArgs e)
    {

        r_amortizationCalculation.Visible = true;
        r_amortizationExtraCalculation.Visible = true;


        string lang_session = Session["_lastCulture"].ToString();
        //frequency at wich the mortgage is paid : Monthly, bi-weekly or weekly
        double frequency = Convert.ToDouble(ddl_mortgage_paid_frequency.SelectedValue);
        //Duration in year of the mortgage ( years of amortization)
        double duration = Convert.ToDouble(RegEx.getMoney(tbx_duration_years.Text));
        double interest = Convert.ToDouble(RegEx.getMoney(tbx_interest.Text));
        double balance = Convert.ToDouble(RegEx.getMoney(tbx_balance.Text));


        DateTime from = new DateTime();

        tiger.Date d = new tiger.Date();
        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime

        from = Convert.ToDateTime(d.DateCulture(ddl_mortgage_begin_m.SelectedValue, ddl_mortgage_begin_d.SelectedValue, ddl_mortgage_begin_y.SelectedValue, lang_session));


        Label1.Text = lang_session;// ddl_mortgage_begin_m.SelectedValue + "/" + ddl_mortgage_begin_y.SelectedValue;

        tiger.Amortization hv = new tiger.Amortization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


        // if the interest is compounded each year we use The America Amortization ( also Euro )
        if (ddl_interest_compounded.SelectedValue == "12")
        {
            gv_amortization.DataSource = hv.getAmortizationTable(from, balance, duration, frequency, interest, Convert.ToDouble(RegEx.getMoney(tbx_additionnal_payment.Text)),
                                                     Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)), Convert.ToInt32(ddl_eachYear.SelectedValue));
            gv_amortization.DataBind();

            // All the calculation of payment, interest, 50/50 point, end point, total payments, total interest etc...
            r_amortizationCalculation.DataSource = hv.getAmortizationCalculation(from, balance, duration, 12.00, interest, 0.00,
                                                    0.00, Convert.ToInt32(ddl_eachYear.SelectedValue));
            r_amortizationCalculation.DataBind();

            // if the mortgage is paid by-weekly or weekly show the values and get the savigs repeater visible or of we add an additional payment
            if (ddl_mortgage_paid_frequency.SelectedValue == "241" || ddl_mortgage_paid_frequency.SelectedValue == "521" || ddl_mortgage_paid_frequency.SelectedValue == "24" || ddl_mortgage_paid_frequency.SelectedValue == "52" || Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)) > 0.00 || Convert.ToInt32(RegEx.getMoney(tbx_additionnal_payment.Text)) > 0.00)
            {
                //All the calculation of payment, interest, 50/50 point, end point,total payments, total interest etc...
                r_amortizationExtraCalculation.DataSource = hv.getAmortizationCalculation(from, balance, duration, frequency, interest, Convert.ToDouble(RegEx.getMoney(tbx_additionnal_payment.Text)),
                                                       Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)), Convert.ToInt32(ddl_eachYear.SelectedValue));
                r_amortizationExtraCalculation.DataBind();
            }
        }

        // if the interest is compounded twice year we use The Canadian Amortization
        if (ddl_interest_compounded.SelectedValue == "6")
        {
            gv_amortization.DataSource = hv.getAmortizationCanTable(from, balance, duration, frequency, interest, Convert.ToDouble(RegEx.getMoney(tbx_additionnal_payment.Text)),
                                                    Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)), Convert.ToInt32(ddl_eachYear.SelectedValue));
            gv_amortization.DataBind();


            // All the calculation of Canadian payment, interest, 50/50 point, end point, total payments, total interest etc...
            r_amortizationCalculation.DataSource = hv.getAmortizationCanCalculation(from, balance, duration, 12.00, interest, 0.00,
                                                   0.00, Convert.ToInt32(ddl_eachYear.SelectedValue));
            r_amortizationCalculation.DataBind();

            // if the mortgage is paid by-weekly or weekly  show the values and get the savigs repeater visible or additional payments
            if (ddl_mortgage_paid_frequency.SelectedValue == "241" || ddl_mortgage_paid_frequency.SelectedValue == "521" || ddl_mortgage_paid_frequency.SelectedValue == "24" || ddl_mortgage_paid_frequency.SelectedValue == "52" || Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)) > 0.00 || Convert.ToInt32(RegEx.getMoney(tbx_additionnal_payment.Text)) > 0.00)
            {


                // All the calculation of payment, interest, 50/50 point, end point, total payments, total interest etc...
                r_amortizationExtraCalculation.DataSource = hv.getAmortizationCanCalculation(from, balance, duration, frequency, interest, Convert.ToDouble(RegEx.getMoney(tbx_additionnal_payment.Text)),
                                                         Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)), Convert.ToInt32(ddl_eachYear.SelectedValue));
                r_amortizationExtraCalculation.DataBind();
            }
        }

        gv_amortization.Visible = true;


        if (ddl_mortgage_paid_frequency.SelectedIndex > 0 || Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)) > Convert.ToDouble(0.00) || Convert.ToDouble(RegEx.getMoney(tbx_additionnal_payment.Text)) > Convert.ToDouble(0.00))
            r_amortizationExtraCalculation.Visible = true;
        else
            r_amortizationExtraCalculation.Visible = false;

    }



    protected string GetYearSave(double timepaywithextraloan)
    {

        double timesaved = 0;
        for (int i = 0; i < r_amortizationCalculation.Items.Count; i++)
        {
            HiddenField h_timetopayloan1 = (HiddenField)r_amortizationCalculation.Items[i].FindControl("h_timetopayloan1");

            timesaved = Convert.ToDouble(h_timetopayloan1.Value) - timepaywithextraloan;

        }

        return String.Format("{0:0.00}", timesaved);
    }


    protected string GetMoneySave(double interestwithextra)
    {
        double moneysaved = 0;
        for (int i = 0; i < r_amortizationCalculation.Items.Count; i++)
        {

            HiddenField h_totalinterest1 = (HiddenField)r_amortizationCalculation.Items[i].FindControl("h_totalinterest1");
            moneysaved = Convert.ToDouble(h_totalinterest1.Value) - interestwithextra;
        }

        return String.Format("{0:0.00}", moneysaved);


    }
    protected void btn_graph_Click(object sender, EventArgs e)
    {
        //Chart1.Series[Resources.Resource.txt_balance].ChartType = SeriesChartType.Area; // Balance
        //Chart1.Width = 760;
        //Chart1.Height = 700;
        //Chart1.Series[Resources.Resource.txt_interest_paid].ChartType = SeriesChartType.Spline; // interest payments
        //Chart1.Series[Resources.Resource.txt_principal_paid].ChartType = SeriesChartType.Spline; // capital payments


        //// Add the second legend
        //Chart1.Legends.Add(new Legend("First"));

        //// Add the second legend
        //Chart1.Legends.Add(new Legend("Second"));

        //// Associate the last three series to the new legend
        //Chart1.Series[Resources.Resource.txt_balance].Legend = "First";
        //Chart1.Series[Resources.Resource.txt_interest_paid].Legend = "Second";
        //Chart1.Series[Resources.Resource.txt_principal_paid].Legend = "Second";

        //// Dock the first legend inside the first chart area
        //Chart1.Legends["First"].IsDockedInsideChartArea = true;
        //Chart1.Legends["First"].DockedToChartArea = "ChartArea1";

        //// Dock the second legend inside the second chart area
        //Chart1.Legends["Second"].IsDockedInsideChartArea = true;
        //Chart1.Legends["Second"].DockedToChartArea = "ChartArea2";




        double t_balance;
        DateTime t_date;
        string[] thedate;
        double t_interest;
        double t_principal;


        string lang_session = Session["_lastCulture"].ToString();
        //frequency at wich the mortgage is paid : Monthly, bi-weekly or weekly
        double frequency = Convert.ToDouble(ddl_mortgage_paid_frequency.SelectedValue);
        //Duration in year of the mortgage ( years of amortization)
        double duration = Convert.ToDouble(RegEx.getMoney(tbx_duration_years.Text));
        double interest = Convert.ToDouble(RegEx.getMoney(tbx_interest.Text));
        double balance = Convert.ToDouble(RegEx.getMoney(tbx_balance.Text));



        DateTime from = new DateTime();

        tiger.Date d = new tiger.Date();
        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime

        from = Convert.ToDateTime(d.DateCulture(ddl_mortgage_begin_m.SelectedValue, ddl_mortgage_begin_d.SelectedValue, ddl_mortgage_begin_y.SelectedValue, lang_session));


        Label1.Text = lang_session;// ddl_mortgage_begin_m.SelectedValue + "/" + ddl_mortgage_begin_y.SelectedValue;

        tiger.Amortization hv = new tiger.Amortization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


        // if the interest is compounded each year we use The America Amortization ( also Euro )
        if (ddl_interest_compounded.SelectedValue == "12")
        {

            // All the calculation of payment, interest, 50/50 point, end point, total payments, total interest etc...
            r_amortizationCalculation.DataSource = hv.getAmortizationCalculation(from, balance, duration, 12.00, interest, 0.00,
                                                    0.00, Convert.ToInt32(ddl_eachYear.SelectedValue));
            r_amortizationCalculation.DataBind();
            r_amortizationCalculation.Visible = false;

            // if the mortgage is paid by-weekly or weekly show the values and get the savigs repeater visible or of we add an additional payment
            if (ddl_mortgage_paid_frequency.SelectedValue == "241" || ddl_mortgage_paid_frequency.SelectedValue == "521" || ddl_mortgage_paid_frequency.SelectedValue == "24" || ddl_mortgage_paid_frequency.SelectedValue == "52" || Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)) > 0.00 || Convert.ToInt32(RegEx.getMoney(tbx_additionnal_payment.Text)) > 0.00)
            {
                //All the calculation of payment, interest, 50/50 point, end point,total payments, total interest etc...
                r_amortizationExtraCalculation.DataSource = hv.getAmortizationCalculation(from, balance, duration, frequency, interest, Convert.ToDouble(RegEx.getMoney(tbx_additionnal_payment.Text)),
                                                       Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)), Convert.ToInt32(ddl_eachYear.SelectedValue));
                r_amortizationExtraCalculation.DataBind();
            }


            for (int i = 0; i < hv.getAmortizationTable(from, balance, duration, frequency, interest, Convert.ToDouble(RegEx.getMoney(tbx_additionnal_payment.Text)),
                                                     Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)), Convert.ToInt32(ddl_eachYear.SelectedValue)).Rows.Count; i++)
            {
                DataRow row = hv.getAmortizationTable(from, balance, duration, frequency, interest, Convert.ToDouble(RegEx.getMoney(tbx_additionnal_payment.Text)),
                                                         Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)), Convert.ToInt32(ddl_eachYear.SelectedValue)).Rows[i];

                t_interest = Convert.ToDouble(row[5]);
                t_principal = Convert.ToDouble(row[7]);
                t_balance = Convert.ToDouble(row[9]);

                thedate = row[10].ToString().Split('-');
                t_date = Convert.ToDateTime(d.DateCulture(thedate[0], thedate[1], thedate[2], Convert.ToString(Session["_lastCulture"])));


                //Chart1.ChartAreas["ChartArea1"].Position.Auto = false;
                //Chart1.ChartAreas["ChartArea1"].Position.X = 0;
                //Chart1.ChartAreas["ChartArea1"].Position.Y = 0;
                //Chart1.ChartAreas["ChartArea1"].Position.Width = 90;
                //Chart1.ChartAreas["ChartArea1"].Position.Height = 50;

                //Chart1.ChartAreas["ChartArea2"].Position.X = 0;
                //Chart1.ChartAreas["ChartArea2"].Position.Y = 50;
                //Chart1.ChartAreas["ChartArea2"].Position.Width = 90;
                //Chart1.ChartAreas["ChartArea2"].Position.Height = 50;


                //Chart1.Series[Resources.Resource.txt_balance].Points.AddXY(t_date, t_balance);
                //Chart1.Series[Resources.Resource.txt_interest_paid].Points.AddXY(t_date, t_interest);
                //Chart1.Series[Resources.Resource.txt_principal_paid].Points.AddXY(t_date, t_principal);
                //Chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.Format = "MMM dd yyyy";
                //Chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;

                //Chart1.ChartAreas["ChartArea2"].AxisX.LabelStyle.Format = "MMM dd yyyy";
                //Chart1.ChartAreas["ChartArea2"].AxisX.LabelStyle.Angle = -45;

            }

        }

        // if the interest is compounded twice year we use The Canadian Amortization
        if (ddl_interest_compounded.SelectedValue == "6")
        {

            // All the calculation of Canadian payment, interest, 50/50 point, end point, total payments, total interest etc...
            r_amortizationCalculation.DataSource = hv.getAmortizationCanCalculation(from, balance, duration, 12.00, interest, 0.00,
                                                   0.00, Convert.ToInt32(ddl_eachYear.SelectedValue));
            r_amortizationCalculation.DataBind();

            // if the mortgage is paid by-weekly or weekly  show the values and get the savigs repeater visible or additional payments
            if (ddl_mortgage_paid_frequency.SelectedValue == "241" || ddl_mortgage_paid_frequency.SelectedValue == "521" || ddl_mortgage_paid_frequency.SelectedValue == "24" || ddl_mortgage_paid_frequency.SelectedValue == "52" || Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)) > 0.00 || Convert.ToInt32(RegEx.getMoney(tbx_additionnal_payment.Text)) > 0.00)
            {


                // All the calculation of payment, interest, 50/50 point, end point, total payments, total interest etc...
                r_amortizationExtraCalculation.DataSource = hv.getAmortizationCanCalculation(from, balance, duration, frequency, interest, Convert.ToDouble(RegEx.getMoney(tbx_additionnal_payment.Text)),
                                                         Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)), Convert.ToInt32(ddl_eachYear.SelectedValue));
                r_amortizationExtraCalculation.DataBind();
            }


            for (int i = 0; i < hv.getAmortizationTable(from, balance, duration, frequency, interest, Convert.ToDouble(RegEx.getMoney(tbx_additionnal_payment.Text)),
                                                   Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)), Convert.ToInt32(ddl_eachYear.SelectedValue)).Rows.Count; i++)
            {
                DataRow row = hv.getAmortizationTable(from, balance, duration, frequency, interest, Convert.ToDouble(RegEx.getMoney(tbx_additionnal_payment.Text)),
                                                         Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)), Convert.ToInt32(ddl_eachYear.SelectedValue)).Rows[i];

                t_interest = Convert.ToDouble(row[5]);
                t_principal = Convert.ToDouble(row[7]);
                t_balance = Convert.ToDouble(row[9]);

                thedate = row[10].ToString().Split('-');
                t_date = Convert.ToDateTime(d.DateCulture(thedate[0], thedate[1], thedate[2], Convert.ToString(Session["_lastCulture"])));

                //Chart1.ChartAreas["ChartArea1"].Position.Auto = false;
                //Chart1.ChartAreas["ChartArea1"].Position.X = 0;
                //Chart1.ChartAreas["ChartArea1"].Position.Y = 0;
                //Chart1.ChartAreas["ChartArea1"].Position.Width = 90;
                //Chart1.ChartAreas["ChartArea1"].Position.Height = 50;

                //Chart1.ChartAreas["ChartArea2"].Position.X = 0;
                //Chart1.ChartAreas["ChartArea2"].Position.Y = 50;
                //Chart1.ChartAreas["ChartArea2"].Position.Width = 90;
                //Chart1.ChartAreas["ChartArea2"].Position.Height = 50;









                //Chart1.Series[Resources.Resource.txt_balance].Points.AddXY(t_date, t_balance);
                //Chart1.Series[Resources.Resource.txt_interest_paid].Points.AddXY(t_date, t_interest);
                //Chart1.Series[Resources.Resource.txt_principal_paid].Points.AddXY(t_date, t_principal);


                //Chart1.Series[Resources.Resource.txt_balance].Points.AddXY(t_date, t_balance);
                //Chart1.Series[Resources.Resource.txt_interest_paid].Points.AddXY(t_date, t_interest);
                //Chart1.Series[Resources.Resource.txt_principal_paid].Points.AddXY(t_date, t_principal);
                //Chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.Format = "MMM dd yyyy";
                //Chart1.ChartAreas["ChartArea1"].AxisX.LabelStyle.Angle = -45;

                //Chart1.ChartAreas["ChartArea2"].AxisX.LabelStyle.Format = "MMM dd yyyy";
                //Chart1.ChartAreas["ChartArea2"].AxisX.LabelStyle.Angle = -45;

            }


        }

        gv_amortization.Visible = false;

        if (ddl_mortgage_paid_frequency.SelectedIndex > 0 || Convert.ToDouble(RegEx.getMoney(tbx_additionnal_eachYear.Text)) > Convert.ToDouble(0.00) || Convert.ToDouble(RegEx.getMoney(tbx_additionnal_payment.Text)) > Convert.ToDouble(0.00))
            r_amortizationExtraCalculation.Visible = true;
        else
            r_amortizationExtraCalculation.Visible = false;


        r_amortizationCalculation.Visible = false;
        r_amortizationExtraCalculation.Visible = false;

    }
}
