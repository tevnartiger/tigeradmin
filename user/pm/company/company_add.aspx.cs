using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
/// <summary>
/// Done by : Stanley Jocelyn
/// date : sept 3, 2007
/// </summary>
public partial class company_company_add : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!RegEx.IsAlpha(Request.QueryString["c"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }


        if (!Page.IsPostBack)
        {
            reg_company_city.ValidationExpression = RegEx.getText();
            reg_company_addr_street.ValidationExpression = RegEx.getText();
            reg_company_contact_email.ValidationExpression = RegEx.getEmail();
            reg_company_contact_fname.ValidationExpression = RegEx.getText();
            reg_company_contact_lname.ValidationExpression = RegEx.getText();
            reg_company_contact_tel.ValidationExpression = RegEx.getText();
            reg_company_name.ValidationExpression = RegEx.getText();
            reg_company_pc.ValidationExpression = RegEx.getText();
            reg_company_tel.ValidationExpression = RegEx.getText();
            reg_company_website.ValidationExpression = RegEx.getText();
            reg_company_prov.ValidationExpression = RegEx.getText();

            // if we want to add a company ( contractor )
            if (Request.QueryString["c"] == "c")
            {
                tb_contractor_question.Visible = true;
                tb_supplier_question.Visible = false;

                lbl_company.Visible = true;
                lbl_supplier_vendor.Visible = false;

                tb_contractor.Visible = true;

                lbl_category.Visible = true;
            
            }

            // if we want to add a supplier / vendor
            else
            {
                tb_contractor_question.Visible = false;
                tb_supplier_question.Visible = true;

                lbl_company.Visible = false;
                lbl_supplier_vendor.Visible = true;

                tb_contractor.Visible = false;

                lbl_category.Visible = false;

            }


        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {

        Page.Validate();
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prCompanyAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

      //  try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@new_company_id", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.NVarChar, 15).Value = Request.UserHostAddress.ToString();
        

            if (Request.QueryString["c"] == "c")
            {
                if(radio_contractor.SelectedValue=="1")
                    cmd.Parameters.Add("@company_type", SqlDbType.Char, 1).Value = "b";
                else
                    cmd.Parameters.Add("@company_type", SqlDbType.Char, 1).Value = "c";

            }
            else   // i.e querystring  == s
            {
                if (radio_supplier.SelectedValue == "1")
                    cmd.Parameters.Add("@company_type", SqlDbType.Char, 1).Value = "b";
                else
                    cmd.Parameters.Add("@company_type", SqlDbType.Char, 1).Value = "s";

            }

            cmd.Parameters.Add("@company_name", SqlDbType.NVarChar, 50).Value = company_name.Text;
            cmd.Parameters.Add("@company_website", SqlDbType.NVarChar, 200).Value = company_website.Text;
           // cmd.Parameters.Add("@company_addr_no", SqlDbType.NVarChar, 50).Value = company_addr_no.Text;
            cmd.Parameters.Add("@company_addr_street", SqlDbType.NVarChar,200).Value = company_addr_street.Text;
            cmd.Parameters.Add("@company_city", SqlDbType.NVarChar, 50).Value = company_city.Text;
            cmd.Parameters.Add("@company_prov", SqlDbType.NVarChar, 50).Value = company_prov.Text;
            cmd.Parameters.Add("@company_pc", SqlDbType.NVarChar, 50).Value = company_pc.Text;
            cmd.Parameters.Add("@company_tel", SqlDbType.NVarChar, 50).Value = company_tel.Text;
            cmd.Parameters.Add("@company_contact_fname", SqlDbType.NVarChar, 50).Value = company_contact_fname.Text;
            cmd.Parameters.Add("@company_contact_lname", SqlDbType.NVarChar, 50).Value = company_contact_lname.Text;
            cmd.Parameters.Add("@company_contact_tel", SqlDbType.NVarChar, 50).Value = company_contact_tel.Text;
            cmd.Parameters.Add("@company_contact_email", SqlDbType.NVarChar, 50).Value = company_contact_email.Text;


            if( company_general_contractor.Checked == true && tb_contractor.Visible == true )
              cmd.Parameters.Add("@company_general_contractor", SqlDbType.Bit).Value = 1;
            else
              cmd.Parameters.Add("@company_general_contractor", SqlDbType.Bit).Value = 0;

            if (company_interior_contractor.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_interior_contractor", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_interior_contractor", SqlDbType.Bit).Value = 0;

            if (company_exterior_contractor.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_exterior_contractor", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_exterior_contractor", SqlDbType.Bit).Value = 0;

            if (company_cleaning.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_cleaning", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_cleaning", SqlDbType.Bit).Value = 0;


            if (company_painting.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_painting", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_painting", SqlDbType.Bit).Value = 0;

            if (company_paving.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_paving", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_paving", SqlDbType.Bit).Value = 0;


            if (company_plumbing.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_plumbing", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_plumbing", SqlDbType.Bit).Value = 0;


            if (company_decoration.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_decoration", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_decoration", SqlDbType.Bit).Value = 0;

            if (company_doors_windows.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_doors_windows", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_doors_windows", SqlDbType.Bit).Value = 0;

            if (company_bricks.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_bricks", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_bricks", SqlDbType.Bit).Value = 0;

            if (company_foundation.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_foundation", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_foundation", SqlDbType.Bit).Value = 0;

            if (company_alarms_security_systems.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_alarms_security_systems", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_alarms_security_systems", SqlDbType.Bit).Value = 0;

            if (company_cable_satellite_dish.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_cable_satellite_dish", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_cable_satellite_dish", SqlDbType.Bit).Value = 0;

            if (company_ciment.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_ciment", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_ciment", SqlDbType.Bit).Value = 0;

            if (company_other.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_other", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_other", SqlDbType.Bit).Value = 0;

            if (company_hvac.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_hvac", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_hvac", SqlDbType.Bit).Value = 0;

            if (company_engineer.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_engineer", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_engineer", SqlDbType.Bit).Value = 0;

            if (company_gypse_installation.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_gypse_installation", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_gypse_installation", SqlDbType.Bit).Value = 0;

            if (company_architech.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_architech", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_architech", SqlDbType.Bit).Value = 0;

            if (company_gardening.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_gardening", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_gardening", SqlDbType.Bit).Value = 0;

            if (company_roofs.Checked == true && tb_contractor.Visible == true)
               cmd.Parameters.Add("@company_roofs", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_roofs", SqlDbType.Bit).Value = 0;

            if (company_flooring.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_flooring", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_flooring", SqlDbType.Bit).Value = 0;

            if (company_basement.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_basement", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_basement", SqlDbType.Bit).Value = 0;

            if (company_inspection.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_inspection", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_inspection", SqlDbType.Bit).Value = 0;

            if (company_kitchen.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_kitchen", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_kitchen", SqlDbType.Bit).Value = 0;

            if (company_electrical.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_electrical", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_electrical", SqlDbType.Bit).Value = 0;

            if (company_locksmith.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_locksmith", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_locksmith", SqlDbType.Bit).Value = 0;


            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            if (Convert.ToInt32(cmd.Parameters["@return_id"].Value) == 0)
                result.InnerHtml = Resources.Resource.lbl_successfull_add;

            conn.Close();
        }
      //  catch (Exception error)
        {
           // tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            //result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }
    }

    protected void btn_othercompany_Click(object sender, EventArgs e)
    {

        Response.Redirect("company_add.aspx");

    }
    protected void radio_supplier_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(radio_supplier.SelectedValue =="1")
          tb_contractor.Visible = true;
        else
            tb_contractor.Visible = false;
    }
   
}
