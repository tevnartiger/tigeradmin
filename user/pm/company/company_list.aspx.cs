using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
/// <summary>
/// Done by : Stanley Jocelyn
/// date    : oct 8 , 2007
/// </summary>
public partial class company_company_list : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prCompanySearchList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            company_all.Checked = true;

            // try
            {
                conn.Open();


                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@company_all", SqlDbType.Bit).Value = 1;
                cmd.Parameters.Add("@company_general_contractor", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_interior_contractor", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_exterior_contractor", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_cleaning", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_painting", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_paving", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_plumbing", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_decoration", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_doors_windows", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_bricks", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_foundation", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_alarms_security_systems", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_cable_satellite_dish", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_ciment", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_other", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_hvac", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_engineer", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_gypse_installation", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_architech", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_gardening", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_roofs", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_flooring", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_basement", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_inspection", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_kitchen", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_electrical", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_locksmith", SqlDbType.Bit).Value = 0;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                dgcompany_list.DataSource = ds;
                dgcompany_list.DataBind();

            }

            // finally
            {
                conn.Close();
            }


        }


    }

    protected void btn_search_Click(object sender, EventArgs e)
    {


        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prCompanySearchList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        // try
        {
            conn.Open();


            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);

            if (company_all.Checked == true)
                cmd.Parameters.Add("@company_all", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_all", SqlDbType.Bit).Value = 0;


            if (company_general_contractor.Checked == true)
                cmd.Parameters.Add("@company_general_contractor", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_general_contractor", SqlDbType.Bit).Value = 0;

            if (company_interior_contractor.Checked == true)
                cmd.Parameters.Add("@company_interior_contractor", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_interior_contractor", SqlDbType.Bit).Value = 0;

            if (company_exterior_contractor.Checked == true)
                cmd.Parameters.Add("@company_exterior_contractor", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_exterior_contractor", SqlDbType.Bit).Value = 0;

            if (company_cleaning.Checked == true)
                cmd.Parameters.Add("@company_cleaning", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_cleaning", SqlDbType.Bit).Value = 0;


            if (company_painting.Checked == true)
                cmd.Parameters.Add("@company_painting", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_painting", SqlDbType.Bit).Value = 0;

            if (company_paving.Checked == true)
                cmd.Parameters.Add("@company_paving", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_paving", SqlDbType.Bit).Value = 0;


            if (company_plumbing.Checked == true)
                cmd.Parameters.Add("@company_plumbing", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_plumbing", SqlDbType.Bit).Value = 0;


            if (company_decoration.Checked == true)
                cmd.Parameters.Add("@company_decoration", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_decoration", SqlDbType.Bit).Value = 0;

            if (company_doors_windows.Checked == true)
                cmd.Parameters.Add("@company_doors_windows", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_doors_windows", SqlDbType.Bit).Value = 0;

            if (company_bricks.Checked == true)
                cmd.Parameters.Add("@company_bricks", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_bricks", SqlDbType.Bit).Value = 0;

            if (company_foundation.Checked == true)
                cmd.Parameters.Add("@company_foundation", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_foundation", SqlDbType.Bit).Value = 0;

            if (company_alarms_security_systems.Checked == true)
                cmd.Parameters.Add("@company_alarms_security_systems", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_alarms_security_systems", SqlDbType.Bit).Value = 0;

            if (company_cable_satellite_dish.Checked == true)
                cmd.Parameters.Add("@company_cable_satellite_dish", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_cable_satellite_dish", SqlDbType.Bit).Value = 0;

            if (company_ciment.Checked == true)
                cmd.Parameters.Add("@company_ciment", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_ciment", SqlDbType.Bit).Value = 0;

            if (company_other.Checked == true)
                cmd.Parameters.Add("@company_other", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_other", SqlDbType.Bit).Value = 0;

            if (company_hvac.Checked == true)
                cmd.Parameters.Add("@company_hvac", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_hvac", SqlDbType.Bit).Value = 0;

            if (company_engineer.Checked == true)
                cmd.Parameters.Add("@company_engineer", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_engineer", SqlDbType.Bit).Value = 0;

            if (company_gypse_installation.Checked == true)
                cmd.Parameters.Add("@company_gypse_installation", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_gypse_installation", SqlDbType.Bit).Value = 0;

            if (company_architech.Checked == true)
                cmd.Parameters.Add("@company_architech", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_architech", SqlDbType.Bit).Value = 0;

            if (company_gardening.Checked == true)
                cmd.Parameters.Add("@company_gardening", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_gardening", SqlDbType.Bit).Value = 0;

            if (company_roofs.Checked == true)
                cmd.Parameters.Add("@company_roofs", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_roofs", SqlDbType.Bit).Value = 0;

            if (company_flooring.Checked == true)
                cmd.Parameters.Add("@company_flooring", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_flooring", SqlDbType.Bit).Value = 0;

            if (company_basement.Checked == true)
                cmd.Parameters.Add("@company_basement", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_basement", SqlDbType.Bit).Value = 0;

            if (company_inspection.Checked == true)
                cmd.Parameters.Add("@company_inspection", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_inspection", SqlDbType.Bit).Value = 0;

            if (company_kitchen.Checked == true)
                cmd.Parameters.Add("@company_kitchen", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_kitchen", SqlDbType.Bit).Value = 0;

            if (company_electrical.Checked == true)
                cmd.Parameters.Add("@company_electrical", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_electrical", SqlDbType.Bit).Value = 0;

            if (company_locksmith.Checked == true)
                cmd.Parameters.Add("@company_locksmith", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_locksmith", SqlDbType.Bit).Value = 0;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            dgcompany_list.DataSource = ds;
            dgcompany_list.DataBind();

        }

        // finally
        {
            conn.Close();
        }



    }


    protected void dgcompany_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prCompanySearchList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        // try
        {
            conn.Open();


            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);

            if (company_all.Checked == true)
                cmd.Parameters.Add("@company_all", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_all", SqlDbType.Bit).Value = 0;


            if (company_general_contractor.Checked == true)
                cmd.Parameters.Add("@company_general_contractor", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_general_contractor", SqlDbType.Bit).Value = 0;

            if (company_interior_contractor.Checked == true)
                cmd.Parameters.Add("@company_interior_contractor", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_interior_contractor", SqlDbType.Bit).Value = 0;

            if (company_exterior_contractor.Checked == true)
                cmd.Parameters.Add("@company_exterior_contractor", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_exterior_contractor", SqlDbType.Bit).Value = 0;

            if (company_cleaning.Checked == true)
                cmd.Parameters.Add("@company_cleaning", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_cleaning", SqlDbType.Bit).Value = 0;


            if (company_painting.Checked == true)
                cmd.Parameters.Add("@company_painting", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_painting", SqlDbType.Bit).Value = 0;

            if (company_paving.Checked == true)
                cmd.Parameters.Add("@company_paving", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_paving", SqlDbType.Bit).Value = 0;


            if (company_plumbing.Checked == true)
                cmd.Parameters.Add("@company_plumbing", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_plumbing", SqlDbType.Bit).Value = 0;


            if (company_decoration.Checked == true)
                cmd.Parameters.Add("@company_decoration", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_decoration", SqlDbType.Bit).Value = 0;

            if (company_doors_windows.Checked == true)
                cmd.Parameters.Add("@company_doors_windows", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_doors_windows", SqlDbType.Bit).Value = 0;

            if (company_bricks.Checked == true)
                cmd.Parameters.Add("@company_bricks", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_bricks", SqlDbType.Bit).Value = 0;

            if (company_foundation.Checked == true)
                cmd.Parameters.Add("@company_foundation", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_foundation", SqlDbType.Bit).Value = 0;

            if (company_alarms_security_systems.Checked == true)
                cmd.Parameters.Add("@company_alarms_security_systems", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_alarms_security_systems", SqlDbType.Bit).Value = 0;

            if (company_cable_satellite_dish.Checked == true)
                cmd.Parameters.Add("@company_cable_satellite_dish", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_cable_satellite_dish", SqlDbType.Bit).Value = 0;

            if (company_ciment.Checked == true)
                cmd.Parameters.Add("@company_ciment", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_ciment", SqlDbType.Bit).Value = 0;

            if (company_other.Checked == true)
                cmd.Parameters.Add("@company_other", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_other", SqlDbType.Bit).Value = 0;

            if (company_hvac.Checked == true)
                cmd.Parameters.Add("@company_hvac", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_hvac", SqlDbType.Bit).Value = 0;

            if (company_engineer.Checked == true)
                cmd.Parameters.Add("@company_engineer", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_engineer", SqlDbType.Bit).Value = 0;

            if (company_gypse_installation.Checked == true)
                cmd.Parameters.Add("@company_gypse_installation", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_gypse_installation", SqlDbType.Bit).Value = 0;

            if (company_architech.Checked == true)
                cmd.Parameters.Add("@company_architech", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_architech", SqlDbType.Bit).Value = 0;

            if (company_gardening.Checked == true)
                cmd.Parameters.Add("@company_gardening", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_gardening", SqlDbType.Bit).Value = 0;

            if (company_roofs.Checked == true)
                cmd.Parameters.Add("@company_roofs", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_roofs", SqlDbType.Bit).Value = 0;

            if (company_flooring.Checked == true)
                cmd.Parameters.Add("@company_flooring", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_flooring", SqlDbType.Bit).Value = 0;

            if (company_basement.Checked == true)
                cmd.Parameters.Add("@company_basement", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_basement", SqlDbType.Bit).Value = 0;

            if (company_inspection.Checked == true)
                cmd.Parameters.Add("@company_inspection", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_inspection", SqlDbType.Bit).Value = 0;

            if (company_kitchen.Checked == true)
                cmd.Parameters.Add("@company_kitchen", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_kitchen", SqlDbType.Bit).Value = 0;

            if (company_electrical.Checked == true)
                cmd.Parameters.Add("@company_electrical", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_electrical", SqlDbType.Bit).Value = 0;

            if (company_locksmith.Checked == true)
                cmd.Parameters.Add("@company_locksmith", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_locksmith", SqlDbType.Bit).Value = 0;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);

            dgcompany_list.PageIndex = e.NewPageIndex;
            dgcompany_list.DataSource = dt;
            dgcompany_list.DataBind();

        }

        // finally
        {
            conn.Close();
        }

    }
}
