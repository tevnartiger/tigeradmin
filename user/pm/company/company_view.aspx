<%@ Page Language="C#" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" CodeFile="company_view.aspx.cs" Inherits="company_company_view" Title="Company Information" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

       COMPANY  INFORMATION<br />
        <br />
        <asp:HyperLink ID="company_add_link" 
            runat="server">add</asp:HyperLink> -&nbsp;  <asp:HyperLink ID="company_update_link"
            runat="server">update</asp:HyperLink> - delete
        &nbsp;<br />
        <br />
        CATEGORIES
        <br />
        <table id="tb_contractor_question" runat="server">
        <tr>
        <td valign="top">Is this contractor is also a vendor/supplier </td>
        <td>
            <asp:RadioButtonList ID="radio_contractor" runat="server" >
                <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
                <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
            </td>
        </tr>
        </table>
         <br />
         
        <table  bgcolor="#ffffcc" id="TABLE1" language="javascript" onclick="return TABLE1_onclick()">
            <tr>
                <td   ><asp:CheckBox  Text="general contractor" ID="company_general_contractor" runat="server" />
                    </td>
                <td  >
                    <asp:CheckBox  Text="cleaning" ID="company_cleaning" runat="server" /></td>
                <td >
                    <asp:CheckBox  Text="HVAC" ID="company_hvac" runat="server" /></td>
            </tr>
            <tr>
                <td  >
                    <asp:CheckBox  Text="interior contractor" ID="company_interior_contractor" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="decoration" ID="company_decoration" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="inspection" ID="company_inspection" runat="server" /></td>
            </tr>
            <tr>
                <td  >
                    <asp:CheckBox  Text="exterior contractor" ID="company_exterior_contractor" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="doors & windows" ID="company_doors_windows" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="kitchen" ID="company_kitchen" runat="server" /></td>
            </tr>
            <tr>
                <td  >
                    <asp:CheckBox  Text="alarms & security systems" ID="company_alarms_security_systems" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="electrical" ID="company_electrical" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="locksmith" ID="company_locksmith" runat="server" /></td>
            </tr>
            <tr>
                <td  >
                   <asp:CheckBox  Text="architech" ID="company_architech" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="engineer" ID="company_engineer" runat="server" /></td>
                <td  ><asp:CheckBox  Text="painting" ID="company_painting" runat="server" />
                    </td>
            </tr>
            <tr>
                <td  >
                    <asp:CheckBox  Text="basement" ID="company_basement" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="flooring" ID="company_flooring" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="paving" ID="company_paving" runat="server" /></td>
            </tr>
            <tr>
                <td  >
                    <asp:CheckBox  Text="bricks" ID="company_bricks" runat="server" /></td>
                <td  >
                   <asp:CheckBox  Text="foundation" ID="company_foundation" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="plumbing" ID="company_plumbing" runat="server" /></td>
            </tr>
            <tr>
                <td  >
                    <asp:CheckBox  Text="cable , satellite , dish" ID="company_cable_satellite_dish" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="gardening" ID="company_gardening" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="roofs" ID="company_roofs" runat="server" /></td>
            </tr>
            <tr>
                <td  >
                    <asp:CheckBox  Text="ciment" ID="company_ciment" runat="server" /></td>
                <td  >
                    <asp:CheckBox   Text="gypse installation" ID="company_gypse_installation" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="other" ID="company_other" runat="server" /></td>
            </tr>
        </table><br /> COMPANY
        <br />
        <table bgcolor="#ffffcc" >
            <tr>
                <td>
                    name</td>
                <td >
                    : 
                    <asp:Label ID="company_name" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    website</td>
                <td >
                    : 
                    <asp:Label ID="company_website" runat="server"></asp:Label></td>
            </tr>
            
            <tr>
                <td  >
                    address street</td>
                <td >
                    :
                    <asp:Label ID="company_addr_street" runat="server"></asp:Label></td>
            </tr>
            
            <tr>
                <td >
                    prov/state</td>
                <td >
                    :
                    <asp:Label ID="company_prov" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    city</td>
                <td>
                    :
                    <asp:Label ID="company_city" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td >
                    postal code</td>
                <td >
                    :
                    <asp:Label ID="company_pc" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td >
                    telephone</td>
                <td >
                    :
                    <asp:Label ID="company_tel" runat="server"></asp:Label>&nbsp;</td>
            </tr>
            <tr>
                <td  >
                    contact first name</td>
                <td >
                    :
                    <asp:Label ID="company_contact_fname" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td >
                    contact last name</td>
                <td  >
                    :
                    <asp:Label ID="company_contact_lname" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td >
                    contact tel.</td>
                <td >
                    :
                    <asp:Label ID="company_contact_tel" runat="server"></asp:Label></td>
            </tr>
       
            <tr>
                <td >
                    contact email</td>
                <td >
                    :
                    <asp:Label ID="company_contact_email" runat="server"></asp:Label></td>
            </tr>
       
        </table>
        <br />
     <br />
        <asp:HyperLink ID="link_go_back_to_list" runat="server" NavigateUrl="~/company/company_list.aspx">GO back to list</asp:HyperLink>
    </asp:Content>