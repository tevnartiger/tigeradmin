﻿<%@ Page Language="C#" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" CodeFile="financial_scenario_view.aspx.cs" Inherits="manager_Financial_financial_scenario_view" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:Label ID="Label37" runat="server" 
                    Text="<%$ Resources:Resource, lbl_financial_analyses %>"/><br /><br />
                    
<table>
<tr><td> <asp:Label ID="lbl_property" runat="server" Text="<%$ Resources:Resource, lbl_property %>"/>
                </td>
                <td>
                  <b><asp:Label ID="lbl_home_name" runat="server"></asp:Label> </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
                    <asp:HyperLink ID="update_link" Text="<%$ Resources:Resource, lbl_update %>" runat="server"></asp:HyperLink>
       </td>  
</tr>
 </table><br />
    <table style="width: 100%">
        <tr>
            <td valign="top">
<asp:Repeater runat="server" ID="rhome_view">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
        
        <tr>
          <td>
                 District&nbsp;:&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_district")%> 
                   </td>
            </tr>
           <tr>
              <td >
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_no")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_street")%></td>
            </tr>
            <tr>
                 <td >
                   <%#DataBinder.Eval(Container.DataItem, "home_city")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_prov")%></td>
               
            </tr>
            <tr>
                 <td >
                   <%#DataBinder.Eval(Container.DataItem, "home_pc")%> 
                
                   </td>
               
            </tr>   
        </table>
        </ItemTemplate>
        </asp:Repeater> 
            </td>
            <td valign="top">
                <table bgcolor="#ffffcc"  >
                    <tr>
                        <td>
                <asp:Label ID="lbl_scenario_name" runat="server" Text="<%$ Resources:Resource, lbl_scenario_name %>"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbl_name" runat="server" Width="95px"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbl_year" runat="server" Text="<%$ Resources:Resource, lbl_year %>"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                            :</td>
                        <td>
                            <asp:Label ID="label_year" runat="server" ></asp:Label>
                        </td>
                    </tr>
                    </table>
               
            </td>
        </tr>
    </table>
    <br />
    <table >
        <tr>
            <td>
               <b> <asp:Label ID="lbl_property_value" runat="server" Text="<%$ Resources:Resource, lbl_property_value %>" ></asp:Label></b>
           &nbsp :</td>
            <td>
                <asp:Label ID="lbl_home_value" runat="server" Width="65px"></asp:Label>
            </td>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td bgcolor="AliceBlue">
                <asp:Label ID="lbl_revenue" runat="server" 
                    Text="<%$ Resources:Resource, lbl_revenue %>" style="font-weight: 700"/> </td>
            <td bgcolor="AliceBlue">
                &nbsp;</td>
            <td bgcolor="AliceBlue">
                <asp:Label ID="lbl_monthly" runat="server" 
                    Text="<%$ Resources:Resource, lbl_monthly %>" style="font-weight: 700"/></td>
            <td bgcolor="AliceBlue">
                <asp:Label ID="lbl_annualy" runat="server" 
                    Text="<%$ Resources:Resource, lbl_annualy %>" style="font-weight: 700"/></td>
            <td bgcolor="AliceBlue">
                &nbsp;(<b>%) income&nbsp;</b></td>
        </tr>
        
    <asp:Repeater ID="rMfsNumberUnitbyBedroomNumber" runat="server">
    <ItemTemplate>
    <tr>
    <td> <%#DataBinder.Eval(Container.DataItem, "number_of_unit")%> &nbsp;X &nbsp;<%#DataBinder.Eval(Container.DataItem, "unit_bedroom_no")%>&nbsp;&nbsp; <asp:Label ID="lbl_bedroom" runat="server" 
                    Text="<%$ Resources:Resource, lbl_bedroom %>"/></td>
    <td></td>
    <td><asp:Label   Width="65" MaxLength="10" Text='<%#DataBinder.Eval(Container.DataItem, "mfsgu_bedroomstotalrent","{0:0.00}")%>'  runat="server"  ID="lbl_bedrooms_total_rent_m"/></td>
    <td><asp:Label ID="lbl_bedrooms_total_rent_y"  runat="server"  />
                     
        <asp:HiddenField Visible="false" ID="h_number_of_unit" Value='<%#DataBinder.Eval(Container.DataItem, "number_of_unit")%>' runat="server" /> 
        <asp:HiddenField Visible="false" ID="h_unit_bedroom_number" Value='<%#DataBinder.Eval(Container.DataItem, "unit_bedroom_no")%>' runat="server" />
        
                     </td>
        <td><asp:Label ID="lbl_bedrooms_total_rent_percent"  runat="server"  /></td>
    </tr>
    </ItemTemplate>
    </asp:Repeater>
    
    
     <asp:Repeater ID="rNumberComUnit" runat="server">
    <ItemTemplate>
    <tr>

    <td> <%#DataBinder.Eval(Container.DataItem, "mfsgu_numberofcomunit")%> &nbsp;&nbsp; <asp:Label ID="lbl_bedroom" runat="server" Text="Commercial Unit"/></td>    
   <td></td>
    <td><asp:Label   Width="65" MaxLength="10"  runat="server"  ID="lbl_commercial_total_rent_m"   Text='<%#DataBinder.Eval(Container.DataItem, "mfsgu_comtotalrent","{0:0.00}")%>' /></td>
    <td><asp:Label ID="lbl_commercial_total_rent_y" runat="server"  />
                     
        <asp:HiddenField Visible="false" ID="h_number_of_unit" Value='<%#DataBinder.Eval(Container.DataItem, "mfsgu_numberofcomunit")%>' runat="server" /> 
        
                     </td>
       <td><asp:Label ID="lbl_commercial_total_rent_percent"  runat="server"  /></td>
    </tr>
    </ItemTemplate>
    </asp:Repeater>
    
    
    
    
    
    
        <tr>
            <td>
                <asp:Label ID="Label19" runat="server" 
                     Text="<%$ Resources:Resource, lbl_parking %>"/></td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_parking_m" runat="server" Width="65px"></asp:Label>
            </td>
            <td>
                 <asp:Label ID="lbl_parking_y" runat="server" 
                     Text="test_park_y"/></td>
            <td>
                 <asp:Label ID="test_park_percent" runat="server" 
                     /></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label20" runat="server" 
                     Text="<%$ Resources:Resource, lbl_laundry %>"/></td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_laundry_m" runat="server" Width="65px"></asp:Label>
            </td>
            <td>
               <asp:Label ID="lbl_laundry_y" runat="server" 
                     Text="lbl_laundry_y"/></td>
            <td>
                <asp:Label ID="lbl_laundry_percent" runat="server" 
                     Text=""/></td>
        </tr>
        <tr>
            <td>
              <asp:Label ID="Label21" runat="server" 
                     Text="<%$ Resources:Resource, lbl_storage %>"/></td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_storage_m" runat="server" Width="65px"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl_storage_y" runat="server" 
                     Text="lbl_storage_y"/></td>
            <td>
                <asp:Label ID="lbl_storage_percent" runat="server" 
                     Text=""/></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label38" runat="server" 
                    Text="<%$ Resources:Resource, lbl_garage %>" /></td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_garage_m" runat="server" Width="65px"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl_garage_y" runat="server" Width="65px"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl_garage_percent" runat="server" Width="65px"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label101" runat="server" 
                    Text="<%$ Resources:Resource, lbl_vending_machine %>"/></td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_vending_machine_m" runat="server" Width="65px"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl_vending_machine_y" runat="server" Width="65px"></asp:Label>
            </td>
            <td>
                 <asp:Label ID="lbl_vending_machine_percent" runat="server" Width="65px"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label102" runat="server" 
                    Text="<%$ Resources:Resource, lbl_cash_machine %>" /></td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_cash_machine_m" runat="server" Width="65px"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl_cash_machine_y" runat="server" Width="65px"></asp:Label>
            </td>
            <td>
                 <asp:Label ID="lbl_cash_machine_percent" runat="server" Width="65px"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label103" runat="server" 
                    Text="<%$ Resources:Resource, lbl_other %>"/></td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_other_m2" runat="server" Width="65px"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl_other_y2" runat="server" Width="65px"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lbl_other_percent2" runat="server" Width="65px"></asp:Label></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label17" runat="server" 
                     Text="<%$ Resources:Resource, lbl_pgi %>" style="font-weight: 700"/>&nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
               <asp:Label ID="lbl_pgi_m" runat="server" 
                     Text="lbl_pgi_m"/></td>
            <td>
                <asp:Label ID="lbl_pgi_y" runat="server" 
                     Text="lbl_pgi_y"/></td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="AliceBlue">
                 <asp:Label ID="lbl_revenue_loss" runat="server" 
                     Text="<%$ Resources:Resource, lbl_revenue_loss %>" style="font-weight: 700"/></td>
            <td style="font-weight: 700" bgcolor="AliceBlue">
                <asp:Label ID="Label4" runat="server" 
                          Text="<%$ Resources:Resource, lbl_pgi_percent %>" /></td>
            <td bgcolor="AliceBlue">
               <asp:Label ID="Label7" runat="server" 
                    Text="<%$ Resources:Resource, lbl_monthly %>" style="font-weight: 700"/></td>

            <td bgcolor="AliceBlue">
                
                      <asp:Label ID="Label6" runat="server" 
                          Text="<%$ Resources:Resource, lbl_annualy %>" style="font-weight: 700" />
                </td>

            <td bgcolor="AliceBlue">
                
                      &nbsp;</td>
        </tr>
        <tr>
            <td>
                 <asp:Label ID="Label27" runat="server" 
                          Text="<%$ Resources:Resource, lbl_vacancy_rate %>" /></td>
            <td>
                <asp:Label ID="lbl_vacancy_rate" runat="server" Width="65px"></asp:Label>
            </td>
            <td>
               <asp:Label ID="lbl_vacancy_m" runat="server" 
                          Text="lbl_vacancy_rate_m" /></td>
            <td>
                <asp:Label ID="lbl_vacancy_y" runat="server" 
                          Text="lbl_vacancy_rate_y" /></td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 22px">
                 <asp:Label ID="Label28" runat="server" 
                          Text="<%$ Resources:Resource, lbl_bda %>" /></td>
            <td style="height: 22px">
                <asp:Label ID="lbl_bda_rate" runat="server" Width="65px"></asp:Label>
            </td>
            <td style="height: 22px">
                <asp:Label ID="lbl_bda_m" runat="server" 
                          Text="lbl_bda_m" /></td>
            <td style="height: 22px">
                <asp:Label ID="lbl_bda_y" runat="server" 
                          Text="lbl_bda_y" /></td>
            <td style="height: 22px">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label18" runat="server" 
                     Text="<%$ Resources:Resource, lbl_egi %>" style="font-weight: 700"/>&nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                 <asp:Label ID="lbl_egi_m" runat="server" 
                     Text="lbl_egi_m" /></td>
            <td>
                 <asp:Label ID="lbl_egi_y" runat="server" 
                     Text="lbl_egi_y" /></td>
            <td>
                 &nbsp;</td>
        </tr>
        <tr>
            <td>
                <br />
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="AliceBlue">
                <asp:Label ID="Label5" runat="server" 
                    Text="<%$ Resources:Resource, lbl_expenses%>" style="font-weight: 700"/></td>
            <td style="font-weight: 700" bgcolor="AliceBlue">
                <asp:Label ID="Label36" runat="server" 
                          Text="<%$ Resources:Resource, lbl_egi_percent %>" /></td>
            <td bgcolor="AliceBlue">
                 <asp:Label ID="Label3" runat="server" 
                     Text="<%$ Resources:Resource, lbl_monthly %>" style="font-weight: 700"/></td>
            <td bgcolor="AliceBlue">
                 <asp:Label ID="Label1" runat="server" 
                     Text="<%$ Resources:Resource, lbl_annualy %>" style="font-weight: 700"/></td>
            <td bgcolor="AliceBlue">
                 <b>(%) expense&nbsp;</b></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label8" runat="server" 
                    Text="<%$ Resources:Resource, lbl_electricity %>" />&nbsp;</td>
            <td  >
                <asp:Label ID="lbl_electricity" runat="server" Width="65px"></asp:Label>
            </td>
            <td >
                <asp:Label ID="lbl_electricity_m" runat="server" 
                    Text="lbl_electricity_m" /></td>
            <td >
                <asp:Label ID="lbl_electricity_y" runat="server" 
                    Text="lbl_electricity_y" /></td>
            <td >
                 <asp:Label ID="lbl_electricity_percent" runat="server" 
                    Text="" /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label9" runat="server" 
                    Text="<%$ Resources:Resource, lbl_energy %>" />
            </td>
            <td >
                <asp:Label ID="lbl_energy" runat="server" Width="65px"></asp:Label>
            </td>
            <td >
                <asp:Label ID="lbl_energy_m" runat="server" 
                    Text="lbl_energy _m" /></td>
            <td >
                <asp:Label ID="lbl_energy_y" runat="server" 
                    Text="lbl_energy_y" /></td>
            <td >
                 <asp:Label ID="lbl_energy_percent" runat="server" 
                    Text="" /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label10" runat="server" 
                    Text="<%$ Resources:Resource, lbl_insurances %>" /></td>
            <td >
                <asp:Label ID="lbl_insurances" runat="server" Width="65px"></asp:Label>
            </td>
            <td >
                <asp:Label ID="lbl_insurances_m" runat="server" 
                    Text="lbl_insurances_m" /></td>
            <td >
               <asp:Label ID="lbl_insurances_y" runat="server" 
                    Text="lbl_insurances_y" /></td>
            <td >
                  <asp:Label ID="lbl_insurances_percent" runat="server" 
                    Text="" /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label11" runat="server" 
                    Text="<%$ Resources:Resource, lbl_janitor %>" /></td>
            <td >
                <asp:Label ID="lbl_janitor" runat="server" Width="65px"></asp:Label>
            </td>
            <td >
                <asp:Label ID="lbl_janitor_m" runat="server" 
                    Text="lbl_janitor_m" /></td>
            <td  >
                 <asp:Label ID="lbl_janitor_y" runat="server" 
                    Text="lbl_janitor_y" /></td>
            <td  >
                 <asp:Label ID="lbl_janitor_percent" runat="server" 
                    Text="" /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label12" runat="server" 
                    Text="<%$ Resources:Resource, lbl_taxes %>"/> </td>
            <td >
                <asp:Label ID="lbl_taxes" runat="server" Width="65px"></asp:Label>
            </td>
            <td >
                <asp:Label ID="lbl_taxes_m" runat="server" 
                    Text="lbl_taxes_m"/></td>
            <td >
               <asp:Label ID="lbl_taxes_y" runat="server" 
                    Text="lbl_taxes_y"/></td>
            <td >
                <asp:Label ID="lbl_taxes_percent" runat="server" 
                    Text=""/></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label13" runat="server" 
                    Text="<%$ Resources:Resource, lbl_maintenance_repair %>" /></td>
            <td >
                <asp:Label ID="lbl_maintenance_repair" runat="server" Width="65px"></asp:Label>
            </td>
            <td >
                <asp:Label ID="lbl_maintenance_repair_m" runat="server" 
                    Text="lbl_maintenance_repair_m" /></td>
            <td >
                <asp:Label ID="lbl_maintenance_repair_y" runat="server" 
                    Text="lbl_maintenance_repair_y" /></td>
            <td >
                <asp:Label ID="lbl_maintenance_repair_percent" runat="server" 
                    Text="" /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label14" runat="server" 
                    Text="<%$ Resources:Resource, lbl_school_taxes %>" /></td>
            <td  >
                <asp:Label ID="lbl_school_taxes" runat="server" Width="65px"></asp:Label>
            </td>
            <td  >
                 <asp:Label ID="lbl_school_taxes_m" runat="server" 
                    Text="lbl_school_taxes_m" /></td>
            <td  >
                 <asp:Label ID="lbl_school_taxes_y" runat="server" 
                    Text="lbl_school_taxes_y" /></td>
            <td  >
                 <asp:Label ID="lbl_school_taxes_percent" runat="server" 
                    Text="" /></td>
        </tr>
        <tr>
            <td  >
            <asp:Label ID="Label22" runat="server" 
                    Text="<%$ Resources:Resource, lbl_management %>" />
            </td>
            <td  >
                <asp:Label ID="lbl_management" runat="server" Width="65px"></asp:Label>
            </td>
            <td ><asp:Label ID="lbl_management_m" runat="server" 
                    Text="lbl_management_m" />
            </td>
            <td  ><asp:Label ID="lbl_management_y" runat="server" 
                    Text="lbl_management_y" />
            </td>
            <td  ><asp:Label ID="lbl_management_percent" runat="server" 
                    Text="" /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label23" runat="server" 
                    Text="<%$ Resources:Resource, lbl_advertising %>" /></td>
            <td style="height: 18px">
                <asp:Label ID="lbl_advertising" runat="server" Width="65px"></asp:Label>
            </td>
            <td  >
                <asp:Label ID="lbl_advertising_m" runat="server" 
                    Text="lbl_advertising_m" /></td>
            <td  >
                <asp:Label ID="lbl_advertising_y" runat="server" 
                    Text="lbl_advertising_y" /></td>
            <td  >
                <asp:Label ID="lbl_advertising_percent" runat="server" 
                    Text="" /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label24" runat="server" 
                    Text="<%$ Resources:Resource, lbl_legal %>" /></td>
            <td >
                <asp:Label ID="lbl_legal" runat="server" Width="65px"></asp:Label>
            </td>
            <td  >
                <asp:Label ID="lbl_legal_m" runat="server" 
                    Text="lbl_legal_m>" /></td>
            <td  >
                <asp:Label ID="lbl_legal_y" runat="server" 
                    Text="lbl_legal_y" /></td>
            <td  >
                <asp:Label ID="lbl_legal_percent" runat="server" 
                    Text="" /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label25" runat="server" 
                    Text="<%$ Resources:Resource, lbl_accounting %>" /></td>
            <td >
                <asp:Label ID="lbl_accounting" runat="server" Width="65px"></asp:Label>
            </td>
            <td >
               <asp:Label ID="lbl_accounting_m" runat="server" 
                    Text="lbl_accounting_m" /></td>
            <td >
                <asp:Label ID="lbl_accounting_y" runat="server" 
                    Text="lbl_accounting_y" /></td>
            <td >
                <asp:Label ID="lbl_accounting_percent" runat="server" 
                    Text="" /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label26" runat="server" 
                    Text="<%$ Resources:Resource, lbl_other %>"/></td>
            <td >
                <asp:Label ID="lbl_other" runat="server" Width="65px"></asp:Label>
            </td>
            <td >
                <asp:Label ID="lbl_other_m" runat="server" 
                    Text="lbl_other_m"/></td>
            <td  >
                <asp:Label ID="lbl_other_y" runat="server" 
                    Text="lbl_other_y"/></td>
            <td  >
                <asp:Label ID="lbl_other_percent" runat="server" 
                    Text=""/></td>
        </tr>
        <tr>
            <td style="height: 18px">
                &nbsp;</td>
            <td style="height: 18px">
                &nbsp;</td>
            <td style="height: 18px">
                &nbsp;</td>
            <td style="height: 18px">
                &nbsp;</td>
            <td style="height: 18px">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label15" runat="server" 
                    Text="<%$ Resources:Resource, lbl_total_expenses %>" 
                    style="font-weight: 700"/></td>
            <td  >
                <asp:Label ID="lbl_total_exp_percent" runat="server"></asp:Label>
            </td>
            <td  >
                <asp:Label ID="lbl_total_expenses_m" runat="server" 
                    Text="lbl_total_expenses_m" /></td>
            <td  >
                <asp:Label ID="lbl_total_expenses_y" runat="server" 
                    Text="lbl_total_expenses_y" /></td>
            <td  >
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 18px">
                <br />
            </td>
            <td style="height: 18px">
                &nbsp;</td>
            <td style="height: 18px">
                &nbsp;</td>
            <td style="height: 18px">
                &nbsp;</td>
            <td style="height: 18px">
                &nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="AliceBlue">
                <asp:Label ID="Label16" runat="server" 
                    Text="<%$ Resources:Resource, lbl_money_flow %>" 
                    style="font-weight: 700" /></td>
            <td bgcolor="AliceBlue">
                &nbsp;</td>
            <td bgcolor="AliceBlue">
                &nbsp;</td>
            <td bgcolor="AliceBlue">
                 <asp:Label ID="Label2" runat="server" 
                     Text="<%$ Resources:Resource, lbl_annualy %>" style="font-weight: 700"/></td>
            <td bgcolor="AliceBlue">
                 &nbsp;</td>
        </tr>
        <tr>
            <td>
               <asp:Label ID="Label29" runat="server" 
                     Text="<%$ Resources:Resource, lbl_net_operating_inc %>" /></td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_net_operating_inc_y" runat="server" 
                     Text="lbl_net_operating_inc_y" /></td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
               <asp:Label ID="Label30" runat="server" 
                     Text="<%$ Resources:Resource, lbl_ads %>"/></td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_ads_y" runat="server" Width="65px" 
                    style="text-decoration: underline"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label31" runat="server" 
                     Text="<%$ Resources:Resource, lbl_liquidity %>" 
                    style="font-weight: 700" /></td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_liquidity_y" runat="server" 
                     Text="lbl_liquidity_y" /></td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                 <asp:Label ID="Label32" runat="server" 
                     Text="<%$ Resources:Resource, lbl_capitalisation %>" /></td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_capitalisation_y" runat="server" Width="65px"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label33" runat="server" 
                     Text="<%$ Resources:Resource, lbl_liquidity_capitalisation %>" 
                    style="font-weight: 700" /></td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_liquidity_capitalisation_y" runat="server" 
                     Text="lbl_liquidity_capitalisation_y" style="text-decoration: underline"/></td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
               <asp:Label ID="Label34" runat="server" 
                     Text="<%$ Resources:Resource, lbl_added_value %>" /></td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_added_value_y" runat="server" Width="65px" 
                    style="text-decoration: underline"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label35" runat="server" 
                     Text="<%$ Resources:Resource, lbl_liq_cap_value %>" 
                    style="font-weight: 700" /></td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_liq_cap_value_y" runat="server" 
                     Text="lbl_liq_cap_value_y" /></td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <br />
                </td>
            <td>
                <asp:HiddenField Visible="false" ID="h_home_id" runat="server" />
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td align="center" bgcolor="AliceBlue" colspan="4">
                <b><asp:Label ID="Label39" runat="server" 
                     Text="<%$ Resources:Resource, lbl_analysis_return %>" /></b></td>
            <td align="center" bgcolor="AliceBlue">
                &nbsp;</td>
        </tr>
        
       <tr>
            <td colspan="2">
               <asp:Label ID="Label40" runat="server" 
                     Text="<%$ Resources:Resource, lbl_appartment_cost%>" /></td>
            <td colspan="2">
                <asp:Label ID="lbl_appartment_cost" runat="server"/></td>
            <td>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label41" runat="server" 
                     Text="<%$ Resources:Resource, lbl_exploitation_ratio%>" /></td>
            <td colspan="2">
                <asp:Label ID="lbl_rde" runat="server"/></td>
            <td>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label42" runat="server" 
                     Text="<%$ Resources:Resource, lbl_min_occupation %>" /></td>
            <td colspan="2">
                <asp:Label ID="lbl_tmo" runat="server"/></td>
            <td>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label43" runat="server" 
                     Text="<%$ Resources:Resource, lbl_performance_on_net %>" /></td>
            <td colspan="2">
                <asp:Label ID="lbl_trn" runat="server"/></td>
            <td>
                &nbsp;</td>
        </tr>
        
         <tr>
            <td colspan="2">
               <asp:Label ID="Label44" runat="server" 
                     Text="<%$ Resources:Resource, lbl_gi_multiplier %>" /></td>
            <td colspan="2">
                <asp:Label ID="lbl_mbre" runat="server"></asp:Label></td>
            <td>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label47" runat="server" 
                     Text="<%$ Resources:Resource, lbl_net_income_multiplier %>" /></td>
            <td colspan="2">
           <asp:Label ID="lbl_mrn" runat="server"></asp:Label>     
            </td>
            <td>
                &nbsp;</td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label46" runat="server" 
                     Text="<%$ Resources:Resource, lbl_debt_cover_ratio %>" /></td>
            <td colspan="2">
               <asp:Label ID="lbl_rcd" runat="server"></asp:Label></td>
            <td>
                &nbsp;</td>
        </tr>
         
        
        
        
    </table>


    <br />
<asp:HiddenField Visible="false" ID="h_total_number_of_unit"  runat="server" />

    <br />

</asp:Content>

