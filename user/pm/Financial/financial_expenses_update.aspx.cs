﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.NameObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : march 25 , 2008
/// </summary>
public partial class manager_Financial_financial_expenses_update : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {


        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        if (!RegEx.IsInteger(Request.QueryString["exp_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ////////
        //////// SECURITY CHECK BEGIN //////////////////
        ////////
        NameObjectAuthorization expenseAuthorization = new NameObjectAuthorization(strconn);

        if (!expenseAuthorization.Expense(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["exp_id"]), Convert.ToInt32(Session["name_id"]), 6))
        {
              Session.Abandon();
              Response.Redirect("~/login.aspx");
        }
        ////////
        //////// SECURITY CHECK END //////////////////
        ////////
        if (!(Page.IsPostBack))
        {
            int home_id = 0 ;

            

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prExpenseId", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@expense_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["exp_id"]);

            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                DateTime expense_date_paid = new DateTime();

                while (dr.Read() == true)
                {

                    home_id = Convert.ToInt32(dr["home_id"]);
                    h_h_id.Value = home_id.ToString();

                    lbl_property2.Text = Convert.ToString(dr["home_name"]);
                    expense_date_paid = Convert.ToDateTime(dr["expense_date_paid"]);

                    ddl_update_date_received_d.SelectedValue = expense_date_paid.Day.ToString();
                    h_d.Value = expense_date_paid.Day.ToString();

                    ddl_update_date_received_m.SelectedValue = expense_date_paid.Month.ToString();
                    h_m.Value = expense_date_paid.Month.ToString();

                    ddl_update_date_received_y.SelectedValue = expense_date_paid.Year.ToString();
                    h_y.Value = expense_date_paid.Year.ToString();


                    lbl_month.Text = expense_date_paid.Month.ToString();
                    lbl_year.Text = expense_date_paid.Year.ToString();

                    lbl_expense_categ.Text = GetExpenseCateg(Convert.ToInt32(dr["expensecateg_id"]));
                   
                    tbx_expense_reference.Text = Convert.ToString(dr["expense_reference"]);
                    tbx_expense_comments.Text = Convert.ToString(dr["expense_comments"]);

                    tbx_expense_amount.Text = String.Format("{0:0.00}", Convert.ToString(dr["expense_amount"]));

                }

            }

            finally
            {
                conn.Close();
            }

            tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
            rhome_view.DataBind();
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_update_Click(object sender, EventArgs e)
    {

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime paid_date = new DateTime();
        tiger.Date d = new tiger.Date();
        paid_date = Convert.ToDateTime(d.DateCulture(ddl_update_date_received_m.SelectedValue, ddl_update_date_received_d.SelectedValue, ddl_update_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prExpenseUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@expense_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["exp_id"]);
            cmd.Parameters.Add("@expense_date_paid", SqlDbType.DateTime).Value = paid_date;
            cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_expense_amount.Text));
            cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(RegEx.getText(tbx_expense_reference.Text));
            cmd.Parameters.Add("@expense_comments", SqlDbType.Text).Value = Convert.ToString(RegEx.getText(tbx_expense_comments.Text));
            
            //execute the insert
            cmd.ExecuteReader();


        }
       catch (Exception error)
        {
          //  tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }

        string url = "financial_expenses.aspx?h_id="+ h_h_id.Value+"&m="+h_m.Value+"&d="+h_d.Value+"&y="+h_y.Value;


        Response.Redirect(url);

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="expensecateg_id"></param>
    /// <returns></returns>
    protected string GetExpenseCateg(int expensecateg_id)
    {
        string expensecateg = "";

        switch (expensecateg_id)
        {
            case 1:
                expensecateg = Resources.Resource.lbl_electricity;
                break;
            case 2:
                expensecateg = Resources.Resource.lbl_energy;
                break;
            case 3:
                expensecateg = Resources.Resource.lbl_insurances;
                break;

            case 4:
                expensecateg = Resources.Resource.lbl_janitor;
                break;
            case 5:
                expensecateg = Resources.Resource.lbl_taxes;
                break;
            case 6:
                expensecateg = Resources.Resource.lbl_maintenance_repair;
                break;


            case 7:
                expensecateg = Resources.Resource.lbl_school_taxes;
                break;
            case 8:
                expensecateg = Resources.Resource.lbl_management;
                break;
            case 9:
                expensecateg = Resources.Resource.lbl_advertising;
                break;



            case 10:
                expensecateg = Resources.Resource.lbl_legal;
                break;
            case 11:
                expensecateg = Resources.Resource.lbl_accounting;
                break;
            case 12:
                expensecateg = Resources.Resource.lbl_other;
                break;

        }

        return expensecateg;
    }
}
