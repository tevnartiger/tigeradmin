﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="financial_group_expenses_graph.aspx.cs" Inherits="manager_Financial_financial_group_expenses_graph" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<asp:Label ID="Label2" runat="server" 
                    Text="<%$ Resources:Resource, lbl_expenses%>" /><br /><br />
<table>
<tr><td> <asp:Label ID="lbl_group" runat="server" Text="<%$ Resources:Resource, lbl_home_group%>"/>
                </td>
                <td>
       <asp:DropDownList ID="ddl_group_id" DataValueField="group_id"
               DataTextField="group_name"   runat="server" 
               autopostback="true" OnSelectedIndexChanged="ddl_group_id_SelectedIndexChanged" />
   </td>  
</tr>
 </table><br />
    <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
 
 <table style="width: 100%">
        <tr>
            <td bgcolor="aliceblue" width="300px" valign="top">
        <asp:Repeater runat="server" ID="rhome_view">
        <ItemTemplate>
          <%#DataBinder.Eval(Container.DataItem, "home_name")%><br />
                   
       
        </ItemTemplate>
        </asp:Repeater>
            </td>
            <td valign="top">
                <table bgcolor="#ffffcc"  >
                    <tr>
                        <td>
                          <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, txt_month %>"></asp:Label>&nbsp;/&nbsp;<asp:Label ID="lbl_year" runat="server" Text="<%$ Resources:Resource, lbl_year %>"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_date_received_m" runat="server" >
                                <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                            </asp:DropDownList>
                            &nbsp; / &nbsp;
                            <asp:DropDownList ID="ddl_date_received_y" runat="server" 
                                ><asp:ListItem>1980</asp:ListItem>
                            <asp:ListItem>1981</asp:ListItem>
                            <asp:ListItem>1982</asp:ListItem>
                            <asp:ListItem>1983</asp:ListItem>
                            <asp:ListItem>1984</asp:ListItem>
                            <asp:ListItem>1985</asp:ListItem>
                            <asp:ListItem>1986</asp:ListItem>
                            <asp:ListItem>1987</asp:ListItem>
                            <asp:ListItem>1988</asp:ListItem>
                            <asp:ListItem>1989</asp:ListItem>
                            <asp:ListItem>1990</asp:ListItem>
                            <asp:ListItem>1991</asp:ListItem>
                            <asp:ListItem>1992</asp:ListItem>
                            <asp:ListItem>1993</asp:ListItem>
                            <asp:ListItem>1994</asp:ListItem>
                            <asp:ListItem>1995</asp:ListItem>
                            <asp:ListItem>1996</asp:ListItem>
                            <asp:ListItem>1997</asp:ListItem>
                            <asp:ListItem>1998</asp:ListItem>
                            <asp:ListItem>1999</asp:ListItem>
                            <asp:ListItem>2000</asp:ListItem>
                                <asp:ListItem>2001</asp:ListItem>
                                <asp:ListItem>2002</asp:ListItem>
                                <asp:ListItem>2003</asp:ListItem>
                                <asp:ListItem>2004</asp:ListItem>
                                <asp:ListItem>2005</asp:ListItem>
                                <asp:ListItem>2006</asp:ListItem>
                                <asp:ListItem>2007</asp:ListItem>
                                <asp:ListItem>2008</asp:ListItem>
                                <asp:ListItem>2009</asp:ListItem>
                                <asp:ListItem>2010</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbl_chartype" runat="server" Text="<%$ Resources:Resource, lbl_chartype %>"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_chartype" runat="server">
                                <asp:ListItem Value="1"  Text="<%$ Resources:Resource, lbl_pie %>" ></asp:ListItem>
                                <asp:ListItem Value="2"  Text="<%$ Resources:Resource, lbl_histogramme %>" ></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            <asp:Label ID="lbl_dimension" runat="server" Text="<%$ Resources:Resource, lbl_dimension %>"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_dimension" runat="server">
                                <asp:ListItem Value="2" Text="<%$ Resources:Resource, lbl_2D %>"></asp:ListItem>
                                <asp:ListItem Value="3" Text="<%$ Resources:Resource, lbl_3D %>"></asp:ListItem>
                            </asp:DropDownList>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Button ID="Button1" runat="server" 
                                Text="<%$ Resources:Resource, lbl_view %>" onclick="btn_view_Click" />
                        </td>
                    </tr>
                    </table>
               
                <br />
                <asp:Button ID="btn_view_graph" runat="server" onclick="btn_view_graph_Click" 
                    Text="<%$ Resources:Resource, lbl_view_income_graph %>" />
               
            </td>
        </tr>
    </table><br />
 
     <asp:Chart EnableViewState="true"  ID="Chart3" runat="server">
      
        <ChartAreas >
            <asp:ChartArea BorderColor="White" 
                           BorderWidth="20"
                           
                           BackColor="white"
                           Name="ChartArea1">
                            
            </asp:ChartArea>        
        </ChartAreas>
    </asp:Chart>
<br />
<asp:Chart EnableViewState="true"   ID="Chart1" runat="server">
      
        <ChartAreas >
            <asp:ChartArea  BorderColor="White" 
                           BorderWidth="20"
                           
                           BackColor="white"
                           Name="ChartArea1">
                            
            </asp:ChartArea>
          
        </ChartAreas>
    </asp:Chart>

</asp:Content>

