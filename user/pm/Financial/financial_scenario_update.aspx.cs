﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.NameObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : march 3, 2008
/// </summary>

public partial class manager_Financial_financial_scenario_update : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!RegEx.IsInteger(Request.QueryString["mfs_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }


        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        ////////
        //////// SECURITY CHECK BEGIN //////////////////
        ////////
        NameObjectAuthorization mfsAuthorization = new NameObjectAuthorization(strconn);

        if (!mfsAuthorization.FinancialMoneyFlowScenario(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["mfs_id"]), Convert.ToInt32(Session["name_id"]),6))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ////////
        //////// SECURITY CHECK END //////////////////
        ////////


        if (!(Page.IsPostBack))
        {
            reg_name.ValidationExpression = RegEx.getText();
           
            tiger.PM h = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            // int home_count = h.getPMHomeCount(Convert.ToInt32(Session["schema_id"]),Convert.ToInt32(Session["name_id"]));
            //  string link_to_unit = "";
            // if (home_count > 0)
            {
                int home_id = 0;

                tiger.Financial f = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


                home_id = f.getFinancialMfsHome(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["mfs_id"]));

                h_home_id.Value = home_id.ToString();

                //tiger.Financial f = new tiger.Financial(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                rMfsNumberUnitbyBedroomNumber.DataSource = f.getMfsNumberUnitbyBedroomNumber(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["mfs_id"]));
                rMfsNumberUnitbyBedroomNumber.DataBind();

                rMfsNumberComUnit.DataSource = f.getMfsNumberComUnit(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["mfs_id"]));
                rMfsNumberComUnit.DataBind();

                

                /*
                                ddl_home_id.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                                ddl_home_id.SelectedValue = Convert.ToString(home_id);
                                ddl_home_id.DataBind();

                */


                //To view the address of the property

                tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
                rhome_view.DataBind();

            }
            

            //**************************************************************************************************************
            //**************************************************************************************************************
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prFinancialMoneyFlowScenarioView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("mfs_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["mfs_id"]);
            //  try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {

                    tbx_name.Text = dr["mfs_name"].ToString();

                    tbx_parking_m.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_parking"]));
                    tbx_storage_m.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_storage"]));
                    tbx_laundry_m.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_laundry"]));


                    //this income section was added after the other, therefore the initial value
                    // of those incume were NULL in the database
                    //***************************************************************************
                    //*************************************************************************

                    if (dr["mfs_garage"] == DBNull.Value)
                        tbx_garage_m.Text = String.Format("{0:0.00}", 0.00);
                    else
                        tbx_garage_m.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_garage"]));


                    if (dr["mfs_vending_machine"] == DBNull.Value)
                        tbx_vending_machine_m.Text = String.Format("{0:0.00}", 0.00);
                    else
                        tbx_vending_machine_m.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_vending_machine"]));


                    if (dr["mfs_cash_machine"] == DBNull.Value)
                        tbx_cash_machine_m.Text = String.Format("{0:0.00}", 0.00);
                    else
                        tbx_cash_machine_m.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_cash_machine"]));



                    if (dr["mfs_other"] == DBNull.Value)
                        tbx_other_m2.Text = String.Format("{0:0.00}", 0.00);
                    else
                        tbx_other_m2.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_other"]));

                    //***************************************************************************
                    //*************************************************************************



                    tbx_vacancy_rate.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_vacancyrate"]));
                    tbx_bda_rate.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_bdarate"]));


                    lbl_home_name.Text = dr["home_name"].ToString();
                    tbx_home_value.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_home_value"]));
                    tbx_electricity_m.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_electricityrate"]));
                    tbx_energy_m.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_energyrate"]));
                    tbx_insurances_m.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_insurancerate"]));
                    tbx_janitor_m.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_janitorrate"]));
                    tbx_taxes_m.Text  = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_propertyrate"]));
                    tbx_maintenance_repair_m.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_maintenancerate"]));
                    tbx_school_taxes_m.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_schoolrate"]));
                    tbx_management_m.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_managementrate"]));
                    tbx_advertising_m.Text= String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_advertiserate"]));
                    tbx_legal_m.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_legalrate"]));
                    tbx_accounting_m.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_accountingrate"]));
                    tbx_other_m.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_otherrate"]));

                    tbx_ads_y.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_ads"]));
                    tbx_capitalisation_y.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_capitalisation"]));
                    tbx_added_value_y.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mfs_addedvalue"]));


                    ddl_year.SelectedValue = dr["mfs_year"].ToString();

                }

            }

            //finally
            {
                conn.Close();
            }

            //**************************************************************************************************************
            //**************************************************************************************************************


            decimal month_total = 0;
            decimal year_total = 0;
            decimal vacancy_rate = 0;
            decimal bad_debt_allowance_rate = 0;

            decimal electricity_rate = 0;
            decimal energy_rate = 0;
            decimal insurance_rate = 0;
            decimal janitor_rate = 0;
            decimal property_taxe_rate = 0;
            decimal maintenance_repair_rate = 0;
            decimal school_taxe_rate = 0;
            decimal management_rate = 0;
            decimal advertising_rate = 0;
            decimal legal_rate = 0;
            decimal accounting_rate = 0;
            decimal other_rate = 0;


            decimal total_expenses_m = 0;
            decimal total_expenses_y = 0;
            double number_of_unit = 0;



            for (int i = 0; i < rMfsNumberUnitbyBedroomNumber.Items.Count; i++)
            {
                TextBox bedrooms_total_rent_m = (TextBox)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("tbx_bedrooms_total_rent_m");
                Label lbl_bedrooms_total_rent_y = (Label)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_y");
                HiddenField h_number_of_unit = (HiddenField)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("h_number_of_unit");

                lbl_bedrooms_total_rent_y.Text =String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text)) * 12));

                month_total = month_total + Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text));
                number_of_unit = number_of_unit + Convert.ToDouble(RegEx.getMoney(h_number_of_unit.Value));
            }



            for (int i = 0; i < rMfsNumberComUnit.Items.Count; i++)
            {
                TextBox commercial_total_rent_m = (TextBox)rMfsNumberComUnit.Items[i].FindControl("tbx_commercial_total_rent_m");
                Label lbl_commercial_total_rent_y = (Label)rMfsNumberComUnit.Items[i].FindControl("lbl_commercial_total_rent_y");

                HiddenField h_number_of_unit = (HiddenField)rMfsNumberComUnit.Items[i].FindControl("h_number_of_unit");

                lbl_commercial_total_rent_y.Text = String.Format("{0:0.00}", Convert.ToDecimal(RegEx.getMoney(commercial_total_rent_m.Text)) * 12);

                month_total = month_total + Convert.ToDecimal(RegEx.getMoney(commercial_total_rent_m.Text));

                number_of_unit = number_of_unit + Convert.ToDouble(RegEx.getMoney(h_number_of_unit.Value));
            }

            h_total_number_of_unit.Value = number_of_unit.ToString();








            month_total = month_total + Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text));


            year_total = month_total * 12;

            lbl_pgi_m.Text = String.Format("{0:0.00}", month_total);
            lbl_pgi_y.Text = String.Format("{0:0.00}", year_total);

            // get the income for parking, laundry and storage
            lbl_parking_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text)) * 12));
            lbl_storage_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text)) * 12));
            lbl_laundry_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text)) * 12));

            lbl_garage_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text)) * 12));
            lbl_vending_machine_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text)) * 12));
            lbl_cash_machine_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text)) * 12));
            lbl_other_y2.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text)) * 12));


            // get the vacancy_rate and bad_debt_allowance_rate
            vacancy_rate = Convert.ToDecimal(RegEx.getMoney(tbx_vacancy_rate.Text)) / 100;
            bad_debt_allowance_rate = Convert.ToDecimal(RegEx.getMoney(tbx_bda_rate.Text)) / 100;


            // Income Loss , for vacancy
            lbl_vacancy_m.Text = String.Format("{0:0.00}",(vacancy_rate * month_total));
            lbl_vacancy_y.Text = String.Format("{0:0.00}",(vacancy_rate * year_total));


            // Income Loss , for bad debt allowance
            lbl_bda_m.Text = String.Format("{0:0.00}",(bad_debt_allowance_rate * month_total));
            lbl_bda_y.Text = String.Format("{0:0.00}", (bad_debt_allowance_rate * year_total));


            // total effective gross income for month and year
            lbl_egi_m.Text = String.Format("{0:0.00}",(month_total - Convert.ToDecimal(RegEx.getMoney(lbl_vacancy_m.Text)) - Convert.ToDecimal(RegEx.getMoney(lbl_bda_m.Text))));
            lbl_egi_y.Text = String.Format("{0:0.00}", (year_total - Convert.ToDecimal(RegEx.getMoney(lbl_vacancy_y.Text)) - Convert.ToDecimal(RegEx.getMoney(lbl_bda_y.Text))));

            /// get the rate of utilities , management ,advertisement , taxes etc...
            /// 
            electricity_rate = Convert.ToDecimal(RegEx.getMoney(tbx_electricity_m.Text)) / 100;
            energy_rate = Convert.ToDecimal(RegEx.getMoney(tbx_energy_m.Text)) / 100;
            insurance_rate = Convert.ToDecimal(RegEx.getMoney(tbx_insurances_m.Text)) / 100;

            janitor_rate = Convert.ToDecimal(RegEx.getMoney(tbx_janitor_m.Text)) / 100;
            property_taxe_rate = Convert.ToDecimal(RegEx.getMoney(tbx_taxes_m.Text)) / 100;
            maintenance_repair_rate = Convert.ToDecimal(RegEx.getMoney(tbx_maintenance_repair_m.Text)) / 100;

            school_taxe_rate = Convert.ToDecimal(RegEx.getMoney(tbx_school_taxes_m.Text)) / 100;
            management_rate = Convert.ToDecimal(RegEx.getMoney(tbx_management_m.Text)) / 100;
            advertising_rate = Convert.ToDecimal(RegEx.getMoney(tbx_advertising_m.Text)) / 100;
            legal_rate = Convert.ToDecimal(RegEx.getMoney(tbx_legal_m.Text)) / 100;
            accounting_rate = Convert.ToDecimal(RegEx.getMoney(tbx_accounting_m.Text)) / 100;
            other_rate = Convert.ToDecimal(RegEx.getMoney(tbx_other_m.Text)) / 100;

            // eletricity expenses
            lbl_electricity_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * electricity_rate));
            lbl_electricity_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * electricity_rate));

            // energy expenses
            lbl_energy_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * energy_rate));
            lbl_energy_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * energy_rate));

            // insurance expenses
            lbl_insurances_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * insurance_rate));
            lbl_insurances_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * insurance_rate));

            // janitor expenses
            lbl_janitor_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * janitor_rate));
            lbl_janitor_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * janitor_rate));

            // property taxes expenses
            lbl_taxes_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * property_taxe_rate));
            lbl_taxes_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * property_taxe_rate));

            // maintenance & repair expenses
            lbl_maintenance_repair_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * maintenance_repair_rate));
            lbl_maintenance_repair_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * maintenance_repair_rate));

            // school taxes expenses
            lbl_school_taxes_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * school_taxe_rate));
            lbl_school_taxes_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * school_taxe_rate));

            // management expenses
            lbl_management_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * management_rate));
            lbl_management_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * management_rate));

            // advertising expenses
            lbl_advertising_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * advertising_rate));
            lbl_advertising_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * advertising_rate));

            // legal expenses
            lbl_legal_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * legal_rate));
            lbl_legal_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * legal_rate));


            // accounting expenses
            lbl_accounting_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * accounting_rate));
            lbl_accounting_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * accounting_rate));

            // Other expenses
            lbl_other_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * other_rate));
            lbl_other_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * other_rate));



            // Montly Gross total expenses


            total_expenses_m = Convert.ToDecimal(RegEx.getMoney(lbl_electricity_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_energy_m.Text)) +
                                       Convert.ToDecimal(RegEx.getMoney(lbl_insurances_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_janitor_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_taxes_m.Text)) +
                                       Convert.ToDecimal(RegEx.getMoney(lbl_maintenance_repair_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_school_taxes_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_management_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_advertising_m.Text)) +
                                      Convert.ToDecimal(RegEx.getMoney(lbl_legal_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_accounting_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_other_m.Text));


            total_expenses_y = Convert.ToDecimal(RegEx.getMoney(lbl_electricity_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_energy_y.Text)) +
                                        Convert.ToDecimal(RegEx.getMoney(lbl_insurances_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_janitor_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_taxes_y.Text)) +
                                        Convert.ToDecimal(RegEx.getMoney(lbl_maintenance_repair_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_school_taxes_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_management_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_advertising_y.Text)) +
                                       Convert.ToDecimal(RegEx.getMoney(lbl_legal_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_accounting_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_other_y.Text));

            lbl_total_expenses_m.Text = String.Format("{0:0.00}", total_expenses_m);
            lbl_total_expenses_y.Text = String.Format("{0:0.00}", total_expenses_y);


            lbl_total_exp_percent.Text = String.Format("{0:0.00}", ((electricity_rate + energy_rate + insurance_rate + janitor_rate + property_taxe_rate + maintenance_repair_rate + school_taxe_rate +
                                          management_rate + advertising_rate + legal_rate + accounting_rate + other_rate) * 100));


            // Net gross exploiation income
            lbl_net_operating_inc_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) - Convert.ToDecimal(RegEx.getMoney(lbl_total_expenses_y.Text))));

            // Generated liquidity ( before income taxes )
            lbl_liquidity_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_net_operating_inc_y.Text)) - Convert.ToDecimal(RegEx.getMoney(tbx_ads_y.Text))));

            // Generated liquidity + capitalisation
            lbl_liquidity_capitalisation_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_liquidity_y.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_capitalisation_y.Text))));

            //Liquidity + Cap. +Added value
            lbl_liq_cap_value_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_liquidity_capitalisation_y.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_added_value_y.Text))));


            // Analysis of Return calculations

            double property_value = 0;
            double added_value = 0;
            double total_number_of_unit = Convert.ToDouble(h_total_number_of_unit.Value);
            double appartment_cost = 0;

            property_value = Convert.ToDouble(tbx_home_value.Text);


            property_value = property_value + Convert.ToDouble(tbx_added_value_y.Text);

            appartment_cost = (property_value / total_number_of_unit);

            lbl_appartment_cost.Text = String.Format("{0:0.00}", appartment_cost);
            lbl_mbre.Text =String.Format("{0:0.00}", ((property_value / Convert.ToDouble(lbl_egi_y.Text))));
            lbl_rde.Text = String.Format("{0:0.00}", ((Convert.ToDouble(lbl_total_expenses_y.Text) / Convert.ToDouble(lbl_egi_y.Text))));

            lbl_tmo.Text = String.Format("{0:0.00}", ((Convert.ToDouble(lbl_total_expenses_y.Text) + Convert.ToDouble(tbx_ads_y.Text)) / Convert.ToDouble(lbl_pgi_y.Text)));

            lbl_trn.Text = String.Format("{0:0.00}", (Convert.ToDouble(lbl_net_operating_inc_y.Text) / property_value));

            lbl_mrn.Text = String.Format("{0:0.00}", (property_value / Convert.ToDouble(lbl_net_operating_inc_y.Text)));

            lbl_rcd.Text = String.Format("{0:0.00}", ((Convert.ToDouble(lbl_net_operating_inc_y.Text) / Convert.ToDouble(tbx_ads_y.Text))));





        }
    }

    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {

        //  rMfsNumberUnitbyBedroomNumber.DataSource = f.getMfsNumberUnitbyBedroomNumber(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["mfs_id"]));
        // rMfsNumberUnitbyBedroomNumber.DataBind();
    }

    protected void ddl_scenario_name_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void ddl_year_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void Caculate_Click(object sender, EventArgs e)
    {

        decimal month_total = 0;
        decimal year_total = 0;
        decimal vacancy_rate = 0;
        decimal bad_debt_allowance_rate = 0;

        decimal electricity_rate = 0;
        decimal energy_rate = 0;
        decimal insurance_rate = 0;
        decimal janitor_rate = 0;
        decimal property_taxe_rate = 0;
        decimal maintenance_repair_rate = 0;
        decimal school_taxe_rate = 0;
        decimal management_rate = 0;
        decimal advertising_rate = 0;
        decimal legal_rate = 0;
        decimal accounting_rate = 0;
        decimal other_rate = 0;


        decimal total_expenses_m = 0;
        decimal total_expenses_y = 0;
        decimal number_of_unit = 0;


        for (int i = 0; i < rMfsNumberUnitbyBedroomNumber.Items.Count; i++)
        {
            TextBox bedrooms_total_rent_m = (TextBox)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("tbx_bedrooms_total_rent_m");
            Label lbl_bedrooms_total_rent_y = (Label)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_y");
            HiddenField h_number_of_unit = (HiddenField)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("h_number_of_unit");

            lbl_bedrooms_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text)) * 12));

            month_total = month_total + Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text));

            number_of_unit = number_of_unit + Convert.ToDecimal(RegEx.getMoney(h_number_of_unit.Value));
        }


        for (int i = 0; i < rMfsNumberComUnit.Items.Count; i++)
        {
            TextBox commercial_total_rent_m = (TextBox)rMfsNumberComUnit.Items[i].FindControl("tbx_commercial_total_rent_m");
            Label lbl_commercial_total_rent_y = (Label)rMfsNumberComUnit.Items[i].FindControl("lbl_commercial_total_rent_y");

            HiddenField h_number_of_unit = (HiddenField)rMfsNumberComUnit.Items[i].FindControl("h_number_of_unit");

            lbl_commercial_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(commercial_total_rent_m.Text)) * 12));

            month_total = month_total + Convert.ToDecimal(RegEx.getMoney(commercial_total_rent_m.Text));

            number_of_unit = number_of_unit + Convert.ToDecimal(RegEx.getMoney(h_number_of_unit.Value));

        }

        h_total_number_of_unit.Value = number_of_unit.ToString();





        month_total = month_total + Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text));

        year_total = month_total * 12;

        lbl_pgi_m.Text = String.Format("{0:0.00}", month_total);
        lbl_pgi_y.Text = String.Format("{0:0.00}", year_total);

        // get the income for parking, laundry and storage
        lbl_parking_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text)) * 12));
        lbl_storage_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text)) * 12));
        lbl_laundry_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text)) * 12));

        lbl_garage_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text)) * 12));
        lbl_vending_machine_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text)) * 12));
        lbl_cash_machine_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text)) * 12));
        lbl_other_y2.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text)) * 12));


        // get the vacancy_rate and bad_debt_allowance_rate
        vacancy_rate = Convert.ToDecimal(RegEx.getMoney(tbx_vacancy_rate.Text)) / 100;
        bad_debt_allowance_rate = Convert.ToDecimal(RegEx.getMoney(tbx_bda_rate.Text)) / 100;


        // Income Loss , for vacancy
        lbl_vacancy_m.Text = String.Format("{0:0.00}",(vacancy_rate * month_total));
        lbl_vacancy_y.Text = String.Format("{0:0.00}",(vacancy_rate * year_total));


        // Income Loss , for bad debt allowance
        lbl_bda_m.Text = String.Format("{0:0.00}", (bad_debt_allowance_rate * month_total));
        lbl_bda_y.Text = String.Format("{0:0.00}", (bad_debt_allowance_rate * year_total));


        // total effective gross income for month and year
        lbl_egi_m.Text = String.Format("{0:0.00}", (month_total - Convert.ToDecimal(RegEx.getMoney(lbl_vacancy_m.Text)) - Convert.ToDecimal(RegEx.getMoney(lbl_bda_m.Text))));
        lbl_egi_y.Text = String.Format("{0:0.00}", (year_total - Convert.ToDecimal(RegEx.getMoney(lbl_vacancy_y.Text)) - Convert.ToDecimal(RegEx.getMoney(lbl_bda_y.Text))));


        /// get the rate of utilities , management ,advertisement , taxes etc...
        /// 
        electricity_rate = Convert.ToDecimal(RegEx.getMoney(tbx_electricity_m.Text)) / 100;
        energy_rate = Convert.ToDecimal(RegEx.getMoney(tbx_energy_m.Text)) / 100;
        insurance_rate = Convert.ToDecimal(RegEx.getMoney(tbx_insurances_m.Text)) / 100;

        janitor_rate = Convert.ToDecimal(RegEx.getMoney(tbx_janitor_m.Text)) / 100;
        property_taxe_rate = Convert.ToDecimal(RegEx.getMoney(tbx_taxes_m.Text)) / 100;
        maintenance_repair_rate = Convert.ToDecimal(RegEx.getMoney(tbx_maintenance_repair_m.Text)) / 100;

        school_taxe_rate = Convert.ToDecimal(RegEx.getMoney(tbx_school_taxes_m.Text)) / 100;
        management_rate = Convert.ToDecimal(RegEx.getMoney(tbx_management_m.Text)) / 100;
        advertising_rate = Convert.ToDecimal(RegEx.getMoney(tbx_advertising_m.Text)) / 100;
        legal_rate = Convert.ToDecimal(RegEx.getMoney(tbx_legal_m.Text)) / 100;
        accounting_rate = Convert.ToDecimal(RegEx.getMoney(tbx_accounting_m.Text)) / 100;
        other_rate = Convert.ToDecimal(RegEx.getMoney(tbx_other_m.Text)) / 100;

        // eletricity expenses
        lbl_electricity_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * electricity_rate));
        lbl_electricity_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * electricity_rate));

        // energy expenses
        lbl_energy_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * energy_rate));
        lbl_energy_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * energy_rate));

        // insurance expenses
        lbl_insurances_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * insurance_rate));
        lbl_insurances_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * insurance_rate));

        // janitor expenses
        lbl_janitor_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * janitor_rate));
        lbl_janitor_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * janitor_rate));

        // property taxes expenses
        lbl_taxes_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * property_taxe_rate));
        lbl_taxes_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * property_taxe_rate));

        // maintenance & repair expenses
        lbl_maintenance_repair_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * maintenance_repair_rate));
        lbl_maintenance_repair_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * maintenance_repair_rate));

        // school taxes expenses
        lbl_school_taxes_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * school_taxe_rate));
        lbl_school_taxes_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * school_taxe_rate));

        // management expenses
        lbl_management_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * management_rate));
        lbl_management_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * management_rate));

        // advertising expenses
        lbl_advertising_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * advertising_rate));
        lbl_advertising_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * advertising_rate));

        // legal expenses
        lbl_legal_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * legal_rate));
        lbl_legal_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * legal_rate));


        // accounting expenses
        lbl_accounting_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * accounting_rate));
        lbl_accounting_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * accounting_rate));

        // Other expenses
        lbl_other_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * other_rate));
        lbl_other_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * other_rate));




        // Montly Gross total expenses


        total_expenses_m = Convert.ToDecimal(RegEx.getMoney(lbl_electricity_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_energy_m.Text)) +
                                   Convert.ToDecimal(RegEx.getMoney(lbl_insurances_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_janitor_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_taxes_m.Text)) +
                                   Convert.ToDecimal(RegEx.getMoney(lbl_maintenance_repair_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_school_taxes_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_management_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_advertising_m.Text)) +
                                  Convert.ToDecimal(RegEx.getMoney(lbl_legal_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_accounting_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_other_m.Text));


        total_expenses_y = Convert.ToDecimal(RegEx.getMoney(lbl_electricity_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_energy_y.Text)) +
                                    Convert.ToDecimal(RegEx.getMoney(lbl_insurances_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_janitor_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_taxes_y.Text)) +
                                    Convert.ToDecimal(RegEx.getMoney(lbl_maintenance_repair_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_school_taxes_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_management_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_advertising_y.Text)) +
                                   Convert.ToDecimal(RegEx.getMoney(lbl_legal_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_accounting_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_other_y.Text));

        lbl_total_expenses_m.Text = String.Format("{0:0.00}", total_expenses_m);
        lbl_total_expenses_y.Text = String.Format("{0:0.00}", total_expenses_y);


        lbl_total_exp_percent.Text = String.Format("{0:0.00}", ((electricity_rate + energy_rate + insurance_rate + janitor_rate + property_taxe_rate + maintenance_repair_rate + school_taxe_rate +
                                      management_rate + advertising_rate + legal_rate + accounting_rate + other_rate) * 100));


        // Net gross exploiation income
        lbl_net_operating_inc_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) - Convert.ToDecimal(RegEx.getMoney(lbl_total_expenses_y.Text))));

        // Generated liquidity ( before income taxes )
        lbl_liquidity_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_net_operating_inc_y.Text)) - Convert.ToDecimal(RegEx.getMoney(tbx_ads_y.Text))));

        // Generated liquidity + capitalisation
        lbl_liquidity_capitalisation_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_liquidity_y.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_capitalisation_y.Text))));

        //Liquidity + Cap. +Added value
        lbl_liq_cap_value_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_liquidity_capitalisation_y.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_added_value_y.Text))));


        lbl_appartment_cost.Text = "";
        lbl_mbre.Text = "";
        lbl_rde.Text = "";
        lbl_tmo.Text = "";
        lbl_trn.Text = "";
        lbl_mrn.Text = "";

        lbl_rcd.Text = "";
    }



    protected void btn_calculate_expenses_Click(object sender, EventArgs e)
    {
        decimal month_total = 0;
        decimal year_total = 0;
        decimal vacancy_rate = 0;
        decimal bad_debt_allowance_rate = 0;

        decimal electricity_rate = 0;
        decimal energy_rate = 0;
        decimal insurance_rate = 0;
        decimal janitor_rate = 0;
        decimal property_taxe_rate = 0;
        decimal maintenance_repair_rate = 0;
        decimal school_taxe_rate = 0;
        decimal management_rate = 0;
        decimal advertising_rate = 0;
        decimal legal_rate = 0;
        decimal accounting_rate = 0;
        decimal other_rate = 0;


        decimal total_expenses_m = 0;
        decimal total_expenses_y = 0;
        decimal number_of_unit = 0;


        for (int i = 0; i < rMfsNumberUnitbyBedroomNumber.Items.Count; i++)
        {
            TextBox bedrooms_total_rent_m = (TextBox)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("tbx_bedrooms_total_rent_m");
            Label lbl_bedrooms_total_rent_y = (Label)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_y");
            HiddenField h_number_of_unit = (HiddenField)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("h_number_of_unit");

            lbl_bedrooms_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text)) * 12));

            month_total = month_total + Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text));

            number_of_unit = number_of_unit + Convert.ToDecimal(RegEx.getMoney(h_number_of_unit.Value));
        }

        for (int i = 0; i < rMfsNumberComUnit.Items.Count; i++)
        {
            TextBox commercial_total_rent_m = (TextBox)rMfsNumberComUnit.Items[i].FindControl("tbx_commercial_total_rent_m");
            Label lbl_commercial_total_rent_y = (Label)rMfsNumberComUnit.Items[i].FindControl("lbl_commercial_total_rent_y");

            HiddenField h_number_of_unit = (HiddenField)rMfsNumberComUnit.Items[i].FindControl("h_number_of_unit");

            lbl_commercial_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(commercial_total_rent_m.Text)) * 12));

            month_total = month_total + Convert.ToDecimal(RegEx.getMoney(commercial_total_rent_m.Text));

            number_of_unit = number_of_unit + Convert.ToDecimal(RegEx.getMoney(h_number_of_unit.Value));

        }


        h_total_number_of_unit.Value = number_of_unit.ToString();


        month_total = month_total + Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text));

        year_total = month_total * 12;

        lbl_pgi_m.Text = String.Format("{0:0.00}", month_total);
        lbl_pgi_y.Text = String.Format("{0:0.00}", year_total);

        // get the income for parking, laundry and storage
        lbl_parking_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text)) * 12));
        lbl_storage_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text)) * 12));
        lbl_laundry_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text)) * 12));

        lbl_garage_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text)) * 12));
        lbl_vending_machine_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text)) * 12));
        lbl_cash_machine_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text)) * 12));
        lbl_other_y2.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text)) * 12));


        // get the vacancy_rate and bad_debt_allowance_rate
        vacancy_rate = Convert.ToDecimal(RegEx.getMoney(tbx_vacancy_rate.Text)) / 100;
        bad_debt_allowance_rate = Convert.ToDecimal(RegEx.getMoney(tbx_bda_rate.Text)) / 100;


        // Income Loss , for vacancy
        lbl_vacancy_m.Text = String.Format("{0:0.00}", (vacancy_rate * month_total));
        lbl_vacancy_y.Text = String.Format("{0:0.00}", (vacancy_rate * year_total));


        // Income Loss , for bad debt allowance
        lbl_bda_m.Text = String.Format("{0:0.00}", (bad_debt_allowance_rate * month_total));
        lbl_bda_y.Text = String.Format("{0:0.00}", (bad_debt_allowance_rate * year_total));


        // total effective gross income for month and year
        lbl_egi_m.Text = String.Format("{0:0.00}", (month_total - Convert.ToDecimal(RegEx.getMoney(lbl_vacancy_m.Text)) - Convert.ToDecimal(RegEx.getMoney(lbl_bda_m.Text))));
        lbl_egi_y.Text = String.Format("{0:0.00}", (year_total - Convert.ToDecimal(RegEx.getMoney(lbl_vacancy_y.Text)) - Convert.ToDecimal(RegEx.getMoney(lbl_bda_y.Text))));


        /// get the rate of utilities , management ,advertisement , taxes etc...
        /// 
        electricity_rate = Convert.ToDecimal(RegEx.getMoney(tbx_electricity_m.Text)) / 100;
        energy_rate = Convert.ToDecimal(RegEx.getMoney(tbx_energy_m.Text)) / 100;
        insurance_rate = Convert.ToDecimal(RegEx.getMoney(tbx_insurances_m.Text)) / 100;

        janitor_rate = Convert.ToDecimal(RegEx.getMoney(tbx_janitor_m.Text)) / 100;
        property_taxe_rate = Convert.ToDecimal(RegEx.getMoney(tbx_taxes_m.Text)) / 100;
        maintenance_repair_rate = Convert.ToDecimal(RegEx.getMoney(tbx_maintenance_repair_m.Text)) / 100;

        school_taxe_rate = Convert.ToDecimal(RegEx.getMoney(tbx_school_taxes_m.Text)) / 100;
        management_rate = Convert.ToDecimal(RegEx.getMoney(tbx_management_m.Text)) / 100;
        advertising_rate = Convert.ToDecimal(RegEx.getMoney(tbx_advertising_m.Text)) / 100;
        legal_rate = Convert.ToDecimal(RegEx.getMoney(tbx_legal_m.Text)) / 100;
        accounting_rate = Convert.ToDecimal(RegEx.getMoney(tbx_accounting_m.Text)) / 100;
        other_rate = Convert.ToDecimal(RegEx.getMoney(tbx_other_m.Text)) / 100;

        // eletricity expenses
        lbl_electricity_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * electricity_rate));
        lbl_electricity_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * electricity_rate));

        // energy expenses
        lbl_energy_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * energy_rate));
        lbl_energy_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * energy_rate));

        // insurance expenses
        lbl_insurances_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * insurance_rate));
        lbl_insurances_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * insurance_rate));

        // janitor expenses
        lbl_janitor_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * janitor_rate));
        lbl_janitor_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * janitor_rate));

        // property taxes expenses
        lbl_taxes_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * property_taxe_rate));
        lbl_taxes_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * property_taxe_rate));

        // maintenance & repair expenses
        lbl_maintenance_repair_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * maintenance_repair_rate));
        lbl_maintenance_repair_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * maintenance_repair_rate));

        // school taxes expenses
        lbl_school_taxes_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * school_taxe_rate));
        lbl_school_taxes_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * school_taxe_rate));

        // management expenses
        lbl_management_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * management_rate));
        lbl_management_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * management_rate));

        // advertising expenses
        lbl_advertising_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * advertising_rate));
        lbl_advertising_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * advertising_rate));

        // legal expenses
        lbl_legal_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * legal_rate));
        lbl_legal_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * legal_rate));


        // accounting expenses
        lbl_accounting_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * accounting_rate));
        lbl_accounting_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * accounting_rate));

        // Other expenses
        lbl_other_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_m.Text)) * other_rate));
        lbl_other_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) * other_rate));




        // Montly Gross total expenses


        total_expenses_m = Convert.ToDecimal(RegEx.getMoney(lbl_electricity_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_energy_m.Text)) +
                                   Convert.ToDecimal(RegEx.getMoney(lbl_insurances_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_janitor_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_taxes_m.Text)) +
                                   Convert.ToDecimal(RegEx.getMoney(lbl_maintenance_repair_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_school_taxes_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_management_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_advertising_m.Text)) +
                                  Convert.ToDecimal(RegEx.getMoney(lbl_legal_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_accounting_m.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_other_m.Text));


        total_expenses_y = Convert.ToDecimal(RegEx.getMoney(lbl_electricity_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_energy_y.Text)) +
                                    Convert.ToDecimal(RegEx.getMoney(lbl_insurances_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_janitor_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_taxes_y.Text)) +
                                    Convert.ToDecimal(RegEx.getMoney(lbl_maintenance_repair_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_school_taxes_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_management_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_advertising_y.Text)) +
                                   Convert.ToDecimal(RegEx.getMoney(lbl_legal_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_accounting_y.Text)) + Convert.ToDecimal(RegEx.getMoney(lbl_other_y.Text));

        lbl_total_expenses_m.Text = String.Format("{0:0.00}", total_expenses_m);
        lbl_total_expenses_y.Text = String.Format("{0:0.00}", total_expenses_y);


        lbl_total_exp_percent.Text = String.Format("{0:0.00}", ((electricity_rate + energy_rate + insurance_rate + janitor_rate + property_taxe_rate + maintenance_repair_rate + school_taxe_rate +
                                      management_rate + advertising_rate + legal_rate + accounting_rate + other_rate) * 100));


        // Net gross exploiation income
        lbl_net_operating_inc_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(lbl_egi_y.Text)) - Convert.ToDecimal(RegEx.getMoney(lbl_total_expenses_y.Text))));

        // Generated liquidity ( before income taxes )
        lbl_liquidity_y.Text = "";
        // Generated liquidity + capitalisation
        lbl_liquidity_capitalisation_y.Text = "";
        //Liquidity + Cap. +Added value
        lbl_liq_cap_value_y.Text = "";

        lbl_appartment_cost.Text = "";
        lbl_mbre.Text =  "";
        lbl_rde.Text =  "";
        lbl_tmo.Text = "";
        lbl_trn.Text = "";
        lbl_mrn.Text = "";
        lbl_rcd.Text = "";

    }
    protected void btn_calculate_egi_Click(object sender, EventArgs e)
    {
        decimal month_total = 0;
        decimal year_total = 0;
        decimal vacancy_rate = 0;
        decimal bad_debt_allowance_rate = 0;


        decimal number_of_unit = 0;


        for (int i = 0; i < rMfsNumberUnitbyBedroomNumber.Items.Count; i++)
        {
            TextBox bedrooms_total_rent_m = (TextBox)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("tbx_bedrooms_total_rent_m");
            Label lbl_bedrooms_total_rent_y = (Label)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_y");
            HiddenField h_number_of_unit = (HiddenField)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("h_number_of_unit");

            lbl_bedrooms_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text)) * 12));

            month_total = month_total + Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text));

            number_of_unit = number_of_unit + Convert.ToDecimal(RegEx.getMoney(h_number_of_unit.Value));
        }

        for (int i = 0; i < rMfsNumberComUnit.Items.Count; i++)
        {
            TextBox commercial_total_rent_m = (TextBox)rMfsNumberComUnit.Items[i].FindControl("tbx_commercial_total_rent_m");
            Label lbl_commercial_total_rent_y = (Label)rMfsNumberComUnit.Items[i].FindControl("lbl_commercial_total_rent_y");

            HiddenField h_number_of_unit = (HiddenField)rMfsNumberComUnit.Items[i].FindControl("h_number_of_unit");

            lbl_commercial_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(commercial_total_rent_m.Text)) * 12));

            month_total = month_total + Convert.ToDecimal(RegEx.getMoney(commercial_total_rent_m.Text));

            number_of_unit = number_of_unit + Convert.ToDecimal(RegEx.getMoney(h_number_of_unit.Value));

        }


        h_total_number_of_unit.Value = number_of_unit.ToString();


        month_total = month_total + Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text));

        year_total = month_total * 12;

        lbl_pgi_m.Text = String.Format("{0:0.00}", month_total);
        lbl_pgi_y.Text = String.Format("{0:0.00}", year_total);

        // get the income for parking, laundry and storage
        lbl_parking_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text)) * 12));
        lbl_storage_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text)) * 12));
        lbl_laundry_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text)) * 12));

        lbl_garage_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text)) * 12));
        lbl_vending_machine_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text)) * 12));
        lbl_cash_machine_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text)) * 12));
        lbl_other_y2.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text)) * 12));

        // get the vacancy_rate and bad_debt_allowance_rate
        vacancy_rate = Convert.ToDecimal(RegEx.getMoney(tbx_vacancy_rate.Text)) / 100;
        bad_debt_allowance_rate = Convert.ToDecimal(RegEx.getMoney(tbx_bda_rate.Text)) / 100;

        // Income Loss , for vacancy
        lbl_vacancy_m.Text = String.Format("{0:0.00}",(vacancy_rate * month_total));
        lbl_vacancy_y.Text = String.Format("{0:0.00}",(vacancy_rate * year_total));


        // Income Loss , for bad debt allowance
        lbl_bda_m.Text = String.Format("{0:0.00}",(bad_debt_allowance_rate * month_total));
        lbl_bda_y.Text = String.Format("{0:0.00}",(bad_debt_allowance_rate * year_total));


        // total effective gross income for month and year
        lbl_egi_m.Text = String.Format("{0:0.00}",(month_total - Convert.ToDecimal(RegEx.getMoney(lbl_vacancy_m.Text)) - Convert.ToDecimal(RegEx.getMoney(lbl_bda_m.Text))));
        lbl_egi_y.Text = String.Format("{0:0.00}", (year_total - Convert.ToDecimal(RegEx.getMoney(lbl_vacancy_y.Text)) - Convert.ToDecimal(RegEx.getMoney(lbl_bda_y.Text))));




        // Net gross exploiation income
        lbl_net_operating_inc_y.Text = "";
        // Generated liquidity ( before income taxes )
        lbl_liquidity_y.Text = "";
        // Generated liquidity + capitalisation
        lbl_liquidity_capitalisation_y.Text = "";
        //Liquidity + Cap. +Added value
        lbl_liq_cap_value_y.Text = "";

        lbl_appartment_cost.Text = "";
        lbl_mbre.Text = "";
        lbl_rde.Text = "";
        lbl_tmo.Text = "";
        lbl_trn.Text = "";
        lbl_mrn.Text = "";

        lbl_rcd.Text = "";
    }
    protected void btn_calculate_pgi_Click(object sender, EventArgs e)
    {

        decimal month_total = 0;
        decimal year_total = 0;

        decimal number_of_unit = 0;


        for (int i = 0; i < rMfsNumberUnitbyBedroomNumber.Items.Count; i++)
        {
            TextBox bedrooms_total_rent_m = (TextBox)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("tbx_bedrooms_total_rent_m");
            Label lbl_bedrooms_total_rent_y = (Label)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_y");
            HiddenField h_number_of_unit = (HiddenField)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("h_number_of_unit");

            lbl_bedrooms_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text)) * 12));

            month_total = month_total + Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text));

            number_of_unit = number_of_unit + Convert.ToDecimal(RegEx.getMoney(h_number_of_unit.Value));
        }

        for (int i = 0; i < rMfsNumberComUnit.Items.Count; i++)
        {
            TextBox commercial_total_rent_m = (TextBox)rMfsNumberComUnit.Items[i].FindControl("tbx_commercial_total_rent_m");
            Label lbl_commercial_total_rent_y = (Label)rMfsNumberComUnit.Items[i].FindControl("lbl_commercial_total_rent_y");

            HiddenField h_number_of_unit = (HiddenField)rMfsNumberComUnit.Items[i].FindControl("h_number_of_unit");

            lbl_commercial_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(commercial_total_rent_m.Text)) * 12));

            month_total = month_total + Convert.ToDecimal(RegEx.getMoney(commercial_total_rent_m.Text));

            number_of_unit = number_of_unit + Convert.ToDecimal(RegEx.getMoney(h_number_of_unit.Value));

        }



        h_total_number_of_unit.Value = number_of_unit.ToString();

        month_total = month_total + Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text));

        year_total = month_total * 12;

        lbl_pgi_m.Text = String.Format("{0:0.00}", month_total);
        lbl_pgi_y.Text = String.Format("{0:0.00}", year_total);

        // get the income for parking, laundry and storage
        lbl_parking_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text)) * 12));
        lbl_storage_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text)) * 12));
        lbl_laundry_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text)) * 12));

        lbl_garage_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text)) * 12));
        lbl_vending_machine_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text)) * 12));
        lbl_cash_machine_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text)) * 12));
        lbl_other_y2.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text)) * 12));




        // Net gross exploiation income
        lbl_net_operating_inc_y.Text = "";
        // Generated liquidity ( before income taxes )
        lbl_liquidity_y.Text = "";
        // Generated liquidity + capitalisation
        lbl_liquidity_capitalisation_y.Text = "";
        //Liquidity + Cap. +Added value
        lbl_liq_cap_value_y.Text = "";


        lbl_appartment_cost.Text = "";
        lbl_mbre.Text = "";
        lbl_rde.Text = "";
        lbl_tmo.Text = "";
        lbl_trn.Text = "";
        lbl_mrn.Text = "";

        lbl_rcd.Text = "";
    }
    protected void btn_update_Click(object sender, EventArgs e)
    {

        Page.Validate("vg_name");
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        String bedrooms_total_rent = "";
        String number_of_unit = "";
        String unit_bedroom_no = "";

        decimal commercial_total_rent = 0;
        int number_of_com_unit = 0;
        int number_of_insert = 0;


        for (int i = 0; i < rMfsNumberUnitbyBedroomNumber.Items.Count; i++)
        {
            TextBox bedrooms_total_rent_m = (TextBox)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("tbx_bedrooms_total_rent_m");
            HiddenField h_number_of_unit = (HiddenField)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("h_number_of_unit");
            HiddenField h_unit_bedroom_no = (HiddenField)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("h_unit_bedroom_number");


            bedrooms_total_rent = bedrooms_total_rent + RegEx.getMoney(bedrooms_total_rent_m.Text) + "|";
            number_of_unit = number_of_unit + h_number_of_unit.Value + "|";
            unit_bedroom_no = unit_bedroom_no + h_unit_bedroom_no.Value + "|";

            number_of_insert++;
        }


        for (int i = 0; i < rMfsNumberComUnit.Items.Count; i++)
        {
            TextBox commercial_total_rent_m = (TextBox)rMfsNumberComUnit.Items[i].FindControl("tbx_commercial_total_rent_m");
            HiddenField h_number_of_com_unit = (HiddenField)rMfsNumberComUnit.Items[i].FindControl("h_number_of_unit");

            commercial_total_rent = Convert.ToDecimal(RegEx.getMoney(commercial_total_rent_m.Text));
            number_of_com_unit = Convert.ToInt32(h_number_of_com_unit.Value);
        }

        //****************************************************************************************************************************
        // Calculation of Liquidity + Cap. +Added value

        //***************************************************************************************************************************
        decimal month_total = 0;
        decimal year_total = 0;
        decimal vacancy_rate = 0;
        decimal bad_debt_allowance_rate = 0;

        decimal electricity_rate = 0;
        decimal energy_rate = 0;
        decimal insurance_rate = 0;
        decimal janitor_rate = 0;
        decimal property_taxe_rate = 0;
        decimal maintenance_repair_rate = 0;
        decimal school_taxe_rate = 0;
        decimal management_rate = 0;
        decimal advertising_rate = 0;
        decimal legal_rate = 0;
        decimal accounting_rate = 0;
        decimal other_rate = 0;


        decimal total_expenses_y = 0;



        for (int i = 0; i < rMfsNumberUnitbyBedroomNumber.Items.Count; i++)
        {
            TextBox bedrooms_total_rent_m = (TextBox)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("tbx_bedrooms_total_rent_m");
            Label lbl_bedrooms_total_rent_y = (Label)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_y");

            lbl_bedrooms_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text)) * 12));

            month_total = month_total + Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text));
        }



        month_total = month_total + Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text))
                  + Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text))
                  + Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text));

        year_total = month_total * 12;


        // get the vacancy_rate and bad_debt_allowance_rate
        vacancy_rate = Convert.ToDecimal(RegEx.getMoney(tbx_vacancy_rate.Text)) / 100;
        bad_debt_allowance_rate = Convert.ToDecimal(RegEx.getMoney(tbx_bda_rate.Text)) / 100;




        // total effective gross income for month and year
        year_total = year_total - (bad_debt_allowance_rate * year_total) - (vacancy_rate * year_total);

        /// get the rate of utilities , management ,advertisement , taxes etc...
        /// 
        electricity_rate = Convert.ToDecimal(RegEx.getMoney(tbx_electricity_m.Text)) / 100;
        energy_rate = Convert.ToDecimal(RegEx.getMoney(tbx_energy_m.Text)) / 100;
        insurance_rate = Convert.ToDecimal(RegEx.getMoney(tbx_insurances_m.Text)) / 100;

        janitor_rate = Convert.ToDecimal(RegEx.getMoney(tbx_janitor_m.Text)) / 100;
        property_taxe_rate = Convert.ToDecimal(RegEx.getMoney(tbx_taxes_m.Text)) / 100;
        maintenance_repair_rate = Convert.ToDecimal(RegEx.getMoney(tbx_maintenance_repair_m.Text)) / 100;

        school_taxe_rate = Convert.ToDecimal(RegEx.getMoney(tbx_school_taxes_m.Text)) / 100;
        management_rate = Convert.ToDecimal(RegEx.getMoney(tbx_management_m.Text)) / 100;
        advertising_rate = Convert.ToDecimal(RegEx.getMoney(tbx_advertising_m.Text)) / 100;
        legal_rate = Convert.ToDecimal(RegEx.getMoney(tbx_legal_m.Text)) / 100;
        accounting_rate = Convert.ToDecimal(RegEx.getMoney(tbx_accounting_m.Text)) / 100;
        other_rate = Convert.ToDecimal(RegEx.getMoney(tbx_other_m.Text)) / 100;



        // Year Gross total expenses

        total_expenses_y = (year_total * electricity_rate) + (year_total * energy_rate) +
                           (year_total * insurance_rate) + (year_total * janitor_rate) + (year_total * property_taxe_rate) +
                           (year_total * maintenance_repair_rate) + (year_total * school_taxe_rate) + (year_total * management_rate) +
                           (year_total * advertising_rate) + (year_total * legal_rate) + (year_total * accounting_rate) + (year_total * other_rate);



        //Net operating Income
        year_total = year_total - total_expenses_y;

        // Generated liquidity ( before income taxes )
        year_total = year_total - Convert.ToDecimal(RegEx.getMoney(tbx_ads_y.Text));

        // Generated liquidity + capitalisation
        year_total = year_total + Convert.ToDecimal(RegEx.getMoney(tbx_capitalisation_y.Text));

        //Liquidity + Cap. +Added value
        year_total = year_total + Convert.ToDecimal(RegEx.getMoney(tbx_added_value_y.Text));

        //****************************************************************************************************************************
        //***************************************************************************************************************************



        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prMoneyFlowScenarioUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);

            // we replace the commas  by dots because SQL "CAST" will convert $100,00 by 100000.00
            // we cast the string "100,00" in money

            cmd.Parameters.Add("@bedrooms_total_rent", SqlDbType.VarChar, 80000).Value = bedrooms_total_rent.Replace(",", ".");
            cmd.Parameters.Add("@number_of_unit", SqlDbType.VarChar, 80000).Value = number_of_unit.Replace(",", ".");
            cmd.Parameters.Add("@unit_bedroom_no", SqlDbType.VarChar, 80000).Value = unit_bedroom_no.Replace(",", ".");
            cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;

            cmd.Parameters.Add("@commercial_total_rent", SqlDbType.Money).Value = commercial_total_rent;
            cmd.Parameters.Add("@number_of_com_unit", SqlDbType.Int).Value = number_of_com_unit;
                       
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(h_home_id.Value);
            cmd.Parameters.Add("@home_value", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_home_value.Text));
            cmd.Parameters.Add("@year", SqlDbType.Int).Value = Convert.ToInt32(ddl_year.SelectedValue);
            cmd.Parameters.Add("@name", SqlDbType.NVarChar, 500).Value = RegEx.getText(tbx_name.Text);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@mfs_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["mfs_id"]);

            cmd.Parameters.Add("@parking", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text));
            cmd.Parameters.Add("@laundry", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text));
            cmd.Parameters.Add("@storage", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text));

            cmd.Parameters.Add("@garage", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text));
            cmd.Parameters.Add("@vending_machine", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text));
            cmd.Parameters.Add("@cash_machine", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text));
            cmd.Parameters.Add("@other", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text));

            cmd.Parameters.Add("@liqcapvalue", SqlDbType.Money).Value = year_total;

            cmd.Parameters.Add("@ads", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_ads_y.Text));
            cmd.Parameters.Add("@capitalisation", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_capitalisation_y.Text));
            cmd.Parameters.Add("@added_value", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_added_value_y.Text));


            cmd.Parameters.Add("@vacancy_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_vacancy_rate.Text));
            cmd.Parameters.Add("@bda_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_bda_rate.Text));
            cmd.Parameters.Add("@electricity_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_electricity_m.Text));
            cmd.Parameters.Add("@energy_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_energy_m.Text));
            cmd.Parameters.Add("@insurance_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_insurances_m.Text));
            cmd.Parameters.Add("@janitor_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_janitor_m.Text));
            cmd.Parameters.Add("@property_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_taxes_m.Text));
            cmd.Parameters.Add("@maintenance_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_maintenance_repair_m.Text));
            cmd.Parameters.Add("@school_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_school_taxes_m.Text));
            cmd.Parameters.Add("@management_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_management_m.Text));
            cmd.Parameters.Add("@advertise_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_advertising_m.Text));
            cmd.Parameters.Add("@legal_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_legal_m.Text));
            cmd.Parameters.Add("@accounting_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_accounting_m.Text));
            cmd.Parameters.Add("@other_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_other_m.Text));


            //execute the insert
            cmd.ExecuteReader();


        }
        finally
        {
            conn.Close();

            
            if ( Convert.ToInt32(cmd.Parameters["@return"].Value) == 0)
            {
                lbl_success.Text = Resources.Resource.lbl_successfull_modification;
            }

          //  Server.Transfer("financial_scenario.aspx");

        }


    }


    protected void Button1_Click(object sender, EventArgs e)
    {

        decimal month_total = 0;
        decimal number_of_unit = 0;
     
       
        //GO TROUGH THE REPEATER

        for (int i = 0; i < rMfsNumberUnitbyBedroomNumber.Items.Count; i++)
        {
            TextBox bedrooms_total_rent_m = (TextBox)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("tbx_bedrooms_total_rent_m");
            Label lbl_bedrooms_total_rent_y = (Label)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_y");
            HiddenField h_number_of_unit = (HiddenField)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("h_number_of_unit");

            lbl_bedrooms_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text)) * 12));

            month_total = month_total + Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text));

            number_of_unit = number_of_unit + Convert.ToDecimal(RegEx.getMoney(h_number_of_unit.Value));
        }

        for (int i = 0; i < rMfsNumberComUnit.Items.Count; i++)
        {
            TextBox commercial_total_rent_m = (TextBox)rMfsNumberComUnit.Items[i].FindControl("tbx_commercial_total_rent_m");
            Label lbl_commercial_total_rent_y = (Label)rMfsNumberComUnit.Items[i].FindControl("lbl_commercial_total_rent_y");

            HiddenField h_number_of_unit = (HiddenField)rMfsNumberComUnit.Items[i].FindControl("h_number_of_unit");

            lbl_commercial_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(commercial_total_rent_m.Text)) * 12));

            month_total = month_total + Convert.ToDecimal(RegEx.getMoney(commercial_total_rent_m.Text));

            number_of_unit = number_of_unit + Convert.ToDecimal(RegEx.getMoney(h_number_of_unit.Value));

        }


        h_total_number_of_unit.Value = number_of_unit.ToString();
      
        /////////////////////////////////////////////////////////////////////////////////////////////////////////


        // Analysis of Return calculations

        double property_value = 0;
        double added_value = 0;
        double total_number_of_unit = Convert.ToDouble(h_total_number_of_unit.Value);
        double appartment_cost = 0;
        property_value = Convert.ToDouble(RegEx.getMoney(tbx_home_value.Text));


        property_value = property_value + Convert.ToDouble(tbx_added_value_y.Text);

        appartment_cost = (property_value / total_number_of_unit);

        lbl_appartment_cost.Text = String.Format("{0:0.00}", appartment_cost);
        lbl_mbre.Text = String.Format("{0:0.00}",((property_value / Convert.ToDouble(lbl_egi_y.Text))));
        
        
        lbl_rde.Text = String.Format("{0:0.00}",((Convert.ToDouble(lbl_total_expenses_y.Text) / Convert.ToDouble(lbl_egi_y.Text))));

        lbl_tmo.Text = String.Format("{0:0.00}",((Convert.ToDouble(lbl_total_expenses_y.Text) + Convert.ToDouble(tbx_ads_y.Text)) / Convert.ToDouble(lbl_pgi_y.Text)));

        lbl_trn.Text = String.Format("{0:0.00}",(Convert.ToDouble(lbl_net_operating_inc_y.Text) / property_value));

        lbl_mrn.Text = String.Format("{0:0.00}",(property_value / Convert.ToDouble(lbl_net_operating_inc_y.Text)));

        lbl_rcd.Text = String.Format("{0:0.00}",((Convert.ToDouble(lbl_net_operating_inc_y.Text) / Convert.ToDouble(tbx_ads_y.Text))));

    }
}
