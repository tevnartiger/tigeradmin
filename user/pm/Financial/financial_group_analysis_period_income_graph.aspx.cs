﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Drawing;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.DataVisualization;
using sinfoca.tiger.security.NameObjectAuthorization;
/// <summary>
/// Done by :Stanley Jocelyn
/// Date    :may 26 , 2009
/// </summary>
public partial class manager_Financial_financial_group_analysis_period_income_graph : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {


        // dotnetCHARTING.DataEngine de = new dotnetCHARTING.DataEngine();

        if (!(Page.IsPostBack))
        {
            ///////// SECUTITY ACCOUNT OBLECT AUTHORIZATION BEGIN declaration  /////////////////
            NameObjectAuthorization groupAuthorization = new NameObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ///////// SECUTITY ACCOUNT OBLECT AUTHORIZATION END   /////////////////


            DateTime to = new DateTime();
            DateTime from = new DateTime();
            to = DateTime.Now; // the date in the to drop downlist

            tiger.Date d = new tiger.Date();
            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            to = Convert.ToDateTime(d.DateCulture(to.Month.ToString(), to.Day.ToString(), to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
            from = Convert.ToDateTime(d.DateCulture("1", "1", to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


            tiger.Date c = new tiger.Date();
            //if we are entering from the update page
            //-----------------------------------------------------
            if (Request.QueryString["gr_id"] != "" && Request.QueryString["gr_id"] != null && Request.QueryString["m"] != "" && Request.QueryString["m"] != null && Request.QueryString["y"] != "" && Request.QueryString["y"] != null)
            {

                if (RegEx.IsInteger(Request.QueryString["gr_id"]) == false ||
                   RegEx.IsInteger(Request.QueryString["m"]) == false ||
                   RegEx.IsInteger(Request.QueryString["y"]) == false ||
                   RegEx.IsInteger(Request.QueryString["d"]) == false)
                {
                    Session.Abandon();
                    Response.Redirect("~/login.aspx");
                }

                from = Convert.ToDateTime(c.DateCulture(Request.QueryString["m"], Request.QueryString["d"], Request.QueryString["y"], Convert.ToString(Session["_lastCulture"])));

            }
            //-----------------------------------------------------


            ddl_from_m.SelectedValue = from.Month.ToString();
            ddl_from_d.SelectedValue = from.Day.ToString();
            ddl_from_y.SelectedValue = from.Year.ToString();

            ddl_to_m.SelectedValue = to.Month.ToString();
            ddl_to_d.SelectedValue = to.Day.ToString();
            ddl_to_y.SelectedValue = to.Year.ToString();


            tiger.PM h = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int group_count = h.getPMGroupHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

            if (group_count > 0)
            {
                int group_id = h.PMgroupFindFirst(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

                //if we are entering from the update page
                //-----------------------------------------------------
                if (Request.QueryString["gr_id"] != "" && Request.QueryString["gr_id"] != null)
                {
                    group_id = Convert.ToInt32(Request.QueryString["gr_id"]);
                }
                //-----------------------------------------------------



                ///////// SECURITY OBJECT CHECK  BEGIN  ////////////////////////////////
                if (!groupAuthorization.Group(Convert.ToInt32(Session["schema_id"]), group_id, Convert.ToInt32(Session["name_id"])))
                {
                    Session.Abandon();
                    Response.Redirect("~/login.aspx");
                }
                ///////// SECURITY OBJECT CHECK  END ////////////////////////////////


                ddl_group_id.DataSource = h.getPMGroupHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                ddl_group_id.SelectedValue = Convert.ToString(group_id);
                ddl_group_id.DataBind();



                //To view the address of the property

                tiger.Group hv = new tiger.Group(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rhome_view.DataSource = hv.getGroupHomeNameView(Convert.ToInt32(Session["schema_id"]), group_id);
                rhome_view.DataBind();


                //set global properties

                /////////////////////   CHARTTING BEGIN FOR CHART 3    ///////////////////////////////////////////////////////////


                Chart3.Width = 900;
                Chart3.Height = 900;
                Chart3.Palette = ChartColorPalette.BrightPastel;
                //   Title t = new Title(Resources.Resource.lbl_u_income, Docking.Top, new System.Drawing.Font("Trebuchet MS", 14, System.Drawing.FontStyle.Bold), System.Drawing.Color.FromArgb(26, 59, 105));
                //  Chart3.Titles.Add(t);

                Chart3.ChartAreas.Add("Series 1");

                // create a couple of series
                Chart3.Series.Add("Series 1");

                // Create a database connection object using the connection string    

                SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                // Create a database command on the connection using query    
                SqlCommand cmd = new SqlCommand("prGroupIncomePeriodViewGroupByCategChart", conn);
                cmd.CommandType = CommandType.StoredProcedure;


                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
                cmd.Parameters.Add("@lastCulture", SqlDbType.VarChar, 20).Value = Convert.ToString(Session["_lastCulture"]);
                cmd.Parameters.Add("@from", SqlDbType.DateTime).Value = from;
                cmd.Parameters.Add("@to", SqlDbType.DateTime).Value = to;

                // Open the connection    
                conn.Open();

                // Create a database reader    
                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);


                // Since the reader implements IEnumerable, pass the reader directly into
                //   the DataBind method with the name of the Column selected in the query    
                Chart3.Series["Series 1"].Points.DataBindXY(dr, "incomecateg_name", dr, "income_amount");

                // Close the reader and the connection
                dr.Close();
                conn.Close();


                // Add the second legend
                Chart3.Legends.Add(new Legend("first"));
                //  Chart3.Series["Series 1"].LegendText = "#AXISLABEL - #VALY{N2}";
                Chart3.Series["Series 1"].Label = "#AXISLABEL \n#PERCENT";

                Chart3.Series["Series 1"].Legend = "first";

                // Add Color column
                LegendCellColumn firstColumn = new LegendCellColumn();
                firstColumn.ColumnType = LegendCellColumnType.SeriesSymbol;
                firstColumn.HeaderText = "";
                firstColumn.HeaderBackColor = Color.WhiteSmoke;
                Chart3.Legends["first"].CellColumns.Add(firstColumn);

                // Add Legend Text column
                LegendCellColumn secondColumn = new LegendCellColumn();
                secondColumn.ColumnType = LegendCellColumnType.Text;
                secondColumn.HeaderText = "Categories";
                secondColumn.Text = "#AXISLABEL";
                secondColumn.HeaderBackColor = Color.WhiteSmoke;
                Chart3.Legends["first"].CellColumns.Add(secondColumn);

                // Add Legend Text column
                LegendCellColumn thirdColumn = new LegendCellColumn();
                thirdColumn.ColumnType = LegendCellColumnType.Text;
                thirdColumn.HeaderText = "Amount";
                thirdColumn.Text = "#VAL{N2}";
                thirdColumn.HeaderBackColor = Color.WhiteSmoke;
                Chart3.Legends["first"].CellColumns.Add(thirdColumn);

                Chart3.Legends["first"].ShadowOffset = 2;
                Chart3.Series["Series 1"].ChartType = SeriesChartType.Pie;

                // Enable 3D
                Chart3.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                Chart3.ChartAreas["ChartArea1"].BorderColor = Color.Black;
                Chart3.ChartAreas["ChartArea1"].BorderWidth = 5;


                for (int i = 1; i < Chart3.Series["Series 1"].Points.Count; i++)
                {
                    Chart3.Series["Series 1"].Points[i]["Exploded"] = "true";
                }

                // Set labels style
                Chart3.Series["Series 1"]["PieLabelStyle"] = "Outside";

                ddl_dimension.SelectedValue = "3";
                //////////////////////////////////////////////////////////////////////////////////////////////




                //   Title t = new Title(Resources.Resource.lbl_u_income, Docking.Top, new System.Drawing.Font("Trebuchet MS", 14, System.Drawing.FontStyle.Bold), System.Drawing.Color.FromArgb(26, 59, 105));
                //  Chart3.Titles.Add(t);
                Chart1.Width = 900;
                Chart1.Height = 900;
                Chart1.Palette = ChartColorPalette.BrightPastel;

                Chart1.ChartAreas.Add("Series 2");

                // create a couple of series
                Chart1.Series.Add("Series 2");

                // Create a database connection object using the connection string    
                SqlConnection conn2 = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

                // Create a database command on the connection using query    
                SqlCommand cmd2 = new SqlCommand("prGroupIncomePeriodViewGroupByCategChart", conn2);
                cmd2.CommandType = CommandType.StoredProcedure;


                cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd2.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
                cmd2.Parameters.Add("@lastCulture", SqlDbType.VarChar, 20).Value = Convert.ToString(Session["_lastCulture"]);
                cmd2.Parameters.Add("@from", SqlDbType.DateTime).Value = from;
                cmd2.Parameters.Add("@to", SqlDbType.DateTime).Value = to;

                // Open the connection    
                conn2.Open();

                // Create a database reader    
                SqlDataReader dr2 = null;

                dr2 = cmd2.ExecuteReader(CommandBehavior.CloseConnection);


                // Since the reader implements IEnumerable, pass the reader directly into
                //   the DataBind method with the name of the Column selected in the query    
                Chart1.Series["Series 2"].Points.DataBindXY(dr2, "incomecateg_name", dr2, "income_amount");

                // Close the reader and the connection
                dr2.Close();
                conn2.Close();


                //  Chart1.Series["Series 2"].LegendText = "#AXISLABEL - #VALY{N2}";
                Chart1.Series["Series 2"].Label = " #VALY{N2} \n#PERCENT";
                Chart1.Series["Series 2"]["BarLabelStyle"] = "Center";


                Chart1.Series["Series 2"].ChartType = SeriesChartType.Bar;


                // Enable 3D
                Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                Chart1.ChartAreas["ChartArea1"].BorderColor = Color.Black;
                Chart1.ChartAreas["ChartArea1"].BorderWidth = 5;

                foreach (DataPoint point in Chart1.Series[0].Points)
                {
                    point.YValues[0] = point.YValues[0];
                    Chart1.Series["Series 2"].Palette = ChartColorPalette.BrightPastel;//assigning colors to datapoint  
                }

                ddl_dimension.SelectedValue = "3";

                ///////////////////// CHARTING 4 BEBIN   ///////////////////////////////////////

            }
            else
            {
                ddl_from_m.Enabled = false;
                ddl_from_y.Enabled = false;
                ddl_dimension.Enabled = false;
                ddl_chartype.Enabled = false;
                btn_view.Enabled = false;
                btn_view_graph.Enabled = false;
                ddl_group_id.Enabled = false;
            }

        }



    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_group_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        DateTime to = new DateTime();
        DateTime from = new DateTime();


        tiger.Date d = new tiger.Date();
        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        //To view the address of the property

        tiger.Group hv = new tiger.Group(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rhome_view.DataSource = hv.getGroupHomeNameView(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_group_id.SelectedValue));
        rhome_view.DataBind();


        if (ddl_chartype.SelectedValue == "2")
        {
            Chart1.Visible = true;
            Chart3.Visible = false;

            Chart1.Width = 900;
            Chart1.Height = 900;
            SqlConnection conn2 = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            // Create a database command on the connection using query    
            SqlCommand cmd2 = new SqlCommand("prGroupIncomePeriodViewGroupByCategChart", conn2);
            cmd2.CommandType = CommandType.StoredProcedure;


            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@group_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_group_id.SelectedValue);
            cmd2.Parameters.Add("@lastCulture", SqlDbType.VarChar, 20).Value = Convert.ToString(Session["_lastCulture"]);
            cmd2.Parameters.Add("@from", SqlDbType.DateTime).Value = from;
            cmd2.Parameters.Add("@to", SqlDbType.DateTime).Value = to;

            // Open the connection    
            conn2.Open();

            // Create a database reader    
            SqlDataReader dr2 = null;

            dr2 = cmd2.ExecuteReader(CommandBehavior.CloseConnection);


            // Since the reader implements IEnumerable, pass the reader directly into
            //   the DataBind method with the name of the Column selected in the query    
            Chart1.Series["Series 2"].Points.DataBindXY(dr2, "incomecateg_name", dr2, "income_amount");

            // Close the reader and the connection
            dr2.Close();
            conn2.Close();

            Chart1.Series["Series 2"].LegendText = "#AXISLABEL - #VALY{N2}";
            Chart1.Series["Series 2"].Label = "#AXISLABEL \n#PERCENT";


            Chart1.Legends["second"].ShadowOffset = 2;
            Chart1.Series["Series 2"].ChartType = SeriesChartType.Bar;


            // Enable 3D

            if (ddl_dimension.SelectedValue == "3")
                Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
            else
                Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;


            Chart1.ChartAreas["ChartArea1"].BorderColor = Color.Black;
            Chart1.ChartAreas["ChartArea1"].BorderWidth = 5;

            foreach (DataPoint point in Chart1.Series[0].Points)
            {
                point.YValues[0] = point.YValues[0];
                Chart1.Series["Series 2"].Palette = ChartColorPalette.BrightPastel;//assigning colors to datapoint  
            }

        }
        //////////////////////////////////////////////////////////////////////////////////
        /////////////////////////   PIE CHART   ///////////////////////////////////////////////

        if (ddl_chartype.SelectedValue == "1")
        {
            Chart1.Visible = false;
            Chart3.Visible = true;

            Chart3.Width = 900;
            Chart3.Height = 900;
            Chart3.Palette = ChartColorPalette.BrightPastel;

            // Create a database connection object using the connection string    

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            // Create a database command on the connection using query    
            SqlCommand cmd = new SqlCommand("prGroupIncomePeriodViewGroupByCategChart", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_group_id.SelectedValue);
            cmd.Parameters.Add("@lastCulture", SqlDbType.VarChar, 20).Value = Convert.ToString(Session["_lastCulture"]);
            cmd.Parameters.Add("@from", SqlDbType.DateTime).Value = from;
            cmd.Parameters.Add("@to", SqlDbType.DateTime).Value = to;

            // Open the connection    
            conn.Open();

            // Create a database reader    
            SqlDataReader dr = null;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);


            // Since the reader implements IEnumerable, pass the reader directly into
            //   the DataBind method with the name of the Column selected in the query    
            Chart3.Series["Series 1"].Points.DataBindXY(dr, "incomecateg_name", dr, "income_amount");

            // Close the reader and the connection
            dr.Close();
            conn.Close();

            Chart3.Series["Series 1"].Label = "#AXISLABEL \n#PERCENT";

            Chart3.Series["Series 1"].Legend = "first";


            Chart3.Legends["first"].ShadowOffset = 2;
            Chart3.Series["Series 1"].ChartType = SeriesChartType.Pie;

            // Enable 3D
            Chart3.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
            Chart3.ChartAreas["ChartArea1"].BorderColor = Color.Black;
            Chart3.ChartAreas["ChartArea1"].BorderWidth = 5;


            for (int i = 1; i < Chart3.Series["Series 1"].Points.Count; i++)
            {
                Chart3.Series["Series 1"].Points[i]["Exploded"] = "true";
            }

            // Set labels style
            Chart3.Series["Series 1"]["PieLabelStyle"] = "Outside";
        }


    }


    protected void btn_view_graph_Click(object sender, EventArgs e)
    {
        Response.Redirect("financial_group_analysis_period_expense_graph.aspx?gr_id=" + ddl_group_id.SelectedValue + "&m=" + ddl_from_m.SelectedValue
                           + "&d=" + ddl_from_d.SelectedValue + "&y=" + ddl_from_y.SelectedValue
                           + "&m2=" + ddl_to_m.SelectedValue
                           + "&d2=" + ddl_to_d.SelectedValue + "&y2=" + ddl_to_y.SelectedValue);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_view_Click(object sender, EventArgs e)
    {


        DateTime to = new DateTime();
        DateTime from = new DateTime();


        tiger.Date d = new tiger.Date();
        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        if (ddl_chartype.SelectedValue == "2")
        {
            Chart1.Visible = true;
            Chart3.Visible = false;

            Chart1.Width = 900;
            Chart1.Height = 900;
            SqlConnection conn2 = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            // Create a database command on the connection using query    
            SqlCommand cmd2 = new SqlCommand("prGroupIncomePeriodViewGroupByCategChart", conn2);
            cmd2.CommandType = CommandType.StoredProcedure;


            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@group_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_group_id.SelectedValue);
            cmd2.Parameters.Add("@lastCulture", SqlDbType.VarChar, 20).Value = Convert.ToString(Session["_lastCulture"]);
            cmd2.Parameters.Add("@from", SqlDbType.DateTime).Value = from;
            cmd2.Parameters.Add("@to", SqlDbType.DateTime).Value = to;

            // Open the connection    
            conn2.Open();

            // Create a database reader    
            SqlDataReader dr2 = null;

            dr2 = cmd2.ExecuteReader(CommandBehavior.CloseConnection);


            // Since the reader implements IEnumerable, pass the reader directly into
            //   the DataBind method with the name of the Column selected in the query    
            Chart1.Series["Series 2"].Points.DataBindXY(dr2, "incomecateg_name", dr2, "income_amount");

            // Close the reader and the connection
            dr2.Close();
            conn2.Close();

            Chart1.Series["Series 2"].LegendText = "#AXISLABEL - #VALY{N2}";
            Chart1.Series["Series 2"].Label = "#AXISLABEL \n#PERCENT";


            Chart1.Series["Series 2"].ChartType = SeriesChartType.Bar;


            // Enable 3D

            if (ddl_dimension.SelectedValue == "3")
                Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
            else
                Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;


            Chart1.ChartAreas["ChartArea1"].BorderColor = Color.Black;
            Chart1.ChartAreas["ChartArea1"].BorderWidth = 5;

            foreach (DataPoint point in Chart1.Series[0].Points)
            {
                point.YValues[0] = point.YValues[0];
                Chart1.Series["Series 2"].Palette = ChartColorPalette.BrightPastel;//assigning colors to datapoint  
            }

        }
        //////////////////////////////////////////////////////////////////////////////////
        /////////////////////////   PIE CHART   ///////////////////////////////////////////////

        if (ddl_chartype.SelectedValue == "1")
        {
            Chart1.Visible = false;
            Chart3.Visible = true;

            Chart3.Width = 900;
            Chart3.Height = 900;
            Chart3.Palette = ChartColorPalette.BrightPastel;

            // Create a database connection object using the connection string    

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            // Create a database command on the connection using query    
            SqlCommand cmd = new SqlCommand("prGroupIncomePeriodViewGroupByCategChart", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_group_id.SelectedValue);
            cmd.Parameters.Add("@lastCulture", SqlDbType.VarChar, 20).Value = Convert.ToString(Session["_lastCulture"]);
            cmd.Parameters.Add("@from", SqlDbType.DateTime).Value = from;
            cmd.Parameters.Add("@to", SqlDbType.DateTime).Value = to;

            // Open the connection    
            conn.Open();

            // Create a database reader    
            SqlDataReader dr = null;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);


            // Since the reader implements IEnumerable, pass the reader directly into
            //   the DataBind method with the name of the Column selected in the query    
            Chart3.Series["Series 1"].Points.DataBindXY(dr, "incomecateg_name", dr, "income_amount");

            // Close the reader and the connection
            dr.Close();
            conn.Close();

            Chart3.Series["Series 1"].Label = "#AXISLABEL \n#PERCENT";

            Chart3.Series["Series 1"].Legend = "first";


            Chart3.Legends["first"].ShadowOffset = 2;
            Chart3.Series["Series 1"].ChartType = SeriesChartType.Pie;

            // Enable 3D
            Chart3.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
            Chart3.ChartAreas["ChartArea1"].BorderColor = Color.Black;
            Chart3.ChartAreas["ChartArea1"].BorderWidth = 5;


            for (int i = 1; i < Chart3.Series["Series 1"].Points.Count; i++)
            {
                Chart3.Series["Series 1"].Points[i]["Exploded"] = "true";
            }

            // Set labels style
            Chart3.Series["Series 1"]["PieLabelStyle"] = "Outside";
        }







    }
}
