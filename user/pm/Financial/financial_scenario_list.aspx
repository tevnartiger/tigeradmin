﻿<%@ Page Language="C#" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" CodeFile="financial_scenario_list.aspx.cs" Inherits="manager_Financial_financial_scenario_list" Title="Money Flow Scenario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_mfs_list %>"></asp:Label><br /><br />

    <asp:GridView  ID="gvFinancialMoneyFlowScenario" runat="server" AutoGenerateColumns="false"
      AllowPaging="false"  AlternatingRowStyle-BackColor="#F0F0F6" 
      BorderColor="#CDCDCD"  BorderWidth="1" GridLines="Both"  Width="100%"
      HeaderStyle-BackColor="#F0F0F6" >
    <Columns>
    
   
    <asp:BoundField   DataField="home_name"  HeaderText="<%$ Resources:Resource, lbl_property %>"/>
     <asp:BoundField  DataField="mfs_name" HeaderText="<%$ Resources:Resource, txt_scenario_name %>" />
    <asp:BoundField  DataField="mfs_year" HeaderText="<%$ Resources:Resource, lbl_year %>" />
    <asp:BoundField  DataField="mfs_liqcapvalue" DataFormatString="{0:0.00}"  HeaderText="Money Flow" />
    
    <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_view %>"
     DataNavigateUrlFields="mfs_id" 
     DataNavigateUrlFormatString="financial_scenario_view.aspx?mfs_id={0}" 
      HeaderText="<%$ Resources:Resource, lbl_view %>" />

   <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_update %>"
     DataNavigateUrlFields="mfs_id" 
     DataNavigateUrlFormatString="financial_scenario_update.aspx?mfs_id={0}" 
      HeaderText="<%$ Resources:Resource, lbl_update %>" />      
      
      
      
    </Columns>
    
    
    </asp:GridView>
    
    
</asp:Content>

