﻿<%@ Page Language="C#" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeFile="financial_expenses.aspx.cs" Inherits="manager_Financial_financial_expenses" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <table style="width: 100%">
        <tr>
            <td valign="top">
<table>
<tr><td> <asp:Label ID="lbl_property" runat="server" Text="<%$ Resources:Resource, lbl_property %>"/>
                </td>
                <td>
       <asp:DropDownList ID="ddl_home_id" DataValueField="home_id" DataTextField="home_name"   runat="server" autopostback="true" OnSelectedIndexChanged="ddl_home_id_SelectedIndexChanged" />
       </td>  
</tr>
<tr><td>
    <br />
    <asp:LinkButton ID="link_expense" runat="server" onclick="link_expense_Click">Add new expenses</asp:LinkButton>
    </td>
    <td>  </td>  
</tr>
 </table>
            </td>
            <td valign="top">
                <table bgcolor="#ffffcc"  >
                    <tr>
                        <td>
                          <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, txt_month %>"></asp:Label>&nbsp;/&nbsp;<asp:Label ID="lbl_year" runat="server" Text="<%$ Resources:Resource, lbl_year %>"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_date_received_m" runat="server" 
                                AutoPostBack="true" onselectedindexchanged="ddl_date_received_m_SelectedIndexChanged" 
                                >
                                <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                            </asp:DropDownList>
                            &nbsp; / &nbsp;
                            <asp:DropDownList ID="ddl_date_received_y" runat="server" onselectedindexchanged="ddl_date_received_y_SelectedIndexChanged" 
                              AutoPostBack="true"   >
                                <asp:ListItem>2001</asp:ListItem>
                                <asp:ListItem>2002</asp:ListItem>
                                <asp:ListItem>2003</asp:ListItem>
                                <asp:ListItem>2004</asp:ListItem>
                                <asp:ListItem>2005</asp:ListItem>
                                <asp:ListItem>2006</asp:ListItem>
                                <asp:ListItem>2007</asp:ListItem>
                                <asp:ListItem>2008</asp:ListItem>
                                <asp:ListItem>2009</asp:ListItem>
                                <asp:ListItem>2010</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    </table>
               
                <br />
                <asp:Button ID="btn_view_graph" runat="server" onclick="btn_view_graph_Click" 
                    Text="View Graph" />
               
            </td>
        </tr>
    </table>
   

                <asp:Label ID="lbl_confirmation" runat="server" ForeColor="Red"></asp:Label>
                <br />
                <asp:Label ID="Label53" runat="server" style="font-weight: 700"></asp:Label>
&nbsp;&nbsp;<asp:Label ID="Label58" runat="server"></asp:Label>
    &nbsp;
                <asp:Label ID="Label55" runat="server" style="font-weight: 700"></asp:Label>
&nbsp;<asp:Label ID="Label56" runat="server"></asp:Label>
&nbsp;<asp:Label ID="lbl_amount" runat="server"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label57" runat="server" style="font-weight: 700"></asp:Label>
&nbsp;<asp:Label ID="Label7" runat="server"></asp:Label>
&nbsp;<asp:Label ID="lbl_date_paid" runat="server"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label59" runat="server" style="font-weight: 700"></asp:Label>
&nbsp;<asp:Label ID="Label60" runat="server"></asp:Label>
&nbsp;<asp:Label ID="lbl_expense_reference" runat="server"></asp:Label>
&nbsp;<table style="width: 100%" >
            
    
    
    
    <tr>
   <td colspan="3">
   <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="83%" BorderColor="White" BorderWidth="3"  ID="gv_Expense" runat="server" AutoGenerateColumns="false"
       AllowSorting="true"   EmptyDataText="" GridLines="Both"   
   BackColor="Beige">
        
     <Columns>
   <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_paid_expenses%>" ItemStyle-VerticalAlign="Top" >
    <ItemTemplate  >
   <asp:Label ID="lbl_expense_categ"  Text='<%#GetExpenseCateg(Convert.ToInt32(Eval("expensecateg_id")))%>' runat="server" />
     
  </ItemTemplate  >
    </asp:TemplateField>
   
   
   <asp:BoundField HeaderText="<%$ Resources:Resource, txt_amount_paid %>" ItemStyle-VerticalAlign="Top"    DataField="expense_amount" DataFormatString="{0:0.00}"  />
   <asp:BoundField HeaderText="<%$ Resources:Resource, txt_date %>"  ItemStyle-VerticalAlign="Top"  DataField="expense_date_paid"  DataFormatString="{0:MMM-dd-yyyy}"  />
   
   <asp:BoundField HeaderText="<%$ Resources:Resource, lbl_reference %>"  ItemStyle-VerticalAlign="Top"  DataField="expense_reference"  DataFormatString="{0:MMM-dd-yyyy}"  />
   
   <asp:TemplateField ItemStyle-Wrap="true" HeaderText="<%$ Resources:Resource, lbl_com %>"  ItemStyle-VerticalAlign="Top" >
      <ItemTemplate>
         <asp:Label ID="lbl_com" Text='<%#Get_Comments(Convert.ToString(Eval("home_id")),Convert.ToString(Eval("wo_id")),Convert.ToString(Eval("expense_comments")),Convert.ToString(Eval("task_id")),Convert.ToString(Eval("wo_title")),
                                                       Convert.ToInt32(Eval("wo_status")),Convert.ToDateTime(Eval("wo_date_begin")),Convert.ToDateTime(Eval("wo_date_end")),
                                                       Convert.ToInt32(Eval("task_status")),Convert.ToDateTime(Eval("task_date_begin")),Convert.ToDateTime(Eval("task_date_end")),
                                                       Convert.ToString(Eval("task_title")))%>'   runat="server"></asp:Label>
     </ItemTemplate>
   </asp:TemplateField>
   
   
      
      <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_update %>" ItemStyle-VerticalAlign="Top" >
               <ItemTemplate>
                 <asp:HyperLink ID="link_update" NavigateUrl='<%#Get_Link(Convert.ToString(Eval("expense_id")),Convert.ToString(Eval("task_id")))%>' Text="<%$ Resources:Resource, lbl_update %>"  runat="server"></asp:HyperLink>
               </ItemTemplate>
       </asp:TemplateField>
      
       <asp:TemplateField >
    <ItemTemplate  >
    
    <asp:HiddenField ID="h_expense_id" Value='<%#Bind("expense_id")%>'  runat="server" />
    <asp:HiddenField ID="h_task_id" Value='<%#Bind("task_id")%>'  runat="server" />
    <asp:HiddenField ID="h_expense_amount" Value='<%#Bind("expense_amount","{0:0.00}")%>'  runat="server" />
    <asp:HiddenField ID="h_expense_date_paid" Value='<%#Bind("expense_date_paid","{0:M-dd-yyyy}")%>'  runat="server" />
    <asp:HiddenField ID="h_expense_reference" Value='<%#Bind("expense_reference")%>'  runat="server" />
    <asp:HiddenField ID="h_expense_categ" Value='<%#GetExpenseCateg(Convert.ToInt32(Eval("expensecateg_id")))%>'  runat="server" />
    
    
   <asp:Button ID="btn_cancel" runat="server" OnClick="btn_cancel_Click"  Text="<%$ Resources:Resource, btn_cancel %>"/>
    </ItemTemplate  >
    </asp:TemplateField>
     
      </Columns>   
        
        
    </asp:GridView>

   </td>
   </tr>     
        
        
        
       <tr>
            <td valign="top" style="height: 18px; width: 136px;">
                &nbsp;</td>
            <td valign="top" style="height: 18px">
            &nbsp;</td>
            <td valign="top" style="height: 18px">
                &nbsp;</td>
        </tr>
        <tr>
            <td valign="top" style="height: 18px; width: 136px;">
                <asp:Label ID="Label52" runat="server" 
                    Text="<%$ Resources:Resource, lbl_total_expenses %>" 
                    style="font-weight: 700"/></td>
            <td valign="top"  >
                <asp:Label ID="lbl_total_month_expense"  runat="server"></asp:Label>
            </td>
            <td valign="top"  >
                &nbsp;</td>
        </tr>
                                
    </table>
    
    <br />
    <br />
    
    <asp:Panel ID="Panel1" runat="server">
        <table style="width: 83%" >
            <tr>
                <td valign="top" bgcolor="White" colspan="4">
                    <asp:Button ID="btn_fill_expense" runat="server" 
                        onclick="btn_fill_expense_Click" Text="Retreive and fill with charges recorded from the previous mont" />
                    <br />
                    <br />
                </td>
                <td valign="top" bgcolor="White" colspan="2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td valign="top" bgcolor="AliceBlue">
                    <asp:Label ID="Label5" runat="server" 
                    Text="<%$ Resources:Resource, lbl_expenses%>" style="font-weight: 700"/>
                </td>
                <td valign="top" style="font-weight: 700" bgcolor="AliceBlue">
                    <asp:Label ID="Label36" runat="server" 
                          Text="<%$ Resources:Resource, txt_amount_paid %>" />
                </td>
                <td valign="top" bgcolor="AliceBlue">
                    <asp:Label ID="Label3" runat="server" 
                     Text="<%$ Resources:Resource, txt_date %>" style="font-weight: 700"/>
                </td>
                <td valign="top" bgcolor="AliceBlue">
                    <asp:Label ID="Label6" runat="server" 
                    Text="<%$ Resources:Resource, lbl_reference%>" style="font-weight: 700"/>
                </td>
                <td valign="top" bgcolor="AliceBlue">
                    <asp:Label ID="Label4" runat="server" 
                    Text="<%$ Resources:Resource, lbl_com%>" style="font-weight: 700"/>
                </td>
                <td valign="top" bgcolor="AliceBlue">
                    &nbsp;</td>
            </tr>
            <tr bgcolor="beige" >
                <td valign="top" style="height: 18px" >
                    <asp:Label ID="Label8" runat="server" 
                    Text="<%$ Resources:Resource, lbl_electricity %>" />
                    &nbsp;</td>
                <td valign="top"  >
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="tbx_electricity_m" runat="server" Width="65px"></asp:TextBox>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger  ControlID="btn_fill_expense" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
                <td valign="top"  >
                    <asp:DropDownList ID="ddl_electricity_date_received_m" runat="server" 
                    AutoPostBack="true" 
                    >
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    / &nbsp;<asp:DropDownList ID="ddl_electricity_date_received_d" runat="server" 
                    >
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;&nbsp; / &nbsp;
                    <asp:DropDownList ID="ddl_electricity_date_received_y" runat="server" 
                    >
                        <asp:ListItem>2001</asp:ListItem>
                        <asp:ListItem>2002</asp:ListItem>
                        <asp:ListItem>2003</asp:ListItem>
                        <asp:ListItem>2004</asp:ListItem>
                        <asp:ListItem>2005</asp:ListItem>
                        <asp:ListItem>2006</asp:ListItem>
                        <asp:ListItem>2007</asp:ListItem>
                        <asp:ListItem>2008</asp:ListItem>
                        <asp:ListItem>2009</asp:ListItem>
                        <asp:ListItem>2010</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td valign="top" >
                    <asp:TextBox ID="tbx_ref_electricity" runat="server" Width="85px"></asp:TextBox>
                </td>
                <td valign="top" >
                    <asp:TextBox ID="tbx_com_electricity" runat="server" Height="65px" TextMode="MultiLine" 
                    Width="131px"></asp:TextBox>
                </td>
                <td valign="top" >
                    <asp:Button ID="btn_electricity" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" 
                    onclick="btn_electricity_Click" />
                </td>
            </tr>
            <tr>
                <td valign="top" style="height: 18px">
                    <asp:Label ID="Label9" runat="server" 
                    Text="<%$ Resources:Resource, lbl_energy %>" />
                </td>
                <td valign="top" >
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="tbx_energy_m" runat="server" Width="65px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td valign="top" >
                    <asp:DropDownList ID="ddl_energy_date_received_m" runat="server" 
                    AutoPostBack="true" 
                    >
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    / &nbsp;
                    <asp:DropDownList ID="ddl_energy_date_received_d" runat="server" 
                    >
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp; / &nbsp;
                    <asp:DropDownList ID="ddl_energy_date_received_y" runat="server" 
                    >
                        <asp:ListItem>2001</asp:ListItem>
                        <asp:ListItem>2002</asp:ListItem>
                        <asp:ListItem>2003</asp:ListItem>
                        <asp:ListItem>2004</asp:ListItem>
                        <asp:ListItem>2005</asp:ListItem>
                        <asp:ListItem>2006</asp:ListItem>
                        <asp:ListItem>2007</asp:ListItem>
                        <asp:ListItem>2008</asp:ListItem>
                        <asp:ListItem>2009</asp:ListItem>
                        <asp:ListItem>2010</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td valign="top" >
                    <asp:TextBox ID="tbx_ref_energy" runat="server" Width="85px"></asp:TextBox>
                </td>
                <td valign="top" >
                    <asp:TextBox ID="tbx_com_energy" runat="server" Height="65px" TextMode="MultiLine" 
                    Width="130px"></asp:TextBox>
                </td>
                <td valign="top" >
                    <asp:Button ID="btn_energy" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" onclick="btn_energy_Click" />
                </td>
            </tr>
            <tr bgcolor="beige">
                <td valign="top" style="height: 18px">
                    <asp:Label ID="Label10" runat="server" 
                    Text="<%$ Resources:Resource, lbl_insurances %>" />
                </td>
                <td valign="top" >
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="tbx_insurances_m" runat="server" Width="65px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td valign="top" >
                    <asp:DropDownList ID="ddl_insurances_date_received_m" runat="server" 
                    AutoPostBack="true" 
                    >
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    / &nbsp;
                    <asp:DropDownList ID="ddl_insurances_date_received_d" runat="server" 
                    >
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp; / &nbsp;
                    <asp:DropDownList ID="ddl_insurances_date_received_y" runat="server" 
                    >
                        <asp:ListItem>2001</asp:ListItem>
                        <asp:ListItem>2002</asp:ListItem>
                        <asp:ListItem>2003</asp:ListItem>
                        <asp:ListItem>2004</asp:ListItem>
                        <asp:ListItem>2005</asp:ListItem>
                        <asp:ListItem>2006</asp:ListItem>
                        <asp:ListItem>2007</asp:ListItem>
                        <asp:ListItem>2008</asp:ListItem>
                        <asp:ListItem>2009</asp:ListItem>
                        <asp:ListItem>2010</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td valign="top" >
                    <asp:TextBox ID="tbx_ref_insurances" runat="server" Width="85px"></asp:TextBox>
                </td>
                <td valign="top" >
                    <asp:TextBox ID="tbx_com_insurances" runat="server" Height="65px" TextMode="MultiLine" 
                    Width="130px"></asp:TextBox>
                </td>
                <td valign="top" >
                    <asp:Button ID="btn_insurances" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" 
                    onclick="btn_insurances_Click" />
                </td>
            </tr>
            <tr>
                <td valign="top" style="height: 18px">
                    <asp:Label ID="Label11" runat="server" 
                    Text="<%$ Resources:Resource, lbl_janitor %>" />
                </td>
                <td valign="top" >
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="tbx_janitor_m" runat="server" Width="65px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td valign="top" >
                    <asp:DropDownList ID="ddl_janitor_date_received_m" runat="server" 
                    AutoPostBack="true" 
                    >
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    / &nbsp;
                    <asp:DropDownList ID="ddl_janitor_date_received_d" runat="server" 
                    >
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp; / &nbsp;
                    <asp:DropDownList ID="ddl_janitor_date_received_y" runat="server" 
                    >
                        <asp:ListItem>2001</asp:ListItem>
                        <asp:ListItem>2002</asp:ListItem>
                        <asp:ListItem>2003</asp:ListItem>
                        <asp:ListItem>2004</asp:ListItem>
                        <asp:ListItem>2005</asp:ListItem>
                        <asp:ListItem>2006</asp:ListItem>
                        <asp:ListItem>2007</asp:ListItem>
                        <asp:ListItem>2008</asp:ListItem>
                        <asp:ListItem>2009</asp:ListItem>
                        <asp:ListItem>2010</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td valign="top" >
                    <asp:TextBox ID="tbx_ref_janitor" runat="server" Width="85px"></asp:TextBox>
                </td>
                <td valign="top" >
                    <asp:TextBox ID="tbx_com_janitor" runat="server" Height="65px" TextMode="MultiLine" 
                    Width="130px"></asp:TextBox>
                </td>
                <td valign="top" >
                    <asp:Button ID="btn_janitor" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" onclick="btn_janitor_Click" />
                </td>
            </tr>
            <tr bgcolor="beige">
                <td valign="top" style="height: 18px">
                    <asp:Label ID="Label12" runat="server" 
                    Text="<%$ Resources:Resource, lbl_taxes %>"/>
                </td>
                <td valign="top" >
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="tbx_taxes_m" runat="server" Width="65px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td valign="top" >
                    <asp:DropDownList ID="ddl_taxes_date_received_m" runat="server" 
                    AutoPostBack="true" 
                    >
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    / &nbsp;
                    <asp:DropDownList ID="ddl_taxes_date_received_d" runat="server" 
                    >
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp; / &nbsp;
                    <asp:DropDownList ID="ddl_taxes_date_received_y" runat="server" 
                    >
                        <asp:ListItem>2001</asp:ListItem>
                        <asp:ListItem>2002</asp:ListItem>
                        <asp:ListItem>2003</asp:ListItem>
                        <asp:ListItem>2004</asp:ListItem>
                        <asp:ListItem>2005</asp:ListItem>
                        <asp:ListItem>2006</asp:ListItem>
                        <asp:ListItem>2007</asp:ListItem>
                        <asp:ListItem>2008</asp:ListItem>
                        <asp:ListItem>2009</asp:ListItem>
                        <asp:ListItem>2010</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td valign="top" >
                    <asp:TextBox ID="tbx_ref_taxes" runat="server" Width="85px"></asp:TextBox>
                </td>
                <td valign="top" >
                    <asp:TextBox ID="tbx_com_taxes" runat="server" Height="65px" TextMode="MultiLine" 
                    Width="130px"></asp:TextBox>
                </td>
                <td valign="top" >
                    <asp:Button ID="btn_taxes" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" onclick="btn_taxes_Click" />
                </td>
            </tr>
            <tr>
                <td valign="top" style="height: 18px">
                    <asp:Label ID="Label13" runat="server" 
                    Text="<%$ Resources:Resource, lbl_maintenance_repair %>" />
                </td>
                <td valign="top" >
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="tbx_maintenance_repair_m" runat="server" 
    Width="65px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td valign="top" >
                    <asp:DropDownList ID="ddl_maintenance_date_received_m" runat="server" 
                    AutoPostBack="true" 
                    >
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    / &nbsp;
                    <asp:DropDownList ID="ddl_maintenance_date_received_d" runat="server" 
                    >
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp; / &nbsp;
                    <asp:DropDownList ID="ddl_maintenance_date_received_y" runat="server" 
                    >
                        <asp:ListItem>2001</asp:ListItem>
                        <asp:ListItem>2002</asp:ListItem>
                        <asp:ListItem>2003</asp:ListItem>
                        <asp:ListItem>2004</asp:ListItem>
                        <asp:ListItem>2005</asp:ListItem>
                        <asp:ListItem>2006</asp:ListItem>
                        <asp:ListItem>2007</asp:ListItem>
                        <asp:ListItem>2008</asp:ListItem>
                        <asp:ListItem>2009</asp:ListItem>
                        <asp:ListItem>2010</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td valign="top" >
                    <asp:TextBox ID="tbx_ref_maintenance" runat="server" Width="85px"></asp:TextBox>
                </td>
                <td valign="top" >
                    <asp:TextBox ID="tbx_com_maintenance" runat="server" Height="65px" TextMode="MultiLine" 
                    Width="130px"></asp:TextBox>
                </td>
                <td valign="top" >
                    <asp:Button ID="btn_maintenance" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" 
                    onclick="btn_maintenance_Click" />
                </td>
            </tr>
            <tr bgcolor="beige">
                <td valign="top" style="height: 18px">
                    <asp:Label ID="Label14" runat="server" 
                    Text="<%$ Resources:Resource, lbl_school_taxes %>" />
                </td>
                <td valign="top"  >
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="tbx_school_taxes_m" runat="server" 
    Width="65px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td valign="top"  >
                    <asp:DropDownList ID="ddl_school_taxes_date_received_m" runat="server" 
                     AutoPostBack="true" 
                     >
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    / &nbsp;
                    <asp:DropDownList ID="ddl_school_taxes_date_received_d" runat="server" 
                     >
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp; / &nbsp;
                    <asp:DropDownList ID="ddl_school_taxes_date_received_y" runat="server" 
                     >
                        <asp:ListItem>2001</asp:ListItem>
                        <asp:ListItem>2002</asp:ListItem>
                        <asp:ListItem>2003</asp:ListItem>
                        <asp:ListItem>2004</asp:ListItem>
                        <asp:ListItem>2005</asp:ListItem>
                        <asp:ListItem>2006</asp:ListItem>
                        <asp:ListItem>2007</asp:ListItem>
                        <asp:ListItem>2008</asp:ListItem>
                        <asp:ListItem>2009</asp:ListItem>
                        <asp:ListItem>2010</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td valign="top"  >
                    <asp:TextBox ID="tbx_ref_school_taxes" runat="server" Width="85px"></asp:TextBox>
                </td>
                <td valign="top"  >
                    <asp:TextBox ID="tbx_com_school_taxes" runat="server" Height="65px" TextMode="MultiLine" 
                    Width="130px"></asp:TextBox>
                </td>
                <td valign="top"  >
                    <asp:Button ID="btn_school_taxes" runat="server" 
                     Text="<%$ Resources:Resource, btn_submit %>" 
                     onclick="btn_school_taxes_Click" />
                </td>
            </tr>
            <tr>
                <td valign="top"  >
                    <asp:Label ID="Label22" runat="server" 
                    Text="<%$ Resources:Resource, lbl_management %>" />
                </td>
                <td valign="top"  >
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="tbx_management_m" runat="server" Width="65px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td valign="top" >
                    <asp:DropDownList ID="ddl_management_date_received_m" runat="server" 
                    AutoPostBack="true" 
                    >
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    / &nbsp;
                    <asp:DropDownList ID="ddl_management_date_received_d" runat="server" 
                    >
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp; / &nbsp;
                    <asp:DropDownList ID="ddl_management_date_received_y" runat="server" 
                    >
                        <asp:ListItem>2001</asp:ListItem>
                        <asp:ListItem>2002</asp:ListItem>
                        <asp:ListItem>2003</asp:ListItem>
                        <asp:ListItem>2004</asp:ListItem>
                        <asp:ListItem>2005</asp:ListItem>
                        <asp:ListItem>2006</asp:ListItem>
                        <asp:ListItem>2007</asp:ListItem>
                        <asp:ListItem>2008</asp:ListItem>
                        <asp:ListItem>2009</asp:ListItem>
                        <asp:ListItem>2010</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td valign="top" >
                    <asp:TextBox ID="tbx_ref_management" runat="server" Width="85px"></asp:TextBox>
                </td>
                <td valign="top" >
                    <asp:TextBox ID="tbx_com_management" runat="server" Height="65px" TextMode="MultiLine" 
                    Width="130px"></asp:TextBox>
                </td>
                <td valign="top" >
                    <asp:Button ID="btn_management" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" 
                    onclick="btn_management_Click" />
                </td>
            </tr>
            <tr bgcolor="beige">
                <td valign="top" style="height: 18px">
                    <asp:Label ID="Label23" runat="server" 
                    Text="<%$ Resources:Resource, lbl_advertising %>" />
                </td>
                <td valign="top" style="height: 18px">
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="tbx_advertising_m" runat="server" Width="65px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td valign="top"  >
                    <asp:DropDownList ID="ddl_advertising_date_received_m" runat="server" 
                    AutoPostBack="true" 
                    >
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    / &nbsp;
                    <asp:DropDownList ID="ddl_advertising_date_received_d" runat="server" 
                    >
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp; / &nbsp;
                    <asp:DropDownList ID="ddl_advertising_date_received_y" runat="server" 
                    >
                        <asp:ListItem>2001</asp:ListItem>
                        <asp:ListItem>2002</asp:ListItem>
                        <asp:ListItem>2003</asp:ListItem>
                        <asp:ListItem>2004</asp:ListItem>
                        <asp:ListItem>2005</asp:ListItem>
                        <asp:ListItem>2006</asp:ListItem>
                        <asp:ListItem>2007</asp:ListItem>
                        <asp:ListItem>2008</asp:ListItem>
                        <asp:ListItem>2009</asp:ListItem>
                        <asp:ListItem>2010</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td valign="top"  >
                    <asp:TextBox ID="tbx_ref_advertising" runat="server" Width="85px"></asp:TextBox>
                </td>
                <td valign="top"  >
                    <asp:TextBox ID="tbx_com_advertising" runat="server" Height="65px" TextMode="MultiLine" 
                    Width="130px"></asp:TextBox>
                </td>
                <td valign="top"  >
                    <asp:Button ID="btn_advertising" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" 
                    onclick="btn_advertising_Click" />
                </td>
            </tr>
            <tr>
                <td valign="top" style="height: 18px">
                    <asp:Label ID="Label24" runat="server" 
                    Text="<%$ Resources:Resource, lbl_legal %>" />
                </td>
                <td valign="top" >
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="tbx_legal_m" runat="server" Width="65px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td valign="top"  >
                    <asp:DropDownList ID="ddl_legal_date_received_m" runat="server" 
                    AutoPostBack="true" 
                    >
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    / &nbsp;
                    <asp:DropDownList ID="ddl_legal_date_received_d" runat="server" 
                    >
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp; / &nbsp;
                    <asp:DropDownList ID="ddl_legal_date_received_y" runat="server" 
                    >
                        <asp:ListItem>2001</asp:ListItem>
                        <asp:ListItem>2002</asp:ListItem>
                        <asp:ListItem>2003</asp:ListItem>
                        <asp:ListItem>2004</asp:ListItem>
                        <asp:ListItem>2005</asp:ListItem>
                        <asp:ListItem>2006</asp:ListItem>
                        <asp:ListItem>2007</asp:ListItem>
                        <asp:ListItem>2008</asp:ListItem>
                        <asp:ListItem>2009</asp:ListItem>
                        <asp:ListItem>2010</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td valign="top"  >
                    <asp:TextBox ID="tbx_ref_legal" runat="server" Width="85px"></asp:TextBox>
                </td>
                <td valign="top"  >
                    <asp:TextBox ID="tbx_com_legal" runat="server" Height="65px" TextMode="MultiLine" 
                    Width="130px"></asp:TextBox>
                </td>
                <td valign="top"  >
                    <asp:Button ID="btn_legal" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" onclick="btn_legal_Click" />
                </td>
            </tr>
            <tr bgcolor="beige">
                <td valign="top" style="height: 18px">
                    <asp:Label ID="Label25" runat="server" 
                    Text="<%$ Resources:Resource, lbl_accounting %>" />
                </td>
                <td valign="top" >
                    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="tbx_accounting_m" runat="server" Width="65px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td valign="top" >
                    <asp:DropDownList ID="ddl_accounting_date_received_m" runat="server" 
                    AutoPostBack="true" 
                    >
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    / &nbsp;
                    <asp:DropDownList ID="ddl_accounting_date_received_d" runat="server" 
                    >
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp; / &nbsp;
                    <asp:DropDownList ID="ddl_accounting_date_received_y" runat="server" 
                    >
                        <asp:ListItem>2001</asp:ListItem>
                        <asp:ListItem>2002</asp:ListItem>
                        <asp:ListItem>2003</asp:ListItem>
                        <asp:ListItem>2004</asp:ListItem>
                        <asp:ListItem>2005</asp:ListItem>
                        <asp:ListItem>2006</asp:ListItem>
                        <asp:ListItem>2007</asp:ListItem>
                        <asp:ListItem>2008</asp:ListItem>
                        <asp:ListItem>2009</asp:ListItem>
                        <asp:ListItem>2010</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td valign="top" >
                    <asp:TextBox ID="tbx_ref_accounting" runat="server" Width="85px"></asp:TextBox>
                </td>
                <td valign="top" >
                    <asp:TextBox ID="tbx_com_accounting" runat="server" Height="65px" TextMode="MultiLine" 
                    Width="130px"></asp:TextBox>
                </td>
                <td valign="top" >
                    <asp:Button ID="btn_accounting" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" 
                    onclick="btn_accounting_Click" />
                </td>
            </tr>
            <tr>
                <td valign="top" style="height: 18px">
                    <asp:Label ID="Label26" runat="server" 
                    Text="<%$ Resources:Resource, lbl_other %>"/>
                </td>
                <td valign="top" >
                    <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="tbx_other_m" runat="server" Width="65px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td valign="top" >
                    <asp:DropDownList ID="ddl_other_date_received_m" runat="server" 
                    AutoPostBack="true" 
                    >
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    / &nbsp;
                    <asp:DropDownList ID="ddl_other_date_received_d" runat="server" 
                    >
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp; / &nbsp;
                    <asp:DropDownList ID="ddl_other_date_received_y" runat="server" 
                    >
                        <asp:ListItem>2001</asp:ListItem>
                        <asp:ListItem>2002</asp:ListItem>
                        <asp:ListItem>2003</asp:ListItem>
                        <asp:ListItem>2004</asp:ListItem>
                        <asp:ListItem>2005</asp:ListItem>
                        <asp:ListItem>2006</asp:ListItem>
                        <asp:ListItem>2007</asp:ListItem>
                        <asp:ListItem>2008</asp:ListItem>
                        <asp:ListItem>2009</asp:ListItem>
                        <asp:ListItem>2010</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td valign="top" >
                    <asp:TextBox ID="tbx_ref_other" runat="server" Width="85px"></asp:TextBox>
                </td>
                <td valign="top" >
                    <asp:TextBox ID="tbx_com_other" runat="server" Height="65px" TextMode="MultiLine" 
                    Width="130px"></asp:TextBox>
                </td>
                <td valign="top" >
                    <asp:Button ID="btn_other" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" onclick="btn_other_Click" />
                </td>
            </tr>
        </table>
        <br />
        <table cellpadding="0" cellspacing="0" style="width: 83%">
            <tr>
                <td align="right" bgcolor="#879EAA">
                    <asp:Button ID="btn_submit_all" runat="server" ForeColor="#0066FF" 
                        onclick="btn_submit_all_Click" 
                        Text="<%$ Resources:Resource, btn_submit_all %>" />
                </td>
            </tr>
        </table>
    </asp:Panel>


    <br />

    </asp:Content>

