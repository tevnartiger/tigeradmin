﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : march 3, 2008
/// </summary>
public partial class manager_Financial_financial_scenario_list : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {


                // get the list of the money flow scenario

                tiger.PM f = new tiger.PM(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                gvFinancialMoneyFlowScenario.DataSource = f.getPMFinancialMoneyFlowScenarioList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                gvFinancialMoneyFlowScenario.DataBind();



        }
    }
}
