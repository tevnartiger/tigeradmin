﻿<%@ Page Language="C#" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" CodeFile="financial_expenses_update.aspx.cs" Inherits="manager_Financial_financial_expenses_update" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<asp:Label ID="Label2" runat="server" 
                    Text="<%$ Resources:Resource, lbl_expenses%>" /><br /><br />
                    
<table>
<tr><td> <asp:Label ID="lbl_property" runat="server" Text="<%$ Resources:Resource, lbl_property %>"/>
                </td>
                <td>
                   &nbsp;:&nbsp;  <asp:Label ID="lbl_property2" runat="server" Text="Label"></asp:Label>
       </td>  
</tr>
 </table><br />
    <table style="width: 100%">
        <tr>
            <td valign="top">
<asp:Repeater runat="server" ID="rhome_view">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
        
        <tr>
          <td>
                 District&nbsp;:&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_district")%> 
                   </td>
            </tr>
           <tr>
              <td valign="top" >
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_no")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_street")%></td>
            </tr>
            <tr>
                 <td valign="top" >
                   <%#DataBinder.Eval(Container.DataItem, "home_city")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_prov")%></td>
               
            </tr>
            <tr>
                 <td valign="top" >
                   <%#DataBinder.Eval(Container.DataItem, "home_pc")%> 
                
                   </td>
               
            </tr>   
        </table>
        </ItemTemplate>
        </asp:Repeater> 
            </td>
            <td valign="top">
                <table bgcolor="#ffffcc"  >
                    <tr>
                        <td>
                          <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, txt_month %>"></asp:Label>&nbsp;/&nbsp;<asp:Label ID="lbl_year1" runat="server" Text="<%$ Resources:Resource, lbl_year %>"></asp:Label>
                        </td>
                        <td>
                          &nbsp;:&nbsp;  <asp:Label ID="lbl_month" runat="server" ></asp:Label>&nbsp;/&nbsp; <asp:Label ID="lbl_year" runat="server" ></asp:Label>
                        </td>
                    </tr>
                    </table>
               
            </td>
        </tr>
    </table>
   
    <br />
    <table style="width: 100%" >
        <tr>
            <td valign="top" bgcolor="AliceBlue">
                <asp:Label ID="Label5" runat="server" 
                    Text="<%$ Resources:Resource, lbl_expenses%>" style="font-weight: 700"/></td>
            <td valign="top" style="font-weight: 700" bgcolor="AliceBlue">
                <asp:Label ID="Label36" runat="server" 
                          Text="<%$ Resources:Resource, txt_amount_paid %>" /></td>
            <td valign="top" bgcolor="AliceBlue">
                 <asp:Label ID="Label3" runat="server" 
                     Text="<%$ Resources:Resource, txt_date %>" style="font-weight: 700"/></td>
            <td valign="top" bgcolor="AliceBlue">
                 <asp:Label ID="Label6" runat="server" 
                    Text="<%$ Resources:Resource, lbl_reference%>" style="font-weight: 700"/></td>
            <td valign="top" bgcolor="AliceBlue">
                <asp:Label ID="Label4" runat="server" 
                    Text="<%$ Resources:Resource, lbl_com%>" style="font-weight: 700"/></td>
            <td valign="top" bgcolor="AliceBlue">
                 &nbsp;</td>
        </tr>
        <tr bgcolor="beige" >
            <td valign="top" style="height: 18px" >
                <asp:Label ID="lbl_expense_categ" runat="server" 
                    />&nbsp;</td>
            <td valign="top"  >
                <asp:TextBox ID="tbx_expense_amount" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td valign="top"  >
                <asp:DropDownList ID="ddl_update_date_received_m" runat="server" 
                    AutoPostBack="true" 
                    >
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                </asp:DropDownList>
                / &nbsp;<asp:DropDownList ID="ddl_update_date_received_d" runat="server" 
                    >
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                </asp:DropDownList>
                &nbsp;&nbsp; / &nbsp;
                <asp:DropDownList ID="ddl_update_date_received_y" runat="server" 
                    >
                    <asp:ListItem>2001</asp:ListItem>
                    <asp:ListItem>2002</asp:ListItem>
                    <asp:ListItem>2003</asp:ListItem>
                    <asp:ListItem>2004</asp:ListItem>
                    <asp:ListItem>2005</asp:ListItem>
                    <asp:ListItem>2006</asp:ListItem>
                    <asp:ListItem>2007</asp:ListItem>
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td valign="top" >
                <asp:TextBox ID="tbx_expense_reference" runat="server" Width="85px"></asp:TextBox>
            </td>
            <td valign="top" >
                <asp:TextBox ID="tbx_expense_comments" runat="server" Height="65px" TextMode="MultiLine" 
                    Width="131px"></asp:TextBox>
            </td>
            <td valign="top" >
                <asp:Button ID="btn_update" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" 
                    onclick="btn_update_Click" />
            </td>
        </tr>
        
      
        <asp:HiddenField Visible="false" ID="h_h_id" runat="server" />
        <asp:HiddenField Visible="false" ID="h_m" runat="server" />
        <asp:HiddenField Visible="false" ID="h_d" runat="server" />
        <asp:HiddenField Visible="false" ID="h_y" runat="server" />                
    </table>


  
</asp:Content>

