﻿using System;
using System.Web.Script.Services;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.NameObjectAuthorization;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : march 19, 2008
/// </summary>

public partial class manager_Financial_financial_expenses : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!(Page.IsPostBack))
        {

            Panel1.Visible = false;

            NameObjectAuthorization homeAuthorization = new NameObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            DateTime date = new DateTime();
            date = DateTime.Now;

            // default Date receive of the expenses
         //   ddl_year.SelectedValue = date.Year.ToString();




            tiger.Date c = new tiger.Date();
            //if we are entering from the update page
            //-----------------------------------------------------
            if (Request.QueryString["h_id"] != "" && Request.QueryString["h_id"] != null && Request.QueryString["m"] != "" && Request.QueryString["m"] != null && Request.QueryString["y"] != "" && Request.QueryString["y"] != null)
            {
                date = Convert.ToDateTime(c.DateCulture(Request.QueryString["m"], Request.QueryString["d"], Request.QueryString["y"], Convert.ToString(Session["_lastCulture"])));

            }
            //-----------------------------------------------------

            ddl_date_received_m.SelectedValue = date.Month.ToString();
            ddl_date_received_y.SelectedValue = date.Year.ToString();

            // electricity
            ddl_electricity_date_received_y.SelectedValue = date.Year.ToString();
            ddl_electricity_date_received_d.SelectedValue = date.Day.ToString();
            ddl_electricity_date_received_m.SelectedValue = date.Month.ToString();


            // energy
            ddl_energy_date_received_y.SelectedValue = date.Year.ToString();
            ddl_energy_date_received_d.SelectedValue = date.Day.ToString();
            ddl_energy_date_received_m.SelectedValue = date.Month.ToString();


            // insurances
            ddl_insurances_date_received_y.SelectedValue = date.Year.ToString();
            ddl_insurances_date_received_d.SelectedValue = date.Day.ToString();
            ddl_insurances_date_received_m.SelectedValue = date.Month.ToString();

            // janitor
            ddl_janitor_date_received_y.SelectedValue = date.Year.ToString();
            ddl_janitor_date_received_d.SelectedValue = date.Day.ToString();
            ddl_janitor_date_received_m.SelectedValue = date.Month.ToString();


            // taxes
            ddl_taxes_date_received_y.SelectedValue = date.Year.ToString();
            ddl_taxes_date_received_d.SelectedValue = date.Day.ToString();
            ddl_taxes_date_received_m.SelectedValue = date.Month.ToString();


            // maintenance
            ddl_maintenance_date_received_y.SelectedValue = date.Year.ToString();
            ddl_maintenance_date_received_d.SelectedValue = date.Day.ToString();
            ddl_maintenance_date_received_m.SelectedValue = date.Month.ToString();


            // school_taxes
            ddl_school_taxes_date_received_y.SelectedValue = date.Year.ToString();
            ddl_school_taxes_date_received_d.SelectedValue = date.Day.ToString();
            ddl_school_taxes_date_received_m.SelectedValue = date.Month.ToString();

            // management
            ddl_management_date_received_y.SelectedValue = date.Year.ToString();
            ddl_management_date_received_d.SelectedValue = date.Day.ToString();
            ddl_management_date_received_m.SelectedValue = date.Month.ToString();

            // advertising
            ddl_advertising_date_received_y.SelectedValue = date.Year.ToString();
            ddl_advertising_date_received_d.SelectedValue = date.Day.ToString();
            ddl_advertising_date_received_m.SelectedValue = date.Month.ToString();

            // legal
            ddl_legal_date_received_y.SelectedValue = date.Year.ToString();
            ddl_legal_date_received_d.SelectedValue = date.Day.ToString();
            ddl_legal_date_received_m.SelectedValue = date.Month.ToString();


            // accounting
            ddl_accounting_date_received_y.SelectedValue = date.Year.ToString();
            ddl_accounting_date_received_d.SelectedValue = date.Day.ToString();
            ddl_accounting_date_received_m.SelectedValue = date.Month.ToString();



            ddl_other_date_received_y.SelectedValue = date.Year.ToString();
            ddl_other_date_received_d.SelectedValue = date.Day.ToString();
            ddl_other_date_received_m.SelectedValue = date.Month.ToString();







          tiger.PM h = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
          int home_count = h.getPMHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
          string link_to_unit = "";
          if (home_count > 0)
          {
              int home_id = h.getPMHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

              //if we are entering from the update page
              //-----------------------------------------------------
              if (Request.QueryString["h_id"] != "" && Request.QueryString["h_id"] != null)
              {
                  home_id = Convert.ToInt32(Request.QueryString["h_id"]);
              }
              //-----------------------------------------------------


              ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
              if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), home_id, Convert.ToInt32(Session["name_id"]), 6))
              {
                  Session.Abandon();
                  Response.Redirect("~/login.aspx");
              }
              ///////// SECURITY OBJECT CHECK  END ////////////////////////////////


              ddl_home_id.DataSource = h.getPMHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
              ddl_home_id.SelectedValue = Convert.ToString(home_id);
              ddl_home_id.DataBind();



              //To view the address of the property

            

              // to view the expenses in the default home
              DateTime expense_date_paid = new DateTime();
              tiger.Date d = new tiger.Date();
              expense_date_paid  = Convert.ToDateTime(d.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

              //if we are entering from the update page
              //-----------------------------------------------------
              if (Request.QueryString["h_id"] != "" && Request.QueryString["h_id"] != null && Request.QueryString["m"] != "" && Request.QueryString["m"] != null && Request.QueryString["y"] != "" && Request.QueryString["y"] != null)
              {
                  expense_date_paid = Convert.ToDateTime(d.DateCulture(Request.QueryString["m"], "1", Request.QueryString["y"], Convert.ToString(Session["_lastCulture"])));

              }
              //-----------------------------------------------------

              tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
              
              ///////////////////////////////////////////////////////////////////////////////////////////////////
              gv_Expense.DataSource = hp.getExpenseMonthView(home_id, Convert.ToInt32(Session["schema_id"]), expense_date_paid);
              gv_Expense.DataBind();

               lbl_total_month_expense.Text = hp.getTotalMonthExpense(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]),expense_date_paid);
           

          }
          // if ther is no home

          else
          {
              //  txt_message.InnerHtml = "There is no property -- Add a property";

              //  txt_link.InnerHtml = home_count + " <a href='home_add.aspx'>Add Home</a>&nbsp;&nbsp;<a href='owner_add.aspx'>Add Owner</a>&nbsp;&nbsp;<a href='janitor_add.aspx'>Add Janitor</a>&nbsp;&nbsp;";
          }


          // Here we are rendering invisible the hyperlink and the bottuon of the 
         // Work orders ( task ) - i.e : maintenance/repair
          for (int j = 0; j < gv_Expense.Rows.Count; j++)
          {
              Button btn_cancel = (Button)gv_Expense.Rows[j].FindControl("btn_cancel");
              HyperLink link_update = (HyperLink)gv_Expense.Rows[j].FindControl("link_update");
              HiddenField h_task_id = (HiddenField)gv_Expense.Rows[j].FindControl("h_task_id");

              if(Convert.ToInt32(h_task_id.Value)>0)
              {
                  btn_cancel.Visible = false;
                  link_update.Visible = false;
              }
               
          }

        }

    }

    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {


        tbx_electricity_m.Text = "";
        tbx_energy_m.Text = "";
        tbx_insurances_m.Text = "";
        tbx_janitor_m.Text = "";
        tbx_taxes_m.Text = "";
        tbx_maintenance_repair_m.Text = "";
        tbx_school_taxes_m.Text = "";
        tbx_management_m.Text = "";
        tbx_advertising_m.Text = "";
        tbx_legal_m.Text = "";
        tbx_accounting_m.Text = "";
        tbx_other_m.Text = "";


        // to view the expenses in the  home
       //////////////////////////////////////// // to view the expenses in the  home  ////////////////////////////
        DateTime expense_date_paid = new DateTime();
        tiger.Date df = new tiger.Date();
        expense_date_paid = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
         
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        gv_Expense.DataSource = hp.getExpenseMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);
        gv_Expense.DataBind();

        lbl_total_month_expense.Text = hp.getTotalMonthExpense(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);

        // Here we are rendering invisible the hyperlink and the bottuon of the 
        // Work orders ( task ) - i.e : maintenance/repair
        for (int j = 0; j < gv_Expense.Rows.Count; j++)
        {
            Button btn_cancel = (Button)gv_Expense.Rows[j].FindControl("btn_cancel");
            HyperLink link_update = (HyperLink)gv_Expense.Rows[j].FindControl("link_update");
            HiddenField h_task_id = (HiddenField)gv_Expense.Rows[j].FindControl("h_task_id");

            if (Convert.ToInt32(h_task_id.Value) > 0)
            {
                btn_cancel.Visible = false;
                link_update.Visible = false;
            }

        }
 
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_all_Click(object sender, EventArgs e)
    {

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime paid_date = new DateTime();
        tiger.Date d = new tiger.Date();

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prExpenseBatchAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        // try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@expensecateg_id", SqlDbType.Int).Value = 1;
            paid_date = Convert.ToDateTime(d.DateCulture(ddl_electricity_date_received_m.SelectedValue, ddl_electricity_date_received_d.SelectedValue, ddl_electricity_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            cmd.Parameters.Add("@expense_date_paid", SqlDbType.DateTime).Value = paid_date;

            if (RegEx.IsMoney(tbx_electricity_m.Text))
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_electricity_m.Text);
            else
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = 0.00;


            cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = RegEx.getText(tbx_ref_electricity.Text);
            cmd.Parameters.Add("@expense_comments", SqlDbType.Text).Value = RegEx.getText(tbx_com_electricity.Text);


            cmd.Parameters.Add("@expensecateg_id2", SqlDbType.Int).Value = 2;
            paid_date = Convert.ToDateTime(d.DateCulture(ddl_energy_date_received_m.SelectedValue, ddl_energy_date_received_d.SelectedValue, ddl_energy_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            cmd.Parameters.Add("@expense_date_paid2", SqlDbType.DateTime).Value = paid_date;

            if (RegEx.IsMoney(tbx_energy_m.Text))
                cmd.Parameters.Add("@expense_amount2", SqlDbType.Money).Value = Convert.ToDecimal(tbx_energy_m.Text);
            else
                cmd.Parameters.Add("@expense_amount2", SqlDbType.Money).Value = 0.00;

            cmd.Parameters.Add("@expense_reference2", SqlDbType.NVarChar, 50).Value = RegEx.getText(tbx_ref_energy.Text);
            cmd.Parameters.Add("@expense_comments2", SqlDbType.Text).Value = RegEx.getText(tbx_com_energy.Text);




            cmd.Parameters.Add("@expensecateg_id4", SqlDbType.Int).Value = 4;
            paid_date = Convert.ToDateTime(d.DateCulture(ddl_janitor_date_received_m.SelectedValue, ddl_janitor_date_received_d.SelectedValue, ddl_janitor_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            cmd.Parameters.Add("@expense_date_paid4", SqlDbType.DateTime).Value = paid_date;

            if (RegEx.IsMoney(tbx_janitor_m.Text))
                cmd.Parameters.Add("@expense_amount4", SqlDbType.Money).Value = Convert.ToDecimal(tbx_janitor_m.Text);
            else
                cmd.Parameters.Add("@expense_amount4", SqlDbType.Money).Value = 0.00;


            cmd.Parameters.Add("@expense_reference4", SqlDbType.NVarChar, 50).Value = RegEx.getText(tbx_ref_janitor.Text);
            cmd.Parameters.Add("@expense_comments4", SqlDbType.Text).Value = RegEx.getText(tbx_com_janitor.Text);



            cmd.Parameters.Add("@expensecateg_id3", SqlDbType.Int).Value = 3;
            paid_date = Convert.ToDateTime(d.DateCulture(ddl_insurances_date_received_m.SelectedValue, ddl_insurances_date_received_d.SelectedValue, ddl_insurances_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            cmd.Parameters.Add("@expense_date_paid3", SqlDbType.DateTime).Value = paid_date;

            if (RegEx.IsMoney(tbx_insurances_m.Text))
                cmd.Parameters.Add("@expense_amount3", SqlDbType.Money).Value = Convert.ToDecimal(tbx_insurances_m.Text);
            else
                cmd.Parameters.Add("@expense_amount3", SqlDbType.Money).Value = 0.00;


            cmd.Parameters.Add("@expense_reference3", SqlDbType.NVarChar, 50).Value = RegEx.getText(tbx_ref_insurances.Text);
            cmd.Parameters.Add("@expense_comments3", SqlDbType.Text).Value = RegEx.getText(tbx_com_insurances.Text);



            cmd.Parameters.Add("@expensecateg_id5", SqlDbType.Int).Value = 5;
            paid_date = Convert.ToDateTime(d.DateCulture(ddl_taxes_date_received_m.SelectedValue, ddl_taxes_date_received_d.SelectedValue, ddl_taxes_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            cmd.Parameters.Add("@expense_date_paid5", SqlDbType.DateTime).Value = paid_date;

            if (RegEx.IsMoney(tbx_taxes_m.Text))
                cmd.Parameters.Add("@expense_amount5", SqlDbType.Money).Value = Convert.ToDecimal(tbx_taxes_m.Text);
            else
                cmd.Parameters.Add("@expense_amount5", SqlDbType.Money).Value = 0.00;


            cmd.Parameters.Add("@expense_reference5", SqlDbType.NVarChar, 50).Value = RegEx.getText(tbx_ref_taxes.Text);
            cmd.Parameters.Add("@expense_comments5", SqlDbType.Text).Value = RegEx.getText(tbx_com_taxes.Text);





            cmd.Parameters.Add("@expensecateg_id6", SqlDbType.Int).Value = 6;
            paid_date = Convert.ToDateTime(d.DateCulture(ddl_maintenance_date_received_m.SelectedValue, ddl_maintenance_date_received_d.SelectedValue, ddl_maintenance_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            cmd.Parameters.Add("@expense_date_paid6", SqlDbType.DateTime).Value = paid_date;

            if (RegEx.IsMoney(tbx_maintenance_repair_m.Text))
                cmd.Parameters.Add("@expense_amount6", SqlDbType.Money).Value = Convert.ToDecimal(tbx_maintenance_repair_m.Text);
            else
                cmd.Parameters.Add("@expense_amount6", SqlDbType.Money).Value = 0.00;


            cmd.Parameters.Add("@expense_reference6", SqlDbType.NVarChar, 50).Value = RegEx.getText(tbx_ref_maintenance.Text);
            cmd.Parameters.Add("@expense_comments6", SqlDbType.Text).Value = RegEx.getText(tbx_com_maintenance.Text);




            cmd.Parameters.Add("@expensecateg_id7", SqlDbType.Int).Value = 7;
            paid_date = Convert.ToDateTime(d.DateCulture(ddl_school_taxes_date_received_m.SelectedValue, ddl_school_taxes_date_received_d.SelectedValue, ddl_school_taxes_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            cmd.Parameters.Add("@expense_date_paid7", SqlDbType.DateTime).Value = paid_date;

            if (RegEx.IsMoney(tbx_school_taxes_m.Text))
                cmd.Parameters.Add("@expense_amount7", SqlDbType.Money).Value = Convert.ToDecimal(tbx_school_taxes_m.Text);
            else
                cmd.Parameters.Add("@expense_amount7", SqlDbType.Money).Value = 0.00;


            cmd.Parameters.Add("@expense_reference7", SqlDbType.NVarChar, 50).Value = RegEx.getText(tbx_ref_school_taxes.Text);
            cmd.Parameters.Add("@expense_comments7", SqlDbType.Text).Value = RegEx.getText(tbx_com_school_taxes.Text);



            cmd.Parameters.Add("@expensecateg_id8", SqlDbType.Int).Value = 8;
            paid_date = Convert.ToDateTime(d.DateCulture(ddl_management_date_received_m.SelectedValue, ddl_management_date_received_d.SelectedValue, ddl_management_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            cmd.Parameters.Add("@expense_date_paid8", SqlDbType.DateTime).Value = paid_date;

            if (RegEx.IsMoney(tbx_management_m.Text))
                cmd.Parameters.Add("@expense_amount8", SqlDbType.Money).Value = Convert.ToDecimal(tbx_management_m.Text);
            else
                cmd.Parameters.Add("@expense_amount8", SqlDbType.Money).Value = 0.00;

            cmd.Parameters.Add("@expense_reference8", SqlDbType.NVarChar, 50).Value = RegEx.getText(tbx_ref_management.Text);
            cmd.Parameters.Add("@expense_comments8", SqlDbType.Text).Value = RegEx.getText(tbx_com_management.Text);



            cmd.Parameters.Add("@expensecateg_id9", SqlDbType.Int).Value = 9;
            paid_date = Convert.ToDateTime(d.DateCulture(ddl_advertising_date_received_m.SelectedValue, ddl_advertising_date_received_d.SelectedValue, ddl_advertising_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            cmd.Parameters.Add("@expense_date_paid9", SqlDbType.DateTime).Value = paid_date;

            if (RegEx.IsMoney(tbx_advertising_m.Text))
                cmd.Parameters.Add("@expense_amount9", SqlDbType.Money).Value = Convert.ToDecimal(tbx_advertising_m.Text);
            else
                cmd.Parameters.Add("@expense_amount9", SqlDbType.Money).Value = 0.00;

            cmd.Parameters.Add("@expense_reference9", SqlDbType.NVarChar, 50).Value = RegEx.getText(tbx_ref_advertising.Text);
            cmd.Parameters.Add("@expense_comments9", SqlDbType.Text).Value = RegEx.getText(tbx_com_advertising.Text);



            cmd.Parameters.Add("@expensecateg_id10", SqlDbType.Int).Value = 10;
            paid_date = Convert.ToDateTime(d.DateCulture(ddl_legal_date_received_m.SelectedValue, ddl_legal_date_received_d.SelectedValue, ddl_legal_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            cmd.Parameters.Add("@expense_date_paid10", SqlDbType.DateTime).Value = paid_date;

            if (RegEx.IsMoney(tbx_legal_m.Text))
                cmd.Parameters.Add("@expense_amount10", SqlDbType.Money).Value = Convert.ToDecimal(tbx_legal_m.Text);
            else
                cmd.Parameters.Add("@expense_amount10", SqlDbType.Money).Value = 0.00;

            cmd.Parameters.Add("@expense_reference10", SqlDbType.NVarChar, 50).Value = RegEx.getText(tbx_ref_legal.Text);
            cmd.Parameters.Add("@expense_comments10", SqlDbType.Text).Value = RegEx.getText(tbx_com_legal.Text);



            cmd.Parameters.Add("@expensecateg_id11", SqlDbType.Int).Value = 11;
            paid_date = Convert.ToDateTime(d.DateCulture(ddl_accounting_date_received_m.SelectedValue, ddl_accounting_date_received_d.SelectedValue, ddl_accounting_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            cmd.Parameters.Add("@expense_date_paid11", SqlDbType.DateTime).Value = paid_date;

            if (RegEx.IsMoney(tbx_accounting_m.Text))
                cmd.Parameters.Add("@expense_amount11", SqlDbType.Money).Value = Convert.ToDecimal(tbx_accounting_m.Text);
            else
                cmd.Parameters.Add("@expense_amount11", SqlDbType.Money).Value = 0.00;

            cmd.Parameters.Add("@expense_reference11", SqlDbType.NVarChar, 50).Value = RegEx.getText(tbx_ref_accounting.Text);
            cmd.Parameters.Add("@expense_comments11", SqlDbType.Text).Value = RegEx.getText(tbx_com_accounting.Text);




            cmd.Parameters.Add("@expensecateg_id12", SqlDbType.Int).Value = 12;
            paid_date = Convert.ToDateTime(d.DateCulture(ddl_other_date_received_m.SelectedValue, ddl_other_date_received_d.SelectedValue, ddl_other_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            cmd.Parameters.Add("@expense_date_paid12", SqlDbType.DateTime).Value = paid_date;

            if (RegEx.IsMoney(tbx_other_m.Text))
                cmd.Parameters.Add("@expense_amount12", SqlDbType.Money).Value = Convert.ToDecimal(tbx_other_m.Text);
            else
                cmd.Parameters.Add("@expense_amount12", SqlDbType.Money).Value = 0.00;

            cmd.Parameters.Add("@expense_reference12", SqlDbType.NVarChar, 50).Value = RegEx.getText(tbx_ref_other.Text);
            cmd.Parameters.Add("@expense_comments12", SqlDbType.Text).Value = RegEx.getText(tbx_com_other.Text);




            //execute the insert
            cmd.ExecuteReader();


        }
        //  catch (Exception error)
        {
            //   tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }




        //////////////////////////////////////// // to view the expenses in the  home  ////////////////////////////
        DateTime expense_date_paid = new DateTime();
        tiger.Date df = new tiger.Date();
        expense_date_paid = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        gv_Expense.DataSource = hp.getExpenseMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);
        gv_Expense.DataBind();




        lbl_total_month_expense.Text = hp.getTotalMonthExpense(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);

        MaintainScrollPositionOnPostBack = false;




        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_expense_reference.Visible = false;



        // Here we are rendering invisible the hyperlink and the bottuon of the 
        // Work orders ( task ) - i.e : maintenance/repair
        for (int j = 0; j < gv_Expense.Rows.Count; j++)
        {
            Button btn_cancel = (Button)gv_Expense.Rows[j].FindControl("btn_cancel");
            HyperLink link_update = (HyperLink)gv_Expense.Rows[j].FindControl("link_update");
            HiddenField h_task_id = (HiddenField)gv_Expense.Rows[j].FindControl("h_task_id");

            if (Convert.ToInt32(h_task_id.Value) > 0)
            {
                btn_cancel.Visible = false;
                link_update.Visible = false;
            }

        }

    }
  
    
    protected void ddl_date_received_y_SelectedIndexChanged(object sender, EventArgs e)
    {
        // electricity
        ddl_electricity_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_electricity_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;


        // energy
        ddl_energy_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_energy_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // insurances
        ddl_insurances_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_insurances_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // janitor
        ddl_janitor_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_janitor_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // taxes
        ddl_taxes_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_taxes_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;


        // maintenance
        ddl_maintenance_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_maintenance_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;


        // school_taxes
        ddl_school_taxes_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_school_taxes_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // management
        ddl_management_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_management_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // advertising
        ddl_advertising_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_advertising_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // legal
        ddl_legal_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_legal_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;


        // accounting
        ddl_accounting_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_accounting_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;



        ddl_other_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_other_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // to view the expenses in the  home
        DateTime expense_date_paid = new DateTime();
        tiger.Date d = new tiger.Date();
        expense_date_paid = Convert.ToDateTime(d.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        gv_Expense.DataSource = hp.getExpenseMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);
        gv_Expense.DataBind();




        // Here we are rendering invisible the hyperlink and the bottuon of the 
        // Work orders ( task ) - i.e : maintenance/repair
        for (int j = 0; j < gv_Expense.Rows.Count; j++)
        {
            Button btn_cancel = (Button)gv_Expense.Rows[j].FindControl("btn_cancel");
            HyperLink link_update = (HyperLink)gv_Expense.Rows[j].FindControl("link_update");
            HiddenField h_task_id = (HiddenField)gv_Expense.Rows[j].FindControl("h_task_id");

            if (Convert.ToInt32(h_task_id.Value) > 0)
            {
                btn_cancel.Visible = false;
                link_update.Visible = false;
            }

        }
        

        lbl_total_month_expense.Text = hp.getTotalMonthExpense(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]),expense_date_paid);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_expense_reference.Visible = false;

    }
    protected void ddl_date_received_m_SelectedIndexChanged(object sender, EventArgs e)
    {


        tbx_electricity_m.Text = "";
        tbx_energy_m.Text = "";
        tbx_insurances_m.Text = "";
        tbx_janitor_m.Text = "";
        tbx_taxes_m.Text = "";
        tbx_maintenance_repair_m.Text = "";
        tbx_school_taxes_m.Text = "";
        tbx_management_m.Text = "";
        tbx_advertising_m.Text = "";
        tbx_legal_m.Text = "";
        tbx_accounting_m.Text = "";
        tbx_other_m.Text = "";

        
        // electricity
        ddl_electricity_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_electricity_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;


        // energy
        ddl_energy_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_energy_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // insurances
        ddl_insurances_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_insurances_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // janitor
        ddl_janitor_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_janitor_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // taxes
        ddl_taxes_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_taxes_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;


        // maintenance
        ddl_maintenance_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_maintenance_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;


        // school_taxes
        ddl_school_taxes_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_school_taxes_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // management
        ddl_management_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_management_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // advertising
        ddl_advertising_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_advertising_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // legal
        ddl_legal_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_legal_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;


        // accounting
        ddl_accounting_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_accounting_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;



        ddl_other_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_other_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // to view the expenses in the  home
        DateTime expense_date_paid = new DateTime();
        tiger.Date d = new tiger.Date();
        expense_date_paid = Convert.ToDateTime(d.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
         
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        gv_Expense.DataSource = hp.getExpenseMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);
        gv_Expense.DataBind();


        // Here we are rendering invisible the hyperlink and the bottuon of the 
        // Work orders ( task ) - i.e : maintenance/repair
        for (int j = 0; j < gv_Expense.Rows.Count; j++)
        {
            Button btn_cancel = (Button)gv_Expense.Rows[j].FindControl("btn_cancel");
            HyperLink link_update = (HyperLink)gv_Expense.Rows[j].FindControl("link_update");
            HiddenField h_task_id = (HiddenField)gv_Expense.Rows[j].FindControl("h_task_id");

            if (Convert.ToInt32(h_task_id.Value) > 0)
            {
                btn_cancel.Visible = false;
                link_update.Visible = false;
            }

        }


        
        lbl_total_month_expense.Text = hp.getTotalMonthExpense(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]),expense_date_paid);

           

        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_expense_reference.Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_electricity_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime paid_date = new DateTime();
        tiger.Date d = new tiger.Date();
        paid_date = Convert.ToDateTime(d.DateCulture(ddl_electricity_date_received_m.SelectedValue, ddl_electricity_date_received_d.SelectedValue, ddl_electricity_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prExpenseAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@expensecateg_id", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@expense_date_paid", SqlDbType.DateTime).Value = paid_date;



            if (RegEx.IsMoney(tbx_electricity_m.Text))
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_electricity_m.Text);
            else
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = 0.00;

            if (RegEx.IsText(tbx_ref_electricity.Text))
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_electricity.Text);
            else
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = "";


            if (RegEx.IsText(tbx_com_electricity.Text))
                cmd.Parameters.Add("@expense_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_electricity.Text);
            else
                cmd.Parameters.Add("@expense_comments", SqlDbType.NVarChar, 50).Value = "";



            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }




        //////////////////////////////////////// // to view the expenses in the  home  ////////////////////////////
        DateTime expense_date_paid = new DateTime();
        tiger.Date df = new tiger.Date();
        expense_date_paid = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        gv_Expense.DataSource = hp.getExpenseMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);
        gv_Expense.DataBind();



        // Here we are rendering invisible the hyperlink and the bottuon of the 
        // Work orders ( task ) - i.e : maintenance/repair
        for (int j = 0; j < gv_Expense.Rows.Count; j++)
        {
            Button btn_cancel = (Button)gv_Expense.Rows[j].FindControl("btn_cancel");
            HyperLink link_update = (HyperLink)gv_Expense.Rows[j].FindControl("link_update");
            HiddenField h_task_id = (HiddenField)gv_Expense.Rows[j].FindControl("h_task_id");

            if (Convert.ToInt32(h_task_id.Value) > 0)
            {
                btn_cancel.Visible = false;
                link_update.Visible = false;
            }

        }

        lbl_total_month_expense.Text = hp.getTotalMonthExpense(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);

        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_expense_reference.Visible = false;


        tbx_electricity_m.Text = "";
        tbx_energy_m.Text = "";
        tbx_insurances_m.Text = "";
        tbx_janitor_m.Text = "";
        tbx_taxes_m.Text = "";
        tbx_maintenance_repair_m.Text = "";
        tbx_school_taxes_m.Text = "";
        tbx_management_m.Text = "";
        tbx_advertising_m.Text = "";
        tbx_legal_m.Text = "";
        tbx_accounting_m.Text = "";
        tbx_other_m.Text = "";


    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_energy_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime paid_date = new DateTime();
        tiger.Date d = new tiger.Date();
        paid_date = Convert.ToDateTime(d.DateCulture(ddl_energy_date_received_m.SelectedValue, ddl_energy_date_received_d.SelectedValue, ddl_energy_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prExpenseAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@expensecateg_id", SqlDbType.Int).Value = 2;
            cmd.Parameters.Add("@expense_date_paid", SqlDbType.DateTime).Value = paid_date;


            if (RegEx.IsMoney(tbx_energy_m.Text))
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_energy_m.Text);
            else
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = 0.00;

            if (RegEx.IsText(tbx_ref_energy.Text))
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_energy.Text);
            else
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = "";


            if (RegEx.IsText(tbx_com_energy.Text))
                cmd.Parameters.Add("@expense_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_energy.Text);
            else
                cmd.Parameters.Add("@expense_comments", SqlDbType.NVarChar, 50).Value = "";



            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }


        //////////////////////////////////////// // to view the expenses in the  home  ////////////////////////////
        DateTime expense_date_paid = new DateTime();
        tiger.Date df = new tiger.Date();
        expense_date_paid = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        gv_Expense.DataSource = hp.getExpenseMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);
        gv_Expense.DataBind();



        // Here we are rendering invisible the hyperlink and the bottuon of the 
        // Work orders ( task ) - i.e : maintenance/repair
        for (int j = 0; j < gv_Expense.Rows.Count; j++)
        {
            Button btn_cancel = (Button)gv_Expense.Rows[j].FindControl("btn_cancel");
            HyperLink link_update = (HyperLink)gv_Expense.Rows[j].FindControl("link_update");
            HiddenField h_task_id = (HiddenField)gv_Expense.Rows[j].FindControl("h_task_id");

            if (Convert.ToInt32(h_task_id.Value) > 0)
            {
                btn_cancel.Visible = false;
                link_update.Visible = false;
            }

        }

        lbl_total_month_expense.Text = hp.getTotalMonthExpense(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_expense_reference.Visible = false;


        tbx_electricity_m.Text = "";
        tbx_energy_m.Text = "";
        tbx_insurances_m.Text = "";
        tbx_janitor_m.Text = "";
        tbx_taxes_m.Text = "";
        tbx_maintenance_repair_m.Text = "";
        tbx_school_taxes_m.Text = "";
        tbx_management_m.Text = "";
        tbx_advertising_m.Text = "";
        tbx_legal_m.Text = "";
        tbx_accounting_m.Text = "";
        tbx_other_m.Text = "";


    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_janitor_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime paid_date = new DateTime();
        tiger.Date d = new tiger.Date();
        paid_date = Convert.ToDateTime(d.DateCulture(ddl_janitor_date_received_m.SelectedValue, ddl_janitor_date_received_d.SelectedValue, ddl_janitor_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prExpenseAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@expensecateg_id", SqlDbType.Int).Value = 4;
            cmd.Parameters.Add("@expense_date_paid", SqlDbType.DateTime).Value = paid_date;


            if (RegEx.IsMoney(tbx_janitor_m.Text))
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_janitor_m.Text);
            else
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = 0.00;

            if (RegEx.IsText(tbx_ref_janitor.Text))
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_janitor.Text);
            else
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = "";


            if (RegEx.IsText(tbx_com_janitor.Text))
                cmd.Parameters.Add("@expense_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_janitor.Text);
            else
                cmd.Parameters.Add("@expense_comments", SqlDbType.NVarChar, 50).Value = "";


            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }




        //////////////////////////////////////// // to view the expenses in the  home  ////////////////////////////
        DateTime expense_date_paid = new DateTime();
        tiger.Date df = new tiger.Date();
        expense_date_paid = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        gv_Expense.DataSource = hp.getExpenseMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);
        gv_Expense.DataBind();



        // Here we are rendering invisible the hyperlink and the bottuon of the 
        // Work orders ( task ) - i.e : maintenance/repair
        for (int j = 0; j < gv_Expense.Rows.Count; j++)
        {
            Button btn_cancel = (Button)gv_Expense.Rows[j].FindControl("btn_cancel");
            HyperLink link_update = (HyperLink)gv_Expense.Rows[j].FindControl("link_update");
            HiddenField h_task_id = (HiddenField)gv_Expense.Rows[j].FindControl("h_task_id");

            if (Convert.ToInt32(h_task_id.Value) > 0)
            {
                btn_cancel.Visible = false;
                link_update.Visible = false;
            }

        }

        lbl_total_month_expense.Text = hp.getTotalMonthExpense(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_expense_reference.Visible = false;


        tbx_electricity_m.Text = "";
        tbx_energy_m.Text = "";
        tbx_insurances_m.Text = "";
        tbx_janitor_m.Text = "";
        tbx_taxes_m.Text = "";
        tbx_maintenance_repair_m.Text = "";
        tbx_school_taxes_m.Text = "";
        tbx_management_m.Text = "";
        tbx_advertising_m.Text = "";
        tbx_legal_m.Text = "";
        tbx_accounting_m.Text = "";
        tbx_other_m.Text = "";

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_insurances_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime paid_date = new DateTime();
        tiger.Date d = new tiger.Date();
        paid_date = Convert.ToDateTime(d.DateCulture(ddl_insurances_date_received_m.SelectedValue, ddl_insurances_date_received_d.SelectedValue, ddl_insurances_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prExpenseAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@expensecateg_id", SqlDbType.Int).Value = 3;
            cmd.Parameters.Add("@expense_date_paid", SqlDbType.DateTime).Value = paid_date;


            if (RegEx.IsMoney(tbx_insurances_m.Text))
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_insurances_m.Text);
            else
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = 0.00;

            if (RegEx.IsText(tbx_ref_insurances.Text))
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_insurances.Text);
            else
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = "";


            if (RegEx.IsText(tbx_com_insurances.Text))
                cmd.Parameters.Add("@expense_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_insurances.Text);
            else
                cmd.Parameters.Add("@expense_comments", SqlDbType.NVarChar, 50).Value = "";

            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }



        //////////////////////////////////////// // to view the expenses in the  home  ////////////////////////////
        DateTime expense_date_paid = new DateTime();
        tiger.Date df = new tiger.Date();
        expense_date_paid = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        gv_Expense.DataSource = hp.getExpenseMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);
        gv_Expense.DataBind();


        // Here we are rendering invisible the hyperlink and the bottuon of the 
        // Work orders ( task ) - i.e : maintenance/repair
        for (int j = 0; j < gv_Expense.Rows.Count; j++)
        {
            Button btn_cancel = (Button)gv_Expense.Rows[j].FindControl("btn_cancel");
            HyperLink link_update = (HyperLink)gv_Expense.Rows[j].FindControl("link_update");
            HiddenField h_task_id = (HiddenField)gv_Expense.Rows[j].FindControl("h_task_id");

            if (Convert.ToInt32(h_task_id.Value) > 0)
            {
                btn_cancel.Visible = false;
                link_update.Visible = false;
            }

        }


        lbl_total_month_expense.Text = hp.getTotalMonthExpense(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_expense_reference.Visible = false;


        tbx_electricity_m.Text = "";
        tbx_energy_m.Text = "";
        tbx_insurances_m.Text = "";
        tbx_janitor_m.Text = "";
        tbx_taxes_m.Text = "";
        tbx_maintenance_repair_m.Text = "";
        tbx_school_taxes_m.Text = "";
        tbx_management_m.Text = "";
        tbx_advertising_m.Text = "";
        tbx_legal_m.Text = "";
        tbx_accounting_m.Text = "";
        tbx_other_m.Text = "";

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_taxes_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime paid_date = new DateTime();
        tiger.Date d = new tiger.Date();
        paid_date = Convert.ToDateTime(d.DateCulture(ddl_taxes_date_received_m.SelectedValue, ddl_taxes_date_received_d.SelectedValue, ddl_taxes_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prExpenseAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@expensecateg_id", SqlDbType.Int).Value = 5;
            cmd.Parameters.Add("@expense_date_paid", SqlDbType.DateTime).Value = paid_date;

            if (RegEx.IsMoney(tbx_taxes_m.Text))
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_taxes_m.Text);
            else
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = 0.00;

            if (RegEx.IsText(tbx_ref_taxes.Text))
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_taxes.Text);
            else
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = "";


            if (RegEx.IsText(tbx_com_taxes.Text))
                cmd.Parameters.Add("@expense_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_taxes.Text);
            else
                cmd.Parameters.Add("@expense_comments", SqlDbType.NVarChar, 50).Value = "";



            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }



        //////////////////////////////////////// // to view the expenses in the  home  ////////////////////////////
        DateTime expense_date_paid = new DateTime();
        tiger.Date df = new tiger.Date();
        expense_date_paid = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        gv_Expense.DataSource = hp.getExpenseMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);
        gv_Expense.DataBind();


        // Here we are rendering invisible the hyperlink and the bottuon of the 
        // Work orders ( task ) - i.e : maintenance/repair
        for (int j = 0; j < gv_Expense.Rows.Count; j++)
        {
            Button btn_cancel = (Button)gv_Expense.Rows[j].FindControl("btn_cancel");
            HyperLink link_update = (HyperLink)gv_Expense.Rows[j].FindControl("link_update");
            HiddenField h_task_id = (HiddenField)gv_Expense.Rows[j].FindControl("h_task_id");

            if (Convert.ToInt32(h_task_id.Value) > 0)
            {
                btn_cancel.Visible = false;
                link_update.Visible = false;
            }

        }

        lbl_total_month_expense.Text = hp.getTotalMonthExpense(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_expense_reference.Visible = false;


        tbx_electricity_m.Text = "";
        tbx_energy_m.Text = "";
        tbx_insurances_m.Text = "";
        tbx_janitor_m.Text = "";
        tbx_taxes_m.Text = "";
        tbx_maintenance_repair_m.Text = "";
        tbx_school_taxes_m.Text = "";
        tbx_management_m.Text = "";
        tbx_advertising_m.Text = "";
        tbx_legal_m.Text = "";
        tbx_accounting_m.Text = "";
        tbx_other_m.Text = "";

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_maintenance_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime paid_date = new DateTime();
        tiger.Date d = new tiger.Date();
        paid_date = Convert.ToDateTime(d.DateCulture(ddl_maintenance_date_received_m.SelectedValue, ddl_maintenance_date_received_d.SelectedValue, ddl_maintenance_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prExpenseAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@expensecateg_id", SqlDbType.Int).Value = 6;
            cmd.Parameters.Add("@expense_date_paid", SqlDbType.DateTime).Value = paid_date;

            if (RegEx.IsMoney(tbx_maintenance_repair_m.Text))
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_maintenance_repair_m.Text);
            else
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = 0.00;

            if (RegEx.IsText(tbx_ref_management.Text))
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_management.Text);
            else
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = "";


            if (RegEx.IsText(tbx_com_maintenance.Text))
                cmd.Parameters.Add("@expense_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_maintenance.Text);
            else
                cmd.Parameters.Add("@expense_comments", SqlDbType.NVarChar, 50).Value = "";


            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }


        //////////////////////////////////////// // to view the expenses in the  home  ////////////////////////////
        DateTime expense_date_paid = new DateTime();
        tiger.Date df = new tiger.Date();
        expense_date_paid = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        gv_Expense.DataSource = hp.getExpenseMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);
        gv_Expense.DataBind();


        // Here we are rendering invisible the hyperlink and the bottuon of the 
        // Work orders ( task ) - i.e : maintenance/repair
        for (int j = 0; j < gv_Expense.Rows.Count; j++)
        {
            Button btn_cancel = (Button)gv_Expense.Rows[j].FindControl("btn_cancel");
            HyperLink link_update = (HyperLink)gv_Expense.Rows[j].FindControl("link_update");
            HiddenField h_task_id = (HiddenField)gv_Expense.Rows[j].FindControl("h_task_id");

            if (Convert.ToInt32(h_task_id.Value) > 0)
            {
                btn_cancel.Visible = false;
                link_update.Visible = false;
            }

        }


        lbl_total_month_expense.Text = hp.getTotalMonthExpense(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_expense_reference.Visible = false;


        tbx_electricity_m.Text = "";
        tbx_energy_m.Text = "";
        tbx_insurances_m.Text = "";
        tbx_janitor_m.Text = "";
        tbx_taxes_m.Text = "";
        tbx_maintenance_repair_m.Text = "";
        tbx_school_taxes_m.Text = "";
        tbx_management_m.Text = "";
        tbx_advertising_m.Text = "";
        tbx_legal_m.Text = "";
        tbx_accounting_m.Text = "";
        tbx_other_m.Text = "";

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_school_taxes_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime paid_date = new DateTime();
        tiger.Date d = new tiger.Date();
        paid_date = Convert.ToDateTime(d.DateCulture(ddl_school_taxes_date_received_m.SelectedValue, ddl_school_taxes_date_received_d.SelectedValue, ddl_school_taxes_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prExpenseAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@expensecateg_id", SqlDbType.Int).Value = 7;
            cmd.Parameters.Add("@expense_date_paid", SqlDbType.DateTime).Value = paid_date;


            if (RegEx.IsMoney(tbx_school_taxes_m.Text))
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_school_taxes_m.Text);
            else
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = 0.00;

            if (RegEx.IsText(tbx_ref_school_taxes.Text))
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_school_taxes.Text);
            else
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = "";


            if (RegEx.IsText(tbx_com_school_taxes.Text))
                cmd.Parameters.Add("@expense_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_school_taxes.Text);
            else
                cmd.Parameters.Add("@expense_comments", SqlDbType.NVarChar, 50).Value = "";

            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }


        //////////////////////////////////////// // to view the expenses in the  home  ////////////////////////////
        DateTime expense_date_paid = new DateTime();
        tiger.Date df = new tiger.Date();
        expense_date_paid = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        gv_Expense.DataSource = hp.getExpenseMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);
        gv_Expense.DataBind();


        // Here we are rendering invisible the hyperlink and the bottuon of the 
        // Work orders ( task ) - i.e : maintenance/repair
        for (int j = 0; j < gv_Expense.Rows.Count; j++)
        {
            Button btn_cancel = (Button)gv_Expense.Rows[j].FindControl("btn_cancel");
            HyperLink link_update = (HyperLink)gv_Expense.Rows[j].FindControl("link_update");
            HiddenField h_task_id = (HiddenField)gv_Expense.Rows[j].FindControl("h_task_id");

            if (Convert.ToInt32(h_task_id.Value) > 0)
            {
                btn_cancel.Visible = false;
                link_update.Visible = false;
            }

        }


        lbl_total_month_expense.Text = hp.getTotalMonthExpense(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_expense_reference.Visible = false;


        tbx_electricity_m.Text = "";
        tbx_energy_m.Text = "";
        tbx_insurances_m.Text = "";
        tbx_janitor_m.Text = "";
        tbx_taxes_m.Text = "";
        tbx_maintenance_repair_m.Text = "";
        tbx_school_taxes_m.Text = "";
        tbx_management_m.Text = "";
        tbx_advertising_m.Text = "";
        tbx_legal_m.Text = "";
        tbx_accounting_m.Text = "";
        tbx_other_m.Text = "";

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_management_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime paid_date = new DateTime();
        tiger.Date d = new tiger.Date();
        paid_date = Convert.ToDateTime(d.DateCulture(ddl_management_date_received_m.SelectedValue, ddl_management_date_received_d.SelectedValue, ddl_management_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prExpenseAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@expensecateg_id", SqlDbType.Int).Value = 8;
            cmd.Parameters.Add("@expense_date_paid", SqlDbType.DateTime).Value = paid_date;


            if (RegEx.IsMoney(tbx_management_m.Text))
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_management_m.Text);
            else
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = 0.00;

            if (RegEx.IsText(tbx_ref_management.Text))
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_management.Text);
            else
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = "";


            if (RegEx.IsText(tbx_com_management.Text))
                cmd.Parameters.Add("@expense_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_management.Text);
            else
                cmd.Parameters.Add("@expense_comments", SqlDbType.NVarChar, 50).Value = "";


            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }


        //////////////////////////////////////// // to view the expenses in the  home  ////////////////////////////
        DateTime expense_date_paid = new DateTime();
        tiger.Date df = new tiger.Date();
        expense_date_paid = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        gv_Expense.DataSource = hp.getExpenseMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);
        gv_Expense.DataBind();



        // Here we are rendering invisible the hyperlink and the bottuon of the 
        // Work orders ( task ) - i.e : maintenance/repair
        for (int j = 0; j < gv_Expense.Rows.Count; j++)
        {
            Button btn_cancel = (Button)gv_Expense.Rows[j].FindControl("btn_cancel");
            HyperLink link_update = (HyperLink)gv_Expense.Rows[j].FindControl("link_update");
            HiddenField h_task_id = (HiddenField)gv_Expense.Rows[j].FindControl("h_task_id");

            if (Convert.ToInt32(h_task_id.Value) > 0)
            {
                btn_cancel.Visible = false;
                link_update.Visible = false;
            }

        }

        lbl_total_month_expense.Text = hp.getTotalMonthExpense(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_expense_reference.Visible = false;


        tbx_electricity_m.Text = "";
        tbx_energy_m.Text = "";
        tbx_insurances_m.Text = "";
        tbx_janitor_m.Text = "";
        tbx_taxes_m.Text = "";
        tbx_maintenance_repair_m.Text = "";
        tbx_school_taxes_m.Text = "";
        tbx_management_m.Text = "";
        tbx_advertising_m.Text = "";
        tbx_legal_m.Text = "";
        tbx_accounting_m.Text = "";
        tbx_other_m.Text = "";

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_advertising_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime paid_date = new DateTime();
        tiger.Date d = new tiger.Date();
        paid_date = Convert.ToDateTime(d.DateCulture(ddl_advertising_date_received_m.SelectedValue, ddl_advertising_date_received_d.SelectedValue, ddl_advertising_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prExpenseAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@expensecateg_id", SqlDbType.Int).Value = 9;
            cmd.Parameters.Add("@expense_date_paid", SqlDbType.DateTime).Value = paid_date;

            if (RegEx.IsMoney(tbx_advertising_m.Text))
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_advertising_m.Text);
            else
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = 0.00;

            if (RegEx.IsText(tbx_ref_advertising.Text))
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_advertising.Text);
            else
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = "";


            if (RegEx.IsText(tbx_com_advertising.Text))
                cmd.Parameters.Add("@expense_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_advertising.Text);
            else
                cmd.Parameters.Add("@expense_comments", SqlDbType.NVarChar, 50).Value = "";


            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }


        //////////////////////////////////////// // to view the expenses in the  home  ////////////////////////////
        DateTime expense_date_paid = new DateTime();
        tiger.Date df = new tiger.Date();
        expense_date_paid = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        gv_Expense.DataSource = hp.getExpenseMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);
        gv_Expense.DataBind();


        // Here we are rendering invisible the hyperlink and the bottuon of the 
        // Work orders ( task ) - i.e : maintenance/repair
        for (int j = 0; j < gv_Expense.Rows.Count; j++)
        {
            Button btn_cancel = (Button)gv_Expense.Rows[j].FindControl("btn_cancel");
            HyperLink link_update = (HyperLink)gv_Expense.Rows[j].FindControl("link_update");
            HiddenField h_task_id = (HiddenField)gv_Expense.Rows[j].FindControl("h_task_id");

            if (Convert.ToInt32(h_task_id.Value) > 0)
            {
                btn_cancel.Visible = false;
                link_update.Visible = false;
            }

        }


        lbl_total_month_expense.Text = hp.getTotalMonthExpense(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_expense_reference.Visible = false;


        tbx_electricity_m.Text = "";
        tbx_energy_m.Text = "";
        tbx_insurances_m.Text = "";
        tbx_janitor_m.Text = "";
        tbx_taxes_m.Text = "";
        tbx_maintenance_repair_m.Text = "";
        tbx_school_taxes_m.Text = "";
        tbx_management_m.Text = "";
        tbx_advertising_m.Text = "";
        tbx_legal_m.Text = "";
        tbx_accounting_m.Text = "";
        tbx_other_m.Text = "";

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_legal_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime paid_date = new DateTime();
        tiger.Date d = new tiger.Date();
        paid_date = Convert.ToDateTime(d.DateCulture(ddl_legal_date_received_m.SelectedValue, ddl_legal_date_received_d.SelectedValue, ddl_legal_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prExpenseAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@expensecateg_id", SqlDbType.Int).Value = 10;
            cmd.Parameters.Add("@expense_date_paid", SqlDbType.DateTime).Value = paid_date;


            if (RegEx.IsMoney(tbx_legal_m.Text))
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_legal_m.Text);
            else
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = 0.00;

            if (RegEx.IsText(tbx_ref_legal.Text))
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_legal.Text);
            else
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = "";


            if (RegEx.IsText(tbx_com_legal.Text))
                cmd.Parameters.Add("@expense_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_legal.Text);
            else
                cmd.Parameters.Add("@expense_comments", SqlDbType.NVarChar, 50).Value = "";



            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }


        //////////////////////////////////////// // to view the expenses in the  home  ////////////////////////////
        DateTime expense_date_paid = new DateTime();
        tiger.Date df = new tiger.Date();
        expense_date_paid = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        gv_Expense.DataSource = hp.getExpenseMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);
        gv_Expense.DataBind();


        // Here we are rendering invisible the hyperlink and the bottuon of the 
        // Work orders ( task ) - i.e : maintenance/repair
        for (int j = 0; j < gv_Expense.Rows.Count; j++)
        {
            Button btn_cancel = (Button)gv_Expense.Rows[j].FindControl("btn_cancel");
            HyperLink link_update = (HyperLink)gv_Expense.Rows[j].FindControl("link_update");
            HiddenField h_task_id = (HiddenField)gv_Expense.Rows[j].FindControl("h_task_id");

            if (Convert.ToInt32(h_task_id.Value) > 0)
            {
                btn_cancel.Visible = false;
                link_update.Visible = false;
            }

        }


        lbl_total_month_expense.Text = hp.getTotalMonthExpense(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_expense_reference.Visible = false;


        tbx_electricity_m.Text = "";
        tbx_energy_m.Text = "";
        tbx_insurances_m.Text = "";
        tbx_janitor_m.Text = "";
        tbx_taxes_m.Text = "";
        tbx_maintenance_repair_m.Text = "";
        tbx_school_taxes_m.Text = "";
        tbx_management_m.Text = "";
        tbx_advertising_m.Text = "";
        tbx_legal_m.Text = "";
        tbx_accounting_m.Text = "";
        tbx_other_m.Text = "";

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_accounting_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime paid_date = new DateTime();
        tiger.Date d = new tiger.Date();
        paid_date = Convert.ToDateTime(d.DateCulture(ddl_accounting_date_received_m.SelectedValue, ddl_accounting_date_received_d.SelectedValue, ddl_accounting_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prExpenseAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@expensecateg_id", SqlDbType.Int).Value = 11;
            cmd.Parameters.Add("@expense_date_paid", SqlDbType.DateTime).Value = paid_date;


            if (RegEx.IsMoney(tbx_accounting_m.Text))
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_accounting_m.Text);
            else
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = 0.00;

            if (RegEx.IsText(tbx_ref_accounting.Text))
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_accounting.Text);
            else
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = "";


            if (RegEx.IsText(tbx_com_accounting.Text))
                cmd.Parameters.Add("@expense_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_accounting.Text);
            else
                cmd.Parameters.Add("@expense_comments", SqlDbType.NVarChar, 50).Value = "";


            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }


        //////////////////////////////////////// // to view the expenses in the  home  ////////////////////////////
        DateTime expense_date_paid = new DateTime();
        tiger.Date df = new tiger.Date();
        expense_date_paid = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        gv_Expense.DataSource = hp.getExpenseMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);
        gv_Expense.DataBind();


        // Here we are rendering invisible the hyperlink and the bottuon of the 
        // Work orders ( task ) - i.e : maintenance/repair
        for (int j = 0; j < gv_Expense.Rows.Count; j++)
        {
            Button btn_cancel = (Button)gv_Expense.Rows[j].FindControl("btn_cancel");
            HyperLink link_update = (HyperLink)gv_Expense.Rows[j].FindControl("link_update");
            HiddenField h_task_id = (HiddenField)gv_Expense.Rows[j].FindControl("h_task_id");

            if (Convert.ToInt32(h_task_id.Value) > 0)
            {
                btn_cancel.Visible = false;
                link_update.Visible = false;
            }

        }


        lbl_total_month_expense.Text = hp.getTotalMonthExpense(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_expense_reference.Visible = false;


        tbx_electricity_m.Text = "";
        tbx_energy_m.Text = "";
        tbx_insurances_m.Text = "";
        tbx_janitor_m.Text = "";
        tbx_taxes_m.Text = "";
        tbx_maintenance_repair_m.Text = "";
        tbx_school_taxes_m.Text = "";
        tbx_management_m.Text = "";
        tbx_advertising_m.Text = "";
        tbx_legal_m.Text = "";
        tbx_accounting_m.Text = "";
        tbx_other_m.Text = "";

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_other_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime paid_date = new DateTime();
        tiger.Date d = new tiger.Date();
        paid_date = Convert.ToDateTime(d.DateCulture(ddl_other_date_received_m.SelectedValue, ddl_other_date_received_d.SelectedValue, ddl_other_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prExpenseAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@expensecateg_id", SqlDbType.Int).Value = 12;
            cmd.Parameters.Add("@expense_date_paid", SqlDbType.DateTime).Value = paid_date;


            if (RegEx.IsMoney(tbx_other_m.Text))
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_other_m.Text);
            else
                cmd.Parameters.Add("@expense_amount", SqlDbType.Money).Value = 0.00;

            if (RegEx.IsText(tbx_ref_other.Text))
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_other.Text);
            else
                cmd.Parameters.Add("@expense_reference", SqlDbType.NVarChar, 50).Value = "";


            if (RegEx.IsText(tbx_com_other.Text))
                cmd.Parameters.Add("@expense_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_other.Text);
            else
                cmd.Parameters.Add("@expense_comments", SqlDbType.NVarChar, 50).Value = "";


            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }


        //////////////////////////////////////// // to view the expenses in the  home  ////////////////////////////
        DateTime expense_date_paid = new DateTime();
        tiger.Date df = new tiger.Date();
        expense_date_paid = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        gv_Expense.DataSource = hp.getExpenseMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);
        gv_Expense.DataBind();


        // Here we are rendering invisible the hyperlink and the bottuon of the 
        // Work orders ( task ) - i.e : maintenance/repair
        for (int j = 0; j < gv_Expense.Rows.Count; j++)
        {
            Button btn_cancel = (Button)gv_Expense.Rows[j].FindControl("btn_cancel");
            HyperLink link_update = (HyperLink)gv_Expense.Rows[j].FindControl("link_update");
            HiddenField h_task_id = (HiddenField)gv_Expense.Rows[j].FindControl("h_task_id");

            if (Convert.ToInt32(h_task_id.Value) > 0)
            {
                btn_cancel.Visible = false;
                link_update.Visible = false;
            }

        }


        lbl_total_month_expense.Text = hp.getTotalMonthExpense(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_expense_reference.Visible = false;

        tbx_electricity_m.Text = "";
        tbx_energy_m.Text = "";
        tbx_insurances_m.Text = "";
        tbx_janitor_m.Text = "";
        tbx_taxes_m.Text = "";
        tbx_maintenance_repair_m.Text = "";
        tbx_school_taxes_m.Text = "";
        tbx_management_m.Text = "";
        tbx_advertising_m.Text = "";
        tbx_legal_m.Text = "";
        tbx_accounting_m.Text = "";
        tbx_other_m.Text = "";


    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetExpenseCateg(int expensecateg_id)
    {
        string expensecateg = "";

        switch (expensecateg_id)
        {
            case 1:
                expensecateg = Resources.Resource.lbl_electricity;
                break;
            case 2:
                expensecateg = Resources.Resource.lbl_energy;
                break ;
            case 3:
                expensecateg = Resources.Resource.lbl_insurances;
                break;

            case 4:
                expensecateg = Resources.Resource.lbl_janitor;
                break;
            case 5:
                expensecateg = Resources.Resource.lbl_taxes;
                break;
            case 6:
                expensecateg = Resources.Resource.lbl_maintenance_repair;
                break;


            case 7:
                expensecateg = Resources.Resource.lbl_school_taxes;
                break;
            case 8:
                expensecateg = Resources.Resource.lbl_management;
                break;
            case 9:
                expensecateg = Resources.Resource.lbl_advertising;
                break;



            case 10:
                expensecateg = Resources.Resource.lbl_legal;
                break;
            case 11:
                expensecateg = Resources.Resource.lbl_accounting;
                break;
            case 12:
                expensecateg = Resources.Resource.lbl_other;
                break;
           
        }

        return expensecateg;
    }

    protected void btn_view_graph_Click(object sender, EventArgs e)
    {
        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_expense_reference.Visible = false;
        
        Response.Redirect("financial_expenses_graph.aspx?h_id="+ddl_home_id.SelectedValue+"&m="+ddl_date_received_m.SelectedValue
                           +"&d=1"+"&y="+ddl_date_received_y.SelectedValue);
    }



    protected void btn_cancel_Click(object sender, EventArgs e)
    {



        //*********************************************************************
        //*********************************************************************
         Button btn_cancel = (Button)sender;
         GridViewRow grdRow = (GridViewRow)btn_cancel.Parent.Parent;

       //  string strField1 = grdRow.Cells[6].Text;

         HiddenField h_expense_id = (HiddenField)grdRow.Cells[6].FindControl("h_expense_id");
         HiddenField h_expense_date_paid = (HiddenField)grdRow.Cells[6].FindControl("h_expense_date_paid");
         HiddenField h_expense_amount = (HiddenField)grdRow.Cells[6].FindControl("h_expense_amount");
         HiddenField h_expense_reference = (HiddenField)grdRow.Cells[6].FindControl("h_expense_reference");
         HiddenField h_expense_categ = (HiddenField)grdRow.Cells[6].FindControl("h_expense_categ");


         lbl_confirmation.Text = Resources.Resource.lbl_cancel_confirmation;

         Label57.Text = Resources.Resource.txt_date;
         Label7.Text = ":";
         Label56.Text = ":";
         Label58.Text = ":";
         Label59.Text = Resources.Resource.lbl_reference;
         Label60.Text = ":";
         Label53.Text = h_expense_categ.Value;

         Label55.Text = Resources.Resource.lbl_amount;



         lbl_amount.Text = h_expense_amount.Value;
         lbl_date_paid.Text = h_expense_date_paid.Value;
         lbl_expense_reference.Text = h_expense_reference.Value;



         lbl_confirmation.Visible = true;

         Label57.Visible = true;
         Label58.Visible = true;
         Label59.Visible = true;
         Label60.Visible = true;
         Label7.Visible = true;
         Label56.Visible = true;
         Label53.Visible = true;
         Label55.Visible = true;



         lbl_amount.Visible = true;
         lbl_date_paid.Visible = true;
         lbl_expense_reference.Visible = true;

       //------------------------------------------------------------------------------------

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prCancelExpense", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@expense_id", SqlDbType.Int).Value = Convert.ToInt32(h_expense_id.Value);

            //execute the insert
            cmd.ExecuteReader();

        }
        catch (Exception error)
        {
            //  tiger.security.Error.errorAdd(conn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
        }

        // to view the expenses in the  home
        //////////////////////////////////////// // to view the expenses in the  home  ////////////////////////////
        DateTime expense_date_paid = new DateTime();
        tiger.Date df = new tiger.Date();
        expense_date_paid = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        gv_Expense.DataSource = hp.getExpenseMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), expense_date_paid);
        gv_Expense.DataBind();



        // Here we are rendering invisible the hyperlink and the bottuon of the 
        // Work orders ( task ) - i.e : maintenance/repair
        for (int j = 0; j < gv_Expense.Rows.Count; j++)
        {
            Button btn_cancel2 = (Button)gv_Expense.Rows[j].FindControl("btn_cancel");
            HyperLink link_update = (HyperLink)gv_Expense.Rows[j].FindControl("link_update");
            HiddenField h_task_id = (HiddenField)gv_Expense.Rows[j].FindControl("h_task_id");

            if (Convert.ToInt32(h_task_id.Value) > 0)
            {
                btn_cancel2.Visible = false;
                link_update.Visible = false;
            }

        }
        lbl_total_month_expense.Text = hp.getTotalMonthExpense(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]),expense_date_paid);




    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="expense_id"></param>
    /// <param name="task_id"></param>
    /// <returns></returns>
    protected string Get_Link(string expense_id, string task_id)
    {

        string link = "";

        if (Convert.ToInt32(expense_id)> 0)
        {
            link = "financial_expenses_update.aspx?exp_id=" + expense_id;
        }

      

        return link;
    }


    protected string Get_Comments(string home_id, string wo_id ,string expense_comments, 
                                  string task_id, string wo_title, int wo_status,
                                  DateTime wo_date_begin, DateTime wo_date_end, int task_status,
                                  DateTime task_date_begin,DateTime task_date_end, string task_title)
    {

        string comments = expense_comments;

        if (Convert.ToInt32(task_id) > 0) 

        { 
            
            
            comments = "<b><u>" + Resources.Resource.lbl_work_order + "</u></b><br/>";
            comments = comments +"<A HREF='/user/pm/workorder/wo_update.aspx?h_id="+home_id+"&wo_id="+wo_id+"'>"+ wo_title + "</A><br/>";
            comments = comments +"<b>"+ Resources.Resource.lbl_status + "</b> : " + GetStatus(Convert.ToInt32(wo_status))+"<br/>";
            comments = comments + "<b>" + Resources.Resource.lbl_date_begin + "</b> : " + wo_date_begin.ToLongDateString() + "<br/>";
           
            if (wo_date_end == Convert.ToDateTime("1/1/1900") || wo_status < 3)
            {
                comments = comments + "<b>" + Resources.Resource.lbl_date_end + "</b> : " + "-" + "<br/><br/>";           
            } 
            else
                comments = comments + "<b>" + Resources.Resource.lbl_date_end + "</b> : " + wo_date_end.ToLongDateString() + "<br/><br/>";
            
            comments = comments + "<b><u>" + Resources.Resource.lbl_task + "</u></b><br/>" ;
            comments = comments + task_title + "<br/>";
            comments = comments + "<b>" + Resources.Resource.lbl_status + "</b> : " + GetStatus(Convert.ToInt32(task_status)) + "<br/>";
            comments = comments + "<b>" + Resources.Resource.lbl_date_begin + "</b> : " + task_date_begin.ToLongDateString() + "<br/>";

            comments = comments + "<b>" + Resources.Resource.lbl_date_end + "</b> : " + task_date_end.ToLongDateString() + "<br/><br/>";
            
        
        }

        return comments;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetStatus(int status_id)
    {
        string status = "";

        switch (status_id)
        {
            case 1:
                status = Resources.Resource.lbl_pending;
                break;
            case 2:
                status = Resources.Resource.lbl_in_progress;
                break;
            case 3:
                status = Resources.Resource.lbl_completed;
                break;

            case 4:
                status = Resources.Resource.lbl_closed;
                break;
        }

        return status;
    }


   


    protected void btn_fill_expense_Click(object sender, EventArgs e)
    {



        tbx_electricity_m.Text = "";
        tbx_energy_m.Text = "";
        tbx_insurances_m.Text = "";
        tbx_janitor_m.Text = "";
        tbx_taxes_m.Text = "";
        tbx_maintenance_repair_m.Text = "";
        tbx_school_taxes_m.Text = "";
        tbx_management_m.Text = "";
        tbx_advertising_m.Text = "";
        tbx_legal_m.Text = "";
        tbx_accounting_m.Text = "";
        tbx_other_m.Text = "";

        DateTime date = new DateTime();

        tiger.Date d = new tiger.Date();
        date = Convert.ToDateTime(d.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        date = date.AddMonths(-1);
        date = Convert.ToDateTime(d.DateCulture(date.Month.ToString(), "1", date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prExpenseMonthView", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        DataTable dt = new DataTable();
            
        try
        {
            conn.Open();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@expense_date_paid", SqlDbType.DateTime).Value = date;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);
           
        }
        finally
        {
            conn.Close();
        }

       double amount = 0.00;
       string categ;


       double electricity = 0.00;
       double energy = 0.00;
       double insurances = 0.00;
       double janitor = 0.00;
       double taxes = 0.00;
       double maintenance_repair = 0.00;
       double school_taxes = 0.00;
       double management = 0.00;
       double advertising = 0.00;
       double legal = 0.00;
       double accounting = 0.00;
       double other = 0.00;

         for (int i = 0; i < dt.Rows.Count; i++)
          {
                DataRow row = dt.Rows[i];

                categ = row[3].ToString();
                amount = Convert.ToDouble(row[5]);
                 
                switch (categ)
                {

                    case "1": electricity = electricity + amount;
                        tbx_electricity_m.Text = String.Format("{0:0.00}", electricity);
                        break;
                    case "2": energy = energy + amount;
                        tbx_energy_m.Text = String.Format("{0:0.00}", energy);
                        break;
                    case "3": insurances = insurances + amount;
                        tbx_insurances_m.Text = String.Format("{0:0.00}", insurances);
                        break;

                    case "4": janitor = janitor + amount;
                        tbx_janitor_m.Text = String.Format("{0:0.00}", janitor);
                        break;

                    case "5": taxes = taxes + amount;
                        tbx_taxes_m.Text = String.Format("{0:0.00}", taxes);
                        break;

                    case "6": maintenance_repair = maintenance_repair + amount;
                        tbx_maintenance_repair_m.Text = String.Format("{0:0.00}", maintenance_repair);
                        break;

                    case "7": school_taxes = school_taxes + amount;
                        tbx_school_taxes_m.Text = String.Format("{0:0.00}", school_taxes);
                        break;

                    case "8": management = management + amount;
                        tbx_management_m.Text = String.Format("{0:0.00}", management);
                        break;

                    case "9": advertising = advertising + amount;
                        tbx_advertising_m.Text = String.Format("{0:0.00}", advertising);
                        break;

                    case "10": legal = legal + amount;
                        tbx_legal_m.Text = String.Format("{0:0.00}", legal);
                        break;

                    case "11": accounting = accounting + amount;
                        tbx_accounting_m.Text = String.Format("{0:0.00}", accounting);
                        break;

                    case "12": other = other + amount;
                        tbx_other_m.Text = String.Format("{0:0.00}", other);
                        break;
                }
           

        }


    }
    protected void link_expense_Click(object sender, EventArgs e)
    {
        Panel1.Visible = true;
    }
}
