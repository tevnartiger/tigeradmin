﻿<%@ Page Language="C#" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" CodeFile="appliance_moving_wiz.aspx.cs" Inherits="manager_appliance_appliance_moving_wiz" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<br />
<b><asp:Label ID="Label3" runat="server"  Text="<%$ Resources:Resource,lbl_u_move_appliance %>"></asp:Label></b>

<br /><br />


<asp:Label ID="Label1" runat="server"  Text="<%$ Resources:Resource,lbl_appliance_wiz_step1 %>"></asp:Label><br />
<asp:Label ID="Label2" runat="server"  Text="<%$ Resources:Resource,lbl_appliance_wiz_step2 %>"></asp:Label><br />
<asp:Label ID="Label4" runat="server"  Text="<%$ Resources:Resource,lbl_appliance_wiz_step3 %>"></asp:Label> <br /><br />





    <br />


<asp:Label ID="Label5" runat="server"  Text="<%$ Resources:Resource,lbl_appliance_wiz_step0 %>"></asp:Label><br /><br />
    <asp:RadioButtonList ID="radio_destination" runat="server" 
                                   RepeatDirection="Vertical">
                       <asp:ListItem  Text="<%$ Resources:Resource,lbl_property %>" Selected="True" Value="1"></asp:ListItem>
                         <asp:ListItem  Text="<%$ Resources:Resource,lbl_external_storage %>" Value="2"></asp:ListItem>
      </asp:RadioButtonList>
      <br />
        <table bgcolor="aliceblue" style="width: 80%;">
            <tr>
                <td align="right">
                    <asp:Button ID="btn_next" runat="server" onclick="btn_next_Click" 
                        Text="<%$ Resources:Resource,lbl_goto_step1 %>" />
                </td>
            </tr>
        </table>
        <br />

</asp:Content>

