﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : november 20 , 2008
/// </summary>

public partial class manager_appliance_appliance_moving_wiz_warehouse : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!(Page.IsPostBack))
        {
            SetDefaultView();
            // default moving date is NOW
            DateTime date = new DateTime();
            date = DateTime.Now;


            ddl_ua_dateadd_m2.SelectedValue = date.Month.ToString();
            ddl_ua_dateadd_d2.SelectedValue = date.Day.ToString();
            ddl_ua_dateadd_y2.SelectedValue = date.Year.ToString();

            // SetDefaultView();
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            //get the list of warehouse 
            tiger.Warehouse warehouse = new tiger.Warehouse(strconn);
            ddl_warehouse_list3.DataSource = warehouse.getWarehouseList(Convert.ToInt32(Session["schema_id"]));
            ddl_warehouse_list3.DataBind();
            ddl_warehouse_list3.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
            ddl_warehouse_list3.SelectedIndex = 0;


            ddl_warehouse_list4.DataSource = warehouse.getWarehouseList(Convert.ToInt32(Session["schema_id"]));
            ddl_warehouse_list4.DataBind();
            ddl_warehouse_list4.Items.Insert(0, new ListItem(Resources.Resource.lbl_select, "-1"));
            ddl_warehouse_list4.SelectedIndex = 0;


            //get the list of appliance category
            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prApplianceCategList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                //    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                ddl_warehouse_appliance_categ3.DataSource = ds;


                if (Session["_lastCulture"].ToString() == "fr-FR")
                {
                    ddl_warehouse_appliance_categ3.DataTextField = "ac_name_fr";
                }
                if (Session["_lastCulture"].ToString() == "en-US")
                {
                    ddl_warehouse_appliance_categ3.DataTextField = "ac_name_en";

                }

                ddl_warehouse_appliance_categ3.DataBind();



            }
            finally
            {
                conn.Close();
            }

            String referer = "";
          //  referer = Request.UrlReferrer.ToString();



            DateTime right_now = new DateTime();
            right_now = DateTime.Now;

            tiger.Date d = new tiger.Date();

            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


            tiger.PM l = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getPMHomeCount(Convert.ToInt32(Session["schema_id"]),Convert.ToInt32(Session["name_id"]));

            int home_id = l.getPMHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

            // first we check if there's any property available
            if (home_count > 0)
            {

                tiger.PM h = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

                ddl_home_list3.DataSource = h.getPMHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                ddl_home_list3.DataBind();

                // DropDownList pour les Unit
                //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

                if (unit_count > 0)
                {

                    int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), home_id);

                    // unit from the property origin
                  
                    // unit from the property we we want to move the appliances to
                    // ... originated from a warehouse
                    ddl_unit_id3.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
                    ddl_unit_id3.DataBind();
                    ddl_unit_id3.Items.Add(new ListItem(Resources.Resource.lbl_storage_unit, "-1"));

                }

            }

            else
            {
                // panel_home_unit_add.Visible = false;
             //   txt_message.InnerHtml = "<b><a href='home/home_add.aspx'>Please add a property</a></b>";
            }


            tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            gv_warehouse_appliance_list3.DataSource = app.getWarehouseApplianceList(Convert.ToInt32(Session["schema_id"]), 0, 1);
            gv_warehouse_appliance_list3.DataBind();

        }


    }

    /// <summary>
    /// 
    /// </summary>
    private void SetDefaultView()
    {
        MultiView1.ActiveViewIndex = 0;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_warehouse_appliance_list3_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_warehouse_appliance_list3.Visible = true;
        gv_warehouse_appliance_search_list3.Visible = false;

        tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        gv_warehouse_appliance_list3.PageIndex = e.NewPageIndex;
        gv_warehouse_appliance_list3.DataSource = app.getWarehouseApplianceList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_warehouse_list3.SelectedValue), Convert.ToInt32(ddl_warehouse_appliance_categ3.SelectedValue));
        gv_warehouse_appliance_list3.DataBind();

        MultiView1.ActiveViewIndex = 0;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_warehouse_appliance_search_list3_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        gv_warehouse_appliance_list3.Visible = false;
        gv_warehouse_appliance_search_list3.Visible = true;

        tiger.Appliance appliance = new tiger.Appliance(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_warehouse_appliance_search_list3.PageIndex = e.NewPageIndex;
        gv_warehouse_appliance_search_list3.DataSource = appliance.getWarehouseApplianceSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(radio_warehouse_search3.SelectedValue), RegEx.getText(tbx_warehouse_search3.Text));
        gv_warehouse_appliance_search_list3.DataBind();

        MultiView1.ActiveViewIndex = 0;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_warehouse_list3_SelectedIndexChanged(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;

        gv_warehouse_appliance_list3.Visible = true;
        gv_warehouse_appliance_search_list3.Visible = false;

        tiger.Appliance app = new tiger.Appliance(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_warehouse_appliance_list3.DataSource = app.getWarehouseApplianceList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_warehouse_list3.SelectedValue), Convert.ToInt32(ddl_warehouse_appliance_categ3.SelectedValue));
        gv_warehouse_appliance_list3.DataBind();


    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_warehouse_appliance_categ3_SelectedIndexChanged(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;

        gv_warehouse_appliance_list3.Visible = true;
        gv_warehouse_appliance_search_list3.Visible = false;

        tiger.Appliance app = new tiger.Appliance(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_warehouse_appliance_list3.DataSource = app.getWarehouseApplianceList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_warehouse_list3.SelectedValue), Convert.ToInt32(ddl_warehouse_appliance_categ3.SelectedValue));
        gv_warehouse_appliance_list3.DataBind();
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_warehouse_submit_Click(object sender, EventArgs e)
    {
        gv_warehouse_appliance_list3.Visible = false;
        gv_warehouse_appliance_search_list3.Visible = true;

        tiger.Appliance appliance = new tiger.Appliance(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        //  gv_warehouse_appliance_search_list3.PageIndex = e.NewPageIndex;
        gv_warehouse_appliance_search_list3.DataSource = appliance.getWarehouseApplianceSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(radio_warehouse_search3.SelectedValue), RegEx.getText(tbx_warehouse_search3.Text));
        gv_warehouse_appliance_search_list3.DataBind();


        MultiView1.ActiveViewIndex = 0;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_move2_Click(object sender, EventArgs e)
    {

        int number_of_insert = 0;
        string appliance_id = "";
        string ua_id = "";

        // here we are counting the number of row of the GridView

        // gets all the appliance to be moved for warehouse appliance_search_list
        if (gv_warehouse_appliance_search_list3.Visible = true)
        {
            for (int j = 0; j < gv_warehouse_appliance_search_list3.Rows.Count; j++)
            {

                CheckBox chk_process_1 = (CheckBox)gv_warehouse_appliance_search_list3.Rows[j].FindControl("chk_process_1");
                // if the checkbox is checked the process
                if (chk_process_1.Checked)
                {
                    HiddenField h_appliance_id = (HiddenField)gv_warehouse_appliance_search_list3.Rows[j].FindControl("h_appliance_id");
                    HiddenField h_ua_id = (HiddenField)gv_warehouse_appliance_search_list3.Rows[j].FindControl("h_ua_id");

                    appliance_id = appliance_id + h_appliance_id.Value + "|";
                    ua_id = ua_id + h_ua_id.Value + "|";

                    number_of_insert++;

                }
            }
        }
        // gets all the appliance to be moved

        if (gv_warehouse_appliance_list3.Visible = true)
        {
            for (int j = 0; j < gv_warehouse_appliance_list3.Rows.Count; j++)
            {

                CheckBox chk_process = (CheckBox)gv_warehouse_appliance_list3.Rows[j].FindControl("chk_process");
                // if the checkbox is checked the process
                if (chk_process.Checked)
                {
                    HiddenField h_appliance_id = (HiddenField)gv_warehouse_appliance_list3.Rows[j].FindControl("h_appliance_id");
                    HiddenField h_ua_id = (HiddenField)gv_warehouse_appliance_list3.Rows[j].FindControl("h_ua_id");

                    appliance_id = appliance_id + h_appliance_id.Value + "|";
                    ua_id = ua_id + h_ua_id.Value + "|";

                    number_of_insert++;

                }
            }
        }


    string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

    //////////
    ////////// SECURITY CHECK BEGIN //////////////
    /////////
     AccountObjectAuthorization applianceAuthorization = new AccountObjectAuthorization(strconn);

     if (applianceAuthorization.ApplianceBatch(Convert.ToInt32(Session["schema_id"]), appliance_id , ua_id , number_of_insert))
     { }
     else
     {
         Session.Abandon();
         Response.Redirect("~/login.aspx");
     }
     //////////
     ////////// SECURITY CHECK END //////////////
     /////////

        if ((ua_id != "" || number_of_insert > 0) && btn_move2.Visible==true &&
            (radio_destination.SelectedValue == "1" || (radio_destination.SelectedValue == "2" && ddl_warehouse_list4.SelectedIndex > 0))) //
        {
            //Here we send the values to the stored procedure

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prWarehouseApplianceMovingBatch", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //  try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
 

                // we replace the commas  by dots because SQL "CAST"
                // we cast the string "100,00" in money

                cmd.Parameters.Add("@appliance_id", SqlDbType.VarChar, 80000).Value = appliance_id;
                cmd.Parameters.Add("@ua_id", SqlDbType.VarChar, 80000).Value = ua_id;
                cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;


                if (radio_destination.SelectedValue == "1")
                {
                    cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_list3.SelectedValue);
                    cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_unit_id3.SelectedValue);
                    cmd.Parameters.Add("@warehouse_id", SqlDbType.Int).Value = DBNull.Value;
                }


                if (radio_destination.SelectedValue == "2")
                {
                    cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = DBNull.Value;
                    cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = DBNull.Value;
                    cmd.Parameters.Add("@warehouse_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_warehouse_list4.SelectedValue);
                }

                DateTime date_add = new DateTime();
                tiger.Date df = new tiger.Date();

                date_add = Convert.ToDateTime(df.DateCulture(ddl_ua_dateadd_m2.SelectedValue, ddl_ua_dateadd_d2.SelectedValue, ddl_ua_dateadd_y2.SelectedValue, Convert.ToString(Session["_lastCulture"])));
                cmd.Parameters.Add("@ua_date_add", SqlDbType.SmallDateTime).Value = date_add;

                //execute the insert
                cmd.ExecuteReader();

            }
            //   finally
            {
                conn.Close();
            }


         

            if (Convert.ToInt32(cmd.Parameters["@return_id"].Value) == 0)
            {
                btn_move2.Visible = false;
                tb_previous.Visible = false;
                lbl_moving_success.Text = Resources.Resource.lbl_appliance_move_success;
            }
            else
            {
                lbl_moving_success.Text = Resources.Resource.lbl_appliance_move_unsuccess;
            }
        }
     


        MultiView1.ActiveViewIndex = 2;
       
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_list3_SelectedIndexChanged(object sender, EventArgs e)
    {
        int home_id = 0;
        // int unit_id = 0;

        MultiView1.ActiveViewIndex = 2;

        home_id = Convert.ToInt32(ddl_home_list3.SelectedValue);


        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ddl_unit_id3.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
        ddl_unit_id3.DataBind();
        ddl_unit_id3.Items.Add(new ListItem(Resources.Resource.lbl_storage_unit, "-1"));

        ddl_unit_id3.SelectedIndex = 0;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_next_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_next2_Click(object sender, EventArgs e)
    {

        lbl_warehouse_origin.Visible = true;
        lbl_origine.Visible = true;
        Label6.Visible = true;  //texte : Orinin
        Label16.Visible = true ; // texte : Warehouse
        lbl_colon.Visible = true;

   
        MultiView1.ActiveViewIndex = 2;
        if (radio_destination.SelectedValue == "1")
        {
            row_property.Visible = true;
            row_unit.Visible = true;
            row_warehouse.Visible = false;

        }

        if (radio_destination.SelectedValue == "2")
        {
            row_property.Visible = false;
            row_unit.Visible = false;
            row_warehouse.Visible = true;
        }

        lbl_warehouse_origin.Text = ddl_warehouse_list3.SelectedItem.ToString();

        if (ddl_warehouse_list3.SelectedIndex == 0)
        {
            lbl_warehouse_origin.Visible = false;
            lbl_origine.Visible = false;
            Label6.Visible = false;
            Label16.Visible = false;
            lbl_colon.Visible = false;
            
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_previous2_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_previous3_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
    }

}
