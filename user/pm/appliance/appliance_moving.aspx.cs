﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;




/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : may 31 , 2008
/// </summary>
public partial class manager_appliance_appliance_moving : BasePage
{


    protected void Page_Load(object sender, EventArgs e)
    {

        if (!(Page.IsPostBack))
        {
            reg_appliance_search.ValidationExpression = RegEx.getAlphaNumeric();
            reg_warehouse_search3.ValidationExpression = RegEx.getAlphaNumeric();
           
            SetDefaultView();
            // default moving date is NOW
            DateTime date = new DateTime();
            date = DateTime.Now;

            ddl_ua_dateadd_m.SelectedValue = date.Month.ToString();
            ddl_ua_dateadd_d.SelectedValue = date.Day.ToString();
            ddl_ua_dateadd_y.SelectedValue = date.Year.ToString();

            ddl_ua_dateadd_m2.SelectedValue = date.Month.ToString();
            ddl_ua_dateadd_d2.SelectedValue = date.Day.ToString();
            ddl_ua_dateadd_y2.SelectedValue = date.Year.ToString();

           // SetDefaultView();
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            //get the list of warehouse 
            tiger.Warehouse warehouse = new tiger.Warehouse(strconn);
            ddl_warehouse_list2.DataSource = warehouse.getWarehouseList(Convert.ToInt32(Session["schema_id"]));
            ddl_warehouse_list2.DataBind();
            ddl_warehouse_list2.Items.Insert(0, new ListItem(Resources.Resource.lbl_select, "-1"));
            ddl_warehouse_list2.SelectedIndex = 0;


            ddl_warehouse_list3.DataSource = ddl_warehouse_list2.DataSource;
            ddl_warehouse_list3.DataBind();
            ddl_warehouse_list3.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
            ddl_warehouse_list3.SelectedIndex = 0;


            ddl_warehouse_list4.DataSource = warehouse.getWarehouseList(Convert.ToInt32(Session["schema_id"]));
            ddl_warehouse_list4.DataBind();
            ddl_warehouse_list4.Items.Insert(0, new ListItem(Resources.Resource.lbl_select, "-1"));
            ddl_warehouse_list4.SelectedIndex = 0;


            //by default we have to make the choice to select either a property or a ware house
            ddl_warehouse_list2.Enabled = false;

            //by default we put new appliance in property therefore the property checkbox is selected
            chk_home.Checked = true;

            //by default we put new appliance in property therefore the property checkbox is selected
            chk_home3.Checked = true;

            //by default we put new appliance in property therefore the property checkbox is selected
            ddl_warehouse_list4.Enabled = false;


            //get the list of appliance category
            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prApplianceCategList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                //    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                ddl_appliance_categ.DataSource = ds;
                ddl_warehouse_appliance_categ3.DataSource = ds;


                if (Session["_lastCulture"].ToString() == "fr-FR")
                {
                    ddl_appliance_categ.DataTextField = "ac_name_fr";
                    ddl_warehouse_appliance_categ3.DataTextField = "ac_name_fr";
                }
                if (Session["_lastCulture"].ToString() == "en-US")
                {
                    ddl_appliance_categ.DataTextField = "ac_name_en";
                    ddl_warehouse_appliance_categ3.DataTextField = "ac_name_en";
                

                }

                ddl_appliance_categ.DataBind();
                ddl_warehouse_appliance_categ3.DataBind();
              
             

            }
            finally
            {
                conn.Close();
            }

            



            DateTime right_now = new DateTime();
            right_now = DateTime.Now;

            tiger.Date d = new tiger.Date();

            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


            tiger.PM l = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getPMHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

            int home_id = l.getPMHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

            // first we check if there's any property available
            if (home_count > 0)
            {



                tiger.PM h = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ddl_home_list.DataSource = h.getPMHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                ddl_home_list.DataBind();
                ddl_home_list.Items.Insert(0, Resources.Resource.lbl_all);
                ddl_home_list.SelectedValue = home_id.ToString();

                ddl_home_list2.DataSource = ddl_home_list.DataSource;
                ddl_home_list2.DataBind();

                ddl_home_list3.DataSource = ddl_home_list.DataSource;
                ddl_home_list3.DataBind();


               // ddl_home_id2.Enabled = false;

                // DropDownList pour les Unit
                //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

                if (unit_count > 0)
                {

                    int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), home_id);
                    
                    // unit from the property origin
                    ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
                    ddl_unit_id.SelectedValue = Convert.ToString(unit_id);
                    ddl_unit_id.DataBind();
                    ddl_unit_id.Items.Insert(0, Resources.Resource.lbl_all);
                    ddl_unit_id.Items.Add(new ListItem(Resources.Resource.lbl_storage_unit, "-1"));
                    ddl_unit_id.SelectedIndex = 0;

                    // unitt from the property we we want to move the appliances to
                    ddl_unit_id2.DataSource = ddl_unit_id.DataSource;
                    ddl_unit_id2.DataBind();
                    ddl_unit_id2.Items.Add(new ListItem(Resources.Resource.lbl_storage_unit, "-1"));


                    // unitt from the property we we want to move the appliances to
                    // ... originated from a warehouse
                    ddl_unit_id3.DataSource = ddl_unit_id.DataSource;
                    ddl_unit_id3.DataBind();
                    ddl_unit_id3.Items.Add(new ListItem(Resources.Resource.lbl_storage_unit, "-1"));

                }

            }

            else
            {
                // panel_home_unit_add.Visible = false;
                txt_message.InnerHtml = "<b><a href='home/home_add.aspx'>Please add a property</a></b>";
            }


            tiger.PM app = new tiger.PM(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            gv_appliance_list.DataSource = app.getPMApplianceList(Convert.ToInt32(Session["schema_id"]), home_id, 0, 1, Convert.ToInt32(Session["name_id"]));
            gv_appliance_list.DataBind();


            tiger.Appliance ware = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            gv_warehouse_appliance_list3.DataSource = ware.getWarehouseApplianceList(Convert.ToInt32(Session["schema_id"]), 0, 1);
            gv_warehouse_appliance_list3.DataBind();


        }


    }

    private void SetDefaultView()
    {
        //TabContainer1.ActiveTabIndex = 0;
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_list_SelectedIndexChanged(object sender, EventArgs e)
    {

        gv_appliance_list.Visible = true;
        gv_appliance_search_list.Visible = false;

        int home_id = 0;
        // int unit_id = 0;

        if (ddl_home_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        //    if (ddl_unit_id.SelectedIndex > 0)
        //    unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        ddl_appliance_categ.SelectedIndex = 0;

        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
        ddl_unit_id.DataBind();
        ddl_unit_id.Items.Insert(0, Resources.Resource.lbl_all);
        ddl_unit_id.Items.Add(new ListItem(Resources.Resource.lbl_storage_unit, "-1"));
       
        ddl_unit_id.SelectedIndex = 0;



        tiger.PM app = new tiger.PM(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        gv_appliance_list.DataSource = app.getPMApplianceList(Convert.ToInt32(Session["schema_id"]), home_id, 0, 1, Convert.ToInt32(Session["name_id"]));
        gv_appliance_list.DataBind();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_unit_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        int home_id = 0;
        int unit_id = 0;

        gv_appliance_list.Visible = true;
        gv_appliance_search_list.Visible = false;


        ddl_appliance_categ.SelectedIndex = 0;

        if (ddl_home_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        if (ddl_unit_id.SelectedIndex > 0)
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        // if the unit selectid is not the storage unit
        if (ddl_unit_id.SelectedValue != "-1")
        {
            tiger.PM app = new tiger.PM(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            gv_appliance_list.DataSource = app.getPMApplianceList(Convert.ToInt32(Session["schema_id"]), home_id, unit_id, Convert.ToInt32(ddl_appliance_categ.SelectedValue), Convert.ToInt32(Session["name_id"]));
            gv_appliance_list.DataBind();
        }

        else
        {

            tiger.PM app = new tiger.PM(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            gv_appliance_list.DataSource = app.getPMApplianceStorageUnitList(Convert.ToInt32(Session["schema_id"]), home_id, Convert.ToInt32(ddl_appliance_categ.SelectedValue), Convert.ToInt32(Session["name_id"]));
            gv_appliance_list.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_appliance_categ_SelectedIndexChanged(object sender, EventArgs e)
    {
        int home_id = 0;
        int unit_id = 0;

        gv_appliance_list.Visible = true;
        gv_appliance_search_list.Visible = false;

        if (ddl_home_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        if (ddl_unit_id.SelectedIndex > 0)
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        // if the unit selectid is not the storage unit
        if (ddl_unit_id.SelectedValue != "-1")
        {
            tiger.PM app = new tiger.PM(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            gv_appliance_list.DataSource = app.getPMApplianceList(Convert.ToInt32(Session["schema_id"]), home_id, unit_id, Convert.ToInt32(ddl_appliance_categ.SelectedValue), Convert.ToInt32(Session["name_id"]));
            gv_appliance_list.DataBind();
        }

        else
        {

            tiger.PM app = new tiger.PM(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            gv_appliance_list.DataSource = app.getPMApplianceStorageUnitList(Convert.ToInt32(Session["schema_id"]), home_id, Convert.ToInt32(ddl_appliance_categ.SelectedValue), Convert.ToInt32(Session["name_id"]));
            gv_appliance_list.DataBind();
        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        gv_appliance_list.Visible = false;
        gv_appliance_search_list.Visible = true;

      Page.Validate("vg_property");
      if (Page.IsValid)
      {
        string name = "";
        tiger.PM appliance = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        gv_appliance_search_list.DataSource = appliance.getPMApplianceSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(radio_appliance_search.SelectedValue), tbx_appliance_search.Text, Convert.ToInt32(Session["name_id"]));
        gv_appliance_search_list.DataBind();


        
        // if we do a textbox search we have to deselect all the item that were selected 
        // in the appliance list gridview
        for (int j = 0; j < gv_appliance_list.Rows.Count; j++)
        {

            CheckBox chk_process_1 = (CheckBox)gv_appliance_list.Rows[j].FindControl("chk_process_1");
            // if the checkbox is checked the process
            if (chk_process_1.Checked)
            {
                chk_process_1.Checked = false;
            }
        }

      }
    }
    protected void gv_appliance_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        int home_id = 0;
        int unit_id = 0;

        if (ddl_home_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        if (ddl_unit_id.SelectedIndex > 0)
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        tiger.PM app = new tiger.PM(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        gv_appliance_list.PageIndex = e.NewPageIndex;
        gv_appliance_list.DataSource = app.getPMApplianceList(Convert.ToInt32(Session["schema_id"]), home_id, unit_id, Convert.ToInt32(ddl_appliance_categ.SelectedValue), Convert.ToInt32(Session["name_id"]));
        gv_appliance_list.DataBind();

        //TabContainer1.ActiveTabIndex = 0;
    }
    protected void gv_appliance_search_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        int home_id = 0;
        int unit_id = 0;

        if (ddl_home_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        if (ddl_unit_id.SelectedIndex > 0)
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        tiger.PM appliance = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_appliance_search_list.PageIndex = e.NewPageIndex;
        gv_appliance_search_list.DataSource = appliance.getPMApplianceSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(radio_appliance_search.SelectedValue), tbx_appliance_search.Text, Convert.ToInt32(Session["name_id"]));
        gv_appliance_search_list.DataBind();

        //TabContainer1.ActiveTabIndex = 0;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chk_warehouse_CheckedChanged(object sender, EventArgs e)
    {

        if (chk_warehouse.Checked == true)
        {
            chk_home.Checked = false;
            ddl_home_list2.Enabled = false;
            ddl_unit_id2.Enabled = false;

            ddl_warehouse_list2.Enabled = true;
        }
        //TabContainer1.ActiveTabIndex = 0;

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chk_home_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_home.Checked == true)
        {
            chk_warehouse.Checked = false;
            ddl_warehouse_list2.Enabled = false;

            ddl_home_list2.Enabled = true;
            ddl_unit_id2.Enabled = true;
        }

        //TabContainer1.ActiveTabIndex = 0;
    }

    protected void ddl_home_list2_SelectedIndexChanged(object sender, EventArgs e)
    {     

        int home_id = 0;
        // int unit_id = 0;
         home_id = Convert.ToInt32(ddl_home_list2.SelectedValue);

        //    if (ddl_unit_id.SelectedIndex > 0)
        //    unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

       // ddl_appliance_categ.SelectedIndex = 0;

        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ddl_unit_id2.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
        ddl_unit_id2.DataBind();
        ddl_unit_id2.Items.Add(new ListItem(Resources.Resource.lbl_storage_unit, "-1"));

        ddl_unit_id2.SelectedIndex = 0;
    }
    protected void btn_move_Click(object sender, EventArgs e)
    {
        int number_of_insert = 0;
        string appliance_id = "";
        string ua_id = "";

        // here we are counting the number of row of the GridView

        // gets all the appliance to be moved for appliance_search_list
        if (gv_appliance_search_list.Visible == true)
            for (int j = 0; j < gv_appliance_search_list.Rows.Count; j++)
            {

                CheckBox chk_process = (CheckBox)gv_appliance_search_list.Rows[j].FindControl("chk_process");
                // if the checkbox is checked the process
                if (chk_process.Checked)
                {
                    HiddenField h_appliance_id = (HiddenField)gv_appliance_search_list.Rows[j].FindControl("h_appliance_id");
                    HiddenField h_ua_id = (HiddenField)gv_appliance_search_list.Rows[j].FindControl("h_ua_id");

                    appliance_id = appliance_id + h_appliance_id.Value + "|";
                    ua_id = ua_id + h_ua_id.Value + "|";

                    number_of_insert++;

                    //  chk_process.Checked = false;
                }
            }



        // gets all the appliance to be moved for appliance_search_list

        if (gv_appliance_list.Visible == true)
            for (int j = 0; j < gv_appliance_list.Rows.Count; j++)
            {

                CheckBox chk_process_1 = (CheckBox)gv_appliance_list.Rows[j].FindControl("chk_process_1");
                // if the checkbox is checked the process
                if (chk_process_1.Checked)
                {
                    HiddenField h_appliance_id = (HiddenField)gv_appliance_list.Rows[j].FindControl("h_appliance_id");
                    HiddenField h_ua_id = (HiddenField)gv_appliance_list.Rows[j].FindControl("h_ua_id");

                    appliance_id = appliance_id + h_appliance_id.Value + "|";
                    ua_id = ua_id + h_ua_id.Value + "|";

                    number_of_insert++;

                    //  chk_process_1.Checked = false;
                }
            }


        Label1.Text = appliance_id;
        Label2.Text = ua_id;

      string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

      //////////
      ////////// SECURITY CHECK  BEGIN //////////////
      /////////

      AccountObjectAuthorization applianceAuthorization = new AccountObjectAuthorization(strconn);

      if (applianceAuthorization.ApplianceBatch(Convert.ToInt32(Session["schema_id"]), appliance_id, ua_id, number_of_insert))
      {
      }
      else
      {
          Session.Abandon();
          Response.Redirect("~/login.aspx");
      }

      //////////
      ////////// SECURITY CHECK END //////////////
      /////////
     
        if (ua_id != "" && number_of_insert > 0) //
        {
            //Here we send the values to the stored procedure

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prAppliaceMovingBatch", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //  try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);

                // we replace the commas  by dots because SQL "CAST"
                // we cast the string "100,00" in money
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@appliance_id", SqlDbType.VarChar, 80000).Value = appliance_id;
                cmd.Parameters.Add("@ua_id", SqlDbType.VarChar, 80000).Value = ua_id;
                cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;


                if (chk_home.Checked == true)
                {
                    cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_list2.SelectedValue);
                    cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_unit_id2.SelectedValue);
                    cmd.Parameters.Add("@warehouse_id", SqlDbType.Int).Value = DBNull.Value;
                }


                if (chk_warehouse.Checked == true)
                {
                    cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = DBNull.Value;
                    cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = DBNull.Value;
                    cmd.Parameters.Add("@warehouse_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_warehouse_list2.SelectedValue);
                }

                DateTime date_add = new DateTime();
                tiger.Date df = new tiger.Date();

                date_add = Convert.ToDateTime(df.DateCulture(ddl_ua_dateadd_m.SelectedValue, ddl_ua_dateadd_d.SelectedValue, ddl_ua_dateadd_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
                cmd.Parameters.Add("@ua_date_add", SqlDbType.SmallDateTime).Value = date_add;



                //execute the insert
                cmd.ExecuteReader();
          
                int return_id = 0;
                return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);
                if (return_id == 0)
                {
                    lbl_success.Text = Resources.Resource.lbl_appliance_move_success;
                  //  tb_successfull_confirmation.Visible = true;
                }
                else
                {
                    lbl_success.Text = Resources.Resource.lbl_appliance_move_unsuccess;
                    //  tb_successfull_confirmation.Visible = true;
                }

            }
            //   finally
            {
                conn.Close();
            }
        }

    
      


        // ----------------------------------------------------------------------


        int home_id = 0;
        int unit_id = 0;


        if (ddl_home_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        if (ddl_unit_id.SelectedIndex > 0)
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        // if the unit selectid is not the storage unit
        tiger.PM app = new tiger.PM(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        if (gv_appliance_list.Visible == true)
        {
            if (ddl_unit_id.SelectedValue != "-1")
            {
                gv_appliance_list.DataSource = app.getPMApplianceList(Convert.ToInt32(Session["schema_id"]), home_id, unit_id, Convert.ToInt32(ddl_appliance_categ.SelectedValue), Convert.ToInt32(Session["name_id"]));
                gv_appliance_list.DataBind();
            }

            else
            {

                gv_appliance_list.DataSource = app.getPMApplianceStorageUnitList(Convert.ToInt32(Session["schema_id"]), home_id, Convert.ToInt32(ddl_appliance_categ.SelectedValue), Convert.ToInt32(Session["name_id"]));
                gv_appliance_list.DataBind();
            }

        }


        if (gv_appliance_search_list.Visible == true)
        {
            // repopulate the appliance search
            gv_appliance_search_list.DataSource = app.getPMApplianceSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(radio_appliance_search.SelectedValue), tbx_appliance_search.Text, Convert.ToInt32(Session["name_id"]));
            gv_appliance_search_list.DataBind();
        }

        // ----------------------------------------------------------------------



        // here we are unchecking the selected value if it is not submitted,
        // if the warehouse  gv_appliance_list gridview is visible we want to deselect all the item
        // from the gv_appliance_search_list   gridview

        if (gv_appliance_list.Visible == true)
        {
            gv_appliance_search_list.Visible = false;

            for (int j = 0; j < gv_appliance_search_list.Rows.Count; j++)
            {

                CheckBox chk_process = (CheckBox)gv_appliance_search_list.Rows[j].FindControl("chk_process");
                // if the checkbox is checked the process
                if (chk_process.Checked)
                {
                    chk_process.Checked = false;
                }
            }

        }


        // here we are unchecking the selected value if it is not submitted,
        // if the warehouse gv_appliance_search_list is visible we want to deselect all the item
        // from the  gv_appliance_list gridview  gridview

        if (gv_appliance_search_list.Visible == true)
        {
            gv_appliance_list.Visible = false;

            for (int j = 0; j < gv_appliance_list.Rows.Count; j++)
            {

                CheckBox chk_process_1 = (CheckBox)gv_appliance_list.Rows[j].FindControl("chk_process_1");
                // if the checkbox is checked the process
                if (chk_process_1.Checked)
                {
                    chk_process_1.Checked = false;
                }
            }
        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_warehouse_appliance_list3_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_warehouse_appliance_list3.Visible = true;
        gv_warehouse_appliance_search_list3.Visible = false;

        tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        gv_warehouse_appliance_list3.PageIndex = e.NewPageIndex;
        gv_warehouse_appliance_list3.DataSource = app.getWarehouseApplianceList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_warehouse_list3.SelectedValue), Convert.ToInt32(ddl_warehouse_appliance_categ3.SelectedValue));
        gv_warehouse_appliance_list3.DataBind();

        //TabContainer1.ActiveTabIndex = 1 ;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_warehouse_appliance_search_list3_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        gv_warehouse_appliance_list3.Visible = false;
        gv_warehouse_appliance_search_list3.Visible = true;

        tiger.Appliance appliance = new tiger.Appliance(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_warehouse_appliance_search_list3.PageIndex = e.NewPageIndex;
        gv_warehouse_appliance_search_list3.DataSource = appliance.getWarehouseApplianceSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(radio_warehouse_search3.SelectedValue), tbx_warehouse_search3.Text);
        gv_warehouse_appliance_search_list3.DataBind();

        //TabContainer1.ActiveTabIndex = 1 ;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_warehouse_list3_SelectedIndexChanged(object sender, EventArgs e)
    {
        //TabContainer1.ActiveTabIndex = 1;
       
        gv_warehouse_appliance_list3.Visible = true;
        gv_warehouse_appliance_search_list3.Visible = false;

        tiger.Appliance app = new tiger.Appliance(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_warehouse_appliance_list3.DataSource = app.getWarehouseApplianceList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_warehouse_list3.SelectedValue), Convert.ToInt32(ddl_warehouse_appliance_categ3.SelectedValue));
        gv_warehouse_appliance_list3.DataBind();

  
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_warehouse_appliance_categ3_SelectedIndexChanged(object sender, EventArgs e)
    {
        //TabContainer1.ActiveTabIndex = 1;
        
        gv_warehouse_appliance_list3.Visible = true;
        gv_warehouse_appliance_search_list3.Visible = false;

        tiger.Appliance app = new tiger.Appliance(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_warehouse_appliance_list3.DataSource = app.getWarehouseApplianceList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_warehouse_list3.SelectedValue), Convert.ToInt32(ddl_warehouse_appliance_categ3.SelectedValue));
        gv_warehouse_appliance_list3.DataBind();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_warehouse_submit_Click(object sender, EventArgs e)
    {
        Page.Validate("vg_warehouse");
        if (Page.IsValid)
        {
            gv_warehouse_appliance_list3.Visible = false;
            gv_warehouse_appliance_search_list3.Visible = true;

            tiger.Appliance appliance = new tiger.Appliance(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
          //  gv_warehouse_appliance_search_list3.PageIndex = e.NewPageIndex;
            gv_warehouse_appliance_search_list3.DataSource = appliance.getWarehouseApplianceSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(radio_warehouse_search3.SelectedValue), tbx_warehouse_search3.Text);
            gv_warehouse_appliance_search_list3.DataBind();


            //TabContainer1.ActiveTabIndex = 1;
       }
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    
    protected void chk_warehouse3_CheckedChanged(object sender, EventArgs e)
    {

        if (chk_warehouse3.Checked == true)
        {
            chk_home3.Checked = false;
            ddl_home_list3.Enabled = false;
            ddl_unit_id3.Enabled = false;

            ddl_warehouse_list4.Enabled = true;
        }

        //TabContainer1.ActiveTabIndex = 1;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chk_home3_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_home3.Checked == true)
        {
            chk_warehouse3.Checked = false;
            ddl_warehouse_list4.Enabled = false;

            ddl_home_list3.Enabled = true;
            ddl_unit_id3.Enabled = true;
        }

        //TabContainer1.ActiveTabIndex = 1;
    }




    protected void btn_move2_Click(object sender, EventArgs e)
    {

        int number_of_insert = 0;
        string appliance_id = "";
        string ua_id = "";

        // here we are counting the number of row of the GridView

        // gets all the appliance to be moved for warehouse appliance_search_list
        if (gv_warehouse_appliance_search_list3.Visible == true)
            for (int j = 0; j < gv_warehouse_appliance_search_list3.Rows.Count; j++)
            {

                CheckBox chk_process_1 = (CheckBox)gv_warehouse_appliance_search_list3.Rows[j].FindControl("chk_process_1");
                // if the checkbox is checked the process
                if (chk_process_1.Checked)
                {
                    HiddenField h_appliance_id = (HiddenField)gv_warehouse_appliance_search_list3.Rows[j].FindControl("h_appliance_id");
                    HiddenField h_ua_id = (HiddenField)gv_warehouse_appliance_search_list3.Rows[j].FindControl("h_ua_id");

                    appliance_id = appliance_id + h_appliance_id.Value + "|";
                    ua_id = ua_id + h_ua_id.Value + "|";

                    number_of_insert++;

                }
            }

        // gets all the appliance to be moved

        if (gv_warehouse_appliance_list3.Visible == true)
            for (int j = 0; j < gv_warehouse_appliance_list3.Rows.Count; j++)
            {

                CheckBox chk_process = (CheckBox)gv_warehouse_appliance_list3.Rows[j].FindControl("chk_process");
                // if the checkbox is checked the process
                if (chk_process.Checked)
                {
                    HiddenField h_appliance_id = (HiddenField)gv_warehouse_appliance_list3.Rows[j].FindControl("h_appliance_id");
                    HiddenField h_ua_id = (HiddenField)gv_warehouse_appliance_list3.Rows[j].FindControl("h_ua_id");

                    appliance_id = appliance_id + h_appliance_id.Value + "|";
                    ua_id = ua_id + h_ua_id.Value + "|";

                    number_of_insert++;

                }
            }

        Label3.Text = appliance_id;
        Label4.Text = ua_id;


        //////////
        ////////// SECURITY CHECK BEGIN  //////////////
        /////////
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

      AccountObjectAuthorization applianceAuthorization = new AccountObjectAuthorization(strconn);

      if (applianceAuthorization.ApplianceBatch(Convert.ToInt32(Session["schema_id"]), appliance_id, ua_id, number_of_insert))
      {

      }
      else
      {
          Session.Abandon();
          Response.Redirect("~/login.aspx");
      }

      //////////
      ////////// SECURITY CHECK END  //////////////
      /////////

        if (ua_id != "" && number_of_insert > 0) //
        {
            //Here we send the values to the stored procedure

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prWarehouseApplianceMovingBatch", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //  try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
        
                // we replace the commas  by dots because SQL "CAST"
                // we cast the string "100,00" in money

                cmd.Parameters.Add("@appliance_id", SqlDbType.VarChar, 80000).Value = appliance_id;
                cmd.Parameters.Add("@ua_id", SqlDbType.VarChar, 80000).Value = ua_id;
                cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;


                if (chk_home3.Checked == true)
                {
                    cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_list3.SelectedValue);
                    cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_unit_id3.SelectedValue);
                    cmd.Parameters.Add("@warehouse_id", SqlDbType.Int).Value = DBNull.Value;
                }


                if (chk_warehouse3.Checked == true)
                {
                    cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = DBNull.Value;
                    cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = DBNull.Value;
                    cmd.Parameters.Add("@warehouse_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_warehouse_list4.SelectedValue);
                }

                DateTime date_add = new DateTime();
                tiger.Date df = new tiger.Date();

                date_add = Convert.ToDateTime(df.DateCulture(ddl_ua_dateadd_m2.SelectedValue, ddl_ua_dateadd_d2.SelectedValue, ddl_ua_dateadd_y2.SelectedValue, Convert.ToString(Session["_lastCulture"])));
                cmd.Parameters.Add("@ua_date_add", SqlDbType.SmallDateTime).Value = date_add;

                //execute the insert
                cmd.ExecuteReader();

                int return_id = 0;
                return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);
                if (return_id == 0)
                {
                    lbl_success.Text = Resources.Resource.lbl_successfull_modification;
                }

            }
            //   finally
            {
                conn.Close();
            }
        }


        // ----------------------------------------------------------------------



        // if the unit selectid is not the storage unit
        tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        if (gv_warehouse_appliance_list3.Visible == true)
        {
            gv_warehouse_appliance_list3.DataSource = app.getWarehouseApplianceList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_warehouse_list3.SelectedValue), Convert.ToInt32(ddl_warehouse_appliance_categ3.SelectedValue));
            gv_warehouse_appliance_list3.DataBind();
        }


        if (gv_warehouse_appliance_search_list3.Visible == true)
        {
            // repopulate the appliance search
            gv_warehouse_appliance_search_list3.DataSource = app.getWarehouseApplianceSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(radio_warehouse_search3.SelectedValue), tbx_warehouse_search3.Text);
            gv_warehouse_appliance_search_list3.DataBind();
        }

        // ----------------------------------------------------------------------


        // here we are unchecking the selected value if it is not submitted,
        // if the warehouse gv_warehouse_appliance_list3 gridview is visible we want to deselect all the item
        // from the gv_warehouse_appliance_search_list3  gridview

        if (gv_warehouse_appliance_list3.Visible == true)
        {
            gv_warehouse_appliance_search_list3.Visible = false;

            for (int j = 0; j < gv_warehouse_appliance_search_list3.Rows.Count; j++)
            {

                CheckBox chk_process_1 = (CheckBox)gv_warehouse_appliance_search_list3.Rows[j].FindControl("chk_process_1");
                // if the checkbox is checked the process
                if (chk_process_1.Checked)
                {
                    chk_process_1.Checked = false;
                }
            }




        }


        // here we are unchecking the selected value if it is not submitted,
        // if the warehouse  gv_warehouse_appliance_search_list3  gridview is visible we want to deselect all the item
        // from the gv_warehouse_appliance_list3   gridview

        if (gv_warehouse_appliance_search_list3.Visible == true)
        {
            gv_warehouse_appliance_list3.Visible = false;

            for (int j = 0; j < gv_warehouse_appliance_list3.Rows.Count; j++)
            {

                CheckBox chk_process = (CheckBox)gv_warehouse_appliance_list3.Rows[j].FindControl("chk_process");
                // if the checkbox is checked the process
                if (chk_process.Checked)
                {
                    chk_process.Checked = false;
                }
            }
        }

        //TabContainer1.ActiveTabIndex = 1;

    }
    protected void ddl_home_list3_SelectedIndexChanged(object sender, EventArgs e)
    {
        int home_id = 0;
        // int unit_id = 0;

        //TabContainer1.ActiveTabIndex = 1;

        home_id = Convert.ToInt32(ddl_home_list3.SelectedValue);


        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ddl_unit_id3.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
        ddl_unit_id3.DataBind();
        ddl_unit_id3.Items.Add(new ListItem(Resources.Resource.lbl_storage_unit, "-1"));

        ddl_unit_id3.SelectedIndex = 0;
    }
   
}
