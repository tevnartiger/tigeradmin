﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : nov 19 , 2008
/// </summary>

public partial class manager_appliance_appliance_moving_wiz_property : BasePage
{


    protected void Page_Load(object sender, EventArgs e)
    {

        if (!(Page.IsPostBack))
        {
            

            SetDefaultView();
            // default moving date is NOW
            DateTime date = new DateTime();
            date = DateTime.Now;

            ddl_ua_dateadd_m.SelectedValue = date.Month.ToString();
            ddl_ua_dateadd_d.SelectedValue = date.Day.ToString();
            ddl_ua_dateadd_y.SelectedValue = date.Year.ToString();

            
            // SetDefaultView();
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            //get the list of warehouse 
            tiger.Warehouse warehouse = new tiger.Warehouse(strconn);
            ddl_warehouse_list2.DataSource = warehouse.getWarehouseList(Convert.ToInt32(Session["schema_id"]));
            ddl_warehouse_list2.DataBind();
            ddl_warehouse_list2.Items.Insert(0, new ListItem(Resources.Resource.lbl_select, "-1"));
            ddl_warehouse_list2.SelectedIndex = 0;

            //by default we put new appliance in property therefore the property checkbox is selected
           // chk_home.Checked = true;

            
            //get the list of appliance category
            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prApplianceCategList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                //    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                ddl_appliance_categ.DataSource = ds;
                

                if (Session["_lastCulture"].ToString() == "fr-FR")
                {
                    ddl_appliance_categ.DataTextField = "ac_name_fr";
                }
                if (Session["_lastCulture"].ToString() == "en-US")
                {
                    ddl_appliance_categ.DataTextField = "ac_name_en";
                    

                }

                ddl_appliance_categ.DataBind();
                

            }
            finally
            {
                conn.Close();
            }

            String referer = "";
         //   referer = Request.UrlReferrer.ToString();



            DateTime right_now = new DateTime();
            right_now = DateTime.Now;

            tiger.Date d = new tiger.Date();

            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


            tiger.Home l = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getHomeCount(Convert.ToInt32(Session["schema_id"]));

            int home_id = l.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));

            // first we check if there's any property available
            if (home_count > 0)
            {



                tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ddl_home_list.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                ddl_home_list.DataBind();
                ddl_home_list.Items.Insert(0, Resources.Resource.lbl_all);
                ddl_home_list.SelectedValue = home_id.ToString();

                ddl_home_list2.DataSource = ddl_home_list.DataSource;
                ddl_home_list2.DataBind();

                

                // ddl_home_id2.Enabled = false;

                // DropDownList pour les Unit
                //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

                if (unit_count > 0)
                {

                    int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), home_id);

                    // unit from the property origin
                    ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
                    ddl_unit_id.SelectedValue = Convert.ToString(unit_id);
                    ddl_unit_id.DataBind();
                    ddl_unit_id.Items.Insert(0, Resources.Resource.lbl_all);
                    ddl_unit_id.Items.Add(new ListItem(Resources.Resource.lbl_storage_unit, "-1"));
                    ddl_unit_id.SelectedIndex = 0;

                    // unitt from the property we we want to move the appliances to
                    ddl_unit_id2.DataSource = ddl_unit_id.DataSource;
                    ddl_unit_id2.DataBind();
                    ddl_unit_id2.Items.Add(new ListItem(Resources.Resource.lbl_storage_unit, "-1"));


                }

            }

            else
            {
                // panel_home_unit_add.Visible = false;
                txt_message.InnerHtml = "<b><a href='home/home_add.aspx'>Please add a property</a></b>";
            }


            tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            gv_appliance_list.DataSource = app.getApplianceList(Convert.ToInt32(Session["schema_id"]), home_id, 0, 1);
            gv_appliance_list.DataBind();





        }


    }

    private void SetDefaultView()
    {
        MultiView1.ActiveViewIndex = 0 ;
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_list_SelectedIndexChanged(object sender, EventArgs e)
    {

        gv_appliance_list.Visible = true;
        gv_appliance_search_list.Visible = false;

        int home_id = 0;
        // int unit_id = 0;

        if (ddl_home_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        //    if (ddl_unit_id.SelectedIndex > 0)
        //    unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        ddl_appliance_categ.SelectedIndex = 0;

        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
        ddl_unit_id.DataBind();
        ddl_unit_id.Items.Insert(0, Resources.Resource.lbl_all);
        ddl_unit_id.Items.Add(new ListItem(Resources.Resource.lbl_storage_unit, "-1"));

        ddl_unit_id.SelectedIndex = 0;



        tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        gv_appliance_list.DataSource = app.getApplianceList(Convert.ToInt32(Session["schema_id"]), home_id, 0, 1);
        gv_appliance_list.DataBind();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_unit_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        int home_id = 0;
        int unit_id = 0;

        gv_appliance_list.Visible = true;
        gv_appliance_search_list.Visible = false;


        ddl_appliance_categ.SelectedIndex = 0;

        if (ddl_home_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        if (ddl_unit_id.SelectedIndex > 0)
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        // if the unit selectid is not the storage unit
        if (ddl_unit_id.SelectedValue != "-1")
        {
            tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            gv_appliance_list.DataSource = app.getApplianceList(Convert.ToInt32(Session["schema_id"]), home_id, unit_id, Convert.ToInt32(ddl_appliance_categ.SelectedValue));
            gv_appliance_list.DataBind();
        }

        else
        {

            tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            gv_appliance_list.DataSource = app.getApplianceStorageUnitList(Convert.ToInt32(Session["schema_id"]), home_id, Convert.ToInt32(ddl_appliance_categ.SelectedValue));
            gv_appliance_list.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_appliance_categ_SelectedIndexChanged(object sender, EventArgs e)
    {
        int home_id = 0;
        int unit_id = 0;

        gv_appliance_list.Visible = true;
        gv_appliance_search_list.Visible = false;

        if (ddl_home_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        if (ddl_unit_id.SelectedIndex > 0)
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        // if the unit selectid is not the storage unit
        if (ddl_unit_id.SelectedValue != "-1")
        {
            tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            gv_appliance_list.DataSource = app.getApplianceList(Convert.ToInt32(Session["schema_id"]), home_id, unit_id, Convert.ToInt32(ddl_appliance_categ.SelectedValue));
            gv_appliance_list.DataBind();
        }

        else
        {

            tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            gv_appliance_list.DataSource = app.getApplianceStorageUnitList(Convert.ToInt32(Session["schema_id"]), home_id, Convert.ToInt32(ddl_appliance_categ.SelectedValue));
            gv_appliance_list.DataBind();
        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        gv_appliance_list.Visible = false;
        gv_appliance_search_list.Visible = true;


        string name = "";
        tiger.Appliance appliance = new tiger.Appliance(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        gv_appliance_search_list.DataSource = appliance.getApplianceSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(radio_appliance_search.SelectedValue), RegEx.getText(tbx_appliance_search.Text));
        gv_appliance_search_list.DataBind();



        // if we do a textbox search we have to deselect all the item that were selected 
        // in the appliance list gridview
        for (int j = 0; j < gv_appliance_list.Rows.Count; j++)
        {

            CheckBox chk_process_1 = (CheckBox)gv_appliance_list.Rows[j].FindControl("chk_process_1");
            // if the checkbox is checked the process
            if (chk_process_1.Checked)
            {
                chk_process_1.Checked = false;
            }
        }
    }
    protected void gv_appliance_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        int home_id = 0;
        int unit_id = 0;

        if (ddl_home_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        if (ddl_unit_id.SelectedIndex > 0)
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        gv_appliance_list.PageIndex = e.NewPageIndex;
        gv_appliance_list.DataSource = app.getApplianceList(Convert.ToInt32(Session["schema_id"]), home_id, unit_id, Convert.ToInt32(ddl_appliance_categ.SelectedValue));
        gv_appliance_list.DataBind();

        MultiView1.ActiveViewIndex = 0;
    }
    protected void gv_appliance_search_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        int home_id = 0;
        int unit_id = 0;

        if (ddl_home_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        if (ddl_unit_id.SelectedIndex > 0)
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        tiger.Appliance appliance = new tiger.Appliance(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_appliance_search_list.PageIndex = e.NewPageIndex;
        gv_appliance_search_list.DataSource = appliance.getApplianceSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(radio_appliance_search.SelectedValue), RegEx.getText(tbx_appliance_search.Text));
        gv_appliance_search_list.DataBind();

        MultiView1.ActiveViewIndex = 0;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// 
    /*
    protected void chk_warehouse_CheckedChanged(object sender, EventArgs e)
    {

        if (chk_warehouse.Checked == true)
        {
            chk_home.Checked = false;
            ddl_home_list2.Enabled = false;
            ddl_unit_id2.Enabled = false;

            ddl_warehouse_list2.Enabled = true;
        }
        MultiView1.ActiveViewIndex = 0;

    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chk_home_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_home.Checked == true)
        {
            chk_warehouse.Checked = false;
            ddl_warehouse_list2.Enabled = false;

            ddl_home_list2.Enabled = true;
            ddl_unit_id2.Enabled = true;
        }

        MultiView1.ActiveViewIndex = 0;
    }
 */
    protected void ddl_home_list2_SelectedIndexChanged(object sender, EventArgs e)
    {




        int home_id = 0;
        // int unit_id = 0;


        home_id = Convert.ToInt32(ddl_home_list2.SelectedValue);

        //    if (ddl_unit_id.SelectedIndex > 0)
        //    unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        // ddl_appliance_categ.SelectedIndex = 0;

        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ddl_unit_id2.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
        ddl_unit_id2.DataBind();
        ddl_unit_id2.Items.Add(new ListItem(Resources.Resource.lbl_storage_unit, "-1"));

        ddl_unit_id2.SelectedIndex = 0;
    }
    protected void btn_move_Click(object sender, EventArgs e)
    {
        int number_of_insert = 0;
        string appliance_id = "";
        string ua_id = "";

        // here we are counting the number of row of the GridView

        // gets all the appliance to be moved for appliance_search_list
        if (gv_appliance_search_list.Visible = true)
            for (int j = 0; j < gv_appliance_search_list.Rows.Count; j++)
            {

                CheckBox chk_process = (CheckBox)gv_appliance_search_list.Rows[j].FindControl("chk_process");
                // if the checkbox is checked the process
                if (chk_process.Checked)
                {
                    HiddenField h_appliance_id = (HiddenField)gv_appliance_search_list.Rows[j].FindControl("h_appliance_id");
                    HiddenField h_ua_id = (HiddenField)gv_appliance_search_list.Rows[j].FindControl("h_ua_id");

                    appliance_id = appliance_id + h_appliance_id.Value + "|";
                    ua_id = ua_id + h_ua_id.Value + "|";

                    number_of_insert++;

                    //  chk_process.Checked = false;
                }
            }



        // gets all the appliance to be moved for appliance_list

        if (gv_appliance_list.Visible = true)
            for (int j = 0; j < gv_appliance_list.Rows.Count; j++)
            {

                CheckBox chk_process_1 = (CheckBox)gv_appliance_list.Rows[j].FindControl("chk_process_1");
                // if the checkbox is checked the process
                if (chk_process_1.Checked)
                {
                    HiddenField h_appliance_id = (HiddenField)gv_appliance_list.Rows[j].FindControl("h_appliance_id");
                    HiddenField h_ua_id = (HiddenField)gv_appliance_list.Rows[j].FindControl("h_ua_id");

                    appliance_id = appliance_id + h_appliance_id.Value + "|";
                    ua_id = ua_id + h_ua_id.Value + "|";

                    number_of_insert++;

                    //  chk_process_1.Checked = false;
                }
            }




        Label1.Text = appliance_id;
        Label2.Text = ua_id;


      string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

      //////////
      ////////// SECURITY CHECK BEGIN  //////////////
      /////////
      AccountObjectAuthorization applianceAuthorization = new AccountObjectAuthorization(strconn);
  
      if (applianceAuthorization.ApplianceBatch(Convert.ToInt32(Session["schema_id"]), appliance_id, ua_id, number_of_insert))
       {
       }
      else
      {
          Session.Abandon();
          Response.Redirect("~/login.aspx");
      }

      //////////
      ////////// SECURITY CHECK END  //////////////
      /////////


        if ((ua_id != "" && number_of_insert > 0) && btn_move.Visible==true &&
            (radio_destination.SelectedValue == "1" || (radio_destination.SelectedValue == "2" && ddl_warehouse_list2.SelectedIndex > 0))) //
        {
            //Here we send the values to the stored procedure

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prAppliaceMovingBatch", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //  try
            {
                conn.Open();
                //Add the params

                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;                    
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();

                // we replace the commas  by dots because SQL "CAST"
                // we cast the string "100,00" in money

                cmd.Parameters.Add("@appliance_id", SqlDbType.VarChar, 80000).Value = appliance_id;
                cmd.Parameters.Add("@ua_id", SqlDbType.VarChar, 80000).Value = ua_id;
                cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;


                if (radio_destination.SelectedValue == "1")
                {
                    cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_list2.SelectedValue);
                    cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_unit_id2.SelectedValue);
                    cmd.Parameters.Add("@warehouse_id", SqlDbType.Int).Value = DBNull.Value;
                }


                if (radio_destination.SelectedValue == "2")
                {
                    cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = DBNull.Value;
                    cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = DBNull.Value;
                    cmd.Parameters.Add("@warehouse_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_warehouse_list2.SelectedValue);
                }

                DateTime date_add = new DateTime();
                tiger.Date df = new tiger.Date();

                date_add = Convert.ToDateTime(df.DateCulture(ddl_ua_dateadd_m.SelectedValue, ddl_ua_dateadd_d.SelectedValue, ddl_ua_dateadd_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
                cmd.Parameters.Add("@ua_date_add", SqlDbType.SmallDateTime).Value = date_add;

                //execute the insert
                cmd.ExecuteReader();

            }
            //   finally
            {
                conn.Close();
            }
            //ICI IL FAUT LOGGER LE IP CAR ON A DU TAMPERING
          //  int authorize = Convert.ToInt32(cmd.Parameters["@authorize"].Value);
          //  if (authorize == 0)
            {
            //    Session.Abandon();
            //    Response.Redirect("~/login.aspx");
            }


            if (Convert.ToInt32(cmd.Parameters["@return_id"].Value) == 0)
            {
                btn_move.Visible = false;
                tb_previous.Visible = false;
                lbl_moving_success.Text = Resources.Resource.lbl_appliance_move_success;
            }
            else
            {
                lbl_moving_success.Text = Resources.Resource.lbl_appliance_move_unsuccess;
            }
        }

   
        // ----------------------------------------------------------------------


        int home_id = 0;
        int unit_id = 0;


        if (ddl_home_list.SelectedIndex > 0)
            home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        if (ddl_unit_id.SelectedIndex > 0)
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        // if the unit selectid is not the storage unit
        tiger.Appliance app = new tiger.Appliance(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        if (gv_appliance_list.Visible == true)
        {
            if (ddl_unit_id.SelectedValue != "-1")
            {
                gv_appliance_list.DataSource = app.getApplianceList(Convert.ToInt32(Session["schema_id"]), home_id, unit_id, Convert.ToInt32(ddl_appliance_categ.SelectedValue));
                gv_appliance_list.DataBind();
            }

            else
            {

                gv_appliance_list.DataSource = app.getApplianceStorageUnitList(Convert.ToInt32(Session["schema_id"]), home_id, Convert.ToInt32(ddl_appliance_categ.SelectedValue));
                gv_appliance_list.DataBind();
            }

        }


        if (gv_appliance_search_list.Visible == true)
        {
            // repopulate the appliance search
            gv_appliance_search_list.DataSource = app.getApplianceSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(radio_appliance_search.SelectedValue), RegEx.getText(tbx_appliance_search.Text));
            gv_appliance_search_list.DataBind();
        }

        // ----------------------------------------------------------------------



        // here we are unchecking the selected value if it is not submitted,
        // if the warehouse  gv_appliance_list gridview is visible we want to deselect all the item
        // from the gv_appliance_search_list   gridview

        if (gv_appliance_list.Visible == true)
        {
            gv_appliance_search_list.Visible = false;

            for (int j = 0; j < gv_appliance_search_list.Rows.Count; j++)
            {

                CheckBox chk_process = (CheckBox)gv_appliance_search_list.Rows[j].FindControl("chk_process");
                // if the checkbox is checked the process
                if (chk_process.Checked)
                {
                    chk_process.Checked = false;
                }
            }

        }


        // here we are unchecking the selected value if it is not submitted,
        // if the warehouse gv_appliance_search_list is visible we want to deselect all the item
        // from the  gv_appliance_list gridview  gridview

        if (gv_appliance_search_list.Visible == true)
        {
            gv_appliance_list.Visible = false;

            for (int j = 0; j < gv_appliance_list.Rows.Count; j++)
            {

                CheckBox chk_process_1 = (CheckBox)gv_appliance_list.Rows[j].FindControl("chk_process_1");
                // if the checkbox is checked the process
                if (chk_process_1.Checked)
                {
                    chk_process_1.Checked = false;
                }
            }
        }


        

    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_next_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_next2_Click(object sender, EventArgs e)
    {

        lbl_unit_origin.Visible = true;
        Label18.Visible = true;
        lbl_colon.Visible = true;

        MultiView1.ActiveViewIndex = 2;
        if (radio_destination.SelectedValue == "1")
         {
             row_property.Visible = true;
             row_unit.Visible = true;
             row_warehouse.Visible = false;
        
         }

        if (radio_destination.SelectedValue == "2")
        {
            row_property.Visible = false;
            row_unit.Visible = false;
            row_warehouse.Visible = true;
        }

        lbl_property_origin.Text = ddl_home_list.SelectedItem.ToString();
        lbl_unit_origin.Text = ddl_unit_id.SelectedItem.ToString();


        if (ddl_unit_id.SelectedIndex == 0)
        {
            lbl_unit_origin.Visible = false;
            Label18.Visible = false;
            lbl_colon.Visible = false;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_previous2_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_previous3_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
    }
}
