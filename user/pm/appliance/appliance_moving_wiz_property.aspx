﻿<%@ Page Language="C#" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" CodeFile="appliance_moving_wiz_property.aspx.cs" Inherits="manager_appliance_appliance_moving_wiz_property" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

     <b style="font-size: medium">
         <asp:Label ID="Label3" runat="server"  Text="<%$ Resources:Resource,lbl_u_move_appliance %>"></asp:Label></b><br /><br />
    <asp:Label ID="lbl_success" runat="server" Text=""></asp:Label>
      <br />
 <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
      
       
       <table style="width: 100%">
           <tr>
               <td bgcolor="AliceBlue" style="font-size: small">
                   <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource,lbl_move_appliance_step1 %>"></asp:Label></td>
           </tr>
       </table><br />
       
       <div ID="txt_message" runat="server">
       </div>
       <table cellspacing="1" style="width: 100%">
           <tr>
               <td>
                   
                   <table cellpadding="0" cellspacing="0" style="width: 100%">
                       <tr>
                           <td valign="top"  >
                               <asp:Label ID="Label9" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_origin %>" style="font-weight: 700"  /></b></td>
                                          <td valign="top">
                                              </b></td>
                                      </tr>
                                      <tr>
                                          <td valign="top"  >
                   <table bgcolor="#F7FAD3" >
                       <tr>
                           <td >
                                <asp:Label ID="Label7" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_search_by %>" style="font-weight: 700"  /> </b></td>
                           <td  >
                               &nbsp;</td>
                           <td  >
                               &nbsp;</td>
                           <td style="width: 24px"  >
                              </td>
                       </tr>
                       <tr>
                           <td  >
                               <asp:Label ID="lbl_property" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_property %>" />
                           </td>
                           <td  >
                               :
                               <asp:DropDownList ID="ddl_home_list" runat="server" AutoPostBack="True" 
                                   DataTextField="home_name" DataValueField="home_id" 
                                   OnSelectedIndexChanged="ddl_home_list_SelectedIndexChanged">
                               </asp:DropDownList>
                               &nbsp;</td>
                           <td style="width: 3px">
                               &nbsp;</td>
                           <td style="width: 24px">
                               &nbsp;</td>
                       </tr>
                       <tr>
                           <td  >
                               <asp:Label ID="lbl_unit" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_unit %>" />
                           </td>
                           <td colspan="3" valign="top">
                               :
                               <asp:DropDownList ID="ddl_unit_id" runat="server" AutoPostBack="True" 
                                   DataTextField="unit_door_no" DataValueField="unit_id" 
                                   OnSelectedIndexChanged="ddl_unit_id_SelectedIndexChanged" />
                               &nbsp;&nbsp;</td>
                       </tr>
                       <tr>
                           <td >
                                <asp:Label ID="Label6" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_category %>"  /> </b></td>
                           <td colspan="3" valign="top">
                               : 
                               <asp:DropDownList ID="ddl_appliance_categ" runat="server" 
                                   AutoPostBack="True" DataValueField="ac_id" 
                                   onselectedindexchanged="ddl_appliance_categ_SelectedIndexChanged">
                               </asp:DropDownList>
                           </td>
                       </tr>
                   </table>
                                  <br />
                           </td>
                           <td valign="top">

                               &nbsp;</td>
                       </tr>
                       <tr>
                           <td valign="top" colspan="2">
                   <table bgcolor="#F7FAD3" cellpadding="0" cellspacing="1" style="width: 35%">
                       <tr>
                           <td style="width: 915px">
                               <asp:Label ID="Label14" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_quick_search_inv_serial %>"  /></td>
                       </tr>
                       <tr>
                           <td style="width: 915px">
                               
                               <asp:RadioButtonList ID="radio_appliance_search" runat="server" 
                                   RepeatDirection="Horizontal">
                                    <asp:ListItem Selected="True"  Text='<%$ Resources:Resource,lbl_starts_with %>' Value="1"></asp:ListItem>
                                        <asp:ListItem Text="<%$ Resources:Resource,lbl_contains %>" Value="2"></asp:ListItem>
                                   </asp:RadioButtonList>
                               </b>
                           </td>
                       </tr>
                       <tr>
                           <td style="width: 915px" valign="top">
                               <asp:TextBox ID="tbx_appliance_search" runat="server"></asp:TextBox>
                               &nbsp;&nbsp;&nbsp;&nbsp;
                               <asp:Button ID="Button1" runat="server" onclick="btn_submit_Click" 
                                   Text="<%$ Resources:Resource,btn_search %>" /><br />
                              
                           </td>
                       </tr>
                   </table>
                               <br />
                               <br />
                              </td>
                       </tr>
                       <tr>
                           <td style="width: 374px">
                               &nbsp;</td>
                           <td>
                               &nbsp;</td>
                       </tr>
                       <tr>
                           <td bgcolor="AliceBlue" colspan="2"> 
                               
                               <asp:Label ID="Label15" runat="server" 
                                   Text="<%$ Resources:Resource,lbl_appliance_to_be_move %>" 
                                   style="font-weight: 700"  /></b></td>
                       </tr>
                   </table>
                              </td>
           </tr>
       </table>
     
       
       
           <asp:GridView ID="gv_appliance_list" runat="server" AllowPaging="True" 
                BorderColor="#CDCDCD"  BorderWidth="1px"
              AutoGenerateColumns="False" 
              EmptyDataText="<%$ Resources:Resource,lbl_no_data %>" 
               OnPageIndexChanging="gv_appliance_list_PageIndexChanging" 
               Width="100%"  >
               <Columns>
                 <asp:TemplateField HeaderText='<%$ Resources:Resource, gv_to_process %>' >
                 <ItemTemplate>
                 <asp:CheckBox  runat="server"   ID="chk_process_1"   /> 
                <asp:HiddenField runat="server" Value='<%# Bind("ua_id")%>'  ID="h_ua_id" />
                <asp:HiddenField runat="server" Value='<%# Bind("appliance_id")%>'  ID="h_appliance_id" />
                   </ItemTemplate>
                 </asp:TemplateField>
                 
                   
                   <asp:BoundField DataField="appliance_name" HeaderText='<%$ Resources:Resource, lbl_appliance_name %>'
                       SortExpression="ac_name_en" />
                   <asp:BoundField DataField="ac_name_en" HeaderText='<%$ Resources:Resource, lbl_category %>'
                       SortExpression="ac_name_en" />
                   <asp:BoundField DataField="appliance_serial_no" HeaderText='<%$ Resources:Resource, lbl_serial_no %>' />
                   <asp:BoundField DataField="home_name" HeaderText='<%$ Resources:Resource, lbl_property %>' />
                   <asp:BoundField DataField="unit_door_no" HeaderText='<%$ Resources:Resource, lbl_unit %>' />
                   
                  
               </Columns>
              
               <AlternatingRowStyle BackColor="#F0F0F6" />
               <HeaderStyle BackColor="#F0F0F6" />
              
           </asp:GridView>
       
      
      


           <asp:GridView ID="gv_appliance_search_list" runat="server" 
            AllowPaging="True" AutoGenerateColumns="False" 
            BorderColor="#CDCDCD"  BorderWidth="1px"
             EmptyDataText="<%$ Resources:Resource,lbl_no_data %>" 
               OnPageIndexChanging="gv_appliance_search_list_PageIndexChanging" 
               Width="100%" >
               <Columns>
               <asp:TemplateField HeaderText='<%$ Resources:Resource, gv_to_process %>' >
                <ItemTemplate>
                <asp:CheckBox  runat="server"   ID="chk_process"   /> 
                <asp:HiddenField runat="server" Value='<%# Bind("ua_id")%>'  ID="h_ua_id" />
                <asp:HiddenField runat="server" Value='<%# Bind("appliance_id")%>'  ID="h_appliance_id" />
                   </ItemTemplate>
                </asp:TemplateField>
                  
                
                    <asp:BoundField DataField="appliance_name" HeaderText='<%$ Resources:Resource, lbl_appliance_name %>'
                       SortExpression="ac_name_en" />
                   <asp:BoundField DataField="ac_name_en" HeaderText='<%$ Resources:Resource, lbl_category %>'
                       SortExpression="ac_name_en" />
                   <asp:BoundField DataField="appliance_serial_no" HeaderText='<%$ Resources:Resource, lbl_serial_no %>' />
                   <asp:BoundField DataField="home_name" HeaderText='<%$ Resources:Resource, lbl_property %>' />
                   <asp:BoundField DataField="unit_door_no" HeaderText='<%$ Resources:Resource, lbl_unit %>' />
                   
                   
               </Columns>
               <AlternatingRowStyle BackColor="#F0F0F6" />
               <HeaderStyle BackColor="#F0F0F6" />
           </asp:GridView>
           <br />
           
           
           <br />
           
        <table  style="width: 100%;" bgcolor="aliceblue">
            <tr>
                <td align="right">           
                <asp:Button ID="btn_next" runat="server" Text="<%$ Resources:Resource,lbl_u_next %>" onclick="btn_next_Click" /> 
                </td>
            </tr>          
        </table>
           
           <br />
           
       
       </asp:View>
     
     
     
     
    <asp:View ID="View2" runat="server">
      <br />  
     <table style="width: 100%">
           <tr>
               <td bgcolor="AliceBlue" style="font-size: small">
                <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource,lbl_move_appliance_step2 %>"></asp:Label></td>
           </tr>
       </table>
     <br />
     <br />
     <asp:RadioButtonList ID="radio_destination" runat="server" 
                                   RepeatDirection="Vertical">
                                    <asp:ListItem  Text="<%$ Resources:Resource,lbl_property %>" Selected="True" Value="1"></asp:ListItem>
                                        <asp:ListItem  Text="<%$ Resources:Resource,lbl_external_storage %>" Value="2"></asp:ListItem>
      </asp:RadioButtonList>
                              
     <br /><br />
      <table  style="width: 50%;" bgcolor="aliceblue">
            <tr>
                <td align="right">           
                <asp:Button ID="btn_previous2" runat="server" Text="<%$ Resources:Resource,lbl_u_previous %>"  onclick="btn_previous2_Click" /> &nbsp;&nbsp;&nbsp; <asp:Button ID="btn_next2" runat="server" Text="<%$ Resources:Resource,lbl_u_next %>" onclick="btn_next2_Click" /> 
                </td>
            </tr>          
        </table>
     
     </asp:View>
     
     
     
     
     
  <asp:View ID="View3" runat="server">
     
        <br />
        <span style="font-size: small">
        <asp:Label ID="Label16" runat="server" 
            Text="<%$ Resources:Resource,lbl_origin %>"></asp:Label>
        &nbsp;<asp:Label ID="lbl_origine" runat="server"></asp:Label>
        <br />
        <asp:Label ID="Label17" runat="server" 
            Text="<%$ Resources:Resource,lbl_property %>"></asp:Label>
        &nbsp; :
        <asp:Label ID="lbl_property_origin" runat="server"></asp:Label>
        <br />
        <asp:Label ID="Label18" runat="server" 
            Text="<%$ Resources:Resource,lbl_unit %>"></asp:Label>
        &nbsp;<asp:Label ID="lbl_colon" runat="server" Text=":"></asp:Label>
        &nbsp;&nbsp;<asp:Label ID="lbl_unit_origin" runat="server"></asp:Label>
        <br />
        </span>
     
        <br />
         <table style="width: 100%">
           <tr>
               <td bgcolor="AliceBlue" style="font-size: small">
                  <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource,lbl_move_appliance_step3 %>"></asp:Label></td>
           </tr>
       </table>
        <br />
        <table bgcolor="#ffffcc">
            <tr ID="row_property" runat="server">
                <td>
                    <asp:Label ID="Label10" runat="server" 
                        Text="<%$ Resources:Resource, lbl_property %>"></asp:Label>
                </td>
                <td style="width: 186px">
                    <asp:DropDownList ID="ddl_home_list2" runat="server" AutoPostBack="True" 
                        DataTextField="home_name" DataValueField="home_id" 
                        OnSelectedIndexChanged="ddl_home_list2_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr ID="row_unit" runat="server">
                <td valign="top">
                    <asp:Label ID="Label11" runat="server" 
                        Text="<%$ Resources:Resource, lbl_unit %>"></asp:Label>
                </td>
                <td style="width: 186px" valign="top">
                    <asp:DropDownList ID="ddl_unit_id2" runat="server" DataTextField="unit_door_no" 
                        DataValueField="unit_id">
                    </asp:DropDownList>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                </td>
            </tr>
            <tr ID="row_warehouse" runat="server">
                <td>
                    <asp:Label ID="Label12" runat="server" 
                        Text="<%$ Resources:Resource, lbl_external_storage %>"></asp:Label>
                    </b>
                </td>
                <td style="width: 186px">
                    <asp:DropDownList ID="ddl_warehouse_list2" runat="server" 
                        DataTextField="warehouse_name" DataValueField="warehouse_id">
                    </asp:DropDownList>
                    &nbsp;
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label13" runat="server" 
                        Text="<%$ Resources:Resource, txt_date %>"></asp:Label>
                </td>
                <td style="width: 186px">
                    <asp:DropDownList ID="ddl_ua_dateadd_m" runat="server">
                        <asp:ListItem Value="1"></asp:ListItem>
                        <asp:ListItem Value="2"></asp:ListItem>
                        <asp:ListItem Value="3"></asp:ListItem>
                        <asp:ListItem Value="4"></asp:ListItem>
                        <asp:ListItem Value="5"></asp:ListItem>
                        <asp:ListItem Value="6"></asp:ListItem>
                        <asp:ListItem Value="7"></asp:ListItem>
                        <asp:ListItem Value="8"></asp:ListItem>
                        <asp:ListItem Value="9"></asp:ListItem>
                        <asp:ListItem Value="10"></asp:ListItem>
                        <asp:ListItem Value="11"></asp:ListItem>
                        <asp:ListItem Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    -<asp:DropDownList ID="ddl_ua_dateadd_d" runat="server">
                        <asp:ListItem Value="0">Day</asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
                    -<asp:DropDownList ID="ddl_ua_dateadd_y" runat="server">
                        <asp:ListItem Value="0">Year</asp:ListItem>
                        <asp:ListItem>1960</asp:ListItem>
                        <asp:ListItem>1961</asp:ListItem>
                        <asp:ListItem>1962</asp:ListItem>
                        <asp:ListItem>1963</asp:ListItem>
                        <asp:ListItem>1964</asp:ListItem>
                        <asp:ListItem>1965</asp:ListItem>
                        <asp:ListItem>1966</asp:ListItem>
                        <asp:ListItem>1967</asp:ListItem>
                        <asp:ListItem>1968</asp:ListItem>
                        <asp:ListItem>1969</asp:ListItem>
                        <asp:ListItem>1970</asp:ListItem>
                        <asp:ListItem>1972</asp:ListItem>
                        <asp:ListItem>1973</asp:ListItem>
                        <asp:ListItem>1974</asp:ListItem>
                        <asp:ListItem>1975</asp:ListItem>
                        <asp:ListItem>1976</asp:ListItem>
                        <asp:ListItem>1977</asp:ListItem>
                        <asp:ListItem>1978</asp:ListItem>
                        <asp:ListItem>1979</asp:ListItem>
                        <asp:ListItem>1980</asp:ListItem>
                        <asp:ListItem>1981</asp:ListItem>
                        <asp:ListItem>1982</asp:ListItem>
                        <asp:ListItem>1983</asp:ListItem>
                        <asp:ListItem>1984</asp:ListItem>
                        <asp:ListItem>1985</asp:ListItem>
                        <asp:ListItem>1986</asp:ListItem>
                        <asp:ListItem>1987</asp:ListItem>
                        <asp:ListItem>1988</asp:ListItem>
                        <asp:ListItem>1989</asp:ListItem>
                        <asp:ListItem>1990</asp:ListItem>
                        <asp:ListItem>1991</asp:ListItem>
                        <asp:ListItem>1992</asp:ListItem>
                        <asp:ListItem>1993</asp:ListItem>
                        <asp:ListItem>1994</asp:ListItem>
                        <asp:ListItem>1995</asp:ListItem>
                        <asp:ListItem>1996</asp:ListItem>
                        <asp:ListItem>1997</asp:ListItem>
                        <asp:ListItem>1998</asp:ListItem>
                        <asp:ListItem>1999</asp:ListItem>
                        <asp:ListItem>2000</asp:ListItem>
                        <asp:ListItem>2001</asp:ListItem>
                        <asp:ListItem>2002</asp:ListItem>
                        <asp:ListItem>2003</asp:ListItem>
                        <asp:ListItem>2004</asp:ListItem>
                        <asp:ListItem>2005</asp:ListItem>
                        <asp:ListItem>2006</asp:ListItem>
                        <asp:ListItem>2007</asp:ListItem>
                        <asp:ListItem>2008</asp:ListItem>
                      <asp:ListItem>2009</asp:ListItem>
                     <asp:ListItem>2010</asp:ListItem>
                     <asp:ListItem>2011</asp:ListItem>
                     <asp:ListItem>2012</asp:ListItem>
                     <asp:ListItem>2013</asp:ListItem>
                     <asp:ListItem>2014</asp:ListItem>
                     <asp:ListItem>2015</asp:ListItem>
                 </asp:DropDownList>
                </td>
            </tr>
        </table>
        <br />
        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        <br />
        <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btn_move" runat="server" OnClick="btn_move_Click" 
            Text="<%$ Resources:Resource,btn_move %>" />
        <br />
        
   
         <br /><br />
      <table id="tb_previous" runat="server" style="width: 50%;" bgcolor="aliceblue">
            <tr>
                <td align="right">           
                <asp:Button ID="btn_previous3" runat="server" Text="<%$ Resources:Resource,lbl_u_previous %>" onclick="btn_previous3_Click" /> 
                </td>
            </tr>          
        </table>
        <br />
        <br />
      <asp:Label ID="lbl_moving_success" runat="server" Text=""></asp:Label>
      
      </asp:View>
     
</asp:MultiView>


</asp:Content>

