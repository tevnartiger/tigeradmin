﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : november 22 , 2008
/// </summary>

public partial class manager_appliance_appliance_moving_wiz : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_next_Click(object sender, EventArgs e)
    {

        if(radio_destination.SelectedValue == "1" )
            Server.Transfer("appliance_moving_wiz_property.aspx");
        else
            Server.Transfer("appliance_moving_wiz_warehouse.aspx");
    }


}
