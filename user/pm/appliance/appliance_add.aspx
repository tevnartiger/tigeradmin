﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="appliance_add.aspx.cs" Inherits="manager_appliance_appliance_add" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">





 <table >
        <tr>
                <td style="width: 258px">
                    <asp:Label ID="Label11" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_supplier %>"  />  <span style="font-size: 8pt">&nbsp; 
                    ( <asp:Label ID="Label2" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_optional %>"  />  &nbsp;)</span></td>
                <td style="width: 340px">
                    <asp:DropDownList ID="ddl_supplier_list"  DataTextField="company_name" DataValueField="company_id" runat="server">
                    </asp:DropDownList>
                    </td>
                <td>
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    <span style="font-size: 7pt">&nbsp; *&nbsp; <asp:Label ID="Label3" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_add_supplier %>"  />&nbsp;&nbsp;<asp:HyperLink ID="add_supplier" runat="server" NavigateUrl="~/manager/company/company_add.aspx?c=s"><asp:Label ID="Label19" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_add_supplier %>"  /></asp:HyperLink></span></td></tr><tr>
        <td 
                style="width: 258px"><strong> <asp:Label ID="Label18" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_property %>"  /></strong></td>
        <td 
                style="width: 340px"><asp:DropDownList ID="ddl_appliance_home_id" runat="server"  OnSelectedIndexChanged="ddl_appliance_home_id_SelectedIndexChanged" DataTextField="home_name" DataValueField="home_id" AutoPostBack="True">
        </asp:DropDownList>&nbsp; <asp:CheckBox ID="chk_home" Text="" runat="server" 
               AutoPostBack="true"  oncheckedchanged="chk_home_CheckedChanged" />
                              </td>
        <td>
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </td></tr><tr>
        <td 
                style="width: 258px"><strong><asp:Label ID="Label4" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_unit %>"  /> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </strong></td><td 
                style="width: 340px"><asp:DropDownList DataTextField="unit_door_no" DataValueField="unit_id" ID="ddl_unit_id" runat="server">
            </asp:DropDownList>
            </td>
        <td>
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; </td></tr><tr>
        <td style="width: 258px"><b><asp:Label ID="Label5" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_external_storage %>"  /></b></td>
        <td 
                colspan="2"><asp:DropDownList    ID="ddl_warehouse_list" DataValueField="warehouse_id"
                                DataTextField="warehouse_name" runat="server">
            </asp:DropDownList>
&nbsp; <asp:CheckBox ID="chk_warehouse" runat="server" 
               AutoPostBack="true"  oncheckedchanged="chk_warehouse_CheckedChanged" />
             <asp:Label 
                    ID="lbl_select_warehouse" runat="server" Text="select a warehouse" 
                    style="color: #FF0000"></asp:Label></td></tr><tr>
        <td style="width: 258px"><asp:Label ID="Label6" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_date_add %>"  /></td>
        <td colspan="2">
             
                 <asp:DropDownList ID="ddl_ua_dateadd_m" runat="server">
                     <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem></asp:DropDownList>&nbsp; / &nbsp; <asp:DropDownList ID="ddl_ua_dateadd_d" runat="server">
                     <asp:ListItem Value="0">Day</asp:ListItem><asp:ListItem>1</asp:ListItem><asp:ListItem>2</asp:ListItem><asp:ListItem>3</asp:ListItem><asp:ListItem>4</asp:ListItem><asp:ListItem>5</asp:ListItem><asp:ListItem>6</asp:ListItem><asp:ListItem>7</asp:ListItem><asp:ListItem>8</asp:ListItem><asp:ListItem>9</asp:ListItem><asp:ListItem>10</asp:ListItem><asp:ListItem>11</asp:ListItem><asp:ListItem>12</asp:ListItem><asp:ListItem>13</asp:ListItem><asp:ListItem>14</asp:ListItem><asp:ListItem>15</asp:ListItem><asp:ListItem>16</asp:ListItem><asp:ListItem>17</asp:ListItem><asp:ListItem>18</asp:ListItem><asp:ListItem>19</asp:ListItem><asp:ListItem>20</asp:ListItem><asp:ListItem>21</asp:ListItem><asp:ListItem>22</asp:ListItem><asp:ListItem>23</asp:ListItem><asp:ListItem>24</asp:ListItem><asp:ListItem>25</asp:ListItem><asp:ListItem>26</asp:ListItem><asp:ListItem>27</asp:ListItem><asp:ListItem>28</asp:ListItem><asp:ListItem>29</asp:ListItem><asp:ListItem>30</asp:ListItem><asp:ListItem>31</asp:ListItem></asp:DropDownList>&nbsp; / &nbsp; <asp:DropDownList ID="ddl_ua_dateadd_y" runat="server" >
                     <asp:ListItem Value="0">Year</asp:ListItem><asp:ListItem>1960</asp:ListItem><asp:ListItem>1961</asp:ListItem><asp:ListItem>1962</asp:ListItem><asp:ListItem>1963</asp:ListItem><asp:ListItem>1964</asp:ListItem><asp:ListItem>1965</asp:ListItem><asp:ListItem>1966</asp:ListItem><asp:ListItem>1967</asp:ListItem><asp:ListItem>1968</asp:ListItem><asp:ListItem>1969</asp:ListItem><asp:ListItem>1970</asp:ListItem><asp:ListItem>1972</asp:ListItem><asp:ListItem>1973</asp:ListItem><asp:ListItem>1974</asp:ListItem><asp:ListItem>1975</asp:ListItem><asp:ListItem>1976</asp:ListItem><asp:ListItem>1977</asp:ListItem><asp:ListItem>1978</asp:ListItem><asp:ListItem>1979</asp:ListItem><asp:ListItem>1980</asp:ListItem><asp:ListItem>1981</asp:ListItem><asp:ListItem>1982</asp:ListItem><asp:ListItem>1983</asp:ListItem><asp:ListItem>1984</asp:ListItem><asp:ListItem>1985</asp:ListItem><asp:ListItem>1986</asp:ListItem><asp:ListItem>1987</asp:ListItem><asp:ListItem>1988</asp:ListItem><asp:ListItem>1989</asp:ListItem><asp:ListItem>1990</asp:ListItem><asp:ListItem>1991</asp:ListItem><asp:ListItem>1992</asp:ListItem><asp:ListItem>1993</asp:ListItem><asp:ListItem>1994</asp:ListItem><asp:ListItem>1995</asp:ListItem><asp:ListItem>1996</asp:ListItem><asp:ListItem>1997</asp:ListItem><asp:ListItem>1998</asp:ListItem><asp:ListItem>1999</asp:ListItem><asp:ListItem>2000</asp:ListItem><asp:ListItem>2001</asp:ListItem><asp:ListItem>2002</asp:ListItem><asp:ListItem>2003</asp:ListItem><asp:ListItem>2004</asp:ListItem><asp:ListItem>2005</asp:ListItem><asp:ListItem>2006</asp:ListItem><asp:ListItem>2007</asp:ListItem><asp:ListItem>2008</asp:ListItem><asp:ListItem>2009</asp:ListItem><asp:ListItem>2010</asp:ListItem><asp:ListItem>2011</asp:ListItem><asp:ListItem>2012</asp:ListItem><asp:ListItem>2013</asp:ListItem><asp:ListItem>2014</asp:ListItem><asp:ListItem>2015</asp:ListItem></asp:DropDownList></td></tr><tr>
             <td colspan="3" >
                 &nbsp;&nbsp; </td></tr><td style="width: 258px"><asp:Label ID="Label7" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_category %>"  /></td>
        <td style="width: 340px">
        <asp:DropDownList ID="ddl_appliance_categ" runat="server"   DataValueField="ac_id">
        </asp:DropDownList> 
         <asp:RequiredFieldValidator ID="req_appliance_categ"  ControlToValidate="ddl_appliance_categ"
                InitialValue="<%$ Resources:Resource, lbl_select %>"
                runat="server" ErrorMessage="select a category"></asp:RequiredFieldValidator></td><td>
            &nbsp;</td><tr><td colspan="2"><asp:Label ID="Label1" runat="server" ></asp:Label></td><td>
            &nbsp;</td></tr><tr><td style="width: 258px" valign="top"><asp:Label ID="Label8" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_appliance_name %>"  />
                </td>
        <td valign="top" colspan="2"><asp:TextBox ID="appliance_name" runat="server"></asp:TextBox><asp:RegularExpressionValidator ID="reg_appliance_name" runat="server"  ControlToValidate="appliance_name"
                    ErrorMessage="invalid name"></asp:RegularExpressionValidator> <asp:RequiredFieldValidator 
                    ID="req_appliance_name" runat="server"  ControlToValidate="appliance_name"
                    ErrorMessage="required"></asp:RequiredFieldValidator></td></tr><tr><td style="width: 258px" valign="top"><asp:Label ID="Label9" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_invoice_no %>"  />
                </td>
        <td valign="top" colspan="2"><asp:TextBox ID="appliance_invoice_no" runat="server"></asp:TextBox><asp:RegularExpressionValidator 
                    ID="reg_appliance_invoice_no" runat="server"  ControlToValidate="appliance_invoice_no"
                    ErrorMessage="invalid invoice # , only numbers or letters"></asp:RegularExpressionValidator></td></tr><tr><td style="width: 258px"><asp:Label ID="Label10" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_invoice_photo %>"  /></td>
        <td 
                style="width: 340px" valign="top"><asp:FileUpload ID="appliance_invoice_photo" runat="server" /></td>
        <td>
            &nbsp;</td></tr><tr><td style="width: 258px"><asp:Label ID="Label12" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_cost %>"  /></td>
        <td 
                style="width: 340px" valign="top"><asp:TextBox ID="appliance_purchase_cost" runat="server"></asp:TextBox><asp:RegularExpressionValidator 
                    ID="reg_appliance_purchase_cost" runat="server"  ControlToValidate="appliance_purchase_cost"
                    ErrorMessage="invalid , must be numeric"></asp:RegularExpressionValidator></td><td>
            &nbsp;</td></tr><tr><td style="width: 258px" valign="top"><asp:Label ID="Label13" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_description %>"  /></td>
        <td 
                valign="top" colspan="2"><asp:TextBox ID="appliance_desc" runat="server" TextMode="MultiLine" Height="90px" Width="281px"></asp:TextBox><asp:RegularExpressionValidator 
                    ID="reg_appliance_desc" runat="server"  ControlToValidate="appliance_desc"
                    ErrorMessage="only text allowed"></asp:RegularExpressionValidator></td></tr><tr><td style="width: 258px"><asp:Label ID="Label14" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_serial_no %>"  /></td>
        <td 
                colspan="2"><asp:TextBox ID="appliance_serial_no" runat="server"></asp:TextBox><asp:RegularExpressionValidator 
                    ID="reg_appliance_serial_no" runat="server"  ControlToValidate="appliance_serial_no"
                    ErrorMessage="invalid invoice # , only numbers or letters"></asp:RegularExpressionValidator></td></tr><tr><td style="width: 258px"><asp:Label ID="Label15" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_purchase_date %>"  />
                </td>
        <td 
                style="width: 340px"><asp:DropDownList ID="ddl_appliance_date_purchase_m" runat="server">
                     <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem><asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem></asp:DropDownList>&nbsp; / &nbsp; <asp:DropDownList ID="ddl_appliance_date_purchase_d" runat="server">
                     <asp:ListItem Text="<%$ Resources:Resource, txt_day %>" Value="0"></asp:ListItem><asp:ListItem>1</asp:ListItem><asp:ListItem>2</asp:ListItem><asp:ListItem>3</asp:ListItem><asp:ListItem>4</asp:ListItem><asp:ListItem>5</asp:ListItem><asp:ListItem>6</asp:ListItem><asp:ListItem>7</asp:ListItem><asp:ListItem>8</asp:ListItem><asp:ListItem>9</asp:ListItem><asp:ListItem>10</asp:ListItem><asp:ListItem>11</asp:ListItem><asp:ListItem>12</asp:ListItem><asp:ListItem>13</asp:ListItem><asp:ListItem>14</asp:ListItem><asp:ListItem>15</asp:ListItem><asp:ListItem>16</asp:ListItem><asp:ListItem>17</asp:ListItem><asp:ListItem>18</asp:ListItem><asp:ListItem>19</asp:ListItem><asp:ListItem>20</asp:ListItem><asp:ListItem>21</asp:ListItem><asp:ListItem>22</asp:ListItem><asp:ListItem>23</asp:ListItem><asp:ListItem>24</asp:ListItem><asp:ListItem>25</asp:ListItem><asp:ListItem>26</asp:ListItem><asp:ListItem>27</asp:ListItem><asp:ListItem>28</asp:ListItem><asp:ListItem>29</asp:ListItem><asp:ListItem>30</asp:ListItem><asp:ListItem>31</asp:ListItem></asp:DropDownList>&nbsp; / &nbsp; <asp:DropDownList ID="ddl_appliance_date_purchase_y" runat="server" >
                     <asp:ListItem Text="<%$ Resources:Resource, txt_year %>" Value="0"></asp:ListItem><asp:ListItem>1960</asp:ListItem><asp:ListItem>1961</asp:ListItem><asp:ListItem>1962</asp:ListItem><asp:ListItem>1963</asp:ListItem><asp:ListItem>1964</asp:ListItem><asp:ListItem>1965</asp:ListItem><asp:ListItem>1966</asp:ListItem><asp:ListItem>1967</asp:ListItem><asp:ListItem>1968</asp:ListItem><asp:ListItem>1969</asp:ListItem><asp:ListItem>1970</asp:ListItem><asp:ListItem>1972</asp:ListItem><asp:ListItem>1973</asp:ListItem><asp:ListItem>1974</asp:ListItem><asp:ListItem>1975</asp:ListItem><asp:ListItem>1976</asp:ListItem><asp:ListItem>1977</asp:ListItem><asp:ListItem>1978</asp:ListItem><asp:ListItem>1979</asp:ListItem><asp:ListItem>1980</asp:ListItem><asp:ListItem>1981</asp:ListItem><asp:ListItem>1982</asp:ListItem><asp:ListItem>1983</asp:ListItem><asp:ListItem>1984</asp:ListItem><asp:ListItem>1985</asp:ListItem><asp:ListItem>1986</asp:ListItem><asp:ListItem>1987</asp:ListItem><asp:ListItem>1988</asp:ListItem><asp:ListItem>1989</asp:ListItem><asp:ListItem>1990</asp:ListItem><asp:ListItem>1991</asp:ListItem><asp:ListItem>1992</asp:ListItem><asp:ListItem>1993</asp:ListItem><asp:ListItem>1994</asp:ListItem><asp:ListItem>1995</asp:ListItem><asp:ListItem>1996</asp:ListItem><asp:ListItem>1997</asp:ListItem><asp:ListItem>1998</asp:ListItem><asp:ListItem>1999</asp:ListItem><asp:ListItem>2000</asp:ListItem><asp:ListItem>2001</asp:ListItem><asp:ListItem>2002</asp:ListItem><asp:ListItem>2003</asp:ListItem><asp:ListItem>2004</asp:ListItem><asp:ListItem>2005</asp:ListItem><asp:ListItem>2006</asp:ListItem><asp:ListItem>2007</asp:ListItem><asp:ListItem>2008</asp:ListItem><asp:ListItem>2009</asp:ListItem><asp:ListItem>2010</asp:ListItem><asp:ListItem>2011</asp:ListItem><asp:ListItem>2012</asp:ListItem><asp:ListItem>2013</asp:ListItem><asp:ListItem>2014</asp:ListItem><asp:ListItem>2015</asp:ListItem></asp:DropDownList></td><td>
            &nbsp;</td></tr><tr><td style="width: 258px"><asp:Label ID="Label16" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_garanty_year %>"  /></td>
        <td 
                style="width: 340px"><asp:DropDownList ID="ddl_appliance_guarantee_year" runat="server">
                        <asp:ListItem Text="<%$ Resources:Resource, lbl_select %>"  Value="0"></asp:ListItem><asp:ListItem>1960</asp:ListItem><asp:ListItem>1961</asp:ListItem><asp:ListItem>1962</asp:ListItem><asp:ListItem>1963</asp:ListItem><asp:ListItem>1964</asp:ListItem><asp:ListItem>1965</asp:ListItem><asp:ListItem>1966</asp:ListItem><asp:ListItem>1967</asp:ListItem><asp:ListItem>1968</asp:ListItem><asp:ListItem>1969</asp:ListItem><asp:ListItem>1970</asp:ListItem><asp:ListItem>1971</asp:ListItem><asp:ListItem>1972</asp:ListItem><asp:ListItem>1973</asp:ListItem><asp:ListItem>1974</asp:ListItem><asp:ListItem>1975</asp:ListItem><asp:ListItem>1976</asp:ListItem><asp:ListItem>1977</asp:ListItem><asp:ListItem>1978</asp:ListItem><asp:ListItem>1979</asp:ListItem><asp:ListItem>1980</asp:ListItem><asp:ListItem>1981</asp:ListItem><asp:ListItem>1982</asp:ListItem><asp:ListItem>1983</asp:ListItem><asp:ListItem>1984</asp:ListItem><asp:ListItem>1985</asp:ListItem><asp:ListItem>1986</asp:ListItem><asp:ListItem>1987</asp:ListItem><asp:ListItem>1988</asp:ListItem><asp:ListItem>1989</asp:ListItem><asp:ListItem>1990</asp:ListItem><asp:ListItem>1991</asp:ListItem><asp:ListItem>1992</asp:ListItem><asp:ListItem>1993</asp:ListItem><asp:ListItem>1994</asp:ListItem><asp:ListItem>1995</asp:ListItem><asp:ListItem>1996</asp:ListItem><asp:ListItem>1997</asp:ListItem><asp:ListItem>1998</asp:ListItem><asp:ListItem>1999</asp:ListItem><asp:ListItem>2000</asp:ListItem><asp:ListItem>2001</asp:ListItem><asp:ListItem>2002</asp:ListItem><asp:ListItem>2003</asp:ListItem><asp:ListItem>2004</asp:ListItem><asp:ListItem>2005</asp:ListItem><asp:ListItem>2006</asp:ListItem><asp:ListItem>2007</asp:ListItem><asp:ListItem>2008</asp:ListItem><asp:ListItem>2009</asp:ListItem><asp:ListItem>2010</asp:ListItem><asp:ListItem>2011</asp:ListItem><asp:ListItem>2012</asp:ListItem><asp:ListItem>2013</asp:ListItem><asp:ListItem>2014</asp:ListItem><asp:ListItem>2015</asp:ListItem><asp:ListItem>2016</asp:ListItem><asp:ListItem>2017</asp:ListItem><asp:ListItem>2018</asp:ListItem><asp:ListItem>2019</asp:ListItem><asp:ListItem>2020</asp:ListItem></asp:DropDownList></td><td>
            &nbsp;</td></tr><tr><td style="width: 258px"><asp:Label ID="Label17" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_appliance_picture %>"  /></td>
        <td 
                style="width: 340px"><asp:FileUpload ID="appliance_photo" runat="server" /></td>
        <td>
            &nbsp;</td></tr><tr><td style="width: 258px">&nbsp;</td><td 
                style="width: 340px">&nbsp;</td><td>
            &nbsp;</td></tr></table><br />
        
        <br />
    <br/>




<asp:Label ID="lbl_success" runat="server" Text=""></asp:Label><br />
<asp:Button ID="btn_add_appliance" runat="server" Text="<%$ Resources:Resource, btn_submit %>"  OnClick="btn_add_appliance_Click" />
</asp:Content>

