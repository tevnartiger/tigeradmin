<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="home_profile.aspx.cs" Inherits="manager_group_group_profile" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div style="float:left;width:100%;text-align:left;">
 <h2><asp:Literal runat="server" ID="lbl_profile" Text="<%$ resources:resource,lbl_group_profile %>" /></h2>
 <br />
<asp:DropDownList ID="ddl_group" runat="server"  AutoPostBack="true" OnSelectedIndexChanged="ddl_group_SelectedIndexChanged"  DataTextField="group_name" DataValueField="group_id" />
<br />

 <h4><asp:Label runat="server" ID="adfadf" Text="<%$ resources:resource, lbl_group %>" /></h4>
 
 <div runat="server" id="group" />
 <br />
 <h3><asp:Label runat="server" id="afdsffa" Text="<%$ resources:resource, lbl_property %>" /></h3>
  
 <asp:GridView ID="gv_home" runat="server" AutoGenerateColumns="false" DataKeyNames="home_id">
 <Columns>
   <asp:TemplateField HeaderText="Property">
  <ItemTemplate>
 <asp:Label ID="lbl_a" runat="server" Text="<%# Bind('home_name') %>" />
 </ItemTemplate>
 </asp:TemplateField>
 <asp:TemplateField HeaderText="Owner">
 <ItemTemplate>
 <asp:Label ID="lbl_b" runat="server" Text='<%#getOwnerProfile(1,79) %>' />
 </ItemTemplate>
 </asp:TemplateField>
 <asp:TemplateField HeaderText="Property Manager">
 <ItemTemplate>
 <asp:Label ID="lbl_b" runat="server" Text='<%#getManagerProfile(1,79) %>' />
 </ItemTemplate>
 </asp:TemplateField>
 <asp:TemplateField HeaderText="Janitor">
 <ItemTemplate>
 <asp:Label ID="lbl_b" runat="server" Text='<%#getJanitorProfile(1,79) %>' />
 </ItemTemplate>
 </asp:TemplateField>
 </Columns>
 </asp:GridView>
  </div>
</asp:Content>

