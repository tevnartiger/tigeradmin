using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class home_home_main : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
            lbl_success.Text = " Schema_id " + Session["schema_id"] + "<br>Group id " + Session["group_id"] + "<br>login_name: " + Session["login_name"] + "<br>Name_id: " + Session["name_id"];


            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prAlertView", conn);

            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = 0;
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            

            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                    // amount of days for insurance policy pending and expiration
                    lbl_as_ip_expiration.Text = dr["as_ip_expiration"].ToString();
                    lbl_as_ip_pending.Text = dr["as_ip_pending"].ToString();

                    lbl_ip_expiration.Text = dr["ip_expiration"].ToString();
                    if (Convert.ToInt32(dr["ip_expiration"]) < 1)
                        tr_ip_expiration.Visible = false;

                    lbl_ip_pending.Text = dr["ip_pending"].ToString();
                    if (Convert.ToInt32(dr["ip_pending"]) < 1)
                        tr_ip_pending.Visible = false;

                    // amount of days for lease pending and expiration
                    lbl_as_lease_expiration.Text = dr["as_lease_expiration"].ToString();
                    lbl_as_lease_pending.Text = dr["as_lease_pending"].ToString();

                    lbl_lease_expiration.Text = dr["lease_expiration"].ToString();
                    if (Convert.ToInt32(dr["lease_expiration"]) < 1)
                        tr_lease_expiration.Visible = false;

                    lbl_lease_pending.Text = dr["lease_pending"].ToString();
                    if (Convert.ToInt32(dr["lease_pending"]) < 1)
                        tr_lease_pending.Visible = false;


                    // amount of days for mortgage pending and expiration
                    lbl_as_mortgage_expiration.Text = dr["as_mortgage_expiration"].ToString();
                    lbl_as_mortgage_pending.Text = dr["as_mortgage_pending"].ToString();

                    lbl_mortgage_expiration.Text = dr["mortgage_expiration"].ToString();
                    if (Convert.ToInt32(dr["mortgage_expiration"]) < 1)
                        tr_mortgage_expiration.Visible = false;

                    lbl_mortgage_pending.Text = dr["mortgage_pending"].ToString();
                    if (Convert.ToInt32(dr["mortgage_pending"]) < 1)
                        tr_mortgage_pending.Visible = false;


                    lbl_rent_delequency.Text = dr["rent_delequency"].ToString();
                    if (Convert.ToInt32(dr["rent_delequency"]) < 1)
                        tr_rent_delequency.Visible = false;

                    lbl_wo_overdue.Text = dr["as_wo_overdue"].ToString();
                    if (Convert.ToInt32(dr["as_wo_overdue"]) < 1)
                        tr_wo_overdue.Visible = false;

                    lbl_wo_pending.Text = dr["as_wo_pending"].ToString();
                    if (Convert.ToInt32(dr["as_wo_pending"]) < 1)
                        tr_wo_pending.Visible = false;

                    lbl_untreated_rent.Text = dr["nb_untreated_rent"].ToString();
                    if (Convert.ToInt32(dr["nb_untreated_rent"]) < 1)
                        tr_wo_pending.Visible = false;
                }

            }
            finally
            {
                  conn.Close();
            }

       
    }
}
