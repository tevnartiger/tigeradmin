<%@ Page Language="C#" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" CodeFile="home_main.aspx.cs" Inherits="home_home_main"   Title="Prospective tenant view" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div style="font-weight: 700">
    <asp:Label ID="lbl_success" runat="server" />
        <br />
        <table >
            <tr>
                <td valign="top">
        <asp:Calendar ID="Calendar1" runat="server" SelectedDayStyle-BackColor="Red">
        </asp:Calendar>
                </td>
                <td valign="top">
                    <table  bgcolor="ALICEBLUE">
                    <tr>
                         <td bgcolor="Red">
                           <b><asp:Label ID="Label15" runat="server" Text="<%$ Resources:Resource, lbl_u_alerts %>"/> </b>
                         </td>
                    </tr>
                     <tr id="tr_lease_expiration" runat="server">
                            <td>
                                &nbsp;
                                <asp:Label ID="lbl_lease_expiration" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lbl_lease_expire %>"></asp:Label>
                                <asp:Label ID="lbl_as_lease_expiration" runat="server"></asp:Label>
                            &nbsp;<asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>" ></asp:Label>
                            </td>
                            
                        </tr>
                        <tr id="tr_lease_pending" runat="server">
                            <td>
                                &nbsp;
                                <asp:Label ID="lbl_lease_pending" runat="server"></asp:Label>
                                &nbsp;<asp:Label ID="Label13" runat="server" Text="<%$ Resources:Resource, lbl_lease_pending %>"></asp:Label>
                                <asp:Label ID="lbl_as_lease_pending" runat="server"></asp:Label>
                                &nbsp;<asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>" ></asp:Label>
                                </b></td>
                            
                        </tr>
                        <tr id="tr_mortgage_expiration" runat="server">
                            <td >
                                &nbsp;
                                <asp:Label ID="lbl_mortgage_expiration" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, lbl_mortgage_expire %>"></asp:Label>
                                <asp:Label ID="lbl_as_mortgage_expiration" runat="server"></asp:Label>
                                      &nbsp;<asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>" ></asp:Label> 
                                              </td>
                            
                        </tr>
                        <tr id="tr_mortgage_pending" runat="server">
                            <td >
                                &nbsp;
                                <asp:Label ID="lbl_mortgage_pending" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label12" runat="server" Text="<%$ Resources:Resource, lbl_mortgage_pending %>"></asp:Label>
                                <asp:Label ID="lbl_as_mortgage_pending" runat="server"></asp:Label>
                                    &nbsp;<asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>" ></asp:Label>
                                              </td>
                            
                        </tr>
                        <tr id="tr_ip_expiration" runat="server">
                            <td>
                                &nbsp;
                                <asp:Label ID="lbl_ip_expiration" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label10" runat="server" Text="<%$ Resources:Resource, lbl_ip_expire %>"></asp:Label>
                                <asp:Label ID="lbl_as_ip_expiration" runat="server"></asp:Label>
                                &nbsp;<asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>" ></asp:Label>
                                              </td>
                            
                        </tr>
                        <tr id="tr_ip_pending" runat="server">
                            <td >
                                &nbsp;
                                <asp:Label ID="lbl_ip_pending" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label11" runat="server" Text="<%$ Resources:Resource, lbl_ip_PENDING %>"></asp:Label>
                                <asp:Label ID="lbl_as_ip_pending" runat="server"></asp:Label>
                                &nbsp;<asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>" ></asp:Label>
                                              </td>
                            
                        </tr>
                        <tr id="tr_rent_delequency" runat="server">
                            <td>
                                &nbsp;
                                <asp:Label ID="lbl_rent_delequency" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label14" runat="server" Text="<%$ Resources:Resource, lbl_late_rent_not_received %>"></asp:Label></td>
                            
                        </tr>
                        
                        <tr id="tr_wo_overdue" runat="server">
                            <td>
                                &nbsp;
                                <asp:Label ID="lbl_wo_overdue" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label16" runat="server" Text="<%$ Resources:Resource, lbl_wo_overdue %>"></asp:Label></td>
                            
                        </tr>
                        <tr id="tr_wo_pending" runat="server">
                            <td>
                                &nbsp;
                                <asp:Label ID="lbl_wo_pending" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label18" runat="server" Text="<%$ Resources:Resource, lbl_wo_pending%>"></asp:Label></td>
                            
                        </tr>
                        <tr id="tr_untreated_rent" runat="server">
                            <td>
                                &nbsp;
                                <asp:Label ID="lbl_untreated_rent" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label17" runat="server" Text="<%$ Resources:Resource, lbl_alert_untreated_rent%>"></asp:Label></td>
                            
                        </tr>
                        <tr id="tr1" runat="server">
                            <td>
                                &nbsp;&nbsp; <asp:HyperLink Text="<%$ Resources:Resource, lbl_setup %>" 
                                    ID="HyperLink1" runat="server" NavigateUrl="~/manager/alerts/alerts_setup.aspx"></asp:HyperLink>
                                
                            &nbsp;&nbsp; <asp:HyperLink Text="<%$ Resources:Resource, lbl_details %>" 
                                    ID="HyperLink46" runat="server" 
                                    NavigateUrl="~/manager/alerts/alerts.aspx"></asp:HyperLink>
                                
                            </td>
                            
                        </tr>
                        
                    </table>
                </td>
            </tr>
        </table>
        <span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA"> 
        <br />
        <asp:HyperLink ID="HyperLink32" runat="server" 
            NavigateUrl="~/home/home_main.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Home 2</span></asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp; 
        &nbsp;<asp:HyperLink ID="HyperLink47" runat="server" NavigateUrl="~/manager/events/default.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Events Calendar</span></asp:HyperLink>
        &nbsp;
        <span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA"> 
                          <asp:HyperLink ID="HyperLink48" runat="server" 
                              NavigateUrl="~/manager/events/event_add.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Add an event</span></asp:HyperLink>
        &nbsp;
        </span>
                          <asp:HyperLink ID="HyperLink49" runat="server" 
                              NavigateUrl="~/manager/Scheduler/default2.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Event cal. 2</span></span></asp:HyperLink>
        &nbsp;<asp:HyperLink ID="HyperLink50" runat="server" 
                              NavigateUrl="~/manager/filemanager/default.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">FileManager</span></asp:HyperLink>
                          &nbsp;<asp:HyperLink ID="HyperLink56" runat="server" 
            NavigateUrl="~/tiger/numberofuser.aspx"># of logged in user</asp:HyperLink>
                          <br />
                          <br />
                          <asp:HyperLink ID="HyperLink51" runat="server" 
                              NavigateUrl="~/EncDecViaCode.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">webconfig encrypt/decrypt</span></asp:HyperLink>
        &nbsp;
        <asp:HyperLink ID="HyperLink53" runat="server" 
            NavigateUrl="~/manager/role/role_wiz.aspx">Role Assignement</asp:HyperLink>
        &nbsp;
        <asp:HyperLink ID="HyperLink54" runat="server" 
            NavigateUrl="~/manager/role/role_member_list.aspx?categ=0&l=">Member search</asp:HyperLink>
        &nbsp;
        <asp:HyperLink ID="HyperLink55" runat="server" 
            NavigateUrl="~/manager/role/role_update.aspx">Role Assignement Update</asp:HyperLink>
        <br />
        <br />
        <asp:HyperLink ID="HyperLink42" runat="server" 
            NavigateUrl="~/manager/insurancepolicy/insurance_policy_expiration.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Insurances 
        expiration</span></asp:HyperLink>
        &nbsp;&nbsp;
        <asp:HyperLink ID="HyperLink43" runat="server" 
            NavigateUrl="~/manager/mortgage/mortgage_expiration.aspx">Mortgage expiration</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;
        <span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA"> 
        <asp:HyperLink ID="HyperLink44" runat="server" 
            NavigateUrl="~/manager/lease/lease_expiration.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Lease expiration</span></span></asp:HyperLink>
        </span>
        <br />
        <br />
        <asp:HyperLink ID="HyperLink36" runat="server" 
            NavigateUrl="~/manager/warehouse/warehouse_add.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">ADD A WAREHOUSE 
        (ENTREP�T )</span></asp:HyperLink>
        &nbsp;
        <asp:HyperLink ID="HyperLink37" runat="server" 
            NavigateUrl="~/manager/warehouse/warehouse_list.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Warehouse list</span></asp:HyperLink>
        &nbsp;
        <asp:HyperLink ID="HyperLink38" runat="server" 
            NavigateUrl="~/manager/appliance/appliance_moving.aspx">APPLIANCE MOVE</asp:HyperLink>
        &nbsp;
                               <asp:HyperLink ID="HyperLink52" runat="server" 
                                   NavigateUrl="~/manager/appliance/appliance_moving_wiz.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Appliance move wizard</span></asp:HyperLink>
        <br />
        <br />
        <asp:HyperLink ID="HyperLink39" runat="server" 
            NavigateUrl="~/manager/appliance/appliance_list.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Appliance list</span></asp:HyperLink>
        &nbsp;&nbsp;
        <asp:HyperLink ID="HyperLink40" runat="server" 
            NavigateUrl="~/manager/property/property_available_units.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">AVAILABLE UNITS</span></asp:HyperLink>
        <br />
        </span><br />
        <asp:HyperLink ID="HyperLink16" runat="server" 
            NavigateUrl="~/mortgage/mortgage_amortization.aspx">Amortization</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:HyperLink ID="HyperLink34" runat="server" 
            NavigateUrl="~/manager/property/property_unit_list.aspx">Update Unit</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;
        <br />
                               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <asp:HyperLink ID="HyperLink20" runat="server" 
            NavigateUrl="~/manager/income/income_late_rent_fees.aspx">Late rent fees 
        payments</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
        <asp:HyperLink ID="HyperLink21" runat="server" 
            NavigateUrl="~/manager/income/income_received_late_rent_fees.aspx">Late rent 
        payment fees received</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:HyperLink ID="HyperLink22" runat="server" 
            NavigateUrl="~/manager/tenant/tenant_cancel_rent_payment.aspx">Cancel Rent 
        Payment</asp:HyperLink>
        <br />
        <br />
        <asp:HyperLink ID="HyperLink24" runat="server" 
            NavigateUrl="~/manager/Financial/financial_analysis.aspx">Financial analysis</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:HyperLink ID="HyperLink25" runat="server" 
            NavigateUrl="~/manager/Financial/financial_analysis_period.aspx">Financial 
        analysis for a period</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
&nbsp;<asp:HyperLink ID="HyperLink26" runat="server" 
            NavigateUrl="~/manager/Financial/financial_analysis_period_list.aspx">List of Financial 
        analysis for a period (views ,update,delete )</asp:HyperLink>
    
        <br />
    
        <br />
        * page
         temporaire</div>
 </asp:Content>
