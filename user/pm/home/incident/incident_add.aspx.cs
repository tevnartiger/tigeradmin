﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : may 28 , 2009
/// </summary>
/// 

public partial class manager_incident_incident_add : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            // First we check if there's home available
            tiger.PM h = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = h.getPMHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
            if (home_count > 0)
            {
                int home_id = h.getPMHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                // link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                //Session["schema_id"]));

                ddl_home_id.Visible = true;

                ddl_home_id.DataSource = h.getPMHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                ddl_home_id.SelectedValue = Convert.ToString(home_id);
                ddl_home_id.DataBind();

                //*********************************************
                // DropDownList pour les Unit
                //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

                if (unit_count > 0)
                {

                    int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), home_id);


                    ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
                    ddl_unit_id.SelectedValue = Convert.ToString(unit_id);
                    ddl_unit_id.DataBind();
                    ddl_unit_id.Items.Insert(0, new ListItem(Resources.Resource.lbl_none, "0"));
                    ddl_unit_id.SelectedIndex = 0;

                }

            }
        }
           
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        tiger.Date df = new tiger.Date();
        DateTime incident_date = Convert.ToDateTime(df.DateCulture(ddl_incident_date_m.SelectedValue, ddl_incident_date_d.SelectedValue, ddl_incident_date_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        incident_date = incident_date.AddMinutes(Convert.ToDouble(ddl_incident_hours.SelectedValue)).AddMinutes(Convert.ToDouble(ddl_incident_minutes.SelectedValue));


        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prIncidentAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

       // try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@new_incident_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_unit_id.SelectedValue);
            cmd.Parameters.Add("@incident_title", SqlDbType.NVarChar, 50).Value = tbx_incident_title.Text;
            cmd.Parameters.Add("@incident_date", SqlDbType.DateTime).Value = incident_date;
            cmd.Parameters.Add("@incident_location", SqlDbType.NVarChar, 50).Value = tbx_incident_location.Text;
            cmd.Parameters.Add("@incident_person_involved", SqlDbType.NVarChar, 200).Value = tbx_incident_person_involved.Text;
            cmd.Parameters.Add("@incident_witnesses", SqlDbType.NVarChar, 200).Value = tbx_incident_witnesses.Text;

            cmd.Parameters.Add("@incident_action_taken", SqlDbType.NVarChar, 4000).Value = tbx_incident_action_taken.Text;
            cmd.Parameters.Add("@incident_description", SqlDbType.NVarChar, 4000).Value = tbx_incident_description.Text;
            
            //execute the insert
            cmd.ExecuteNonQuery();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            int success = Convert.ToInt32(cmd.Parameters["@new_incident_id"].Value);
            if (success > 0)
            {
                Label11.Text =  Resources.Resource.lbl_successfull_add;
                hd_inc_id.Value = success.ToString();

                string url="";
                url = "incident_wo_add.aspx?inc="+success.ToString()+"&h="+ddl_home_id.SelectedValue+"&u="+ddl_unit_id.SelectedValue;


               if(r_add_wo.SelectedValue == "1")
                 Response.Redirect(url, true);

            }

           


        }
       // catch (Exception error)
        {
       //     tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "incident_add.aspx", error.ToString());
        }


    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {
            tiger.Unit u = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
            //change list of unit id

            if (unit_id > 0)
            {
                ddl_unit_id.Visible = true;
                //first dispose of old ddl_unit
                ddl_unit_id.Dispose();
                ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
                ddl_unit_id.DataBind();
                ddl_unit_id.Items.Insert(0, new ListItem(Resources.Resource.lbl_none, "0"));
                ddl_unit_id.SelectedIndex = 0;

            }

    }
   
}
