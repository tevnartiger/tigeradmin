﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : may 28 , 2009
/// </summary>
///
public partial class manager_incident_incident_update : BasePage
{

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            // First we check if there's home available
            tiger.PM h = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = h.getPMHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
            if (home_count > 0)
            {
                int home_id = h.getPMHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                // link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                //Session["schema_id"]));

                ddl_home_id.Visible = true;

                ddl_home_id.DataSource = h.getPMHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                ddl_home_id.SelectedValue = Convert.ToString(home_id);
                ddl_home_id.DataBind();

                //*********************************************
                // DropDownList pour les Unit
                //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

                if (unit_count > 0)
                {

                    int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), home_id);


                    ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
                    ddl_unit_id.SelectedValue = Convert.ToString(unit_id);
                    ddl_unit_id.DataBind();
                    ddl_unit_id.Items.Insert(0, new ListItem(Resources.Resource.lbl_none, "0"));
                    ddl_unit_id.SelectedIndex = 0;

                }

            }



            //-----------------------------------------------------------------------------------


            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prIncidentView", conn);
           
            cmd.CommandType = CommandType.StoredProcedure;
           
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@incident_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["inc_id"]);
            try
            {
                   conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                    tbx_incident_title.Text = dr["incident_title"].ToString();

                    DateTime incident_date = new DateTime();
                    incident_date = Convert.ToDateTime(dr["incident_date"]);
                    if (incident_date != null)
                    {
                        ddl_incident_date_y.SelectedValue = incident_date.Year.ToString();
                        ddl_incident_date_d.Text = incident_date.Day.ToString();
                        ddl_incident_date_m.SelectedValue = incident_date.Month.ToString();

                        ddl_incident_hours.SelectedValue = (incident_date.Hour * 60).ToString();
                        ddl_incident_minutes.SelectedValue = incident_date.Minute.ToString();

                    }



                    ddl_home_id.SelectedValue = dr["home_id"].ToString();
                    ddl_unit_id.SelectedValue = dr["unit_id"].ToString();
                    tbx_incident_location.Text = dr["incident_location"].ToString();
                    tbx_incident_person_involved.Text = dr["incident_person_involved"].ToString();
                    tbx_incident_witnesses.Text = dr["incident_witnesses"].ToString();
                    tbx_incident_description.Text = dr["incident_description"].ToString();
                    tbx_incident_action_taken.Text = dr["incident_action_taken"].ToString();
                    

                }

            }

            finally
            {
                conn.Close();
            }


            // construction of the gridview of the rent payment archive
            tiger.Incident v = new tiger.Incident(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_wo_list.DataSource = v.getIncidentWorkOrderList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["inc_id"]));
            gv_wo_list.DataBind();

        }
       
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        tiger.Date df = new tiger.Date();
        DateTime incident_date = Convert.ToDateTime(df.DateCulture(ddl_incident_date_m.SelectedValue, ddl_incident_date_d.SelectedValue, ddl_incident_date_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        incident_date = incident_date.AddMinutes(Convert.ToDouble(ddl_incident_hours.SelectedValue)).AddMinutes(Convert.ToDouble(ddl_incident_minutes.SelectedValue));


        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prIncidentUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;


        // try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@incident_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["inc_id"]);
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_unit_id.SelectedValue);
            cmd.Parameters.Add("@incident_title", SqlDbType.NVarChar, 50).Value = tbx_incident_title.Text;
            cmd.Parameters.Add("@incident_date", SqlDbType.DateTime).Value = incident_date;
            cmd.Parameters.Add("@incident_location", SqlDbType.NVarChar, 50).Value = tbx_incident_location.Text;
            cmd.Parameters.Add("@incident_person_involved", SqlDbType.NVarChar, 200).Value = tbx_incident_person_involved.Text;
            cmd.Parameters.Add("@incident_witnesses", SqlDbType.NVarChar, 200).Value = tbx_incident_witnesses.Text;

            cmd.Parameters.Add("@incident_action_taken", SqlDbType.NVarChar, 4000).Value = tbx_incident_action_taken.Text;
            cmd.Parameters.Add("@incident_description", SqlDbType.NVarChar, 4000).Value = tbx_incident_description.Text;

            //execute the insert
            cmd.ExecuteNonQuery();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            int success = Convert.ToInt32(cmd.Parameters["@return"].Value);
            if (success == 0)
            {
                Label11.Text = Resources.Resource.lbl_successfull_add;

            }


        }
        // catch (Exception error)
        {
            //     tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "incident_add.aspx", error.ToString());
        }


    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        tiger.Unit u = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
        //change list of unit id

        if (unit_id > 0)
        {
            ddl_unit_id.Visible = true;
            //first dispose of old ddl_unit
            ddl_unit_id.Dispose();
            ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
            ddl_unit_id.DataBind();
            ddl_unit_id.Items.Insert(0, new ListItem(Resources.Resource.lbl_none, "0"));
            ddl_unit_id.SelectedIndex = 0;

        }

    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetPriority(int priority_id)
    {
        string priority = "";

        switch (priority_id)
        {
            case 1:
                priority = Resources.Resource.lbl_urgent;
                break;
            case 2:
                priority = Resources.Resource.lbl_high;
                break;
            case 3:
                priority = Resources.Resource.lbl_medium;
                break;

            case 4:
                priority = Resources.Resource.lbl_low;
                break;
        }

        return priority;
    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetStatus(int status_id)
    {
        string status = "";

        switch (status_id)
        {
            case 1:
                status = Resources.Resource.lbl_pending;
                break;
            case 2:
                status = Resources.Resource.lbl_in_progress;
                break;
            case 3:
                status = Resources.Resource.lbl_completed;
                break;

            case 4:
                status = Resources.Resource.lbl_closed;
                break;
        }

        return status;
    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_cancel_Click(object sender, EventArgs e)
    {

        //*********************************************************************
        //*********************************************************************
        Button btn_cancel = (Button)sender;
        GridViewRow grdRow = (GridViewRow)btn_cancel.Parent.Parent;
        string strField1 = grdRow.Cells[6].Text;

        HiddenField h_wo_id = (HiddenField)grdRow.Cells[6].FindControl("h_wo_id");
        HiddenField h_wo_title = (HiddenField)grdRow.Cells[6].FindControl("h_wo_title");
        HiddenField h_wo_date_begin = (HiddenField)grdRow.Cells[6].FindControl("h_wo_date_begin");
        HiddenField h_home_name = (HiddenField)grdRow.Cells[6].FindControl("h_home_name");

        //  lbl_confirmation.ForeColor = "Red";
        lbl_confirmation.Text = Resources.Resource.lbl_delete_confirmation;

        Label2.Text = Resources.Resource.lbl_property;
        Label4.Text = Resources.Resource.lbl_title;
        Label6.Text = Resources.Resource.lbl_date;

        Label3.Text = ":";
        Label5.Text = ":";
        Label7.Text = ":";

        lbl_title.Text = h_wo_title.Value;
        lbl_date.Text = h_wo_date_begin.Value;
       // lbl_home_name.Text = h_home_name.Value;


        lbl_confirmation.Visible = true;
     //   lbl_home_name.Visible = true;

        lbl_title.Visible = true;
        lbl_date.Visible = true;

        Label2.Visible = true;
        Label3.Visible = true;
        Label4.Visible = true;
        Label5.Visible = true;
        Label6.Visible = true;
        Label7.Visible = true;



        //------------------------------------------------------------------------------------

        // check if the rp_id belong to the account
        AccountObjectAuthorization rpAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!rpAuthorization.WorkOrder(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(h_wo_id.Value)))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prWorkOrderDelete", conn);
        cmd.CommandType = CommandType.StoredProcedure;


        conn.Open();
        //Add the params
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@wo_id", SqlDbType.Int).Value = Convert.ToInt32(h_wo_id.Value);

        //execute the insert
        cmd.ExecuteReader();

        conn.Close();

        //-------------------------------------------------------------------------------------

        // construction of the gridview of the rent payment archive
        tiger.Incident v = new tiger.Incident(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_wo_list.DataSource = v.getIncidentWorkOrderList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["inc_id"]));
        gv_wo_list.DataBind();

    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_wo_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // construction of the gridview of the rent payment archive
        tiger.Incident v = new tiger.Incident(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_wo_list.PageIndex = e.NewPageIndex;
        gv_wo_list.DataSource = v.getIncidentWorkOrderList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["inc_id"]));
        gv_wo_list.DataBind();

    }

    protected void link_add_wo_Click(object sender, EventArgs e)
    {

        string url = "incident_wo_add.aspx?inc=" + Request.QueryString["inc_id"] + "&h=" + ddl_home_id.SelectedValue + "&u=" + ddl_unit_id.SelectedValue;
        Response.Redirect(url);
    }
}
