<%@ Page Language="C#" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" CodeFile="portfolio_home_add.aspx.cs" Inherits="portfolio_group_home_add" Title="New group" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
    ADD-Update a Home/Property to a group<br /><br />
     <div id="txt_message" runat=server></div>
        Properties without a group
        <asp:Repeater runat=server ID="r_home_without_group_list">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
            <tr>
                <td >&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_name")%></td>
            </tr>
        </table>
        </ItemTemplate>
        </asp:Repeater><br />
    
        Group :
    
    <asp:DropDownList ID="ddl_group_home_list" runat=server DataTextField="group_name" DataValueField="group_id"></asp:DropDownList><br /><br />
        Property :
    <asp:DropDownList ID="ddl_home_list" runat=server DataTextField="home_name" DataValueField="home_id"></asp:DropDownList><br /><br />
    <br /><br />
   <asp:Button ID="btn_submit" runat="server" Text="submit" OnClick="btn_submit_Click" />
    </div>
</asp:Content>
