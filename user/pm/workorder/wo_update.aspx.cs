﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.NameObjectAuthorization;

// <summary>
/// Done by : Stanley Jocelyn
/// date    : march 6 , 2009
/// </summary>
/// 

public partial class manager_workorder_wo_update : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RegEx.IsInteger(Request.QueryString["wo_id"]) ||
               !RegEx.IsInteger(Request.QueryString["h_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        NameObjectAuthorization workorderAuthorization = new NameObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!workorderAuthorization.HomeWorkOrder(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["h_id"]), Convert.ToInt32(Request.QueryString["wo_id"]), Convert.ToInt32(Session["name_id"]), 6))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////


        if (!Page.IsPostBack)
        {
           
            reg_company_city.ValidationExpression = RegEx.getText();
            reg_company_addr_street.ValidationExpression = RegEx.getText();
            reg_company_contact_email.ValidationExpression = RegEx.getEmail();
            reg_company_contact_fname.ValidationExpression = RegEx.getText();
            reg_company_contact_lname.ValidationExpression = RegEx.getText();
            reg_company_contact_tel.ValidationExpression = RegEx.getText();
            reg_company_name.ValidationExpression = RegEx.getText();
            reg_company_pc.ValidationExpression = RegEx.getText();
            reg_company_tel.ValidationExpression = RegEx.getText();
            reg_company_website.ValidationExpression = RegEx.getText();
            reg_company_prov.ValidationExpression = RegEx.getText();


            reg_tbx_task_cost_estimation.ValidationExpression = RegEx.getMoney();
            reg_tbx_task_title.ValidationExpression = RegEx.getText();
            reg_tbx_wo_title.ValidationExpression = RegEx.getText();
            reg_tbx_task_cost.ValidationExpression = RegEx.getText();
  
            
            Panel1.Visible = false;
            // task_panel.Visible = false;

            MultiView1.ActiveViewIndex = 0;

            // First we check if there's home available
            tiger.PM h = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = h.getPMHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
            if (home_count > 0)
            {
                int home_id = h.getPMHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                
                ddl_home_id.Visible = true;

                ddl_home_id.DataSource = h.getPMHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                ddl_home_id.DataBind();
              
                //*********************************************
                // DropDownList pour les Unit
                //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

                if (unit_count > 0)
                {

                    int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), home_id);


                    ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]),Convert.ToInt32( Request.QueryString["h_id"]));
                    ddl_unit_id.DataBind();
                    ddl_unit_id.Items.Insert(0, new ListItem(Resources.Resource.lbl_none, "-1"));
                    ddl_unit_id.SelectedIndex = 0;

                }

               }


            //-----------  get contactor list and a work order  ----------------------

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prCompanySearch", conn);
            SqlCommand cmd2 = new SqlCommand("prWorkOrderView", conn);
            SqlCommand cmd3 = new SqlCommand("prTaskList", conn);
            SqlCommand cmd4 = new SqlCommand("prIncidentView", conn);
           

            cmd.CommandType = CommandType.StoredProcedure;
            cmd2.CommandType = CommandType.StoredProcedure;
            cmd3.CommandType = CommandType.StoredProcedure;
            cmd4.CommandType = CommandType.StoredProcedure;

 
               conn.Open();


                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@company_all", SqlDbType.Bit).Value = 1;
                cmd.Parameters.Add("@company_general_contractor", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_interior_contractor", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_exterior_contractor", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_cleaning", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_painting", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_paving", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_plumbing", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_decoration", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_doors_windows", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_bricks", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_foundation", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_alarms_security_systems", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_cable_satellite_dish", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_ciment", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_other", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_hvac", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_engineer", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_gypse_installation", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_architech", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_gardening", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_roofs", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_flooring", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_basement", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_inspection", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_kitchen", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_electrical", SqlDbType.Bit).Value = 0;
                cmd.Parameters.Add("@company_locksmith", SqlDbType.Bit).Value = 0;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                ddl_task_contractor.DataSource = ds;
                ddl_task_contractor.DataBind();
                ddl_task_contractor.Items.Insert(0, new ListItem(Resources.Resource.lbl_none, "-1"));
                ddl_task_contractor.SelectedIndex = 0;




                cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd2.Parameters.Add("@wo_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["wo_id"]); ;
               
                SqlDataReader dr2 = null;
                dr2 = cmd2.ExecuteReader();

                while (dr2.Read() == true)
                {
                   ddl_home_id.SelectedValue = dr2["home_id"].ToString();
                   ddl_unit_id.SelectedValue = dr2["unit_id"].ToString();
                   tbx_wo_title.Text = dr2["wo_title"].ToString();
                   tbx_wo_location.Text = dr2["wo_location"].ToString();
                   ddl_wo_priority.SelectedValue = dr2["wo_priority"].ToString();
                   ddl_wo_status.SelectedValue = dr2["wo_status"].ToString();

                   ddl_wo_date_begin_m.SelectedValue = Convert.ToDateTime(dr2["wo_date_begin"]).Month.ToString();
                   ddl_wo_date_begin_d.SelectedValue = Convert.ToDateTime(dr2["wo_date_begin"]).Day.ToString();
                   ddl_wo_date_begin_y.SelectedValue = Convert.ToDateTime(dr2["wo_date_begin"]).Year.ToString();

                   ddl_wo_exp_date_end_m.SelectedValue = Convert.ToDateTime(dr2["wo_exp_date_end"]).Month.ToString();
                   ddl_wo_exp_date_end_d.SelectedValue = Convert.ToDateTime(dr2["wo_exp_date_end"]).Day.ToString();
                   ddl_wo_exp_date_end_y.SelectedValue = Convert.ToDateTime(dr2["wo_exp_date_end"]).Year.ToString();

                   if (DBNull.Value == dr2["wo_date_end"])
                   {}
                   else
                   {
                       ddl_wo_date_end_m.SelectedValue = Convert.ToDateTime(dr2["wo_date_end"]).Month.ToString();
                       ddl_wo_date_end_d.SelectedValue = Convert.ToDateTime(dr2["wo_date_end"]).Day.ToString();
                       ddl_wo_date_end_y.SelectedValue = Convert.ToDateTime(dr2["wo_date_end"]).Year.ToString();

                    
                   
                   }
                }


                //Add the params
                cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd3.Parameters.Add("@wo_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["wo_id"]);


                SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
                DataTable ds3 = new DataTable();
                da3.Fill(ds3);

                gv_task.DataSource = ds3;
                gv_task.DataBind();




                cmd4.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd4.Parameters.Add("@incident_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["inc"]);

                SqlDataReader dr4 = null;
                dr4 = cmd4.ExecuteReader(CommandBehavior.SingleRow);

                while (dr4.Read() == true)
                {
                    lbl_incident_title.Text = dr4["incident_title"].ToString();

                }

                lbl_incident_title0.Text = lbl_incident_title.Text;

            // finally
            {
                conn.Close();
            }


            hd_last_wo_status.Value = ddl_wo_status.SelectedValue;


            if (Request.QueryString["inc"] == "0")
            {
                lbl_wo_incident_related.Visible = false;
                lbl_wo_incident_related0.Visible = false;
                br_incident1.Visible = false;
                br_incident2.Visible = false;
                br_incident3.Visible = false;
                br_incident4.Visible = false;
                br_incident5.Visible = false;
                br_incident6.Visible = false;
                tb_incident.Visible = false;
                tb_incident0.Visible = false;


            }

        }

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        tiger.Unit u = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
        //change list of unit id

        if (unit_id > 0)
        {
            ddl_unit_id.Visible = true;
            //first dispose of old ddl_unit
            ddl_unit_id.Dispose();
            ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
            ddl_unit_id.DataBind();
            ddl_unit_id.Items.Insert(0, new ListItem(Resources.Resource.lbl_none, "-1"));
            ddl_unit_id.SelectedIndex = 0;

        }

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        Page.Validate("vg_task");
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        
        string task_type_work = "";

        if (company_cleaning.Checked == true && tb_contractor.Visible == true)
            task_type_work = "1";
        else
            task_type_work = "0";


        if (company_painting.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_paving.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_plumbing.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_decoration.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";


        if (company_doors_windows.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";


        if (company_bricks.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";


        if (company_foundation.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_alarms_security_systems.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_cable_satellite_dish.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_ciment.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_other.Checked == true && tb_contractor.Visible == true)

            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_hvac.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_engineer.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_gypse_installation.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_architech.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_gardening.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_roofs.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_flooring.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_basement.Checked == true && tb_contractor.Visible == true)

            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";


        if (company_inspection.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_kitchen.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_electrical.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_locksmith.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        int invalid = 0;


        //-------------------- validation section begin -----------------------------

        lbl_exp_date_end_greater.Visible = false;

        tiger.Date df = new tiger.Date();
        DateTime date_begin = Convert.ToDateTime(df.DateCulture(ddl_task_date_begin_m.SelectedValue, ddl_task_date_begin_d.SelectedValue, ddl_task_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        DateTime exp_date_end = Convert.ToDateTime(df.DateCulture(ddl_task_exp_date_end_m.SelectedValue, ddl_task_exp_date_end_d.SelectedValue, ddl_task_exp_date_end_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        if (date_begin > exp_date_end)
        {
            lbl_exp_date_end_greater.Visible = true;
            invalid++;
        }

        //-------------------- validation section begin -----------------------------

        if(invalid == 0)
        {
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prTaskAdd", conn);
            SqlCommand cmd2 = new SqlCommand("prTaskList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd2.CommandType = CommandType.StoredProcedure;




            //  try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@task_no", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@wo_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["wo_id"]);
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.NVarChar, 15).Value = Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@task_title", SqlDbType.NVarChar, 50).Value = RegEx.getText(tbx_task_title.Text);
                cmd.Parameters.Add("@task_status", SqlDbType.Int).Value = Convert.ToInt32(ddl_task_status.SelectedValue);
                cmd.Parameters.Add("@task_date_begin", SqlDbType.DateTime).Value = date_begin;
                cmd.Parameters.Add("@task_exp_date_end", SqlDbType.DateTime).Value = exp_date_end;
                cmd.Parameters.Add("@task_priority", SqlDbType.Int).Value = Convert.ToInt32(ddl_priority.SelectedValue);
                cmd.Parameters.Add("@task_type_work", SqlDbType.NVarChar, 50).Value = task_type_work;
                cmd.Parameters.Add("@task_description", SqlDbType.NVarChar, 4000).Value = RegEx.getText(tbx_task_description.Text);
                cmd.Parameters.Add("@task_notes", SqlDbType.NVarChar, 4000).Value = RegEx.getText(tbx_task_notes.Text);
                cmd.Parameters.Add("@company_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_task_contractor.SelectedValue);
                cmd.Parameters.Add("@task_cost_estimation", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_task_cost_estimation.Text));

                //execute the insert
                cmd.ExecuteNonQuery();
                //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

                if (Convert.ToInt32(cmd.Parameters["@return_id"].Value) == 0)
                    Label11.Text = Resources.Resource.lbl_successfull_add;

                tbx_task_title.Text = Resources.Resource.lbl_task + " " + cmd.Parameters["@task_no"].Value.ToString();

                //----------------------------------------------------------------------------------------------
                //----------------------------------------------------------------------------------------------
                //Add the params
                cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd2.Parameters.Add("@wo_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["wo_id"]);


                SqlDataAdapter da = new SqlDataAdapter(cmd2);
                DataTable ds = new DataTable();
                da.Fill(ds);

                gv_task2.DataSource = ds;
                gv_task2.DataBind();

                conn.Close();
                MaintainScrollPositionOnPostBack = false;
            }


            tbx_task_description.Text = "";
            tbx_task_notes.Text = "";
            tbx_task_cost_estimation.Text = "";
            tbx_task_cost.Text = "";

            ddl_task_status.SelectedIndex = 0;
            ddl_task_contractor.SelectedIndex = 0;

            company_cleaning.Checked = false;
            company_painting.Checked = false;
            company_paving.Checked = false;
            company_plumbing.Checked = false;
            company_decoration.Checked = false;
            company_doors_windows.Checked = false;
            company_bricks.Checked = false;
            company_foundation.Checked = false;
            company_alarms_security_systems.Checked = false;
            company_cable_satellite_dish.Checked = false;
            company_ciment.Checked = false;
            company_other.Checked = false;
            company_hvac.Checked = false;
            company_engineer.Checked = false;
            company_gypse_installation.Checked = false;
            company_architech.Checked = false;
            company_gardening.Checked = false;
            company_roofs.Checked = false;
            company_flooring.Checked = false;
            company_basement.Checked = false;
            company_inspection.Checked = false;
            company_kitchen.Checked = false;
            company_electrical.Checked = false;
            company_locksmith.Checked = false;

        }
        MaintainScrollPositionOnPostBack = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Button1_Click(object sender, EventArgs e)
    {

        Page.Validate("vg_company");
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        string new_company_id = "";
        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prCompanyAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        //  try
        {
            conn.Open();
            //Add the params

            cmd.Parameters.Add("@new_company_id", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.NVarChar, 15).Value = Request.UserHostAddress.ToString();



            if (radio_contractor.SelectedValue == "1")
                cmd.Parameters.Add("@company_type", SqlDbType.Char, 1).Value = "b";
            else
                cmd.Parameters.Add("@company_type", SqlDbType.Char, 1).Value = "c";

            cmd.Parameters.Add("@company_name", SqlDbType.NVarChar, 50).Value = RegEx.getText(company_name.Text);
            cmd.Parameters.Add("@company_website", SqlDbType.NVarChar, 200).Value = RegEx.getText(company_website.Text);
            // cmd.Parameters.Add("@company_addr_no", SqlDbType.NVarChar, 50).Value = company_addr_no.Text;
            cmd.Parameters.Add("@company_addr_street", SqlDbType.NVarChar, 200).Value = RegEx.getText(company_addr_street.Text);
            cmd.Parameters.Add("@company_city", SqlDbType.NVarChar, 50).Value = RegEx.getText(company_city.Text);
            cmd.Parameters.Add("@company_prov", SqlDbType.NVarChar, 50).Value = RegEx.getText(company_prov.Text);
            cmd.Parameters.Add("@company_pc", SqlDbType.NVarChar, 50).Value = RegEx.getText(company_pc.Text);
            cmd.Parameters.Add("@company_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(company_tel.Text);
            cmd.Parameters.Add("@company_contact_fname", SqlDbType.NVarChar, 50).Value = RegEx.getText(company_contact_fname.Text);
            cmd.Parameters.Add("@company_contact_lname", SqlDbType.NVarChar, 50).Value = RegEx.getText(company_contact_lname.Text);
            cmd.Parameters.Add("@company_contact_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(company_contact_tel.Text);
            cmd.Parameters.Add("@company_contact_email", SqlDbType.NVarChar, 50).Value = RegEx.getText(company_contact_email.Text);


            if (company_general_contractor.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_general_contractor", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_general_contractor", SqlDbType.Bit).Value = 0;

            if (company_interior_contractor.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_interior_contractor", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_interior_contractor", SqlDbType.Bit).Value = 0;

            if (company_exterior_contractor.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_exterior_contractor", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_exterior_contractor", SqlDbType.Bit).Value = 0;

            if (company_cleaning.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_cleaning", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_cleaning", SqlDbType.Bit).Value = 0;


            if (company_painting.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_painting", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_painting", SqlDbType.Bit).Value = 0;

            if (company_paving.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_paving", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_paving", SqlDbType.Bit).Value = 0;


            if (company_plumbing.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_plumbing", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_plumbing", SqlDbType.Bit).Value = 0;


            if (company_decoration.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_decoration", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_decoration", SqlDbType.Bit).Value = 0;

            if (company_doors_windows.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_doors_windows", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_doors_windows", SqlDbType.Bit).Value = 0;

            if (company_bricks.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_bricks", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_bricks", SqlDbType.Bit).Value = 0;

            if (company_foundation.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_foundation", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_foundation", SqlDbType.Bit).Value = 0;

            if (company_alarms_security_systems.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_alarms_security_systems", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_alarms_security_systems", SqlDbType.Bit).Value = 0;

            if (company_cable_satellite_dish.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_cable_satellite_dish", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_cable_satellite_dish", SqlDbType.Bit).Value = 0;

            if (company_ciment.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_ciment", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_ciment", SqlDbType.Bit).Value = 0;

            if (company_other.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_other", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_other", SqlDbType.Bit).Value = 0;

            if (company_hvac.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_hvac", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_hvac", SqlDbType.Bit).Value = 0;

            if (company_engineer.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_engineer", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_engineer", SqlDbType.Bit).Value = 0;

            if (company_gypse_installation.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_gypse_installation", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_gypse_installation", SqlDbType.Bit).Value = 0;

            if (company_architech.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_architech", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_architech", SqlDbType.Bit).Value = 0;

            if (company_gardening.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_gardening", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_gardening", SqlDbType.Bit).Value = 0;

            if (company_roofs.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_roofs", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_roofs", SqlDbType.Bit).Value = 0;

            if (company_flooring.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_flooring", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_flooring", SqlDbType.Bit).Value = 0;

            if (company_basement.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_basement", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_basement", SqlDbType.Bit).Value = 0;

            if (company_inspection.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_inspection", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_inspection", SqlDbType.Bit).Value = 0;

            if (company_kitchen.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_kitchen", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_kitchen", SqlDbType.Bit).Value = 0;

            if (company_electrical.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_electrical", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_electrical", SqlDbType.Bit).Value = 0;

            if (company_locksmith.Checked == true && tb_contractor.Visible == true)
                cmd.Parameters.Add("@company_locksmith", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_locksmith", SqlDbType.Bit).Value = 0;


            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            if (Convert.ToInt32(cmd.Parameters["@return_id"].Value) == 0)
                result.InnerHtml = Resources.Resource.lbl_successfull_add;

            new_company_id = Convert.ToString(cmd.Parameters["@new_company_id"].Value);
        }



        //------------------------------------------------------------------------------------



        SqlCommand cmd2 = new SqlCommand("prCompanySearch", conn);
        cmd2.CommandType = CommandType.StoredProcedure;


        // try
        {
            // conn.Open();


            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@company_all", SqlDbType.Bit).Value = 1;
            cmd2.Parameters.Add("@company_general_contractor", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_interior_contractor", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_exterior_contractor", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_cleaning", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_painting", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_paving", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_plumbing", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_decoration", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_doors_windows", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_bricks", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_foundation", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_alarms_security_systems", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_cable_satellite_dish", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_ciment", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_other", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_hvac", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_engineer", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_gypse_installation", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_architech", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_gardening", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_roofs", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_flooring", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_basement", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_inspection", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_kitchen", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_electrical", SqlDbType.Bit).Value = 0;
            cmd2.Parameters.Add("@company_locksmith", SqlDbType.Bit).Value = 0;

            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataSet ds = new DataSet();
            da.Fill(ds);

            ddl_task_contractor.DataSource = ds;
            ddl_task_contractor.DataBind();
            ddl_task_contractor.Items.Insert(0, new ListItem(Resources.Resource.lbl_none, "-1"));
            ddl_task_contractor.SelectedValue = new_company_id;

        }

        // finally
        {
            conn.Close();
        }

        Panel1.Visible = false;


    }

    protected void link_company_add_Click(object sender, EventArgs e)
    {
        Panel1.Visible = true;
    }
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Panel1.Visible = false;
    }



    protected void btn_step_2_Click(object sender, EventArgs e)
    {
        Page.Validate("vg_wo");
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }


        int number_task_incomplete = 0;

        for (int j = 0; j < gv_task.Rows.Count; j++)
        {

            HiddenField h_task_status = (HiddenField)gv_task.Rows[j].FindControl("h_task_status");

            if (h_task_status.Value != "4")
            {
                number_task_incomplete++;

            }
        }


        if (number_task_incomplete > 0 && ddl_wo_status.SelectedValue == "4")
        {
            ddl_wo_status.SelectedValue = hd_last_wo_status.Value;
            lbl_close_all_task.Visible = true;
        }
        else
        {
            hd_last_wo_status.Value = ddl_wo_status.SelectedValue;
            lbl_close_all_task.Visible = false;
        }


        ddl_task_date_begin_m.SelectedValue = ddl_wo_date_begin_m.SelectedValue;
        ddl_task_date_begin_d.SelectedValue = ddl_wo_date_begin_d.SelectedValue;
        ddl_task_date_begin_y.SelectedValue = ddl_wo_date_begin_y.SelectedValue;

        ddl_task_exp_date_end_m.SelectedValue = ddl_wo_exp_date_end_m.SelectedValue;
        ddl_task_exp_date_end_d.SelectedValue = ddl_wo_exp_date_end_d.SelectedValue;
        ddl_task_exp_date_end_y.SelectedValue = ddl_wo_exp_date_end_y.SelectedValue;

        ddl_task_status.SelectedValue = ddl_wo_status.SelectedValue;

        lbl_wo_title.Text = tbx_wo_title.Text;
        lbl_home_name.Text = ddl_home_id.SelectedItem.Text;
        lbl_unit_door_no.Text = ddl_unit_id.SelectedItem.Text;

        lbl_exact_location.Text = tbx_wo_location.Text;

        lbl_wo_priority.Text = ddl_wo_priority.SelectedItem.Text;
        lbl_wo_status.Text = ddl_wo_status.SelectedItem.Text;

        lbl_wo_date_begin.Text = ddl_wo_date_begin_m.SelectedItem.Text + " - "
                                 + ddl_wo_date_begin_d.SelectedValue + " - " + ddl_wo_date_begin_y.SelectedValue;

        lbl_wo_exp_date_end.Text = "";

        if (ddl_wo_exp_date_end_m.SelectedIndex > 0 && ddl_wo_exp_date_end_d.SelectedIndex > 0 && ddl_wo_exp_date_end_y.SelectedIndex > 0)
        {
            lbl_wo_exp_date_end.Text = ddl_wo_exp_date_end_m.SelectedItem.Text + " - "
                                     + ddl_wo_exp_date_end_d.SelectedValue + " - " + ddl_wo_exp_date_end_y.SelectedValue;
        }


        // update a work order


        DateTime exp_date_end = new DateTime();

        tiger.Date df = new tiger.Date();
        DateTime date_begin = Convert.ToDateTime(df.DateCulture(ddl_wo_date_begin_m.SelectedValue, ddl_wo_date_begin_d.SelectedValue, ddl_wo_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        if (ddl_wo_exp_date_end_m.SelectedIndex > 0 && ddl_wo_exp_date_end_d.SelectedIndex > 0 && ddl_wo_exp_date_end_y.SelectedIndex > 0)
            exp_date_end = Convert.ToDateTime(df.DateCulture(ddl_wo_exp_date_end_m.SelectedValue, ddl_wo_exp_date_end_d.SelectedValue, ddl_wo_exp_date_end_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        else
            exp_date_end = date_begin;

        int invalid = 0;
        //-------------------- validation section begin -----------------------------

        lbl_date_end_greater0.Visible = false;
        lbl_exp_date_end_greater0.Visible = false;


        if (ddl_wo_date_end_m.SelectedIndex > 0 && ddl_wo_date_end_d.SelectedIndex > 0 && ddl_wo_date_end_y.SelectedIndex > 0)
        {
            DateTime date_end = Convert.ToDateTime(df.DateCulture(ddl_wo_date_end_m.SelectedValue, ddl_wo_date_end_d.SelectedValue, ddl_wo_date_end_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

            if (date_begin > date_end)
            {
                lbl_date_end_greater0.Visible = true;
                invalid++;
            }
        }

        if (date_begin > exp_date_end)
        {
            lbl_exp_date_end_greater0.Visible = true;
            invalid++;
        }


        lbl_must_select_date_before_closing0.Visible = false;
        if (ddl_wo_status.SelectedValue == "4")
        {
            if (ddl_wo_date_end_m.SelectedIndex == 0 || ddl_wo_date_end_d.SelectedIndex == 0 || ddl_wo_date_end_y.SelectedIndex == 0)
            {
                invalid++;
                lbl_must_select_date_before_closing0.Visible = true;
            }

       }
        //---------------------------------------------------------------------------------------

        if(invalid == 0)
        {
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prWorkOrderUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@wo_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["wo_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.NVarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@wo_title", SqlDbType.NVarChar, 50).Value = RegEx.getText(tbx_wo_title.Text);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_unit_id.SelectedValue);
            cmd.Parameters.Add("@wo_priority", SqlDbType.Int).Value = Convert.ToInt32(ddl_wo_priority.SelectedValue);
            cmd.Parameters.Add("@wo_status", SqlDbType.Int).Value = Convert.ToInt32(ddl_wo_status.SelectedValue);
            cmd.Parameters.Add("@wo_location", SqlDbType.NVarChar, 200).Value = RegEx.getText(tbx_wo_location.Text);
            cmd.Parameters.Add("@wo_date_begin", SqlDbType.DateTime).Value = date_begin;
            cmd.Parameters.Add("@wo_exp_date_end", SqlDbType.DateTime).Value = exp_date_end;


            if (ddl_wo_date_end_m.SelectedIndex > 0 && ddl_wo_date_end_d.SelectedIndex > 0 && ddl_wo_date_end_y.SelectedIndex > 0)
            {
                cmd.Parameters.Add("@wo_date_end", SqlDbType.DateTime).Value = Convert.ToDateTime(df.DateCulture(ddl_wo_date_begin_m.SelectedValue, ddl_wo_date_end_d.SelectedValue, ddl_wo_date_end_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            }
            else
                cmd.Parameters.Add("@wo_date_end", SqlDbType.DateTime).Value = DBNull.Value;

            //execute the insert
            cmd.ExecuteNonQuery();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            if (Convert.ToInt32(cmd.Parameters["@return_id"].Value) == 0)
                Label11.Text = Resources.Resource.lbl_successfull_add;

            //
            //Label28.Text = cmd.Parameters["@new_wo_id"].Value.ToString();

            conn.Close();
        }
        if (invalid > 0)
            MaintainScrollPositionOnPostBack = false;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetPriority(int priority_id)
    {
        string priority = "";

        switch (priority_id)
        {
            case 1:
                priority = Resources.Resource.lbl_urgent;
                break;
            case 2:
                priority = Resources.Resource.lbl_high;
                break;
            case 3:
                priority = Resources.Resource.lbl_medium;
                break;

            case 4:
                priority = Resources.Resource.lbl_low;
                break;
        }

        return priority;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetStatus(int status_id)
    {
        string status = "";

        switch (status_id)
        {
            case 1:
                status = Resources.Resource.lbl_pending;
                break;
            case 2:
                status = Resources.Resource.lbl_in_progress;
                break;
            case 3:
                status = Resources.Resource.lbl_completed;
                break;

            case 4:
                status = Resources.Resource.lbl_closed;
                break;
        }

        return status;
    }


    protected void btn_cancel_task_Click(object sender, EventArgs e)
    {


        //*********************************************************************
        //*********************************************************************
        Button btn_cancel = (Button)sender;
        GridViewRow grdRow = (GridViewRow)btn_cancel.Parent.Parent;

        //  string strField1 = grdRow.Cells[6].Text;

        HiddenField h_task_id = (HiddenField)grdRow.Cells[8].FindControl("h_task_id");

        //------------------------------------------------------------------------------------

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prTaskDelete", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@task_id", SqlDbType.Int).Value = Convert.ToInt32(h_task_id.Value);

            //execute the insert
            cmd.ExecuteReader();

        }
        catch (Exception error)
        {
            //  tiger.security.Error.errorAdd(conn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
        }



        tiger.Task hp = new tiger.Task(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        gv_task.DataSource = hp.getTaskList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["wo_id"]));
        gv_task.DataBind();

        gv_task2.DataSource = gv_task.DataSource;
        gv_task2.DataBind();



    }


    protected void link_btn_update_task_Click(object sender, EventArgs e)
    {

        MultiView1.ActiveViewIndex = 1;

        btn_submit.Visible = false;
        btn_update.Visible = true;

        lbl_wo_submit.Visible = false;
        lbl_wo_update.Visible = true;


        tr_task_final_date_end.Visible = true;
        tr_task_final_cost.Visible = true;

        lbl_wo_title.Text = tbx_wo_title.Text;
        lbl_home_name.Text = ddl_home_id.SelectedItem.Text;
        lbl_unit_door_no.Text = ddl_unit_id.SelectedItem.Text;

        lbl_exact_location.Text = tbx_wo_location.Text;

        lbl_wo_priority.Text = ddl_wo_priority.SelectedItem.Text;
        lbl_wo_status.Text = ddl_wo_status.SelectedItem.Text;

        lbl_wo_date_begin.Text = ddl_wo_date_begin_m.SelectedItem.Text + " - "
                                 + ddl_wo_date_begin_d.SelectedValue + " - " + ddl_wo_date_begin_y.SelectedValue;

        lbl_wo_exp_date_end.Text = "";

        if (ddl_wo_exp_date_end_m.SelectedIndex > 0 && ddl_wo_exp_date_end_d.SelectedIndex > 0 && ddl_wo_exp_date_end_y.SelectedIndex > 0)
        {
            lbl_wo_exp_date_end.Text = ddl_wo_exp_date_end_m.SelectedItem.Text + " - "
                                     + ddl_wo_exp_date_end_d.SelectedValue + " - " + ddl_wo_exp_date_end_y.SelectedValue;
        }


        //*********************************************************************
        LinkButton link_btn_update_task = (LinkButton)sender;
        GridViewRow grdRow = (GridViewRow)link_btn_update_task.Parent.Parent;

        //  string strField1 = grdRow.Cells[6].Text;

        HiddenField h_task_id = (HiddenField)grdRow.Cells[8].FindControl("h_task_id");

        int task_id = Convert.ToInt32(h_task_id.Value);


        // this is the hiddenfield at the bottom of the markup page , we will use it with the update button
        //    protected void btn_update_Click(object sender, EventArgs e)

        hd_task_id.Value = task_id.ToString();
        
        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prTaskView", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        conn.Open();
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@task_id", SqlDbType.Int).Value = task_id ;
               
        SqlDataReader dr = null;
        dr = cmd.ExecuteReader();

        while (dr.Read() == true)
        {
            tbx_task_title.Text = dr["task_title"].ToString();

            ddl_task_date_begin_m.SelectedValue = Convert.ToDateTime(dr["task_date_begin"]).Month.ToString();
            ddl_task_date_begin_d.SelectedValue = Convert.ToDateTime(dr["task_date_begin"]).Day.ToString();
            ddl_task_date_begin_y.SelectedValue = Convert.ToDateTime(dr["task_date_begin"]).Year.ToString();

            ddl_task_exp_date_end_m.SelectedValue = Convert.ToDateTime(dr["task_exp_date_end"]).Month.ToString();
            ddl_task_exp_date_end_d.SelectedValue = Convert.ToDateTime(dr["task_exp_date_end"]).Day.ToString();
            ddl_task_exp_date_end_y.SelectedValue = Convert.ToDateTime(dr["task_exp_date_end"]).Year.ToString();


            if (dr["task_date_end"] == DBNull.Value)
            { }
            else
            {
                ddl_task_date_end_m.SelectedValue = Convert.ToDateTime(dr["task_date_end"]).Month.ToString();
                ddl_task_date_end_d.SelectedValue = Convert.ToDateTime(dr["task_date_end"]).Day.ToString();
                ddl_task_date_end_y.SelectedValue = Convert.ToDateTime(dr["task_date_end"]).Year.ToString();
            }
    

            ddl_task_status.SelectedValue = dr["task_status"].ToString();
            ddl_priority.SelectedValue = dr["task_priority"].ToString();

            ddl_task_contractor.SelectedValue = dr["company_id"].ToString();
           
            tbx_task_notes.Text = dr["task_notes"].ToString();
            tbx_task_description.Text = dr["task_description"].ToString();
            tbx_task_cost_estimation.Text = dr["task_cost_estimation"].ToString();

            if (dr["task_cost"] == DBNull.Value)
                tbx_task_cost.Text = "";
            else
                tbx_task_cost.Text = dr["task_cost"].ToString();


            // this is the category of task being done by a company
            string[] task_type_work = dr["task_type_work"].ToString().Split(',');


            if (task_type_work[0] == "1")
                company_cleaning.Checked = true;
            else
                company_cleaning.Checked = false;


            if (task_type_work[1] == "1")
                company_painting.Checked = true;
            else
                company_painting.Checked = false;


            if (task_type_work[2] == "1")
                company_paving.Checked = true;
            else
                company_paving.Checked = false;




            if (task_type_work[3] == "1")
                company_plumbing.Checked = true;
            else
                company_plumbing.Checked = false;


            if (task_type_work[4] == "1")
                company_decoration.Checked = true;
            else
                company_decoration.Checked = false;


            if (task_type_work[5] == "1")
                company_doors_windows.Checked = true;
            else
                company_doors_windows.Checked = false;


            if (task_type_work[6] == "1")
                company_bricks.Checked = true;
            else
                company_bricks.Checked = false;


            if (task_type_work[7] == "1")
                company_foundation.Checked = true;
            else
                company_foundation.Checked = false;



            if (task_type_work[8] == "1")
                company_alarms_security_systems.Checked = true;
            else
                company_alarms_security_systems.Checked = false;




            if (task_type_work[9] == "1")
                company_cable_satellite_dish.Checked = true;
            else
                company_cable_satellite_dish.Checked = false;


            if (task_type_work[10] == "1")
                company_ciment.Checked = true;
            else
                company_ciment.Checked = false;



            if (task_type_work[11] == "1")
                company_other.Checked = true;
            else
                company_other.Checked = false;


            if (task_type_work[12] == "1")
                company_hvac.Checked = true;
            else
                company_hvac.Checked = false;


            if (task_type_work[13] == "1")
                company_engineer.Checked = true;
            else
                company_engineer.Checked = false;




            if (task_type_work[14] == "1")
                company_gypse_installation.Checked = true;
            else
                company_gypse_installation.Checked = false;


            if (task_type_work[15] == "1")
                company_architech.Checked = true;
            else
                company_architech.Checked = false;





            if (task_type_work[16] == "1")
                company_gardening.Checked = true;
            else
                company_gardening.Checked = false;



            if (task_type_work[17] == "1")
                company_roofs.Checked = true;
            else
                company_roofs.Checked = false;




            if (task_type_work[18] == "1")
                company_flooring.Checked = true;
            else
                company_flooring.Checked = false;


            if (task_type_work[19] == "1")
                company_basement.Checked = true;
            else
                company_basement.Checked = false;





            if (task_type_work[20] == "1")
                company_inspection.Checked = true;
            else
                company_inspection.Checked = false;



            if (task_type_work[21] == "1")
                company_kitchen.Checked = true;
            else
                company_kitchen.Checked = false;


            if (task_type_work[22] == "1")
                company_electrical.Checked = true;
            else
                company_electrical.Checked = false;


            if (task_type_work[23] == "1")
                company_locksmith.Checked = true;
            else
                company_locksmith.Checked = false;


        }

        conn.Close();




       
    }


    protected void link_btn_new_task_Click(object sender, EventArgs e)
    {
        tr_task_final_cost.Visible = false;
        tr_task_final_date_end.Visible = false;
        
        MultiView1.ActiveViewIndex = 1;

        btn_submit.Visible = true;
        btn_update.Visible = false;

        lbl_wo_submit.Visible = true;
        lbl_wo_update.Visible = false;


        ddl_task_date_begin_m.SelectedValue = ddl_wo_date_begin_m.SelectedValue;
        ddl_task_date_begin_d.SelectedValue = ddl_wo_date_begin_d.SelectedValue;
        ddl_task_date_begin_y.SelectedValue = ddl_wo_date_begin_y.SelectedValue;

        ddl_task_exp_date_end_m.SelectedValue = ddl_wo_exp_date_end_m.SelectedValue;
        ddl_task_exp_date_end_d.SelectedValue = ddl_wo_exp_date_end_d.SelectedValue;
        ddl_task_exp_date_end_y.SelectedValue = ddl_wo_exp_date_end_y.SelectedValue;

        ddl_task_status.SelectedValue = ddl_wo_status.SelectedValue;

        lbl_wo_title.Text = tbx_wo_title.Text;
        lbl_home_name.Text = ddl_home_id.SelectedItem.Text;
        lbl_unit_door_no.Text = ddl_unit_id.SelectedItem.Text;

        lbl_exact_location.Text = tbx_wo_location.Text;

        lbl_wo_priority.Text = ddl_wo_priority.SelectedItem.Text;
        lbl_wo_status.Text = ddl_wo_status.SelectedItem.Text;

        lbl_wo_date_begin.Text = ddl_wo_date_begin_m.SelectedItem.Text + " - "
                                 + ddl_wo_date_begin_d.SelectedValue + " - " + ddl_wo_date_begin_y.SelectedValue;

        lbl_wo_exp_date_end.Text = "";

        if (ddl_wo_exp_date_end_m.SelectedIndex > 0 && ddl_wo_exp_date_end_d.SelectedIndex > 0 && ddl_wo_exp_date_end_y.SelectedIndex > 0)
        {
            lbl_wo_exp_date_end.Text = ddl_wo_exp_date_end_m.SelectedItem.Text + " - "
                                     + ddl_wo_exp_date_end_d.SelectedValue + " - " + ddl_wo_exp_date_end_y.SelectedValue;
        }


        tbx_task_title.Text = Resources.Resource.lbl_task + " ";

        
       
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_update_Click(object sender, EventArgs e)
    {
        Page.Validate("vg_task");
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        
        string task_type_work = "";

        if (company_cleaning.Checked == true && tb_contractor.Visible == true)
            task_type_work = "1";
        else
            task_type_work = "0";


        if (company_painting.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_paving.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_plumbing.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_decoration.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";


        if (company_doors_windows.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";


        if (company_bricks.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";


        if (company_foundation.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_alarms_security_systems.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_cable_satellite_dish.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_ciment.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_other.Checked == true && tb_contractor.Visible == true)

            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_hvac.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_engineer.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_gypse_installation.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_architech.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_gardening.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_roofs.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_flooring.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_basement.Checked == true && tb_contractor.Visible == true)

            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";


        if (company_inspection.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_kitchen.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_electrical.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        if (company_locksmith.Checked == true && tb_contractor.Visible == true)
            task_type_work = task_type_work + ",1";
        else
            task_type_work = task_type_work + ",0";

        int invalid = 0;

        //-------------------- validation section begin -----------------------------
        lbl_date_end_greater.Visible = false;
        lbl_exp_date_end_greater.Visible = false;

        tiger.Date df = new tiger.Date();
        DateTime date_begin = Convert.ToDateTime(df.DateCulture(ddl_task_date_begin_m.SelectedValue, ddl_task_date_begin_d.SelectedValue, ddl_task_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        DateTime exp_date_end = Convert.ToDateTime(df.DateCulture(ddl_task_exp_date_end_m.SelectedValue, ddl_task_exp_date_end_d.SelectedValue, ddl_task_exp_date_end_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        if (ddl_task_date_end_m.SelectedIndex > 0 && ddl_task_date_end_d.SelectedIndex > 0 && ddl_task_date_end_y.SelectedIndex > 0)
        {
            DateTime task_date_end = Convert.ToDateTime(df.DateCulture(ddl_task_date_end_m.SelectedValue, ddl_task_date_end_d.SelectedValue, ddl_task_date_end_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

            if (date_begin > task_date_end)
            {
                lbl_date_end_greater.Visible = true;
                invalid++;
            }
        }

        if (date_begin > exp_date_end)
        {
            lbl_exp_date_end_greater.Visible = true;
            invalid++;
        }

        lbl_must_select_date_before_closing.Visible = false;
        lbl_must_add_task_cost.Visible = false;

        if( ddl_task_status.SelectedValue == "4")
        {

            if (ddl_task_date_end_m.SelectedIndex == 0 || ddl_task_date_end_d.SelectedIndex == 0 || ddl_task_date_end_y.SelectedIndex == 0)
            { 
                invalid++;
                lbl_must_select_date_before_closing.Visible = true;
            }



            if (tbx_task_cost.Text == "" || tbx_task_cost.Text == String.Empty)
            {
                invalid++;
                lbl_must_add_task_cost.Visible = true;
            }
          
        
        }


        //-------------------- validation section end -----------------------------
        


      if(invalid ==0 )
      {
          string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
          SqlConnection conn = new SqlConnection(strconn);
          SqlCommand cmd = new SqlCommand("prTaskUpdate", conn);
          SqlCommand cmd2 = new SqlCommand("prTaskList", conn);
          cmd.CommandType = CommandType.StoredProcedure;
          cmd2.CommandType = CommandType.StoredProcedure;




          //  try
          {
              conn.Open();
              //Add the params
              cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
              cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
              cmd.Parameters.Add("@task_id", SqlDbType.Int).Value = Convert.ToInt32(hd_task_id.Value);
              cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
              cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.NVarChar, 15).Value = Request.UserHostAddress.ToString();
              cmd.Parameters.Add("@task_title", SqlDbType.NVarChar, 50).Value = RegEx.getText(tbx_task_title.Text);
              cmd.Parameters.Add("@task_status", SqlDbType.Int).Value = Convert.ToInt32(ddl_task_status.SelectedValue);
              cmd.Parameters.Add("@task_date_begin", SqlDbType.DateTime).Value = date_begin;
              cmd.Parameters.Add("@task_exp_date_end", SqlDbType.DateTime).Value = exp_date_end;
              cmd.Parameters.Add("@task_priority", SqlDbType.Int).Value = Convert.ToInt32(ddl_priority.SelectedValue);
              cmd.Parameters.Add("@task_type_work", SqlDbType.NVarChar, 50).Value = task_type_work;
              cmd.Parameters.Add("@task_description", SqlDbType.NVarChar, 4000).Value = RegEx.getText(tbx_task_description.Text);
              cmd.Parameters.Add("@task_notes", SqlDbType.NVarChar, 4000).Value = RegEx.getText(tbx_task_notes.Text);
              cmd.Parameters.Add("@company_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_task_contractor.SelectedValue);
              cmd.Parameters.Add("@task_cost_estimation", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_task_cost_estimation.Text));

              cmd.Parameters.Add("@task_cost", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_task_cost.Text));



              if (ddl_task_date_end_m.SelectedIndex > 0 && ddl_task_date_end_d.SelectedIndex > 0 && ddl_task_date_end_y.SelectedIndex > 0)
              {
                  cmd.Parameters.Add("@task_date_end", SqlDbType.DateTime).Value = Convert.ToDateTime(df.DateCulture(ddl_task_date_end_m.SelectedValue, ddl_task_date_end_d.SelectedValue, ddl_task_date_end_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
              }
              else
                  cmd.Parameters.Add("@task_date_end", SqlDbType.DateTime).Value = DBNull.Value;



              //execute the insert
              cmd.ExecuteNonQuery();
              //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

              if (Convert.ToInt32(cmd.Parameters["@return_id"].Value) == 0)
                  Label11.Text = Resources.Resource.lbl_successfull_add;


              //----------------------------------------------------------------------------------------------
              //----------------------------------------------------------------------------------------------
              //Add the params
              cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
              cmd2.Parameters.Add("@wo_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["wo_id"]);


              SqlDataAdapter da = new SqlDataAdapter(cmd2);
              DataTable ds = new DataTable();
              da.Fill(ds);

              gv_task.DataSource = ds;
              gv_task.DataBind();

              conn.Close();

              if(lbl_wo_status.Text == Resources.Resource.lbl_closed)
               lbl_wo_status.Text = GetStatus( Convert.ToInt32(ddl_task_status.SelectedValue));
            
          }

        
      }

      if (invalid > 0)
          MaintainScrollPositionOnPostBack = false;

      else
          MaintainScrollPositionOnPostBack = true;

      tiger.PageUtility.SetFocus(tbx_task_title);
    //  arranger le focus de facon approprie : user experience
          

    }
}
