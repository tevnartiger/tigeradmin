<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" CodeFile="property_view.aspx.cs" Inherits="property_property_view" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager> 
    
<br /><br /><br /><br /><br /><br /><br />
<cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0"  >
    <cc1:TabPanel ID="tab1" runat="server" HeaderText="Address"  >
        <HeaderTemplate>
            Address
        </HeaderTemplate>
     <ContentTemplate  >
        <table width="100%"  cellpadding="0" cellspacing="0">
            <tr valign="top">
                <td>
                    <table width="100%" border="0" cellpadding="1" cellspacing="3" bgcolor="white"> 

<tr><td ><asp:Label ID="txt" runat="server" /></td></tr>
                        <tr>
                            <td align="right" valign="top">
                                <asp:Label ID="Label1" runat="server" style="font-weight: 700" 
                                    Text="<%$ Resources:Resource, lbl_home_name %>"></asp:Label>
                                : </td>
                            <td style="border-width: thin;" valign="top">
                                </b>
                                &nbsp;<asp:Label ID="home_name" runat="server"></asp:Label>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                            <td rowspan="17" valign="top">
                                <asp:Repeater ID="rHomeImage" runat="server">
                                    <ItemTemplate>
                                        <asp:Image ID="Image2" runat="server" Height="270" 
                                            ImageUrl='<%# "~/mediahandler/HomeImage.ashx?h_id="+ Eval("home_id")%>' 
                                            Width="300" />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
<tr><td align="right" valign="top" >
    <asp:Label ID="Label2" runat="server" style="font-weight: 700" 
        Text="<%$ Resources:Resource, lbl_address_no %>"></asp:Label>
    &nbsp;:</td>
    <td valign="top" ><asp:Label id="home_addr_no" runat="server"/></td><td  >&nbsp;</td>
                        </tr>
<tr><td align="right" valign="top" >
    <asp:Label ID="Label3" runat="server" style="font-weight: 700" 
        Text="<%$ Resources:Resource, lbl_address_street %>"></asp:Label>
    : </td>
    <td valign="top" ><asp:Label id="home_addr_street" runat="server"/></td><td  >&nbsp;</td>
                        </tr>
<tr><td align="right" valign="top" >
    <asp:Label ID="Label4" runat="server" style="font-weight: 700" 
        Text="<%$ Resources:Resource, lbl_pc %>"></asp:Label>
    &nbsp;:</td>
    <td valign="top" ><asp:Label id="home_pc" runat="server"/></td><td  >&nbsp;</td>
                        </tr>
<tr><td align="right" valign="top" >
    <asp:Label ID="Label5" runat="server" style="font-weight: 700" 
        Text="<%$ Resources:Resource, lbl_city %>"></asp:Label>
    &nbsp;:</td><td ><asp:Label id="home_city" runat="server"/></td><td  >&nbsp;</td></tr>
<tr><td align="right" valign="top" >
    <asp:Label ID="Label6" runat="server" style="font-weight: 700" 
        Text="<%$ Resources:Resource, txt_district %>"></asp:Label>
    &nbsp;:</td><td ><asp:Label id="home_district" runat="server"/></td><td  >&nbsp;</td></tr>
<tr><td align="right" valign="top" >
    <asp:Label ID="Label7" runat="server" style="font-weight: 700" 
        Text="<%$ Resources:Resource, lbl_province %>"></asp:Label>
    &nbsp;:</td><td ><asp:Label id="home_prov" runat="server"/></td><td  >&nbsp;</td></tr>
<tr><td align="right" valign="top" >
    <asp:Label ID="Label8" runat="server" style="font-weight: 700" 
        Text="<%$ Resources:Resource, lbl_type %>"></asp:Label>
    &nbsp;:</td><td ><asp:Label id="home_type" runat="server"/></td><td  >&nbsp;</td></tr>
<tr><td align="right" valign="top" >
    <asp:Label ID="Label9" runat="server" style="font-weight: 700" 
        Text="<%$ Resources:Resource, lbl_style %>"></asp:Label>
    &nbsp;:</td><td ><asp:Label id="home_style" runat="server"/></td><td  >&nbsp;</td></tr>
<tr><td align="right" valign="top" >
    <asp:Label ID="Label10" runat="server" style="font-weight: 700" 
        Text="<%$ Resources:Resource, lbl_nb_floor %>"></asp:Label>
    &nbsp;:</td><td ><asp:Label id="home_unit" runat="server" /></td><td  >&nbsp;</td></tr>
<tr><td align="right" valign="top" >
    <asp:Label ID="Label11" runat="server" style="font-weight: 700" 
        Text="<%$ Resources:Resource, lbl_nb_unit %>"></asp:Label>
    &nbsp;:</td><td ><asp:Label id="home_floor" runat="server" /></td><td  >&nbsp;</td></tr>
<tr><td align="right" valign="top" >
    <asp:Label ID="Label12" runat="server" style="font-weight: 700" 
        Text="<%$ Resources:Resource, lbl_date_built %>"></asp:Label>
    &nbsp;:</td><td ><asp:Label id="home_date_built" runat="server" /></td><td  >&nbsp;</td></tr>
<tr><td valign="top" align="right"  ><asp:Label ID="Label13" 
        runat="server" Text="<%$ Resources:Resource, lbl_date_purchase %>" 
        style="font-weight: 700"/>&nbsp;:<td><asp:Label id="home_date_purchase" runat="server" /><td>
    &nbsp;</tr>
                        <tr>
                            <td align="right" valign="top">
                                <asp:Label ID="Label14" runat="server" style="font-weight: 700" 
                                    Text="<%$ Resources:Resource, lbl_purchase_price %>"></asp:Label>
                                &nbsp;:</td>
                            <td>
                                <asp:Label ID="home_purchase_price" runat="server"></asp:Label>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
<tr><td valign="top" align="right"  ><b>
    <asp:Label ID="Label15" runat="server" 
        Text="<%$ Resources:Resource, lbl_living_space %>"></asp:Label>
    &nbsp;:</b></td>
    <td >  
        <asp:Label ID="home_living_space" runat="server"></asp:Label>
    </td>
    <td >  </td>
                        </tr>
                        <tr>
                            <td align="right" valign="top">
                                <asp:Label ID="Label16" runat="server" style="font-weight: 700" 
                                    Text="<%$ Resources:Resource, lbl_land_size %>"></asp:Label>
                                &nbsp;:</td>
                            <td valign="top">
                                <asp:Label ID="home_land_size" runat="server"></asp:Label>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td valign="top" align="right">
                                <asp:Label ID="Label17" runat="server" style="font-weight: 700" 
                                    Text="<%$ Resources:Resource, lbl_description %>"></asp:Label>
                                &nbsp;:</td>
                            <td valign="top">
                                <div ID="home_desc" runat="server">
                                </div>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>

</table>
                </td>
            </tr>
        </table>
        </ContentTemplate  >
</cc1:TabPanel>
     
     
     
     
     
     
     
     
   <cc1:TabPanel ID="tab2" runat="server" HeaderText="Dimension , matricule , certificat"  >
     <ContentTemplate  >
        <table width="100%"   cellpadding="0" cellspacing="0">
            <tr valign="top">
                <td  style="width: 100%">
                <table border="0" width="100%" cellpadding="1" cellspacing="3" bgcolor="white"  >
             <tr>
                    <td style="width: 229px; height: 19px;" align="right" >
                        <asp:Label ID="Label23" runat="server" 
                            Text="<%$ Resources:Resource, lbl_land_dimension %>" 
                            style="font-weight: 700" />
                        &nbsp;:</td>
                    <td style="width: 164px; height: 19px;" >
                        <asp:Label ID="home_land_dimension" runat="server"></asp:Label>
                    </td>
                    <td style="width: 225px; height: 19px;" align="right" >
                        &nbsp;<asp:Label ID="Label22" runat="server" 
                            Text="<%$ Resources:Resource, lbl_property_dimension %>" 
                            style="font-weight: 700" />
                        &nbsp;:&nbsp;&nbsp;&nbsp;
                    </td>
                    <td style="height: 19px" valign="top">
                        <asp:Label ID="home_dimension" runat="server"></asp:Label>
                   </td>
                </tr>
                <tr>
                    <td style="width: 229px" align="right" >
                        <asp:Label ID="Label24" runat="server" 
                            Text="<%$ Resources:Resource, lbl_land_surface %>" 
                            style="font-weight: 700" />
                        &nbsp;:</td>
                    <td style="width: 164px" >
                        <asp:Label ID="home_land_surface" runat="server"></asp:Label>
                    </td>
                    <td style="width: 225px" align="right" >
                        <asp:Label ID="Label27" runat="server" 
                            Text="<%$ Resources:Resource, lbl_living_area %>" 
                            style="font-weight: 700" />
                        &nbsp;:</td>
                    <td >
                        <asp:Label ID="home_area" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 229px" align="right">
                        <asp:Label ID="Label25" runat="server" 
                            Text="<%$ Resources:Resource, lbl_localisation_certificat %>" 
                            style="font-weight: 700" />
                        &nbsp;:</td>
                    <td style="width: 164px" >
                        <asp:Label ID="home_localisation_certificat" runat="server"></asp:Label>
                    </td>
                    
                    <td style="width: 225px" >
                        &nbsp;</td>
                    
                    <td>
                        &nbsp;</td>
                    
                </tr>
                    <tr>
                        <td style="width: 229px" align="right">
                            <asp:Label ID="Label21" runat="server" 
                                Text="<%$ Resources:Resource, lbl_matricule %>" style="font-weight: 700" />
                            &nbsp;:</td>
                        <td style="width: 164px">
                            <asp:Label ID="home_matricule" runat="server"></asp:Label>
                        </td>
                        <td style="width: 225px">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                <tr>
                <td style="width: 229px" align="right">
                        <asp:Label ID="Label26" runat="server" 
                            Text="<%$ Resources:Resource, lbl_register %>" style="font-weight: 700" />
                        &nbsp;:</td>
                    <td style="width: 164px" >
                        <asp:Label ID="home_register" runat="server"></asp:Label>
                    </td>
                    <td style="width: 225px" >
                        &nbsp;</td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 229px">
                        &nbsp;</td>
                    <td style="width: 164px" >
                        &nbsp;</td>
                    <td style="width: 225px" >
                        &nbsp;</td>
                    <td>
                    </td>
                     
     
                </tr>
        </table>
                </td>
            </tr>
        </table>
   </ContentTemplate  >
</cc1:TabPanel>
    
    
    
    
    
    
    
    
    
    
    
  <cc1:TabPanel ID="tab3" runat="server" HeaderText="Municipal evaluation"  >
     <ContentTemplate  >
       
                
                <table border="0" width="100%" cellpadding="1" cellspacing="3" bgcolor="white">
                
   
    <tr>
                    <td colspan="2" >
                        <b>
                        <asp:Label ID="Label18" runat="server" ForeColor="#FF3300" 
                            Text="<%$ Resources:Resource, lbl_property_municipal_evaluation_add %>"></asp:Label>
                        </b></td>
                    <td>
                        
                        &nbsp;</td>
                    <td>
                        
                        &nbsp;</td>
      </tr>              
    
   
    <tr>
                    <td style="width: 190px" valign="top" align="right" >
                        <asp:Label ID="Label19" runat="server" 
                            Text="<%$ Resources:Resource, txt_year %>" style="font-weight: 700"></asp:Label>
                        &nbsp;:</td>
                    <td style="width: 162px">
                        
                        <asp:DropDownList ID="ddl_year_evaluation" runat="server">
                            <asp:ListItem Selected="True" Text="<%$ Resources:Resource, txt_year %>" 
                                Value="0"></asp:ListItem>
                            <asp:ListItem Value="1981">1981</asp:ListItem>
                            <asp:ListItem Value="1982">1982</asp:ListItem>
                            <asp:ListItem Value="1983">1983</asp:ListItem>
                            <asp:ListItem Value="1984">1984</asp:ListItem>
                            <asp:ListItem Value="1985">1985</asp:ListItem>
                            <asp:ListItem Value="1986">1986</asp:ListItem>
                            <asp:ListItem Value="1987">1987</asp:ListItem>
                            <asp:ListItem Value="1988">1998</asp:ListItem>
                            <asp:ListItem Value="1989">1989</asp:ListItem>
                            <asp:ListItem Value="1990">1990</asp:ListItem>
                            <asp:ListItem Value="1991">1991</asp:ListItem>
                            <asp:ListItem Value="1992">1992</asp:ListItem>
                            <asp:ListItem Value="1993">1993</asp:ListItem>
                            <asp:ListItem Value="1994">1994</asp:ListItem>
                            <asp:ListItem Value="1995">1995</asp:ListItem>
                            <asp:ListItem Value="1996">1996</asp:ListItem>
                            <asp:ListItem Value="1997">1997</asp:ListItem>
                            <asp:ListItem Value="1998">1998</asp:ListItem>
                            <asp:ListItem Value="1999">1999</asp:ListItem>
                            <asp:ListItem Value="2000">2000</asp:ListItem>
                            <asp:ListItem Value="2001">2001</asp:ListItem>
                            <asp:ListItem Value="2002">2002</asp:ListItem>
                            <asp:ListItem Value="2003">2003</asp:ListItem>
                            <asp:ListItem Value="2004">2004</asp:ListItem>
                            <asp:ListItem Value="2005">2005</asp:ListItem>
                            <asp:ListItem Value="2006">2006</asp:ListItem>
                            <asp:ListItem Value="2007">2007</asp:ListItem>
                            <asp:ListItem Value="2008">2008</asp:ListItem>
                            <asp:ListItem Value="2009">2009</asp:ListItem>
                            <asp:ListItem Value="2010">2010</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td colspan="2" rowspan="4" valign="top">
                        <asp:GridView ID="gv_home_year_evaluation" runat="server" AllowSorting="true" 
                            AlternatingRowStyle-BackColor="Beige" AutoGenerateColumns="false" 
                            BorderColor="White" BorderWidth="3" EmptyDataText="no unit rented" 
                            GridLines="Both" HeaderStyle-BackColor="AliceBlue" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="home_year_evaluation" 
                                    HeaderText="<%$ Resources:Resource, txt_year %>" />
                                <asp:BoundField DataField="home_land_evaluation" DataFormatString="{0:0.00}" 
                                    HeaderText="<%$ Resources:Resource, lbl_land_evaluation %>" />
                                <asp:BoundField DataField="home_building_evaluation" 
                                    DataFormatString="{0:0.00}" 
                                    HeaderText="<%$ Resources:Resource, lbl_building_evaluation %>" />
                                <asp:BoundField DataField="home_total_evaluation" 
                                    HeaderText="<%$ Resources:Resource, lbl_total_evaluation %>" />
                            </Columns>
                        </asp:GridView>
                    </td>
      </tr>              
                <tr>  <td style="width: 190px" valign="top" align="right" >
                        <asp:Label ID="Label20" runat="server" 
                            Text="<%$ Resources:Resource, lbl_land_evaluation %>" 
                            style="font-weight: 700"></asp:Label>&nbsp;:</td>
               
             
                    <td style="width: 162px" valign="top" >
                        <asp:TextBox ID="home_land_evaluation" runat="server" Height="17px" 
                            Width="80px"></asp:TextBox>
                    </td>
               
             
                    </tr>     
                    
                <tr>
                
                <td style="width: 190px" valign="top" align="right" >
                    <asp:Label ID="Label28" runat="server" 
                        Text="<%$ Resources:Resource, lbl_building_evaluation %>" 
                        style="font-weight: 700"></asp:Label>&nbsp;:</td>
                
                
                <td style="width: 162px" valign="top" >
                    <asp:TextBox ID="home_building_evaluation" runat="server" 
                        Width="80px"></asp:TextBox>
               </td>
                
                
               </tr>
               <tr>
                
                <td style="width: 190px" valign="top" align="right" >
                    <asp:Label ID="Label29" runat="server" 
                        Text="<%$ Resources:Resource, lbl_total_evaluation %>" 
                        style="font-weight: 700"></asp:Label>&nbsp;:</td>
                
                
                <td style="width: 162px" valign="top" >
                    <asp:TextBox ID="home_total_evaluation" runat="server" 
                        Width="80px"></asp:TextBox>
               </td>
                
                
               </tr>
                
                
                
                    <tr>
                        <td style="width: 190px">
                            <asp:Button ID="btn_sumbit_evaluation" runat="server" 
                                onclick="btn_sumbit_evaluation_Click" 
                                Text="<%$ Resources:Resource, btn_submit %>" />
                        </td>
                        <td style="width: 162px">
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                        <td>
                            &nbsp;</td>
                    </tr>
                
                
                
                </table>
            
    </ContentTemplate  >
</cc1:TabPanel>
    
    
    
    
    
    
    
    <cc1:TabPanel ID="tab4" runat="server" HeaderText="Characteristic"  >
        <HeaderTemplate>
            Characteristic
        </HeaderTemplate>
     <ContentTemplate  >
         <table border="0" width="100%" cellpadding="1" cellspacing="3"  >
        
        <tr>
            <td align="right" style="width: 234px"> 
                <asp:Label ID="Label33" runat="server" style="font-weight: 700" 
                    Text="<%$ Resources:Resource, lbl_fondation %>"></asp:Label>
                &nbsp;:</td>
        
        <td ><asp:Label ID="home_fondation" runat="server"></asp:Label></td>
        
        </tr>
        <tr>
        <td style="width: 234px" align="right"><asp:Label ID="Label34" runat="server" 
                Text="<%$ Resources:Resource, lbl_frame %>" style="font-weight: 700" />&nbsp;:</td>
        
        <td ><asp:Label ID="home_frame" runat="server"></asp:Label></td>
        </tr>
        <tr>
        <td style="width: 234px" align="right"><asp:Label ID="Label35" runat="server" 
                Text="<%$ Resources:Resource, lbl_roof %>" style="font-weight: 700"/>&nbsp;:</td>
        
        <td ><asp:Label ID="home_roof" runat="server"></asp:Label></td>
        </tr>
         <tr>
        <td style="width: 234px" align="right"><asp:Label ID="Label36" runat="server" 
                Text="<%$ Resources:Resource, lbl_windows %>" style="font-weight: 700"/>&nbsp;:</td>
        
        <td ><asp:Label ID="home_windows" runat="server"></asp:Label></td>
        </tr>
        <tr>
        <td style="width: 234px" align="right"><asp:Label ID="Label37" runat="server" 
                Text="<%$ Resources:Resource, lbl_floors %>" style="font-weight: 700"/>&nbsp;:</td>
        
        <td ><asp:Label ID="home_floors" runat="server"></asp:Label></td>
        </tr>
        <tr>
        <td style="width: 234px" align="right"><asp:Label ID="Label38" runat="server" 
                Text="<%$ Resources:Resource, lbl_under_floors %>" style="font-weight: 700"/>
            &nbsp;:</td>
        
        <td ><asp:Label ID="home_under_floors" runat="server"></asp:Label></td>
        </tr>

        <tr>
        <td style="width: 234px" align="right"><asp:Label ID="Label39" runat="server" 
                Text="<%$ Resources:Resource, lbl_walls %>" style="font-weight: 700"/>&nbsp;:</td>
        
        <td ><asp:Label ID="home_wall" runat="server"></asp:Label></td>
        </tr>
        <tr>
        <td style="width: 234px" align="right"><asp:Label ID="Label40" runat="server" 
                Text="<%$ Resources:Resource, lbl_water_heater %>" style="font-weight: 700"/>
            &nbsp;:</td>
        
        <td ><asp:Label ID="home_water_heater" runat="server"></asp:Label></td>
        </tr>
        <tr>
        
        <td align="right" style="width: 234px" >
            <asp:Label ID="Label41" runat="server" style="font-weight: 700" 
                Text="<%$ Resources:Resource, lbl_lav_sech %>"></asp:Label>
            &nbsp;:</td>
        <td>
            <asp:RadioButtonList ID="home_lav_sech" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="0"></asp:ListItem>
                <asp:ListItem Value="1"></asp:ListItem>
            </asp:RadioButtonList>
            </td>
        
            </tr>
          <tr>
        
        <td align="right" style="width: 234px" >
            <asp:Label ID="Label42" runat="server" style="font-weight: 700" 
                Text="<%$ Resources:Resource, lbl_fire_protection %>"></asp:Label>
            &nbsp;:</td>
        <td>
            <asp:RadioButtonList ID="home_fire_protection" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="0"></asp:ListItem>
                <asp:ListItem Value="1"></asp:ListItem>
            </asp:RadioButtonList>
              </td>
        
            </tr>
        <tr>
        
        <td align="right" style="width: 234px" >
            <asp:Label ID="Label43" runat="server" style="font-weight: 700" 
                Text="<%$ Resources:Resource, lbl_laundry %>"></asp:Label>
            &nbsp;:</td>
        <td>
            <asp:RadioButtonList ID="home_laundry" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="0"></asp:ListItem>
                <asp:ListItem Value="1"></asp:ListItem>
            </asp:RadioButtonList>
            </td>
        
            </tr>
        <tr>
        <td style="width: 234px" align="right"><asp:Label ID="Label44" runat="server" 
                Text="<%$ Resources:Resource, lbl_parking %>" style="font-weight: 700"/>&nbsp;:</td>
        
            <td>
                <asp:Label ID="Label45" runat="server" 
                    Text="<%$ Resources:Resource, lbl_int %>"></asp:Label>
                &nbsp; :
                <asp:Label ID="home_parking_int" runat="server" Width="70px"></asp:Label>
                &nbsp;
                <asp:Label ID="Label46" runat="server" 
                    Text="<%$ Resources:Resource, lbl_int %>"></asp:Label>
                &nbsp; :<asp:Label ID="home_parking_ext" runat="server" Width="68px"></asp:Label>
            </td>
        
        <td style="width: 55px" >
            &nbsp;&nbsp;</td>
            </tr>

        <tr>
        
        <td align="right" style="width: 234px" >
            <asp:Label ID="Label47" runat="server" style="font-weight: 700" 
                Text="<%$ Resources:Resource, lbl_external_plugs %>"></asp:Label>
            &nbsp;:</td>
        <td>
            <asp:RadioButtonList ID="home_prise_ext" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="0"></asp:ListItem>
                <asp:ListItem Value="1"></asp:ListItem>
            </asp:RadioButtonList>
            </td>
        
            </tr>
        <tr>
        <td style="width: 234px" align="right"><asp:Label ID="Label48" runat="server" 
                Text="<%$ Resources:Resource, lbl_water %>" style="font-weight: 700"/>&nbsp;:</td>
        
        <td ><asp:Label ID="home_water" runat="server"></asp:Label></td>
        </tr>
        <tr>
        <td style="width: 234px" align="right"><asp:Label ID="Label49" runat="server" 
                Text="<%$ Resources:Resource, lbl_heating %>" style="font-weight: 700"/>&nbsp;:</td>
        
        
        
        <td ><asp:Label ID="home_heating" runat="server"></asp:Label></td>
        </tr>

        <tr>
        <td style="width: 234px" align="right"><asp:Label ID="Label50" runat="server" 
                Text="<%$ Resources:Resource, lbl_combustible %>" style="font-weight: 700"/>
            &nbsp;:</td>
        
        <td ><asp:Label ID="home_combustible" runat="server"></asp:Label></td>
        </tr>
       <tr>
        <td style="width: 234px" align="right"><asp:Label ID="Label51" runat="server" 
                Text="<%$ Resources:Resource, lbl_sewage %>" style="font-weight: 700"/>&nbsp;:</td>
        
        <td ><asp:Label ID="home_sewage" runat="server"></asp:Label></td>
        </tr>
        <tr>
        <td style="width: 234px" align="right"><asp:Label ID="Label52" runat="server" 
                Text="<%$ Resources:Resource, lbl_exterior_finition %>" 
                style="font-weight: 700"/>&nbsp;:</td>
        
        <td ><asp:Label ID="home_ext_finition" runat="server"></asp:Label></td>
        </tr>
        <tr>
        
        <td align="right" style="width: 234px" valign="top" >
                
            <asp:Label ID="Label53" runat="server" style="font-weight: 700" 
                Text="<%$ Resources:Resource, lbl_site_influence %>"></asp:Label>
            &nbsp;:<td>
                <table cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td style="height: 20px; width: 153px">
                            <asp:CheckBox ID="home_site_corner" runat="server" AutoPostBack="True" 
                                Text="corner" />
                        </td>
                        <td style="height: 20px">
                            <asp:CheckBox ID="home_site_school" runat="server" AutoPostBack="True" 
                                Text="school" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 153px">
                            <asp:CheckBox ID="home_site_highway" runat="server" AutoPostBack="True" 
                                Text="highway" />
                        </td>
                        <td>
                            <asp:CheckBox ID="home_site_services" runat="server" AutoPostBack="True" 
                                Text="services" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 153px">
                            <asp:CheckBox ID="home_site_transport" runat="server" AutoPostBack="True" 
                                Text="transport" />
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
            </td>
        
            </tr>
             <tr>
                 <td align="right" style="width: 234px">
                     <asp:Label ID="Label54" runat="server" style="font-weight: 700" 
                         Text="<%$ Resources:Resource, lbl_parking %>"></asp:Label>
                     &nbsp;:</td>
                 <td>
                     <asp:Label ID="home_parking" runat="server"></asp:Label>
                 </td>
             </tr>
             <tr>
                 <td align="right" style="height: 143px; width: 234px;" valign="top">
                     <asp:Label ID="Label55" runat="server" style="font-weight: 700" 
                         Text="<%$ Resources:Resource, lbl_particularity %>"></asp:Label>
                     &nbsp;:</td>
                 <td style="height: 143px" valign="top">
                     <div ID="home_particularity" runat="server">
                     </div>
                 </td>
             </tr>
        </table>
    </ContentTemplate  >
  </cc1:TabPanel>
    
    
</cc1:TabContainer>


</asp:Content>