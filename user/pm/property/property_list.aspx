<%@ Page Language="C#" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" CodeFile="property_list.aspx.cs" Inherits="property_property_list" Title="property list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <br /><br /><br /><br /><br /> <br /><br />
     <asp:GridView ID="dghome_list" runat="server" AutoGenerateColumns="false" GridLines="Both"  
           AllowPaging="true" AllowSorting="true"    AlternatingRowStyle-BackColor="#F0F0F6" 
      BorderColor="#CDCDCD"  BorderWidth="1"  Width="70%"
      HeaderStyle-BackColor="#F0F0F6" PageSize="15"  OnPageIndexChanging="dghome_list_PageIndexChanging"
         onselectedindexchanged="dghome_list_SelectedIndexChanged" 
           >
    <Columns>
    
    <asp:BoundField   DataField="home_name"   HeaderText="Property name"/>
    <asp:BoundField   DataField="home_addr_no"  HeaderText="Address #"/>
    <asp:BoundField  DataField="home_addr_street" HeaderText="Street" />
    <asp:BoundField  DataField="home_unit" HeaderText="Unit(s)" />
    <asp:BoundField  DataField="home_prov" HeaderText="Prov/State" />
    <asp:BoundField  DataField="home_city" HeaderText="City"/>
    <asp:BoundField  DataField="home_district" HeaderText="District"/>
    
     <asp:HyperLinkField   Text="View"
     DataNavigateUrlFields="home_id" 
     DataNavigateUrlFormatString="~/user/pm/property/property_view.aspx?h_id={0}" 
      HeaderText="View" />
    </Columns>
    
    
    </asp:GridView>
    
   </asp:Content>
