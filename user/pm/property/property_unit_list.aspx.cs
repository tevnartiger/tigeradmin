﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.NameObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : may 25, 2009
/// </summary>
public partial class manager_property_property_unit_list :BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
           
            //  RESIDENTIAL UNITS ---------------------------------------------------------------------
            //-----------------------------------------------------------------------------------------
            tiger.PM h = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = h.getPMHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
            string link_to_unit = "";
            if (home_count > 0)
            {
                int home_id = h.getPMHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));


                string referrer = "";

                referrer = Request.UrlReferrer.AbsolutePath.ToString();

                if (referrer == "/sinfo/manager/property/property_view.aspx" || referrer == "/manager/property/property_view.aspx")
                {
                    //this is the pending tab
                    if (!RegEx.IsInteger(Request.QueryString["h_id"]))
                    {
                        Session.Abandon();
                        Response.Redirect("~/login.aspx");
                    }

                    home_id =Convert.ToInt32(Request.QueryString["h_id"]);

                    NameObjectAuthorization homeAuthorization = new NameObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                    if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), home_id, Convert.ToInt32(Session["name_id"]), 6))
                    {
                        Session.Abandon();
                        Response.Redirect("~/login.aspx");
                    }
                    ///////// SECURITY OBJECT CHECK  END ////////////////////////////////


                }

                ddl_home_id.DataSource = h.getPMHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                ddl_home_id.SelectedValue = home_id.ToString();
                ddl_home_id.DataBind();

                //To view the address of the property

                tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
                rhome_view.DataBind();            

            }

            //  COMMERCIAL UNITS ---------------------------------------------------------------------
            //-----------------------------------------------------------------------------------------

            int homecom_count = h.getPMHomeComCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
            if (homecom_count > 0)
            {
                int home_id = h.getPMHomeComFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));


                string referrer = "";

                referrer = Request.UrlReferrer.AbsolutePath.ToString();

                if (referrer == "/sinfo/manager/property/property_view.aspx" || referrer == "/manager/property/property_view.aspx")
                {
                    //this is the pending tab

                    if (!RegEx.IsInteger(Request.QueryString["h_id"]))
                    {
                        Session.Abandon();
                        Response.Redirect("~/login.aspx");
                    }

                    home_id = Convert.ToInt32(Request.QueryString["h_id"]);

                    NameObjectAuthorization homeAuthorization = new NameObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                    if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), home_id, Convert.ToInt32(Session["name_id"]), 6))
                    {
                        Session.Abandon();
                        Response.Redirect("~/login.aspx");
                    }
                    ///////// SECURITY OBJECT CHECK  END ////////////////////////////////


                }

                ddl_home_id2.DataSource = h.getPMHomeComList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                ddl_home_id2.SelectedValue = home_id.ToString();
                ddl_home_id2.DataBind();

                //To view the address of the property

                tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rhome_view2.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
                rhome_view2.DataBind();

            }

           

        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        ///To view the address of the property
    
        tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
        rhome_view.DataBind();

        //TabContainer1.ActiveTabIndex = 0;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_id2_SelectedIndexChanged(object sender, EventArgs e)
    {
        ///To view the address of the property

        tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rhome_view2.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id2.SelectedValue));
        rhome_view2.DataBind();

        //TabContainer1.ActiveTabIndex = 1;

    }

    //NO NEED TO USE THE ONINDEXCHANGED , IT'S GOT TAKEN CARE OF AUTOMATICALY BY .NET
    // WHEN WE USE THE MARKU INSTEAD OF THE CODE BEHIN



    protected void GridView2_RowCommand(object sender, EventArgs e)
    {
        //TabContainer1.ActiveTabIndex = 1;
    }

    protected void GridView1_RowCommand(object sender, EventArgs e)
    {
        //TabContainer1.ActiveTabIndex = 0;
    }


}
