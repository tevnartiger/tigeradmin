﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" CodeFile="property_default.aspx.cs" Inherits="manager_property_property_default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<table>
<tr><td colspan="2"><h1>PROPERTY</h1></td></tr>
<tr><td><h2>View Property</h2></td><td>Enable you to view all properties from your account.</td></tr>
<tr><td><h2>Create  Property</h2></td><td>Enable you to create a new property to your account.</td></tr>
<tr><td><h2>Update  Property</h2></td><td>Enable you to modify a selected property information.</td></tr>
<tr><td><h2>Unit List</h2></td><td>Enable you to view units of a property.</td></tr>
<tr><td><h2>Add Unit</h2></td><td>Enable to add more unit(s) to an existing property.</td></tr>
<tr><td><h2>Profile</h2></td><td>Enable you to view general information of one or more properties all in 1 page.</td></tr>

</table>
</asp:Content>

