﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" CodeFile="group_default.aspx.cs" Inherits="manager_group_group_default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table>
<tr><td colspan="2"><h1>PROFILES</h1></td></tr>
<tr><td><h2>Property profile</h2></td><td>Enable you to group properties to better manage. <a href="group_profile.aspx">View</a></td></tr>
<tr><td><h2>Name profile</h2></td><td>Enable you to view a 1 page summary of a person roles and information in the system.  <a href="/manager/name/name_profile.aspx">View</a></td></tr>
  </table>
</asp:Content>

