﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="Copy of name_profile.aspx.cs" Inherits="manager_name_name_profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:HiddenField ID="name_id" runat="server" 
 />
 <asp:Image ID="name_img" runat="server" Visible="false" />
<asp:FileUpload id="fileImage" runat="server"  />
<asp:CustomValidator ID="valFile" runat="server"
 ErrorMessage="No file uploaded" OnServerValidate= "valFile_ServerValidate" Display="Dynamic" />
 <asp:CustomValidator ID="valFileType" runat="server"
 ErrorMessage="This is not an image" OnServerValidate="valFileType_ServerValidate" Display="Dynamic" />
 <h2>Name of ... </h2>
 <br />Info
<br /><br />
<h3>Roles</h3>
 <asp:Button  ID="btn_submit" text="Upload" OnClick="btn_submit_Click" runat="server" />
<br />
<div ID="lb_txt" runat="server" />
</asp:Content>

