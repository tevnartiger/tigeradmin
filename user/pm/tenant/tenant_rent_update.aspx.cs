﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.NameObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : april 5 , 2008
/// </summary>


public partial class manager_tenant_tenant_rent_update : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!RegEx.IsInteger(Request.QueryString["rpp_id"]) ||
            !RegEx.IsInteger(Request.QueryString["h_id"]) ||
            !RegEx.IsInteger(Request.QueryString["fm"]) ||
            !RegEx.IsInteger(Request.QueryString["fy"]) ||
            !RegEx.IsInteger(Request.QueryString["fd"]) ||
            !RegEx.IsInteger(Request.QueryString["tm"]) ||
            !RegEx.IsInteger(Request.QueryString["ty"]) ||
            !RegEx.IsInteger(Request.QueryString["td"]) ||
            !RegEx.IsInteger(Request.QueryString["uid"])
            )
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        NameObjectAuthorization rpAuthorization = new NameObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        // check prospective tenant authorization
        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!rpAuthorization.RentPaid(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["rpp_id"]), Convert.ToInt32(Session["name_id"]), 6))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
             

        if (!Page.IsPostBack)
        {
            reg_tbx_rent_paid_amount.ValidationExpression = RegEx.getMoney();
            
          ///*************************************************************************************  

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prRentPaidId", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@rp_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["rpp_id"]);

            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                DateTime rent_date_received = new DateTime();

                while (dr.Read() == true)
                {

                  //  home_id = Convert.ToInt32(dr["home_id"]);
                  //  h_h_id.Value = home_id.ToString();

                    lbl_unit_no.Text = Convert.ToString(dr["unit_door_no"]);
                    lbl_rent_amount.Text = String.Format("{0:0.00}", Convert.ToString(dr["rl_rent_amount"]));
                    h_rl_rent_amount.Value = lbl_rent_amount.Text;
                    tbx_rent_paid_amount.Text = String.Format("{0:0.00}", Convert.ToString(dr["rp_amount"]));
                    lbl_due_date.Text = String.Format("{0:M-dd-yyyy}", Convert.ToDateTime(dr["rp_due_date"]));
                    lbl_property2.Text = Convert.ToString(dr["home_name"]);
                    tbx_notes.Text = Convert.ToString(dr["rp_notes"]);

                    rent_date_received = Convert.ToDateTime(dr["rp_paid_date"]);
                    ddl_edit_date_received_d.SelectedValue = rent_date_received.Day.ToString();
                    ddl_edit_date_received_m.SelectedValue = rent_date_received.Month.ToString();
                    ddl_edit_date_received_y.SelectedValue = rent_date_received.Year.ToString();

                  }

            }

            finally
            {
                conn.Close();
            }
 
            ////************************************************************************************
           
                int home_id = Convert.ToInt32(Request.QueryString["h_id"]);


                NameObjectAuthorization homeAuthorization = new NameObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
              
                // check acciunt home authorization
                ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), home_id, Convert.ToInt32(Session["name_id"]), 6))
                {
                    Session.Abandon();
                    Response.Redirect("~/login.aspx");
                }
                ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
             
               
                //To view the address of the property

                tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
                rhome_view.DataBind();



        }

    }

 
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_update_Click(object sender, EventArgs e)
    {
     
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime paid_date = new DateTime();
        tiger.Date d = new tiger.Date();
        paid_date = Convert.ToDateTime(d.DateCulture(ddl_edit_date_received_m.SelectedValue, ddl_edit_date_received_d.SelectedValue, ddl_edit_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prRentUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@rp_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["rpp_id"]);
            cmd.Parameters.Add("@rp_paid_date", SqlDbType.DateTime).Value = paid_date;
            cmd.Parameters.Add("@rp_amount", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_rent_paid_amount.Text));
            cmd.Parameters.Add("@rl_rent_amount", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(h_rl_rent_amount.Value));
            cmd.Parameters.Add("@rp_notes", SqlDbType.Text).Value = RegEx.getText(tbx_notes.Text);

            //execute the insert
            cmd.ExecuteReader();
            conn.Close();

            if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 0)
            {
                lbl_success.Text = Resources.Resource.lbl_successfull_modification;
            }


        }
        catch (Exception error)
        {
            //  tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }

        string url = "tenant_cancel_rent_payment.aspx?h_id=" + Request.QueryString["h_id"] + "&fm=" + Request.QueryString["fm"] + "&fd=" + Request.QueryString["fd"]
                                                        + "&fy=" + Request.QueryString["fy"] + "&tm=" + Request.QueryString["tm"]
                                                        + "&td=" + Request.QueryString["td"] + "&ty=" + Request.QueryString["ty"]
                                                        + "&uid=" + Request.QueryString["uid"];


        Response.Redirect(url);
       
    }



}
