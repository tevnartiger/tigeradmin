using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
/// <summary>
/// Created by : Stanley Jocelyn   
/// Date       : oct 13, 2007
///              Add a tenant prospect - application form
/// </summary>
public partial class tenant_tenant_prospect_add : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
       if(!Page.IsPostBack)
        {
            reg_name_addr.ValidationExpression = RegEx.getText();
            reg_name_addr_city.ValidationExpression = RegEx.getText();
            reg_name_addr_pc.ValidationExpression = RegEx.getText();
            reg_name_fname.ValidationExpression = RegEx.getText();
            reg_name_lname.ValidationExpression = RegEx.getText();
            reg_name_tel.ValidationExpression = RegEx.getText();
            reg_tenantapplication_current_employer.ValidationExpression = RegEx.getText();
            reg_tenantapplication_landlord_name.ValidationExpression = RegEx.getText();
            reg_tenantapplication_landlord_tel.ValidationExpression = RegEx.getText();
            reg_tenantapplication_monthly_income.ValidationExpression = RegEx.getMoney();

            tiger.Country ic = new tiger.Country(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_country_list.DataSource = ic.getCountryList();
            ddl_country_list.DataBind();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        tiger.Date d = new tiger.Date();
     
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prProspectiveTenantAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();

            // new tenant parameter - tenant from the database
            //  cmd.Parameters.Add("@return_success", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.NVarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@pt_date_begin", SqlDbType.DateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_pt_date_begin_m.SelectedValue, ddl_pt_date_begin_d.SelectedValue, ddl_pt_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

            cmd.Parameters.Add("@name_ssn", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_ssn.Text);

            cmd.Parameters.Add("@name_lname", SqlDbType.NVarChar, 50).Value = Convert.ToString(RegEx.getText(name_lname.Text));
            cmd.Parameters.Add("@name_fname", SqlDbType.NVarChar, 50).Value = Convert.ToString(RegEx.getText(name_fname.Text));


            cmd.Parameters.Add("@name_addr", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr.Text);
            cmd.Parameters.Add("@name_addr_city", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_city.Text);
            cmd.Parameters.Add("@name_addr_pc", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_pc.Text);
            cmd.Parameters.Add("@name_addr_state", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_state.Text);
            cmd.Parameters.Add("@country_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_country_list.SelectedValue);
            cmd.Parameters.Add("@name_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_tel.Text);
            cmd.Parameters.Add("@name_tel_work", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_tel_work.Text);
            cmd.Parameters.Add("@name_tel_work_ext", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_tel_work_ext.Text);

            cmd.Parameters.Add("@name_cell", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_cell.Text);
            cmd.Parameters.Add("@name_fax", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_fax.Text);
            cmd.Parameters.Add("@name_email", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_email.Text);
            cmd.Parameters.Add("@name_com", SqlDbType.Text).Value = RegEx.getText(name_com.Text);

            cmd.Parameters.Add("@name_dob", SqlDbType.SmallDateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_name_dob_m.SelectedValue, ddl_name_dob_d.SelectedValue, ddl_name_dob_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


            cmd.Parameters.Add("@pt_bedroom_no", SqlDbType.Int).Value = Convert.ToInt32(ddl_bedroom_no.SelectedValue);
            cmd.Parameters.Add("@pt_bathroom_no", SqlDbType.Int).Value = Convert.ToInt32(ddl_bathroom_no.SelectedValue);

            cmd.Parameters.Add("@pt_rent_min", SqlDbType.Int).Value = Convert.ToInt32(ddl_rent_min.SelectedValue);
            cmd.Parameters.Add("@pt_rent_max", SqlDbType.Int).Value = Convert.ToInt32(ddl_rent_max.SelectedValue);
            cmd.Parameters.Add("@pt_min_sqft", SqlDbType.Int).Value = Convert.ToInt32(ddl_min_sqft.SelectedValue);



            if (pt_electricity_inc.Checked == true)
                cmd.Parameters.Add("@pt_electricity_inc", SqlDbType.Int).Value = 1;
            else
                cmd.Parameters.Add("@pt_electricity_inc", SqlDbType.Int).Value = 0;




            if (pt_heat_inc.Checked == true)
                cmd.Parameters.Add("@pt_heat_inc", SqlDbType.Int).Value = 1;
            else
                cmd.Parameters.Add("@pt_heat_inc", SqlDbType.Int).Value = 0;





            if (pt_water_inc.Checked == true)
                cmd.Parameters.Add("@pt_water_inc", SqlDbType.Int).Value = 1;
            else
                cmd.Parameters.Add("@pt_water_inc", SqlDbType.Int).Value = 0;


            if (pt_water_tax_inc.Checked == true)
                cmd.Parameters.Add("@pt_water_tax_inc", SqlDbType.Int).Value = 1;
            else
                cmd.Parameters.Add("@pt_water_tax_inc", SqlDbType.Int).Value = 0;


            if (pt_parking_inc.Checked == true)
                cmd.Parameters.Add("@pt_parking_inc", SqlDbType.Int).Value = 1;
            else
                cmd.Parameters.Add("@pt_parking_inc", SqlDbType.Int).Value = 0;





            if (pt_garage_inc.Checked == true)
                cmd.Parameters.Add("@pt_garage_inc", SqlDbType.Int).Value = 1;
            else
                cmd.Parameters.Add("@pt_garage_inc", SqlDbType.Int).Value = 0;




            if (pt_furnished_inc.Checked == true)
                cmd.Parameters.Add("@pt_furnished_inc", SqlDbType.Int).Value = 1;
            else
                cmd.Parameters.Add("@pt_furnished_inc", SqlDbType.Int).Value = 0;



            if (pt_semi_furnished_inc.Checked == true)
                cmd.Parameters.Add("@pt_semi_furnished_inc", SqlDbType.Int).Value = 1;
            else
                cmd.Parameters.Add("@pt_semi_furnished_inc", SqlDbType.Int).Value = 0;

            cmd.Parameters.Add("@pt_com", SqlDbType.Text).Value = RegEx.getText(pt_com.Text);

            cmd.Parameters.Add("@tenantapplication_landlord_name", SqlDbType.NVarChar, 50).Value = RegEx.getText(tenantapplication_landlord_name.Text);
            cmd.Parameters.Add("@tenantapplication_current_employer", SqlDbType.NVarChar, 50).Value = RegEx.getText(tenantapplication_current_employer.Text);
            cmd.Parameters.Add("@tenantapplication_landlord_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(tenantapplication_landlord_tel.Text);
            cmd.Parameters.Add("@tenantapplication_monthly_income", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tenantapplication_monthly_income.Text));




            cmd.Parameters.Add("@tenantapplication_people_with_prospect", SqlDbType.Text).Value = RegEx.getText(tenantapplication_people_with_prospect.Text);
            cmd.Parameters.Add("@tenantapplication_reason_depart", SqlDbType.Text).Value = RegEx.getText(tenantapplication_reason_depart.Text);
            cmd.Parameters.Add("@tenantapplication_employer_since", SqlDbType.SmallDateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_tenantapplication_employer_since_m.SelectedValue, ddl_tenantapplication_employer_since_d.SelectedValue, ddl_tenantapplication_employer_since_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            cmd.ExecuteReader();
            //   if (Convert.ToInt32(cmd.Parameters["@return_success"].Value) == 0)
            //  result.InnerHtml = "add successful";
        }
        finally
        {
            conn.Close();
        }
    }
    
}
