﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : jun 11 , 2008
/// </summary>
public partial class manager_tenant_tenant_inactive_view : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RegEx.IsInteger(Request.QueryString["n_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        AccountObjectAuthorization nameAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!nameAuthorization.Name(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["n_id"])))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
             


        tenantapplication_add_link.NavigateUrl = "tenant_prospect_add.aspx";
        tenantapplication_update_link.NavigateUrl = "tenant_update.aspx?n_id=" + Request.QueryString["n_id"];

        SetDefaultView();
      

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prTenantView", conn);
      //  SqlCommand cmd2 = new SqlCommand("prTenantRoomates", conn);
        SqlCommand cmd3 = new SqlCommand("prTenantLeaseArchives", conn);
        SqlCommand cmd4 = new SqlCommand("prTenantAmountOfLateRent", conn);
        SqlCommand cmd5 = new SqlCommand("prTenantAmountOnTimeRent", conn);
        SqlCommand cmd6 = new SqlCommand("prTenantPaymentArchive", conn);
        SqlCommand cmd7 = new SqlCommand("prTenantLatePaymentArchive", conn);

        cmd.CommandType = CommandType.StoredProcedure;

        //Add the params
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["n_id"]);
        //  try
        {
            conn.Open();
            SqlDataReader dr = null;
            dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

            while (dr.Read() == true)
            {

                DateTime date_begin = new DateTime();

                if (dr["pt_date_begin"] != DBNull.Value)
                {
                    date_begin = Convert.ToDateTime(dr["pt_date_begin"]);
                    lbl_pt_date_begin.Text = date_begin.Month.ToString() + "/" + date_begin.Day.ToString() + "/" + date_begin.Year.ToString();
                }






                lbl_name_ssn.Text = dr["name_ssn"].ToString();
                lbl_tenantapplication_current_employer.Text = dr["tenantapplication_current_employer"].ToString();





                lbl_name_lname.Text = dr["name_lname"].ToString();
                lbl_name_fname.Text = dr["name_fname"].ToString();


                lbl_name_addr.Text = dr["name_addr"].ToString();
                lbl_name_addr_city.Text = dr["name_addr_city"].ToString();
                lbl_name_addr_pc.Text = dr["name_addr_pc"].ToString();
                lbl_name_addr_state.Text = dr["name_addr_state"].ToString();
                lbl_country_name.Text = dr["country_name"].ToString();
                lbl_name_tel.Text = dr["name_tel"].ToString();
                lbl_name_tel_work.Text = dr["name_tel_work"].ToString();
                lbl_name_tel_work_ext.Text = dr["name_tel_work_ext"].ToString();

                lbl_name_cell.Text = dr["name_cell"].ToString();
                lbl_name_fax.Text = dr["name_fax"].ToString();
                lbl_name_email.Text = dr["name_email"].ToString();
                lbl_name_com.Text = dr["name_com"].ToString();


                DateTime name_date_insert = new DateTime();
                if (dr["trace_date_insert"] != System.DBNull.Value)
                {
                    name_date_insert = Convert.ToDateTime(dr["trace_date_insert"]);
                    lbl_tenantapplication_date_insert.Text = name_date_insert.Month.ToString() + "/" + name_date_insert.Day.ToString() + "/" + name_date_insert.Year.ToString();
                }
                else
                    lbl_tenantapplication_date_insert.Text = "N/A";

                DateTime name_dob = new DateTime();
                if (dr["name_dob"] != System.DBNull.Value)
                {
                    name_dob = Convert.ToDateTime(dr["name_dob"]);
                    lbl_name_dob.Text = name_dob.Month.ToString() + "/" + name_dob.Day.ToString() + "/" + name_dob.Year.ToString();
                }



                if (dr["pt_bedroom_no"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_bedroom_no"]) == 0)
                        lbl_pt_bedroom_no.Text = "No Minimum";
                    else
                        lbl_pt_bedroom_no.Text = dr["pt_bedroom_no"].ToString();

                }

                if (dr["pt_bathroom_no"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_bathroom_no"]) == 0)
                        lbl_pt_bathroom_no.Text = "No Maximum";
                    else
                        lbl_pt_bathroom_no.Text = dr["pt_bathroom_no"].ToString();

                }


                if (dr["pt_rent_min"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_rent_min"]) == 0)
                        lbl_pt_rent_min.Text = "Any";
                    else
                        lbl_pt_rent_min.Text = dr["pt_rent_min"].ToString();

                }


                if (dr["pt_rent_max"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_rent_max"]) == 0)
                        lbl_pt_rent_max.Text = "Any";
                    else
                        lbl_pt_rent_max.Text = dr["pt_rent_max"].ToString();

                }


                if (dr["pt_min_sqft"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_min_sqft"]) == 0)
                        lbl_pt_min_sqft.Text = "Any";
                    else
                        lbl_pt_min_sqft.Text = dr["pt_min_sqft"].ToString();
                }



                if (dr["pt_electricity_inc"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_electricity_inc"]) == 1)
                        pt_electricity_inc.Checked = true;
                    else
                        pt_electricity_inc.Checked = false;
                }


                if (dr["pt_heat_inc"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_heat_inc"]) == 1)
                        pt_heat_inc.Checked = true;
                    else
                        pt_heat_inc.Checked = false;
                }



                if (dr["pt_water_inc"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_water_inc"]) == 1)
                        pt_water_inc.Checked = true;
                    else
                        pt_water_inc.Checked = false;
                }


                if (dr["pt_water_tax_inc"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_water_tax_inc"]) == 1)
                        pt_water_tax_inc.Checked = true;
                    else
                        pt_water_tax_inc.Checked = false;
                }



                if (dr["pt_parking_inc"] != System.DBNull.Value)
                {

                    if (Convert.ToInt32(dr["pt_parking_inc"]) == 1)
                        pt_parking_inc.Checked = true;
                    else
                        pt_parking_inc.Checked = false;

                }


                if (dr["pt_garage_inc"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_garage_inc"]) == 1)
                        pt_garage_inc.Checked = true;
                    else
                        pt_garage_inc.Checked = false;
                }


                if (dr["pt_furnished_inc"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_furnished_inc"]) == 1)
                        pt_furnished_inc.Checked = true;
                    else
                        pt_furnished_inc.Checked = false;
                }

                if (dr["pt_semi_furnished_inc"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_semi_furnished_inc"]) == 1)
                        pt_semi_furnished_inc.Checked = true;
                    else
                        pt_semi_furnished_inc.Checked = false;
                }


                lbl_tenantapplication_landlord_name.Text = dr["tenantapplication_landlord_name"].ToString();
                lbl_tenantapplication_current_employer.Text = dr["tenantapplication_current_employer"].ToString();
                lbl_tenantapplication_landlord_tel.Text = dr["tenantapplication_landlord_tel"].ToString();
                lbl_tenantapplication_monthly_income.Text = dr["tenantapplication_monthly_income"].ToString();
                lbl_tenantapplication_people_with_prospect.Text = dr["tenantapplication_people_with_prospect"].ToString();
                lbl_tenantapplication_reason_depart.Text = dr["tenantapplication_reason_depart"].ToString();
                // lbl_name_ssn.Text = dr["tenantapplication_ssn"].ToString();

                DateTime date_begin_ta = new DateTime();

                if (dr["tenantapplication_employer_since"] != System.DBNull.Value)
                {
                    date_begin_ta = Convert.ToDateTime(dr["tenantapplication_employer_since"]);
                    lbl_tenantapplication_employer_since.Text = date_begin_ta.Month.ToString() + "/" + date_begin_ta.Day.ToString() + "/" + date_begin_ta.Year.ToString();
                }

                pt_com.Text = dr["pt_com"].ToString();

            }

            cmd3.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd3.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["n_id"]);


                SqlDataAdapter da = new SqlDataAdapter(cmd3);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_tenant_lease_archives.DataSource = dt;
                gv_tenant_lease_archives.DataBind();
            }
            finally
            {
                //conn.Close();
            }

            cmd4.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd4.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd4.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["n_id"]);


                SqlDataAdapter da = new SqlDataAdapter(cmd4);
                DataTable dt = new DataTable();
                da.Fill(dt);


                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
            finally
            {
                //  conn.Close();
            }


            cmd5.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd5.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd5.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["n_id"]);


                SqlDataAdapter da = new SqlDataAdapter(cmd5);
                DataTable dt = new DataTable();
                da.Fill(dt);


                r_ontime_rent.DataSource = dt;
                r_ontime_rent.DataBind();
            }
            finally
            {
                // conn.Close();
            }


            cmd6.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd6.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd6.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["n_id"]);


                SqlDataAdapter da = new SqlDataAdapter(cmd6);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_tenant_payment_archive.DataSource = dt;
                gv_tenant_payment_archive.DataBind();
            }
            finally
            {
                //  conn.Close();
            }


            cmd7.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd7.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd7.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["n_id"]);


                SqlDataAdapter da = new SqlDataAdapter(cmd7);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_tenant_late_payment_archive.DataSource = dt;
                gv_tenant_late_payment_archive.DataBind();
            }
            finally
            {
                conn.Close();
            }

        }
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_tenant_payment_archive_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        tiger.Tenant tenant = new tiger.Tenant(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_tenant_payment_archive.PageIndex = e.NewPageIndex;
        gv_tenant_payment_archive.DataSource = tenant.getTenantPaymentArchive(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["n_id"]));
        gv_tenant_payment_archive.DataBind();

    }


    /// <summary>
    /// 
    /// </summary>
    private void SetDefaultView()
    {
        TabContainer1.ActiveTabIndex = 0;

    }
  

}
