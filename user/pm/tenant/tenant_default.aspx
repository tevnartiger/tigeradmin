﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" CodeFile="tenant_default.aspx.cs" Inherits="manager_tenant_tenant_default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table>
<tr><td colspan="2"><h1>TENANT</h1></td></tr>
<tr><td><h2>Search</h2></td><td>Enable you to search for all tenants and prospective tenant on your account.</td></tr>
<tr><td><h2>Update Tenant</h2></td><td>Enable you to modify a selected tenant personal information.</td></tr>
<tr><td><h2>Add Prospect</h2></td><td>Enable you to add a possible futur tenant to your account.</td></tr>
<tr><td><h2>Delinquant Tenant</h2></td><td>Enable you to view and manage delinquant tenants.</td></tr>
<tr><td><h2>Profile</h2></td><td>Enable you to view summary of tenant information.</td></tr>

</table>
</asp:Content>

