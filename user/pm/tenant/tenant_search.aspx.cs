﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.NameObjectAuthorization;


/// <summary>
/// Done by : Stanley Jocelyn
/// date    : may 22 , 2008
/// </summary>
public partial class manager_tenant_tenant_search : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Request.QueryString["l"] != string.Empty && Request.QueryString["l"] != null && Request.QueryString["l"] != "")
            {
                if (!RegEx.IsAlpha(Request.QueryString["l"]))
                {
                    Session.Abandon();
                    Response.Redirect("~/login.aspx");
                }

            }



            if (!RegEx.IsInteger(Request.QueryString["h"]))
            {
                Session.Abandon();
                Response.Redirect("~/login.aspx");
            }


          // VOIR LE MARK-UP  POUR LE PAGE LOAD 

            SetDefaultView();


            if(Request.QueryString["a"] == "i")
            {
               //TabContainer1.ActiveTabIndex = 2;
            }


            //ATTENTION INTRODUIRE UN REGEXPRESSION pour verifier le querystring

            if (Request.QueryString["h"] != "" && Request.QueryString["h"] != null)
            {
              //  ddl_contact_category.SelectedValue = "0";

                link_a.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=a";
                link_b.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=b";
                link_c.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=c";
                link_d.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=d";
                link_e.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=e";
                link_f.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=f";
                link_g.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=g";
                link_h.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=h";
                link_i.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=i";
                link_j.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=j";
                link_k.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=k";
                link_l.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=l";
                link_m.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=m";
                link_n.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=n";
                link_o.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=o";
                link_p.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=p";
                link_q.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=q";
                link_r.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=r";
                link_s.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=s";
                link_t.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=t";
                link_u.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=u";
                link_v.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=v";
                link_w.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=w";
                link_x.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=x";
                link_y.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=y";
                link_z.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + Request.QueryString["h"] + "&l=z";



                link_i_a.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=a&cat=in";
                link_i_b.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=b&cat=in";
                link_i_c.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=c&cat=in";
                link_i_d.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=d&cat=in";
                link_i_e.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=e&cat=in";
                link_i_f.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=f&cat=in";
                link_i_g.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=g&cat=in";
                link_i_h.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=h&cat=in";
                link_i_i.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=i&cat=in";
                link_i_j.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=j&cat=in";
                link_i_k.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=k&cat=in";
                link_i_l.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=l&cat=in";
                link_i_m.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=m&cat=in";
                link_i_n.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=n&cat=in";
                link_i_o.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=o&cat=in";
                link_i_p.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=p&cat=in";
                link_i_q.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=q&cat=in";
                link_i_r.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=r&cat=in";
                link_i_s.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=s&cat=in";
                link_i_t.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=t&cat=in";
                link_i_u.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=u&cat=in";
                link_i_v.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=v&cat=in";
                link_i_w.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=w&cat=in";
                link_i_x.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=x&cat=in";
                link_i_y.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=y&cat=in";
                link_i_z.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=0&l=z&cat=in";

            }



            tiger.PM l = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getPMHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
            tiger.PM tenant = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


            int home_id = l.getPMHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

            // first we check if there's any property available
            if (home_count > 0)
            {
                //tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ddl_home_active_list.DataSource = l.getPMHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                ddl_home_active_list.DataBind();
                ddl_home_active_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));

                if (Request.QueryString["h"] != "" && Request.QueryString["h"] != null)
                {

                    NameObjectAuthorization homeAuthorization = new NameObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

                    // check home authorization
                    ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                    if (Convert.ToInt32(Request.QueryString["h"]) > 0 && !homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["h"]), Convert.ToInt32(Session["name_id"]), 6))
                    {
                        Session.Abandon();
                        Response.Redirect("~/login.aspx");
                    }
                    ///////// SECURITY OBJECT CHECK  END ////////////////////////////////


                    ddl_home_active_list.SelectedValue = Request.QueryString["h"];


                    //-------------------

                   //TabContainer1.ActiveTabIndex = 0;
                   if (Request.QueryString["l"] == "" || Request.QueryString["l"] == null)
                   {
                       gv_tenant_list.Visible = false;

                       gv_tenant_search_list.DataSource = tenant.getPMTenantCurrentSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_active_list.SelectedValue), RegEx.getText(TextBox1.Text), Convert.ToInt32(Session["name_id"]));
                       gv_tenant_search_list.DataBind();
                    }
                    //=-------------

                  
                }

                
            }

            tiger.Tenant pt = new tiger.Tenant(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_pt_list.DataSource = pt.getProspectiveTenantList(Convert.ToInt32(Session["schema_id"]));
            gv_pt_list.DataBind();

            if (Request.QueryString["cat"] == "in")
            {
                //TabContainer1.ActiveTabIndex = 2;

                gv_tenant_search_list2.DataSource = pt.getTenantInactiveSearchList(Convert.ToInt32(Session["schema_id"]), 0, Request.QueryString["l"]);
                gv_tenant_search_list2.DataBind();

            }
            else
            {
                gv_tenant_search_list2.DataSource = pt.getTenantInactiveSearchList(Convert.ToInt32(Session["schema_id"]), 0, "");
                gv_tenant_search_list2.DataBind();
            }

            



        }
        else 
        {
           // if(gv_tenant_list.Visible==false)
           // gv_tenant_list.Visible = true;

           // gv_tenant_search_list.Visible = false;

        }
    }





    protected void submit_Click(object sender, EventArgs e)
    {
        //TabContainer1.ActiveTabIndex = 0;

        gv_tenant_list.Visible = false;
        string name = "";
        tiger.PM tenant = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_tenant_search_list.DataSource = tenant.getPMTenantCurrentSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_active_list.SelectedValue), RegEx.getText(TextBox1.Text), Convert.ToInt32(Session["name_id"]));
        gv_tenant_search_list.DataBind();



       /* WebService2 ser = new WebService2();
        gv_tenant_list.Visible = false;
        string name = "";
        tiger.Tenant tenant = new tiger.Tenant(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_tenant_search_list.DataSource = ser.getTenantCurrentSearchList(Convert.ToInt32(Session["schema_id"]), 0, TextBox1.Text); //tenant.getTenantCurrentSearchList(Convert.ToInt32(Session["schema_id"]), 0, TextBox1.Text);
        gv_tenant_search_list.DataBind(); */
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void submit2_Click(object sender, EventArgs e)
    {
        //TabContainer1.ActiveTabIndex = 2;
        

        gv_tenant_list2.Visible = false;
        string name = "";
        tiger.Tenant tenant = new tiger.Tenant(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_tenant_search_list2.DataSource = tenant.getTenantInactiveSearchList(Convert.ToInt32(Session["schema_id"]), 0, TextBox2.Text);
        gv_tenant_search_list2.DataBind();


        /* WebService2 ser = new WebService2();
         gv_tenant_list.Visible = false;
         string name = "";
         tiger.Tenant tenant = new tiger.Tenant(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
         gv_tenant_search_list.DataSource = ser.getTenantCurrentSearchList(Convert.ToInt32(Session["schema_id"]), 0, TextBox1.Text); //tenant.getTenantCurrentSearchList(Convert.ToInt32(Session["schema_id"]), 0, f.Text);
         gv_tenant_search_list.DataBind(); */
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_tenant_search_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        
        //TabContainer1.ActiveTabIndex = 0;

            // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
            tiger.PM tenant = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_tenant_search_list.PageIndex = e.NewPageIndex;
            gv_tenant_search_list.DataSource = tenant.getPMTenantCurrentSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_active_list.SelectedValue), RegEx.getText(TextBox1.Text), Convert.ToInt32(Session["name_id"]));
            gv_tenant_search_list.DataBind();
     
    }

/// <summary>
/// 
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
    protected void gv_tenant_search_list2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        //TabContainer1.ActiveTabIndex = 2;
      

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
        tiger.Tenant tenant = new tiger.Tenant(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_tenant_search_list2.PageIndex = e.NewPageIndex;
        gv_tenant_search_list2.DataSource = tenant.getTenantInactiveSearchList(Convert.ToInt32(Session["schema_id"]), 0, TextBox2.Text);
        gv_tenant_search_list2.DataBind();
    }


    /// <summary>
    /// 
    /// </summary>
    private void SetDefaultView()
    {
      //TabContainer1.ActiveTabIndex = 0;
    }



    protected void ddl_home_active_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        link_a.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=a";
        link_b.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=b";
        link_c.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=c";
        link_d.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=d";
        link_e.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=e";
        link_f.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=f";
        link_g.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=g";
        link_h.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=h";
        link_i.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=i";
        link_j.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=j";
        link_k.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=k";
        link_l.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=l";
        link_m.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=m";
        link_n.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=n";
        link_o.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=o";
        link_p.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=p";
        link_q.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=q";
        link_r.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=r";
        link_s.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=s";
        link_t.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=t";
        link_u.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=u";
        link_v.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=v";
        link_w.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=w";
        link_x.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=x";
        link_y.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=y";
        link_z.NavigateUrl = "~/user/pm/tenant/tenant_search.aspx?h=" + ddl_home_active_list.SelectedValue + "&l=z";


        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
        tiger.PM tenant = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_tenant_search_list.DataSource = tenant.getPMTenantCurrentSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_active_list.SelectedValue), Request.QueryString["l"], Convert.ToInt32(Session["name_id"]));
        gv_tenant_search_list.DataBind();

        gv_tenant_list.Visible = false;

        //TabContainer1.ActiveTabIndex = 0;
        
       

        
      
    }


    
}
