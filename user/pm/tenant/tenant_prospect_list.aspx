
<%@ Page Language="C#" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true"CodeFile="tenant_prospect_list.aspx.cs" Inherits="tenant_tenant_prospect_list" Title="Tenant Prospect View" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
    PROSPECTIVE TENANT LIST<br /><br />
    <div>
    <asp:GridView ID="gv_pt_list" AutoGenerateColumns="false" 
     AlternatingRowStyle-BackColor="#F0F0F6"  HeaderStyle-BackColor="#F0F0F6"
     Width="100%" BorderColor="#CDCDCD" BorderWidth="1"  runat="server" >
    <Columns>
    <asp:BoundField HeaderText="Name"  DataField="name" />
    <asp:BoundField DataField="income" HeaderText="Monthly Income"  DataFormatString="{0:0.00}" />
    <asp:BoundField DataField="city" HeaderText="Current city" />
    <asp:BoundField DataField="nb_bedrooms" HeaderText="Nb bedrooms" />
    <asp:BoundField DataField="trace_date_insert" HeaderText="Application date"
       DataFormatString="{0:MMM dd , yyyy}"  
       HtmlEncode="false" />
    <asp:BoundField DataField="pt_date_begin" HeaderText="Desired begin date"
      DataFormatString="{0:MMM dd , yyyy}"   HtmlEncode="false" />
    <asp:HyperLinkField  HeaderText="View details"  Text="view"
     DataNavigateUrlFields="pt_id" 
     DataNavigateUrlFormatString="~/manager/tenant/tenant_prospect_view.aspx?pt_id={0}"/>
    </Columns> 
    <PagerSettings Mode="NumericFirstLast" />   
    </asp:GridView><br /><br />
     <asp:HyperLink ID="link_main" runat="server" NavigateUrl="~/home/home_main.aspx">go back to main</asp:HyperLink>           
    </div>
   
  </asp:Content>