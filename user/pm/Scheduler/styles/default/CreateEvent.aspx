<%@ Page Language="C#" AutoEventWireup="true" Inherits="OboutInc.Scheduler.Templates.CreateEvent" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link rel="Stylesheet" media="all" href="res/CreateEvent.css" />
</head>
<body>
    <div>
    <table style="width:100%;background-color:#C6DBFF;">
        <tr>
            <td valign="top">
            <a id="btnBack" class="btnBack" href="javascript:;">�Back to scheduler</a>
            <input id="btnSave" class="btnSave" type="button" value="Save" />
            <input id="btnCancel" class="btnCancel" type="button" value="Cancel" />
            
            </td>
        </tr>
    </table>
    
    <table style="width:100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td style="width:5px;height:5px;"></td>
            <td></td>
            <td style="width:5px;height:5px;"></td>           
        </tr>
        <tr>
            <td></td>
            <td>
                    <table style="width:100%;" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="width:1px;"><img src="res/CreateEventTopLeft.gif" /></td>
                            <td class="tdBackground"></td>
                            <td style="width:1px;"><img src="res/CreateEventTopRight.gif" /></td>            
                        </tr>
                        <tr>
                            <td class="tdBackground"></td>
                            <td class="tdBackground">
                                    <table style="width:95%;" cellpadding="0" cellspacing="0">
                                        <tr><td class="CssSeparator"></td></tr>
                                        <tr>
                                            <td style="width:100px;">&nbsp;<b>What</b></td>
                                            <td>
                                                <input type="text" id="txtSubject" class="txtSubject" />
                                            </td>
                                        </tr>
                                        <tr><td class="CssSeparator"></td></tr>
                                        <tr>
                                            <td>&nbsp;<b>When</b></td>
                                            <td>
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td><input type="text" id="txtStartDate" class="txtStartDate" /></td>
                                                        <td id="tdStartTime">
                                                            &nbsp;<input type="text" id="txtStartTime" class="txtStartTime" />
                                                            <div id="ddStartTime" class="customDropDown" style="width:98px;height:120px;display:none;">
                                                                <table id="tableddStartTime" style="width:100%;" cellpadding="0" cellspacing="0">
                                                                    <tbody></tbody>
                                                                </table>
                                                            </div>
                                                        
                                                        </td>
                                                        <td>&nbsp;to</td>
                                                        <td id="tdEndTime">
                                                            &nbsp;<input type="text" id="txtEndTime" class="txtEndTime" />
                                                            <div id="ddEndTime" class="customDropDown" style="width:150px;height:120px;display:none;">
                                                                <table id="tableddEndTime" style="width:100%;" cellpadding="0" cellspacing="0">
                                                                    <tbody></tbody>
                                                                </table>
                                                            </div>                                                        
                                                        </td>
                                                        <td>&nbsp;<input type="text" id="txtEndDate" class="txtEndDate" /></td>
                                                        <td>&nbsp;<input type="checkbox" id="chkAllDay" /><label for="chkAllDay">All day</label></td>
                                                        
                                                    </tr>
                                                </table>
                                                <iframe id="iframeCalendar" style="display:none;position:absolute;width:150px;height:160px;" frameborder="0" scrolling="no"  ></iframe>
                                            </td>
                                        </tr>
                                        <tr><td class="CssSeparator"></td></tr>                                        
                                        <tr>
                                            <td></td>
                                            <td>
                                                <table style="width:100%;">
                                                    <tr>
                                                        <td style="width:20px;"></td>
                                                        <td>
                                                        
                                                        
                                                            <table style="width:100%;display:none;" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td>
                                                                    
                                                                        <table >
                                                                            <tr>
                                                                                <td >
                                                                                <b>Repeats:</b>
                                                                                </td>
                                                                                <td>
                                                                                    <select id="ddRepeats" class="ddRepeats" disabled="disabled">
                                                                                        <option value="no">Do not repeat</option>
                                                                                        <option value="daily">Daily</option>
                                                                                        <option value="weekly">Weekly</option>
                                                                                        <option value="monthly">Monthly</option>
                                                                                        <option value="yearly">Yearly</option>
                                                                                    </select>
                                                                                
                                                                                
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    
                                                                    </td>
                                                                </tr>
                                                                <tr><td class="CssSeparator"></td></tr>
                                                                <tr><td class="RepeatSeparator"></td></tr>
                                                            </table>
                                                            
                                                            <div id="divRepeatSumary" style="display:none;">
                                                            <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="height:44px;">
                                                                        
                                                                        <span id="txtRepeatSumary" class="txtRepeatSumary">Daily</span>
                                                                    </td>
                                                                </tr>
                                                                <tr><td class="RepeatSeparator"></td></tr>
                                                            </table>
                                                            </div>
                                                            <div id="divRepeatFrequency" style="display:none;">
                                                            <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                                <tr><td class="CssSeparator"></td></tr>
                                                                <tr>
                                                                    <td >
                                                                        <b>Repeat every:</b>
                                                                        <select id="ddRepeatFrequency" style="width:40px;" >
                                                                            <option value="1">1</option>                                                                            
                                                                            <option value="2">2</option>
                                                                            <option value="3">3</option>
                                                                            <option value="4">4</option>
                                                                            <option value="5">5</option>
                                                                            <option value="6">6</option>
                                                                            <option value="7">7</option>
                                                                            <option value="8">8</option>
                                                                            <option value="9">9</option>
                                                                            <option value="10">10</option>
                                                                            <option value="11">11</option>
                                                                            <option value="12">12</option>
                                                                            <option value="13">13</option>
                                                                            <option value="14">14</option>
                                                                            
                                                                        </select>
                                                                        <span id="txtRepeatFrequencyUnit">day</span>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr><td class="CssSeparator"></td></tr>
                                                                <tr><td class="RepeatSeparator"></td></tr>
                                                            </table>
                                                            </div>
                                                            <div id="divRepeatBy" style="display:none;">
                                                            <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                                <tr><td class="CssSeparator"></td></tr>
                                                                <tr>
                                                                    <td >
                                                                        <b>Repeat By:</b>
                                                                        <br />
                                                                        <table>
                                                                            <tr>
                                                                                <td class="indent10px"></td>
                                                                                <td>                                                                                
                                                                                    <input id="rdDayOfMonth" type="radio" checked="checked" name="RepeatByType" />
                                                                                    <label for="rdDayOfMonth">day of the month</label>
                                                                                </td>
                                                                                <td>                                                                                
                                                                                    <input id="rdDayOfWeek" type="radio" name="RepeatByType" />
                                                                                    <label for="rdDayOfWeek">day of the week</label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>              
                                                                    </td>
                                                                </tr>
                                                                <tr><td class="CssSeparator"></td></tr>
                                                                <tr><td class="RepeatSeparator"></td></tr>
                                                            </table>
                                                            </div>
                                                            <div id="divRepeatOn" style="display:none;">
                                                            <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                                <tr><td class="CssSeparator"></td></tr>
                                                                <tr>
                                                                    <td >
                                                                        <b>Repeat On:</b>
                                                                        <br />
                                                                        <table>
                                                                            <tr>
                                                                                <td class="indent10px"></td>
                                                                                <td>
                                                                                    <input type="checkbox" id="chkSun" name="chkRepeatOnGroups" /><label for="chkS">Sun</label>
                                                                                    <input type="checkbox" id="chkMon" name="chkRepeatOnGroups" /><label for="chkS">Mon</label>
                                                                                    <input type="checkbox" id="chkTue" name="chkRepeatOnGroups" /><label for="chkS">Tue</label>
                                                                                    <input type="checkbox" id="chkWed" name="chkRepeatOnGroups" /><label for="chkS">Wed</label>
                                                                                    <input type="checkbox" id="chkThu" name="chkRepeatOnGroups" /><label for="chkS">Thu</label>
                                                                                    <input type="checkbox" id="chkFri" name="chkRepeatOnGroups" /><label for="chkS">Fri</label>
                                                                                    <input type="checkbox" id="chkSat" name="chkRepeatOnGroups" /><label for="chkS">Sat</label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr><td class="CssSeparator"></td></tr>
                                                                <tr><td class="RepeatSeparator"></td></tr>
                                                            </table>
                                                            </div>
                                                            <div id="divRepeatRange" style="display:none;">
                                                            <table style="width:100%;" cellpadding="0" cellspacing="0">
                                                                <tr><td class="CssSeparator"></td></tr>
                                                                <tr>
                                                                    <td >
                                                                        <b>Range:</b>
                                                                        <table>
                                                                            <tr>
                                                                                <td class="indent10px"></td>
                                                                                <td>
                                                                                    Start: <input type="text" id="txtRepeatStartDate" class="textRepeatStartDate" disabled="disabled" />
                                                                                </td>
                                                                                <td>
                                                                                    <table cellpadding="0" cellspacing="0">
                                                                                        <tr>
                                                                                            <td rowspan="2">Ends:</td>
                                                                                            <td>
                                                                                                
                                                                                                <input id="rdNever" type="radio" checked="checked" name="endType" >
                                                                                                <label for="rdNever">Never</label>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>
                                                                                                <table style="height:25px;" cellpadding="0" cellspacing="0">
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <input id="rdUntil" type="radio" name="endType" >
                                                                                                            <label for="rdUntil">Until</label>
                                                                                                        </td>
                                                                                                        <td id="tdRangeUntilDate"  style="display:none;">
                                                                                                            &nbsp;<input type="text" id="txtRangeUntilDate" class="txtRangeUntilDate" />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                                                                                                                           
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>                                                                                
                                                                            </tr>
                                                                        </table>                                                                        
                                                                    </td>
                                                                </tr>
                                                                <tr><td class="CssSeparator"></td></tr>
                                                                <tr><td class="RepeatSeparator"></td></tr>
                                                            </table>
                                                            </div>
                                                        
                                                        </td>
                                                    </tr>
                                                </table>
                                            
                                            </td>
                                        </tr>
                                        
                                        <tr><td class="CssSeparator"></td></tr>
                                        <tr>
                                            <td>&nbsp;<b>Where</b></td>
                                            <td>
                                                <input type="text" id="txtPlace" class="txtPlace"/>
                                            </td>
                                        </tr>
                                        <tr><td class="CssSeparator"></td></tr>
                                        <tr>
                                            <td>&nbsp;<b>Category</b></td>
                                            <td>
                                                <select id="selCategories" style="width:50%;"></select>
                                            </td>
                                        </tr>
                                                    
                                        <tr><td class="CssSeparator"></td></tr>
                                        <tr>
                                            <td valign="top">&nbsp;<b>Description</b></td>
                                            <td>
                                                <textarea id="txtDescription" class="txtDescription" rows="5" ></textarea>
                                            </td>
                                        </tr>
                                                    
                                        <tr><td class="CssSeparator"></td></tr>
                                        
                                    </table>
                            </td>
                            <td class="tdBackground"></td>            
                        </tr>
                        <tr>
                            <td style="width:1px;"><img src="res/CreateEventBottomLeft.gif" /></td>
                            <td class="tdBackground"></td>
                            <td style="width:1px;"><img src="res/CreateEventBottomRight.gif" /></td>            
                        </tr>        
                    </table>
            </td>
            <td></td>
        </tr>        
    </table>
    
    
    
    
    </div>
    <%=EmbededScript%>    
</body>
</html>
