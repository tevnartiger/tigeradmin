<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="aspnet_scheduler.master" %>
<%@ Register TagPrefix="osd" Namespace="OboutInc.Scheduler" Assembly="obout_Scheduler_NET"%>

    <script runat="server">
        void Page_Load(Object o, EventArgs e)
        {
            Page.Title = "Scheduler Client-side Update Event Handle Example";
            schedule.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("../App_Data/scheduler.mdb");
            schedule.Login(1);            
        }
    </script>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
            <osd:Scheduler runat="server" ID="schedule" StyleFolder="styles/default" 
            Width="990" Height="500" 
            ProviderName = "System.Data.OleDb"
            EventsTableName="Events"
            UserSettingsTableName="UserSettings"
            CategoriesTableName="Categories"
            OnClientBeforeUpdateEvent = "handleBeforeUpdate"
            OnClientUpdateEvent = "handleAfterUpdate"
            >
            </osd:Scheduler>
<br />
<i>Note</i>: Try to move event arround
    <script type="text/javascript">
        function handleBeforeUpdate(sender,args)
        {
            
            if (args.OriginalEvent.StartTime-args.UpdatedEvent.StartTime!=0)
            {
                if (!confirm("The event ["+args.OriginalEvent.Subject+"] will be moved to "+args.UpdatedEvent.StartTime.format("HH:MM mm/dd/yyyy")))
                {
                    alert("Update has been canceled.Event will be resumed back to its original time which is "+args.OriginalEvent.StartTime.format("HH:MM mm/dd/yyyy"));
                    args.UpdateEventCancel = true;
                }
            }
            
            //args.UpdateEventCancel = true;
        }
        function handleAfterUpdate(sender,args)
        {
            alert("Your event has been updated to "+args.UpdatedEvent.StartTime.format("HH:MM mm/dd/yyyy"));
        }        
    </script>            
            
</asp:Content>            