<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="aspnet_scheduler.master" %>
<%@ Register TagPrefix="osd" Namespace="OboutInc.Scheduler" Assembly="obout_Scheduler_NET"%>

    <script runat="server">
        void Page_Load(Object o, EventArgs e)
        {
            Page.Title = "Scheduler Client-side Update Event Handle Example";
            schedule.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("../App_Data/scheduler.mdb");
            schedule.Login(1);            
        }
    </script>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
            <osd:Scheduler runat="server" ID="schedule" StyleFolder="styles/default" 
            Width="990" Height="500" 
            ProviderName = "System.Data.OleDb"
            EventsTableName="Events"
            UserSettingsTableName="UserSettings"
            CategoriesTableName="Categories"
            OnClientBeforeDeleteEvent = "handleBeforeDelete"
            OnClientDeleteEvent = "handleAfterDelete"
            >
            </osd:Scheduler>
<br />
<i>Note</i>: Delete event has been cancelled
    <script type="text/javascript">
    
    
        function handleBeforeDelete(sender,args)
        {
            alert("Sorry, you have no right to delete events");
            args.DeleteEventCancel = true;
        }
        function handleAfterDelete(sender,args)
        {
            //never reach, since the delete action has been cancelled in OnClientBeforeDeleteEvent
        }        
    </script>            
            
</asp:Content>            