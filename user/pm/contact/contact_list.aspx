﻿<%@ Page Language="C#" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" CodeFile="contact_list.aspx.cs" Inherits="manager_contact_contact_list" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <table width="100%">
           <tr>
               <td bgcolor="aliceblue">
                <b>  <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_u_contacts %>"> </asp:Label></b>&nbsp;</td>
           </tr>
       </table>
       &nbsp;<br />
       <asp:Label ID="lbl_contact_search" runat="server" style="font-weight: 700" 
           Text="<%$ Resources:Resource, lbl_contact_search %>"></asp:Label>
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
       &nbsp;&nbsp;&nbsp;&nbsp;
       <asp:Button ID="submit" runat="server" onclick="submit_Click" 
           Text="<%$ Resources:Resource, lbl_submit %>" />
           &nbsp;&nbsp;
                    <asp:RegularExpressionValidator 
                        ID="reg_TextBox1" runat="server" 
                         ControlToValidate="TextBox1"
                        ErrorMessage="invalid name">
                        </asp:RegularExpressionValidator>
       <br />
    <hr />      
       <br />
       &nbsp;<asp:HyperLink ID="link_a" runat="server" >[A]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_b" runat="server" >[B]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_c" runat="server" >[C]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_d" runat="server" >[D]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_e" runat="server" >[E]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_f" runat="server" >[F]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_g" runat="server" >[G]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_h" runat="server" >[H]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_i" runat="server" >[I]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_j" runat="server" >[J]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_k" runat="server" >[K]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_l" runat="server" >[L]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_m" runat="server" >[M]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_n" runat="server" >[N]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_o" runat="server" >[O]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_p" runat="server" >[P]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_q" runat="server" >[Q]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_r" runat="server" >[R]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_s" runat="server" >[S]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_t" runat="server" >[T]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_u" runat="server" >[U]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_v" runat="server" >[V]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_w" runat="server">[W]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_x" runat="server" >[X]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_y" runat="server" >[Y]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_z" runat="server" >[Z]</asp:HyperLink>
       <br />
       <br />
       <b>Contact category</b>&nbsp;
       <asp:DropDownList ID="ddl_contact_category" runat="server"  AutoPostBack="true"
        onselectedindexchanged="ddl_contact_category_SelectedIndexChanged">
           <asp:ListItem Text="<%$ Resources:Resource, lbl_all %>" Value="0"></asp:ListItem>
           <asp:ListItem Text="<%$ Resources:Resource, lbl_contractor %>"  Value="1"></asp:ListItem>
           <asp:ListItem Text="<%$ Resources:Resource, lbl_supplier_vendor %>" Value="2"></asp:ListItem>
           <asp:ListItem Text="<%$ Resources:Resource, lbl_warehouse %>"  Value="5"></asp:ListItem>
    </asp:DropDownList>
       <br />
       <br />
       <asp:GridView ID="gv_contact_list" runat="server" AllowPaging="True" 
           AllowSorting="True" AlternatingRowStyle-BackColor="#F0F0F6"  
           AutoGenerateColumns="false" BorderColor="#CDCDCD" BorderWidth="1" 
           GridLines="Both"
           DataKeyNames="id" OnPageIndexChanging="gv_contact_list_PageIndexChanging"
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" 
           HeaderStyle-BackColor="AliceBlue" PageSize="10" Width="90%">
           
 
           
          
           
           
           <Columns>
               <asp:BoundField DataField="lname" 
                   HeaderText="<%$ Resources:Resource, lbl_lname %>" ReadOnly="True" 
                   />
               <asp:BoundField DataField="fname" 
                   HeaderText="<%$ Resources:Resource, lbl_fname %>" ReadOnly="True" 
                    />
               <asp:BoundField DataField="tel" 
                   HeaderText="<%$ Resources:Resource, lbl_tel %>" 
                    />
               <asp:BoundField DataField="email" 
                   HeaderText="<%$ Resources:Resource, lbl_email %>" 
                   />
               
               <asp:TemplateField HeaderText='<%$ Resources:Resource, lbl_category  %>' >
               <ItemTemplate>
               <asp:Label runat="server"  ID="lbl_categ1"  
                          Text='<%#Get_ContactCateg(Convert.ToString(Eval("categ")))%>'/> 
               </ItemTemplate>
               </asp:TemplateField>
               
               <asp:BoundField DataField="company_name" 
                   HeaderText="<%$ Resources:Resource, lbl_company %>"
                   />
                
               <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_view %>"  >
               <ItemTemplate>
                 <asp:HyperLink NavigateUrl='<%#Get_Link(Convert.ToString(Eval("id")),Convert.ToString(Eval("categ")))%>' Text="<%$ Resources:Resource, lbl_view %>" ID="HyperLink1" runat="server"></asp:HyperLink>
               </ItemTemplate>
               </asp:TemplateField>
               
           </Columns>
       </asp:GridView>
       <asp:GridView ID="gv_contact_search_list" runat="server" AllowPaging="True" 
           AllowSorting="True" AlternatingRowStyle-BackColor="#F0F0F6"  
           AutoGenerateColumns="false" BorderColor="#CDCDCD" BorderWidth="1" 
           GridLines="Both" EmptyDataText="<%$ Resources:Resource, lbl_none %>" 
           HeaderStyle-BackColor="AliceBlue" 
           OnPageIndexChanging="gv_contact_search_list_PageIndexChanging" PageSize="15" 
           Width="90%">
           <Columns>
               <asp:BoundField DataField="lname" 
                   HeaderText="<%$ Resources:Resource, lbl_lname %>" ReadOnly="True" 
                    />
               <asp:BoundField DataField="fname" 
                   HeaderText="<%$ Resources:Resource, lbl_fname %>" ReadOnly="True" 
                   />
               <asp:BoundField DataField="tel" 
                   HeaderText="<%$ Resources:Resource, lbl_tel %>" 
                   />
               <asp:BoundField DataField="email" 
                   HeaderText="<%$ Resources:Resource, lbl_email %>" 
                   />
             
               <asp:TemplateField HeaderText='<%$ Resources:Resource, lbl_category  %>' >
               <ItemTemplate>
               <asp:Label runat="server"  ID="lbl_categ2"  
                          Text='<%#Get_ContactCateg(Convert.ToString(Eval("categ")))%>'/> 
               </ItemTemplate>
               </asp:TemplateField>
    
                   
                   
                   
                   
               <asp:BoundField DataField="company_name" 
                   HeaderText="<%$ Resources:Resource, lbl_company %>"
                   SortExpression="company_name" />
               
               <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_view %>"  >
               <ItemTemplate>
                 <asp:HyperLink NavigateUrl='<%#Get_Link(Convert.ToString(Eval("id")),Convert.ToString(Eval("categ")))%>' Text="<%$ Resources:Resource, lbl_view %>" ID="HyperLink1" runat="server"></asp:HyperLink>
               </ItemTemplate>
               </asp:TemplateField>
           </Columns>
       </asp:GridView>
   
 
</asp:Content>

