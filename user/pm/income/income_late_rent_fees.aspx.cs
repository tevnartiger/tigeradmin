﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : may 25, 2009
/// </summary>
/// 
public partial class manager_income_income_late_rent_fees : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DateTime to = new DateTime();
            DateTime from = new DateTime();
            to = DateTime.Now; // the date in the to drop downlist

            tiger.Date d = new tiger.Date();
            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            to = Convert.ToDateTime(d.DateCulture(to.Month.ToString(), to.Day.ToString(), to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
            from = Convert.ToDateTime(d.DateCulture("1", "1", to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


            ddl_from_m.SelectedValue = from.Month.ToString();
            ddl_from_d.SelectedValue = from.Day.ToString();
            ddl_from_y.SelectedValue = from.Year.ToString();

            from = from.AddYears(-5);


            ddl_to_m.SelectedValue = to.Month.ToString();
            ddl_to_d.SelectedValue = to.Day.ToString();
            ddl_to_y.SelectedValue = to.Year.ToString();

            // First we check if there's home available
            tiger.PM h = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = h.getPMHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
            string link_to_unit = "";
            if (home_count > 0)
            {
                int home_id = h.getPMHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                // link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                //Session["schema_id"]));


                ddl_home_id.Visible = true;


                ddl_home_id.DataSource = h.getPMHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                ddl_home_id.SelectedValue = Convert.ToString(home_id);
                ddl_home_id.DataBind();
                ddl_home_id.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
                ddl_home_id.SelectedIndex = 0;


                //*********************************************
                // DropDownList pour les Unit
                //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

                if (unit_count > 0)
                {

                     ddl_unit_id.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
                 

                    // construction of the gridview of the rent payment archive
                    tiger.PM v = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    gv_rent_payment_archive.DataSource = v.getPMLateRentList(Convert.ToInt32(Session["schema_id"]), 0, 0, from, to, Convert.ToInt32(Session["name_id"]));
                    gv_rent_payment_archive.DataBind();

                    // if we find late rent then get the lowest due date
                    DateTime lowest_due_date;

                    if (gv_rent_payment_archive.Rows.Count > 0)
                    {
                       SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                       SqlCommand cmd = new SqlCommand("prPM_LateRentLowestDueDate", conn);
                       cmd.CommandType = CommandType.StoredProcedure;
       
                        //Add the params
                        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                        cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = 0;
                        cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                        cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = 0;
                        cmd.Parameters.Add("@date_from", SqlDbType.DateTime).Value = from;
                        cmd.Parameters.Add("@date_to", SqlDbType.DateTime).Value = to;

                      
                        try
                        {
                            conn.Open();

                            SqlDataReader dr = null;
                            dr = cmd.ExecuteReader(CommandBehavior.SingleRow);
                            while (dr.Read() == true)
                            {

                              lowest_due_date =  Convert.ToDateTime(dr["rp_due_date"]);
                              
                              ddl_from_m.SelectedValue = lowest_due_date.Month.ToString();
                              ddl_from_d.SelectedValue = lowest_due_date.Day.ToString();
                              ddl_from_y.SelectedValue = lowest_due_date.Year.ToString();
                               
                            }

                            

                        }


                        finally
                        {
                            conn.Close();
                        }
                        
                    }
                }

                 // if ther is no unit
                else
                {

                    //   txt_message.InnerHtml = "There is no unit in this property -- add unit";

                }

                //hidden fields



            }
            // if ther is no home

            else
            {
                //  txt_message.InnerHtml = "There is no property -- Add a property";

                //  txt_link.InnerHtml = home_count + " <a href='home_add.aspx'>Add Home</a>&nbsp;&nbsp;<a href='owner_add.aspx'>Add Owner</a>&nbsp;&nbsp;<a href='janitor_add.aspx'>Add Janitor</a>&nbsp;&nbsp;";
            }

            lbl_confirmation.Visible = false;
            lbl_unit_door_no.Visible = false;

            lbl_amount.Visible = false;
            lbl_late_fee_amount.Visible = false;

            Label2.Visible = false;
            lbl_unit2.Visible = false;
            Label3.Visible = false;


        }

    }

    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {

           ddl_home_id.Visible = true;

      
            tiger.Unit u = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
            //change list of unit id

          //  if (unit_id > 0)
            {
                ddl_unit_id.Visible = true;
                //first dispose of old ddl_unit
                ddl_unit_id.Dispose();
                ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
                ddl_unit_id.DataBind();
                ddl_unit_id.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
                ddl_unit_id.SelectedIndex = 0;

                DateTime to = new DateTime();
                DateTime from = new DateTime();
                to = DateTime.Now; // the date in the to drop downlist

                tiger.Date d = new tiger.Date();
                to = Convert.ToDateTime(d.DateCulture(to.Month.ToString(), to.Day.ToString(), to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
                from = Convert.ToDateTime(d.DateCulture("1", "1", to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));

                ddl_from_m.SelectedValue = from.Month.ToString();
                ddl_from_d.SelectedValue = from.Day.ToString();
                ddl_from_y.SelectedValue = from.Year.ToString();

                from = from.AddYears(-5);

                // construction of the gridview for the payment archive
                tiger.PM v = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                gv_rent_payment_archive.DataSource = v.getPMLateRentList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), 0, from, to, Convert.ToInt32(Session["name_id"]));
                gv_rent_payment_archive.DataBind();

                DateTime lowest_due_date;

                // if we find late rent then get the lowest due date
                if (gv_rent_payment_archive.Rows.Count > 0)
                {
                    SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    SqlCommand cmd = new SqlCommand("prPM_LateRentLowestDueDate", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    //Add the params
                    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                    cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
                    cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                    cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = 0;
                    cmd.Parameters.Add("@date_from", SqlDbType.DateTime).Value = from;
                    cmd.Parameters.Add("@date_to", SqlDbType.DateTime).Value = to;

                    try
                    {
                        conn.Open();

                        SqlDataReader dr = null;
                        dr = cmd.ExecuteReader(CommandBehavior.SingleRow);
                        while (dr.Read() == true)
                        {
                            lowest_due_date = Convert.ToDateTime(dr["rp_due_date"]);

                            ddl_from_m.SelectedValue = lowest_due_date.Month.ToString();
                            ddl_from_d.SelectedValue = lowest_due_date.Day.ToString();
                            ddl_from_y.SelectedValue = lowest_due_date.Year.ToString();

                        }
                    }
                    finally
                    {
                        conn.Close();
                    }

                }
                

                ddl_to_m.SelectedValue = to.Month.ToString();
                ddl_to_d.SelectedValue = to.Day.ToString();
                ddl_to_y.SelectedValue = to.Year.ToString();

            }
           
            //   }// fin if not postback
       
        lbl_confirmation.Visible = false;
        lbl_unit_door_no.Visible = false;

        lbl_amount.Visible = false;
        lbl_late_fee_amount.Visible = false;

        Label2.Visible = false;
        lbl_unit2.Visible = false;
        Label3.Visible = false;
    }


    protected void ddl_unit_id_SelectedIndexChanged(object sender, EventArgs e)
    {

        int unit_id;

        if (ddl_unit_id.SelectedIndex == 0)
            unit_id = 0;
        else
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        DateTime to = new DateTime();
        DateTime from = new DateTime();


        tiger.Date d = new tiger.Date();
        to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        // construction of the gridview for the payment archive
        tiger.PM v = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_rent_payment_archive.DataSource = v.getPMLateRentList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), unit_id, from, to, Convert.ToInt32(Session["name_id"]));
        gv_rent_payment_archive.DataBind();


        lbl_confirmation.Visible = false;
        lbl_unit_door_no.Visible = false;

        lbl_amount.Visible = false;
        lbl_late_fee_amount.Visible = false;

        Label2.Visible = false;
        lbl_unit2.Visible = false;
        Label3.Visible = false;



    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        int unit_id;

        if (ddl_unit_id.SelectedIndex == 0)
            unit_id = 0;
        else
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        //hd_unit_id.Value = Convert.ToString(ddl_unit_id.SelectedValue);
        // label_added_unit.Text = ddl_unit_id.SelectedItem.Text;
        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


        DateTime to = new DateTime();
        DateTime from = new DateTime();
        to = DateTime.Now; // the date in the to drop downlist

        tiger.Date d = new tiger.Date();
        to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        // construction of the gridview for the payment archive
        tiger.PM v = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_rent_payment_archive.DataSource = v.getPMLateRentList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), unit_id, from, to, Convert.ToInt32(Session["name_id"]));
        gv_rent_payment_archive.DataBind();


        lbl_confirmation.Visible = false;
        lbl_unit_door_no.Visible = false;

        lbl_amount.Visible = false;
        lbl_late_fee_amount.Visible = false;

        Label2.Visible = false;
        lbl_unit2.Visible = false;
        Label3.Visible = false;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_paid_Click(object sender, EventArgs e)
    {
        // get data from the textbox in the gridview ( from the GridView)


        //*********************************************************************
        //*********************************************************************
        Button btn_paid = (Button)sender;
        GridViewRow grdRow = (GridViewRow)btn_paid.Parent.Parent;
        string strField1 = grdRow.Cells[6].Text;

        TextBox tbx = (TextBox)grdRow.Cells[6].FindControl("gv_tbx_late_fee");
        lbl_late_fee_amount.Text = tbx.Text;


        HiddenField h_rp_id = (HiddenField)grdRow.Cells[6].FindControl("h_rp_id");

        HiddenField h_unit_id = (HiddenField)grdRow.Cells[6].FindControl("h_unit_id");

        HiddenField h_unit_door_no = (HiddenField)grdRow.Cells[6].FindControl("h_unit_door_no");
        //*********************************************************************
        //*********************************************************************
        lbl_unit_door_no.Text = h_unit_door_no.Value;

        // updating the database to pay the late fees from the row submited ( from the GridView)

        lbl_confirmation.Text = Resources.Resource.lbl_paid_late_fee_confirmation;
        //lbl_confirmation.ForeColor = "red";

        lbl_confirmation.Visible = true;
        lbl_unit_door_no.Visible = true;

        lbl_amount.Visible = true;
        lbl_amount.Text = Resources.Resource.lbl_amount;

        lbl_late_fee_amount.Visible = true;
        Label2.Visible = true;
        lbl_unit2.Visible = true;
        lbl_unit2.Text = Resources.Resource.lbl_unit;

        Label3.Visible = true;


        

               //------------------------------------------------------------------------------------

        if (RegEx.IsMoney(tbx.Text))
        {
               SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
               SqlCommand cmd = new SqlCommand("prLateRentFeePayment", conn);
               cmd.CommandType = CommandType.StoredProcedure;

               try
               {
                   conn.Open();
                   //Add the params
                   cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                   cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                   cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
                   cmd.Parameters.Add("@rp_id", SqlDbType.Int).Value = Convert.ToInt32(h_rp_id.Value);
                   cmd.Parameters.Add("@rp_paid_late_fee_amount", SqlDbType.SmallMoney).Value = Convert.ToDecimal(RegEx.getMoney(tbx.Text));

                   //execute the insert
                   cmd.ExecuteReader();
            
               }
               catch (Exception error)
               {
                 //  tiger.security.Error.errorAdd(conn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
               }


               //-------------------------------------------------------------------------------------

               DateTime to = new DateTime();
               DateTime from = new DateTime();

               tiger.Date d = new tiger.Date();
               to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
               from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


               // construction of the gridview for the payment archive
               tiger.PM v = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
               gv_rent_payment_archive.DataSource = v.getPMLateRentList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), 0, from, to, Convert.ToInt32(Session["name_id"]));
               gv_rent_payment_archive.DataBind();

        }



    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_ignore_Click(object sender, EventArgs e)
    {
        // get data from the textbox in the gridview ( from the GridView)
         //*********************************************************************
        //*********************************************************************
        Button btn_ignore = (Button)sender;
        GridViewRow grdRow = (GridViewRow)btn_ignore.Parent.Parent;
        string strField1 = grdRow.Cells[6].Text;

        TextBox tbx = (TextBox)grdRow.Cells[6].FindControl("gv_tbx_late_fee");
        lbl_late_fee_amount.Text = tbx.Text;

        HiddenField h_rp_id = (HiddenField)grdRow.Cells[6].FindControl("h_rp_id");

        HiddenField h_unit_id = (HiddenField)grdRow.Cells[6].FindControl("h_unit_id");

        HiddenField h_unit_door_no = (HiddenField)grdRow.Cells[6].FindControl("h_unit_door_no");
        //*********************************************************************
        //*********************************************************************
        lbl_unit_door_no.Text = h_unit_door_no.Value;

        // updating the database to pay the late fees from the row submited ( from the GridView)

        lbl_confirmation.Text = Resources.Resource.lbl_paid_late_fee_ignore_confirmation;
        // lbl_confirmation.ForeColor = "red";

        lbl_confirmation.Visible = true;
        lbl_unit_door_no.Visible = true;

        lbl_amount.Visible = false;
        lbl_late_fee_amount.Visible = false;

        Label2.Visible = false;
        lbl_unit2.Visible = true;
        lbl_unit2.Text = Resources.Resource.lbl_unit;

        Label3.Visible = true;




        //------------------------------------------------------------------------------------


        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prLateRentFeeIgnore", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@rp_id", SqlDbType.Int).Value = Convert.ToInt32(h_rp_id.Value);
        
            //execute the insert
            cmd.ExecuteReader();

        }
        catch (Exception error)
        {
            //  tiger.security.Error.errorAdd(conn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
        }


        //-------------------------------------------------------------------------------------

        DateTime to = new DateTime();
        DateTime from = new DateTime();

        tiger.Date d = new tiger.Date();
        to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        // construction of the gridview for the payment archive
        tiger.PM v = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_rent_payment_archive.DataSource = v.getPMLateRentList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), 0, from, to, Convert.ToInt32(Session["name_id"]));
        gv_rent_payment_archive.DataBind();
    }

}
