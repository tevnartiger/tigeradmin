﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;


public partial class manager_income_income_received_late_rent_fees : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DateTime to = new DateTime();
            DateTime from = new DateTime();
            to = DateTime.Now; // the date in the to drop downlist

            tiger.Date d = new tiger.Date();
            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            to = Convert.ToDateTime(d.DateCulture(to.Month.ToString(), to.Day.ToString(), to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
            from = Convert.ToDateTime(d.DateCulture("1", "1", to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));




            ddl_from_m.SelectedValue = from.Month.ToString();
            ddl_from_d.SelectedValue = from.Day.ToString();
            ddl_from_y.SelectedValue = from.Year.ToString();

            ddl_to_m.SelectedValue = to.Month.ToString();
            ddl_to_d.SelectedValue = to.Day.ToString();
            ddl_to_y.SelectedValue = to.Year.ToString();

            // First we check if there's home available
            tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = h.getHomeCount(Convert.ToInt32(Session["schema_id"]));
            string link_to_unit = "";
            if (home_count > 0)
            {
                int home_id = h.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));
                // link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                //Session["schema_id"]));


                ddl_home_id.Visible = true;


                ddl_home_id.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                ddl_home_id.SelectedValue = Convert.ToString(home_id);
                ddl_home_id.DataBind();



                //To view the address of the property

                tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
                rhome_view.DataBind();




                //*********************************************
                // DropDownList pour les Unit
                //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

                if (unit_count > 0)
                {

                    int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), home_id);


                    ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
                    ddl_unit_id.SelectedValue = Convert.ToString(unit_id);
                    ddl_unit_id.DataBind();
                    ddl_unit_id.Items.Insert(0, "-- All Units --");
                    ddl_unit_id.SelectedIndex = 0;




                    // construction of the gridview of the rent payment archive
                    tiger.Rent v = new tiger.Rent(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    gv_paid_late_rent_fee_archive.DataSource = v.getPaidLateRentFeeList(Convert.ToInt32(Session["schema_id"]), home_id, 0, from, to);
                    gv_paid_late_rent_fee_archive.DataBind();



                }

                 // if ther is no unit
                else
                {

                    //   txt_message.InnerHtml = "There is no unit in this property -- add unit";

                }

                //hidden fields



            }
            // if ther is no home

            else
            {
                //  txt_message.InnerHtml = "There is no property -- Add a property";

                //  txt_link.InnerHtml = home_count + " <a href='home_add.aspx'>Add Home</a>&nbsp;&nbsp;<a href='owner_add.aspx'>Add Owner</a>&nbsp;&nbsp;<a href='janitor_add.aspx'>Add Janitor</a>&nbsp;&nbsp;";
            }

        }

    }

    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {
            ddl_home_id.Visible = true;

            // txt_pending.InnerHtml = "";

            //To view the address of the property

            tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
            rhome_view.DataBind();

            tiger.Unit u = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
            //change list of unit id

            if (unit_id > 0)
            {
                ddl_unit_id.Visible = true;
                //first dispose of old ddl_unit
                ddl_unit_id.Dispose();
                ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
                ddl_unit_id.DataBind();
                ddl_unit_id.Items.Insert(0, "-- All Units --");
                ddl_unit_id.SelectedIndex = 0;

                DateTime to = new DateTime();
                DateTime from = new DateTime();
                to = DateTime.Now; // the date in the to drop downlist

                tiger.Date d = new tiger.Date();
                to = Convert.ToDateTime(d.DateCulture(to.Month.ToString(), to.Day.ToString(), to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
                from = Convert.ToDateTime(d.DateCulture("1", "1", to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


                // construction of the gridview for the payment archive
                tiger.Rent v = new tiger.Rent(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                gv_paid_late_rent_fee_archive.DataSource = v.getPaidLateRentFeeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), 0, from, to);
                gv_paid_late_rent_fee_archive.DataBind();

                ddl_from_m.SelectedValue = from.Month.ToString();
                ddl_from_d.SelectedValue = from.Day.ToString();
                ddl_from_y.SelectedValue = from.Year.ToString();

                ddl_to_m.SelectedValue = to.Month.ToString();
                ddl_to_d.SelectedValue = to.Day.ToString();
                ddl_to_y.SelectedValue = to.Year.ToString();

            }
            else
            {
                // txt_message.InnerHtml = "There is no unit in this house -- add unit(s)";

                //  txt_link.InnerHtml = "<a href='unit_add.aspx?home_id='>Add Unit</a>";


            }
            //   }// fin if not postback
        


    }


    protected void ddl_unit_id_SelectedIndexChanged(object sender, EventArgs e)
    {

        int unit_id;

        if (ddl_unit_id.SelectedIndex == 0)
            unit_id = 0;
        else
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        DateTime to = new DateTime();
        DateTime from = new DateTime();


        tiger.Date d = new tiger.Date();
        to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        // construction of the gridview for the payment archive
        tiger.Rent v = new tiger.Rent(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_paid_late_rent_fee_archive.DataSource = v.getPaidLateRentFeeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), unit_id, from, to);
        gv_paid_late_rent_fee_archive.DataBind();


       


    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        int unit_id;

        if (ddl_unit_id.SelectedIndex == 0)
            unit_id = 0;
        else
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        //hd_unit_id.Value = Convert.ToString(ddl_unit_id.SelectedValue);
        // label_added_unit.Text = ddl_unit_id.SelectedItem.Text;
        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


        DateTime to = new DateTime();
        DateTime from = new DateTime();
        to = DateTime.Now; // the date in the to drop downlist

        tiger.Date d = new tiger.Date();
        to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        // construction of the gridview for the payment archive
        tiger.Rent v = new tiger.Rent(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_paid_late_rent_fee_archive.DataSource = v.getPaidLateRentFeeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), unit_id, from, to);
        gv_paid_late_rent_fee_archive.DataBind();



    }

    

    
}
