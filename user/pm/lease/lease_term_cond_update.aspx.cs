using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : sept 24 , 2007
/// modidy    : may 27 , 2009
/// </summary>

public partial class home_lease_term_cond_update : BasePage
{

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        // "_2" for commercial units

           if (!Page.IsPostBack)
            {

                r_pendingtermsandconditionslist.Visible = true;
                txt_pending.InnerHtml = "";
                btn_continue.Enabled = true;

                r_pendingtermsandconditionslist_2.Visible = true;
                txt_pending_2.InnerHtml = "";
                btn_continue_2.Enabled = true;
                
                txt_message.InnerHtml = "";
                txt_message_2.InnerHtml = "";


                tiger.Name n = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ddl_guarantor_name_id.DataSource = n.getNameList(Convert.ToInt32(Session["schema_id"]), 0);
                ddl_guarantor_name_id.DataBind();
                ddl_guarantor_name_id.Items.Insert(0, "select a person");

                tiger.Country ic = new tiger.Country(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ddl_guarantor_country_id.DataSource = ic.getCountryList();
                ddl_guarantor_country_id.DataBind();

               //----- commercial units

                ddl_guarantor_name_id_2.DataSource = n.getNameList(Convert.ToInt32(Session["schema_id"]), 0);
                ddl_guarantor_name_id_2.DataBind();
                ddl_guarantor_name_id_2.Items.Insert(0, "select a person");

                ddl_guarantor_country_id_2.DataSource = ic.getCountryList();
                ddl_guarantor_country_id_2.DataBind();

                //int home_id = 1;// Convert.ToInt32(Request.QueryString["home_id"]);

                tiger.PM h = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int home_count = h.getPMHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                string link_to_unit = "";
                if (home_count > 0)
                {
                    int home_id = h.getPMHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                    link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                    //Session["schema_id"]));


                    ddl_home_id.Visible = true;
                    //hidden fields
                    hd_home_id.Value = Convert.ToString(home_id);

                    ddl_home_id.DataSource = h.getPMHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                    ddl_home_id.SelectedValue = Convert.ToString(home_id);
                    ddl_home_id.DataBind();

                    //*********************************************
                    // DropDownList pour les Unit
                    //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                    tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

                    if (unit_count > 0)
                    {

                        int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), home_id);
                        //link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                        //Session["schema_id"]));

                        // unit_id hiddenfield
                        hd_unit_id.Value = Convert.ToString(unit_id);

                        ddl_unit_id.DataSource = u.getUnitResList(Convert.ToInt32(Session["schema_id"]), home_id);
                        ddl_unit_id.SelectedValue = Convert.ToString(unit_id);
                        ddl_unit_id.DataBind();


                        //------------ (here we check if there is any pending new terms and conditions
                        //get current tenant id
                        int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), unit_id);
                        //if there is a current tenant in the unit then get name(s)
                        if (temp_tenant_id > 0)
                        {
                            panel_current_tenant.Visible = true;
                            txt_current_tenant_name.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);

                            tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                            hd_current_tu_id.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), unit_id));


                            panel_term_cond_update.Visible = true;


                            // here we check the amount of pending terms and conditions for this tenant unit
                            int count = 0;

                            tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                            count = p.getCountPendingTermsAndConditions(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id.Value));


                            if (count > 0)
                            {
                                btn_continue.Enabled = false;
                                txt_pending.InnerHtml = "<strong><span style='color: #ff3300'>You already have a pending terms and conditions ,<br /> please remove pending before creating another terms and conditions, ( link goes here)</span></strong> ";

                            }

                            //----- Now we show there is any pending accommodation in this tenant unit
                            tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                            r_pendingtermsandconditionslist.DataSource = v.getPendingTermsAndConditionsList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id.Value));

                            r_pendingtermsandconditionslist.DataBind();

                            ///------------------- 
  
                            // ( begin ) get the the terms and conditions from the data base

                            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                            SqlCommand cmd = new SqlCommand("prTermsAndConditionsView", conn);
                            cmd.CommandType = CommandType.StoredProcedure;


                            DateTime the_date = new DateTime();
                            the_date = DateTime.Now; // the date in the to drop downlist

                            tiger.Date d = new tiger.Date();
                            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
                            // and convert it in Datetime
                            the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


                            //Add the params
                            // cmd.Parameters.Add("@return_guarantor_name_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id.Value);
                            cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;


                            try
                            {
                                conn.Open();

                                SqlDataReader dr = null;
                                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                                int guarantor_name_id;

                                while (dr.Read() == true)
                                {

                                   // lbl_current_tt_date_begin.Text = dr["tt_date_begin"].ToString();

                                    DateTime la_date = new DateTime();
                                    la_date = Convert.ToDateTime(dr["tt_date_begin"]);

                                    lbl_current_tt_date_begin.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();

                                    lbl_current_tt_date_begin1.Text = lbl_current_tt_date_begin.Text;

                                    if (dr["tt_guarantor"].ToString() == "1")
                                        panel_guarantor.Visible = true;
                                    else
                                        panel_guarantor.Visible = false;


                                    if (Convert.ToInt32(dr["tt_guarantor_name_id"]) > 0)
                                    {
                                        ddl_guarantor_name_id.SelectedValue = Convert.ToString(dr["tt_guarantor_name_id"]);
                                        guarantor_name_fname.Enabled = false;
                                        guarantor_name_lname.Enabled = false;
                                        guarantor_name_addr.Enabled = false;
                                        guarantor_name_addr_city.Enabled = false;
                                        guarantor_name_addr_pc.Enabled = false;
                                        guarantor_name_addr_state.Enabled = false;
                                        ddl_guarantor_country_id.Enabled = false;
                                        guarantor_name_tel.Enabled = false;
                                        guarantor_name_tel_work.Enabled = false;
                                        guarantor_name_tel_work_ext.Enabled = false;
                                        guarantor_name_cell.Enabled = false;
                                        guarantor_name_fax.Enabled = false;
                                        guarantor_name_email.Enabled = false;
                                        guarantor_name_com.Enabled = false;
                                        txt.Visible = true;
                                    }
                                    else
                                    {
                                        guarantor_name_fname.Enabled = true;
                                        guarantor_name_lname.Enabled = true;
                                        guarantor_name_addr.Enabled = true;
                                        guarantor_name_addr_city.Enabled = true;
                                        guarantor_name_addr_pc.Enabled = true;
                                        guarantor_name_addr_state.Enabled = true;
                                        ddl_guarantor_country_id.Enabled = true;
                                        guarantor_name_tel.Enabled = true;
                                        guarantor_name_tel_work.Enabled = true;
                                        guarantor_name_tel_work_ext.Enabled = true;
                                        guarantor_name_cell.Enabled = true;
                                        guarantor_name_fax.Enabled = true;
                                        guarantor_name_email.Enabled = true;
                                        guarantor_name_com.Enabled = true;
                                        txt.Visible = false;

                                    }




                                    decimal nsf, late_fee, security_deposit_amount;

                                    if (dr["tt_nsf"].ToString() != "")
                                        nsf = Convert.ToDecimal(dr["tt_nsf"]);
                                    else
                                        nsf = 0;

                                    if (dr["tt_late_fee"].ToString() != "")
                                        late_fee = Convert.ToDecimal(dr["tt_late_fee"]);
                                    else
                                        late_fee = 0;

                                    if (dr["tt_security_deposit_amount"].ToString() != "")
                                        security_deposit_amount = Convert.ToDecimal(dr["tt_security_deposit_amount"]);
                                    else
                                        security_deposit_amount = 0;


                                    tt_nsf.Text = String.Format("{0:0.00}", nsf);
                                    tt_late_fee.Text = String.Format("{0:0.00}", late_fee);


                                    tt_security_deposit.SelectedValue = dr["tt_security_deposit"].ToString();

                                    tt_security_deposit_amount.Text = String.Format("{0:0.00}", security_deposit_amount);


                                    ddl_tt_form_of_payment.SelectedValue = dr["tt_form_of_payment"].ToString();
                                    tt_guarantor.SelectedValue = dr["tt_guarantor"].ToString();



                                    tt_pets.SelectedValue = dr["tt_pets"].ToString();
                                    tt_maintenance.SelectedValue = dr["tt_maintenance"].ToString();
                                    tt_specify_maintenance.Text = dr["tt_specify_maintenance"].ToString();
                                    tt_improvement.SelectedValue = dr["tt_improvement"].ToString();
                                    tt_specify_improvement.Text = dr["tt_specify_improvement"].ToString();
                                    tt_notice_to_enter.SelectedValue = dr["tt_notice_to_enter"].ToString();

                                    if (dr["tt_specify_number_of_hours"].ToString() == "0")
                                        tt_specify_number_of_hours.Text = "";
                                    else
                                        tt_specify_number_of_hours.Text = dr["tt_specify_number_of_hours"].ToString();

                                    ddl_tt_tenant_content_ins.SelectedValue = dr["tt_tenant_content_ins"].ToString();
                                    ddl_tt_landlord_content_ins.SelectedValue = dr["tt_landlord_content_ins"].ToString();
                                    ddl_tt_injury_ins.SelectedValue = dr["tt_injury_ins"].ToString();
                                    ddl_tt_premises_ins.SelectedValue = dr["tt_premises_ins"].ToString();

                                    tt_additional_terms.Text = dr["tt_additional_terms"].ToString();
                                }
                            }

                            finally
                            {

                                conn.Close();
                            }


                            // ( end ) get the the terms and conditions from the data base
                        }

                        else
                        {
                            txt_message.InnerHtml = "This unit is not rented";
                            panel_term_cond_update.Visible = false;
                            hd_current_tu_id.Value = "0";
                            //hd_unit_id.Value = Convert.ToString(unit_id);
                        }

                    }

                    // if we dont fin an unit
                    else
                    {
                        txt_message.InnerHtml = "There is no unit on this property";
                        r_pendingtermsandconditionslist.Visible = false;
                        panel_term_cond_update.Visible = false;
                        hd_current_tu_id.Value = "0";
                        hd_unit_id.Value = "0";
                    }
                    /************************Now get home information*****************************/

                }
                // if ther is no home
                else
                {
                    txt_message.InnerHtml = "There is no property";
                    r_pendingtermsandconditionslist.Visible = false;
                    panel_term_cond_update.Visible = false;
                    hd_home_id.Value = "0";
                    hd_current_tu_id.Value = "0";
                    hd_unit_id.Value = "0";
                    ddl_home_id.Visible = false;
                    txt_link.InnerHtml = home_count + " <a href='home_add.aspx'>Add Home</a>&nbsp;&nbsp;<a href='owner_add.aspx'>Add Owner</a>&nbsp;&nbsp;<a href='janitor_add.aspx'>Add Janitor</a>&nbsp;&nbsp;";
                }

                int homecom_count = h.getPMHomeComCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                if (homecom_count > 0)
                {
                    int home_id = h.getPMHomeComFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                    link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                    //Session["schema_id"]));


                    ddl_home_id_2.Visible = true;
                    //hidden fields
                    hd_home_id_2.Value = Convert.ToString(home_id);

                    ddl_home_id_2.DataSource = h.getPMHomeComList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                    ddl_home_id_2.SelectedValue = Convert.ToString(home_id);
                    ddl_home_id_2.DataBind();

                    

                    //*********************************************
                    // DropDownList pour les Unit
                    //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                    tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    int unit_count = u.getUnitComCount(Convert.ToInt32(Session["schema_id"]), home_id);

                    if (unit_count > 0)
                    {

                        int unit_id = u.getUnitComFirstId(Convert.ToInt32(Session["schema_id"]), home_id);
                        //link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                        //Session["schema_id"]));

                        // unit_id hiddenfield
                        hd_unit_id_2.Value = Convert.ToString(unit_id);

                        ddl_unit_id_2.DataSource = u.getUnitComList(Convert.ToInt32(Session["schema_id"]), home_id);
                        ddl_unit_id_2.SelectedValue = Convert.ToString(unit_id);
                        ddl_unit_id_2.DataBind();


                        //------------ (here we check if there is any pending new terms and conditions


                        //get current tenant id
                        int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), unit_id);
                        //if there is a current tenant in the unit then get name(s)
                        if (temp_tenant_id > 0)
                        {
                            panel_current_tenant_2.Visible = true;
                            txt_current_tenant_name_2.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);

                            tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                            hd_current_tu_id_2.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), unit_id));


                            panel_term_cond_update_2.Visible = true;


                            // here we check the amount of pending terms and conditions for this tenant unit
                            int count = 0;

                            tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                            count = p.getCountPendingTermsAndConditions(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id_2.Value));


                            if (count > 0)
                            {
                                btn_continue_2.Enabled = false;
                                txt_pending_2.InnerHtml = "<strong><span style='color: #ff3300'>You already have a pending terms and conditions ,<br /> please remove pending before creating another terms and conditions, ( link goes here)</span></strong> ";

                            }

                            //----- Now we show there is any pending accommodation in this tenant unit
                            tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                            r_pendingtermsandconditionslist_2.DataSource = v.getPendingTermsAndConditionsList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id_2.Value));
                            r_pendingtermsandconditionslist_2.DataBind();

                            ///------------------- 

                            // ( begin ) get the the terms and conditions from the data base

                            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                            SqlCommand cmd = new SqlCommand("prTermsAndConditionsView", conn);
                            cmd.CommandType = CommandType.StoredProcedure;


                            DateTime the_date = new DateTime();
                            the_date = DateTime.Now; // the date in the to drop downlist

                            tiger.Date d = new tiger.Date();
                            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
                            // and convert it in Datetime
                            the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


                            //Add the params
                            // cmd.Parameters.Add("@return_guarantor_name_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id_2.Value);
                            cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;


                            try
                            {
                                conn.Open();

                                SqlDataReader dr = null;
                                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                                int guarantor_name_id;

                                while (dr.Read() == true)
                                {

                                    // lbl_current_tt_date_begin.Text = dr["tt_date_begin"].ToString();

                                    DateTime la_date = new DateTime();
                                    la_date = Convert.ToDateTime(dr["tt_date_begin"]);

                                    lbl_current_tt_date_begin_2.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();

                                    lbl_current_tt_date_begin_3.Text = lbl_current_tt_date_begin_2.Text;

                                    if (dr["tt_guarantor"].ToString() == "1")
                                        panel_guarantor_2.Visible = true;
                                    else
                                        panel_guarantor_2.Visible = false;


                                    if (Convert.ToInt32(dr["tt_guarantor_name_id"]) > 0)
                                    {
                                        ddl_guarantor_name_id_2.SelectedValue = Convert.ToString(dr["tt_guarantor_name_id"]);
                                        guarantor_name_fname_2.Enabled = false;
                                        guarantor_name_lname_2.Enabled = false;
                                        guarantor_name_addr_2.Enabled = false;
                                        guarantor_name_addr_city_2.Enabled = false;
                                        guarantor_name_addr_pc_2.Enabled = false;
                                        guarantor_name_addr_state_2.Enabled = false;
                                        ddl_guarantor_country_id_2.Enabled = false;
                                        guarantor_name_tel_2.Enabled = false;
                                        guarantor_name_tel_work_2.Enabled = false;
                                        guarantor_name_tel_work_ext_2.Enabled = false;
                                        guarantor_name_cell_2.Enabled = false;
                                        guarantor_name_fax_2.Enabled = false;
                                        guarantor_name_email_2.Enabled = false;
                                        guarantor_name_com_2.Enabled = false;
                                        txt_2.Visible = true;
                                    }
                                    else
                                    {
                                        guarantor_name_fname_2.Enabled = true;
                                        guarantor_name_lname_2.Enabled = true;
                                        guarantor_name_addr_2.Enabled = true;
                                        guarantor_name_addr_city_2.Enabled = true;
                                        guarantor_name_addr_pc_2.Enabled = true;
                                        guarantor_name_addr_state_2.Enabled = true;
                                        ddl_guarantor_country_id_2.Enabled = true;
                                        guarantor_name_tel_2.Enabled = true;
                                        guarantor_name_tel_work_2.Enabled = true;
                                        guarantor_name_tel_work_ext_2.Enabled = true;
                                        guarantor_name_cell_2.Enabled = true;
                                        guarantor_name_fax_2.Enabled = true;
                                        guarantor_name_email_2.Enabled = true;
                                        guarantor_name_com_2.Enabled = true;
                                        txt_2.Visible = false;

                                    }

                                    decimal nsf, late_fee, security_deposit_amount;

                                    if (dr["tt_nsf"].ToString() != "")
                                        nsf = Convert.ToDecimal(dr["tt_nsf"]);
                                    else
                                        nsf = 0;

                                    if (dr["tt_late_fee"].ToString() != "")
                                        late_fee = Convert.ToDecimal(dr["tt_late_fee"]);
                                    else
                                        late_fee = 0;

                                    if (dr["tt_security_deposit_amount"].ToString() != "")
                                        security_deposit_amount = Convert.ToDecimal(dr["tt_security_deposit_amount"]);
                                    else
                                        security_deposit_amount = 0;


                                    tt_nsf_2.Text = String.Format("{0:0.00}", nsf);
                                    tt_late_fee_2.Text = String.Format("{0:0.00}", late_fee);


                                    tt_security_deposit_2.SelectedValue = dr["tt_security_deposit"].ToString();

                                    tt_security_deposit_amount_2.Text = String.Format("{0:0.00}", security_deposit_amount);


                                    ddl_tt_form_of_payment_2.SelectedValue = dr["tt_form_of_payment"].ToString();
                                    tt_guarantor_2.SelectedValue = dr["tt_guarantor"].ToString();

                                    tt_pets_2.SelectedValue = dr["tt_pets"].ToString();
                                    tt_maintenance_2.SelectedValue = dr["tt_maintenance"].ToString();
                                    tt_specify_maintenance_2.Text = dr["tt_specify_maintenance"].ToString();
                                    tt_improvement_2.SelectedValue = dr["tt_improvement"].ToString();
                                    tt_specify_improvement_2.Text = dr["tt_specify_improvement"].ToString();
                                    tt_notice_to_enter_2.SelectedValue = dr["tt_notice_to_enter"].ToString();

                                    if (dr["tt_specify_number_of_hours"].ToString() == "0")
                                        tt_specify_number_of_hours_2.Text = "";
                                    else
                                        tt_specify_number_of_hours_2.Text = dr["tt_specify_number_of_hours"].ToString();

                                    ddl_tt_tenant_content_ins_2.SelectedValue = dr["tt_tenant_content_ins"].ToString();
                                    ddl_tt_landlord_content_ins_2.SelectedValue = dr["tt_landlord_content_ins"].ToString();
                                    ddl_tt_injury_ins_2.SelectedValue = dr["tt_injury_ins"].ToString();
                                    ddl_tt_premises_ins_2.SelectedValue = dr["tt_premises_ins"].ToString();

                                    tt_additional_terms_2.Text = dr["tt_additional_terms"].ToString();

                                    if (dr["unit_type"].ToString() == "C" && Convert.ToInt32(dr["company_id"]) > 0)
                                    {
                                        lbl_company.Text = dr["company_name"].ToString();
                                    }
                                    else
                                    {
                                        tr_company.Visible = false;
                                    }
                                }
                            }

                            finally
                            {

                                conn.Close();
                            }
                            // ( end ) get the the terms and conditions from the data base
                        }

                        else
                        {
                            txt_message_2.InnerHtml = "This unit is not rented";
                            panel_term_cond_update_2.Visible = false;
                            hd_current_tu_id_2.Value = "0";
                            //hd_unit_id.Value = Convert.ToString(unit_id);
                        }

                    }

                    // if we dont fin an unit
                    else
                    {
                        txt_message_2.InnerHtml = "There is no unit on this property";
                        r_pendingtermsandconditionslist_2.Visible = false;
                        panel_term_cond_update_2.Visible = false;
                        hd_current_tu_id_2.Value = "0";
                        hd_unit_id_2.Value = "0";
                    }
                    /************************Now get home information*****************************/

                }
                else
                {
                    //tab2.Visible = false;
                    //tab2.HeaderText = "";

                    txt_message_2.InnerHtml = "There is no property";
                    r_pendingtermsandconditionslist_2.Visible = false;
                    panel_term_cond_update_2.Visible = false;
                    hd_home_id_2.Value = "0";
                    hd_current_tu_id_2.Value = "0";
                    hd_unit_id_2.Value = "0";
                    ddl_home_id_2.Visible = false;
                    btn_continue_2.Visible = false;
                    txt_link_2.InnerHtml = homecom_count + " <a href='home_add.aspx'>Add Home</a>&nbsp;&nbsp;<a href='owner_add.aspx'>Add Owner</a>&nbsp;&nbsp;<a href='janitor_add.aspx'>Add Janitor</a>&nbsp;&nbsp;";
                }
            }    
    }

    //-------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {

           r_pendingtermsandconditionslist.Visible = true;
            txt_pending.InnerHtml = "";
            btn_continue.Enabled = true;

            txt_message.InnerHtml = "";

            ddl_home_id.Visible = true;

            hd_home_id.Value = Convert.ToString(ddl_home_id.SelectedValue);
            tiger.Unit u = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
            //change list of unit id

            if (unit_id > 0)
            {
                ddl_unit_id.Visible = true;
                //first dispose of old ddl_unit
                ddl_unit_id.Dispose();
                ddl_unit_id.DataSource = u.getUnitResList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
                ddl_unit_id.DataBind();


                hd_unit_id.Value = Convert.ToString(unit_id);

                //----- Now we check if there is any pending terms and conditions
                // tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                // dgpendinglist.DataSource = v.getPendingLeasesList(Convert.ToInt32(Session["schema_id"]), unit_id);

                // dgpendinglist.DataBind();

                ///------------------- 

                //get current tenant id
                int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), unit_id);
                //if there is a current tenant in the unit then get name(s)
                if (temp_tenant_id > 0)
                {
                    panel_current_tenant.Visible = true;
                    txt_current_tenant_name.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);

                    tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                    hd_current_tu_id.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), unit_id));

                    panel_term_cond_update.Visible = true;


                    // here we check the amount of pending terms and conditions for this tenant unit
                    int count = 0;

                    tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                    count = p.getCountPendingTermsAndConditions(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id.Value));


                    if (count > 0)
                    {
                        btn_continue.Enabled = false;
                        txt_pending.InnerHtml = "<strong><span style='color: #ff3300'>You already have a pending terms and conditions ,<br /> please remove pending before creating another terms and conditions, ( link goes here)</span></strong> ";

                    }

                    //----- Now we show there is any pending accommodation in this tenant unit
                    tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                    r_pendingtermsandconditionslist.DataSource = v.getPendingTermsAndConditionsList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id.Value));

                    r_pendingtermsandconditionslist.DataBind();

                    ///------------------- 
     
                    // ( begin ) get the the terms and conditions from the data base

                    SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    SqlCommand cmd = new SqlCommand("prTermsAndConditionsView", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    DateTime the_date = new DateTime();
                    the_date = DateTime.Now; // the date in the to drop downlist

                    tiger.Date d = new tiger.Date();
                    // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
                    // and convert it in Datetime
                    the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


                    //Add the params
                    // cmd.Parameters.Add("@return_guarantor_name_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                    cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id.Value);
                    cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;

                    try
                    {
                        conn.Open();

                        SqlDataReader dr = null;
                        dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                        int guarantor_name_id;

                        while (dr.Read() == true)
                        {

                            DateTime la_date = new DateTime();
                            la_date = Convert.ToDateTime(dr["tt_date_begin"]);

                            lbl_current_tt_date_begin.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();

                            lbl_current_tt_date_begin1.Text = lbl_current_tt_date_begin.Text;



                            if (dr["tt_guarantor"].ToString() == "1")
                                panel_guarantor.Visible = true;
                            else
                                panel_guarantor.Visible = false;


                            if (Convert.ToInt32(dr["tt_guarantor_name_id"]) > 0)
                            {
                                ddl_guarantor_name_id.SelectedValue = Convert.ToString(dr["tt_guarantor_name_id"]);
                                guarantor_name_fname.Enabled = false;
                                guarantor_name_lname.Enabled = false;
                                guarantor_name_addr.Enabled = false;
                                guarantor_name_addr_city.Enabled = false;
                                guarantor_name_addr_pc.Enabled = false;
                                guarantor_name_addr_state.Enabled = false;
                                ddl_guarantor_country_id.Enabled = false;
                                guarantor_name_tel.Enabled = false;
                                guarantor_name_tel_work.Enabled = false;
                                guarantor_name_tel_work_ext.Enabled = false;
                                guarantor_name_cell.Enabled = false;
                                guarantor_name_fax.Enabled = false;
                                guarantor_name_email.Enabled = false;
                                guarantor_name_com.Enabled = false;
                                txt.Visible = true;
                            }
                            else
                            {
                                guarantor_name_fname.Enabled = true;
                                guarantor_name_lname.Enabled = true;
                                guarantor_name_addr.Enabled = true;
                                guarantor_name_addr_city.Enabled = true;
                                guarantor_name_addr_pc.Enabled = true;
                                guarantor_name_addr_state.Enabled = true;
                                ddl_guarantor_country_id.Enabled = true;
                                guarantor_name_tel.Enabled = true;
                                guarantor_name_tel_work.Enabled = true;
                                guarantor_name_tel_work_ext.Enabled = true;
                                guarantor_name_cell.Enabled = true;
                                guarantor_name_fax.Enabled = true;
                                guarantor_name_email.Enabled = true;
                                guarantor_name_com.Enabled = true;
                                txt.Visible = false;

                            }

                            decimal nsf, late_fee, security_deposit_amount;

                            if (dr["tt_nsf"].ToString() != "")
                                nsf = Convert.ToDecimal(dr["tt_nsf"]);
                            else
                                nsf = 0;

                            if (dr["tt_late_fee"].ToString() != "")
                                late_fee = Convert.ToDecimal(dr["tt_late_fee"]);
                            else
                                late_fee = 0;

                            if (dr["tt_security_deposit_amount"].ToString() != "")
                                security_deposit_amount = Convert.ToDecimal(dr["tt_security_deposit_amount"]);
                            else
                                security_deposit_amount = 0;


                            tt_nsf.Text = String.Format("{0:0.00}", nsf);
                            tt_late_fee.Text = String.Format("{0:0.00}", late_fee);


                            tt_security_deposit.SelectedValue = dr["tt_security_deposit"].ToString();

                            tt_security_deposit_amount.Text = String.Format("{0:0.00}", security_deposit_amount);


                            tt_guarantor.SelectedValue = dr["tt_guarantor"].ToString();
                            ddl_tt_form_of_payment.SelectedValue = dr["tt_form_of_payment"].ToString();
                            


                            tt_pets.SelectedValue = dr["tt_pets"].ToString();
                            tt_maintenance.SelectedValue = dr["tt_maintenance"].ToString();
                            tt_specify_maintenance.Text = dr["tt_specify_maintenance"].ToString();
                            tt_improvement.SelectedValue = dr["tt_improvement"].ToString();
                            tt_specify_improvement.Text = dr["tt_specify_improvement"].ToString();
                            tt_notice_to_enter.SelectedValue = dr["tt_notice_to_enter"].ToString();

                            if (dr["tt_specify_number_of_hours"].ToString() == "0")
                                tt_specify_number_of_hours.Text = "";
                            else
                                tt_specify_number_of_hours.Text = dr["tt_specify_number_of_hours"].ToString();

                            ddl_tt_tenant_content_ins.SelectedValue = dr["tt_tenant_content_ins"].ToString();
                            ddl_tt_landlord_content_ins.SelectedValue = dr["tt_landlord_content_ins"].ToString();
                            ddl_tt_injury_ins.SelectedValue = dr["tt_injury_ins"].ToString();
                            ddl_tt_premises_ins.SelectedValue = dr["tt_premises_ins"].ToString();

                            tt_additional_terms.Text = dr["tt_additional_terms"].ToString();

                        }
                    }

                    finally
                    {
                        conn.Close();
                    }
                    // ( end ) get the the terms and conditions from the data base

                }
                else
                {
                    txt_message.InnerHtml = "This unit is not rented";
                    r_pendingtermsandconditionslist.Visible = false;
                    panel_term_cond_update.Visible = false;
                    panel_current_tenant.Visible = false;
                    hd_current_tu_id.Value = "0";
                }
                //hidden fields
                //hd_home_id.Value = ddl_home_id.Text;
                //  hd_unit_id.Value = Convert.ToString(ddl_unit_id.SelectedValue);
            }
            else
            {

                txt_message.InnerHtml = "There is no unit on this property";
                r_pendingtermsandconditionslist.Visible = false;
                panel_term_cond_update.Visible = false;
                ddl_unit_id.Visible = false;
                //  txt_link.InnerHtml = "<a href='unit_add.aspx?home_id='>Add Unit</a>";
                hd_current_tu_id.Value = "0";
                hd_unit_id.Value = "0";
            }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_id_2_SelectedIndexChanged(object sender, EventArgs e)
    {

        r_pendingtermsandconditionslist_2.Visible = true;
        txt_pending_2.InnerHtml = "";
        btn_continue_2.Enabled = true;

        txt_message_2.InnerHtml = "";

        ddl_home_id_2.Visible = true;

        hd_home_id_2.Value = Convert.ToString(ddl_home_id_2.SelectedValue);
        tiger.Unit u = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        int unit_id = u.getUnitComFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id_2.SelectedValue));
        //change list of unit id

        if (unit_id > 0)
        {
            ddl_unit_id_2.Visible = true;
            //first dispose of old ddl_unit
            ddl_unit_id_2.Dispose();
            ddl_unit_id_2.DataSource = u.getUnitComList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id_2.SelectedValue));
            ddl_unit_id_2.DataBind();


            hd_unit_id_2.Value = Convert.ToString(unit_id);


            //get current tenant id
            int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), unit_id);

            //if there is a current tenant in the unit then get name(s)

            if (temp_tenant_id > 0)
            {
                panel_current_tenant_2.Visible = true;
                txt_current_tenant_name_2.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);

                tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                hd_current_tu_id_2.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), unit_id));

                panel_term_cond_update_2.Visible = true;


                // here we check the amount of pending terms and conditions for this tenant unit
                int count = 0;

                tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                count = p.getCountPendingTermsAndConditions(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id_2.Value));


                if (count > 0)
                {
                    btn_continue_2.Enabled = false;
                    txt_pending_2.InnerHtml = "<strong><span style='color: #ff3300'>You already have a pending terms and conditions ,<br /> please remove pending before creating another terms and conditions, ( link goes here)</span></strong> ";

                }

                //----- Now we show there is any pending accommodation in this tenant unit
                tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                r_pendingtermsandconditionslist_2.DataSource = v.getPendingTermsAndConditionsList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id_2.Value));

                r_pendingtermsandconditionslist_2.DataBind();

                ///------------------- 

                // ( begin ) get the the terms and conditions from the data base

                SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                SqlCommand cmd = new SqlCommand("prTermsAndConditionsView", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                DateTime the_date = new DateTime();
                the_date = DateTime.Now; // the date in the to drop downlist

                tiger.Date d = new tiger.Date();
                // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
                // and convert it in Datetime
                the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


                //Add the params
                // cmd.Parameters.Add("@return_guarantor_name_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id_2.Value);
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;

                try
                {
                    conn.Open();

                    SqlDataReader dr = null;
                    dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                    int guarantor_name_id;

                    while (dr.Read() == true)
                    {

                        DateTime la_date = new DateTime();
                        la_date = Convert.ToDateTime(dr["tt_date_begin"]);

                        lbl_current_tt_date_begin_2.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();

                        lbl_current_tt_date_begin_3.Text = lbl_current_tt_date_begin_2.Text;



                        if (dr["tt_guarantor"].ToString() == "1")
                            panel_guarantor_2.Visible = true;
                        else
                            panel_guarantor_2.Visible = false;


                        if (Convert.ToInt32(dr["tt_guarantor_name_id"]) > 0)
                        {
                            ddl_guarantor_name_id_2.SelectedValue = Convert.ToString(dr["tt_guarantor_name_id"]);
                            guarantor_name_fname_2.Enabled = false;
                            guarantor_name_lname_2.Enabled = false;
                            guarantor_name_addr_2.Enabled = false;
                            guarantor_name_addr_city_2.Enabled = false;
                            guarantor_name_addr_pc_2.Enabled = false;
                            guarantor_name_addr_state_2.Enabled = false;
                            ddl_guarantor_country_id_2.Enabled = false;
                            guarantor_name_tel_2.Enabled = false;
                            guarantor_name_tel_work_2.Enabled = false;
                            guarantor_name_tel_work_ext_2.Enabled = false;
                            guarantor_name_cell_2.Enabled = false;
                            guarantor_name_fax_2.Enabled = false;
                            guarantor_name_email_2.Enabled = false;
                            guarantor_name_com_2.Enabled = false;
                            txt_2.Visible = true;
                        }
                        else
                        {
                            guarantor_name_fname_2.Enabled = true;
                            guarantor_name_lname_2.Enabled = true;
                            guarantor_name_addr_2.Enabled = true;
                            guarantor_name_addr_city_2.Enabled = true;
                            guarantor_name_addr_pc_2.Enabled = true;
                            guarantor_name_addr_state_2.Enabled = true;
                            ddl_guarantor_country_id_2.Enabled = true;
                            guarantor_name_tel_2.Enabled = true;
                            guarantor_name_tel_work_2.Enabled = true;
                            guarantor_name_tel_work_ext_2.Enabled = true;
                            guarantor_name_cell_2.Enabled = true;
                            guarantor_name_fax_2.Enabled = true;
                            guarantor_name_email_2.Enabled = true;
                            guarantor_name_com_2.Enabled = true;
                            txt_2.Visible = false;

                        }

                        decimal nsf, late_fee, security_deposit_amount;

                        if (dr["tt_nsf"].ToString() != "")
                            nsf = Convert.ToDecimal(dr["tt_nsf"]);
                        else
                            nsf = 0;

                        if (dr["tt_late_fee"].ToString() != "")
                            late_fee = Convert.ToDecimal(dr["tt_late_fee"]);
                        else
                            late_fee = 0;

                        if (dr["tt_security_deposit_amount"].ToString() != "")
                            security_deposit_amount = Convert.ToDecimal(dr["tt_security_deposit_amount"]);
                        else
                            security_deposit_amount = 0;


                        tt_nsf_2.Text = String.Format("{0:0.00}", nsf);
                        tt_late_fee_2.Text = String.Format("{0:0.00}", late_fee);


                        tt_security_deposit_2.SelectedValue = dr["tt_security_deposit"].ToString();

                        tt_security_deposit_amount_2.Text = String.Format("{0:0.00}", security_deposit_amount);


                        tt_guarantor_2.SelectedValue = dr["tt_guarantor"].ToString();
                        ddl_tt_form_of_payment_2.SelectedValue = dr["tt_form_of_payment"].ToString();



                        tt_pets_2.SelectedValue = dr["tt_pets"].ToString();
                        tt_maintenance_2.SelectedValue = dr["tt_maintenance"].ToString();
                        tt_specify_maintenance_2.Text = dr["tt_specify_maintenance"].ToString();
                        tt_improvement_2.SelectedValue = dr["tt_improvement"].ToString();
                        tt_specify_improvement_2.Text = dr["tt_specify_improvement"].ToString();
                        tt_notice_to_enter_2.SelectedValue = dr["tt_notice_to_enter"].ToString();

                        if (dr["tt_specify_number_of_hours"].ToString() == "0")
                            tt_specify_number_of_hours_2.Text = "";
                        else
                            tt_specify_number_of_hours_2.Text = dr["tt_specify_number_of_hours"].ToString();

                        ddl_tt_tenant_content_ins_2.SelectedValue = dr["tt_tenant_content_ins"].ToString();
                        ddl_tt_landlord_content_ins_2.SelectedValue = dr["tt_landlord_content_ins"].ToString();
                        ddl_tt_injury_ins_2.SelectedValue = dr["tt_injury_ins"].ToString();
                        ddl_tt_premises_ins_2.SelectedValue = dr["tt_premises_ins"].ToString();

                        tt_additional_terms_2.Text = dr["tt_additional_terms"].ToString();


                        if (dr["unit_type"].ToString() == "C" && Convert.ToInt32(dr["company_id"]) > 0)
                        {
                            lbl_company.Text = dr["company_name"].ToString();
                        }
                        else
                        {
                            tr_company.Visible = false;
                        }

                    }
                }

                finally
                {
                    conn.Close();
                }
                // ( end ) get the the terms and conditions from the data base

            }
            else
            {
                txt_message_2.InnerHtml = "This unit is not rented";
                r_pendingtermsandconditionslist_2.Visible = false;
                panel_term_cond_update_2.Visible = false;
                panel_current_tenant_2.Visible = false;
                hd_current_tu_id_2.Value = "0";
            }
            //hidden fields
            //hd_home_id.Value = ddl_home_id_2.Text;
            //  hd_unit_id_2.Value = Convert.ToString(ddl_unit_id_2.SelectedValue);
        }
        else
        {

            txt_message_2.InnerHtml = "There is no unit on this property";
            r_pendingtermsandconditionslist_2.Visible = false;
            panel_term_cond_update_2.Visible = false;
            ddl_unit_id_2.Visible = false;
            //  txt_link.InnerHtml = "<a href='unit_add.aspx?home_id='>Add Unit</a>";
            hd_current_tu_id_2.Value = "0";
            hd_unit_id_2.Value = "0";

        }

        //TabContainer1.ActiveTabIndex = 1;
    }
    //-------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_unit_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        // label_added_unit.Text = ddl_unit_id.SelectedItem.Text;
        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        //get current tenant
        //get current tenant id
        //get current tenant id

        r_pendingtermsandconditionslist.Visible = true;
        txt_pending.InnerHtml = "";
        btn_continue.Enabled = true;

        txt_message.InnerHtml = "";

        hd_unit_id.Value = Convert.ToString(ddl_unit_id.SelectedValue);

        
        int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_unit_id.SelectedValue));
        //if there is a current tenant in the unit then get name(s)
        if (temp_tenant_id > 0)
        {
            panel_current_tenant.Visible = true;
            txt_current_tenant_name.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);


            //tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            hd_current_tu_id.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), Convert.ToInt32(ddl_unit_id.SelectedValue)));

            panel_term_cond_update.Visible = true;


            // here we check the amount of pending terms and conditions for this tenant unit
            int count = 0;

            tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            count = p.getCountPendingTermsAndConditions(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id.Value));


            if (count > 0)
            {
                btn_continue.Enabled = false;
                txt_pending.InnerHtml = "<strong><span style='color: #ff3300'>You already have a pending terms and conditions ,<br /> please remove pending before creating another terms and conditions, ( link goes here)</span></strong> ";

            }

            //----- Now we show there is any pending accommodation in this tenant unit
            tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            r_pendingtermsandconditionslist.DataSource = v.getPendingTermsAndConditionsList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id.Value));

            r_pendingtermsandconditionslist.DataBind();

            ///------------------- 
            // ( begin ) get the the terms and conditions from the data base

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prTermsAndConditionsView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            DateTime the_date = new DateTime();
            the_date = DateTime.Now; // the date in the to drop downlist

            tiger.Date d = new tiger.Date();
            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


            //Add the params
            // cmd.Parameters.Add("@return_guarantor_name_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id.Value);
            cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;

            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                int guarantor_name_id;

                while (dr.Read() == true)
                {

                    DateTime la_date = new DateTime();
                    la_date = Convert.ToDateTime(dr["tt_date_begin"]);

                    lbl_current_tt_date_begin.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();

                    lbl_current_tt_date_begin1.Text = lbl_current_tt_date_begin.Text;



                    if (dr["tt_guarantor"].ToString() == "1")
                        panel_guarantor.Visible = true;
                    else
                        panel_guarantor.Visible = false;


                    if (Convert.ToInt32(dr["tt_guarantor_name_id"]) > 0)
                    {
                        ddl_guarantor_name_id.SelectedValue = Convert.ToString(dr["tt_guarantor_name_id"]);
                        guarantor_name_fname.Enabled = false;
                        guarantor_name_lname.Enabled = false;
                        guarantor_name_addr.Enabled = false;
                        guarantor_name_addr_city.Enabled = false;
                        guarantor_name_addr_pc.Enabled = false;
                        guarantor_name_addr_state.Enabled = false;
                        ddl_guarantor_country_id.Enabled = false;
                        guarantor_name_tel.Enabled = false;
                        guarantor_name_tel_work.Enabled = false;
                        guarantor_name_tel_work_ext.Enabled = false;
                        guarantor_name_cell.Enabled = false;
                        guarantor_name_fax.Enabled = false;
                        guarantor_name_email.Enabled = false;
                        guarantor_name_com.Enabled = false;
                        txt.Visible = true;
                    }
                    else
                    {
                        guarantor_name_fname.Enabled = true;
                        guarantor_name_lname.Enabled = true;
                        guarantor_name_addr.Enabled = true;
                        guarantor_name_addr_city.Enabled = true;
                        guarantor_name_addr_pc.Enabled = true;
                        guarantor_name_addr_state.Enabled = true;
                        ddl_guarantor_country_id.Enabled = true;
                        guarantor_name_tel.Enabled = true;
                        guarantor_name_tel_work.Enabled = true;
                        guarantor_name_tel_work_ext.Enabled = true;
                        guarantor_name_cell.Enabled = true;
                        guarantor_name_fax.Enabled = true;
                        guarantor_name_email.Enabled = true;
                        guarantor_name_com.Enabled = true;
                        txt.Visible = false;

                    }


                    /*
                    tt_nsf.Text = String.Format("{0:0.00}", dr["tt_nsf"].ToString());
                    tt_late_fee.Text = String.Format("{0:0.00}", dr["tt_late_fee"].ToString())

                    tt_security_deposit.SelectedValue = dr["tt_security_deposit"].ToString();
                    */

                    decimal nsf, late_fee, security_deposit_amount;

                    if (dr["tt_nsf"].ToString() != "")
                        nsf = Convert.ToDecimal(dr["tt_nsf"]);
                    else
                        nsf = 0;

                    if (dr["tt_late_fee"].ToString() != "")
                        late_fee = Convert.ToDecimal(dr["tt_late_fee"]);
                    else
                        late_fee = 0;

                    if (dr["tt_security_deposit_amount"].ToString() != "")
                        security_deposit_amount = Convert.ToDecimal(dr["tt_security_deposit_amount"]);
                    else
                        security_deposit_amount = 0;


                    tt_nsf.Text = String.Format("{0:0.00}", nsf);
                    tt_late_fee.Text = String.Format("{0:0.00}", late_fee);


                    tt_security_deposit.SelectedValue = dr["tt_security_deposit"].ToString();

                    tt_security_deposit_amount.Text = String.Format("{0:0.00}", security_deposit_amount);
                    

                    tt_guarantor.SelectedValue = dr["tt_guarantor"].ToString();
                    ddl_tt_form_of_payment.SelectedValue = dr["tt_form_of_payment"].ToString();
                    
                    tt_pets.SelectedValue = dr["tt_pets"].ToString();
                    tt_maintenance.SelectedValue = dr["tt_maintenance"].ToString();
                    tt_specify_maintenance.Text = dr["tt_specify_maintenance"].ToString();
                    tt_improvement.SelectedValue = dr["tt_improvement"].ToString();
                    tt_specify_improvement.Text = dr["tt_specify_improvement"].ToString();
                    tt_notice_to_enter.SelectedValue = dr["tt_notice_to_enter"].ToString();

                    if (dr["tt_specify_number_of_hours"].ToString() == "0")
                        tt_specify_number_of_hours.Text = "";
                    else
                        tt_specify_number_of_hours.Text = dr["tt_specify_number_of_hours"].ToString();

                    ddl_tt_tenant_content_ins.SelectedValue = dr["tt_tenant_content_ins"].ToString();
                    ddl_tt_landlord_content_ins.SelectedValue = dr["tt_landlord_content_ins"].ToString();
                    ddl_tt_injury_ins.SelectedValue = dr["tt_injury_ins"].ToString();
                    ddl_tt_premises_ins.SelectedValue = dr["tt_premises_ins"].ToString();

                    tt_additional_terms.Text = dr["tt_additional_terms"].ToString();
                }

            }

            finally
            {
                conn.Close();
            }
        }
        else
        {
            txt_message.InnerHtml = "This unit is not rented";
            r_pendingtermsandconditionslist.Visible = false;
            panel_term_cond_update.Visible = false;
            panel_current_tenant.Visible = false;
            hd_current_tu_id.Value = "0";
        }

        //TabContainer1.ActiveTabIndex = 0 ;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_unit_id_2_SelectedIndexChanged(object sender, EventArgs e)
    {
        // label_added_unit.Text = ddl_unit_id.SelectedItem.Text;
        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        //get current tenant
        //get current tenant id
        //get current tenant id

        r_pendingtermsandconditionslist_2.Visible = true;
        txt_pending_2.InnerHtml = "";
        btn_continue_2.Enabled = true;

        txt_message_2.InnerHtml = "";

        hd_unit_id_2.Value = Convert.ToString(ddl_unit_id_2.SelectedValue);

        int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_unit_id_2.SelectedValue));

        //if there is a current tenant in the unit then get name(s)

        if (temp_tenant_id > 0)
        {
            panel_current_tenant_2.Visible = true;
            txt_current_tenant_name_2.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);


            //tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            hd_current_tu_id_2.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), Convert.ToInt32(ddl_unit_id_2.SelectedValue)));

            panel_term_cond_update_2.Visible = true;


            // here we check the amount of pending terms and conditions for this tenant unit
            int count = 0;

            tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            count = p.getCountPendingTermsAndConditions(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id_2.Value));


            if (count > 0)
            {
                btn_continue_2.Enabled = false;
                txt_pending_2.InnerHtml = "<strong><span style='color: #ff3300'>You already have a pending terms and conditions ,<br /> please remove pending before creating another terms and conditions, ( link goes here)</span></strong> ";

            }

            //----- Now we show there is any pending accommodation in this tenant unit
            tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            r_pendingtermsandconditionslist_2.DataSource = v.getPendingTermsAndConditionsList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id_2.Value));

            r_pendingtermsandconditionslist_2.DataBind();

            ///------------------- 
            // ( begin ) get the the terms and conditions from the data base

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prTermsAndConditionsView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            DateTime the_date = new DateTime();
            the_date = DateTime.Now; // the date in the to drop downlist

            tiger.Date d = new tiger.Date();
            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


            //Add the params
            // cmd.Parameters.Add("@return_guarantor_name_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id_2.Value);
            cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;


            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                int guarantor_name_id;

                while (dr.Read() == true)
                {

                    DateTime la_date = new DateTime();
                    la_date = Convert.ToDateTime(dr["tt_date_begin"]);

                    lbl_current_tt_date_begin_2.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();

                    lbl_current_tt_date_begin_3.Text = lbl_current_tt_date_begin_2.Text;


                    if (dr["tt_guarantor"].ToString() == "1")
                        panel_guarantor_2.Visible = true;
                    else
                        panel_guarantor_2.Visible = false;


                    if (Convert.ToInt32(dr["tt_guarantor_name_id"]) > 0)
                    {
                        ddl_guarantor_name_id_2.SelectedValue = Convert.ToString(dr["tt_guarantor_name_id"]);
                        guarantor_name_fname_2.Enabled = false;
                        guarantor_name_lname_2.Enabled = false;
                        guarantor_name_addr_2.Enabled = false;
                        guarantor_name_addr_city_2.Enabled = false;
                        guarantor_name_addr_pc_2.Enabled = false;
                        guarantor_name_addr_state_2.Enabled = false;
                        ddl_guarantor_country_id_2.Enabled = false;
                        guarantor_name_tel_2.Enabled = false;
                        guarantor_name_tel_work_2.Enabled = false;
                        guarantor_name_tel_work_ext_2.Enabled = false;
                        guarantor_name_cell_2.Enabled = false;
                        guarantor_name_fax_2.Enabled = false;
                        guarantor_name_email_2.Enabled = false;
                        guarantor_name_com_2.Enabled = false;
                        txt_2.Visible = true;
                    }
                    else
                    {
                        guarantor_name_fname_2.Enabled = true;
                        guarantor_name_lname_2.Enabled = true;
                        guarantor_name_addr_2.Enabled = true;
                        guarantor_name_addr_city_2.Enabled = true;
                        guarantor_name_addr_pc_2.Enabled = true;
                        guarantor_name_addr_state_2.Enabled = true;
                        ddl_guarantor_country_id_2.Enabled = true;
                        guarantor_name_tel_2.Enabled = true;
                        guarantor_name_tel_work_2.Enabled = true;
                        guarantor_name_tel_work_ext_2.Enabled = true;
                        guarantor_name_cell_2.Enabled = true;
                        guarantor_name_fax_2.Enabled = true;
                        guarantor_name_email_2.Enabled = true;
                        guarantor_name_com_2.Enabled = true;
                        txt_2.Visible = false;

                    }


                    /*
                    tt_nsf.Text = String.Format("{0:0.00}", dr["tt_nsf"].ToString());
                    tt_late_fee.Text = String.Format("{0:0.00}", dr["tt_late_fee"].ToString())

                    tt_security_deposit.SelectedValue = dr["tt_security_deposit"].ToString();
                    */

                    decimal nsf, late_fee, security_deposit_amount;

                    if (dr["tt_nsf"].ToString() != "")
                        nsf = Convert.ToDecimal(dr["tt_nsf"]);
                    else
                        nsf = 0;

                    if (dr["tt_late_fee"].ToString() != "")
                        late_fee = Convert.ToDecimal(dr["tt_late_fee"]);
                    else
                        late_fee = 0;

                    if (dr["tt_security_deposit_amount"].ToString() != "")
                        security_deposit_amount = Convert.ToDecimal(dr["tt_security_deposit_amount"]);
                    else
                        security_deposit_amount = 0;


                    tt_nsf_2.Text = String.Format("{0:0.00}", nsf);
                    tt_late_fee_2.Text = String.Format("{0:0.00}", late_fee);


                    tt_security_deposit_2.SelectedValue = dr["tt_security_deposit"].ToString();

                    tt_security_deposit_amount_2.Text = String.Format("{0:0.00}", security_deposit_amount);


                    tt_guarantor_2.SelectedValue = dr["tt_guarantor"].ToString();
                    ddl_tt_form_of_payment_2.SelectedValue = dr["tt_form_of_payment"].ToString();

                    tt_pets_2.SelectedValue = dr["tt_pets"].ToString();
                    tt_maintenance_2.SelectedValue = dr["tt_maintenance"].ToString();
                    tt_specify_maintenance_2.Text = dr["tt_specify_maintenance"].ToString();
                    tt_improvement_2.SelectedValue = dr["tt_improvement"].ToString();
                    tt_specify_improvement_2.Text = dr["tt_specify_improvement"].ToString();
                    tt_notice_to_enter_2.SelectedValue = dr["tt_notice_to_enter"].ToString();

                    if (dr["tt_specify_number_of_hours"].ToString() == "0")
                        tt_specify_number_of_hours_2.Text = "";
                    else
                        tt_specify_number_of_hours_2.Text = dr["tt_specify_number_of_hours"].ToString();

                    ddl_tt_tenant_content_ins_2.SelectedValue = dr["tt_tenant_content_ins"].ToString();
                    ddl_tt_landlord_content_ins_2.SelectedValue = dr["tt_landlord_content_ins"].ToString();
                    ddl_tt_injury_ins_2.SelectedValue = dr["tt_injury_ins"].ToString();
                    ddl_tt_premises_ins_2.SelectedValue = dr["tt_premises_ins"].ToString();

                    tt_additional_terms_2.Text = dr["tt_additional_terms"].ToString();

                    if (dr["unit_type"].ToString() == "C" && Convert.ToInt32(dr["company_id"]) > 0)
                    {
                        lbl_company.Text = dr["company_name"].ToString();
                    }
                    else
                    {
                        tr_company.Visible = false;
                    }
                }

            }

            finally
            {
                conn.Close();
            }


        }
        else
        {
            txt_message_2.InnerHtml = "This unit is not rented";
            r_pendingtermsandconditionslist_2.Visible = false;
            panel_term_cond_update_2.Visible = false;
            panel_current_tenant_2.Visible = false;
            hd_current_tu_id_2.Value = "0";
        }

        //TabContainer1.ActiveTabIndex = 1;
    }
    //-------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void tt_guarantor_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(tt_guarantor.SelectedValue) == "1")
            panel_guarantor.Visible = true;
        else
            panel_guarantor.Visible = false;

        //TabContainer1.ActiveTabIndex = 0;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void tt_guarantor_2_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(tt_guarantor_2.SelectedValue) == "1")
            panel_guarantor_2.Visible = true;
        else
            panel_guarantor_2.Visible = false;

        //TabContainer1.ActiveTabIndex = 1;
    }

    //-------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void tt_maintenance_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(tt_maintenance.SelectedValue) == "1")
            panel_specify_maintenance.Visible = true;
        else
            panel_specify_maintenance.Visible = true;

        //TabContainer1.ActiveTabIndex = 0 ;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void tt_maintenance_2_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(tt_maintenance_2.SelectedValue) == "1")
            panel_specify_maintenance_2.Visible = true;
        else
            panel_specify_maintenance_2.Visible = true;

        //TabContainer1.ActiveTabIndex = 1;
    }

    //-------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void tt_improvement_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(tt_improvement.SelectedValue) == "1")
            panel_specify_improvement.Visible = true;
        else
            panel_specify_improvement.Visible = true;

        //TabContainer1.ActiveTabIndex = 0 ;

    }

    protected void tt_improvement_2_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(tt_improvement_2.SelectedValue) == "1")
            panel_specify_improvement_2.Visible = true;
        else
            panel_specify_improvement_2.Visible = true;

        //TabContainer1.ActiveTabIndex = 1;

    }


    //-------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void tt_notice_to_enter_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(tt_notice_to_enter.SelectedValue) == "1")
            panel_specify_notice_to_enter.Visible = true;
        else
            panel_specify_notice_to_enter.Visible = true;

        //TabContainer1.ActiveTabIndex = 0 ;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void tt_notice_to_enter_2_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(tt_notice_to_enter_2.SelectedValue) == "1")
            panel_specify_notice_to_enter_2.Visible = true;
        else
            panel_specify_notice_to_enter_2.Visible = true;

        //TabContainer1.ActiveTabIndex = 1;
    }

    //-------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chk_guarantor_OnCheckedChanged(object sender, EventArgs e)
    {
        if (chk_guarantor.Checked == true)
        {
            ddl_guarantor_name_id.Enabled = false;
            guarantor_name_fname.Enabled = true;
            guarantor_name_lname.Enabled = true;
            guarantor_name_addr.Enabled = true;
            guarantor_name_addr_city.Enabled = true;
            guarantor_name_addr_pc.Enabled = true;
            guarantor_name_addr_state.Enabled = true;
            ddl_guarantor_country_id.Enabled = true;
            guarantor_name_tel.Enabled = true;
            guarantor_name_tel_work.Enabled = true;
            guarantor_name_tel_work_ext.Enabled = true;
            guarantor_name_cell.Enabled = true;
            guarantor_name_fax.Enabled = true;
            guarantor_name_email.Enabled = true;
            guarantor_name_com.Enabled = true;
            txt.Visible = false;

        }
        else
        {
            ddl_guarantor_name_id.Enabled = true;
            guarantor_name_fname.Enabled = false;
            guarantor_name_lname.Enabled = false;
            guarantor_name_addr.Enabled = false;
            guarantor_name_addr_city.Enabled = false;
            guarantor_name_addr_pc.Enabled = false;
            guarantor_name_addr_state.Enabled = false;
            ddl_guarantor_country_id.Enabled = false;
            guarantor_name_tel.Enabled = false;
            guarantor_name_tel_work.Enabled = false;
            guarantor_name_tel_work_ext.Enabled = false;
            guarantor_name_cell.Enabled = false;
            guarantor_name_fax.Enabled = false;
            guarantor_name_email.Enabled = false;
            guarantor_name_com.Enabled = false;
            txt.Visible = true;
        }

        //TabContainer1.ActiveTabIndex = 0 ;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chk_guarantor_2_OnCheckedChanged(object sender, EventArgs e)
    {
        if (chk_guarantor_2.Checked == true)
        {
            ddl_guarantor_name_id_2.Enabled = false;
            guarantor_name_fname_2.Enabled = true;
            guarantor_name_lname_2.Enabled = true;
            guarantor_name_addr_2.Enabled = true;
            guarantor_name_addr_city_2.Enabled = true;
            guarantor_name_addr_pc_2.Enabled = true;
            guarantor_name_addr_state_2.Enabled = true;
            ddl_guarantor_country_id_2.Enabled = true;
            guarantor_name_tel_2.Enabled = true;
            guarantor_name_tel_work_2.Enabled = true;
            guarantor_name_tel_work_ext_2.Enabled = true;
            guarantor_name_cell_2.Enabled = true;
            guarantor_name_fax_2.Enabled = true;
            guarantor_name_email_2.Enabled = true;
            guarantor_name_com_2.Enabled = true;
            txt_2.Visible = false;

        }
        else
        {
            ddl_guarantor_name_id_2.Enabled = true;
            guarantor_name_fname_2.Enabled = false;
            guarantor_name_lname_2.Enabled = false;
            guarantor_name_addr_2.Enabled = false;
            guarantor_name_addr_city_2.Enabled = false;
            guarantor_name_addr_pc_2.Enabled = false;
            guarantor_name_addr_state_2.Enabled = false;
            ddl_guarantor_country_id_2.Enabled = false;
            guarantor_name_tel_2.Enabled = false;
            guarantor_name_tel_work_2.Enabled = false;
            guarantor_name_tel_work_ext_2.Enabled = false;
            guarantor_name_cell_2.Enabled = false;
            guarantor_name_fax_2.Enabled = false;
            guarantor_name_email_2.Enabled = false;
            guarantor_name_com_2.Enabled = false;
            txt_2.Visible = true;
        }

        //TabContainer1.ActiveTabIndex = 1;
    }

//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_guarantor_name_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(ddl_guarantor_name_id.SelectedIndex) > 0)
        {
            guarantor_name_fname.Enabled = false;
            guarantor_name_lname.Enabled = false;
            guarantor_name_addr.Enabled = false;
            guarantor_name_addr_city.Enabled = false;
            guarantor_name_addr_pc.Enabled = false;
            guarantor_name_addr_state.Enabled = false;
            ddl_guarantor_country_id.Enabled = false;
            guarantor_name_tel.Enabled = false;
            guarantor_name_tel_work.Enabled = false;
            guarantor_name_tel_work_ext.Enabled = false;
            guarantor_name_cell.Enabled = false;
            guarantor_name_fax.Enabled = false;
            guarantor_name_email.Enabled = false;
            guarantor_name_com.Enabled = false;
            txt.Visible = true;
        }
        else
        {
            guarantor_name_fname.Enabled = true;
            guarantor_name_lname.Enabled = true;
            guarantor_name_addr.Enabled = true;
            guarantor_name_addr_city.Enabled = true;
            guarantor_name_addr_pc.Enabled = true;
            guarantor_name_addr_state.Enabled = true;
            ddl_guarantor_country_id.Enabled = true;
            guarantor_name_tel.Enabled = true;
            guarantor_name_tel_work.Enabled = true;
            guarantor_name_tel_work_ext.Enabled = true;
            guarantor_name_cell.Enabled = true;
            guarantor_name_fax.Enabled = true;
            guarantor_name_email.Enabled = true;
            guarantor_name_com.Enabled = true;
            txt.Visible = false;

        }

        //TabContainer1.ActiveTabIndex = 0 ;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_guarantor_name_id_2_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(ddl_guarantor_name_id_2.SelectedIndex) > 0)
        {
            guarantor_name_fname_2.Enabled = false;
            guarantor_name_lname_2.Enabled = false;
            guarantor_name_addr_2.Enabled = false;
            guarantor_name_addr_city_2.Enabled = false;
            guarantor_name_addr_pc_2.Enabled = false;
            guarantor_name_addr_state_2.Enabled = false;
            ddl_guarantor_country_id_2.Enabled = false;
            guarantor_name_tel_2.Enabled = false;
            guarantor_name_tel_work_2.Enabled = false;
            guarantor_name_tel_work_ext_2.Enabled = false;
            guarantor_name_cell_2.Enabled = false;
            guarantor_name_fax_2.Enabled = false;
            guarantor_name_email_2.Enabled = false;
            guarantor_name_com_2.Enabled = false;
            txt.Visible = true;
        }
        else
        {
            guarantor_name_fname_2.Enabled = true;
            guarantor_name_lname_2.Enabled = true;
            guarantor_name_addr_2.Enabled = true;
            guarantor_name_addr_city_2.Enabled = true;
            guarantor_name_addr_pc_2.Enabled = true;
            guarantor_name_addr_state_2.Enabled = true;
            ddl_guarantor_country_id_2.Enabled = true;
            guarantor_name_tel_2.Enabled = true;
            guarantor_name_tel_work_2.Enabled = true;
            guarantor_name_tel_work_ext_2.Enabled = true;
            guarantor_name_cell_2.Enabled = true;
            guarantor_name_fax_2.Enabled = true;
            guarantor_name_email_2.Enabled = true;
            guarantor_name_com_2.Enabled = true;
            txt_2.Visible = false;


        }

        //TabContainer1.ActiveTabIndex = 1;
    }

//-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_continue_Onclick(object sender, EventArgs e)
    {


        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prTermsAndConditionsUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        conn.Open();
        //Add the params
        cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
        cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
        cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id.Value);

        DateTime the_date = new DateTime();
        the_date = DateTime.Now; // the date in the to drop downlist

        tiger.Date d = new tiger.Date();
        the_date = Convert.ToDateTime(d.DateCulture(ddl_tt_date_begin_m.SelectedValue, ddl_tt_date_begin_d.SelectedValue, ddl_tt_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        cmd.Parameters.Add("@current_tt_date_end", SqlDbType.SmallDateTime).Value = the_date ;
        cmd.Parameters.Add("@tt_date_begin", SqlDbType.SmallDateTime).Value = the_date ;


        if(ddl_guarantor_name_id.SelectedIndex == 0)
            cmd.Parameters.Add("@guarantor_name_id", SqlDbType.Int).Value = 0 ;
        else
            cmd.Parameters.Add("@guarantor_name_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_guarantor_name_id.SelectedValue);


        cmd.Parameters.Add("@guarantor_name_lname", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_fname.Text);
        cmd.Parameters.Add("@guarantor_name_fname", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_lname.Text);
        cmd.Parameters.Add("@guarantor_name_addr", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_addr_city.Text);
        cmd.Parameters.Add("@guarantor_name_addr_city", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_addr_state.Text);
        cmd.Parameters.Add("@guarantor_name_addr_pc", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_addr_pc.Text);
        cmd.Parameters.Add("@guarantor_name_addr_state", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_addr_state.Text);
        cmd.Parameters.Add("@guarantor_country_id", SqlDbType.NVarChar, 50).Value = Convert.ToInt32(ddl_guarantor_country_id.SelectedValue);
        cmd.Parameters.Add("@guarantor_name_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_tel.Text);
        cmd.Parameters.Add("@guarantor_name_tel_work", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_tel_work.Text);
        cmd.Parameters.Add("@guarantor_name_tel_work_ext", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_tel_work_ext.Text);
        cmd.Parameters.Add("@guarantor_name_cell", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_cell.Text);
        cmd.Parameters.Add("@guarantor_name_fax", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_fax.Text);
        cmd.Parameters.Add("@guarantor_name_email", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_email.Text);
        cmd.Parameters.Add("@guarantor_name_com", SqlDbType.Text).Value = RegEx.getText(guarantor_name_com.Text);


        if (Convert.ToString(tt_guarantor.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_guarantor", SqlDbType.Int).Value = 1;
        else
            cmd.Parameters.Add("@tt_guarantor", SqlDbType.Int).Value = 0;


        if (Convert.ToString(tt_security_deposit.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_security_deposit", SqlDbType.Int).Value = 1;
        else
            cmd.Parameters.Add("@tt_security_deposit", SqlDbType.Int).Value = 0;


        if (tt_security_deposit_amount.Text != String.Empty || tt_security_deposit_amount.Text != "")
            cmd.Parameters.Add("@tt_security_deposit_amount", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tt_security_deposit_amount.Text));
        else
            cmd.Parameters.Add("@tt_security_deposit_amount", SqlDbType.Money).Value = 0;
              

        if (Convert.ToString(tt_pets.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_pets", SqlDbType.Int).Value = 1;
        else
            cmd.Parameters.Add("@tt_pets", SqlDbType.Int).Value = 0;


        if (Convert.ToString(tt_maintenance.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_maintenance", SqlDbType.Int).Value = 1;
        else
            cmd.Parameters.Add("@tt_maintenance", SqlDbType.Int).Value = 0;

        cmd.Parameters.Add("@tt_specify_maintenance", SqlDbType.Text).Value = RegEx.getText(tt_specify_maintenance.Text);


        if (Convert.ToString(tt_improvement.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_improvement", SqlDbType.Int).Value = 1;
        else
            cmd.Parameters.Add("@tt_improvement", SqlDbType.Int).Value = 0;

        cmd.Parameters.Add("@tt_specify_improvement", SqlDbType.Text).Value = RegEx.getText(tt_specify_improvement.Text);


        if (Convert.ToString(tt_notice_to_enter.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_notice_to_enter", SqlDbType.Int).Value = 1;
        else
            cmd.Parameters.Add("@tt_notice_to_enter", SqlDbType.Int).Value = 0;

        if (tt_specify_number_of_hours.Text == "")
            cmd.Parameters.Add("@tt_specify_number_of_hours", SqlDbType.Int).Value = 0;
        else
            cmd.Parameters.Add("@tt_specify_number_of_hours", SqlDbType.Int).Value = Convert.ToInt32(RegEx.getInteger(tt_specify_number_of_hours.Text));

        if (Convert.ToString(ddl_tt_tenant_content_ins.SelectedValue) == "0")
            cmd.Parameters.Add("@tt_tenant_content_ins", SqlDbType.Int).Value = 0;

        if (Convert.ToString(ddl_tt_tenant_content_ins.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_tenant_content_ins", SqlDbType.Int).Value = 1;

        if (Convert.ToString(ddl_tt_tenant_content_ins.SelectedValue) == "2")
            cmd.Parameters.Add("@tt_tenant_content_ins", SqlDbType.Int).Value = 2;


        if (Convert.ToString(ddl_tt_landlord_content_ins.SelectedValue) == "0")
            cmd.Parameters.Add("@tt_landlord_content_ins", SqlDbType.Int).Value = 0;
        if (Convert.ToString(ddl_tt_landlord_content_ins.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_landlord_content_ins", SqlDbType.Int).Value = 1;
        if (Convert.ToString(ddl_tt_landlord_content_ins.SelectedValue) == "2")
            cmd.Parameters.Add("@tt_landlord_content_ins", SqlDbType.Int).Value = 2;



        if (Convert.ToString(ddl_tt_injury_ins.SelectedValue) == "0")
            cmd.Parameters.Add("@tt_injury_ins", SqlDbType.Int).Value = 0;
        if (Convert.ToString(ddl_tt_injury_ins.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_injury_ins", SqlDbType.Int).Value = 1;
        if (Convert.ToString(ddl_tt_injury_ins.SelectedValue) == "2")
            cmd.Parameters.Add("@tt_injury_ins", SqlDbType.Int).Value = 2;



        if (Convert.ToString(ddl_tt_premises_ins.SelectedValue) == "0")
            cmd.Parameters.Add("@tt_premises_ins", SqlDbType.Int).Value = 0;
        if (Convert.ToString(ddl_tt_premises_ins.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_premises_ins", SqlDbType.Int).Value = 1;
        if (Convert.ToString(ddl_tt_premises_ins.SelectedValue) == "2")
            cmd.Parameters.Add("@tt_premises_ins", SqlDbType.Int).Value = 2;

        cmd.Parameters.Add("@tt_additional_terms", SqlDbType.Text).Value = RegEx.getText(tt_additional_terms.Text);
        cmd.Parameters.Add("@tt_nsf", SqlDbType.SmallMoney).Value = Convert.ToDecimal(RegEx.getMoney(tt_nsf.Text));

        if (tt_late_fee.Text == "")
            cmd.Parameters.Add("@tt_late_fee", SqlDbType.Int).Value = 0;
        else
            cmd.Parameters.Add("@tt_late_fee", SqlDbType.SmallMoney).Value = Convert.ToDecimal(RegEx.getMoney(tt_late_fee.Text));
        cmd.Parameters.Add("@tt_form_of_payment", SqlDbType.Int).Value = Convert.ToInt32(ddl_tt_form_of_payment.SelectedValue);
        cmd.ExecuteReader();
        conn.Close();

        //TabContainer1.ActiveTabIndex = 0 ;
    }




    protected void btn_continue_2_Onclick(object sender, EventArgs e)
    {


        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prTermsAndConditionsUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        conn.Open();
        //Add the params
        cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
        cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
        cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id_2.Value);


        DateTime the_date = new DateTime();
        the_date = DateTime.Now; // the date in the to drop downlist

        tiger.Date d = new tiger.Date();
        the_date = Convert.ToDateTime(d.DateCulture(ddl_tt_date_begin_m2.SelectedValue, ddl_tt_date_begin_d2.SelectedValue, ddl_tt_date_begin_y2.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        cmd.Parameters.Add("@current_tt_date_end", SqlDbType.SmallDateTime).Value = the_date;
        cmd.Parameters.Add("@tt_date_begin", SqlDbType.SmallDateTime).Value = the_date;

        if (ddl_guarantor_name_id_2.SelectedIndex == 0)
            cmd.Parameters.Add("@guarantor_name_id", SqlDbType.Int).Value = 0;
        else
            cmd.Parameters.Add("@guarantor_name_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_guarantor_name_id_2.SelectedValue);

        cmd.Parameters.Add("@guarantor_name_lname", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_fname_2.Text);
        cmd.Parameters.Add("@guarantor_name_fname", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_lname_2.Text);
        cmd.Parameters.Add("@guarantor_name_addr", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_addr_city_2.Text);
        cmd.Parameters.Add("@guarantor_name_addr_city", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_addr_state_2.Text);
        cmd.Parameters.Add("@guarantor_name_addr_pc", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_addr_pc_2.Text);
        cmd.Parameters.Add("@guarantor_name_addr_state", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_addr_state_2.Text);
        cmd.Parameters.Add("@guarantor_country_id", SqlDbType.NVarChar, 50).Value = Convert.ToInt32(ddl_guarantor_country_id_2.SelectedValue);
        cmd.Parameters.Add("@guarantor_name_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_tel_2.Text);
        cmd.Parameters.Add("@guarantor_name_tel_work", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_tel_work_2.Text);
        cmd.Parameters.Add("@guarantor_name_tel_work_ext", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_tel_work_ext_2.Text);
        cmd.Parameters.Add("@guarantor_name_cell", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_cell_2.Text);
        cmd.Parameters.Add("@guarantor_name_fax", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_fax_2.Text);
        cmd.Parameters.Add("@guarantor_name_email", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_name_email_2.Text);
        cmd.Parameters.Add("@guarantor_name_com", SqlDbType.Text).Value = RegEx.getText(guarantor_name_com_2.Text);


        if (Convert.ToString(tt_guarantor_2.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_guarantor", SqlDbType.Int).Value = 1;
        else
            cmd.Parameters.Add("@tt_guarantor", SqlDbType.Int).Value = 0;


        if (Convert.ToString(tt_security_deposit_2.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_security_deposit", SqlDbType.Int).Value = 1;
        else
            cmd.Parameters.Add("@tt_security_deposit", SqlDbType.Int).Value = 0;


       cmd.Parameters.Add("@tt_security_deposit_amount", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tt_security_deposit_amount_2.Text));
        
        

        if (Convert.ToString(tt_pets_2.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_pets", SqlDbType.Int).Value = 1;
        else
            cmd.Parameters.Add("@tt_pets", SqlDbType.Int).Value = 0;


        if (Convert.ToString(tt_maintenance_2.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_maintenance", SqlDbType.Int).Value = 1;
        else
            cmd.Parameters.Add("@tt_maintenance", SqlDbType.Int).Value = 0;

        cmd.Parameters.Add("@tt_specify_maintenance", SqlDbType.Text).Value = RegEx.getText(tt_specify_maintenance_2.Text);


        if (Convert.ToString(tt_improvement_2.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_improvement", SqlDbType.Int).Value = 1;
        else
            cmd.Parameters.Add("@tt_improvement", SqlDbType.Int).Value = 0;

        cmd.Parameters.Add("@tt_specify_improvement", SqlDbType.Text).Value = RegEx.getText(tt_specify_improvement_2.Text);


        if (Convert.ToString(tt_notice_to_enter_2.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_notice_to_enter", SqlDbType.Int).Value = 1;
        else
            cmd.Parameters.Add("@tt_notice_to_enter", SqlDbType.Int).Value = 0;

        if (tt_specify_number_of_hours_2.Text == "")
            cmd.Parameters.Add("@tt_specify_number_of_hours", SqlDbType.Int).Value = 0;
        else
            cmd.Parameters.Add("@tt_specify_number_of_hours", SqlDbType.Int).Value = Convert.ToInt32(RegEx.getInteger(tt_specify_number_of_hours_2.Text));

        if (Convert.ToString(ddl_tt_tenant_content_ins_2.SelectedValue) == "0")
            cmd.Parameters.Add("@tt_tenant_content_ins", SqlDbType.Int).Value = 0;

        if (Convert.ToString(ddl_tt_tenant_content_ins_2.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_tenant_content_ins", SqlDbType.Int).Value = 1;

        if (Convert.ToString(ddl_tt_tenant_content_ins_2.SelectedValue) == "2")
            cmd.Parameters.Add("@tt_tenant_content_ins", SqlDbType.Int).Value = 2;

        if (Convert.ToString(ddl_tt_landlord_content_ins_2.SelectedValue) == "0")
            cmd.Parameters.Add("@tt_landlord_content_ins", SqlDbType.Int).Value = 0;
        if (Convert.ToString(ddl_tt_landlord_content_ins_2.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_landlord_content_ins", SqlDbType.Int).Value = 1;
        if (Convert.ToString(ddl_tt_landlord_content_ins_2.SelectedValue) == "2")
            cmd.Parameters.Add("@tt_landlord_content_ins", SqlDbType.Int).Value = 2;

        if (Convert.ToString(ddl_tt_injury_ins_2.SelectedValue) == "0")
            cmd.Parameters.Add("@tt_injury_ins", SqlDbType.Int).Value = 0;
        if (Convert.ToString(ddl_tt_injury_ins_2.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_injury_ins", SqlDbType.Int).Value = 1;
        if (Convert.ToString(ddl_tt_injury_ins_2.SelectedValue) == "2")
            cmd.Parameters.Add("@tt_injury_ins", SqlDbType.Int).Value = 2;

        if (Convert.ToString(ddl_tt_premises_ins_2.SelectedValue) == "0")
            cmd.Parameters.Add("@tt_premises_ins", SqlDbType.Int).Value = 0;
        if (Convert.ToString(ddl_tt_premises_ins_2.SelectedValue) == "1")
            cmd.Parameters.Add("@tt_premises_ins", SqlDbType.Int).Value = 1;
        if (Convert.ToString(ddl_tt_premises_ins_2.SelectedValue) == "2")
            cmd.Parameters.Add("@tt_premises_ins", SqlDbType.Int).Value = 2;



        cmd.Parameters.Add("@tt_additional_terms", SqlDbType.Text).Value = tt_additional_terms_2.Text;
        cmd.Parameters.Add("@tt_nsf", SqlDbType.SmallMoney).Value = Convert.ToDecimal(RegEx.getMoney(tt_nsf_2.Text));


        if (tt_late_fee_2.Text == "")
            cmd.Parameters.Add("@tt_late_fee", SqlDbType.Int).Value = 0;
        else
            cmd.Parameters.Add("@tt_late_fee", SqlDbType.SmallMoney).Value = Convert.ToDecimal(RegEx.getMoney(tt_late_fee_2.Text));
        cmd.Parameters.Add("@tt_form_of_payment", SqlDbType.Int).Value = Convert.ToInt32(ddl_tt_form_of_payment_2.SelectedValue);
        cmd.ExecuteReader();

        conn.Close();

        //TabContainer1.ActiveTabIndex = 1;
    }

}
