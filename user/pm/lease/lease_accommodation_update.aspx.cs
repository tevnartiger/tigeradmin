using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
/// <summary>
/// Done by : Stanley Jocelyn
/// </summary>

public partial class home_lease_accommodation_update : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
          if (!Page.IsPostBack)
            {

                reg_ta_com.ValidationExpression = RegEx.getText();
                reg_ta_com_2.ValidationExpression = RegEx.getText();
           

                   
                //int home_id = 1;// Convert.ToInt32(Request.QueryString["home_id"]);
                r_pendingaccommodationlist.Visible = true;
                txt_pending.InnerHtml = "";
                btn_continue.Enabled = true;

                r_pendingaccommodationlist_2.Visible = true;
                txt_pending_2.InnerHtml = "";
                btn_continue_2.Enabled = true;
             

                txt_message.InnerHtml = "";
                txt_message_2.InnerHtml = "";

                tiger.PM h = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int home_count = h.getPMHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                string link_to_unit = "";
                if (home_count > 0)
                {
                    int home_id = h.getPMHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                    ddl_home_id.Visible = true ;
                    //hidden fields
                    hd_home_id.Value = Convert.ToString(home_id);

                    link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                    //Session["schema_id"]));

                    ddl_home_id.DataSource = h.getPMHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                    ddl_home_id.SelectedValue = Convert.ToString(home_id);
                    ddl_home_id.DataBind();

                    //*********************************************
                    // DropDownList pour les Unit
                    //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                    tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

                    if (unit_count > 0)
                    {

                        int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), home_id);
                        //link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                        //Session["schema_id"]));

                        // unit_id hiddenfield
                        hd_unit_id.Value = Convert.ToString(unit_id);

                        ddl_unit_id.DataSource = u.getUnitResList(Convert.ToInt32(Session["schema_id"]), home_id);
                        ddl_unit_id.SelectedValue = Convert.ToString(unit_id);
                        ddl_unit_id.DataBind();




                        //get current tenant id
                        int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), unit_id);
                        //if there is a current tenant in the unit then get name(s)
                        if (temp_tenant_id > 0)
                        {
                            panel_current_tenant.Visible = true;
                            txt_current_tenant_name.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);

                            tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                            hd_current_tu_id.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), unit_id));


                            panel_accommodation_update.Visible = true;


                          // here we check the amount of pending accommodation for this tenant unit
                            int count = 0;
                            
                            tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                            count = p.getCountPendingAccommodation(Convert.ToInt32(Session["schema_id"]),Convert.ToInt32(hd_current_tu_id.Value));


                            if (count > 0)
                            {
                                btn_continue.Enabled = false;
                                txt_pending.InnerHtml = "<strong><span style='color: #ff3300'>You already have a pending accommodation ,<br /> please remove pending before creating another accommodation, ( link goes here)</span></strong> ";

                            }

                            //----- Now we show there is any pending accommodation in this tenant unit
                            tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                            r_pendingaccommodationlist.DataSource = v.getPendingAccommodationList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id.Value));

                            r_pendingaccommodationlist.DataBind();

                            ///------------------- 
                            ///




                            // The accommodation for the current tu_id ( current tenant in the unit )

                            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                            SqlCommand cmd = new SqlCommand("prAccommodationView", conn);
                            cmd.CommandType = CommandType.StoredProcedure;

                            DateTime the_date = new DateTime();
                            the_date = DateTime.Now; // the date in the to drop downlist

                            tiger.Date d = new tiger.Date();
                            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
                            // and convert it in Datetime
                            the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


                            //Add the params
                            cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                            cmd.Parameters.Add("tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id.Value);
                            cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;
                            

                            try
                            {
                                conn.Open();

                                SqlDataReader dr = null;
                                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                                while (dr.Read() == true)
                                {

                                   // lbl_current_ta_date_begin.Text = dr["ta_date_begin"].ToString();

                                    DateTime la_date = new DateTime();
                                    la_date = Convert.ToDateTime(dr["ta_date_begin"]);

                                    lbl_current_ta_date_begin.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();

                                    
                                    
                                    lbl_current_ta_date_begin1.Text = lbl_current_ta_date_begin.Text;


                                    DateTime new_date_begin = new DateTime();
                                    new_date_begin = Convert.ToDateTime(dr["ta_date_begin"]);

                                    hd_min_begin_date_m.Value = new_date_begin.Month.ToString();
                                    hd_min_begin_date_d.Value = new_date_begin.Day.ToString();
                                    hd_min_begin_date_y.Value = new_date_begin.Year.ToString();


                                    if (Convert.ToInt32(dr["ta_electricity_inc"]) == 1)
                                        ta_electricity_inc.Checked = true;
                                    else
                                        ta_electricity_inc.Checked = false;

                                    if (Convert.ToInt32(dr["ta_heat_inc"]) == 1)
                                        ta_heat_inc.Checked = true;
                                    else
                                        ta_heat_inc.Checked = false;

                                    if (Convert.ToInt32(dr["ta_water_inc"]) == 1)
                                        ta_water_inc.Checked = true;
                                    else
                                        ta_water_inc.Checked = false;

                                    if (Convert.ToInt32(dr["ta_water_tax_inc"]) == 1)
                                        ta_water_tax_inc.Checked = true;
                                    else
                                        ta_water_tax_inc.Checked = false;

                                    if (Convert.ToInt32(dr["ta_garage_inc"]) == 1)
                                        ta_garage_inc.Checked = true;
                                    else
                                        ta_garage_inc.Checked = false;

                                    if (Convert.ToInt32(dr["ta_parking_inc"]) == 1)
                                        ta_parking_inc.Checked = true;
                                    else
                                        ta_parking_inc.Checked = false;

                                    if (Convert.ToInt32(dr["ta_semi_furnished_inc"]) == 1)
                                        ta_semi_furnished_inc.Checked = true;
                                    else
                                        ta_semi_furnished_inc.Checked = false;

                                    if (Convert.ToInt32(dr["ta_furnished_inc"]) == 1)
                                        ta_furnished_inc.Checked = true;
                                    else
                                        ta_furnished_inc.Checked = false;


                                    ta_com.Text = Convert.ToString(dr["ta_com"]);

                                }

                            }

                            finally
                            {

                                conn.Close();
                            }
                        }

                        else
                        {
                            hd_current_tu_id.Value = "0";
                            //hidden fields
                            txt_message.InnerHtml = "This unit is not rented";
                            panel_accommodation_update.Visible = false;
                            r_pendingaccommodationlist.Visible = false;

                        }


                    }

                    // if we dont fin an unit
                    else
                    {
                        txt_message.InnerHtml = "This is no unit in this property";
                        r_pendingaccommodationlist.Visible = false;
                        panel_accommodation_update.Visible = false;
                        hd_current_tu_id.Value = "0";
                        hd_unit_id.Value = "0";
                     }
                    

                    /************************Now get home information*****************************/

                    //txt_link.InnerHtml = h.getHomeViewInfo(Session["schema_id"], home_id, Convert.ToChar(Session["user_lang"]));
                    // txt_link.InnerHtml = link_to_unit;
                }
               // if ther is no home
                else
                {

                    txt_message.InnerHtml = "This is no property -- add a property";
                    r_pendingaccommodationlist.Visible = false;
                    panel_accommodation_update.Visible = false;
                    hd_home_id.Value = "0";
                    hd_current_tu_id.Value = "0";
                    hd_unit_id.Value = "0";
                    ddl_home_id.Visible = false;
                    txt_link.InnerHtml = home_count + " <a href='home_add.aspx'>Add Home</a>&nbsp;&nbsp;<a href='owner_add.aspx'>Add Owner</a>&nbsp;&nbsp;<a href='janitor_add.aspx'>Add Janitor</a>&nbsp;&nbsp;";
                }

              //------------- COMMERCIAL UNITS ------------------------------------------------

               // tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int homecom_count = h.getPMHomeComCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                string link_to_unit2 = "";
              
               if (homecom_count > 0)
                {
                    int home_id = h.getPMHomeComFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                    ddl_home_id_2.Visible = true;
                    //hidden fields
                    hd_home_id_2.Value = Convert.ToString(home_id);

                    link_to_unit2 = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                    //Session["schema_id"]));

                    ddl_home_id_2.DataSource = h.getPMHomeComList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                    ddl_home_id_2.SelectedValue = Convert.ToString(home_id);
                    ddl_home_id_2.DataBind();

                    //*********************************************
                    // DropDownList pour les Unit
                    //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                    tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    int unit_count = u.getUnitComCount(Convert.ToInt32(Session["schema_id"]), home_id);

                    if (unit_count > 0)
                    {

                        int unit_id = u.getUnitComFirstId(Convert.ToInt32(Session["schema_id"]), home_id);
                        //link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                        //Session["schema_id"]));

                        // unit_id hiddenfield
                        hd_unit_id_2.Value = Convert.ToString(unit_id);

                        ddl_unit_id_2.DataSource = u.getUnitComList(Convert.ToInt32(Session["schema_id"]), home_id);
                        ddl_unit_id_2.SelectedValue = Convert.ToString(unit_id);
                        ddl_unit_id_2.DataBind();




                        //get current tenant id
                        int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), unit_id);
                        //if there is a current tenant in the unit then get name(s)
                        if (temp_tenant_id > 0)
                        {
                            panel_current_tenant_2.Visible = true;
                            txt_current_tenant_name_2.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);

                            tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                            hd_current_tu_id_2.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), unit_id));


                            panel_accommodation_update_2.Visible = true;


                            // here we check the amount of pending accommodation for this tenant unit
                            int count = 0;

                            tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                            count = p.getCountPendingAccommodation(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id_2.Value));


                            if (count > 0)
                            {
                                btn_continue_2.Enabled = false;
                                txt_pending_2.InnerHtml = "<strong><span style='color: #ff3300'>You already have a pending accommodation ,<br /> please remove pending before creating another accommodation, ( link goes here)</span></strong> ";

                            }

                            //----- Now we show there is any pending accommodation in this tenant unit
                            tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                            r_pendingaccommodationlist_2.DataSource = v.getPendingAccommodationList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id_2.Value));

                            r_pendingaccommodationlist_2.DataBind();

                            ///------------------- 
                            ///




                            // The accommodation for the current tu_id ( current tenant in the unit )

                            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                            SqlCommand cmd = new SqlCommand("prAccommodationView", conn);
                            cmd.CommandType = CommandType.StoredProcedure;

                            DateTime the_date = new DateTime();
                            the_date = DateTime.Now; // the date in the to drop downlist

                            tiger.Date d = new tiger.Date();
                            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
                            // and convert it in Datetime
                            the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


                            //Add the params
                            cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                            cmd.Parameters.Add("tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id_2.Value);
                            cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;


                            try
                            {
                                conn.Open();

                                SqlDataReader dr = null;
                                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                                while (dr.Read() == true)
                                {

                                    // lbl_current_ta_date_begin.Text = dr["ta_date_begin"].ToString();

                                    DateTime la_date = new DateTime();
                                    la_date = Convert.ToDateTime(dr["ta_date_begin"]);

                                    lbl_current_ta_date_begin2.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();



                                    lbl_current_ta_date_begin3.Text = lbl_current_ta_date_begin2.Text;


                                    DateTime new_date_begin = new DateTime();
                                    new_date_begin = Convert.ToDateTime(dr["ta_date_begin"]);

                                    hd_min_begin_date_m2.Value = new_date_begin.Month.ToString();
                                    hd_min_begin_date_d2.Value = new_date_begin.Day.ToString();
                                    hd_min_begin_date_y2.Value = new_date_begin.Year.ToString();


                                    if (Convert.ToInt32(dr["ta_electricity_inc"]) == 1)
                                        ta_electricity_inc_2.Checked = true;
                                    else
                                        ta_electricity_inc_2.Checked = false;

                                    if (Convert.ToInt32(dr["ta_heat_inc"]) == 1)
                                        ta_heat_inc_2.Checked = true;
                                    else
                                        ta_heat_inc_2.Checked = false;

                                    if (Convert.ToInt32(dr["ta_water_inc"]) == 1)
                                        ta_water_inc_2.Checked = true;
                                    else
                                        ta_water_inc_2.Checked = false;

                                    if (Convert.ToInt32(dr["ta_water_tax_inc"]) == 1)
                                        ta_water_tax_inc_2.Checked = true;
                                    else
                                        ta_water_tax_inc_2.Checked = false;

                                    if (Convert.ToInt32(dr["ta_garage_inc"]) == 1)
                                        ta_garage_inc_2.Checked = true;
                                    else
                                        ta_garage_inc_2.Checked = false;

                                    if (Convert.ToInt32(dr["ta_parking_inc"]) == 1)
                                        ta_parking_inc_2.Checked = true;
                                    else
                                        ta_parking_inc_2.Checked = false;

                                    if (Convert.ToInt32(dr["ta_semi_furnished_inc"]) == 1)
                                        ta_semi_furnished_inc_2.Checked = true;
                                    else
                                        ta_semi_furnished_inc_2.Checked = false;

                                    if (Convert.ToInt32(dr["ta_furnished_inc"]) == 1)
                                        ta_furnished_inc_2.Checked = true;
                                    else
                                        ta_furnished_inc_2.Checked = false;


                                    ta_com_2.Text = Convert.ToString(dr["ta_com"]);


                                    if (dr["unit_type"].ToString() == "C" && Convert.ToInt32(dr["company_id"]) > 0)
                                    {
                                        lbl_company.Text = dr["company_name"].ToString();
                                    }
                                    else
                                    {
                                        tr_company.Visible = false;
                                    }

                                }

                            }

                            finally
                            {

                                conn.Close();
                            }
                        }

                        else
                        {
                            hd_current_tu_id_2.Value = "0";
                            //hidden fields
                            txt_message_2.InnerHtml = "This unit is not rented";
                            panel_accommodation_update_2.Visible = false;
                            r_pendingaccommodationlist_2.Visible = false;

                        }


                    }

                    // if we dont fin an unit
                    else
                    {
                        txt_message_2.InnerHtml = "This is no unit in this property";
                        r_pendingaccommodationlist_2.Visible = false;
                        panel_accommodation_update_2.Visible = false;
                        hd_current_tu_id_2.Value = "0";
                        hd_unit_id_2.Value = "0";
                    }


                    /************************Now get home information*****************************/

                    //txt_link.InnerHtml = h.getHomeViewInfo(Session["schema_id"], home_id, Convert.ToChar(Session["user_lang"]));
                    // txt_link.InnerHtml = link_to_unit;
                }
                // if ther is no home
                else
                {

                    txt_message_2.InnerHtml = "This is no property -- add a property";
                    r_pendingaccommodationlist_2.Visible = false;
                    panel_accommodation_update_2.Visible = false;
                    hd_home_id_2.Value = "0";
                    hd_current_tu_id_2.Value = "0";
                    hd_unit_id_2.Value = "0";
                    ddl_home_id_2.Visible = false;
                    btn_continue_2.Visible = false;
                    txt_link_2.InnerHtml = homecom_count + " <a href='home_add.aspx'>Add Home</a>&nbsp;&nbsp;<a href='owner_add.aspx'>Add Owner</a>&nbsp;&nbsp;<a href='janitor_add.aspx'>Add Janitor</a>&nbsp;&nbsp;";
                }

            }
        
    }

    //-----------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------

    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {

            //  if (!Page.IsPostBack)
            //  { 
            txt_message.InnerHtml = "";
            r_pendingaccommodationlist.Visible = true;
            ddl_home_id.Visible = true;

            txt_pending.InnerHtml = "";
            btn_continue.Enabled = true;

 
            hd_home_id.Value = Convert.ToString(ddl_home_id.SelectedValue);
            tiger.Unit u = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
            //change list of unit id

            if (unit_id > 0)
            {
                ddl_unit_id.Visible = true;
                //first dispose of old ddl_unit
                ddl_unit_id.Dispose();
                ddl_unit_id.DataSource = u.getUnitResList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
                ddl_unit_id.DataBind();

                hd_unit_id.Value = Convert.ToString(unit_id);



                //get current tenant id
                int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), unit_id);
                //if there is a current tenant in the unit then get name(s)
                if (temp_tenant_id > 0)
                {
                    panel_current_tenant.Visible = true;

                    txt_current_tenant_name.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);

                    tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                    hd_current_tu_id.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), unit_id));

                    panel_accommodation_update.Visible = true;

                    // The accommodation for the current tu_id ( current tenant in the unit )




                    // here we check the amount of pending accommodation for this tenant unit
                    int count = 0;

                    tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                    count = p.getCountPendingAccommodation(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id.Value));


                    if (count > 0)
                    {
                        btn_continue.Enabled = false;
                        txt_pending.InnerHtml = "<strong><span style='color: #ff3300'>You already have a pending accommodation ,<br /> please remove pending before creating another accommodation, ( link goes here)</span></strong> ";

                    }

                    //----- Now we show there is any pending accommodation in this tenant unit
                    tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                    r_pendingaccommodationlist.DataSource = v.getPendingAccommodationList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id.Value));

                    r_pendingaccommodationlist.DataBind();

                    ///------------------- 
                    ///


                    SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    SqlCommand cmd = new SqlCommand("prAccommodationView", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    DateTime the_date = new DateTime();
                    the_date = DateTime.Now; // the date in the to drop downlist

                    tiger.Date d = new tiger.Date();
                    // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
                    // and convert it in Datetime
                    the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


                    //Add the params
                    cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                    cmd.Parameters.Add("tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id.Value);
                    cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;

                    try
                    {
                        conn.Open();

                        SqlDataReader dr = null;
                        dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                        while (dr.Read() == true)
                        {
                           // lbl_rent_amount.Text = Convert.ToString(Convert.ToInt32(dr["rl_rent_amount"]));
                          //  ddl_ta_rent_paid_every.SelectedValue = dr["rl_rent_paid_every"].ToString();

                            

                         //   lbl_current_ta_date_begin.Text = String.Format("{0:d}", dr["ta_date_begin"]);


                            DateTime la_date = new DateTime();
                            la_date = Convert.ToDateTime(dr["ta_date_begin"]);

                            lbl_current_ta_date_begin.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();


                            
                            lbl_current_ta_date_begin1.Text = lbl_current_ta_date_begin.Text;


                            DateTime new_date_begin = new DateTime();
                            new_date_begin = Convert.ToDateTime(dr["ta_date_begin"]);

                            hd_min_begin_date_m.Value = new_date_begin.Month.ToString();
                            hd_min_begin_date_d.Value = new_date_begin.Day.ToString();
                            hd_min_begin_date_y.Value = new_date_begin.Year.ToString();


                            if (Convert.ToInt32(dr["ta_electricity_inc"]) == 1)
                                ta_electricity_inc.Checked = true;
                           
                            else
                                ta_electricity_inc.Checked = false;

                            if (Convert.ToInt32(dr["ta_heat_inc"]) == 1)
                                ta_heat_inc.Checked = true;
                            else
                                ta_heat_inc.Checked = false;

                            if (Convert.ToInt32(dr["ta_water_inc"]) == 1)
                                ta_water_inc.Checked = true;
                            else
                                ta_water_inc.Checked = false;

                            if (Convert.ToInt32(dr["ta_water_tax_inc"]) == 1)
                                ta_water_tax_inc.Checked = true;
                            else
                                ta_water_tax_inc.Checked = false;

                            if (Convert.ToInt32(dr["ta_garage_inc"]) == 1)
                                ta_garage_inc.Checked = true;
                            else
                                ta_garage_inc.Checked = false;

                            if (Convert.ToInt32(dr["ta_parking_inc"]) == 1)
                                ta_parking_inc.Checked = true;
                            else
                                ta_parking_inc.Checked = false;

                            if (Convert.ToInt32(dr["ta_semi_furnished_inc"]) == 1)
                                ta_semi_furnished_inc.Checked = true;
                            else
                                ta_semi_furnished_inc.Checked = false;

                            if (Convert.ToInt32(dr["ta_furnished_inc"]) == 1)
                                ta_furnished_inc.Checked = true;
                            else
                                ta_furnished_inc.Checked = false;

                            
                            ta_com.Text = Convert.ToString(dr["ta_com"]);
                        }



                    }


                    finally
                    {

                        conn.Close();
                    }



                }
                else
                {
                    txt_message.InnerHtml = "This is unit is not rented";
                    r_pendingaccommodationlist.Visible = false;
                    panel_accommodation_update.Visible = false;
                    panel_current_tenant.Visible = false;
                    hd_current_tu_id.Value = "0";
                }
                //hidden fields

             //   hd_unit_id.Value = Convert.ToString(ddl_unit_id.SelectedValue);
            }
            else
            {
                txt_message.InnerHtml = "This is no unit in this property";
                r_pendingaccommodationlist.Visible = false;
                panel_accommodation_update.Visible = false;
                ddl_unit_id.Visible = false;
                //  txt_link.InnerHtml = "<a href='unit_add.aspx?home_id='>Add Unit</a>";
                hd_current_tu_id.Value = "0";
                hd_unit_id.Value = "0";

            }

            //TabContainer1.ActiveTabIndex = 0;

    }




    protected void ddl_home_id_2_SelectedIndexChanged(object sender, EventArgs e)
    {

        //  if (!Page.IsPostBack)
        //  { 
        txt_message_2.InnerHtml = "";
        r_pendingaccommodationlist_2.Visible = true;
        ddl_home_id_2.Visible = true;

        txt_pending_2.InnerHtml = "";
        btn_continue_2.Enabled = true;


        hd_home_id_2.Value = Convert.ToString(ddl_home_id_2.SelectedValue);
        tiger.Unit u = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id_2.SelectedValue));
        //change list of unit id

        if (unit_id > 0)
        {
            ddl_unit_id_2.Visible = true;
            //first dispose of old ddl_unit
            ddl_unit_id_2.Dispose();
            ddl_unit_id_2.DataSource = u.getUnitComList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id_2.SelectedValue));
            ddl_unit_id_2.DataBind();

            hd_unit_id_2.Value = Convert.ToString(unit_id);



            //get current tenant id
            int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), unit_id);
            //if there is a current tenant in the unit then get name(s)
            if (temp_tenant_id > 0)
            {
                panel_current_tenant_2.Visible = true;

                txt_current_tenant_name_2.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);

                tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                hd_current_tu_id_2.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), unit_id));

                panel_accommodation_update_2.Visible = true;

                // The accommodation for the current tu_id ( current tenant in the unit )

                // here we check the amount of pending accommodation for this tenant unit
                int count = 0;

                tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                count = p.getCountPendingAccommodation(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id_2.Value));


                if (count > 0)
                {
                    btn_continue_2.Enabled = false;
                    txt_pending_2.InnerHtml = "<strong><span style='color: #ff3300'>You already have a pending accommodation ,<br /> please remove pending before creating another accommodation, ( link goes here)</span></strong> ";

                }

                //----- Now we show there is any pending accommodation in this tenant unit
                tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                r_pendingaccommodationlist_2.DataSource = v.getPendingAccommodationList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id_2.Value));

                r_pendingaccommodationlist_2.DataBind();

                ///------------------- 
                ///


                SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                SqlCommand cmd = new SqlCommand("prAccommodationView", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                DateTime the_date = new DateTime();
                the_date = DateTime.Now; // the date in the to drop downlist

                tiger.Date d = new tiger.Date();
                // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
                // and convert it in Datetime
                the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


                //Add the params
                cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id_2.Value);
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;

                try
                {
                    conn.Open();

                    SqlDataReader dr = null;
                    dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                    while (dr.Read() == true)
                    {
                        // lbl_rent_amount.Text = Convert.ToString(Convert.ToInt32(dr["rl_rent_amount"]));
                        //  ddl_ta_rent_paid_every.SelectedValue = dr["rl_rent_paid_every"].ToString();



                        //   lbl_current_ta_date_begin.Text = String.Format("{0:d}", dr["ta_date_begin"]);


                        DateTime la_date = new DateTime();
                        la_date = Convert.ToDateTime(dr["ta_date_begin"]);

                        lbl_current_ta_date_begin2.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();
                        lbl_current_ta_date_begin3.Text = lbl_current_ta_date_begin2.Text;


                        DateTime new_date_begin = new DateTime();
                        new_date_begin = Convert.ToDateTime(dr["ta_date_begin"]);

                        hd_min_begin_date_m2.Value = new_date_begin.Month.ToString();
                        hd_min_begin_date_d2.Value = new_date_begin.Day.ToString();
                        hd_min_begin_date_y2.Value = new_date_begin.Year.ToString();


                        if (Convert.ToInt32(dr["ta_electricity_inc"]) == 1)
                            ta_electricity_inc_2.Checked = true;

                        else
                            ta_electricity_inc_2.Checked = false;

                        if (Convert.ToInt32(dr["ta_heat_inc"]) == 1)
                            ta_heat_inc_2.Checked = true;
                        else
                            ta_heat_inc_2.Checked = false;

                        if (Convert.ToInt32(dr["ta_water_inc"]) == 1)
                            ta_water_inc_2.Checked = true;
                        else
                            ta_water_inc_2.Checked = false;

                        if (Convert.ToInt32(dr["ta_water_tax_inc"]) == 1)
                            ta_water_tax_inc_2.Checked = true;
                        else
                            ta_water_tax_inc.Checked = false;

                        if (Convert.ToInt32(dr["ta_garage_inc"]) == 1)
                            ta_garage_inc_2.Checked = true;
                        else
                            ta_garage_inc_2.Checked = false;

                        if (Convert.ToInt32(dr["ta_parking_inc"]) == 1)
                            ta_parking_inc_2.Checked = true;
                        else
                            ta_parking_inc_2.Checked = false;

                        if (Convert.ToInt32(dr["ta_semi_furnished_inc"]) == 1)
                            ta_semi_furnished_inc_2.Checked = true;
                        else
                            ta_semi_furnished_inc_2.Checked = false;

                        if (Convert.ToInt32(dr["ta_furnished_inc"]) == 1)
                            ta_furnished_inc_2.Checked = true;
                        else
                            ta_furnished_inc_2.Checked = false;


                        ta_com_2.Text = Convert.ToString(dr["ta_com"]);

                        if (dr["unit_type"].ToString() == "C" && Convert.ToInt32(dr["company_id"]) > 0)
                        {
                            lbl_company.Text = dr["company_name"].ToString();
                        }
                        else
                        {
                            tr_company.Visible = false;
                        }
                    }



                }


                finally
                {

                    conn.Close();
                }



            }
            else
            {
                txt_message_2.InnerHtml = "This is unit is not rented";
                r_pendingaccommodationlist_2.Visible = false;
                panel_accommodation_update_2.Visible = false;
                panel_current_tenant_2.Visible = false;
                hd_current_tu_id_2.Value = "0";
            }
            //hidden fields

            //   hd_unit_id.Value = Convert.ToString(ddl_unit_id.SelectedValue);
        }
        else
        {
            txt_message_2.InnerHtml = "This is no unit in this property";
            r_pendingaccommodationlist_2.Visible = false;
            panel_accommodation_update_2.Visible = false;
            ddl_unit_id_2.Visible = false;
            //  txt_link.InnerHtml = "<a href='unit_add.aspx?home_id='>Add Unit</a>";
            hd_current_tu_id_2.Value = "0";
            hd_unit_id_2.Value = "0";

        }

        //TabContainer1.ActiveTabIndex = 1;

    }



//---------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_unit_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        // label_added_unit.Text = ddl_unit_id.SelectedItem.Text;
        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        //get current tenant
        //get current tenant id
        //get current tenant id

        txt_message.InnerHtml = "";
        r_pendingaccommodationlist.Visible = true;

        txt_pending.InnerHtml = "";
        btn_continue.Enabled = true;

        hd_unit_id.Value = Convert.ToString(ddl_unit_id.SelectedValue);


        int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_unit_id.SelectedValue));
        //if there is a current tenant in the unit then get name(s)
        if (temp_tenant_id > 0)
        {
            panel_current_tenant.Visible = true;
            txt_current_tenant_name.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);


            //tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            hd_current_tu_id.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), Convert.ToInt32(ddl_unit_id.SelectedValue)));

            panel_accommodation_update.Visible = true;

            // here we check the amount of pending accommodation for this tenant unit
            int count = 0;

            tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            count = p.getCountPendingAccommodation(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id.Value));


            if (count > 0)
            {
                btn_continue.Enabled = false;
                txt_pending.InnerHtml = "<strong><span style='color: #ff3300'>You already have a pending accommodation ,<br /> please remove pending before creating another accommodation, ( link goes here)</span></strong> ";

            }

            //----- Now we show there is any pending accommodation in this tenant unit
            tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            r_pendingaccommodationlist.DataSource = v.getPendingAccommodationList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id.Value));

            r_pendingaccommodationlist.DataBind();

            ///------------------- 
            ///


            // The accommodation for the current tu_id ( current tenant in the unit )

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prAccommodationView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            DateTime the_date = new DateTime();
            the_date = DateTime.Now; // the date in the to drop downlist

            tiger.Date d = new tiger.Date();
            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


            //Add the params
            cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id.Value);
            cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;


            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                   // lbl_rent_amount.Text = Convert.ToString(Convert.ToInt32(dr["rl_rent_amount"]));
                  //  ddl_ta_rent_paid_every.SelectedValue = dr["rl_rent_paid_every"].ToString();

                   // lbl_current_ta_date_begin.Text = dr["ta_date_begin"].ToString();


                   // lbl_current_ta_date_begin.Text = String.Format("{0:d}", dr["ta_date_begin"]);

                    DateTime la_date = new DateTime();
                    la_date = Convert.ToDateTime(dr["ta_date_begin"]);

                    lbl_current_ta_date_begin.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();


                    lbl_current_ta_date_begin1.Text = lbl_current_ta_date_begin.Text;


                    DateTime new_date_begin = new DateTime();
                    new_date_begin = Convert.ToDateTime(dr["ta_date_begin"]);

                    hd_min_begin_date_m.Value = new_date_begin.Month.ToString();
                    hd_min_begin_date_d.Value = new_date_begin.Day.ToString();
                    hd_min_begin_date_y.Value = new_date_begin.Year.ToString();



                    if (Convert.ToInt32(dr["ta_electricity_inc"]) == 1)
                        ta_electricity_inc.Checked = true;
                    else
                        ta_electricity_inc.Checked = false;

                    if (Convert.ToInt32(dr["ta_heat_inc"]) == 1)
                        ta_heat_inc.Checked = true;
                    else
                        ta_heat_inc.Checked = false;

                    if (Convert.ToInt32(dr["ta_water_inc"]) == 1)
                        ta_water_inc.Checked = true;
                    else
                        ta_water_inc.Checked = false;

                    if (Convert.ToInt32(dr["ta_water_tax_inc"]) == 1)
                        ta_water_tax_inc.Checked = true;
                    else
                        ta_water_tax_inc.Checked = false;

                    if (Convert.ToInt32(dr["ta_garage_inc"]) == 1)
                        ta_garage_inc.Checked = true;
                    else
                        ta_garage_inc.Checked = false;

                    if (Convert.ToInt32(dr["ta_parking_inc"]) == 1)
                        ta_parking_inc.Checked = true;
                    else
                        ta_parking_inc.Checked = false;

                    if (Convert.ToInt32(dr["ta_semi_furnished_inc"]) == 1)
                        ta_semi_furnished_inc.Checked = true;
                    else
                        ta_semi_furnished_inc.Checked = false;

                    if (Convert.ToInt32(dr["ta_furnished_inc"]) == 1)
                        ta_furnished_inc.Checked = true;
                    else
                        ta_furnished_inc.Checked = false;


                    ta_com.Text = Convert.ToString(dr["ta_com"]);
                }

            }

            finally
            {

                conn.Close();
            }


           // hd_home_id.Value = ddl_home_id.Text;
           // hd_unit_id.Value = ddl_unit_id.Text;

        }
        else
        {
            txt_message.InnerHtml = "This is not rented";
            r_pendingaccommodationlist.Visible = false;
            panel_accommodation_update.Visible = false;
            panel_current_tenant.Visible = false;
            hd_current_tu_id.Value = "0";
        }

        //TabContainer1.ActiveTabIndex = 0;

    }





    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_unit_id_2_SelectedIndexChanged(object sender, EventArgs e)
    {
        // label_added_unit.Text = ddl_unit_id.SelectedItem.Text;
        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        //get current tenant
        //get current tenant id
        //get current tenant id

        txt_message_2.InnerHtml = "";
        r_pendingaccommodationlist_2.Visible = true;

        txt_pending_2.InnerHtml = "";
        btn_continue_2.Enabled = true;

        hd_unit_id_2.Value = Convert.ToString(ddl_unit_id_2.SelectedValue);


        int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_unit_id_2.SelectedValue));
        //if there is a current tenant in the unit then get name(s)
        if (temp_tenant_id > 0)
        {
            panel_current_tenant_2.Visible = true;
            txt_current_tenant_name_2.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);


            //tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            hd_current_tu_id_2.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), Convert.ToInt32(ddl_unit_id_2.SelectedValue)));

            panel_accommodation_update_2.Visible = true;

            // here we check the amount of pending accommodation for this tenant unit
            int count = 0;

            tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            count = p.getCountPendingAccommodation(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id_2.Value));


            if (count > 0)
            {
                btn_continue_2.Enabled = false;
                txt_pending_2.InnerHtml = "<strong><span style='color: #ff3300'>You already have a pending accommodation ,<br /> please remove pending before creating another accommodation, ( link goes here)</span></strong> ";

            }

            //----- Now we show there is any pending accommodation in this tenant unit
            tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            r_pendingaccommodationlist_2.DataSource = v.getPendingAccommodationList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id_2.Value));
            r_pendingaccommodationlist_2.DataBind();

            ///------------------- 
            ///


            // The accommodation for the current tu_id ( current tenant in the unit )

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prAccommodationView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            DateTime the_date = new DateTime();
            the_date = DateTime.Now; // the date in the to drop downlist

            tiger.Date d = new tiger.Date();
            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


            //Add the params
            cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id_2.Value);
            cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;


            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                    // lbl_rent_amount.Text = Convert.ToString(Convert.ToInt32(dr["rl_rent_amount"]));
                    //  ddl_ta_rent_paid_every.SelectedValue = dr["rl_rent_paid_every"].ToString();

                    // lbl_current_ta_date_begin.Text = dr["ta_date_begin"].ToString();


                    // lbl_current_ta_date_begin.Text = String.Format("{0:d}", dr["ta_date_begin"]);

                    DateTime la_date = new DateTime();
                    la_date = Convert.ToDateTime(dr["ta_date_begin"]);

                    lbl_current_ta_date_begin2.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();
                    lbl_current_ta_date_begin3.Text = lbl_current_ta_date_begin2.Text;


                    DateTime new_date_begin = new DateTime();
                    new_date_begin = Convert.ToDateTime(dr["ta_date_begin"]);

                    hd_min_begin_date_m2.Value = new_date_begin.Month.ToString();
                    hd_min_begin_date_d2.Value = new_date_begin.Day.ToString();
                    hd_min_begin_date_y2.Value = new_date_begin.Year.ToString();



                    if (Convert.ToInt32(dr["ta_electricity_inc"]) == 1)
                        ta_electricity_inc_2.Checked = true;
                    else
                        ta_electricity_inc_2.Checked = false;

                    if (Convert.ToInt32(dr["ta_heat_inc"]) == 1)
                        ta_heat_inc_2.Checked = true;
                    else
                        ta_heat_inc_2.Checked = false;

                    if (Convert.ToInt32(dr["ta_water_inc"]) == 1)
                        ta_water_inc_2.Checked = true;
                    else
                        ta_water_inc_2.Checked = false;

                    if (Convert.ToInt32(dr["ta_water_tax_inc"]) == 1)
                        ta_water_tax_inc_2.Checked = true;
                    else
                        ta_water_tax_inc_2.Checked = false;

                    if (Convert.ToInt32(dr["ta_garage_inc"]) == 1)
                        ta_garage_inc_2.Checked = true;
                    else
                        ta_garage_inc_2.Checked = false;

                    if (Convert.ToInt32(dr["ta_parking_inc"]) == 1)
                        ta_parking_inc_2.Checked = true;
                    else
                        ta_parking_inc_2.Checked = false;

                    if (Convert.ToInt32(dr["ta_semi_furnished_inc"]) == 1)
                        ta_semi_furnished_inc_2.Checked = true;
                    else
                        ta_semi_furnished_inc_2.Checked = false;

                    if (Convert.ToInt32(dr["ta_furnished_inc"]) == 1)
                        ta_furnished_inc_2.Checked = true;
                    else
                        ta_furnished_inc_2.Checked = false;


                    ta_com_2.Text = Convert.ToString(dr["ta_com"]);

                    if (dr["unit_type"].ToString() == "C" && Convert.ToInt32(dr["company_id"]) > 0)
                    {
                        lbl_company.Text = dr["company_name"].ToString();
                    }
                    else
                    {
                        tr_company.Visible = false;
                    }
                }

            }

            finally
            {

                conn.Close();
            }


            // hd_home_id.Value = ddl_home_id.Text;
            // hd_unit_id.Value = ddl_unit_id.Text;

        }
        else
        {
            txt_message_2.InnerHtml = "This is not rented";
            r_pendingaccommodationlist_2.Visible = false;
            panel_accommodation_update_2.Visible = false;
            panel_current_tenant_2.Visible = false;
            hd_current_tu_id_2.Value = "0";
        }

        //TabContainer1.ActiveTabIndex = 1;

    }

//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------

    protected void btn_continue_OnClick(object sender, EventArgs e)
    {

        Page.Validate("vg_res");
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }


        DateTime min_date_begin = new DateTime();
        DateTime date_begin = new DateTime();

        tiger.Date d = new tiger.Date();
        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        min_date_begin = Convert.ToDateTime(d.DateCulture(hd_min_begin_date_m.Value, hd_min_begin_date_d.Value, hd_min_begin_date_y.Value, Convert.ToString(Session["_lastCulture"])));
        date_begin = Convert.ToDateTime(d.DateCulture(ddl_ta_date_begin_m.SelectedValue, ddl_ta_date_begin_d.SelectedValue, ddl_ta_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));




        if (date_begin > min_date_begin)
        {
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prAccommodationUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {

                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id.Value);

                cmd.Parameters.Add("@ta_date_begin", SqlDbType.SmallDateTime).Value = date_begin;
                cmd.Parameters.Add("@current_ta_date_end", SqlDbType.SmallDateTime).Value = date_begin;



                if (ta_electricity_inc.Checked == true)
                    cmd.Parameters.Add("@ta_electricity_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_electricity_inc", SqlDbType.Int).Value = 0;

                if (ta_heat_inc.Checked == true)
                    cmd.Parameters.Add("@ta_heat_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_heat_inc", SqlDbType.Int).Value = 0;

                if (ta_water_inc.Checked == true)
                    cmd.Parameters.Add("@ta_water_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_water_inc", SqlDbType.Int).Value = 0;

                if (ta_water_tax_inc.Checked == true)
                    cmd.Parameters.Add("@ta_water_tax_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_water_tax_inc", SqlDbType.Int).Value = 0;

                if (ta_garage_inc.Checked == true)
                    cmd.Parameters.Add("@ta_garage_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_garage_inc", SqlDbType.Int).Value = 0;


                if (ta_parking_inc.Checked == true)
                    cmd.Parameters.Add("@ta_parking_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_parking_inc", SqlDbType.Int).Value = 0;

                if (ta_semi_furnished_inc.Checked == true)
                    cmd.Parameters.Add("@ta_semi_furnished_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_semi_furnished_inc", SqlDbType.Int).Value = 0;

                if (ta_furnished_inc.Checked == true)
                    cmd.Parameters.Add("@ta_furnished_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_furnished_inc", SqlDbType.Int).Value = 0;

                if (ta_none.Checked == true)
                    cmd.Parameters.Add("@ta_none", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_none", SqlDbType.Int).Value = 0;


                cmd.Parameters.Add("@ta_com", SqlDbType.Text).Value = RegEx.getText(ta_com.Text);



                cmd.ExecuteReader();

            }

            catch { }


        }

        //TabContainer1.ActiveTabIndex = 0;
    }




    protected void btn_continue_2_OnClick(object sender, EventArgs e)
    {

        Page.Validate("vg_com");
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }


        DateTime min_date_begin = new DateTime();
        DateTime date_begin = new DateTime();
        
        tiger.Date d = new tiger.Date();
        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        min_date_begin = Convert.ToDateTime(d.DateCulture(hd_min_begin_date_m2.Value, hd_min_begin_date_d2.Value, hd_min_begin_date_y2.Value, Convert.ToString(Session["_lastCulture"])));
        date_begin = Convert.ToDateTime(d.DateCulture(ddl_ta_date_begin_m2.SelectedValue, ddl_ta_date_begin_d2.SelectedValue, ddl_ta_date_begin_y2.SelectedValue, Convert.ToString(Session["_lastCulture"])));




        if (date_begin > min_date_begin)
        {
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prAccommodationUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

           // try
            {

                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id_2.Value);

                cmd.Parameters.Add("@ta_date_begin", SqlDbType.SmallDateTime).Value = date_begin;
                cmd.Parameters.Add("@current_ta_date_end", SqlDbType.SmallDateTime).Value = date_begin;



                if (ta_electricity_inc_2.Checked == true)
                    cmd.Parameters.Add("@ta_electricity_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_electricity_inc", SqlDbType.Int).Value = 0;

                if (ta_heat_inc_2.Checked == true)
                    cmd.Parameters.Add("@ta_heat_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_heat_inc", SqlDbType.Int).Value = 0;

                if (ta_water_inc_2.Checked == true)
                    cmd.Parameters.Add("@ta_water_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_water_inc", SqlDbType.Int).Value = 0;

                if (ta_water_tax_inc_2.Checked == true)
                    cmd.Parameters.Add("@ta_water_tax_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_water_tax_inc", SqlDbType.Int).Value = 0;

                if (ta_garage_inc_2.Checked == true)
                    cmd.Parameters.Add("@ta_garage_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_garage_inc", SqlDbType.Int).Value = 0;


                if (ta_parking_inc_2.Checked == true)
                    cmd.Parameters.Add("@ta_parking_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_parking_inc", SqlDbType.Int).Value = 0;

                if (ta_semi_furnished_inc_2.Checked == true)
                    cmd.Parameters.Add("@ta_semi_furnished_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_semi_furnished_inc", SqlDbType.Int).Value = 0;

                if (ta_furnished_inc_2.Checked == true)
                    cmd.Parameters.Add("@ta_furnished_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_furnished_inc", SqlDbType.Int).Value = 0;

                if (ta_none_2.Checked == true)
                    cmd.Parameters.Add("@ta_none", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_none", SqlDbType.Int).Value = 0;


                cmd.Parameters.Add("@ta_com", SqlDbType.Text).Value = RegEx.getText(ta_com_2.Text);



                cmd.ExecuteReader();

            }

          //  catch { }


        }

        //TabContainer1.ActiveTabIndex = 1;
    }




//-----------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------


 //------------------   

    protected void ta_none_CheckedChanged(object sender, EventArgs e)
    {
        if (ta_none.Checked == true)
        {
            ta_heat_inc.Checked = false;
            ta_electricity_inc.Checked = false;
            ta_water_inc.Checked = false;
            ta_water_tax_inc.Checked = false;

        }
        //TabContainer1.ActiveTabIndex = 0;

    }


    protected void ta_none_2_CheckedChanged(object sender, EventArgs e)
    {
        if (ta_none_2.Checked == true)
        {
            ta_heat_inc_2.Checked = false;
            ta_electricity_inc_2.Checked = false;
            ta_water_inc_2.Checked = false;
            ta_water_tax_inc_2.Checked = false;

        }
        //TabContainer1.ActiveTabIndex = 1;

    }

    //---------------------------------

    protected void ta_electricity_inc_CheckedChanged(object sender, EventArgs e)
    {
        if (ta_electricity_inc.Checked == true)
        {
            ta_none.Checked = false;
        }
        //TabContainer1.ActiveTabIndex = 0;

    }


    protected void ta_electricity_inc_2_CheckedChanged(object sender, EventArgs e)
    {
        if (ta_electricity_inc_2.Checked == true)
        {
            ta_none_2.Checked = false;
        }
        //TabContainer1.ActiveTabIndex = 1;

    }

    //----------------------------------------------------------
    
    protected void ta_heat_inc_CheckedChanged(object sender, EventArgs e)
    {
        if (ta_heat_inc.Checked == true)
        {
            ta_none.Checked = false;
        }
        //TabContainer1.ActiveTabIndex = 0;

    }

    protected void ta_heat_inc_2_CheckedChanged(object sender, EventArgs e)
    {
        if (ta_heat_inc_2.Checked == true)
        {
            ta_none_2.Checked = false;
        }
        //TabContainer1.ActiveTabIndex = 1;

    }

    //----------------------------------------
    protected void ta_water_inc_CheckedChanged(object sender, EventArgs e)
    {
        if (ta_water_inc.Checked == true)
        {
            ta_none.Checked = false;
        }
        //TabContainer1.ActiveTabIndex = 0;


    }

    protected void ta_water_inc_2_CheckedChanged(object sender, EventArgs e)
    {
        if (ta_water_inc_2.Checked == true)
        {
            ta_none_2.Checked = false;
        }
        //TabContainer1.ActiveTabIndex = 1;


    }

    //---------------------------------------------------
    protected void ta_water_tax_inc_CheckedChanged(object sender, EventArgs e)
    {
        if (ta_water_tax_inc.Checked == true)
        {
            ta_none.Checked = false;
        }
        //TabContainer1.ActiveTabIndex = 0;
    }

    protected void ta_water_tax_inc_2_CheckedChanged(object sender, EventArgs e)
    {
        if (ta_water_tax_inc_2.Checked == true)
        {
            ta_none_2.Checked = false;
        }
        //TabContainer1.ActiveTabIndex = 1;
    }
}
