﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;


/// <summary>
/// Done by     : Stanley Jocelyn
/// last modifidication  : janv 2 , 2009
/// 
/// </summary>

public partial class manager_lease_lease_res_add_2 : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            reg_rl_rent_amount.ValidationExpression = RegEx.getMoney();
            reg_ta_com.ValidationExpression = RegEx.getText();
           

            tiger.Country ic = new tiger.Country(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_country_id_1.DataSource = ic.getCountryList();
            ddl_country_id_1.DataBind();


            ddl_guarantor_country_id.DataSource = ddl_country_id_1.DataSource;
            ddl_guarantor_country_id.DataBind();

            //int home_id = 1;// Convert.ToInt32(Request.QueryString["home_id"]);

            tiger.Name n = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_name_id_1.DataSource = n.getNameList(Convert.ToInt32(Session["schema_id"]), 0);
            ddl_name_id_1.DataBind();
            ddl_name_id_1.Items.Insert(0, "Select a name");
            ddl_name_id_1.SelectedIndex = 0;


            ddl_guarantor_name_id.DataSource = ddl_name_id_1.DataSource;
            ddl_guarantor_name_id.DataBind();


            // list of tenant in the lease we are going to create
            rTenantTempoList.Visible = false;
            lbl_tenants.Visible = false;

            tiger.Date d = new tiger.Date();


            hd_home_id.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$ddl_home_id"]);
            hd_unit_id.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$ddl_unit_id"]);

            hd_tu_date_end.Value = d.DateCulture(Request.Form["ctl00$ContentPlaceHolder1$ddl_tu_date_end_m"], Request.Form["ctl00$ContentPlaceHolder1$ddl_tu_date_end_d"], Request.Form["ctl00$ContentPlaceHolder1$ddl_tu_date_end_y"], Convert.ToString(Session["_lastCulture"]));
            hd_tu_date_begin.Value = d.DateCulture(Request.Form["ctl00$ContentPlaceHolder1$ddl_tu_date_begin_m"], Request.Form["ctl00$ContentPlaceHolder1$ddl_tu_date_begin_d"], Request.Form["ctl00$ContentPlaceHolder1$ddl_tu_date_begin_y"], Convert.ToString(Session["_lastCulture"]));

            hd_current_tu_id.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_current_tu_id"]);



            // by default the tenant_id we are going to create is = 0 , 
            if (hd_tenant_id.Value == null || hd_tenant_id.Value == "")
            {
                hd_tenant_id.Value = "0";
            }


            if (hd_tenant_unit_id.Value == null || hd_tenant_unit_id.Value == "")
            {
                hd_tenant_unit_id.Value = "0";
            }

        }

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chb_1_CheckedChanged(object sender, EventArgs e)
    {
        if (chb_1.Checked)
        {
            panel_1.Visible = true;
        }
        else
        {
            panel_1.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chb_name_add_1_CheckedChanged(object sender, EventArgs e)
    {
        if (chb_name_add_1.Checked)
        {
            panel_name_add_1.Visible = true;

        }
        else
        {
            panel_name_add_1.Visible = false;

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_other_Click(object sender, EventArgs e)
    {

        AccountObjectAuthorization tenantTempoAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!tenantTempoAuthorization.TenantTempo(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_tenant_id.Value)) && Convert.ToInt32(hd_tenant_id.Value) != 0)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////



        int new_tenant_id = 0;
        int new_tenant_unit_id = 0;
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prTenantAddTempo", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return_new_tenant_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = Convert.ToInt32(hd_tenant_id.Value);



            if (chb_1.Checked == true && ddl_name_id_1.SelectedIndex > 0)
                cmd.Parameters.Add("@name_id_1", SqlDbType.Int).Value = Convert.ToInt32(ddl_name_id_1.SelectedValue);
            else
                cmd.Parameters.Add("@name_id_1", SqlDbType.Int).Value = 0;

            // new tenants paramater - new person add to the database   
            //name 1
            cmd.Parameters.Add("@name_lname_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_lname_1.Text);
            cmd.Parameters.Add("@name_fname_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_fname_1.Text);


            cmd.Parameters.Add("@name_addr_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_1.Text);
            cmd.Parameters.Add("@name_addr_city_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_city_1.Text);
            cmd.Parameters.Add("@name_addr_pc_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_pc_1.Text);
            cmd.Parameters.Add("@name_addr_state_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_state_1.Text);

            if (chb_name_add_1.Checked == false)
                cmd.Parameters.Add("@country_id_1", SqlDbType.Int).Value = System.DBNull.Value;
            else
                cmd.Parameters.Add("@country_id_1", SqlDbType.Int).Value = Convert.ToInt32(ddl_country_id_1.SelectedValue);

            cmd.Parameters.Add("@name_tel_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_tel_1.Text);
            cmd.Parameters.Add("@name_tel_work_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_tel_work_1.Text);
            cmd.Parameters.Add("@name_tel_work_ext_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_tel_work_ext_1.Text);

            tiger.Date d = new tiger.Date();

            cmd.Parameters.Add("@name_cell_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_cell_1.Text);
            cmd.Parameters.Add("@name_fax_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_fax_1.Text);
            cmd.Parameters.Add("@name_email_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_email_1.Text);
            cmd.Parameters.Add("@name_com_1", SqlDbType.Text).Value = RegEx.getText(name_com_1.Text);

            if (chb_name_add_1.Checked == false)
                cmd.Parameters.Add("@name_dob_1", SqlDbType.DateTime).Value = System.DBNull.Value;
            else
                cmd.Parameters.Add("@name_dob_1", SqlDbType.DateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_name_1_dob_m.SelectedValue, ddl_name_1_dob_d.SelectedValue, ddl_name_1_dob_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            if (Convert.ToInt32(cmd.Parameters["@return_new_tenant_id"].Value) > 0)
            {
                new_tenant_id = Convert.ToInt32(cmd.Parameters["@return_new_tenant_id"].Value);
                hd_tenant_id.Value = Convert.ToString(new_tenant_id);


                // If we have a new tenant_id , display the tenants
                tiger.Tenant g = new tiger.Tenant(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                rTenantTempoList.DataSource = g.getTenantTempoList(Convert.ToInt32(Session["schema_id"]), new_tenant_id);
                rTenantTempoList.DataBind();
                rTenantTempoList.Visible = true;
                lbl_tenants.Visible = true;

            }

        }
        catch (Exception error)
        {
            //   tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }

        conn.Close();

        //  Server.Transfer("lease_add_2_tmp.aspx",true);   
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
 /*   protected void btn_continue_Click(object sender, EventArgs e)
    {


        AccountObjectAuthorization tenantTempoAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!tenantTempoAuthorization.TenantTempo(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_tenant_id.Value)) && Convert.ToInt32(hd_tenant_id.Value) != 0)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////


        int new_tenant_id = 0;
        int new_tenant_unit_id = 0;
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prTenantAddTempo", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return_new_tenant_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = Convert.ToInt32(hd_tenant_id.Value);



            if (chb_1.Checked == true && ddl_name_id_1.SelectedIndex > 0)
                cmd.Parameters.Add("@name_id_1", SqlDbType.Int).Value = Convert.ToInt32(ddl_name_id_1.SelectedValue);
            else
                cmd.Parameters.Add("@name_id_1", SqlDbType.Int).Value = 0;

            // new tenants paramater - new person add to the database   
            //name 1
            cmd.Parameters.Add("@name_lname_1", SqlDbType.NVarChar, 50).Value = name_lname_1.Text;
            cmd.Parameters.Add("@name_fname_1", SqlDbType.NVarChar, 50).Value = name_fname_1.Text;


            cmd.Parameters.Add("@name_addr_1", SqlDbType.NVarChar, 50).Value = name_addr_1.Text;
            cmd.Parameters.Add("@name_addr_city_1", SqlDbType.NVarChar, 50).Value = name_addr_city_1.Text;
            cmd.Parameters.Add("@name_addr_pc_1", SqlDbType.NVarChar, 50).Value = name_addr_pc_1.Text;
            cmd.Parameters.Add("@name_addr_state_1", SqlDbType.NVarChar, 50).Value = name_addr_state_1.Text;

            if (chb_name_add_1.Checked == false)
                cmd.Parameters.Add("@country_id_1", SqlDbType.Int).Value = System.DBNull.Value;
            else
                cmd.Parameters.Add("@country_id_1", SqlDbType.Int).Value = Convert.ToInt32(ddl_country_id_1.SelectedValue);

            cmd.Parameters.Add("@name_tel_1", SqlDbType.NVarChar, 50).Value = name_tel_1.Text;
            cmd.Parameters.Add("@name_tel_work_1", SqlDbType.NVarChar, 50).Value = name_tel_work_1.Text;
            cmd.Parameters.Add("@name_tel_work_ext_1", SqlDbType.NVarChar, 50).Value = name_tel_work_ext_1.Text;

            tiger.Date d = new tiger.Date();

            cmd.Parameters.Add("@name_cell_1", SqlDbType.NVarChar, 50).Value = name_cell_1.Text;
            cmd.Parameters.Add("@name_fax_1", SqlDbType.NVarChar, 50).Value = name_fax_1.Text;
            cmd.Parameters.Add("@name_email_1", SqlDbType.NVarChar, 50).Value = name_email_1.Text;
            cmd.Parameters.Add("@name_com_1", SqlDbType.Text).Value = name_com_1.Text;

            if (chb_name_add_1.Checked == false)
                cmd.Parameters.Add("@name_dob_1", SqlDbType.DateTime).Value = System.DBNull.Value;
            else
                cmd.Parameters.Add("@name_dob_1", SqlDbType.DateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_name_1_dob_m.SelectedValue, ddl_name_1_dob_d.SelectedValue, ddl_name_1_dob_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            if (Convert.ToInt32(cmd.Parameters["@return_new_tenant_id"].Value) > 0)
            {
                new_tenant_id = Convert.ToInt32(cmd.Parameters["@return_new_tenant_id"].Value);
                hd_tenant_id.Value = Convert.ToString(new_tenant_id);
            }

        }
        catch (Exception error)
        {
            //   tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }

        conn.Close();


        //-----------------------------------------------------------------------------------------------



        //  string strconn2 = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        // SqlConnection conn2 = new SqlConnection(strconn2);
        SqlCommand cmd2 = new SqlCommand("prTenantUnitAddTempo", conn);
        cmd2.CommandType = CommandType.StoredProcedure;


        //  try
        {
            conn.Open();
            //Add the params     @return_new_tenant_unit_id
            cmd2.Parameters.Add("@return_new_tenant_unit_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@new_tenant_id", SqlDbType.Int).Value = Convert.ToInt32(hd_tenant_id.Value);

            // new tenants paramater - new person add to the database   
            //name 1
            cmd2.Parameters.Add("@unit_id", SqlDbType.Int).Value = Convert.ToInt32(hd_unit_id.Value);

            cmd2.Parameters.Add("@tu_date_begin", SqlDbType.DateTime).Value = Convert.ToDateTime(hd_tu_date_begin.Value);
            cmd2.Parameters.Add("@tu_date_end", SqlDbType.DateTime).Value = Convert.ToDateTime(hd_tu_date_end.Value);

            //execute the insert
            cmd2.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            if (Convert.ToInt32(cmd2.Parameters["@return_new_tenant_unit_id"].Value) > 0)
            {
                new_tenant_unit_id = Convert.ToInt32(cmd2.Parameters["@return_new_tenant_unit_id"].Value);
                hd_tenant_unit_id.Value = Convert.ToString(new_tenant_unit_id);
            }

        }
        // catch (Exception error)
        {
            //  tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }

        //-----------------------------------------------------------------------------------------------
        Response.Redirect("lease_add_3_tmp.aspx?tu_id=" + new_tenant_unit_id + "&tenant_id=" + new_tenant_id);
        conn.Close();



    }
  */  

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_previous_1_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_next_1_Click(object sender, EventArgs e)
    {
        if (rTenantTempoList.Items.Count > 0)       
            MultiView1.ActiveViewIndex = 1; 
       
    }

  
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_previous_2_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_next_2_Click(object sender, EventArgs e)
    {
        Page.Validate("vg_acc");
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        else
            MultiView1.ActiveViewIndex = 2;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ta_electricity_inc_CheckedChanged(object sender, EventArgs e)
    {
        if (ta_electricity_inc.Checked == true)
        {
            ta_none.Checked = false;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ta_heat_inc_CheckedChanged(object sender, EventArgs e)
    {
        if (ta_heat_inc.Checked == true)
        {
            ta_none.Checked = false;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ta_water_inc_CheckedChanged(object sender, EventArgs e)
    {
        if (ta_water_inc.Checked == true)
        {
            ta_none.Checked = false;
        }


    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ta_water_tax_inc_CheckedChanged(object sender, EventArgs e)
    {
        if (ta_water_tax_inc.Checked == true)
        {
            ta_none.Checked = false;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ta_none_CheckedChanged(object sender, EventArgs e)
    {
        if (ta_none.Checked == true)
        {
            ta_heat_inc.Checked = false;
            ta_electricity_inc.Checked = false;
            ta_water_inc.Checked = false;
            ta_water_tax_inc.Checked = false;

        }

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void tt_security_deposit_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(tt_security_deposit.SelectedValue) == "1")
            Panel_security_deposit.Visible = true;
        else
            Panel_security_deposit.Visible = false;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void tt_guarantor_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(tt_guarantor.SelectedValue) == "1")
            panel_guarantor.Visible = true;
        else
            panel_guarantor.Visible = false;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_guarantor_name_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(ddl_guarantor_name_id.SelectedIndex) > 0)
        {
            guarantor_fname.Enabled = false;
            guarantor_lname.Enabled = false;
            guarantor_addr.Enabled = false;
            guarantor_addr_city.Enabled = false;
            guarantor_addr_pc.Enabled = false;
            guarantor_addr_state.Enabled = false;
            ddl_guarantor_country_id.Enabled = false;
            guarantor_tel.Enabled = false;
            guarantor_tel_work.Enabled = false;
            guarantor_tel_work_ext.Enabled = false;
            guarantor_cell.Enabled = false;
            guarantor_fax.Enabled = false;
            guarantor_email.Enabled = false;
            guarantor_com.Enabled = false;
            txt.Visible = true;
        }
        else
        {
            guarantor_fname.Enabled = true;
            guarantor_lname.Enabled = true;
            guarantor_addr.Enabled = true;
            guarantor_addr_city.Enabled = true;
            guarantor_addr_pc.Enabled = true;
            guarantor_addr_state.Enabled = true;
            ddl_guarantor_country_id.Enabled = true;
            guarantor_tel.Enabled = true;
            guarantor_tel_work.Enabled = true;
            guarantor_tel_work_ext.Enabled = true;
            guarantor_cell.Enabled = true;
            guarantor_fax.Enabled = true;
            guarantor_email.Enabled = true;
            guarantor_com.Enabled = true;
            txt.Visible = false;

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_Click(object sender, EventArgs e)
    {

        if (Convert.ToInt32(ddl_guarantor_name_id.SelectedIndex) > 0)
        {
            guarantor_fname.Enabled = false;
            guarantor_lname.Enabled = false;
            guarantor_addr.Enabled = false;
            guarantor_addr_city.Enabled = false;
            guarantor_addr_pc.Enabled = false;
            guarantor_addr_state.Enabled = false;
            ddl_guarantor_country_id.Enabled = false;
            guarantor_tel.Enabled = false;
            guarantor_tel_work.Enabled = false;
            guarantor_tel_work_ext.Enabled = false;
            guarantor_cell.Enabled = false;
            guarantor_fax.Enabled = false;
            guarantor_email.Enabled = false;
            guarantor_com.Enabled = false;
            txt.Visible = true;
        }
        else
        {
            guarantor_fname.Enabled = true;
            guarantor_lname.Enabled = true;
            guarantor_addr.Enabled = true;
            guarantor_addr_city.Enabled = true;
            guarantor_addr_pc.Enabled = true;
            guarantor_addr_state.Enabled = true;
            ddl_guarantor_country_id.Enabled = true;
            guarantor_tel.Enabled = true;
            guarantor_tel_work.Enabled = true;
            guarantor_tel_work_ext.Enabled = true;
            guarantor_cell.Enabled = true;
            guarantor_fax.Enabled = true;
            guarantor_email.Enabled = true;
            guarantor_com.Enabled = true;
            txt.Visible = false;

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_lease_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prCreateResLease", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();

            // new tenant parameter - tenant from the database
            cmd.Parameters.Add("@return_success", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.NVarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(hd_home_id.Value);
            cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = Convert.ToInt32(hd_unit_id.Value);
            cmd.Parameters.Add("@current_tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id.Value);


            cmd.Parameters.Add("@tu_date_begin", SqlDbType.DateTime).Value = Convert.ToDateTime(hd_tu_date_begin.Value);

            if (Convert.ToString(hd_tu_date_end.Value) == "//")
                cmd.Parameters.Add("@tu_date_end", SqlDbType.DateTime).Value = Convert.ToDateTime("1/1/1900");
            else
                cmd.Parameters.Add("@tu_date_end", SqlDbType.DateTime).Value = Convert.ToDateTime(hd_tu_date_end.Value);


            cmd.Parameters.Add("@tempo_tenant_id", SqlDbType.Int).Value = Convert.ToInt32(hd_tenant_id.Value);


            //------------------------------------------------------------

            if (ddl_guarantor_name_id.SelectedIndex == 0)
                cmd.Parameters.Add("@guarantor_name_id", SqlDbType.Int).Value = 0;
            else
                cmd.Parameters.Add("@guarantor_name_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_guarantor_name_id.SelectedValue);


            // if guarantor is not in the system we create him
            cmd.Parameters.Add("@guarantor_name_fname", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_fname.Text);
            cmd.Parameters.Add("@guarantor_name_lname", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_lname.Text);
            cmd.Parameters.Add("@guarantor_name_addr", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_addr.Text);
            cmd.Parameters.Add("@guarantor_name_addr_city", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_addr_city.Text);
            cmd.Parameters.Add("@guarantor_name_addr_pc", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_addr_pc.Text);
            cmd.Parameters.Add("@guarantor_name_addr_state", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_addr_state.Text);
            cmd.Parameters.Add("@guarantor_country_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_guarantor_country_id.SelectedValue);
            cmd.Parameters.Add("@guarantor_name_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_tel.Text);
            cmd.Parameters.Add("@guarantor_name_tel_work", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_tel_work.Text);
            cmd.Parameters.Add("@guarantor_name_tel_work_ext", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_tel_work_ext.Text);
            cmd.Parameters.Add("@guarantor_name_cell", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_cell.Text);
            cmd.Parameters.Add("@guarantor_name_fax", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_fax.Text);
            cmd.Parameters.Add("@guarantor_name_email", SqlDbType.NVarChar, 50).Value = RegEx.getText(guarantor_email.Text);
            cmd.Parameters.Add("@guarantor_name_com", SqlDbType.Text).Value = RegEx.getText(guarantor_com.Text);

            // Rent amount and accomodations

            cmd.Parameters.Add("@rl_rent_amount", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(rl_rent_amount.Text));
            cmd.Parameters.Add("@rl_rent_paid_every", SqlDbType.Int).Value = Convert.ToInt32(ddl_rl_rent_paid_every.SelectedValue);



            if (ta_none.Checked)
            {
                cmd.Parameters.Add("@ta_electricity_inc", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ta_heat_inc", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ta_water_inc", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ta_water_tax_inc", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@ta_none", SqlDbType.Int).Value = 1;
            }
            else
            {
                if (ta_electricity_inc.Checked)
                    cmd.Parameters.Add("@ta_electricity_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_electricity_inc", SqlDbType.Int).Value = 0;




                if (ta_heat_inc.Checked)
                    cmd.Parameters.Add("@ta_heat_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_heat_inc", SqlDbType.Int).Value = 0;





                if (ta_water_inc.Checked)
                    cmd.Parameters.Add("@ta_water_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_water_inc", SqlDbType.Int).Value = 0;


                if (ta_water_tax_inc.Checked)
                    cmd.Parameters.Add("@ta_water_tax_inc", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@ta_water_tax_inc", SqlDbType.Int).Value = 0;

                cmd.Parameters.Add("@ta_none", SqlDbType.Int).Value = 0;
            }



            if (ta_parking_inc.Checked)
                cmd.Parameters.Add("@ta_parking_inc", SqlDbType.Int).Value = 1;
            else
                cmd.Parameters.Add("@ta_parking_inc", SqlDbType.Int).Value = 0;





            if (ta_garage_inc.Checked)
                cmd.Parameters.Add("@ta_garage_inc", SqlDbType.Int).Value = 1;
            else
                cmd.Parameters.Add("@ta_garage_inc", SqlDbType.Int).Value = 0;




            if (ta_furnished_inc.Checked)
                cmd.Parameters.Add("@ta_furnished_inc", SqlDbType.Int).Value = 1;
            else
                cmd.Parameters.Add("@ta_furnished_inc", SqlDbType.Int).Value = 0;



            if (ta_semi_furnished_inc.Checked)
                cmd.Parameters.Add("@ta_semi_furnished_inc", SqlDbType.Int).Value = 1;
            else
                cmd.Parameters.Add("@ta_semi_furnished_inc", SqlDbType.Int).Value = 0;


            cmd.Parameters.Add("@ta_com", SqlDbType.Text).Value = ta_com.Text;


            // Terms and Conditions parameters

            if (Convert.ToString(tt_security_deposit.SelectedValue) == "1")
                cmd.Parameters.Add("@tt_security_deposit", SqlDbType.Int).Value = 1;
            else
                cmd.Parameters.Add("@tt_security_deposit", SqlDbType.Int).Value = 0;


            if (Convert.ToString(tt_security_deposit.SelectedValue) == "1")
                cmd.Parameters.Add("@tt_security_deposit_amount", SqlDbType.SmallMoney).Value = Convert.ToDecimal(RegEx.getMoney(security_deposit_amount.Text));
            else
                cmd.Parameters.Add("@tt_security_deposit_amount", SqlDbType.SmallMoney).Value = 0;


            // if (tt_guarantor.SelectedIndex == 0)
            //    cmd.Parameters.Add("@tt_guarantor", SqlDbType.Int).Value = 1;
            if (Convert.ToString(tt_guarantor.SelectedValue) == "1")
                cmd.Parameters.Add("@tt_guarantor", SqlDbType.Int).Value = 1;
            else
                cmd.Parameters.Add("@tt_guarantor", SqlDbType.Int).Value = 0;


            if (Convert.ToString(tt_pets.SelectedValue) == "1")
                cmd.Parameters.Add("@tt_pets", SqlDbType.Int).Value = 1;
            else
                cmd.Parameters.Add("@tt_pets", SqlDbType.Int).Value = 0;


            if (Convert.ToString(tt_maintenance.SelectedValue) == "1")
                cmd.Parameters.Add("@tt_maintenance", SqlDbType.Int).Value = 1;
            else
                cmd.Parameters.Add("@tt_maintenance", SqlDbType.Int).Value = 0;

            cmd.Parameters.Add("@tt_specify_maintenance", SqlDbType.Text).Value = RegEx.getText(tt_specify_maintenance.Text);


            if (Convert.ToString(tt_improvement.SelectedValue) == "1")
                cmd.Parameters.Add("@tt_improvement", SqlDbType.Int).Value = 1;
            else
                cmd.Parameters.Add("@tt_improvement", SqlDbType.Int).Value = 0;

            cmd.Parameters.Add("@tt_specify_improvement", SqlDbType.Text).Value = RegEx.getText(tt_specify_improvement.Text);


            if (Convert.ToString(tt_notice_to_enter.SelectedValue) == "1")
                cmd.Parameters.Add("@tt_notice_to_enter", SqlDbType.Int).Value = 1;
            else
                cmd.Parameters.Add("@tt_notice_to_enter", SqlDbType.Int).Value = 0;

            if (tt_specify_number_of_hours.Text == "")
                cmd.Parameters.Add("@tt_specify_number_of_hours", SqlDbType.Int).Value = 0;
            else
                cmd.Parameters.Add("@tt_specify_number_of_hours", SqlDbType.Int).Value = Convert.ToInt32(RegEx.getInteger(tt_specify_number_of_hours.Text));

            if (Convert.ToString(ddl_tt_tenant_content_ins.SelectedValue) == "0")
                cmd.Parameters.Add("@tt_tenant_content_ins", SqlDbType.Int).Value = 0;

            if (Convert.ToString(ddl_tt_tenant_content_ins.SelectedValue) == "1")
                cmd.Parameters.Add("@tt_tenant_content_ins", SqlDbType.Int).Value = 1;

            if (Convert.ToString(ddl_tt_tenant_content_ins.SelectedValue) == "2")
                cmd.Parameters.Add("@tt_tenant_content_ins", SqlDbType.Int).Value = 2;




            if (Convert.ToString(ddl_tt_landlord_content_ins.SelectedValue) == "0")
                cmd.Parameters.Add("@tt_landlord_content_ins", SqlDbType.Int).Value = 0;
            if (Convert.ToString(ddl_tt_landlord_content_ins.SelectedValue) == "1")
                cmd.Parameters.Add("@tt_landlord_content_ins", SqlDbType.Int).Value = 1;
            if (Convert.ToString(ddl_tt_landlord_content_ins.SelectedValue) == "2")
                cmd.Parameters.Add("@tt_landlord_content_ins", SqlDbType.Int).Value = 2;



            if (Convert.ToString(ddl_tt_injury_ins.SelectedValue) == "0")
                cmd.Parameters.Add("@tt_injury_ins", SqlDbType.Int).Value = 0;
            if (Convert.ToString(ddl_tt_injury_ins.SelectedValue) == "1")
                cmd.Parameters.Add("@tt_injury_ins", SqlDbType.Int).Value = 1;
            if (Convert.ToString(ddl_tt_injury_ins.SelectedValue) == "2")
                cmd.Parameters.Add("@tt_injury_ins", SqlDbType.Int).Value = 2;



            if (Convert.ToString(ddl_tt_premises_ins.SelectedValue) == "0")
                cmd.Parameters.Add("@tt_premises_ins", SqlDbType.Int).Value = 0;
            if (Convert.ToString(ddl_tt_premises_ins.SelectedValue) == "1")
                cmd.Parameters.Add("@tt_premises_ins", SqlDbType.Int).Value = 1;
            if (Convert.ToString(ddl_tt_premises_ins.SelectedValue) == "2")
                cmd.Parameters.Add("@tt_premises_ins", SqlDbType.Int).Value = 2;



            cmd.Parameters.Add("@tt_additional_terms", SqlDbType.Text).Value = RegEx.getText(tt_additional_terms.Text);

            cmd.Parameters.Add("@tt_nsf", SqlDbType.SmallMoney).Value = Convert.ToDecimal(RegEx.getMoney(tt_nsf.Text));
            cmd.Parameters.Add("@tt_form_of_payment", SqlDbType.Int).Value = Convert.ToInt32(ddl_tt_form_of_payment.SelectedValue);

            cmd.ExecuteReader();
        }

        finally
        {
            conn.Close();
        }

        btn_submit.Enabled = false;

    }
    protected void btn_previous_3_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;
    }
}
