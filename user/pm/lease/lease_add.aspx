﻿<%@ Page Language="C#" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" CodeFile="lease_add.aspx.cs" Inherits="manager_home_lease_add" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<br />
    <br /> 
     <span style="font-size: small">Do you want to create a new or an old lease</span>
    <br />
    <br />
    <asp:RadioButtonList ID="radio_lease_time" runat="server">
        <asp:ListItem Text="<%$ Resources:Resource, lbl_new_lease %>" Selected="True" Value="1"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:Resource, lbl_old_lease %>" Value="2"></asp:ListItem>
    </asp:RadioButtonList>
    
    <br />
    <br />
    <asp:Label ID="lbl_lease_type" runat="server" 
        Text="<%$ Resources:Resource, lbl_lease_lease_type %>" 
        style="font-size: x-small"></asp:Label>
    <br />
    <br /><br />
    <asp:RadioButtonList ID="radio_lease_type" runat="server">
        <asp:ListItem Text="<%$ Resources:Resource, lbl_lease_res %>" Selected="True" Value="1"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:Resource, lbl_lease_com %>" Value="2"></asp:ListItem>
    </asp:RadioButtonList>
    <br />
        <table bgcolor="aliceblue" style="width: 80%;">
            <tr>
                <td align="right">
                    <asp:Button ID="btn_next" runat="server" onclick="btn_next_Click" 
                        Text="<%$ Resources:Resource,lbl_next %>" />
                </td>
            </tr>
        </table>
        <br />
    
    
    
</asp:Content>

