﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.DataVisualization;
using sinfoca.tiger.security.NameObjectAuthorization;

/// <summary>
/// Created by : Stanley Jocelyn
/// date       : May 21, 2009
/// </summary>
public partial class manager_home_lease_list : BasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((!Page.IsPostBack))
        {
            string referrer = "";

            SetDefaultView();


            //  Label2.Text = referer;
            // if value = 0 form was not submit
            h_btn_submit.Value = "0";


            DateTime test_date = new DateTime();
            test_date = DateTime.Now;


            String the_date = "";
            DateTime right_now = new DateTime();
            right_now = DateTime.Now;

            tiger.Date d = new tiger.Date();

            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


            tiger.PM l = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getPMHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

            // first we check if there's any property available
            if (home_count > 0)
            {
                int home_id = l.getPMHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));


                ddl_home_list.DataSource = l.getPMHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                ddl_home_list2.DataSource = ddl_home_list.DataSource;
                ddl_home_pending_list.DataSource = ddl_home_list2.DataSource;


                ddl_home_list.DataBind();
                ddl_home_list2.DataBind();
                ddl_home_pending_list.DataBind();
                ddl_home_pending_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));



                // DropDownList pour les Unit
                //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

                if (unit_count > 0)
                {

                    int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), home_id);

                    ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
                    ddl_unit_id2.DataSource = ddl_unit_id.DataSource;

                    ddl_unit_id.DataBind();
                    ddl_unit_id2.DataBind();

                    ddl_unit_id.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));

                    ddl_unit_id.SelectedIndex = 0;

                    ddl_unit_id2.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
                    ddl_unit_id2.SelectedIndex = 0;

                    ///////////////////////////////////////////////////////////////////////////////////////////
                    ///////////////////////////////////////////////////////////////////////////////////////////
                    //////////////////////////////////////////////////////////////////////////////////////////


                    if (referrer == "/sinfo/manager/alerts/alerts.aspx" || referrer == "/manager/alerts/alerts.aspx")
                    {
                        if (!RegEx.IsInteger(Request.QueryString["home_id"]))
                        {
                            Session.Abandon();
                            Response.Redirect("~/login.aspx");
                        }

                        NameObjectAuthorization homeAuthorization = new NameObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                        if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["home_id"]), Convert.ToInt32(Session["name_id"]), 4))
                        {
                            Session.Abandon();
                            Response.Redirect("~/login.aspx");
                        }
                        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////



                        home_id = Convert.ToInt32(Request.QueryString["home_id"]);
                        ddl_home_pending_list.SelectedValue = home_id.ToString();
                        //TabContainer1.ActiveTabIndex = 1;
                    }

                    SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    SqlCommand cmd1 = new SqlCommand("prLeasePendingList", conn);
                    SqlCommand cmd2 = new SqlCommand("prLeasePendingList", conn);
                    SqlCommand cmd3 = new SqlCommand("prLeasePendingList", conn);
                    SqlCommand cmd4 = new SqlCommand("prLeasePendingList", conn);

                    cmd1.CommandType = CommandType.StoredProcedure;

                    try
                    {

                        conn.Open();

                        cmd1.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                        cmd1.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                        cmd1.Parameters.Add("@time_frame", SqlDbType.Int).Value = 1;


                        SqlDataAdapter da = new SqlDataAdapter(cmd1);
                        DataTable dt = new DataTable();
                        da.Fill(dt);


                        gv_lease_pending_list1.DataSource = dt;
                        gv_lease_pending_list1.DataBind();
                    }
                    finally
                    {
                        //  conn.Close();
                    }



                    cmd2.CommandType = CommandType.StoredProcedure;

                    try
                    {

                        cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                        cmd2.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                        cmd2.Parameters.Add("@time_frame", SqlDbType.Int).Value = 2;


                        SqlDataAdapter da = new SqlDataAdapter(cmd2);
                        DataTable dt = new DataTable();
                        da.Fill(dt);


                        gv_lease_pending_list2.DataSource = dt;
                        gv_lease_pending_list2.DataBind();
                    }
                    finally
                    {
                        //  conn.Close();
                    }




                    cmd3.CommandType = CommandType.StoredProcedure;

                    try
                    {

                        cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                        cmd3.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                        cmd3.Parameters.Add("@time_frame", SqlDbType.Int).Value = 3;


                        SqlDataAdapter da = new SqlDataAdapter(cmd3);
                        DataTable dt = new DataTable();
                        da.Fill(dt);


                        gv_lease_pending_list3.DataSource = dt;
                        gv_lease_pending_list3.DataBind();
                    }
                    finally
                    {
                        //  conn.Close();
                    }


                    cmd4.CommandType = CommandType.StoredProcedure;

                    try
                    {

                        cmd4.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                        cmd4.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                        cmd4.Parameters.Add("@time_frame", SqlDbType.Int).Value = 4;


                        SqlDataAdapter da = new SqlDataAdapter(cmd4);
                        DataTable dt = new DataTable();
                        da.Fill(dt);


                        gv_lease_pending_list4.DataSource = dt;
                        gv_lease_pending_list4.DataBind();
                    }
                    finally
                    {
                        conn.Close();
                    }





                    ///////////////////////////////////////////////////////////////////////////////////////////
                    ///////////////////////////////////////////////////////////////////////////////////////////
                    //////////////////////////////////////////////////////////////////////////////////////////

                }

            }

            else
            {
                // panel_home_unit_add.Visible = false;
                txt_message.InnerHtml = "<b><a href='home/home_add.aspx'>Please add a property</a></b>";
            }

        }
        // if the referer is self i.e : from the respon redirect of the submit button
        //********************************************************************
        /*  if (Request.UrlReferrer.ToString() == Request.Url.ToString())
          {
              Session.Contents.Remove("home_id");
              Session.Contents.Remove("rent_frequency");
              Session.Contents.Remove("date_received_m");
              Session.Contents.Remove("date_received_d");
              Session.Contents.Remove("date_received_y");
          }
       */
        //********************************************************************



    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_list_SelectedIndexChanged(object sender, EventArgs e)
    {

        gv_rented_paid_unit_list.Visible = true;
        gv_rented_paid_unit_list.DataSourceID = "ObjectDataSource1";

        // if value = 0 form was not submit
        h_btn_submit.Value = "0";

        
        int home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        // here we check if there's any unit in this property
        tiger.Unit uc = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        int unit_count = uc.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);


        //   if (unit_count > 0)
        // here we get the list of unit that were already added 
        {
            // repopulating the ddl for the property selected
            ddl_unit_id.Dispose();
            ddl_unit_id.DataSource = uc.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
            // ddl_unit_id.SelectedValue = Convert.ToString(unit_id);
            ddl_unit_id.DataBind();
            ddl_unit_id.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
            ddl_unit_id.SelectedIndex = 0;

        }
        //   else
        {

        }

      
        //TabContainer1.ActiveTabIndex = 0;

    }


    protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        DateTime right_now = new DateTime();
        right_now = DateTime.Now;
        tiger.Date d = new tiger.Date();
        // and convert it in Datetime
        right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));



        if (!Page.IsPostBack)
        {
            tiger.PM l = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getPMHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

            // first we check if there's any property available
            if (home_count > 0)
            {
                int home_id = l.getPMHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

                e.InputParameters["home_id"] = home_id;
                e.InputParameters["unit_id"] = 0;
                e.InputParameters["the_date"] = right_now;
            }


        }

        else
        {
            if (ddl_unit_id.SelectedIndex == 0)
            {
                e.InputParameters["home_id"] = Convert.ToInt32(ddl_home_list.SelectedValue);
                e.InputParameters["unit_id"] = 0;
                e.InputParameters["the_date"] = right_now;
            }

            else
            {
                e.InputParameters["home_id"] = Convert.ToInt32(ddl_home_list.SelectedValue);
                e.InputParameters["unit_id"] = Convert.ToInt32(ddl_unit_id.SelectedValue);
                e.InputParameters["the_date"] = right_now;
            }

        }

        //TabContainer1.ActiveTabIndex = 0;
    }



    protected void ObjectDataSource2_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        DateTime right_now = new DateTime();
        right_now = DateTime.Now;
        tiger.Date d = new tiger.Date();
        // and convert it in Datetime
        right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));

        if (!Page.IsPostBack)
        {
            tiger.PM l = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getPMHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

            // first we check if there's any property available
            if (home_count > 0)
            {
                int home_id = l.getPMHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

                e.InputParameters["home_id"] = home_id;
                e.InputParameters["unit_id"] = 0;
                e.InputParameters["the_date"] = right_now;
            }
        }

        else
        {
            if (ddl_unit_id2.SelectedIndex == 0)
            {
                e.InputParameters["home_id"] = Convert.ToInt32(ddl_home_list2.SelectedValue);
                e.InputParameters["unit_id"] = 0;
                e.InputParameters["the_date"] = right_now;
            }

            else
            {
                e.InputParameters["home_id"] = Convert.ToInt32(ddl_home_list2.SelectedValue);
                e.InputParameters["unit_id"] = Convert.ToInt32(ddl_unit_id2.SelectedValue);
                e.InputParameters["the_date"] = right_now;
            }

            //TabContainer1.ActiveTabIndex = 2;
        }


    }

    protected void ddl_unit_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        DateTime right_now = new DateTime();
        right_now = DateTime.Now;
        tiger.Date d = new tiger.Date();

        //TabContainer1.ActiveTabIndex = 0;

        gv_rented_paid_unit_list.DataSourceID = "ObjectDataSource1";

    }
    /// <summary>
    /// 
    /// </summary>
    private string GridViewSortExpression
    {

        get { return ViewState["SortExpression"] as string ?? string.Empty; }

        set { ViewState["SortExpression"] = value; }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_rented_paid_unit_list_Sorting(object sender, GridViewSortEventArgs e)
    {
        DateTime right_now = new DateTime();
        right_now = DateTime.Now;
        tiger.Date d = new tiger.Date();
    }


    private void SetDefaultView()
    {
        //TabContainer1.ActiveTabIndex = 0;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_list2_SelectedIndexChanged(object sender, EventArgs e)
    {
        //TabContainer1.ActiveTabIndex = 2;

        gv_archive_lease_list.Visible = true;
        gv_archive_lease_list.DataSourceID = "ObjectDataSource2";

        // if value = 0 form was not submit
        h_btn_submit.Value = "0";

        DateTime right_now = new DateTime();
        right_now = DateTime.Now;

        tiger.Date d = new tiger.Date();

        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        // right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        // right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));

        // tiger.Date d = new tiger.Date();

        //  the_date = 
        right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


        int home_id = Convert.ToInt32(ddl_home_list2.SelectedValue);



        // here we check if there's any unit in this property
        tiger.Unit uc = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        int unit_count = uc.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

        //  if (unit_count > 0)
        // here we get the list of unit that were already added 
        {


            // repopulating the ddl for the property selected
            ddl_unit_id2.Dispose();
            ddl_unit_id2.DataSource = uc.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
            // ddl_unit_id.SelectedValue = Convert.ToString(unit_id);
            ddl_unit_id2.DataBind();
            ddl_unit_id2.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
            ddl_unit_id2.SelectedIndex = 0;


        }
        //  else
        {

        }

      }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_unit_id2_SelectedIndexChanged(object sender, EventArgs e)
    {
        DateTime right_now = new DateTime();
        right_now = DateTime.Now;
        tiger.Date d = new tiger.Date();

        //TabContainer1.ActiveTabIndex = 2;

        gv_archive_lease_list.DataSourceID = "ObjectDataSource2";

        if (Convert.ToInt32(ddl_unit_id2.SelectedValue) > 0)
        {
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prUnitLeaseArchiveList", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_unit_id2.SelectedValue);

            DateTime datemin = new DateTime();
            DateTime datemax = new DateTime();


            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            datemin = Convert.ToDateTime(d.DateCulture("1", "1", "3000", Convert.ToString(Session["_lastCulture"])));
            datemax = Convert.ToDateTime(d.DateCulture("1", "1", "1900", Convert.ToString(Session["_lastCulture"])));


            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader();

                while (dr.Read() == true)
                {

                    // Populate series data
                    //Chart1.Series["Series1"].Points.AddXY(1, Convert.ToDateTime(dr["tu_date_begin"]), Convert.ToDateTime(dr["tu_date_end"]));



                    if (Convert.ToDateTime(dr["tu_date_begin"]) < datemin)
                        datemin = Convert.ToDateTime(dr["tu_date_begin"]);

                    if (Convert.ToDateTime(dr["tu_date_end"]) > datemax)
                        datemax = Convert.ToDateTime(dr["tu_date_end"]);


                    // Set axis labels
                    //Chart1.Series["Series1"].Points[0].AxisLabel = dr["unit_door_no"].ToString();



                }

                //// Set Range Bar chart type
                //Chart1.Series["Series1"].ChartType = SeriesChartType.RangeBar;

                //Chart1.Series["Series1"].ToolTip = Resources.Resource.lbl_lease_date_begin + " : #VALY  ,\n " + Resources.Resource.lbl_lease_date_end + " :\n#VALY2";

                //// Set point width
                //Chart1.Series["Series1"]["PointWidth"] = "0.25";

                //// Set Y axis Minimum and Maximum
                //Chart1.ChartAreas["ChartArea1"].AxisY.Minimum = datemin.AddDays(-30).ToOADate();
                //Chart1.ChartAreas["ChartArea1"].AxisY.Maximum = datemax.AddDays(30).ToOADate();
                //Chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Format = "MMM dd yyyy";


                //foreach (DataPoint point in Chart1.Series[0].Points)
                //{
                //    point.YValues[0] = point.YValues[0];
                //    Chart1.Series["Series1"].Palette = ChartColorPalette.BrightPastel;//assigning colors to datapoint  
                //}

                //Chart1.Width = 650;
                //Chart1.Height = 120;
                //Chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Angle = -45;
            }
            finally
            {
                conn.Close();
            }

            //Chart1.Visible = true;
        }
        else
        {
            //Chart1.Visible = false;
        }






        if (Convert.ToInt32(ddl_unit_id2.SelectedIndex) == 0)
        {
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prRangeBarChartLease", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_list2.SelectedValue);


            DateTime datebegin = new DateTime();
            DateTime dateend = new DateTime();


            DateTime datemin = new DateTime();
            DateTime datemax = new DateTime();


            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            datemin = Convert.ToDateTime(d.DateCulture("1", "1", "3000", Convert.ToString(Session["_lastCulture"])));
            datemax = Convert.ToDateTime(d.DateCulture("1", "1", "1900", Convert.ToString(Session["_lastCulture"])));


            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader();

                int i = 0;
                int j = 1;
                string daterangestring = "";
                string[] datestring;
                string[] from;
                string[] to;

                while (dr.Read() == true)
                {
                    // Populate series data
                    daterangestring = dr["daterange"].ToString();

                    // Split string on spaces.
                    // This will separate all the words.

                    datestring = daterangestring.Split(',');

                    for (int k = 0; k < datestring.Length - 1; k = k + 2)
                    {

                        //daterangestring=datestring[k];
                        from = datestring[k].Split('/');
                        to = datestring[k + 1].Split('/');


                        if (datestring[k] != "")
                            datebegin = Convert.ToDateTime(d.DateCulture(from[0], from[1], from[2], Convert.ToString(Session["_lastCulture"])));


                        if (datestring[k + 1] != "")
                            dateend = Convert.ToDateTime(d.DateCulture(to[0], to[1], to[2], Convert.ToString(Session["_lastCulture"])));


                        if (datebegin < datemin)
                            datemin = datebegin;

                        if (dateend > datemax)
                            datemax = dateend;

                        //Chart1.Series["Series1"].Points.AddXY(j, datebegin, dateend);
                    }

                    // Set axis labels
                    CustomLabel sd = new CustomLabel();
                    sd.Text = dr["unit_door_no"].ToString();

                    sd.FromPosition = i + 4;
                    sd.ToPosition = i - 2;

                    //Chart1.ChartAreas["ChartArea1"].AxisX.CustomLabels.Add(sd);

                    //Chart1.ChartAreas[0].AxisX.LabelStyle.Interval = 1;
                    //Chart1.Series["Series1"].ToolTip = Resources.Resource.lbl_lease_date_begin + " : #VALY  ,\n " + Resources.Resource.lbl_lease_date_end + " :\n#VALY2";


                    //foreach (DataPoint point in Chart1.Series[0].Points)
                    //{
                    //    point.YValues[0] = point.YValues[0];
                    //    Chart1.Series["Series1"].Palette = ChartColorPalette.BrightPastel;//assigning colors to datapoint  
                    //}

                    j++;
                    i++;

                }


                // Set Range Bar chart type
                //Chart1.Series["Series1"].ChartType = SeriesChartType.RangeBar;

                //// Set point width
                //Chart1.Series["Series1"]["PointWidth"] = "0.2";

                //// Set Y axis Minimum and Maximum
                //Chart1.ChartAreas["ChartArea1"].AxisY.Minimum = datemin.AddDays(-30).ToOADate();
                //Chart1.ChartAreas["ChartArea1"].AxisY.Maximum = datemax.AddDays(30).ToOADate();
                //Chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Format = "MMM dd yyyy";
                //Chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Angle = -45;

                //Chart1.Width = 650;
                //Chart1.Height = 400;

            }
            finally
            {
                conn.Close();
            }

            //Chart1.Visible = true;
        }


    }

    protected void ddl_home_pending_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        int home_id = Convert.ToInt32(ddl_home_pending_list.SelectedValue);

        //TabContainer1.ActiveTabIndex = 1;

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd1 = new SqlCommand("prLeasePendingList", conn);
        SqlCommand cmd2 = new SqlCommand("prLeasePendingList", conn);
        SqlCommand cmd3 = new SqlCommand("prLeasePendingList", conn);
        SqlCommand cmd4 = new SqlCommand("prLeasePendingList", conn);

        cmd1.CommandType = CommandType.StoredProcedure;

        try
        {

            conn.Open();

            cmd1.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd1.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd1.Parameters.Add("@time_frame", SqlDbType.Int).Value = 1;


            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da.Fill(dt);


            gv_lease_pending_list1.DataSource = dt;
            gv_lease_pending_list1.DataBind();
        }
        finally
        {
            //  conn.Close();
        }



        cmd2.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd2.Parameters.Add("@time_frame", SqlDbType.Int).Value = 2;


            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);


            gv_lease_pending_list2.DataSource = dt;
            gv_lease_pending_list2.DataBind();
        }
        finally
        {
            //  conn.Close();
        }




        cmd3.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd3.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd3.Parameters.Add("@time_frame", SqlDbType.Int).Value = 3;


            SqlDataAdapter da = new SqlDataAdapter(cmd3);
            DataTable dt = new DataTable();
            da.Fill(dt);


            gv_lease_pending_list3.DataSource = dt;
            gv_lease_pending_list3.DataBind();
        }
        finally
        {
            //  conn.Close();
        }


        cmd4.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd4.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd4.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd4.Parameters.Add("@time_frame", SqlDbType.Int).Value = 4;


            SqlDataAdapter da = new SqlDataAdapter(cmd4);
            DataTable dt = new DataTable();
            da.Fill(dt);


            gv_lease_pending_list4.DataSource = dt;
            gv_lease_pending_list4.DataBind();
        }
        finally
        {
            conn.Close();
        }


    }

    protected void gv_lease_pending_list1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //TabContainer1.ActiveTabIndex = 1;

        int home_id = Convert.ToInt32(ddl_home_pending_list.SelectedValue);

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.Lease hv = new tiger.Lease(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_lease_pending_list1.PageIndex = e.NewPageIndex;
        gv_lease_pending_list1.DataSource = hv.getLeasePendingList(Convert.ToInt32(Session["schema_id"]), home_id, 1);
        gv_lease_pending_list1.DataBind();

    }


    protected void gv_lease_pending_list2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //TabContainer1.ActiveTabIndex = 1;

        int home_id = Convert.ToInt32(ddl_home_pending_list.SelectedValue);

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.Lease hv = new tiger.Lease(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_lease_pending_list2.PageIndex = e.NewPageIndex;
        gv_lease_pending_list2.DataSource = hv.getLeasePendingList(Convert.ToInt32(Session["schema_id"]), home_id, 2);
        gv_lease_pending_list2.DataBind();

    }


    protected void gv_lease_pending_list3_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        //TabContainer1.ActiveTabIndex = 1;

        int home_id = Convert.ToInt32(ddl_home_pending_list.SelectedValue);

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.Lease hv = new tiger.Lease(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_lease_pending_list3.PageIndex = e.NewPageIndex;
        gv_lease_pending_list3.DataSource = hv.getLeasePendingList(Convert.ToInt32(Session["schema_id"]), home_id, 3);
        gv_lease_pending_list3.DataBind();

    }


    protected void gv_lease_pending_list4_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //TabContainer1.ActiveTabIndex = 1;

        int home_id = Convert.ToInt32(ddl_home_pending_list.SelectedValue);

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.Lease hv = new tiger.Lease(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_lease_pending_list4.PageIndex = e.NewPageIndex;
        gv_lease_pending_list4.DataSource = hv.getLeasePendingList(Convert.ToInt32(Session["schema_id"]), home_id, 4);
        gv_lease_pending_list4.DataBind();

    }

    protected string GetLeaseType(string type)
    {
        string lease_type = "";

        if (type == "R")
            lease_type = "Residential";
        else
            lease_type = "Commercial";

        return lease_type;

    }


    protected void gv_rented_paid_unit_list_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView gridView = (GridView)sender;

        if (e.Row.RowType == DataControlRowType.Header)
        {
            int cellIndex = -1;
            foreach (DataControlField field in gridView.Columns)
            {
                e.Row.Cells[gridView.Columns.IndexOf(field)].CssClass = "headerstyle";

                if (field.SortExpression == gridView.SortExpression)
                {
                    cellIndex = gridView.Columns.IndexOf(field);
                }
            }

            if (cellIndex > -1)
            {
                //  this is a header row,
                //  set the sort style
                e.Row.Cells[cellIndex].CssClass =
                    gridView.SortDirection == SortDirection.Ascending
                    ? "sortascheaderstyle" : "sortdescheaderstyle";
            }
        }
    }


}
