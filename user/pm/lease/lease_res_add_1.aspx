<%@ Page Language="C#" MasterPageFile="~/user/mp_property_manager.master" MaintainScrollPositionOnPostback="true"  AutoEventWireup="true" CodeFile="lease_res_add_1.aspx.cs" Inherits="lease_lease_res_add_1"%>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <B>CREATE LEASE - STEP 1 </B><br /><br />
   
         <table>
            <tr><td>
            <asp:Panel ID="panel_add_home" runat="server" Height="40px" Width="330px">
          <asp:DropDownList ID="ddl_home_id" DataValueField="home_id" DataTextField="home_name"   runat="server" autopostback="true" OnSelectedIndexChanged="ddl_home_id_SelectedIndexChanged" />
                </asp:Panel>
           </td></tr>
           
           <tr><td><div id="txt_pending" runat=server></div></td></tr>
           <tr><td><div id="txt_message" runat=server></div></td></tr>
         
         <tr>
         
         <td>
         
    <asp:Repeater ID="r_pendingleaseslist"  runat="server">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
            <tr>
                <td>
                   <b>Pending lease date begin</b> </td>
                <td >
                    : 
                    <%#DataBinder.Eval(Container.DataItem, "tu_date_begin")%>
                    <input type="hidden" id="hd_pending_tu_date_begin" value ='<%#DataBinder.Eval(Container.DataItem, "tu_date_begin")%>' runat="server" />
                    
               </td>
            </tr>
         </table>
        </ItemTemplate>
        
    </asp:Repeater>
            
         
         
         </td>
         </tr>
        
           <tr><td>
        <asp:Panel ID="panel_add_unit"  runat="server" Height="5px" Width="328px">
            <strong><span style="font-size: 10pt">select unit</span></strong> :
            <asp:DropDownList ID="ddl_unit_id" DataValueField="unit_id" DataTextField="unit_door_no"   runat="server" OnSelectedIndexChanged="ddl_unit_id_SelectedIndexChanged" AutoPostBack="true" />
                </asp:Panel>
            
           </td></tr>
           
           <asp:Panel ID="panel_create_lease" runat="server">
           <tr><td><br />
             <asp:Panel ID="panel_current_tenant"  Visible="false" runat="server">
             <table><tr><td colspan='2'><div id="txt_current_tenant_name" runat="server" /></td></tr><tr><td>
                 Date of departure(mm-dd-yyy)</td><td>
                     <asp:DropDownList ID="ddl_tu_date_end_m" runat="server">
                     <asp:ListItem Value="0">Month</asp:ListItem>
                     <asp:ListItem Value="1">January</asp:ListItem>
                     <asp:ListItem Value="2">February</asp:ListItem>
                     <asp:ListItem Value="3">March</asp:ListItem>
                     <asp:ListItem Value="4">April</asp:ListItem>
                     <asp:ListItem Value="5">May</asp:ListItem>
                     <asp:ListItem Selected="True" Value="6">June</asp:ListItem>
                     <asp:ListItem Value="7">July</asp:ListItem>
                     <asp:ListItem Value="8">August</asp:ListItem>
                     <asp:ListItem Value="9">September</asp:ListItem>
                     <asp:ListItem Value="10">October</asp:ListItem>
                     <asp:ListItem Value="11">November</asp:ListItem>
                     <asp:ListItem Value="12">December</asp:ListItem>
                 </asp:DropDownList>&nbsp; / &nbsp;
                 <asp:DropDownList ID="ddl_tu_date_end_d" runat="server">
                     <asp:ListItem Value="0">Day</asp:ListItem>
                     <asp:ListItem Selected="True">1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>&nbsp; / &nbsp;
                 <asp:DropDownList ID="ddl_tu_date_end_y" runat="server" >
                     <asp:ListItem Value="0">Year</asp:ListItem>
                     <asp:ListItem Selected="True">1960</asp:ListItem>
                     <asp:ListItem>1961</asp:ListItem>
                     <asp:ListItem>1962</asp:ListItem>
                     <asp:ListItem>1963</asp:ListItem>
                     <asp:ListItem>1964</asp:ListItem>
                     <asp:ListItem>1965</asp:ListItem>
                     <asp:ListItem>1966</asp:ListItem>
                     <asp:ListItem>1967</asp:ListItem>
                     <asp:ListItem>1968</asp:ListItem>
                     <asp:ListItem>1969</asp:ListItem>
                     <asp:ListItem>1970</asp:ListItem>
                     <asp:ListItem>1971</asp:ListItem>
                     <asp:ListItem>1972</asp:ListItem>
                     <asp:ListItem>1973</asp:ListItem>
                     <asp:ListItem>1974</asp:ListItem>
                     <asp:ListItem>1975</asp:ListItem>
                     <asp:ListItem>1976</asp:ListItem>
                     <asp:ListItem>1977</asp:ListItem>
                     <asp:ListItem>1978</asp:ListItem>
                     <asp:ListItem>1979</asp:ListItem>
                     <asp:ListItem>1980</asp:ListItem>
                     <asp:ListItem>1981</asp:ListItem>
                     <asp:ListItem>1982</asp:ListItem>
                     <asp:ListItem>1983</asp:ListItem>
                     <asp:ListItem>1984</asp:ListItem>
                     <asp:ListItem>1985</asp:ListItem>
                     <asp:ListItem>1986</asp:ListItem>
                     <asp:ListItem>1987</asp:ListItem>
                     <asp:ListItem>1988</asp:ListItem>
                     <asp:ListItem>1989</asp:ListItem>
                     <asp:ListItem>1990</asp:ListItem>
                     <asp:ListItem>1991</asp:ListItem>
                     <asp:ListItem>1992</asp:ListItem>
                     <asp:ListItem>1993</asp:ListItem>
                     <asp:ListItem>1994</asp:ListItem>
                     <asp:ListItem>1995</asp:ListItem>
                     <asp:ListItem>1996</asp:ListItem>
                     <asp:ListItem>1997</asp:ListItem>
                     <asp:ListItem>1998</asp:ListItem>
                     <asp:ListItem>1999</asp:ListItem>
                     <asp:ListItem>2000</asp:ListItem>
                     <asp:ListItem >2001</asp:ListItem>
                     <asp:ListItem>2002</asp:ListItem>
                     <asp:ListItem >2003</asp:ListItem>
                     <asp:ListItem>2004</asp:ListItem>
                     <asp:ListItem>2005</asp:ListItem>
                     <asp:ListItem>2006</asp:ListItem>
                     <asp:ListItem >2007</asp:ListItem>
                     <asp:ListItem>2008</asp:ListItem>
                     <asp:ListItem>2009</asp:ListItem>
                 </asp:DropDownList></td></tr></table>
             </asp:Panel>
           </td></tr>
           <tr><td>
           <table><tr><td colspan="2" class="nameb">New tenant</td></tr><tr><td class="letter">
               Lease starting date(mm-dd-yyy)</td><td>
           <asp:DropDownList ID="ddl_tu_date_begin_m" runat="server">
                     <asp:ListItem Value="0">Month</asp:ListItem>
                     <asp:ListItem Value="1">January</asp:ListItem>
                     <asp:ListItem Value="2">February</asp:ListItem>
                     <asp:ListItem Value="3">March</asp:ListItem>
                     <asp:ListItem Value="4">April</asp:ListItem>
                     <asp:ListItem Value="5">May</asp:ListItem>
                     <asp:ListItem Value="6">June</asp:ListItem>
                     <asp:ListItem Value="7">July</asp:ListItem>
                     <asp:ListItem Value="8">August</asp:ListItem>
                     <asp:ListItem Value="9">September</asp:ListItem>
                     <asp:ListItem Value="10">October</asp:ListItem>
                     <asp:ListItem Value="11">November</asp:ListItem>
                     <asp:ListItem Value="12">December</asp:ListItem>
                 </asp:DropDownList>&nbsp; / &nbsp;
                 <asp:DropDownList ID="ddl_tu_date_begin_d" runat="server">
                     <asp:ListItem Value="0">Day</asp:ListItem>
                     <asp:ListItem >1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>&nbsp; / &nbsp;
                 <asp:DropDownList ID="ddl_tu_date_begin_y" runat="server" >
                     <asp:ListItem Value="0">Year</asp:ListItem>
                     <asp:ListItem >1959</asp:ListItem>
                     <asp:ListItem >1960</asp:ListItem>
                     <asp:ListItem>1961</asp:ListItem>
                     <asp:ListItem>1962</asp:ListItem>
                     <asp:ListItem>1963</asp:ListItem>
                     <asp:ListItem>1964</asp:ListItem>
                     <asp:ListItem>1965</asp:ListItem>
                     <asp:ListItem>1966</asp:ListItem>
                     <asp:ListItem>1967</asp:ListItem>
                     <asp:ListItem>1968</asp:ListItem>
                     <asp:ListItem>1969</asp:ListItem>
                     <asp:ListItem>1970</asp:ListItem>
                     <asp:ListItem>1971</asp:ListItem>
                     <asp:ListItem>1972</asp:ListItem>
                     <asp:ListItem>1973</asp:ListItem>
                     <asp:ListItem>1974</asp:ListItem>
                     <asp:ListItem>1975</asp:ListItem>
                     <asp:ListItem>1976</asp:ListItem>
                     <asp:ListItem>1977</asp:ListItem>
                     <asp:ListItem>1978</asp:ListItem>
                     <asp:ListItem>1979</asp:ListItem>
                     <asp:ListItem>1980</asp:ListItem>
                     <asp:ListItem>1981</asp:ListItem>
                     <asp:ListItem>1982</asp:ListItem>
                     <asp:ListItem>1983</asp:ListItem>
                     <asp:ListItem>1984</asp:ListItem>
                     <asp:ListItem>1985</asp:ListItem>
                     <asp:ListItem>1986</asp:ListItem>
                     <asp:ListItem>1987</asp:ListItem>
                     <asp:ListItem>1988</asp:ListItem>
                     <asp:ListItem>1989</asp:ListItem>
                     <asp:ListItem>1990</asp:ListItem>
                     <asp:ListItem>1991</asp:ListItem>
                     <asp:ListItem>1992</asp:ListItem>
                     <asp:ListItem>1993</asp:ListItem>
                     <asp:ListItem>1994</asp:ListItem>
                     <asp:ListItem>1995</asp:ListItem>
                     <asp:ListItem>1996</asp:ListItem>
                     <asp:ListItem>1997</asp:ListItem>
                     <asp:ListItem>1998</asp:ListItem>
                     <asp:ListItem>1999</asp:ListItem>
                     <asp:ListItem>2000</asp:ListItem>
                     <asp:ListItem >2001</asp:ListItem>
                     <asp:ListItem>2002</asp:ListItem>
                     <asp:ListItem >2003</asp:ListItem>
                     <asp:ListItem>2004</asp:ListItem>
                     <asp:ListItem>2005</asp:ListItem>
                     <asp:ListItem>2006</asp:ListItem>
                     <asp:ListItem >2007</asp:ListItem>
                     <asp:ListItem >2008</asp:ListItem>
                     <asp:ListItem>2009</asp:ListItem>
                     <asp:ListItem>2010</asp:ListItem>
                     <asp:ListItem>2011</asp:ListItem>
                     <asp:ListItem>2013</asp:ListItem>
                     <asp:ListItem>2014</asp:ListItem>
                     <asp:ListItem>2015</asp:ListItem>
                     <asp:ListItem>2016</asp:ListItem>
                     <asp:ListItem>2017</asp:ListItem>
                     <asp:ListItem>2018</asp:ListItem>
                     <asp:ListItem>2019</asp:ListItem>
                     <asp:ListItem>2020</asp:ListItem>
                 </asp:DropDownList></td></tr></table>
           </td></tr>
           </asp:Panel>
           
           
           
           
           <tr><td>
               
               <table   cellpadding="0" cellspacing="0" style="width: 100%">
                   <tr>
                       <td valign="top">
                          
                           <table runat="server" id="tb_last_paid_rent" bgcolor="#ffffcc" cellpadding="0" cellspacing="0" style="width: 100%">
                               <tr>
                                   <td>
                                       Last rent due date of this unit :
                                       <asp:Label ID="lbl_last_due_date" runat="server" style="font-weight: 700"></asp:Label>
                                       <br />
                                       <br />
                                   </td>
                               </tr>
                               <tr>
                                   <td>
                                       Last rent payment&nbsp; of this unit:&nbsp;&nbsp;
                                       <asp:Label ID="lbl_last_date_paid" runat="server" style="font-weight: 700"></asp:Label>
                                       <br />
                                       <br />
                                       the date of departure and the&nbsp; new lease start date<br />
                                       must be greater than
                                       <asp:Label ID="lbl_min_date_begin2" runat="server" style="font-weight: 700"></asp:Label>
                                   </td>
                               </tr>
                           </table>
                           <br />
                       </td>
                   </tr>
               </table>
               <br />
           <asp:Button ID="btn_continue" runat="server" Text="Continue"  OnClick="btn_continue_Onclick"/>
          </td></tr>
          <tr><td><div ID="txt_link" runat="server" /></td></tr>
         <tr><td>
         </td></tr>
          </table>
          
  
    <!--Hidden fields BEGIN SECTION 1-->
         <input type="hidden" id="hd_home_id" runat="server" />
         <input type="hidden" id="hd_unit_id" runat="server" />
         <input type="hidden" id="hd_current_tu_id" runat="server" />
         <!--Hidden fields END SECTION 1-->
           <!--Hidden fields temporaire-->
          <input type="hidden" id="didden" runat="server" />
          
    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label><br />
    <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label><br />
    <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label><br />
    <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label><br />
    
           
 </asp:Content>