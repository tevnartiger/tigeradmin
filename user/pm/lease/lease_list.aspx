﻿<%@ Page Language="C#" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" CodeFile="lease_list.aspx.cs" Inherits="manager_home_lease_list" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
    <br />
<h1><%= Resources.Resource.lbl_u_lease %></h1>
   
       <div id="txt_message" runat="server"></div>
    <table>
        <tr>
            <td>
      
   <asp:Label ID="lbl_property" runat="server" Text="<%$ Resources:Resource, lbl_property %>"/> 
            </td>
            <td>
                :
                <asp:DropDownList AutoPostBack="true" DataTextField="home_name" DataValueField="home_id" ID="ddl_home_list" runat="server" OnSelectedIndexChanged="ddl_home_list_SelectedIndexChanged">
        </asp:DropDownList>
            &nbsp;</td>
        </tr>
        <tr>
        
                  <td>
      <asp:Label ID="lbl_unit" runat="server" Text="<%$ Resources:Resource, lbl_unit %>"/>
                </td>
                <td valign="top">
                    :
        <asp:DropDownList ID="ddl_unit_id" DataValueField="unit_id" DataTextField="unit_door_no"   runat="server" OnSelectedIndexChanged="ddl_unit_id_SelectedIndexChanged" AutoPostBack="true" />   
    
                </td>
        </tr>
        
        </table>
         
  
    <br />

         <asp:GridView ID="gv_rented_paid_unit_list" runat="server" 
           AllowPaging="true"   
           AutoGenerateColumns="false" DataSourceID="ObjectDataSource1"
           EmptyDataText="<%$ Resources:Resource, lbl_none %>"      
           PageSize="10" Width="80%">
     
    <Columns>
   <asp:BoundField ItemStyle-VerticalAlign="Top"  DataField="unit_door_no" SortExpression="Unit"   />
   <asp:BoundField ItemStyle-VerticalAlign="Top"    DataField="tu_date_begin" DataFormatString="{0:MMM dd , yyyy}"  
     HeaderText="Date Begin" HtmlEncode="false" SortExpression="Date Begin" />
   <asp:BoundField ItemStyle-VerticalAlign="Top"    DataField="tu_date_end" DataFormatString="{0:MMMM dd , yyyy}"  
     HeaderText="Date End" HtmlEncode="false" SortExpression="Date End" />
      <asp:BoundField DataField="tu_id" HeaderText="tu_id"   />
      
      
    <asp:TemplateField HeaderText="Lease type"   >
    <ItemTemplate  >
    
    <asp:Label  runat="server"  ID="lbl_lease_type"    Text='<%#GetLeaseType(Convert.ToString(Eval("unit_type")))%>'    /> 
    
    </ItemTemplate>
    </asp:TemplateField>
      
    <asp:HyperLinkField ItemStyle-VerticalAlign="Top"   Text="View"
     DataNavigateUrlFields="tu_id,unit_id,home_id" 
     DataNavigateUrlFormatString="~/manager/lease/lease_view.aspx?tu_id={0}&unit_id={1}&h_id={2}" 
      HeaderText="View" />
       
   </Columns>
   </asp:GridView>
        <br />
   
   
   
     <asp:ObjectDataSource SelectMethod="getLeaseList_2" EnablePaging ="True"  
           EnableViewState="true"
            OnSelecting="ObjectDataSource1_Selecting" 
        TypeName="tiger.Lease"  SelectCountMethod ="getTotalLeaseCount"    
        runat="server" ID="ObjectDataSource1">
      
 </asp:ObjectDataSource>
       
   
  
    <asp:HiddenField ID="h_btn_submit" Value="0" runat="server" />


   
   
   
   
   
   <h1><%= Resources.Resource.lbl_u_pending_lease %></h1>
    
 
     
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" style="font-weight: 700" 
                        Text="<%$ Resources:Resource, lbl_property %>" />
                </td>
                <td>
                    :
                    <asp:DropDownList ID="ddl_home_pending_list" runat="server" AutoPostBack="true" 
                        DataTextField="home_name" DataValueField="home_id" 
                        OnSelectedIndexChanged="ddl_home_pending_list_SelectedIndexChanged">
                    </asp:DropDownList>
                    &nbsp;</td>
            </tr>
        </table>
    
    <br />
       <table width="80%">
           <tr>
               <td bgcolor="aliceblue">
                   <b>
                   <asp:Label ID="Label42" runat="server" 
                       Text="<%$ Resources:Resource, lbl_less31%>" />
                   </b>
               </td>
           </tr>
       </table>
    <br />
       <asp:GridView ID="gv_lease_pending_list1" runat="server" 
           AllowPaging="true"
           AutoGenerateColumns="false"  
           EmptyDataText="<%$ Resources:Resource, lbl_none %>"  
          
           OnPageIndexChanging="gv_lease_pending_list1_PageIndexChanging" 
           PageSize="10" Width="80%">
      <Columns>
           <asp:BoundField ItemStyle-VerticalAlign="Top"  DataField="unit_door_no" SortExpression="Unit"
             HeaderText="<%$ Resources:Resource, lbl_door_no %>"    />
           <asp:BoundField ItemStyle-VerticalAlign="Top"   DataField="tu_date_begin" DataFormatString="{0:MMMM dd , yyyy}"  
             HeaderText="<%$ Resources:Resource, lbl_date_begin %>" 
             HtmlEncode="false" SortExpression="<%$ Resources:Resource, lbl_date_begin %>"  />
           <asp:BoundField  ItemStyle-VerticalAlign="Top"   DataField="tu_date_end" DataFormatString="{0:MMMM dd , yyyy}"  
             HeaderText="<%$ Resources:Resource, lbl_date_end %>"  HtmlEncode="false"
             SortExpression="<%$ Resources:Resource, lbl_date_end %>"  />
             
             
              <asp:TemplateField HeaderText="Lease type"   >
          <ItemTemplate  >
    
         <asp:Label  runat="server"  ID="lbl_lease_type"    Text='<%#GetLeaseType(Convert.ToString(Eval("unit_type")))%>'    /> 
    
       </ItemTemplate>
        </asp:TemplateField>
             
             
           <asp:BoundField ItemStyle-VerticalAlign="Top"  DataField="tu_id" HeaderText="tu_id"   />
           <asp:HyperLinkField ItemStyle-VerticalAlign="Top"    Text="<%$ Resources:Resource, lbl_view %>" 
              DataNavigateUrlFields="tu_id,unit_id,home_id" 
              DataNavigateUrlFormatString="~/manager/lease/lease_view.aspx?tu_id={0}&unit_id={1}&h_id={2}" 
              HeaderText="View" />
       
           </Columns>
       </asp:GridView>
     <br />
     
  <br />
       <table Width="80%">
           <tr>
               <td bgcolor="aliceblue">
                   <b>
                   <asp:Label ID="Label6" runat="server" 
                       Text="<%$ Resources:Resource, lbl_between_31and60%>" />
                   </b>
               </td>
           </tr>
       </table>
    <br />
       <asp:GridView ID="gv_lease_pending_list2" runat="server" 
           AllowPaging="true" AlternatingRowStyle-BackColor="#F0F0F6" 
           AutoGenerateColumns="false"  
           EmptyDataText="<%$ Resources:Resource, lbl_none %>"  
          
           OnPageIndexChanging="gv_lease_pending_list2_PageIndexChanging" 
           PageSize="10" Width="80%">
           <Columns>
            <asp:BoundField ItemStyle-VerticalAlign="Top"  DataField="unit_door_no" SortExpression="Unit"
             HeaderText="<%$ Resources:Resource, lbl_door_no %>"    />
           <asp:BoundField ItemStyle-VerticalAlign="Top"    DataField="tu_date_begin" DataFormatString="{0:MMMM dd , yyyy}"  
             HeaderText="<%$ Resources:Resource, lbl_date_begin %>" 
             HtmlEncode="false" SortExpression="<%$ Resources:Resource, lbl_date_begin %>"  />
           <asp:BoundField ItemStyle-VerticalAlign="Top"    DataField="tu_date_end" DataFormatString="{0:MMMM dd , yyyy}"  
             HeaderText="<%$ Resources:Resource, lbl_date_end %>"  HtmlEncode="false"
             SortExpression="<%$ Resources:Resource, lbl_date_end %>"  />
             
           <asp:BoundField ItemStyle-VerticalAlign="Top"  DataField="tu_id" HeaderText="tu_id"   />
           <asp:HyperLinkField ItemStyle-VerticalAlign="Top"    Text="<%$ Resources:Resource, lbl_view %>" 
              DataNavigateUrlFields="tu_id,unit_id,home_id" 
              DataNavigateUrlFormatString="~/manager/lease/lease_view.aspx?tu_id={0}&unit_id={1}&h_id={2}" 
              HeaderText="View" />
           </Columns>
       </asp:GridView>
       <br />
    
        <table width="80%">
            <tr>
                <td bgcolor="aliceblue">
                    <b>
                    <asp:Label ID="Label7" runat="server" 
                        Text="<%$ Resources:Resource, lbl_between_61and90%>" />
                    </b>
                </td>
            </tr>
       </table>
    <br />
       <asp:GridView ID="gv_lease_pending_list3" runat="server" 
           AllowPaging="true" AlternatingRowStyle-BackColor="#F0F0F6" 
           AutoGenerateColumns="false"  
           EmptyDataText="<%$ Resources:Resource, lbl_none %>"  
           
           OnPageIndexChanging="gv_lease_pending_list3_PageIndexChanging" 
           PageSize="10" Width="80%">
           <Columns>
           <asp:BoundField ItemStyle-VerticalAlign="Top"  DataField="unit_door_no" SortExpression="Unit"
             HeaderText="<%$ Resources:Resource, lbl_door_no %>"    />
           <asp:BoundField ItemStyle-VerticalAlign="Top"   DataField="tu_date_begin" DataFormatString="{0:MMMM dd , yyyy}"  
             HeaderText="<%$ Resources:Resource, lbl_date_begin %>" 
             HtmlEncode="false" SortExpression="<%$ Resources:Resource, lbl_date_begin %>"  />
           <asp:BoundField ItemStyle-VerticalAlign="Top"    DataField="tu_date_end" DataFormatString="{0:MMMM dd , yyyy}"  
             HeaderText="<%$ Resources:Resource, lbl_date_end %>"  HtmlEncode="false"
             SortExpression="<%$ Resources:Resource, lbl_date_end %>"  />
             
           <asp:BoundField ItemStyle-VerticalAlign="Top"  DataField="tu_id" HeaderText="tu_id"   />
           <asp:HyperLinkField ItemStyle-VerticalAlign="Top"    Text="<%$ Resources:Resource, lbl_view %>" 
              DataNavigateUrlFields="tu_id,unit_id,home_id" 
              DataNavigateUrlFormatString="~/manager/lease/lease_view.aspx?tu_id={0}&unit_id={1}&h_id={2}" 
              HeaderText="View" />
           </Columns>
       </asp:GridView>
       <br />
    
    
        <br />
        <table width="80%">
            <tr>
                <td bgcolor="aliceblue">
                    <b>
                    <asp:Label ID="Label8" runat="server" 
                        Text="<%$ Resources:Resource, lbl_more90%>" />
                    </b>
                </td>
            </tr>
       </table>
       <br />
       <asp:GridView ID="gv_lease_pending_list4" runat="server" 
           AllowPaging="true" AlternatingRowStyle-BackColor="#F0F0F6" 
           AutoGenerateColumns="false" 
           
           EmptyDataText="<%$ Resources:Resource, lbl_none %>"  
          
           OnPageIndexChanging="gv_lease_pending_list4_PageIndexChanging" 
           PageSize="10" Width="80%">
           <Columns>
           <asp:BoundField ItemStyle-VerticalAlign="Top"  DataField="unit_door_no" SortExpression="Unit"
             HeaderText="<%$ Resources:Resource, lbl_door_no %>"    />
           <asp:BoundField ItemStyle-VerticalAlign="Top"    DataField="tu_date_begin" DataFormatString="{0:MMMM dd , yyyy}"  
             HeaderText="<%$ Resources:Resource, lbl_date_begin %>" 
             HtmlEncode="false" SortExpression="<%$ Resources:Resource, lbl_date_begin %>"  />
           <asp:BoundField ItemStyle-VerticalAlign="Top"    DataField="tu_date_end" DataFormatString="{0:MMMM dd , yyyy}"  
             HeaderText="<%$ Resources:Resource, lbl_date_end %>"  HtmlEncode="false"
             SortExpression="<%$ Resources:Resource, lbl_date_end %>"  />
             
           <asp:BoundField ItemStyle-VerticalAlign="Top"  DataField="tu_id" HeaderText="tu_id"   />
           <asp:HyperLinkField ItemStyle-VerticalAlign="Top"    Text="<%$ Resources:Resource, lbl_view %>" 
              DataNavigateUrlFields="tu_id,unit_id,home_id" 
              DataNavigateUrlFormatString="~/manager/lease/lease_view.aspx?tu_id={0}&unit_id={1}&h_id={2}" 
              HeaderText="View" />
           </Columns>
       </asp:GridView>
    

   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   <h1><%= Resources.Resource.lbl_u_archive_lease %></h1>
  
 
    <table>
        <tr>
            <td>
      
   <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_property %>"/> 
            </td>
            <td>
                :
                <asp:DropDownList AutoPostBack="true" DataTextField="home_name" DataValueField="home_id" ID="ddl_home_list2" runat="server" OnSelectedIndexChanged="ddl_home_list2_SelectedIndexChanged">
        </asp:DropDownList>
            &nbsp;</td>
        </tr>
        <tr>
        
                  <td>
      <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lbl_unit %>"/>
                </td>
                <td valign="top">
                    :
        <asp:DropDownList ID="ddl_unit_id2" DataValueField="unit_id" DataTextField="unit_door_no"   runat="server" OnSelectedIndexChanged="ddl_unit_id2_SelectedIndexChanged" AutoPostBack="true" />
    
    
    
    
                </td>
        </tr>
        </table>     
    
    <br />
     <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="80%"  BorderColor="#CDCDCD"  
      DataSourceID="ObjectDataSource2"
      ID="gv_archive_lease_list" runat="server" AutoGenerateColumns="false" BorderWidth="1"     
       AllowSorting="true"  EmptyDataText="no unit rented"   PageSize="10" AllowPaging="true"
        AlternatingRowStyle-BackColor="#F0F0F6">
        
    <Columns>
   <asp:BoundField ItemStyle-VerticalAlign="Top"  DataField="unit_door_no"   />
   <asp:BoundField ItemStyle-VerticalAlign="Top"   DataField="tu_date_begin" DataFormatString="{0:MMM dd , yyyy}"  
     HeaderText="Date Begin" HtmlEncode="false" />
   <asp:BoundField ItemStyle-VerticalAlign="Top"   DataField="tu_date_end" DataFormatString="{0:MMM dd , yyyy}"  
     HeaderText="Date End" HtmlEncode="false" />
      <asp:BoundField ItemStyle-VerticalAlign="Top"  DataField="tu_id" HeaderText="tu_id"   />
      
        <asp:TemplateField HeaderText="Lease type"   >
    <ItemTemplate  >
    
    <asp:Label  runat="server"  ID="lbl_lease_type"    Text='<%#GetLeaseType(Convert.ToString(Eval("unit_type")))%>'    /> 
    
    </ItemTemplate>
    </asp:TemplateField>
      
      
       <asp:HyperLinkField ItemStyle-VerticalAlign="Top"   Text="View"
     DataNavigateUrlFields="tu_id,unit_id,home_id,tenant_id" 
     DataNavigateUrlFormatString="~/manager/lease/lease_archive_view.aspx?tu_id={0}&unit_id={1}&h_id={2}&t_id={3}" 
      HeaderText="View" />
       
   </Columns>
   </asp:GridView>
        <br />
   
    
   <asp:ObjectDataSource SelectMethod="getLeaseArchiveList" EnablePaging ="True"  
           EnableViewState="true"
            OnSelecting="ObjectDataSource2_Selecting" 
        TypeName="tiger.Lease"  SelectCountMethod ="getTotalLeaseCount"    
        runat="server" ID="ObjectDataSource2">
      
 </asp:ObjectDataSource>
       
 
    <asp:HiddenField ID="HiddenField1" Value="0" runat="server" />
    
     
     
     
     <br />
     
     
        
        
        <br /><br />
       Date begin : <asp:Label ID="date_range1" runat="server" Text="Label"></asp:Label><br />
       Date end : <asp:Label ID="date_range2" runat="server" Text="Label"></asp:Label>


    
    
    
    
    
   
    
    
    
    
    
    
</asp:Content>

