﻿<%@ Page Language="C#" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" CodeFile="lease_com_add_2.aspx.cs" Inherits="manager_lease_lease_com_add_2" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">




<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
      <asp:View ID="View1" runat="server">
          <b>
         <h3> <asp:Label ID="lbl_create_lease_3" runat="server" 
              Text="CREATE COMMERCIAL LEASE - STEP 2" /></h3>
          <br />
          </b>
      <br />&nbsp;
              Add a company
          <br />
          <br />
          <asp:CheckBox ID="chb_company" runat="server" AutoPostBack="true" 
              oncheckedchanged="chb_company_CheckedChanged" 
              Text="select a company from the system" />
          <br />
          <asp:Panel ID="panel_company" runat="server">
              &nbsp;<asp:DropDownList DataTextField="company_name" DataValueField="company_id" ID="ddl_company" runat="server">
              </asp:DropDownList>
          </asp:Panel>
          <br />
          <br />
&nbsp;<asp:CheckBox ID="chb_newcompany" Text="Add a new company" runat="server" 
               AutoPostBack="true" oncheckedchanged="chb_newcompany_CheckedChanged" />
          <br />
          <asp:Panel ID="panel_new_company" runat="server" >
          <table bgcolor="#ffffcc">
              <tr>
                  <td>
                      company name</td>
                  <td>
                      :
                      <asp:TextBox ID="company_name" runat="server"></asp:TextBox>
                      &nbsp;<asp:RegularExpressionValidator ID="reg_company_name" runat="server" 
                            ValidationGroup="vg_company"
                          ControlToValidate="company_name" ErrorMessage="invalid name">
                        </asp:RegularExpressionValidator>
                      &nbsp;<asp:RequiredFieldValidator ID="req_company_name" runat="server" 
                               ValidationGroup="vg_company"
                          ControlToValidate="company_name" ErrorMessage="required">
                          </asp:RequiredFieldValidator>
                  </td>
              </tr>
              <tr>
                  <td>
                      website</td>
                  <td>
                      :
                      <asp:TextBox ID="company_website" runat="server"></asp:TextBox>
                      &nbsp;<asp:RegularExpressionValidator ID="reg_company_website" runat="server" 
                                ValidationGroup="vg_company"
                          ControlToValidate="company_website" ErrorMessage="invalid link">
                        </asp:RegularExpressionValidator>
                  </td>
              </tr>
              <tr>
                  <td>
                      address
                  </td>
                  <td>
                      :
                      <asp:TextBox ID="company_addr" runat="server"></asp:TextBox>
                      &nbsp;<asp:RegularExpressionValidator ID="reg_company_addr_street" runat="server" 
                             ValidationGroup="vg_company"
                          ControlToValidate="company_addr_street" ErrorMessage="invalid address">
                        </asp:RegularExpressionValidator>
                  </td>
              </tr>
              <tr>
                  <td>
                      telephone</td>
                  <td>
                      :
                      <asp:TextBox ID="company_tel" runat="server"></asp:TextBox>
                      &nbsp;<asp:RegularExpressionValidator ID="reg_company_tel" runat="server" 
                           ValidationGroup="vg_company"
                          ControlToValidate="company_tel" ErrorMessage="invalid telephone">
                        </asp:RegularExpressionValidator>
                  </td>
              </tr>
              <tr>
                  <td>
                      email</td>
                  <td>
                      &nbsp;&nbsp;
                      <asp:TextBox ID="company_email" runat="server"></asp:TextBox>
                      &nbsp;<asp:RegularExpressionValidator ID="reg_company_contact_email" runat="server"
                               ValidationGroup="vg_company"
                          ControlToValidate="company_contact_email" ErrorMessage="invalid email">
                        </asp:RegularExpressionValidator>
                  </td>
              </tr>
              <tr>
                  <td>
                      city</td>
                  <td>
                      :
                      <asp:TextBox ID="company_city" runat="server"></asp:TextBox>
                      &nbsp;<asp:RegularExpressionValidator ID="reg_company_city" runat="server" 
                        ValidationGroup="vg_company"   
                          ControlToValidate="company_city" ErrorMessage="enter letters only">
                        </asp:RegularExpressionValidator>
                  </td>
              </tr>
              <tr>
                  <td>
                      fax.</td>
                  <td>
                      :
                      <asp:TextBox ID="company_fax" runat="server"></asp:TextBox>
                      &nbsp;<asp:RegularExpressionValidator ID="reg_company_fax" runat="server" 
                            ValidationGroup="vg_company"
                          ControlToValidate="companyfax" ErrorMessage="enter fax number">
                        </asp:RegularExpressionValidator>
                  </td>
              </tr>
              <tr>
                  <td valign="top">
                      comments</td>
                  <td>
                      <asp:TextBox ID="company_com" runat="server" Height="100px" 
                          TextMode="MultiLine" Width="300px"></asp:TextBox>
                      <asp:RegularExpressionValidator ID="reg_company_com" runat="server" 
                            ValidationGroup="vg_company"
                          ControlToValidate="company_com" ErrorMessage="invalid text">
                        </asp:RegularExpressionValidator>
                  </td>
              </tr>
          </table>
  
          </asp:Panel><br />
          <asp:Button ID="btn_company_next" runat="server" Text="Next" 
                   ValidationGroup="vg_company"
              onclick="btn_company_next_Click" />
          
        
      </asp:View>
      
       
      
      
      
      
      
      
      <asp:View ID="View2" runat="server">
      <b> &nbsp;&nbsp;
                <br /><h3><asp:Label ID="lbl_create_lease_2" runat="server" Text="CREATE COMMERCIAL LEASE - STEP 3"/></h3>
                <br /></b><br />
                
                
                    <asp:Label ID="lbl_tenants" runat="server" ></asp:Label>  Person(s) responsable
                
                <br />
                <br />
          <asp:Repeater ID="rTenantTempoList" runat="server">
              <ItemTemplate>
                  &nbsp;&nbsp; <%#DataBinder.Eval(Container.DataItem, "name_fname")%>&nbsp;,&nbsp;
                  <%#DataBinder.Eval(Container.DataItem, "name_lname")%><br />
              </ItemTemplate>
          </asp:Repeater>
          <br />
                 
     <table><tr><td style="width: 460px">
     <table>
     <tr><td><asp:CheckBox ID="chb_1" Text="<%$ Resources:Resource, lbl_select_name %>" runat="server" OnCheckedChanged="chb_1_CheckedChanged" AutoPostBack="true" /></td></tr>
     <asp:Panel ID="panel_1" runat="server" Visible="false">
     <tr><td ><asp:Label ID="lbl_tenant" runat="server" Text="<%$ Resources:Resource, lbl_tenant %>"/> &nbsp;&nbsp;<asp:DropDownList ID="ddl_name_id_1"   DataValueField="name_id" DataTextField="name_name" runat="server" />
        </td></tr>
        </asp:Panel>
            </table>
     </td></tr>
     <tr><td style="width: 460px"><asp:CheckBox ID="chb_name_add_1"  Text="<%$ Resources:Resource, lbl_new_name %>" runat="server" OnCheckedChanged="chb_name_add_1_CheckedChanged"  AutoPostBack="true" /></td></tr>
     <asp:Panel ID="panel_name_add_1" Visible="false" runat="server">
     <tr><td> 
     <table border="0" cellpadding="1" cellspacing="3" bgcolor="#ffffcc">
            <tr><td ><asp:Label ID="lbl_fname" runat="server" Text="<%$ Resources:Resource, lbl_fname %>"/></td>
                <td><asp:TextBox ID="name_fname_1"  runat="server"/></td>
                <td ><asp:Label ID="lbl_lname" runat="server" Text="<%$ Resources:Resource, lbl_lname %>"/></td>
                <td><asp:TextBox ID="name_lname_1"  runat="server"/></td>
            </tr>
            <tr>
             <td >
                 <asp:Label ID="lbl_dob" runat="server" Text="<%$ Resources:Resource, lbl_dob %>"/></td>
             <td colspan="3">
                 <asp:DropDownList ID="ddl_name_1_dob_m" runat="server">
                    <asp:ListItem Text="<%$ Resources:Resource, txt_month %>" Value="0"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    
                 </asp:DropDownList>&nbsp; / &nbsp;
                 <asp:DropDownList ID="ddl_name_1_dob_d" runat="server">
                     <asp:ListItem Text="<%$ Resources:Resource, txt_day %>" Value="0"></asp:ListItem>
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>&nbsp; / &nbsp;
                 <asp:DropDownList ID="ddl_name_1_dob_y" runat="server" >
                     <asp:ListItem Text="<%$ Resources:Resource, txt_year %>"  Value="0"></asp:ListItem>
                     <asp:ListItem>1960</asp:ListItem>
                     <asp:ListItem>1961</asp:ListItem>
                     <asp:ListItem>1962</asp:ListItem>
                     <asp:ListItem>1963</asp:ListItem>
                     <asp:ListItem>1964</asp:ListItem>
                     <asp:ListItem>1965</asp:ListItem>
                     <asp:ListItem>1966</asp:ListItem>
                     <asp:ListItem>1967</asp:ListItem>
                     <asp:ListItem>1968</asp:ListItem>
                     <asp:ListItem>1969</asp:ListItem>
                     <asp:ListItem>1970</asp:ListItem>
                     <asp:ListItem>1972</asp:ListItem>
                     <asp:ListItem>1973</asp:ListItem>
                     <asp:ListItem>1974</asp:ListItem>
                     <asp:ListItem>1975</asp:ListItem>
                     <asp:ListItem>1976</asp:ListItem>
                     <asp:ListItem>1977</asp:ListItem>
                     <asp:ListItem>1978</asp:ListItem>
                     <asp:ListItem>1979</asp:ListItem>
                     <asp:ListItem>1980</asp:ListItem>
                     <asp:ListItem>1981</asp:ListItem>
                     <asp:ListItem>1982</asp:ListItem>
                     <asp:ListItem>1983</asp:ListItem>
                     <asp:ListItem>1984</asp:ListItem>
                     <asp:ListItem>1985</asp:ListItem>
                     <asp:ListItem>1986</asp:ListItem>
                     <asp:ListItem>1987</asp:ListItem>
                     <asp:ListItem>1988</asp:ListItem>
                     <asp:ListItem>1989</asp:ListItem>
                     <asp:ListItem>1990</asp:ListItem>
                     <asp:ListItem>1991</asp:ListItem>
                     <asp:ListItem>1992</asp:ListItem>
                     <asp:ListItem>1993</asp:ListItem>
                     <asp:ListItem>1994</asp:ListItem>
                     <asp:ListItem>1995</asp:ListItem>
                     <asp:ListItem>1996</asp:ListItem>
                     <asp:ListItem>1997</asp:ListItem>
                     <asp:ListItem>1998</asp:ListItem>
                     <asp:ListItem>1999</asp:ListItem>
                     <asp:ListItem>2000</asp:ListItem>
                     <asp:ListItem>2001</asp:ListItem>
                     <asp:ListItem>2002</asp:ListItem>
                     <asp:ListItem>2003</asp:ListItem>
                     <asp:ListItem>2004</asp:ListItem>
                     <asp:ListItem>2005</asp:ListItem>
                     <asp:ListItem>2006</asp:ListItem>
                     <asp:ListItem>2007</asp:ListItem>
                     <asp:ListItem>2008</asp:ListItem>
                 </asp:DropDownList></td>
         </tr>
            <tr><td ><asp:Label ID="lbl_address" runat="server" Text="<%$ Resources:Resource, lbl_address %>"/></td>
                <td><asp:TextBox ID="name_addr_1"  runat="server"></asp:TextBox></td>
                <td ><asp:Label ID="lbl_city" runat="server" Text="<%$ Resources:Resource, lbl_city %>"/></td>
                <td><asp:TextBox ID="name_addr_city_1"  runat="server"/></td>
            </tr>
            <tr><td ><asp:Label ID="lbl_pc" runat="server" Text="<%$ Resources:Resource, lbl_pc %>"/></td>
                <td><asp:TextBox ID="name_addr_pc_1"  runat="server"/></td>
                <td ><asp:Label ID="lbl_prov" runat="server" Text="<%$ Resources:Resource, lbl_prov %>"/></td>
                <td>
                    <asp:TextBox ID="name_addr_state_1"  runat="server"/></td>
            </tr>
            <tr><td  ><asp:Label ID="lbl_country" runat="server" Text="<%$ Resources:Resource, lbl_country %>"/></td>
                <td colspan="3"><asp:DropDownList ID="ddl_country_id_1"  runat="server" DataTextField="country_name"
                        DataValueField="country_id"/></td>
            </tr>
            <tr><td ><asp:Label ID="lbl_tel" runat="server" Text="<%$ Resources:Resource, lbl_tel %>"/></td>
                <td><asp:TextBox ID="name_tel_1"  runat="server"/></td>
            <td ><asp:Label ID="lbl_tel_work" runat="server" Text="<%$ Resources:Resource, lbl_tel_work %>"/></td>
                <td><asp:TextBox ID="name_tel_work_1"  runat="server"/></td>
            </tr>
            <tr><td ><asp:Label ID="lbl_tel_work_ext" runat="server" Text="<%$ Resources:Resource, lbl_tel_work_ext %>"/></td>
                <td><asp:TextBox ID="name_tel_work_ext_1"  runat="server"/></td>
		<td ><asp:Label ID="lbl_cell" runat="server" Text="<%$ Resources:Resource, lbl_cell %>"/></td>
                <td><asp:TextBox ID="name_cell_1"  runat="server"/></td>
            </tr>
            <tr><td ><asp:Label ID="lbl_fax" runat="server" Text="<%$ Resources:Resource, lbl_fax %>"/></td>
                <td><asp:TextBox ID="name_fax_1"  runat="server"/></td>
                <td ><asp:Label ID="lbl_email" runat="server" Text="<%$ Resources:Resource, lbl_email %>"/></td>
                <td><asp:TextBox ID="name_email_1"  runat="server"/></td>
            </tr>
            <tr><td ><asp:Label ID="lbl_com" Visible="false" runat="server" Text="<%$ Resources:Resource, lbl_com %>"/></td>
                <td colspan="3"><asp:TextBox ID="name_com_1" Visible="false" runat="server" TextMode="MultiLine" Width="389px"/></td>
            </tr>
        </table>
     </td></tr>
     </asp:Panel>
    <tr><td style="width: 460px"><br />
        <asp:Button ID="btn_previous_1" runat="server" Text="Previous" 
            onclick="btn_previous_1_Click" />
        &nbsp; <asp:Button ID="btn_other" runat="server"  OnClick="btn_other_Click" Text="Add a person" />
        &nbsp; <asp:Button ID="btn_next_1" runat="server" Text="Next" 
            onclick="btn_next_1_Click" />&nbsp;&nbsp;&nbsp;<asp:Button 
            ID="btn_continue" Visible="false" runat="server" OnClick="btn_continue_Click" 
            Text="<%$ Resources:Resource, btn_add_name_continu %>" />
        </td></tr>
    <tr><td style="width: 460px">
     
    </td></tr>
     </table>
      </asp:View>
      
      
      
      
      
      
         
      
      
       
      
      
      
      
      
      
      <asp:View ID="View3" runat="server">
      
       <br /><b>
          <h3><asp:Label ID="lbl_create_lease_4" runat="server" 
              Text="CREATE COMMERCIAL LEASE - STEP 4" /></h3>
          <br />
          <br />
          
              Lease accommodation
          <br />
          </b><br />
     <table>
     <tr><td class="letter_bold">Rent Amount</td><td><asp:TextBox ID="rl_rent_amount" runat="server" Width="87px" />
         &nbsp;<asp:RegularExpressionValidator ID="reg_rl_rent_amount" runat="server" 
             ControlToValidate="rl_rent_amount" ErrorMessage="enter an amount" 
             ValidationGroup="vg_acc">
                        </asp:RegularExpressionValidator>
         &nbsp;<asp:RequiredFieldValidator ID="req_rl_rent_amount" runat="server" 
             ControlToValidate="rl_rent_amount" ErrorMessage="required" 
             ValidationGroup="vg_acc">
                          </asp:RequiredFieldValidator>
         </td></tr>
     <tr><td class="letter_bold">Rent will be paid every</td><td>
         <asp:DropDownList ID="ddl_rl_rent_paid_every" runat="server">
            
             <asp:ListItem   Selected=True Value="1">1st of Month</asp:ListItem>
             <asp:ListItem Value="2">2nd of Month</asp:ListItem>
             <asp:ListItem Value="3">3rd  of Month</asp:ListItem>
             <asp:ListItem Value="4">4th of Month</asp:ListItem>
             <asp:ListItem Value="5">5  of  Month</asp:ListItem>
             <asp:ListItem Value="6">6  of  Month</asp:ListItem>
             <asp:ListItem Value="7">7  of  Month</asp:ListItem>
             <asp:ListItem Value="8">8  of  Month</asp:ListItem>
             <asp:ListItem Value="9">9  of  Month</asp:ListItem>
             <asp:ListItem Value="10">10 of Month</asp:ListItem>
             <asp:ListItem Value="11">11 of Month</asp:ListItem>
             <asp:ListItem Value="12">12 of Month</asp:ListItem>
             <asp:ListItem Value="13">13 of Month</asp:ListItem>
             <asp:ListItem Value="14">14 of Month</asp:ListItem>
             <asp:ListItem Value="15">15 of Month</asp:ListItem>
             <asp:ListItem Value="16">16 of Month</asp:ListItem>
             <asp:ListItem Value="17">17 of Month</asp:ListItem>
             <asp:ListItem Value="18">18 of Month</asp:ListItem>
             <asp:ListItem Value="19">19 of Month</asp:ListItem>
             <asp:ListItem Value="20">20 of Month</asp:ListItem>
             <asp:ListItem Value="21">21 of Month</asp:ListItem>
             <asp:ListItem Value="22">22 of Month</asp:ListItem>
             <asp:ListItem Value="23">23 of Month</asp:ListItem>
             <asp:ListItem Value="24">24 of Month</asp:ListItem>
             <asp:ListItem Value="25">25 of Month</asp:ListItem>
             <asp:ListItem Value="26">26 of Month</asp:ListItem>
             <asp:ListItem Value="27">27 of Month</asp:ListItem>
             <asp:ListItem Value="28">28 of Month</asp:ListItem>
             <asp:ListItem Value="29">29 of Month</asp:ListItem>
             <asp:ListItem Value="30">30 of Month</asp:ListItem>
             <asp:ListItem Value="31">31 of Month</asp:ListItem>
             <asp:ListItem Value="32">1st monday of Month</asp:ListItem>
             <asp:ListItem Value="33">2nd monday of Month</asp:ListItem>
             <asp:ListItem Value="34">3rd monday of Month</asp:ListItem>
             <asp:ListItem Value="35">4th monday of Month</asp:ListItem>
             <asp:ListItem Value="36">1st tuesday of Month</asp:ListItem>
             <asp:ListItem Value="37">2nd tuesday of Month</asp:ListItem>
             <asp:ListItem Value="38">3rd tuesday of Month</asp:ListItem>
             <asp:ListItem Value="39">4th tuesday of Month</asp:ListItem>
             <asp:ListItem Value="40">1st wednesday of Month</asp:ListItem>
             <asp:ListItem Value="41">2nd wednesday of Month</asp:ListItem>
             <asp:ListItem Value="42">3rd wednesday of Month</asp:ListItem>
             <asp:ListItem Value="43">4th wednesday of Month</asp:ListItem>
             <asp:ListItem Value="44">1st thursday of Month</asp:ListItem>
             <asp:ListItem Value="45">2nd thursday of Month</asp:ListItem>
             <asp:ListItem Value="46">3rd thursday of Month</asp:ListItem>
             <asp:ListItem Value="47">4th thursday of Month</asp:ListItem>
             <asp:ListItem Value="48">1st friday of Month</asp:ListItem>
             <asp:ListItem Value="49">2nd friday of Month</asp:ListItem>
             <asp:ListItem Value="50">3rd friday of Month</asp:ListItem>
             <asp:ListItem Value="51">4th friday of Month</asp:ListItem>
             <asp:ListItem Value="52">1st saturday of Month</asp:ListItem>
             <asp:ListItem Value="53">2nd saturday of Month</asp:ListItem>
             <asp:ListItem Value="54">3rd saturday of Month</asp:ListItem>
             <asp:ListItem Value="55">4th saturday of Month</asp:ListItem>
             <asp:ListItem Value="56">1st sunday of Month</asp:ListItem>
             <asp:ListItem Value="57">2nd sunday of Month</asp:ListItem>
             <asp:ListItem Value="58">3rd sunday of Month</asp:ListItem>
             <asp:ListItem Value="59">4th sunday of Month</asp:ListItem>
             <asp:ListItem Value="60">Every 3 Months</asp:ListItem>
             <asp:ListItem Value="61">Every 6 Months</asp:ListItem>
             <asp:ListItem Value="62">Year</asp:ListItem>
             
         </asp:DropDownList></td></tr>
     
     <tr>
     <td colspan="2">
         <strong><span style="font-size: 10pt">Utilities includes</span></strong><br />
     <asp:CheckBox AutoPostBack=true CssClass="letter" ID="ta_electricity_inc" text="Electricity" runat="server" OnCheckedChanged="ta_electricity_inc_CheckedChanged" />
     <br />
     <asp:CheckBox AutoPostBack=true CssClass="letter" ID="ta_heat_inc" text="Heat" runat="server" OnCheckedChanged="ta_heat_inc_CheckedChanged" />
     <br />
     <asp:CheckBox AutoPostBack=true CssClass="letter" ID="ta_water_inc" text="Water" runat="server" OnCheckedChanged="ta_water_inc_CheckedChanged" />
     <br />
     <asp:CheckBox AutoPostBack=true CssClass="letter" ID="ta_water_tax_inc" text="Water tax" runat="server" OnCheckedChanged="ta_water_tax_inc_CheckedChanged" />
     <br />
     <asp:CheckBox AutoPostBack=true CssClass="letter" ID="ta_none" text="None ( utilities are not included in the lease )" runat="server" OnCheckedChanged="ta_none_CheckedChanged" />
     <br />
     
         <br />
         <span style="font-size: 10pt"><strong>Accomodations included</strong></span><br />
     <asp:CheckBox CssClass="letter" ID="ta_parking_inc" text="Parking" runat="server" />
     <br />
     <asp:CheckBox CssClass="letter" ID="ta_garage_inc" text="Garage" runat="server" />
     <br />
     <asp:CheckBox CssClass="letter" ID="ta_furnished_inc" text="Furnished" runat="server" />
     <br />
     <asp:CheckBox CssClass="letter" ID="ta_semi_furnished_inc" text="Semi-furnished" runat="server" />
     <br />
     
     </td></tr>
     
         
           <tr><td class="letter_bold" valign=top>Comments & extras</td>
                <td><asp:TextBox ID="ta_com" CssClass="letter" runat="server" TextMode="MultiLine" Width="389px"/>
                    <asp:RegularExpressionValidator ID="reg_ta_com" runat="server" 
                         ValidationGroup="vg_acc"
                          ControlToValidate="ta_com"
                        ErrorMessage="invalid text">
                        </asp:RegularExpressionValidator>
               </td>
            </tr>
       
      <tr><td colspan="2"><asp:Button ID="btn_previous_2" runat="server" 
              CssClass="letter_bold" Text="Previous" onclick="btn_previous_2_Click" />
          &nbsp;
          <asp:Button ID="btn_next_2" runat="server" Text="Next" 
              ValidationGroup="vg_acc"
              onclick="btn_next_2_Click" />
          </td></tr>
    <tr><td colspan="2">
     
      
     </td></tr>
     </table>
      
      </asp:View>
      
      
      
      
      
   
      
      
      <asp:View ID="View4" runat="server">
      
      <b>
          <br />
         <h3> CREATE COMMERCIAL LEASE - STEP 5</h3></b>    
          <br />
<br>
Lease terms and conditions
    <asp:HyperLink ID="main" runat="server" NavigateUrl="~/manager/home/home_main.aspx">main</asp:HyperLink><br />
<br />
<div>
<table>
<tr>
<td valign="top">
 Form of payment </td>
<td>
<asp:DropDownList ID="ddl_tt_form_of_payment" runat="server">
<asp:ListItem Value="0">Not specified</asp:ListItem>
<asp:ListItem Value="1">Personal check</asp:ListItem>
<asp:ListItem Value="1">Cashier's check </asp:ListItem>
<asp:ListItem Value="3">Cash</asp:ListItem>
<asp:ListItem Value="4">Money order</asp:ListItem>
<asp:ListItem Value="5">Credit card</asp:ListItem>
 <asp:ListItem Value="6">Other</asp:ListItem>
</asp:DropDownList></td>
</tr>
<tr>
<td valign="top">
Non-sufficient fund fee  &nbsp;</td>
<td>
<asp:TextBox ID="tt_nsf" Text="0" runat="server" Width="128px"></asp:TextBox></td>
</tr>
<tr>
<td  valign=top>Security deposit required</td>
<td>
<asp:RadioButtonList  AutoPostBack=true CssClass="letter" ID="tt_security_deposit" runat="server" OnSelectedIndexChanged="tt_security_deposit_SelectedIndexChanged">
<asp:ListItem Selected="True" Value="0">no</asp:ListItem>
<asp:ListItem Value="1">yes</asp:ListItem>
</asp:RadioButtonList>
<asp:Panel Visible=false ID="Panel_security_deposit" runat="server" >
&nbsp;&nbsp;&nbsp;amount <asp:TextBox runat=server  Text="0" ID="security_deposit_amount"></asp:TextBox></asp:Panel>
</td>
 </tr>
<tr><td valign=top>Guarantor required </td><td>
<asp:RadioButtonList  AutoPostBack=true CssClass="letter" ID="tt_guarantor" runat="server" OnSelectedIndexChanged="tt_guarantor_SelectedIndexChanged">
<asp:ListItem Selected="True" Value="0">no</asp:ListItem>
<asp:ListItem Value="1">yes</asp:ListItem>
</asp:RadioButtonList>
 <asp:Panel ID="panel_guarantor" Visible=false runat="server" >
 from the system &nbsp;<asp:DropDownList ID="ddl_guarantor_name_id"  AutoPostBack=true CssClass="letter" DataValueField="name_id" DataTextField="name_name" runat="server" OnSelectedIndexChanged="ddl_guarantor_name_id_SelectedIndexChanged" />
<br ><b>Or</b><br > add a guarantor <asp:Label Visible=false ID="txt" runat=server Font-Bold=true> * Disabled *</asp:Label>
 <!--  BEGIN GUARANTOR INFORMATIONS    -->
<table border="0" cellpadding="1" cellspacing="3" bgcolor="#ffffcc">
<tr><td class="letter_bold">first name</td>
<td><asp:TextBox ID="guarantor_fname" CssClass="letter" runat="server"/></td>
</tr>
 <tr>
<td class="letter_bold">last name</td>
<td><asp:TextBox ID="guarantor_lname" CssClass="letter" runat="server"/></td>
</tr>
<tr><td class="letter_bold">address</td>
<td><asp:TextBox ID="guarantor_addr" CssClass="letter" runat="server"></asp:TextBox></td>
</tr>
 <tr>
<td class="letter_bold">City</td>
<td><asp:TextBox ID="guarantor_addr_city" CssClass="letter" runat="server"/></td>
</tr>
<tr><td class="letter_bold">Pc</td>
<td><asp:TextBox ID="guarantor_addr_pc" CssClass="letter" runat="server"/></td>
 </tr>
<tr>
<td class="letter_bold">State/Prov</td>
<td>
<asp:TextBox ID="guarantor_addr_state" CssClass="letter" runat="server"/></td>
</tr>
<tr><td  class="letter_bold">Country</td>
<td colspan="3"><asp:DropDownList ID="ddl_guarantor_country_id" CssClass="letter" runat="server" DataTextField="country_name"
DataValueField="country_id"/></td>
</tr>
<tr><td class="letter_bold">Telephone</td>
<td><asp:TextBox ID="guarantor_tel" CssClass="letter" runat="server"/></td>
</tr>      
<tr> 
<td class="letter_bold">Tel. Work</td>
<td><asp:TextBox ID="guarantor_tel_work" CssClass="letter" runat="server"/></td>
</tr>
<tr><td class="letter_bold">Tel. Work ext.</td>
<td><asp:TextBox ID="guarantor_tel_work_ext" CssClass="letter" runat="server"/>                                   </td>
</tr>
<tr>
<td class="letter_bold">Cell</td>
 <td><asp:TextBox ID="guarantor_cell" CssClass="letter" runat="server"/></td>
</tr>
<tr><td class="letter_bold">Fax</td>
<td><asp:TextBox ID="guarantor_fax" CssClass="letter" runat="server"/></td>
</tr>
<tr>
<td class="letter_bold">Email</td>
<td><asp:TextBox ID="guarantor_email" CssClass="letter" runat="server"/></td>
</tr>
<tr><td class="letter_bold">Comments</td>
<td colspan="3"><asp:TextBox ID="guarantor_com" CssClass="letter" runat="server" TextMode="MultiLine" Width="200px"/></td>
</tr>
</table>  
            <!-- END GUARANTOR INFORMATIONS      -->
</asp:Panel>  
</td>
</tr>
<tr><td valign=top>Pets allowed</td><td>
<asp:RadioButtonList CssClass="letter" ID="tt_pets" runat="server">
<asp:ListItem Selected="True" Value="0">no</asp:ListItem>
<asp:ListItem Value="1">yes</asp:ListItem>
</asp:RadioButtonList></td></tr>
<tr valign="top">
<td valign=top>Tenant is responsible for maintenance</td>
<td>
<asp:RadioButtonList   CssClass="letter" ID="tt_maintenance" runat="server"  >
<asp:ListItem Selected="True" Value="0">no</asp:ListItem>
<asp:ListItem Value="1">yes</asp:ListItem>
</asp:RadioButtonList>
<asp:Panel  Visible=true ID="panel_specify_maintenance" runat="server">
&nbsp;&nbsp;&nbsp;specify maintenance allowed<br />&nbsp;<asp:TextBox runat=server ID="tt_specify_maintenance"  Width="300px" TextMode="MultiLine" ></asp:TextBox></asp:Panel>
 </td>
</tr>
<tr>
<td valign=top>Can tenant make improvement</td>
<td>
<asp:RadioButtonList   CssClass="letter" ID="tt_improvement" runat="server"  >
<asp:ListItem Selected="True" Value="0">no</asp:ListItem>
<asp:ListItem Value="1">yes</asp:ListItem>
</asp:RadioButtonList>
<asp:Panel Visible=true ID="panel_specify_improvement" runat="server">
&nbsp;&nbsp;&nbsp;specify improvement allowed<br />&nbsp;<asp:TextBox runat=server ID="tt_specify_improvement"  Width="300px" TextMode="MultiLine" ></asp:TextBox></asp:Panel>         
</td>             
</tr>
<tr>
<td valign=top>Notice to enter
</td>
<td>
<asp:RadioButtonList   CssClass="letter" ID="tt_notice_to_enter" runat="server">
<asp:ListItem Selected="True" Value="0">legal minimum</asp:ListItem>
<asp:ListItem Value="1">number of hours</asp:ListItem>
</asp:RadioButtonList>
<asp:Panel Visible=true ID="panel_specify_notice_to_enter" runat="server">
&nbsp;specify number of hours notice&nbsp;<asp:TextBox runat=server Width="100px" ID="tt_specify_number_of_hours"  ></asp:TextBox></asp:Panel>
</td>
</tr>
<tr>
<td valign=top>Who pay these insurances</td>
<td>
<table> 
<tr><td>tenant content</td> <td><asp:DropDownList CssClass="letter" ID="ddl_tt_tenant_content_ins" runat="server">
<asp:ListItem Selected="True" Value="0">not specified</asp:ListItem>
<asp:ListItem  Value="1">landlord</asp:ListItem>
<asp:ListItem  Value="2">tenant</asp:ListItem>
</asp:DropDownList></td></tr>
<tr><td>landlord content</td> <td><asp:DropDownList CssClass="letter" ID="ddl_tt_landlord_content_ins" runat="server">
<asp:ListItem Selected="True" Value="0">not specified</asp:ListItem>
<asp:ListItem  Value="1">landlord</asp:ListItem>
<asp:ListItem  Value="2">tenant</asp:ListItem>
</asp:DropDownList></td></tr>
<tr><td>personal injury on property</td> <td><asp:DropDownList CssClass="letter" ID="ddl_tt_injury_ins" runat="server">
<asp:ListItem Selected="True" Value="0">not specified</asp:ListItem>
<asp:ListItem  Value="1">landlord</asp:ListItem>
<asp:ListItem  Value="2">tenant</asp:ListItem>
</asp:DropDownList></td></tr>
<tr><td>lease premises</td> <td><asp:DropDownList ID="ddl_tt_premises_ins" CssClass="letter" runat="server">
<asp:ListItem Selected="True" Value="0">not specified</asp:ListItem>
<asp:ListItem  Value="1">landlord</asp:ListItem>
<asp:ListItem  Value="2">tenant</asp:ListItem>
</asp:DropDownList></td></tr>
</table>    
 </td>
</tr>
<tr><td valign=top>Additional Rules and Regulations</td>
<td><asp:TextBox runat=server Width="400px"  Height="200px" ID="tt_additional_terms" TextMode=MultiLine></asp:TextBox></td>
</tr>
</table>
   <br />   <br />   <br />
       <br />
    <asp:Button ID="btn_previous_3" runat="server" Text="Previous" 
        onclick="btn_previous_3_Click" />

       &nbsp;&nbsp;
    <asp:Button ID="btn_submit" runat="server" onclick="btn_submit_lease_Click" 
        Text="Submit" />

       </asp:View>
      
      
      
   
     
</asp:MultiView>








  <!--Hidden fields BEGIN SECTION 1-->
         <asp:HiddenField Visible="false" id="hd_home_id" runat="server" />
         <asp:HiddenField Visible="false" id="hd_unit_id" runat="server" />
         <asp:HiddenField Visible="false" id="hd_tu_date_begin" runat="server" />
         <asp:HiddenField Visible="false" id="hd_current_tu_id" runat="server" />
         <asp:HiddenField Visible="false" id="hd_tu_date_end" runat="server" />
          <asp:HiddenField Visible="false" id="hd_tenant_id" runat="server" />
         <asp:HiddenField Visible="false" id="hd_tenant_unit_id" runat="server" />
        
</asp:Content>

