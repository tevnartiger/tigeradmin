﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;


/// created by : Stanley Jocelyn
/// Date        : dec 29, 2008
////
/// </summary>
public partial class manager_lease_lease_com_add_1 : BasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            // by default date begin of the new lease is today
            //
            ddl_tu_date_begin_m.SelectedValue = DateTime.Now.Month.ToString();
            ddl_tu_date_begin_d.SelectedValue = DateTime.Now.Day.ToString();
            ddl_tu_date_begin_y.SelectedValue = DateTime.Now.Year.ToString();

            //int home_id = 1;// Convert.ToInt32(Request.QueryString["home_id"]);

            int unit_id = 0;

            tiger.PM h = new tiger.PM(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = h.getPMHomeComCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
            string link_to_unit = "";
            btn_continue.Enabled = true;
            panel_create_lease.Visible = true;

            txt_pending.InnerHtml = "";
            txt_message.InnerHtml = "";
            r_pendingleaseslist.Visible = true;

            if (home_count > 0)
            {
                ddl_home_id.Visible = true;
                int home_id = h.getPMHomeComFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

                // home_id hiddenfield
                hd_home_id.Value = Convert.ToString(home_id);

                link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                //Session["schema_id"]));

                ddl_home_id.DataSource = h.getPMHomeComList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                ddl_home_id.SelectedValue = Convert.ToString(home_id);
                ddl_home_id.DataBind();

                //*********************************************
                // DropDownList pour les Unit
                //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitComCount(Convert.ToInt32(Session["schema_id"]), home_id);

                if (unit_count > 0)
                {

                    unit_id = u.getUnitComFirstId(Convert.ToInt32(Session["schema_id"]), home_id);

                    hd_unit_id.Value = Convert.ToString(unit_id);

                    int count = 0;
                    // here we check the amount of pending leases
                    tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                    count = p.getCountPendingLeases(Convert.ToInt32(Session["schema_id"]), unit_id);


                    if (count > 0)
                    {
                        btn_continue.Enabled = false;
                        txt_pending.InnerHtml = "<strong><span style='color: #ff3300'>(1) You already have a pending lease ,<br /> please remove pending before creating another lease , ( link goes here)</span></strong> ";

                    }

                    //----- Now we show there is any pending lease in this unit
                    tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                    r_pendingleaseslist.DataSource = v.getPendingLeasesList(Convert.ToInt32(Session["schema_id"]), unit_id);
                    r_pendingleaseslist.DataBind();

                    ///------------------- 
                    ///
                    //link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                    //Session["schema_id"]));

                    ddl_unit_id.DataSource = u.getUnitComList(Convert.ToInt32(Session["schema_id"]), home_id);
                    ddl_unit_id.SelectedValue = Convert.ToString(unit_id);
                    ddl_unit_id.DataBind();


                    //get current tenant id
                    int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), unit_id);
                    //if there is a current tenant in the unit then get name(s)
                    if (temp_tenant_id > 0)
                    {
                        panel_current_tenant.Visible = true;
                        txt_current_tenant_name.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);

                        tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                        hd_current_tu_id.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), unit_id));
                       
                        tiger.Company com = new tiger.Company(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                        lbl_company_name.Text = com.getCompanyName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);


                        // hd_unit_id.Value = Convert.ToString(unit_id);
                    }

                   // there is no tenant in the unit currently - yes create a lease
                    else
                    {
                        //hidden fields
                        // hd_unit_id.Value = Convert.ToString(unit_id);
                        hd_current_tu_id.Value = "0";
                        btn_continue.Enabled = true;

                    }

                }
                // if we dont find a unit then there is no unit_id and tu_id
                else
                {
                    txt_message.InnerHtml = "There is no unit in this property -- add unit";
                    r_pendingleaseslist.Visible = false;
                    hd_current_tu_id.Value = "0";
                    hd_unit_id.Value = "0";
                    btn_continue.Enabled = false;
                    panel_create_lease.Visible = false;

                }
           }

       // If there's no Home don't show the Home dropdownlist
            else
            {

                txt_message.InnerHtml = "There is no property -- add a property";
                r_pendingleaseslist.Visible = false;
                btn_continue.Enabled = false;
                hd_current_tu_id.Value = "0";
                hd_unit_id.Value = "0";
                ddl_home_id.Visible = false;
                panel_create_lease.Visible = false;
                txt_link.InnerHtml = home_count + " <a href='home_add.aspx'>Add Home</a>&nbsp;&nbsp;<a href='owner_add.aspx'>Add Owner</a>&nbsp;&nbsp;<a href='janitor_add.aspx'>Add Janitor</a>&nbsp;&nbsp;";
            }

            //if the continu button is disable then there is a pending lease 
            // and it is not essesary to view the table of the last paid rent
            if (btn_continue.Enabled == false)
                tb_last_paid_rent.Visible = false;
            else
            {
               
                tb_last_paid_rent.Visible = true;
                int previous_lease_exist = 0;

                SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                SqlCommand cmd = new SqlCommand("prUnitMinimumLeaseDateBegin", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;

                try
                {
                    conn.Open();

                    SqlDataReader dr = null;
                    dr = cmd.ExecuteReader(CommandBehavior.SingleRow);


                    DateTime date_depart = new DateTime(); // minimum date of departure
                    DateTime last_date_paid = new DateTime();
                    DateTime date_begin = new DateTime();
                    DateTime min_date_begin = new DateTime();

                    while (dr.Read() == true)
                    {
                        
                        date_depart = Convert.ToDateTime(dr["rp_due_date"]);
                        last_date_paid = Convert.ToDateTime(dr["rp_paid_date"]);

                        lbl_min_date_begin.Text = date_depart.Month.ToString() + "/" + date_depart.Day.ToString() + "/" + date_depart.Year.ToString();
                        lbl_last_date_paid.Text = last_date_paid.Month.ToString() + "/" + last_date_paid.Day.ToString() + "/" + last_date_paid.Year.ToString();

                        if (dr["minleasedatebegin"] == DBNull.Value || dr["minleasedatebegin"].ToString() == String.Empty)
                        {
                            lbl_min_date_begin2.Text = "";
                            tiger.Date d = new tiger.Date();

                            // ACHANGER ET METTRE NOW
                            min_date_begin = Convert.ToDateTime(d.DateCulture("12", "30", "1959", Convert.ToString(Session["_lastCulture"])));
                        }
                        else
                        {
                            min_date_begin = Convert.ToDateTime(dr["minleasedatebegin"]);
                            lbl_min_date_begin2.Text = min_date_begin.Month.ToString() + "/" + min_date_begin.Day.ToString() + "/" + min_date_begin.Year.ToString();
                        }

                        //by default the date of departure must be at least 1 day greater  than
                        // the last due date
                        date_depart = min_date_begin.AddDays(1);
                        date_begin = date_depart.AddDays(1);

                        ddl_tu_date_end_m.SelectedValue = date_depart.Month.ToString();
                        ddl_tu_date_end_d.SelectedValue = date_depart.Day.ToString();

                        // if there is no previuos tenant therefore we don't have a previous
                        //departure date
                        if (date_depart.Year < 1900)
                            ddl_tu_date_end_y.SelectedIndex = 1;
                        else
                            ddl_tu_date_end_y.SelectedValue = date_depart.Year.ToString();


                        // if no currently with tenants ...
                        // therefore no departure date
                        //  date_depart = min_date_begin.AddDays(1); ( does not apply )
                        // date_begin = date_depart.AddDays(1); ( does not apply )
                        //
                        // (min_date_begin) is the greatest among last due date , last date end and
                        //   current date begin
                        //
                        // ... here the , when no currently with
                        //           ( min_date_begin ) isthe last date end ,

                        if (hd_current_tu_id.Value == "0")
                        {
                            date_begin = date_begin.AddDays(-1);  // equal to date_depart
                        }



                        //by default the lease start date must be at least 1 day greater  than
                        // of depart of the old lease
                        ddl_tu_date_begin_m.SelectedValue = date_begin.Month.ToString();
                        ddl_tu_date_begin_d.SelectedValue = date_begin.Day.ToString();
                        ddl_tu_date_begin_y.SelectedValue = date_begin.Year.ToString();

                        previous_lease_exist = 1 ;


                        // if there is a current tenant  but no payment has been made by this tenant
                        // and there is no previous lease in the unit ,then the datareader returns nothing.
                        // Therefore the last rent due date is the start of lease
                        // The date of departure and the  new lease start date 
                        // is the ( start of lease + 1 day  and start of lease + 2) at least

                        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                        int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), unit_id);
                   
                        if (temp_tenant_id > 0 && dr.HasRows == false)
                        {
                            Label1.Text = dr.HasRows.ToString();
                            min_date_begin = u.getLeaseDateBegin(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);
                            lbl_min_date_begin.Text = min_date_begin.Month.ToString() + "/" + min_date_begin.Day.ToString() + "/" + min_date_begin.Year.ToString();

                            min_date_begin = min_date_begin.AddDays(1);
                            lbl_last_date_paid.Text = "NONE";

                            lbl_min_date_begin2.Text = min_date_begin.Month.ToString() + "/" + min_date_begin.Day.ToString() + "/" + min_date_begin.Year.ToString();

                            ddl_tu_date_end_m.SelectedValue = min_date_begin.Month.ToString();
                            ddl_tu_date_end_d.SelectedValue = min_date_begin.Day.ToString();
                            ddl_tu_date_end_y.SelectedValue = min_date_begin.Year.ToString();
                        }

                    }
                }

                finally
                {
                    conn.Close();
                }

            }
            if (panel_current_tenant.Visible == false)
                tb_last_paid_rent.Visible = false;
        }




    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {

        Label1.Text = "";
        lbl_company_name.Text = "";

        // by default date begin of the new lease is today
        //
        ddl_tu_date_begin_m.SelectedValue = DateTime.Now.Month.ToString();
        ddl_tu_date_begin_d.SelectedValue = DateTime.Now.Day.ToString();
        ddl_tu_date_begin_y.SelectedValue = DateTime.Now.Year.ToString();


        //  if (!Page.IsPostBack)
        //  { 
        txt_pending.InnerHtml = "";
        txt_message.InnerHtml = "";
        btn_continue.Enabled = true;
        txt_current_tenant_name.InnerHtml = "";
        r_pendingleaseslist.Visible = true;
        panel_create_lease.Visible = true;

        ddl_home_id.Visible = true;

        hd_home_id.Value = Convert.ToString(ddl_home_id.SelectedValue);
        tiger.Unit u = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        int unit_id = u.getUnitComFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
        //change list of unit id

        if (unit_id > 0)
        {
            ddl_unit_id.Visible = true;
            //first dispose of old ddl_unit
            ddl_unit_id.Dispose();
            ddl_unit_id.DataSource = u.getUnitComList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
            ddl_unit_id.DataBind();

            hd_unit_id.Value = Convert.ToString(unit_id);

            int count = 0;
            // here we check the amount of pending leases
            tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            count = p.getCountPendingLeases(Convert.ToInt32(Session["schema_id"]), unit_id);


            if (count > 0)
            {
                btn_continue.Enabled = false;
                txt_pending.InnerHtml = "<strong><span style='color: #ff3300'>(2) You already have a pending lease , <br /> please remove pending before creating another lease , ( link goes here)</span></strong> ";
            }

            //----- pending lease for this unit
            tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            r_pendingleaseslist.DataSource = v.getPendingLeasesList(Convert.ToInt32(Session["schema_id"]), unit_id);

            r_pendingleaseslist.DataBind();

            ///------------------- 


            tiger.Lease l = new tiger.Lease(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            {
                //get current tenant id
                int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), unit_id);
                //if there is a current tenant in the unit then get name(s)
                if (temp_tenant_id > 0)
                {
                    panel_current_tenant.Visible = true;
                    txt_current_tenant_name.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);

                    tiger.Company com = new tiger.Company(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                    lbl_company_name.Text = com.getCompanyName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);


                    tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                    hd_current_tu_id.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), unit_id));
                }

                // if there is no tenant in the unit currently - yes create a lease
                else
                {
                    panel_current_tenant.Visible = false;
                    hd_current_tu_id.Value = "0";
                    //  r_pendingleaseslist.Visible = false;
                    btn_continue.Enabled = true;
                }
                //hidden fields
            }
            //  else
            {
                //  hd_current_tu_id.Value = "0";
                // hd_unit_id.Value = Convert.ToString(ddl_unit_id.SelectedValue);
            }
        }
        else
        {
            txt_message.InnerHtml = "There is no unit in this property -- add unit";
            ddl_unit_id.Visible = false;
            panel_create_lease.Visible = false;
            r_pendingleaseslist.Visible = false;
            //  txt_link.InnerHtml = "<a href='unit_add.aspx?home_id='>Add Unit</a>";
            hd_current_tu_id.Value = "0";
            hd_unit_id.Value = "0";
            btn_continue.Enabled = false;

        }
        //   }// fin if not postback

        //if the continu button is disable then there is a pending lease 
        // and it is not essesary to view the table of the last paid rent
        if (btn_continue.Enabled == false)
            tb_last_paid_rent.Visible = false;

        else
        {
            tb_last_paid_rent.Visible = true;
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prUnitMinimumLeaseDateBegin", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;

            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                DateTime date_depart = new DateTime(); // minimum date of departure
                DateTime last_date_paid = new DateTime();
                DateTime date_begin = new DateTime();
                DateTime min_date_begin = new DateTime();

                while (dr.Read() == true)
                {

                    

                    date_depart = Convert.ToDateTime(dr["rp_due_date"]);
                    last_date_paid = Convert.ToDateTime(dr["rp_paid_date"]);

                    lbl_min_date_begin.Text = date_depart.Month.ToString() + "/" + date_depart.Day.ToString() + "/" + date_depart.Year.ToString();
                    lbl_last_date_paid.Text = last_date_paid.Month.ToString() + "/" + last_date_paid.Day.ToString() + "/" + last_date_paid.Year.ToString();

                 
                    if (dr["minleasedatebegin"] == DBNull.Value || dr["minleasedatebegin"].ToString() == String.Empty)
                    {
                        lbl_min_date_begin2.Text = "";
                        tiger.Date d = new tiger.Date();
                        min_date_begin = Convert.ToDateTime(d.DateCulture("1", "1", "1960", Convert.ToString(Session["_lastCulture"])));
                    }
                    else
                    {
                        min_date_begin = Convert.ToDateTime(dr["minleasedatebegin"]);
                        lbl_min_date_begin2.Text = min_date_begin.Month.ToString() + "/" + min_date_begin.Day.ToString() + "/" + min_date_begin.Year.ToString();
                    }


                    //by default the date of departure must be at least 1 day greater  than
                    // the last due date
                    date_depart = min_date_begin.AddDays(1);
                    date_begin = date_depart.AddDays(1);

                    ddl_tu_date_end_m.SelectedValue = date_depart.Month.ToString();
                    ddl_tu_date_end_d.SelectedValue = date_depart.Day.ToString();


                    // if there is no previuos tenant therefore we don't have a previous
                    //departure date
                    if (date_depart.Year < 1900)
                        ddl_tu_date_end_y.SelectedIndex = 1;
                    else
                        ddl_tu_date_end_y.SelectedValue = date_depart.Year.ToString();



                    // if no currently with tenants ...
                    // therefore no departure date
                    //  date_depart = min_date_begin.AddDays(1); ( does not apply )
                    // date_begin = date_depart.AddDays(1); ( does not apply )
                    //
                    // (min_date_begin) is the greatest among last due date , last date end and
                    //   current date begin
                    //
                    // ... here the , when no currently with
                    //           ( min_date_begin ) isthe last date end ,

                    if (hd_current_tu_id.Value == "0")
                    {
                        date_begin = date_begin.AddDays(-1);  // equal to date_depart
                    }


                    //by default the lease start date must be at least 1 day greater  than
                    // of depart of the old lease
                    ddl_tu_date_begin_m.SelectedValue = date_begin.Month.ToString();
                    ddl_tu_date_begin_d.SelectedValue = date_begin.Day.ToString();
                    ddl_tu_date_begin_y.SelectedValue = date_begin.Year.ToString();

                }


                // if there is a current tenant  but no payment has been made by this tenant
                // and there is no previous lease in the unit ,then the datareader returns nothing.
                // Therefore the last rent due date is the start of lease
                // The date of departure and the  new lease start date 
                // is the ( start of lease + 1 day  and start of lease + 2) at least

                int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), unit_id);
                if (temp_tenant_id > 0 && dr.HasRows == false)
                {
                    Label1.Text = dr.HasRows.ToString();
                    min_date_begin = u.getLeaseDateBegin(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);
                    lbl_min_date_begin.Text = min_date_begin.Month.ToString() + "/" + min_date_begin.Day.ToString() + "/" + min_date_begin.Year.ToString();

                    min_date_begin = min_date_begin.AddDays(1);
                    lbl_last_date_paid.Text = "NONE";

                    lbl_min_date_begin2.Text = min_date_begin.Month.ToString() + "/" + min_date_begin.Day.ToString() + "/" + min_date_begin.Year.ToString();

                    ddl_tu_date_end_m.SelectedValue = min_date_begin.Month.ToString();
                    ddl_tu_date_end_d.SelectedValue = min_date_begin.Day.ToString();
                    ddl_tu_date_end_y.SelectedValue = min_date_begin.Year.ToString();
                }
            }

            finally
            {
                conn.Close();
            }

        }

        if (panel_current_tenant.Visible == false)
            tb_last_paid_rent.Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_unit_id_SelectedIndexChanged(object sender, EventArgs e)
    {
            Label1.Text = "";
            lbl_company_name.Text = "";

            // by default date begin of the new lease is today
            //
            ddl_tu_date_begin_m.SelectedValue = DateTime.Now.Month.ToString();
            ddl_tu_date_begin_d.SelectedValue = DateTime.Now.Day.ToString();
            ddl_tu_date_begin_y.SelectedValue = DateTime.Now.Year.ToString();


            // label_added_unit.Text = ddl_unit_id.SelectedItem.Text;
            tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            //get current tenant
            //get current tenant id
            //get current tenant id
            btn_continue.Enabled = true;
            txt_pending.InnerHtml = "";
            txt_message.InnerHtml = "";
            txt_current_tenant_name.InnerHtml = "";
            r_pendingleaseslist.Visible = true;
            panel_create_lease.Visible = true;

                tiger.Lease l = new tiger.Lease(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            
                hd_unit_id.Value = Convert.ToString(ddl_unit_id.SelectedValue);

                int count = 0;
                // here we check the amount of pending leases
                tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                count = p.getCountPendingLeases(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_unit_id.SelectedValue));


                if (count > 0)
                {
                    btn_continue.Enabled = false;
                    txt_pending.InnerHtml = "<strong><span style='color: #ff3300'>(3) You already have a pending lease ,<br />please remove pending before creating another lease , ( link goes here)</span></strong> ";

                }

                //----- Now we check if there is any pending lease in this unit
                tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                r_pendingleaseslist.DataSource = v.getPendingLeasesList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_unit_id.SelectedValue));
                r_pendingleaseslist.DataBind();

                ///------------------- 

                int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_unit_id.SelectedValue));
                //if there is a current tenant in the unit then get name(s)
                if (temp_tenant_id > 0)
                {
                    panel_current_tenant.Visible = true;
                    txt_current_tenant_name.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);

                    tiger.Company com = new tiger.Company(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                    lbl_company_name.Text = com.getCompanyName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);


                    //tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                    tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                    hd_current_tu_id.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), Convert.ToInt32(ddl_unit_id.SelectedValue)));

                }

                // if there is no tenant in the unit - yes create a lease
                else
                {
                    panel_current_tenant.Visible = false;
                    btn_continue.Enabled = true;
                    hd_current_tu_id.Value = "0";
                }

            
            


            //if the continu button is disable then there is a pending lease 
            // and it is not essesary to view the table of the last paid rent
            if (btn_continue.Enabled == false)
                tb_last_paid_rent.Visible = false;

            else
            {
                tb_last_paid_rent.Visible = true;
                SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                SqlCommand cmd = new SqlCommand("prUnitMinimumLeaseDateBegin", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = Convert.ToString(ddl_unit_id.SelectedValue);

                try
                {
                    conn.Open();

                    SqlDataReader dr = null;
                    dr = cmd.ExecuteReader(CommandBehavior.SingleRow);
                   
                    DateTime date_depart = new DateTime(); // minimum date of departure
                    DateTime last_date_paid = new DateTime();
                    DateTime date_begin = new DateTime();
                    DateTime min_date_begin = new DateTime();

                    while (dr.Read() == true)
                    {
                        
                        date_depart = Convert.ToDateTime(dr["rp_due_date"]);
                        last_date_paid = Convert.ToDateTime(dr["rp_paid_date"]);

                       
                        lbl_min_date_begin.Text = date_depart.Month.ToString() + "/" + date_depart.Day.ToString() + "/" + date_depart.Year.ToString();
                        lbl_last_date_paid.Text = last_date_paid.Month.ToString() + "/" + last_date_paid.Day.ToString() + "/" + last_date_paid.Year.ToString();


                        if (dr["minleasedatebegin"] == DBNull.Value || dr["minleasedatebegin"].ToString() == String.Empty)
                        {
                            lbl_min_date_begin2.Text = "";
                            tiger.Date d = new tiger.Date();
                            min_date_begin = Convert.ToDateTime(d.DateCulture("1", "1", "1960", Convert.ToString(Session["_lastCulture"])));
                        }
                        else
                        {
                            min_date_begin = Convert.ToDateTime(dr["minleasedatebegin"]);
                            lbl_min_date_begin2.Text = min_date_begin.Month.ToString() + "/" + min_date_begin.Day.ToString() + "/" + min_date_begin.Year.ToString();
                        }

                        //by default the date of departure must be at least 1 day greater  than
                        // the last due date
                        date_depart = min_date_begin.AddDays(1);
                        date_begin = date_depart.AddDays(1);

                        ddl_tu_date_end_m.SelectedValue = date_depart.Month.ToString();
                        ddl_tu_date_end_d.SelectedValue = date_depart.Day.ToString();

                        // if there is no previuos tenant therefore we don't have a previous
                        //departure date , default date when stored procedure don't return a value
                        if (date_depart.Year < 1900)
                            ddl_tu_date_end_y.SelectedIndex = 1;
                        else
                            ddl_tu_date_end_y.SelectedValue = date_depart.Year.ToString();



                        // if no currently with tenants ...
                        // therefore no departure date
                        //  date_depart = min_date_begin.AddDays(1); ( does not apply )
                        // date_begin = date_depart.AddDays(1); ( does not apply )
                        //
                        // (min_date_begin) is the greatest among last due date , last date end and
                        //   current date begin
                        //
                        // ... here the , when no currently with
                        //           ( min_date_begin ) isthe last date end ,

                        if (hd_current_tu_id.Value == "0")
                        {
                            date_begin = date_begin.AddDays(-1);  // equal to date_depart
                        }


                        //by default the lease start date must be at least 1 day greater  than
                        // of depart of the old lease
                        ddl_tu_date_begin_m.SelectedValue = date_begin.Month.ToString();
                        ddl_tu_date_begin_d.SelectedValue = date_begin.Day.ToString();
                        ddl_tu_date_begin_y.SelectedValue = date_begin.Year.ToString();
                    }

                   // if there is a current tenant  but no payment has been made by this tenant
                   // and there is no previous lease in the unit ,then the datareader returns nothing.
                   // Therefore the last rent due date is the start of lease
                   // The date of departure and the  new lease start date 
                   // is the ( start of lease + 1 day  and start of lease + 2) at least

                    if (temp_tenant_id > 0 && dr.HasRows == false)
                    {
                        Label1.Text = dr.HasRows.ToString();
                        min_date_begin = u.getLeaseDateBegin(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);
                        lbl_min_date_begin.Text = min_date_begin.Month.ToString() + "/" + min_date_begin.Day.ToString() + "/" + min_date_begin.Year.ToString();

                        min_date_begin = min_date_begin.AddDays(1);
                        lbl_last_date_paid.Text = "NONE";

                        lbl_min_date_begin2.Text = min_date_begin.Month.ToString() + "/" + min_date_begin.Day.ToString() + "/" + min_date_begin.Year.ToString();

                        ddl_tu_date_end_m.SelectedValue = min_date_begin.Month.ToString();
                        ddl_tu_date_end_d.SelectedValue = min_date_begin.Day.ToString();
                        ddl_tu_date_end_y.SelectedValue = min_date_begin.Year.ToString();
                    }
                    
                }
                finally
                {
                    conn.Close();
                }
            }

            if (panel_current_tenant.Visible == false)
                tb_last_paid_rent.Visible = false;      
    }

    protected void btn_continue_Onclick(object sender, EventArgs e)
    {

        lbl_company_name.Text = "";

        DateTime date_depart = new DateTime(); // minimum date of departure
        DateTime last_date_paid = new DateTime();
        DateTime last_due_date = new DateTime();
        DateTime date_begin = new DateTime();
        DateTime min_date_begin = new DateTime();
        int previous_lease_exist = 0; // check if there is a previous lease with minimum date begin in
                                      // the specified unit , if there is a result in the TRY section it
                                      // means that t

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prUnitMinimumLeaseDateBegin", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        //Add the params
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = Convert.ToString(ddl_unit_id.SelectedValue);

        try
        {
            conn.Open();

            SqlDataReader dr = null;
            dr = cmd.ExecuteReader(CommandBehavior.SingleRow);
            

            while (dr.Read() == true)
            {

                date_depart = Convert.ToDateTime(dr["rp_due_date"]);
                last_due_date = Convert.ToDateTime(dr["rp_due_date"]);
                last_date_paid = Convert.ToDateTime(dr["rp_paid_date"]);

                lbl_min_date_begin.Text = date_depart.Month.ToString() + "/" + date_depart.Day.ToString() + "/" + date_depart.Year.ToString();
                lbl_last_date_paid.Text = last_date_paid.Month.ToString() + "/" + last_date_paid.Day.ToString() + "/" + last_date_paid.Year.ToString();

                if (dr["minleasedatebegin"] == DBNull.Value || dr["minleasedatebegin"].ToString() == String.Empty)
                {
                    lbl_min_date_begin2.Text = "";
                    tiger.Date d = new tiger.Date();
                    min_date_begin = Convert.ToDateTime(d.DateCulture("1","1", "1960", Convert.ToString(Session["_lastCulture"])));
                }
                else
                {
                    min_date_begin = Convert.ToDateTime(dr["minleasedatebegin"]);
                    lbl_min_date_begin2.Text = min_date_begin.Month.ToString() + "/" + min_date_begin.Day.ToString() + "/" + min_date_begin.Year.ToString();
                }
                            

                //by default the date of departure must be at least 1 day greater  than
                // the last due date
                date_depart = min_date_begin.AddDays(1);
                date_begin = date_depart.AddDays(1);
                previous_lease_exist = 1;

            }

        }

        finally
        {
            conn.Close();
        }




        DateTime tu_date_begin = new DateTime();
        DateTime tu_date_end = new DateTime();

        tiger.Date dv = new tiger.Date();
        tu_date_begin = Convert.ToDateTime(dv.DateCulture(ddl_tu_date_begin_m.SelectedValue, ddl_tu_date_begin_d.SelectedValue, ddl_tu_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        tu_date_end = Convert.ToDateTime(dv.DateCulture(ddl_tu_date_end_m.SelectedValue, ddl_tu_date_end_d.SelectedValue, ddl_tu_date_end_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

       
        // no currently with tenants
        if (hd_current_tu_id.Value == "0")
        {
            if (tu_date_begin > min_date_begin)
                Server.Transfer("lease_com_add_2.aspx", true);
        }
        else
        {
            if (tu_date_begin > tu_date_end && min_date_begin < tu_date_end && min_date_begin < tu_date_begin )
            Server.Transfer("lease_com_add_2.aspx", true);
        }



        Label1.Text = "NO TRANSACTION  HAS BEEN MADE IN THIS UNIT ,I.E : NO RENT PAYMENT , FOR THE FIRST LEASE OF THIS UNIT";
      
    }


}
