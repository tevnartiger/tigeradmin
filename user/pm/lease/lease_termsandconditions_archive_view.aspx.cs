﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.NameObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : may 20 , 2008
/// </summary>
public partial class manager_lease_lease_termsandconditions_archive_view : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!RegEx.IsInteger(Request.QueryString["tt_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
            NameObjectAuthorization termsAuthorization = new NameObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


            // CHECK IF HOME AND UNIT IS AUTHORIZED
            ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
            if (!termsAuthorization.TermsAndConditionsArchive(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["tt_id"]), Convert.ToInt32(Session["name_id"]), 6))
            {
                Session.Abandon();
                Response.Redirect("~/login.aspx");
            }
            ///////// SECURITY OBJECT CHECK  END ////////////////////////////////


            if (!Page.IsPostBack)
            {
                SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                SqlCommand cmd = new SqlCommand("prTermsAndConditionsPreviousView", conn);
                SqlCommand cmd2 = new SqlCommand("prHomeUnitView", conn);

                cmd.CommandType = CommandType.StoredProcedure;

                //Add the params
                // cmd.Parameters.Add("@return_guarantor_name_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@tt_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tt_id"]);
              


                try
                {
                    conn.Open();

                    SqlDataReader dr = null;
                    dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                    int guarantor_name_id;
                    /*
                    tiger.Name n = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    ddl_guarantor_name_id.DataSource = n.getNameList(Convert.ToInt32(Session["schema_id"]), 0);
                    ddl_guarantor_name_id.DataBind();
                    ddl_guarantor_name_id.Items.Insert(0, "select a person");
                    */
                    while (dr.Read() == true)
                    {

                        //  lbl_current_tt_date_begin.Text = String.Format("{0:d}", dr["tt_date_begin"]);



                        DateTime date_begin = new DateTime();
                        DateTime date_end = new DateTime();

                        date_begin = Convert.ToDateTime(dr["tt_date_begin"]);
                        if (dr["tt_date_end"] != DBNull.Value)
                        {
                            date_end = Convert.ToDateTime(dr["tt_date_end"]);
                            lbl_tt_date_end.Text = date_end.Month.ToString() + "-" + date_end.Day.ToString() + "-" + date_end.Year.ToString();
                        }

                        lbl_tt_date_begin.Text = date_begin.Month.ToString() + "-" + date_begin.Day.ToString() + "-" + date_begin.Year.ToString();
                        




                        if (dr["tt_guarantor"].ToString() == "1")
                            panel_guarantor.Visible = true;
                        else
                            panel_guarantor.Visible = false;

                        tt_guarantor.SelectedValue = dr["tt_guarantor"].ToString();

                        guarantor_name.Text = dr["tt_guarantor_name"].ToString();

                        if (guarantor_name.Text == "")
                            guarantor_name.Text = Resources.Resource.lbl_none;

                        if (Convert.ToInt32(dr["tt_guarantor_name_id"]) > 0)
                        {
                            // ddl_guarantor_name_id.SelectedValue = Convert.ToString(dr["tt_guarantor_name_id"]);
                            /*  guarantor_name_fname.Enabled = false;
                              guarantor_name_lname.Enabled = false;
                              guarantor_name_addr.Enabled = false;
                              guarantor_name_addr_city.Enabled = false;
                              guarantor_name_addr_pc.Enabled = false;
                              guarantor_name_addr_state.Enabled = false;
                              ddl_guarantor_country_id.Enabled = false;
                              guarantor_name_tel.Enabled = false;
                              guarantor_name_tel_work.Enabled = false;
                              guarantor_name_tel_work_ext.Enabled = false;
                              guarantor_name_cell.Enabled = false;
                              guarantor_name_fax.Enabled = false;
                              guarantor_name_email.Enabled = false;
                              guarantor_name_com.Enabled = false;
                              txt.Visible = true; */
                        }
                        else
                        {
                            /*  guarantor_name_fname.Enabled = true;
                              guarantor_name_lname.Enabled = true;
                              guarantor_name_addr.Enabled = true;
                              guarantor_name_addr_city.Enabled = true;
                              guarantor_name_addr_pc.Enabled = true;
                              guarantor_name_addr_state.Enabled = true;
                              ddl_guarantor_country_id.Enabled = true;
                              guarantor_name_tel.Enabled = true;
                              guarantor_name_tel_work.Enabled = true;
                              guarantor_name_tel_work_ext.Enabled = true;
                              guarantor_name_cell.Enabled = true;
                              guarantor_name_fax.Enabled = true;
                              guarantor_name_email.Enabled = true;
                              guarantor_name_com.Enabled = true;
                              txt.Visible = false; */

                        }



                        //  dr["tt_form_of_payment"].ToString();

                        switch (Convert.ToInt32(dr["tt_form_of_payment"]))
                        {
                            case 0: tt_form_of_payment.Text = Resources.Resource.lbl_not_specified;
                                break;
                            case 1: tt_form_of_payment.Text = Resources.Resource.lbl_personal_check;
                                break;
                            case 2: tt_form_of_payment.Text = Resources.Resource.lbl_cashier_check;
                                break;
                            case 3: tt_form_of_payment.Text = Resources.Resource.lbl_cash;
                                break;
                            case 4: tt_form_of_payment.Text = Resources.Resource.lbl_money_order;
                                break;
                            case 5: tt_form_of_payment.Text = Resources.Resource.lbl_credit_card;
                                break;
                            case 6: tt_form_of_payment.Text = Resources.Resource.lbl_other;
                                break;

                        }


                        decimal nsf, late_fee, security_deposit_amount;

                        if (dr["tt_nsf"].ToString() != "")
                            nsf = Convert.ToDecimal(dr["tt_nsf"]);
                        else
                            nsf = 0;

                        if (dr["tt_late_fee"].ToString() != "")
                            late_fee = Convert.ToDecimal(dr["tt_late_fee"]);
                        else
                            late_fee = 0;

                        if (dr["tt_security_deposit_amount"].ToString() != "")
                            security_deposit_amount = Convert.ToDecimal(dr["tt_security_deposit_amount"]);
                        else
                            security_deposit_amount = 0;


                        tt_nsf.Text = String.Format("{0:0.00}", nsf);
                        tt_late_fee.Text = String.Format("{0:0.00}", late_fee);


                        tt_security_deposit.SelectedValue = dr["tt_security_deposit"].ToString();

                        tt_security_deposit_amount.Text = String.Format("{0:0.00}", security_deposit_amount);



                        tt_pets.SelectedValue = dr["tt_pets"].ToString();
                        tt_maintenance.SelectedValue = dr["tt_maintenance"].ToString();


                        tt_specify_maintenance.Text = dr["tt_specify_maintenance"].ToString();
                        if (tt_specify_maintenance.Text == "")
                            tt_specify_maintenance.Text = Resources.Resource.lbl_none;

                        tt_improvement.SelectedValue = dr["tt_improvement"].ToString();

                        tt_specify_improvement.Text = dr["tt_specify_improvement"].ToString();
                        if (tt_specify_improvement.Text == "")
                            tt_specify_improvement.Text = Resources.Resource.lbl_none;


                        tt_notice_to_enter.SelectedValue = dr["tt_notice_to_enter"].ToString();



                        if (dr["tt_specify_number_of_hours"].ToString() == "0")
                            tt_specify_number_of_hours.Text = "";
                        else
                            tt_specify_number_of_hours.Text = dr["tt_specify_number_of_hours"].ToString();




                        switch (Convert.ToInt32(dr["tt_tenant_content_ins"]))
                        {

                            case 0: tt_tenant_content_ins.Text = Resources.Resource.lbl_not_specified;
                                break;
                            case 1: tt_tenant_content_ins.Text = Resources.Resource.lbl_landlord;
                                break;
                            case 2: tt_tenant_content_ins.Text = Resources.Resource.lbl_tenant;
                                break;

                        }


                        switch (Convert.ToInt32(dr["tt_landlord_content_ins"]))
                        {

                            case 0: tt_landlord_content_ins.Text = Resources.Resource.lbl_not_specified;
                                break;
                            case 1: tt_landlord_content_ins.Text = Resources.Resource.lbl_landlord;
                                break;
                            case 2: tt_landlord_content_ins.Text = Resources.Resource.lbl_tenant;
                                break;

                        }


                        switch (Convert.ToInt32(dr["tt_injury_ins"]))
                        {

                            case 0: tt_injury_ins.Text = Resources.Resource.lbl_not_specified;
                                break;
                            case 1: tt_injury_ins.Text = Resources.Resource.lbl_landlord;
                                break;
                            case 2: tt_injury_ins.Text = Resources.Resource.lbl_tenant;
                                break;

                        }


                        switch (Convert.ToInt32(dr["tt_premises_ins"]))
                        {

                            case 0: tt_premises_ins.Text = Resources.Resource.lbl_not_specified;
                                break;
                            case 1: tt_premises_ins.Text = Resources.Resource.lbl_landlord;
                                break;
                            case 2: tt_premises_ins.Text = Resources.Resource.lbl_tenant;
                                break;

                        }



                        tt_additional_terms.Text = dr["tt_additional_terms"].ToString();
                        if (tt_additional_terms.Text == "")
                            tt_additional_terms.Text = Resources.Resource.lbl_none;


                    }



                }

                finally
                {

                   // conn.Close();
                }




                cmd2.CommandType = CommandType.StoredProcedure;

                try
                {

                    cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                    cmd2.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);


                    SqlDataAdapter da = new SqlDataAdapter(cmd2);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    r_HomeUnitView.DataSource = ds;
                    r_HomeUnitView.DataBind();
                }
                finally
                {
                    conn.Close();
                }
            }
        
    }
    
}
