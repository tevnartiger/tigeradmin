﻿using System;
using System.IO;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class manager_notice_notice_late_rent_template_letters : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        

        if (!Page.IsPostBack)
        {
                   
            // setting the culture of the Obout editor
            tbx_notice.PathPrefix = "../../App_themes/Obout/editor/";
            tbx_notice.Language = tiger.PageUtility.getLanguage(Session["_lastCulture"].ToString());

            tb_successfull_confirmation.Visible = false;

            String txt_first_notice = GetTemplate(Session["_lastCulture"].ToString(), 1).Replace("\n", "<br/>");
            ddl_language.SelectedValue = Session["_lastCulture"].ToString();

            txt_first_notice = txt_first_notice.Replace("((1))","(("+Resources.Resource.lbl_tag_unpaid_amount+"))");
            txt_first_notice = txt_first_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
            txt_first_notice = txt_first_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
            txt_first_notice = txt_first_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
            txt_first_notice = txt_first_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
            txt_first_notice = txt_first_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
            txt_first_notice = txt_first_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

            if (ddl_notice.SelectedValue == "1")
            {
                tbx_notice.Content = txt_first_notice;

            }

            if (ddl_notice.SelectedValue == "2")
            {
                string notice = "";
                notice = GetTemplate(Session["_lastCulture"].ToString(), 2).Replace("\n", "<br/>");
                  
                   // Resources.Resource.lbl_second_notice_text.Replace("\n", "<br/>"); ;


                notice = notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                notice = notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                notice = notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                notice = notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                notice = notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                notice = notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                notice = notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

                tbx_notice.Content = notice;
            }


            if (ddl_notice.SelectedValue == "3")
            {
                string notice = "";
                notice = GetTemplate(Session["_lastCulture"].ToString(), 3).Replace("\n", "<br/>");
                    //Resources.Resource.lbl_third_notice_text.Replace("\n", "<br/>"); ;
              
                notice = notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                notice = notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                notice = notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                notice = notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                notice = notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                notice = notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                notice = notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

                tbx_notice.Content = notice;
            }



            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prLateRentNoticeTemplateView", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            try
            {

                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@lrnt_lang", SqlDbType.VarChar, 10).Value = ddl_language.SelectedValue;
                
                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {

                    String first_notice;
                    String second_notice;
                    String third_notice;

                    if (dr["lrnt_text_1"] != DBNull.Value && ddl_notice.SelectedValue == "1")
                    {
                        first_notice = dr["lrnt_text_1"].ToString();
                        first_notice = first_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                        first_notice = first_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                        first_notice = first_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                        first_notice = first_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                        first_notice = first_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                        first_notice = first_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                        first_notice = first_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

                        tbx_notice.Content = first_notice;
                    }

                    if (dr["lrnt_text_2"] != DBNull.Value && ddl_notice.SelectedValue == "2")
                    {
                        second_notice = dr["lrnt_text_2"].ToString();
                        second_notice = second_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                        second_notice = second_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                        second_notice = second_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                        second_notice = second_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                        second_notice = second_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                        second_notice = second_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                        second_notice = second_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

                        tbx_notice.Content = second_notice;
                    }

                    if (dr["lrnt_text_3"] != DBNull.Value && ddl_notice.SelectedValue == "3")
                    {
                        third_notice = dr["lrnt_text_3"].ToString();
                        third_notice = third_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                        third_notice = third_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                        third_notice = third_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                        third_notice = third_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                        third_notice = third_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                        third_notice = third_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                        third_notice = third_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

                        tbx_notice.Content = third_notice;
                    }




                }
            }
            finally
            {
                conn.Close();
            }


        }


    }
    protected void ddl_notice_SelectedIndexChanged(object sender, EventArgs e)
    {
        tb_successfull_confirmation.Visible = false;
        btn_save.Visible = true;
        btn_update.Visible = true;

        if (ddl_notice.SelectedValue == "1")
        {
            string notice = "";
            notice = GetTemplate(ddl_language.SelectedValue, 1).Replace("\n", "<br/>");
        
            notice = notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
            notice = notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
            notice = notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
            notice = notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
            notice = notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
            notice = notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
            notice = notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

            tbx_notice.Content = notice;

        }

        if (ddl_notice.SelectedValue == "2")
        {
            string notice = "";
            notice = GetTemplate(ddl_language.SelectedValue, 2).Replace("\n", "<br/>");
                   
         
            notice = notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
            notice = notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
            notice = notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
            notice = notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
            notice = notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
            notice = notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
            notice = notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

            tbx_notice.Content = notice;
        }


        if (ddl_notice.SelectedValue == "3")
        {
            string notice = "";
            notice = GetTemplate(ddl_language.SelectedValue, 3).Replace("\n", "<br/>");
       
            notice = notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
            notice = notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
            notice = notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
            notice = notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
            notice = notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
            notice = notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
            notice = notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

            tbx_notice.Content = notice;
        }


        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prLateRentNoticeTemplateView", conn);

        cmd.CommandType = CommandType.StoredProcedure;
        conn.Open();

        try
        {

            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@lrnt_lang", SqlDbType.VarChar,10).Value = ddl_language.SelectedValue;
            SqlDataReader dr = null;
            dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

            while (dr.Read() == true)
            {

                String first_notice;
                String second_notice;
                String third_notice;

                if (dr["lrnt_text_1"] != DBNull.Value && ddl_notice.SelectedValue == "1")
                {
                    first_notice = dr["lrnt_text_1"].ToString();
                    first_notice = first_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    first_notice = first_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    first_notice = first_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    first_notice = first_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    first_notice = first_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    first_notice = first_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    first_notice = first_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

                    tbx_notice.Content = first_notice;
                    btn_save.Visible = false;
                }
                if (dr["lrnt_text_1"] == DBNull.Value && ddl_notice.SelectedValue == "1")
                {
                    btn_update.Visible = false;
                }


                if (dr["lrnt_text_2"] != DBNull.Value && ddl_notice.SelectedValue == "2")
                {
                    second_notice = dr["lrnt_text_2"].ToString();
                    second_notice = second_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    second_notice = second_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    second_notice = second_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    second_notice = second_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    second_notice = second_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    second_notice = second_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    second_notice = second_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

                    tbx_notice.Content = second_notice;
                    btn_save.Visible = false;
                }
                if (dr["lrnt_text_2"] == DBNull.Value && ddl_notice.SelectedValue == "2")
                {
                    btn_update.Visible = false;
                }



                if (dr["lrnt_text_3"] != DBNull.Value && ddl_notice.SelectedValue == "3")
                {
                    third_notice = dr["lrnt_text_3"].ToString();
                    third_notice = third_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    third_notice = third_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    third_notice = third_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    third_notice = third_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    third_notice = third_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    third_notice = third_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    third_notice = third_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

                    tbx_notice.Content = third_notice;
                    btn_save.Visible = false;
                }
                if (dr["lrnt_text_3"] == DBNull.Value && ddl_notice.SelectedValue == "3")
                {
                    btn_update.Visible = false;
                }





            }
        }
        finally
        {
            conn.Close();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_update_Click(object sender, EventArgs e)
    {
       if (ddl_notice.SelectedIndex > 0)
       {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        string notice = tbx_notice.Content;
        btn_save.Visible = true;
        btn_update.Visible = true;

        notice = notice.Replace("((" + Resources.Resource.lbl_tag_unpaid_amount + "))", "((1))");
        notice = notice.Replace("((" + Resources.Resource.lbl_tag_late_fee + "))", "((2))");
        notice = notice.Replace("((" + Resources.Resource.lbl_tag_total + "))", "((3))");
        notice = notice.Replace("((" + Resources.Resource.lbl_tag_days_late + "))", "((4))");
        notice = notice.Replace("((" + Resources.Resource.lbl_tag_tenants_name + "))", "((5))");
        notice = notice.Replace("((" + Resources.Resource.lbl_tag_date + "))", "((6))");
        notice = notice.Replace("((" + Resources.Resource.lbl_tag_my_name + "))", "((7))");


        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prLateRentNoticeTemplateUpdate", conn);
        SqlCommand cmd2 = new SqlCommand("prLateRentNoticeTemplateView", conn);

        cmd.CommandType = CommandType.StoredProcedure;

        conn.Open();
        //Add the params

        cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
        cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
        cmd.Parameters.Add("@lrnt_lang", SqlDbType.VarChar, 10).Value = ddl_language.SelectedValue ;
        cmd.Parameters.Add("@lrnt_number", SqlDbType.Int).Value = Convert.ToInt32(ddl_notice.SelectedValue);
        cmd.Parameters.Add("@lrnt_text", SqlDbType.NText).Value =  RegEx.getText(notice);

        cmd.ExecuteNonQuery();

        int return_id = 0;
        return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);
        if (return_id == 0)
        {
            //   result.InnerHtml = " add mortgage successful";
            tb_successfull_confirmation.Visible = true;
        }

        cmd2.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@lrnt_lang", SqlDbType.VarChar, 10).Value = ddl_language.SelectedValue;
            SqlDataReader dr2 = null;
            dr2 = cmd2.ExecuteReader(CommandBehavior.SingleRow);

            while (dr2.Read() == true)
            {

                if (dr2["lrnt_text_1"] != DBNull.Value && ddl_notice.SelectedValue == "1")
                {
                    btn_save.Visible = false;
                }
                if (dr2["lrnt_text_1"] == DBNull.Value && ddl_notice.SelectedValue == "1")
                {
                    btn_update.Visible = false;
                }


                if (dr2["lrnt_text_2"] != DBNull.Value && ddl_notice.SelectedValue == "2")
                {
                    btn_save.Visible = false;
                }
                if (dr2["lrnt_text_2"] == DBNull.Value && ddl_notice.SelectedValue == "2")
                {
                    btn_update.Visible = false;
                }



                if (dr2["lrnt_text_3"] != DBNull.Value && ddl_notice.SelectedValue == "3")
                {
                    btn_save.Visible = false;
                }
                if (dr2["lrnt_text_3"] == DBNull.Value && ddl_notice.SelectedValue == "3")
                {
                    btn_update.Visible = false;
                }
            }
        }
        finally
        {
            conn.Close();
        }


    }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_revert_Click(object sender, EventArgs e)
    {
      if (ddl_notice.SelectedIndex > 0)
      {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        btn_save.Visible = true;
        btn_update.Visible = true;

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prLateRentNoticeTemplateRevert", conn);
        SqlCommand cmd2 = new SqlCommand("prLateRentNoticeTemplateView", conn);

        cmd.CommandType = CommandType.StoredProcedure;
        
        conn.Open();
        //Add the params

        cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@lrnt_lang", SqlDbType.VarChar, 10).Value = ddl_language.SelectedValue;
        cmd.Parameters.Add("@lrnt_number", SqlDbType.Int).Value = Convert.ToInt32(ddl_notice.SelectedValue);
        cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
        cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();

        cmd.ExecuteNonQuery();

        int return_id = 0;
        return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);
        if (return_id == 0)
        {
            //   result.InnerHtml = " add mortgage successful";
            tb_successfull_confirmation.Visible = true;
        }

      
        if (ddl_notice.SelectedValue == "1")
        {
            string notice = "";
            notice = GetTemplate(ddl_language.SelectedValue, 1).Replace("\n", "<br/>");
                   
            notice = notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
            notice = notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
            notice = notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
            notice = notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
            notice = notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
            notice = notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
            notice = notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

            tbx_notice.Content = notice;

        }

        if (ddl_notice.SelectedValue == "2")
        {
            string notice = "";
            notice = GetTemplate(ddl_language.SelectedValue, 2).Replace("\n", "<br/>");
                   
                //Resources.Resource.lbl_second_notice_text.Replace("\n", "<br/>"); ;


            notice = notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
            notice = notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
            notice = notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
            notice = notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
            notice = notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
            notice = notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
            notice = notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

            tbx_notice.Content = notice;
        }


        if (ddl_notice.SelectedValue == "3")
        {
            string notice = "";
            notice = GetTemplate(ddl_language.SelectedValue, 3).Replace("\n", "<br/>");
                   
            notice = notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
            notice = notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
            notice = notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
            notice = notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
            notice = notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
            notice = notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
            notice = notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

            tbx_notice.Content = notice;
        }


        //SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
       
        cmd2.CommandType = CommandType.StoredProcedure;
       // conn.Open();

        try
        {

            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@lrnt_lang", SqlDbType.VarChar, 10).Value = ddl_language.SelectedValue;
            SqlDataReader dr2 = null;
            dr2 = cmd2.ExecuteReader(CommandBehavior.SingleRow);

            while (dr2.Read() == true)
            {

                String first_notice;
                String second_notice;
                String third_notice;

                if (dr2["lrnt_text_1"] != DBNull.Value && ddl_notice.SelectedValue == "1")
                {
                    first_notice = dr2["lrnt_text_1"].ToString();

                    first_notice = first_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    first_notice = first_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    first_notice = first_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    first_notice = first_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    first_notice = first_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    first_notice = first_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    first_notice = first_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                    
                    tbx_notice.Content = first_notice;
                    btn_save.Visible = false;
                }
                if (dr2["lrnt_text_1"] == DBNull.Value && ddl_notice.SelectedValue == "1")
                {
                    btn_update.Visible = false;
                }




                if (dr2["lrnt_text_2"] != DBNull.Value && ddl_notice.SelectedValue == "2")
                {
                    second_notice = dr2["lrnt_text_2"].ToString();
                    second_notice = second_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    second_notice = second_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    second_notice = second_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    second_notice = second_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    second_notice = second_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    second_notice = second_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    second_notice = second_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                        
                    tbx_notice.Content = second_notice;
                    btn_save.Visible = false;
                }
                if (dr2["lrnt_text_2"] == DBNull.Value && ddl_notice.SelectedValue == "2")
                {
                    btn_update.Visible = false;
                }





                if (dr2["lrnt_text_3"] != DBNull.Value && ddl_notice.SelectedValue == "3")
                {
                    third_notice = dr2["lrnt_text_3"].ToString();
                    third_notice = third_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    third_notice = third_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    third_notice = third_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    third_notice = third_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    third_notice = third_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    third_notice = third_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    third_notice = third_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                    
                    tbx_notice.Content = third_notice;
                    btn_save.Visible = false;
                }
                if (dr2["lrnt_text_3"] == DBNull.Value && ddl_notice.SelectedValue == "3")
                {
                    btn_update.Visible = false;
                }




            }
        }
        finally
        {
            conn.Close();
        }

    }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_save_Click(object sender, EventArgs e)
    {

       if (ddl_notice.SelectedIndex > 0)
       {

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        string notice = tbx_notice.Content;
        btn_save.Visible = true;
        btn_update.Visible = true;

        notice = notice.Replace("((" + Resources.Resource.lbl_tag_unpaid_amount + "))", "((1))");
        notice = notice.Replace("((" + Resources.Resource.lbl_tag_late_fee + "))", "((2))");
        notice = notice.Replace("((" + Resources.Resource.lbl_tag_total + "))", "((3))");
        notice = notice.Replace("((" + Resources.Resource.lbl_tag_days_late + "))", "((4))");
        notice = notice.Replace("((" + Resources.Resource.lbl_tag_tenants_name + "))", "((5))");
        notice = notice.Replace("((" + Resources.Resource.lbl_tag_date + "))", "((6))");
        notice = notice.Replace("((" + Resources.Resource.lbl_tag_my_name + "))", "((7))");



        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prLateRentNoticeTemplateAdd", conn);
        SqlCommand cmd2 = new SqlCommand("prLateRentNoticeTemplateView", conn);

        cmd.CommandType = CommandType.StoredProcedure;

        conn.Open();
        //Add the params

        cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@lrnt_lang", SqlDbType.VarChar, 10).Value = ddl_language.SelectedValue;
        cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
        cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
        cmd.Parameters.Add("@lrnt_number", SqlDbType.Int).Value = Convert.ToInt32(ddl_notice.SelectedValue);
        cmd.Parameters.Add("@lrnt_text", SqlDbType.NText).Value = RegEx.getText(notice);

        cmd.ExecuteNonQuery();

        int return_id = 0;
        return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);
        if (return_id == 0)
        {
            //   result.InnerHtml = " add mortgage successful";
            tb_successfull_confirmation.Visible = true;
        }

        cmd2.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@lrnt_lang", SqlDbType.VarChar, 10).Value = ddl_language.SelectedValue;
            SqlDataReader dr2 = null;
            dr2 = cmd2.ExecuteReader(CommandBehavior.SingleRow);

            while (dr2.Read() == true)
            {
                if (dr2["lrnt_text_1"] != DBNull.Value && ddl_notice.SelectedValue == "1")
                {
                    btn_save.Visible = false;
                }
                if (dr2["lrnt_text_1"] == DBNull.Value && ddl_notice.SelectedValue == "1")
                {
                    btn_update.Visible = false;
                }


                if (dr2["lrnt_text_2"] != DBNull.Value && ddl_notice.SelectedValue == "2")
                {
                    btn_save.Visible = false;
                }
                if (dr2["lrnt_text_2"] == DBNull.Value && ddl_notice.SelectedValue == "2")
                {
                    btn_update.Visible = false;
                }



                if (dr2["lrnt_text_3"] != DBNull.Value && ddl_notice.SelectedValue == "3")
                {
                    btn_save.Visible = false;
                }
                if (dr2["lrnt_text_3"] == DBNull.Value && ddl_notice.SelectedValue == "3")
                {
                    btn_update.Visible = false;
                }
            }
        }
        finally
        {
            conn.Close();
        }
     }
    }



    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    protected string GetTemplate(string lang , int number)
    {

        string filename = "";

        if (lang == "en-US")
        {
            switch (number)
            {
                case 1: filename = Server.MapPath("en-template_first_notice.txt");
                    break;        
                case 2: filename = Server.MapPath("en-template_second_notice.txt");
                    break;
                case 3: filename = Server.MapPath("en-template_third_notice.txt");
                    break;            
            }
        
        }

        if (lang == "fr-FR")
        {
            switch (number)
            {
                case 1: filename = Server.MapPath("fr-template_first_notice.txt");
                    break;
                case 2: filename = Server.MapPath("fr-template_second_notice.txt");
                    break;
                case 3: filename = Server.MapPath("fr-template_third_notice.txt");
                    break;    
            }
        }
        // Create a new stream to read from a file
        StreamReader sr = new StreamReader(filename);

        // Read contents of file into a string
        string s = sr.ReadToEnd();

        // Close StreamReader
        sr.Close();

        return s ;
    }


    protected void ddl_language_SelectedIndexChanged(object sender, EventArgs e)
    {
        tb_successfull_confirmation.Visible = false;
        btn_save.Visible = true;
        btn_update.Visible = true;

        if (ddl_notice.SelectedValue == "1")
        {
            string notice = "";
            notice = GetTemplate(ddl_language.SelectedValue, 1).Replace("\n", "<br/>");

            notice = notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
            notice = notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
            notice = notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
            notice = notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
            notice = notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
            notice = notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
            notice = notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

            tbx_notice.Content = notice;

        }

        if (ddl_notice.SelectedValue == "2")
        {
            string notice = "";
            notice = GetTemplate(ddl_language.SelectedValue, 2).Replace("\n", "<br/>");


            notice = notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
            notice = notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
            notice = notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
            notice = notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
            notice = notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
            notice = notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
            notice = notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

            tbx_notice.Content = notice;
        }


        if (ddl_notice.SelectedValue == "3")
        {
            string notice = "";
            notice = GetTemplate(ddl_language.SelectedValue, 3).Replace("\n", "<br/>");

            notice = notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
            notice = notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
            notice = notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
            notice = notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
            notice = notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
            notice = notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
            notice = notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

            tbx_notice.Content = notice;
        }


        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prLateRentNoticeTemplateView", conn);

        cmd.CommandType = CommandType.StoredProcedure;
        conn.Open();

        try
        {

            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@lrnt_lang", SqlDbType.VarChar, 10).Value = ddl_language.SelectedValue;
            SqlDataReader dr = null;
            dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

            while (dr.Read() == true)
            {

                String first_notice;
                String second_notice;
                String third_notice;

                if (dr["lrnt_text_1"] != DBNull.Value && ddl_notice.SelectedValue == "1")
                {
                    first_notice = dr["lrnt_text_1"].ToString();
                    first_notice = first_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    first_notice = first_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    first_notice = first_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    first_notice = first_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    first_notice = first_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    first_notice = first_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    first_notice = first_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

                    tbx_notice.Content = first_notice;
                    btn_save.Visible = false;
                }
                if (dr["lrnt_text_1"] == DBNull.Value && ddl_notice.SelectedValue == "1")
                {
                    btn_update.Visible = false;
                }


                if (dr["lrnt_text_2"] != DBNull.Value && ddl_notice.SelectedValue == "2")
                {
                    second_notice = dr["lrnt_text_2"].ToString();
                    second_notice = second_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    second_notice = second_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    second_notice = second_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    second_notice = second_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    second_notice = second_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    second_notice = second_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    second_notice = second_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

                    tbx_notice.Content = second_notice;
                    btn_save.Visible = false;
                }
                if (dr["lrnt_text_2"] == DBNull.Value && ddl_notice.SelectedValue == "2")
                {
                    btn_update.Visible = false;
                }



                if (dr["lrnt_text_3"] != DBNull.Value && ddl_notice.SelectedValue == "3")
                {
                    third_notice = dr["lrnt_text_3"].ToString();
                    third_notice = third_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    third_notice = third_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    third_notice = third_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    third_notice = third_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    third_notice = third_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    third_notice = third_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    third_notice = third_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

                    tbx_notice.Content = third_notice;
                    btn_save.Visible = false;
                }
                if (dr["lrnt_text_3"] == DBNull.Value && ddl_notice.SelectedValue == "3")
                {
                    btn_update.Visible = false;
                }





            }
        }
        finally
        {
            conn.Close();
        }
    }
}
