﻿<%@ Page Language="C#" MasterPageFile="~/user/mp_property_manager.master" AutoEventWireup="true" CodeFile="notice_delequency_preview.aspx.cs" Inherits="manager_notice_notice_delequency_preview" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_notice_rent %>"
     Font-Underline="true" ForeColor="Red" Font-Bold="false" Font-Size="Large"
      ></asp:Label>
    <br /><br />
<table id="tb_notice" visible="true" runat="server" style="width: 100%">
    
    <tr>
            <td>
                <table style="width: 100%">
                    <tr>
                        <td align="right">
                <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lbl_date_of_notice %>" 
                                style="font-weight: 700"></asp:Label>
                        </td>
                        <td>
                <asp:Label ID="lbl_notice_date" runat="server" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right">
                            <asp:Label ID="Label13" runat="server" 
                                style="font-weight: 700"></asp:Label>
                            &nbsp;-<br />
                        </td>
                        <td valign="top">
                            <asp:Label ID="Label14" runat="server" 
                                Text="<%$ Resources:Resource, lbl_unpaid_amount %>" style="font-weight: 700"></asp:Label>  &nbsp;:&nbsp;<asp:Label ID="lbl_amount_owed" runat="server" ></asp:Label> &nbsp;,&nbsp;<asp:Label 
                                ID="Label16" runat="server" Text="<%$ Resources:Resource, lbl_late_fee %>" 
                                style="font-weight: 700"></asp:Label>&nbsp;:&nbsp;<asp:Label
                                ID="lbl_late_fee" runat="server" ></asp:Label>&nbsp;,&nbsp;<asp:Label ID="Label18"
                                    runat="server" Text="<%$ Resources:Resource, lbl_total %>" 
                                style="font-weight: 700"></asp:Label>&nbsp;:&nbsp;<asp:Label
                                        ID="lbl_total" runat="server" Text="Label"></asp:Label>&nbsp;
                            <asp:Label ID="Label23" runat="server" 
                                Text="<%$ Resources:Resource, lbl_days_late%>" style="font-weight: 700"></asp:Label>
                        &nbsp;<asp:Label ID="lbl_days_late" runat="server" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right">
                <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, lbl_dear %>" 
                                style="font-weight: 700"></asp:Label>
                        </td>
                        <td>
                            <asp:Repeater ID="r_tenant_list" runat="server">
                            <ItemTemplate>
                            <%#Eval("name_lname")%> &nbsp;,&nbsp;
                            <%#Eval("name_fname")%><br />
                            </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
                <br />
            </td>
        </tr>
        
        
         <tr>
            <td>
             
            </td>
        </tr>
        
        
        <tr>
            <td>
            
            <div id="div_notice" runat="server"></div>
                <asp:Label ID="lbl_notice" runat="server" Height="320px" 
                    TextMode="MultiLine" Width="544px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
    </table>
  


</asp:Content>

