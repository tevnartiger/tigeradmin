﻿Final Notice

Date of notice : ((6))

Dear : ((5))

This notice is to inform you of our intentions to report your delinquent payments to the appropriate
agency(s). It is important to maintain a favorable payment history, as it may affect your ability to 
rent , purchase a home, acquire loans and/or credit cards .After numerous attemps at trying to resolve 
this issue, your account remains seroiusly deliquent.

Taking care of this situation immedialtely will stop the reporting process. Ignoring this notice may 
negatively impact your credit history and/or score. This will also lead the eviction proceedings.

Amount due : ((1))
Late fee : ((2))
Total : ((3))

Sincerely, ((7))

PS. You may disregard this notice if you have already sent your payment