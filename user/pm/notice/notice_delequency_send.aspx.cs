﻿using System;
using System.IO;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : july 31 , 2008
/// </summary>
public partial class manager_notice_notice_delequency_send : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            // setting the culture of the Obout editor
            tbx_first_notice.PathPrefix = "../../App_themes/Obout/editor/";
            tbx_first_notice.Language = tiger.PageUtility.getLanguage(Session["_lastCulture"].ToString());

            tbx_second_notice.PathPrefix = "../../App_themes/Obout/editor/";
            tbx_second_notice.Language = tiger.PageUtility.getLanguage(Session["_lastCulture"].ToString());

            tbx_third_notice.PathPrefix = "../../App_themes/Obout/editor/";
            tbx_third_notice.Language = tiger.PageUtility.getLanguage(Session["_lastCulture"].ToString());


            h_tu_id.Value = Request.QueryString["tu_id"];
            h_rp_id.Value = Request.QueryString["rp_id"];
            h_tenant_id.Value = Request.QueryString["tenant_id"];


            tb_successfull_confirmation.Visible = false;

            // the preview, before "printing" the notice , is not visible


            DateTime la_date = new DateTime();
            la_date = DateTime.Now;


            string first_notice = "";
            string second_notice = "";
            string third_notice = "";

            ddl_language.SelectedValue = Session["_lastCulture"].ToString();

            
            first_notice = GetTemplate(Session["_lastCulture"].ToString(), 1).Replace("\n", "<br/>");
            first_notice = first_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
            first_notice = first_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
            first_notice = first_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
            first_notice = first_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
            first_notice = first_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
            first_notice = first_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
            first_notice = first_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

            second_notice = GetTemplate(Session["_lastCulture"].ToString(), 2).Replace("\n", "<br/>");
            second_notice = second_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
            second_notice = second_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
            second_notice = second_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
            second_notice = second_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
            second_notice = second_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
            second_notice = second_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
            second_notice = second_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");


            third_notice = GetTemplate(Session["_lastCulture"].ToString(), 3).Replace("\n", "<br/>");
            third_notice = third_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
            third_notice = third_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
            third_notice = third_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
            third_notice = third_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
            third_notice = third_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
            third_notice = third_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
            third_notice = third_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");

                      
            double amount_owed = 0;
            int days_late = 0;

            // by default will not send email
            radio_send_mail_1.SelectedValue = "0";
            radio_send_mail_2.SelectedValue = "0";
            radio_send_mail_3.SelectedValue = "0";

            ////////////////////////////////
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prLateRentNoticeTemplateView", conn);
            SqlCommand cmd2 = new SqlCommand("prTenantUnitNameList", conn);
            SqlCommand cmd3 = new SqlCommand("prRentDelequency", conn);
            SqlCommand cmd4 = new SqlCommand("prLateFee", conn);
            SqlCommand cmd5 = new SqlCommand("prNameView", conn);
           
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();

                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@lrnt_lang", SqlDbType.VarChar, 10).Value = ddl_language.SelectedValue;
                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {

                    if (dr["lrnt_text_1"] != DBNull.Value)
                    {
                        first_notice = dr["lrnt_text_1"].ToString().Replace("\n", "<br/>");
                        first_notice = first_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                        first_notice = first_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                        first_notice = first_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                        first_notice = first_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                        first_notice = first_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                        first_notice = first_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                        first_notice = first_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                        tbx_first_notice.Content = first_notice;
                    }
                    else
                    {
                        first_notice = Resources.Resource.lbl_first_notice_text.Replace("\n", "<br/>") ;
                        first_notice = first_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                        first_notice = first_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                        first_notice = first_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                        first_notice = first_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                        first_notice = first_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                        first_notice = first_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                        first_notice = first_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                        tbx_first_notice.Content = first_notice;

                    }


                    if (dr["lrnt_text_2"] != DBNull.Value)
                    {
                        second_notice = tbx_second_notice.Content = dr["lrnt_text_2"].ToString().Replace("\n", "<br/>");
                        second_notice = second_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                        second_notice = second_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                        second_notice = second_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                        second_notice = second_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                        second_notice = second_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                        second_notice = second_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                        second_notice = second_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                        tbx_second_notice.Content = second_notice;
                    }
                    else
                    {
                        second_notice = Resources.Resource.lbl_second_notice_text.Replace("\n", "<br/>");
                        second_notice = second_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                        second_notice = second_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                        second_notice = second_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                        second_notice = second_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                        second_notice = second_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                        second_notice = second_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                        second_notice = second_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                        tbx_second_notice.Content = second_notice;
                    }

                    if (dr["lrnt_text_3"] != DBNull.Value)
                    {
                        third_notice = tbx_third_notice.Content = dr["lrnt_text_3"].ToString().Replace("\n", "<br/>");
                        third_notice = third_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                        third_notice = third_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                        third_notice = third_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                        third_notice = third_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                        third_notice = third_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                        third_notice = third_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                        third_notice = third_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                        tbx_third_notice.Content = third_notice;
                    }
                    else
                    {
                        third_notice = Resources.Resource.lbl_third_notice_text.Replace("\n", "<br/>");
                        third_notice = third_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                        third_notice = third_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                        third_notice = third_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                        third_notice = third_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                        third_notice = third_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                        third_notice = third_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                        third_notice = third_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                        tbx_third_notice.Content = third_notice;


                    }

                }
            }
            finally
            {
              //  conn.Close();
            }


            cmd2.CommandType = CommandType.StoredProcedure;

            try
            {
                cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd2.Parameters.Add("@tenant_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tenant_id"]);

                SqlDataAdapter da = new SqlDataAdapter(cmd2);
                DataTable dt = new DataTable();
                da.Fill(dt);

                string names = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dt.Rows[i];
                    names = names + row[5].ToString() + " " + row[4].ToString() +", ";
                }
                names = names.Remove(names.LastIndexOf(","), 1);

                first_notice = first_notice.Replace("((" + Resources.Resource.lbl_tag_tenants_name + "))", names);
                second_notice = second_notice.Replace("((" + Resources.Resource.lbl_tag_tenants_name + "))",names);
                third_notice = third_notice.Replace("((" + Resources.Resource.lbl_tag_tenants_name + "))",  names);
                   
            }
            finally
            {
                //  conn.Close();
            }


            cmd3.CommandType = CommandType.StoredProcedure;
            try
            {

                cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd3.Parameters.Add("@rp_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["rp_id"]);


                SqlDataReader dr3 = null;
                dr3 = cmd3.ExecuteReader(CommandBehavior.SingleRow);
                

                while (dr3.Read() == true)
                {

                    amount_owed = Convert.ToDouble(dr3["amount_owed"]);
                    days_late = Convert.ToInt32(dr3["days"]);

                    first_notice = first_notice.Replace("((" + Resources.Resource.lbl_tag_unpaid_amount + "))",String.Format("{0:0.00}", amount_owed));
                    first_notice = first_notice.Replace("((" + Resources.Resource.lbl_tag_days_late + "))",days_late.ToString());

                    second_notice = second_notice.Replace("((" + Resources.Resource.lbl_tag_unpaid_amount + "))", String.Format("{0:0.00}", amount_owed));
                    second_notice = second_notice.Replace("((" + Resources.Resource.lbl_tag_days_late + "))", days_late.ToString());

                    third_notice = third_notice.Replace("((" + Resources.Resource.lbl_tag_unpaid_amount + "))", String.Format("{0:0.00}", amount_owed));
                    third_notice = third_notice.Replace("((" + Resources.Resource.lbl_tag_days_late + "))", days_late.ToString());// +String.Format("{0:0.00}", amount_owed);
                    
                    tbx_first_notice.Content = first_notice;
                    tbx_second_notice.Content = second_notice;
                    tbx_third_notice.Content = third_notice;
                   
                }
            }
            finally
            {
                //  conn.Close();
            }

            cmd4.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd4.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd4.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);


                SqlDataReader dr4 = null;
                dr4 = cmd4.ExecuteReader(CommandBehavior.SingleRow);

                while (dr4.Read() == true)
                {

                    double late_fee = Convert.ToDouble(dr4["tt_late_fee"]);
                    double total = 0;                   
                    total = late_fee + amount_owed;

                    first_notice = first_notice.Replace("((" + Resources.Resource.lbl_tag_late_fee + "))", String.Format("{0:0.00}", late_fee));
                    first_notice = first_notice.Replace("((" + Resources.Resource.lbl_tag_total + "))", String.Format("{0:0.00}",total));

                    second_notice = second_notice.Replace("((" + Resources.Resource.lbl_tag_late_fee + "))", String.Format("{0:0.00}", late_fee));
                    second_notice = second_notice.Replace("((" + Resources.Resource.lbl_tag_total + "))", String.Format("{0:0.00}", total));

                    third_notice = third_notice.Replace("((" + Resources.Resource.lbl_tag_late_fee + "))", String.Format("{0:0.00}", late_fee));
                    third_notice = third_notice.Replace("((" + Resources.Resource.lbl_tag_total + "))", String.Format("{0:0.00}", total));

                    tbx_first_notice.Content = first_notice;
                    tbx_second_notice.Content = second_notice;
                    tbx_third_notice.Content = third_notice;
                }
            }
            finally
            {
             //   conn.Close();
            }



            cmd5.CommandType = CommandType.StoredProcedure;
            try
            {
               //Add the params
                cmd5.Parameters.Add("@schema_id", SqlDbType.Int).Value =  Convert.ToInt32(Session["schema_id"]);
                cmd5.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);

                SqlDataReader dr5 = null;
                dr5 = cmd5.ExecuteReader(CommandBehavior.SingleRow);
                
                string name = "";
                while (dr5.Read())
                {
                   
                    name =  dr5["name_fname"] + " " + dr5["name_lname"] ;

                }

                first_notice = first_notice.Replace("((" + Resources.Resource.lbl_tag_my_name + "))", name) ;
                second_notice = second_notice.Replace("((" + Resources.Resource.lbl_tag_my_name + "))", name) ;
                third_notice  = third_notice.Replace("((" + Resources.Resource.lbl_tag_my_name + "))", name) ;

              
            }
            finally
            {
                conn.Close();
            }

            tbx_first_notice.Content = first_notice.Replace("((" + Resources.Resource.lbl_tag_date + "))", String.Format("{0:MMM dd , yyyy}", la_date)); ;
            tbx_second_notice.Content = second_notice.Replace("((" + Resources.Resource.lbl_tag_date + "))", String.Format("{0:MMM dd , yyyy}", la_date)); ;
            tbx_third_notice.Content = third_notice.Replace("((" + Resources.Resource.lbl_tag_date + "))", String.Format("{0:MMM dd , yyyy}", la_date)); ;

           
            ///////////////////////////////


        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_confirm_Click(object sender, EventArgs e)
    {
        if(ddl_notice.SelectedIndex > 0)
        {
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prLateRentNoticeAdd", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            DateTime right_now = new DateTime();
            right_now = DateTime.Now;

            tiger.Date d = new tiger.Date();

            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));

            conn.Open();
            //Add the params

            cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);
            cmd.Parameters.Add("@rp_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["rp_id"]);
            cmd.Parameters.Add("@lrn_date", SqlDbType.DateTime).Value = right_now;
            cmd.Parameters.Add("@lrn_number", SqlDbType.Int).Value = Convert.ToInt32(ddl_notice.SelectedValue);
            cmd.Parameters.Add("@lrn_email", SqlDbType.Bit).Value = Convert.ToByte(radio_send_mail_1.SelectedValue);
            cmd.Parameters.Add("@lrn_text", SqlDbType.VarChar, 8000).Value = tbx_first_notice.Content;

            cmd.ExecuteNonQuery();

            int return_id = 0;
            return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);
            if (return_id == 0)
            {
                //   result.InnerHtml = " add mortgage successful";
                tb_successfull_confirmation.Visible = true;
            }

            conn.Close();


            string tenant_email = "";

            if (radio_send_mail_1.SelectedValue == "1")
            {
                SqlCommand cmd2 = new SqlCommand("prTenantUnitNameList", conn);
                cmd2.CommandType = CommandType.StoredProcedure;

                try
                {
                    conn.Open();

                    //Add the params
                    cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                    cmd2.Parameters.Add("@tenant_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tenant_id"]);

                    SqlDataReader dr2 = null;
                    dr2 = cmd2.ExecuteReader();

                    while (dr2.Read())
                    {
                        tenant_email = tenant_email + dr2["name_email"].ToString() + ", ";
                        lbl_mail.Text = tenant_email;
                    }
                }

                finally
                {
                    conn.Close();
                }


                string notice_sent = "";
                notice_sent = tbx_first_notice.Content;
                tiger.Email.sendMailHtml("sinfo@sinfoca.com", tenant_email, Resources.Resource.lbl_u_unpaid_rent, notice_sent);

                /// CONTINU HERE


            }
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_confirm_2_Click(object sender, EventArgs e)
    {


        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prLateRentNoticeAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        DateTime right_now = new DateTime();
        right_now = DateTime.Now;

        tiger.Date d = new tiger.Date();

        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));



        conn.Open();
        //Add the params

        cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
        cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar , 15).Value = Request.UserHostAddress.ToString();
        cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);
        cmd.Parameters.Add("@rp_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["rp_id"]);
        cmd.Parameters.Add("@lrn_date", SqlDbType.DateTime).Value = right_now;
        cmd.Parameters.Add("@lrn_number", SqlDbType.Int).Value = Convert.ToInt32(ddl_notice.SelectedValue);
        cmd.Parameters.Add("@lrn_email", SqlDbType.Bit).Value = Convert.ToByte(radio_send_mail_2.SelectedValue);
        cmd.Parameters.Add("@lrn_text", SqlDbType.NText).Value = tbx_second_notice.Content;

        cmd.ExecuteNonQuery();

        int return_id = 0;
        return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);
        if (return_id == 0)
        {
            //   result.InnerHtml = " add mortgage successful";
            tb_successfull_confirmation.Visible = true;
        }

        conn.Close();

       string tenant_email = "";

       if (radio_send_mail_2.SelectedValue == "1")
       {
           // send mail


           // first get the tenant's email

           // string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

           //   SqlConnection conn = new SqlConnection(strconn);
           SqlCommand cmd2 = new SqlCommand("prTenantUnitNameList", conn);
           cmd2.CommandType = CommandType.StoredProcedure;

           try
           {

               conn.Open();

               //Add the params

               cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
               cmd2.Parameters.Add("@tenant_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tenant_id"]);


               SqlDataReader dr2 = null;
               dr2 = cmd2.ExecuteReader();

               while (dr2.Read())
               {
                   tenant_email = tenant_email + dr2["name_email"].ToString() + ",";
                   lbl_mail.Text = tenant_email;
               }
           }

           finally
           {
               conn.Close();
           }

           string notice_sent = "";
           notice_sent = tbx_second_notice.Content;
               
           tiger.Email.sendMailHtml("sinfo@sinfoca.com", tenant_email,Resources.Resource.lbl_important_notice, notice_sent);
            

       }



    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_confirm_3_Click(object sender, EventArgs e)
    {
       
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prLateRentNoticeAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        DateTime right_now = new DateTime();
        right_now = DateTime.Now;

        tiger.Date d = new tiger.Date();

        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));



        conn.Open();
        //Add the params
        cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
        cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar ,15).Value = Request.UserHostAddress.ToString();
        cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);
        cmd.Parameters.Add("@rp_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["rp_id"]);
        cmd.Parameters.Add("@lrn_date", SqlDbType.DateTime).Value = right_now;
        cmd.Parameters.Add("@lrn_number", SqlDbType.Int).Value = Convert.ToInt32(ddl_notice.SelectedValue);
        cmd.Parameters.Add("@lrn_email", SqlDbType.Bit).Value = Convert.ToByte(radio_send_mail_3.SelectedValue);
        cmd.Parameters.Add("@lrn_text", SqlDbType.NText).Value = tbx_third_notice.Content;

        cmd.ExecuteNonQuery();

        int return_id = 0;
        return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);
        if (return_id == 0)
        {
            //   result.InnerHtml = " add mortgage successful";
            tb_successfull_confirmation.Visible = true;
        }

        conn.Close();



        string tenant_email = "";


        if (radio_send_mail_3.SelectedValue == "1")
        {
            SqlCommand cmd2 = new SqlCommand("prTenantUnitNameList", conn);
            cmd2.CommandType = CommandType.StoredProcedure;

            try
            {

                conn.Open();

                //Add the params

                cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd2.Parameters.Add("@tenant_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tenant_id"]);


                SqlDataReader dr2 = null;
                dr2 = cmd2.ExecuteReader();

                while (dr2.Read())
                {
                    tenant_email = tenant_email + dr2["name_email"].ToString() + ",";
                    lbl_mail.Text = tenant_email;
                }
            }

            finally
            {
                conn.Close();
            }


            string notice_sent =  tbx_third_notice.Content;
            tiger.Email.sendMailHtml("sinfo@sinfoca.com", tenant_email, Resources.Resource.lbl_urgent_notice, notice_sent);
                          
        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_notice_SelectedIndexChanged(object sender, EventArgs e)
    {
     
        if (ddl_notice.SelectedValue == "1")
        {
            MultiView1.ActiveViewIndex = 0;

        }

        if (ddl_notice.SelectedValue == "2")
        {
            MultiView1.ActiveViewIndex = 1;
        }


        if (ddl_notice.SelectedValue == "3")
        {
            MultiView1.ActiveViewIndex = 2;
        }

        //////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////

        DateTime la_date = new DateTime();
        la_date = DateTime.Now;


        string first_notice = "";
        string second_notice = "";
        string third_notice = "";


        double amount_owed = 0;
        int days_late = 0;

        // by default will not send email
        radio_send_mail_1.SelectedValue = "0";
        radio_send_mail_2.SelectedValue = "0";
        radio_send_mail_3.SelectedValue = "0";

        ////////////////////////////////
        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prLateRentNoticeTemplateView", conn);
        SqlCommand cmd2 = new SqlCommand("prTenantUnitNameList", conn);
        SqlCommand cmd3 = new SqlCommand("prRentDelequency", conn);
        SqlCommand cmd4 = new SqlCommand("prLateFee", conn);
        SqlCommand cmd5 = new SqlCommand("prNameView", conn);

        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();

            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@lrnt_lang", SqlDbType.VarChar, 10).Value = ddl_language.SelectedValue;
            SqlDataReader dr = null;
            dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

            while (dr.Read() == true)
            {

                if (dr["lrnt_text_1"] != DBNull.Value)
                {
                    first_notice = dr["lrnt_text_1"].ToString().Replace("\n", "<br/>");
                    first_notice = first_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    first_notice = first_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    first_notice = first_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    first_notice = first_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    first_notice = first_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    first_notice = first_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    first_notice = first_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                    tbx_first_notice.Content = first_notice;
                }
                else
                {
                    first_notice = GetTemplate(ddl_language.SelectedValue, 1).Replace("\n", "<br/>");
                    first_notice = first_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    first_notice = first_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    first_notice = first_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    first_notice = first_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    first_notice = first_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    first_notice = first_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    first_notice = first_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                    tbx_first_notice.Content = first_notice;

                }


                if (dr["lrnt_text_2"] != DBNull.Value)
                {
                    second_notice = tbx_second_notice.Content = dr["lrnt_text_2"].ToString().Replace("\n", "<br/>");
                    second_notice = second_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    second_notice = second_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    second_notice = second_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    second_notice = second_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    second_notice = second_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    second_notice = second_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    second_notice = second_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                    tbx_second_notice.Content = second_notice;
                }
                else
                {
                    second_notice = GetTemplate(ddl_language.SelectedValue, 2).Replace("\n", "<br/>");
                    second_notice = second_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    second_notice = second_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    second_notice = second_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    second_notice = second_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    second_notice = second_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    second_notice = second_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    second_notice = second_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                    tbx_second_notice.Content = second_notice;
                }

                if (dr["lrnt_text_3"] != DBNull.Value)
                {
                    third_notice = tbx_third_notice.Content = dr["lrnt_text_3"].ToString().Replace("\n", "<br/>");
                    third_notice = third_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    third_notice = third_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    third_notice = third_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    third_notice = third_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    third_notice = third_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    third_notice = third_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    third_notice = third_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                    tbx_third_notice.Content = third_notice;
                }
                else
                {
                    third_notice = GetTemplate(ddl_language.SelectedValue, 3).Replace("\n", "<br/>");
                    third_notice = third_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    third_notice = third_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    third_notice = third_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    third_notice = third_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    third_notice = third_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    third_notice = third_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    third_notice = third_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                    tbx_third_notice.Content = third_notice;
                }

            }
        }
        finally
        {
            //  conn.Close();
        }


        cmd2.CommandType = CommandType.StoredProcedure;

        try
        {
            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@tenant_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tenant_id"]);

            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);

            string names = "";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dt.Rows[i];
                names = names + row[5].ToString() + " " + row[4].ToString() + ", ";
            }
            names = names.Remove(names.LastIndexOf(","), 1);

            first_notice = first_notice.Replace("((" + Resources.Resource.lbl_tag_tenants_name + "))", names);
            second_notice = second_notice.Replace("((" + Resources.Resource.lbl_tag_tenants_name + "))", names);
            third_notice = third_notice.Replace("((" + Resources.Resource.lbl_tag_tenants_name + "))", names);

        }
        finally
        {
            //  conn.Close();
        }


        cmd3.CommandType = CommandType.StoredProcedure;
        try
        {

            cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd3.Parameters.Add("@rp_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["rp_id"]);


            SqlDataReader dr3 = null;
            dr3 = cmd3.ExecuteReader(CommandBehavior.SingleRow);


            while (dr3.Read() == true)
            {

                amount_owed = Convert.ToDouble(dr3["amount_owed"]);
                days_late = Convert.ToInt32(dr3["days"]);

                first_notice = first_notice.Replace("((" + Resources.Resource.lbl_tag_unpaid_amount + "))", String.Format("{0:0.00}", amount_owed));
                first_notice = first_notice.Replace("((" + Resources.Resource.lbl_tag_days_late + "))", days_late.ToString());

                second_notice = second_notice.Replace("((" + Resources.Resource.lbl_tag_unpaid_amount + "))", String.Format("{0:0.00}", amount_owed));
                second_notice = second_notice.Replace("((" + Resources.Resource.lbl_tag_days_late + "))", days_late.ToString());

                third_notice = third_notice.Replace("((" + Resources.Resource.lbl_tag_unpaid_amount + "))", String.Format("{0:0.00}", amount_owed));
                third_notice = third_notice.Replace("((" + Resources.Resource.lbl_tag_days_late + "))", days_late.ToString());// +String.Format("{0:0.00}", amount_owed);

                tbx_first_notice.Content = first_notice;
                tbx_second_notice.Content = second_notice;
                tbx_third_notice.Content = third_notice;

            }
        }
        finally
        {
            //  conn.Close();
        }

        cmd4.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd4.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd4.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);


            SqlDataReader dr4 = null;
            dr4 = cmd4.ExecuteReader(CommandBehavior.SingleRow);

            while (dr4.Read() == true)
            {

                double late_fee = Convert.ToDouble(dr4["tt_late_fee"]);
                double total = 0;
                total = late_fee + amount_owed;

                first_notice = first_notice.Replace("((" + Resources.Resource.lbl_tag_late_fee + "))", String.Format("{0:0.00}", late_fee));
                first_notice = first_notice.Replace("((" + Resources.Resource.lbl_tag_total + "))", String.Format("{0:0.00}", total));

                second_notice = second_notice.Replace("((" + Resources.Resource.lbl_tag_late_fee + "))", String.Format("{0:0.00}", late_fee));
                second_notice = second_notice.Replace("((" + Resources.Resource.lbl_tag_total + "))", String.Format("{0:0.00}", total));

                third_notice = third_notice.Replace("((" + Resources.Resource.lbl_tag_late_fee + "))", String.Format("{0:0.00}", late_fee));
                third_notice = third_notice.Replace("((" + Resources.Resource.lbl_tag_total + "))", String.Format("{0:0.00}", total));

                tbx_first_notice.Content = first_notice;
                tbx_second_notice.Content = second_notice;
                tbx_third_notice.Content = third_notice;
            }
        }
        finally
        {
            //   conn.Close();
        }



        cmd5.CommandType = CommandType.StoredProcedure;
        try
        {
            //Add the params
            cmd5.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd5.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);

            SqlDataReader dr5 = null;
            dr5 = cmd5.ExecuteReader(CommandBehavior.SingleRow);

            string name = "";
            while (dr5.Read())
            {

                name = dr5["name_fname"] + " " + dr5["name_lname"];

            }

            first_notice = first_notice.Replace("((" + Resources.Resource.lbl_tag_my_name + "))", name);
            second_notice = second_notice.Replace("((" + Resources.Resource.lbl_tag_my_name + "))", name);
            third_notice = third_notice.Replace("((" + Resources.Resource.lbl_tag_my_name + "))", name);


        }
        finally
        {
            conn.Close();
        }

        tbx_first_notice.Content = first_notice.Replace("((" + Resources.Resource.lbl_tag_date + "))", String.Format("{0:MMM dd , yyyy}", la_date)); ;
        tbx_second_notice.Content = second_notice.Replace("((" + Resources.Resource.lbl_tag_date + "))", String.Format("{0:MMM dd , yyyy}", la_date)); ;
        tbx_third_notice.Content = third_notice.Replace("((" + Resources.Resource.lbl_tag_date + "))", String.Format("{0:MMM dd , yyyy}", la_date)); ;


        ///////////////////////////////


        /////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////

    }


    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    protected string GetTemplate(string lang, int number)
    {

        string filename = "";

        if (lang == "en-US")
        {
            switch (number)
            {
                case 1: filename = Server.MapPath("en-template_first_notice.txt");
                    break;
                case 2: filename = Server.MapPath("en-template_second_notice.txt");
                    break;
                case 3: filename = Server.MapPath("en-template_third_notice.txt");
                    break;
            }

        }

        if (lang == "fr-FR")
        {
            switch (number)
            {
                case 1: filename = Server.MapPath("fr-template_first_notice.txt");
                    break;
                case 2: filename = Server.MapPath("fr-template_second_notice.txt");
                    break;
                case 3: filename = Server.MapPath("fr-template_third_notice.txt");
                    break;
            }
        }
        // Create a new stream to read from a file
        StreamReader sr = new StreamReader(filename);

        // Read contents of file into a string
        string s = sr.ReadToEnd();

        // Close StreamReader
        sr.Close();

        return s;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_preview_1_Click(object sender, EventArgs e)
    {
        Server.Transfer("../notice/notice_delequency_preview.aspx", true);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_preview_2_Click(object sender, EventArgs e)
    {

        Server.Transfer("../notice/notice_delequency_preview.aspx", true);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_preview_3_Click(object sender, EventArgs e)
    {
        Server.Transfer("../notice/notice_delequency_preview.aspx", true);
    }
    protected void ddl_language_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddl_notice.SelectedValue == "1")
        {
            MultiView1.ActiveViewIndex = 0;

        }

        if (ddl_notice.SelectedValue == "2")
        {
            MultiView1.ActiveViewIndex = 1;
        }


        if (ddl_notice.SelectedValue == "3")
        {
            MultiView1.ActiveViewIndex = 2;
        }

        //////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////

        DateTime la_date = new DateTime();
        la_date = DateTime.Now;


        string first_notice = "";
        string second_notice = "";
        string third_notice = "";


        double amount_owed = 0;
        int days_late = 0;

        // by default will not send email
        radio_send_mail_1.SelectedValue = "0";
        radio_send_mail_2.SelectedValue = "0";
        radio_send_mail_3.SelectedValue = "0";

        ////////////////////////////////
        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prLateRentNoticeTemplateView", conn);
        SqlCommand cmd2 = new SqlCommand("prTenantUnitNameList", conn);
        SqlCommand cmd3 = new SqlCommand("prRentDelequency", conn);
        SqlCommand cmd4 = new SqlCommand("prLateFee", conn);
        SqlCommand cmd5 = new SqlCommand("prNameView", conn);

        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();

            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@lrnt_lang", SqlDbType.VarChar, 10).Value = ddl_language.SelectedValue;
            SqlDataReader dr = null;
            dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

            while (dr.Read() == true)
            {

                if (dr["lrnt_text_1"] != DBNull.Value)
                {
                    first_notice = dr["lrnt_text_1"].ToString().Replace("\n", "<br/>");
                    first_notice = first_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    first_notice = first_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    first_notice = first_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    first_notice = first_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    first_notice = first_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    first_notice = first_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    first_notice = first_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                    tbx_first_notice.Content = first_notice;
                }
                else
                {
                    first_notice = GetTemplate(ddl_language.SelectedValue, 1).Replace("\n", "<br/>");
                    first_notice = first_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    first_notice = first_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    first_notice = first_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    first_notice = first_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    first_notice = first_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    first_notice = first_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    first_notice = first_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                    tbx_first_notice.Content = first_notice;

                }


                if (dr["lrnt_text_2"] != DBNull.Value)
                {
                    second_notice = tbx_second_notice.Content = dr["lrnt_text_2"].ToString().Replace("\n", "<br/>");
                    second_notice = second_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    second_notice = second_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    second_notice = second_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    second_notice = second_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    second_notice = second_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    second_notice = second_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    second_notice = second_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                    tbx_second_notice.Content = second_notice;
                }
                else
                {
                    second_notice = GetTemplate(ddl_language.SelectedValue, 2).Replace("\n", "<br/>");
                    second_notice = second_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    second_notice = second_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    second_notice = second_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    second_notice = second_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    second_notice = second_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    second_notice = second_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    second_notice = second_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                    tbx_second_notice.Content = second_notice;
                }

                if (dr["lrnt_text_3"] != DBNull.Value)
                {
                    third_notice = tbx_third_notice.Content = dr["lrnt_text_3"].ToString().Replace("\n", "<br/>");
                    third_notice = third_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    third_notice = third_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    third_notice = third_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    third_notice = third_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    third_notice = third_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    third_notice = third_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    third_notice = third_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                    tbx_third_notice.Content = third_notice;
                }
                else
                {
                    third_notice = GetTemplate(ddl_language.SelectedValue, 3).Replace("\n", "<br/>");
                    third_notice = third_notice.Replace("((1))", "((" + Resources.Resource.lbl_tag_unpaid_amount + "))");
                    third_notice = third_notice.Replace("((2))", "((" + Resources.Resource.lbl_tag_late_fee + "))");
                    third_notice = third_notice.Replace("((3))", "((" + Resources.Resource.lbl_tag_total + "))");
                    third_notice = third_notice.Replace("((4))", "((" + Resources.Resource.lbl_tag_days_late + "))");
                    third_notice = third_notice.Replace("((5))", "((" + Resources.Resource.lbl_tag_tenants_name + "))");
                    third_notice = third_notice.Replace("((6))", "((" + Resources.Resource.lbl_tag_date + "))");
                    third_notice = third_notice.Replace("((7))", "((" + Resources.Resource.lbl_tag_my_name + "))");
                    tbx_third_notice.Content = third_notice;
                }

            }
        }
        finally
        {
            //  conn.Close();
        }


        cmd2.CommandType = CommandType.StoredProcedure;

        try
        {
            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@tenant_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tenant_id"]);

            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);

            string names = "";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow row = dt.Rows[i];
                names = names + row[5].ToString() + " " + row[4].ToString() + ", ";
            }
            names = names.Remove(names.LastIndexOf(","), 1);

            first_notice = first_notice.Replace("((" + Resources.Resource.lbl_tag_tenants_name + "))", names);
            second_notice = second_notice.Replace("((" + Resources.Resource.lbl_tag_tenants_name + "))", names);
            third_notice = third_notice.Replace("((" + Resources.Resource.lbl_tag_tenants_name + "))", names);

        }
        finally
        {
            //  conn.Close();
        }


        cmd3.CommandType = CommandType.StoredProcedure;
        try
        {

            cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd3.Parameters.Add("@rp_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["rp_id"]);


            SqlDataReader dr3 = null;
            dr3 = cmd3.ExecuteReader(CommandBehavior.SingleRow);


            while (dr3.Read() == true)
            {

                amount_owed = Convert.ToDouble(dr3["amount_owed"]);
                days_late = Convert.ToInt32(dr3["days"]);

                first_notice = first_notice.Replace("((" + Resources.Resource.lbl_tag_unpaid_amount + "))", String.Format("{0:0.00}", amount_owed));
                first_notice = first_notice.Replace("((" + Resources.Resource.lbl_tag_days_late + "))", days_late.ToString());

                second_notice = second_notice.Replace("((" + Resources.Resource.lbl_tag_unpaid_amount + "))", String.Format("{0:0.00}", amount_owed));
                second_notice = second_notice.Replace("((" + Resources.Resource.lbl_tag_days_late + "))", days_late.ToString());

                third_notice = third_notice.Replace("((" + Resources.Resource.lbl_tag_unpaid_amount + "))", String.Format("{0:0.00}", amount_owed));
                third_notice = third_notice.Replace("((" + Resources.Resource.lbl_tag_days_late + "))", days_late.ToString());// +String.Format("{0:0.00}", amount_owed);

                tbx_first_notice.Content = first_notice;
                tbx_second_notice.Content = second_notice;
                tbx_third_notice.Content = third_notice;

            }
        }
        finally
        {
            //  conn.Close();
        }

        cmd4.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd4.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd4.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);


            SqlDataReader dr4 = null;
            dr4 = cmd4.ExecuteReader(CommandBehavior.SingleRow);

            while (dr4.Read() == true)
            {

                double late_fee = Convert.ToDouble(dr4["tt_late_fee"]);
                double total = 0;
                total = late_fee + amount_owed;

                first_notice = first_notice.Replace("((" + Resources.Resource.lbl_tag_late_fee + "))", String.Format("{0:0.00}", late_fee));
                first_notice = first_notice.Replace("((" + Resources.Resource.lbl_tag_total + "))", String.Format("{0:0.00}", total));

                second_notice = second_notice.Replace("((" + Resources.Resource.lbl_tag_late_fee + "))", String.Format("{0:0.00}", late_fee));
                second_notice = second_notice.Replace("((" + Resources.Resource.lbl_tag_total + "))", String.Format("{0:0.00}", total));

                third_notice = third_notice.Replace("((" + Resources.Resource.lbl_tag_late_fee + "))", String.Format("{0:0.00}", late_fee));
                third_notice = third_notice.Replace("((" + Resources.Resource.lbl_tag_total + "))", String.Format("{0:0.00}", total));

                tbx_first_notice.Content = first_notice;
                tbx_second_notice.Content = second_notice;
                tbx_third_notice.Content = third_notice;
            }
        }
        finally
        {
            //   conn.Close();
        }



        cmd5.CommandType = CommandType.StoredProcedure;
        try
        {
            //Add the params
            cmd5.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd5.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);

            SqlDataReader dr5 = null;
            dr5 = cmd5.ExecuteReader(CommandBehavior.SingleRow);

            string name = "";
            while (dr5.Read())
            {

                name = dr5["name_fname"] + " " + dr5["name_lname"];

            }

            first_notice = first_notice.Replace("((" + Resources.Resource.lbl_tag_my_name + "))", name);
            second_notice = second_notice.Replace("((" + Resources.Resource.lbl_tag_my_name + "))", name);
            third_notice = third_notice.Replace("((" + Resources.Resource.lbl_tag_my_name + "))", name);


        }
        finally
        {
            conn.Close();
        }

        tbx_first_notice.Content = first_notice.Replace("((" + Resources.Resource.lbl_tag_date + "))", String.Format("{0:MMM dd , yyyy}", la_date)); ;
        tbx_second_notice.Content = second_notice.Replace("((" + Resources.Resource.lbl_tag_date + "))", String.Format("{0:MMM dd , yyyy}", la_date)); ;
        tbx_third_notice.Content = third_notice.Replace("((" + Resources.Resource.lbl_tag_date + "))", String.Format("{0:MMM dd , yyyy}", la_date)); ;

        /////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////
   
    }
}
