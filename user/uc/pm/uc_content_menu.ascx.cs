using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class manager_uc_uc_content_menu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string menu = "";
        switch (System.IO.Path.GetFileName(Request.Path.ToString()))
        {
            case "man_default.aspx":
            case "man_mod_pwd.aspx":
                menu += "<a style='color: #003300;' href='man_default.aspx'>"+Resources.Resource.lbl_home+" </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='man_mod_pwd.aspx'>"+Resources.Resource.lbl_mod_pwd+" </a>&nbsp;&nbsp;";
            break;
        /*    case "group_add.aspx":
            case "group_modify.aspx":
            case "group_profile.aspx":
            case "group_remove.aspx":
                menu += "<a style='color: #003300;' href='group_profile.aspx'>" + Resources.Resource.lbl_group_profile + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='group_add.aspx'>" + Resources.Resource.lbl_group_create + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='group_modify.aspx'>" + Resources.Resource.lbl_group_modify + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='group_remove.aspx'>" + Resources.Resource.lbl_gl_remove + " </a>&nbsp;&nbsp;";
          */      break;
            case "property_view.aspx":
            case "property_add.aspx":
            case "property_update.aspx":
            case "property_unit_add.aspx":
            case "property_list.aspx":
            case "property_unit_list.aspx":
            case "property_profile.aspx":
            case "property_default.aspx":
                menu += "<a style='color: #003300;' href='property_default.aspx'>" + Resources.Resource.lbl_gl_home + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='property_list.aspx?h_id=" + Request.QueryString["h_id"] + "'>" + Resources.Resource.lbl_gl_view + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='property_add.aspx?h_id=" + Request.QueryString["h_id"] + "'>" + Resources.Resource.lbl_gl_add + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='property_update.aspx?h_id="+Request.QueryString["h_id"]+"'>" + Resources.Resource.lbl_gl_update + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='property_unit_list.aspx?h_id=" + Request.QueryString["h_id"] + "'>" + Resources.Resource.lbl_gl_unit_list + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='property_unit_add.aspx?h_id=" + Request.QueryString["h_id"] + "'>" + Resources.Resource.lbl_gl_unit_add + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='property_profile.aspx?h_id=" + Request.QueryString["h_id"] + "'>" + Resources.Resource.lbl_gl_profile + " </a>&nbsp;&nbsp;";
      
                break;
            case "role_wiz.aspx":
            case "role_create_account.aspx":
                menu += "<a style='color: #003300;' href='property_unit_list.aspx?h_id=" + Request.QueryString["h_id"] + "'>" + Resources.Resource.lbl_gl_unit_list + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='property_unit_list.aspx?h_id=" + Request.QueryString["h_id"] + "'>" + Resources.Resource.lbl_gl_create_account + " </a>&nbsp;&nbsp;";
     
                break;
            case "tenant_default.aspx":
            case "tenant_view.aspx":
            case "tenant_search.aspx":
            case "tenant_prospect_list.aspx":
            case "tenant_prospect_add.aspx":
            case "tenant_list.aspx":
            case "tenant_update.aspx":
                menu += "<a style='color: #003300;' href='tenant_default.aspx'>" + Resources.Resource.lbl_gl_home + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='tenant_view.aspx?n_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_view + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='tenant_update.aspx?h_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_update + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='tenant_prospect_add.aspx'>" + Resources.Resource.lbl_gl_prospect_add + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='tenant_prospect_add.aspx'>" + Resources.Resource.lbl_gl_tenant_delinquant + " </a>&nbsp;&nbsp;";

                menu += "<a style='color: #003300;' href='tenant_search.aspx?h=0&l='>" + Resources.Resource.lbl_gl_search + " </a>&nbsp;&nbsp;";
                
                menu += "<a style='color: #003300;' href='manager/group/profile_view.aspx?n_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_profile + " </a>&nbsp;&nbsp;";
            break;
            case "lease_add.aspx":
            case "lease_view.aspx":
            case "lease_list.aspx":
            case "timeline_lease.aspx":
            case "lease_rent_update.aspx":
            case "lease_term_cond_update.aspx":
            case "lease_accommodation_update.aspx":

            menu += "<a style='color: #003300;' href='/user/pm/lease/lease_list.aspx?n_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_view + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/lease/lease_add.aspx?h_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/timeline/timeline_lease.aspx?n_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_timeline + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/lease/lease_accommodation_update.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_lease_update_accomodation + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/lease/lease_rent_update.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_lease_update_rent + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/lease/lease_term_cond_update.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_lease_update_terms + " </a>&nbsp;&nbsp;";
            break;
            case "alerts.aspx":
            case "calendar.aspx":
            case "incident_list.aspx":
            case "filemanager.aspx":
            case "appliance_list.aspx":
            menu += "<a style='color: #003300;' href='/user/pm/alerts/alerts.aspx?n_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_alert +" </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/scheduler/calendar.aspx?h_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_calendar + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/filemanager/filemanager.aspx?n_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_file_manager + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/appliance/appliance_list.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_appliance + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/appliance/appliance_moving.aspx" + Request.QueryString["n_id"] + "'>" + "appliance moving" + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/appliance/appliance_moving_wiz.aspx" + Request.QueryString["n_id"] + "'>" + "appliance moving wiz" + " </a>&nbsp;&nbsp;";
            break;
            case "mortgage_amortization.aspx":
            menu += "<a style='color: #003300;' href='/user/pm/mortgage/mortgage_amortization.aspx?n_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_mortgage + " </a>&nbsp;&nbsp;";
             break;
             case "home_unit_rented_list.aspx":
            case "tenant_cancel_rent_payment.aspx":
            case "income_late_rent_fees.aspx":
            case "financial_income.aspx":
            case "tenant_rent_update.aspx":
            case "income_default.aspx":
            case "income_report.apsx":
            menu += "<a style='color: #003300;' href='/user/pm/home/home_unit_rented_list.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_rent_income + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/tenant/tenant_cancel_rent_payment.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_rent_income_update + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/income/income_late_rent_fees.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_rent_late + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/Financial/financial_income.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_income_report + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/Financial/financial_income.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_income_other + " </a>&nbsp;&nbsp;";
            break;
            case "role_member_list.aspx":
            case "role_wiz1.aspx":
            case "role_update1.aspx":
            menu += "<a style='color: #003300;' href='/user/pm/role/role_member_list.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_role_user + " </a>&nbsp;&nbsp;";
             menu += "<a style='color: #003300;' href='/user/pm/role/role_wiz.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_role_assign + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/role/role_update.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_role_update + " </a>&nbsp;&nbsp;";
           
            break;
            case "financial_expenses.aspx":
            case "financial_report.aspx":
            menu += "<a style='color: #003300;' href='/user/pm/financial/financial_expense_view.aspx'>" + Resources.Resource.lbl_gl_view + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/financial/financial_expenses.aspx'>" + Resources.Resource.lbl_gl_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/financial/expense_report.aspx'>" + Resources.Resource.lbl_gl_report + " </a>&nbsp;&nbsp;";
           
            break;
            case "contact_list.aspx":
            case "company_add.aspx?c=c":
            case "company_add.aspx?c=s":
            case "financial_institution_add.aspx?c=":
            case "insurance_company_add.aspx?c=":
            case "wharehouse_add.aspx?c=":
            menu += "<a style='color: #003300;' href='/user/pm/contact/contact_list.aspx'>" + Resources.Resource.lbl_gl_view + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/company/company_add.aspx?c=c'>" + Resources.Resource.lbl_gl_contact_contractor_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/company/company_add.aspx?c=s'>" + Resources.Resource.lbl_gl_contact_vendor_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/financialinstitution/financial_institution_add.aspx'>" + Resources.Resource.lbl_gl_contact_fi_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/contact/contact_list.aspx'>" + Resources.Resource.lbl_gl_view + " </a>&nbsp;&nbsp;";
            
            break;
                //rent
            case "tenant_untreated_rent.aspx":
            case "rent_default.aspx":
            menu += "<a style='color: #003300;' href='/user/pm/rent/rent_default.aspx'>" + Resources.Resource.lbl_gl_home + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/tenant/tenant_untreated_rent.aspx'>" + Resources.Resource.lbl_gl_tenant_untreated + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/'>" + Resources.Resource.lbl_gl_contact_contractor_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/company/company_add.aspx?c=c'>" + Resources.Resource.lbl_gl_contact_contractor_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/company/company_add.aspx?c=c'>" + Resources.Resource.lbl_gl_contact_contractor_add + " </a>&nbsp;&nbsp;";
            break;
            case "company_list.aspx":
            case "company_add.aspx":
            case "warehouse_add.aspx":
            case "insurance_company_add.aspx":
            case "financial_institution_add.aspx":
            menu += "<a style='color: #003300;' href='/user/pm/company/company_add.aspx?c=c'>" + Resources.Resource.lbl_gl_contact_contractor_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/company/company_add.aspx?c=s'>" + Resources.Resource.lbl_gl_contact_contractor_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/financialinstitution/financial_institution_add.aspx?c=c'>Financial institution</a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/insurancecompany/insurance_company_add.aspx?c=c'>Insurance company </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/warehouse/warehouse_add.aspx?c=c'>Warehouse </a>&nbsp;&nbsp;";
            break;
            case "financial_analysis.aspx":
            case "financial_analysis_period.aspx":
            case "financial_group_analysis.aspx":
            case "financial_group_analysis_period.aspx":

            menu += "<a style='color: #003300;' href='/user/pm/financial/financial_analysis.aspx'>by property ( Monthly ) </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/financial/financial_analysis_period.aspx'>by property ( Date Range ) </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/financial/financial_group_analysis.aspx'>by group ( Monthly ) </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/financial/financial_group_analysis_period.aspx'>by group ( Date Range ) </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/financial/financial_scenario_list.aspx'>" + Resources.Resource.lbl_gl_fscenario_list + " </a>&nbsp;&nbsp;";
            break;


            case "financial_scenario_list.aspx":
            case "financial_scenario.aspx":
            case "financial_scenario_update.aspx":            
            menu += "<a style='color: #003300;' href='/user/pm/financial/financial_scenario.aspx'>Financial scenario add  </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/financial/financial_scenario_list.aspx'>Financial scenario list </a>&nbsp;&nbsp;";
            break;

            case "mortgage_list.aspx":
            case "insurance_policy_list.aspx":
            menu += "<a style='color: #003300;' href='/user/pm/mortgage/mortgage_list.aspx'>Property mortgage</a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/insurancepolicy/insurance_policy_list.aspx'>Property Insurance Policy </a>&nbsp;&nbsp;";
            break;
            case "name_profile.aspx":
            case "name_search.aspx":
            case "group_default.aspx":
            case "group_modify.aspx":
            case "group_profile.aspx":
            case "group_remove.aspx":
           
            menu += "<a style='color: #003300;' href='/user/pm/group/group_default.aspx'>Profiles home</a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/group/group_profile.aspx'>Property</a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/user/pm/name/name_profile.aspx'>Person </a>&nbsp;&nbsp;";
            //menu += "<a style='color: #003300;' href='/user/pm/name/name_search.aspx'>Search </a>&nbsp;&nbsp;";
            //menu += "<a style='color: #003300;' href='/user/pm/name/name_img_upload.aspx'>Add/Modify image</a>&nbsp;&nbsp;";
        
                break;

            case "pm_main.aspx":

                menu += "<b ><span style='color: black;font-size:12px;'>PROPERTY MANAGER DASHBOARD</span></b>&nbsp;&nbsp;";
                break;
         }
        //menu += "<FORM method='get'><INPUT type='button' value='Help' class='letter' onClick='window.open('http://www.familyship.com/private/familyship_help.php?name=member_lineage','mywindow','width=300,height=400,scrollbars=yes,screenX=0,screenY=0')'></FORM>";
        content_menu.InnerHtml = menu;

    }
}
