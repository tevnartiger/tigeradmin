using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class manager_uc_uc_adm_lmenu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

/*
        if (!Page.IsPostBack)
        {
            pro_7.SelectedId = "b";
          //  pro_7.Height = 190;
        }
      
*/
         
       /* if (!Page.IsPostBack)
        {
            
          //  Reg_search.ValidationExpression =  tiger.security.RegEx.getName();
            tiger.Owner o = new tiger.Owner(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
           // ddl_home_list.DataSource = o.getOwnerList(Convert.ToInt32(Session["schema_id"]));
           // ddl_home_list.DataBind();
           // ddl_home_list.Items.Insert(0, new ListItem(Resources.Resource.dd_owner, "0"));
           
            tiger.Group g = new tiger.Group(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
          //  ddl_group_list.DataSource =  g.getGroupHomeList(Convert.ToInt32(Session["schema_id"]));
          //  ddl_group_list.DataBind();
          //  ddl_group_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_group_profile, "0"));
           
            string menu = "";
            switch (System.IO.Path.GetFileName(Request.Path.ToString()))
            {
               /* case "group_home_add2.aspx":
                    break;
                case "group_add.aspx":
                    menu = "<h2>Group</h2>";
                    menu += "<ul>";
                    menu += "<li><a href='group_list.aspx'>List</a></li>";
                    menu += "<li><a href='group_add.aspx'>New</a></li>";
                    menu += "<li><a href='group_home_add.aspx'>Link to property</a></li>";
                    menu += "</ul>";

                    break;

                case "property_default.aspx":
                case "property_add.aspx":
                case "property_list.aspx":
                case "property_create.aspx":
                case "property_unit_add.aspx":
                case "property_view.aspx":
                case "property_mortgage_update.aspx":
                case "property_mortgage_add.aspx":
                case "property_mortgage_list.aspx":
                    menu = "<h2>Property</h2>";
                    menu += "<ol>";
                    //   menu += "<li><a href='property_add.aspx'>"+Resources.Resource.lbl_menu_dossier+"</a></li>"; 

                    menu += "<li><a href='property_add.aspx'>" + Resources.Resource.lbl_menu_add + "</a></li>";
                    menu += "<li><a href='property_list.aspx'>" + Resources.Resource.lbl_menu_view + "</a></li>";
                    // menu += "<li><a href='property_unit_list.aspx?h_id="+Request.QueryString["h_id"].ToString()+"'>View Units</a></li>";
                    // menu += "<li><a href='property_unit_add.aspx?h_id=" + Request.QueryString["h_id"].ToString() + "'>Add Units</a></li>";

                    menu += "<li><a href='property_mortgage_list.aspx'>View mortgage</a></li>";

                    //menu += "<li><a href='group_home_add.aspx'>Link to property</a></li>";
                    menu += "</ol>";
                    break;
                case "mortgage_update.aspx":
                case "mortgage_add.aspx":
                case "mortgage_list.aspx":
                    menu = "<h2>Mortgage</h2>";
                    menu += "<ul>";
                    menu += "<li><a href='mortgage_add.aspx'>New</a></li>";
                    menu += "<li><a href='mortgage_list.aspx'>View</a></li>";

                    //menu += "<li><a href='group_home_add.aspx'>Link to property</a></li>";
                    menu += "</ul>";
                    break;
                case "insurance_policy_update.aspx":
                case "insurance_policy_add.aspx":
                case "insurance_list.aspx":
                    menu = "<h2>Insurance Policy</h2>";
                    menu += "<ul>";
                    menu += "<li><a href='insurance_policy_add.aspx'>New</a></li>";
                    menu += "<li><a href='insurance_policy_list.aspx'>View</a></li>";
                    menu += "</ul>";



                    break;
                case "tenant_prospect_list.aspx":
                case "tenant_prospect_add.aspx":
                case "tenant_view.aspx":
                case "tenant_inactive_view.aspx":
                case "tenant_search.aspx":
                    menu = "<h2>Tenant</h2>";
                    menu += "<ul>";
                    menu += "<li><a href='tenant_search.aspx'>Tenant List</a></li>";
                    menu += "<li><a href='tenant_prospect_list.aspx'>Prospect List</a></li>";
                    menu += "<li><a href='tenant_prospect_add.aspx'>New Prospect</a></li>";

                    //menu += "<li><a href='group_home_add.aspx'>Link to property</a></li>";
                    menu += "</ul>";

                    break;
                case "lease_list.aspx":
                case "lease_add.aspx":
                case "lease_add1.aspx":
                case "lease_add2.aspx":
                case "lease_add3.aspx":
                case "lease_add4.aspx":
                case "lease_accommodation_update.aspx":
                case "lease_add_acc_clause.aspx":
                case "lease_rent_update.aspx":
                case "lease_term_con_update.aspx":
                    menu = "<h2>Lease</h2>";
                    menu += "<ul>";
                    menu += "<li><a href='lease_add.aspx'>New</a></li>";
                    menu += "<li><a href='lease_list.aspx'>View</a></li>";
                    menu += "<li><a href='lease_list.aspx'>All Changes</a></li>";
                    //menu += "<li><a href='group_home_add.aspx'>Link to property</a></li>";
                    menu += "</ul>";

                    break;
                case "rent_payment_processing.aspx":
                    menu = "<h2>Income</h2>";
                    menu += "<ul>";
                    menu += "<li><a href='rent_payment_processing.aspx'>Rent processing</a></li>";

                    //menu += "<li><a href='group_home_add.aspx'>Link to property</a></li>";
                    menu += "</ul>";

                    break;

                case "expense_processing_list.aspx":
                case "expense_processing.aspx":
                    menu = "<h2>Expense</h2>";
                    menu += "<ul>";
                    menu += "<li><a href='expense_processing_list.aspx'>Expense processing list</a></li>";

                    menu += "<li><a href='expense_processing.aspx'>Expense processing</a></li>";

                    //menu += "<li><a href='group_home_add.aspx'>Link to property</a></li>";
                    menu += "</ul>";

                    break;
                case "insurance_company_list.aspx":
                case "insurance_company_update.aspx":
                case "insurance_company_add.aspx":
                case "insurance_company_view.aspx":
                    menu = "<h2>Insurance Cie</h2>";
                    menu += "<ul>";
                    menu += "<li><a href='insurance_company_add.aspx'>New</a></li>";

                    menu += "<li><a href='insurance_company_list.aspx'>View</a></li>";

                    //menu += "<li><a href='group_home_add.aspx'>Link to property</a></li>";
                    menu += "</ul>";

                    break;
           
                case "property_add.aspx":
                case "property_list.aspx":
                case "home_main.aspx":
                    panel_property.Visible = true;
                    panel_home.Visible = false;
                    panel_daily.Visible = false;
                    q.Visible = false;
                    panel_accounting.Visible = false;
                    panel_tool.Visible = false;
                    panel_archive.Visible = false;
                    break;
                case "group_view.aspx":
panel_home.Visible = true;
panel_property.Visible = false;
panel_daily.Visible = false;
q.Visible = false;
panel_accounting.Visible = false;
panel_tool.Visible = false;
panel_archive.Visible = false;
break;
                case "alerts.aspx":
                case "lease_add.aspx":
                case "appliance_list.aspx":
                case "income_late_rent_fees.aspx":
panel_daily.Visible = true;
q.Visible = false;
panel_home.Visible = false;
panel_property.Visible = false;
panel_accounting.Visible = false;
panel_tool.Visible = false;
panel_archive.Visible = false;
break;
                case "role_wiz.aspx":
q.Visible = true;
panel_home.Visible = false;
panel_property.Visible = false;
panel_daily.Visible = false;
panel_accounting.Visible = false;
panel_tool.Visible = false;
panel_archive.Visible = false;
break;
                case "financial_income_graph.aspx":
                case "financial_expenses_graph.aspx":
                case "financial_scenario_list.aspx":
                case "mortgage_add.aspx":
q.Visible = false;
panel_home.Visible = false;
panel_property.Visible = false;
panel_daily.Visible = false;
panel_accounting.Visible = true;
panel_tool.Visible = false;
panel_archive.Visible = false;
break;
                case "mortgage_amortization.aspx":
q.Visible = false;
panel_home.Visible = false;
panel_property.Visible = false;
panel_daily.Visible = false;
panel_accounting.Visible = false;
panel_tool.Visible = true;
panel_archive.Visible = false;
break;
                case "tenant_rent_archives.aspx":
q.Visible = false;
panel_home.Visible = false;
panel_property.Visible = false;
panel_daily.Visible = false;
panel_accounting.Visible = false;
panel_tool.Visible = false;
panel_archive.Visible = true;
break;
                  default  :
//panel_home.Visible = true;
q.Visible = false;
panel_home.Visible = true;
panel_property.Visible = false;
panel_daily.Visible = false;
panel_accounting.Visible = false;
panel_tool.Visible = false;
panel_archive.Visible = false;
                    break; 
            }
            //menu += "<FORM method='get'><INPUT type='button' value='Help' class='letter' onClick='window.open('http://www.familyship.com/private/familyship_help.php?name=member_lineage','mywindow','width=300,height=400,scrollbars=yes,screenX=0,screenY=0')'></FORM>";
            lm.InnerHtml = menu;
        }

       */
    }
   /* protected void ddl_home_list_SelectedIndexChanged(object sender, EventArgs e)
    {   
        Server.Transfer("~/manager/home/home_profile.aspx", false);
    }
    */
    /*
    protected void ddl_group_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        Server.Transfer("~/manager/group/group_profile.aspx?group_id={0}", false);
    }*/


  /*  protected void selectedSlide(object sender, EventArgs e)
    {
        if (pro_7.SelectedId == "a")
            pro_7.SelectedId = "a";
    }
    */
}

