using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class manager_uc_uc_content_menu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
       

        string menu = "";
        switch (System.IO.Path.GetFileName(Request.Path.ToString()))
        {
            case "man_default.aspx":
            case "man_mod_pwd.aspx":
                menu += "<a style='color: #003300;' href='man_default.aspx'>"+Resources.Resource.lbl_home+" </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='man_mod_pwd.aspx'>"+Resources.Resource.lbl_mod_pwd+" </a>&nbsp;&nbsp;";
            break;
        /*    case "group_add.aspx":
            case "group_modify.aspx":
            case "group_profile.aspx":
            case "group_remove.aspx":
                menu += "<a style='color: #003300;' href='group_profile.aspx'>" + Resources.Resource.lbl_group_profile + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='group_add.aspx'>" + Resources.Resource.lbl_group_create + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='group_modify.aspx'>" + Resources.Resource.lbl_group_modify + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='group_remove.aspx'>" + Resources.Resource.lbl_gl_remove + " </a>&nbsp;&nbsp;";
               break;
         */
            case "property_view.aspx":
            case "property_add.aspx":
            case "property_update.aspx":
            case "property_unit_add.aspx":
            case "property_list.aspx":
            case "property_unit_list.aspx":
            case "property_profile.aspx":
            case "property_default.aspx":
                menu += "<a style='color: #003300;' href='property_default.aspx'>" + Resources.Resource.lbl_gl_home + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='property_list.aspx?h_id=" + Request.QueryString["h_id"] + "'>" + Resources.Resource.lbl_gl_view + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='property_add.aspx?h_id=" + Request.QueryString["h_id"] + "'>" + Resources.Resource.lbl_gl_add + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='property_update.aspx?h_id="+Request.QueryString["h_id"]+"'>" + Resources.Resource.lbl_gl_update + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='property_unit_list.aspx?h_id=" + Request.QueryString["h_id"] + "'>" + Resources.Resource.lbl_gl_unit_list + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='property_unit_add.aspx?h_id=" + Request.QueryString["h_id"] + "'>" + Resources.Resource.lbl_gl_unit_add + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='property_profile.aspx?h_id=" + Request.QueryString["h_id"] + "'>" + Resources.Resource.lbl_gl_profile + " </a>&nbsp;&nbsp;";
      
                break;
            case "role_wiz.aspx":
            case "role_create_account.aspx":
                menu += "<a style='color: #003300;' href='property_unit_list.aspx?h_id=" + Request.QueryString["h_id"] + "'>" + Resources.Resource.lbl_gl_unit_list + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #003300;' href='property_unit_list.aspx?h_id=" + Request.QueryString["h_id"] + "'>" + Resources.Resource.lbl_gl_create_account + " </a>&nbsp;&nbsp;";
     
                break;
            case "tenant_default.aspx":
            case "tenant_view.aspx":
            case "tenant_search.aspx":
            case "tenant_prospect_list.aspx":
            case "tenant_prospect_add.aspx":
            case "tenant_list.aspx":
            case "tenant_update.aspx":

                menu += "<b ><span style='color: black'>Modules  access:</span></b>&nbsp;&nbsp;";

                if (Session["is_owner"].ToString() == "1")
                    menu += "<a style='color: #003300;' href='/user/owner/owner_main.aspx'>Owner</a>&nbsp;|&nbsp;";
                if (Session["is_tenant"].ToString() == "1")
                    menu += "<a style='color: #003300;' href='/user/tenant/tenant_main.aspx'>Tenant</a>&nbsp;|&nbsp;";
                if (Session["is_janitor"].ToString() == "1")
                    menu += "<a style='color: #003300;' href='/user/janitor/janitor_main.aspx'>Janitor</a>&nbsp;|&nbsp;";
                if (Session["is_pm"].ToString() == "1")
                    menu += "<a style='color: #003300;' href='/user/pm/ipm_main.aspx'>Property manager </a>&nbsp;|&nbsp;";
                break;
            case "lease_add.aspx":
            case "lease_view.aspx":
            case "lease_list.aspx":
            case "timeline_lease.aspx":
            case "lease_rent_update.aspx":
            case "lease_term_cond_update.aspx":
            case "lease_accommodation_update.aspx":

            menu += "<a style='color: #003300;' href='/manager/lease/lease_list.aspx?n_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_view + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/lease/lease_add.aspx?h_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/timeline/timeline_lease.aspx?n_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_timeline + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/lease/lease_accommodation_update.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_lease_update_accomodation + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/lease/lease_rent_update.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_lease_update_rent + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/lease/lease_term_cond_update.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_lease_update_terms + " </a>&nbsp;&nbsp;";
            break;
            case "alerts.aspx":
            case "calendar.aspx":
            case "incident_list.aspx":
            case "filemanager.aspx":
            case "appliance_list.aspx":
            menu += "<a style='color: #003300;' href='/manager/alerts/alerts.aspx?n_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_alert +" </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/scheduler/calendar.aspx?h_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_calendar + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/filemanager/filemanager.aspx?n_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_file_manager + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/appliance/appliance_list.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_appliance + " </a>&nbsp;&nbsp;";
            break;
            case "mortgage_amortization.aspx":
            menu += "<a style='color: #003300;' href='/manager/mortgage/mortgage_amortization.aspx?n_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_mortgage + " </a>&nbsp;&nbsp;";
             break;
             case "home_unit_rented_list.aspx":
            case "tenant_cancel_rent_payment.aspx":
            case "income_late_rent_fees.aspx":
            case "financial_income.aspx":
            case "tenant_rent_update.aspx":
            case "income_default.aspx":
            case "income_report.apsx":
            menu += "<a style='color: #003300;' href='/manager/home/home_unit_rented_list.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_rent_income + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/tenant/tenant_cancel_rent_payment.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_rent_income_update + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/income/income_late_rent_fees.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_rent_late + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/Financial/financial_income.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_income_report + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/Financial/financial_income.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_income_other + " </a>&nbsp;&nbsp;";
            break;
            case "role_member_list.aspx":
            case "role_wiz1.aspx":
            case "role_update1.aspx":
            menu += "<a style='color: #003300;' href='/manager/role/role_member_list.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_role_user + " </a>&nbsp;&nbsp;";
             menu += "<a style='color: #003300;' href='/manager/role/role_wiz.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_role_assign + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/role/role_update.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_role_update + " </a>&nbsp;&nbsp;";
           
            break;
            case "financial_expenses.aspx":
            case "financial_report.aspx":
            menu += "<a style='color: #003300;' href='/manager/financial/financial_expense_view.aspx'>" + Resources.Resource.lbl_gl_view + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/financial/financial_expenses.aspx'>" + Resources.Resource.lbl_gl_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/financial/expense_report.aspx'>" + Resources.Resource.lbl_gl_report + " </a>&nbsp;&nbsp;";
           
            break;
            case "contact_list.aspx":

            menu += "<b ><span style='color: black'>Modules  access:</span></b>&nbsp;&nbsp;";

            if (Session["is_owner"].ToString() == "1")
                menu += "<a style='color: #003300;' href='/user/owner/owner_main.aspx'>Owner</a>&nbsp;|&nbsp;";
            if (Session["is_tenant"].ToString() == "1")
                menu += "<a style='color: #003300;' href='/user/tenant/tenant_main.aspx'>Tenant</a>&nbsp;|&nbsp;";
            if (Session["is_janitor"].ToString() == "1")
                menu += "<a style='color: #003300;' href='/user/janitor/janitor_main.aspx'>Janitor</a>&nbsp;|&nbsp;";
            if (Session["is_pm"].ToString() == "1")
                menu += "<a style='color: #003300;' href='/user/pm/ipm_main.aspx'>Property manager </a>&nbsp;|&nbsp;";

            break;


            case "company_add.aspx?c=c":
            case "company_add.aspx?c=s":
            case "financial_institution_add.aspx?c=":
            case "insurance_company_add.aspx?c=":
            case "wharehouse_add.aspx?c=":
            menu += "<a style='color: #003300;' href='/manager/contact/contact_list.aspx'>" + Resources.Resource.lbl_gl_view + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/company/company_add.aspx?c=c'>" + Resources.Resource.lbl_gl_contact_contractor_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/company/company_add.aspx?c=s'>" + Resources.Resource.lbl_gl_contact_vendor_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/financialinstitution/financial_institution_add.aspx'>" + Resources.Resource.lbl_gl_contact_fi_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/contact/contact_list.aspx'>" + Resources.Resource.lbl_gl_view + " </a>&nbsp;&nbsp;";
            
            break;
                //rent
            case "tenant_untreated_rent.aspx":
            case "rent_default.aspx":
            menu += "<a style='color: #003300;' href='/manager/rent/rent_default.aspx'>" + Resources.Resource.lbl_gl_home + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/tenant/tenant_untreated_rent.aspx'>" + Resources.Resource.lbl_gl_tenant_untreated + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/'>" + Resources.Resource.lbl_gl_contact_contractor_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/company/company_add.aspx?c=c'>" + Resources.Resource.lbl_gl_contact_contractor_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #003300;' href='/manager/company/company_add.aspx?c=c'>" + Resources.Resource.lbl_gl_contact_contractor_add + " </a>&nbsp;&nbsp;";
            break;
            

            case "name_profile.aspx":
              menu += "<b ><span style='color: black'>Modules  access:</span></b>&nbsp;&nbsp;";
   
              if(Session["is_owner"].ToString()=="1")
                menu += "<a style='color: #003300;' href='/user/owner/owner_main.aspx'>Owner</a>&nbsp;|&nbsp;";
              if (Session["is_tenant"].ToString() == "1")
                  menu += "<a style='color: #003300;' href='/user/tenant/tenant_main.aspx'>Tenant</a>&nbsp;|&nbsp;";
              if (Session["is_janitor"].ToString() == "1")
                  menu += "<a style='color: #003300;' href='/user/janitor/janitor_main.aspx'>Janitor</a>&nbsp;|&nbsp;";
              if (Session["is_pm"].ToString() == "1")
                  menu += "<a style='color: #003300;' href='/user/pm/ipm_main.aspx'>Property manager </a>&nbsp;|&nbsp;";
              break;




            case "user_files.aspx":
              menu += "<b ><span style='color: black'>Modules  access:</span></b>&nbsp;&nbsp;";

              if (Session["is_owner"].ToString() == "1")
                  menu += "<a style='color: #003300;' href='/user/owner/owner_main.aspx'>Owner</a>&nbsp;|&nbsp;";
              if (Session["is_tenant"].ToString() == "1")
                  menu += "<a style='color: #003300;' href='/user/tenant/tenant_main.aspx'>Tenant</a>&nbsp;|&nbsp;";
              if (Session["is_janitor"].ToString() == "1")
                  menu += "<a style='color: #003300;' href='/user/janitor/janitor_main.aspx'>Janitor</a>&nbsp;|&nbsp;";
              if (Session["is_pm"].ToString() == "1")
                  menu += "<a style='color: #003300;' href='/user/pm/ipm_main.aspx'>Property manager </a>&nbsp;|&nbsp;";
              break;


            
              
         }
        //menu += "<FORM method='get'><INPUT type='button' value='Help' class='letter' onClick='window.open('http://www.familyship.com/private/familyship_help.php?name=member_lineage','mywindow','width=300,height=400,scrollbars=yes,screenX=0,screenY=0')'></FORM>";
        content_menu.InnerHtml = menu;

    }
}
