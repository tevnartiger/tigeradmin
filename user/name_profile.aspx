﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/mp_default.master" AutoEventWireup="true" CodeFile="name_profile.aspx.cs" Inherits="manager_name_name_profile_view" %>

<%-- 
<%@ Register Src="/manager/uc/uc_content_menu_group.ascx" TagName="uc_content_menu_group" TagPrefix="uc_group" %>
--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="name_id" runat="server" />
 
 <%--
  <uc_group:uc_content_menu_group />
  --%>
  <h3><asp:Label ID="txt" ForeColor="Red" runat="server" /></h3>
<h2><asp:Label ID="lbl_name" runat="server" /> </h2>
    <table style="width: 100%">
        <tr >
            <td valign="top" width="230" >
                          <asp:Repeater runat="server" 
                        ID="rNameImage"><ItemTemplate>
                                        <asp:Image ID="name_photo" 
                                            runat="server" Height="179" 
                                            ImageUrl='<%# "~/mediahandler/NameImage.ashx?n_id="+ Eval("name_id") %>' 
                                            Width="196" /></ItemTemplate>
</asp:Repeater>

                            <br />
                            <asp:Panel ID="panel_lb" runat="server">
                            <asp:LinkButton ID="lb_upload" runat="server" Text="Update Picture"  OnClick="lb_upload_OnClick"/>
                            </asp:Panel>
<asp:Panel ID="panel_upload" runat="server" Visible="false">
<asp:FileUpload ID="FileUpload1" runat="server" Visible="true" />
    &nbsp;
    <asp:Button ID="bn_upload" runat="server" OnClick="bn_upload_OnCLick" 
        text="Upload photo" />
</asp:Panel><br />
            
            </td>
            <td valign="top">
  

                Tel. :<asp:Label ID="lbl_tel" runat="server" />
<br />Tel. Work :<asp:Label ID="lbl_tel_work" runat="server" />
<br />Cell :<asp:Label ID="lbl_cell" runat="server" />
<br />Fax :<asp:Label ID="lbl_fax" runat="server" />
<br />email :<asp:Label ID="lbl_email" runat="server" />
<br />Comments:<asp:Label ID="lbl_com" runat="server" />
  

                            <br />
                <br />
<div ID="lb_txt0" runat="server" />
     <asp:GridView ID="gv_person_role" runat="server" AutoGenerateColumns="false" 
         Width="50%">
         <Columns>
         
    
   
             <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderText="<%$ Resources:Resource, lbl_gl_roles %>">
                 <ItemTemplate>
                     <asp:Label ID="lbl_role_name" runat="server" 
                         Text='<%#GetRoleName(Convert.ToInt32(Eval("role_id")))%>' />
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_property %>">
                 <ItemTemplate>
                     <asp:Repeater ID="r_property" runat="server" 
                         DataSource='<%#GetPropertyForNameRole(Convert.ToInt32(Eval("role_id")))%>'>
                         <ItemTemplate>
                           <%#DataBinder.Eval(Container.DataItem, "home_name")%><br />
                         </ItemTemplate>
                         
                     </asp:Repeater>
                 </ItemTemplate>
             </asp:TemplateField>
         </Columns>
     </asp:GridView>
     
     <br />
      

                            </td>
        </tr>
    </table>
 <br />
    <div ID="lb_txt" runat="server" />
    <h3><asp:Label ID="lbl_user_leases" runat="server" Text="USER LEASE(S)"></asp:Label></h3>
    <asp:GridView ID="gv_name_lease_list" runat="server" AllowPaging="True" 
           AllowSorting="True"  AutoGenerateColumns="false"               
           DataKeyNames="tu_id" DataSourceID="SqlDataSource1"  Width="50%">
           <Columns>
              
               <asp:BoundField DataField="home_name"  
                   HeaderText="<%$ Resources:Resource, lbl_property %>"  >
                 
                   
               </asp:BoundField>
               <asp:BoundField DataField="unit_door_no"  
                   HeaderText="<%$ Resources:Resource, lbl_door_no %>" >
                 
                   
               </asp:BoundField>
               <asp:BoundField DataField="tu_date_begin" DataFormatString="{0:M-dd-yyyy}"  
                   HeaderText="<%$ Resources:Resource, lbl_lease_date_begin %>">
                 
                   
               </asp:BoundField>
               <asp:BoundField DataField="tu_date_end" DataFormatString="{0:M-dd-yyyy}"  
                   HeaderText="<%$ Resources:Resource, lbl_lease_date_end %>" >
               </asp:BoundField>
               
               <asp:HyperLinkField DataNavigateUrlFields="tu_id,unit_id,home_id,tenant_id,name_id"   
                   DataNavigateUrlFormatString="~/user/tenant/tenant/tenant_view.aspx?tu_id={0}&amp;unit_id={1}&amp;h_id={2}&amp;t_id={3}&amp;n_id={4}" 
                   HeaderText="Tenant File" 
                   Text="File" >
                 
                   
               </asp:HyperLinkField>
           </Columns>
           
       </asp:GridView>
         <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
           ConnectionString="<%$ ConnectionStrings:sinfoca_tigerConnectionString2 %>" 
           SelectCommand="prNameLeaseList" SelectCommandType="StoredProcedure">
           <SelectParameters>
               <asp:SessionParameter Name="schema_id" SessionField="schema_id" Type="Int32" />
               <asp:SessionParameter Name="name_id" SessionField="name_id" Type="Int32" />
               
           </SelectParameters>
       </asp:SqlDataSource>
<br /><br />
</asp:Content>

