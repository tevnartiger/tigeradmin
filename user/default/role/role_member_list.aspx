﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/mp_default.master" AutoEventWireup="true" CodeFile="role_member_list.aspx.cs" Inherits="manager_role_role_member_list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <table width="100%">
           <tr>
               <td bgcolor="aliceblue" style="font-size: small">
                   <b>Management Phonebook</b></td>
           </tr>
       </table>
       &nbsp;<br />
       <asp:Label ID="lbl_member_search" runat="server" style="font-weight: 700" 
           Text="<%$ Resources:Resource, lbl_member_search %>"></asp:Label>
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
       &nbsp;&nbsp;&nbsp;&nbsp;
       <asp:Button ID="submit" runat="server" onclick="submit_Click" 
           Text="<%$ Resources:Resource, lbl_submit %>" />
       <br />
    <hr />      
       <br />
       &nbsp;<asp:HyperLink ID="link_a" runat="server" >[A]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_b" runat="server" >[B]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_c" runat="server" >[C]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_d" runat="server" >[D]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_e" runat="server" >[E]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_f" runat="server" >[F]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_g" runat="server" >[G]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_h" runat="server" >[H]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_i" runat="server" >[I]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_j" runat="server" >[J]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_k" runat="server" >[K]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_l" runat="server" >[L]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_m" runat="server" >[M]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_n" runat="server" >[N]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_o" runat="server" >[O]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_p" runat="server" >[P]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_q" runat="server" >[Q]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_r" runat="server" >[R]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_s" runat="server" >[S]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_t" runat="server" >[T]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_u" runat="server" >[U]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_v" runat="server" >[V]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_w" runat="server">[W]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_x" runat="server" >[X]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_y" runat="server" >[Y]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_z" runat="server" >[Z]</asp:HyperLink>
       <br />
       <br />
       <b>Category</b>&nbsp;
       <asp:DropDownList ID="ddl_member_category" runat="server"  AutoPostBack="true"
        onselectedindexchanged=" ddl_member_category_SelectedIndexChanged">
           <asp:ListItem Text="<%$ Resources:Resource, lbl_all %>" Value="0"></asp:ListItem>
           <asp:ListItem Text="<%$ Resources:Resource, lbl_maintenance_worker %>"  Value="5"></asp:ListItem>
           <asp:ListItem Text="<%$ Resources:Resource, lbl_owner %>" Value="4"></asp:ListItem>
          <asp:ListItem Text="<%$ Resources:Resource, lbl_property_manager %>" Value="6"></asp:ListItem>             
         <asp:ListItem Text="<%$ Resources:Resource, lbl_tenant %>" Value="3"></asp:ListItem>
       
       </asp:DropDownList>
       <br />
       <br />
       <asp:GridView ID="gv_member_list" runat="server" AllowPaging="True" 
           AllowSorting="True" AlternatingRowStyle-BackColor="#F0F0F6"  
           AutoGenerateColumns="false" BorderColor="#CDCDCD" BorderWidth="1" 
           GridLines="Both"
           DataKeyNames="name_id" OnPageIndexChanging="gv_member_list_PageIndexChanging"
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" 
           HeaderStyle-BackColor="AliceBlue" PageSize="10" Width="90%">
           
 
           
          
           
           
           <Columns>
               <asp:BoundField DataField="home_name" 
                   HeaderText="<%$ Resources:Resource, lbl_home_name %>" 
                   />
               <asp:BoundField DataField="lname" 
                   HeaderText="<%$ Resources:Resource, lbl_lname %>" ReadOnly="True" 
                   />
               <asp:BoundField DataField="fname" 
                   HeaderText="<%$ Resources:Resource, lbl_fname %>" ReadOnly="True" 
                    />
               <asp:BoundField DataField="tel" 
                   HeaderText="<%$ Resources:Resource, lbl_tel %>" 
                    />
               <asp:BoundField DataField="email" 
                   HeaderText="<%$ Resources:Resource, lbl_email %>" 
                   />
                
               
           
               
              
                
               <asp:HyperLinkField  Text="<%$ Resources:Resource, lbl_view %>" 
              DataNavigateUrlFields="name_id" 
              DataNavigateUrlFormatString="~/manager/name/name_profile.aspx?name_id={0}" 
              HeaderText="<%$ Resources:Resource, lbl_view %>" />
               
           </Columns>
       </asp:GridView>
       <asp:GridView  ID="gv_member_search_list" runat="server" AllowPaging="True" 
           AllowSorting="True" AlternatingRowStyle-BackColor="#F0F0F6"  
           AutoGenerateColumns="false" BorderColor="#CDCDCD" BorderWidth="1" 
           GridLines="Both" EmptyDataText="<%$ Resources:Resource, lbl_none %>" 
           HeaderStyle-BackColor="AliceBlue" 
           OnPageIndexChanging="gv_member_search_list_PageIndexChanging" PageSize="15" 
           Width="90%">
           <Columns>
                <asp:BoundField DataField="home_name" 
                   HeaderText="<%$ Resources:Resource, lbl_home_name %>" 
                   />
               <asp:BoundField DataField="lname" 
                   HeaderText="<%$ Resources:Resource, lbl_lname %>" ReadOnly="True" 
                    />
               <asp:BoundField DataField="fname" 
                   HeaderText="<%$ Resources:Resource, lbl_fname %>" ReadOnly="True" 
                   />
               <asp:BoundField DataField="tel" 
                   HeaderText="<%$ Resources:Resource, lbl_tel %>" 
                   />
               <asp:BoundField DataField="email" 
                   HeaderText="<%$ Resources:Resource, lbl_email %>" 
                   />
           
                   
              <asp:HyperLinkField  Text="<%$ Resources:Resource, lbl_view %>" 
              DataNavigateUrlFields="name_id" 
              DataNavigateUrlFormatString="~/manager/name/name_profile.aspx?name_id={0}" 
              HeaderText="<%$ Resources:Resource, lbl_view %>" />
               
               
           </Columns>
       </asp:GridView>



</asp:Content>

