﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
/// <summary>
/// Done by :Stanley Jocelyn
/// Date    :may 21 ,2009
/// </summary>
public partial class manager_role_role_member_list : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RegEx.IsInteger(Request.QueryString["categ"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        // the categ must be between 0 and 6
        int category = Convert.ToInt32(Request.QueryString["categ"]);
        if (category > 7 || category < 0)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }


        if (Request.QueryString["l"] != string.Empty && Request.QueryString["l"] != null && Request.QueryString["l"] != "")
        {
            if (!RegEx.IsAlpha(Request.QueryString["l"]))
            {
                Session.Abandon();
                Response.Redirect("~/login.aspx");
            }

        }

        if (!Page.IsPostBack)
        {

            tiger.Role contact = new tiger.Role(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_member_list.DataSource = contact.getRoleNameSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["categ"]),Convert.ToInt32(Session["name_id"]), Request.QueryString["l"]);
            gv_member_list.DataBind();

          //////////////////
            ddl_member_category.Items.Remove(new ListItem(Resources.Resource.lbl_all, "0"));
            // if user is not a pm or a owner it cannot see the owner list and the tenant list
            ddl_member_category.Items.Remove(new ListItem(Resources.Resource.lbl_tenant, "3"));
     
            if (Session["is_pm"].ToString() != "1" && Session["is_owner"].ToString() != "1")
            {
                ddl_member_category.Items.Remove(new ListItem(Resources.Resource.lbl_owner, "4"));
            }

          //////////////////////
         
            if (Request.QueryString["categ"] == "0")
            {
                ddl_member_category.SelectedValue = "0";

                link_a.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=a";
                link_b.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=b";
                link_c.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=c";
                link_d.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=d";
                link_e.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=e";
                link_f.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=f";
                link_g.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=g";
                link_h.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=h";
                link_i.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=i";
                link_j.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=j";
                link_k.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=k";
                link_l.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=l";
                link_m.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=m";
                link_n.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=n";
                link_o.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=o";
                link_p.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=p";
                link_q.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=q";
                link_r.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=r";
                link_s.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=s";
                link_t.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=t";
                link_u.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=u";
                link_v.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=v";
                link_w.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=w";
                link_x.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=x";
                link_y.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=y";
                link_z.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=z";

            }



            if (Request.QueryString["categ"] == "1")
            {
                ddl_member_category.SelectedValue = "1";

                link_a.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=a";
                link_b.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=b";
                link_c.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=c";
                link_d.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=d";
                link_e.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=e";
                link_f.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=f";
                link_g.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=g";
                link_h.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=h";
                link_i.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=i";
                link_j.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=j";
                link_k.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=k";
                link_l.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=l";
                link_m.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=m";
                link_n.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=n";
                link_o.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=o";
                link_p.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=p";
                link_q.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=q";
                link_r.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=r";
                link_s.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=s";
                link_t.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=t";
                link_u.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=u";
                link_v.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=v";
                link_w.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=w";
                link_x.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=x";
                link_y.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=y";
                link_z.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=z";

            }

            if (Request.QueryString["categ"] == "2")
            {
                ddl_member_category.SelectedValue = "2";

                link_a.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=a";
                link_b.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=b";
                link_c.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=c";
                link_d.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=d";
                link_e.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=e";
                link_f.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=f";
                link_g.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=g";
                link_h.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=h";
                link_i.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=i";
                link_j.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=j";
                link_k.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=k";
                link_l.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=l";
                link_m.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=m";
                link_n.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=n";
                link_o.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=o";
                link_p.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=p";
                link_q.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=q";
                link_r.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=r";
                link_s.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=s";
                link_t.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=t";
                link_u.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=u";
                link_v.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=v";
                link_w.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=w";
                link_x.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=x";
                link_y.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=y";
                link_z.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=z";

            }

            if (Request.QueryString["categ"] == "3")
            {
                ddl_member_category.SelectedValue = "3";

                link_a.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=a";
                link_b.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=b";
                link_c.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=c";
                link_d.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=d";
                link_e.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=e";
                link_f.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=f";
                link_g.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=g";
                link_h.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=h";
                link_i.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=i";
                link_j.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=j";
                link_k.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=k";
                link_l.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=l";
                link_m.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=m";
                link_n.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=n";
                link_o.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=o";
                link_p.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=p";
                link_q.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=q";
                link_r.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=r";
                link_s.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=s";
                link_t.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=t";
                link_u.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=u";
                link_v.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=v";
                link_w.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=w";
                link_x.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=x";
                link_y.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=y";
                link_z.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=z";
            }

            if (Request.QueryString["categ"] == "4")
            {
                ddl_member_category.SelectedValue = "4";

                link_a.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=a";
                link_b.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=b";
                link_c.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=c";
                link_d.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=d";
                link_e.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=e";
                link_f.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=f";
                link_g.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=g";
                link_h.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=h";
                link_i.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=i";
                link_j.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=j";
                link_k.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=k";
                link_l.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=l";
                link_m.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=m";
                link_n.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=n";
                link_o.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=o";
                link_p.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=p";
                link_q.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=q";
                link_r.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=r";
                link_s.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=s";
                link_t.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=t";
                link_u.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=u";
                link_v.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=v";
                link_w.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=w";
                link_x.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=x";
                link_y.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=y";
                link_z.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=z";

            }

            if (Request.QueryString["categ"] == "5")
            {
                ddl_member_category.SelectedValue = "5";

                link_a.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=a";
                link_b.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=b";
                link_c.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=c";
                link_d.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=d";
                link_e.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=e";
                link_f.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=f";
                link_g.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=g";
                link_h.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=h";
                link_i.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=i";
                link_j.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=j";
                link_k.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=k";
                link_l.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=l";
                link_m.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=m";
                link_n.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=n";
                link_o.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=o";
                link_p.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=p";
                link_q.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=q";
                link_r.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=r";
                link_s.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=s";
                link_t.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=t";
                link_u.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=u";
                link_v.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=v";
                link_w.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=w";
                link_x.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=x";
                link_y.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=y";
                link_z.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=z";

            }


            if (Request.QueryString["categ"] == "6")
            {
                ddl_member_category.SelectedValue = "6";

                link_a.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=a";
                link_b.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=b";
                link_c.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=c";
                link_d.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=d";
                link_e.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=e";
                link_f.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=f";
                link_g.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=g";
                link_h.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=h";
                link_i.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=i";
                link_j.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=j";
                link_k.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=k";
                link_l.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=l";
                link_m.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=m";
                link_n.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=n";
                link_o.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=o";
                link_p.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=p";
                link_q.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=q";
                link_r.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=r";
                link_s.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=s";
                link_t.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=t";
                link_u.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=u";
                link_v.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=v";
                link_w.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=w";
                link_x.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=x";
                link_y.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=y";
                link_z.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=6&l=z";

            }


        }

    }


    protected void submit_Click(object sender, EventArgs e)
    {
        gv_member_list.Visible = false;
        string name = "";
        tiger.Role contact = new tiger.Role(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_member_search_list.DataSource = contact.getRoleNameSearchList(Convert.ToInt32(Session["schema_id"]), 0,Convert.ToInt32(Session["name_id"]), RegEx.getText(TextBox1.Text));
        gv_member_search_list.DataBind();

        ddl_member_category.SelectedIndex = 0;


        /* WebService2 ser = new WebService2();
         gv_contact_list.Visible = false;
         string name = "";
         tiger.Contact contact = new tiger.Contact(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
         gv_member_search_list.DataSource = ser.getTenantCurrentSearchList(Convert.ToInt32(Session["schema_id"]), 0, TextBox1.Text); //tenant.getTenantCurrentSearchList(Convert.ToInt32(Session["schema_id"]), 0, TextBox1.Text);
         gv_member_search_list.DataBind(); */
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_member_search_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
        tiger.Role contact = new tiger.Role(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_member_search_list.PageIndex = e.NewPageIndex;
        gv_member_search_list.DataSource = contact.getRoleNameSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_member_category.SelectedValue),Convert.ToInt32(Session["name_id"]), RegEx.getText(TextBox1.Text));
        gv_member_search_list.DataBind();

    }
    protected void ddl_member_category_SelectedIndexChanged(object sender, EventArgs e)
    {
        int category = Convert.ToInt32(ddl_member_category.SelectedValue);

        if (category == 0)
        {
            link_a.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=a";
            link_b.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=b";
            link_c.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=c";
            link_d.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=d";
            link_e.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=e";
            link_f.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=f";
            link_g.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=g";
            link_h.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=h";
            link_i.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=i";
            link_j.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=j";
            link_k.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=k";
            link_l.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=l";
            link_m.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=m";
            link_n.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=n";
            link_o.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=o";
            link_p.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=p";
            link_q.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=q";
            link_r.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=r";
            link_s.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=s";
            link_t.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=t";
            link_u.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=u";
            link_v.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=v";
            link_w.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=w";
            link_x.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=x";
            link_y.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=y";
            link_z.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=0&l=z";

        }



        if (category == 1)
        {
            link_a.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=a";
            link_b.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=b";
            link_c.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=c";
            link_d.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=d";
            link_e.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=e";
            link_f.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=f";
            link_g.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=g";
            link_h.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=h";
            link_i.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=i";
            link_j.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=j";
            link_k.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=k";
            link_l.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=l";
            link_m.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=m";
            link_n.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=n";
            link_o.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=o";
            link_p.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=p";
            link_q.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=q";
            link_r.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=r";
            link_s.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=s";
            link_t.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=t";
            link_u.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=u";
            link_v.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=v";
            link_w.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=w";
            link_x.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=x";
            link_y.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=y";
            link_z.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=1&l=z";

        }

        if (category == 2)
        {
            link_a.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=a";
            link_b.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=b";
            link_c.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=c";
            link_d.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=d";
            link_e.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=e";
            link_f.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=f";
            link_g.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=g";
            link_h.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=h";
            link_i.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=i";
            link_j.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=j";
            link_k.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=k";
            link_l.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=l";
            link_m.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=m";
            link_n.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=n";
            link_o.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=o";
            link_p.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=p";
            link_q.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=q";
            link_r.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=r";
            link_s.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=s";
            link_t.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=t";
            link_u.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=u";
            link_v.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=v";
            link_w.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=w";
            link_x.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=x";
            link_y.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=y";
            link_z.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=2&l=z";

        }

        if (category == 3)
        {

            link_a.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=a";
            link_b.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=b";
            link_c.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=c";
            link_d.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=d";
            link_e.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=e";
            link_f.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=f";
            link_g.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=g";
            link_h.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=h";
            link_i.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=i";
            link_j.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=j";
            link_k.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=k";
            link_l.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=l";
            link_m.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=m";
            link_n.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=n";
            link_o.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=o";
            link_p.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=p";
            link_q.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=q";
            link_r.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=r";
            link_s.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=s";
            link_t.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=t";
            link_u.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=u";
            link_v.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=v";
            link_w.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=w";
            link_x.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=x";
            link_y.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=y";
            link_z.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=3&l=z";
        }

        if (category == 4)
        {
            link_a.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=a";
            link_b.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=b";
            link_c.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=c";
            link_d.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=d";
            link_e.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=e";
            link_f.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=f";
            link_g.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=g";
            link_h.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=h";
            link_i.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=i";
            link_j.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=j";
            link_k.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=k";
            link_l.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=l";
            link_m.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=m";
            link_n.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=n";
            link_o.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=o";
            link_p.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=p";
            link_q.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=q";
            link_r.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=r";
            link_s.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=s";
            link_t.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=t";
            link_u.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=u";
            link_v.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=v";
            link_w.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=w";
            link_x.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=x";
            link_y.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=y";
            link_z.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=4&l=z";

        }

        if (category == 5)
        {
            link_a.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=a";
            link_b.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=b";
            link_c.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=c";
            link_d.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=d";
            link_e.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=e";
            link_f.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=f";
            link_g.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=g";
            link_h.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=h";
            link_i.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=i";
            link_j.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=j";
            link_k.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=k";
            link_l.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=l";
            link_m.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=m";
            link_n.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=n";
            link_o.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=o";
            link_p.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=p";
            link_q.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=q";
            link_r.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=r";
            link_s.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=s";
            link_t.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=t";
            link_u.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=u";
            link_v.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=v";
            link_w.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=w";
            link_x.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=x";
            link_y.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=y";
            link_z.NavigateUrl = "~/user/default/role/role_member_list.aspx?categ=5&l=z";

        }


        gv_member_list.Visible = false;

        tiger.Role contact = new tiger.Role(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_member_search_list.DataSource = contact.getRoleNameSearchList(Convert.ToInt32(Session["schema_id"]), category, Convert.ToInt32(Session["name_id"]), "");
        gv_member_search_list.DataBind();

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_member_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.Role contact = new tiger.Role(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_member_list.PageIndex = e.NewPageIndex;
        //gv_contact_list.DataSource = contact.getContactSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["categ"]), Request.QueryString["l"]);
        gv_member_list.DataSource = contact.getRoleNameSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_member_category.SelectedValue),Convert.ToInt32(Session["name_id"]), Request.QueryString["l"]);
        gv_member_list.DataBind();

      

    }


    protected string Get_RoleCateg(int name_id)
    {

            string category = "";
            int role_id = 0;

               SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
               SqlCommand cmd = new SqlCommand("prNameRoleList", conn);
               
               cmd.CommandType = CommandType.StoredProcedure;

               //Add the params
               cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
               cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
               try
               {
                   conn.Open();

                   SqlDataReader dr = null;
                   dr = cmd.ExecuteReader();


                   while (dr.Read() == true)
                   {

                       role_id = Convert.ToInt32(dr["role_id"]);
                      
                       if (role_id == 5)
                       {
                           category = category + Resources.Resource.lbl_maintenance_worker + "<br/>";
                       }

                       if ( role_id == 6)
                       {
                           category = category + Resources.Resource.lbl_property_manager + "<br/>";
                       }

                       if (role_id == 3)
                       {
                         //  category = category + Resources.Resource.lbl_tenant + "<br/>";
                       }

                       if (role_id == 4)
                       {
                           category = category + Resources.Resource.lbl_owner + "<br/>";
                       }


                   }
               }
               finally
               {
                   conn.Close();
               }

        return category;
    }


    protected string Get_Link(string id, string categ)
    {

        string link = "";

       
        if (categ == "5")
        {
            link = "~/user/default/warehouse/warehouse_view.aspx?w_id=" + id;
        }

        if (categ == "6")
        {
            link = "~/user/default/warehouse/warehouse_view.aspx?w_id=" + id;
        }


        return link;
    }


}
