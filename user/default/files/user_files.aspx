<%@ Page Language="C#" MasterPageFile="~/user/mp_default.master" AutoEventWireup="true" CodeFile="user_files.aspx.cs" Inherits="user_files" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asc" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asc:ScriptManager ID="ScriptManager1" runat="server">
</asc:ScriptManager>
       <style type="text/css">
body
{
	font-family:Verdana;
}
.text
{
	background-color:#ebe9ed;
	font-size:11px;
	text-align:center;
}
.textContent
{
	font-size:11px;
	text-align:center;
}
.modalBackground 
{
   background-color:#000;
   filter:alpha(opacity=70);
   opacity:0.7;
}
           .style1
           {
               width: 100%;
           }
       </style>
    <script language="javascript" type="text/javascript">



        function fnClickUpdate(sender, e) {

            var tbx1 = document.getElementById('ctl00_ContentPlaceHolder1_TabContainer1_tab1_TextBox1').value;


            // alert(tbx1);
            if (tbx1.length > 0) {
                __doPostBack(sender, e);
            }


        }


        function ConfirmDelete() {
            if (confirm('Do you wish to delete this file/folder?'))
                return true;
            else
                return false;
        }



        function CheckAllDataViewCheckBoxes(checkVal) {

            for (i = 0; i < document.forms[0].elements.length; i++) {

                elm = document.forms[0].elements[i];
                if (elm.type == 'checkbox' && elm.name.substring(elm.name.length - 11, elm.name.length) == 'chk_process') {
                    { elm.checked = checkVal }
                }
            }
        }


        function CheckAllDataViewCheckBoxes2(checkVal) {

            for (i = 0; i < document.forms[0].elements.length; i++) {

                elm = document.forms[0].elements[i];
                if (elm.type == 'checkbox' && elm.name.substring(elm.name.length - 12, elm.name.length) == 'chk_process2') {
                    { elm.checked = checkVal }
                }
            }
        }

        function CheckAllDataViewCheckBoxes3(checkVal) {

            for (i = 0; i < document.forms[0].elements.length; i++) {

                elm = document.forms[0].elements[i];
                if (elm.type == 'checkbox' && elm.name.substring(elm.name.length - 12, elm.name.length) == 'chk_process3') {
                    { elm.checked = checkVal }
                }
            }
        }


        function CheckAllDataViewCheckBoxes4(checkVal) {

            for (i = 0; i < document.forms[0].elements.length; i++) {

                elm = document.forms[0].elements[i];
                if (elm.type == 'checkbox' && elm.name.substring(elm.name.length - 12, elm.name.length) == 'chk_process4') {
                    { elm.checked = checkVal }
                }
            }
        }
</script>
    <asp:Panel ID="tab1" runat="server">
     
     <h1> OWNER</h1>
        <table ><tr ><td  bgcolor="#003366"><B  >
                    <asp:Label ID="Label4"  EnableTheming="False" ForeColor="White"  
            runat="server" Text="VIEW FILE"></asp:Label>


                    </B></td>
        
        </tr><tr><td valign="top">Shared With owner(s) of&nbsp;&nbsp;&nbsp;
     <asp:DropDownList ID="ddl_home_owner_list" runat="server"  AutoPostBack="True"
                                   DataTextField="home_name" DataValueField="home_id" 
                       onselectedindexchanged="ddl_home_owner_list_SelectedIndexChanged" ></asp:DropDownList></td>
           
        </tr></table><br />
                       <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
                         BorderWidth="1px"
                        DataKeyNames="ownerfiles_id" OnRowCommand="GridView2_RowCommand"
                        Width="83%"
                        OnRowDataBound="GridView2_RowDataBound">
                        <AlternatingRowStyle BackColor="White" /><Columns><asp:BoundField DataField="home_name" HeaderText="Property" ReadOnly="True" /><asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_file_name %>"><EditItemTemplate><asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FileName") %>'></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1"
                                        Display="Dynamic" ErrorMessage="Please enter file name" Font-Bold="True"></asp:RequiredFieldValidator></EditItemTemplate><ItemTemplate><asp:Label ID="Label1" runat="server" Text='<%# Eval("FileName") %>'></asp:Label></ItemTemplate></asp:TemplateField><asp:BoundField DataField="DateCreated" 
                                HeaderText="<%$ Resources:Resource, lbl_created_on %>" ReadOnly="True" 
                                DataFormatString="{0:MMM dd , yyyy}"  HtmlEncode="False"/>
                                <asp:ButtonField ButtonType="Image" ImageUrl="Earth-Download-24x24.png" CommandName="Download" Text="<%$ Resources:Resource, lbl_download %>" />
                               
                           </Columns><FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" /><PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" /><RowStyle BackColor="#E3EAEB" /><SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" /></asp:GridView><br /><br />
        <asp:Label ID="Label15" runat="server" ></asp:Label><br />
        <asp:Label ID="Label19" runat="server" ></asp:Label>
        
      
        
     </asp:Panel>
   
     
     <asp:Panel ID="tab2" runat="server">
     
     <h1>TENANTS</h1>
        
            
                <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" 
                    DataKeyNames="tenantfiles_id" 
                    OnRowCommand="GridView3_RowCommand" 
                    OnRowDataBound="GridView3_RowDataBound"
                    Width="83%">
                    <Columns>
                        <asp:BoundField DataField="home_name" HeaderText="Property" ReadOnly="True" />
                        <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_file_name %>">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FileName") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="TextBox1" Display="Dynamic" 
                                    ErrorMessage="Please enter file name" Font-Bold="True">
                         </asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("FileName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DateCreated" DataFormatString="{0:MMMM dd , yyyy}" 
                            HeaderText="<%$ Resources:Resource, lbl_created_on %>" HtmlEncode="False" 
                            ReadOnly="True" />
                        <asp:ButtonField ButtonType="Image" CommandName="Download" 
                            ImageUrl="Earth-Download-24x24.png" 
                            Text="<%$ Resources:Resource, lbl_download %>" />
                       
                    </Columns>
                </asp:GridView>
                <br />
                <br />
                
               
                <br />
                <br />
                <asp:Label ID="Label13" runat="server"></asp:Label>
                <br />
                <asp:Label ID="Label14" runat="server"></asp:Label>
            
        
</asp:Panel>
     
    <asp:Panel ID="tab3" runat="server">
    <h1>PROPERTY MANAGER</h1> 
            
        
         <table>
                    <tr>
                        <td bgcolor="#003366">
                            <b>
                            <asp:Label ID="Label8" runat="server" EnableTheming="False" ForeColor="White" 
                                Text="VIEW FILE"></asp:Label>
                    </b></td>
                        
                    </tr>
                    <tr>
                        <td valign="top">
                            Shared With property manager(s) of &nbsp;&nbsp;&nbsp;
                            <asp:DropDownList ID="ddl_home_pm_list" runat="server" AutoPostBack="True" 
                                DataTextField="home_name" DataValueField="home_id" 
                                OnSelectedIndexChanged="ddl_home_pm_list_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        
                    </tr>
                </table>
                <br />
                <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="False" 
                    DataKeyNames="pmfiles_id" 
                    OnRowCommand="GridView4_RowCommand"
                    OnRowDataBound="GridView4_RowDataBound" 
                    Width="83%">
                    <Columns>
                        <asp:BoundField DataField="home_name" HeaderText="Property" ReadOnly="True" />
                        <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_file_name %>">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FileName") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="TextBox1" Display="Dynamic" 
                                    ErrorMessage="Please enter file name" Font-Bold="True">
                         </asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("FileName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DateCreated" DataFormatString="{0:MMM dd , yyyy}" 
                            HeaderText="<%$ Resources:Resource, lbl_created_on %>" HtmlEncode="False" 
                            ReadOnly="True" />
                        <asp:ButtonField ButtonType="Image" CommandName="Download" 
                            ImageUrl="Earth-Download-24x24.png" 
                            Text="<%$ Resources:Resource, lbl_download %>" />
                        
                    </Columns>
                </asp:GridView>
                <br />
               
                <br />
                <asp:Label ID="Label20" runat="server"></asp:Label>
                <br />
                <asp:Label ID="Label21" runat="server"></asp:Label>
    </asp:Panel>
             
<asp:Panel ID="tab4" runat="server">
    
    <h1>JANITOR</h1>
              
         <table>
                    <tr>
                        <td bgcolor="#003366">
                            <b>
                            
                        <asp:Label ID="Label22" runat="server" EnableTheming="False" ForeColor="White" 
                                Text="VIEW FILE"></asp:Label>
                            
                    </b>
                        </td>
                      
                    </tr>
                    <tr>
                        <td valign="top">
                            Shared With property janitor(s) of &nbsp;&nbsp;&nbsp;
                            <asp:DropDownList ID="ddl_home_janitor_list" runat="server" AutoPostBack="True" 
                                DataTextField="home_name" DataValueField="home_id" 
                                onselectedindexchanged="ddl_home_janitor_list_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        
                    </tr>
                </table>
                <br />
                <asp:GridView ID="GridView5" runat="server" AutoGenerateColumns="False" 
                    DataKeyNames="janitorfiles_id" 
                    OnRowCommand="GridView5_RowCommand"
                    OnRowDataBound="GridView5_RowDataBound" 
                    Width="83%">
                    <Columns>
                        <asp:BoundField DataField="home_name" HeaderText="Property" ReadOnly="True" />
                        <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_file_name %>">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FileName") %>'></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="TextBox1" Display="Dynamic" 
                                    ErrorMessage="Please enter file name" Font-Bold="True">
                         </asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("FileName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DateCreated" DataFormatString="{0:MMM dd , yyyy}" 
                            HeaderText="<%$ Resources:Resource, lbl_created_on %>" HtmlEncode="False" 
                            ReadOnly="True" />
                        <asp:ButtonField ButtonType="Image" CommandName="Download" 
                            ImageUrl="Earth-Download-24x24.png" 
                            Text="<%$ Resources:Resource, lbl_download %>" />
                       
                    </Columns>
                </asp:GridView>
    </asp:Panel>
                <br />
                <br />
                
                <br />
                <asp:Label ID="Label25" runat="server"></asp:Label>
                <br />
                <asp:Label ID="Label26" runat="server"></asp:Label>
            
        
        
        
        
         &nbsp;

    
</asp:Content>