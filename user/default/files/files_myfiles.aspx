﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/mp_default.master" AutoEventWireup="true" CodeFile="files_myfiles.aspx.cs" Inherits="user_default_files_files_myfiles" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">

 div.row {float: left; margin:  0px 0px 8px 0px; padding: 0; width: 98%;}
 div.col500px {float: left; width: 500px; margin: 0 3px 0 0; padding: 0;}
 

</style>

 <script language="javascript" type="text/javascript">



        function fnClickUpdate(sender, e) {

            var tbx1 = document.getElementById('<%= TextBox1.ClientID %>').value;


            // alert(tbx1);
            if (tbx1.length > 0) {
                __doPostBack(sender, e);
            }


        }


        function ConfirmDelete() {
            if (confirm('Do you wish to delete this file/folder?'))
                return true;
            else
                return false;
        }

 
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
<div id="top_row" class="row">
<div class="col500px">     
 <div class="myform">
        <h1>My Files</h1>
        <hr/>
   </div>
  </div>
</div>
    
 <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server"
          TargetControlID="LinkButton1"
          PopupControlID="Panel1"
          BackgroundCssClass="modalBackground" 
          DropShadow="True" 
          OkControlID="Button1" 
          CancelControlID="btn_cancel" DynamicServicePath="" Enabled="True" >
         </cc1:ModalPopupExtender>
         
         
     <asp:Panel ID="Panel1"  runat="server" style="display:none">
           
                <table >
                <tr>
                <td bgcolor="aliceblue"><br /><asp:Label   ID="Label1"  runat="server" Font-Bold="True" 
                                    Text="<%$ Resources:Resource, lbl_new_folder %>"></asp:Label>
                                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                <asp:Button ID="Button1" runat="server" Font-Bold="True"  
                        Height="16px" OnClick="Button1_Click" Text="OK" ValidationGroup="createfolder" 
                        Width="29px" />
                                &nbsp;&nbsp;
                    <asp:Button ID="btn_cancel" runat="server" Text="Cancel" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="TextBox1" Display="Dynamic" 
                                    ErrorMessage="<%$ Resources:Resource, lbl_enter_folder_name %>" 
                                    Font-Bold="True" ValidationGroup="createfolder"></asp:RequiredFieldValidator>
                                <br />
                                <asp:CheckBox ID="CheckBox1" runat="server" Font-Bold="True" Font-Size="Small" 
                                    Text="<%$ Resources:Resource, lbl_create_at_root_level %>" />
                          </td>
                          </tr>
                          </table>
                      </asp:Panel>
                      
                      
                 <table width="35%"> 
                        <tr>  
                <td  bgcolor="#003366" valign="top">
                    &nbsp;<asp:Label EnableTheming="False" ForeColor="White"  ID="Label2" 
                        runat="server" Font-Bold="True" 
                                    Text="<%$ Resources:Resource, lbl_rename_folder %>"></asp:Label>
                                :<asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                <asp:Button ID="Button2" runat="server" Font-Bold="True" 
                                    OnClick="Button2_Click" Text="Ok" ValidationGroup="renamefolder" />
                                &nbsp;&nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                    ControlToValidate="TextBox2" Display="Dynamic" 
                                    ErrorMessage="<%$ Resources:Resource, lbl_folder_new_name %>" Font-Bold="True" 
                                    ValidationGroup="renamefolder"></asp:RequiredFieldValidator>
                         
                </td>
                
                </tr></table>
                
                 <table> <tr>
                  <td><asp:Label ID="Label7" runat="server" BackColor="Yellow" EnableViewState="False"
                ForeColor="Red" Font-Bold="True" ></asp:Label></td>
                  <td></td>
                  </tr>
                </table>
           
             
        
        
			<table width="95%">
			<tr>
			<td width="33%" bgcolor="#003366">
			
			<table width="100%">
			<tr>
			<td><b>
                <asp:Label EnableTheming="False" ForeColor="White"  ID="Label3" runat="server" 
                    Text="FOLDER :"></asp:Label></b></td>
			<td>:&nbsp;<asp:Label ID="Label10"  EnableTheming="False" ForeColor="White"  
                    runat="server" Font-Bold="True"></asp:Label></td>
			<td>
                <asp:Button ID="Button3" runat="server" Font-Bold="True" 
                    OnClick="Button3_Click" OnClientClick="return ConfirmDelete();" 
                    Text="<%$ Resources:Resource, lbl_delete %>"></asp:Button>
                </td>
			
			</tr>
			
			
			
			
			    <tr>
                    <td>
                        <asp:LinkButton EnableTheming="False" ForeColor="White" ID="LinkButton1" 
                            runat="server">New</asp:LinkButton>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
			
			
			
			
			</table>
			
			
			
              
                
             
               

                
                
                </td>
			<td width="67%" bgcolor="#003366" ></td>
			</tr>
			
			<tr>
			
			<td width="33%"  valign="top">			
				 
				 <asp:TreeView ID="TreeView1" runat="server"   
                             Font-Bold="True"  
                             OnSelectedNodeChanged="TreeView1_SelectedNodeChanged"  
                             ForeColor="White" ShowLines="True">
                             <ParentNodeStyle ImageUrl="~/manager/filemanager/Folder.gif" Font-Bold="False" />
        <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
        <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" 
            HorizontalPadding="0px" VerticalPadding="0px" />
                  <NodeStyle ChildNodesPadding="5px" ImageUrl="~/manager/filemanager/Folder_o.gif" 
                             HorizontalPadding="3px" />
                </asp:TreeView>
        
					
			</td>
			
			
			
			<td width="67%" valign="top">
			
			 
				  <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                        DataKeyNames="Id" OnRowCommand="GridView1_RowCommand"
                        OnRowEditing="GridView1_RowEditing"   Width="100%"
                        OnRowCancelingEdit="GridView1_RowCancelingEdit" 
                        OnRowUpdating="GridView1_RowUpdating" OnRowDataBound="GridView1_RowDataBound">
                        <Columns><asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_file_name %>">
                         <EditItemTemplate><asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FileName") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                                                ControlToValidate="TextBox1"
                                                                Display="Dynamic" 
                                                                ErrorMessage="Please enter file name">
                         </asp:RequiredFieldValidator></EditItemTemplate><ItemTemplate><asp:Label ID="Label1" runat="server" Text='<%# Eval("FileName") %>'></asp:Label></ItemTemplate></asp:TemplateField><asp:BoundField DataField="FileSize" HeaderText="<%$ Resources:Resource, lbl_size_bytes %>" ReadOnly="True" /><asp:BoundField DataField="DateCreated" 
                        HeaderText="<%$ Resources:Resource, lbl_created_on %>" ReadOnly="True" 
                        DataFormatString="{0:MMM dd , yyyy}"  HtmlEncode="False"/>
                        <asp:ButtonField ButtonType="Image" ImageUrl="Earth-Download-24x24.png"  CommandName="Download" Text="<%$ Resources:Resource, lbl_download %>" />
                        <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                        <asp:HiddenField Visible="false" ID="h_folder_id" runat="server" Value='<%# Bind("FolderId") %>' /><asp:HiddenField Visible="false" ID="h_file_id" runat="server" Value='<%# Bind("Id") %>'  />
                        <asp:ImageButton ImageUrl="Symbols-Delete-16x16.png" ID="Button1" runat="server" CausesValidation="False" CommandArgument='<%# Eval("Id") %>'
                        CommandName="DeleteFile" OnClick="Button1_Click1" OnClientClick="return ConfirmDelete();"
                                        Text="<%$ Resources:Resource, lbl_delete %>" /></ItemTemplate></asp:TemplateField>
                  <asp:CommandField  ButtonType="Button"  ShowEditButton="True"
                                    EditText="<%$ Resources:Resource, lbl_rename %>" 
                                    UpdateText="<%$ Resources:Resource, lbl_change %>" >
                                    <ControlStyle BackColor="#003366" ForeColor="White" />
                            </asp:CommandField>
                                    </Columns><FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" /><PagerStyle  BackColor="#666666" ForeColor="White" HorizontalAlign="Center" /><SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
               </asp:GridView>
             
               <br />
               
              
               
    
		
			
			
			</td>
			
			
			</tr>
			
			<tr>
			<td width="33%" bgcolor="#003366"><b></b></td>
			<td width="67%" bgcolor="#003366" >
			 <table>
			  <tr>
			    <td align="right"><asp:Label EnableTheming="False" ForeColor="White"  ID="Label9" 
                        runat="server" Font-Bold="True"  
                        Text="<%$ Resources:Resource, lbl_upload_file %>"></asp:Label></td><td align="left">
                  <asp:FileUpload ID="FileUpload1" runat="server" />
                  <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                      ControlToValidate="FileUpload1" Display="Dynamic" 
                      ErrorMessage="<%$ Resources:Resource, lbl_file_to_upload %>" Font-Bold="True" 
                      ValidationGroup="uploadfile"></asp:RequiredFieldValidator>
                  </td>
                  <td align="left">
                      <asp:Button ID="Button4" runat="server" Font-Bold="True" 
                          OnClick="Button4_Click" Text="<%$ Resources:Resource, lbl_upload %>" 
                          ValidationGroup="uploadfile" />
                  </td>
               </tr>
			  </table>
			  
			</td>
			</tr>
			</table>
	
    
</asp:Content>