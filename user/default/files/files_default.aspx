﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/mp_default.master" AutoEventWireup="true" CodeFile="files_default.aspx.cs" Inherits="user_default_files_files_default" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table>


<tr><td> <asp:HyperLink ID="HyperLink2" NavigateUrl="~/user/default/files/files_myfiles.aspx" runat="server"><h2>  <asp:Literal ID="Literal2" Text="My files" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to manage your files.</td></tr>


<tr><td> <asp:HyperLink ID="HyperLink6" NavigateUrl="~/user/default/files/user_files.aspx" runat="server"><h2>  <asp:Literal ID="Literal6" Text="Shared files" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to view uploaded files for the the different groups of users.</td></tr>


<tr><td> <asp:HyperLink ID="HyperLink1" NavigateUrl="~/user/default/files/files_upload.aspx" runat="server"><h2>  <asp:Literal ID="Literal1" Text="Upload a file" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to upload a file</td></tr>

       
  </table>

</asp:Content>