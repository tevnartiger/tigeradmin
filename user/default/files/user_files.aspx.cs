using System;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Briefcase;
using System.IO;

// Done by : Stanley Jocelyn
// Date : sept 12 , 2008
// Briefcase
public partial class user_files : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

           
            if (Session["is_owner"].ToString() == "0")
                tab1.Visible = false;
            if (Session["is_tenant"].ToString() == "0")
            {
                tab2.Visible = false;
            }

            if (Session["is_pm"].ToString() == "0")
            {
                tab3.Visible = false;
            }
            if (Session["is_janitor"].ToString() == "0")
                tab4.Visible = false;
            


                tiger.Owner o = new tiger.Owner(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
           
             
                ddl_home_owner_list.DataSource = o.getOwnerHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                ddl_home_owner_list.DataBind();
             
                tiger.Name n = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ddl_home_pm_list.DataSource = n.getPropertyForNameRole(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), 6);
                ddl_home_pm_list.DataBind();
               
                ddl_home_janitor_list.DataSource = n.getPropertyForNameRole(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), 5);
                ddl_home_janitor_list.DataBind();
               

                TenantBindGrid();
                OwnerBindGrid();
                PMBindGrid();
                JanitorBindGrid();

              
        }
    }

  
    private void OwnerBindGrid()
    {
        if (ddl_home_owner_list.Items.Count > 0)
        {
            DataTable files = Files.GetFilesForOwner(int.Parse(ddl_home_owner_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
            GridView2.DataSource = files;
            GridView2.DataBind();
        }
    }


    private void TenantBindGrid()
    {

        DataTable files = Files.GetTenantFileList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
        GridView3.DataSource = files;
        GridView3.DataBind();
    }


    private void PMBindGrid()
    {
        if (ddl_home_pm_list.Items.Count > 0)
        {
            DataTable files = Files.GetFilesForPM(int.Parse(ddl_home_pm_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
            GridView4.DataSource = files;
            GridView4.DataBind();
        }
    }

    private void JanitorBindGrid()
    {
        if (ddl_home_janitor_list.Items.Count > 0)
        {
            DataTable files = Files.GetFilesForJanitor(int.Parse(ddl_home_janitor_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
            GridView5.DataSource = files;
            GridView5.DataBind();
        }
    }
    
  

  
    protected void ddl_home_owner_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable files = Files.GetFilesForOwner(int.Parse(ddl_home_owner_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
        GridView2.DataSource = files;
        GridView2.DataBind();
       // TabContainer1.ActiveTabIndex = 0;

    }

    protected void ddl_home_pm_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable files = Files.GetFilesForPM(int.Parse(ddl_home_pm_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
        GridView4.DataSource = files;
        GridView4.DataBind();
       // TabContainer1.ActiveTabIndex = 2;

    }


    protected void ddl_home_janitor_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable files = Files.GetFilesForJanitor(int.Parse(ddl_home_janitor_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
        GridView5.DataSource = files;
        GridView5.DataBind();
        //TabContainer1.ActiveTabIndex = 3;

    }



    protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int fileid = int.Parse(GridView2.DataKeys[int.Parse(e.CommandArgument.ToString())].Value.ToString());

        if (e.CommandName == "Download")
        {
            DataTable file = Files.OwnerGetFile(fileid, Convert.ToInt32(Session["schema_id"]));
            byte[] filedata = (byte[])file.Rows[0]["filedata"];
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment;filename=" + file.Rows[0]["filename"]);
            Response.BinaryWrite(filedata);
            Response.End();
        }

       // TabContainer1.ActiveTabIndex = 0;
    }
  
   

    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }

 
    protected void GridView3_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int fileid = int.Parse(GridView3.DataKeys[int.Parse(e.CommandArgument.ToString())].Value.ToString());

        if (e.CommandName == "Download")
        {
            DataTable file = Files.TenantGetFile(fileid, Convert.ToInt32(Session["schema_id"]));
            byte[] filedata = (byte[])file.Rows[0]["filedata"];
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment;filename=" + file.Rows[0]["filename"]);
            Response.BinaryWrite(filedata);
            Response.End();
        }
    }
   
    protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }


    protected void GridView4_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int fileid = int.Parse(GridView4.DataKeys[int.Parse(e.CommandArgument.ToString())].Value.ToString());

        if (e.CommandName == "Download")
        {
            DataTable file = Files.PMGetFile(fileid, Convert.ToInt32(Session["schema_id"]));
            byte[] filedata = (byte[])file.Rows[0]["filedata"];
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment;filename=" + file.Rows[0]["filename"]);
            Response.BinaryWrite(filedata);
            Response.End();
        }
    }
   
    protected void GridView4_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }



    protected void GridView5_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int fileid = int.Parse(GridView5.DataKeys[int.Parse(e.CommandArgument.ToString())].Value.ToString());

        if (e.CommandName == "Download")
        {
            DataTable file = Files.JanitorGetFile(fileid, Convert.ToInt32(Session["schema_id"]));
            byte[] filedata = (byte[])file.Rows[0]["filedata"];
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment;filename=" + file.Rows[0]["filename"]);
            Response.BinaryWrite(filedata);
            Response.End();
        }
    }
   

    protected void GridView5_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }


}
