using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.NameObjectAuthorization;
/// <summary>
/// done by : Stanley Jocelyn
/// date    : sept 10 , 2007
/// </summary>

public partial class appliance_appliance_view : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!RegEx.IsInteger(Request.QueryString["appliance_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

      string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

       /////////////
       /////////////// SECURITY CHECK BEGIN   //////////////
      ////////
      NameObjectAuthorization applianceAuthorization = new NameObjectAuthorization(strconn);
      if (!applianceAuthorization.Appliance(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["appliance_id"]), Convert.ToInt32(Session["name_id"]), 4))
      {
          Session.Abandon();
          Response.Redirect("~/login.aspx");
      }
      ////////
       /////////////
       /////////////// SECURITY CHECK END  //////////////
       //////////////


           if (!Page.IsPostBack)
           {
               appliance_add_link.NavigateUrl = "appliance_add.aspx?appliance_id=" + Request.QueryString["appliance_id"];
               appliance_update_link.NavigateUrl = "appliance_update.aspx?appliance_id=" + Request.QueryString["appliance_id"];
               appliance_list_link.NavigateUrl = "appliance_list.aspx";

              

               //     if (!(Page.IsPostBack))
               //   {

               SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
               SqlCommand cmd = new SqlCommand("prApplianceView2", conn);
               SqlCommand cmd2 = new SqlCommand("prAppliancePreviousStorageList", conn);


               cmd.CommandType = CommandType.StoredProcedure;

               //Add the params
               cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
               cmd.Parameters.Add("@appliance_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["appliance_id"]);
               try
               {
                   conn.Open();


                   SqlDataAdapter da = new SqlDataAdapter(cmd);
                   DataSet ds = new DataSet();
                   da.Fill(ds);


                   rApplianceInvoiceImage.DataSource = ds;
                   rApplianceInvoiceImage.DataBind();

                   if (rApplianceInvoiceImage.Items.Count > 0)
                   {
                       HyperLink link = (HyperLink)rApplianceInvoiceImage.Items[0].FindControl("link_appliance_invoice_image");
                       link.NavigateUrl = "appliance_invoice_image.aspx?appliance_id=" + Request.QueryString["appliance_id"]; 
                      
                   }


                  
                   rApplianceImage.DataSource = ds;
                   rApplianceImage.DataBind();


                   if (rApplianceImage.Items.Count > 0)
                   {
                       HyperLink link = (HyperLink)rApplianceImage.Items[0].FindControl("link_appliance_image");
                       link.NavigateUrl = "appliance_image.aspx?appliance_id=" + Request.QueryString["appliance_id"];

                   }


                   SqlDataReader dr = null;
                   dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                 
                   while (dr.Read() == true)
                   {
                       supplier_name.Text = dr["company_name"].ToString();
                       appliance_categ.Text = dr["ac_name_en"].ToString();
                       appliance_name.Text = dr["appliance_name"].ToString();
                       appliance_invoice_no.Text = dr["appliance_invoice_no"].ToString();


                       if (dr["ua_dateadd"] != System.DBNull.Value)
                       {
                           DateTime dateadd = new DateTime();
                           dateadd = Convert.ToDateTime(dr["ua_dateadd"]);

                           ua_dateadd.Text = dateadd.Month.ToString() + "/" + dateadd.Day.ToString() + "/" + dateadd.Year.ToString();

                       }

                       //if the appliance is not in a property do not dispaly the property info. row
                       // or else do not display the warehouse info. row
                       if (dr["home_name"] == DBNull.Value)
                           tr_property_row.Visible = false;
                       else
                           tr_warehouse_row.Visible = false;

                       home_name.Text = dr["home_name"].ToString();

                       // if the appliance is not in the property but not in a rented unit
                       // display "Storage Unit" as the unit
                       if (dr["unit_door_no"] == DBNull.Value)
                           unit_door_no.Text = Resources.Resource.lbl_storage_unit;
                       else
                           unit_door_no.Text = dr["unit_door_no"].ToString();



                       if (dr["warehouse_name"] != DBNull.Value)
                       {
                           tr_property_row.Visible = false;
                           tr_unit_row.Visible = false;
                           lbl_warehouse.Text = dr["warehouse_name"].ToString();
                       }









                       appliance_purchase_cost.Text = dr["appliance_purchase_cost"].ToString();
                       appliance_desc.Text = dr["appliance_desc"].ToString();
                       appliance_serial_no.Text = dr["appliance_serial_no"].ToString();

                       DateTime date_purchase = new DateTime();
                       date_purchase = Convert.ToDateTime(dr["appliance_date_purchase"]);
                       appliance_date_purchase.Text = date_purchase.Month.ToString() + "/" + date_purchase.Day.ToString() + "/" + date_purchase.Year.ToString();



                       appliance_guarantee_year.Text = dr["appliance_guarantee_year"].ToString();

                       




                   }





               }
               finally
               {
                   //conn.Close();
               }


               //-------------------------------------------------------------------------------------------------

               cmd2.CommandType = CommandType.StoredProcedure;


               try
               {


                   cmd2.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                   cmd2.Parameters.Add("appliance_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["appliance_id"]);
                   SqlDataAdapter da = new SqlDataAdapter(cmd2);
                   DataTable dt = new DataTable();
                   da.Fill(dt);

                   gv_appliance_previous_storage.DataSource = dt;
                   gv_appliance_previous_storage.DataBind();

               }
               finally
               {
                   conn.Close();
               }




               // IF THERE'S NO VALUE FROM PREVIOUS STORAGE (gv_appliance_previous_storage) , RENDER THE TABLE
               // CONTAINING THE GRIDVIEW NOT VISIBLE
               if (gv_appliance_previous_storage.Rows.Count == 0)
               {
                   tb_appliance_pervious_storage.Visible = false;
                   td_appliance_pervious_storage.BgColor = "White";
                   btn_revert.Visible = false;
               }

           }
      

    }
    protected void btn_revert_Click(object sender, EventArgs e)
    {


        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

 

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prApplianceCancelMoving", conn);
        cmd.CommandType = CommandType.StoredProcedure;

       //try
       {

        conn.Open();
        //Add the params
        cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@appliance_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["appliance_id"]);
        cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
        cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar,15).Value = Request.UserHostAddress.ToString();
       
        //execute the insert
        cmd.ExecuteReader();
           //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

           // if (Convert.ToInt32(cmd.Parameters["@return"].Value) > 0)
           //    result.InnerHtml = " add successful";


       }
      // catch (Exception error)
       {

       }



       Response.Redirect("appliance_view.aspx?appliance_id=" + Request.QueryString["appliance_id"]);
    }
}
