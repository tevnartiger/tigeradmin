<%@ Page Language="C#" MasterPageFile="~/user/mp_owner.master" AutoEventWireup="true" CodeFile="appliance_view.aspx.cs" Inherits="appliance_appliance_view" Title="VIEW APPLIANCE & FURNITURE" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


        <strong>
        <asp:HyperLink ID="appliance_add_link" 
            runat="server"><asp:Label ID="Label2" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_add %>"  /></asp:HyperLink>-&nbsp; <asp:HyperLink ID="appliance_update_link"
            runat="server"><asp:Label ID="Label3" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_update %>"  /></asp:HyperLink>- <asp:HyperLink ID="appliance_list_link" 
            runat="server"><asp:Label ID="Label19" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_appliance_list %>"  /></asp:HyperLink>- delete
        &nbsp;<br />
        <br />
        
        </strong>
        <br />
        
       <asp:Button ID="btn_revert" runat="server" onclick="btn_revert_Click" 
            Text="<%$ Resources:Resource, btn_cancel_moving %>" />
       
                      <br />
                     
              
        
                     
              
        <br />
        <table style="width: 100%">
            <tr>
                <td valign="top" style="width: 486px">


     
        
       
       
        <table width="100%"  >
        <tr>
                <td style="width: 198px">
                    &nbsp;<b><asp:Label ID="Label11" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_supplier %>"  />  </b> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</td><td style="width: 287px">
                    : <asp:Label ID="supplier_name"   runat="server">
                    </asp:Label></td></tr><tr id="tr_property_row" runat="server">
                <td style="width: 198px; font-weight: 700;">
                    &nbsp;<asp:Label ID="Label18" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_property %>"  /></td>
                <td style="width: 287px">
                    : <asp:Label ID="home_name" runat="server" ></asp:Label></td></tr><tr id="tr_unit_row" runat="server">
                <td style="width: 198px">
                    &nbsp;<b><asp:Label ID="Label4" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_unit %>"  /></b></td>
                <td style="width: 287px">
                    : <asp:Label ID="unit_door_no" runat="server" ></asp:Label></td></tr><tr id="tr_warehouse_row" runat="server">
                <td style="width: 198px">
                    &nbsp;<b><asp:Label ID="Label5" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_external_storage %>"  /></b></td>
                <td style="width: 287px">
                    &nbsp; <asp:Label ID="lbl_warehouse" runat="server"  ></asp:Label></td></tr><tr>
                <td style="width: 198px">
                    &nbsp;<asp:Label ID="Label6" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_date_add %>"  /></td>
                <td style="width: 287px">
                    : <asp:Label ID="ua_dateadd" runat="server" ></asp:Label></td></tr></table></td><td id="td_appliance_pervious_storage" runat="server" valign="top">

                    <table width="100%" id="tb_appliance_pervious_storage" runat="server">
                    <tr>
                    
                    <td width="100%"  runat="server">
            <asp:GridView ID="gv_appliance_previous_storage" runat="server" 
            AlternatingRowStyle-BackColor="Beige" 
                AutoGenerateColumns="false" BorderColor="White" BorderWidth="3" 
                EmptyDataText='<%$ Resources:Resource, lbl_no_data %>' GridLines="Both" HeaderStyle-BackColor="AliceBlue" 
                 Width="100%" >
                <Columns>
                  <asp:BoundField DataField="storage_name" HeaderText="Previously Stored"/>  
               
                 <asp:BoundField DataField="unit_door_no" HeaderText='<%$ Resources:Resource, lbl_unit %>' />
                <asp:BoundField DataField="ua_dateadd" HeaderText="Add" DataFormatString="{0:M-dd-yyyy}"  
                   HtmlEncode="false" />
               <asp:BoundField DataField="ua_date_remove" HeaderText="Removed" DataFormatString="{0:M-dd-yyyy}"  
                     HtmlEncode="false"/>
                    
                        
                </Columns>
            </asp:GridView>
                    
                    
                    </td>
                    </tr>
                    
                    </table>

                    
                    
                              </td>
                          </tr>
                      </table>
                     
              
        <asp:Label ID="Label1" runat="server" Text=""></asp:Label><br />
       
        <table  style="width: 504px">
        <tr>
        <td style="width: 1100px">&nbsp; &nbsp;<asp:Label ID="Label7" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_category %>"  /></td>
        <td style="width: 364px" valign="top">
            : <asp:Label ID="appliance_categ" runat="server">
        </asp:Label></td></tr><tr>
                <td style="width: 1100px">&nbsp; &nbsp;<asp:Label ID="Label8" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_appliance_name %>"  />
                </td>
                <td style="width: 364px"  >
                    : <asp:Label ID="appliance_name" runat="server"></asp:Label></td></tr><tr>
                <td style="width: 1100px" >&nbsp; &nbsp;<asp:Label ID="Label9" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_invoice_no %>"  />
                </td>
                <td style="width: 364px" >
                    : <asp:Label ID="appliance_invoice_no" runat="server"></asp:Label></td></tr><tr>
                <td valign="top" style="width: 1100px; "  >
                    &nbsp; &nbsp;<asp:Label ID="Label10" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_invoice_photo %>"  /></td>
                <td style="width: 364px;"  >
                    <asp:Repeater runat="server" ID="rApplianceInvoiceImage"><ItemTemplate>
                                       <asp:HyperLink ID="link_appliance_invoice_image"  runat="server"><asp:Image ID="appliance_invoice_photo" runat="server" Height="270" 
                                            ImageUrl='<%# "~/mediahandler/ApplianceInvoiceImage.ashx?app_id="+ Eval("appliance_id") %>' 
                                            Width="300" /></asp:HyperLink>
                                    
</ItemTemplate>
</asp:Repeater>

                            </td>
            </tr>
            <tr>
                <td style="width: 1100px"  >
                    &nbsp; &nbsp; <asp:Label ID="Label12" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_cost %>"  /></td>
                <td style="width: 364px"  >
                    : <asp:Label ID="appliance_purchase_cost" runat="server"></asp:Label></td></tr><tr>
                              <td 
                                  style="width: 1100px" valign="top"  >&nbsp; &nbsp; <asp:Label ID="Label13" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_description %>"  /></td>
                <td style="width: 364px"  >
                    <asp:Label ID="appliance_desc" runat="server"  Height="90px" Width="281px"></asp:Label></td></tr><tr>
                <td style="width: 1100px"  >
                    &nbsp; &nbsp;<asp:Label ID="Label14" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_serial_no %>"  /></td>
                <td style="width: 364px"  >
                    : <asp:Label ID="appliance_serial_no" runat="server"></asp:Label></td></tr><tr>
                <td style="width: 1100px"  >
                    &nbsp; &nbsp;<asp:Label ID="Label15" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_purchase_date %>"  /></td>
                <td style="width: 364px"  >
                    : <asp:Label ID="appliance_date_purchase" runat="server" Width="66px"></asp:Label></tr><tr>
                <td style="width: 1100px"  >
                    &nbsp; &nbsp; <asp:Label ID="Label16" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_garanty_year %>"  /></td>
                <td style="width: 364px"  >
                    : <asp:Label ID="appliance_guarantee_year" runat="server">
                        </asp:Label></td></tr><tr>
                <td valign=top style="width: 1100px" >
                    &nbsp; &nbsp;<asp:Label ID="Label17" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_appliance_picture %>"  /></td>
                <td style="width: 364px"  >
                   <asp:Repeater runat="server" ID="rApplianceImage">
                       <ItemTemplate>
                       <asp:HyperLink ID="link_appliance_image" runat="server">  <asp:Image ID="appliance_photo" 
                                            runat="server" Height="270" 
                                            ImageUrl='<%# "~/mediahandler/ApplianceImage.ashx?app_id="+ Eval("appliance_id") %>' 
                                            Width="300" />
                           </asp:HyperLink> 
                           </ItemTemplate>
                          </asp:Repeater>
                    
                            </td>
            </tr>
        </table>
       
        
       
        <br />
        <br />
  </asp:Content>
