﻿<%@ Page Language="C#" MasterPageFile="~/user/mp_owner.master" AutoEventWireup="true" CodeFile="alerts.aspx.cs" Inherits="manager_alerts_alerts" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   <table id="tb_rent_delequency" runat="server" width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b><asp:Label ID="Label3" runat="server" 
                        Text="<%$ Resources:Resource, lbl_u_delequency%>"/> </b>
                </td>
            </tr>
        </table>
    
     <asp:GridView Width="83%" ID="gv_rent_delequency" runat="server" AutoGenerateColumns="false"
                   AllowPaging="true" AllowSorting="true"  AutoGenerateSelectButton="false">
    <Columns>
     <asp:BoundField DataField="home_name"  HeaderText="<%$ Resources:Resource, lbl_property%>"  />
     
      <asp:BoundField DataField="unit_door_no"  HeaderText="<%$ Resources:Resource, gv_unit%>"  />
      
      
      <asp:BoundField DataField="amount_owed"  HeaderText="<%$ Resources:Resource, gv_amount_owed%>"
             DataFormatString="{0:0.00}"   HtmlEncode="false"  />
    
     
      
      
      <asp:BoundField   DataField="rp_due_date" DataFormatString="{0:M-dd-yyyy}"  
       HtmlEncode="false" HeaderText="<%$ Resources:Resource, gv_due_date%>"  />
      <asp:BoundField  DataField="days" HeaderText="<%$ Resources:Resource, lbl_days %>"/> 
     
      <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_notice_sent%>"   >
      <ItemTemplate>
      <asp:Label  runat="server"  ID="notice_sent"    Text='<%#Get_AmountOfWarningSent(Convert.ToInt32(Eval("amount_of_warning_sent")))%>'    /> 
      </ItemTemplate>
      </asp:TemplateField>
      </Columns>
   </asp:GridView>
      
       <br />
       
<br />
    


    <br />
    

</asp:Content>

