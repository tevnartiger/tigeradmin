﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    :may 21, 2009
/// </summary>
public partial class user_owner_property_property_profile : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
       

        if (!Page.IsPostBack)
        {
            tiger.Owner g = new tiger.Owner(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_home.DataSource = g.getOwnerHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
            gv_home.DataBind();
                //gv_home.Rows[1].ToString();

        }
    }

   
    protected string getOwnerProfile(int home_id)
    {
        string str_view = "";
        //get owner
        SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        SqlCommand cmd = new SqlCommand("prProfileOwnerInfo", conn);
        cmd.CommandType = CommandType.StoredProcedure;


        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

        try
        {
            conn.Open();

            SqlDataReader dr = null;
            dr = cmd.ExecuteReader();

            while (dr.Read() == true)
            {
                str_view += "<a href='/manager/name/name_profile.aspx?name_id=" + Convert.ToString(dr["name_id"]) + "'>" + Convert.ToString(dr["owner"]) + "</a><br/>" + Convert.ToString(dr["owner_tel"]) + "<br/>" + Convert.ToString(dr["owner_email"]) + "<br/>";
                //get owner
            }

            return str_view;
        }
        finally
        {
            conn.Close();
        }

    }

    protected string getManagerProfile(int home_id)
    {
        string str_view = "";
        //get owner
        SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        SqlCommand cmd = new SqlCommand("prProfileManagerInfo", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

        try
        {
            conn.Open();

            SqlDataReader dr = null;
            dr = cmd.ExecuteReader();

            while (dr.Read() == true)
            {
                str_view += "<a href='/manager/name/name_profile.aspx?name_id=" + Convert.ToString(dr["name_id"]) + "'>" + Convert.ToString(dr["manager"]) + "</a><br/>" + Convert.ToString(dr["manager_tel"]) + "<br/>" + Convert.ToString(dr["manager_email"]) + "<br/>";
                //get owner
            }

            return str_view;
        }
        finally
        {
            conn.Close();
        }



    }

    protected string getJanitorProfile(int home_id)
    {
        //get Janitor
        string str_view = "";
        //get owner
        SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        SqlCommand cmd = new SqlCommand("prProfileJanitorInfo", conn);
        cmd.CommandType = CommandType.StoredProcedure;


        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

        try
        {
            conn.Open();

            SqlDataReader dr = null;
            dr = cmd.ExecuteReader();

            while (dr.Read() == true)
            {
                str_view += "<a href='/manager/name/name_profile.aspx?name_id=" + Convert.ToString(dr["name_id"]) + "'>" + Convert.ToString(dr["janitor"]) + "</a><br/>" + Convert.ToString(dr["janitor_tel"]) + "<br/>" + Convert.ToString(dr["janitor_email"]);
                //get owner
            }

            return str_view;
        }
        finally
        {
            conn.Close();
        }

    }

}
