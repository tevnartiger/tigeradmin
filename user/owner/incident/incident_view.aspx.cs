﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.NameObjectAuthorization;

/// <summary>
/// done by : Stanley Jocelyn
/// date    : march 5 , 2007
/// desc    : view a incident
/// </summary>

public partial class manager_incident_incident_view : BasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!RegEx.IsInteger(Request.QueryString["inc_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        /////////////
        /////////////// SECURITY CHECK BEGIN   //////////////
        //////////////
        ////////
        NameObjectAuthorization incidentAuthorization = new NameObjectAuthorization(strconn);
        if (!incidentAuthorization.Incident(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["inc_id"]), Convert.ToInt32(Session["name_id"]), 4))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ////////
        /////////////// SECURITY CHECK END  //////////////
        //////////////

        if (!Page.IsPostBack)
        {
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prIncidentView", conn);

            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@incident_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["inc_id"]);
            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                    lbl_incident_title.Text = dr["incident_title"].ToString();

                    DateTime incident_date = new DateTime();
                    incident_date = Convert.ToDateTime(dr["incident_date"]);
                    if (incident_date != null)
                    {
                        lbl_incident_date.Text = incident_date.ToLongDateString() + " , " + incident_date.ToShortTimeString();


                    }



                    lbl_home_name.Text = dr["home_name"].ToString();

                    lbl_unit_door_no.Text = dr["unit_door_no"].ToString();
                    
                    
                    lbl_incident_location.Text = dr["incident_location"].ToString();
                    lbl_incident_person_involved.Text = dr["incident_person_involved"].ToString();
                    lbl_incident_witnesses.Text = dr["incident_witnesses"].ToString();
                    lbl_incident_description.Text = dr["incident_description"].ToString();
                    lbl_incident_action_taken.Text = dr["incident_action_taken"].ToString();


                }

            }

            finally
            {
                conn.Close();
            }
              // construction of the gridview of the rent payment archive
               tiger.Incident v = new tiger.Incident(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
               gv_wo_list.DataSource = v.getIncidentWorkOrderList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["inc_id"]));
               gv_wo_list.DataBind();

        }

    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetPriority(int priority_id)
    {
        string priority = "";

        switch (priority_id)
        {
            case 1:
                priority = Resources.Resource.lbl_urgent;
                break;
            case 2:
                priority = Resources.Resource.lbl_high;
                break;
            case 3:
                priority = Resources.Resource.lbl_medium;
                break;

            case 4:
                priority = Resources.Resource.lbl_low;
                break;
        }

        return priority;
    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetStatus(int status_id)
    {
        string status = "";

        switch (status_id)
        {
            case 1:
                status = Resources.Resource.lbl_pending;
                break;
            case 2:
                status = Resources.Resource.lbl_in_progress;
                break;
            case 3:
                status = Resources.Resource.lbl_completed;
                break;

            case 4:
                status = Resources.Resource.lbl_closed;
                break;
        }

        return status;
    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_cancel_Click(object sender, EventArgs e)
    {

        //*********************************************************************
        //*********************************************************************
        Button btn_cancel = (Button)sender;
        GridViewRow grdRow = (GridViewRow)btn_cancel.Parent.Parent;
        string strField1 = grdRow.Cells[6].Text;

        HiddenField h_wo_id = (HiddenField)grdRow.Cells[6].FindControl("h_wo_id");
        HiddenField h_wo_title = (HiddenField)grdRow.Cells[6].FindControl("h_wo_title");
        HiddenField h_wo_date_begin = (HiddenField)grdRow.Cells[6].FindControl("h_wo_date_begin");
        HiddenField h_home_name = (HiddenField)grdRow.Cells[6].FindControl("h_home_name");

        //  lbl_confirmation.ForeColor = "Red";
        lbl_confirmation.Text = Resources.Resource.lbl_delete_confirmation;

        Label2.Text = Resources.Resource.lbl_property;
        Label4.Text = Resources.Resource.lbl_title;
        Label6.Text = Resources.Resource.lbl_date;

        Label3.Text = ":";
        Label5.Text = ":";
        Label7.Text = ":";

        lbl_title.Text = h_wo_title.Value;
        lbl_date.Text = h_wo_date_begin.Value;
        lbl_home_name.Text = h_home_name.Value;


        lbl_confirmation.Visible = true;
        lbl_home_name.Visible = true;

        lbl_title.Visible = true;
        lbl_date.Visible = true;

        Label2.Visible = true;
        Label3.Visible = true;
        Label4.Visible = true;
        Label5.Visible = true;
        Label6.Visible = true;
        Label7.Visible = true;



        //------------------------------------------------------------------------------------

        // check if the rp_id belong to the account
        NameObjectAuthorization rpAuthorization = new NameObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!rpAuthorization.WorkOrder(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(h_wo_id.Value), Convert.ToInt32(Session["name_id"]), 4))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prWorkOrderDelete", conn);
        cmd.CommandType = CommandType.StoredProcedure;


        conn.Open();
        //Add the params
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@wo_id", SqlDbType.Int).Value = Convert.ToInt32(h_wo_id.Value);

        //execute the insert
        cmd.ExecuteReader();

        conn.Close();

        //-------------------------------------------------------------------------------------

        // construction of the gridview of the rent payment archive
        tiger.Incident v = new tiger.Incident(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_wo_list.DataSource = v.getIncidentWorkOrderList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["inc_id"]));
        gv_wo_list.DataBind();

    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_wo_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // construction of the gridview of the rent payment archive
        tiger.Incident v = new tiger.Incident(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_wo_list.PageIndex = e.NewPageIndex;
        gv_wo_list.DataSource = v.getIncidentWorkOrderList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["inc_id"]));
        gv_wo_list.DataBind();
        
    }

   

}
