﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.NameObjectAuthorization;


/// <summary>
/// Done by : Stanley Jocelyn
/// date    : may 23 , 2009
/// </summary>
/// 
public partial class manager_incident_incident_list : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DateTime to = new DateTime();
            DateTime from = new DateTime();
            to = DateTime.Now; // the date in the to drop downlist


            DateTime today = DateTime.Today;
            DateTime lastDayOfThisMonth = new DateTime(today.Year, today.Month, 1).AddMonths(1).AddDays(-1);

            tiger.Date c = new tiger.Date();
            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime



            //if we are entering from the update page
            //-----------------------------------------------------
            if (Request.QueryString["h_id"] != "" && Request.QueryString["h_id"] != null && Request.QueryString["fm"] != "" && Request.QueryString["fm"] != null && Request.QueryString["fy"] != "" && Request.QueryString["fy"] != null)
            {

                NameObjectAuthorization homeAuthorization = new NameObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

                ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["h_id"]), Convert.ToInt32(Session["name_id"]), 5))
                {
                    Session.Abandon();
                    Response.Redirect("~/login.aspx");
                }
                ///////// SECURITY OBJECT CHECK  END ////////////////////////////////


                from = Convert.ToDateTime(c.DateCulture(Request.QueryString["fm"], Request.QueryString["fd"], Request.QueryString["fy"], Convert.ToString(Session["_lastCulture"])));

            }
            else
                // BY DEFAULT WE WANT TO SEE THE RENT THAT WAS PAID THIS MONTH , I.E: FROM THE 1 ST DAY OF MONTH TO TODAY
                from = Convert.ToDateTime(c.DateCulture("1", "1", to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));

            //-----------------------------------------------------
            //-----------------------------------------------------
            if (Request.QueryString["h_id"] != "" && Request.QueryString["h_id"] != null && Request.QueryString["tm"] != "" && Request.QueryString["tm"] != null && Request.QueryString["ty"] != "" && Request.QueryString["fy"] != null)
            {

                NameObjectAuthorization homeAuthorization = new NameObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

                ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["h_id"]), Convert.ToInt32(Session["name_id"]), 6))
                {
                    Session.Abandon();
                    Response.Redirect("~/login.aspx");
                }
                ///////// SECURITY OBJECT CHECK  END ////////////////////////////////


                to = Convert.ToDateTime(c.DateCulture(Request.QueryString["tm"], Request.QueryString["td"], Request.QueryString["ty"], Convert.ToString(Session["_lastCulture"])));

            }
            else
                // BY DEFAULT WE WANT TO SEE THE RENT THAT WAS PAID THIS MONTH , I.E: FROM THE 1 ST DAY OF MONTH TO TODAY
                to = Convert.ToDateTime(c.DateCulture(to.Month.ToString(), to.Day.ToString(), to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
            //-----------------------------------------------------


            ddl_from_m.SelectedValue = from.Month.ToString();
            ddl_from_d.SelectedValue = from.Day.ToString();
            ddl_from_y.SelectedValue = from.Year.ToString();

            ddl_to_m.SelectedValue = to.Month.ToString();
            ddl_to_d.SelectedValue = to.Day.ToString();
            ddl_to_y.SelectedValue = to.Year.ToString();

            // First we check if there's home available
            tiger.Owner h = new tiger.Owner(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = h.getOwnerHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

            if (home_count > 0)
            {
                int home_id = h.getOwnerHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                // link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                //Session["schema_id"]));

                ddl_home_id.Visible = true;

                ddl_home_id.DataSource = h.getOwnerHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                // ddl_home_id.SelectedValue = Convert.ToString(home_id);
                ddl_home_id.DataBind();
                ddl_home_id.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));

            }


        }

    }

    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {

        ddl_home_id.Visible = true;



        DateTime to = new DateTime();
        DateTime from = new DateTime();
        to = DateTime.Now; // the date in the to drop downlist

        tiger.Date d = new tiger.Date();
        // BY DEFAULT WE WANT TO SEE THE RENT THAT WAS PAID THIS MONTH , I.E: FROM THE 1 ST DAY OF MONTH TO TODAY
        to = Convert.ToDateTime(d.DateCulture(to.Month.ToString(), to.Day.ToString(), to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
        from = Convert.ToDateTime(d.DateCulture("1", "1", to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));

        gv_incident_list.DataSourceID = "ObjectDataSource1";

        ddl_from_m.SelectedValue = from.Month.ToString();
        ddl_from_d.SelectedValue = from.Day.ToString();
        ddl_from_y.SelectedValue = from.Year.ToString();

        ddl_to_m.SelectedValue = to.Month.ToString();
        ddl_to_d.SelectedValue = to.Day.ToString();
        ddl_to_y.SelectedValue = to.Year.ToString();


        lbl_confirmation.Visible = false;
        lbl_home_name.Visible = false;

        lbl_title.Visible = false;
        lbl_date.Visible = false;

        Label2.Visible = false;
        Label3.Visible = false;
        Label4.Visible = false;
        Label5.Visible = false;
        Label6.Visible = false;
        Label7.Visible = false;

    }



    protected void Button1_Click(object sender, EventArgs e)
    {

        DateTime to = new DateTime();
        DateTime from = new DateTime();
        to = DateTime.Now; // the date in the to drop downlist

        tiger.Date d = new tiger.Date();
        to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        gv_incident_list.DataSourceID = "ObjectDataSource1";

        lbl_confirmation.Visible = false;
        lbl_home_name.Visible = false;

        lbl_title.Visible = false;
        lbl_date.Visible = false;

        Label2.Visible = false;
        Label3.Visible = false;
        Label4.Visible = false;
        Label5.Visible = false;
        Label6.Visible = false;
        Label7.Visible = false;

    }








    protected void ObjectDataSource1_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {


        DateTime to = new DateTime();
        DateTime from = new DateTime();


        tiger.Date c = new tiger.Date();

        from = Convert.ToDateTime(c.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        to = Convert.ToDateTime(c.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        e.InputParameters["home_id"] = Convert.ToInt32(ddl_home_id.SelectedValue);
        e.InputParameters["date_from"] = from;
        e.InputParameters["date_to"] = to;



    }



   
}
