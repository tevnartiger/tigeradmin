﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.NameObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : may 23 , 2009
/// </summary>

public partial class manager_tenant_tenant_cancel_rent_payment : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DateTime to = new DateTime();
            DateTime from = new DateTime();
            to = DateTime.Now; // the date in the to drop downlist


            DateTime today = DateTime.Today;
            DateTime lastDayOfThisMonth = new DateTime(today.Year, today.Month, 1).AddMonths(1).AddDays(-1);

            tiger.Date c = new tiger.Date();
            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime

            

            //if we are entering from the update page
            //-----------------------------------------------------
            if (Request.QueryString["h_id"] != "" && Request.QueryString["h_id"] != null && Request.QueryString["fm"] != "" && Request.QueryString["fm"] != null && Request.QueryString["fy"] != "" && Request.QueryString["fy"] != null)
            {
                if (!RegEx.IsInteger(Request.QueryString["h_id"]) ||
                    !RegEx.IsInteger(Request.QueryString["fm"]) ||
                    !RegEx.IsInteger(Request.QueryString["fy"]) ||
                    !RegEx.IsInteger(Request.QueryString["fd"]))
                {
                    Session.Abandon();
                    Response.Redirect("~/login.aspx");
                }

                NameObjectAuthorization homeAuthorization = new NameObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

                ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["h_id"]), Convert.ToInt32(Session["name_id"]),4))
                {
                    Session.Abandon();
                    Response.Redirect("~/login.aspx");
                }
                ///////// SECURITY OBJECT CHECK  END ////////////////////////////////


                from = Convert.ToDateTime(c.DateCulture(Request.QueryString["fm"], Request.QueryString["fd"], Request.QueryString["fy"], Convert.ToString(Session["_lastCulture"])));

            }
            else
                // BY DEFAULT WE WANT TO SEE THE RENT THAT WAS PAID THIS MONTH , I.E: FROM THE 1 ST DAY OF MONTH TO TODAY
                from = Convert.ToDateTime(c.DateCulture(to.Month.ToString(), "1", to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));

             //-----------------------------------------------------
            //-----------------------------------------------------
            if (Request.QueryString["h_id"] != "" && Request.QueryString["h_id"] != null && Request.QueryString["tm"] != "" && Request.QueryString["tm"] != null && Request.QueryString["ty"] != "" && Request.QueryString["fy"] != null)
            {
                if (!RegEx.IsInteger(Request.QueryString["h_id"]) ||
                    !RegEx.IsInteger(Request.QueryString["tm"]) ||
                    !RegEx.IsInteger(Request.QueryString["ty"]) ||
                    !RegEx.IsInteger(Request.QueryString["td"]) ||
                    !RegEx.IsInteger(Request.QueryString["fy"]))
                {
                    Session.Abandon();
                    Response.Redirect("~/login.aspx");
                }

                NameObjectAuthorization homeAuthorization = new NameObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

                ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["h_id"]), Convert.ToInt32(Session["name_id"]), 4))
                {
                    Session.Abandon();
                    Response.Redirect("~/login.aspx");
                }
                ///////// SECURITY OBJECT CHECK  END ////////////////////////////////


                to = Convert.ToDateTime(c.DateCulture(Request.QueryString["tm"], Request.QueryString["td"], Request.QueryString["ty"], Convert.ToString(Session["_lastCulture"])));

            }
            else
                // BY DEFAULT WE WANT TO SEE THE RENT THAT WAS PAID THIS MONTH , I.E: FROM THE 1 ST DAY OF MONTH TO TODAY
                to = Convert.ToDateTime(c.DateCulture(to.Month.ToString(), to.Day.ToString(), to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
            //-----------------------------------------------------


            ddl_from_m.SelectedValue = from.Month.ToString();
            ddl_from_d.SelectedValue = from.Day.ToString();
            ddl_from_y.SelectedValue = from.Year.ToString();

            ddl_to_m.SelectedValue = to.Month.ToString();
            ddl_to_d.SelectedValue = to.Day.ToString();
            ddl_to_y.SelectedValue = to.Year.ToString();

            // First we check if there's home available
            tiger.Owner h = new tiger.Owner(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = h.getOwnerHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
            string link_to_unit = "";
            if (home_count > 0)
            {
                int home_id = h.getOwnerHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                // link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                //Session["schema_id"]));


                ddl_home_id.Visible = true;


                ddl_home_id.DataSource = h.getOwnerHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                ddl_home_id.SelectedValue = Convert.ToString(home_id);
                ddl_home_id.DataBind();
                ddl_home_id.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
                ddl_home_id.SelectedIndex = 0;


               

                //*********************************************
                // DropDownList pour les Unit
                //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

                if (unit_count > 0)
                {

                    int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), home_id);


                  //  ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
                  //  ddl_unit_id.SelectedValue = Convert.ToString(unit_id);
                   // ddl_unit_id.DataBind();
                    ddl_unit_id.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
                    ddl_unit_id.SelectedIndex = 0;
                    




                    // construction of the gridview of the rent payment archive
                    tiger.Owner v = new tiger.Owner(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    gv_rent_payment_archive.DataSource = v.getOwnerRentPaymentArchive(Convert.ToInt32(Session["schema_id"]), 0, 0, from, to, Convert.ToInt32(Session["name_id"]));
                    gv_rent_payment_archive.DataBind();



                }

                 // if ther is no unit
                else
                {

                    //   txt_message.InnerHtml = "There is no unit in this property -- add unit";

                }

                //hidden fields



            }
            // if ther is no home

            else
            {
                //  txt_message.InnerHtml = "There is no property -- Add a property";

                //  txt_link.InnerHtml = home_count + " <a href='home_add.aspx'>Add Home</a>&nbsp;&nbsp;<a href='owner_add.aspx'>Add Owner</a>&nbsp;&nbsp;<a href='janitor_add.aspx'>Add Janitor</a>&nbsp;&nbsp;";
            }


            lbl_confirmation.Visible = false;
            lbl_unit_door_no.Visible = false;

            lbl_amount.Visible = false;
            lbl_date_paid.Visible = false;

            Label2.Visible = false;
            Label3.Visible = false;
            Label4.Visible = false;
            Label5.Visible = false;
            Label6.Visible = false;
            Label7.Visible = false;

        }

    }

    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {

            ddl_home_id.Visible = true;

            // txt_pending.InnerHtml = "";

            //To view the address of the property

            tiger.Unit u = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
            //change list of unit id

             DateTime to = new DateTime();
             DateTime from = new DateTime();
             to = DateTime.Now; // the date in the to drop downlist
             
             tiger.Date d = new tiger.Date();
             // BY DEFAULT WE WANT TO SEE THE RENT THAT WAS PAID THIS MONTH , I.E: FROM THE 1 ST DAY OF MONTH TO TODAY
             to = Convert.ToDateTime(d.DateCulture(to.Month.ToString(), to.Day.ToString(), to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
             from = Convert.ToDateTime(d.DateCulture(to.Month.ToString(), "1", to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));

            if (unit_id > 0)
            {
                ddl_unit_id.Visible = true;
                //first dispose of old ddl_unit
                

                
               
                    tiger.Owner v = new tiger.Owner(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                
                    ddl_unit_id.Dispose();
                    ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
                    ddl_unit_id.DataBind();
                    ddl_unit_id.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
                    ddl_unit_id.SelectedIndex = 0;

                    // construction of the gridview for the payment archive
                    gv_rent_payment_archive.DataSource = v.getOwnerRentPaymentArchive(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), 0, from, to, Convert.ToInt32(Session["name_id"]));
                    gv_rent_payment_archive.DataBind();
                


                ddl_from_m.SelectedValue = from.Month.ToString();
                ddl_from_d.SelectedValue = from.Day.ToString();
                ddl_from_y.SelectedValue = from.Year.ToString();

                ddl_to_m.SelectedValue = to.Month.ToString();
                ddl_to_d.SelectedValue = to.Day.ToString();
                ddl_to_y.SelectedValue = to.Year.ToString();

            }
            else
            {
                tiger.Owner v = new tiger.Owner(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                if (ddl_home_id.SelectedValue == "0")
                {
                    ddl_unit_id.Dispose();
                    ddl_unit_id.Items.Clear();
                    ddl_unit_id.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
                    ddl_unit_id.SelectedIndex = 0;

                    gv_rent_payment_archive.DataSource = v.getOwnerRentPaymentArchive(Convert.ToInt32(Session["schema_id"]), 0, 0, from, to, Convert.ToInt32(Session["name_id"]));
                    gv_rent_payment_archive.DataBind();
                }
            }
            //   }// fin if not postback
     

        lbl_confirmation.Visible = false;
        lbl_unit_door_no.Visible = false;

        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;

        Label2.Visible = false;
        Label3.Visible = false;
        Label4.Visible = false;
        Label5.Visible = false;
        Label6.Visible = false;
        Label7.Visible = false;

    }


    protected void ddl_unit_id_SelectedIndexChanged(object sender, EventArgs e)
    {

        int unit_id;

        if (ddl_unit_id.SelectedIndex == 0)
            unit_id = 0;
        else
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        DateTime to = new DateTime();
        DateTime from = new DateTime();


        tiger.Date d = new tiger.Date();
        to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        // construction of the gridview for the payment archive
        tiger.Owner v = new tiger.Owner(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_rent_payment_archive.DataSource = v.getOwnerRentPaymentArchive(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), unit_id, from, to, Convert.ToInt32(Session["name_id"]));
        gv_rent_payment_archive.DataBind();


        lbl_confirmation.Visible = false;
        lbl_unit_door_no.Visible = false;

        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;

        Label2.Visible = false;
        Label3.Visible = false;
        Label4.Visible = false;
        Label5.Visible = false;
        Label6.Visible = false;
        Label7.Visible = false;

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        int unit_id;

        if (ddl_unit_id.SelectedIndex == 0)
            unit_id = 0;
        else
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        //hd_unit_id.Value = Convert.ToString(ddl_unit_id.SelectedValue);
        // label_added_unit.Text = ddl_unit_id.SelectedItem.Text;
        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


        DateTime to = new DateTime();
        DateTime from = new DateTime();
        to = DateTime.Now; // the date in the to drop downlist

        tiger.Date d = new tiger.Date();
        to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        // construction of the gridview for the payment archive
        tiger.Owner v = new tiger.Owner(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_rent_payment_archive.DataSource = v.getOwnerRentPaymentArchive(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), unit_id, from, to, Convert.ToInt32(Session["name_id"]));
        gv_rent_payment_archive.DataBind();

        lbl_confirmation.Visible = false;
        lbl_unit_door_no.Visible = false;

        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;

        Label2.Visible = false;
        Label3.Visible = false;
        Label4.Visible = false;
        Label5.Visible = false;
        Label6.Visible = false;
        Label7.Visible = false;

    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_cancel_Click(object sender, EventArgs e)
    {


        int unit_id;

        if (ddl_unit_id.SelectedIndex == 0)
            unit_id = 0;
        else
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        // get data from the Hiddenfiel in the gridview ( from the GridView)


        //*********************************************************************
        //*********************************************************************
        Button btn_cancel = (Button)sender;
        GridViewRow grdRow = (GridViewRow)btn_cancel.Parent.Parent;
        string strField1 = grdRow.Cells[6].Text;

        HiddenField h_rp_id = (HiddenField)grdRow.Cells[6].FindControl("h_rp_id");
        HiddenField h_rp_amount = (HiddenField)grdRow.Cells[6].FindControl("h_rp_amount");
        HiddenField h_rp_paid_date = (HiddenField)grdRow.Cells[6].FindControl("h_rp_paid_date");
        HiddenField h_unit_door_no = (HiddenField)grdRow.Cells[6].FindControl("h_unit_door_no");

      //  lbl_confirmation.ForeColor = "Red";
        lbl_confirmation.Text = Resources.Resource.lbl_cancel_confirmation;

        Label2.Text = Resources.Resource.lbl_door_no;
        Label4.Text = Resources.Resource.lbl_amount;
        Label6.Text = Resources.Resource.lbl_date_received;

        Label3.Text = ":";
        Label5.Text = ":";
        Label7.Text = ":";

        lbl_amount.Text = h_rp_amount.Value;
        lbl_date_paid.Text = h_rp_paid_date.Value;
        lbl_unit_door_no.Text = h_unit_door_no.Value;
        

        lbl_confirmation.Visible = true;
        lbl_unit_door_no.Visible = true;

        lbl_amount.Visible = true;
        lbl_date_paid.Visible = true;

        Label2.Visible = true;
        Label3.Visible = true;
        Label4.Visible = true;
        Label5.Visible = true;
        Label6.Visible = true;
        Label7.Visible = true;
    

        
        //------------------------------------------------------------------------------------

        // check if the rp_id belong to the account
        NameObjectAuthorization rpAuthorization = new NameObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!rpAuthorization.RentPaid(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(h_rp_id.Value), Convert.ToInt32(Session["name_id"]), 4))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
             
        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prCancelRentPayment", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@rp_id", SqlDbType.Int).Value = Convert.ToInt32(h_rp_id.Value);
     
            //execute the insert
            cmd.ExecuteReader();

        }
        catch (Exception error)
        {
            //  tiger.security.Error.errorAdd(conn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
        }


        //-------------------------------------------------------------------------------------

        DateTime to = new DateTime();
        DateTime from = new DateTime();

        tiger.Date d = new tiger.Date();
        to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        // construction of the gridview for the payment archive
        tiger.Owner v = new tiger.Owner(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_rent_payment_archive.DataSource = v.getOwnerRentPaymentArchive(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), unit_id, from, to, Convert.ToInt32(Session["name_id"]));
        gv_rent_payment_archive.DataBind();


    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_update_Click(object sender, EventArgs e)
    {


        int unit_id;

        if (ddl_unit_id.SelectedIndex == 0)
            unit_id = 0;
        else
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        // get data from the Hiddenfiel in the gridview ( from the GridView)


        //*********************************************************************
        //*********************************************************************
        Button btn_update = (Button)sender;
        GridViewRow grdRow = (GridViewRow)btn_update.Parent.Parent;
        string strField1 = grdRow.Cells[6].Text;

        HiddenField h_rp_id = (HiddenField)grdRow.Cells[6].FindControl("h_rp_id");
     


        h_rp_id2.Value = h_rp_id.Value;

        Response.Redirect("tenant_rent_update",true);
    }


    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    protected string GetTrasnl()
    {
        
        string fm, fd, fy, tm, td, ty, uid, hid;

        fm = ddl_from_m.SelectedValue;
        fd = ddl_from_d.SelectedValue;
        fy = ddl_from_y.SelectedValue;

        tm = ddl_to_m.SelectedValue;
        td = ddl_to_d.SelectedValue;
        ty = ddl_to_y.SelectedValue;

        uid = ddl_unit_id.SelectedValue;

          if (uid.StartsWith("--") )
        {
           uid = "0";
        }

        return "&fm=" + fm + "&fd=" + fd + "&fy=" + fy + "&tm=" 
                      + tm + "&td=" + td + "&ty=" + ty + "&uid=" + uid;

    }


    protected void gv_rent_payment_archive_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        int unit_id;

        if (ddl_unit_id.SelectedIndex == 0)
            unit_id = 0;
        else
            unit_id = Convert.ToInt32(ddl_unit_id.SelectedValue);

        DateTime to = new DateTime();
        DateTime from = new DateTime();


        tiger.Date d = new tiger.Date();
        to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

         tiger.Owner v = new tiger.Owner(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            
        if (ddl_unit_id.SelectedIndex == 0)
        {

            // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
            gv_rent_payment_archive.PageIndex = e.NewPageIndex;
            gv_rent_payment_archive.DataSource = v.getOwnerRentPaymentArchive(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), 0, from, to, Convert.ToInt32(Session["name_id"]));
            gv_rent_payment_archive.DataBind();

        }

        else
        { 
            // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
            gv_rent_payment_archive.PageIndex = e.NewPageIndex;
            gv_rent_payment_archive.DataSource = v.getOwnerRentPaymentArchive(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(ddl_unit_id.SelectedValue), from, to, Convert.ToInt32(Session["name_id"]));
            gv_rent_payment_archive.DataBind();
        }



    }
}
