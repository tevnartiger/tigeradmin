﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/user/mp_owner.master" AutoEventWireup="true" CodeFile="financial_analysis.aspx.cs" Inherits="manager_Financial_financial_analysis" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:Label ID="Label2" runat="server" 
                    Text="<%$ Resources:Resource, lbl_financial_analysis%>" /><br /><br />
<table>
<tr><td> <asp:Label ID="lbl_property" runat="server" Text="<%$ Resources:Resource, lbl_property %>"/>
                </td>
                <td>
       <asp:DropDownList ID="ddl_home_id" DataValueField="home_id" DataTextField="home_name"   runat="server" autopostback="true" OnSelectedIndexChanged="ddl_home_id_SelectedIndexChanged" />
       </td>  
</tr>
 </table><br />
 
  <table style="width: 100%">
        <tr>
            <td valign="top">
 <asp:Repeater runat="server" ID="rhome_view">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
        
        <tr>
          <td>
                 District&nbsp;:&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_district")%> 
                   </td>
            </tr>
           <tr>
              <td valign="top" >
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_no")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_street")%></td>
            </tr>
            <tr>
                 <td valign="top" >
                   <%#DataBinder.Eval(Container.DataItem, "home_city")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_prov")%></td>
               
            </tr>
            <tr>
                 <td valign="top" >
                   <%#DataBinder.Eval(Container.DataItem, "home_pc")%> 
                
                   </td>
               
            </tr>   
        </table>
        </ItemTemplate>
        </asp:Repeater>
            </td>
            <td valign="top">
                <table bgcolor="#ffffcc"  >
                    <tr>
                        <td>
                          <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, txt_month %>"></asp:Label>&nbsp;/&nbsp;<asp:Label ID="lbl_year" runat="server" Text="<%$ Resources:Resource, lbl_year %>"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_date_received_m" runat="server" 
                                AutoPostBack="true" onselectedindexchanged="ddl_date_received_m_SelectedIndexChanged" 
                                >
                                <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                            </asp:DropDownList>
                            &nbsp; / &nbsp;
                            <asp:DropDownList ID="ddl_date_received_y" runat="server" onselectedindexchanged="ddl_date_received_y_SelectedIndexChanged" 
                              AutoPostBack="true"   >
                                <asp:ListItem>2001</asp:ListItem>
                                <asp:ListItem>2002</asp:ListItem>
                                <asp:ListItem>2003</asp:ListItem>
                                <asp:ListItem>2004</asp:ListItem>
                                <asp:ListItem>2005</asp:ListItem>
                                <asp:ListItem>2006</asp:ListItem>
                                <asp:ListItem>2007</asp:ListItem>
                                <asp:ListItem>2008</asp:ListItem>
                                <asp:ListItem>2009</asp:ListItem>
                                <asp:ListItem>2010</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    </table>
               
                <br />
               
                <br />
              <asp:Button ID="Button1" runat="server" onclick="btn_income_view_graph_Click" 
                    Text="<%$ Resources:Resource, lbl_view_income_graph %>" /> &nbsp;&nbsp; <asp:Button ID="btn_view_graph" runat="server" onclick="btn_view_graph_Click" 
                    Text="<%$ Resources:Resource, lbl_view_expense_graph %>" />
            </td>
        </tr>
    </table>
    <br />
    
    
    
     <table style="width: 100%">
        <tr>
            <td bgcolor="AliceBlue">
                <asp:Label ID="lbl_revenue" runat="server" 
                    Text="<%$ Resources:Resource, lbl_revenue %>" style="font-weight: 700"/> </td>
            
            <td bgcolor="AliceBlue">
                <asp:Label ID="lbl_monthly" runat="server" 
                    Text="<%$ Resources:Resource, lbl_monthly %>" style="font-weight: 700"/></td>
           
            <td bgcolor="AliceBlue">
                <b>(%) income</b></td>
           
            <td bgcolor="AliceBlue">
                &nbsp;</td>
           
        </tr>
        
        
        <asp:Repeater ID="rNumberUnitbyBedroomNumber" runat="server">
    <ItemTemplate>
    <tr>
    <td> <%#DataBinder.Eval(Container.DataItem, "number_of_unit")%> &nbsp;X &nbsp;<%#DataBinder.Eval(Container.DataItem, "unit_bedroom_no")%>&nbsp;&nbsp; <asp:Label ID="lbl_bedroom" runat="server" 
                    Text="<%$ Resources:Resource, lbl_bedroom %>"/></td>
    
   
    <td><asp:Label ID="lbl_bedrooms_total_rent_m" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "total_rent_received","{0:0.00}")%>'  />
                                                                       
                     
        <asp:HiddenField ID="h_number_of_unit" Value='<%#DataBinder.Eval(Container.DataItem, "number_of_unit")%>' runat="server" /> 
        <asp:HiddenField ID="h_unit_bedroom_number" Value='<%#DataBinder.Eval(Container.DataItem, "unit_bedroom_no")%>' runat="server" />
        
                     </td>
                     
       <td><asp:Label ID="lbl_rent_income_amount_percent" runat="server" 
                    Text=""  />
                   </td>
    </tr>
    </ItemTemplate>
    </asp:Repeater>
    
    
    
    <asp:Repeater ID="rNumberCommercialUnit" runat="server">
    <ItemTemplate>
    <tr>
    <td> <%#DataBinder.Eval(Container.DataItem, "number_of_unit")%> &nbsp;&nbsp; <asp:Label ID="lbl_bedroom" runat="server" Text="Commercial Unit"/></td>
    
   
    <td><asp:Label ID="lbl_commercial_total_rent_m" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "total_rent_received","{0:0.00}")%>'  />
                                                                       
                     
        <asp:HiddenField ID="h_number_of_unit" Value='<%#DataBinder.Eval(Container.DataItem, "number_of_unit")%>' runat="server" /> 
        <asp:HiddenField ID="h_total_rent_received" Value='<%#DataBinder.Eval(Container.DataItem, "total_rent_received")%>' runat="server" />
        
                     </td>
                     
       <td><asp:Label ID="lbl_rent_income_amount_percent" runat="server" 
                    Text=""  />
                   </td>
    </tr>
    </ItemTemplate>
    </asp:Repeater>
    
    
    
  
   <asp:Repeater ID="rIncome"   runat="server">
    <ItemTemplate  >
        <tr  >
            <td valign="top" style="height: 18px">
                <asp:Label ID="Label40" runat="server" 
                    Text='<%#GetIncomeCateg(Convert.ToInt32(Eval("incomecateg_id")))%>'   />&nbsp;</td>
            <td valign="top"  >
                <%#DataBinder.Eval(Container.DataItem, "income_amount","{0:0.00}")%>
                <asp:HiddenField ID="h_income_amount" Value='<%#DataBinder.Eval(Container.DataItem, "income_amount")%>' runat="server" />
        
            </td>
            
            <td>
                <asp:Label ID="lbl_income_amount_percent" runat="server" Width="65px"></asp:Label>
                </td>
           
           
        </tr>
        
        </ItemTemplate>
    </asp:Repeater>
  
  
        
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" 
                    Text="<%$ Resources:Resource, lbl_late_fee %>"/></td>
            
            <td>
                <asp:Label ID="lbl_late_fee" runat="server" Width="65px"></asp:Label>
            </td>
           
            <td>
                <asp:Label ID="lbl_late_fee_percent" runat="server" Width="65px"></asp:Label></td>
           
            <td>
                &nbsp;</td>
           
        </tr>
        
          <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            
            <td>
                &nbsp;</td>
            
            <td>
                &nbsp;</td>
            
        </tr>
        
        <tr>
            <td>
                <asp:Label ID="Label17" runat="server" 
                     Text="<%$ Resources:Resource, lbl_egi %>" style="font-weight: 700"/>&nbsp;</td>
           
            <td>
               <asp:Label ID="lbl_gi_m" runat="server" /></td>
            
            <td>
                &nbsp;</td>
            
            <td>
                &nbsp;</td>
            
        </tr>
        
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
           
            <td>
                &nbsp;</td>
           
            <td>
                &nbsp;</td>
           
        </tr>
        
        <tr>
            <td bgcolor="AliceBlue">
                <asp:Label ID="Label5" runat="server" 
                    Text="<%$ Resources:Resource, lbl_expenses%>" style="font-weight: 700"/></td>
            
            <td bgcolor="AliceBlue">
                 <asp:Label ID="Label3" runat="server" 
                     Text="<%$ Resources:Resource, lbl_monthly %>" style="font-weight: 700"/></td>
            
            <td bgcolor="AliceBlue">
                 <b>(%) expense</b></td>
            
            <td bgcolor="AliceBlue">
                 <asp:Label ID="Label7" runat="server" 
                     Text="<%$ Resources:Resource, lbl_egi_percent %>" style="font-weight: 700"/>
                </td>
            
        </tr>
        
             
    <asp:Repeater ID="rExpense"   runat="server">
    <ItemTemplate  >
        <tr  >
            <td valign="top" style="height: 18px">
                <asp:Label ID="Label40" runat="server" 
                    Text='<%#GetExpenseCateg(Convert.ToInt32(Eval("expensecateg_id")))%>'   />&nbsp;</td>
            <td valign="top"  >
                <%#DataBinder.Eval(Container.DataItem, "expense_amount","{0:0.00}")%>
                
                <asp:HiddenField ID="h_expense_amount" Value='<%#DataBinder.Eval(Container.DataItem, "expense_amount")%>' runat="server" />
        
            </td>
          
            <td><asp:Label ID="lbl_expense_amount_percent" runat="server" 
                    Text=""  />
                   </td>
                   
          <td><asp:Label ID="lbl_egi_percent" runat="server" 
                    Text=""  />
                   </td>
        </tr>
        
        </ItemTemplate>
    </asp:Repeater>
       
        <tr>
            <td style="height: 18px">
                &nbsp;</td>
            <td style="height: 18px">
                &nbsp;</td>
            
            <td style="height: 18px">
                &nbsp;</td>
            
            <td style="height: 18px">
                &nbsp;</td>
            
        </tr>
    
     <tr>
            <td style="height: 18px">
                <asp:Label ID="Label15" runat="server" 
                    Text="<%$ Resources:Resource, lbl_total_expenses %>" 
                    style="font-weight: 700"/></td>
            
            <td  >
                <asp:Label ID="lbl_total_expenses_m" runat="server" 
                    Text="lbl_total_expenses_m" /></td>
            
            <td  >
                &nbsp;</td>
            
            <td  >
                &nbsp;</td>
            
        </tr>
        <tr>
            <td style="height: 18px">
                <br />
            </td>
            <td style="height: 18px">
                &nbsp;</td>
            
            <td style="height: 18px">
                &nbsp;</td>
            
            <td style="height: 18px">
                &nbsp;</td>
            
        </tr>
        
        
        
        <tr>
            <td bgcolor="AliceBlue">
                <asp:Label ID="Label16" runat="server" 
                    Text="<%$ Resources:Resource, lbl_money_flow %>" 
                    style="font-weight: 700" /></td>
           
            <td bgcolor="AliceBlue">
                 <asp:Label ID="Label4" runat="server" 
                     Text="<%$ Resources:Resource, lbl_monthly %>" style="font-weight: 700"/></td>
           
            <td bgcolor="AliceBlue">
                 &nbsp;</td>
           
            <td bgcolor="AliceBlue">
                 &nbsp;</td>
        </tr>
        <tr>
            <td>
               <asp:Label ID="Label29" runat="server" 
                     Text="<%$ Resources:Resource, lbl_net_operating_inc %>" /></td>
            
            <td>
                <asp:Label ID="lbl_net_operating_inc_m" runat="server"   /></td>
            
            <td>
                &nbsp;</td>
            
            <td>
                &nbsp;</td>
        </tr>
    
        <tr>
            <td>
               <asp:Label ID="Label30" runat="server" 
                     Text="<%$ Resources:Resource, lbl_mortgage %>"/></td>
           
            <td>
                <asp:TextBox ID="tbx_mortgage_m" runat="server" Width="65px"></asp:TextBox>
                &nbsp;<asp:RegularExpressionValidator ID="reg_mortgage_m"  
                         ControlToValidate="tbx_mortgage_m"
                         ValidationGroup="vg_cash_flow"
                    runat="server" ErrorMessage="enter the mortgage">
                    </asp:RegularExpressionValidator><hr />
            </td>
           
            <td>
                &nbsp;</td>
           
            <td>
                &nbsp;</td>
        </tr>
    
         <tr>
            <td>
                <asp:Label ID="Label31" runat="server" 
                     Text="<%$ Resources:Resource, lbl_liquidity %>" 
                    style="font-weight: 700" /></td>
            
            <td>
                <asp:Label ID="lbl_liquidity_m" runat="server"  /></td>
            
            <td>
                &nbsp;</td>
            
            <td>
                &nbsp;</td>
        </tr>
    
    
    <tr>
            <td>
                <asp:Button ID="Caculate" runat="server" onclick="Caculate_Click" 
                 ValidationGroup="vg_cash_flow"
                    Text="<%$ Resources:Resource, btn_calculate_liquidity %>" ForeColor="#0066FF" />
                &nbsp;<br />
                </td>
            <td>
                &nbsp;</td>
            
            <td>
                &nbsp;</td>
            
            <td>
                &nbsp;</td>
            
        </tr>
    </table>
    
    
    
    

</asp:Content>

