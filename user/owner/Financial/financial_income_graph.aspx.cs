﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Drawing;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.DataVisualization;
using sinfoca.tiger.security.NameObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : april 17 , 2008
/// </summary>
/// 
public partial class manager_Financial_financial_income_graph : BasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        // dotnetCHARTING.DataEngine de = new dotnetCHARTING.DataEngine();

        if (!(Page.IsPostBack))
        {

            NameObjectAuthorization homeAuthorization = new NameObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            DateTime date = new DateTime();
            date = DateTime.Now;
            tiger.Date c = new tiger.Date();
            //if we are entering from the update page
            //-----------------------------------------------------
           if (Request.QueryString["h_id"] != "" && Request.QueryString["h_id"] != null && Request.QueryString["m"] != "" && Request.QueryString["m"] != null && Request.QueryString["y"] != "" && Request.QueryString["y"] != null)
            {
                if (RegEx.IsInteger(Request.QueryString["h_id"]) == false ||
                    RegEx.IsInteger(Request.QueryString["m"]) == false ||
                    RegEx.IsInteger(Request.QueryString["y"]) == false ||
                    RegEx.IsInteger(Request.QueryString["d"]) == false)
                {
                    Session.Abandon();
                    Response.Redirect("~/login.aspx");
                }


                date = Convert.ToDateTime(c.DateCulture(Request.QueryString["m"], Request.QueryString["d"], Request.QueryString["y"], Convert.ToString(Session["_lastCulture"])));

            }
           //-----------------------------------------------------


            ddl_date_received_m.SelectedValue = date.Month.ToString();
            ddl_date_received_y.SelectedValue = date.Year.ToString();

            ddl_dimension.SelectedValue = "3";


            tiger.Owner h = new tiger.Owner(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = h.getOwnerHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

            if (home_count > 0)
            {
                int home_id = h.getOwnerHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

                //if we are entering from the update page
                //-----------------------------------------------------
                if (Request.QueryString["h_id"] != "" && Request.QueryString["h_id"] != null)
                {
                    if (!RegEx.IsInteger(Request.QueryString["h_id"]))
                    {
                        Session.Abandon();
                        Response.Redirect("~/login.aspx");
                    }

                    home_id = Convert.ToInt32(Request.QueryString["h_id"]);
                }
                //-----------------------------------------------------

                ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), home_id, Convert.ToInt32(Session["name_id"]), 4))
                {
                    Session.Abandon();
                    Response.Redirect("~/login.aspx");
                }
                ///////// SECURITY OBJECT CHECK  END ////////////////////////////////


                ddl_home_id.DataSource = h.getOwnerHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                ddl_home_id.SelectedValue = Convert.ToString(home_id);
                ddl_home_id.DataBind();



                //To view the address of the property

                tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
                rhome_view.DataBind();

                // to view the incomes in the default home
                DateTime income_date_received = new DateTime();
                tiger.Date d = new tiger.Date();
                date = Convert.ToDateTime(d.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


                //set global properties

                /////////////////////   CHARTTING BEGIN FOR CHART 3    ///////////////////////////////////////////////////////////


                Chart3.Width = 900;
                Chart3.Height = 900;
                Chart3.Palette = ChartColorPalette.BrightPastel;
                //   Title t = new Title(Resources.Resource.lbl_u_income, Docking.Top, new System.Drawing.Font("Trebuchet MS", 14, System.Drawing.FontStyle.Bold), System.Drawing.Color.FromArgb(26, 59, 105));
                //  Chart3.Titles.Add(t);

                Chart3.ChartAreas.Add("Series 1");

                // create a couple of series
                Chart3.Series.Add("Series 1");

                // Create a database connection object using the connection string    


                SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                // Create a database command on the connection using query    
                SqlCommand cmd = new SqlCommand("prIncomeMonthViewGroupByCategChart", conn);
                cmd.CommandType = CommandType.StoredProcedure;


                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@lastCulture", SqlDbType.VarChar, 20).Value = Convert.ToString(Session["_lastCulture"]);
                cmd.Parameters.Add("@date_month_year", SqlDbType.DateTime).Value = date;

                // Open the connection    
                conn.Open();

                // Create a database reader    
                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);


                // Since the reader implements IEnumerable, pass the reader directly into
                //   the DataBind method with the name of the Column selected in the query    
                Chart3.Series["Series 1"].Points.DataBindXY(dr, "incomecateg_name", dr, "income_amount");

                // Close the reader and the connection
                dr.Close();
                conn.Close();


                // Add the second legend
                Chart3.Legends.Add(new Legend("first"));
                //  Chart3.Series["Series 1"].LegendText = "#AXISLABEL - #VALY{N2}";
                Chart3.Series["Series 1"].Label = "#AXISLABEL \n#PERCENT";

                Chart3.Series["Series 1"].Legend = "first";

                // Add Color column
                LegendCellColumn firstColumn = new LegendCellColumn();
                firstColumn.ColumnType = LegendCellColumnType.SeriesSymbol;
                firstColumn.HeaderText = "";
                firstColumn.HeaderBackColor = Color.WhiteSmoke;
                Chart3.Legends["first"].CellColumns.Add(firstColumn);

                // Add Legend Text column
                LegendCellColumn secondColumn = new LegendCellColumn();
                secondColumn.ColumnType = LegendCellColumnType.Text;
                secondColumn.HeaderText = "Categories";
                secondColumn.Text = "#AXISLABEL";
                secondColumn.HeaderBackColor = Color.WhiteSmoke;
                Chart3.Legends["first"].CellColumns.Add(secondColumn);

                // Add Legend Text column
                LegendCellColumn thirdColumn = new LegendCellColumn();
                thirdColumn.ColumnType = LegendCellColumnType.Text;
                thirdColumn.HeaderText = "Amount";
                thirdColumn.Text = "#VAL{N2}";
                thirdColumn.HeaderBackColor = Color.WhiteSmoke;
                Chart3.Legends["first"].CellColumns.Add(thirdColumn);

                Chart3.Legends["first"].ShadowOffset = 2;
                Chart3.Series["Series 1"].ChartType = SeriesChartType.Pie;

                // Enable 3D
                Chart3.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                Chart3.ChartAreas["ChartArea1"].BorderColor = Color.Black;
                Chart3.ChartAreas["ChartArea1"].BorderWidth = 5;


                for (int i = 1; i < Chart3.Series["Series 1"].Points.Count; i++)
                {
                    Chart3.Series["Series 1"].Points[i]["Exploded"] = "true";
                }

                // Set labels style
                Chart3.Series["Series 1"]["PieLabelStyle"] = "Outside";

                ddl_dimension.SelectedValue = "3";
                //////////////////////////////////////////////////////////////////////////////////////////////




                //   Title t = new Title(Resources.Resource.lbl_u_income, Docking.Top, new System.Drawing.Font("Trebuchet MS", 14, System.Drawing.FontStyle.Bold), System.Drawing.Color.FromArgb(26, 59, 105));
                //  Chart3.Titles.Add(t);
                Chart1.Width = 900;
                Chart1.Height = 900;
                Chart1.Palette = ChartColorPalette.BrightPastel;

                Chart1.ChartAreas.Add("Series 2");

                // create a couple of series
                Chart1.Series.Add("Series 2");

                // Create a database connection object using the connection string    
                SqlConnection conn2 = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

                // Create a database command on the connection using query    
                SqlCommand cmd2 = new SqlCommand("prIncomeMonthViewGroupByCategChart", conn2);
                cmd2.CommandType = CommandType.StoredProcedure;


                cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd2.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd2.Parameters.Add("@lastCulture", SqlDbType.VarChar, 20).Value = Convert.ToString(Session["_lastCulture"]);
                cmd2.Parameters.Add("@date_month_year", SqlDbType.DateTime).Value = date;

                // Open the connection    
                conn2.Open();

                // Create a database reader    
                SqlDataReader dr2 = null;

                dr2 = cmd2.ExecuteReader(CommandBehavior.CloseConnection);


                // Since the reader implements IEnumerable, pass the reader directly into
                //   the DataBind method with the name of the Column selected in the query    
                Chart1.Series["Series 2"].Points.DataBindXY(dr2, "incomecateg_name", dr2, "income_amount");

                // Close the reader and the connection
                dr2.Close();
                conn2.Close();

               
                //  Chart1.Series["Series 2"].LegendText = "#AXISLABEL - #VALY{N2}";
                Chart1.Series["Series 2"].Label = " #VALY{N2} \n#PERCENT";
               
                Chart1.Series["Series 2"].ChartType = SeriesChartType.Bar;
                // Set data points label style
                Chart1.Series["Series 2"]["BarLabelStyle"] = "Center";
    
             
                // Enable 3D
                Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
                Chart1.ChartAreas["ChartArea1"].BorderColor = Color.Black;
                Chart1.ChartAreas["ChartArea1"].BorderWidth = 5;

                foreach (DataPoint point in Chart1.Series[0].Points)
                {
                    point.YValues[0] = point.YValues[0];
                    Chart1.Series["Series 2"].Palette = ChartColorPalette.BrightPastel;//assigning colors to datapoint  
                }

                ddl_dimension.SelectedValue = "3";

                ///////////////////// CHARTING 4 BEBIN   ///////////////////////////////////////


            }


        }


        //Chart1.SeriesCollection.Add(getLiveData());

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        //To view the address of the property

        tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
        rhome_view.DataBind();

        ddl_dimension.SelectedValue = "3";

        // to view the expense in the  home for the date
        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        DateTime date = new DateTime();
        date = DateTime.Now;

        tiger.Date df = new tiger.Date();
        date = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        if (ddl_chartype.SelectedValue == "2")
        {
            Chart1.Visible = true;
            Chart3.Visible = false;

            Chart1.Width = 900;
            Chart1.Height = 900;
            SqlConnection conn2 = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            // Create a database command on the connection using query    
            SqlCommand cmd2 = new SqlCommand("prIncomeMonthViewGroupByCategChart", conn2);
            cmd2.CommandType = CommandType.StoredProcedure;

            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd2.Parameters.Add("@lastCulture", SqlDbType.VarChar, 20).Value = Convert.ToString(Session["_lastCulture"]);
            cmd2.Parameters.Add("@date_month_year", SqlDbType.DateTime).Value = date;

            // Open the connection    
            conn2.Open();

            // Create a database reader    
            SqlDataReader dr2 = null;

            dr2 = cmd2.ExecuteReader(CommandBehavior.CloseConnection);


            // Since the reader implements IEnumerable, pass the reader directly into
            //   the DataBind method with the name of the Column selected in the query    
            Chart1.Series["Series 2"].Points.DataBindXY(dr2, "incomecateg_name", dr2, "income_amount");

            // Close the reader and the connection
            dr2.Close();
            conn2.Close();

            Chart1.Series["Series 2"].LegendText = "#AXISLABEL - #VALY{N2}";
            Chart1.Series["Series 2"].Label = "#AXISLABEL \n#PERCENT";


            Chart1.Legends["second"].ShadowOffset = 2;
            Chart1.Series["Series 2"].ChartType = SeriesChartType.Bar;


            // Enable 3D

            if (ddl_dimension.SelectedValue == "3")
                Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
            else
                Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;


            Chart1.ChartAreas["ChartArea1"].BorderColor = Color.Black;
            Chart1.ChartAreas["ChartArea1"].BorderWidth = 5;

            foreach (DataPoint point in Chart1.Series[0].Points)
            {
                point.YValues[0] = point.YValues[0];
                Chart1.Series["Series 2"].Palette = ChartColorPalette.BrightPastel;//assigning colors to datapoint  
            }

        }
        //////////////////////////////////////////////////////////////////////////////////
        /////////////////////////   PIE CHART   ///////////////////////////////////////////////

        if (ddl_chartype.SelectedValue == "1")
        {
            Chart1.Visible = false;
            Chart3.Visible = true;

            Chart3.Width = 900;
            Chart3.Height = 900;
            Chart3.Palette = ChartColorPalette.BrightPastel;

            // Create a database connection object using the connection string    

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            // Create a database command on the connection using query    
            SqlCommand cmd = new SqlCommand("prIncomeMonthViewGroupByCategChart", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@lastCulture", SqlDbType.VarChar, 20).Value = Convert.ToString(Session["_lastCulture"]);
            cmd.Parameters.Add("@date_month_year", SqlDbType.DateTime).Value = date;

            // Open the connection    
            conn.Open();

            // Create a database reader    
            SqlDataReader dr = null;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);


            // Since the reader implements IEnumerable, pass the reader directly into
            //   the DataBind method with the name of the Column selected in the query    
            Chart3.Series["Series 1"].Points.DataBindXY(dr, "incomecateg_name", dr, "income_amount");

            // Close the reader and the connection
            dr.Close();
            conn.Close();

            Chart3.Series["Series 1"].Label = "#AXISLABEL \n#PERCENT";

            Chart3.Series["Series 1"].Legend = "first";


            Chart3.Legends["first"].ShadowOffset = 2;
            Chart3.Series["Series 1"].ChartType = SeriesChartType.Pie;

            // Enable 3D
            Chart3.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
            Chart3.ChartAreas["ChartArea1"].BorderColor = Color.Black;
            Chart3.ChartAreas["ChartArea1"].BorderWidth = 5;


            for (int i = 1; i < Chart3.Series["Series 1"].Points.Count; i++)
            {
                Chart3.Series["Series 1"].Points[i]["Exploded"] = "true";
            }

            // Set labels style
            Chart3.Series["Series 1"]["PieLabelStyle"] = "Outside";
        }




        ddl_date_received_m.SelectedValue = date.Month.ToString();
        ddl_date_received_y.SelectedValue = date.Year.ToString();

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_view_Click(object sender, EventArgs e)
    {


        // to view the incomes in the  home
        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        DateTime date = new DateTime();
        tiger.Date df = new tiger.Date();
        date = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        if (ddl_chartype.SelectedValue == "2")
        {
            Chart1.Visible = true;
            Chart3.Visible = false;

            Chart1.Width = 900;
            Chart1.Height = 900;
            SqlConnection conn2 = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            // Create a database command on the connection using query    
            SqlCommand cmd2 = new SqlCommand("prIncomeMonthViewGroupByCategChart", conn2);
            cmd2.CommandType = CommandType.StoredProcedure;

            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd2.Parameters.Add("@lastCulture", SqlDbType.VarChar, 20).Value = Convert.ToString(Session["_lastCulture"]);
            cmd2.Parameters.Add("@date_month_year", SqlDbType.DateTime).Value = date;

            // Open the connection    
            conn2.Open();

            // Create a database reader    
            SqlDataReader dr2 = null;

            dr2 = cmd2.ExecuteReader(CommandBehavior.CloseConnection);


            // Since the reader implements IEnumerable, pass the reader directly into
            //   the DataBind method with the name of the Column selected in the query    
            Chart1.Series["Series 2"].Points.DataBindXY(dr2, "incomecateg_name", dr2, "income_amount");

            // Close the reader and the connection
            dr2.Close();
            conn2.Close();

            Chart1.Series["Series 2"].LegendText = "#AXISLABEL - #VALY{N2}";
            Chart1.Series["Series 2"].Label = "#AXISLABEL \n#PERCENT";


            //Chart1.Legends["second"].ShadowOffset = 2;
            Chart1.Series["Series 2"].ChartType = SeriesChartType.Bar;


            // Enable 3D

            if (ddl_dimension.SelectedValue == "3")
                Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
            else
                Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;


            Chart1.ChartAreas["ChartArea1"].BorderColor = Color.Black;
            Chart1.ChartAreas["ChartArea1"].BorderWidth = 5;

            foreach (DataPoint point in Chart1.Series[0].Points)
            {
                point.YValues[0] = point.YValues[0];
                Chart1.Series["Series 2"].Palette = ChartColorPalette.BrightPastel;//assigning colors to datapoint  
            }

        }
        //////////////////////////////////////////////////////////////////////////////////
        /////////////////////////   PIE CHART   ///////////////////////////////////////////////

        if (ddl_chartype.SelectedValue == "1")
        {
            Chart1.Visible = false;
            Chart3.Visible = true;

            Chart3.Width = 900;
            Chart3.Height = 900;
            Chart3.Palette = ChartColorPalette.BrightPastel;

            // Create a database connection object using the connection string    

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            // Create a database command on the connection using query    
            SqlCommand cmd = new SqlCommand("prIncomeMonthViewGroupByCategChart", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@lastCulture", SqlDbType.VarChar, 20).Value = Convert.ToString(Session["_lastCulture"]);
            cmd.Parameters.Add("@date_month_year", SqlDbType.DateTime).Value = date;

            // Open the connection    
            conn.Open();

            // Create a database reader    
            SqlDataReader dr = null;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);


            // Since the reader implements IEnumerable, pass the reader directly into
            //   the DataBind method with the name of the Column selected in the query    
            Chart3.Series["Series 1"].Points.DataBindXY(dr, "incomecateg_name", dr, "income_amount");

            // Close the reader and the connection
            dr.Close();
            conn.Close();

            Chart3.Series["Series 1"].Label = "#AXISLABEL \n#PERCENT";

            Chart3.Series["Series 1"].Legend = "first";


            Chart3.Legends["first"].ShadowOffset = 2;
            Chart3.Series["Series 1"].ChartType = SeriesChartType.Pie;

            // Enable 3D
            Chart3.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
            Chart3.ChartAreas["ChartArea1"].BorderColor = Color.Black;
            Chart3.ChartAreas["ChartArea1"].BorderWidth = 5;


            for (int i = 1; i < Chart3.Series["Series 1"].Points.Count; i++)
            {
                Chart3.Series["Series 1"].Points[i]["Exploded"] = "true";
            }

            // Set labels style
            Chart3.Series["Series 1"]["PieLabelStyle"] = "Outside";
        }


    }

    protected void btn_view_graph_Click(object sender, EventArgs e)
    {
        Response.Redirect("financial_expenses_graph.aspx?h_id=" + ddl_home_id.SelectedValue + "&m=" + ddl_date_received_m.SelectedValue
                          + "&d=1" + "&y=" + ddl_date_received_y.SelectedValue);
    }


}
