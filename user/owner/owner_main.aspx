﻿<%@ Page Title="" Language="C#" MasterPageFile="~/user/mp_owner.master" AutoEventWireup="true" CodeFile="owner_main.aspx.cs" Inherits="user_owner_owner_main" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css"  >
 
 
 
 
 table.padded-table td { padding:2px; }

.divborder {
/*z-index : 100000;*/
border: 1px solid #ccc; 
} 
 
 #dashboard_container { 
 float: left;
 color: #333;
 border: 1px solid #fff;
 background:#ffffff;
 margin:0 auto; 
 padding: 0px 10px 10px 10px;
 height: 100%;
 width: 98%;
 display: inline;
}


.dashboarddivheader {
font-family : verdana,arial,helvetica,sans-serif; 
font-size : 11px; 
text-align : left; 
font-weight : bold; 
color : #000000; 
background: #ffffff; 
background:url(../../App_Themes/SinfoTiger/images/grid_bg.png) repeat-x;
padding-top : 3px; 
padding-bottom : 3px; 
padding-left : 14px; 
border-bottom : 1px solid #000000; 
border-right : 1px solid #fff; 
border-top : 1px solid #ECF4FB; 
display : block; 
} 




div.row {float: left; margin:  0px 0px 10px 0px; padding: 0; width: 98%;}
div.col98 {float: left; width: 39%; margin: 0 3px 0 0; padding: 0;}
div.col59 {float: left; width: 59%; margin: 0 3px 0 0; padding: 0;}
div.col39 {float: left; width: 39%; margin: 0 3px 0 0; padding: 0;}
div.col30 {float: left; width: 39%; margin: 0 10px 0 0; padding-top:10px;}
div.col29 {float: left; width: 39%; margin: 0 3px 0 0; padding-top:10px;}






</style>
</asp:Content>












<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="dashboard_container">
 


<div id="second_row" class="row">
  <div id="second_row_left" class="col59">


    <div id="second_row_left_top" class="row">
      <div class="col59"> 

          
       
       <div class="divborder" style="text-align: left; width:500px; height:280px">
        <div class="dashboarddivheader">MONTHLY INCOME &amp; EXPENSE GRAPH</div>
        <div >
         
        </div>
       </div>
       
      
            
            <table class="padded-table" width="100%">
                 <tr><td>Total income for all the propertie</td><td>
                <asp:Label EnableTheming="false" Font-Size="14px"  ID="lbl_income" runat="server"></asp:Label>
                </td></tr>
                <tr><td>Total expense for all the properties</td><td>
                <asp:Label EnableTheming="false" Font-Size="14px"  ID="lbl_expense" runat="server"></asp:Label>
                </td></tr>
                <tr><td>Cash flow ( Income - Expense )</td><td>
                <asp:Label EnableTheming="false" Font-Size="14px"  ID="lbl_cash_flow" runat="server"></asp:Label>
                </td></tr>
            </table>
         
       
       
       
       
       
       
       
       
      </div>
    </div>



    <div id="second_row_left_bottom" class="row">
     <div id="second_row_left_bottom_left" class="col30"> 
     
      <div class="divborder" style="text-align: left; width: 100%;" >
        <div class="dashboarddivheader">Property</div>
         <table class="padded-table" width="100%">
            <tr><td><a href="/manager/property/property_list.aspx">Properties</a></td><td>
                <asp:Label EnableTheming="false" Font-Size="14px"  ID="lbl_home_count" runat="server" ></asp:Label></td></tr>
            <tr ><td>Units</td><td><asp:Label EnableTheming="false" Font-Size="14px"  ID="lbl_unit_count" runat="server" ></asp:Label></td></tr>
            <tr><td>Occupied units</td><td><asp:Label EnableTheming="false" Font-Size="14px"  ID="lbl_occ_unit_count" runat="server" ></asp:Label></td></tr>
            <tr ><td>Vacant units</td><td><asp:Label ID="lbl_vacant_unit_count" runat="server" ></asp:Label></td></tr>
         </table>
       </div>
      <br /><br />
      
      
       
    </div>
     
     
     
     
     
     <div id="second_row_left_bottom_right" class="col29"> 
     
      <div class="divborder" style="text-align: left; width: 100%;" >
        <div class="dashboarddivheader">USER</div>
         <table class="padded-table" width="100%">
            <tr><td >Tenants</td><td>
                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_tenant_count" runat="server"></asp:Label>
                </td></tr>
            <tr ><td>Owner</td><td>
                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_owner_count" runat="server"></asp:Label>
                </td></tr>
            <tr><td>Janitor</td><td>
                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_janitor_count" runat="server"></asp:Label>
                </td></tr>
            <tr><td>Property Manager</td><td>
                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_pm_count" runat="server"></asp:Label>
                </td></tr>
           </table>
        </div>
        <br /><br />
        
        <div class="divborder" style="text-align: left; width: 100%;" >
            <div class="dashboarddivheader">INVENTORY</div>
            <table class="padded-table" width="100%">
                <tr><td>Items in the properties</td><td>
                    <asp:Label  EnableTheming="false" Font-Size="14px" ID="lbl_home_item_count" runat="server"></asp:Label>
                    </td></tr>
                <tr><td>Items in external storage ( warehouse )</td><td>
                    <asp:Label  EnableTheming="false" Font-Size="14px" ID="lbl_warehouse_item_count" runat="server"></asp:Label>
                    </td></tr>
            </table>
        </div>
        
     
     </div>
    </div>


  </div>








  <div id="second_row_right" class="col39">
  
    <div class="divborder" style="text-align: left; width: 100%;" >
        <table class="padded-table" width="100%" bgcolor="#FFFFcc">
         <tr >
                <td >
                    <asp:Label  ID="Label6" Text="<%$ Resources:Resource, lbl_u_alerts %>" 
                                 runat="server" style="font-weight: 900; font-size: small"></asp:Label>
                         </td>
                    </tr>
                     <tr id="tr_lease_expiration" runat="server">
                            <td>
                                &nbsp;
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_lease_expiration" runat="server"></asp:Label>
                         &nbsp;<asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lbl_lease_expire %>"></asp:Label>
                                <asp:Label ID="lbl_as_lease_expiration" runat="server"></asp:Label>
                            &nbsp;<asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>" ></asp:Label>
                            </td>
                            
                        </tr>
                        <tr id="tr_lease_pending" runat="server">
                            <td>
                                &nbsp;
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_lease_pending" runat="server"></asp:Label>
                                &nbsp;<asp:Label ID="Label13" runat="server" Text="<%$ Resources:Resource, lbl_lease_pending %>"></asp:Label>
                                <asp:Label ID="lbl_as_lease_pending" runat="server"></asp:Label>
                                &nbsp;<asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>" ></asp:Label>
                                </td>
                            
                        </tr>
                        <tr id="tr_rent_delequency" runat="server">
                            <td>
                                &nbsp;
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_rent_delequency" runat="server"></asp:Label>
                              &nbsp;<asp:Label ID="Label14" runat="server" Text="<%$ Resources:Resource, lbl_late_rent_not_received %>"></asp:Label></td>
                            
                        </tr>
                        
                        <tr id="tr_wo_overdue" runat="server">
                            <td>
                                &nbsp;
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_wo_overdue" runat="server"></asp:Label>
                          &nbsp;<asp:Label ID="Label16" runat="server" Text="<%$ Resources:Resource, lbl_wo_overdue %>"></asp:Label></td>
                            
                        </tr>
                        <tr id="tr_wo_pending" runat="server">
                            <td>
                                &nbsp;
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_wo_pending" runat="server"></asp:Label>
                               &nbsp;<asp:Label ID="Label18" runat="server" Text="<%$ Resources:Resource, lbl_wo_pending%>"></asp:Label></td>
                            
                        </tr>
                        <tr id="tr_untreated_rent" runat="server">
                            <td>
                                &nbsp;
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_untreated_rent" runat="server"></asp:Label>
                          &nbsp;<asp:Label ID="Label17" runat="server" Text="<%$ Resources:Resource, lbl_alert_untreated_rent%>"></asp:Label></td>
                            
                        </tr>
                        <tr id="tr1" runat="server">
                            <td>
                                &nbsp;&nbsp; <asp:HyperLink Text="<%$ Resources:Resource, lbl_setup %>" 
                                    ID="HyperLink1" runat="server" NavigateUrl="~/manager/alerts/alerts_setup.aspx"></asp:HyperLink>
                                
                            &nbsp;&nbsp; <asp:HyperLink Text="<%$ Resources:Resource, lbl_details %>" 
                                    ID="HyperLink46" runat="server" 
                                    NavigateUrl="~/manager/alerts/alerts.aspx"></asp:HyperLink>                              
                            </td>                           
                        </tr>
                        <tr>
                            <td>
                                <br />

                            </td>                           
                        </tr>
               </table>
    </div>
       <br /><br />
  
  
  
  
  
  
  
      <div class="divborder" style="text-align: left; width: 100%;" >
        <div class="dashboarddivheader">DAILY TASK</div>
          <table class="padded-table" width="100%">
            <tr><td>Incident in the last 48 hours</td><td>
                <asp:Label  EnableTheming="false" ID="lbl_incident_last48hrs_count" Font-Size="14px" runat="server"></asp:Label>
                </td></tr>
            <tr><td><a href="/manager/incident/incident_list.aspx">Incident in the last 30 days</a></td><td>
                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_incident_last30days_count" runat="server"></asp:Label>
                </td></tr>
            <tr ><td><a href="/manager/workorder/wo_list.aspx">Open work order</a></td><td>
                <asp:Label  EnableTheming="false" ID="lbl_wo_count" Font-Size="14px" runat="server" ></asp:Label>
                </td></tr>
            <tr ><td><a href="/manager/Scheduler/default2.aspx">Calendar message +7 days</a></td><td>
                <asp:Label  EnableTheming="false" ID="lbl_event_within7days_count" Font-Size="14px" runat="server"></asp:Label>
                </td></tr>
         </table>
       </div>
       <br /><br />
       
       
      <div class="divborder" style="text-align: left; width: 100%;" >
          <table class="padded-table" width="100%">
            <tr><td  colspan="2" style="padding-left:10px;border-bottom : 3px solid #000000; background:url(../../App_Themes/SinfoTiger/images/grid_bg.png) repeat-x;"><b>MONTHLY RENT</b></td><td style="border-bottom : 3px solid #000000; background:url(../../App_Themes/SinfoTiger/images/grid_bg.png) repeat-x;">&nbsp;&nbsp; ( % )</td></tr>
            <tr><td><a href="/manager/property/property_list.aspx"># rents</a></td><td>
                <asp:Label   EnableTheming="false" Font-Size="14px" ID="lbl_lease_count2" runat="server"></asp:Label>
                </td><td>
                &nbsp; -&nbsp;</td></tr>
            <tr><td>Paid rent</td><td>
                <asp:Label  EnableTheming="false" Font-Size="14px"  ID="lbl_paid_rent_count" runat="server"></asp:Label>
                </td><td>
                    <asp:Label  EnableTheming="false" Font-Size="14px"  ID="lbl_paid_rent_count_percent" runat="server"></asp:Label>
                </td></tr>
            <tr><td>Unpaid rent</td><td>
                <asp:Label  EnableTheming="false" Font-Size="14px"  ID="lbl_unpaid_rent_count" runat="server"></asp:Label>
                </td><td>
                    <asp:Label  EnableTheming="false" Font-Size="14px"  ID="lbl_unpaid_rent_count_percent" runat="server"></asp:Label>
                </td></tr>
            <tr ><td>Partial payment </td><td>
                <asp:Label  EnableTheming="false" Font-Size="14px"  ID="lbl_partial_rent_count" runat="server"></asp:Label>
                </td><td>
                    <asp:Label  EnableTheming="false" Font-Size="14px"  ID="lbl_partial_rent_count_percent" runat="server"></asp:Label>
                </td></tr>
            </table>
       </div>
       <br /><br />
       <div class="divborder" style="text-align: left; width: 100%;" >
        <div class="dashboarddivheader">LEASE</div>
          <table class="padded-table" width="100%">
            <tr><td><a href="/manager/lease/lease_list.aspx">Current Lease</a></td><td><asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_lease_count" runat="server" ></asp:Label></td></tr>
             <tr ><td>Pending Lease</td><td><asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_pending_lease_count" runat="server" ></asp:Label></td></tr>
            <tr><td>Archived Lease</td><td><asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_archive_lease_count" runat="server" ></asp:Label></td></tr>
         </table>
       </div>
  
  
  </div>
  
  
  
</div> 

</div>



</asp:Content>

