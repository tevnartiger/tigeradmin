﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
/// <summary>
/// done by : Stanley Jocelyn
/// date    : may 17, 2009
/// </summary>
public partial class user_mp_property_manager : System.Web.UI.MasterPage
{

    //By doing so, each session is ensuring a unique view state key and thus preventing the 
    //Cross-Site Request Forgery attacks. You can read more about that here 
    // http://en.wikipedia.org/wiki/Cross-site_request_forgery.

    void Page_Init(object sender, EventArgs e)
    {
        Page.ViewStateUserKey = Session.SessionID;

    }

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["is_pm"].ToString() == "0")
        {
            link_contact.Visible = false;
            li_contact.Visible = false;
        }
    }
}
