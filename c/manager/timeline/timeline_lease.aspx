﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="timeline_lease.aspx.cs" Inherits="manager_timeline_timeline_lease" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <div id="Div1" runat="server"></div>
    <table>
        <tr>
            <td valign="top">
      
   <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_property %>"/> 
            </td>
            <td valign="top">
                :
                <asp:DropDownList AutoPostBack="true" DataTextField="home_name" DataValueField="home_id" ID="ddl_home_list2" runat="server" OnSelectedIndexChanged="ddl_home_list2_SelectedIndexChanged">
        </asp:DropDownList>
            &nbsp;</td>
            <td>
                &nbsp; </td>
            <td valign="top">
                <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_start_of_timeline %>"></asp:Label></td>
            <td valign="top">
                &nbsp;
                <asp:DropDownList ID="ddl_timeline_begin_m"  runat="server" >
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    / &nbsp;
                    <asp:DropDownList ID="ddl_timeline_begin_d" runat="server" >
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>&nbsp; / &nbsp;
                 <asp:DropDownList ID="ddl_timeline_begin_y" runat="server" >
                    
                    <asp:ListItem>1900</asp:ListItem>
                     <asp:ListItem>1901</asp:ListItem>
                     <asp:ListItem>1902</asp:ListItem>
                     <asp:ListItem>1903</asp:ListItem>
                     <asp:ListItem>1904</asp:ListItem>
                     <asp:ListItem>1905</asp:ListItem>
                     <asp:ListItem>1906</asp:ListItem>
                     <asp:ListItem>1907</asp:ListItem>
                     <asp:ListItem>1908</asp:ListItem>
                     <asp:ListItem>1909</asp:ListItem>
                     <asp:ListItem>1910</asp:ListItem>
                     <asp:ListItem>1911</asp:ListItem>
                     <asp:ListItem>1912</asp:ListItem>
                     <asp:ListItem>1913</asp:ListItem>
                     <asp:ListItem>1914</asp:ListItem>
                     <asp:ListItem>1915</asp:ListItem>
                     <asp:ListItem>1916</asp:ListItem>
                     <asp:ListItem>1917</asp:ListItem>
                     <asp:ListItem>1918</asp:ListItem>
                     <asp:ListItem>1919</asp:ListItem>
                     <asp:ListItem>1920</asp:ListItem>
                     <asp:ListItem>1921</asp:ListItem>
                     <asp:ListItem>1922</asp:ListItem>
                     <asp:ListItem>1923</asp:ListItem>
                     <asp:ListItem>1924</asp:ListItem>
                     <asp:ListItem>1925</asp:ListItem>
                     <asp:ListItem>1926</asp:ListItem>
                     <asp:ListItem>1927</asp:ListItem>
                     <asp:ListItem>1928</asp:ListItem>
                     <asp:ListItem>1929</asp:ListItem>
                     <asp:ListItem>1930</asp:ListItem>
                     <asp:ListItem>1931</asp:ListItem>
                     <asp:ListItem>1932</asp:ListItem>
                     <asp:ListItem>1933</asp:ListItem>
                     <asp:ListItem>1934</asp:ListItem>
                     <asp:ListItem>1935</asp:ListItem>
                     <asp:ListItem>1936</asp:ListItem>
                     <asp:ListItem>1937</asp:ListItem>
                     <asp:ListItem>1938</asp:ListItem>
                     <asp:ListItem>1939</asp:ListItem>
                     <asp:ListItem>1940</asp:ListItem>
                     <asp:ListItem>1941</asp:ListItem>
                     <asp:ListItem>1942</asp:ListItem>
                     <asp:ListItem>1943</asp:ListItem>
                     <asp:ListItem>1944</asp:ListItem>
                     <asp:ListItem>1945</asp:ListItem>
                     <asp:ListItem>1946</asp:ListItem>
                     <asp:ListItem>1947</asp:ListItem>
                     <asp:ListItem>1948</asp:ListItem>
                     <asp:ListItem>1949</asp:ListItem>
                     <asp:ListItem>1950</asp:ListItem>
                     <asp:ListItem>1951</asp:ListItem>
                     <asp:ListItem>1952</asp:ListItem>
                     <asp:ListItem>1953</asp:ListItem>
                     <asp:ListItem>1954</asp:ListItem>
                     <asp:ListItem>1955</asp:ListItem>
                     <asp:ListItem>1956</asp:ListItem>
                     <asp:ListItem>1957</asp:ListItem>
                     <asp:ListItem>1958</asp:ListItem>
                     <asp:ListItem>1959</asp:ListItem>
                     <asp:ListItem>1960</asp:ListItem>
                     <asp:ListItem>1961</asp:ListItem>
                     <asp:ListItem>1962</asp:ListItem>
                     <asp:ListItem>1963</asp:ListItem>
                     <asp:ListItem>1964</asp:ListItem>
                     <asp:ListItem>1965</asp:ListItem>
                     <asp:ListItem>1966</asp:ListItem>
                     <asp:ListItem>1967</asp:ListItem>
                     <asp:ListItem>1968</asp:ListItem>
                     <asp:ListItem>1969</asp:ListItem>
                     <asp:ListItem>1970</asp:ListItem>
                     <asp:ListItem>1972</asp:ListItem>
                     <asp:ListItem>1973</asp:ListItem>
                     <asp:ListItem>1974</asp:ListItem>
                     <asp:ListItem>1975</asp:ListItem>
                     <asp:ListItem>1976</asp:ListItem>
                     <asp:ListItem>1977</asp:ListItem>
                     <asp:ListItem>1978</asp:ListItem>
                     <asp:ListItem>1979</asp:ListItem>
                     <asp:ListItem>1980</asp:ListItem>
                     <asp:ListItem>1981</asp:ListItem>
                     <asp:ListItem>1982</asp:ListItem>
                     <asp:ListItem>1983</asp:ListItem>
                     <asp:ListItem>1984</asp:ListItem>
                     <asp:ListItem>1985</asp:ListItem>
                     <asp:ListItem>1986</asp:ListItem>
                     <asp:ListItem>1987</asp:ListItem>
                     <asp:ListItem>1988</asp:ListItem>
                     <asp:ListItem>1989</asp:ListItem>
                     <asp:ListItem>1990</asp:ListItem>
                     <asp:ListItem>1991</asp:ListItem>
                     <asp:ListItem>1992</asp:ListItem>
                     <asp:ListItem>1993</asp:ListItem>
                     <asp:ListItem>1994</asp:ListItem>
                     <asp:ListItem>1995</asp:ListItem>
                     <asp:ListItem>1996</asp:ListItem>
                     <asp:ListItem>1997</asp:ListItem>
                     <asp:ListItem>1998</asp:ListItem>
                     <asp:ListItem>1999</asp:ListItem>
                     <asp:ListItem>2000</asp:ListItem>    
                     <asp:ListItem>2001</asp:ListItem>
                     <asp:ListItem>2002</asp:ListItem>
                     <asp:ListItem>2003</asp:ListItem>
                     <asp:ListItem>2004</asp:ListItem>
                     <asp:ListItem>2005</asp:ListItem>
                     <asp:ListItem>2006</asp:ListItem>
                     <asp:ListItem>2007</asp:ListItem>
                     <asp:ListItem>2008</asp:ListItem>
                     <asp:ListItem>2009</asp:ListItem>
                     <asp:ListItem>2010</asp:ListItem>
                 </asp:DropDownList>
                 </td>
        </tr>
        <tr>
        
                  <td valign="top">
      <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lbl_unit %>"/>
                </td>
                <td valign="top">
                    :
        <asp:DropDownList ID="ddl_unit_id2" DataValueField="unit_id" DataTextField="unit_door_no"   runat="server" OnSelectedIndexChanged="ddl_unit_id2_SelectedIndexChanged" AutoPostBack="true" />
  
    
                </td>
                <td valign="top">
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    &nbsp;</td>
                <td valign="top">
                     <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lbl_end_of_timeline %>"></asp:Label>&nbsp;</td>
                <td valign="top">
                    &nbsp;
                    <asp:DropDownList ID="ddl_timeline_end_m"  runat="server" >
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    / &nbsp;
                    <asp:DropDownList ID="ddl_timeline_end_d" runat="server" >
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>&nbsp; / &nbsp;
                 <asp:DropDownList ID="ddl_timeline_end_y" runat="server" >
                     
                     
                     
                     <asp:ListItem>1900</asp:ListItem>
                     <asp:ListItem>1901</asp:ListItem>
                     <asp:ListItem>1902</asp:ListItem>
                     <asp:ListItem>1903</asp:ListItem>
                     <asp:ListItem>1904</asp:ListItem>
                     <asp:ListItem>1905</asp:ListItem>
                     <asp:ListItem>1906</asp:ListItem>
                     <asp:ListItem>1907</asp:ListItem>
                     <asp:ListItem>1908</asp:ListItem>
                     <asp:ListItem>1909</asp:ListItem>
                     <asp:ListItem>1910</asp:ListItem>
                     <asp:ListItem>1911</asp:ListItem>
                     <asp:ListItem>1912</asp:ListItem>
                     <asp:ListItem>1913</asp:ListItem>
                     <asp:ListItem>1914</asp:ListItem>
                     <asp:ListItem>1915</asp:ListItem>
                     <asp:ListItem>1916</asp:ListItem>
                     <asp:ListItem>1917</asp:ListItem>
                     <asp:ListItem>1918</asp:ListItem>
                     <asp:ListItem>1919</asp:ListItem>
                     <asp:ListItem>1920</asp:ListItem>
                     <asp:ListItem>1921</asp:ListItem>
                     <asp:ListItem>1922</asp:ListItem>
                     <asp:ListItem>1923</asp:ListItem>
                     <asp:ListItem>1924</asp:ListItem>
                     <asp:ListItem>1925</asp:ListItem>
                     <asp:ListItem>1926</asp:ListItem>
                     <asp:ListItem>1927</asp:ListItem>
                     <asp:ListItem>1928</asp:ListItem>
                     <asp:ListItem>1929</asp:ListItem>
                     <asp:ListItem>1930</asp:ListItem>
                     <asp:ListItem>1931</asp:ListItem>
                     <asp:ListItem>1932</asp:ListItem>
                     <asp:ListItem>1933</asp:ListItem>
                     <asp:ListItem>1934</asp:ListItem>
                     <asp:ListItem>1935</asp:ListItem>
                     <asp:ListItem>1936</asp:ListItem>
                     <asp:ListItem>1937</asp:ListItem>
                     <asp:ListItem>1938</asp:ListItem>
                     <asp:ListItem>1939</asp:ListItem>
                     <asp:ListItem>1940</asp:ListItem>
                     <asp:ListItem>1941</asp:ListItem>
                     <asp:ListItem>1942</asp:ListItem>
                     <asp:ListItem>1943</asp:ListItem>
                     <asp:ListItem>1944</asp:ListItem>
                     <asp:ListItem>1945</asp:ListItem>
                     <asp:ListItem>1946</asp:ListItem>
                     <asp:ListItem>1947</asp:ListItem>
                     <asp:ListItem>1948</asp:ListItem>
                     <asp:ListItem>1949</asp:ListItem>
                     <asp:ListItem>1950</asp:ListItem>
                     <asp:ListItem>1951</asp:ListItem>
                     <asp:ListItem>1952</asp:ListItem>
                     <asp:ListItem>1953</asp:ListItem>
                     <asp:ListItem>1954</asp:ListItem>
                     <asp:ListItem>1955</asp:ListItem>
                     <asp:ListItem>1956</asp:ListItem>
                     <asp:ListItem>1957</asp:ListItem>
                     <asp:ListItem>1958</asp:ListItem>
                     <asp:ListItem>1959</asp:ListItem>
                     <asp:ListItem>1960</asp:ListItem>
                     <asp:ListItem>1961</asp:ListItem>
                     <asp:ListItem>1962</asp:ListItem>
                     <asp:ListItem>1963</asp:ListItem>
                     <asp:ListItem>1964</asp:ListItem>
                     <asp:ListItem>1965</asp:ListItem>
                     <asp:ListItem>1966</asp:ListItem>
                     <asp:ListItem>1967</asp:ListItem>
                     <asp:ListItem>1968</asp:ListItem>
                     <asp:ListItem>1969</asp:ListItem>
                     <asp:ListItem>1970</asp:ListItem>
                     <asp:ListItem>1972</asp:ListItem>
                     <asp:ListItem>1973</asp:ListItem>
                     <asp:ListItem>1974</asp:ListItem>
                     <asp:ListItem>1975</asp:ListItem>
                     <asp:ListItem>1976</asp:ListItem>
                     <asp:ListItem>1977</asp:ListItem>
                     <asp:ListItem>1978</asp:ListItem>
                     <asp:ListItem>1979</asp:ListItem>
                     <asp:ListItem>1980</asp:ListItem>
                     <asp:ListItem>1981</asp:ListItem>
                     <asp:ListItem>1982</asp:ListItem>
                     <asp:ListItem>1983</asp:ListItem>
                     <asp:ListItem>1984</asp:ListItem>
                     <asp:ListItem>1985</asp:ListItem>
                     <asp:ListItem>1986</asp:ListItem>
                     <asp:ListItem>1987</asp:ListItem>
                     <asp:ListItem>1988</asp:ListItem>
                     <asp:ListItem>1989</asp:ListItem>
                     <asp:ListItem>1990</asp:ListItem>
                     <asp:ListItem>1991</asp:ListItem>
                     <asp:ListItem>1992</asp:ListItem>
                     <asp:ListItem>1993</asp:ListItem>
                     <asp:ListItem>1994</asp:ListItem>
                     <asp:ListItem>1995</asp:ListItem>
                     <asp:ListItem>1996</asp:ListItem>
                     <asp:ListItem>1997</asp:ListItem>
                     <asp:ListItem>1998</asp:ListItem>
                     <asp:ListItem>1999</asp:ListItem>
                     <asp:ListItem>2000</asp:ListItem>
                     <asp:ListItem>2001</asp:ListItem>
                     <asp:ListItem>2002</asp:ListItem>
                     <asp:ListItem>2003</asp:ListItem>
                     <asp:ListItem>2004</asp:ListItem>
                     <asp:ListItem>2005</asp:ListItem>
                     <asp:ListItem>2006</asp:ListItem>
                     <asp:ListItem>2007</asp:ListItem>
                     <asp:ListItem>2008</asp:ListItem>
                     <asp:ListItem>2009</asp:ListItem>
                     <asp:ListItem>2010</asp:ListItem>
                 </asp:DropDownList>
                
                    </td>
        </tr>
        <tr>
        
                  <td valign="top">
                      &nbsp;</td>
                <td valign="top">
                    &nbsp;</td>
                <td valign="top">
                    &nbsp;</td>
                <td valign="top">
                    &nbsp;</td>
                <td valign="top">
                    &nbsp;&nbsp;
                    <asp:Button ID="btn_view" runat="server" Text="View" onclick="btn_view_Click" />
                
                    </td>
        </tr>
        </table>     
    
    <br />
    
    
   
 
    <asp:HiddenField ID="HiddenField1" Value="0" runat="server" />
    
     
     
     
     <br />
     
     
        <asp:Chart  ID="Chart1" runat="server" >
            <Series>
                <asp:Series   Name="Series1"  YValuesPerPoint="2" XValueType="String" YValueType="DateTime"  >
                </asp:Series>
            </Series>
            <ChartAreas>
                <asp:ChartArea  Name="ChartArea1">
                
                <axisy LineColor="64, 64, 64, 64"   IsStartedFromZero="False">
					<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
					<MajorGrid LineColor="64, 64, 64, 64" />
				</axisy>
				<axisx  LineColor="64, 64, 64, 64">
					<LabelStyle  Font="Trebuchet MS, 8.25pt, style=Bold" IsEndLabelVisible="False" />
					<MajorGrid LineColor="64, 64, 64, 64" />
				
         
				</axisx>
                </asp:ChartArea>                      
            </ChartAreas>
        </asp:Chart>
   

</asp:Content>

