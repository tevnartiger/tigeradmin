﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="lease_expiration.aspx.cs" Inherits="manager_lease_lease_expiration" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<b><asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lbl_u_lease_expiration%>"/> </b>
               <br />
<br />
    <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
<table>
    
 <tr>
     <td  >
       <asp:Label ID="lbl_property" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_property %>" style="font-weight: 700" />
      </td>
         <td  >
                               :
                               <asp:DropDownList ID="ddl_home_list" runat="server" AutoPostBack="true" 
                                   DataTextField="home_name" DataValueField="home_id" 
                                   OnSelectedIndexChanged="ddl_home_list_SelectedIndexChanged">
                               </asp:DropDownList>
                               &nbsp;</td>
                               
        
   </tr>
                      
</table>



 <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b>
                <asp:Label ID="Label42" runat="server" Text="<%$ Resources:Resource, lbl_less31%>"/></b>
                </td>
            </tr>
        </table>
     
<br />

 <asp:GridView ID="gv_lease_list1" 
    runat="server" AutoGenerateColumns="false" GridLines="Both"  
       AlternatingRowStyle-BackColor="#F0F0F6" 
      BorderColor="#6aa4d4"  BorderWidth="1"  Width="100%"
       
      HeaderStyle-CssClass="ob_gHCont"
       AllowPaging="true"  PageSize="10"  OnPageIndexChanging="gv_lease_list1_PageIndexChanging"
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" >
    
    <Columns>
    
    <asp:BoundField DataField="home_name"  HeaderText="<%$ Resources:Resource, lbl_home_name %>" />
    
   <asp:BoundField DataField="unit_door_no"  HeaderText="<%$ Resources:Resource, lbl_door_number %>" />
    <asp:BoundField DataField="tu_date_begin" DataFormatString="{0:M-dd-yyyy}"  HtmlEncode="false" HeaderText="<%$ Resources:Resource, lbl_date_begin %>" />
   
    <asp:BoundField DataField="tu_date_end" DataFormatString="{0:M-dd-yyyy}"  HtmlEncode="false"  HeaderText="<%$ Resources:Resource, lbl_date_end %>"/>
    
   <asp:BoundField DataField="amount_days_left" HeaderText="<%$ Resources:Resource, lbl_days %>" />
    
   
     <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_view %>"
     DataNavigateUrlFields="tu_id,unit_id,home_id" 
     DataNavigateUrlFormatString="~/manager/lease/lease_view.aspx?tu_id={0}&unit_id={1}&h_id={2}" 
      HeaderText="<%$ Resources:Resource, gv_view_detail %>" />
    </Columns>
    </asp:GridView>
    
    
    <br />
   <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b>
                <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_between_31and60%>"/></b>
                </td>
            </tr>
        </table>
     
  <br />

    
     <asp:GridView ID="gv_lease_list2" 
    runat="server" AutoGenerateColumns="false" GridLines="Both"  
        AlternatingRowStyle-BackColor="#F0F0F6" 
      BorderColor="#CDCDCD"  BorderWidth="1"  Width="100%"
        HeaderStyle-CssClass="ob_gHCont"
       AllowPaging="true"  PageSize="10"  OnPageIndexChanging="gv_lease_list2_PageIndexChanging"
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" >
    
    <Columns>
    
        
    <asp:BoundField DataField="home_name"  HeaderText="<%$ Resources:Resource, lbl_home_name %>" />
    
     <asp:BoundField DataField="unit_door_no"  HeaderText="<%$ Resources:Resource, lbl_door_number %>" />
  

   <asp:BoundField DataField="tu_date_begin" DataFormatString="{0:M-dd-yyyy}"  HtmlEncode="false" HeaderText="<%$ Resources:Resource, lbl_date_begin %>" />
    <asp:BoundField DataField="tu_date_end" DataFormatString="{0:M-dd-yyyy}"  HtmlEncode="false"  HeaderText="<%$ Resources:Resource, lbl_date_end %>"/>
    
   <asp:BoundField DataField="amount_days_left" HeaderText="<%$ Resources:Resource, lbl_days %>" />
    
   
    <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_view %>"
     DataNavigateUrlFields="tu_id,unit_id,home_id" 
     DataNavigateUrlFormatString="~/manager/lease/lease_view.aspx?tu_id={0}&unit_id={1}&h_id={2}" 
      HeaderText="<%$ Resources:Resource, gv_view_detail %>" />
    </Columns>
    </asp:GridView>
    
    <br />
    
    
     <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b>
                <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_between_61and90%>"/></b>
                </td>
            </tr>
        </table>
     
<br />

     <asp:GridView ID="gv_lease_list3" 
    runat="server" AutoGenerateColumns="false" GridLines="Both"  
       AlternatingRowStyle-BackColor="#F0F0F6" 
      BorderColor="#CDCDCD"  BorderWidth="1"  Width="100%"
       HeaderStyle-CssClass="ob_gHCont"
       AllowPaging="true"  PageSize="10"  OnPageIndexChanging="gv_lease_list3_PageIndexChanging"
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" >
    
    <Columns>
    
    
    <asp:BoundField DataField="home_name"  HeaderText="<%$ Resources:Resource, lbl_home_name %>" />
    
     <asp:BoundField DataField="unit_door_no"  HeaderText="<%$ Resources:Resource, lbl_door_number %>" />
  

   <asp:BoundField DataField="tu_date_begin" DataFormatString="{0:M-dd-yyyy}"  HtmlEncode="false" HeaderText="<%$ Resources:Resource, lbl_date_begin %>" />
    <asp:BoundField DataField="tu_date_end" DataFormatString="{0:M-dd-yyyy}"  HtmlEncode="false"  HeaderText="<%$ Resources:Resource, lbl_date_end %>"/>
    
   <asp:BoundField DataField="amount_days_left" HeaderText="<%$ Resources:Resource, lbl_days %>" />
    
   
    <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_view %>"
     DataNavigateUrlFields="tu_id,unit_id,home_id" 
     DataNavigateUrlFormatString="~/manager/lease/lease_view.aspx?tu_id={0}&unit_id={1}&h_id={2}" 
      HeaderText="<%$ Resources:Resource, gv_view_detail %>" />
    </Columns>
    </asp:GridView>
    
    
    <br />
    
    <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label>

</asp:Content>

