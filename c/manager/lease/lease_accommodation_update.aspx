<%@ Page Language="C#" MasterPageFile="../mp_manager.master" MaintainScrollPositionOnPostback="true"  AutoEventWireup="true" CodeFile="lease_accommodation_update.aspx.cs" Inherits="home_lease_accommodation_update" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
    NEW ACCOMODATIONS <br /><br />
    
  <cc1:TabContainer Width="80%" ID="TabContainer1"  runat="server">
   <cc1:TabPanel ID="tab1" runat="server"  HeaderText="RESIDENTIAL" >
       <HeaderTemplate>
           RESIDENTIAL
       </HeaderTemplate>
    <ContentTemplate  >
      
    <table>
    <asp:Panel ID="panel_add_home" runat="server" >
   <tr>
   <td>
   <strong><span style="font-size: 12pt">Home</span></strong> :
   <asp:DropDownList ID="ddl_home_id" DataValueField="home_id" 
           DataTextField="home_name"   runat="server" autopostback="True" 
           OnSelectedIndexChanged="ddl_home_id_SelectedIndexChanged" />
   </td>
   </tr>
   </asp:Panel>
   
   <tr><td><div id="txt_pending" runat="server"></div></td></tr>
   
   
   
   <tr>
    <td>
         
   <asp:Repeater ID="r_pendingaccommodationlist"   runat="server">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
            <tr>
                <td>
                   <b>Pending accommodation date begin</b> </td>
                <td >
                    : 
                    <%#DataBinder.Eval(Container.DataItem, "ta_date_begin")%>
                    <input type="hidden" id="hd_pending_ta_date_begin" value =<%#DataBinder.Eval(Container.DataItem, "ta_date_begin")%> runat="server" />
                    
               </td>
            </tr>
         </table>
        </ItemTemplate>
        
    </asp:Repeater>
         
         
         
         
         
    </td>
   </tr>
   
   <asp:Panel ID="panel_add_unit"  runat="server" >
   <tr>
   <td>
   <strong><span style="font-size: 12pt">Unit</span></strong> &nbsp;&nbsp;&nbsp;&nbsp;:
   <asp:DropDownList ID="ddl_unit_id" DataValueField="unit_id" 
           DataTextField="unit_door_no"   runat="server" 
           OnSelectedIndexChanged="ddl_unit_id_SelectedIndexChanged" AutoPostBack="True" />
   <br /><br /><br />
   </td>
   </tr>
   </asp:Panel>
  </table>   
  
<asp:Panel ID=panel_accommodation_update runat=server>
<asp:Panel ID="panel_current_tenant"  Visible="False" runat="server">   

<table>
<tr>
<td><div id="txt_current_tenant_name" runat="server" />
</td>
</tr>

</table>

<hr />


</asp:Panel>







<table>

 <tr>
 <td>
     Accommodation Since &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; :
 <asp:Label ID="lbl_current_ta_date_begin" runat="server"></asp:Label>
</td>
 </tr>
</table>
   
  <hr />
     
           <table>
         
           <tr>
 <td valign="top">
 new accommodation begins &nbsp;(mm / dd / yyyy) &nbsp;&nbsp;: &nbsp;
<asp:DropDownList ID="ddl_ta_date_begin_m" runat="server">
<asp:ListItem Value="0">Month</asp:ListItem>
<asp:ListItem Value="1">January</asp:ListItem>
<asp:ListItem Value="2">February</asp:ListItem>
<asp:ListItem Value="3">March</asp:ListItem>
<asp:ListItem Value="4">April</asp:ListItem>
<asp:ListItem Value="5">May</asp:ListItem>
<asp:ListItem Value="6">June</asp:ListItem>
<asp:ListItem Value="7">July</asp:ListItem>
<asp:ListItem Value="8">August</asp:ListItem>
<asp:ListItem Value="9">September</asp:ListItem>
<asp:ListItem Value="10">October</asp:ListItem>
<asp:ListItem Value="11">November</asp:ListItem>
<asp:ListItem Value="12">December</asp:ListItem>
</asp:DropDownList>&nbsp; / &nbsp;
<asp:DropDownList ID="ddl_ta_date_begin_d" runat="server">
    <asp:ListItem Value="0">Day</asp:ListItem>
    <asp:ListItem>1</asp:ListItem>
    <asp:ListItem>2</asp:ListItem>
    <asp:ListItem>3</asp:ListItem>
    <asp:ListItem>4</asp:ListItem>
    <asp:ListItem>5</asp:ListItem>
    <asp:ListItem>6</asp:ListItem>
    <asp:ListItem>7</asp:ListItem>
    <asp:ListItem>8</asp:ListItem>
    <asp:ListItem>9</asp:ListItem>
    <asp:ListItem>10</asp:ListItem>
    <asp:ListItem>11</asp:ListItem>
    <asp:ListItem>12</asp:ListItem>
    <asp:ListItem>13</asp:ListItem>
    <asp:ListItem>14</asp:ListItem>
    <asp:ListItem>15</asp:ListItem>
    <asp:ListItem>16</asp:ListItem>
    <asp:ListItem>17</asp:ListItem>
    <asp:ListItem>18</asp:ListItem>
    <asp:ListItem>19</asp:ListItem>
    <asp:ListItem>20</asp:ListItem>
    <asp:ListItem>21</asp:ListItem>
    <asp:ListItem>22</asp:ListItem>
    <asp:ListItem>23</asp:ListItem>
    <asp:ListItem>24</asp:ListItem>
    <asp:ListItem>25</asp:ListItem>
    <asp:ListItem>26</asp:ListItem>
    <asp:ListItem>27</asp:ListItem>
    <asp:ListItem>28</asp:ListItem>
    <asp:ListItem>29</asp:ListItem>
    <asp:ListItem>30</asp:ListItem>
    <asp:ListItem>31</asp:ListItem>

</asp:DropDownList>&nbsp; / &nbsp;
 
 <asp:DropDownList ID="ddl_ta_date_begin_y" runat="server">
<asp:ListItem Value="0">Year</asp:ListItem>
<asp:ListItem>2007</asp:ListItem>
<asp:ListItem>2008</asp:ListItem>
<asp:ListItem>2009</asp:ListItem>
<asp:ListItem>2010</asp:ListItem>
<asp:ListItem>2011</asp:ListItem>
<asp:ListItem>2012</asp:ListItem>
<asp:ListItem>2013</asp:ListItem>
<asp:ListItem>2014</asp:ListItem>
<asp:ListItem>2015</asp:ListItem>
</asp:DropDownList> </td>
 </tr> 
           
       
               <tr>
                   <td bgcolor="#FFFFCC">
                       <b>Minnium begin date of new accommodation must be greater than :
                       <asp:Label ID="lbl_current_ta_date_begin1" runat="server"></asp:Label>
                       </b>
                   </td>
               </tr>
           
       
          </table>
          <table>
     
     
     <tr>
     <td colspan="2">
         &nbsp;</td></tr>
     
         
              <tr>
                  <td colspan="2">
                      <strong><span style="font-size: 10pt">Utilities includes</span></strong><br />
                      <asp:CheckBox ID="ta_electricity_inc" runat="server" AutoPostBack="True" 
                          CssClass="letter" OnCheckedChanged="ta_electricity_inc_CheckedChanged" 
                          text="Electricity" />
                      <br />
                      <asp:CheckBox ID="ta_heat_inc" runat="server" AutoPostBack="True" 
                          CssClass="letter" OnCheckedChanged="ta_heat_inc_CheckedChanged" 
                          text="Heat" />
                      <br />
                      <asp:CheckBox ID="ta_water_inc" runat="server" AutoPostBack="True" 
                          CssClass="letter" OnCheckedChanged="ta_water_inc_CheckedChanged" 
                          text="Water" />
                      <br />
                      <asp:CheckBox ID="ta_water_tax_inc" runat="server" AutoPostBack="True" 
                          CssClass="letter" OnCheckedChanged="ta_water_tax_inc_CheckedChanged" 
                          text="Water tax" />
                      <br />
                      <asp:CheckBox ID="ta_none" runat="server" AutoPostBack="True" CssClass="letter" 
                          OnCheckedChanged="ta_none_CheckedChanged" 
                          text="None ( utilities are not included in the lease )" />
                      <br />
                      <br />
                      <span style="font-size: 10pt"><strong>Accomodations included</strong></span><br />
                      <asp:CheckBox ID="ta_parking_inc" runat="server" CssClass="letter" 
                          text="Parking" />
                      <br />
                      <asp:CheckBox ID="ta_garage_inc" runat="server" CssClass="letter" 
                          text="Garage" />
                      <br />
                      <asp:CheckBox ID="ta_furnished_inc" runat="server" CssClass="letter" 
                          text="Furnished" />
                      <br />
                      <asp:CheckBox ID="ta_semi_furnished_inc" runat="server" CssClass="letter" 
                          text="Semi-furnished" />
                      <br />
                  </td>
              </tr>
     
         
           <tr><td class="letter_bold" valign=top>Comments & extras</td>
                <td><asp:TextBox ID="ta_com" CssClass="letter" runat="server" TextMode="MultiLine" Width="389px"/>
                    <asp:RegularExpressionValidator ID="reg_ta_com"
                         ValidationGroup="vg_res"
                            runat="server"  ControlToValidate="ta_com"
                             ErrorMessage="invalid text"></asp:RegularExpressionValidator>
               </td>
            </tr>
       
      <tr><td colspan="2">
      <asp:Button ID="btn_continue" runat="server" 
         ValidationGroup="vg_res"
      OnClick="btn_continue_OnClick" Text="Continue" /></td></tr>
    <tr><td colspan="2">
     
      
     </td></tr>
          <tr><td><div ID="txt_link" runat="server" /></td></tr>
         <tr><td>
         </td></tr>
         
     </table>
    </asp:Panel>      
    
    <div id="txt_message" runat="server" />
    
    <asp:HiddenField Visible="False" ID="hd_min_begin_date_m" runat="server" />
             <asp:HiddenField Visible="False" ID="hd_min_begin_date_d" runat="server" />
             <asp:HiddenField Visible="False" ID="hd_min_begin_date_y" runat="server" />
  
    <!--Hidden fields BEGIN SECTION 1-->
         <input type="hidden" id="hd_home_id" runat="server" >







             

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             <input id="hd_unit_id" runat="server" type="hidden"></input>








&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             <input id="hd_current_tu_id" runat="server" type="hidden"></input>








&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             <!--Hidden fields END SECTION 1-->
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             <!--Hidden fields temporaire-->
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             <input id="didden" runat="server" type="hidden"></input></input></input></input></input>








&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
             
            
            
            
            
            
            
</ContentTemplate  ></cc1:TabPanel><cc1:TabPanel ID="tab2" runat="server"  HeaderText="COMMERCIAL" >
    <ContentTemplate  >
    
    <table>
    <asp:Panel ID="panel_add_home_2" runat="server" >
   <tr>
   <td>
   <strong><span style="font-size: 12pt">Home</span></strong> :
   <asp:DropDownList ID="ddl_home_id_2" DataValueField="home_id" DataTextField="home_name"   runat="server" autopostback="true" OnSelectedIndexChanged="ddl_home_id_2_SelectedIndexChanged" />
   </td>
   </tr>
   </asp:Panel>
   
   <tr><td><div id="txt_pending_2" runat="server"></div></td></tr>
   
   
   
   <tr>
    <td>
         
   <asp:Repeater ID="r_pendingaccommodationlist_2"   runat="server">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
            <tr>
                <td>
                   <b>Pending accommodation date begin</b> </td>
                <td >
                    : 
                    <%#DataBinder.Eval(Container.DataItem, "ta_date_begin")%>
                    <input type="hidden" id="hd_pending_ta_date_begin_2" value =<%#DataBinder.Eval(Container.DataItem, "ta_date_begin")%> runat="server" />
                    
               </td>
            </tr>
         </table>
        </ItemTemplate>
        
    </asp:Repeater>
         
         
         
         
         
    </td>
   </tr>
   
   <asp:Panel ID="panel_add_unit_2"  runat="server" >
   <tr>
   <td>
   <strong><span style="font-size: 12pt">Unit</span></strong> &nbsp;&nbsp;&nbsp;&nbsp;:
   <asp:DropDownList ID="ddl_unit_id_2" DataValueField="unit_id" DataTextField="unit_door_no"   runat="server" OnSelectedIndexChanged="ddl_unit_id_2_SelectedIndexChanged" AutoPostBack="true" />
   <br /><br />
   </td>
   </tr>
   </asp:Panel>
  </table>   
  
  
    
  
<asp:Panel ID="panel_accommodation_update_2" runat="server">
<asp:Panel ID="panel_current_tenant_2"  Visible="false" runat="server">   
<br />
  <table >
        <tr id="tr_company" runat="server">
            <td>
               <b> Company</b></td><td>
               &nbsp;:&nbsp;&nbsp; <asp:Label ID="lbl_company" runat="server" ></asp:Label></td></tr></table><br />
<table>
<tr>
<td><div id="txt_current_tenant_name_2" runat="server" />
</td>
</tr>

</table>

<hr />


</asp:Panel>







<table>

 <tr>
 <td>
     Accommodation Since &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; :
 <asp:Label ID="lbl_current_ta_date_begin2" runat="server"></asp:Label></td></tr></table><hr />
     
           <table>
         
           <tr>
 <td valign="top">
 new accommodation begins &nbsp;(mm / dd / yyyy) &nbsp;&nbsp;: &nbsp;
<asp:DropDownList ID="ddl_ta_date_begin_m2" runat="server">
<asp:ListItem Value="0">Month</asp:ListItem><asp:ListItem Value="1">January</asp:ListItem><asp:ListItem Value="2">February</asp:ListItem><asp:ListItem Value="3">March</asp:ListItem><asp:ListItem Value="4">April</asp:ListItem><asp:ListItem Value="5">May</asp:ListItem><asp:ListItem Value="6">June</asp:ListItem><asp:ListItem Value="7">July</asp:ListItem><asp:ListItem Value="8">August</asp:ListItem><asp:ListItem Value="9">September</asp:ListItem><asp:ListItem Value="10">October</asp:ListItem><asp:ListItem Value="11">November</asp:ListItem><asp:ListItem Value="12">December</asp:ListItem></asp:DropDownList>&nbsp; / &nbsp;
<asp:DropDownList ID="ddl_ta_date_begin_d2" runat="server">
    <asp:ListItem Value="0">Day</asp:ListItem><asp:ListItem>1</asp:ListItem><asp:ListItem>2</asp:ListItem><asp:ListItem>3</asp:ListItem><asp:ListItem>4</asp:ListItem><asp:ListItem>5</asp:ListItem><asp:ListItem>6</asp:ListItem><asp:ListItem>7</asp:ListItem><asp:ListItem>8</asp:ListItem><asp:ListItem>9</asp:ListItem><asp:ListItem>10</asp:ListItem><asp:ListItem>11</asp:ListItem><asp:ListItem>12</asp:ListItem><asp:ListItem>13</asp:ListItem><asp:ListItem>14</asp:ListItem><asp:ListItem>15</asp:ListItem><asp:ListItem>16</asp:ListItem><asp:ListItem>17</asp:ListItem><asp:ListItem>18</asp:ListItem><asp:ListItem>19</asp:ListItem><asp:ListItem>20</asp:ListItem><asp:ListItem>21</asp:ListItem><asp:ListItem>22</asp:ListItem><asp:ListItem>23</asp:ListItem><asp:ListItem>24</asp:ListItem><asp:ListItem>25</asp:ListItem><asp:ListItem>26</asp:ListItem><asp:ListItem>27</asp:ListItem><asp:ListItem>28</asp:ListItem><asp:ListItem>29</asp:ListItem><asp:ListItem>30</asp:ListItem><asp:ListItem>31</asp:ListItem></asp:DropDownList>&nbsp; / &nbsp;
 
 <asp:DropDownList ID="ddl_ta_date_begin_y2" runat="server">
<asp:ListItem Value="0">Year</asp:ListItem><asp:ListItem>2007</asp:ListItem><asp:ListItem>2008</asp:ListItem><asp:ListItem>2009</asp:ListItem><asp:ListItem>2010</asp:ListItem><asp:ListItem>2011</asp:ListItem><asp:ListItem>2012</asp:ListItem><asp:ListItem>2013</asp:ListItem><asp:ListItem>2014</asp:ListItem><asp:ListItem>2015</asp:ListItem></asp:DropDownList></td></tr><tr>
                   <td bgcolor="#FFFFCC">
                       <b>Minium begin date of new accommodation must be greater than :
                       <asp:Label ID="lbl_current_ta_date_begin3" runat="server"></asp:Label></b></td></tr></table><table>
     
     
     <tr>
     <td colspan="2">
         &nbsp;</td></tr><tr>
                  <td colspan="2">
                      <strong><span style="font-size: 10pt">Utilities includes</span></strong><br />
                      <asp:CheckBox ID="ta_electricity_inc_2" runat="server" AutoPostBack="true" 
                          CssClass="letter" OnCheckedChanged="ta_electricity_inc_2_CheckedChanged" 
                          text="Electricity" />
                      <br />
                      <asp:CheckBox ID="ta_heat_inc_2" runat="server" AutoPostBack="true" 
                          CssClass="letter" OnCheckedChanged="ta_heat_inc_2_CheckedChanged" text="Heat" />
                      <br />
                      <asp:CheckBox ID="ta_water_inc_2" runat="server" AutoPostBack="true" 
                          CssClass="letter" OnCheckedChanged="ta_water_inc_2_CheckedChanged" text="Water" />
                      <br />
                      <asp:CheckBox ID="ta_water_tax_inc_2" runat="server" AutoPostBack="true" 
                          CssClass="letter" OnCheckedChanged="ta_water_tax_inc_2_CheckedChanged" 
                          text="Water tax" />
                      <br />
                      <asp:CheckBox ID="ta_none_2" runat="server" AutoPostBack="true" CssClass="letter" 
                          OnCheckedChanged="ta_none_2_CheckedChanged" 
                          text="None ( utilities are not included in the lease )" />
                      <br />
                      <br />
                      <span style="font-size: 10pt"><strong>Accomodations included</strong></span><br />
                      <asp:CheckBox ID="ta_parking_inc_2" runat="server" CssClass="letter" 
                          text="Parking" />
                      <br />
                      <asp:CheckBox ID="ta_garage_inc_2" runat="server" CssClass="letter" 
                          text="Garage" />
                      <br />
                      <asp:CheckBox ID="ta_furnished_inc_2" runat="server" CssClass="letter" 
                          text="Furnished" />
                      <br />
                      <asp:CheckBox ID="ta_semi_furnished_inc_2" runat="server" CssClass="letter" 
                          text="Semi-furnished" />
                      <br />
                  </td>
              </tr>
     
         
           <tr><td class="letter_bold" valign=top>Comments & extras</td><td><asp:TextBox ID="ta_com_2" CssClass="letter" runat="server" TextMode="MultiLine" Width="389px"/>
           <asp:RegularExpressionValidator ID="reg_ta_com_2"
                            ValidationGroup="vg_com"
                            runat="server"  ControlToValidate="ta_com_2"
                             ErrorMessage="invalid text">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                            
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                            
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                            
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </asp:RegularExpressionValidator></td>
            </tr>
       
      <tr><td colspan="2"><asp:Button ID="btn_continue_2" runat="server"
                              ValidationGroup="vg_com"
                          OnClick="btn_continue_2_OnClick" Text="Continue" /></td></tr>
    <tr><td colspan="2">
     
      
     </td></tr>
          <tr><td><div ID="txt_link_2" runat="server" /></td></tr>
         <tr><td>
         </td></tr>
         
     </table>
    </asp:Panel>      
    
    <div id="txt_message_2" runat="server" />
    
    <asp:HiddenField Visible="false" ID="hd_min_begin_date_m2" runat="server" />
             <asp:HiddenField Visible="false" ID="hd_min_begin_date_d2" runat="server" />
             <asp:HiddenField Visible="false" ID="hd_min_begin_date_y2" runat="server" />
  
    <!--Hidden fields BEGIN SECTION 1-->
         <input type="hidden" id="hd_home_id_2" runat="server" />
         <input type="hidden" id="hd_unit_id_2" runat="server" />
         <input type="hidden" id="hd_current_tu_id_2" runat="server" />
         <!--Hidden fields END SECTION 1-->
           <!--Hidden fields temporaire-->
          <input type="hidden" id="Hidden1" runat="server" />
    
    </ContentTemplate  >
  </cc1:TabPanel>
  
  
  
  
   </cc1:TabContainer>
  
           
</asp:Content>