<%@ Page Language="C#" MasterPageFile="../mp_manager.master" MaintainScrollPositionOnPostback="true"  AutoEventWireup="true" CodeFile="lease_term_cond_update.aspx.cs" Inherits="home_lease_term_cond_update"   %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
 NEW TERMS AND CONDITIONS <br /><br />

 <cc1:TabContainer Width="80%" ID="TabContainer1"  runat="server" >
   <cc1:TabPanel ID="tab1" runat="server"  HeaderText="RESIDENTIAL" >
    <ContentTemplate  >
       
  <table>
    <asp:Panel ID="panel_add_home" runat="server" BackColor="#FFFFCC" >
   <tr>
   <td>
   <strong><span style="font-size: 12pt">Home</span></strong> :
   <asp:DropDownList ID="ddl_home_id" DataValueField="home_id" 
           DataTextField="home_name"   runat="server" autopostback="True" 
           OnSelectedIndexChanged="ddl_home_id_SelectedIndexChanged" />
   </td>
   </tr>
   </asp:Panel>
   
   
   
   <tr><td><div id="txt_pending" runat="server"></div></td></tr>
   
   
   
   <tr>
    <td>
     <asp:Repeater ID="r_pendingtermsandconditionslist"    runat="server" 
           >
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
            <tr>
                <td>
                   <b>Pending terms date begin</b> </td>
                <td >
                    : 
                    <%#DataBinder.Eval(Container.DataItem, "tt_date_begin")%>
                    <asp:HiddenField Visible="false" id="hd_pending_tt_date_begin" value ='<%#DataBinder.Eval(Container.DataItem, "tt_date_begin")%>' runat="server" />
                    
               </td>
            </tr>
         </table>
        </ItemTemplate>
        
    </asp:Repeater>
    </td>
   </tr>
   
   
   <asp:Panel ID="panel_add_unit"  runat="server" >
   <tr>
   <td>
   <strong><span style="font-size: 12pt">Unit</span></strong> &nbsp;&nbsp;&nbsp;&nbsp;:
   <asp:DropDownList ID="ddl_unit_id" DataValueField="unit_id" 
           DataTextField="unit_door_no"   runat="server" 
           OnSelectedIndexChanged="ddl_unit_id_SelectedIndexChanged" AutoPostBack="True" />
   <br /><br /><br />
   
   </td>
   </tr>
   </asp:Panel>
  </table>  
  
  <asp:Panel ID=panel_term_cond_update runat=server>
  <hr />
  
  
  <asp:Panel ID="panel_current_tenant"  Visible="False" runat="server">   

<table>
<tr>
<td><div id="txt_current_tenant_name" runat="server" />
</td>
</tr>

</table>
<hr />


</asp:Panel>
  
  <table>
  <tr>
  <td>
  Terms and conditions since&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :
  <asp:Label ID="lbl_current_tt_date_begin" runat=server></asp:Label>
  </td>
  </tr>
  </table>
 
 <hr />
     
  
<table>
 <tr>
 <td>
 new terms and condition begins &nbsp;(mm / dd / yyyy) &nbsp;&nbsp;: &nbsp;
<asp:DropDownList ID="ddl_tt_date_begin_m" runat="server">
<asp:ListItem Value="0">Month</asp:ListItem>
<asp:ListItem Value="1">January</asp:ListItem>
<asp:ListItem Value="2">February</asp:ListItem>
<asp:ListItem Value="3">March</asp:ListItem>
<asp:ListItem Value="4">April</asp:ListItem>
<asp:ListItem Value="5">May</asp:ListItem>
<asp:ListItem Value="6">June</asp:ListItem>
<asp:ListItem Value="7">July</asp:ListItem>
<asp:ListItem Value="8">August</asp:ListItem>
<asp:ListItem Value="9">September</asp:ListItem>
<asp:ListItem Value="10">October</asp:ListItem>
<asp:ListItem Value="11">November</asp:ListItem>
<asp:ListItem Value="12">December</asp:ListItem>
</asp:DropDownList>&nbsp; / &nbsp;
<asp:DropDownList ID="ddl_tt_date_begin_d" runat="server">
    <asp:ListItem Value="0">Day</asp:ListItem>
    <asp:ListItem>1</asp:ListItem>
    <asp:ListItem>2</asp:ListItem>
    <asp:ListItem>3</asp:ListItem>
    <asp:ListItem>4</asp:ListItem>
    <asp:ListItem>5</asp:ListItem>
    <asp:ListItem>6</asp:ListItem>
    <asp:ListItem>7</asp:ListItem>
    <asp:ListItem>8</asp:ListItem>
    <asp:ListItem>9</asp:ListItem>
    <asp:ListItem>10</asp:ListItem>
    <asp:ListItem>11</asp:ListItem>
    <asp:ListItem>12</asp:ListItem>
    <asp:ListItem>13</asp:ListItem>
    <asp:ListItem>14</asp:ListItem>
    <asp:ListItem>15</asp:ListItem>
    <asp:ListItem>16</asp:ListItem>
    <asp:ListItem>17</asp:ListItem>
    <asp:ListItem>18</asp:ListItem>
    <asp:ListItem>19</asp:ListItem>
    <asp:ListItem>20</asp:ListItem>
    <asp:ListItem>21</asp:ListItem>
    <asp:ListItem>22</asp:ListItem>
    <asp:ListItem>23</asp:ListItem>
    <asp:ListItem>24</asp:ListItem>
    <asp:ListItem>25</asp:ListItem>
    <asp:ListItem>26</asp:ListItem>
    <asp:ListItem>27</asp:ListItem>
    <asp:ListItem>28</asp:ListItem>
    <asp:ListItem>29</asp:ListItem>
    <asp:ListItem>30</asp:ListItem>
    <asp:ListItem>31</asp:ListItem>

</asp:DropDownList>&nbsp; / &nbsp;
 
 <asp:DropDownList ID="ddl_tt_date_begin_y" runat="server">
<asp:ListItem Value="0">Year</asp:ListItem>
<asp:ListItem>2007</asp:ListItem>
<asp:ListItem>2008</asp:ListItem>
<asp:ListItem>2009</asp:ListItem>
<asp:ListItem>2010</asp:ListItem>
<asp:ListItem>2011</asp:ListItem>
<asp:ListItem>2012</asp:ListItem>
<asp:ListItem>2013</asp:ListItem>
<asp:ListItem>2014</asp:ListItem>
<asp:ListItem>2015</asp:ListItem>
</asp:DropDownList> </td>
 </tr> 
    <tr>
        <td bgcolor="#ffffcc">
            <b>new terms and condtions date must be greater than&nbsp;&nbsp;:<asp:Label 
                ID="lbl_current_tt_date_begin1" runat="server"></asp:Label>
            </b>
        </td>
    </tr>
</table>    
          
          
 <hr />         
          
          
          
          
          
          
          <table>
          
          
          <tr>
<td valign="top">
 Form of payment </td>
<td>
<asp:DropDownList ID="ddl_tt_form_of_payment" runat="server">
<asp:ListItem Value="0">Not specified</asp:ListItem>
<asp:ListItem Value="1">Personal check</asp:ListItem>
<asp:ListItem Value="2">Cashier's check </asp:ListItem>
<asp:ListItem Value="3">Cash</asp:ListItem>
<asp:ListItem Value="4">Money order</asp:ListItem>
<asp:ListItem Value="5">Credit card</asp:ListItem>
 <asp:ListItem Value="6">Other</asp:ListItem>
</asp:DropDownList></td>
</tr>
<tr>
<td valign="top">
Non-sufficient fund fee  &nbsp; </td>
<td>
<asp:TextBox ID="tt_nsf" runat="server" Width="128px">0</asp:TextBox></td>
</tr>
        <tr>
        <td  valign=top>late fee</td>
        <td>
            <asp:TextBox ID="tt_late_fee" runat="server" Width="128px">0</asp:TextBox>
        </td>
        
        
        </tr>
        <tr><td valign=top>Security deposit required</td><td>
            <asp:RadioButtonList CssClass="letter" ID="tt_security_deposit" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
                   <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
            
            <asp:Panel ID="Panel_security_deposit" runat="server" >
            
            
                &nbsp;&nbsp;&nbsp;amount&nbsp;<asp:TextBox ID="tt_security_deposit_amount" Text="0" runat="server"></asp:TextBox>
     </asp:Panel>       
            
            
            
            </td>
       </tr>
            
            
            
            
            
            
            
 
            
            
            
            
            
            
            
            
            
            
        <tr><td valign=top>Guarantor required </td><td>
            <asp:RadioButtonList CssClass="letter" ID="tt_guarantor" runat="server" 
                AutoPostBack="True" 
                OnSelectedIndexChanged="tt_guarantor_SelectedIndexChanged" 
                RepeatDirection="Horizontal">
            <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
              <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
            <asp:Panel ID="panel_guarantor" runat="server" Visible="False">
                from the system &nbsp;<asp:DropDownList ID="ddl_guarantor_name_id" runat="server" 
                    AutoPostBack="True" CssClass="letter" DataTextField="name_name" 
                    DataValueField="name_id" 
                    OnSelectedIndexChanged="ddl_guarantor_name_id_SelectedIndexChanged" />
                <br />
                <b>Or</b>&nbsp;<br />
                Guarantor &nbsp;<asp:CheckBox ID="chk_guarantor" runat="server" AutoPostBack="True" 
                    OnCheckedChanged="chk_guarantor_OnCheckedChanged" />
                &nbsp;
                <asp:Label ID="txt" runat="server" Font-Bold="True" Visible="False"> * Disabled 
                *</asp:Label>
                <!--  BEGIN GUARANTOR INFORMATIONS    -->
                <table bgcolor="#ffffcc" border="0" cellpadding="1" cellspacing="3">
                    <tr>
                        <td class="letter_bold">
                            first name</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_fname" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            last name</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_lname" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            address</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_addr" runat="server" CssClass="letter"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            City</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_addr_city" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            Pc</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_addr_pc" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            State/Prov</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_addr_state" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            Country</td>
                        <td>
                            <asp:DropDownList ID="ddl_guarantor_country_id" runat="server" 
                                CssClass="letter" DataTextField="country_name" DataValueField="country_id" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            Telephone</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_tel" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            Tel. Work</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_tel_work" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            Tel. Work ext.</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_tel_work_ext" runat="server" 
                                CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            Cell</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_cell" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            Fax</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_fax" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            Email</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_email" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            Comments</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_com" runat="server" CssClass="letter" 
                                TextMode="MultiLine" Width="200px" />
                        </td>
                    </tr>
                </table>
                <!-- END GUARANTOR INFORMATIONS      -->
            </asp:Panel>
            </td></tr>
        <tr>
        <td valign=top>Pets allowed</td>
        <td>
            <asp:RadioButtonList   CssClass="letter" ID="tt_pets" runat="server" 
                RepeatDirection="Horizontal" >
                <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
                <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
         </td>
        </tr>
        <tr valign="top">
            <td valign=top>Tenant is responsible for maintenance</td>
         <td>
          <asp:RadioButtonList   CssClass="letter" ID="tt_maintenance" runat="server" 
                 OnSelectedIndexChanged="tt_maintenance_SelectedIndexChanged" 
                 RepeatDirection="Horizontal" >
            <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
            <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
            <asp:Panel ID="panel_specify_maintenance" runat="server">
                &nbsp;&nbsp;&nbsp;specify maintenance allowed<br />&nbsp;<asp:TextBox runat=server 
                    ID="tt_specify_maintenance"  Width="300px" TextMode="MultiLine" ></asp:TextBox>
            </asp:Panel>
         
         </td> 
            
        </tr>
        <tr>
        <td valign=top>Can tenant make improvement</td>
        <td>
            <asp:RadioButtonList   CssClass="letter" ID="tt_improvement" runat="server" 
                OnSelectedIndexChanged="tt_improvement_SelectedIndexChanged" 
                RepeatDirection="Horizontal" >
                <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
                <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
             <asp:Panel ID="panel_specify_improvement" runat="server">
                 &nbsp;&nbsp;&nbsp;specify improvement allowed<br />
                 &nbsp;<asp:TextBox runat=server Width="300px" ID="tt_specify_improvement" 
                     TextMode="MultiLine"  ></asp:TextBox>
            </asp:Panel>
         
        </td>
        </tr>
        <tr>
        <td valign=top>Notice to enter </td>
        <td>
            <asp:RadioButtonList ID="tt_notice_to_enter" runat="server" CssClass="letter" 
                OnSelectedIndexChanged="tt_notice_to_enter_SelectedIndexChanged">
                <asp:ListItem Selected="True" Value="0">legal minimum</asp:ListItem>
                <asp:ListItem Value="1">number of hours</asp:ListItem>
            </asp:RadioButtonList>
            <asp:Panel ID="panel_specify_notice_to_enter" runat="server">
                &nbsp;specify number of hours notice&nbsp;<asp:TextBox ID="tt_specify_number_of_hours" 
                    runat="server" Width="100px"></asp:TextBox>
            </asp:Panel>
        </td>
        </tr>
        <tr><td valign=top>Who pay these insurances</td>
        
        <td>
            <table>
                <tr>
                    <td>
                        tenant content</td>
                    <td>
                        <asp:DropDownList ID="ddl_tt_tenant_content_ins" runat="server" 
                            CssClass="letter">
                            <asp:ListItem Selected="True" Value="0">not specified</asp:ListItem>
                            <asp:ListItem Value="1">landlord</asp:ListItem>
                            <asp:ListItem Value="2">tenant</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        landlord content</td>
                    <td>
                        <asp:DropDownList ID="ddl_tt_landlord_content_ins" runat="server" 
                            CssClass="letter">
                            <asp:ListItem Selected="True" Value="0">not specified</asp:ListItem>
                            <asp:ListItem Value="1">landlord</asp:ListItem>
                            <asp:ListItem Value="2">tenant</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        personal injury on property</td>
                    <td>
                        <asp:DropDownList ID="ddl_tt_injury_ins" runat="server" CssClass="letter">
                            <asp:ListItem Selected="True" Value="0">not specified</asp:ListItem>
                            <asp:ListItem Value="1">landlord</asp:ListItem>
                            <asp:ListItem Value="2">tenant</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        lease premises</td>
                    <td>
                        <asp:DropDownList ID="ddl_tt_premises_ins" runat="server" CssClass="letter">
                            <asp:ListItem Selected="True" Value="0">not specified</asp:ListItem>
                            <asp:ListItem Value="1">landlord</asp:ListItem>
                            <asp:ListItem Value="2">tenant</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            </td>
        </tr>
        
        
        
        <tr><td valign="top">
            Additional terms &amp; conditions</td>
            <td>
                <asp:TextBox ID="tt_additional_terms" runat="server" Height="200px" 
                    TextMode="MultiLine" Width="400px"></asp:TextBox>
            </td>
              </tr>
          <tr><td>
              <asp:Button ID="btn_continue" runat="server" OnClick="btn_continue_Onclick" 
                  Text="Continue" />
              </td></tr>
              <tr>
                  <td>
                      <div ID="txt_link" runat="server" />
                      </td>
                  </tr>
        </table>
          
  </asp:Panel>  
  
  <div id="txt_message" runat="server" />  
       
   <asp:HiddenField ID="hd_min_begin_date_m" runat="server" />
             <asp:HiddenField ID="hd_min_begin_date_d" runat="server" />
             <asp:HiddenField ID="hd_min_begin_date_y" runat="server" />
             
    <!--Hidden fields BEGIN SECTION 1-->
         <asp:HiddenField Visible="False" id="hd_home_id" runat="server" />
         <asp:HiddenField Visible="False" id="hd_unit_id" runat="server" />
         <asp:HiddenField Visible="False" id="hd_current_tu_id" runat="server" />
         <!--Hidden fields END SECTION 1-->
           <!--Hidden fields temporaire-->
          <asp:HiddenField Visible="False" id="didden" runat="server" />
          
                  
            </ContentTemplate  >
  </cc1:TabPanel>
  
  
  
  
  
  
  
  
  
  
  
  
  
  
   <cc1:TabPanel ID="tab2" runat="server"   HeaderText="COMMERCIAL" >
    <ContentTemplate  >
    
  <table>
    <asp:Panel ID="panel_add_home_2" runat="server" BackColor="#FFFFCC" >
   <tr>
   <td>
   <strong><span style="font-size: 12pt">Home</span></strong> :
   <asp:DropDownList ID="ddl_home_id_2" DataValueField="home_id" DataTextField="home_name"   runat="server" autopostback="true" OnSelectedIndexChanged="ddl_home_id_2_SelectedIndexChanged" />
   </td>
   </tr>
   </asp:Panel>
   
   
   
   <tr><td><div id="txt_pending_2" runat="server"></div></td></tr>
   
   
   
   <tr>
    <td>
     <asp:Repeater ID="r_pendingtermsandconditionslist_2"    runat="server" 
           >
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
            <tr>
                <td>
                   <b>Pending terms date begin</b> </td>
                <td >
                    : 
                    <%#DataBinder.Eval(Container.DataItem, "tt_date_begin")%>
                    <asp:HiddenField Visible="false" id="hd_pending_tt_date_begin_2" value ='<%#DataBinder.Eval(Container.DataItem, "tt_date_begin")%>' runat="server" />
                    
               </td>
            </tr>
         </table>
        </ItemTemplate>
        
    </asp:Repeater>
    </td>
   </tr>
   
   
   <asp:Panel ID="panel_add_unit_2"  runat="server" >
   <tr>
   <td>
   <strong><span style="font-size: 12pt">Unit</span></strong> &nbsp;&nbsp;&nbsp;&nbsp;:
   <asp:DropDownList ID="ddl_unit_id_2" DataValueField="unit_id" DataTextField="unit_door_no"   runat="server" OnSelectedIndexChanged="ddl_unit_id_2_SelectedIndexChanged" AutoPostBack="true" />
   <br /><br />
   
   </td>
   </tr>
   </asp:Panel>
  </table>  
  
  <asp:Panel ID="panel_term_cond_update_2" runat="server">
  <hr />
  
  
  <asp:Panel ID="panel_current_tenant_2"  Visible="false" runat="server">   
<table >
         <tr id="tr_company" runat="server">
            <td>
               <b> Company</b></td>
            <td>
               &nbsp;:&nbsp;&nbsp; <asp:Label ID="lbl_company" runat="server" ></asp:Label></td>
        </tr>
    </table>
   <br />

<table>
<tr>
<td><div id="txt_current_tenant_name_2" runat="server" />
</td>
</tr>

</table>
<hr />


</asp:Panel>
  
  <table>
  <tr>
  <td>
  Terms and conditions since&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :
  <asp:Label ID="lbl_current_tt_date_begin_2" runat=server></asp:Label>
  </td>
  </tr>
  </table>
 
 <hr />
     
  
<table>
 <tr>
 <td>
 new terms and condition begins &nbsp;(mm / dd / yyyy) &nbsp;&nbsp;: &nbsp;
<asp:DropDownList ID="ddl_tt_date_begin_m2" runat="server">
<asp:ListItem Value="0">Month</asp:ListItem>
<asp:ListItem Value="1">January</asp:ListItem>
<asp:ListItem Value="2">February</asp:ListItem>
<asp:ListItem Value="3">March</asp:ListItem>
<asp:ListItem Value="4">April</asp:ListItem>
<asp:ListItem Value="5">May</asp:ListItem>
<asp:ListItem Value="6">June</asp:ListItem>
<asp:ListItem Value="7">July</asp:ListItem>
<asp:ListItem Value="8">August</asp:ListItem>
<asp:ListItem Value="9">September</asp:ListItem>
<asp:ListItem Value="10">October</asp:ListItem>
<asp:ListItem Value="11">November</asp:ListItem>
<asp:ListItem Value="12">December</asp:ListItem>
</asp:DropDownList>&nbsp; / &nbsp;
<asp:DropDownList ID="ddl_tt_date_begin_d2" runat="server">
    <asp:ListItem Value="0">Day</asp:ListItem>
    <asp:ListItem>1</asp:ListItem>
    <asp:ListItem>2</asp:ListItem>
    <asp:ListItem>3</asp:ListItem>
    <asp:ListItem>4</asp:ListItem>
    <asp:ListItem>5</asp:ListItem>
    <asp:ListItem>6</asp:ListItem>
    <asp:ListItem>7</asp:ListItem>
    <asp:ListItem>8</asp:ListItem>
    <asp:ListItem>9</asp:ListItem>
    <asp:ListItem>10</asp:ListItem>
    <asp:ListItem>11</asp:ListItem>
    <asp:ListItem>12</asp:ListItem>
    <asp:ListItem>13</asp:ListItem>
    <asp:ListItem>14</asp:ListItem>
    <asp:ListItem>15</asp:ListItem>
    <asp:ListItem>16</asp:ListItem>
    <asp:ListItem>17</asp:ListItem>
    <asp:ListItem>18</asp:ListItem>
    <asp:ListItem>19</asp:ListItem>
    <asp:ListItem>20</asp:ListItem>
    <asp:ListItem>21</asp:ListItem>
    <asp:ListItem>22</asp:ListItem>
    <asp:ListItem>23</asp:ListItem>
    <asp:ListItem>24</asp:ListItem>
    <asp:ListItem>25</asp:ListItem>
    <asp:ListItem>26</asp:ListItem>
    <asp:ListItem>27</asp:ListItem>
    <asp:ListItem>28</asp:ListItem>
    <asp:ListItem>29</asp:ListItem>
    <asp:ListItem>30</asp:ListItem>
    <asp:ListItem>31</asp:ListItem>

</asp:DropDownList>&nbsp; / &nbsp;
 
 <asp:DropDownList ID="ddl_tt_date_begin_y2" runat="server">
<asp:ListItem Value="0">Year</asp:ListItem>
<asp:ListItem>2007</asp:ListItem>
<asp:ListItem>2008</asp:ListItem>
<asp:ListItem>2009</asp:ListItem>
<asp:ListItem>2010</asp:ListItem>
<asp:ListItem>2011</asp:ListItem>
<asp:ListItem>2012</asp:ListItem>
<asp:ListItem>2013</asp:ListItem>
<asp:ListItem>2014</asp:ListItem>
<asp:ListItem>2015</asp:ListItem>
</asp:DropDownList> </td>
 </tr> 
    <tr>
        <td bgcolor="#ffffcc">
            <b>new terms and condtions date must be greater than&nbsp;&nbsp;:<asp:Label 
                ID="lbl_current_tt_date_begin_3" runat="server"></asp:Label>
            </b>
        </td>
    </tr>
</table>    
          
          
 <hr />         
          
          
          
          
          
          
          <table>
          
          
          <tr>
<td valign="top">
 Form of payment </td>
<td>
<asp:DropDownList ID="ddl_tt_form_of_payment_2" runat="server">
<asp:ListItem Value="0">Not specified</asp:ListItem>
<asp:ListItem Value="1">Personal check</asp:ListItem>
<asp:ListItem Value="2">Cashier's check </asp:ListItem>
<asp:ListItem Value="3">Cash</asp:ListItem>
<asp:ListItem Value="4">Money order</asp:ListItem>
<asp:ListItem Value="5">Credit card</asp:ListItem>
 <asp:ListItem Value="6">Other</asp:ListItem>
</asp:DropDownList></td>
</tr>
<tr>
<td valign="top">
Non-sufficient fund fee  &nbsp; </td>
<td>
<asp:TextBox ID="tt_nsf_2" Text="0" runat="server" Width="128px"></asp:TextBox></td>
</tr>
        <tr>
        <td  valign=top>late fee</td>
        <td>
            <asp:TextBox ID="tt_late_fee_2" Text="0" runat="server" Width="128px"></asp:TextBox>
        </td>
        
        
        </tr>
        <tr><td valign=top>Security deposit required</td><td>
            <asp:RadioButtonList CssClass="letter" ID="tt_security_deposit_2" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
                   <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
            
            <asp:Panel ID="Panel_security_deposit_2" Visible=true runat="server" >
            
            
                &nbsp;&nbsp;&nbsp;amount&nbsp;<asp:TextBox ID="tt_security_deposit_amount_2" runat="server"></asp:TextBox>
     </asp:Panel>       
            
            
            
            </td>
       </tr>
            
            
           
            
        <tr><td valign=top>Guarantor required </td><td>
            <asp:RadioButtonList CssClass="letter" ID="tt_guarantor_2" runat="server" 
                AutoPostBack="true" 
                OnSelectedIndexChanged="tt_guarantor_2_SelectedIndexChanged" 
                RepeatDirection="Horizontal">
            <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
              <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
            <asp:Panel ID="panel_guarantor_2" runat="server" Visible="false">
                from the system &nbsp;<asp:DropDownList ID="ddl_guarantor_name_id_2" runat="server" 
                    AutoPostBack="true" CssClass="letter" DataTextField="name_name" 
                    DataValueField="name_id" 
                    OnSelectedIndexChanged="ddl_guarantor_name_id_2_SelectedIndexChanged" />
                <br />
                <b>Or</b>&nbsp;<br />
                Guarantor &nbsp;<asp:CheckBox ID="chk_guarantor_2" runat="server" AutoPostBack="true" 
                    OnCheckedChanged="chk_guarantor_2_OnCheckedChanged" />
                &nbsp;
                <asp:Label ID="txt_2" runat="server" Font-Bold="true" Visible="false"> * Disabled 
                *</asp:Label>
                <!--  BEGIN GUARANTOR INFORMATIONS    -->
                <table bgcolor="#ffffcc" border="0" cellpadding="1" cellspacing="3">
                    <tr>
                        <td class="letter_bold">
                            first name</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_fname_2" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            last name</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_lname_2" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            address</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_addr_2" runat="server" CssClass="letter"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            City</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_addr_city_2" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            Pc</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_addr_pc_2" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            State/Prov</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_addr_state_2" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            Country</td>
                        <td>
                            <asp:DropDownList ID="ddl_guarantor_country_id_2" runat="server" 
                                CssClass="letter" DataTextField="country_name" DataValueField="country_id" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            Telephone</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_tel_2" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            Tel. Work</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_tel_work_2" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            Tel. Work ext.</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_tel_work_ext_2" runat="server" 
                                CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            Cell</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_cell_2" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            Fax</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_fax_2" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            Email</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_email_2" runat="server" CssClass="letter" />
                        </td>
                    </tr>
                    <tr>
                        <td class="letter_bold">
                            Comments</td>
                        <td>
                            <asp:TextBox ID="guarantor_name_com_2" runat="server" CssClass="letter" 
                                TextMode="MultiLine" Width="200px" />
                        </td>
                    </tr>
                </table>
                <!-- END GUARANTOR INFORMATIONS      -->
            </asp:Panel>
            </td></tr>
        <tr>
        <td valign=top>Pets allowed</td>
        <td>
            <asp:RadioButtonList   CssClass="letter" ID="tt_pets_2" runat="server" 
                RepeatDirection="Horizontal" >
                <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
                <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
         </td>
        </tr>
        <tr valign="top">
            <td valign=top>Tenant is responsible for maintenance</td>
         <td>
          <asp:RadioButtonList   CssClass="letter" ID="tt_maintenance_2" runat="server" 
                 OnSelectedIndexChanged="tt_maintenance_2_SelectedIndexChanged" 
                 RepeatDirection="Horizontal" >
            <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
            <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
            <asp:Panel Visible=true ID="panel_specify_maintenance_2" runat="server">
                &nbsp;&nbsp;&nbsp;specify maintenance allowed<br />&nbsp;<asp:TextBox runat=server 
                    ID="tt_specify_maintenance_2"  Width="300px" TextMode="MultiLine" ></asp:TextBox>
            </asp:Panel>
         
         </td> 
            
        </tr>
        <tr>
        <td valign=top>Can tenant make improvement</td>
        <td>
            <asp:RadioButtonList   CssClass="letter" ID="tt_improvement_2" runat="server" 
                OnSelectedIndexChanged="tt_improvement_2_SelectedIndexChanged" 
                RepeatDirection="Horizontal" >
                <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
                <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
             <asp:Panel Visible=true ID="panel_specify_improvement_2" runat="server">
                 &nbsp;&nbsp;&nbsp;specify improvement allowed<br />
                 &nbsp;<asp:TextBox runat=server Width="300px" ID="tt_specify_improvement_2" 
                     TextMode="MultiLine"  ></asp:TextBox>
            </asp:Panel>
         
        </td>
        </tr>
        <tr>
        <td valign=top>Notice to enter </td>
        <td>
            <asp:RadioButtonList ID="tt_notice_to_enter_2" runat="server" CssClass="letter" 
                OnSelectedIndexChanged="tt_notice_to_enter_2_SelectedIndexChanged">
                <asp:ListItem Selected="True" Value="0">legal minimum</asp:ListItem>
                <asp:ListItem Value="1">number of hours</asp:ListItem>
            </asp:RadioButtonList>
            <asp:Panel ID="panel_specify_notice_to_enter_2" runat="server" Visible="true">
                &nbsp;specify number of hours notice&nbsp;<asp:TextBox ID="tt_specify_number_of_hours_2" 
                    runat="server" Width="100px"></asp:TextBox>
            </asp:Panel>
        </td>
        </tr>
        <tr><td valign=top>Who pay these insurances</td>
        
        <td>
            <table>
                <tr>
                    <td>
                        tenant content</td>
                    <td>
                        <asp:DropDownList ID="ddl_tt_tenant_content_ins_2" runat="server" 
                            CssClass="letter">
                            <asp:ListItem Selected="True" Value="0">not specified</asp:ListItem>
                            <asp:ListItem Value="1">landlord</asp:ListItem>
                            <asp:ListItem Value="2">tenant</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        landlord content</td>
                    <td>
                        <asp:DropDownList ID="ddl_tt_landlord_content_ins_2" runat="server" 
                            CssClass="letter">
                            <asp:ListItem Selected="True" Value="0">not specified</asp:ListItem>
                            <asp:ListItem Value="1">landlord</asp:ListItem>
                            <asp:ListItem Value="2">tenant</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        personal injury on property</td>
                    <td>
                        <asp:DropDownList ID="ddl_tt_injury_ins_2" runat="server" CssClass="letter">
                            <asp:ListItem Selected="True" Value="0">not specified</asp:ListItem>
                            <asp:ListItem Value="1">landlord</asp:ListItem>
                            <asp:ListItem Value="2">tenant</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        lease premises</td>
                    <td>
                        <asp:DropDownList ID="ddl_tt_premises_ins_2" runat="server" CssClass="letter">
                            <asp:ListItem Selected="True" Value="0">not specified</asp:ListItem>
                            <asp:ListItem Value="1">landlord</asp:ListItem>
                            <asp:ListItem Value="2">tenant</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            </td>
        </tr>
        
        
        
        <tr><td valign="top">
            Additional terms &amp; conditions</td>
            <td>
                <asp:TextBox ID="tt_additional_terms_2" runat="server" Height="200px" 
                    TextMode="MultiLine" Width="400px"></asp:TextBox>
            </td>
              </tr>
          <tr><td>
              <asp:Button ID="btn_continue_2" runat="server" OnClick="btn_continue_2_Onclick" 
                  Text="Continue" />
              </td></tr>
              <tr>
                  <td>
                      <div ID="txt_link_2" runat="server" />
                      </td>
                  </tr>
        </table>
          
  </asp:Panel>  
  
  <div id="txt_message_2" runat="server" />  
       
   <asp:HiddenField ID="hd_min_begin_date_m2" runat="server" />
             <asp:HiddenField ID="hd_min_begin_date_d2" runat="server" />
             <asp:HiddenField ID="hd_min_begin_date_y2" runat="server" />
             
    <!--Hidden fields BEGIN SECTION 1-->
         <asp:HiddenField Visible="false" id="hd_home_id_2" runat="server" />
         <asp:HiddenField Visible="false" id="hd_unit_id_2" runat="server" />
         <asp:HiddenField Visible="false" id="hd_current_tu_id_2" runat="server" />
         <!--Hidden fields END SECTION 1-->
           <!--Hidden fields temporaire-->
          <asp:HiddenField Visible="false" id="Hidden1" runat="server" />
    
    
    
    
      </ContentTemplate  >
  </cc1:TabPanel>
   </cc1:TabContainer>
  
           
</asp:Content>