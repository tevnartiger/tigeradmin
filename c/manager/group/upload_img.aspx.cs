﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class manager_group_upload_img : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        { 
          
             tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = h.getHomeCount(Convert.ToInt32(Session["schema_id"]));

            if (home_count > 0)
            {
                int home_id = h.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));
                ddl_home_id.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                ddl_home_id.SelectedValue = Convert.ToString(home_id);
                ddl_home_id.DataBind();
            }
        
        }
    }

    protected void btn_upload_Click(object sender, EventArgs e)
    {
        string filename = Path.GetFileName(FileUpload1.PostedFile.FileName);
        int filesize = 0;
        if (FileUpload1.HasFile)
        {

            Stream stream = FileUpload1.PostedFile.InputStream;
            filesize = FileUpload1.PostedFile.ContentLength;
            byte[] filedata = new byte[filesize];
            stream.Read(filedata, 0, filesize);

            // explaining the parameters
            // 0 : the File will belong to everybody in the account (schema)
            // Convert.ToInt32(Session["name_id"]) : who ceated the folder
            // name_id_ip : the ip of the user who created the folder


        int success = 0;
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        tiger.Media upload = new tiger.Media(strconn);

        success = upload.HomeImageUpdate(filedata, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), 
                                    Convert.ToInt32(Session["name_id"]), Request.UserHostAddress.ToString());

        /*if (success == 0)
            lbl_success.Text = "SUCCESS";
        else
            lbl_success.Text = "FAILURE";*/

        lbl_success.Text = success.ToString();



       
        }
    }
}
