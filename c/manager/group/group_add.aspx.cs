using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using tiger.security;

public partial class group_group_add : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Page.IsPostBack == false)
        {
            reg_group_name.ValidationExpression = RegEx.getAddress();
            tiger.Home l = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getHomeCount(Convert.ToInt32(Session["schema_id"]));

            // first we check if there's any property available
            if (home_count > 0)
            {

                /*tiger.Group h = new tiger.Group(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                group_list.DataSource = h.getGroupHomeList(Convert.ToInt32(Session["schema_id"]));
                group_list.DataBind();*/
                tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                cbl_home_list.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                cbl_home_list.DataBind();
            }

            else
            {

                txt_message.InnerHtml = "<b><a href='~/home/home_add.aspx'>Please create properties before creating a group of properties</a></b>";
            }

        }

    }
    
    
    
   /* protected void btn_to2_Click(object sender, EventArgs e)
    {

        string temp_group_name = tb_group_name.Text;
        if (RegEx.IsAddress(temp_group_name))
        {
            //verify if name already exist
            tiger.Group g = new tiger.Group(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


            if (g.groupExist(Convert.ToInt32(Session["schema_id"]), temp_group_name))
            {
                lt_message1.Text = Resources.Resource.lbl_group_exist;
            }
            else
            {
                mv.SetActiveView(view2);

            }
        }
    }
    */
   /*
      protected void btn_to2_Click(object sender, EventArgs e)
    {
        string temp_body = tb_group_name.Text+"<br/><br/>";
         foreach (ListItem li in cbl_home_list.Items)
        {

            if (li.Selected)
            {
            temp_body +=  li.Text+"<br/>";
                 
            }
        }
        lt_group.Text  = temp_body;
       
        mv.SetActiveView(view3);

    }
    */
    protected void btn_create_Click(object sender, EventArgs e)
    {
        string temp_group_name = RegEx.getText(tb_group_name.Text);

        //create group and return new group id
        tiger.Group g = new tiger.Group(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        int temp_group_id = g.addGroup(temp_group_name,Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), Convert.ToString(Request.UserHostAddress));
        if (temp_group_id == -1)
        {
            //group already exist
            lt_message.Text = "groupname: " + temp_group_name + "<br/>result: " + temp_group_id + "<br/>schema_id: " + Convert.ToString(Session["schema_id"]) + "<br/>name_id: " + Convert.ToString(Session["name_id"]) + "<br/>IP: " + Convert.ToString(Request.UserHostAddress) + "---" + Resources.Resource.lbl_group_exist;
        }
        else
        {
            lt_message.Text += temp_group_name;

            foreach (ListItem li in cbl_home_list.Items)
            {

                if (li.Selected)
                {
                    //temp_body += li.Text + "<br/>";
                    if (g.addGroupHome(Convert.ToInt32(Session["schema_id"]), temp_group_id, Convert.ToInt32(li.Value), Convert.ToInt32(Session["name_id"]), Convert.ToString(Request.UserHostAddress)) == -1)
                    {
                        lt_message.Text += li.Text + " already exist<br/>";
                    }
                    else
                    {
                        lt_message.Text += li.Text + "inserted<br/>";
                    }
                }
            }
        }
    }
   
}
