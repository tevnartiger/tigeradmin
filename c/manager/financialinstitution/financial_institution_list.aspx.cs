using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class financialinstitution_financial_institution_list : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        tiger.FinancialInstitution fi = new tiger.FinancialInstitution(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_fi_list.DataSource = fi.getFinancialInstitutionList(Convert.ToInt32(Session["schema_id"]));
        gv_fi_list.DataBind();
    }


    protected void gv_fi_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        tiger.FinancialInstitution fi = new tiger.FinancialInstitution(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_fi_list.PageIndex = e.NewPageIndex;
        gv_fi_list.DataSource = fi.getFinancialInstitutionList(Convert.ToInt32(Session["schema_id"]));
        gv_fi_list.DataBind();
    }
}
