<%@ Page Language="C#" MasterPageFile="../mp_manager.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="financial_institution_list.aspx.cs" Inherits="financialinstitution_financial_institution_list" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
    <asp:GridView  Width="100%"   ID="gv_fi_list" AutoGenerateColumns="false"
           AllowPaging="true" AllowSorting="true"    AlternatingRowStyle-BackColor="#F0F0F6" 
      BorderColor="#CDCDCD"  BorderWidth="1" GridLines="Both"   OnPageIndexChanging="gv_fi_list_PageIndexChanging"
      HeaderStyle-BackColor="#F0F0F6" runat="server" >
    <Columns>
    <asp:BoundField HeaderStyle-VerticalAlign="Top" DataField="fi_name" HeaderText="Fianancial Institution name" />
    <asp:BoundField HeaderStyle-VerticalAlign="Top" DataField="fi_addr" HeaderText="Address" />
    <asp:BoundField HeaderStyle-VerticalAlign="Top" DataField="fi_city" HeaderText="City" />
    <asp:BoundField HeaderStyle-VerticalAlign="Top" DataField="fi_contact_fname" HeaderText="Contact first name" />
    <asp:BoundField HeaderStyle-VerticalAlign="Top" DataField="fi_contact_lname" HeaderText="Contact last name" />
    <asp:BoundField HeaderStyle-VerticalAlign="Top" DataField="fi_contact_tel" HeaderText="Contact tel." />
    <asp:BoundField HeaderStyle-VerticalAlign="Top" DataField="fi_website" HeaderText="Website" />
    <asp:HyperLinkField   HeaderText="View details"  Text="view"
     DataNavigateUrlFields="fi_id" 
     DataNavigateUrlFormatString="~/manager/financialinstitution/financial_institution_view.aspx?fi_id={0}" 
       />
    </Columns>    
    </asp:GridView><br /><br />
     <asp:HyperLink ID="link_main" runat="server" NavigateUrl="~/home/home_main.aspx">go back to main</asp:HyperLink>
               
    </div>
   </asp:Content>