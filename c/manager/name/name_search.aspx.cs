﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manager_name_name_search : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            tiger.Name a = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            search.InnerHtml = a.nameSearch(Convert.ToInt32(Session["schema_id"]), Request.QueryString["v"], 0);
        
        }
    }
}
