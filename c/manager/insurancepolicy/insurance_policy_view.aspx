
<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="insurance_policy_view.aspx.cs" Inherits="insurancepolicy_insurance_policy_view" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
     <b>   <asp:Label ID="Label11" runat="server" Text="<%$ Resources:Resource, lbl_u_insurance_policy %>"></asp:Label></b><br /><br /><asp:HyperLink ID="insurance_policy_add_link" 
          Text="<%$ Resources:Resource, lbl_add %>"   runat="server"> </asp:HyperLink> -&nbsp;  <asp:HyperLink ID="insurance_policy_update_link"
          Text="<%$ Resources:Resource, lbl_update %>"  runat="server"></asp:HyperLink>-&nbsp;  <asp:HyperLink ID="insurance_policy_renew_link"
          Text="<%$ Resources:Resource, lbl_renew %>"  runat="server"></asp:HyperLink>  - delete insurance 
        &nbsp;<br />
       
        <br />
        <br />
        <table style="width: 100%">
            <tr>
                <td valign="top">
       
    <table width="100%" bgcolor="#ffffcc" >
            <tr >
                <td valign="top">
                    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_home_name %>"></asp:Label></td>
                <td>
                    :<asp:Label ID="lbl_home_name" runat="server" Text=""></asp:Label>
                    
                    
                </td>
            </tr>
            <tr >
                <td valign=top > 
                    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_insurance_provider %>"></asp:Label> </td>
                <td >:<asp:Label ID="lbl_ip_name" runat="server" Text=""></asp:Label>
                  </td>
            </tr>
            <tr >
                <td>
                    <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lbl_insurance_type %>"></asp:Label> </td>
                <td>
                    :
                    <asp:Label ID="lbl_insurance_type" runat="server" Text=""></asp:Label></td>
            </tr>
            <tr  >
                <td >
                    <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lbl_policy_number %>"></asp:Label></td>
                <td >
                    :<asp:Label ID="lbl_ip_number" runat="server" Text=""></asp:Label>
                   </td>
            </tr>
            
            <tr>
                <td >
                    <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lbl_coverage %>"></asp:Label></td>
                <td >:<asp:Label ID="lbl_ip_amount_covered" runat="server" Text=""></asp:Label>
                    
                    </td>
            </tr>
            <tr>
                <td style="height: 26px">
                    <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lbl_deductible %>"></asp:Label></td>
                <td style="height: 26px">
                    :<asp:Label ID="lbl_ip_deductible" runat="server" Text=""></asp:Label>
                    </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, lbl_year_premium %>"></asp:Label> </td>
                <td >:<asp:Label ID="lbl_ip_year_premium" runat="server" Text=""></asp:Label>
                    
                   </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lbl_monthly_payment %>"></asp:Label></td>
                <td >:<asp:Label ID="lbl_ip_monthly_payment" runat="server" Text=""></asp:Label>
                    
                    </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, lbl_date_begin %>"></asp:Label></td>
                <td ><asp:Label ID="lbl_ip_date_begin" runat="server" Text=""></asp:Label>    
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label10" runat="server" Text="<%$ Resources:Resource, lbl_date_end %>"></asp:Label></td>
                <td>
                    :
                   <asp:Label ID="lbl_ip_date_end" runat="server" Text=""></asp:Label></td>
            </tr>
        </table>
         
      
              
                    <br />
                    <asp:CheckBox ID="chk_enable_button" Text="<%$ Resources:Resource, lbl_enable_button %>"  runat="server" 
         oncheckedchanged="chk_enable_button_CheckedChanged" AutoPostBack="true" /><br />
                    &nbsp;<asp:Button ID="btn_delete" runat="server" 
         Text="<%$ Resources:Resource, lbl_delete %>" 
         onclick="btn_delete_Click" />
       <br />
      
              
                </td>
                <td valign="top" align="left">
                     
         <table width="100%">
           <tr>
               <td bgcolor="aliceblue">
          <b><asp:Label ID="Label13" runat="server" Text="<%$ Resources:Resource, lbl_u_archive_insurance_policy %>"></asp:Label></b></td>
           </tr>
           
           
           
             <tr>
               <td valign="top" >
             <asp:GridView ID="gv_ip_home_archive_list" 
    runat="server" AutoGenerateColumns="false" GridLines="Both"  
        AlternatingRowStyle-BackColor="Beige"  HeaderStyle-BackColor="AliceBlue" 
         Width="100%" BorderColor="AliceBlue" BorderWidth="3"
       AllowPaging="true"  PageSize="10" EmptyDataText="<%$ Resources:Resource, lbl_none %>"  >
           
    
    <Columns>
    
    <asp:BoundField DataField="ip_name" HeaderText="<%$ Resources:Resource, lbl_insurance_provider %>" />
    <asp:BoundField DataField="ip_number" HeaderText="<%$ Resources:Resource, lbl_policy_number %>" />
    <asp:BoundField DataField="ip_date_begin" DataFormatString="{0:M-dd-yyyy}"  HtmlEncode="false" HeaderText="<%$ Resources:Resource, lbl_date_begin %>" />
    <asp:BoundField DataField="ip_date_end" DataFormatString="{0:M-dd-yyyy}"  HtmlEncode="false"  HeaderText="<%$ Resources:Resource, lbl_date_end %>"/>
    
   
   
    <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_view %>"
     DataNavigateUrlFields="ip_id" 
     DataNavigateUrlFormatString="~/manager/insurancepolicy/insurance_policy_archive_view.aspx?ip_id={0}" 
      HeaderText="<%$ Resources:Resource, gv_view_detail %>" />
    </Columns>  
    </asp:GridView>
               
            </td>
           </tr>
       </table>

                </td>
            </tr>
        </table>
         
      
              
    </div>
  </asp:Content>
