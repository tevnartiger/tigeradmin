﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;


/// <summary>
/// Done by : Stanley Jocelyn
/// date    : july 15 , 2008
/// </summary>

public partial class manager_insurancepolicy_insurance_policy_renew : BasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!RegEx.IsInteger(Request.QueryString["ip_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }


        reg_ip_amount_covered.ValidationExpression = RegEx.getMoney();
        reg_ip_deductible.ValidationExpression = RegEx.getMoney();
        reg_ip_monthly_payment.ValidationExpression = RegEx.getMoney();
        reg_ip_number.ValidationExpression = RegEx.getAlphaNumeric();
        reg_ip_year_premium.ValidationExpression = RegEx.getMoney();
             

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        /////////////
        /////////////// SECURITY CHECK BEGIN   //////////////
        //////////////
        AccountObjectAuthorization ipAuthorization = new AccountObjectAuthorization(strconn);

        if (!ipAuthorization.InsurancePolicy(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["ip_id"])))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        /////////////
        /////////////// SECURITY CHECK END  //////////////
        //////////////


        if (!Page.IsPostBack)
        {
            insurance_policy_pending_link.Visible = false;
            lbl_already_pending_insurance_policy.Visible = false;


            tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_home_list.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
            ddl_home_list.DataBind();

            tiger.Country ddl_ic = new tiger.Country(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_ic_country_list.DataSource = ddl_ic.getCountryList();
            ddl_ic_country_list.DataBind();

            tiger.Country ddl_fi = new tiger.Country(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_fi_country_list.DataSource = ddl_fi.getCountryList();
            ddl_fi_country_list.DataBind();

            tiger.InsuranceCompany ic = new tiger.InsuranceCompany(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_ic_list.DataSource = ic.getInsuranceCompanyList(Convert.ToInt32(Session["schema_id"]));
            ddl_ic_list.DataBind();

            tiger.FinancialInstitution fi = new tiger.FinancialInstitution(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_fi_list.DataSource = fi.getFinancialInstitutionList(Convert.ToInt32(Session["schema_id"]));
            ddl_fi_list.DataBind();

            ddl_fi_list.Enabled = false;
            chk_insurance_company.Checked = true;
            chk_ic_add.Visible = true;
            chk_fi_add.Visible = false;

            panel_ic.Visible = false;
            panel_fi.Visible = false;

            //   lbl_message.Text = "Hello World";



            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prInsurancePolicyView", conn);
            SqlCommand cmd2 = new SqlCommand("prInsurancePolicyPendingId", conn);

           
           
            cmd.CommandType = CommandType.StoredProcedure;



            //Add the params
            cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("ip_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["ip_id"]);
            //  try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);



                while (dr.Read() == true)
                {
                    ddl_home_list.SelectedValue = dr["home_id"].ToString();
                    ddl_home_list.Enabled = false;


                    //DateTime date_begin = new DateTime();
                    //date_begin = Convert.ToDateTime(dr["ip_date_begin"]);
                    //lbl_ip_date_begin.Text = date_begin.Month.ToString() + "/" + date_begin.Day.ToString() + "/" + date_begin.Year.ToString();


                    DateTime date_end = new DateTime();
                    date_end = Convert.ToDateTime(dr["ip_date_end"]);
                    date_end = date_end.AddDays(1);
                    lbl_min_renewal_date.Text = date_end.Month.ToString() + "/" + date_end.Day.ToString() + "/" + date_end.Year.ToString();


                    h_date_begin_d.Value = date_end.Day.ToString();
                    h_date_begin_m.Value = date_end.Month.ToString();
                    h_date_begin_y.Value = date_end.Year.ToString();

                    ddl_ip_date_begin_d.SelectedValue = date_end.Day.ToString();
                    ddl_ip_date_begin_m.SelectedValue = date_end.Month.ToString();
                    ddl_ip_date_begin_y.SelectedValue = date_end.Year.ToString();

                }

            }



            //finally
            {
               // conn.Close();
            }



            cmd2.CommandType = CommandType.StoredProcedure;

            int pending_insurance_policy_id = 0;

            try
            {
                // conn.Open();
                //Add the params
                cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd2.Parameters.Add("@ip_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["ip_id"]);
                cmd2.Parameters.Add("@pending_insurance_policy_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                //execute the insert
                cmd2.ExecuteReader();
                pending_insurance_policy_id = Convert.ToInt32(cmd2.Parameters["@pending_insurance_policy_id"].Value);
            }
            finally
            {
                conn.Close();
            }



            if (pending_insurance_policy_id > 0)
            {
                tb_insurance_policy_renew.Visible = false;
               // btn_submit.Enabled = false;
                insurance_policy_pending_link.NavigateUrl = "insurance_policy_pending_view.aspx?ip_id=" + Request.QueryString["ip_id"]
                                                    + "&pending_ip_id=" + pending_insurance_policy_id.ToString();
                insurance_policy_pending_link.Visible = true;
                lbl_already_pending_insurance_policy.Visible = true;


                tb_already_pending_insurance_policy.Visible = true;
                chk_fi_add.Visible = false;
                chk_ic_add.Visible = false;

            }







        }



    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chk_financial_institution_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_financial_institution.Checked == true)
        {
            ddl_ic_list.Enabled = false;
            chk_ic_add.Visible = false;
            chk_ic_add.Checked = false;

            ddl_fi_list.Enabled = true;
            chk_fi_add.Enabled = true;
            chk_fi_add.Visible = true;

            chk_insurance_company.Checked = false;

            panel_ic.Visible = false;



        }

        if (chk_financial_institution.Checked == false)
        {
            panel_fi.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chk_insurance_company_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_insurance_company.Checked == true)
        {
            ddl_fi_list.Enabled = false;
            chk_fi_add.Visible = false;
            chk_fi_add.Checked = false;

            ddl_ic_list.Enabled = true;
            chk_ic_add.Enabled = true;
            chk_ic_add.Visible = true;

            panel_fi.Visible = false;

            chk_financial_institution.Checked = false;
        }
        if (chk_insurance_company.Checked == false)
        {
            panel_ic.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_Click(object sender, EventArgs e)
    {

        Page.Validate();
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        
        DateTime min_date_begin = new DateTime();
        DateTime new_date_begin = new DateTime();
        DateTime new_date_end = new DateTime();


        tiger.Date dt = new tiger.Date();

        new_date_begin = Convert.ToDateTime(dt.DateCulture(ddl_ip_date_begin_m.SelectedValue, ddl_ip_date_begin_d.SelectedValue, ddl_ip_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        new_date_end = Convert.ToDateTime(dt.DateCulture(ddl_ip_date_end_m.SelectedValue, ddl_ip_date_end_d.SelectedValue, ddl_ip_date_end_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        min_date_begin = Convert.ToDateTime(dt.DateCulture(h_date_begin_m.Value, h_date_begin_d.Value, h_date_begin_y.Value, Convert.ToString(Session["_lastCulture"])));

        // if the date of the renewal is greater or equal than the minimum date begin then enter the insurance
        // policy renewal into the database

        if (min_date_begin <= new_date_begin && new_date_end > new_date_begin)
       {
        if (chk_insurance_company.Checked == false && chk_financial_institution.Checked == false)
        {
            // please select an insurance provider
            lbl_message.Visible = true;
            lbl_message.Text = "Please select an insurance provider";


        }

        else
        {
            lbl_message.Visible = false;
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prInsurancePolicyAdd", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@new_ip_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@home_id", SqlDbType.VarChar, 50).Value = Convert.ToInt32(ddl_home_list.SelectedValue);

                if (chk_insurance_company.Checked == true)
                {
                    cmd.Parameters.Add("@ip_provider_type", SqlDbType.VarChar, 50).Value = "insurance company";
                    cmd.Parameters.Add("@ip_name", SqlDbType.VarChar, 50).Value = Convert.ToString(ddl_ic_list.SelectedItem);

                }

                if (chk_financial_institution.Checked == true)
                {
                    cmd.Parameters.Add("@ip_provider_type", SqlDbType.VarChar, 50).Value = "financial institution";
                    cmd.Parameters.Add("@ip_name", SqlDbType.VarChar, 50).Value = Convert.ToString(ddl_fi_list.SelectedItem);


                }
                string upload_pic_path = "";
                if (upload_pic.HasFile)
                    try
                    {
                        // upload_pic.PostedFile.SaveAs(Server.MapPath("/sinfo/")+upload_pic.PostedFile.FileName);
                        /*  Label1.Text = "File name: " +
                               appliance_photo.PostedFile.FileName + "<br>" +
                               appliance_photo.PostedFile.ContentLength + " kb<br>" +
                               "Content type: " +
                               appliance_photo.PostedFile.ContentType; */
                        upload_pic_path = upload_pic.PostedFile.FileName;
                        lbl_mess.Text = upload_pic.PostedFile.FileName;
                        // int last_index = appliance_photo_name.LastIndexOf('\\');
                        // upload_pic_path = appliance_photo_name.Substring(last_index + 1);
                    }
                    catch (Exception ex)
                    {
                        lbl_mess.Text = "ERROR: " + ex.Message.ToString();
                    }
                else
                {
                    // Label1.Text = "You have not specified a file.";
                }


                cmd.Parameters.Add("@ip_number", SqlDbType.VarChar, 50).Value = ip_number.Text;
                cmd.Parameters.Add("@ip_amount_covered", SqlDbType.Money).Value = Convert.ToDecimal(ip_amount_covered.Text);

                cmd.Parameters.Add("@ip_insurance_type", SqlDbType.Money).Value = Convert.ToInt32(ddl_insurance_type.SelectedValue);

                cmd.Parameters.Add("@ip_pic_path", SqlDbType.VarChar, 100).Value = upload_pic_path;

                cmd.Parameters.Add("@ip_deductible", SqlDbType.Money).Value = Convert.ToDecimal(ip_deductible.Text);
                cmd.Parameters.Add("@ip_year_premium", SqlDbType.Money).Value = Convert.ToDecimal(ip_year_premium.Text);
                cmd.Parameters.Add("@ip_monthly_payment", SqlDbType.Money).Value = Convert.ToDecimal(ip_monthly_payment.Text);




               // tiger.Date df = new tiger.Date();

                cmd.Parameters.Add("@ip_date_begin", SqlDbType.DateTime).Value = Convert.ToDateTime(dt.DateCulture(ddl_ip_date_begin_m.SelectedValue, ddl_ip_date_begin_d.SelectedValue, ddl_ip_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
                cmd.Parameters.Add("@ip_date_end", SqlDbType.DateTime).Value = Convert.ToDateTime(dt.DateCulture(ddl_ip_date_end_m.SelectedValue, ddl_ip_date_end_d.SelectedValue, ddl_ip_date_end_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));





                //execute the insert
                cmd.ExecuteReader();
                //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

                if (Convert.ToInt32(cmd.Parameters["@new_ip_id"].Value) > 0)
                    result.InnerHtml = Resources.Resource.lbl_add_successfull;

                if (Convert.ToInt32(cmd.Parameters["@new_ip_id"].Value) == -2)
                    result.InnerHtml = "Time line unavailable, on this property for the type of insurance";

            }
            catch (Exception error)
            {
                tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
                //Response.Redirect("home_main.aspx");
                //  error.ToString();
                result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
            }
        }
      }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_fi_Click(object sender, EventArgs e)
    {

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        int new_fi = 0;
        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prFinancialInstitutionAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@new_fi_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@fi_name", SqlDbType.VarChar, 50).Value = fi_name.Text;
            cmd.Parameters.Add("@fi_website", SqlDbType.VarChar, 200).Value = fi_website.Text;

            cmd.Parameters.Add("@fi_addr", SqlDbType.VarChar, 50).Value = fi_addr.Text;
            cmd.Parameters.Add("@fi_city", SqlDbType.VarChar, 50).Value = fi_city.Text;
            cmd.Parameters.Add("@fi_prov", SqlDbType.VarChar, 50).Value = fi_prov.Text;
            cmd.Parameters.Add("@fi_pc", SqlDbType.VarChar, 50).Value = fi_pc.Text;
            cmd.Parameters.Add("@fi_tel", SqlDbType.VarChar, 50).Value = fi_tel.Text;
            cmd.Parameters.Add("@fi_contact_fname", SqlDbType.VarChar, 50).Value = fi_contact_fname.Text;
            cmd.Parameters.Add("@fi_contact_lname", SqlDbType.VarChar, 50).Value = fi_contact_lname.Text;
            cmd.Parameters.Add("@fi_contact_tel", SqlDbType.VarChar, 50).Value = fi_contact_tel.Text;
            cmd.Parameters.Add("@fi_contact_email", SqlDbType.VarChar, 50).Value = fi_contact_email.Text;
            cmd.Parameters.Add("@fi_contact_fax", SqlDbType.VarChar, 50).Value = fi_contact_fax.Text;
            cmd.Parameters.Add("@country_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_fi_country_list.SelectedValue);
            cmd.Parameters.Add("@fi_com", SqlDbType.Text).Value = fi_com.Text;




            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            new_fi = Convert.ToInt32(cmd.Parameters["@new_fi_id"].Value);
            if (new_fi > 0)
                result.InnerHtml = Resources.Resource.lbl_add_successfull;


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }


        tiger.FinancialInstitution fi = new tiger.FinancialInstitution(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ddl_fi_list.DataSource = fi.getFinancialInstitutionList(Convert.ToInt32(Session["schema_id"]));
        ddl_fi_list.DataBind();
        ddl_fi_list.SelectedValue = Convert.ToString(new_fi);
        panel_fi.Visible = false;
        chk_fi_add.Checked = false;


    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_ic_Click(object sender, EventArgs e)
    {

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        int new_ic = 0;
        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prInsuranceCompanyAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@new_ic_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@ic_name", SqlDbType.VarChar, 50).Value = ic_name.Text;
            cmd.Parameters.Add("@ic_website", SqlDbType.VarChar, 200).Value = ic_website.Text;

            cmd.Parameters.Add("@ic_addr", SqlDbType.VarChar, 50).Value = ic_addr.Text;
            cmd.Parameters.Add("@ic_city", SqlDbType.VarChar, 50).Value = ic_city.Text;
            cmd.Parameters.Add("@ic_prov", SqlDbType.VarChar, 50).Value = ic_prov.Text;
            cmd.Parameters.Add("@ic_pc", SqlDbType.VarChar, 50).Value = ic_pc.Text;
            cmd.Parameters.Add("@ic_tel", SqlDbType.VarChar, 50).Value = ic_tel.Text;
            cmd.Parameters.Add("@ic_contact_fname", SqlDbType.VarChar, 50).Value = ic_contact_fname.Text;
            cmd.Parameters.Add("@ic_contact_lname", SqlDbType.VarChar, 50).Value = ic_contact_lname.Text;
            cmd.Parameters.Add("@ic_contact_tel", SqlDbType.VarChar, 50).Value = ic_contact_tel.Text;
            cmd.Parameters.Add("@ic_contact_email", SqlDbType.VarChar, 50).Value = ic_contact_email.Text;
            cmd.Parameters.Add("@ic_contact_fax", SqlDbType.VarChar, 50).Value = ic_contact_fax.Text;
            cmd.Parameters.Add("@country_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_ic_country_list.SelectedValue);
            cmd.Parameters.Add("@ic_com", SqlDbType.Text).Value = ic_com.Text;




            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            new_ic = Convert.ToInt32(cmd.Parameters["@new_ic_id"].Value);
            if (new_ic > 0)
                result.InnerHtml = Resources.Resource.lbl_add_successfull;


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }
        tiger.InsuranceCompany ic = new tiger.InsuranceCompany(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ddl_ic_list.DataSource = ic.getInsuranceCompanyList(Convert.ToInt32(Session["schema_id"]));
        ddl_ic_list.DataBind();
        ddl_ic_list.SelectedValue = Convert.ToString(new_ic);
        panel_ic.Visible = false;
        chk_ic_add.Checked = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chk_ic_add_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_ic_add.Checked == true)
        {
            panel_ic.Visible = true;
            panel_fi.Visible = false;
        }

        if (chk_ic_add.Checked == false)
        {
            panel_ic.Visible = false;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chk_fi_add_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_fi_add.Checked == true)
        {
            panel_ic.Visible = false;
            panel_fi.Visible = true;
        }
        if (chk_fi_add.Checked == false)
        {
            panel_fi.Visible = false;
        }
    }
}
