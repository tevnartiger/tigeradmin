﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="insurance_policy_expiration.aspx.cs" Inherits="manager_insurancepolicy_insurance_policy_expiration" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


 <b><asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lbl_u_insurance_policy_expiration%>"/> 
               </b><br /><br />

<table>
 <tr>
     <td  >
       <asp:Label ID="lbl_property" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_property %>" style="font-weight: 700" />
      </td>
         <td  >
                               :
                               <asp:DropDownList ID="ddl_home_list" runat="server" AutoPostBack="true" 
                                   DataTextField="home_name" DataValueField="home_id" 
                                   OnSelectedIndexChanged="ddl_home_list_SelectedIndexChanged">
                               </asp:DropDownList>
                               &nbsp;</td>
                               
        
   </tr>
                      
</table>



 <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b>
                <asp:Label ID="Label42" runat="server" Text="<%$ Resources:Resource, lbl_less31%>"/></b>
                </td>
            </tr>
        </table>
     
<br />

 <asp:GridView ID="gv_insurance_policy_list1" 
    runat="server" AutoGenerateColumns="false" GridLines="Both"  
        AlternatingRowStyle-BackColor="#F0F0F6"    HeaderStyle-BackColor="AliceBlue" 
         Width="100%" BorderColor="#CDCDCD" BorderWidth="1"
       AllowPaging="true"  PageSize="10"  OnPageIndexChanging="gv_insurance_policy_list1_PageIndexChanging"
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" >
            
    <Columns>
    
    <asp:BoundField DataField="home_name"   HeaderText="<%$ Resources:Resource, lbl_home_name %>" />
    <asp:BoundField DataField="ip_name" HeaderText="<%$ Resources:Resource, lbl_insurance_provider %>" />
    <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_insurance_type %>"   >
               <ItemTemplate>
               <asp:Label  runat="server"  ID="lbl_insurance_type"   
                 Text='<%#Get_InsuranceType(Convert.ToInt32(Eval("ip_insurance_type")))%>'    /> 
               </ItemTemplate>
               </asp:TemplateField>
               
    <asp:BoundField DataField="ip_date_begin" DataFormatString="{0:M-dd-yyyy}"  HtmlEncode="false" HeaderText="<%$ Resources:Resource, lbl_date_begin %>" />
    <asp:BoundField DataField="ip_date_end" DataFormatString="{0:M-dd-yyyy}"  HtmlEncode="false"  HeaderText="<%$ Resources:Resource, lbl_date_end %>"/> 
    <asp:BoundField DataField="amount_days_left" HeaderText="<%$ Resources:Resource, lbl_days %>" />
    <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_view %>"
     DataNavigateUrlFields="ip_id" 
     DataNavigateUrlFormatString="~/manager/insurancepolicy/insurance_policy_view.aspx?ip_id={0}" 
      HeaderText= "<%$ Resources:Resource, lbl_view%>" />
    </Columns>
    </asp:GridView>
    
    
    <br />
   <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b>
                <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_between_31and60%>"/></b>
                </td>
            </tr>
        </table>
     
  <br />

    
     <asp:GridView ID="gv_insurance_policy_list2" 
    runat="server" AutoGenerateColumns="false" GridLines="Both"  
        AlternatingRowStyle-BackColor="#F0F0F6"  HeaderStyle-BackColor="#F0F0F6"
         Width="100%" BorderColor="#CDCDCD" BorderWidth="1"
       AllowPaging="true"  PageSize="10"  OnPageIndexChanging="gv_insurance_policy_list2_PageIndexChanging"
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" >
              
    <Columns>
    
    <asp:BoundField DataField="home_name"   HeaderText="<%$ Resources:Resource, lbl_home_name %>" />
    <asp:BoundField DataField="ip_name" HeaderText="<%$ Resources:Resource, lbl_insurance_provider %>" />
    <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_insurance_type %>"   >
               <ItemTemplate>
               <asp:Label  runat="server"  ID="lbl_insurance_type"   
                 Text='<%#Get_InsuranceType(Convert.ToInt32(Eval("ip_insurance_type")))%>'    /> 
               </ItemTemplate>
               </asp:TemplateField>
               
    <asp:BoundField DataField="ip_date_begin" DataFormatString="{0:M-dd-yyyy}"  HtmlEncode="false" HeaderText="<%$ Resources:Resource, lbl_date_begin %>" />
    <asp:BoundField DataField="ip_date_end" DataFormatString="{0:M-dd-yyyy}"  HtmlEncode="false"  HeaderText="<%$ Resources:Resource, lbl_date_end %>"/> 
    <asp:BoundField DataField="amount_days_left" HeaderText="<%$ Resources:Resource, lbl_days %>" />
    <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_view %>"
     DataNavigateUrlFields="ip_id" 
     DataNavigateUrlFormatString="~/manager/insurancepolicy/insurance_policy_view.aspx?ip_id={0}" 
      HeaderText= "<%$ Resources:Resource, lbl_view%>" />
    </Columns>
    </asp:GridView>
    
    <br />
    
    
     <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b>
                <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_between_61and90%>"/></b>
                </td>
            </tr>
        </table>
     
<br />

     <asp:GridView ID="gv_insurance_policy_list3" 
    runat="server" AutoGenerateColumns="false" GridLines="Both"  
   AlternatingRowStyle-BackColor="#F0F0F6"  HeaderStyle-BackColor="#F0F0F6"
         Width="100%" BorderColor="#CDCDCD" BorderWidth="1"
       AllowPaging="true"  PageSize="10"  OnPageIndexChanging="gv_insurance_policy_list3_PageIndexChanging"
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" >
    
    <Columns>
    
    <asp:BoundField DataField="home_name"   HeaderText="<%$ Resources:Resource, lbl_home_name %>" />
    <asp:BoundField DataField="ip_name" HeaderText="<%$ Resources:Resource, lbl_insurance_provider %>" />
    <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_insurance_type %>"   >
               <ItemTemplate>
               <asp:Label  runat="server"  ID="lbl_insurance_type"   
                 Text='<%#Get_InsuranceType(Convert.ToInt32(Eval("ip_insurance_type")))%>'    /> 
               </ItemTemplate>
               </asp:TemplateField>
               
    <asp:BoundField DataField="ip_date_begin" DataFormatString="{0:M-dd-yyyy}"  HtmlEncode="false" HeaderText="<%$ Resources:Resource, lbl_date_begin %>" />
    <asp:BoundField DataField="ip_date_end" DataFormatString="{0:M-dd-yyyy}"  HtmlEncode="false"  HeaderText="<%$ Resources:Resource, lbl_date_end %>"/> 
    <asp:BoundField DataField="amount_days_left" HeaderText="<%$ Resources:Resource, lbl_days %>" />
    <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_view %>"
     DataNavigateUrlFields="ip_id" 
     DataNavigateUrlFormatString="~/manager/insurancepolicy/insurance_policy_view.aspx?ip_id={0}" 
      HeaderText= "<%$ Resources:Resource, lbl_view%>" />
    </Columns>
    </asp:GridView>
    
    
    <br />
    
    
    
</asp:Content>

