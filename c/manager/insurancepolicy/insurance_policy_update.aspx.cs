using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.AccountObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : nov 2 , 2007
/// </summary>
public partial class insurancepolicy_insurance_policy_update : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!RegEx.IsInteger(Request.QueryString["ip_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        reg_ip_amount_covered.ValidationExpression = RegEx.getMoney();
        reg_ip_deductible.ValidationExpression = RegEx.getMoney();
        reg_ip_monthly_payment.ValidationExpression = RegEx.getMoney();
        reg_ip_number.ValidationExpression = RegEx.getAlphaNumeric();
        reg_ip_year_premium.ValidationExpression = RegEx.getMoney();
             

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        /////////////
        /////////////// SECURITY CHECK BEGIN   //////////////
        //////////////
        AccountObjectAuthorization ipAuthorization = new AccountObjectAuthorization(strconn);

        if (!ipAuthorization.InsurancePolicy(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["ip_id"])))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        /////////////
        /////////////// SECURITY CHECK END  //////////////
        //////////

        if (!Page.IsPostBack)
        {
            // ajouter le datareader

            tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_home_list.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
            ddl_home_list.DataBind();

            tiger.Country ddl_ic = new tiger.Country(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_ic_country_list.DataSource = ddl_ic.getCountryList();
            ddl_ic_country_list.DataBind();

            tiger.Country ddl_fi = new tiger.Country(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_fi_country_list.DataSource = ddl_fi.getCountryList();
            ddl_fi_country_list.DataBind();

            tiger.InsuranceCompany ic = new tiger.InsuranceCompany(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_ic_list.DataSource = ic.getInsuranceCompanyList(Convert.ToInt32(Session["schema_id"]));
            ddl_ic_list.DataBind();

            tiger.FinancialInstitution fi = new tiger.FinancialInstitution(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_fi_list.DataSource = fi.getFinancialInstitutionList(Convert.ToInt32(Session["schema_id"]));
            ddl_fi_list.DataBind();

            ddl_fi_list.Enabled = false;
            chk_insurance_company.Checked = true;
            chk_ic_add.Visible = true;
            chk_fi_add.Visible = false;

            panel_ic.Visible = false;
            panel_fi.Visible = false;



            string insurance_provider_type = "";
            string insurance_provider_name = "";




            int insurance_type = 0;
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prInsurancePolicyView", conn);
            cmd.CommandType = CommandType.StoredProcedure;



            //Add the params
            cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("ip_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["ip_id"]);
            //  try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                

                while (dr.Read() == true)
                {
                    ddl_home_list.SelectedValue = dr["home_id"].ToString();


                    insurance_provider_name = dr["ip_name"].ToString();
                    insurance_provider_type = dr["ip_provider_type"].ToString();

                    insurance_type = Convert.ToInt32(dr["ip_insurance_type"]);

                    ip_number.Text = dr["ip_number"].ToString();
                    ip_amount_covered.Text = dr["ip_amount_covered"].ToString();
                    ip_deductible.Text = dr["ip_deductible"].ToString();
                    ip_year_premium.Text = dr["ip_year_premium"].ToString();

                    ip_monthly_payment.Text = dr["ip_monthly_payment"].ToString();

                    DateTime date_begin = new DateTime();
                    date_begin = Convert.ToDateTime(dr["ip_date_begin"]);
                    if (date_begin != null)
                    {
                      
                      ddl_ip_date_begin_y.SelectedValue = date_begin.Year.ToString();
                      ddl_ip_date_begin_d.Text = date_begin.Day.ToString();
                      ddl_ip_date_begin_m.SelectedValue = date_begin.Month.ToString();
                    }

                    DateTime date_end = new DateTime();
                     date_end = Convert.ToDateTime(dr["ip_date_end"]);
                    if (date_end != null)
                    {
                      
                      ddl_ip_date_end_y.SelectedValue = date_end.Year.ToString();
                      ddl_ip_date_end_d.Text = date_end.Day.ToString();
                      ddl_ip_date_end_m.SelectedValue = date_end.Month.ToString();
                    }

                }

            }



            //finally
            {
                conn.Close();
            }

            if (insurance_provider_type == "insurance company")
            {
                 ddl_ic_list.SelectedItem.Value = insurance_provider_name ;
                 chk_financial_institution.Checked = false ;
                 chk_insurance_company.Checked = true;
            }
            if (insurance_provider_type == "financial institution")
            {
                ddl_fi_list.SelectedValue = insurance_provider_name;
                chk_financial_institution.Checked = true;
                chk_insurance_company.Checked = false;
            }



            if (insurance_type == 1)
            {
                ddl_insurance_type.SelectedIndex= 0;
            }

            if (insurance_type == 2)
            {
                ddl_insurance_type.SelectedIndex= 1;
            }

            if (insurance_type == 3)
            {
               ddl_insurance_type.SelectedIndex = 2;
            }
       

            //   lbl_message.Text = "Hello World";
        }



    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chk_financial_institution_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_financial_institution.Checked == true)
        {
            ddl_ic_list.Enabled = false;
            chk_ic_add.Visible = false;
            chk_ic_add.Checked = false;

            ddl_fi_list.Enabled = true;
            chk_fi_add.Enabled = true;
            chk_fi_add.Visible = true;

            chk_insurance_company.Checked = false;

            panel_ic.Visible = false;



        }

        if (chk_financial_institution.Checked == false)
        {
            panel_fi.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chk_insurance_company_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_insurance_company.Checked == true)
        {
            ddl_fi_list.Enabled = false;
            chk_fi_add.Visible = false;
            chk_fi_add.Checked = false;

            ddl_ic_list.Enabled = true;
            chk_ic_add.Enabled = true;
            chk_ic_add.Visible = true;

            panel_fi.Visible = false;

            chk_financial_institution.Checked = false;
        }
        if (chk_insurance_company.Checked == false)
        {
            panel_ic.Visible = false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_Click(object sender, EventArgs e)
    {


        Page.Validate();
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }


        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        

        if (chk_insurance_company.Checked == false && chk_financial_institution.Checked == false)
        {
            // please select an insurance provider
            lbl_message.Visible = true;
            lbl_message.Text = "Please select an insurance provider";


        }

        else
        {
            lbl_message.Visible = false;
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prInsurancePolicyUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@update_successful", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@ip_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["ip_id"]); ;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value =  Convert.ToInt32(ddl_home_list.SelectedValue);

                if (chk_insurance_company.Checked == true)
                {
                    cmd.Parameters.Add("@ip_provider_type", SqlDbType.VarChar, 50).Value = "insurance company";
                    cmd.Parameters.Add("@ip_name", SqlDbType.VarChar, 50).Value = Convert.ToString(ddl_ic_list.SelectedItem);

                }

                if (chk_financial_institution.Checked == true)
                {
                    cmd.Parameters.Add("@ip_provider_type", SqlDbType.VarChar, 50).Value = "financial institution";
                    cmd.Parameters.Add("@ip_name", SqlDbType.VarChar, 50).Value = Convert.ToString(ddl_fi_list.SelectedItem);


                }
                string upload_pic_path = "";
                if (upload_pic.HasFile)
                    try
                    {
                        // upload_pic.PostedFile.SaveAs(Server.MapPath("/sinfo/")+upload_pic.PostedFile.FileName);
                        /*  Label1.Text = "File name: " +
                               appliance_photo.PostedFile.FileName + "<br>" +
                               appliance_photo.PostedFile.ContentLength + " kb<br>" +
                               "Content type: " +
                               appliance_photo.PostedFile.ContentType; */
                        upload_pic_path = upload_pic.PostedFile.FileName;
                        lbl_mess.Text = upload_pic.PostedFile.FileName;
                        // int last_index = appliance_photo_name.LastIndexOf('\\');
                        // upload_pic_path = appliance_photo_name.Substring(last_index + 1);
                    }
                    catch (Exception ex)
                    {
                        lbl_mess.Text = "ERROR: " + ex.Message.ToString();
                    }
                else
                {
                    // Label1.Text = "You have not specified a file.";
                }


                cmd.Parameters.Add("@ip_number", SqlDbType.VarChar, 50).Value = ip_number.Text;
                cmd.Parameters.Add("@ip_amount_covered", SqlDbType.Money).Value = Convert.ToDecimal(ip_amount_covered.Text);

                cmd.Parameters.Add("@ip_insurance_type", SqlDbType.Money).Value = Convert.ToInt32(ddl_insurance_type.SelectedValue);

                cmd.Parameters.Add("@ip_pic_path", SqlDbType.VarChar, 100).Value = upload_pic_path;

                cmd.Parameters.Add("@ip_deductible", SqlDbType.Money).Value = Convert.ToDecimal(ip_deductible.Text);
                cmd.Parameters.Add("@ip_year_premium", SqlDbType.Money).Value = Convert.ToDecimal(ip_year_premium.Text);
                cmd.Parameters.Add("@ip_monthly_payment", SqlDbType.Money).Value = Convert.ToDecimal(ip_monthly_payment.Text);


                tiger.Date df = new tiger.Date();


                cmd.Parameters.Add("@ip_date_begin", SqlDbType.DateTime).Value = Convert.ToDateTime(df.DateCulture(ddl_ip_date_begin_m.SelectedValue, ddl_ip_date_begin_d.SelectedValue, ddl_ip_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
                cmd.Parameters.Add("@ip_date_end", SqlDbType.DateTime).Value = Convert.ToDateTime(df.DateCulture(ddl_ip_date_end_m.SelectedValue, ddl_ip_date_end_d.SelectedValue, ddl_ip_date_end_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
               


                //execute the insert
                cmd.ExecuteReader();
                //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

                if (Convert.ToInt32(cmd.Parameters["@update_successful"].Value) == 0)
                    result.InnerHtml = " update successful";


            }
            catch (Exception error)
            {
                tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
                //Response.Redirect("home_main.aspx");
                //  error.ToString();
                result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
            }




        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_fi_Click(object sender, EventArgs e)
    {

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        int new_fi = 0;
        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prFinancialInstitutionAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@new_fi_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@fi_name", SqlDbType.VarChar, 50).Value = fi_name.Text;
            cmd.Parameters.Add("@fi_website", SqlDbType.VarChar, 200).Value = fi_website.Text;

            cmd.Parameters.Add("@fi_addr", SqlDbType.VarChar, 50).Value = fi_addr.Text;
            cmd.Parameters.Add("@fi_city", SqlDbType.VarChar, 50).Value = fi_city.Text;
            cmd.Parameters.Add("@fi_prov", SqlDbType.VarChar, 50).Value = fi_prov.Text;
            cmd.Parameters.Add("@fi_pc", SqlDbType.VarChar, 50).Value = fi_pc.Text;
            cmd.Parameters.Add("@fi_tel", SqlDbType.VarChar, 50).Value = fi_tel.Text;
            cmd.Parameters.Add("@fi_contact_fname", SqlDbType.VarChar, 50).Value = fi_contact_fname.Text;
            cmd.Parameters.Add("@fi_contact_lname", SqlDbType.VarChar, 50).Value = fi_contact_lname.Text;
            cmd.Parameters.Add("@fi_contact_tel", SqlDbType.VarChar, 50).Value = fi_contact_tel.Text;
            cmd.Parameters.Add("@fi_contact_email", SqlDbType.VarChar, 50).Value = fi_contact_email.Text;
            cmd.Parameters.Add("@fi_contact_fax", SqlDbType.VarChar, 50).Value = fi_contact_fax.Text;
            cmd.Parameters.Add("@country_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_fi_country_list.SelectedValue);
            cmd.Parameters.Add("@fi_com", SqlDbType.Text).Value = fi_com.Text;




            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            new_fi = Convert.ToInt32(cmd.Parameters["@new_fi_id"].Value);
            if (new_fi > 0)
                result.InnerHtml = " add successful";


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }


        tiger.FinancialInstitution fi = new tiger.FinancialInstitution(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ddl_fi_list.DataSource = fi.getFinancialInstitutionList(Convert.ToInt32(Session["schema_id"]));
        ddl_fi_list.DataBind();
        ddl_fi_list.SelectedValue = Convert.ToString(new_fi);
        panel_fi.Visible = false;
        chk_fi_add.Checked = false;


    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_ic_Click(object sender, EventArgs e)
    {

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        int new_ic = 0;
        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prInsuranceCompanyAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@new_ic_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@ic_name", SqlDbType.VarChar, 50).Value = ic_name.Text;
            cmd.Parameters.Add("@ic_website", SqlDbType.VarChar, 200).Value = ic_website.Text;

            cmd.Parameters.Add("@ic_addr", SqlDbType.VarChar, 50).Value = ic_addr.Text;
            cmd.Parameters.Add("@ic_city", SqlDbType.VarChar, 50).Value = ic_city.Text;
            cmd.Parameters.Add("@ic_prov", SqlDbType.VarChar, 50).Value = ic_prov.Text;
            cmd.Parameters.Add("@ic_pc", SqlDbType.VarChar, 50).Value = ic_pc.Text;
            cmd.Parameters.Add("@ic_tel", SqlDbType.VarChar, 50).Value = ic_tel.Text;
            cmd.Parameters.Add("@ic_contact_fname", SqlDbType.VarChar, 50).Value = ic_contact_fname.Text;
            cmd.Parameters.Add("@ic_contact_lname", SqlDbType.VarChar, 50).Value = ic_contact_lname.Text;
            cmd.Parameters.Add("@ic_contact_tel", SqlDbType.VarChar, 50).Value = ic_contact_tel.Text;
            cmd.Parameters.Add("@ic_contact_email", SqlDbType.VarChar, 50).Value = ic_contact_email.Text;
            cmd.Parameters.Add("@ic_contact_fax", SqlDbType.VarChar, 50).Value = ic_contact_fax.Text;
            cmd.Parameters.Add("@country_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_ic_country_list.SelectedValue);
            cmd.Parameters.Add("@ic_com", SqlDbType.Text).Value = ic_com.Text;




            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            new_ic = Convert.ToInt32(cmd.Parameters["@new_ic_id"].Value);
            if (new_ic > 0)
                result.InnerHtml = " add successful";


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }
        tiger.InsuranceCompany ic = new tiger.InsuranceCompany(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ddl_ic_list.DataSource = ic.getInsuranceCompanyList(Convert.ToInt32(Session["schema_id"]));
        ddl_ic_list.DataBind();
        ddl_ic_list.SelectedValue = Convert.ToString(new_ic);
        panel_ic.Visible = false;
        chk_ic_add.Checked = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chk_ic_add_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_ic_add.Checked == true)
        {
            panel_ic.Visible = true;
            panel_fi.Visible = false;
        }

        if (chk_ic_add.Checked == false)
        {
            panel_ic.Visible = false;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chk_fi_add_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_fi_add.Checked == true)
        {
            panel_ic.Visible = false;
            panel_fi.Visible = true;
        }
        if (chk_fi_add.Checked == false)
        {
            panel_fi.Visible = false;
        }
    }
}
