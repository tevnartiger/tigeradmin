<%@ Page Language="C#"   MaintainScrollPositionOnPostback="true" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="insurance_policy_add.aspx.cs" Inherits="insurancepolicy_insurance_policy_add" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    
        <b>   <asp:Label ID="Label11" runat="server" Text="<%$ Resources:Resource, lbl_u_insurance_policy %>"></asp:Label></b><br />
        <br />
        <asp:CheckBox ID="chk_ic_add" runat="server" AutoPostBack="True" OnCheckedChanged="chk_ic_add_CheckedChanged"
            Text="<%$ Resources:Resource, lbl_add_insurance_company %>" /><br />
        <asp:CheckBox ID="chk_fi_add" runat="server" AutoPostBack="True" OnCheckedChanged="chk_fi_add_CheckedChanged"
            Text="<%$ Resources:Resource, lbl_add_financial_institution %>" /><br />
        <table>
            <tr>
                <td valign= top>
                    <asp:Label ID="lbl_message" runat="server"></asp:Label><br />
        <table bgcolor="#ffffcc" >
            <tr>
                <td valign="top">
                     <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_home_name %>"></asp:Label></td>
                <td>
                    :
                    <asp:DropDownList ID="ddl_home_list" DataTextField="home_name" DataValueField="home_id" runat="server">
                    </asp:DropDownList><br />
                    <br />
                </td>
            </tr>
            <tr>
                <td valign=top > 
                     <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_insurance_provider %>"></asp:Label></td>
                <td >
                    &nbsp;
                    <asp:DropDownList ID="ddl_ic_list" DataTextField="ic_name" DataValueField="ic_id" runat="server">
                    </asp:DropDownList>&nbsp;<asp:CheckBox ID="chk_insurance_company" AutoPostBack=true runat="server" Text="<%$ Resources:Resource, lbl_insurance_company %>" OnCheckedChanged="chk_insurance_company_CheckedChanged" /><br />
                    &nbsp; <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lbl_u_or %>"></asp:Label>&nbsp;<br />
                    &nbsp;
                    <asp:DropDownList ID="ddl_fi_list" DataTextField="fi_name" DataValueField="fi_id" runat="server">
                    </asp:DropDownList>&nbsp;
                    <asp:CheckBox ID="chk_financial_institution" AutoPostBack=true runat="server" Text="<%$ Resources:Resource, lbl_financial_institution %>" OnCheckedChanged="chk_financial_institution_CheckedChanged" /><br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lbl_insurance_type %>"></asp:Label></td>
                <td>
                    :
                    <asp:DropDownList ID="ddl_insurance_type" runat="server">
                        <asp:ListItem Text="<%$ Resources:Resource, lbl_property_insurance %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, lbl_mortgage_insurance %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, lbl_mortgage_life_insurance %>" Value="3"></asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lbl_policy_number %>"></asp:Label></td>
                <td >
                    :
                    <asp:TextBox ID="ip_number" runat="server"></asp:TextBox>&nbsp;
                    <asp:RequiredFieldValidator 
                        ID="req_ip_number" runat="server"
                         ControlToValidate="ip_number" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                &nbsp;<asp:RegularExpressionValidator ID="reg_ip_number"
                              ControlToValidate="ip_number"  
                        runat="server" ErrorMessage="enter number and letters only">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label14" runat="server" Text="<%$ Resources:Resource, lbl_policy_picture %>"></asp:Label></td>
                <td >
                    :<asp:FileUpload ID="upload_pic" runat="server" /></td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lbl_coverage %>"></asp:Label></td>
                <td >
                    :
                    <asp:TextBox ID="ip_amount_covered" runat="server"></asp:TextBox>&nbsp;
                    <asp:RequiredFieldValidator 
                        ID="req_ip_amount_covered" runat="server"
                         ControlToValidate="ip_amount_covered"  
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                &nbsp;<asp:RegularExpressionValidator ID="reg_ip_amount_covered"
                              ControlToValidate="ip_amount_covered"  
                        runat="server" ErrorMessage="enter an amount">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td style="height: 26px">
                      <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, lbl_deductible %>"></asp:Label></td>
                <td style="height: 26px">
                    :
                    <asp:TextBox ID="ip_deductible" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator ID="reg_ip_deductible"
                              ControlToValidate="ip_deductible"  
                        runat="server" ErrorMessage="enter an amount">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lbl_year_premium %>"></asp:Label> </td>
                <td >
                    :
                    <asp:TextBox ID="ip_year_premium" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator ID="reg_ip_year_premium"
                              ControlToValidate="ip_year_premium"  
                        runat="server" ErrorMessage="enter an amount">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, lbl_monthly_payment %>"></asp:Label></td>
                <td >
                    :
                    <asp:TextBox ID="ip_monthly_payment" runat="server"></asp:TextBox>&nbsp;
                    <asp:RequiredFieldValidator 
                        ID="req_ip_monthly_payment" runat="server" 
                        ControlToValidate="ip_monthly_payment"  
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                &nbsp;<asp:RegularExpressionValidator ID="reg_ip_monthly_payment"
                              ControlToValidate="ip_monthly_payment"  
                        runat="server" ErrorMessage="enter an amount">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label10" runat="server" Text="<%$ Resources:Resource, lbl_date_begin %>"></asp:Label></td>
                <td >
                
                    <asp:DropDownList ID="ddl_ip_date_begin_m" runat="server">
                        <asp:ListItem Text="<%$ Resources:Resource, txt_month %>" Value="0"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;/&nbsp;
                    <asp:DropDownList ID="ddl_ip_date_begin_d" runat="server" >
                     <asp:ListItem Text="<%$ Resources:Resource, txt_day %>" Value="0"></asp:ListItem>
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>&nbsp; &nbsp;/&nbsp;    
                    

                     <asp:DropDownList ID="ddl_ip_date_begin_y" runat="server">
                            <asp:ListItem Text="<%$ Resources:Resource, txt_year %>" Value="0"></asp:ListItem>
                            <asp:ListItem>1950</asp:ListItem>
                            <asp:ListItem>1951</asp:ListItem>
                            <asp:ListItem>1952</asp:ListItem>
                            <asp:ListItem>1953</asp:ListItem>
                            <asp:ListItem>1954</asp:ListItem>
                            <asp:ListItem>1955</asp:ListItem>
                            <asp:ListItem>1956</asp:ListItem>
                            <asp:ListItem>1957</asp:ListItem>
                            <asp:ListItem>1958</asp:ListItem>
                            <asp:ListItem>1959</asp:ListItem>
                            <asp:ListItem>1960</asp:ListItem>
                            <asp:ListItem>1961</asp:ListItem>
                            <asp:ListItem>1962</asp:ListItem>
                            <asp:ListItem>1963</asp:ListItem>
                            <asp:ListItem>1964</asp:ListItem>
                            <asp:ListItem>1965</asp:ListItem>
                            <asp:ListItem>1966</asp:ListItem>
                            <asp:ListItem>1967</asp:ListItem>
                            <asp:ListItem>1968</asp:ListItem>
                            <asp:ListItem>1969</asp:ListItem>
                            <asp:ListItem>1970</asp:ListItem>
                            <asp:ListItem>1971</asp:ListItem>
                            <asp:ListItem>1972</asp:ListItem>
                            <asp:ListItem>1973</asp:ListItem>
                            <asp:ListItem>1974</asp:ListItem>
                            <asp:ListItem>1975</asp:ListItem>
                            <asp:ListItem>1976</asp:ListItem>
                            <asp:ListItem>1977</asp:ListItem>
                            <asp:ListItem>1978</asp:ListItem>
                            <asp:ListItem>1979</asp:ListItem>
                            <asp:ListItem>1980</asp:ListItem>
                            <asp:ListItem>1981</asp:ListItem>
                            <asp:ListItem>1982</asp:ListItem>
                            <asp:ListItem>1983</asp:ListItem>
                            <asp:ListItem>1984</asp:ListItem>
                            <asp:ListItem>1985</asp:ListItem>
                            <asp:ListItem>1986</asp:ListItem>
                            <asp:ListItem>1987</asp:ListItem>
                            <asp:ListItem>1988</asp:ListItem>
                            <asp:ListItem>1989</asp:ListItem>
                            <asp:ListItem>1990</asp:ListItem>
                            <asp:ListItem>1991</asp:ListItem>
                            <asp:ListItem>1992</asp:ListItem>
                            <asp:ListItem>1993</asp:ListItem>
                            <asp:ListItem>1994</asp:ListItem>
                            <asp:ListItem>1995</asp:ListItem>
                            <asp:ListItem>1996</asp:ListItem>
                            <asp:ListItem>1997</asp:ListItem>
                            <asp:ListItem>1998</asp:ListItem>
                            <asp:ListItem>1999</asp:ListItem>
                            <asp:ListItem>2000</asp:ListItem>
                            <asp:ListItem>2001</asp:ListItem>
                            <asp:ListItem>2002</asp:ListItem>
                            <asp:ListItem>2003</asp:ListItem>
                            <asp:ListItem>2004</asp:ListItem>
                            <asp:ListItem>2005</asp:ListItem>
                            <asp:ListItem>2006</asp:ListItem>
                            <asp:ListItem>2007</asp:ListItem>
                            <asp:ListItem>2008</asp:ListItem>
                            <asp:ListItem>2009</asp:ListItem>
                            <asp:ListItem>2010</asp:ListItem>
                            <asp:ListItem>2011</asp:ListItem>
                            <asp:ListItem>2012</asp:ListItem>
                            <asp:ListItem>2013</asp:ListItem>
                            <asp:ListItem>2014</asp:ListItem>
                            <asp:ListItem>2015</asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>
                    
                    
                    
                    
                    
                    
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label12" runat="server" Text="<%$ Resources:Resource, lbl_date_end %>"></asp:Label></td>
                <td>
                    :
                    <asp:DropDownList ID="ddl_ip_date_end_m" runat="server">
                    <asp:ListItem Text="<%$ Resources:Resource, txt_month %>" Value="0"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                       </asp:DropDownList>
                    /
                   <asp:DropDownList ID="ddl_ip_date_end_d" runat="server" >
                     <asp:ListItem Text="<%$ Resources:Resource, txt_day %>" Value="0"></asp:ListItem>
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>&nbsp; &nbsp;/&nbsp;    
                   
                    /
                    <asp:DropDownList ID="ddl_ip_date_end_y" runat="server">
                    <asp:ListItem Text="<%$ Resources:Resource, txt_year %>" Value="0"></asp:ListItem>
                    <asp:ListItem>1950</asp:ListItem>
                            <asp:ListItem>1951</asp:ListItem>
                            <asp:ListItem>1952</asp:ListItem>
                            <asp:ListItem>1953</asp:ListItem>
                            <asp:ListItem>1954</asp:ListItem>
                            <asp:ListItem>1955</asp:ListItem>
                            <asp:ListItem>1956</asp:ListItem>
                            <asp:ListItem>1957</asp:ListItem>
                            <asp:ListItem>1958</asp:ListItem>
                            <asp:ListItem>1959</asp:ListItem>
                            <asp:ListItem>1960</asp:ListItem>
                            <asp:ListItem>1961</asp:ListItem>
                            <asp:ListItem>1962</asp:ListItem>
                            <asp:ListItem>1963</asp:ListItem>
                            <asp:ListItem>1964</asp:ListItem>
                            <asp:ListItem>1965</asp:ListItem>
                            <asp:ListItem>1966</asp:ListItem>
                            <asp:ListItem>1967</asp:ListItem>
                            <asp:ListItem>1968</asp:ListItem>
                            <asp:ListItem>1969</asp:ListItem>
                            <asp:ListItem>1970</asp:ListItem>
                            <asp:ListItem>1971</asp:ListItem>
                            <asp:ListItem>1972</asp:ListItem>
                            <asp:ListItem>1973</asp:ListItem>
                            <asp:ListItem>1974</asp:ListItem>
                            <asp:ListItem>1975</asp:ListItem>
                            <asp:ListItem>1976</asp:ListItem>
                            <asp:ListItem>1977</asp:ListItem>
                            <asp:ListItem>1978</asp:ListItem>
                            <asp:ListItem>1979</asp:ListItem>
                            <asp:ListItem>1980</asp:ListItem>
                            <asp:ListItem>1981</asp:ListItem>
                            <asp:ListItem>1982</asp:ListItem>
                            <asp:ListItem>1983</asp:ListItem>
                            <asp:ListItem>1984</asp:ListItem>
                            <asp:ListItem>1985</asp:ListItem>
                            <asp:ListItem>1986</asp:ListItem>
                            <asp:ListItem>1987</asp:ListItem>
                            <asp:ListItem>1988</asp:ListItem>
                            <asp:ListItem>1989</asp:ListItem>
                            <asp:ListItem>1990</asp:ListItem>
                            <asp:ListItem>1991</asp:ListItem>
                            <asp:ListItem>1992</asp:ListItem>
                            <asp:ListItem>1993</asp:ListItem>
                            <asp:ListItem>1994</asp:ListItem>
                            <asp:ListItem>1995</asp:ListItem>
                            <asp:ListItem>1996</asp:ListItem>
                            <asp:ListItem>1997</asp:ListItem>
                            <asp:ListItem>1998</asp:ListItem>
                            <asp:ListItem>1999</asp:ListItem>
                            <asp:ListItem>2000</asp:ListItem>
                            <asp:ListItem>2001</asp:ListItem>
                            <asp:ListItem>2002</asp:ListItem>
                            <asp:ListItem>2003</asp:ListItem>
                            <asp:ListItem>2004</asp:ListItem>
                            <asp:ListItem>2005</asp:ListItem>
                            <asp:ListItem>2006</asp:ListItem>
                            
                    <asp:ListItem>2007</asp:ListItem>
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                    <asp:ListItem>2011</asp:ListItem>
                    <asp:ListItem>2012</asp:ListItem>
                    <asp:ListItem>2013</asp:ListItem>
                     <asp:ListItem>2014</asp:ListItem>
                            <asp:ListItem>2015</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
        </table>
                    <br />
        <asp:Button ID="btn_submit" runat="server" Text="<%$ Resources:Resource, lbl_submit %>" OnClick="btn_submit_Click"  /></td>
                <td valign = top>
                    <table>
                    <asp:Panel runat="server" ID="panel_ic">
                        <tr>
                            <td>
                           
                                <asp:Label ID="Label15" runat="server" Text="<%$ Resources:Resource, lbl_u_insurance_company %>" ></asp:Label><br />
                        <table bgcolor="#ffffcc" >
                          
                                <tr>
                                    <td>
                                        <asp:Label ID="Label16" runat="server" Text="<%$ Resources:Resource, lbl_name_name %>"></asp:Label></td>
                                    <td>
                                        :
                                        <asp:TextBox ID="ic_name" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label17" runat="server" Text="<%$ Resources:Resource, lbl_website %>"></asp:Label></td>
                                    <td>
                                        :
                                        <asp:TextBox ID="ic_website" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label19" runat="server" Text= "<%$ Resources:Resource, lbl_address %>"></asp:Label>
                                    </td>
                                    <td>
                                        :
                                        <asp:TextBox ID="ic_addr" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label18" runat="server" Text="<%$ Resources:Resource, lbl_tel %>"></asp:Label></td>
                                    <td>
                                        :
                                        <asp:TextBox ID="ic_tel" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label20" runat="server" Text= "<%$ Resources:Resource, lbl_country %>"></asp:Label></td>
                                    <td>
                                        :
                                        <asp:DropDownList ID="ddl_ic_country_list" runat="server" DataTextField="country_name"
                                            DataValueField="country_id">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label21" runat="server" Text= "<%$ Resources:Resource, lbl_prov %>"></asp:Label></td>
                                    <td>
                                        :
                                        <asp:TextBox ID="ic_prov" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label22" runat="server" Text= "<%$ Resources:Resource, lbl_city %>"></asp:Label></td>
                                    <td>
                                        :
                                        <asp:TextBox ID="ic_city" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label23" runat="server" Text= "<%$ Resources:Resource, lbl_pc %>"></asp:Label></td>
                                    <td>
                                        :
                                        <asp:TextBox ID="ic_pc" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label24" runat="server" Text= "<%$ Resources:Resource, lbl_contact_fname %>"></asp:Label></td>
                                    <td>
                                        :
                                        <asp:TextBox ID="ic_contact_fname" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label25" runat="server" Text= "<%$ Resources:Resource, lbl_contact_lname %>"></asp:Label></td>
                                    <td>
                                        :
                                        <asp:TextBox ID="ic_contact_lname" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label26" runat="server" Text= "<%$ Resources:Resource, lbl_contact_tel %>"></asp:Label></td>
                                    <td>
                                        :
                                        <asp:TextBox ID="ic_contact_tel" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label27" runat="server" Text= "<%$ Resources:Resource, lbl_fax %>"></asp:Label></td>
                                    <td>
                                        :
                                        <asp:TextBox ID="ic_contact_fax" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label28" runat="server" Text= "<%$ Resources:Resource, lbl_email %>"></asp:Label></td>
                                    <td>
                                        :
                                        <asp:TextBox ID="ic_contact_email" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:Label ID="Label29" runat="server" Text= "<%$ Resources:Resource, lbl_comments %>"></asp:Label></td>
                                    <td>
                                        <asp:TextBox ID="ic_com" runat="server" Height="100px" TextMode="MultiLine" Width="300px"></asp:TextBox></td>
                                </tr>
                        
                        </table>
                        <br>
                        <asp:Button ID="btn_submit_ic" runat="server" OnClick="btn_submit_ic_Click" Text="<%$ Resources:Resource, lbl_submit %>" />
                        
                            </td>
                        </tr>
                   </asp:Panel>
                   
                   <asp:Panel runat=server ID=panel_fi>
                        <tr>
                            <td>
                            &nbsp; <asp:Label ID="Label13" runat="server" Text="<%$ Resources:Resource, lbl_u_financial_institution %>"></asp:Label><br />
                    <br />
                    <table bgcolor="#ffffcc">
                        <tr>
                            <td>
                                <asp:Label ID="Label30" runat="server" Text="<%$ Resources:Resource, lbl_name_name %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_name" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                               <asp:Label ID="Label31" runat="server" Text="<%$ Resources:Resource, lbl_website %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_website" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label32" runat="server" Text= "<%$ Resources:Resource, lbl_address %>"></asp:Label>
                            </td>
                            <td>
                                :
                                <asp:TextBox ID="fi_addr" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                 <asp:Label ID="Label33" runat="server" Text="<%$ Resources:Resource, lbl_tel %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_tel" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                               
                                        <asp:Label ID="Label34" runat="server" Text= "<%$ Resources:Resource, lbl_country %>"></asp:Label></td>
                            <td>
                                :
                                <asp:DropDownList ID="ddl_fi_country_list" runat="server" DataTextField="country_name"
                                    DataValueField="country_id">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>
                                  <asp:Label ID="Label35" runat="server" Text= "<%$ Resources:Resource, lbl_prov %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_prov" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label36" runat="server" Text= "<%$ Resources:Resource, lbl_city %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_city" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label37" runat="server" Text= "<%$ Resources:Resource, lbl_pc %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_pc" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                               <asp:Label ID="Label38" runat="server" Text= "<%$ Resources:Resource, lbl_contact_fname %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_contact_fname" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                              <asp:Label ID="Label39" runat="server" Text= "<%$ Resources:Resource, lbl_contact_lname %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_contact_lname" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                 <asp:Label ID="Label40" runat="server" Text= "<%$ Resources:Resource, lbl_contact_tel %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_contact_tel" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label41" runat="server" Text= "<%$ Resources:Resource, lbl_fax %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_contact_fax" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label42" runat="server" Text= "<%$ Resources:Resource, lbl_email %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_contact_email" runat="server"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td valign="top">
                                 <asp:Label ID="Label43" runat="server" Text= "<%$ Resources:Resource, lbl_comments %>"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="fi_com" runat="server" Height="100px" TextMode="MultiLine" Width="300px"></asp:TextBox></td>
                        </tr>
                    </table>
                    <br />
                    <br />
                    <asp:Button ID="btn_submit_fi" runat="server" OnClick="btn_submit_fi_Click" Text="<%$ Resources:Resource, lbl_submit %>" />
                    <br /> 
                            </td>
                        </tr>
                    </asp:Panel>
                    </table>
                    
                   
                </td>
            </tr>
        </table>
        <br />
        <asp:Label ID="lbl_mess" runat="server"></asp:Label><br />
        <div id=result runat=server></div>
        <br />
        <br />
    
  </asp:Content>
