<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="insurance_policy_list.aspx.cs" Inherits="insurancepolicy_insurance_policy_list" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<br />

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
   
    <cc1:TabContainer ID="TabContainer1"  Width="100%" runat="server"  >
    <cc1:TabPanel ID="tab1" Width="100%"  BackColor="AliceBlue" runat="server" HeaderText="<%$ Resources:Resource, lbl_u_insurance_policy %>">
        <ContentTemplate  >
            
    <div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label7" runat="server" style="font-weight: 700" 
                        Text="<%$ Resources:Resource, lbl_property %>" />
                </td>
                <td>
                    :
                    <asp:DropDownList ID="ddl_home_list" runat="server" AutoPostBack="true" 
                        DataTextField="home_name" DataValueField="home_id" 
                        OnSelectedIndexChanged="ddl_home_list_SelectedIndexChanged">
                    </asp:DropDownList>
                    &nbsp;</td>
            </tr>
        </table>
    
    <br />
  
     <asp:GridView ID="gv_insurance_policy_list" 
    runat="server" AutoGenerateColumns="false" GridLines="Both"  
         AlternatingRowStyle-BackColor="#F0F0F6"  HeaderStyle-BackColor="#F0F0F6"
         Width="100%" BorderColor="#CDCDCD" BorderWidth="1" 
       AllowPaging="true"  PageSize="10"  OnPageIndexChanging="gv_insurance_policy_list_PageIndexChanging"
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" >
    
    <Columns>
    
    <asp:BoundField DataField="home_name"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top" HeaderText="<%$ Resources:Resource, lbl_home_name %>" />
    <asp:BoundField DataField="ip_name" ItemStyle-VerticalAlign="Top" HeaderText="<%$ Resources:Resource, lbl_insurance_provider %>" />
    
    <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-VerticalAlign="Top" HeaderText="<%$ Resources:Resource, lbl_insurance_type %>"   >
               <ItemTemplate>
               <asp:Label  runat="server"  ID="lbl_insurance_type"   
                 Text='<%#Get_InsuranceType(Convert.ToInt32(Eval("ip_insurance_type")))%>'    /> 
               </ItemTemplate>
               </asp:TemplateField>
     <asp:BoundField ItemStyle-VerticalAlign="Top" HeaderStyle-VerticalAlign="Top" DataField="ip_amount_covered" DataFormatString="{0:0.00}"   HeaderText="<%$ Resources:Resource, lbl_coverage %>" />
    <asp:BoundField ItemStyle-VerticalAlign="Top" HeaderStyle-VerticalAlign="Top" DataField="ip_date_end" DataFormatString="{0:MM-dd-yyyy}"  HtmlEncode="false"  HeaderText="<%$ Resources:Resource, lbl_date_end %>"/> 
    <asp:HyperLinkField ItemStyle-VerticalAlign="Top" HeaderStyle-VerticalAlign="Top"   Text="<%$ Resources:Resource, lbl_view %>"
     DataNavigateUrlFields="ip_id" 
     DataNavigateUrlFormatString="~/manager/insurancepolicy/insurance_policy_view.aspx?ip_id={0}" 
      HeaderText= "<%$ Resources:Resource, lbl_view%>" />
    </Columns>
    </asp:GridView>
    </div>

        
        </ContentTemplate>
    </cc1:TabPanel>
    
   
   
   
   
    <cc1:TabPanel ID="tab2" runat="server" HeaderText="<%$ Resources:Resource, lbl_u_pending_insurance_policy %>">
    <ContentTemplate>
        <table>
            <tr>
                <td>
                    <asp:Label ID="lbl_property" runat="server" style="font-weight: 700" 
                        Text="<%$ Resources:Resource, lbl_property %>" />
                </td>
                <td>
                    :
                    <asp:DropDownList ID="ddl_home_pending_list" runat="server" AutoPostBack="true" 
                        DataTextField="home_name" DataValueField="home_id" 
                        OnSelectedIndexChanged="ddl_home_pending_list_SelectedIndexChanged">
                    </asp:DropDownList>
                    &nbsp;</td>
            </tr>
        </table>
    
    <br />
       <table width="100%">
           <tr>
               <td bgcolor="aliceblue">
                   <b>
                   <asp:Label ID="Label42" runat="server" 
                       Text="<%$ Resources:Resource, lbl_less31%>" />
                   </b>
               </td>
           </tr>
       </table>
    <br />
       <asp:GridView ID="gv_insurance_policy_pending_list1" runat="server" 
           AllowPaging="true" 
           AutoGenerateColumns="false" 
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" GridLines="Both" 
           AlternatingRowStyle-BackColor="#F0F0F6"  HeaderStyle-BackColor="#F0F0F6"
           BorderColor="#CDCDCD" BorderWidth="1" 
           OnPageIndexChanging="gv_insurance_policy_pending_list1_PageIndexChanging" 
           PageSize="10" Width="100%">
           <Columns>
               <asp:BoundField DataField="home_name" 
                   HeaderText="<%$ Resources:Resource, lbl_home_name %>" />
               <asp:BoundField DataField="ip_name" 
                   HeaderText="<%$ Resources:Resource, lbl_insurance_provider %>" />
               <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_insurance_type %>"   >
               <ItemTemplate>
               <asp:Label  runat="server"  ID="lbl_insurance_type"   
                 Text='<%#Get_InsuranceType(Convert.ToInt32(Eval("ip_insurance_type")))%>'    /> 
               </ItemTemplate>
               </asp:TemplateField>
               <asp:BoundField DataField="ip_amount_covered" DataFormatString="{0:0.00}" 
                   HeaderText="<%$ Resources:Resource, lbl_coverage %>" />
               <asp:BoundField DataField="ip_date_begin" DataFormatString="{0:MM-dd-yyyy}" 
                   HeaderText="<%$ Resources:Resource, lbl_date_begin %>" HtmlEncode="false" />
               <asp:HyperLinkField DataNavigateUrlFields="ip_id" 
                   DataNavigateUrlFormatString="~/manager/insurancepolicy/insurance_policy_pending_view.aspx?ip_id={0}" 
                   HeaderText="<%$ Resources:Resource, lbl_view%>" 
                   Text="<%$ Resources:Resource, lbl_view %>" />
           </Columns>
       </asp:GridView>
     <br />
     
  <br />
       <table width="100%">
           <tr>
               <td bgcolor="aliceblue">
                   <b>
                   <asp:Label ID="Label4" runat="server" 
                       Text="<%$ Resources:Resource, lbl_between_31and60%>" />
                   </b>
               </td>
           </tr>
       </table>
    <br />
       <asp:GridView ID="gv_insurance_policy_pending_list2" runat="server" 
           AllowPaging="true" 
           AutoGenerateColumns="false" 
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" GridLines="Both" 
            AlternatingRowStyle-BackColor="#F0F0F6"  HeaderStyle-BackColor="#F0F0F6"
           Width="100%" BorderColor="#CDCDCD" BorderWidth="1"
           OnPageIndexChanging="gv_insurance_policy_pending_list2_PageIndexChanging" 
           PageSize="10" >
           <Columns>
               <asp:BoundField DataField="home_name" 
                   HeaderText="<%$ Resources:Resource, lbl_home_name %>" />
               <asp:BoundField DataField="ip_name" 
                   HeaderText="<%$ Resources:Resource, lbl_insurance_provider %>" />
               <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_insurance_type %>"   >
               <ItemTemplate>
               <asp:Label  runat="server"  ID="lbl_insurance_type"   
                 Text='<%#Get_InsuranceType(Convert.ToInt32(Eval("ip_insurance_type")))%>'    /> 
               </ItemTemplate>
               </asp:TemplateField>
               <asp:BoundField DataField="ip_amount_covered" DataFormatString="{0:0.00}" 
                   HeaderText="<%$ Resources:Resource, lbl_coverage %>" />
               <asp:BoundField DataField="ip_date_begin" DataFormatString="{0:MM-dd-yyyy}" 
                   HeaderText="<%$ Resources:Resource, lbl_date_begin %>" HtmlEncode="false" />
               <asp:HyperLinkField DataNavigateUrlFields="ip_id" 
                   DataNavigateUrlFormatString="~/manager/insurancepolicy/insurance_policy_pending_view.aspx?ip_id={0}" 
                   HeaderText="<%$ Resources:Resource, lbl_view%>" 
                   Text="<%$ Resources:Resource, lbl_view %>" />
           </Columns>
       </asp:GridView>
       <br />
    
    
        <br />
        <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b>
                    <asp:Label ID="Label5" runat="server" 
                        Text="<%$ Resources:Resource, lbl_between_61and90%>" />
                    </b>
                </td>
            </tr>
       </table>
    <br />
       <asp:GridView ID="gv_insurance_policy_pending_list3" runat="server" 
           AllowPaging="true" 
           AutoGenerateColumns="false"  
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" GridLines="Both" 
           AlternatingRowStyle-BackColor="#F0F0F6"  HeaderStyle-BackColor="#F0F0F6"
           Width="100%" BorderColor="#CDCDCD" BorderWidth="1"
           OnPageIndexChanging="gv_insurance_policy_pending_list3_PageIndexChanging" 
           PageSize="10" >
           <Columns>
               <asp:BoundField DataField="home_name" 
                   HeaderText="<%$ Resources:Resource, lbl_home_name %>" />
               <asp:BoundField DataField="ip_name" 
                   HeaderText="<%$ Resources:Resource, lbl_insurance_provider %>" />
               <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_insurance_type %>"   >
               <ItemTemplate>
               <asp:Label  runat="server"  ID="lbl_insurance_type"   
                 Text='<%#Get_InsuranceType(Convert.ToInt32(Eval("ip_insurance_type")))%>'    /> 
               </ItemTemplate>
               </asp:TemplateField>
               <asp:BoundField DataField="ip_amount_covered" DataFormatString="{0:0.00}" 
                   HeaderText="<%$ Resources:Resource, lbl_coverage %>" />
               <asp:BoundField DataField="ip_date_begin" DataFormatString="{0:MM-dd-yyyy}" 
                   HeaderText="<%$ Resources:Resource, lbl_date_begin %>" HtmlEncode="false" />
               <asp:BoundField DataField="ip_date_end" DataFormatString="{0:MM-dd-yyyy}" 
                   HeaderText="<%$ Resources:Resource, lbl_date_end %>" HtmlEncode="false" />
               <asp:HyperLinkField DataNavigateUrlFields="ip_id" 
                   DataNavigateUrlFormatString="~/manager/insurancepolicy/insurance_policy_pending_view.aspx?ip_id={0}" 
                   HeaderText="<%$ Resources:Resource, lbl_view%>" 
                   Text="<%$ Resources:Resource, lbl_view %>" />
           </Columns>
       </asp:GridView>
       <br />
    
    
        <br />
        <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b>
                    <asp:Label ID="Label6" runat="server" 
                        Text="<%$ Resources:Resource, lbl_more90%>" />
                    </b>
                </td>
            </tr>
       </table>
       <br />
       <asp:GridView ID="gv_insurance_policy_pending_list4" runat="server" 
           AllowPaging="true"  
           AutoGenerateColumns="false"
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" GridLines="Both" 
           AlternatingRowStyle-BackColor="#F0F0F6"  HeaderStyle-BackColor="#F0F0F6"
           Width="100%" BorderColor="#CDCDCD" BorderWidth="1"
           OnPageIndexChanging="gv_insurance_policy_pending_list4_PageIndexChanging" 
           PageSize="10" >
           <Columns>
               <asp:BoundField DataField="home_name" 
                   HeaderText="<%$ Resources:Resource, lbl_home_name %>" />
               <asp:BoundField DataField="ip_name" 
                   HeaderText="<%$ Resources:Resource, lbl_insurance_provider %>" />
               <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_insurance_type %>"   >
               <ItemTemplate>
               <asp:Label  runat="server"  ID="lbl_insurance_type"   
                 Text='<%#Get_InsuranceType(Convert.ToInt32(Eval("ip_insurance_type")))%>'    /> 
               </ItemTemplate>
               </asp:TemplateField>
               <asp:BoundField DataField="ip_amount_covered" DataFormatString="{0:0.00}" 
                   HeaderText="<%$ Resources:Resource, lbl_coverage %>" />
               <asp:BoundField DataField="ip_date_begin" DataFormatString="{0:MM-dd-yyyy}" 
                   HeaderText="<%$ Resources:Resource, lbl_date_begin %>" HtmlEncode="false" />
               <asp:BoundField DataField="ip_date_end" DataFormatString="{0:MM-dd-yyyy}" 
                   HeaderText="<%$ Resources:Resource, lbl_date_end %>" HtmlEncode="false" />
               <asp:HyperLinkField DataNavigateUrlFields="ip_id" 
                   DataNavigateUrlFormatString="~/manager/insurancepolicy/insurance_policy_pending_view.aspx?ip_id={0}" 
                   HeaderText="<%$ Resources:Resource, lbl_view%>" 
                   Text="<%$ Resources:Resource, lbl_view %>" />
           </Columns>
       </asp:GridView>
 
    
    </ContentTemplate>
    </cc1:TabPanel>
    
    
    
    
    
    
    <cc1:TabPanel   ID="tab3" runat="server" HeaderText="<%$ Resources:Resource, lbl_u_archive_insurance_policy %>">
   
    <ContentTemplate >
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label8" runat="server" style="font-weight: 700" 
                        Text="<%$ Resources:Resource, lbl_property %>" />
                </td>
                <td>
                    :
                    <asp:DropDownList ID="ddl_home_archive_list" runat="server" AutoPostBack="true" 
                        DataTextField="home_name" DataValueField="home_id" 
                        OnSelectedIndexChanged="ddl_home_archive_list_SelectedIndexChanged">
                    </asp:DropDownList>
                    &nbsp;</td>
            </tr>
        </table>
    
    <br />
    
   
    <asp:GridView ID="gv_insurance_policy_archive_list" 
    runat="server" AutoGenerateColumns="false" GridLines="Both"  
        AlternatingRowStyle-BackColor="#F0F0F6"  HeaderStyle-BackColor="#F0F0F6"
        Width="100%" BorderColor="#CDCDCD" BorderWidth="1" 
       AllowPaging="true"  PageSize="10"  OnPageIndexChanging="gv_insurance_policy_archive_list_PageIndexChanging"
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" >
    
    <Columns>
    
    <asp:BoundField DataField="home_name"   HeaderText="<%$ Resources:Resource, lbl_home_name %>" />
    <asp:BoundField DataField="ip_name" HeaderText="<%$ Resources:Resource, lbl_insurance_provider %>" />
    
    <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_insurance_type %>"   >
      <ItemTemplate>
      <asp:Label  runat="server"  ID="lbl_insurance_type"    Text='<%#Get_InsuranceType(Convert.ToInt32(Eval("ip_insurance_type")))%>'    /> 
      </ItemTemplate>
      </asp:TemplateField>
    
    
    <asp:BoundField DataField="ip_amount_covered" DataFormatString="{0:0.00}"   HeaderText="<%$ Resources:Resource, lbl_coverage %>" />
    <asp:BoundField DataField="ip_date_begin" DataFormatString="{0:MM-dd-yyyy}" 
                   HeaderText="<%$ Resources:Resource, lbl_date_begin %>" HtmlEncode="false" />
    <asp:BoundField DataField="ip_date_end" DataFormatString="{0:MM-dd-yyyy}" 
                   HeaderText="<%$ Resources:Resource, lbl_date_end %>" HtmlEncode="false" />
               
    <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_view %>"
     DataNavigateUrlFields="ip_id" 
     DataNavigateUrlFormatString="~/manager/insurancepolicy/insurance_policy_archive_view.aspx?ip_id={0}" 
      HeaderText= "<%$ Resources:Resource, lbl_view%>" />
    </Columns>
    </asp:GridView>
   
    
    </ContentTemplate>
    </cc1:TabPanel>
   
    
    
    </cc1:TabContainer>



 
   
</asp:Content>