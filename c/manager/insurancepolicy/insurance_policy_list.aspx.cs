using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.AccountObjectAuthorization;


/// <summary>
/// Done by : Stanley Jocelyn
/// date    : nov 1 , 2007
/// </summary>
public partial class insurancepolicy_insurance_policy_list : BasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string referrer = "";

            //  referrer = Request.UrlReferrer.ToString();
            // the absolute path is "/sinfo/alerts/alerts.aspx "

            referrer = Request.UrlReferrer.AbsolutePath.ToString();
            //if the url referer is from the reminder page
            if (referrer == "/sinfo/manager/alerts/alerts.aspx" || referrer == "/manager/alerts/alerts.aspx")
            {
                //this is the pending tab
                TabContainer1.ActiveTabIndex = 1;
                
               
            }
            else
            {
                SetDefaultView();
            }





            tiger.Home l = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getHomeCount(Convert.ToInt32(Session["schema_id"]));

            int home_id = l.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));

            // first we check if there's any property available
            if (home_count > 0)
            {

                  referrer = Request.UrlReferrer.AbsolutePath.ToString();
              //if the url referer is from the reminder page
                  if (referrer == "/sinfo/manager/alerts/alerts.aspx" || referrer == "/manager/alerts/alerts.aspx")
               {
                   if (Request.QueryString["home_id"] != string.Empty)
                       home_id = Convert.ToInt32(Request.QueryString["home_id"]);

                   string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                   AccountObjectAuthorization homeAuthorization = new AccountObjectAuthorization(strconn);

                   ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                   if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), home_id) && home_id !=0)
                   {
                       Session.Abandon();
                       Response.Redirect("~/login.aspx");
                   }
                   ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
             
               }

             //   tiger.Home l = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ddl_home_pending_list.DataSource = l.getHomeList(Convert.ToInt32(Session["schema_id"]));
                ddl_home_list.DataSource = ddl_home_pending_list.DataSource;
                ddl_home_archive_list.DataSource = ddl_home_pending_list.DataSource;

                ddl_home_pending_list.DataBind();
                ddl_home_pending_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
                ddl_home_pending_list.SelectedValue = home_id.ToString();

                ddl_home_list.DataBind();
                ddl_home_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));

                ddl_home_archive_list.DataBind();
                ddl_home_archive_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
                
            }



            // THIS SECTION HAS TO BE REDONNE WITH M.A.R.S ( multiple active results set )
          //  tiger.InsurancePolicy hv = new tiger.InsurancePolicy(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
          //  gv_insurance_policy_list.DataSource = hv.getInsurancePolicyList(Convert.ToInt32(Session["schema_id"]));
          //  gv_insurance_policy_archive_list.DataSource = hv.getInsurancePolicyArchiveList(Convert.ToInt32(Session["schema_id"]));

         //   gv_insurance_policy_list.DataBind();
          //  gv_insurance_policy_archive_list.DataBind();


            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd1 = new SqlCommand("prInsurancePolicyPendingList", conn);
            SqlCommand cmd2 = new SqlCommand("prInsurancePolicyPendingList", conn);
            SqlCommand cmd3 = new SqlCommand("prInsurancePolicyPendingList", conn);
            SqlCommand cmd4 = new SqlCommand("prInsurancePolicyPendingList", conn);
            SqlCommand cmd5 = new SqlCommand("prInsurancePolicyList", conn);
            SqlCommand cmd6 = new SqlCommand("prInsurancePolicyArchiveList", conn);


            cmd1.CommandType = CommandType.StoredProcedure;

            try
            {

                conn.Open();

                cmd1.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd1.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd1.Parameters.Add("@time_frame", SqlDbType.Int).Value = 1;


                SqlDataAdapter da = new SqlDataAdapter(cmd1);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_insurance_policy_pending_list1.DataSource = dt;
                gv_insurance_policy_pending_list1.DataBind();
            }
            finally
            {
                //  conn.Close();
            }



            cmd2.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd2.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd2.Parameters.Add("@time_frame", SqlDbType.Int).Value = 2;


                SqlDataAdapter da = new SqlDataAdapter(cmd2);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_insurance_policy_pending_list2.DataSource = dt;
                gv_insurance_policy_pending_list2.DataBind();
            }
            finally
            {
                //  conn.Close();
            }




            cmd3.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd3.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd3.Parameters.Add("@time_frame", SqlDbType.Int).Value = 3;


                SqlDataAdapter da = new SqlDataAdapter(cmd3);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_insurance_policy_pending_list3.DataSource = dt;
                gv_insurance_policy_pending_list3.DataBind();
            }
            finally
            {
                //  conn.Close();
            }


            cmd4.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd4.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd4.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd4.Parameters.Add("@time_frame", SqlDbType.Int).Value = 4;


                SqlDataAdapter da = new SqlDataAdapter(cmd4);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_insurance_policy_pending_list4.DataSource = dt;
                gv_insurance_policy_pending_list4.DataBind();
            }
            finally
            {
              //    conn.Close();
            }



            cmd5.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd5.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd5.Parameters.Add("@home_id", SqlDbType.Int).Value = 0;
               
                SqlDataAdapter da = new SqlDataAdapter(cmd5);
                DataTable dt = new DataTable();
                da.Fill(dt);

                gv_insurance_policy_list.DataSource = dt;
                gv_insurance_policy_list.DataBind();
            }
            finally
            {
               // conn.Close();
            }



            cmd6.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd6.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd6.Parameters.Add("@home_id", SqlDbType.Int).Value = 0;

                SqlDataAdapter da = new SqlDataAdapter(cmd6);
                DataTable dt = new DataTable();
                da.Fill(dt);

                gv_insurance_policy_archive_list.DataSource = dt;
                gv_insurance_policy_archive_list.DataBind();
            }
            finally
            {
                conn.Close();
            }

        }

        
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_insurance_policy_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        int home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.InsurancePolicy hv = new tiger.InsurancePolicy(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_insurance_policy_list.PageIndex = e.NewPageIndex;
        gv_insurance_policy_list.DataSource = hv.getInsurancePolicyList(Convert.ToInt32(Session["schema_id"]),home_id);
        gv_insurance_policy_list.DataBind();

        TabContainer1.ActiveTabIndex = 0;

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_insurance_policy_pending_list1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        int home_id = Convert.ToInt32(ddl_home_pending_list.SelectedValue);

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.InsurancePolicy hv = new tiger.InsurancePolicy(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_insurance_policy_pending_list1.PageIndex = e.NewPageIndex;
        gv_insurance_policy_pending_list1.DataSource = hv.getInsurancePolicyPendingList(Convert.ToInt32(Session["schema_id"]),home_id,1);
        gv_insurance_policy_pending_list1.DataBind();
      
        TabContainer1.ActiveTabIndex = 1;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_insurance_policy_pending_list2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        int home_id = Convert.ToInt32(ddl_home_pending_list.SelectedValue);

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.InsurancePolicy hv = new tiger.InsurancePolicy(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_insurance_policy_pending_list2.PageIndex = e.NewPageIndex;
        gv_insurance_policy_pending_list2.DataSource = hv.getInsurancePolicyPendingList(Convert.ToInt32(Session["schema_id"]), home_id, 2);
        gv_insurance_policy_pending_list2.DataBind();

        TabContainer1.ActiveTabIndex = 1;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_insurance_policy_pending_list3_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        int home_id = Convert.ToInt32(ddl_home_pending_list.SelectedValue);

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.InsurancePolicy hv = new tiger.InsurancePolicy(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_insurance_policy_pending_list3.PageIndex = e.NewPageIndex;
        gv_insurance_policy_pending_list3.DataSource = hv.getInsurancePolicyPendingList(Convert.ToInt32(Session["schema_id"]), home_id, 3);
        gv_insurance_policy_pending_list3.DataBind();

        TabContainer1.ActiveTabIndex = 1;

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_insurance_policy_pending_list4_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        int home_id = Convert.ToInt32(ddl_home_pending_list.SelectedValue);

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.InsurancePolicy hv = new tiger.InsurancePolicy(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_insurance_policy_pending_list4.PageIndex = e.NewPageIndex;
        gv_insurance_policy_pending_list4.DataSource = hv.getInsurancePolicyPendingList(Convert.ToInt32(Session["schema_id"]), home_id, 4);
        gv_insurance_policy_pending_list4.DataBind();

        TabContainer1.ActiveTabIndex = 1;

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_insurance_policy_archive_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        int home_id = Convert.ToInt32(ddl_home_archive_list.SelectedValue);

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.InsurancePolicy hv = new tiger.InsurancePolicy(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_insurance_policy_archive_list.PageIndex = e.NewPageIndex;
        gv_insurance_policy_archive_list.DataSource = hv.getInsurancePolicyArchiveList(Convert.ToInt32(Session["schema_id"]),home_id);
        gv_insurance_policy_archive_list.DataBind();


        TabContainer1.ActiveTabIndex = 2;
    }

    /// <summary>
    /// 
    /// </summary>
    private void SetDefaultView()
    {
        TabContainer1.ActiveTabIndex = 0;

    }

    /*
    protected void lnkTab1_Click(object sender, EventArgs e)
    {
        TabContainer1.ActiveTabIndex = 0;

        tab1.BackColor = "#ffffcc";
        tab2.BackColor = "AliceBlue";
        tab3.BackColor = "AliceBlue";

    }
    protected void lnkTab2_Click(object sender, EventArgs e)
    {
        TabContainer1.ActiveTabIndex = 1;

        tab1.BackColor = "AliceBlue";
        tab2.BackColor = "#ffffcc";
        tab3.BackColor = "AliceBlue";

    }


    protected void lnkTab3_Click(object sender, EventArgs e)
    {
        TabContainer1.ActiveTabIndex = 2;

        tab1.BackColor = "AliceBlue";
        tab2.BackColor = "AliceBlue";
        tab3.BackColor = "#ffffcc";

    }
     */
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_pending_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        int home_id = Convert.ToInt32(ddl_home_pending_list.SelectedValue);

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd1 = new SqlCommand("prInsurancePolicyPendingList", conn);
        SqlCommand cmd2 = new SqlCommand("prInsurancePolicyPendingList", conn);
        SqlCommand cmd3 = new SqlCommand("prInsurancePolicyPendingList", conn);
        SqlCommand cmd4 = new SqlCommand("prInsurancePolicyPendingList", conn);

        TabContainer1.ActiveTabIndex =  1 ;

        cmd1.CommandType = CommandType.StoredProcedure;

        try
        {

            conn.Open();

            cmd1.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd1.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd1.Parameters.Add("@time_frame", SqlDbType.Int).Value = 1;


            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da.Fill(dt);


            gv_insurance_policy_pending_list1.DataSource = dt;
            gv_insurance_policy_pending_list1.DataBind();
        }
        finally
        {
            //  conn.Close();
        }



        cmd2.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd2.Parameters.Add("@time_frame", SqlDbType.Int).Value = 2;


            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);


            gv_insurance_policy_pending_list2.DataSource = dt;
            gv_insurance_policy_pending_list2.DataBind();
        }
        finally
        {
            //  conn.Close();
        }




        cmd3.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd3.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd3.Parameters.Add("@time_frame", SqlDbType.Int).Value = 3;


            SqlDataAdapter da = new SqlDataAdapter(cmd3);
            DataTable dt = new DataTable();
            da.Fill(dt);


            gv_insurance_policy_pending_list3.DataSource = dt;
            gv_insurance_policy_pending_list3.DataBind();
        }
        finally
        {
            //  conn.Close();
        }


        cmd4.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd4.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd4.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd4.Parameters.Add("@time_frame", SqlDbType.Int).Value = 4;


            SqlDataAdapter da = new SqlDataAdapter(cmd4);
            DataTable dt = new DataTable();
            da.Fill(dt);


            gv_insurance_policy_pending_list4.DataSource = dt;
            gv_insurance_policy_pending_list4.DataBind();
        }
        finally
        {
            conn.Close();
        }

    }


     /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        int home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        TabContainer1.ActiveTabIndex = 0 ;

        tiger.InsurancePolicy hv = new tiger.InsurancePolicy(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_insurance_policy_list.DataSource = hv.getInsurancePolicyList(Convert.ToInt32(Session["schema_id"]),home_id);
        gv_insurance_policy_list.DataBind();
     


    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_archive_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        int home_id = Convert.ToInt32(ddl_home_archive_list.SelectedValue);

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.InsurancePolicy hv = new tiger.InsurancePolicy(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_insurance_policy_archive_list.DataSource = hv.getInsurancePolicyArchiveList(Convert.ToInt32(Session["schema_id"]), home_id);
        gv_insurance_policy_archive_list.DataBind();

        TabContainer1.ActiveTabIndex = 2 ;


    }
    ///
    //
    protected string Get_InsuranceType(int insurane_categorie)
    {

        string insurance_type = "";




        if (insurane_categorie == 1)
        {
            insurance_type = Resources.Resource.lbl_property_insurance;
        }

        if (insurane_categorie == 2)
        {
            insurance_type = Resources.Resource.lbl_mortgage_insurance;
        }

        if (insurane_categorie == 3)
        {
            insurance_type = Resources.Resource.lbl_mortgage_life_insurance;
        }

        return insurance_type;
    }

}
