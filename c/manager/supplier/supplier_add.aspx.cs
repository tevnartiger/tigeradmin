using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class supplier_supplier_add : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prSupplierAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return_supplier_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@supplier_name", SqlDbType.VarChar, 50).Value = supplier_name.Text;
            cmd.Parameters.Add("@supplier_addr_no", SqlDbType.VarChar, 50).Value = supplier_addr_no.Text;
            cmd.Parameters.Add("@supplier_addr_street", SqlDbType.VarChar, 50).Value = supplier_addr_street.Text;
            cmd.Parameters.Add("@supplier_city", SqlDbType.VarChar, 50).Value = supplier_city.Text;
            cmd.Parameters.Add("@supplier_prov", SqlDbType.VarChar, 50).Value = supplier_prov.Text;
            cmd.Parameters.Add("@supplier_pc", SqlDbType.VarChar, 50).Value = supplier_pc.Text;
            cmd.Parameters.Add("@supplier_tel", SqlDbType.VarChar, 50).Value = supplier_tel.Text;
            cmd.Parameters.Add("@supplier_contact_fname", SqlDbType.VarChar, 50).Value = supplier_contact_fname.Text;
            cmd.Parameters.Add("@supplier_contact_lname", SqlDbType.VarChar, 50).Value = supplier_contact_lname.Text;
            cmd.Parameters.Add("@supplier_contact_tel", SqlDbType.VarChar, 50).Value = supplier_contact_tel.Text;
            cmd.Parameters.Add("@supplier_contact_email", SqlDbType.VarChar, 50).Value = supplier_contact_email.Text;

            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            if (Convert.ToInt32(cmd.Parameters["@return_supplier_id"].Value) > 0)
                result.InnerHtml = " add successful";


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }
            }






    protected void btn_supplier_Click(object sender, EventArgs e)
    {
        Response.Redirect("supplier_add.aspx");
    }
}
