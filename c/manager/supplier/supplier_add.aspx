<%@ Page Language="C#" MasterPageFile="../mp_manager.master" AutoEventWireup="true" CodeFile="supplier_add.aspx.cs" Inherits="supplier_supplier_add"  Title="Prospective tenant view" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div>
        ADD SUPPLIER<br />
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/appliance/appliance_add.aspx">add appliance</asp:HyperLink><br />
        <br />
        <table bgcolor="#ffffcc" >
            <tr>
                <td>
                    name</td>
                <td >
                    : 
                    <asp:TextBox ID="supplier_name" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td  >
                    address number</td>
                <td >
                    :
                    <asp:TextBox ID="supplier_addr_no" runat="server"></asp:TextBox></td>
            </tr>
            
            <tr>
                <td  >
                    address street</td>
                <td >
                    :
                    <asp:TextBox ID="supplier_addr_street" runat="server"></asp:TextBox></td>
            </tr>
            
            <tr>
                <td >
                    prov/state</td>
                <td >
                    :
                    <asp:TextBox ID="supplier_prov" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    city</td>
                <td>
                    :
                    <asp:TextBox ID="supplier_city" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td >
                    postal code</td>
                <td >
                    :
                    <asp:TextBox ID="supplier_pc" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td >
                    telephone</td>
                <td >
                    :
                    <asp:TextBox ID="supplier_tel" runat="server"></asp:TextBox>&nbsp;</td>
            </tr>
            <tr>
                <td  >
                    contact first name</td>
                <td >
                    :
                    <asp:TextBox ID="supplier_contact_fname" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td >
                    contact last name</td>
                <td  >
                    :
                    <asp:TextBox ID="supplier_contact_lname" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td >
                    contact tel.</td>
                <td >
                    :
                    <asp:TextBox ID="supplier_contact_tel" runat="server"></asp:TextBox></td>
            </tr>
            
            <tr>
                <td >
                    contact email</td>
                <td >
                    :
                    <asp:TextBox ID="supplier_contact_email" runat="server"></asp:TextBox></td>
            </tr>
        </table>
        <br />
        <asp:Button ID="btn_submit" runat="server" Text="submit"  OnClick="btn_submit_Click"/>
        &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
        <asp:Button ID="btn_anothersupplier" runat="server" Text="add another supplier"  OnClick="btn_supplier_Click"/><br />
    
    </div>
    <div id="result" runat=server></div>
 </asp:Content>