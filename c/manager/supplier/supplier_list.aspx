<%@ Page Language="C#" MasterPageFile="../mp_manager.master" AutoEventWireup="true" CodeFile="supplier_list.aspx.cs" Inherits="supplier_supplier_list" Title="Prospective tenant view" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div>
    <asp:GridView ID="dgsupplier_list" runat="server" AutoGenerateColumns="false"
      AllowPaging="true" AllowSorting="true"    AlternatingRowStyle-BackColor="#F0F0F6" 
      BorderColor="#CDCDCD"  BorderWidth="1" GridLines="Both" 
      HeaderStyle-BackColor="#F0F0F6" Width="100%"   >
       
         <Columns>
    
    <asp:HyperLinkField  DataTextField="company_name" 
     DataNavigateUrlFields="company_id" 
     DataNavigateUrlFormatString="~/manager/supplier/supplier_view.aspx?company_id={0}" 
      HeaderText="Supplier name"   SortExpression="supplier_name"  />
    
    <asp:BoundField   DataField="company_addr_no"  HeaderText="Address #"/>
    <asp:BoundField  DataField="company_addr_street" HeaderText="Street" />
    <asp:BoundField  DataField="company_city" HeaderText="City"/>
    <asp:BoundField  DataField="company_tel" HeaderText="Telephone" />
    </Columns>
    
    
    </asp:GridView>
    
    </div>
 </asp:Content>