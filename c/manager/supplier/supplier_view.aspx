<%@ Page Language="C#" MasterPageFile="../mp_manager.master" AutoEventWireup="true" CodeFile="supplier_view.aspx.cs" Inherits="supplier_supplier_view" Title="Prospective tenant view" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div>
        SUPPLIER INFORMATIONS<br />
        <br />
        <asp:HyperLink ID="supplier_add_link" 
            runat="server">add </asp:HyperLink> -&nbsp;  <asp:HyperLink ID="supplier_update_link"
            runat="server">update</asp:HyperLink> - delete supplier
        &nbsp;<br />
        <br />
       
        <asp:Repeater ID="rsupplierView" runat="server">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
            <tr>
                <td>
                    name</td>
                <td >
                    : 
                    <%#DataBinder.Eval(Container.DataItem, "company_name")%></td>
            </tr>
            <tr>
                <td  >
                    address number</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "company_addr_no")%></td>
            </tr>
            
            <tr>
                <td  >
                    address street</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "company_addr_street")%></td>
            </tr>
            
            <tr>
                <td >
                    prov/state</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "company_prov")%></td>
            </tr>
            <tr>
                <td>
                    city</td>
                <td>
                    :
                    <%#DataBinder.Eval(Container.DataItem, "company_city")%>
                </td>
            </tr>
            <tr>
                <td >
                    postal code</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "company_pc")%></td>
            </tr>
            <tr>
                <td >
                    telephone</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "company_tel")%>&nbsp;</td>
            </tr>
            <tr>
                <td  >
                    contact first name</td>
                <td >
                    :
                    
                    <%#DataBinder.Eval(Container.DataItem, "company_contact_fname")%></td>
            </tr>
            <tr>
                <td >
                    contact last name</td>
                <td  >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "company_contact_lname")%></td>
            </tr>
            <tr>
                <td >
                    contact tel.</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "company_contact_tel")%></td>
            </tr>
            <tr>
                <td >
                    contact email</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "company_contact_email")%></td>
            </tr>
        </table>
        </ItemTemplate>
        
        </asp:Repeater>
        <br />
        Go back to list
        
    </div>
    <div id="result" runat="server" ></div>
 </asp:Content>