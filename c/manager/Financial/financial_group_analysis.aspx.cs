﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
/// <summary>
/// Done by : Stanley Joce;yn
/// Date    : may 5 , 2009
/// </summary>
public partial class manager_Financial_financial_group_analysis : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!(Page.IsPostBack))
        {
            reg_mortgage_m.ValidationExpression = RegEx.getMoney();

            DateTime date = new DateTime();
            date = DateTime.Now;
            // default Date receive of the incomes
            //   ddl_year.SelectedValue = date.Year.ToString();

            ddl_date_received_m.SelectedValue = date.Month.ToString();
            ddl_date_received_y.SelectedValue = date.Year.ToString();


            //////////////////////////////////////////////////////////////////
            tiger.Date c = new tiger.Date();

            date = Convert.ToDateTime(c.DateCulture(date.Month.ToString(), "1", date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
            ///////////////////////////////////////////////////////////////////




            tiger.Group h = new tiger.Group(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int group_count = h.getGroupHomeCount(Convert.ToInt32(Session["schema_id"]));

            if (group_count > 0)
            {
                int group_id = h.groupFindFirst(Convert.ToInt32(Session["schema_id"]));

                tiger.Financial f = new tiger.Financial(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                rNumberUnitbyBedroomNumber.DataSource = f.getGroupNumberUnitbyBedroomNumberTotalMonthlyRentIncome(Convert.ToInt32(Session["schema_id"]), group_id, date);
                rNumberUnitbyBedroomNumber.DataBind();



                rNumberCommercialUnit.DataSource = f.getGroupNumberCommercialUnitTotalMonthlyRentIncome(Convert.ToInt32(Session["schema_id"]), group_id, date);
                rNumberCommercialUnit.DataBind();



                ddl_group_id.DataSource = h.getGroupHomeList(Convert.ToInt32(Session["schema_id"]));
                ddl_group_id.SelectedValue = Convert.ToString(group_id);
                ddl_group_id.DataBind();

                ////////////////////////////////


                // tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rExpense.DataSource = f.getGroupExpenseMonthViewGroupByCateg(group_id, Convert.ToInt32(Session["schema_id"]), date);
                rExpense.DataBind();


                /////////////////////////////////////////////////////////////

                //To view the address of the property

                tiger.Group hv = new tiger.Group(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rhome_view.DataSource = hv.getGroupHomeNameView(Convert.ToInt32(Session["schema_id"]), group_id);
                rhome_view.DataBind();





                // tiger.Financial hpp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rIncome.DataSource = f.getGroupIncomeMonthViewGroupByCateg(group_id, Convert.ToInt32(Session["schema_id"]), date);
                rIncome.DataBind();

                lbl_late_fee.Text = String.Format("{0:0.00}", Convert.ToDouble(f.getGroupPaidLateRentFeeHomeSum(group_id, Convert.ToInt32(Session["schema_id"]), date)));


                //// CALCULATION OF TOTAL INCOME
                decimal month_total = 0;

                for (int i = 0; i < rNumberUnitbyBedroomNumber.Items.Count; i++)
                {
                    Label bedrooms_total_rent_m = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_m");
                    month_total = month_total + Convert.ToDecimal(bedrooms_total_rent_m.Text);
                }


                for (int i = 0; i < rNumberCommercialUnit.Items.Count; i++)
                {
                    Label total_rent_received_m = (Label)rNumberCommercialUnit.Items[i].FindControl("lbl_commercial_total_rent_m");
                    month_total = month_total + Convert.ToDecimal(total_rent_received_m.Text);
                }


                for (int i = 0; i < rIncome.Items.Count; i++)
                {
                    HiddenField h_income_amount = (HiddenField)rIncome.Items[i].FindControl("h_income_amount");
                    month_total = month_total + Convert.ToDecimal(h_income_amount.Value);
                }



                month_total = month_total + Convert.ToDecimal(lbl_late_fee.Text);
                lbl_gi_m.Text = String.Format("{0:0.00}", month_total);

                // (%) of total income -----------------------------------------------------
                //--------------------------------------------------------------------------
                for (int i = 0; i < rNumberUnitbyBedroomNumber.Items.Count; i++)
                {
                    Label lbl_bedrooms_total_rent_m = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_m");
                    Label lbl_rent_income_amount_percent = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_rent_income_amount_percent");


                    if (month_total == 0)
                        lbl_rent_income_amount_percent.Text = String.Format("{0:0.00}", 0.00);
                    else
                        lbl_rent_income_amount_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_bedrooms_total_rent_m.Text) / month_total) * 100);

                }

                //------(%) Commercial rent income 
                //---------------------------------------------------------------------------

                for (int i = 0; i < rNumberCommercialUnit.Items.Count; i++)
                {
                    Label lbl_commercial_total_rent_m = (Label)rNumberCommercialUnit.Items[i].FindControl("lbl_commercial_total_rent_m");
                    Label lbl_rent_income_amount_percent = (Label)rNumberCommercialUnit.Items[i].FindControl("lbl_rent_income_amount_percent");


                    if (month_total == 0)
                        lbl_rent_income_amount_percent.Text = String.Format("{0:0.00}", 0.00);
                    else
                        lbl_rent_income_amount_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_commercial_total_rent_m.Text) / month_total) * 100);

                }




                if (month_total == 0)
                    lbl_late_fee_percent.Text = String.Format("{0:0.00}", 0.00);
                else
                    lbl_late_fee_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_late_fee.Text) / month_total) * 100);

                //--------------------------------------------------------------------------
                for (int i = 0; i < rIncome.Items.Count; i++)
                {
                    HiddenField h_income_amount = (HiddenField)rIncome.Items[i].FindControl("h_income_amount");
                    Label lbl_income_amount_percent = (Label)rIncome.Items[i].FindControl("lbl_income_amount_percent");

                    if (month_total == 0)
                        lbl_income_amount_percent.Text = String.Format("{0:0.00}", 0.00);
                    else
                        lbl_income_amount_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(h_income_amount.Value) / month_total) * 100);

                }
                //--------------------------------------------------------------------------
                //--------------------------------------------------------------------------

                //// END OF CALCULATION OF TOTAL INCOME


                //// CALCULATION OF TOTAL EXPENSES

                decimal month_total_expense = 0;

                for (int i = 0; i < rExpense.Items.Count; i++)
                {
                    HiddenField h_expense_amount = (HiddenField)rExpense.Items[i].FindControl("h_expense_amount");
                    month_total_expense = month_total_expense + Convert.ToDecimal(h_expense_amount.Value);
                }

                // (%) of total expense -----------------------------------------------------
                //---------------------------------------------------------------------------
                for (int i = 0; i < rExpense.Items.Count; i++)
                {
                    HiddenField h_expense_amount = (HiddenField)rExpense.Items[i].FindControl("h_expense_amount");
                    Label lbl_expense_amount_percent = (Label)rExpense.Items[i].FindControl("lbl_expense_amount_percent");
                    Label lbl_egi_percent = (Label)rExpense.Items[i].FindControl("lbl_egi_percent");
                    lbl_expense_amount_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(h_expense_amount.Value) / month_total_expense) * 100);


                    if (month_total == 0)
                        lbl_egi_percent.Text = String.Format("{0:0.00}", 0.00);
                    else
                        lbl_egi_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(h_expense_amount.Value) / month_total) * 100);


                }
                //--------------------------------------------------------------------------
                //--------------------------------------------------------------------------


                lbl_total_expenses_m.Text = String.Format("{0:0.00}", month_total_expense);

                //// END OF CALCULATION OF TOTAL EXPENSE

                decimal net_income = 0;

                net_income = month_total - month_total_expense;

                lbl_net_operating_inc_m.Text = String.Format("{0:0.00}", net_income);

                lbl_liquidity_m.Text = "";

            }
            // if ther is no home

            else
            {
                //  txt_message.InnerHtml = "There is no property -- Add a property";

                //  txt_link.InnerHtml = group_count + " <a href='home_add.aspx'>Add Home</a>&nbsp;&nbsp;<a href='owner_add.aspx'>Add Owner</a>&nbsp;&nbsp;<a href='storage_add.aspx'>Add storage</a>&nbsp;&nbsp;";
            }

        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_group_id_SelectedIndexChanged(object sender, EventArgs e)
    {




        DateTime date = new DateTime();
        // date = DateTime.Now;
        // default Date receive of the incomes




        //////////////////////////////////////////////////////////////////
        tiger.Date c = new tiger.Date();

        date = Convert.ToDateTime(c.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        ///////////////////////////////////////////////////////////////////

        // to view the incomes in the  home
        //////////////////////////////////////// // to view the total rent incomes in the  home  ////////////////////////////

        tiger.Financial f = new tiger.Financial(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        rNumberUnitbyBedroomNumber.DataSource = f.getGroupNumberUnitbyBedroomNumberTotalMonthlyRentIncome(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_group_id.SelectedValue), date);
        rNumberUnitbyBedroomNumber.DataBind();

        rNumberCommercialUnit.DataSource = f.getGroupNumberCommercialUnitTotalMonthlyRentIncome(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_group_id.SelectedValue), date);
        rNumberCommercialUnit.DataBind();


        /////////////////////////////////////////////////////////


        // tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rExpense.DataSource = f.getGroupExpenseMonthViewGroupByCateg(Convert.ToInt32(ddl_group_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), date);
        rExpense.DataBind();

        ///////////////////////////////////////////////////

        //To view the address of the property

        tiger.Group hv = new tiger.Group(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rhome_view.DataSource = hv.getGroupHomeNameView(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_group_id.SelectedValue));
        rhome_view.DataBind();


        // to view the incomes in the  home
        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        // DateTime income_date_received = new DateTime();
        //// tiger.Date df = new tiger.Date();
        // income_date_received = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        // tiger.Financial hpp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rIncome.DataSource = f.getGroupIncomeMonthViewGroupByCateg(Convert.ToInt32(ddl_group_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), date);
        rIncome.DataBind();




        // lbl_total_month_income.Text = hp.getTotalMonthIncome(, Convert.ToInt32(Session["schema_id"]), income_date_received);
        lbl_late_fee.Text = String.Format("{0:0.00}", Convert.ToDouble(f.getGroupPaidLateRentFeeHomeSum(Convert.ToInt32(ddl_group_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), date)));

        //// CALCULATION OF TOTAL INCOME
        decimal month_total = 0;

        for (int i = 0; i < rNumberUnitbyBedroomNumber.Items.Count; i++)
        {
            Label bedrooms_total_rent_m = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_m");
            month_total = month_total + Convert.ToDecimal(bedrooms_total_rent_m.Text);
        }

        for (int i = 0; i < rNumberCommercialUnit.Items.Count; i++)
        {
            Label total_rent_received_m = (Label)rNumberCommercialUnit.Items[i].FindControl("lbl_commercial_total_rent_m");
            month_total = month_total + Convert.ToDecimal(total_rent_received_m.Text);
        }

        for (int i = 0; i < rIncome.Items.Count; i++)
        {
            HiddenField h_income_amount = (HiddenField)rIncome.Items[i].FindControl("h_income_amount");
            month_total = month_total + Convert.ToDecimal(h_income_amount.Value);
        }



        month_total = month_total + Convert.ToDecimal(lbl_late_fee.Text);
        lbl_gi_m.Text = String.Format("{0:0.00}", month_total);

        // (%) of total income -----------------------------------------------------
        //--------------------------------------------------------------------------
        for (int i = 0; i < rNumberUnitbyBedroomNumber.Items.Count; i++)
        {
            Label lbl_bedrooms_total_rent_m = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_m");
            Label lbl_rent_income_amount_percent = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_rent_income_amount_percent");

            if (month_total == 0)
                lbl_rent_income_amount_percent.Text = String.Format("{0:0.00}", 0.00);
            else
                lbl_rent_income_amount_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_bedrooms_total_rent_m.Text) / month_total) * 100);

        }


        //------(%) Commercial rent income 
        //---------------------------------------------------------------------------

        for (int i = 0; i < rNumberCommercialUnit.Items.Count; i++)
        {
            Label lbl_commercial_total_rent_m = (Label)rNumberCommercialUnit.Items[i].FindControl("lbl_commercial_total_rent_m");
            Label lbl_rent_income_amount_percent = (Label)rNumberCommercialUnit.Items[i].FindControl("lbl_rent_income_amount_percent");


            if (month_total == 0)
                lbl_rent_income_amount_percent.Text = String.Format("{0:0.00}", 0.00);
            else
                lbl_rent_income_amount_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_commercial_total_rent_m.Text) / month_total) * 100);

        }


        if (month_total == 0)
            lbl_late_fee_percent.Text = String.Format("{0:0.00}", 0.00);
        else
            lbl_late_fee_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_late_fee.Text) / month_total) * 100);
        //--------------------------------------------------------------------------
        //--------------------------------------------------------------------------
        for (int i = 0; i < rIncome.Items.Count; i++)
        {
            HiddenField h_income_amount = (HiddenField)rIncome.Items[i].FindControl("h_income_amount");
            Label lbl_income_amount_percent = (Label)rIncome.Items[i].FindControl("lbl_income_amount_percent");
            if (month_total == 0)
                lbl_income_amount_percent.Text = String.Format("{0:0.00}", 0.00);
            else
                lbl_income_amount_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(h_income_amount.Value) / month_total) * 100);

        }
        //--------------------------------------------------------------------------
        //--------------------------------------------------------------------------
        //// END OF CALCULATION OF TOTAL INCOME

        //// CALCULATION OF TOTAL EXPENSES

        decimal month_total_expense = 0;

        for (int i = 0; i < rExpense.Items.Count; i++)
        {
            HiddenField h_expense_amount = (HiddenField)rExpense.Items[i].FindControl("h_expense_amount");
            month_total_expense = month_total_expense + Convert.ToDecimal(h_expense_amount.Value);
        }

        lbl_total_expenses_m.Text = String.Format("{0:0.00}", month_total_expense);

        // (%) of total expense -----------------------------------------------------
        //---------------------------------------------------------------------------
        for (int i = 0; i < rExpense.Items.Count; i++)
        {
            HiddenField h_expense_amount = (HiddenField)rExpense.Items[i].FindControl("h_expense_amount");
            Label lbl_expense_amount_percent = (Label)rExpense.Items[i].FindControl("lbl_expense_amount_percent");
            Label lbl_egi_percent = (Label)rExpense.Items[i].FindControl("lbl_egi_percent");
            lbl_expense_amount_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(h_expense_amount.Value) / month_total_expense) * 100);

            if (month_total == 0)
                lbl_egi_percent.Text = String.Format("{0:0.00}", 0.00);
            else
                lbl_egi_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(h_expense_amount.Value) / month_total) * 100);


        }
        //--------------------------------------------------------------------------
        //--------------------------------------------------------------------------

        //// END OF CALCULATION OF TOTAL EXPENSE


        decimal net_income = 0;

        net_income = month_total - month_total_expense;

        lbl_net_operating_inc_m.Text = String.Format("{0:0.00}", net_income);

        lbl_liquidity_m.Text = "";
    }



    protected void ddl_date_received_y_SelectedIndexChanged(object sender, EventArgs e)
    {

        DateTime date = new DateTime();
        date = DateTime.Now;
        // default Date receive of the incomes




        //////////////////////////////////////////////////////////////////
        tiger.Date c = new tiger.Date();

        date = Convert.ToDateTime(c.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        ///////////////////////////////////////////////////////////////////

        // to view the incomes in the  home
        //////////////////////////////////////// // to view the total rent incomes in the  home  ////////////////////////////


        /////////////////////////////////////////////////////////


        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rExpense.DataSource = hp.getGroupExpenseMonthViewGroupByCateg(Convert.ToInt32(ddl_group_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), date);
        rExpense.DataBind();

        ///////////////////////////////////////////////////


        tiger.Financial f = new tiger.Financial(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        rNumberUnitbyBedroomNumber.DataSource = f.getGroupNumberUnitbyBedroomNumberTotalMonthlyRentIncome(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_group_id.SelectedValue), date);
        rNumberUnitbyBedroomNumber.DataBind();


        rNumberCommercialUnit.DataSource = f.getGroupNumberCommercialUnitTotalMonthlyRentIncome(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_group_id.SelectedValue), date);
        rNumberCommercialUnit.DataBind();


        // tiger.Financial hpp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rIncome.DataSource = f.getGroupIncomeMonthViewGroupByCateg(Convert.ToInt32(ddl_group_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), date);
        rIncome.DataBind();



        lbl_late_fee.Text = String.Format("{0:0.00}", Convert.ToDouble(f.getGroupPaidLateRentFeeHomeSum(Convert.ToInt32(ddl_group_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), date)));


        //// CALCULATION OF TOTAL INCOME
        decimal month_total = 0;

        for (int i = 0; i < rNumberUnitbyBedroomNumber.Items.Count; i++)
        {
            Label bedrooms_total_rent_m = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_m");
            month_total = month_total + Convert.ToDecimal(bedrooms_total_rent_m.Text);
        }

        for (int i = 0; i < rNumberCommercialUnit.Items.Count; i++)
        {
            Label total_rent_received_m = (Label)rNumberCommercialUnit.Items[i].FindControl("lbl_commercial_total_rent_m");
            month_total = month_total + Convert.ToDecimal(total_rent_received_m.Text);
        }


        for (int i = 0; i < rIncome.Items.Count; i++)
        {
            HiddenField h_income_amount = (HiddenField)rIncome.Items[i].FindControl("h_income_amount");
            month_total = month_total + Convert.ToDecimal(h_income_amount.Value);
        }



        month_total = month_total + Convert.ToDecimal(lbl_late_fee.Text);
        lbl_gi_m.Text = String.Format("{0:0.00}", month_total);


        // (%) of total income -----------------------------------------------------
        //--------------------------------------------------------------------------
        for (int i = 0; i < rNumberUnitbyBedroomNumber.Items.Count; i++)
        {
            Label lbl_bedrooms_total_rent_m = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_m");
            Label lbl_rent_income_amount_percent = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_rent_income_amount_percent");
            if (month_total == 0)
                lbl_rent_income_amount_percent.Text = String.Format("{0:0.00}", 0.00);
            else
                lbl_rent_income_amount_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_bedrooms_total_rent_m.Text) / month_total) * 100);

        }


        //------(%) Commercial rent income 
        //---------------------------------------------------------------------------

        for (int i = 0; i < rNumberCommercialUnit.Items.Count; i++)
        {
            Label lbl_commercial_total_rent_m = (Label)rNumberCommercialUnit.Items[i].FindControl("lbl_commercial_total_rent_m");
            Label lbl_rent_income_amount_percent = (Label)rNumberCommercialUnit.Items[i].FindControl("lbl_rent_income_amount_percent");


            if (month_total == 0)
                lbl_rent_income_amount_percent.Text = String.Format("{0:0.00}", 0.00);
            else
                lbl_rent_income_amount_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_commercial_total_rent_m.Text) / month_total) * 100);

        }

        if (month_total == 0)
            lbl_late_fee_percent.Text = String.Format("{0:0.00}", 0.00);
        else
            lbl_late_fee_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_late_fee.Text) / month_total) * 100);

        //--------------------------------------------------------------------------
        for (int i = 0; i < rIncome.Items.Count; i++)
        {
            HiddenField h_income_amount = (HiddenField)rIncome.Items[i].FindControl("h_income_amount");
            Label lbl_income_amount_percent = (Label)rIncome.Items[i].FindControl("lbl_income_amount_percent");
            if (month_total == 0)
                lbl_income_amount_percent.Text = String.Format("{0:0.00}", 0.00);
            else
                lbl_income_amount_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(h_income_amount.Value) / month_total) * 100);

        }
        //--------------------------------------------------------------------------
        //----------------------------------------------------------------------------------------------------------------------------------------------------

        //// END OF CALCULATION OF TOTAL INCOME

        //// CALCULATION OF TOTAL EXPENSES

        decimal month_total_expense = 0;

        for (int i = 0; i < rExpense.Items.Count; i++)
        {
            HiddenField h_expense_amount = (HiddenField)rExpense.Items[i].FindControl("h_expense_amount");
            month_total_expense = month_total_expense + Convert.ToDecimal(h_expense_amount.Value);
        }

        lbl_total_expenses_m.Text = String.Format("{0:0.00}", month_total_expense);

        // (%) of total expense -----------------------------------------------------
        //---------------------------------------------------------------------------
        for (int i = 0; i < rExpense.Items.Count; i++)
        {
            HiddenField h_expense_amount = (HiddenField)rExpense.Items[i].FindControl("h_expense_amount");
            Label lbl_expense_amount_percent = (Label)rExpense.Items[i].FindControl("lbl_expense_amount_percent");
            Label lbl_egi_percent = (Label)rExpense.Items[i].FindControl("lbl_egi_percent");
            lbl_expense_amount_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(h_expense_amount.Value) / month_total_expense) * 100);


            if (month_total == 0)
                lbl_egi_percent.Text = String.Format("{0:0.00}", 0.00);
            else
                lbl_egi_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(h_expense_amount.Value) / month_total) * 100);


        }
        //--------------------------------------------------------------------------
        //--------------------------------------------------------------------------

        //// END OF CALCULATION OF TOTAL EXPENSE

        decimal net_income = 0;

        net_income = month_total - month_total_expense;

        lbl_net_operating_inc_m.Text = String.Format("{0:0.00}", net_income);

        lbl_liquidity_m.Text = "";
    }
    protected void ddl_date_received_m_SelectedIndexChanged(object sender, EventArgs e)
    {

        DateTime date = new DateTime();
        // date = DateTime.Now;
        // default Date receive of the incomes




        //////////////////////////////////////////////////////////////////
        tiger.Date c = new tiger.Date();

        date = Convert.ToDateTime(c.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        ///////////////////////////////////////////////////////////////////

        // to view the incomes in the  home
        //////////////////////////////////////// // to view the total rent incomes in the  home  ////////////////////////////


        /////////////////////////////////////////////////////////
        //****************************************************

        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rExpense.DataSource = hp.getGroupExpenseMonthViewGroupByCateg(Convert.ToInt32(ddl_group_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), date);
        rExpense.DataBind();
        //****************************************************
        ///////////////////////////////////////////////////


        tiger.Financial f = new tiger.Financial(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        rNumberUnitbyBedroomNumber.DataSource = f.getGroupNumberUnitbyBedroomNumberTotalMonthlyRentIncome(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_group_id.SelectedValue), date);
        rNumberUnitbyBedroomNumber.DataBind();


        rNumberCommercialUnit.DataSource = f.getGroupNumberCommercialUnitTotalMonthlyRentIncome(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_group_id.SelectedValue), date);
        rNumberCommercialUnit.DataBind();


        // tiger.Financial hpp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rIncome.DataSource = f.getGroupIncomeMonthViewGroupByCateg(Convert.ToInt32(ddl_group_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), date);
        rIncome.DataBind();



        lbl_late_fee.Text = String.Format("{0:0.00}", Convert.ToDouble(f.getGroupPaidLateRentFeeHomeSum(Convert.ToInt32(ddl_group_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), date)));


        //// CALCULATION OF TOTAL INCOME
        decimal month_total = 0;

        for (int i = 0; i < rNumberUnitbyBedroomNumber.Items.Count; i++)
        {
            Label bedrooms_total_rent_m = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_m");
            month_total = month_total + Convert.ToDecimal(bedrooms_total_rent_m.Text);
        }

        for (int i = 0; i < rNumberCommercialUnit.Items.Count; i++)
        {
            Label total_rent_received_m = (Label)rNumberCommercialUnit.Items[i].FindControl("lbl_commercial_total_rent_m");
            month_total = month_total + Convert.ToDecimal(total_rent_received_m.Text);
        }


        for (int i = 0; i < rIncome.Items.Count; i++)
        {
            HiddenField h_income_amount = (HiddenField)rIncome.Items[i].FindControl("h_income_amount");
            month_total = month_total + Convert.ToDecimal(h_income_amount.Value);
        }



        month_total = month_total + Convert.ToDecimal(lbl_late_fee.Text);
        lbl_gi_m.Text = String.Format("{0:0.00}", month_total);


        // (%) of total income -----------------------------------------------------
        //--------------------------------------------------------------------------
        for (int i = 0; i < rNumberUnitbyBedroomNumber.Items.Count; i++)
        {
            Label lbl_bedrooms_total_rent_m = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_m");
            Label lbl_rent_income_amount_percent = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_rent_income_amount_percent");
            if (month_total == 0)
                lbl_rent_income_amount_percent.Text = String.Format("{0:0.00}", 0.00);
            else
                lbl_rent_income_amount_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_bedrooms_total_rent_m.Text) / month_total) * 100);

        }


        //------(%) Commercial rent income 
        //---------------------------------------------------------------------------

        for (int i = 0; i < rNumberCommercialUnit.Items.Count; i++)
        {
            Label lbl_commercial_total_rent_m = (Label)rNumberCommercialUnit.Items[i].FindControl("lbl_commercial_total_rent_m");
            Label lbl_rent_income_amount_percent = (Label)rNumberCommercialUnit.Items[i].FindControl("lbl_rent_income_amount_percent");


            if (month_total == 0)
                lbl_rent_income_amount_percent.Text = String.Format("{0:0.00}", 0.00);
            else
                lbl_rent_income_amount_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_commercial_total_rent_m.Text) / month_total) * 100);

        }

        if (month_total == 0)
            lbl_late_fee_percent.Text = String.Format("{0:0.00}", 0.00);
        else
            lbl_late_fee_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_late_fee.Text) / month_total) * 100);


        //--------------------------------------------------------------------------
        for (int i = 0; i < rIncome.Items.Count; i++)
        {
            HiddenField h_income_amount = (HiddenField)rIncome.Items[i].FindControl("h_income_amount");
            Label lbl_income_amount_percent = (Label)rIncome.Items[i].FindControl("lbl_income_amount_percent");
            if (month_total == 0)
                lbl_income_amount_percent.Text = String.Format("{0:0.00}", 0.00);
            else
                lbl_income_amount_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(h_income_amount.Value) / month_total) * 100);

        }
        //--------------------------------------------------------------------------
        //--------------------------------------------------------------------------
        //// END OF CALCULATION OF TOTAL INCOME

        //// CALCULATION OF TOTAL EXPENSES

        decimal month_total_expense = 0;

        for (int i = 0; i < rExpense.Items.Count; i++)
        {
            HiddenField h_expense_amount = (HiddenField)rExpense.Items[i].FindControl("h_expense_amount");
            month_total_expense = month_total_expense + Convert.ToDecimal(h_expense_amount.Value);
        }

        lbl_total_expenses_m.Text = String.Format("{0:0.00}", month_total_expense);

        // (%) of total expense -----------------------------------------------------
        //---------------------------------------------------------------------------
        for (int i = 0; i < rExpense.Items.Count; i++)
        {
            HiddenField h_expense_amount = (HiddenField)rExpense.Items[i].FindControl("h_expense_amount");
            Label lbl_expense_amount_percent = (Label)rExpense.Items[i].FindControl("lbl_expense_amount_percent");
            Label lbl_egi_percent = (Label)rExpense.Items[i].FindControl("lbl_egi_percent");
            if (month_total == 0)
                lbl_expense_amount_percent.Text = String.Format("{0:0.00}", 0.00);
            else
                lbl_expense_amount_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(h_expense_amount.Value) / month_total_expense) * 100);

            if (month_total == 0)
                lbl_egi_percent.Text = String.Format("{0:0.00}", 0.00);
            else
                lbl_egi_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(h_expense_amount.Value) / month_total) * 100);


        }
        //--------------------------------------------------------------------------
        //--------------------------------------------------------------------------

        //// END OF CALCULATION OF TOTAL EXPENSE

        decimal net_income = 0;

        net_income = month_total - month_total_expense;

        lbl_net_operating_inc_m.Text = String.Format("{0:0.00}", net_income);

        lbl_liquidity_m.Text = "";

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetExpenseCateg(int expensecateg_id)
    {
        string expensecateg = "";

        switch (expensecateg_id)
        {
            case 1:
                expensecateg = Resources.Resource.lbl_electricity;
                break;
            case 2:
                expensecateg = Resources.Resource.lbl_energy;
                break;
            case 3:
                expensecateg = Resources.Resource.lbl_insurances;
                break;

            case 4:
                expensecateg = Resources.Resource.lbl_janitor;
                break;
            case 5:
                expensecateg = Resources.Resource.lbl_taxes;
                break;
            case 6:
                expensecateg = Resources.Resource.lbl_maintenance_repair;
                break;


            case 7:
                expensecateg = Resources.Resource.lbl_school_taxes;
                break;
            case 8:
                expensecateg = Resources.Resource.lbl_management;
                break;
            case 9:
                expensecateg = Resources.Resource.lbl_advertising;
                break;



            case 10:
                expensecateg = Resources.Resource.lbl_legal;
                break;
            case 11:
                expensecateg = Resources.Resource.lbl_accounting;
                break;
            case 12:
                expensecateg = Resources.Resource.lbl_other;
                break;

        }

        return expensecateg;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetIncomeCateg(int incomecateg_id)
    {
        string incomecateg = "";

        switch (incomecateg_id)
        {
            case 1:
                incomecateg = Resources.Resource.lbl_garage;
                break;
            case 2:
                incomecateg = Resources.Resource.lbl_parking;
                break;
            case 3:
                incomecateg = Resources.Resource.lbl_laundry;
                break;

            case 4:
                incomecateg = Resources.Resource.lbl_storage;
                break;
            case 5:
                incomecateg = Resources.Resource.lbl_vending_machine;
                break;
            case 6:
                incomecateg = Resources.Resource.lbl_cash_machine;
                break;

            case 7:
                incomecateg = Resources.Resource.lbl_other;
                break;

        }

        return incomecateg;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Caculate_Click(object sender, EventArgs e)
    {

        Page.Validate("vg_calculate");
        if (Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
       

        decimal liquidity = 0;

        if (tbx_mortgage_m.Text == "")
            tbx_mortgage_m.Text = "0";


        liquidity = Convert.ToDecimal(lbl_net_operating_inc_m.Text) - Convert.ToDecimal(tbx_mortgage_m.Text);

        lbl_liquidity_m.Text = String.Format("{0:0.00}", liquidity);
       }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_view_graph_Click(object sender, EventArgs e)
    {

        Response.Redirect("financial_group_expenses_graph.aspx?gr_id=" + ddl_group_id.SelectedValue + "&m=" + ddl_date_received_m.SelectedValue
                           + "&d=1" + "&y=" + ddl_date_received_y.SelectedValue);
    }

    protected void btn_income_view_graph_Click(object sender, EventArgs e)
    {
        Response.Redirect("financial_group_income_graph.aspx?gr_id=" + ddl_group_id.SelectedValue + "&m=" + ddl_date_received_m.SelectedValue
                          + "&d=1" + "&y=" + ddl_date_received_y.SelectedValue);
    }
   
}
