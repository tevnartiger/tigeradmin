﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : march 30, 2008
/// </summary>

public partial class manager_Financial_financial_income_update : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RegEx.IsInteger(Request.QueryString["inc_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        ////////
        //////// SECURITY CHECK BEGIN //////////////////
        ////////
        AccountObjectAuthorization incomeAuthorization = new AccountObjectAuthorization(strconn);

        if (!incomeAuthorization.Income(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["inc_id"])))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ////////
        //////// SECURITY CHECK END //////////////////
        ////////

        if (!(Page.IsPostBack))
        {
            int home_id = 0;

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prIncomeId", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@income_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["inc_id"]);

            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                DateTime income_date_received = new DateTime();

                while (dr.Read() == true)
                {

                    home_id = Convert.ToInt32(dr["home_id"]);
                    h_h_id.Value = home_id.ToString();

                    lbl_property2.Text = Convert.ToString(dr["home_name"]);
                    income_date_received = Convert.ToDateTime(dr["income_date_received"]);

                    ddl_update_date_received_d.SelectedValue = income_date_received.Day.ToString();
                    h_d.Value = income_date_received.Day.ToString();

                    ddl_update_date_received_m.SelectedValue = income_date_received.Month.ToString();
                    h_m.Value = income_date_received.Month.ToString();

                    ddl_update_date_received_y.SelectedValue = income_date_received.Year.ToString();
                    h_y.Value = income_date_received.Year.ToString();


                    lbl_month.Text = income_date_received.Month.ToString();
                    lbl_year.Text = income_date_received.Year.ToString();

                    lbl_income_categ.Text = GetIncomeCateg(Convert.ToInt32(dr["incomecateg_id"]));

                    tbx_income_reference.Text = Convert.ToString(dr["income_reference"]);
                    tbx_income_comments.Text = Convert.ToString(dr["income_comments"]);

                    tbx_income_amount.Text = String.Format("{0:0.00}", Convert.ToString(dr["income_amount"]));

                }

            }

            finally
            {
                conn.Close();
            }

            tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
            rhome_view.DataBind();
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_update_Click(object sender, EventArgs e)
    {

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime received_date = new DateTime();
        tiger.Date d = new tiger.Date();
        received_date = Convert.ToDateTime(d.DateCulture(ddl_update_date_received_m.SelectedValue, ddl_update_date_received_d.SelectedValue, ddl_update_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prIncomeUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@income_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["inc_id"]);
            cmd.Parameters.Add("@income_date_received", SqlDbType.DateTime).Value = received_date;
            cmd.Parameters.Add("@income_amount", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_income_amount.Text));
            cmd.Parameters.Add("@income_reference", SqlDbType.NVarChar, 50).Value = RegEx.getText(tbx_income_reference.Text);
            cmd.Parameters.Add("@income_comments", SqlDbType.Text).Value = RegEx.getText(tbx_income_comments.Text);



            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            //  tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }

        string url = "financial_income.aspx?h_id=" + h_h_id.Value + "&m=" + h_m.Value + "&d=" + h_d.Value + "&y=" + h_y.Value;


        Response.Redirect(url);

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetIncomeCateg(int incomecateg_id)
    {
        string incomecateg = "";

        switch (incomecateg_id)
        {
            case 1:
                incomecateg = Resources.Resource.lbl_garage;
                break;
            case 2:
                incomecateg = Resources.Resource.lbl_parking;
                break;
            case 3:
                incomecateg = Resources.Resource.lbl_laundry;
                break;

            case 4:
                incomecateg = Resources.Resource.lbl_storage;
                break;
            case 5:
                incomecateg = Resources.Resource.lbl_vending_machine;
                break;
            case 6:
                incomecateg = Resources.Resource.lbl_cash_machine;
                break;

            case 7:
                incomecateg = Resources.Resource.lbl_other;
                break;

        }

        return incomecateg;
    }

}
