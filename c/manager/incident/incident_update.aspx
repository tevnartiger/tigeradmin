﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="incident_update.aspx.cs" Inherits="manager_incident_incident_update" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <p style="font-size: small">
        <b>
          <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_incident_dammage_report %>"></asp:Label></b></p>
    <table cellpadding="0" cellspacing="0" style="width: 70%">
        <tr>
            <td valign="top">
              <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_title %>"></asp:Label></td>
            <td >
                <asp:TextBox ID="tbx_incident_title" runat="server" Height="16px" Width="228px"></asp:TextBox>
              <br /><hr />      
            </td>
        </tr>
        <tr>
            <td valign="top">
                  <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lbl_incident_datetime %>"></asp:Label>
           
            </td>
            <td >
                <asp:DropDownList ID="ddl_incident_date_m" runat="server">
                        <asp:ListItem Text="<%$ Resources:Resource, txt_month %>" Value="0"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;/&nbsp;
                    <asp:DropDownList ID="ddl_incident_date_d" runat="server" >
                     <asp:ListItem Text="<%$ Resources:Resource, txt_day %>" Value="0"></asp:ListItem>
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>&nbsp;/&nbsp;    
                    

                     <asp:DropDownList ID="ddl_incident_date_y" runat="server">
                            <asp:ListItem Text="<%$ Resources:Resource, txt_year %>" Value="0"></asp:ListItem>
                            <asp:ListItem>1950</asp:ListItem>
                            <asp:ListItem>1951</asp:ListItem>
                            <asp:ListItem>1952</asp:ListItem>
                            <asp:ListItem>1953</asp:ListItem>
                            <asp:ListItem>1954</asp:ListItem>
                            <asp:ListItem>1955</asp:ListItem>
                            <asp:ListItem>1956</asp:ListItem>
                            <asp:ListItem>1957</asp:ListItem>
                            <asp:ListItem>1958</asp:ListItem>
                            <asp:ListItem>1959</asp:ListItem>
                            <asp:ListItem>1960</asp:ListItem>
                            <asp:ListItem>1961</asp:ListItem>
                            <asp:ListItem>1962</asp:ListItem>
                            <asp:ListItem>1963</asp:ListItem>
                            <asp:ListItem>1964</asp:ListItem>
                            <asp:ListItem>1965</asp:ListItem>
                            <asp:ListItem>1966</asp:ListItem>
                            <asp:ListItem>1967</asp:ListItem>
                            <asp:ListItem>1968</asp:ListItem>
                            <asp:ListItem>1969</asp:ListItem>
                            <asp:ListItem>1970</asp:ListItem>
                            <asp:ListItem>1971</asp:ListItem>
                            <asp:ListItem>1972</asp:ListItem>
                            <asp:ListItem>1973</asp:ListItem>
                            <asp:ListItem>1974</asp:ListItem>
                            <asp:ListItem>1975</asp:ListItem>
                            <asp:ListItem>1976</asp:ListItem>
                            <asp:ListItem>1977</asp:ListItem>
                            <asp:ListItem>1978</asp:ListItem>
                            <asp:ListItem>1979</asp:ListItem>
                            <asp:ListItem>1980</asp:ListItem>
                            <asp:ListItem>1981</asp:ListItem>
                            <asp:ListItem>1982</asp:ListItem>
                            <asp:ListItem>1983</asp:ListItem>
                            <asp:ListItem>1984</asp:ListItem>
                            <asp:ListItem>1985</asp:ListItem>
                            <asp:ListItem>1986</asp:ListItem>
                            <asp:ListItem>1987</asp:ListItem>
                            <asp:ListItem>1988</asp:ListItem>
                            <asp:ListItem>1989</asp:ListItem>
                            <asp:ListItem>1990</asp:ListItem>
                            <asp:ListItem>1991</asp:ListItem>
                            <asp:ListItem>1992</asp:ListItem>
                            <asp:ListItem>1993</asp:ListItem>
                            <asp:ListItem>1994</asp:ListItem>
                            <asp:ListItem>1995</asp:ListItem>
                            <asp:ListItem>1996</asp:ListItem>
                            <asp:ListItem>1997</asp:ListItem>
                            <asp:ListItem>1998</asp:ListItem>
                            <asp:ListItem>1999</asp:ListItem>
                            <asp:ListItem>2000</asp:ListItem>
                            <asp:ListItem>2001</asp:ListItem>
                            <asp:ListItem>2002</asp:ListItem>
                            <asp:ListItem>2003</asp:ListItem>
                            <asp:ListItem>2004</asp:ListItem>
                            <asp:ListItem>2005</asp:ListItem>
                            <asp:ListItem>2006</asp:ListItem>
                            <asp:ListItem>2007</asp:ListItem>
                            <asp:ListItem>2008</asp:ListItem>
                            <asp:ListItem>2009</asp:ListItem>
                            <asp:ListItem>2010</asp:ListItem>
                            <asp:ListItem>2011</asp:ListItem>
                            <asp:ListItem>2012</asp:ListItem>
                            <asp:ListItem>2013</asp:ListItem>
                            <asp:ListItem>2014</asp:ListItem>
                            <asp:ListItem>2015</asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>
                    
                    
                    
                    
                    
                    
                &nbsp;
                    <asp:DropDownList ID="ddl_incident_hours" runat="server">
                        <asp:ListItem Value="0">00 - 12 AM</asp:ListItem>
                        <asp:ListItem Value="60">01 - 01 AM</asp:ListItem>
                        <asp:ListItem Value="120">02 - 02 AM</asp:ListItem>
                        <asp:ListItem Value="180">03 - 03 AM</asp:ListItem>
                        <asp:ListItem Value="240">04 - 04 AM</asp:ListItem>
                        <asp:ListItem Value="300">05 - 05 AM</asp:ListItem>
                        <asp:ListItem Value="360">06 - 06 AM</asp:ListItem>
                        <asp:ListItem Value="420">07 - 07 AM</asp:ListItem>
                        <asp:ListItem Value="480">08 - 08 AM</asp:ListItem>
                        <asp:ListItem Value="540">09 - 09 AM</asp:ListItem>
                        <asp:ListItem Value="600">10 - 10 AM</asp:ListItem>
                        <asp:ListItem Value="660">11 - 11 AM</asp:ListItem>
                        <asp:ListItem Value="720">12 - 12 PM</asp:ListItem>
                        <asp:ListItem Value="780">13 - 01 PM</asp:ListItem>
                        <asp:ListItem Value="840">14 - 02 PM</asp:ListItem>
                        <asp:ListItem Value="900">15 - 03 PM</asp:ListItem>
                        <asp:ListItem Value="960">16 - 04 PM</asp:ListItem>
                        <asp:ListItem Value="1020">17 - 05 PM</asp:ListItem>
                        <asp:ListItem Value="1080">18 - 06 PM</asp:ListItem>
                        <asp:ListItem Value="1140">19 - 07 PM</asp:ListItem>
                        <asp:ListItem Value="1200">20 - 08 PM</asp:ListItem>
                        <asp:ListItem Value="1260">21 - 09 PM</asp:ListItem>
                        <asp:ListItem Value="1320">22 - 10 PM</asp:ListItem>
                        <asp:ListItem Value="1380">23 - 11 PM</asp:ListItem>
                    </asp:DropDownList>
                    
                    
                    
                    
                    
                    
                &nbsp;<asp:DropDownList ID="ddl_incident_minutes" runat="server">
                        <asp:ListItem Value="0">00</asp:ListItem>
                        <asp:ListItem Value="1">01</asp:ListItem>
                        <asp:ListItem Value="2">02</asp:ListItem>
                        <asp:ListItem Value="3">03</asp:ListItem>
                        <asp:ListItem Value="4">04</asp:ListItem>
                        <asp:ListItem Value="5">05</asp:ListItem>
                        <asp:ListItem Value="6">06</asp:ListItem>
                        <asp:ListItem Value="7">07</asp:ListItem>
                        <asp:ListItem Value="8">08</asp:ListItem>
                        <asp:ListItem Value="9">09</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                        <asp:ListItem>32</asp:ListItem>
                        <asp:ListItem>33</asp:ListItem>
                        <asp:ListItem>34</asp:ListItem>
                        <asp:ListItem>35</asp:ListItem>
                        <asp:ListItem>36</asp:ListItem>
                        <asp:ListItem>37</asp:ListItem>
                        <asp:ListItem>38</asp:ListItem>
                        <asp:ListItem>39</asp:ListItem>
                        <asp:ListItem>40</asp:ListItem>
                        <asp:ListItem>41</asp:ListItem>
                        <asp:ListItem>42</asp:ListItem>
                        <asp:ListItem>43</asp:ListItem>
                        <asp:ListItem>44</asp:ListItem>
                        <asp:ListItem>45</asp:ListItem>
                        <asp:ListItem>46</asp:ListItem>
                        <asp:ListItem>47</asp:ListItem>
                        <asp:ListItem>48</asp:ListItem>
                        <asp:ListItem>49</asp:ListItem>
                        <asp:ListItem>50</asp:ListItem>
                        <asp:ListItem>51</asp:ListItem>
                        <asp:ListItem>52</asp:ListItem>
                        <asp:ListItem>53</asp:ListItem>
                        <asp:ListItem>54</asp:ListItem>
                        <asp:ListItem>55</asp:ListItem>
                        <asp:ListItem>56</asp:ListItem>
                        <asp:ListItem>57</asp:ListItem>
                        <asp:ListItem>58</asp:ListItem>
                        <asp:ListItem>59</asp:ListItem>
                    </asp:DropDownList>
                    <br /><hr />
            </td>
        </tr>
        <tr>
            <td valign="top" >
                <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lbl_property %>"></asp:Label></td>
            <td >
                <asp:DropDownList DataValueField="home_id" DataTextField="home_name"  
                    ID="ddl_home_id" runat="server"  AutoPostBack="true"
                    onselectedindexchanged="ddl_home_id_SelectedIndexChanged">
                </asp:DropDownList>
                <br /><hr />
            </td>
        </tr>
        <tr>
            <td valign="top" >
                <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lbl_unit_if_applicable %>"></asp:Label></td>
            <td >
                <asp:DropDownList DataValueField="unit_id" DataTextField="unit_door_no"  ID="ddl_unit_id" runat="server">
                </asp:DropDownList>
                <br /><hr />
            </td>
        </tr>
        <tr>
            <td valign="top" >
                <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lbl_exact_location %>"></asp:Label></td>
            <td >
                <asp:TextBox ID="tbx_incident_location" runat="server" Height="16px" Width="229px"></asp:TextBox><br />
                <hr />
            </td>
        </tr>
        <tr>
            <td valign="top" >
                <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, lbl_person_involved %>"/></td>
            <td >
                <asp:TextBox ID="tbx_incident_person_involved" runat="server" Height="75px" TextMode="MultiLine" 
                    Width="231px"></asp:TextBox>
                    <br /><hr />
            </td>
        </tr>
        <tr>
            <td valign="top" >
             <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lbl_witnesses %>"/><br />
                </td>
            <td >
                <asp:TextBox ID="tbx_incident_witnesses" runat="server" Height="76px" TextMode="MultiLine" 
                    Width="234px"></asp:TextBox>
                <hr />
            </td>
        </tr>
        <tr>
            <td valign="top" >
                <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, lbl_description %>"/></td>
            <td >
                <asp:TextBox ID="tbx_incident_description" runat="server" Height="141px" TextMode="MultiLine" 
                    Width="360px"></asp:TextBox>
                    <br /><hr />
            </td>
        </tr>
        <tr>
            <td valign="top" >
                <asp:Label ID="Label10" runat="server" Text="<%$ Resources:Resource, lbl_action_taken %>"/></td>
            <td >
                <asp:TextBox ID="tbx_incident_action_taken" runat="server" Height="140px" TextMode="MultiLine" 
                    Width="360px"></asp:TextBox>
                    <br /><hr />
            </td>
        </tr>
        <tr>
            <td valign="top">
                &nbsp;</td>
            <td align="right" bgcolor="aliceblue">
                
                <asp:RegularExpressionValidator 
                        ID="reg_tbx_incident_title" runat="server" 
                         ControlToValidate="tbx_incident_title"
                        ErrorMessage="invalid incident name">
                        </asp:RegularExpressionValidator>
                &nbsp;<asp:RequiredFieldValidator  ControlToValidate="tbx_incident_title"
                           ID="req_tbx_incident_title" runat="server" 
                        ErrorMessage="incident name required"></asp:RequiredFieldValidator>
                
                <asp:Button ID="btn_submit" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" onclick="btn_submit_Click" />
              
            </td>
        </tr>
    </table>

<br />

    <asp:LinkButton ID="link_add_wo" Text="<%$ Resources:Resource,lbl_add_wo %>" 
        runat="server" onclick="link_add_wo_Click"></asp:LinkButton>

<br />
                <asp:Label ID="lbl_confirmation" runat="server" ForeColor="Red"></asp:Label>
                <br />
                <asp:Label ID="Label12" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label13" runat="server"></asp:Label>
&nbsp;<asp:Label ID="lbl_title" runat="server"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label14" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label15" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label16" runat="server"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label17" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label18" runat="server"></asp:Label>
&nbsp;<asp:Label ID="lbl_date" runat="server"></asp:Label>
&nbsp;<br />
  
<asp:GridView HeaderStyle-BackColor="AliceBlue" Width="83%" BorderColor="White" BorderWidth="3"  ID="gv_wo_list" runat="server" AutoGenerateColumns="false"
       AllowSorting="true" GridLines="Both"   AllowPaging="true" PageSize="10"
      OnPageIndexChanging="gv_wo_list_PageIndexChanging"   AlternatingRowStyle-BackColor="Beige">
        
     <Columns>
   <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_work_order %>" DataField="wo_title"   />
   <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_property %>" DataField="home_name"   />
   <asp:BoundField HeaderText="<%$ Resources:Resource,gv_unit %>" DataField="unit_door_no"   />
   <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_date_begin %>"  DataField="wo_date_begin" DataFormatString="{0:MMM-dd-yyyy}"  
     HtmlEncode="false" />
      
      
  <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_due_date %>"  DataField="wo_exp_date_end" DataFormatString="{0:MMM-dd-yyyy}"  
     HtmlEncode="false" />
      
     <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_status%>"  >
       <ItemTemplate  >
       <asp:Label ID="lbl_wo_status"  
                  Text='<%#GetStatus(Convert.ToInt32(Eval("wo_status")))%>' 
                  runat="server" />   
       </ItemTemplate  >
    </asp:TemplateField>
       
       
       <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_priority%>"  >
       <ItemTemplate  >
       <asp:Label ID="lbl_task_priority"  
                  Text='<%#GetPriority(Convert.ToInt32(Eval("wo_priority")))%>' 
                  runat="server" />   
       </ItemTemplate  >
    </asp:TemplateField>
       
       
      <asp:HyperLinkField  Text="<%$ Resources:Resource,lbl_view %>" 
     DataNavigateUrlFields="wo_id,home_id,incident_id" 
     DataNavigateUrlFormatString="../workorder/wo_view.aspx?wo_id={0}&h_id={1}&inc={2}" 
      HeaderText="<%$ Resources:Resource,lbl_view %>"  />
      
    
    
      <asp:HyperLinkField  Text="<%$ Resources:Resource,lbl_update %>" 
     DataNavigateUrlFields="wo_id,home_id,incident_id" 
     DataNavigateUrlFormatString="../workorder/wo_update.aspx?wo_id={0}&h_id={1}&inc={2}" 
      HeaderText="<%$ Resources:Resource,lbl_update %>"  />
    
    
    
      
        <asp:TemplateField >
    <ItemTemplate  >
    <asp:HiddenField ID="h_home_name" Value='<%#Bind("home_name")%>'  runat="server"   />
   <asp:HiddenField  ID="h_wo_title" Value='<%#Bind("wo_title")%>'  runat="server" />
   
   <asp:HiddenField ID="h_wo_date_begin" Value='<%#Bind("wo_date_begin","{0:MMM-dd-yyyy}" )%>' runat="server" 
      />
    <asp:HiddenField ID="h_wo_id" Value='<%#Bind("wo_id")%>'  runat="server" />
   <asp:Button ID="btn_cancel" runat="server" OnClick="btn_cancel_Click"  Text="<%$ Resources:Resource, lbl_delete %>"/>
    </ItemTemplate  >
    </asp:TemplateField>
    
    
    
    
    
      </Columns>   
        
        
    </asp:GridView>
    <asp:Label ID="Label11" runat="server" ></asp:Label>

</asp:Content>

