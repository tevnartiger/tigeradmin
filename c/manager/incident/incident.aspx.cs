﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class c_manager_incident_incident : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Control FeaturedProductUserControl;
        switch (Session["role_name_abbr"].ToString())
        {
            case "EXDW":
            case "EX":
                FeaturedProductUserControl = LoadControl("uc/dash/uc_dash_exec.ascx");
                break;
            case "CS":
                FeaturedProductUserControl = LoadControl("uc/dash/uc_dash_cs.ascx");
                break;


            default:
                FeaturedProductUserControl = LoadControl("uc_incident_default.ascx");
                break;
        }
        panel_incident.Controls.Add(FeaturedProductUserControl);
    }
}