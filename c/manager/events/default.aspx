﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="manager_events_default" Title="Untitled Page" %>
<%@ Register Assembly="DayPilot" Namespace="DayPilot.Web.Ui" TagPrefix="DayPilot" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register TagPrefix="ECalendar" Namespace="ExtendedControls" Assembly="EventCalendar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server">    
    </asp:ScriptManager>
    <div> 
        <asp:Label ID="Label1" runat="server" 
            Text="<%$ Resources:Resource, lbl_u_events_calendar%>" 
            style="font-size: medium; font-weight: 700"></asp:Label>
        &nbsp;&nbsp;<asp:HyperLink ID="HyperLink1" runat="server" 
            NavigateUrl="~/manager/events/event_add.aspx">(add an event)</asp:HyperLink> 
        <br />
        <br />
        <asp:GridView AlternatingRowStyle-BackColor="Beige" ID="gvSelectedDateEvents" 
          BorderColor="AliceBlue" BorderWidth="3" HeaderStyle-BackColor="AliceBlue" 
           Width="100%" runat="server" AutoGenerateColumns="false" >      
      <Columns>
      <asp:BoundField   DataField="event_date_begin" DataFormatString="{0:M-dd-yyyy hh:mm tt}"  
       HtmlEncode="true" HeaderText="<%$ Resources:Resource, lbl_date_begin%>"  >
       <ItemStyle VerticalAlign="top"   />
       </asp:BoundField>
       
      <asp:BoundField    DataField="event_date_end" DataFormatString="{0:M-dd-yyyy hh:mm tt}"  
       HtmlEncode="true" HeaderText="<%$ Resources:Resource, lbl_date_end%>"  >
       <ItemStyle VerticalAlign="top"   />
       </asp:BoundField>
      
       <asp:BoundField  DataField="event_header" HeaderText="<%$ Resources:Resource, lbl_event %>">
       <ItemStyle VerticalAlign="top"  Wrap="True" Width="100" />
       </asp:BoundField>
       
       <asp:BoundField DataField="event_description" HeaderText="<%$ Resources:Resource, lbl_description %>">
       <ItemStyle VerticalAlign="top"   Wrap="True" Width="150" />
       </asp:BoundField>
       
      <asp:TemplateField  ItemStyle-VerticalAlign="Top" >
      <ItemTemplate    >
      <asp:HyperLink ID="link_event"  Text="<%$ Resources:Resource, lbl_update %>"  runat="server" NavigateUrl='<%#"event_update.aspx?ev_id="+DataBinder.Eval(Container.DataItem,"event_id")%>'></asp:HyperLink>
      </ItemTemplate  >
      </asp:TemplateField>
      
      <asp:TemplateField ItemStyle-VerticalAlign="Top"  >
      <ItemTemplate  >
      <asp:HiddenField ID="h_event_id" Value='<%#Bind("event_id")%>'  runat="server" />
      <asp:Button ID="btn_delete" runat="server" OnClick="btn_delete_Click"  Text="<%$ Resources:Resource, lbl_delete %>"/>
      </ItemTemplate  >
      </asp:TemplateField>
    
       
       
       
       
     </Columns>
        </asp:GridView>
        <hr id="hr_separator" visible="false" runat="server" />
      
        <DayPilot:DayPilotCalendar ID="DayPilotCalendar1" runat="server" />
      
        <hr id="hr1" visible="false" runat="server" />
      
      
      
    
         <ECalendar:EventCalendar ID="Calendar1" BackColor="Beige" Width="100%" runat="server"  BorderColor="Silver"
            BorderWidth="1px" Font-Names="Verdana" 
            Font-Size="7pt" ForeColor="Black" 
             FirstDayOfWeek="Monday" NextMonthText="Next &gt;" PrevMonthText="&lt; Prev" SelectionMode="DayWeekMonth" ShowGridLines="True" NextPrevFormat="FullMonth" 
            ShowDescriptionAsToolTip="True" BorderStyle="Solid" EventDateColumnName="" EventDescriptionColumnName="" EventHeaderColumnName="" OnSelectionChanged="Calendar1_SelectionChanged">
            <SelectedDayStyle BackColor="#333399" ForeColor="White" />
            <TodayDayStyle BackColor="#CCCCCC" />
            
            <SelectorStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"  />
            <DayStyle HorizontalAlign="Left" VerticalAlign="Top" Wrap="True" />
            <OtherMonthDayStyle ForeColor="#999999" />
            <NextPrevStyle Font-Size="8pt" ForeColor="#333333" Font-Bold="True" VerticalAlign="Bottom" />
            <DayHeaderStyle BorderWidth="1px" Font-Bold="True" Font-Size="8pt" />
            <TitleStyle BackColor="aliceblue" BorderColor="Black" BorderWidth="2px" Font-Bold="True"
                Font-Size="12pt" ForeColor="#333399" HorizontalAlign="Center" VerticalAlign="Middle" />
        </ECalendar:EventCalendar>
    
    </div>
    <asp:HiddenField ID="h_backcolor_no" runat="server" />
   
    
    

</asp:Content>

