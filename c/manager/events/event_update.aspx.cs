﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;

/// <summary>
/// done by : Stanley Jocelyn
/// date    : july 23 , 2008
/// desc    : updating an event
/// </summary>
public partial class manager_events_event_update : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        ////////
        //////// SECURITY CHECK BEGIN //////////////////
        ////////
        AccountObjectAuthorization eventAuthorization = new AccountObjectAuthorization(strconn);

        if (eventAuthorization.Event(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["ev_id"])))
        { }
        else
        {
            Session.Abandon();
            Response.Redirect("http://www.sinfocatiger.com/login.aspx");
        }
        ////////
        //////// SECURITY CHECK END //////////////////
        ////////

        if (!Page.IsPostBack)
        {
            lbl_update_confirmation.Visible = false;
            tb_confirmation.Visible = false;

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prEventView", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            //Add the params
            cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("event_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["ev_id"]);
            
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);
                while (dr.Read() == true)
                {

                    tbx_description.Text = dr["event_description"].ToString();
                    tbx_event.Text = dr["event_header"].ToString();

                    DateTime date_begin = new DateTime();
                    date_begin = Convert.ToDateTime(dr["event_date_begin"]);
                    if (date_begin != null)
                    {
                        ddl_event_date_begin_y.SelectedValue = date_begin.Year.ToString();
                        ddl_event_date_begin_d.Text = date_begin.Day.ToString();
                        ddl_event_date_begin_m.SelectedValue = date_begin.Month.ToString();

                        ddl_date_begin_hours.SelectedValue = (date_begin.Hour * 60).ToString();
                        ddl_date_begin_minutes.SelectedValue = date_begin.Minute.ToString();
                    }

                    DateTime date_end = new DateTime();
                    date_end = Convert.ToDateTime(dr["event_date_end"]);
                    if (date_end != null)
                    {
                        ddl_event_date_end_y.SelectedValue = date_end.Year.ToString();
                        ddl_event_date_end_d.Text = date_end.Day.ToString();
                        ddl_event_date_end_m.SelectedValue = date_end.Month.ToString();

                        ddl_date_end_hours.SelectedValue = (date_end.Hour * 60).ToString();
                        ddl_date_end_minutes.SelectedValue = date_end.Minute.ToString();
                        
                    }

                   

                }
                 conn.Close();
                
            }
        }
    }
    protected void btn_update_Click(object sender, EventArgs e)
    {
        lbl_update_confirmation.Visible = false;
        tb_confirmation.Visible = false;
        
        //*********************************************************************
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prEventUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        int success = 0;

        string event_date_begin;
        string event_date_end;
        string event_header;


        tiger.Date df = new tiger.Date();
        DateTime date_begin = Convert.ToDateTime(df.DateCulture(ddl_event_date_begin_m.SelectedValue, ddl_event_date_begin_d.SelectedValue, ddl_event_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        DateTime date_end = Convert.ToDateTime(df.DateCulture(ddl_event_date_end_m.SelectedValue, ddl_event_date_end_d.SelectedValue, ddl_event_date_end_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        date_begin = date_begin.AddMinutes(Convert.ToDouble(ddl_date_begin_hours.SelectedValue)).AddMinutes(Convert.ToDouble(ddl_date_begin_minutes.SelectedValue));
        date_end = date_end.AddMinutes(Convert.ToDouble(ddl_date_end_hours.SelectedValue)).AddMinutes(Convert.ToDouble(ddl_date_end_minutes.SelectedValue));
       

        if (date_end >= date_begin)
        {
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["ev_id"]);


                cmd.Parameters.Add("@event_date_begin", SqlDbType.DateTime).Value = date_begin;
                event_date_begin = ddl_event_date_begin_m.SelectedValue + "-" + ddl_event_date_begin_d.SelectedValue + "-" + ddl_event_date_begin_y.SelectedValue;

                cmd.Parameters.Add("@event_date_end", SqlDbType.DateTime).Value = date_end ;
                event_date_end = ddl_event_date_end_m.SelectedValue + "-" + ddl_event_date_end_d.SelectedValue + "-" + ddl_event_date_end_y.SelectedValue;


                cmd.Parameters.Add("@event_header", SqlDbType.NVarChar, 50).Value = tbx_event.Text;
                event_header = tbx_event.Text;

                cmd.Parameters.Add("@event_description", SqlDbType.NText).Value = tbx_description.Text;

                //execute the insert
                cmd.ExecuteNonQuery();
                //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

                success = Convert.ToInt32(cmd.Parameters["@return"].Value);
                if (success == 0)
                {
                    lbl_update_confirmation.Visible = true;
                    tb_confirmation.Visible = true;
                    
                    lbl_event.Text = event_header;
                    lbl_event_date_begin.Text = event_date_begin;
                    lbl_event_date_end.Text = event_date_end;

                    tbx_description.Text = "";
                    tbx_event.Text = "";

                }


            }
            catch (Exception error)
            {
                tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "manager/event_update.aspx", error.ToString());
            }

        }

    }
}
