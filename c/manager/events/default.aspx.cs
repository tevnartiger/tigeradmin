﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
/// <summary>
/// Copied and modified by : Stanley Jocelyn
/// Date    : july 22, 2008
/// code taken on the internet
/// from :  Neeraj Saluja
/// url : http://secure.codeproject.com/KB/aspnet/EventCalendar.aspx
/// </summary>
public partial class manager_events_default : BasePage
{

    private DataTable GetEvents()
    {
        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prEventList", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
           
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        finally
        {
            conn.Close();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        h_backcolor_no.Value = "1";

        if (!Page.IsPostBack)
        {
            Calendar1.EventStartDateColumnName = "event_date_begin";
            Calendar1.EventEndDateColumnName = "event_date_end";
            Calendar1.EventDescriptionColumnName = "event_description";
            Calendar1.EventHeaderColumnName = "event_header";
            Calendar1.EventBackColorName = "event_backcolor";
            Calendar1.EventForeColorName = "event_forecolor";
            Calendar1.EventSource = GetEvents();

          
            DayPilotCalendar1.DataSource = GetEvents();
            DayPilotCalendar1.HeaderDateFormat = "MMMM dd yyyy";
            DayPilotCalendar1.DataStartField = "event_date_begin";
            DayPilotCalendar1.DataEndField = "event_date_end";
            DayPilotCalendar1.DataTextField = "event_header";
            DayPilotCalendar1.DataValueField = "event_id";

            DateTime d = DateTime.Now ;
          //  while (d.DayOfWeek.ToString() != Calendar1.FirstDayOfWeek.ToString())
            {
              //  d = d.AddDays(-1);
            }
           
            DayPilotCalendar1.StartDate = d ; 
            DayPilotCalendar1.Days = 5;
            DayPilotCalendar1.DataBind();
           
        }
    }
    protected void Calendar1_SelectionChanged(object sender, EventArgs e)
    {
        SelectedDatesCollection theDates = Calendar1.SelectedDates;
        DataTable dtSelectedDateEvents = Calendar1.EventSource.Clone();

        DateTime minDayofWeek = new DateTime();
        minDayofWeek = DateTime.Now.AddYears(10);
        int days_selected = 0;

        DataRow dr;

        foreach (DataRow drEvent in Calendar1.EventSource.Rows)
            foreach (DateTime selectedDate in theDates)
                if (selectedDate.Date >= (Convert.ToDateTime(drEvent[Calendar1.EventStartDateColumnName])).Date
                    && selectedDate.Date <= (Convert.ToDateTime(drEvent[Calendar1.EventEndDateColumnName])).Date)
                {
                    if (selectedDate < minDayofWeek)
                    {
                        minDayofWeek = selectedDate;                       
                    }

                    days_selected++;

                    // This Condition is just to ensure that Every Event Details are added just only once
                    // irrespective of the number of days for which the event occurs.
                    if (dtSelectedDateEvents.Select("event_id= " + Convert.ToInt32(drEvent["event_id"])).Length > 0)
                        continue;

                    dr = dtSelectedDateEvents.NewRow();
                    dr["event_id"] = drEvent["event_id"];
                    dr[Calendar1.EventStartDateColumnName] = drEvent[Calendar1.EventStartDateColumnName];
                    dr[Calendar1.EventEndDateColumnName] = drEvent[Calendar1.EventEndDateColumnName];
                    dr[Calendar1.EventHeaderColumnName] = drEvent[Calendar1.EventHeaderColumnName];
                    dr[Calendar1.EventDescriptionColumnName] = drEvent[Calendar1.EventDescriptionColumnName];

                    dr[Calendar1.EventForeColorName] = drEvent[Calendar1.EventForeColorName];
                    dr[Calendar1.EventBackColorName] = drEvent[Calendar1.EventBackColorName];

                    dtSelectedDateEvents.Rows.Add(dr);
                }

        gvSelectedDateEvents.DataSource = dtSelectedDateEvents;
        gvSelectedDateEvents.DataBind();
       
    

        if (dtSelectedDateEvents.Rows.Count == 1)
        {
            DayPilotCalendar1.HeaderDateFormat = "MMMM dd yyyy";
            DayPilotCalendar1.DataSource = dtSelectedDateEvents;
            DayPilotCalendar1.StartDate = Calendar1.SelectedDate;
            DayPilotCalendar1.DataStartField = "event_date_begin";
            DayPilotCalendar1.DataEndField = "event_date_end";
            DayPilotCalendar1.DataTextField = "event_header";
            DayPilotCalendar1.DataValueField = "event_id";
            DayPilotCalendar1.Days = 1;
            DayPilotCalendar1.DataBind();

            DayPilotCalendar1.Visible = true;

        }
        


        if (dtSelectedDateEvents.Rows.Count > 1 && days_selected == 1)
        {
            DayPilotCalendar1.HeaderDateFormat = "MMMM dd yyyy";
            DayPilotCalendar1.DataSource = dtSelectedDateEvents;
            DayPilotCalendar1.StartDate = minDayofWeek;
            DayPilotCalendar1.DataStartField = "event_date_begin";
            DayPilotCalendar1.DataEndField = "event_date_end";
            DayPilotCalendar1.DataTextField = "event_header";
            DayPilotCalendar1.DataValueField = "event_id";

            
            DayPilotCalendar1.Days = 1;

            DayPilotCalendar1.DataBind();

            DayPilotCalendar1.Visible = true;

        }



        if (dtSelectedDateEvents.Rows.Count > 1 && days_selected > 1)
        {
            DayPilotCalendar1.HeaderDateFormat = "MMMM dd yyyy";
            DayPilotCalendar1.DataSource = dtSelectedDateEvents;
            DayPilotCalendar1.StartDate = minDayofWeek;
            DayPilotCalendar1.DataStartField = "event_date_begin";
            DayPilotCalendar1.DataEndField = "event_date_end";
            DayPilotCalendar1.DataTextField = "event_header";
            DayPilotCalendar1.DataValueField = "event_id";

            DayPilotCalendar1.Days = 5;

            DayPilotCalendar1.DataBind();

            DayPilotCalendar1.Visible = true;

        }



        if (dtSelectedDateEvents.Rows.Count < 1 )
        {
            DayPilotCalendar1.Visible = false;
        }


        if (gvSelectedDateEvents.Rows.Count > 0)
            hr_separator.Visible = true;
        else
            hr_separator.Visible = false;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_delete_Click(object sender, EventArgs e)
    {

     //*********************************************************************
        Button btn_delete = (Button)sender;
        GridViewRow grdRow = (GridViewRow)btn_delete.Parent.Parent;
        string strField1 = grdRow.Cells[5].Text;

        HiddenField h_event_id = (HiddenField)grdRow.Cells[5].FindControl("h_event_id");

       

         string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prEventDelete", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        conn.Open();
            //Add the params
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = Convert.ToInt32(h_event_id.Value);
            //execute the insert
        cmd.ExecuteNonQuery();
       



        //---------------------------------------------------------------------------
/*
        SelectedDatesCollection theDates = Calendar1.SelectedDates;
        DataTable dtSelectedDateEvents = Calendar1.EventSource.Clone();
        DataRow dr;

        foreach (DataRow drEvent in Calendar1.EventSource.Rows)
            foreach (DateTime selectedDate in theDates)
                if (selectedDate.Date >= (Convert.ToDateTime(drEvent[Calendar1.EventStartDateColumnName])).Date
                    && selectedDate.Date <= (Convert.ToDateTime(drEvent[Calendar1.EventEndDateColumnName])).Date)
                {
                    // This Condition is just to ensure that Every Event Details are added just only once
                    // irrespective of the number of days for which the event occurs.
                    if (dtSelectedDateEvents.Select("event_id= " + Convert.ToInt32(drEvent["event_id"])).Length > 0)
                        continue;

                    dr = dtSelectedDateEvents.NewRow();
                    dr["event_id"] = drEvent["event_id"];
                    dr[Calendar1.EventStartDateColumnName] = drEvent[Calendar1.EventStartDateColumnName];
                    dr[Calendar1.EventEndDateColumnName] = drEvent[Calendar1.EventEndDateColumnName];
                    dr[Calendar1.EventHeaderColumnName] = drEvent[Calendar1.EventHeaderColumnName];
                    dr[Calendar1.EventDescriptionColumnName] = drEvent[Calendar1.EventDescriptionColumnName];

                    dr[Calendar1.EventForeColorName] = drEvent[Calendar1.EventForeColorName];
                    dr[Calendar1.EventBackColorName] = drEvent[Calendar1.EventBackColorName];

                    dtSelectedDateEvents.Rows.Add(dr);
                }

        gvSelectedDateEvents.DataSource = dtSelectedDateEvents;
        gvSelectedDateEvents.DataBind();

        */
        Response.Redirect("default.aspx");
    }


}
