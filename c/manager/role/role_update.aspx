﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="role_update.aspx.cs" Inherits="manager_role_role_update" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script language="javascript" type="text/javascript">
        function ConfirmDelete()
        {
            if(confirm('Do you wish to delete this file/folder?'))
            return true;
           else
           return false;
        }
  
   

function CheckAllDataViewCheckBoxes( checkVal)
{

 for(i = 0; i < document.forms[0].elements.length; i++)
  {
    
    elm = document.forms[0].elements[i];
    if (elm.type == 'checkbox' && elm.name.substring(elm.name.length-11,elm.name.length) == 'chk_process' )
    {
     {elm.checked = checkVal }
    }
  }
}

</script>

 
     
      <b><span style="font-size: small">Role Assignement Update</span></b><br />
     <br />
     &nbsp;
     Name&nbsp;&nbsp; <asp:DropDownList ID="ddl_name_id_1"   
        DataValueField="name_id" DataTextField="name_name" runat="server" 
        style="height: 16px; margin-top: 4px" 
         AutoPostBack="true"
        onselectedindexchanged="ddl_name_id_1_SelectedIndexChanged" />
     <br />
     &nbsp; Person :<asp:Label ID="lbl_person" runat="server" ></asp:Label>
     <br />
       
     <br />
     <asp:GridView ID="gv_person_role" runat="server" AutoGenerateColumns="false" 
         Width="40%">
         <Columns>
             <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_current_role_assignement %>">
                 <ItemTemplate>
                     <asp:Label ID="lbl_role_name" runat="server" 
                         Text='<%#GetRoleName(Convert.ToInt32(Eval("role_id")))%>' />
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_property %>">
                 <ItemTemplate>
                     <asp:Repeater ID="r_property" runat="server" 
                         DataSource='<%#GetPropertyForNameRole(Convert.ToInt32(Eval("role_id")))%>'>
                         <ItemTemplate>
                             <%# Eval("home_name")%><br />
                         </ItemTemplate>
                     </asp:Repeater>
                 </ItemTemplate>
             </asp:TemplateField>
         </Columns>
     </asp:GridView>
     <hr />
     &nbsp;Select Role Assignement to update :&nbsp;      
    <asp:DropDownList ID="ddl_role" runat="server" 
        AutoPostBack="true"
        onselectedindexchanged="ddl_role_SelectedIndexChanged">
         <asp:ListItem Value="6">Property Manager</asp:ListItem>
         <asp:ListItem Value="5">Maintenance</asp:ListItem>
         <asp:ListItem Text="<%$ Resources:Resource, lbl_owner %>"  Value="4"></asp:ListItem>
     </asp:DropDownList>
     
        <hr />
        Select the properties assigned to the person for his/her role<br />
      <asp:Button ID="btn_update" runat="server" 
        Text="<%$ Resources:Resource, lbl_update %>" onclick="btn_update_Click" />
       
     <br />
        <asp:GridView ID="gv_home_list" runat="server" AllowPaging="True"  
            AllowSorting="True" AutoGenerateColumns="False" BorderColor="#CDCDCD" 
            BorderWidth="1px" PageSize="20">
            <AlternatingRowStyle BackColor="#F0F0F6" />
            <Columns>
                <asp:TemplateField HeaderText="&lt;input id='chkAllItems' type='checkbox' onclick='CheckAllDataViewCheckBoxes(document.forms[0].chkAllItems.checked)' /&gt;">
                    <ItemTemplate>
                        <asp:CheckBox ID="chk_process" runat="server" />
                        <asp:HiddenField ID="h_home_id" runat="server" Value='<%# Bind("home_id")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="home_name" HeaderText="Property name" />
                <asp:HyperLinkField DataNavigateUrlFields="home_id" 
                    DataNavigateUrlFormatString="~/manager/property/property_view.aspx?h_id={0}" 
                    HeaderText="View" Text="View" />
            </Columns>
            <HeaderStyle BackColor="#F0F0F6" />
        </asp:GridView>
      
      
        <br />
        <asp:Label ID="lbl_success" runat="server" Text=""></asp:Label>
      
      
 



</asp:Content>

