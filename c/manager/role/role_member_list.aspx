﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="role_member_list.aspx.cs" Inherits="manager_role_role_member_list" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <table width="100%">
           <tr>
               <td bgcolor="aliceblue">
                <b>  <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_u_members %>"> </asp:Label></b>&nbsp;</td>
           </tr>
       </table>
       &nbsp;<br />
       <asp:Label ID="lbl_member_search" runat="server" style="font-weight: 700" 
           Text="<%$ Resources:Resource, lbl_member_search %>"></asp:Label>
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
       &nbsp;&nbsp;&nbsp;&nbsp;
       <asp:Button ID="submit" runat="server" onclick="submit_Click" 
           Text="<%$ Resources:Resource, lbl_submit %>" style="height: 26px" />
       <br />
    <hr />      
       <br />
       &nbsp;<asp:HyperLink ID="link_a" runat="server" CssClass="style2" >[A]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_b" runat="server" CssClass="style2" >[B]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_c" runat="server" CssClass="style2" >[C]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_d" runat="server" CssClass="style2" >[D]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_e" runat="server" CssClass="style2" >[E]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_f" runat="server" CssClass="style2" >[F]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_g" runat="server" CssClass="style2" >[G]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_h" runat="server" CssClass="style2" >[H]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_i" runat="server" CssClass="style2" >[I]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_j" runat="server" CssClass="style2" >[J]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_k" runat="server" CssClass="style2" >[K]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_l" runat="server" CssClass="style2" >[L]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_m" runat="server" CssClass="style2" >[M]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_n" runat="server" CssClass="style2" >[N]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_o" runat="server" CssClass="style2" >[O]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_p" runat="server" CssClass="style2" >[P]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_q" runat="server" CssClass="style2" >[Q]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_r" runat="server" CssClass="style2" >[R]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_s" runat="server" CssClass="style2" >[S]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_t" runat="server" CssClass="style2" >[T]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_u" runat="server" CssClass="style2" >[U]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_v" runat="server" CssClass="style2" >[V]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_w" runat="server" CssClass="style2">[W]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_x" runat="server" CssClass="style2" >[X]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_y" runat="server" CssClass="style2" >[Y]</asp:HyperLink>
       <span class="style2">&nbsp;
       </span>
       <asp:HyperLink ID="link_z" runat="server" CssClass="style2" >[Z]</asp:HyperLink>
       <br />
       <br />
       <b>Member category</b>&nbsp;
       <asp:DropDownList ID="ddl_member_category" runat="server"  AutoPostBack="true"
        onselectedindexchanged=" ddl_member_category_SelectedIndexChanged">
           <asp:ListItem Text="<%$ Resources:Resource, lbl_all %>" Value="0"></asp:ListItem>
           <asp:ListItem Text="<%$ Resources:Resource, lbl_maintenance_worker %>"  Value="5"></asp:ListItem>
           <asp:ListItem Text="<%$ Resources:Resource, lbl_owner %>" Value="4"></asp:ListItem>
          <asp:ListItem Text="<%$ Resources:Resource, lbl_property_manager %>" Value="6"></asp:ListItem>             
         <asp:ListItem Text="<%$ Resources:Resource, lbl_tenant %>" Value="3"></asp:ListItem>
       
       </asp:DropDownList>
       <br />
       <br />
       <asp:GridView ID="gv_member_list" runat="server" AllowPaging="True" 
           AllowSorting="True" AlternatingRowStyle-BackColor="#F0F0F6"  
           AutoGenerateColumns="false" BorderColor="#CDCDCD" BorderWidth="1" 
           GridLines="Both"
           DataKeyNames="name_id" OnPageIndexChanging="gv_member_list_PageIndexChanging"
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" 
           HeaderStyle-BackColor="AliceBlue" PageSize="10" Width="90%">
           
 
           
          
           
           
           <Columns>
               <asp:BoundField DataField="lname" 
                   HeaderText="<%$ Resources:Resource, lbl_lname %>" ReadOnly="True" 
                   />
               <asp:BoundField DataField="fname" 
                   HeaderText="<%$ Resources:Resource, lbl_fname %>" ReadOnly="True" 
                    />
               <asp:BoundField DataField="tel" 
                   HeaderText="<%$ Resources:Resource, lbl_tel %>" 
                    />
               <asp:BoundField DataField="email" 
                   HeaderText="<%$ Resources:Resource, lbl_email %>" 
                   />
               
               <asp:TemplateField HeaderText='<%$ Resources:Resource, lbl_role  %>' >
               <ItemTemplate>
               <asp:Label runat="server"  ID="lbl_categ1"  
                          Text='<%#Get_RoleCateg(Convert.ToInt32(Eval("name_id")))%>'/> 
               </ItemTemplate>
               </asp:TemplateField>
               
              
                
               <asp:HyperLinkField  Text="<%$ Resources:Resource, lbl_view %>" 
              DataNavigateUrlFields="name_id" 
              DataNavigateUrlFormatString="~/manager/name/name_profile.aspx?name_id={0}" 
              HeaderText="<%$ Resources:Resource, lbl_view %>" />
               
           </Columns>
       </asp:GridView>
       <asp:GridView ID="gv_member_search_list" runat="server" AllowPaging="True" 
           AllowSorting="True" AlternatingRowStyle-BackColor="#F0F0F6"  
           AutoGenerateColumns="false" BorderColor="#CDCDCD" BorderWidth="1" 
           GridLines="Both" EmptyDataText="<%$ Resources:Resource, lbl_none %>" 
           HeaderStyle-BackColor="AliceBlue" 
           OnPageIndexChanging="gv_member_search_list_PageIndexChanging" PageSize="15" 
           Width="90%">
           <Columns>
               <asp:BoundField DataField="lname" 
                   HeaderText="<%$ Resources:Resource, lbl_lname %>" ReadOnly="True" 
                    />
               <asp:BoundField DataField="fname" 
                   HeaderText="<%$ Resources:Resource, lbl_fname %>" ReadOnly="True" 
                   />
               <asp:BoundField DataField="tel" 
                   HeaderText="<%$ Resources:Resource, lbl_tel %>" 
                   />
               <asp:BoundField DataField="email" 
                   HeaderText="<%$ Resources:Resource, lbl_email %>" 
                   />
             
               <asp:TemplateField HeaderText='<%$ Resources:Resource, lbl_role  %>' >
               <ItemTemplate>
               <asp:Label runat="server"  ID="lbl_categ2"  
                          Text='<%#Get_RoleCateg(Convert.ToInt32(Eval("name_id")))%>'/> 
               </ItemTemplate>
               </asp:TemplateField>
                   
              <asp:HyperLinkField  Text="<%$ Resources:Resource, lbl_view %>" 
              DataNavigateUrlFields="name_id" 
              DataNavigateUrlFormatString="~/manager/name/name_profile.aspx?name_id={0}" 
              HeaderText="<%$ Resources:Resource, lbl_view %>" />
               
               
           </Columns>
       </asp:GridView>



</asp:Content>

<asp:Content ID="Content2" runat="server" contentplaceholderid="head">

        <%  //Response.Cache.SetExpires(DateTime.Now.AddMinutes(5)); %><style 
        type="text/css">                                                                           .style2
                                                                           {
                                                                               font-size: xx-small;
                                                                           }
                                                                       </style>

</asp:Content>


