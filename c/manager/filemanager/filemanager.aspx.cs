using System;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Briefcase;
using System.IO;

// Done by : Stanley Jocelyn
// Date : sept 12 , 2008
// Briefcase
public partial class _default : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
           // ddl_home_list.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
           // ddl_home_list.DataBind();
           // ddl_home_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
            int home_count = h.getHomeCount(Convert.ToInt32(Session["schema_id"]));

            if (home_count > 0)
            {
                int home_id = h.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));

                ddl_home_owner_list.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                ddl_home_owner_list.DataBind();
                ddl_home_owner_list.SelectedValue = Convert.ToString(home_id);

                lbl_owner_list.Text = getOwnerProfile(home_id);
   

                ddl_home_tenant_list.DataSource = ddl_home_owner_list.DataSource;
                ddl_home_tenant_list.DataBind();
                ddl_home_tenant_list.SelectedValue = Convert.ToString(home_id);


                ddl_home_pm_list.DataSource = ddl_home_owner_list.DataSource;
                ddl_home_pm_list.DataBind();
                ddl_home_pm_list.SelectedValue = Convert.ToString(home_id);

                lbl_pm_list.Text = getManagerProfile(home_id);
   
                ddl_home_janitor_list.DataSource = ddl_home_owner_list.DataSource;
                ddl_home_janitor_list.DataBind();
                ddl_home_janitor_list.SelectedValue = Convert.ToString(home_id);

                lbl_janitor_list.Text = getJanitorProfile(home_id);
   
              

                TenantBindGrid();
                OwnerBindGrid();
                PMBindGrid();
                JanitorBindGrid();        
            }

        }
    }
  private void OwnerBindGrid()
    {

        DataTable files = Files.GetFilesForOwner(int.Parse(ddl_home_owner_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
        GridView2.DataSource = files;
        GridView2.DataBind();
    }


    private void TenantBindGrid()
    {

        DataTable files = Files.GetFilesForTenant(int.Parse(ddl_home_tenant_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
        GridView3.DataSource = files;
        GridView3.DataBind();
    }



    private void PMBindGrid()
    {

        DataTable files = Files.GetFilesForPM(int.Parse(ddl_home_pm_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
        GridView4.DataSource = files;
        GridView4.DataBind();
    }

    private void JanitorBindGrid()
    {

        DataTable files = Files.GetFilesForJanitor(int.Parse(ddl_home_janitor_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
        GridView5.DataSource = files;
        GridView5.DataBind();
    }
    
 
   
    protected void ddl_home_owner_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable files = Files.GetFilesForOwner(int.Parse(ddl_home_owner_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
        GridView2.DataSource = files;
        GridView2.DataBind();

        lbl_owner_list.Text = getOwnerProfile(Convert.ToInt32(ddl_home_owner_list.SelectedValue));

        TabContainer1.ActiveTabIndex = 0;

    }


    protected void ddl_home_tenant_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable files = Files.GetFilesForTenant(int.Parse(ddl_home_tenant_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
        GridView3.DataSource = files;
        GridView3.DataBind();

        TabContainer1.ActiveTabIndex = 1;

    }


    protected void ddl_home_pm_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable files = Files.GetFilesForPM(int.Parse(ddl_home_pm_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
        GridView4.DataSource = files;
        GridView4.DataBind();

         lbl_pm_list.Text = getManagerProfile(Convert.ToInt32(ddl_home_pm_list.SelectedValue));


        TabContainer1.ActiveTabIndex = 2;

    }


    protected void ddl_home_janitor_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable files = Files.GetFilesForJanitor(int.Parse(ddl_home_janitor_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
        GridView5.DataSource = files;
        GridView5.DataBind();

         lbl_janitor_list.Text = getJanitorProfile(Convert.ToInt32(ddl_home_janitor_list.SelectedValue));


        TabContainer1.ActiveTabIndex = 3;

    }


  
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }

    protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int fileid = int.Parse(GridView2.DataKeys[int.Parse(e.CommandArgument.ToString())].Value.ToString());

        if (e.CommandName == "Download")
        {
            DataTable file = Files.OwnerGetFile(fileid, Convert.ToInt32(Session["schema_id"]));
            byte[] filedata = (byte[])file.Rows[0]["filedata"];
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment;filename=" + file.Rows[0]["filename"]);
            Response.BinaryWrite(filedata);
            Response.End();
        }

        TabContainer1.ActiveTabIndex = 0;
    }
    protected void GridView2_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView2.EditIndex = e.NewEditIndex;
        OwnerBindGrid();
        TabContainer1.ActiveTabIndex = 0;
    }
    protected void GridView2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView2.EditIndex = -1;
        OwnerBindGrid();
        TabContainer1.ActiveTabIndex = 0;
    }
    protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int fileid = int.Parse(GridView2.DataKeys[e.RowIndex].Value.ToString());
        GridViewRow row = GridView2.Rows[e.RowIndex];
        Files.OwnerRename(fileid, ((TextBox)row.Cells[0].FindControl("TextBox1")).Text, Convert.ToInt32(Session["schema_id"])
                       , Convert.ToInt32(Session["name_id"]) , Request.UserHostAddress.ToString());
        GridView2.EditIndex = -1;
        OwnerBindGrid();
        TabContainer1.ActiveTabIndex = 0;
    }

    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }

 
    protected void GridView3_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int fileid = int.Parse(GridView3.DataKeys[int.Parse(e.CommandArgument.ToString())].Value.ToString());

        if (e.CommandName == "Download")
        {
            DataTable file = Files.TenantGetFile(fileid, Convert.ToInt32(Session["schema_id"]));
            byte[] filedata = (byte[])file.Rows[0]["filedata"];
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment;filename=" + file.Rows[0]["filename"]);
            Response.BinaryWrite(filedata);
            Response.End();
        }
    }
    protected void GridView3_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView3.EditIndex = e.NewEditIndex;
        TenantBindGrid();
        TabContainer1.ActiveTabIndex = 1;
    }
    protected void GridView3_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView3.EditIndex = -1;
        TenantBindGrid();
    }
    protected void GridView3_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int fileid = int.Parse(GridView3.DataKeys[e.RowIndex].Value.ToString());
        GridViewRow row = GridView3.Rows[e.RowIndex];
        Files.TenantRename(fileid, ((TextBox)row.Cells[0].FindControl("TextBox1")).Text, Convert.ToInt32(Session["schema_id"])
                            , Convert.ToInt32(Session["name_id"]) , Request.UserHostAddress.ToString());
        GridView3.EditIndex = -1;
        TenantBindGrid();

        TabContainer1.ActiveTabIndex = 1;
    }

    protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }


    protected void GridView4_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int fileid = int.Parse(GridView4.DataKeys[int.Parse(e.CommandArgument.ToString())].Value.ToString());

        if (e.CommandName == "Download")
        {
            DataTable file = Files.PMGetFile(fileid, Convert.ToInt32(Session["schema_id"]));
            byte[] filedata = (byte[])file.Rows[0]["filedata"];
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment;filename=" + file.Rows[0]["filename"]);
            Response.BinaryWrite(filedata);
            Response.End();
        }
    }
    protected void GridView4_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView4.EditIndex = e.NewEditIndex;
        PMBindGrid();
        TabContainer1.ActiveTabIndex = 2;
    }
    protected void GridView4_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView4.EditIndex = -1;
        PMBindGrid();
    }
    protected void GridView4_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int fileid = int.Parse(GridView4.DataKeys[e.RowIndex].Value.ToString());
        GridViewRow row = GridView4.Rows[e.RowIndex];
        Files.PMRename(fileid, ((TextBox)row.Cells[0].FindControl("TextBox1")).Text, Convert.ToInt32(Session["schema_id"])
                            , Convert.ToInt32(Session["name_id"]), Request.UserHostAddress.ToString());
        GridView4.EditIndex = -1;
        PMBindGrid();

        TabContainer1.ActiveTabIndex = 2;
    }

    protected void GridView4_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }



    protected void GridView5_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int fileid = int.Parse(GridView5.DataKeys[int.Parse(e.CommandArgument.ToString())].Value.ToString());

        if (e.CommandName == "Download")
        {
            DataTable file = Files.JanitorGetFile(fileid, Convert.ToInt32(Session["schema_id"]));
            byte[] filedata = (byte[])file.Rows[0]["filedata"];
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment;filename=" + file.Rows[0]["filename"]);
            Response.BinaryWrite(filedata);
            Response.End();
        }
    }
    protected void GridView5_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView5.EditIndex = e.NewEditIndex;
        JanitorBindGrid();
        TabContainer1.ActiveTabIndex = 3;
    }
    protected void GridView5_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView5.EditIndex = -1;
        JanitorBindGrid();
    }
    protected void GridView5_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int fileid = int.Parse(GridView5.DataKeys[e.RowIndex].Value.ToString());
        GridViewRow row = GridView5.Rows[e.RowIndex];
        Files.JanitorRename(fileid, ((TextBox)row.Cells[0].FindControl("TextBox1")).Text, Convert.ToInt32(Session["schema_id"])
                            , Convert.ToInt32(Session["name_id"]), Request.UserHostAddress.ToString());
        GridView5.EditIndex = -1;
        JanitorBindGrid();

        TabContainer1.ActiveTabIndex = 3;
    }

    protected void GridView5_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }

    protected void btn_delete_Click(object sender, ImageClickEventArgs e)
    {
        int fileid = int.Parse(((ImageButton)sender).CommandArgument);
        Files.OwnerDelete(fileid, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_owner_list.SelectedValue));
        OwnerBindGrid();
        TabContainer1.ActiveTabIndex = 0;
    }


    protected void btn_tenant_delete_Click(object sender, ImageClickEventArgs e)
    {
        int fileid = int.Parse(((ImageButton)sender).CommandArgument);
        Files.TenantDelete(fileid, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_tenant_list.SelectedValue));
        TenantBindGrid();
        TabContainer1.ActiveTabIndex = 1;
    }


    protected void btn_pm_delete_Click(object sender, ImageClickEventArgs e)
    {
        int fileid = int.Parse(((ImageButton)sender).CommandArgument);
        Files.PMDelete(fileid, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_pm_list.SelectedValue));
        PMBindGrid();
        TabContainer1.ActiveTabIndex = 2;
    }

    protected void btn_janitor_delete_Click(object sender, ImageClickEventArgs e)
    {
        int fileid = int.Parse(((ImageButton)sender).CommandArgument);
        Files.JanitorDelete(fileid, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_janitor_list.SelectedValue));
        JanitorBindGrid();
        TabContainer1.ActiveTabIndex = 3;
    }

    protected string getOwnerProfile(int home_id)
    {
        string str_view = "";
        //get owner
        SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]); 
        SqlCommand cmd = new SqlCommand("prProfileOwnerInfo", conn);
        cmd.CommandType = CommandType.StoredProcedure;


        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

        try
        {
            conn.Open();

            SqlDataReader dr = null;
            dr = cmd.ExecuteReader();

            while (dr.Read() == true)
            {
                str_view += "<a href='/manager/name/name_profile.aspx?name_id=" + Convert.ToString(dr["name_id"]) + "'>" + Convert.ToString(dr["owner"]) + "</a><br/>"
                    + Convert.ToString(dr["owner_tel"]);

             
                str_view += Convert.ToString(dr["owner_email"]);
                if (Convert.ToString(dr["owner_email"]) == null || Convert.ToString(dr["owner_email"]) == string.Empty)
                    str_view += "";
                else
                    str_view += "<br/>";
                //get owner
            }

            return str_view;
        }
        finally
        {
            conn.Close();
        }

    }

     protected string getManagerProfile(int home_id)
        {
            string str_view = "";
            //get owner
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]); 
            SqlCommand cmd = new SqlCommand("prProfileManagerInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader();

                while (dr.Read() == true)
                {
                    str_view += "<a href='/manager/name/name_profile.aspx?name_id=" + Convert.ToString(dr["name_id"]) + "'>" + Convert.ToString(dr["manager"]) + "</a><br/>"
                      + Convert.ToString(dr["manager_tel"]);

                   if( Convert.ToString(dr["manager_tel"])== null || Convert.ToString(dr["manager_tel"])==string.Empty)
                    str_view += "";
                     else
                    str_view += "<br/>";


                   str_view += Convert.ToString(dr["manager_email"]) ;
                    if (Convert.ToString(dr["manager_email"]) == null || Convert.ToString(dr["manager_email"]) == string.Empty)
                        str_view += "";
                    else
                        str_view += "<br/>";



                    //get owner
                }

                return str_view;
            }
            finally
            {
                conn.Close();
            }



        }

        protected string getJanitorProfile(int home_id)
        {
            //get Janitor
            string str_view = "";
            //get owner
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]); 
            SqlCommand cmd = new SqlCommand("prProfileJanitorInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader();

                while (dr.Read() == true)
                {
                    str_view += "<a href='/manager/name/name_profile.aspx?name_id=" + Convert.ToString(dr["name_id"]) + "'>" + Convert.ToString(dr["janitor"]) + "</a><br/>"
                        + Convert.ToString(dr["janitor_tel"]);

                    if (Convert.ToString(dr["janitor_tel"]) == null || Convert.ToString(dr["janitor_tel"]) == string.Empty)
                        str_view += "";
                    else
                        str_view += "<br/>";

                    str_view += Convert.ToString(dr["janitor_email"]);
                    if (Convert.ToString(dr["janitor_email"]) == null || Convert.ToString(dr["janitor_email"]) == string.Empty)
                        str_view += "";
                    else
                        str_view += "<br/>";
                    //get owner
                }

                return str_view;
            }
            finally
            {
                conn.Close();
            }

        }



}
