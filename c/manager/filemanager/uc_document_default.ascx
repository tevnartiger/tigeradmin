﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_document_default.ascx.cs" Inherits="c_manager_filemanager_uc_document_default" %>
<br />

<table>


<tr><td> <asp:HyperLink ID="HyperLink2" NavigateUrl="~/manager/filemanager/filemanager_myfiles.aspx" runat="server"><h2>  <asp:Literal ID="Literal2" Text="My files" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to manage your files.</td></tr>


<tr><td> <asp:HyperLink ID="HyperLink6" NavigateUrl="~/manager/filemanager/filemanager.aspx" runat="server"><h2>  <asp:Literal ID="Literal6" Text="Shared files" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to view uploaded files for the the different groups of users.</td></tr>


<tr><td> <asp:HyperLink ID="HyperLink1" NavigateUrl="~/manager/filemanager/filemanager_upload.aspx" runat="server"><h2>  <asp:Literal ID="Literal1" Text="Upload a file" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to upload a file</td></tr>

       
  </table>
