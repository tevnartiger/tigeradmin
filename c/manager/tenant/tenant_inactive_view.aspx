﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="tenant_inactive_view.aspx.cs" Inherits="manager_tenant_tenant_inactive_view" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

<h4>INACTIVE <b>TENANT&nbsp; - FOLDER</b></h4>
    <asp:HyperLink ID="tenantapplication_add_link" 
            runat="server">add </asp:HyperLink> -&nbsp;  <asp:HyperLink ID="tenantapplication_update_link"
            runat="server">update</asp:HyperLink> - delete mortgage
        &nbsp;<br />
        <br />
        <b>NAME&nbsp; </b>: <b><asp:Label ID="lbl_name_lname"  runat="server"/>&nbsp;, <asp:Label ID="lbl_name_fname"  runat="server"/>
        </b>
        <br />
        <b>APPLICATION DATE :&nbsp;&nbsp;<asp:Label ID="lbl_tenantapplication_date_insert" runat="server"  Text=""></asp:Label>
        </b>
        <br />
        <br />
 
<cc1:TabContainer ID="TabContainer1" runat="server"  >
  <cc1:TabPanel ID="tab1" HeaderText="Application" runat="server"   >
    <ContentTemplate  >
           <table width="100%">
               <tr>
                   <td bgcolor="aliceblue">
                       <b>APPLICATION INFORMATION</b></td>
               </tr>
           </table>
           <br />

       <table bgcolor="#ffffcc" border="0" cellpadding="1" cellspacing="3">
           <tr>
               <td>
                   date of birth (mm / dd /yyyy )</td>
               <td colspan="2">
                   <asp:Label ID="lbl_name_dob" runat="server" />
               </td>
           </tr>
           <tr>
               <td>
                   address</td>
               <td>
                   <asp:Label ID="lbl_name_addr" runat="server"></asp:Label>
               </td>
               <td>
                   City</td>
               <td>
                   <asp:Label ID="lbl_name_addr_city" runat="server" />
               </td>
           </tr>
           <tr>
               <td>
                   Pc</td>
               <td>
                   <asp:Label ID="lbl_name_addr_pc" runat="server" />
               </td>
               <td>
                   State/Prov</td>
               <td>
                   <asp:Label ID="lbl_name_addr_state" runat="server" />
               </td>
           </tr>
           <tr>
               <td>
                   Current employer</td>
               <td>
                   <asp:Label ID="lbl_tenantapplication_current_employer" runat="server"></asp:Label>
               </td>
               <td>
                   Monthly Income</td>
               <td>
                   <asp:Label ID="lbl_tenantapplication_monthly_income" runat="server"></asp:Label>
               </td>
           </tr>
           <tr>
               <td>
                   Current employer since ( mm / dd / yyyy )</td>
               <td colspan="3">
                   <asp:Label ID="lbl_tenantapplication_employer_since" runat="server"></asp:Label>
               </td>
           </tr>
           <tr>
               <td valign="top">
                   People who will live in the property with you</td>
               <td colspan="3">
                   &nbsp;
                   <asp:Label ID="lbl_tenantapplication_people_with_prospect" runat="server" 
                       Height="50px" TextMode="MultiLine" Width="350px"></asp:Label>
               </td>
           </tr>
           <tr>
               <td>
                   Current landlord`s name</td>
               <td colspan="3">
                   <asp:Label ID="lbl_tenantapplication_landlord_name" runat="server"></asp:Label>
               </td>
           </tr>
           <tr>
               <td>
                   Current landlord &nbsp;telephone</td>
               <td colspan="3">
                   <asp:Label ID="lbl_tenantapplication_landlord_tel" runat="server"></asp:Label>
               </td>
           </tr>
           <tr>
               <td valign="top">
                   Reason of departure</td>
               <td colspan="3">
                   &nbsp;
                   <asp:Label ID="lbl_tenantapplication_reason_depart" runat="server" 
                       Height="50px" TextMode="MultiLine" Width="350px"></asp:Label>
               </td>
           </tr>
           <tr>
               <td>
                   SSN</td>
               <td colspan="3">
                   <asp:Label ID="lbl_name_ssn" runat="server"></asp:Label>
               </td>
           </tr>
           <tr>
               <td>
                   Country</td>
               <td colspan="3">
                   <asp:Label ID="lbl_country_name" runat="server" />
               </td>
           </tr>
           <tr>
               <td>
                   Telephone</td>
               <td>
                   <asp:Label ID="lbl_name_tel" runat="server" />
               </td>
               <td>
                   Tel. Work</td>
               <td>
                   <asp:Label ID="lbl_name_tel_work" runat="server" />
               </td>
           </tr>
           <tr>
               <td>
                   Tel. Work ext.</td>
               <td>
                   <asp:Label ID="lbl_name_tel_work_ext" runat="server" />
               </td>
               <td>
                   Cell</td>
               <td>
                   <asp:Label ID="lbl_name_cell" runat="server" />
               </td>
           </tr>
           <tr>
               <td>
                   Fax</td>
               <td>
                   <asp:Label ID="lbl_name_fax" runat="server" />
               </td>
               <td>
                   Email</td>
               <td>
                   <asp:Label ID="lbl_name_email" runat="server" />
               </td>
           </tr>
           <tr>
               <td valign="top">
                   Comments</td>
               <td colspan="3">
                   <asp:Label ID="lbl_name_com" runat="server" TextMode="MultiLine" 
                       Width="389px" />
               </td>
           </tr>
       </table>
       <br />
       <br />
       <table bgcolor="#ffffcc" border="0" cellpadding="1" cellspacing="3">
           <tr>
               <td>
                   Minimum price</td>
               <td>
                   <asp:Label ID="lbl_pt_rent_min" runat="server"> </asp:Label>
               </td>
               <td>
                   Maximum price</td>
               <td>
                   <asp:Label ID="lbl_pt_rent_max" runat="server"> </asp:Label>
               </td>
           </tr>
           <tr>
               <td>
                   Bedrooms</td>
               <td>
                   <asp:Label ID="lbl_pt_bedroom_no" runat="server"> </asp:Label>
               </td>
               <td>
                   Bathrooms</td>
               <td>
                   <asp:Label ID="lbl_pt_bathroom_no" runat="server"> </asp:Label>
               </td>
           </tr>
           <tr>
               <td>
                   Min. square foot</td>
               <td colspan="3">
                   <asp:Label ID="lbl_pt_min_sqft" runat="server"> </asp:Label>
               </td>
           </tr>
           <tr>
               <td>
                   Begining date (mm/dd/yyyy )
               </td>
               <td colspan="3">
                   <asp:Label ID="lbl_pt_date_begin" runat="server" Width="57px"></asp:Label>
               </td>
           </tr>
           <tr>
               <td>
                   <strong><span>Utilities</span></strong><br />
                   <asp:CheckBox ID="pt_electricity_inc" runat="server" text="Electricity" />
                   <br />
                   <asp:CheckBox ID="pt_heat_inc" runat="server" text="Heat" />
                   <br />
                   <asp:CheckBox ID="pt_water_inc" runat="server" text="Water" />
                   <br />
                   <asp:CheckBox ID="pt_water_tax_inc" runat="server" text="Water tax" />
               </td>
               <td>
               </td>
               <td valign="top">
                   <span><strong>Accomodations</strong></span><br />
                   <asp:CheckBox ID="pt_parking_inc" runat="server" text="Parking" />
                   <br />
                   <asp:CheckBox ID="pt_garage_inc" runat="server" text="Garage" />
                   <br />
                   <asp:CheckBox ID="pt_furnished_inc" runat="server" text="Furnished" />
                   <br />
                   <asp:CheckBox ID="pt_semi_furnished_inc" runat="server" text="Semi-furnished" />
                   <br />
               </td>
           </tr>
           <tr>
               <td>
                   Comments</td>
               <td colspan="3">
                   <asp:Label ID="pt_com" runat="server" TextMode="MultiLine" Width="389px" />
               </td>
           </tr>
       </table>
        
        
        
        
        
        
        
        
        
        
     </ContentTemplate>
  </cc1:TabPanel>
     
     
     
     
     
     
     
     
    <cc1:TabPanel ID="tab2" HeaderText="Payment History" runat="server"   >
    <ContentTemplate  >
        
        
        
        
        
        
        
        
        <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b>PAYMENT HISTORY</b>
                </td>
            </tr>
        </table>
        <asp:GridView ID="gv_tenant_payment_archive" runat="server" AllowPaging="true" 
            AllowSorting="true" 
            AutoGenerateColumns="false" 
            EmptyDataText="NONE" GridLines="Both" 
            AlternatingRowStyle-BackColor="#F0F0F6" 
              BorderColor="#CDCDCD"  BorderWidth="1"
              HeaderStyle-BackColor="#F0F0F6"
            OnPageIndexChanging="gv_tenant_payment_archive_PageIndexChanging" PageSize="10" 
            Width="70%">
            <Columns>
                <asp:BoundField DataField="home_name" HeaderText="Property" />
                <asp:BoundField DataField="unit_door_no" HeaderText="Unit #" />
                <asp:BoundField DataField="rp_due_date" DataFormatString="{0:M-dd-yyyy}" 
                    HeaderText="Due date" />
                <asp:BoundField DataField="rp_paid_date" DataFormatString="{0:M-dd-yyyy}" 
                    HeaderText="Paid Date" />
            </Columns>
        </asp:GridView>
        
        
        
        
        
        
        
        
        
        <br />
       
        
        <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b>AMOUNT OF RENTS PAID ON TIME</b>
                </td>
            </tr>
        </table>
        <br />
        </b>
        <asp:Repeater ID="r_ontime_rent" runat="server">
            <ItemTemplate>
                Number of rent paid on time&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <%# Eval("ontime")%>
                <br />
                Number of payments maid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <%# Eval("total")%>
                <br />
                Percentage ( % ) &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<%# Eval("percentage")%>
            </ItemTemplate>
        </asp:Repeater>
        <br />
        
        
        <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b>AMOUNT OF LATE RENT&nbsp; PAYMENTS SINCE APPLICATION </b>
                </td>
            </tr>
        </table>
        
        
        <br />
        <br />
        <asp:Repeater ID="Repeater1" runat="server">
            <ItemTemplate>
                LESS THAN 31 DAYS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <%# Eval("less31")%>
                <br />
                BETWEEN 31 AND 60 DAYS&nbsp; : <%# Eval("between31and60")%>
                <br />
                BETWEEN 61 AND 90 DAYS&nbsp; :&nbsp;<%# Eval("between61and90")%>
                <br />
                MORE THAN 90 DAYS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;<%# Eval("more91")%>
            </ItemTemplate>
        </asp:Repeater>
        <br />
        <br />
        <br />
        <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b>LATE RENT LIST</b>
                </td>
            </tr>
        </table>
        <br />
        <asp:GridView ID="gv_tenant_late_payment_archive" runat="server" 
            AutoGenerateColumns="false" 
            EmptyDataText="NONE" GridLines="Both" 
            AlternatingRowStyle-BackColor="#F0F0F6" 
              BorderColor="#CDCDCD"  BorderWidth="1"
              HeaderStyle-BackColor="#F0F0F6"
             Width="70%">
            <Columns>
                <asp:BoundField DataField="home_name" HeaderText="Property" />
                <asp:BoundField DataField="unit_door_no" HeaderText="Unit #" />
                <asp:BoundField DataField="rp_due_date" DataFormatString="{0:MM-dd-yyyy}" 
                    HeaderText="Due date" />
                <asp:BoundField DataField="rp_paid_date" DataFormatString="{0:MM-dd-yyyy}" 
                    HeaderText="Paid Date" />
            </Columns>
        </asp:GridView>
        <br />
        
        
        
        
        
        
        
        
        
      </ContentTemplate>
  </cc1:TabPanel>
    
    
    
    
    <cc1:TabPanel ID="tab3" HeaderText="Lease atchive" runat="server"   >
    <ContentTemplate  >
        
       
       
        <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b>LEASE ARCHIVE</b>
                </td>
            </tr>
        </table>
        <br />
        <asp:GridView ID="gv_tenant_lease_archives" runat="server" AllowSorting="true" 
            AutoGenerateColumns="false" 
            AlternatingRowStyle-BackColor="#F0F0F6" 
              BorderColor="#CDCDCD"  BorderWidth="1"
              HeaderStyle-BackColor="#F0F0F6"
            EmptyDataText="NONE" GridLines="Both" 
            Width="70%">
            <Columns>
                <asp:BoundField DataField="home_name" HeaderText="Property" />
                <asp:BoundField DataField="unit_door_no" HeaderText="Unit #" />
                <asp:BoundField DataField="tu_date_begin" DataFormatString="{0:MM-dd-yyyy}" 
                    HeaderText="Begin" />
                <asp:BoundField DataField="tu_date_end" DataFormatString="{0:MM-dd-yyyy}" 
                    HeaderText="End" />
                <asp:HyperLinkField DataNavigateUrlFields="tu_id" 
                    DataNavigateUrlFormatString="~/manager/lease/lease_archive_view.aspx?tu_id={0}" 
                    Text="View" />
            </Columns>
        </asp:GridView>
        
       
       
         </ContentTemplate  >
   </cc1:TabPanel>
    
    
  <cc1:TabPanel ID="tab4" HeaderText="Request history" runat="server"   >
    <ContentTemplate  >
  
  
      <table width="100%">
          <tr>
              <td bgcolor="aliceblue">
                  <b>REQUEST HISTORY</b>
              </td>
          </tr>
      </table>
      request 1 request 2 ... 2nnnnnn<br />
  
  
  
    </ContentTemplate>
  </cc1:TabPanel>
    
    
   <cc1:TabPanel ID="tab5" HeaderText="Incident history" runat="server"   >
    <ContentTemplate  >
  
  
  
       <table width="100%">
           <tr>
               <td bgcolor="aliceblue">
                   <b>INCIDENT HISTORY</b>
               </td>
           </tr>
       </table>
       <br />
  
  
  
       incidentvhg,hjhjg,hg<br />
  
  
  
  </ContentTemplate>
  </cc1:TabPanel>
  
  
  
 </cc1:TabContainer>



</asp:Content>

