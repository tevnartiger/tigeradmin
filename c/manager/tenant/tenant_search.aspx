﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="tenant_search.aspx.cs" Inherits="manager_tenant_tenant_search" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

      <asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
 <br />
<cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="2"   >
    
  <cc1:TabPanel ID="tab1" runat="server" HeaderText="ACTIVE TENANT(S)"  >
    <ContentTemplate  >
        
   
      
       <asp:Label ID="lbl_tenant_search" runat="server" style="font-weight: 700" 
           Text="<%$ Resources:Resource, lbl_tenant_search %>"></asp:Label>
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       <asp:TextBox ID="TextBox1" runat="server" ></asp:TextBox>
       &nbsp;&nbsp;&nbsp;&nbsp;
       <asp:Button ID="submit" runat="server" onclick="submit_Click" 
           Text="<%$ Resources:Resource, lbl_submit %>" />
       <br />
     
       &nbsp;<asp:HyperLink ID="link_a" runat="server" >[A]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_b" runat="server" >[B]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_c" runat="server" >[C]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_d" runat="server" >[D]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_e" runat="server" >[E]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_f" runat="server" >[F]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_g" runat="server" >[G]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_h" runat="server" >[H]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_i" runat="server" >[I]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_j" runat="server" >[J]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_k" runat="server" >[K]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_l" runat="server" >[L]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_m" runat="server" >[M]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_n" runat="server" >[N]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_o" runat="server" >[O]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_p" runat="server" >[P]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_q" runat="server" >[Q]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_r" runat="server" >[R]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_s" runat="server" >[S]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_t" runat="server" >[T]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_u" runat="server" >[U]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_v" runat="server" >[V]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_w" runat="server">[W]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_x" runat="server" >[X]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_y" runat="server" >[Y]</asp:HyperLink>
       &nbsp;
       <asp:HyperLink ID="link_z" runat="server" >[Z]</asp:HyperLink>
  <br />
       <br />
       
       <table width="50%" >
        <tr>
            <td  >
      
              <b>  <asp:Label ID="Label9" runat="server" 
                    Text="<%$ Resources:Resource, lbl_property%>" />
               </b>
                </td>
            <td valign="top"  >
              
                <asp:DropDownList ID="ddl_home_active_list" runat="server" 
                    AutoPostBack="true" DataTextField="home_name" DataValueField="home_id" 
                    OnSelectedIndexChanged="ddl_home_active_list_SelectedIndexChanged">
                </asp:DropDownList>
             </td>
       
        </tr>
     </table>
       
       
       <br /><br />
       <asp:GridView ID="gv_tenant_list" runat="server" AllowPaging="True" 
           AllowSorting="True"  AutoGenerateColumns="false"               
           DataKeyNames="tu_id" DataSourceID="SqlDataSource1" 
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" Width="80%">
           <Columns>
               <asp:BoundField DataField="name_lname"  
                   HeaderText="<%$ Resources:Resource, lbl_lname %>" ReadOnly="True" 
                   SortExpression="name_lname" >
                 
                   
               </asp:BoundField>
               <asp:BoundField DataField="name_fname"  
                   HeaderText="<%$ Resources:Resource, lbl_fname %>" ReadOnly="True" 
                   SortExpression="name_fname" >
                 
                   
               </asp:BoundField>
               <asp:BoundField DataField="home_name"  
                   HeaderText="<%$ Resources:Resource, lbl_property %>" 
                   SortExpression="home_name" >
                 
                   
               </asp:BoundField>
               <asp:BoundField DataField="unit_door_no"  
                   HeaderText="<%$ Resources:Resource, lbl_door_no %>" 
                   SortExpression="unit_door_no" >
                 
                   
               </asp:BoundField>
               <asp:BoundField DataField="tu_date_begin" DataFormatString="{0:M-dd-yyyy}"  
                   HeaderText="<%$ Resources:Resource, lbl_lease_date_begin %>" HtmlEncode="False" 
                   SortExpression="tu_date_begin" >
                 
                   
               </asp:BoundField>
               <asp:BoundField DataField="tu_date_end" DataFormatString="{0:M-dd-yyyy}"  
                   HeaderText="<%$ Resources:Resource, lbl_lease_date_end %>" HtmlEncode="False" 
                   SortExpression="tu_date_end" >
               </asp:BoundField>
               
               <asp:HyperLinkField DataNavigateUrlFields="tu_id,unit_id,home_id,tenant_id,name_id"   
                   DataNavigateUrlFormatString="~/manager/tenant/tenant_view.aspx?tu_id={0}&amp;unit_id={1}&amp;h_id={2}&amp;t_id={3}&amp;n_id={4}" 
                   HeaderText="<%$ Resources:Resource, lbl_info %>" 
                   Text="<%$ Resources:Resource, lbl_info %>" >
                 
                   
               </asp:HyperLinkField>
           </Columns>
           
       </asp:GridView>
       <asp:GridView ID="gv_tenant_search_list" runat="server" AllowPaging="True" 
           AllowSorting="True"  AutoGenerateColumns="false"       
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" 
           OnPageIndexChanging="gv_tenant_search_list_PageIndexChanging" PageSize="15" 
           Width="80%">
           <Columns>
               <asp:BoundField DataField="name_lname"  
                   HeaderText="<%$ Resources:Resource, lbl_lname %>" ReadOnly="True" >
                 
                   
               </asp:BoundField>
               <asp:BoundField DataField="name_fname"  
                   HeaderText="<%$ Resources:Resource, lbl_fname %>" ReadOnly="True" >
                 
                   
               </asp:BoundField>
               <asp:BoundField DataField="home_name"  
                   HeaderText="<%$ Resources:Resource, lbl_property %>" >
                 
                   
               </asp:BoundField>
               <asp:BoundField DataField="unit_door_no"  
                   HeaderText="<%$ Resources:Resource, lbl_door_no %>" >
                 
                   
               </asp:BoundField>
               <asp:BoundField DataField="tu_date_begin" DataFormatString="{0:M-dd-yyyy}"  
                   HeaderText="<%$ Resources:Resource, lbl_lease_date_begin %>" 
                   HtmlEncode="False" >
                 
                   
               </asp:BoundField>
               <asp:BoundField DataField="tu_date_end" DataFormatString="{0:M-dd-yyyy}"  
                   HeaderText="<%$ Resources:Resource, lbl_lease_date_end %>" 
                   HtmlEncode="False" >
                 
                   
               </asp:BoundField>
               <asp:HyperLinkField DataNavigateUrlFields="tu_id,unit_id,home_id,tenant_id,name_id"  
                   DataNavigateUrlFormatString="~/manager/tenant/tenant_view.aspx?tu_id={0}&amp;unit_id={1}&amp;h_id={2}&amp;t_id={3}&amp;n_id={4}" 
                   HeaderText="<%$ Resources:Resource, lbl_info %>" 
                   Text="<%$ Resources:Resource, lbl_info %>" >
                 
                   
               </asp:HyperLinkField>
           </Columns>
       </asp:GridView>
       
      
      
       <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
           ConnectionString="<%$ ConnectionStrings:sinfoca_tigerConnectionString2 %>" 
           SelectCommand="prTenantCurrentSearchList" SelectCommandType="StoredProcedure">
           <SelectParameters>
               <asp:SessionParameter Name="schema_id" SessionField="schema_id" Type="Int32" />
               <asp:QueryStringParameter Name="home_id" QueryStringField="h" Type="Int32" />
               <asp:QueryStringParameter Name="name" QueryStringField="l" Type="String" />
           </SelectParameters>
       </asp:SqlDataSource>
       
      
    
       
      
    
</ContentTemplate  >
   
  

</cc1:TabPanel>
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   <cc1:TabPanel ID="tab2" runat="server" HeaderText="PROSPECTIVE TENANT(S)"  >
    <ContentTemplate  >
   
   
    PROSPECTIVE TENANT LIST<br /><br />
    <div>
    <asp:GridView ID="gv_pt_list" AutoGenerateColumns="false" 
     AlternatingRowStyle-BackColor="#F0F0F6"  HeaderStyle-BackColor="#F0F0F6"
     Width="100%" BorderColor="#CDCDCD" BorderWidth="1"  runat="server" >
    <Columns>
    <asp:BoundField HeaderText="Name"  DataField="name" />
    <asp:BoundField DataField="income" HeaderText="Monthly Income"  DataFormatString="{0:0.00}" />
    <asp:BoundField DataField="city" HeaderText="Current city" />
    <asp:BoundField DataField="nb_bedrooms" HeaderText="Nb bedrooms" />
    <asp:BoundField DataField="trace_date_insert" HeaderText="Application date"
       DataFormatString="{0:MMMM dd , yyyy}"  
       HtmlEncode="false" />
    <asp:BoundField DataField="pt_date_begin" HeaderText="Desired begin date"
      DataFormatString="{0:MMMM dd , yyyy}"   HtmlEncode="false" />
    <asp:HyperLinkField  HeaderText="View details"  Text="view"
     DataNavigateUrlFields="name_id" 
     DataNavigateUrlFormatString="~/manager/tenant/tenant_prospect_view.aspx?pt_id={0}"/>
    </Columns> 
    <PagerSettings Mode="NumericFirstLast" />   
    </asp:GridView><br /><br />
     <asp:HyperLink ID="link_main" runat="server" NavigateUrl="~/home/home_main.aspx">go back to main</asp:HyperLink>           
    </div>
   
   
    
   
   
    
</ContentTemplate  >
   
  

</cc1:TabPanel>
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   <cc1:TabPanel ID="tab3" runat="server" HeaderText="INACTIVE TENANT(S)"  >
    
  
<HeaderTemplate>
INACTIVE TENANT(S)
</HeaderTemplate>
<ContentTemplate  >
        
   
      
       <asp:Label ID="Label1" runat="server" style="font-weight: 700" 
            Text="<%$ Resources:Resource, lbl_tenant_search %>"></asp:Label>


       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>


       &nbsp;&nbsp;&nbsp;&nbsp;
       <asp:Button ID="btn_submit2" runat="server" onclick="submit2_Click" 
            Text="<%$ Resources:Resource, lbl_submit %>" />


       <br />
     
       &nbsp;<asp:HyperLink ID="link_i_a" runat="server" >[A]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_b" runat="server" >[B]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_c" runat="server" >[C]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_d" runat="server" >[D]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_e" runat="server" >[E]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_f" runat="server" >[F]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_g" runat="server" >[G]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_h" runat="server" >[H]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_i" runat="server" >[I]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_j" runat="server" >[J]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_k" runat="server" >[K]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_l" runat="server" >[L]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_m" runat="server" >[M]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_n" runat="server" >[N]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_o" runat="server" >[O]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_p" runat="server" >[P]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_q" runat="server" >[Q]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_r" runat="server" >[R]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_s" runat="server" >[S]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_t" runat="server" >[T]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_u" runat="server" >[U]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_v" runat="server" >[V]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_w" runat="server">[W]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_x" runat="server" >[X]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_y" runat="server" >[Y]</asp:HyperLink>


       &nbsp;
       <asp:HyperLink ID="link_i_z" runat="server" >[Z]</asp:HyperLink>


  <br />
       <br />
       
       
       <br />
       <asp:GridView ID="gv_tenant_list2" runat="server" AllowPaging="True" 
            AllowSorting="True"
            AutoGenerateColumns="False" 
            DataKeyNames="name_id" 
            EmptyDataText="<%$ Resources:Resource, lbl_none %>" Width="100%"><Columns>
<asp:BoundField DataField="name_lname"  
                    HeaderText="<%$ Resources:Resource, lbl_lname %>" ReadOnly="True" 
                    SortExpression="name_lname" >
<HeaderStyle VerticalAlign="Top"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="name_fname"  
                    HeaderText="<%$ Resources:Resource, lbl_fname %>" ReadOnly="True" 
                    SortExpression="name_fname" >
<HeaderStyle VerticalAlign="Top"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>
<asp:HyperLinkField DataNavigateUrlFields="name_id"  
                    DataNavigateUrlFormatString="~/manager/tenant/tenant_inactive_view.aspx?n_id={0}" 
                    HeaderText="<%$ Resources:Resource, lbl_info %>" 
                    Text="<%$ Resources:Resource, lbl_info %>" >
<HeaderStyle VerticalAlign="Top"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:HyperLinkField>
</Columns>
</asp:GridView>


       <asp:GridView ID="gv_tenant_search_list2" runat="server" AllowPaging="True" 
            AllowSorting="True"  
            AutoGenerateColumns="False"  
            EmptyDataText="<%$ Resources:Resource, lbl_none %>"              
            OnPageIndexChanging="gv_tenant_search_list2_PageIndexChanging" PageSize="15" 
            Width="100%"><Columns>
<asp:BoundField DataField="name_lname"  
                    HeaderText="<%$ Resources:Resource, lbl_lname %>" ReadOnly="True" >
<HeaderStyle VerticalAlign="Top"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="name_fname"  
                    HeaderText="<%$ Resources:Resource, lbl_fname %>" ReadOnly="True" >
<HeaderStyle VerticalAlign="Top"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:BoundField>
<asp:HyperLinkField DataNavigateUrlFields="name_id"  
                    DataNavigateUrlFormatString="~/manager/tenant/tenant_inactive_view.aspx?n_id={0}" 
                    HeaderText="<%$ Resources:Resource, lbl_info %>" 
                    Text="<%$ Resources:Resource, lbl_info %>" >
<HeaderStyle VerticalAlign="Top"></HeaderStyle>

<ItemStyle VerticalAlign="Top"></ItemStyle>
</asp:HyperLinkField>
</Columns>
</asp:GridView>


       
      
      
       <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
            ConnectionString="<%$ ConnectionStrings:sinfoca_tigerConnectionString2 %>" 
            SelectCommand="prTenantInactiveSearchList" SelectCommandType="StoredProcedure"><SelectParameters>
<asp:SessionParameter Name="schema_id" SessionField="schema_id" Type="Int32" />
<asp:QueryStringParameter Name="home_id" QueryStringField="h" Type="Int32" />
<asp:QueryStringParameter Name="name" QueryStringField="l" Type="String" />
</SelectParameters>
</asp:SqlDataSource>

    
    

       
      
    
</ContentTemplate>
</cc1:TabPanel>
   
   
   </cc1:TabContainer>
   
  
</asp:Content>

