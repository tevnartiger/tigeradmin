﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : jun 3 , 2008
/// </summary>

public partial class manager_tenant_tenant_view : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AccountObjectAuthorization nameAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


        if (!RegEx.IsInteger(Request.QueryString["n_id"]) ||
            !RegEx.IsInteger(Request.QueryString["t_id"]) ||
            !RegEx.IsInteger(Request.QueryString["h_id"]) ||
            !RegEx.IsInteger(Request.QueryString["tu_id"]) ||
            !RegEx.IsInteger(Request.QueryString["unit_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        int tu_id = Convert.ToInt32(Request.QueryString["tu_id"]);
        int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
        int home_id = Convert.ToInt32(Request.QueryString["h_id"]);
        int tenant_id = Convert.ToInt32(Request.QueryString["t_id"]);
        int name_id = Convert.ToInt32(Request.QueryString["n_id"]);

        // check if we can access a tenant folder informations within the account
        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////

        //this one verify if name id has autorisation
        if (!nameAuthorization.TenantFolder(Convert.ToInt32(Session["schema_id"]),tu_id,unit_id,home_id,tenant_id,name_id))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
             


        tenantapplication_add_link.NavigateUrl = "tenant_prospect_add.aspx";
        tenantapplication_update_link.NavigateUrl = "tenant_update.aspx?n_id=" + Request.QueryString["n_id"];

        SetDefaultView();
        LeaseView();

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prTenantView", conn);
        SqlCommand cmd2 = new SqlCommand("prTenantRoomates", conn);
        SqlCommand cmd3 = new SqlCommand("prTenantLeaseArchives", conn);
        SqlCommand cmd4 = new SqlCommand("prTenantAmountOfLateRent", conn);
        SqlCommand cmd5 = new SqlCommand("prTenantAmountOnTimeRent", conn);
        SqlCommand cmd6 = new SqlCommand("prTenantPaymentArchive", conn);
        SqlCommand cmd7 = new SqlCommand("prTenantLatePaymentArchive", conn);
        SqlCommand cmd8 = new SqlCommand("prNameView", conn);
        SqlCommand cmd9 = new SqlCommand("prTenantIncidentArchives", conn);

        cmd.CommandType = CommandType.StoredProcedure;

        //Add the params
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["n_id"]);
        //  try
        {
            conn.Open();
            SqlDataReader dr = null;
            dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

            while (dr.Read() == true)
            {

                DateTime date_begin = new DateTime();

                if (dr["pt_date_begin"] != DBNull.Value)
                {
                    date_begin = Convert.ToDateTime(dr["pt_date_begin"]);
                    lbl_pt_date_begin.Text = date_begin.Month.ToString() + "/" + date_begin.Day.ToString() + "/" + date_begin.Year.ToString();
                }
               





                lbl_name_ssn.Text = dr["name_ssn"].ToString();
                lbl_tenantapplication_current_employer.Text = dr["tenantapplication_current_employer"].ToString();





                lbl_name_lname.Text = dr["name_lname"].ToString();
                lbl_name_fname.Text = dr["name_fname"].ToString();


                lbl_name_addr.Text = dr["name_addr"].ToString();
                lbl_name_addr_city.Text = dr["name_addr_city"].ToString();
                lbl_name_addr_pc.Text = dr["name_addr_pc"].ToString();
                lbl_name_addr_state.Text = dr["name_addr_state"].ToString();
                lbl_country_name.Text = dr["country_name"].ToString();
                lbl_name_tel.Text = dr["name_tel"].ToString();
                lbl_name_tel_work.Text = dr["name_tel_work"].ToString();
                lbl_name_tel_work_ext.Text = dr["name_tel_work_ext"].ToString();

                lbl_name_cell.Text = dr["name_cell"].ToString();
                lbl_name_fax.Text = dr["name_fax"].ToString();
                lbl_name_email.Text = dr["name_email"].ToString();
                lbl_name_com.Text = dr["name_com"].ToString();


                DateTime name_date_insert = new DateTime();
                if (dr["trace_date_insert"] != System.DBNull.Value)
                {
                    name_date_insert = Convert.ToDateTime(dr["trace_date_insert"]);
                    lbl_tenantapplication_date_insert.Text = name_date_insert.Month.ToString() + "/" + name_date_insert.Day.ToString() + "/" + name_date_insert.Year.ToString();
                }
                else
                    lbl_tenantapplication_date_insert.Text = "N/A";

                DateTime name_dob = new DateTime();
                if (dr["name_dob"] != System.DBNull.Value)
                {
                    name_dob = Convert.ToDateTime(dr["name_dob"]);
                    lbl_name_dob.Text = name_dob.Month.ToString() + "/" + name_dob.Day.ToString() + "/" + name_dob.Year.ToString();
                }



                if (dr["pt_bedroom_no"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_bedroom_no"]) == 0)
                        lbl_pt_bedroom_no.Text = "No Minimum";
                    else
                        lbl_pt_bedroom_no.Text = dr["pt_bedroom_no"].ToString();

                }

                if (dr["pt_bathroom_no"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_bathroom_no"]) == 0)
                        lbl_pt_bathroom_no.Text = "No Maximum";
                    else
                        lbl_pt_bathroom_no.Text = dr["pt_bathroom_no"].ToString();

                }


                if (dr["pt_rent_min"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_rent_min"]) == 0)
                        lbl_pt_rent_min.Text = "Any";
                    else
                        lbl_pt_rent_min.Text = dr["pt_rent_min"].ToString();

                }


                if (dr["pt_rent_max"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_rent_max"]) == 0)
                        lbl_pt_rent_max.Text = "Any";
                    else
                        lbl_pt_rent_max.Text = dr["pt_rent_max"].ToString();

                }


                if (dr["pt_min_sqft"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_min_sqft"]) == 0)
                        lbl_pt_min_sqft.Text = "Any";
                    else
                        lbl_pt_min_sqft.Text = dr["pt_min_sqft"].ToString();
                }



                if (dr["pt_electricity_inc"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_electricity_inc"]) == 1)
                        pt_electricity_inc.Checked = true;
                    else
                        pt_electricity_inc.Checked = false;
                }


                if (dr["pt_heat_inc"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_heat_inc"]) == 1)
                        pt_heat_inc.Checked = true;
                    else
                        pt_heat_inc.Checked = false;
                }



                if (dr["pt_water_inc"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_water_inc"]) == 1)
                        pt_water_inc.Checked = true;
                    else
                        pt_water_inc.Checked = false;
                }


                if (dr["pt_water_tax_inc"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_water_tax_inc"]) == 1)
                        pt_water_tax_inc.Checked = true;
                    else
                        pt_water_tax_inc.Checked = false;
                }



                if (dr["pt_parking_inc"] != System.DBNull.Value)
                {

                    if (Convert.ToInt32(dr["pt_parking_inc"]) == 1)
                        pt_parking_inc.Checked = true;
                    else
                        pt_parking_inc.Checked = false;

                }


                if (dr["pt_garage_inc"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_garage_inc"]) == 1)
                        pt_garage_inc.Checked = true;
                    else
                        pt_garage_inc.Checked = false;
                }


                if (dr["pt_furnished_inc"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_furnished_inc"]) == 1)
                        pt_furnished_inc.Checked = true;
                    else
                        pt_furnished_inc.Checked = false;
                }

                if (dr["pt_semi_furnished_inc"] != System.DBNull.Value)
                {
                    if (Convert.ToInt32(dr["pt_semi_furnished_inc"]) == 1)
                        pt_semi_furnished_inc.Checked = true;
                    else
                        pt_semi_furnished_inc.Checked = false;
                }


                lbl_tenantapplication_landlord_name.Text = dr["tenantapplication_landlord_name"].ToString();
                lbl_tenantapplication_current_employer.Text = dr["tenantapplication_current_employer"].ToString();
                lbl_tenantapplication_landlord_tel.Text = dr["tenantapplication_landlord_tel"].ToString();
                lbl_tenantapplication_monthly_income.Text = dr["tenantapplication_monthly_income"].ToString();
                lbl_tenantapplication_people_with_prospect.Text = dr["tenantapplication_people_with_prospect"].ToString();
                lbl_tenantapplication_reason_depart.Text = dr["tenantapplication_reason_depart"].ToString();
                // lbl_name_ssn.Text = dr["tenantapplication_ssn"].ToString();

                DateTime date_begin_ta = new DateTime();

                if (dr["tenantapplication_employer_since"] != System.DBNull.Value)
                {
                    date_begin_ta = Convert.ToDateTime(dr["tenantapplication_employer_since"]);
                    lbl_tenantapplication_employer_since.Text = date_begin_ta.Month.ToString() + "/" + date_begin_ta.Day.ToString() + "/" + date_begin_ta.Year.ToString();
                }

                pt_com.Text = dr["pt_com"].ToString();



            }



            cmd2.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd2.Parameters.Add("@tenant_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["t_id"]);
                cmd2.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["n_id"]);


                SqlDataAdapter da = new SqlDataAdapter(cmd2);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_roomates_list.DataSource = dt;
                gv_roomates_list.DataBind();
            }
            finally
            {
                //conn.Close();
            }




            cmd3.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd3.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["n_id"]);


                SqlDataAdapter da = new SqlDataAdapter(cmd3);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_tenant_lease_archives.DataSource = dt;
                gv_tenant_lease_archives.DataBind();
            }
            finally
            {
                //conn.Close();
            }



            cmd4.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd4.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd4.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["n_id"]);


                SqlDataAdapter da = new SqlDataAdapter(cmd4);
                DataTable dt = new DataTable();
                da.Fill(dt);


                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
            finally
            {
              //  conn.Close();
            }


            cmd5.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd5.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd5.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["n_id"]);


                SqlDataAdapter da = new SqlDataAdapter(cmd5);
                DataTable dt = new DataTable();
                da.Fill(dt);


                r_ontime_rent.DataSource = dt;
                r_ontime_rent.DataBind();
            }
            finally
            {
               // conn.Close();
            }


            cmd6.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd6.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd6.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["n_id"]);


                SqlDataAdapter da = new SqlDataAdapter(cmd6);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_tenant_payment_archive.DataSource = dt;
                gv_tenant_payment_archive.DataBind();
            }
            finally
            {
              //  conn.Close();
            }


            cmd7.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd7.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd7.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["n_id"]);


                SqlDataAdapter da = new SqlDataAdapter(cmd7);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_tenant_late_payment_archive.DataSource = dt;
                gv_tenant_late_payment_archive.DataBind();
            }
            finally
            {
              //  conn.Close();
            }

            cmd8.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd8.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd8.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["n_id"]);


                SqlDataAdapter da = new SqlDataAdapter(cmd8);
                DataTable dt = new DataTable();
                da.Fill(dt);


                rNameImage.DataSource = dt;
                rNameImage.DataBind();
            }
            finally
            {
                // conn.Close();
            }



            cmd9.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd9.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd9.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["n_id"]);


                SqlDataAdapter da = new SqlDataAdapter(cmd9);
                DataTable dt = new DataTable();
                da.Fill(dt);

                gv_incident_list.DataSource = dt;
                gv_incident_list.DataBind();

            }
            finally
            {
                conn.Close();
            }
        }
    }

    protected void gv_incident_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        tiger.Tenant v = new tiger.Tenant(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        gv_incident_list.PageIndex = e.NewPageIndex;
        gv_incident_list.DataSource = v.getTenantIncidentArchives(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["n_id"]));
        gv_incident_list.DataBind();
       



    }

/// <summary>
/// 
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
    protected void gv_tenant_payment_archive_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        tiger.Tenant tenant = new tiger.Tenant(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_tenant_payment_archive.PageIndex = e.NewPageIndex;
        gv_tenant_payment_archive.DataSource = tenant.getTenantPaymentArchive(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["n_id"]));
        gv_tenant_payment_archive.DataBind();

        TabContainer1.ActiveTabIndex = 1;
        
    }


    protected void LeaseView()
    {


        tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        int temp_tenant_id = unit.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["unit_id"])); // unit_id

        txt_current_tenant_name.InnerHtml = unit.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);


        /// get the rent amount
        ///////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prRentView", conn);
        SqlCommand cmd2 = new SqlCommand("prAccommodationView", conn);
        SqlCommand cmd3 = new SqlCommand("prTermsAndConditionsView", conn);
        SqlCommand cmd4 = new SqlCommand("prRentlogList", conn);
        SqlCommand cmd5 = new SqlCommand("prAccommodationList", conn);
        SqlCommand cmd6 = new SqlCommand("prTermsAndConditionsList", conn);
        SqlCommand cmd7 = new SqlCommand("prHomeUnitView", conn);

        cmd.CommandType = CommandType.StoredProcedure;


        DateTime the_date = new DateTime();
        the_date = DateTime.Now; // the date in the to drop downlist

        tiger.Date d = new tiger.Date();
        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


        //Add the params
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);
        cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;
        try
        {
            conn.Open();

            SqlDataReader dr = null;
            dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

            while (dr.Read() == true)
            {
                lbl_rent_amount.Text = Convert.ToString(Convert.ToInt32(dr["rl_rent_amount"]));
                //  ddl_rl_rent_paid_every.SelectedValue = dr["re_id"].ToString();

                DateTime la_date = new DateTime();
                la_date = Convert.ToDateTime(dr["rl_date_begin"]);

                lbl_current_rl_date_begin.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();

                if (dr["unit_type"].ToString() == "C" && Convert.ToInt32(dr["company_id"]) > 0)
                {
                    lbl_company.Text = dr["company_name"].ToString();
                    lbl_lease_type.Text = "Commercial";
                }
                else
                {
                    tr_company.Visible = false;
                    lbl_lease_type.Text = "Residential";
                }

            }
        }

        finally
        {

            //  conn.Close();
        }


        cmd2.CommandType = CommandType.StoredProcedure;
        //Add the params
        cmd2.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd2.Parameters.Add("tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);
        cmd2.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;


        try
        {
            // conn.Open();

            SqlDataReader dr2 = null;
            dr2 = cmd2.ExecuteReader(CommandBehavior.SingleRow);

            while (dr2.Read() == true)
            {

                // lbl_current_ta_date_begin.Text = String.Format("{0:d}", dr2["ta_date_begin"]);




                DateTime la_date = new DateTime();
                la_date = Convert.ToDateTime(dr2["ta_date_begin"]);

                lbl_current_ta_date_begin.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();






                if (Convert.ToInt32(dr2["ta_electricity_inc"]) == 1)
                    ta_electricity_inc.Checked = true;
                else
                    ta_electricity_inc.Checked = false;

                if (Convert.ToInt32(dr2["ta_heat_inc"]) == 1)
                    ta_heat_inc.Checked = true;
                else
                    ta_heat_inc.Checked = false;

                if (Convert.ToInt32(dr2["ta_water_inc"]) == 1)
                    ta_water_inc.Checked = true;
                else
                    ta_water_inc.Checked = false;

                if (Convert.ToInt32(dr2["ta_water_tax_inc"]) == 1)
                    ta_water_tax_inc.Checked = true;
                else
                    ta_water_tax_inc.Checked = false;

                if (Convert.ToInt32(dr2["ta_garage_inc"]) == 1)
                    ta_garage_inc.Checked = true;
                else
                    ta_garage_inc.Checked = false;

                if (Convert.ToInt32(dr2["ta_parking_inc"]) == 1)
                    ta_parking_inc.Checked = true;
                else
                    ta_parking_inc.Checked = false;

                if (Convert.ToInt32(dr2["ta_semi_furnished_inc"]) == 1)
                    ta_semi_furnished_inc.Checked = true;
                else
                    ta_semi_furnished_inc.Checked = false;

                if (Convert.ToInt32(dr2["ta_furnished_inc"]) == 1)
                    ta_furnished_inc.Checked = true;
                else
                    ta_furnished_inc.Checked = false;


                lbl_ta_com.InnerHtml = Convert.ToString(dr2["ta_com"]).Replace("\n","<br/>").Replace("\r","<br/>");
                if (lbl_ta_com.InnerHtml == "")
                    lbl_ta_com.InnerHtml = Resources.Resource.lbl_none;




            }



        }

        finally
        {

            //  conn.Close();
        }















        cmd3.CommandType = CommandType.StoredProcedure;

        //Add the params
        // cmd.Parameters.Add("@return_guarantor_name_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd3.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);
        cmd3.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;


        try
        {
            //conn.Open();

            SqlDataReader dr3 = null;
            dr3 = cmd3.ExecuteReader(CommandBehavior.SingleRow);

            int guarantor_name_id;
            /*
            tiger.Name n = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_guarantor_name_id.DataSource = n.getNameList(Convert.ToInt32(Session["schema_id"]), 0);
            ddl_guarantor_name_id.DataBind();
            ddl_guarantor_name_id.Items.Insert(0, "select a person");
            */
            while (dr3.Read() == true)
            {

                //  lbl_current_tt_date_begin.Text = String.Format("{0:d}", dr3["tt_date_begin"]);




                DateTime la_date = new DateTime();
                la_date = Convert.ToDateTime(dr3["tt_date_begin"]);

                lbl_current_tt_date_begin.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();





                if (dr3["tt_guarantor"].ToString() == "1")
                    panel_guarantor.Visible = true;
                else
                    panel_guarantor.Visible = false;

                tt_guarantor.SelectedValue = dr3["tt_guarantor"].ToString();

                guarantor_name.Text = dr3["tt_guarantor_name"].ToString();

                if (guarantor_name.Text == "")
                    guarantor_name.Text = Resources.Resource.lbl_none;

                if (Convert.ToInt32(dr3["tt_guarantor_name_id"]) > 0)
                {
                    // ddl_guarantor_name_id.SelectedValue = Convert.ToString(dr3["tt_guarantor_name_id"]);
                    /*  guarantor_name_fname.Enabled = false;
                      guarantor_name_lname.Enabled = false;
                      guarantor_name_addr.Enabled = false;
                      guarantor_name_addr_city.Enabled = false;
                      guarantor_name_addr_pc.Enabled = false;
                      guarantor_name_addr_state.Enabled = false;
                      ddl_guarantor_country_id.Enabled = false;
                      guarantor_name_tel.Enabled = false;
                      guarantor_name_tel_work.Enabled = false;
                      guarantor_name_tel_work_ext.Enabled = false;
                      guarantor_name_cell.Enabled = false;
                      guarantor_name_fax.Enabled = false;
                      guarantor_name_email.Enabled = false;
                      guarantor_name_com.Enabled = false;
                      txt.Visible = true; */
                }
                else
                {
                    /*  guarantor_name_fname.Enabled = true;
                      guarantor_name_lname.Enabled = true;
                      guarantor_name_addr.Enabled = true;
                      guarantor_name_addr_city.Enabled = true;
                      guarantor_name_addr_pc.Enabled = true;
                      guarantor_name_addr_state.Enabled = true;
                      ddl_guarantor_country_id.Enabled = true;
                      guarantor_name_tel.Enabled = true;
                      guarantor_name_tel_work.Enabled = true;
                      guarantor_name_tel_work_ext.Enabled = true;
                      guarantor_name_cell.Enabled = true;
                      guarantor_name_fax.Enabled = true;
                      guarantor_name_email.Enabled = true;
                      guarantor_name_com.Enabled = true;
                      txt.Visible = false; */

                }



                //  dr3["tt_form_of_payment"].ToString();

                switch (Convert.ToInt32(dr3["tt_form_of_payment"]))
                {
                    case 0: tt_form_of_payment.Text = Resources.Resource.lbl_not_specified;
                        break;
                    case 1: tt_form_of_payment.Text = Resources.Resource.lbl_personal_check;
                        break;
                    case 2: tt_form_of_payment.Text = Resources.Resource.lbl_cashier_check;
                        break;
                    case 3: tt_form_of_payment.Text = Resources.Resource.lbl_cash;
                        break;
                    case 4: tt_form_of_payment.Text = Resources.Resource.lbl_money_order;
                        break;
                    case 5: tt_form_of_payment.Text = Resources.Resource.lbl_credit_card;
                        break;
                    case 6: tt_form_of_payment.Text = Resources.Resource.lbl_other;
                        break;

                }


                decimal nsf, late_fee, security_deposit_amount;

                if (dr3["tt_nsf"].ToString() != "")
                    nsf = Convert.ToDecimal(dr3["tt_nsf"]);
                else
                    nsf = 0;

                if (dr3["tt_late_fee"].ToString() != "")
                    late_fee = Convert.ToDecimal(dr3["tt_late_fee"]);
                else
                    late_fee = 0;

                if (dr3["tt_security_deposit_amount"].ToString() != "")
                    security_deposit_amount = Convert.ToDecimal(dr3["tt_security_deposit_amount"]);
                else
                    security_deposit_amount = 0;


                tt_nsf.Text = String.Format("{0:0.00}", nsf);
                tt_late_fee.Text = String.Format("{0:0.00}", late_fee);


                tt_security_deposit.SelectedValue = dr3["tt_security_deposit"].ToString();

                tt_security_deposit_amount.Text = String.Format("{0:0.00}", security_deposit_amount);



                tt_pets.SelectedValue = dr3["tt_pets"].ToString();
                tt_maintenance.SelectedValue = dr3["tt_maintenance"].ToString();


                tt_specify_maintenance.Text = dr3["tt_specify_maintenance"].ToString();
                if (tt_specify_maintenance.Text == "")
                    tt_specify_maintenance.Text = Resources.Resource.lbl_none;

                tt_improvement.SelectedValue = dr3["tt_improvement"].ToString();

                tt_specify_improvement.Text = dr3["tt_specify_improvement"].ToString();
                if (tt_specify_improvement.Text == "")
                    tt_specify_improvement.Text = Resources.Resource.lbl_none;


                tt_notice_to_enter.SelectedValue = dr3["tt_notice_to_enter"].ToString();



                if (dr3["tt_specify_number_of_hours"].ToString() == "0")
                    tt_specify_number_of_hours.Text = "";
                else
                    tt_specify_number_of_hours.Text = dr3["tt_specify_number_of_hours"].ToString();




                switch (Convert.ToInt32(dr3["tt_tenant_content_ins"]))
                {

                    case 0: tt_tenant_content_ins.Text = Resources.Resource.lbl_not_specified;
                        break;
                    case 1: tt_tenant_content_ins.Text = Resources.Resource.lbl_landlord;
                        break;
                    case 2: tt_tenant_content_ins.Text = Resources.Resource.lbl_tenant;
                        break;

                }


                switch (Convert.ToInt32(dr3["tt_landlord_content_ins"]))
                {

                    case 0: tt_landlord_content_ins.Text = Resources.Resource.lbl_not_specified;
                        break;
                    case 1: tt_landlord_content_ins.Text = Resources.Resource.lbl_landlord;
                        break;
                    case 2: tt_landlord_content_ins.Text = Resources.Resource.lbl_tenant;
                        break;

                }


                switch (Convert.ToInt32(dr3["tt_injury_ins"]))
                {

                    case 0: tt_injury_ins.Text = Resources.Resource.lbl_not_specified;
                        break;
                    case 1: tt_injury_ins.Text = Resources.Resource.lbl_landlord;
                        break;
                    case 2: tt_injury_ins.Text = Resources.Resource.lbl_tenant;
                        break;

                }


                switch (Convert.ToInt32(dr3["tt_premises_ins"]))
                {

                    case 0: tt_premises_ins.Text = Resources.Resource.lbl_not_specified;
                        break;
                    case 1: tt_premises_ins.Text = Resources.Resource.lbl_landlord;
                        break;
                    case 2: tt_premises_ins.Text = Resources.Resource.lbl_tenant;
                        break;

                }



                tt_additional_terms.InnerHtml = dr3["tt_additional_terms"].ToString().Replace("\n", "<br>").Replace("\r", "<br>");
                if (tt_additional_terms.InnerHtml == "")
                    tt_additional_terms.InnerHtml = Resources.Resource.lbl_none;


            }



        }

        finally
        {

            //conn.Close();
        }



        // SqlConnection conn = new SqlConnection(str_conn);

        cmd4.CommandType = CommandType.StoredProcedure;

        try
        {
            // conn.Open();
            cmd4.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd4.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);


            SqlDataAdapter da = new SqlDataAdapter(cmd4);
            DataSet ds = new DataSet();
            da.Fill(ds);

            gv_rent_list.DataSource = ds;
            gv_rent_list.DataBind();
        }
        finally
        {
            // conn.Close();
        }



        cmd5.CommandType = CommandType.StoredProcedure;

        try
        {
            // conn.Open();
            cmd5.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd5.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);


            SqlDataAdapter da = new SqlDataAdapter(cmd5);
            DataSet ds = new DataSet();
            da.Fill(ds);

            gv_accommodation_list.DataSource = ds;
            gv_accommodation_list.DataBind();
        }
        finally
        {
            // conn.Close();
        }


        cmd6.CommandType = CommandType.StoredProcedure;

        try
        {
            // conn.Open();
            cmd6.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd6.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);


            SqlDataAdapter da = new SqlDataAdapter(cmd6);
            DataSet ds = new DataSet();
            da.Fill(ds);

            gv_terms_list.DataSource = ds;
            gv_terms_list.DataBind();
        }
        finally
        {
            // conn.Close();
        }


        cmd7.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd7.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd7.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);


            SqlDataAdapter da = new SqlDataAdapter(cmd7);
            DataSet ds = new DataSet();
            da.Fill(ds);

            r_HomeUnitView.DataSource = ds;
            r_HomeUnitView.DataBind();
        }
        finally
        {
            conn.Close();
        }


    }



    /// <summary>
    /// 
    /// </summary>
    private void SetDefaultView()
    {
        TabContainer1.ActiveTabIndex = 0;

        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    protected string GetLeaseType(string type)
    {
        string lease_type = "";

        if (type == "R")
            lease_type = "Residential";
        else
            lease_type = "Commercial";

        return lease_type;

    }

    
}
