using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : nov 18 , 2007
/// </summary>

public partial class tenant_tenant_prospect_list : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        tiger.Tenant pt = new tiger.Tenant(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_pt_list.DataSource = pt.getProspectiveTenantList(Convert.ToInt32(Session["schema_id"]));
        gv_pt_list.DataBind();
        
    }
}
