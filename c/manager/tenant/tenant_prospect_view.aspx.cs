using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.AccountObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : nov 20 , 2007
/// </summary>

public partial class tenant_tenant_prospect_view : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        tenantapplication_add_link.NavigateUrl = "tenant_prospect_add.aspx";
        tenantapplication_update_link.NavigateUrl = "tenant_prospect_update.aspx?pt_id=" + Request.QueryString["pt_id"];

        if (!RegEx.IsInteger(Request.QueryString["pt_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        AccountObjectAuthorization ptAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        // check prospective tenant authorization
        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!ptAuthorization.ProspectiveTenant(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["pt_id"])))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
       

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prProspectiveTenantView", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        //Add the params
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@pt_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["pt_id"]);
        //  try
        {
            conn.Open();
            SqlDataReader dr = null;
            dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

            while (dr.Read() == true)
            {

           DateTime date_begin = new DateTime();
           date_begin = Convert.ToDateTime(dr["pt_date_begin"]);
           lbl_pt_date_begin.Text = date_begin.Month.ToString() + "/" + date_begin.Day.ToString() + "/" + date_begin.Year.ToString();

           lbl_name_ssn.Text = dr["name_ssn"].ToString();
           lbl_tenantapplication_current_employer.Text = dr["tenantapplication_current_employer"].ToString();

           lbl_name_lname.Text = dr["name_lname"].ToString();
           lbl_name_fname.Text = dr["name_fname"].ToString();


           lbl_name_addr.Text = dr["name_addr"].ToString();
           lbl_name_addr_city.Text = dr["name_addr_city"].ToString();
           lbl_name_addr_pc.Text = dr["name_addr_pc"].ToString();
           lbl_name_addr_state.Text = dr["name_addr_state"].ToString();
           lbl_country_name.Text = dr["country_name"].ToString();
           lbl_name_tel.Text = dr["name_tel"].ToString();
           lbl_name_tel_work.Text = dr["name_tel_work"].ToString();
           lbl_name_tel_work_ext.Text = dr["name_tel_work_ext"].ToString();

           lbl_name_cell.Text = dr["name_cell"].ToString();
           lbl_name_fax.Text = dr["name_fax"].ToString();
           lbl_name_email.Text = dr["name_email"].ToString();
           lbl_name_com.Text = dr["name_com"].ToString();


           DateTime name_dob = new DateTime();
           if (dr["name_dob"] != System.DBNull.Value)
           {
               name_dob = Convert.ToDateTime(dr["name_dob"]);
               lbl_name_dob.Text = name_dob.Month.ToString() + "/" + name_dob.Day.ToString() + "/" + name_dob.Year.ToString();
           }


           if (Convert.ToInt32(dr["pt_bedroom_no"]) == 0)
               lbl_pt_bedroom_no.Text = "No Minimum";
           else
           lbl_pt_bedroom_no.Text = dr["pt_bedroom_no"].ToString();

           if (Convert.ToInt32(dr["pt_bathroom_no"]) == 0)
              lbl_pt_bathroom_no.Text = "No Maximum";
           else
           lbl_pt_bathroom_no.Text = dr["pt_bathroom_no"].ToString();

           if (Convert.ToInt32(dr["pt_rent_min"]) == 0)
               lbl_pt_rent_min.Text = "Any";
           else
               lbl_pt_rent_min.Text = dr["pt_rent_min"].ToString();

           if (Convert.ToInt32(dr["pt_rent_max"]) == 0)
               lbl_pt_rent_max.Text = "Any";
           else
           lbl_pt_rent_max.Text = dr["pt_rent_max"].ToString();

           if (Convert.ToInt32(dr["pt_min_sqft"]) == 0)
              lbl_pt_min_sqft.Text = "Any";
           else
           lbl_pt_min_sqft.Text = dr["pt_min_sqft"].ToString();


           if (Convert.ToInt32(dr["pt_electricity_inc"]) == 1)
               pt_electricity_inc.Checked = true;
           else
               pt_electricity_inc.Checked = false;




           if (Convert.ToInt32(dr["pt_heat_inc"]) == 1)
               pt_heat_inc.Checked = true;
           else
               pt_heat_inc.Checked = false;



                

           if (Convert.ToInt32(dr["pt_water_inc"]) == 1)
               pt_water_inc.Checked = true;
           else
               pt_water_inc.Checked = false;

           if (Convert.ToInt32(dr["pt_water_tax_inc"]) == 1)
               pt_water_tax_inc.Checked = true;
           else
               pt_water_tax_inc.Checked = false;


           if (Convert.ToInt32(dr["pt_parking_inc"]) == 1)
               pt_parking_inc.Checked = true;
           else
               pt_parking_inc.Checked = false;





           if (Convert.ToInt32(dr["pt_garage_inc"]) == 1)
               pt_garage_inc.Checked = true;
           else
               pt_garage_inc.Checked = false;




           if (Convert.ToInt32(dr["pt_furnished_inc"])  == 1)
               pt_furnished_inc.Checked = true;
           else
               pt_furnished_inc.Checked = false;



           if (Convert.ToInt32(dr["pt_semi_furnished_inc"]) == 1)
               pt_semi_furnished_inc.Checked = true;
           else
               pt_semi_furnished_inc.Checked = false;



           lbl_tenantapplication_landlord_name.Text = dr["tenantapplication_landlord_name"].ToString();
           lbl_tenantapplication_current_employer.Text = dr["tenantapplication_current_employer"].ToString();
           lbl_tenantapplication_landlord_tel.Text = dr["tenantapplication_landlord_tel"].ToString();
           lbl_tenantapplication_monthly_income.Text = dr["tenantapplication_monthly_income"].ToString();
           lbl_tenantapplication_people_with_prospect.Text = dr["tenantapplication_people_with_prospect"].ToString();
           lbl_tenantapplication_reason_depart.Text = dr["tenantapplication_reason_depart"].ToString();
          // lbl_name_ssn.Text = dr["tenantapplication_ssn"].ToString();

           DateTime date_begin_ta = new DateTime();
           date_begin_ta = Convert.ToDateTime(dr["tenantapplication_employer_since"]);
           lbl_tenantapplication_employer_since.Text = date_begin_ta.Month.ToString() + "/" + date_begin_ta.Day.ToString() + "/" + date_begin_ta.Year.ToString();

            pt_com.Text = dr["pt_com"].ToString();


            
            }
            conn.Close();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_delete_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        
        tiger.Name name = new tiger.Name(strconn);

        int result = name.ProspectDelete(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["pt_id"]));

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_insert_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        tiger.Name name = new tiger.Name(strconn);

        int result = name.ProspectAccept(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["pt_id"]), Convert.ToInt32(Session["schema_id"]), Request.UserHostAddress.ToString());

        Response.Redirect("~/manager/tenant/tenant_search.aspx?h=0&l=");

    }
}
