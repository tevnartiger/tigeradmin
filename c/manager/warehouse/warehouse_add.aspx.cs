﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
/// <summary>
/// Done by : Stanley Jocelyn
/// date    : may 28, 2008
/// </summary>
/// 
public partial class manager_warehouse_warehouse_add : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        reg_warehouse_city.ValidationExpression = RegEx.getText();
        reg_warehouse_addr_street.ValidationExpression = RegEx.getText();
        reg_warehouse_contact_email.ValidationExpression = RegEx.getEmail();
        reg_warehouse_contact_fname.ValidationExpression = RegEx.getText();
        reg_warehouse_contact_lname.ValidationExpression = RegEx.getText();
        reg_warehouse_contact_tel.ValidationExpression = RegEx.getText();
        reg_warehouse_name.ValidationExpression = RegEx.getText();
        reg_warehouse_pc.ValidationExpression = RegEx.getText();
        reg_warehouse_tel.ValidationExpression = RegEx.getText();
        reg_warehouse_website.ValidationExpression = RegEx.getText();
        reg_warehouse_prov.ValidationExpression = RegEx.getText();
        reg_warehouse_contact_fax.ValidationExpression = RegEx.getText();


        if (!(Page.IsPostBack))
        {
            tiger.Country ware = new tiger.Country(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_warehouse_country_list.DataSource = ware.getCountryList();
            ddl_warehouse_country_list.DataBind();
        }

    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {

        Page.Validate();
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prWarehouseAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@new_warehouse_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@warehouse_name", SqlDbType.NVarChar, 50).Value = RegEx.getText(warehouse_name.Text);
            cmd.Parameters.Add("@warehouse_website", SqlDbType.NVarChar, 200).Value = RegEx.getText(warehouse_website.Text);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.NVarChar, 15).Value = Request.UserHostAddress.ToString();

            cmd.Parameters.Add("@warehouse_addr", SqlDbType.NVarChar, 50).Value = RegEx.getText(warehouse_addr.Text);
            cmd.Parameters.Add("@warehouse_city", SqlDbType.NVarChar, 50).Value = RegEx.getText(warehouse_city.Text);
            cmd.Parameters.Add("@warehouse_prov", SqlDbType.NVarChar, 50).Value = RegEx.getText(warehouse_prov.Text);
            cmd.Parameters.Add("@warehouse_pc", SqlDbType.NVarChar, 50).Value = RegEx.getText(warehouse_pc.Text);
            cmd.Parameters.Add("@warehouse_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(warehouse_tel.Text);
            cmd.Parameters.Add("@warehouse_contact_fname", SqlDbType.NVarChar, 50).Value = RegEx.getText(warehouse_contact_fname.Text);
            cmd.Parameters.Add("@warehouse_contact_lname", SqlDbType.NVarChar, 50).Value = RegEx.getText(warehouse_contact_lname.Text);
            cmd.Parameters.Add("@warehouse_contact_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(warehouse_contact_tel.Text);
            cmd.Parameters.Add("@warehouse_contact_email", SqlDbType.NVarChar, 50).Value = RegEx.getText(warehouse_contact_email.Text);
            cmd.Parameters.Add("@warehouse_contact_fax", SqlDbType.NVarChar, 50).Value = RegEx.getText(warehouse_contact_fax.Text);
            cmd.Parameters.Add("@country_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_warehouse_country_list.SelectedValue);
            cmd.Parameters.Add("@warehouse_com", SqlDbType.Text).Value = RegEx.getText(warehouse_com.Text);




            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            if (Convert.ToInt32(cmd.Parameters["@new_warehouse_id"].Value) > 0)
                result.InnerHtml = " add successful";


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }
    }
}
