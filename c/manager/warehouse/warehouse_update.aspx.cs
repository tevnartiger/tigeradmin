﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : may 28, 2008
/// </summary>
public partial class manager_warehouse_warehouse_update : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RegEx.IsInteger(Request.QueryString["w_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        reg_warehouse_city.ValidationExpression = RegEx.getText();
        reg_warehouse_addr.ValidationExpression = RegEx.getText();
        reg_warehouse_contact_email.ValidationExpression = RegEx.getEmail();
        reg_warehouse_contact_fname.ValidationExpression = RegEx.getText();
        reg_warehouse_contact_lname.ValidationExpression = RegEx.getText();
        reg_warehouse_contact_tel.ValidationExpression = RegEx.getText();
        reg_warehouse_name.ValidationExpression = RegEx.getText();
        reg_warehouse_pc.ValidationExpression = RegEx.getText();
        reg_warehouse_tel.ValidationExpression = RegEx.getText();
        reg_warehouse_website.ValidationExpression = RegEx.getText();
        reg_warehouse_prov.ValidationExpression = RegEx.getText();
        reg_warehouse_contact_fax.ValidationExpression = RegEx.getText();

       
        //---------------------------------------------------------------------------------------------------
        AccountObjectAuthorization warehouseAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!warehouseAuthorization.Warehouse(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["w_id"])))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
        //---------------------------------------------------------------------------------------------------
      
        if (!Page.IsPostBack)
        {
            // The country list
            tiger.Country warehouse = new tiger.Country(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_warehouse_country_list.DataSource = warehouse.getCountryList();
            ddl_warehouse_country_list.DataBind();

            //  Information about a WAREHOUSE ( warehouse_id )
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prWarehouseView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@warehouse_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["w_id"]);
            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                    warehouse_name.Text = dr["warehouse_name"].ToString();
                    warehouse_website.Text = dr["warehouse_website"].ToString();
                    warehouse_addr.Text = dr["warehouse_addr"].ToString();

                    //lbl.Text = dr["country_id"].ToString();
                    if (dr["country_id"].ToString() == "")
                    { }
                    else
                        ddl_warehouse_country_list.SelectedValue = dr["country_id"].ToString();

                    warehouse_city.Text = dr["warehouse_city"].ToString();
                    warehouse_prov.Text = dr["warehouse_prov"].ToString();
                    warehouse_pc.Text = dr["warehouse_pc"].ToString();
                    warehouse_tel.Text = dr["warehouse_tel"].ToString();
                    warehouse_contact_fname.Text = dr["warehouse_contact_fname"].ToString();
                    warehouse_contact_lname.Text = dr["warehouse_contact_lname"].ToString();
                    warehouse_contact_fax.Text = dr["warehouse_fax"].ToString();
                    warehouse_contact_tel.Text = dr["warehouse_tel"].ToString();
                    warehouse_contact_email.Text = dr["warehouse_email"].ToString();
                    warehouse_com.Text = dr["warehouse_com"].ToString();

                }

            }
            finally
            {
                conn.Close();
            }
        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        Page.Validate();
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prWarehouseUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

     //  try
        {
            conn.Open();
            //Add the params
           // cmd.Parameters.Add("@update_warehouse_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@warehouse_name", SqlDbType.VarChar, 100).Value = RegEx.getText(warehouse_name.Text);
            cmd.Parameters.Add("@warehouse_website", SqlDbType.VarChar, 200).Value = RegEx.getText(warehouse_website.Text);
            cmd.Parameters.Add("@warehouse_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["w_id"]);
            cmd.Parameters.Add("@warehouse_addr", SqlDbType.VarChar, 200).Value = RegEx.getText(warehouse_addr.Text);
            cmd.Parameters.Add("@warehouse_city", SqlDbType.VarChar, 200).Value = RegEx.getText(warehouse_city.Text);
            cmd.Parameters.Add("@warehouse_prov", SqlDbType.VarChar, 200).Value = RegEx.getText(warehouse_prov.Text);
            cmd.Parameters.Add("@warehouse_pc", SqlDbType.VarChar, 50).Value = RegEx.getText(warehouse_pc.Text);
            cmd.Parameters.Add("@warehouse_tel", SqlDbType.VarChar, 50).Value = RegEx.getText(warehouse_tel.Text);
            cmd.Parameters.Add("@warehouse_contact_fname", SqlDbType.VarChar, 100).Value = RegEx.getText(warehouse_contact_fname.Text);
            cmd.Parameters.Add("@warehouse_contact_lname", SqlDbType.VarChar, 100).Value = RegEx.getText(warehouse_contact_lname.Text);
            cmd.Parameters.Add("@warehouse_email", SqlDbType.VarChar, 100).Value = RegEx.getText(warehouse_contact_email.Text);
            cmd.Parameters.Add("@warehouse_fax", SqlDbType.VarChar, 50).Value = RegEx.getText(warehouse_contact_email.Text);
            cmd.Parameters.Add("@country_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_warehouse_country_list.SelectedValue);
            cmd.Parameters.Add("@warehouse_com", SqlDbType.Text).Value = RegEx.getText(warehouse_com.Text);




 


            //execute the insert
            cmd.ExecuteReader();
            //  

          //  if (Convert.ToInt32(cmd.Parameters["@update_warehouse_id"].Value) > 0)
            //    result.InnerHtml = " add successful";


        }
        //catch (Exception error)
        {
          //  tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
           // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }
    }

}
