﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="warehouse_list.aspx.cs" Inherits="manager_warehouse_warehouse_list" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <b>WAREHOUSE LIST
    <br />
    <br />
    
    
   
    </b>
    <asp:GridView ID="GridView1" runat="server"  
         EmptyDataText="No Data" GridLines="Both"  PageSize="10" AllowPaging="true"
        AlternatingRowStyle-BackColor="#F0F0F6"  HeaderStyle-BackColor="#F0F0F6"
         Width="100%" BorderColor="#CDCDCD" BorderWidth="1" 
         AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="warehouse_id" 
        DataSourceID="SqlDataSource1">
        <Columns>
            
           
            <asp:BoundField DataField="warehouse_name" HeaderText="Name"   />
            
            <asp:BoundField DataField="warehouse_addr" HeaderText="Address"  />
            <asp:BoundField DataField="warehouse_city" HeaderText="City"  />
         
            <asp:BoundField DataField="warehouse_tel" HeaderText="Tel." />
            
            <asp:BoundField DataField="warehouse_contact_name"   HeaderText="Contact name"   />
            
            <asp:BoundField DataField="warehouse_email" HeaderText="Email" />
            
            <asp:BoundField DataField="warehouse_website" HeaderText="Website"  />
                
            <asp:HyperLinkField Text="<%$ Resources:Resource, lbl_view %>" 
              DataNavigateUrlFields="warehouse_id" 
               DataNavigateUrlFormatString="~/manager/warehouse/warehouse_view.aspx?w_id={0}" 
             HeaderText="<%$ Resources:Resource, lbl_view %>" />
            
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:sinfoca_tigerConnectionString2 %>" 
        SelectCommand="prWarehouseList" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter Name="schema_id" SessionField="schema_id" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>
    <br />
    <br />
 


</asp:Content>

