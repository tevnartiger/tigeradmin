﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;
using SubSonic;
using System.IO;
/// <summary>
/// Done by : Stanley Jocelyn
/// date    : may 28 , 2008
/// UPDATE ( MODIDFY ) APPLIANCE
/// </summary>
public partial class manager_appliance_appliance_update : BasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RegEx.IsInteger(Request.QueryString["appliance_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
       

        // setting the validation expression

        reg_appliance_desc.ValidationExpression = RegEx.getText();
        reg_appliance_invoice_no.ValidationExpression = RegEx.getAlphaNumeric();
        reg_appliance_name.ValidationExpression = RegEx.getText();
        reg_appliance_purchase_cost.ValidationExpression = RegEx.getMoney();
        reg_appliance_serial_no.ValidationExpression = RegEx.getAlphaNumeric();


        lbl_select_warehouse.Visible = false;

       string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


       ////////
       //////// SECURITY CHECK BEGIN //////////////////
       ////////
       AccountObjectAuthorization applianceAuthorization = new AccountObjectAuthorization(strconn);

       if (applianceAuthorization.Appliance(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["appliance_id"])))
       {
       }
       else
       {
           Session.Abandon();
           Response.Redirect("~/login.aspx");
       }
       ////////
       //////// SECURITY CHECK END //////////////////
       ////////

           if (!Page.IsPostBack)
           {

               // we get the list of warehouse available

               //get the list of warehouse 
               tiger.Warehouse warehouse = new tiger.Warehouse(strconn);
               ddl_warehouse_list.DataSource = warehouse.getWarehouseList(Convert.ToInt32(Session["schema_id"]));
               ddl_warehouse_list.DataBind();
               ddl_warehouse_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_select, "-1"));
               ddl_warehouse_list.SelectedIndex = 0;

               //by default we wanna put a new appliance in a property
               ddl_warehouse_list.Enabled = false;

               //by default we put new appliance in property therefore the property checkbox is selected
               chk_home.Checked = true;


               // List of Houses and Units in each house
               tiger.Home h = new tiger.Home(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
               int home_count = h.getHomeCount(Convert.ToInt32(Session["schema_id"]));

               //  tiger.Home h = new tiger.Home(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
               //  ddl_appliance_home_id.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
               //  ddl_appliance_home_id.DataBind();

               tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

               // BY DEFAULT, GET THE FIRST HOME IN A SCHEMA
               int home_id = h.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));

               if (home_count > 0)
               {


                   ddl_appliance_home_id.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                   ddl_appliance_home_id.SelectedValue = Convert.ToString(home_id);
                   ddl_appliance_home_id.DataBind();
                   ddl_appliance_home_id.Items.Add(Resources.Resource.lbl_select);
                   //ddl_appliance_categ.Items.

                   //Manage request unit_id
                   //if unit_id doesn't exist
                   //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);

                   int unit_id = 0;
                   int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);
                   if (unit_count > 0)
                   {
                       // BY DEFAULT WE POPULATE THE UNIT DROPDOWN WITH THE UNITS IN THE FIRST HOME

                       unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), home_id);
                       int temp_unit_id = unit_count;
                       //show list of unit_id    
                       ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
                       //ddl_unit_id.SelectedValue = Convert.ToString(unit_id);
                       ddl_unit_id.DataBind();
                       ddl_unit_id.Items.Add(new ListItem(Resources.Resource.lbl_storage_unit, "-1"));

                   }
                   else
                   {
                       //     txt_link.InnerHtml = "<a href='unit_add.aspx?home_id='>Add Unit</a>";
                   }

               }


               //  SUPPLIER DROPDOWNLIST

               // The list of suppliers
               tiger.Supplier i = new tiger.Supplier(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
               ddl_supplier_list.DataSource = i.getSupplierList(Convert.ToInt32(Session["schema_id"]));
               ddl_supplier_list.DataBind();
               ddl_supplier_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_select, "0"));
           

               // ---------------------------------------------------------------


               // MARS ACTIVE MULTIPLE RESULT SET
               // VIEW  AN APPLIANCE
               SqlConnection conn1 = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
               SqlCommand cmd = new SqlCommand("prApplianceView2", conn1);
               SqlCommand cmd2 = new SqlCommand("prApplianceShortCategList", conn1);
               SqlCommand cmd3 = new SqlCommand("prAppliancePreviousStorageList", conn1);


               cmd.CommandType = CommandType.StoredProcedure;
               cmd2.CommandType = CommandType.StoredProcedure;
               cmd3.CommandType = CommandType.StoredProcedure;

               //Add the params
               cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
               cmd.Parameters.Add("appliance_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["appliance_id"]);
               try
               {
                   conn1.Open();

                   SqlDataAdapter da = new SqlDataAdapter(cmd);
                   DataSet ds = new DataSet();
                   da.Fill(ds);


                   rApplianceInvoiceImage.DataSource = ds;
                   rApplianceInvoiceImage.DataBind();

                   rApplianceImage.DataSource = ds;
                   rApplianceImage.DataBind();

                   SqlDataReader dr = null;
                   dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                   while (dr.Read() == true)
                   {
                       ddl_supplier_list.SelectedValue = dr["company_id"].ToString();
                       ddl_appliance_categ.SelectedValue = dr["ac_id"].ToString();
                       appliance_name.Text = dr["appliance_name"].ToString();
                       appliance_invoice_no.Text = dr["appliance_invoice_no"].ToString();

                       double purchase_cost = Convert.ToDouble(dr["appliance_purchase_cost"]);
                       appliance_purchase_cost.Text = String.Format("{0:0.00}", purchase_cost);



                       appliance_desc.Text = dr["appliance_desc"].ToString();
                       appliance_serial_no.Text = dr["appliance_serial_no"].ToString();
                       ua_id.Value = dr["ua_id"].ToString();
                       DateTime date_purchase = new DateTime();
                       date_purchase = Convert.ToDateTime(dr["appliance_date_purchase"]);
                       ddl_appliance_date_purchase_y.SelectedValue = date_purchase.Year.ToString();
                       ddl_appliance_date_purchase_m.SelectedValue = date_purchase.Month.ToString();
                       ddl_appliance_date_purchase_d.SelectedValue = date_purchase.Day.ToString();



                       if (dr["ua_dateadd"] != System.DBNull.Value)
                       {
                           DateTime dateadd = new DateTime();
                           dateadd = Convert.ToDateTime(dr["ua_dateadd"]);

                           ddl_ua_dateadd_y.Text = dateadd.Year.ToString();
                           ddl_ua_dateadd_m.Text = dateadd.Month.ToString();
                           ddl_ua_dateadd_d.Text = dateadd.Day.ToString();
                       }



                       if (dr["home_name"] == DBNull.Value)
                       {
                           chk_home.Checked = false;
                           ddl_appliance_home_id.Enabled = false;
                           ddl_unit_id.Enabled = false;

                           ddl_warehouse_list.Enabled = true;
                           chk_warehouse.Checked = true;
                           ddl_warehouse_list.SelectedValue = dr["warehouse_id"].ToString();
                       }
                       else
                           ddl_appliance_home_id.SelectedValue = dr["home_id"].ToString();


                       // we check if the value of unit is null , because previously
                       // we have putted the value "null" instead of -1 do designate that the appliance
                       // was in the storage unit
                       if (dr["unit_id"] == DBNull.Value)
                           ddl_unit_id.SelectedValue = "-1";
                       else
                       {
                           // WE GET THE LIST OF UNIT FROM THE PROPERTY RETREIVE FROM THE DB 
                           // i.e : dr["home_id"]

                           ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_appliance_home_id.SelectedValue));
                           //ddl_unit_id.SelectedValue = Convert.ToString(unit_id);
                           ddl_unit_id.DataBind();
                           ddl_unit_id.Items.Add(new ListItem(Resources.Resource.lbl_storage_unit, "-1"));
                           ddl_unit_id.SelectedValue = dr["unit_id"].ToString();
                       }



                       ddl_appliance_guarantee_year.Text = dr["appliance_guarantee_year"].ToString();

                   }

               }
               finally
               {
                   //conn1.Close();
               }




               try
               {
                   //conn1.Open();
                   //Add the params
                   // cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);

                   SqlDataAdapter da = new SqlDataAdapter(cmd2);
                   DataSet ds = new DataSet();
                   da.Fill(ds);
                   ddl_appliance_categ.DataSource = ds;

                   string lang = "";

                   if (Session["_lastCulture"].ToString() == "fr-FR")
                       lang = "ac_name_fr";

                   if (Session["_lastCulture"].ToString() == "en-US")
                       lang = "ac_name_en";

                   ddl_appliance_categ.DataTextField = lang;

                   ddl_appliance_categ.DataBind();
                   //ddl_appliance_categ.Items.Insert(0, " -- select a category --");
               }

               finally
               {

               }


               try
               {

                   cmd3.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                   cmd3.Parameters.Add("appliance_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["appliance_id"]);
                   SqlDataAdapter da = new SqlDataAdapter(cmd3);
                   DataTable dt = new DataTable();
                   da.Fill(dt);

                   gv_appliance_previous_storage.DataSource = dt;
                   gv_appliance_previous_storage.DataBind();
               }

               finally
               {
                   conn1.Close();
               }
           }
      


    }
    protected void ddl_appliance_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        //get home_id
        //    temp_home_id.Value = ddl_appliance_home_id.SelectedValue;
        //get first unit_id of home
        tiger.Unit u = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        int unit_id;
        if (ddl_appliance_home_id.SelectedItem.Text == " ** put in storage ** ")
        {
            unit_id = 0;
        }
        else
            unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_appliance_home_id.SelectedValue));
        //change list of unit id

        if (unit_id > 0)
        {
            ddl_unit_id.Visible = true;
            //first dispose of old ddl_unit
            ddl_unit_id.Dispose();
            ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_appliance_home_id.SelectedValue));
            ddl_unit_id.DataBind();
            ddl_unit_id.Items.Add(new ListItem(Resources.Resource.lbl_storage_unit, "-1"));

        }
        else
        {
            ddl_unit_id.Visible = false;
            //  txt_link.InnerHtml = "<a href='unit_add.aspx?home_id='>Add Unit</a>";

        }


    }


    protected void btn_update_Click(object sender, EventArgs e)
    {
      if (chk_warehouse.Checked == true || chk_home.Checked == true)
       {
        string invoice_photo_name = "";
        string appliance_photo_name = "";

        bool continu = true;

        // if the warehouse checkbox is selected , the user must choose a warehouse from
        // the warehouse dropdownlist
        if (chk_warehouse.Checked == true)
        {
            if (ddl_warehouse_list.SelectedIndex == 0)
            {
                continu = false;
                lbl_select_warehouse.Visible = true;
            }
        }

        Page.Validate();
        if (Page.IsValid && continu == true)
        {
            lbl_select_warehouse.Visible = false;

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prApplianceUpdate", conn);
        SqlCommand cmd2 = new SqlCommand("prAppliancePreviousStorageList", conn);

        cmd.CommandType = CommandType.StoredProcedure;

     //  try
       {
             DateTime date_purchase = new DateTime();
             DateTime date_add = new DateTime();
             tiger.Date df = new tiger.Date();

        conn.Open();
        //Add the params
        cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
        cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
        cmd.Parameters.Add("@appliance_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["appliance_id"]);

      
        cmd.Parameters.Add("@ac_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_appliance_categ.SelectedValue);


        if (chk_home.Checked == true)
        {
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_appliance_home_id.SelectedValue);
            cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_unit_id.SelectedValue);
            cmd.Parameters.Add("@warehouse_id", SqlDbType.Int).Value = DBNull.Value;
        }


        if (chk_warehouse.Checked == true)
        {
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = DBNull.Value;
            cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = DBNull.Value;
            cmd.Parameters.Add("@warehouse_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_warehouse_list.SelectedValue);
        }



        date_add = Convert.ToDateTime(df.DateCulture(ddl_ua_dateadd_m.SelectedValue, ddl_ua_dateadd_d.SelectedValue, ddl_ua_dateadd_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        cmd.Parameters.Add("@ua_dateadd", SqlDbType.SmallDateTime).Value = date_add;   
        cmd.Parameters.Add("@ua_id", SqlDbType.Int).Value = Convert.ToInt32(ua_id.Value);


        string filename ;
           
        filename = Path.GetFileName(appliance_photo.PostedFile.FileName);

         
        int filesize = 0;
        if (appliance_photo.HasFile)
        {

            Stream stream = appliance_photo.PostedFile.InputStream;
            filesize = appliance_photo.PostedFile.ContentLength;
            byte[] filedata = new byte[filesize];
            stream.Read(filedata, 0, filesize);

            int success = -1;
            
            tiger.Media upload = new tiger.Media(strconn);

            success = upload.ApplianceImageUpdate(filedata, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["appliance_id"]),
                                     Convert.ToInt32(Session["name_id"]), Request.UserHostAddress.ToString());
            Label21.Text = success.ToString();
        }



        if (appliance_invoice_photo.HasFile)
        {
            invoice_photo_name = appliance_invoice_photo.PostedFile.FileName;
            int last_index_a = invoice_photo_name.LastIndexOf('\\');
            invoice_photo_name = invoice_photo_name.Substring(last_index_a + 1);

            Stream stream = appliance_invoice_photo.PostedFile.InputStream;
            filesize = appliance_invoice_photo.PostedFile.ContentLength;
            byte[] filedata = new byte[filesize];
            stream.Read(filedata, 0, filesize);

            int success = 0;

            tiger.Media upload = new tiger.Media(strconn);

            success = upload.ApplianceInvoiceImageUpdate(filedata, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["appliance_id"]),
                                     Convert.ToInt32(Session["name_id"]), Request.UserHostAddress.ToString());
        }


        cmd.Parameters.Add("@company_id", SqlDbType.Int).Value = ddl_supplier_list.SelectedValue;
        cmd.Parameters.Add("@appliance_name", SqlDbType.VarChar, 50).Value = appliance_name.Text;
        cmd.Parameters.Add("@appliance_invoice_no", SqlDbType.VarChar, 50).Value = appliance_invoice_no.Text;
        cmd.Parameters.Add("@appliance_invoice_photo_name", SqlDbType.VarChar, 300).Value = invoice_photo_name;
        cmd.Parameters.Add("@appliance_desc", SqlDbType.VarChar, 50).Value = appliance_desc.Text;
        cmd.Parameters.Add("@appliance_purchase_cost", SqlDbType.Money).Value = appliance_purchase_cost.Text;
        cmd.Parameters.Add("@appliance_serial_no", SqlDbType.VarChar, 50).Value = appliance_serial_no.Text;


        date_purchase = Convert.ToDateTime(df.DateCulture(ddl_appliance_date_purchase_m.SelectedValue, ddl_appliance_date_purchase_d.SelectedValue, ddl_appliance_date_purchase_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        cmd.Parameters.Add("@appliance_date_purchase", SqlDbType.DateTime).Value = date_purchase;

           
           
        cmd.Parameters.Add("@appliance_guarantee_year", SqlDbType.Int).Value = Convert.ToInt32(ddl_appliance_guarantee_year.SelectedValue);
        cmd.Parameters.Add("@appliance_photo_name", SqlDbType.VarChar, 300).Value = appliance_photo_name;

        //execute the insert
        cmd.ExecuteReader();
       

          }
         // catch (Exception error)
         {
       
         }


       if (Convert.ToInt32(cmd.Parameters["@return_id"].Value) == 0)
       {
           lbl_success.Text = Resources.Resource.lbl_successfull_modification;
       }

     //  conn.Close();

    //----------------------------------------------------------------------------------
     
       cmd2.CommandType = CommandType.StoredProcedure;

       try
       {

           cmd2.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
           cmd2.Parameters.Add("appliance_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["appliance_id"]);
           SqlDataAdapter da = new SqlDataAdapter(cmd2);
           DataTable dt = new DataTable();
           da.Fill(dt);

           gv_appliance_previous_storage.DataSource = dt;
           gv_appliance_previous_storage.DataBind();
       }

       finally
       {
           conn.Close();
       }

    }
    }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chk_warehouse_CheckedChanged(object sender, EventArgs e)
    {

        if (chk_warehouse.Checked == true)
        {
            chk_home.Checked = false;
            ddl_appliance_home_id.Enabled = false;
            ddl_unit_id.Enabled = false;

            ddl_warehouse_list.Enabled = true;
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chk_home_CheckedChanged(object sender, EventArgs e)
    {

        lbl_select_warehouse.Visible = false;

        if (chk_home.Checked == true)
        {
            chk_warehouse.Checked = false;
            ddl_warehouse_list.Enabled = false;

            ddl_appliance_home_id.Enabled = true;
            ddl_unit_id.Enabled = true;
        }
    }
}
