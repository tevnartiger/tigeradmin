﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_appliance_default.ascx.cs" Inherits="c_manager_appliance_uc_appliance_default" %>
<br />


<table>


<tr><td> <asp:HyperLink ID="HyperLink5" NavigateUrl="~/manager/appliance/appliance_list.aspx" runat="server"><h2>  <asp:Literal ID="Literal5" Text="Items list" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to view and edit all items in your properties.</td></tr>



<tr><td> <asp:HyperLink ID="HyperLink4" NavigateUrl="~/manager/appliance/appliance_add.aspx" runat="server"><h2>  <asp:Literal ID="Literal4" Text="New AItem" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to record a new item in a property or in a warehouse.</td></tr>

<tr><td> <asp:HyperLink ID="HyperLink3" NavigateUrl="~/manager/appliance/appliance_moving_wiz.aspx" runat="server"><h2>  <asp:Literal ID="Literal3" Text="Move Items - Wizard" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to move items from one place to another ( with a wizard )</td></tr>


<tr><td> <asp:HyperLink ID="HyperLink2" NavigateUrl="~/manager/appliance/appliance_moving.aspx" runat="server"><h2>  <asp:Literal ID="Literal2" Text="Move Items " runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to move items from one place to another ( quickly )</td></tr>



</table>
   