﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="user_add.aspx.cs" Inherits="manager_user_user_add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
 <style type="text/css"  >
 div.row  {float: left; margin:  0px 0px 8px 0px; padding: 0; width: 98%;}
 div.col500px {float: left; width: 500px; margin: 0 3px 0 0; padding: 0;}
 div.col65em {float: left; width:940px; margin: 0 3px 0 0; padding: 0;}
 </style>
 
  <script language="javascript" type="text/javascript">
   function CheckAllDataViewCheckBoxes(checkVal) {

            for (i = 0; i < document.forms[0].elements.length; i++) {

                elm = document.forms[0].elements[i];
                if (elm.type == 'checkbox' && elm.name.substring(elm.name.length - 11, elm.name.length) == 'chk_process') {
                    { elm.checked = checkVal }
                }
            }
        }

</script>

 
 
 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
<asp:MultiView ID="MultiView1" runat="server">
  <asp:View ID="View1" runat="server">
<div id="top_row" class="row">
<div class="col65em">     
 <div class="myform">

      
        <h1>New User Account</h1>
        <p>Step 1 - Add User information<br />
           Step 2 - Assign a role to a user
        </p>
        <h1>Step 1 - Add User information</h1>
        <label>first name
        <span class="small"><asp:RegularExpressionValidator 
                        ID="reg_name_fname" runat="server" 
                         ControlToValidate="name_fname"
                          ErrorMessage="Invalid Name">
                        </asp:RegularExpressionValidator><asp:RequiredFieldValidator  ControlToValidate="name_fname"
                           ID="req_name_fname" runat="server" 
                            ErrorMessage="required"></asp:RequiredFieldValidator></span>
        </label>
        <asp:TextBox ID="name_fname"  runat="server"/> 
        
        
        
        <label>last name
        <span class="small"><asp:RegularExpressionValidator 
                        ID="reg_name_lname" runat="server" 
                         ControlToValidate="name_lname"
                        ErrorMessage="invalid">
                        </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator  ControlToValidate="name_lname"
                           ID="req_name_lname" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
        </span>
        </label>
        <asp:TextBox ID="name_lname"  runat="server"/>
        
        
        <div id="Div1" class="row">
        <div class="col500px">
        <label for="TextBox2">Date of Birth
        <span class="small">Add date of birth</span>
        </label>&nbsp;
        <asp:DropDownList ID="ddl_name_dob_m" runat="server">
                     <asp:ListItem Value="1">January</asp:ListItem>
                     <asp:ListItem Value="2">February</asp:ListItem>
                     <asp:ListItem Value="3">March</asp:ListItem>
                     <asp:ListItem Value="4">April</asp:ListItem>
                     <asp:ListItem Value="4">May</asp:ListItem>
                     <asp:ListItem Value="6">June</asp:ListItem>
                     <asp:ListItem Value="7">July</asp:ListItem>
                     <asp:ListItem Value="8">August</asp:ListItem>
                     <asp:ListItem Value="9">September</asp:ListItem>
                     <asp:ListItem Value="10">October</asp:ListItem>
                     <asp:ListItem Value="11">November</asp:ListItem>
                     <asp:ListItem Value="12">December</asp:ListItem>
                 </asp:DropDownList> / 
                 <asp:DropDownList ID="ddl_name_dob_d" runat="server">
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList> / 
                 <asp:DropDownList ID="ddl_name_dob_y" runat="server" >
                     <asp:ListItem>1960</asp:ListItem>
                     <asp:ListItem>1961</asp:ListItem>
                     <asp:ListItem>1962</asp:ListItem>
                     <asp:ListItem>1963</asp:ListItem>
                     <asp:ListItem>1964</asp:ListItem>
                     <asp:ListItem>1965</asp:ListItem>
                     <asp:ListItem>1966</asp:ListItem>
                     <asp:ListItem>1967</asp:ListItem>
                     <asp:ListItem>1968</asp:ListItem>
                     <asp:ListItem>1969</asp:ListItem>
                     <asp:ListItem>1970</asp:ListItem>
                     <asp:ListItem>1972</asp:ListItem>
                     <asp:ListItem>1973</asp:ListItem>
                     <asp:ListItem>1974</asp:ListItem>
                     <asp:ListItem>1975</asp:ListItem>
                     <asp:ListItem>1976</asp:ListItem>
                     <asp:ListItem>1977</asp:ListItem>
                     <asp:ListItem>1978</asp:ListItem>
                     <asp:ListItem>1979</asp:ListItem>
                     <asp:ListItem>1980</asp:ListItem>
                     <asp:ListItem>1981</asp:ListItem>
                     <asp:ListItem>1982</asp:ListItem>
                     <asp:ListItem>1983</asp:ListItem>
                     <asp:ListItem>1984</asp:ListItem>
                     <asp:ListItem>1985</asp:ListItem>
                     <asp:ListItem>1986</asp:ListItem>
                     <asp:ListItem>1987</asp:ListItem>
                     <asp:ListItem>1988</asp:ListItem>
                     <asp:ListItem>1989</asp:ListItem>
                     <asp:ListItem>1990</asp:ListItem>
                     <asp:ListItem>1991</asp:ListItem>
                     <asp:ListItem>1992</asp:ListItem>
                     <asp:ListItem>1993</asp:ListItem>
                     <asp:ListItem>1994</asp:ListItem>
                     <asp:ListItem>1995</asp:ListItem>
                     <asp:ListItem>1996</asp:ListItem>
                     <asp:ListItem>1997</asp:ListItem>
                     <asp:ListItem>1998</asp:ListItem>
                     <asp:ListItem>1999</asp:ListItem>
                     <asp:ListItem>2000</asp:ListItem>
                     <asp:ListItem>2001</asp:ListItem>
                     <asp:ListItem>2002</asp:ListItem>
                     <asp:ListItem>2003</asp:ListItem>
                     <asp:ListItem>2004</asp:ListItem>
                     <asp:ListItem>2005</asp:ListItem>
                     <asp:ListItem>2006</asp:ListItem>
                     <asp:ListItem>2007</asp:ListItem>
                     <asp:ListItem>2008</asp:ListItem>
                 </asp:DropDownList>
        </div>       
        </div>
        <br />
        
       <label>Address
        <span class="small"> <asp:RegularExpressionValidator 
                        ID="reg_name_addr" runat="server" 
                         ControlToValidate="name_addr"
                        ErrorMessage="invalid">
                        </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator  ControlToValidate="name_addr"
                           ID="req_name_addr" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
        </span>
        </label>
        <asp:TextBox ID="name_addr"  runat="server"></asp:TextBox>
        
        
        

        <label>City
        <span class="small"> <asp:RegularExpressionValidator 
                        ID="reg_name_addr_city" runat="server" 
                         ControlToValidate="name_addr_city"
                        ErrorMessage="invalid">
                        </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator  ControlToValidate="name_addr_city"
                           ID="req_name_addr_city" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
        </span>
        </label>
        <asp:TextBox ID="name_addr_city"  runat="server"/>
        
        
        

        <label>Postal Code
        <span class="small"> <asp:RegularExpressionValidator 
                        ID="reg_name_addr_pc" runat="server" 
                         ControlToValidate="name_addr_pc"
                        ErrorMessage="invalid">
                        </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator  ControlToValidate="name_addr_pc"
                           ID="req_name_addr_pc" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
        </span>
        </label>
        <asp:TextBox ID="name_addr_pc"  runat="server"/>
        
        
        
        
        <label>State / Prov
        <span class="small"> 
        </span>
        </label>
       <asp:TextBox ID="name_addr_state"  runat="server"/>
        
        
        
        
        <div id="Div3" class="row">
        <div class="col65em">
        <label >username
        <span class="small"><asp:RegularExpressionValidator 
                        ID="reg_name_login" runat="server" 
                         ControlToValidate="name_login"
                          ErrorMessage="Invalid Name">
                        </asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator  ControlToValidate="name_login"
                           ID="req_name_login" runat="server" 
                            ErrorMessage="required"></asp:RequiredFieldValidator>
        </span>
        </label>
        <asp:TextBox ID="name_login" runat="server" />
        </div>       
        </div>



        <div class="row">
        <div class="col65em">
        <label >password
        <span class="small"><asp:RegularExpressionValidator 
                        ID="reg_name_password" runat="server" 
                         ControlToValidate="name_password"
                          ErrorMessage="Invalid Name">
                        </asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator  ControlToValidate="name_password"
                           ID="req_name_password" runat="server" 
                            ErrorMessage="required"></asp:RequiredFieldValidator>
        </span>
        </label>
        <asp:TextBox ID="name_password" runat="server" />
        </div>       
        </div>
        
        
        <div  class="row">
        <div class="col65em">
        <label >Re-type password
        <span class="small"><asp:RegularExpressionValidator 
                        ID="reg_name_password2" runat="server" 
                         ControlToValidate="name_password2"
                          ErrorMessage="Invalid Name">
                        </asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator  ControlToValidate="name_password2"
                           ID="req_name_password2" runat="server" 
                            ErrorMessage="required"></asp:RequiredFieldValidator>
                            
            <asp:CompareValidator ID="comp_name_password2" runat="server"
             ControlToCompare="name_password" ControlToValidate="name_password2"
             ErrorMessage="he password do not match"></asp:CompareValidator>
         </span>
        </label>
        <asp:TextBox ID="name_password2" runat="server" />
        </div>       
        </div>
        
        <div  class="row">
        <div class="col65em">
        <label >Phone number
        <span class="small">
        <asp:RegularExpressionValidator 
                        ID="reg_name_tel" runat="server" 
                         ControlToValidate="name_tel"
                        ErrorMessage="invalid">
                        </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator  ControlToValidate="name_tel"
                           ID="req_name_tel" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
        </span>
        </label>
        <asp:TextBox ID="name_tel"  runat="server"/>
        </div>       
        </div>
        
        
        <div  class="row">
        <div class="col65em">
        <label >Email
        <span class="small">
         <asp:RegularExpressionValidator 
                        ID="reg_name_email" runat="server" 
                         ControlToValidate="name_email"
                        ErrorMessage="invalid">
                        </asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator  ControlToValidate="name_email"
                           ID="req_name_email" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
        </span>
        </label>
        <asp:TextBox ID="name_email"  runat="server"/>
        </div>       
        </div>
        
             <div style="padding-left:140px; float:left;">
            <asp:Button Width="100px" ID="btn_next" runat="server" Text="Next" 
                     onclick="btn_next_Click" />
      
           </div>
         </div>   
 </div>
    
</div> 


 
 </asp:View>
 
 
 
 
  <asp:View ID="View2" runat="server">
  
  <div id="basic2"   class="myform">

  <h1>New User Account</h1>
   <p></p>
   <h1>Step 2 - Assign a role to a user</h1><br />
    
       
       
        <div id="Div2" class="row">
        <div class="col500px">
        <label >Person
        <span class="small"></span>
        </label>&nbsp;&nbsp;
         <asp:Label ID="lbl_person" runat="server" ></asp:Label>
        </div>       
        </div>
       
      
        <div id="Div4" class="row">
        <div class="col500px">
        <label >Select Role to update 
        <span class="small"></span>
        </label>&nbsp;&nbsp;
        
        <asp:DropDownList ID="ddl_role" runat="server" >
         <asp:ListItem Value="6">Property Manager</asp:ListItem>
         <asp:ListItem Value="5">Maintenance</asp:ListItem>
         <asp:ListItem Text="<%$ Resources:Resource, lbl_owner %>"  Value="4"></asp:ListItem>
        </asp:DropDownList>
        </div>       
        </div>
       
     </div>  
       
 
     
       Select the properties assigned to the person for his/her role<br /><br />
        
     
        
       <div class="row">
        <div class="col500px">
      
     
        
        
        <div style="padding-left:220px;">
        
        
       <label><asp:Button ID="btn_previous" Width="100px" runat="server" Text="Previous" 
                onclick="btn_previous_Click" />
        <span class="small"></span>
        </label>
         <asp:Button ID="btn_submit" runat="server" Text="Submit" 
           onclick="btn_submit_Click" />
            <br /><br />
           
           <asp:GridView ID="gv_home_list" runat="server" AllowPaging="True"  
            AllowSorting="True" AutoGenerateColumns="False" BorderColor="#CDCDCD" 
            BorderWidth="1px" PageSize="20">
            <AlternatingRowStyle BackColor="#F0F0F6" />
            <Columns>
                <asp:TemplateField HeaderText="&lt;input id='chkAllItems' type='checkbox' onclick='CheckAllDataViewCheckBoxes(document.forms[0].chkAllItems.checked)' /&gt;">
                    <ItemTemplate>
                        <asp:CheckBox ID="chk_process" runat="server" />
                        <asp:HiddenField ID="h_home_id" runat="server" Value='<%# Bind("home_id")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="home_name" HeaderText="Property name" />
               
            </Columns>
            <HeaderStyle BackColor="#F0F0F6" />
        </asp:GridView>
       </div>     
     <br />
        
      
      </div>
      </div> 
        <br />
        <asp:Label ID="lbl_success" runat="server" Text=""></asp:Label>
      
 
 </asp:View>
 </asp:MultiView>





         
</asp:Content>

