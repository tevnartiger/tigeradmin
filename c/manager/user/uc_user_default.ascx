﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_user_default.ascx.cs" Inherits="c_manager_user_uc_user_default" %>

<br />


<table>

<tr><td> <asp:HyperLink ID="HyperLink1" NavigateUrl="~/manager/user/user_add.aspx" runat="server"><h2>  <asp:Literal ID="Literal1" Text="New User" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to add a new user to your account.</td></tr>


<tr><td> <asp:HyperLink ID="HyperLink5" NavigateUrl="~/manager/role/role_member_list.aspx?categ=0&l=" runat="server"><h2>  <asp:Literal ID="Literal5" Text="Users Profile" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to view the *profiles of the users of your account </td></tr>



<tr><td> <asp:HyperLink ID="HyperLink4" NavigateUrl="~/manager/role/role_wiz.aspx" runat="server"><h2>  <asp:Literal ID="Literal4" Text="Assign a Role" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to add a new role to a user profile.</td></tr>



<tr><td> <asp:HyperLink ID="HyperLink2" NavigateUrl="~/manager/role/role_update.aspx" runat="server"><h2>  <asp:Literal ID="Literal2" Text="Edit Profile" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to edit the profile of a user.</td></tr>

</table>
