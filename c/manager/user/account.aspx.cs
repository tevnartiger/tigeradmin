﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manager_user_account : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string temp_salt = sinfoca.tiger.security.Hash.createSalt(30);
        string temp_hash = sinfoca.tiger.security.Hash.createPasswordHash(RegEx.getText(TextBox2.Text), temp_salt);

        Label1.Text = temp_salt;
        Label2.Text = temp_hash;
    }
}
