using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class manager_mp_manager2 : System.Web.UI.MasterPage
{

    //By doing so, each session is ensuring a unique view state key and thus preventing the 
    //Cross-Site Request Forgery attacks. You can read more about that here 
    // http://en.wikipedia.org/wiki/Cross-site_request_forgery.

    void Page_Init(object sender, EventArgs e)
    {
        Page.ViewStateUserKey = Session.SessionID;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}
