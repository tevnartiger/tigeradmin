﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_tools_default.ascx.cs" Inherits="c_manager_tools_uc_tools_default" %>

<br />


<table>

<tr><td> <asp:HyperLink ID="HyperLink5" NavigateUrl="~/manager/tools/tools_amortization.aspx" runat="server">
<h2>  <asp:Literal ID="Literal5" Text="Mortage Calculator" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to calculate your mortage payments and generate charts and grahps of payments.</td></tr>


<tr><td> <asp:HyperLink ID="HyperLink1" NavigateUrl="~/manager/financial/financial_analysis_period_list.aspx" runat="server">
<h2>  <asp:Literal ID="Literal1" Text="Time range Financial scenario" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to view the list of saved time range financial scenario </td></tr>


<tr><td> <asp:HyperLink ID="HyperLink4" NavigateUrl="~/manager/tools/financial_scenario_list.aspx" runat="server">
<h2>  <asp:Literal ID="Literal4" Text="Financial scenario" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to view the list of saved financial scenario.</td></tr>

</table>
