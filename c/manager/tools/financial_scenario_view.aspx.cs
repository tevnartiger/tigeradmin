﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : march 4, 2008
/// </summary>
public partial class manager_Financial_financial_scenario_view : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!RegEx.IsInteger(Request.QueryString["mfs_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        ////////
        //////// SECURITY CHECK BEGIN //////////////////
        ////////
        AccountObjectAuthorization mfsAuthorization = new AccountObjectAuthorization(strconn);

        if (mfsAuthorization.FinancialMoneyFlowScenario(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["mfs_id"])))
        { }
        else
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ////////
        //////// SECURITY CHECK END //////////////////
        ////////


        if (!(Page.IsPostBack))
        {

            update_link.NavigateUrl = "financial_scenario_update.aspx?mfs_id=" + Request.QueryString["mfs_id"];

            tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            
              int home_id = 0;

                tiger.Financial f = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

                home_id = f.getFinancialMfsHome(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["mfs_id"]));

                h_home_id.Value = home_id.ToString();

                //tiger.Financial f = new tiger.Financial(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                rMfsNumberUnitbyBedroomNumber.DataSource = f.getMfsNumberUnitbyBedroomNumber(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["mfs_id"]));
                rMfsNumberUnitbyBedroomNumber.DataBind();

                rNumberComUnit.DataSource = f.getMfsNumberComUnit(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["mfs_id"]));
                rNumberComUnit.DataBind();

               
                //To view the address of the property

                tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
                rhome_view.DataBind();

            //**************************************************************************************************************
            //**************************************************************************************************************
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prFinancialMoneyFlowScenarioView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@mfs_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["mfs_id"]);
           
              try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);



                while (dr.Read() == true)
                {

                    lbl_name.Text = dr["mfs_name"].ToString();

                    lbl_parking_m.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_parking"])) ;
                    lbl_storage_m.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_storage"])) ;
                    lbl_laundry_m.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_laundry"])) ;


                    //this income section was added after the other, therefore the initial value
                    // of those incume were NULL in the database
                    //***************************************************************************
                    //*************************************************************************

                    if (dr["mfs_garage"] == DBNull.Value)
                        lbl_garage_m.Text = String.Format("{0:0.00}",0.00);
                    else
                        lbl_garage_m.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_garage"])) ;


                    if (dr["mfs_vending_machine"] == DBNull.Value)
                        lbl_vending_machine_m.Text = String.Format("{0:0.00}",0.00);
                    else
                        lbl_vending_machine_m.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_vending_machine"])) ;


                    if (dr["mfs_cash_machine"] == DBNull.Value)
                        lbl_cash_machine_m.Text = String.Format("{0:0.00}",0.00);
                    else
                        lbl_cash_machine_m.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_cash_machine"]));



                    if (dr["mfs_other"] == DBNull.Value)
                        lbl_other_m2.Text = String.Format("{0:0.00}",0.00);
                    else
                        lbl_other_m2.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_other"]));

                    //***************************************************************************
                    //*************************************************************************


                    lbl_vacancy_rate.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_vacancyrate"]));
                    lbl_bda_rate.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_bdarate"]));

                    lbl_home_name.Text = dr["home_name"].ToString();
                    lbl_home_value.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_home_value"]));


                    lbl_electricity.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_electricityrate"]));
                    lbl_energy.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_energyrate"]));
                    lbl_insurances.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_insurancerate"]));
                    lbl_janitor.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_janitorrate"]));
                    lbl_taxes.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_propertyrate"]));
                    lbl_maintenance_repair.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_maintenancerate"]));
                    lbl_school_taxes.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_schoolrate"]));
                    lbl_management.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_managementrate"]));
                    lbl_advertising.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_advertiserate"]));
                    lbl_legal.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_legalrate"]));
                    lbl_accounting.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_accountingrate"]));
                    lbl_other.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_otherrate"]));

                    lbl_ads_y.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_ads"]));
                    lbl_capitalisation_y.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_capitalisation"]));
                    lbl_added_value_y.Text = String.Format("{0:0.00}",Convert.ToDouble(dr["mfs_addedvalue"]));

                    //ddl_year.SelectedValue = dr["mfs_year"].ToString();
                    label_year.Text = dr["mfs_year"].ToString();

                   
                }

            }

            finally
            {
                conn.Close();
            }

          
            //**************************************************************************************************************
            //**************************************************************************************************************


            decimal month_total = 0;
            decimal year_total = 0.01M;
            decimal vacancy_rate = 0;
            decimal bad_debt_allowance_rate = 0;

            decimal electricity_rate = 0;
            decimal energy_rate = 0;
            decimal insurance_rate = 0;
            decimal janitor_rate = 0;
            decimal property_taxe_rate = 0;
            decimal maintenance_repair_rate = 0;
            decimal school_taxe_rate = 0;
            decimal management_rate = 0;
            decimal advertising_rate = 0;
            decimal legal_rate = 0;
            decimal accounting_rate = 0;
            decimal other_rate = 0;


            decimal total_expenses_m = 0;
            decimal total_expenses_y = 0;



            decimal number_of_unit = 0;

            //GO TROUGH THE REPEATER

            for (int i = 0; i < rMfsNumberUnitbyBedroomNumber.Items.Count; i++)
            {
                Label bedrooms_total_rent_m = (Label)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_m");
                Label lbl_bedrooms_total_rent_y = (Label)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_y");
                HiddenField h_number_of_unit = (HiddenField)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("h_number_of_unit");

                lbl_bedrooms_total_rent_y.Text = String.Format("{0:0.00}", Convert.ToDecimal(bedrooms_total_rent_m.Text) * 12);

                month_total = month_total + Convert.ToDecimal(bedrooms_total_rent_m.Text);

                number_of_unit = number_of_unit + Convert.ToDecimal(h_number_of_unit.Value);
            }


            for (int i = 0; i < rNumberComUnit.Items.Count; i++)
            {
                Label commercial_total_rent_m = (Label)rNumberComUnit.Items[i].FindControl("lbl_commercial_total_rent_m");
                Label lbl_commercial_total_rent_y = (Label)rNumberComUnit.Items[i].FindControl("lbl_commercial_total_rent_y");

                HiddenField h_number_of_unit = (HiddenField)rNumberComUnit.Items[i].FindControl("h_number_of_unit");

                lbl_commercial_total_rent_y.Text = String.Format("{0:0.00}", Convert.ToDecimal(commercial_total_rent_m.Text) * 12);

                month_total = month_total + Convert.ToDecimal(commercial_total_rent_m.Text);

                number_of_unit = number_of_unit + Convert.ToDecimal(h_number_of_unit.Value);
            }

            h_total_number_of_unit.Value = number_of_unit.ToString();



            month_total = month_total + Convert.ToDecimal(lbl_parking_m.Text) + Convert.ToDecimal(lbl_storage_m.Text)
                          + Convert.ToDecimal(lbl_laundry_m.Text) + Convert.ToDecimal(lbl_garage_m.Text)
                      + Convert.ToDecimal(lbl_vending_machine_m.Text) + Convert.ToDecimal(lbl_cash_machine_m.Text)
                      + Convert.ToDecimal(lbl_other_m2.Text); 

            year_total = month_total * 12;



            lbl_pgi_m.Text = String.Format("{0:0.00}", month_total);
            lbl_pgi_y.Text = String.Format("{0:0.00}", year_total);



            // diplay the (%) of income -----------------------------------
            decimal rent = 0;
            for (int i = 0; i < rMfsNumberUnitbyBedroomNumber.Items.Count; i++)
            {
                rent = 0;
                Label lbl_bedrooms_total_rent_y = (Label)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_y");
                Label lbl_bedrooms_total_rent_percent = (Label)rMfsNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_percent");

                if(year_total>0)
                 lbl_bedrooms_total_rent_percent.Text =  String.Format("{0:0.00}",(Convert.ToDecimal(lbl_bedrooms_total_rent_y.Text) / year_total) * 100);

                String.Format("{0:0.00}", lbl_bedrooms_total_rent_percent.Text);
            }


            for (int i = 0; i < rNumberComUnit.Items.Count; i++)
            {
                rent = 0;
                Label lbl_commercial_total_rent_y = (Label)rNumberComUnit.Items[i].FindControl("lbl_commercial_total_rent_y");
                Label lbl_commercial_total_rent_percent = (Label)rNumberComUnit.Items[i].FindControl("lbl_commercial_total_rent_percent");

                lbl_commercial_total_rent_percent.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_commercial_total_rent_y.Text) / year_total) * 100);

                String.Format("{0:0.00}", lbl_commercial_total_rent_percent.Text);
            }
            //---------------------------------------------------------------



            // get the income for parking, laundry and storage
            lbl_parking_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_parking_m.Text) * 12));
            lbl_storage_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_storage_m.Text) * 12));
            lbl_laundry_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_laundry_m.Text) * 12));

            lbl_garage_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_garage_m.Text) * 12));
            lbl_vending_machine_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_vending_machine_m.Text) * 12));
            lbl_cash_machine_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_cash_machine_m.Text) * 12));
            lbl_other_y2.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_other_m2.Text) * 12));



            // diplay the (%) of income -----------------------------------
            if(year_total >0)
            {
                test_park_percent.Text = String.Format("{0:0.00}",((Convert.ToDecimal(lbl_parking_y.Text) / year_total) * 100));
                lbl_storage_percent.Text = String.Format("{0:0.00}",((Convert.ToDecimal(lbl_storage_y.Text) / year_total) * 100));
                lbl_laundry_percent.Text = String.Format("{0:0.00}",((Convert.ToDecimal(lbl_laundry_y.Text) / year_total) * 100));
                lbl_garage_percent.Text = String.Format("{0:0.00}",((Convert.ToDecimal(lbl_garage_y.Text) / year_total) * 100));
                lbl_vending_machine_percent.Text = String.Format("{0:0.00}",((Convert.ToDecimal(lbl_vending_machine_y.Text) / year_total) * 100));
                lbl_cash_machine_percent.Text = String.Format("{0:0.00}",((Convert.ToDecimal(lbl_cash_machine_y.Text) / year_total) * 100));
                lbl_other_percent2.Text = String.Format("{0:0.00}", ((Convert.ToDecimal(lbl_other_y2.Text) / year_total) * 100));
            }

            //---------------------------------------------------------------





            // get the vacancy_rate and bad_debt_allowance_rate
            vacancy_rate = Convert.ToDecimal(lbl_vacancy_rate.Text) / 100;
            bad_debt_allowance_rate = Convert.ToDecimal(lbl_bda_rate.Text) / 100;


            // Income Loss , for vacancy
            lbl_vacancy_m.Text = String.Format("{0:0.00}",(vacancy_rate * month_total));
            lbl_vacancy_y.Text = String.Format("{0:0.00}",(vacancy_rate * year_total));


            // Income Loss , for bad debt allowance
            lbl_bda_m.Text = String.Format("{0:0.00}",(bad_debt_allowance_rate * month_total));
            lbl_bda_y.Text = String.Format("{0:0.00}",(bad_debt_allowance_rate * year_total));


            // total effective gross income for month and year
            lbl_egi_m.Text = String.Format("{0:0.00}",(month_total - Convert.ToDecimal(lbl_vacancy_m.Text) - Convert.ToDecimal(lbl_bda_m.Text)));
            lbl_egi_y.Text = String.Format("{0:0.00}",(year_total - Convert.ToDecimal(lbl_vacancy_y.Text) - Convert.ToDecimal(lbl_bda_y.Text)));

            /// get the rate of utilities , management ,advertisement , taxes etc...
            /// 
            electricity_rate = Convert.ToDecimal(lbl_electricity.Text) / 100;
            energy_rate = Convert.ToDecimal(lbl_energy.Text) / 100;
            insurance_rate = Convert.ToDecimal(lbl_insurances.Text) / 100;

            janitor_rate = Convert.ToDecimal(lbl_janitor.Text) / 100;
            property_taxe_rate = Convert.ToDecimal(lbl_taxes.Text) / 100;
            maintenance_repair_rate = Convert.ToDecimal(lbl_maintenance_repair.Text) / 100;

            school_taxe_rate = Convert.ToDecimal(lbl_school_taxes.Text) / 100;
            management_rate = Convert.ToDecimal(lbl_management.Text) / 100;
            advertising_rate = Convert.ToDecimal(lbl_advertising.Text) / 100;
            legal_rate = Convert.ToDecimal(lbl_legal.Text) / 100;
            accounting_rate = Convert.ToDecimal(lbl_accounting.Text) / 100;
            other_rate = Convert.ToDecimal(lbl_other.Text) / 100;

            // eletricity expenses
            lbl_electricity_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_m.Text) * electricity_rate));
            lbl_electricity_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_y.Text) * electricity_rate));

            // energy expenses
            lbl_energy_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_m.Text) * energy_rate));
            lbl_energy_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_y.Text) * energy_rate));

            // insurance expenses
            lbl_insurances_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_m.Text) * insurance_rate));
            lbl_insurances_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_y.Text) * insurance_rate));

            // janitor expenses
            lbl_janitor_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_m.Text) * janitor_rate));
            lbl_janitor_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_y.Text) * janitor_rate));

            // property taxes expenses
            lbl_taxes_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_m.Text) * property_taxe_rate));
            lbl_taxes_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_y.Text) * property_taxe_rate));

            // maintenance & repair expenses
            lbl_maintenance_repair_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_m.Text) * maintenance_repair_rate));
            lbl_maintenance_repair_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_y.Text) * maintenance_repair_rate));

            // school taxes expenses
            lbl_school_taxes_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_m.Text) * school_taxe_rate));
            lbl_school_taxes_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_y.Text) * school_taxe_rate));

            // management expenses
            lbl_management_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_m.Text) * management_rate));
            lbl_management_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_y.Text) * management_rate));

            // advertising expenses
            lbl_advertising_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_m.Text) * advertising_rate));
            lbl_advertising_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_y.Text) * advertising_rate));

            // legal expenses
            lbl_legal_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_m.Text) * legal_rate));
            lbl_legal_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_y.Text) * legal_rate));


            // accounting expenses
            lbl_accounting_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_m.Text) * accounting_rate));
            lbl_accounting_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_y.Text) * accounting_rate));

            // Other expenses
            lbl_other_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_m.Text) * other_rate));
            lbl_other_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_egi_y.Text) * other_rate));



            // Montly Gross total expenses


            total_expenses_m = Convert.ToDecimal(lbl_electricity_m.Text) + Convert.ToDecimal(lbl_energy_m.Text) +
                                       Convert.ToDecimal(lbl_insurances_m.Text) + Convert.ToDecimal(lbl_janitor_m.Text) + Convert.ToDecimal(lbl_taxes_m.Text) +
                                       Convert.ToDecimal(lbl_maintenance_repair_m.Text) + Convert.ToDecimal(lbl_school_taxes_m.Text) + Convert.ToDecimal(lbl_management_m.Text) + Convert.ToDecimal(lbl_advertising_m.Text) +
                                      Convert.ToDecimal(lbl_legal_m.Text) + Convert.ToDecimal(lbl_accounting_m.Text) + Convert.ToDecimal(lbl_other_m.Text);


            total_expenses_y = Convert.ToDecimal(lbl_electricity_y.Text) + Convert.ToDecimal(lbl_energy_y.Text) +
                                        Convert.ToDecimal(lbl_insurances_y.Text) + Convert.ToDecimal(lbl_janitor_y.Text) + Convert.ToDecimal(lbl_taxes_y.Text) +
                                        Convert.ToDecimal(lbl_maintenance_repair_y.Text) + Convert.ToDecimal(lbl_school_taxes_y.Text) + Convert.ToDecimal(lbl_management_y.Text) + Convert.ToDecimal(lbl_advertising_y.Text) +
                                       Convert.ToDecimal(lbl_legal_y.Text) + Convert.ToDecimal(lbl_accounting_y.Text) + Convert.ToDecimal(lbl_other_y.Text);

            lbl_total_expenses_m.Text = String.Format("{0:0.00}", total_expenses_m);
            lbl_total_expenses_y.Text = String.Format("{0:0.00}", total_expenses_y);



            // diplay the (%) of expense -----------------------------------
            if (total_expenses_y >0)
            {
                lbl_electricity_percent.Text = String.Format("{0:0.00}",((Convert.ToDecimal(lbl_electricity_y.Text) / total_expenses_y) * 100));
                lbl_energy_percent.Text =String.Format("{0:0.00}",((Convert.ToDecimal(lbl_energy_y.Text) / total_expenses_y) * 100));
                lbl_insurances_percent.Text = String.Format("{0:0.00}",((Convert.ToDecimal(lbl_insurances_y.Text) / total_expenses_y) * 100));
                lbl_janitor_percent.Text = String.Format("{0:0.00}",((Convert.ToDecimal(lbl_janitor_y.Text) / total_expenses_y) * 100));
                lbl_taxes_percent.Text = String.Format("{0:0.00}",((Convert.ToDecimal(lbl_taxes_y.Text) / total_expenses_y) * 100));
                lbl_maintenance_repair_percent.Text = String.Format("{0:0.00}",((Convert.ToDecimal(lbl_maintenance_repair_y.Text) / total_expenses_y) * 100));

                lbl_school_taxes_percent.Text = String.Format("{0:0.00}",((Convert.ToDecimal(lbl_school_taxes_y.Text) / total_expenses_y) * 100));
                lbl_management_percent.Text = String.Format("{0:0.00}",((Convert.ToDecimal(lbl_management_y.Text) / total_expenses_y) * 100));
                lbl_advertising_percent.Text = String.Format("{0:0.00}",((Convert.ToDecimal(lbl_advertising_y.Text) / total_expenses_y) * 100));
                lbl_legal_percent.Text = String.Format("{0:0.00}",((Convert.ToDecimal(lbl_legal_y.Text) / total_expenses_y) * 100));
                lbl_accounting_percent.Text = String.Format("{0:0.00}",((Convert.ToDecimal(lbl_accounting_y.Text) / total_expenses_y) * 100));
                lbl_other_percent.Text = String.Format("{0:0.00}", ((Convert.ToDecimal(lbl_other_y.Text) / total_expenses_y) * 100));
            }

            //---------------------------------------------------------------





            lbl_total_exp_percent.Text = Convert.ToString((electricity_rate + energy_rate + insurance_rate + janitor_rate + property_taxe_rate + maintenance_repair_rate + school_taxe_rate +
                                          management_rate + advertising_rate + legal_rate + accounting_rate + other_rate) * 100);


            // Net gross exploiation income
            lbl_net_operating_inc_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) - Convert.ToDecimal(lbl_total_expenses_y.Text)));

            // Generated liquidity ( before income taxes )
            lbl_liquidity_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_net_operating_inc_y.Text) - Convert.ToDecimal(lbl_ads_y.Text)));

            // Generated liquidity + capitalisation
            lbl_liquidity_capitalisation_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_liquidity_y.Text) + Convert.ToDecimal(lbl_capitalisation_y.Text)));

            //Liquidity + Cap. +Added value
            lbl_liq_cap_value_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_liquidity_capitalisation_y.Text) + Convert.ToDecimal(lbl_added_value_y.Text)));


            //GO TROUGH THE REPEATER


            // Analysis of Return calculations

            double property_value = 0;
            double added_value = 0;
            double total_number_of_unit = Convert.ToDouble(h_total_number_of_unit.Value);
            double appartment_cost = 0;

            property_value = Convert.ToDouble(lbl_home_value.Text);

          
            property_value = property_value + Convert.ToDouble(lbl_added_value_y.Text);

            appartment_cost = (property_value / total_number_of_unit);

            lbl_appartment_cost.Text = String.Format("{0:0.00}",appartment_cost);
            lbl_mbre.Text = String.Format("{0:0.00}",((property_value / Convert.ToDouble(lbl_egi_y.Text))));
            lbl_rde.Text = String.Format("{0:0.00}",((Convert.ToDouble(lbl_total_expenses_y.Text) / Convert.ToDouble(lbl_egi_y.Text))));

            lbl_tmo.Text = String.Format("{0:0.00}",((Convert.ToDouble(lbl_total_expenses_y.Text) + Convert.ToDouble(lbl_ads_y.Text)) / Convert.ToDouble(lbl_pgi_y.Text)));

            lbl_trn.Text = String.Format("{0:0.00}",(Convert.ToDouble(lbl_net_operating_inc_y.Text) / property_value));

            lbl_mrn.Text = String.Format("{0:0.00}",(property_value / Convert.ToDouble(lbl_net_operating_inc_y.Text)));

            lbl_rcd.Text = String.Format("{0:0.00}",((Convert.ToDouble(lbl_net_operating_inc_y.Text) / Convert.ToDouble(lbl_ads_y.Text))));


        }
    }
}
