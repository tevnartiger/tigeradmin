<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="portfolio_modify.aspx.cs" Inherits="portfolio_portfolio_modify" Title="New group" %>
<%@ Register Src="~/manager/uc/uc_content_menu_group.ascx" TagName="uc_content_menu_group"
                      TagPrefix="uc_content_menu_group" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
 <table width="900px">
 <tr><td colspan="3"><h2>Modify Property Group</h2></td></tr>
 <tr><td colspan="3"> 
<asp:DropDownList ID="ddl_group" runat="server"  AutoPostBack="true" OnSelectedIndexChanged="ddl_group_SelectedIndexChanged"  DataTextField="group_name" DataValueField="group_id" />
 </td></tr>
<tr><td colspan="3" align="center"><uc_content_menu_group:uc_content_menu_group ID="uc_group" runat="server" />
<br />
<br /></td></tr>

<tr><td valign="top" width="175">
<asp:TextBox ID="tbx_group_name" runat="server" Width="150" MaxLength="15" />
<br />
<br />
    <asp:RegularExpressionValidator 
    ID="reg_tbx_group_name" runat="server" 
     ControlToValidate="tbx_group_name"
        ErrorMessage="invalid name">
        </asp:RegularExpressionValidator>
</td>
<td valign="top" width="200">
  <asp:GridView ID="gv_hg" runat="server" AutoGenerateColumns="False"  DataKeyNames="home_id"   >
  
   <Columns>
       <asp:TemplateField>
          <ItemTemplate>
            <asp:CheckBox ID="chk_hg_id"  runat="server" />
            <asp:HiddenField Visible="false" ID="h_exist" runat="server" Value='<%# Bind("exist") %>' />
         <asp:HiddenField Visible="false" ID="h_home_id" runat="server" Value='<%# Bind("home_id") %>' />
           </ItemTemplate>
       </asp:TemplateField>
 
<asp:BoundField DataField="home_name" HeaderText="" SortExpression="home_name" />
 </Columns>
</asp:GridView></td>
<td valign="top" align="left">
 <asp:Button ID="btn_update" runat="server" Text="<%$ resources:resource, lbl_update %>" OnClick="btn_update_Click"  />      
      
    </td></tr></table>  
 
 
</asp:Content>