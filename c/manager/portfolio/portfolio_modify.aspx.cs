using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.AccountObjectAuthorization;

public partial class portfolio_portfolio_modify : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        reg_tbx_group_name.ValidationExpression = RegEx.getName();
        
        
        if(!Page.IsPostBack)
        {
            tiger.Group g = new tiger.Group(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_group.DataSource =  g.getGroupHomeList(Convert.ToInt32(Session["schema_id"]));
            ddl_group.DataBind();
         //  ddl_group.Items.Insert(0, new ListItem(Resources.Resource.lbl_group_profile, "0"));
           //get first occurence of group
            int temp_group_id = g.groupFindFirst(Convert.ToInt32(Session["schema_id"]));
            ddl_group.SelectedValue = Convert.ToString(temp_group_id);
            

       // int temp_group_id = Convert.ToInt32(Request.QueryString["group_id"]);
       // tiger.Group g = new tiger.Group(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        tbx_group_name.Text = g.retrieveGroupName(Convert.ToInt32(Session["schema_id"]), temp_group_id);

        gv_hg.DataSource = g.getGroupProfileHomeList(Convert.ToInt32(Session["schema_id"]), temp_group_id);
        gv_hg.DataBind();
 

        // Looping through all the rows in the GridView
        foreach (GridViewRow row in gv_hg.Rows)
        {

            CheckBox checkbox = (CheckBox)row.FindControl("chk_hg_id");
            HiddenField exist = (HiddenField)row.FindControl("h_exist");
            //Response.Write(exist.Value);
            if (exist.Value == "1")
            {
                Response.Write(checkbox.Checked);
                // checkbox.Text = "ON --";
                checkbox.Checked = true;
                //Response.Write(+ "&nbsp;");
                //  checkbox.Checked;

                //true;
            }
        }
        }
  
    }

    
    protected void btn_update_Click(object sender, EventArgs e)
    {

        Page.Validate();
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        
        string temp_group_id = Convert.ToString(ddl_group.SelectedValue);
        string temp_group_name = RegEx.getText(tbx_group_name.Text);
      
        string home_id = "";
        string exist = "";
        int number_of_insert = 0;

        for (int j = 0; j < gv_hg.Rows.Count; j++)
        {

            CheckBox checkBox = (CheckBox)gv_hg.Rows[j].FindControl("chk_hg_id");
            HiddenField h_home_id = (HiddenField)gv_hg.Rows[j].FindControl("h_home_id");
            HiddenField h_exist = (HiddenField)gv_hg.Rows[j].FindControl("h_exist");

            if (checkBox.Checked)
            {
                exist = exist + "1" + "|";
                home_id = home_id + h_home_id.Value + "|";
                number_of_insert++;
            }
        }

        

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prGroupUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

       // try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.NVarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@home_id", SqlDbType.NVarChar, 3900).Value = home_id;
            cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = temp_group_id;
            cmd.Parameters.Add("@exist", SqlDbType.NVarChar, 3900).Value = exist;
            cmd.Parameters.Add("@group_name", SqlDbType.NVarChar, 15).Value = RegEx.getText(tbx_group_name.Text);
            cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;
            
            //execute the insert
            cmd.ExecuteReader();

        }
      //  finally
        {
            conn.Close();
            if (Convert.ToInt32(cmd.Parameters["@return_id"].Value) == 0)
            {
               // Response.Redirect("group_modify.aspx?group_id="+Request.QueryString["group_id"]);
            }

        }
  
    }
   protected string getOwnerProfile(int schema_id, int home_id)
        {
            string str_view = "";
        //get owner
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
 
            SqlCommand cmd = new SqlCommand("prProfileOwnerInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                   
            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader();

                while (dr.Read() == true)
                {
                    str_view +=   Convert.ToString(dr["owner"])+"<br/>"+Convert.ToString(dr["owner_tel"])+"<br/>"+Convert.ToString(dr["owner_email"]);
                    //get owner
                }
                
                return str_view;
            }
            finally
            {
                conn.Close();
            }

        }

    protected void ddl_group_SelectedIndexChanged(object sender, EventArgs e)
    {
        int temp_group_id = Convert.ToInt32(ddl_group.SelectedValue);
        tiger.Group g = new tiger.Group(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        tbx_group_name.Text = g.retrieveGroupName(Convert.ToInt32(Session["schema_id"]), temp_group_id);

        gv_hg.DataSource = g.getGroupProfileHomeList(Convert.ToInt32(Session["schema_id"]), temp_group_id);
        gv_hg.DataBind();
        gv_hg.SelectedIndex = temp_group_id;
        // Looping through all the rows in the GridView
        foreach (GridViewRow row in gv_hg.Rows)
        {

            CheckBox checkbox = (CheckBox)row.FindControl("chk_hg_id");
            HiddenField exist = (HiddenField)row.FindControl("h_exist");
            //Response.Write(exist.Value);
            if (exist.Value == "1")
            {
                Response.Write(checkbox.Checked);
                // checkbox.Text = "ON --";
                checkbox.Checked = true;
                //Response.Write(+ "&nbsp;");
                //  checkbox.Checked;

                //true;
            }
        }
         
   
    }
}
