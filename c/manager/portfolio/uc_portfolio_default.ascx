﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_portfolio_default.ascx.cs" Inherits="c_manager_portfolio_uc_portfolio_default" %>
<br />

<table>

<tr><td> <asp:HyperLink ID="HyperLink6" NavigateUrl="~/manager/portfolio/portfolio_add.aspx" runat="server"><h2>  <asp:Literal ID="Literal6" Text="New Portfolio" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to group the properties of your account into portfolios.<br /> These portfolios will be use to
         make financial reports by portfolios</td></tr>


<tr><td> <asp:HyperLink ID="HyperLink1" NavigateUrl="~/manager/portfolio/portfolio_modify.aspx" runat="server"><h2>  <asp:Literal ID="Literal1" Text="Edit Portfolio" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to edit your portfolios</td></tr>


<tr><td> <asp:HyperLink ID="HyperLink2" NavigateUrl="~/manager/portfolio/portfolio_remove.aspx" runat="server"><h2>  <asp:Literal ID="Literal2" Text="Delete Portfolio" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to delete your portfolios</td></tr>
         
 <tr><td> <asp:HyperLink ID="HyperLink3" NavigateUrl="~/manager/portfolio/portfolio_profile.aspx" runat="server"><h2>  <asp:Literal ID="Literal3" Text="Portfolio Profile" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to view your portfolios profile</td></tr>
         
         
  </table>
    
  <br />
  <br />
  <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>