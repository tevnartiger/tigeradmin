using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.AccountObjectAuthorization;

public partial class home_home_view_photo : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
      
            if (!Page.IsPostBack)
            {
                int home_id = Convert.ToInt32(Request.QueryString["home_id"]);



                ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
               // if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), home_id))
                {
                //    Session.Abandon();
                //    Response.Redirect("http://www.sinfocatiger.com/login.aspx");
                }
                ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
             

                tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                if (home_id < 1)
                {
                      home_id = h.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));
                }
                ddl_home.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                 ddl_home.DataBind();
                 ddl_home.SelectedIndex = home_id;
               
                /************************Now get home information*****************************/

                txt.InnerHtml = h.getHomeViewInfo(Convert.ToInt32(Session["schema_id"]), home_id, Convert.ToChar(Session["user_lang"]));
            }
     
    }
    protected void ddl_home_SelectedIndexChanged(object sender, EventArgs e)
    {
      
        
        int home_id = Convert.ToInt32(ddl_home.SelectedValue);
        //get home info  
        tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        

        /************************Now get home information*****************************/

        txt.InnerHtml = h.getHomeViewInfo(Convert.ToInt32(Session["schema_id"]), home_id, Convert.ToChar(Session["user_lang"]));
            

    
    }
   
}
