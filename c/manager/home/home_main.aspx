<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="home_main.aspx.cs" Inherits="home_home_main"   Title="Prospective tenant view" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div style="font-weight: 700">
        <span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA"> 
        <br />
        <asp:HyperLink ID="HyperLink47" runat="server" NavigateUrl="~/manager/events/default.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Events Calendar</span></asp:HyperLink>
        &nbsp;
        <span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA"> 
                          <asp:HyperLink ID="HyperLink48" runat="server" 
                              NavigateUrl="~/manager/events/event_add.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Add an event</span></asp:HyperLink>
        &nbsp;
        </span>
                          <asp:HyperLink ID="HyperLink49" runat="server" 
                              NavigateUrl="~/manager/Scheduler/default2.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Event cal. 2</span></span></asp:HyperLink>
        &nbsp;&nbsp;<asp:HyperLink ID="HyperLink56" runat="server" 
            NavigateUrl="~/tiger/numberofuser.aspx"># of logged in user</asp:HyperLink>
                          <br />
                          <br />
                          <asp:HyperLink ID="HyperLink51" runat="server" 
                              NavigateUrl="~/EncDecViaCode.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">webconfig encrypt/decrypt</span></asp:HyperLink>
        &nbsp;
        &nbsp;
        &nbsp;
        <br />
        <asp:HyperLink ID="HyperLink36" runat="server" 
            NavigateUrl="~/manager/warehouse/warehouse_add.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">ADD A WAREHOUSE 
        (ENTREP�T )</span></asp:HyperLink>
        &nbsp;
        <asp:HyperLink ID="HyperLink37" runat="server" 
            NavigateUrl="~/manager/warehouse/warehouse_list.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Warehouse list</span></asp:HyperLink>
        &nbsp;
        <asp:HyperLink ID="HyperLink38" runat="server" 
            NavigateUrl="~/manager/appliance/appliance_moving.aspx">APPLIANCE MOVE</asp:HyperLink>
        &nbsp;
                               <asp:HyperLink ID="HyperLink52" runat="server" 
                                   NavigateUrl="~/manager/appliance/appliance_moving_wiz.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Appliance move wizard</span></asp:HyperLink>
        <br />
        &nbsp;
        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <asp:HyperLink ID="HyperLink20" runat="server" 
            NavigateUrl="~/manager/income/income_late_rent_fees.aspx">Late rent fees 
        payments</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:HyperLink ID="HyperLink21" runat="server" 
            NavigateUrl="~/manager/income/income_received_late_rent_fees.aspx">Late rent 
        payment fees received</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:HyperLink ID="HyperLink26" runat="server" 
            NavigateUrl="~/manager/Financial/financial_analysis_period_list.aspx">List of Financial 
        analysis for a period (views ,update,delete )</asp:HyperLink>
    
        <br />
    
        <br />
        <strong>REPORTS (todo) , </strong><span lang="EN-US" style="font-size: 10pt; font-family: 'Courier New';
            mso-ansi-language: EN-US">tenant invoice,tenant financial,Building financials, Management</span><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">. 
        </span><br />
&nbsp;<asp:HyperLink ID="HyperLink59" runat="server" 
            NavigateUrl="~/manager/income/income_received_late_rent_fees.aspx">Late rent 
        payment fees received</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <asp:HyperLink ID="HyperLink23" runat="server" 
            NavigateUrl="~/manager/tenant/tenant_rent_update.aspx">Update/edit Rent</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
&nbsp;<asp:HyperLink ID="HyperLink63" runat="server" 
            NavigateUrl="~/manager/Financial/financial_analysis_period_list.aspx">List of Financial 
        analysis for a period (views ,update,delete )</asp:HyperLink>
    
        <br />
                        
         <strong>
        Mortgage renewal - year term ( TODO )<br />
        Move appliance ( TODO )<br />
        Prospect screening - reference, income &amp; employement, eviction, criminal record,
        identification,is applicant free of collection, credit check (TODO)<br />
        Litigation record - with tenant , contractor , bank , insurance agencies (TODO)<br />
        </strong>
        </div>
 </asp:Content>
