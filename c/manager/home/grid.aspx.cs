﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.AccountObjectAuthorization;

public partial class manager_home_grid : BasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((!Page.IsPostBack))
        {
            String referer = "";
            referer = Request.UrlReferrer.ToString();

            //  Label2.Text = referer;
            // if value = 0 form was not submit
            h_btn_submit.Value = "0";


            DateTime test_date = new DateTime();
            test_date = DateTime.Now;


            String the_date = "";
            DateTime right_now = new DateTime();
            right_now = DateTime.Now;

            tiger.Date d = new tiger.Date();

            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));

            // if the referer is self i.e : from the respon redirect of the submit button
            //********************************************************************
            if (Request.UrlReferrer.ToString() == Request.Url.ToString())
            {
                right_now = Convert.ToDateTime(d.DateCulture(Session["date_received_m"].ToString(), Session["date_received_d"].ToString(), Session["date_received_y"].ToString(), Convert.ToString(Session["_lastCulture"])));
            }
            //********************************************************************





            tiger.Home l = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getHomeCount(Convert.ToInt32(Session["schema_id"]));


            // construction of ddl of months ( globalized )
            ddl_date_received_m.Items.Insert(0, Resources.Resource.txt_month);

            // construction of ddl of days ( globalized )
            ddl_date_received_d.Items.Insert(0, Resources.Resource.txt_day);

            // construction of ddl of years ( globalized )
            ddl_date_received_y.Items.Insert(0, Resources.Resource.txt_year);



            // first we check if there's any property available
            if (home_count > 0)
            {
                int home_id = l.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));


                // if the referer is self i.e : from the respon redirect of the submit button
                //********************************************************************
                if (Request.UrlReferrer.ToString() == Request.Url.ToString())
                {
                    home_id = Convert.ToInt32(Session["home_id"]);
                }
                //********************************************************************



                tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ddl_home_list.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                ddl_home_list.DataBind();

                // if the referer is self i.e : from the respon redirect of the submit button
                //********************************************************************
                if (Request.UrlReferrer.ToString() == Request.Url.ToString())
                {
                    ddl_home_list.SelectedValue = Convert.ToString(Session["home_id"]);
                }
                //********************************************************************




                tiger.Rent hp = new tiger.Rent(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                gv_rent_delequency.DataSource = hp.getRentDelequencyList(Convert.ToInt32(Session["schema_id"]), home_id);

                gv_rent_delequency.Columns[1].HeaderText = Resources.Resource.gv_unit;
                gv_rent_delequency.Columns[2].HeaderText = Resources.Resource.gv_rent;
                gv_rent_delequency.Columns[3].HeaderText = Resources.Resource.gv_amount_paid;
                gv_rent_delequency.Columns[4].HeaderText = Resources.Resource.gv_amount_owed;
                gv_rent_delequency.Columns[5].HeaderText = Resources.Resource.gv_additional_amount;
                gv_rent_delequency.Columns[6].HeaderText = Resources.Resource.gv_due_date;
                //   gv_rent_delequency.Columns[7].HeaderText = Resources.Resource.gv_paid_date;
                gv_rent_delequency.Columns[7].HeaderText = Resources.Resource.lbl_date_received;
                gv_rent_delequency.DataBind();


                // dropdownlist rent frequency
                tiger.Home v = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ddl_rent_frequency.DataSource = v.getHomeUnitRentedSumList(Convert.ToInt32(Session["schema_id"]), home_id, right_now);
                ddl_rent_frequency.DataBind();

                int rent_frequency;
                // si mois invalide System.NullReferenceException, Object reference
                try
                {
                    rent_frequency = Convert.ToInt32(ddl_rent_frequency.SelectedItem.Value);
                }
                catch
                {


                    if (ddl_rent_frequency.Items.Count > 0)
                    {
                        rent_frequency = Convert.ToInt32(ddl_rent_frequency.SelectedItem.Value);
                    }
                    else
                        rent_frequency = Convert.ToInt32(Request.Form["ddl_rent_frequency"]);
                }

                // if the referer is self i.e : from the respon redirect of the submit button
                //********************************************************************
                if (Request.UrlReferrer.ToString() == Request.Url.ToString())
                {
                    rent_frequency = Convert.ToInt32(Session["rent_frequency"]);
                    ddl_rent_frequency.SelectedValue = Session["rent_frequency"].ToString();
                }
                //********************************************************************


                // here we get the list of rented unit (gridview of the unpaid rent for the current month )

                tiger.Home u = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                gv_rented_unit_list.DataSource = u.getHomeUnitUnpaidRentedList(Convert.ToInt32(Session["schema_id"]), home_id, rent_frequency, right_now);

                gv_rented_unit_list.Columns[1].HeaderText = Resources.Resource.gv_unit;
                gv_rented_unit_list.Columns[2].HeaderText = Resources.Resource.gv_rent;
                gv_rented_unit_list.Columns[3].HeaderText = Resources.Resource.gv_amount_paid;
                gv_rented_unit_list.Columns[4].HeaderText = "*";
                gv_rented_unit_list.Columns[5].HeaderText = Resources.Resource.gv_notes;
                gv_rented_unit_list.Columns[6].HeaderText = Resources.Resource.lbl_due_date;
                gv_rented_unit_list.Columns[7].HeaderText = Resources.Resource.lbl_date_received;
                // gv_rented_unit_list.Columns[8].HeaderText = Resources.Resource.gv_view_detail;
                gv_rented_unit_list.DataBind();

                 // here we get the list of paid rented unit (gridview of the paid rent for the current month )


                grid_rented_unit_list.DataSource = u.getHomeUnitUnpaidRentedList(Convert.ToInt32(Session["schema_id"]), home_id, rent_frequency, right_now);
                grid_rented_unit_list.DataBind();


                tiger.Home paid = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                gv_rented_paid_unit_list.DataSource = paid.getHomeUnitPaidRentedList(Convert.ToInt32(Session["schema_id"]), home_id, rent_frequency, right_now);

                gv_rented_paid_unit_list.Columns[0].HeaderText = Resources.Resource.gv_unit;
                gv_rented_paid_unit_list.Columns[1].HeaderText = Resources.Resource.gv_rent;
                gv_rented_paid_unit_list.Columns[2].HeaderText = Resources.Resource.gv_amount_paid;
                gv_rented_paid_unit_list.Columns[3].HeaderText = Resources.Resource.lbl_due_date;
                gv_rented_paid_unit_list.Columns[4].HeaderText = Resources.Resource.lbl_date_received;

                gv_rented_paid_unit_list.DataBind();


                //To view the address of the property



                // Rent due date

                tiger.Rent r = new tiger.Rent(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                // lbl_rent_due_date.Text = r.getRentCurrentDueDate(rent_frequency, right_now);

                // default value of Date received rent payment ( today )
                DateTime date_received = new DateTime();
                date_received = DateTime.Now;

                ddl_date_received_m.SelectedValue = date_received.Month.ToString();
                ddl_date_received_d.SelectedValue = date_received.Day.ToString();
                ddl_date_received_y.SelectedValue = date_received.Year.ToString();

                // if the referer is self i.e : from the respon redirect of the submit button
                //********************************************************************
                if (Request.UrlReferrer.ToString() == Request.Url.ToString())
                {
                    ddl_date_received_m.SelectedValue = Session["date_received_m"].ToString();
                    ddl_date_received_d.SelectedValue = Session["date_received_d"].ToString();
                    ddl_date_received_y.SelectedValue = Session["date_received_y"].ToString();
                }
                //********************************************************************


                /*   
                   Label9.Text=Convert.ToString(Session["schema_id"]);
                   Label3.Text= Convert.ToString(home_id);
                   Label4.Text = Convert.ToString(rent_frequency);
                   Label5.Text = Convert.ToString(right_now);
            
                */

            }

            else
            {
                // panel_home_unit_add.Visible = false;
                txt_message.InnerHtml = "<b><a href='home/home_add.aspx'>Please add a property</a></b>";
            }

            // if the gridview of delequency is empty then do not display 
            // the table in wich the gridview is
            if (gv_rent_delequency.Rows.Count == 0)
            {
                tb_delequency.Visible = false;
                lbl_delequency.Visible = false;
            }

            // if the gridview of unpaid rent is empty then do not display 
            // the table in wich the gridview is
            if (gv_rented_unit_list.Rows.Count == 0)
            {
                tb_rented_unit_list.Visible = false;
                Label1.Visible = false;
                lbl_late_rent.Visible = false;
                lbl_no_delequency.Visible = false;
            }

            if (ddl_rent_frequency.Items.Count == 0)
            {
                tb_paid_rent.Visible = false;
                lbl_u_paid_rent.Visible = false;
                ddl_rent_frequency.Visible = false;
            }
        }
        // if the referer is self i.e : from the respon redirect of the submit button
        //********************************************************************
        /*  if (Request.UrlReferrer.ToString() == Request.Url.ToString())
          {
              Session.Contents.Remove("home_id");
              Session.Contents.Remove("rent_frequency");
              Session.Contents.Remove("date_received_m");
              Session.Contents.Remove("date_received_d");
              Session.Contents.Remove("date_received_y");
          }
       */
        //********************************************************************



    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_list_SelectedIndexChanged(object sender, EventArgs e)
    {

        tb_delequency.Visible = true;
        lbl_delequency.Visible = true;

        tb_rented_unit_list.Visible = true;
        Label1.Visible = true;
        lbl_late_rent.Visible = true;
        lbl_no_delequency.Visible = true;

        tb_paid_rent.Visible = true;
        lbl_u_paid_rent.Visible = true;
        ddl_rent_frequency.Visible = true;

        // if value = 0 form was not submit
        h_btn_submit.Value = "0";

        DateTime right_now = new DateTime();
        right_now = DateTime.Now;

        tiger.Date d = new tiger.Date();

        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        // right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        // right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));

        // tiger.Date d = new tiger.Date();

        //  the_date = 
        right_now = Convert.ToDateTime(d.DateCulture(ddl_date_received_m.SelectedValue, ddl_date_received_d.SelectedValue, ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        //rhome_view.Visible = true;
        gv_rented_unit_list.Visible = true;
        int home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        //delequency list for a home
        tiger.Rent hp = new tiger.Rent(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_rent_delequency.DataSource = hp.getRentDelequencyList(Convert.ToInt32(Session["schema_id"]), home_id);
        gv_rent_delequency.DataBind();



        // dropdownlist rent frequency
        tiger.Home v = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ddl_rent_frequency.DataSource = v.getHomeUnitRentedSumList(Convert.ToInt32(Session["schema_id"]), home_id, right_now);
        ddl_rent_frequency.DataBind();



        // here we check if there's any unit in this property
        tiger.Unit uc = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        int unit_count = uc.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);





        if (unit_count > 0)
        // here we get the list of unit that were already added 
        {
            int rent_frequency = 0;

            if (ddl_rent_frequency.Items.Count > 0)
            {
                rent_frequency = Convert.ToInt32(ddl_rent_frequency.SelectedItem.Value);
            }


            tiger.Home u = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            // here we get the list of rented unit (gridview of the unpaid rent for the current month )
            gv_rented_unit_list.DataSource = u.getHomeUnitUnpaidRentedList(Convert.ToInt32(Session["schema_id"]), home_id, rent_frequency, right_now);
            gv_rented_unit_list.DataBind();

            grid_rented_unit_list.DataSource = u.getHomeUnitUnpaidRentedList(Convert.ToInt32(Session["schema_id"]), home_id, rent_frequency, right_now);
            grid_rented_unit_list.DataBind();

             // here we get the list of rented unit (gridview of the paid rent for the current month )

            tiger.Home paid = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_rented_paid_unit_list.DataSource = paid.getHomeUnitPaidRentedList(Convert.ToInt32(Session["schema_id"]), home_id, rent_frequency, right_now);
            gv_rented_paid_unit_list.EmptyDataText = Resources.Resource.gv_no_payment;
            gv_rented_paid_unit_list.Columns[0].HeaderText = Resources.Resource.gv_unit;
            gv_rented_paid_unit_list.Columns[1].HeaderText = Resources.Resource.gv_rent;
            gv_rented_paid_unit_list.Columns[2].HeaderText = Resources.Resource.gv_amount_paid;
            gv_rented_paid_unit_list.Columns[3].HeaderText = Resources.Resource.lbl_due_date;
            gv_rented_paid_unit_list.Columns[4].HeaderText = Resources.Resource.lbl_date_received;
            gv_rented_paid_unit_list.DataBind();


             tiger.Rent r = new tiger.Rent(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            //lbl_rent_due_date.Text = r.getRentCurrentDueDate(rent_frequency, right_now);


            // default value of Date received rent payment ( today )
            DateTime date_received = new DateTime();
            date_received = right_now;

            ddl_date_received_m.SelectedValue = date_received.Month.ToString();
            ddl_date_received_d.SelectedValue = date_received.Day.ToString();
            ddl_date_received_y.SelectedValue = date_received.Year.ToString();

        }
        else
        {
            gv_rented_unit_list.Visible = false;
        }

        if (gv_rent_delequency.Rows.Count == 0)
        {
            tb_delequency.Visible = false;
            lbl_delequency.Visible = false;
        }
        // if the gridview of unpaid rent is empty then do not display 
        // the table in wich the gridview is
        if (gv_rented_unit_list.Rows.Count == 0)
        {
            tb_rented_unit_list.Visible = false;
            Label1.Visible = false;
            lbl_late_rent.Visible = false;
            lbl_no_delequency.Visible = false;
        }

        if (ddl_rent_frequency.Items.Count == 0)
        {
            tb_paid_rent.Visible = false;
            lbl_u_paid_rent.Visible = false;
            ddl_rent_frequency.Visible = false;
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_rent_frequency_SelectedIndexChanged(object sender, EventArgs e)
    {


        tb_delequency.Visible = true;
        lbl_delequency.Visible = true;

        tb_rented_unit_list.Visible = true;
        Label1.Visible = true;
        lbl_late_rent.Visible = true;
        lbl_no_delequency.Visible = true;

        tb_paid_rent.Visible = true;
        lbl_u_paid_rent.Visible = true;
        ddl_rent_frequency.Visible = true;

        // if value = 0 form was not submit
        h_btn_submit.Value = "0";


        DateTime the_date = new DateTime();
        DateTime right_now = new DateTime();
        right_now = DateTime.Now;
        tiger.Date d = new tiger.Date();

        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        // right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));

        // tiger.Date d = new tiger.Date();

        //  the_date = 
        right_now = Convert.ToDateTime(d.DateCulture(ddl_date_received_m.SelectedValue, ddl_date_received_d.SelectedValue, ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        int home_id = Convert.ToInt32(ddl_home_list.SelectedValue);
        int rent_frequency = Convert.ToInt32(ddl_rent_frequency.SelectedValue);

        //delequency list for a home
        tiger.Rent hp = new tiger.Rent(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_rent_delequency.DataSource = hp.getRentDelequencyList(Convert.ToInt32(Session["schema_id"]), home_id);
        gv_rent_delequency.DataBind();

        tiger.Home u = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


        // here we get the list of rented unit (gridview of the unpaid rent for the current month )
        gv_rented_unit_list.DataSource = u.getHomeUnitUnpaidRentedList(Convert.ToInt32(Session["schema_id"]), home_id, rent_frequency, right_now);
        gv_rented_unit_list.DataBind();



        grid_rented_unit_list.DataSource = u.getHomeUnitUnpaidRentedList(Convert.ToInt32(Session["schema_id"]), home_id, rent_frequency, right_now);
        grid_rented_unit_list.DataBind();
 

        // here we get the list of rented unit (gridview of the paid rent for the current month )
        tiger.Home paid = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_rented_paid_unit_list.DataSource = paid.getHomeUnitPaidRentedList(Convert.ToInt32(Session["schema_id"]), home_id, rent_frequency, right_now);
        gv_rented_paid_unit_list.EmptyDataText = Resources.Resource.gv_no_payment;
        gv_rented_paid_unit_list.DataBind();

  
        tiger.Rent r = new tiger.Rent(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        //lbl_rent_due_date.Text = r.getRentCurrentDueDate(rent_frequency, right_now);

        // default value of Date received rent payment ( today )
        DateTime date_received = new DateTime();
        date_received = right_now;

        ddl_date_received_m.SelectedValue = date_received.Month.ToString();
        ddl_date_received_d.SelectedValue = date_received.Day.ToString();
        ddl_date_received_y.SelectedValue = date_received.Year.ToString();

        if (gv_rent_delequency.Rows.Count == 0)
        {
            tb_delequency.Visible = false;
            lbl_delequency.Visible = false;
        }
        // if the gridview of unpaid rent is empty then do not display 
        // the table in wich the gridview is
        if (gv_rented_unit_list.Rows.Count == 0)
        {
            tb_rented_unit_list.Visible = false;
            Label1.Visible = false;
            lbl_late_rent.Visible = false;
            lbl_no_delequency.Visible = false;
        }

        if (ddl_rent_frequency.Items.Count == 0)
        {
            tb_paid_rent.Visible = false;
        }

        if (ddl_rent_frequency.Items.Count == 0)
        {
            tb_paid_rent.Visible = false;
            lbl_u_paid_rent.Visible = false;
            ddl_rent_frequency.Visible = false;
        }


    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    protected string Get_UnpaidRent_Month(int rl_id)
    {
        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prRentDueDate", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        DateTime date = new DateTime();
        if (!Page.IsPostBack)
        {
            date = DateTime.Now;

            tiger.Date d = new tiger.Date();

            // if the referer is self i.e : from the respon redirect of the submit button
            //********************************************************************
            if (Request.UrlReferrer.ToString() == Request.Url.ToString())
            {
                date = Convert.ToDateTime(d.DateCulture(Session["date_received_m"].ToString(), Session["date_received_d"].ToString(), Session["date_received_y"].ToString(), Convert.ToString(Session["_lastCulture"])));
            }
            //********************************************************************





            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@rl_id", SqlDbType.Int).Value = rl_id;
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = date;
                cmd.Parameters.Add("@due_date", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;

                //execute the insert
                cmd.ExecuteReader();
                date = Convert.ToDateTime(cmd.Parameters["@due_date"].Value);
            }
            finally
            {
                conn.Close();
            }
        }
        else
        {
            try
            {

                date = DateTime.Now;
                tiger.Date d = new tiger.Date();

                conn.Open();
                //Add the params
                cmd.Parameters.Add("@rl_id", SqlDbType.Int).Value = rl_id;
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_date_received_m.SelectedValue, ddl_date_received_d.SelectedValue, ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
                cmd.Parameters.Add("@due_date", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;

                //execute the insert
                cmd.ExecuteReader();
                date = Convert.ToDateTime(cmd.Parameters["@due_date"].Value);
            }
            finally
            {
                conn.Close();
            }
        }
        return date.Month.ToString();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    protected string Get_UnpaidRent_Day(int rl_id)
    {
        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prRentDueDate", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        DateTime date = new DateTime();
        if (!Page.IsPostBack)
        {
            date = DateTime.Now;

            tiger.Date d = new tiger.Date();

            // if the referer is self i.e : from the respon redirect of the submit button
            //********************************************************************
            if (Request.UrlReferrer.ToString() == Request.Url.ToString())
            {
                date = Convert.ToDateTime(d.DateCulture(Session["date_received_m"].ToString(), Session["date_received_d"].ToString(), Session["date_received_y"].ToString(), Convert.ToString(Session["_lastCulture"])));
            }
            //********************************************************************


            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@rl_id", SqlDbType.Int).Value = rl_id;
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = date;
                cmd.Parameters.Add("@due_date", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;

                //execute the insert
                cmd.ExecuteReader();
                date = Convert.ToDateTime(cmd.Parameters["@due_date"].Value);
            }
            finally
            {
                conn.Close();
            }
        }
        else
        {
            try
            {

                date = DateTime.Now;
                tiger.Date d = new tiger.Date();
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@rl_id", SqlDbType.Int).Value = rl_id;
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_date_received_m.SelectedValue, ddl_date_received_d.SelectedValue, ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
                cmd.Parameters.Add("@due_date", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;

                //execute the insert
                cmd.ExecuteReader();
                date = Convert.ToDateTime(cmd.Parameters["@due_date"].Value);
            }
            finally
            {
                conn.Close();
            }
        }
        return date.Day.ToString();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>


    protected string Get_UnpaidRent_Year(int rl_id)
    {
        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prRentDueDate", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        DateTime date = new DateTime();
        if (!Page.IsPostBack)
        {
            date = DateTime.Now;

            tiger.Date d = new tiger.Date();

            // if the referer is self i.e : from the respon redirect of the submit button
            //********************************************************************
            if (Request.UrlReferrer.ToString() == Request.Url.ToString())
            {
                date = Convert.ToDateTime(d.DateCulture(Session["date_received_m"].ToString(), Session["date_received_d"].ToString(), Session["date_received_y"].ToString(), Convert.ToString(Session["_lastCulture"])));
            }
            //********************************************************************


            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@rl_id", SqlDbType.Int).Value = rl_id;
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = date;
                cmd.Parameters.Add("@due_date", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;

                //execute the insert
                cmd.ExecuteReader();
                date = Convert.ToDateTime(cmd.Parameters["@due_date"].Value);
            }
            finally
            {
                conn.Close();
            }
        }
        else
        {
            try
            {

                date = DateTime.Now;
                conn.Open();
                //Add the params
                tiger.Date d = new tiger.Date();

                //Add the params
                cmd.Parameters.Add("@rl_id", SqlDbType.Int).Value = rl_id;
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_date_received_m.SelectedValue, ddl_date_received_d.SelectedValue, ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
                cmd.Parameters.Add("@due_date", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;

                //execute the insert
                cmd.ExecuteReader();
                date = Convert.ToDateTime(cmd.Parameters["@due_date"].Value);
            }
            finally
            {
                conn.Close();
            }
        }
        return date.Year.ToString();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="rl_id"></param>

    /// <returns></returns>

    protected string Get_UnpaidRent_Due_Date(int rl_id)
    {

        String due_date = "";
        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prRentDueDate", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        DateTime date = new DateTime();
        if (!Page.IsPostBack)
        {
            date = DateTime.Now;

            tiger.Date d = new tiger.Date();

            // if the referer is self i.e : from the respon redirect of the submit button
            //********************************************************************
            if (Request.UrlReferrer.ToString() == Request.Url.ToString())
            {
                date = Convert.ToDateTime(d.DateCulture(Session["date_received_m"].ToString(), Session["date_received_d"].ToString(), Session["date_received_y"].ToString(), Convert.ToString(Session["_lastCulture"])));
            }
            //********************************************************************


            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@rl_id", SqlDbType.Int).Value = rl_id;
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = date;
                cmd.Parameters.Add("@due_date", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;

                //execute the insert
                cmd.ExecuteReader();
                date = Convert.ToDateTime(cmd.Parameters["@due_date"].Value);

                due_date = date.Month.ToString() + "/" + date.Day.ToString() + "/" + date.Year.ToString();
                // Label9.Text = Convert.ToString(rl_id);
            }
            finally
            {
                conn.Close();
            }
        }
        else
        {
            try
            {

                date = DateTime.Now;
                conn.Open();
                //Add the params
                tiger.Date d = new tiger.Date();
                //Add the params
                cmd.Parameters.Add("@rl_id", SqlDbType.Int).Value = rl_id;
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_date_received_m.SelectedValue, ddl_date_received_d.SelectedValue, ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
                cmd.Parameters.Add("@due_date", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;

                //execute the insert
                cmd.ExecuteReader();
                date = Convert.ToDateTime(cmd.Parameters["@due_date"].Value);
                due_date = date.Month.ToString() + "/" + date.Day.ToString() + "/" + date.Year.ToString();
                //  Label9.Text = Convert.ToString(rl_id);
            }
            finally
            {
                conn.Close();
            }
        }
        // return due_date; DataFormatString="{0:M-dd-yyyy}
        return String.Format("{0:MMM-dd-yyyy}", date);
    }

    protected string Get_CultureName()
    {
        return Session["_lastCulture"].ToString();
    }

    protected DateTime Get_UnpaidRent_Due_Date2(int rl_id)
    {

        String due_date = "";
        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prRentDueDate", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        DateTime date = new DateTime();
        if (!Page.IsPostBack)
        {
            date = DateTime.Now;

            tiger.Date d = new tiger.Date();

            // if the referer is self i.e : from the respon redirect of the submit button
            //********************************************************************
            if (Request.UrlReferrer.ToString() == Request.Url.ToString())
            {
                date = Convert.ToDateTime(d.DateCulture(Session["date_received_m"].ToString(), Session["date_received_d"].ToString(), Session["date_received_y"].ToString(), Convert.ToString(Session["_lastCulture"])));
            }
            //********************************************************************


            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@rl_id", SqlDbType.Int).Value = rl_id;
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = date;
                cmd.Parameters.Add("@due_date", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;

                //execute the insert
                cmd.ExecuteReader();
                date = Convert.ToDateTime(cmd.Parameters["@due_date"].Value);

                due_date = date.Month.ToString() + "/" + date.Day.ToString() + "/" + date.Year.ToString();
                // Label9.Text = Convert.ToString(rl_id);
            }
            finally
            {
                conn.Close();
            }
        }
        else
        {
            try
            {

                date = DateTime.Now;
                conn.Open();
                //Add the params
                tiger.Date d = new tiger.Date();
                //Add the params
                cmd.Parameters.Add("@rl_id", SqlDbType.Int).Value = rl_id;
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_date_received_m.SelectedValue, ddl_date_received_d.SelectedValue, ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
                cmd.Parameters.Add("@due_date", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;

                //execute the insert
                cmd.ExecuteReader();
                date = Convert.ToDateTime(cmd.Parameters["@due_date"].Value);
                due_date = date.Month.ToString() + "/" + date.Day.ToString() + "/" + date.Year.ToString();
                //  Label9.Text = Convert.ToString(rl_id);
            }
            finally
            {
                conn.Close();
            }
        }
        // return due_date; DataFormatString="{0:M-dd-yyyy}
        return  date ;
    }

    protected string Set_Delequency_Month()
    {
        DateTime right_now = new DateTime();
        right_now = DateTime.Now;

        return right_now.Month.ToString();
    }


    protected string Set_Delequency_Day()
    {
        DateTime right_now = new DateTime();
        right_now = DateTime.Now;
        return right_now.Day.ToString();
    }


    protected string Set_Delequency_Year()
    {
        DateTime right_now = new DateTime();
        right_now = DateTime.Now;
        return right_now.Year.ToString();
    }
    // the amount owed ,in the GridView RentDelequency
    protected string RentDelequencyAmount(int rp_id)
    {

        tiger.Rent hp = new tiger.Rent(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        return String.Format("{0:0.00}", (hp.getRentDelequencyAmount(rp_id) * -1));


    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        // if value = 0 form was not submit
        h_btn_submit.Value = "1";

        // Creation of the session variable to be use in the page load after the submit event and the 
        // response redirect.
        // ...In the page load , if page is not postback and the urlreferer is self the use the session
        // variable to set the web control ( ddl & gridview etc... )
        Session["home_id"] = ddl_home_list.SelectedValue;
        Session["rent_frequency"] = ddl_rent_frequency.SelectedValue;
        Session["date_received_m"] = ddl_date_received_m.SelectedValue;
        Session["date_received_d"] = ddl_date_received_d.SelectedValue;
        Session["date_received_y"] = ddl_date_received_y.SelectedValue;




        String compare_rl_rent_amount = "";
        String rp_amount = "";
        String rp_paid_date = "";
        String rp_due_date = "";
        String tu_id = "";
        String rl_id = "";
        int number_of_insert = 0;
        String no_delinquent = "";
        String rp_notes = "";

        // here we are counting the number of row of the GridView


        for (int j = 0; j < gv_rented_unit_list.Rows.Count; j++)
        {

            CheckBox chk_process = (CheckBox)gv_rented_unit_list.Rows[j].FindControl("chk_process");
            // if the checkbox is checked the process
            if (chk_process.Checked)
            {


                CheckBox chk_no_delequency = (CheckBox)gv_rented_unit_list.Rows[j].FindControl("chk_no_delequency");
                TextBox tbx_rl_rent_amount = (TextBox)gv_rented_unit_list.Rows[j].FindControl("txtbx");
                Label rent_due_date = (Label)gv_rented_unit_list.Rows[j].FindControl("rent_due_date");


                TextBox tbx_notes = (TextBox)gv_rented_unit_list.Rows[j].FindControl("tbx_notes");

                DropDownList date_received_m = (DropDownList)gv_rented_unit_list.Rows[j].FindControl("ddl_unpaid_rent_m");
                DropDownList date_received_d = (DropDownList)gv_rented_unit_list.Rows[j].FindControl("ddl_unpaid_rent_d");
                DropDownList date_received_y = (DropDownList)gv_rented_unit_list.Rows[j].FindControl("ddl_unpaid_rent_y");

                HiddenField h_tu_id = (HiddenField)gv_rented_unit_list.Rows[j].FindControl("h_tu_id");
                HiddenField h_rl_id = (HiddenField)gv_rented_unit_list.Rows[j].FindControl("h_rl_id");
                HiddenField h_rl_rent_amount = (HiddenField)gv_rented_unit_list.Rows[j].FindControl("h_rl_rent_amount");

                if (chk_no_delequency.Checked)
                    no_delinquent = no_delinquent + "1|";
                else
                    no_delinquent = no_delinquent + "0|";



                rp_amount = rp_amount + tbx_rl_rent_amount.Text + "|";
                compare_rl_rent_amount = compare_rl_rent_amount + h_rl_rent_amount.Value + "|";
                rp_paid_date = rp_paid_date + date_received_m.SelectedValue + "/" + date_received_d.SelectedValue + "/" + date_received_y.SelectedValue + "|";
                rp_due_date = rp_due_date + rent_due_date.Text + "|";
                tu_id = tu_id + h_tu_id.Value + "|";
                rl_id = rl_id + h_rl_id.Value + "|";
                rp_notes = rp_notes + tbx_notes.Text + "|";

                number_of_insert++;
            }
        }


        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        //////////
        ////////// SECURITY CHECK  BEGIN //////////////
        /////////

        AccountObjectAuthorization rentAuthorization = new AccountObjectAuthorization(strconn);

        if (rentAuthorization.RentPaymentBatch(Convert.ToInt32(Session["schema_id"]), tu_id, rl_id, number_of_insert))
        {
        }
        else
        {
            Session.Abandon();
            Response.Redirect("http://www.sinfocatiger.com/login.aspx");
        }

        //////////
        ////////// SECURITY CHECK END //////////////
        /////////


        if (rl_id != "" && number_of_insert > 0) //
        {
            //Here we send the values to the stored procedure

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prRentPaymentBatchAdd", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
                // we replace the commas  by dots because SQL "CAST" will convert $100,00 by 100000.00
                // we cast the string "100,00" in money
                cmd.Parameters.Add("@rp_amount", SqlDbType.VarChar, 8000).Value = rp_amount.Replace(",", ".");
                cmd.Parameters.Add("@compare_rl_rent_amount", SqlDbType.VarChar, 8000).Value = compare_rl_rent_amount.Replace(",", ".");
                cmd.Parameters.Add("@rp_paid_date", SqlDbType.VarChar, 8000).Value = rp_paid_date;
                cmd.Parameters.Add("@rp_due_date", SqlDbType.VarChar, 8000).Value = rp_due_date;
                cmd.Parameters.Add("@tu_id", SqlDbType.VarChar, 8000).Value = tu_id;
                cmd.Parameters.Add("@rl_id", SqlDbType.VarChar, 8000).Value = rl_id;
                cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;
                cmd.Parameters.Add("@rp_no_delenquent", SqlDbType.VarChar, 8000).Value = no_delinquent;
                cmd.Parameters.Add("@rp_notes", SqlDbType.VarChar, 8000).Value = rp_notes;

                //execute the insert
                cmd.ExecuteReader();

            }
            finally
            {
                conn.Close();
            }


        }

        Response.Redirect("home_unit_rented_list.aspx");


    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name=_datew"e"></param>
    protected void ddl_date_received_m_SelectedIndexChanged(object sender, EventArgs e)
    {

        tb_delequency.Visible = true;
        lbl_delequency.Visible = true;


        tb_rented_unit_list.Visible = true;
        Label1.Visible = true;
        lbl_late_rent.Visible = true;
        lbl_no_delequency.Visible = true;

        tb_paid_rent.Visible = true;
        lbl_u_paid_rent.Visible = true;
        ddl_rent_frequency.Visible = true;

        int rent_frequency;

        string lang = "";
        DateTime the_date = new DateTime();

        int home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        //delequency list for a home
        tiger.Rent hp = new tiger.Rent(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_rent_delequency.DataSource = hp.getRentDelequencyList(Convert.ToInt32(Session["schema_id"]), home_id);
        gv_rent_delequency.DataBind();


        tiger.Date d = new tiger.Date();

        the_date = Convert.ToDateTime(d.DateCulture(ddl_date_received_m.SelectedValue, ddl_date_received_d.SelectedValue, ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        // dropdownlist rent frequency
        tiger.Home v = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ddl_rent_frequency.DataSource = v.getHomeUnitRentedSumList(Convert.ToInt32(Session["schema_id"]), home_id, the_date);
        ddl_rent_frequency.DataBind();


        // si mois invalide System.NullReferenceException, Object reference
        try
        {
            rent_frequency = Convert.ToInt32(ddl_rent_frequency.SelectedItem.Value);
        }
        catch
        {
            rent_frequency = Convert.ToInt32(Request.Form["ddl_rent_frequency"]);
        }


        // here we get the list of rented unit (gridview of the unpaid rent for the current month )

        tiger.Home u = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_rented_unit_list.DataSource = u.getHomeUnitUnpaidRentedList(Convert.ToInt32(Session["schema_id"]), home_id, rent_frequency, the_date);
        gv_rented_unit_list.DataBind();

        // here we get the list of paid rented unit (gridview of the paid rent for the current month )

        tiger.Home paid = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_rented_paid_unit_list.DataSource = paid.getHomeUnitPaidRentedList(Convert.ToInt32(Session["schema_id"]), home_id, rent_frequency, the_date);

        gv_rented_paid_unit_list.DataBind();


         // Rent due date

        tiger.Rent r = new tiger.Rent(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        //lbl_rent_due_date.Text = r.getRentCurrentDueDate(rent_frequency, the_date);

        // default value of Date received rent payment ( today )
        DateTime date_received = new DateTime();
        date_received = the_date;

        ddl_date_received_m.SelectedValue = date_received.Month.ToString();
        ddl_date_received_d.SelectedValue = date_received.Day.ToString();
        ddl_date_received_y.SelectedValue = date_received.Year.ToString();

        if (gv_rent_delequency.Rows.Count == 0)
        {
            tb_delequency.Visible = false;
            lbl_delequency.Visible = false;
        }
        // if the gridview of unpaid rent is empty then do not display 
        // the table in wich the gridview is
        if (gv_rented_unit_list.Rows.Count == 0)
        {
            tb_rented_unit_list.Visible = false;
            Label1.Visible = false;
            lbl_late_rent.Visible = false;
            lbl_no_delequency.Visible = false;
        }

        if (ddl_rent_frequency.Items.Count == 0)
        {
            tb_paid_rent.Visible = false;
            lbl_u_paid_rent.Visible = false;
            ddl_rent_frequency.Visible = false;
        }


    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_date_received_d_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_date_received_y_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btn_delequency_Click(object sender, EventArgs e)
    {
        Session["home_id"] = ddl_home_list.SelectedValue;
        Session["rent_frequency"] = ddl_rent_frequency.SelectedValue;
        Session["date_received_m"] = ddl_date_received_m.SelectedValue;
        Session["date_received_d"] = ddl_date_received_d.SelectedValue;
        Session["date_received_y"] = ddl_date_received_y.SelectedValue;

        String rd_amount = "";
        String rd_date = "";
        String rp_id = "";
        int number_of_insert = 0;


        for (int j = 0; j < gv_rent_delequency.Rows.Count; j++)
        {
            Label amount_owed = (Label)gv_rent_delequency.Rows[j].FindControl("rd_amount_owed");
            CheckBox chk_process_1 = (CheckBox)gv_rent_delequency.Rows[j].FindControl("chk_process_1");
            TextBox tbx_delequency = (TextBox)gv_rent_delequency.Rows[j].FindControl("tbx_delequency");

            // if the checkbox is checked  and the amount entered is less or equal then the amount owed, process
            if (chk_process_1.Checked && (Convert.ToDecimal(tbx_delequency.Text) <= Convert.ToDecimal(amount_owed.Text)))
            {

                DropDownList date_received_m = (DropDownList)gv_rent_delequency.Rows[j].FindControl("ddl_date_delequency_m");
                DropDownList date_received_d = (DropDownList)gv_rent_delequency.Rows[j].FindControl("ddl_date_delequency_d");
                DropDownList date_received_y = (DropDownList)gv_rent_delequency.Rows[j].FindControl("ddl_date_delequency_y");

                HiddenField h_rp_id = (HiddenField)gv_rent_delequency.Rows[j].FindControl("h_rp_id");

                rd_amount = rd_amount + tbx_delequency.Text + "|";
                rd_date = rd_date + date_received_m.SelectedValue + "/" + date_received_d.SelectedValue + "/" + date_received_y.SelectedValue + "|";
                rp_id = rp_id + h_rp_id.Value + "|";

                number_of_insert++;
            }
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        //////////
        ////////// SECURITY CHECK  BEGIN //////////////
        /////////

        AccountObjectAuthorization rentAuthorization = new AccountObjectAuthorization(strconn);

        if (!rentAuthorization.RentPaidBatch(Convert.ToInt32(Session["schema_id"]), rp_id, number_of_insert))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        //////////
        ////////// SECURITY CHECK END //////////////
        /////////

        if (rp_id != "" && number_of_insert > 0) //
        {
            //Here we send the values to the stored procedure

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prRentDelequencyBatchAdd", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;
                cmd.Parameters.Add("@rd_amount", SqlDbType.VarChar, 8000).Value = rd_amount.Replace(",", ".");
                cmd.Parameters.Add("@rd_date", SqlDbType.VarChar, 8000).Value = rd_date;
                cmd.Parameters.Add("@rp_id", SqlDbType.VarChar, 8000).Value = rp_id;

                //execute the insert
                cmd.ExecuteReader();

            }
            finally
            {
                conn.Close();
            }


        }
        Response.Redirect("home_unit_rented_list.aspx");

    }


 


}
