using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class home_name_view : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        ///////// SECUTITY ACCOUNT OBLECT AUTHORIZATION BEGIN declaration  /////////////////
        AccountObjectAuthorization nameAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ///////// SECUTITY ACCOUNT OBLECT AUTHORIZATION END   /////////////////

        int name_id = Convert.ToInt32(Request.QueryString["name_id"]);

        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!nameAuthorization.Home(Convert.ToInt32(Session["schema_id"]), name_id))
        {
            Session.Abandon();
            Response.Redirect("http://www.sinfocatiger.com/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
                
        if (tiger.security.Access.hasAccess("1100000000", Convert.ToInt32(Session["group_id"]), Convert.ToInt32(Session["schema_id"])))
        {
            tiger.Name n = new tiger.Name(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            txt_lside.InnerHtml = n.getNameView(Convert.ToInt32(Session["schema_id"]),name_id, Convert.ToChar(Session["user_lang"]));
            //check if person is manager
            txt_manager.InnerHtml = n.getManager(Convert.ToInt32(Session["schema_id"]), name_id);
            txt_owner.InnerHtml = n.getOwnerJanitorHome(Convert.ToInt32(Session["schema_id"]), name_id, 4);
      txt_janitor.InnerHtml = n.getOwnerJanitorHome(Convert.ToInt32(Session["schema_id"]), name_id, 2);
      
 
        }
        else
        {
            Response.Redirect(tiger.security.Access.toLoginPage());
        }
    }
}
