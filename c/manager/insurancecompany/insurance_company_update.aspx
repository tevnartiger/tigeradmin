<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="insurance_company_update.aspx.cs" Inherits="insurancecompany_insurance_company_update" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    INSURANCE COMPANY UPDATE<br /><br />
        <table bgcolor="#ffffcc" >
            <tr>
                <td >
                    name</td>
                <td  >
                    : 
                    <asp:TextBox ID="ic_name" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator 
                        ID="reg_ic_name" runat="server" 
                         ControlToValidate="ic_name"
                        ErrorMessage="invalid name">
                        </asp:RegularExpressionValidator>
                &nbsp;<asp:RequiredFieldValidator  ControlToValidate="ic_name"
                           ID="req_ic_name" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    website</td>
                <td >
                    : 
                    <asp:TextBox ID="ic_website" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_ic_website" runat="server" 
                         ControlToValidate="ic_website"
                        ErrorMessage="invalid link">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td  >
                    address </td>
                <td  >
                    :
                    <asp:TextBox ID="ic_addr" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_ic_addr" runat="server" 
                        ControlToValidate="ic_addr"
                        ErrorMessage="invalid address">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td >
                    telephone</td>
                <td >
                    :
                    <asp:TextBox ID="ic_tel" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator 
                        ID="reg_ic_tel" runat="server" 
                        ControlToValidate="ic_tel"
                        ErrorMessage="invalid telephone">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td >
                    country</td>
                <td >
                    :
                    <asp:DropDownList ID="ddl_ic_country_list" DataTextField="country_name" DataValueField="country_id" runat="server">
                    
                    </asp:DropDownList></td>
            </tr>
            
            <tr>
                <td  >
                    prov/state</td>
                <td  >
                    :
                    <asp:TextBox ID="ic_prov" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_ic_prov" runat="server" 
                        ControlToValidate="ic_prov"
                        ErrorMessage="enter letters only ">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td >
                    city</td>
                <td >
                    :
                    <asp:TextBox ID="ic_city" runat="server"></asp:TextBox>
                &nbsp;<asp:RegularExpressionValidator ID="reg_ic_city" runat="server" 
                        ControlToValidate="ic_city"
                        ErrorMessage="enter letters only">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td  >
                    postal code</td>
                <td  >
                    :
                    <asp:TextBox ID="ic_pc" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_ic_pc" runat="server" 
                        ControlToValidate="ic_pc"
                        ErrorMessage="invalid postal code">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td   >
                    contact first name</td>
                <td  >
                    :
                    <asp:TextBox ID="ic_contact_fname" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_ic_contact_fname" runat="server" 
                        ControlToValidate="ic_contact_fname"
                        ErrorMessage="invalid name">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td  >
                    contact last name</td>
                <td   >
                    :
                    <asp:TextBox ID="ic_contact_lname" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_ic_contact_lname" runat="server" 
                        ControlToValidate="ic_contact_lname"
                        ErrorMessage="invalid last name">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td  >
                    contact tel.</td>
                <td  >
                    :
                    <asp:TextBox ID="ic_contact_tel" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_ic_contact_tel" 
                        ControlToValidate="ic_contact_tel"
                        runat="server" ErrorMessage="invalid telephone">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td  >
                    contact fax.</td>
                <td  >
                    :
                    <asp:TextBox ID="ic_contact_fax" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_ic_contact_fax" 
                        ControlToValidate="ic_contact_fax"
                        runat="server" ErrorMessage="invalid fax">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            
            <tr>
                <td  >
                    contact email</td>
                <td  >
                    :
                    <asp:TextBox ID="ic_contact_email" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_ic_contact_email" 
                        ControlToValidate="ic_contact_email"
                        runat="server" ErrorMessage="invalid email">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td valign=top>
                    comments</td>
                <td>
                    
                    <asp:TextBox ID="ic_com" runat="server" Height="100px" TextMode="MultiLine" Width="300px"></asp:TextBox></td>
            </tr>
        
        </table>
        <br />
       
        <br />
        <asp:Button ID="btn_submit" runat="server" Text="submit"  OnClick="btn_submit_Click"/>
        &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <asp:Label ID="lbl" runat="server" ></asp:Label>
  
    <div id="result" runat=server></div>

    </asp:Content>
