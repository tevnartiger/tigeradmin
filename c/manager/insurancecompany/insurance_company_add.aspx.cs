using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class insurancecompany_insurance_company_add : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        reg_ic_city.ValidationExpression = RegEx.getText();
        reg_ic_addr.ValidationExpression = RegEx.getText();
        reg_ic_contact_email.ValidationExpression = RegEx.getEmail();
        reg_ic_contact_fname.ValidationExpression = RegEx.getText();
        reg_ic_contact_lname.ValidationExpression = RegEx.getText();
        reg_ic_contact_tel.ValidationExpression = RegEx.getText();
        reg_ic_name.ValidationExpression = RegEx.getText();
        reg_ic_pc.ValidationExpression = RegEx.getText();
        reg_ic_tel.ValidationExpression = RegEx.getText();
        reg_ic_website.ValidationExpression = RegEx.getText();
        reg_ic_prov.ValidationExpression = RegEx.getText();
        reg_ic_contact_fax.ValidationExpression = RegEx.getText();

        tiger.Country ic = new tiger.Country(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
       ddl_ic_country_list.DataSource = ic.getCountryList();
       ddl_ic_country_list.DataBind();
    }

    protected void btn_submit_Click(object sender, EventArgs e)
    {

        Page.Validate();
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prInsuranceCompanyAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.NVarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@ic_name", SqlDbType.NVarChar, 50).Value = ic_name.Text;
            cmd.Parameters.Add("@ic_website", SqlDbType.NVarChar, 200).Value = ic_website.Text;

            cmd.Parameters.Add("@ic_addr", SqlDbType.NVarChar, 50).Value = ic_addr.Text;
            cmd.Parameters.Add("@ic_city", SqlDbType.NVarChar, 50).Value = ic_city.Text;
            cmd.Parameters.Add("@ic_prov", SqlDbType.NVarChar, 50).Value = ic_prov.Text;
            cmd.Parameters.Add("@ic_pc", SqlDbType.NVarChar, 50).Value = ic_pc.Text;
            cmd.Parameters.Add("@ic_tel", SqlDbType.NVarChar, 50).Value = ic_tel.Text;
            cmd.Parameters.Add("@ic_contact_fname", SqlDbType.NVarChar, 50).Value = ic_contact_fname.Text;
            cmd.Parameters.Add("@ic_contact_lname", SqlDbType.NVarChar, 50).Value = ic_contact_lname.Text;
            cmd.Parameters.Add("@ic_contact_tel", SqlDbType.NVarChar, 50).Value = ic_contact_tel.Text;
            cmd.Parameters.Add("@ic_contact_email", SqlDbType.NVarChar, 50).Value = ic_contact_email.Text;
            cmd.Parameters.Add("@ic_contact_fax", SqlDbType.NVarChar, 50).Value = ic_contact_fax.Text;
            cmd.Parameters.Add("@country_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_ic_country_list.SelectedValue);
            cmd.Parameters.Add("@ic_com", SqlDbType.NText).Value = RegEx.getText(ic_com.Text);




            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 0)
                result.InnerHtml = " add successful";


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }
    }
}
