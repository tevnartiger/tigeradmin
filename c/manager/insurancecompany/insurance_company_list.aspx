<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="insurance_company_list.aspx.cs" Inherits="insurancecompany_insurance_company_list" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    INSURANCE COMPANY LIST<br /><br />
    <div>
    <asp:GridView ID="gv_ic_list" AutoGenerateColumns="false"
        runat="server"    AlternatingRowStyle-BackColor="#F0F0F6" 
      BorderColor="#CDCDCD"  BorderWidth="1"  Width="100%"
      HeaderStyle-BackColor="#F0F0F6" >
    <Columns>
     
    <asp:BoundField HeaderStyle-VerticalAlign="Top" HeaderText="Insurance company name"  DataField="ic_name" />
    <asp:BoundField HeaderStyle-VerticalAlign="Top" DataField="ic_addr" HeaderText="Address" />
    <asp:BoundField HeaderStyle-VerticalAlign="Top" DataField="ic_city" HeaderText="City" />
    <asp:BoundField HeaderStyle-VerticalAlign="Top" DataField="ic_contact_fname" HeaderText="Contact first name" />
    <asp:BoundField HeaderStyle-VerticalAlign="Top" DataField="ic_contact_lname" HeaderText="Contact last name" />
    <asp:BoundField HeaderStyle-VerticalAlign="Top" DataField="ic_contact_tel" HeaderText="Contact tel." />
    <asp:BoundField HeaderStyle-VerticalAlign="Top" DataField="ic_website" HeaderText="Website" />
    <asp:HyperLinkField HeaderStyle-VerticalAlign="Top"  HeaderText="View details"  Text="view"
     DataNavigateUrlFields="ic_id" 
     DataNavigateUrlFormatString="~/manager/insurancecompany/insurance_company_view.aspx?ic_id={0}" 
       />
    </Columns>    
    </asp:GridView><br /><br />
     <asp:HyperLink ID="link_main" runat="server" NavigateUrl="~/home/home_main.aspx">go back to main</asp:HyperLink>
               
    </div>
   </asp:Content>
