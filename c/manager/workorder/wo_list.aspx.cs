﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;


/// <summary>
/// Done by : Stanley Jocelyn
/// date    : march 6 , 2009
/// </summary>
///

public partial class manager_workorder_wo_list : BasePage
{
        protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DateTime to = new DateTime();
            DateTime from = new DateTime();
            to = DateTime.Now.AddDays(31); // the date in the to drop downlist


            DateTime today = DateTime.Today;
            DateTime lastDayOfThisMonth = new DateTime(today.Year, today.Month, 1).AddMonths(1).AddDays(-1);

            tiger.Date c = new tiger.Date();
            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime



            //if we are entering from the update page
            //-----------------------------------------------------
         
                // BY DEFAULT WE WANT TO SEE THE RENT THAT WAS PAID THIS MONTH , I.E: FROM THE 1 ST DAY OF MONTH TO TODAY
                from = Convert.ToDateTime(c.DateCulture("1", "1", to.Year.ToString()));

        
                // BY DEFAULT WE WANT TO SEE THE RENT THAT WAS PAID THIS MONTH , I.E: FROM THE 1 ST DAY OF MONTH TO TODAY
                to = Convert.ToDateTime(c.DateCulture(to.Month.ToString(), to.Day.ToString(), to.Year.ToString()));
           //-----------------------------------------------------


            ddl_from_m.SelectedValue = from.Month.ToString();
            ddl_from_d.SelectedValue = from.Day.ToString();
            ddl_from_y.SelectedValue = from.Year.ToString();

            ddl_to_m.SelectedValue = to.Month.ToString();
            ddl_to_d.SelectedValue = to.Day.ToString();
            ddl_to_y.SelectedValue = to.Year.ToString();

            // First we check if there's home available
            tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = h.getHomeCount(Convert.ToInt32(Session["schema_id"]));
           
            if (home_count > 0)
            {
                int home_id = h.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));
                // link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                //Session["schema_id"]));

                ddl_home_id.Visible = true;
                ddl_home_id.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                ddl_home_id.DataBind();
                ddl_home_id.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));


             

              // construction of the gridview of the rent payment archive
                tiger.WorkOrder v = new tiger.WorkOrder(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                gv_wo_list.DataSource = v.getWorkOrderList(Convert.ToInt32(Session["schema_id"]), 0,0,0,from, to);
                gv_wo_list.DataBind();
               
            }
              
        }

    }



     
    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {

            ddl_home_id.Visible = true;

         

                DateTime to = new DateTime();
                DateTime from = new DateTime();
                to = DateTime.Now.AddDays(31); // the date in the to drop downlist

                tiger.Date d = new tiger.Date();
                // BY DEFAULT WE WANT TO SEE THE RENT THAT WAS PAID THIS MONTH , I.E: FROM THE 1 ST DAY OF MONTH TO TODAY
                to = Convert.ToDateTime(d.DateCulture(to.Month.ToString(), to.Day.ToString(), to.Year.ToString()));
                from = Convert.ToDateTime(d.DateCulture("1", "1", to.Year.ToString()));


                // construction of the gridview for the payment archive
                tiger.WorkOrder v = new tiger.WorkOrder(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                gv_wo_list.DataSource = v.getWorkOrderList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(ddl_wo_status.SelectedValue),Convert.ToInt32(ddl_wo_priority.SelectedValue), from, to);
                gv_wo_list.DataBind();

                ddl_from_m.SelectedValue = from.Month.ToString();
                ddl_from_d.SelectedValue = from.Day.ToString();
                ddl_from_y.SelectedValue = from.Year.ToString();

                ddl_to_m.SelectedValue = to.Month.ToString();
                ddl_to_d.SelectedValue = to.Day.ToString();
                ddl_to_y.SelectedValue = to.Year.ToString();

         
        lbl_confirmation.Visible = false;
        lbl_home_name.Visible = false;

        lbl_title.Visible = false;
        lbl_date.Visible = false;

        Label2.Visible = false;
        Label3.Visible = false;
        Label4.Visible = false;
        Label5.Visible = false;
        Label6.Visible = false;
        Label7.Visible = false;

    }


    
    protected void Button1_Click(object sender, EventArgs e)
    {
       
        DateTime to = new DateTime();
        DateTime from = new DateTime();
        to = DateTime.Now; // the date in the to drop downlist

        tiger.Date d = new tiger.Date();
        to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue));
        from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue));


        // construction of the gridview for the payment archive
        tiger.WorkOrder v = new tiger.WorkOrder(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_wo_list.DataSource = v.getWorkOrderList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(ddl_wo_status.SelectedValue), Convert.ToInt32(ddl_wo_priority.SelectedValue), from, to);
        gv_wo_list.DataBind();

        lbl_confirmation.Visible = false;
        lbl_home_name.Visible = false;

        lbl_title.Visible = false;
        lbl_date.Visible = false;

        Label2.Visible = false;
        Label3.Visible = false;
        Label4.Visible = false;
        Label5.Visible = false;
        Label6.Visible = false;
        Label7.Visible = false;

    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_cancel_Click(object sender, EventArgs e)
    {

        //*********************************************************************
        //*********************************************************************
        Button btn_cancel = (Button)sender;
        GridViewRow grdRow = (GridViewRow)btn_cancel.Parent.Parent;
        string strField1 = grdRow.Cells[6].Text;

        HiddenField h_wo_id = (HiddenField)grdRow.Cells[6].FindControl("h_wo_id");
        HiddenField h_wo_title = (HiddenField)grdRow.Cells[6].FindControl("h_wo_title");
        HiddenField h_wo_date_begin = (HiddenField)grdRow.Cells[6].FindControl("h_wo_date_begin");
        HiddenField h_home_name = (HiddenField)grdRow.Cells[6].FindControl("h_home_name");

        //  lbl_confirmation.ForeColor = "Red";
        lbl_confirmation.Text = Resources.Resource.lbl_delete_confirmation;

        Label2.Text = Resources.Resource.lbl_property;
        Label4.Text = Resources.Resource.lbl_title;
        Label6.Text = Resources.Resource.lbl_date;

        Label3.Text = ":";
        Label5.Text = ":";
        Label7.Text = ":";

        lbl_title.Text = h_wo_title.Value;
        lbl_date.Text = h_wo_date_begin.Value;
        lbl_home_name.Text = h_home_name.Value;


        lbl_confirmation.Visible = true;
        lbl_home_name.Visible = true;

        lbl_title.Visible = true;
        lbl_date.Visible = true;

        Label2.Visible = true;
        Label3.Visible = true;
        Label4.Visible = true;
        Label5.Visible = true;
        Label6.Visible = true;
        Label7.Visible = true;



        //------------------------------------------------------------------------------------

        // check if the rp_id belong to the account
        AccountObjectAuthorization rpAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!rpAuthorization.WorkOrder(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(h_wo_id.Value)))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prWorkOrderDelete", conn);
        cmd.CommandType = CommandType.StoredProcedure;

       
             conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@wo_id", SqlDbType.Int).Value = Convert.ToInt32(h_wo_id.Value);

            //execute the insert
            cmd.ExecuteReader();

            conn.Close();

        //-------------------------------------------------------------------------------------

        DateTime to = new DateTime();
        DateTime from = new DateTime();

        tiger.Date d = new tiger.Date();
        to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue));
        from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue));


        // construction of the gridview for the payment archive
        tiger.WorkOrder v = new tiger.WorkOrder(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_wo_list.DataSource = v.getWorkOrderList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(ddl_wo_status.SelectedValue), Convert.ToInt32(ddl_wo_priority.SelectedValue), from, to);
        gv_wo_list.DataBind();


    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_wo_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        DateTime to = new DateTime();
        DateTime from = new DateTime();


        tiger.Date d = new tiger.Date();
        to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue));
        from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue));

        tiger.WorkOrder v = new tiger.WorkOrder(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        if (ddl_home_id.SelectedIndex == 0)
        {

            // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
            gv_wo_list.PageIndex = e.NewPageIndex;
            gv_wo_list.DataSource = v.getWorkOrderList(Convert.ToInt32(Session["schema_id"]), 0, Convert.ToInt32(ddl_wo_status.SelectedValue), Convert.ToInt32(ddl_wo_priority.SelectedValue), from, to);
            gv_wo_list.DataBind();

            
        }

        else
        {
            // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
            gv_wo_list.PageIndex = e.NewPageIndex;
            gv_wo_list.DataSource = v.getWorkOrderList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(ddl_wo_status.SelectedValue), Convert.ToInt32(ddl_wo_priority.SelectedValue), from, to);
            gv_wo_list.DataBind();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_wo_list_SelectedIndexChanged(object sender, EventArgs e)
    {
      
        DateTime to = new DateTime();
        DateTime from = new DateTime();


        tiger.Date d = new tiger.Date();
        to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue));
        from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue));

        tiger.WorkOrder v = new tiger.WorkOrder(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        if (ddl_home_id.SelectedIndex == 0)
        {

            // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
            gv_wo_list.DataSource = v.getWorkOrderList(Convert.ToInt32(Session["schema_id"]), 0, Convert.ToInt32(ddl_wo_status.SelectedValue), Convert.ToInt32(ddl_wo_priority.SelectedValue), from, to);
            gv_wo_list.DataBind();


        }

        else
        {
            // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
            gv_wo_list.DataSource = v.getWorkOrderList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(ddl_wo_status.SelectedValue), Convert.ToInt32(ddl_wo_priority.SelectedValue), from, to);
            gv_wo_list.DataBind();
        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetPriority(int priority_id)
    {
        string priority = "";

        switch (priority_id)
        {
            case 1:
                priority = Resources.Resource.lbl_urgent;
                break;
            case 2:
                priority = Resources.Resource.lbl_high;
                break;
            case 3:
                priority = Resources.Resource.lbl_medium;
                break;

            case 4:
                priority = Resources.Resource.lbl_low;
                break;
        }

        return priority;
    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetStatus(int status_id)
    {
        string status = "";

        switch (status_id)
        {
            case 1:
                status = Resources.Resource.lbl_pending;
                break;
            case 2:
                status = Resources.Resource.lbl_in_progress;
                break;
            case 3:
                status = Resources.Resource.lbl_completed;
                break;

            case 4:
                status = Resources.Resource.lbl_closed;
                break;
        }

        return status;
    }

  }
