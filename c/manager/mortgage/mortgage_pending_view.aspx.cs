﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : july 4 , 2008
/// </summary>
public partial class manager_mortgage_mortgage_pending_view :BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AccountObjectAuthorization mortgageAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        if (!Page.IsPostBack)
        {
            if (!RegEx.IsInteger(Request.QueryString["mortgage_id"]))
            {
                Session.Abandon();
                Response.Redirect("~/login.aspx");
            }

            //mortgage_add_link.NavigateUrl = "mortgage_add.aspx";
            //mortgage_update_link.NavigateUrl = "mortgage_update.aspx?mortgage_id=" + Request.QueryString["mortgage_id"];
            //  mortgage_renew_link.NavigateUrl = "mortgage_renew.aspx?mortgage_id=" + Request.QueryString["mortgage_id"];
            btn_delete.Enabled = false;

            int pf_id = 0;
            int pi_id = 0;


            DateTime date_begin = new DateTime();
            DateTime date_end = new DateTime();

            // Variable for the amortization table   
            //frequency at wich the mortgage is paid : Monthly, bi-weekly or weekly
            double frequency = 0;
            //Duration in year of the mortgage ( years of amortization)
            double duration = 0;
            double interest_rate = 0;
            double balance = 0;
            double interest_coumpounded = 0;
         


            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prMortgageView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params

            string referrer = "";

          //  referrer = Request.UrlReferrer.AbsolutePath.ToString();
              //if the url referer is from the mortgage renewal
         /*   if (referrer == "/sinfo/manager/mortgage/mortgage_renew.aspx" || referrer == "/manager/mortgage/mortgage_renew.aspx")
            {

                
                ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                if (!mortgageAuthorization.Mortgage(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["pending_mortgage_id"])))
                {
                    Session.Abandon();
                    Response.Redirect("~/login.aspx");
                }
                ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
        
                cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("mortgage_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["pending_mortgage_id"]);

                // store the pending mortgage value in a hiddenfield
                h_mortgage_pending_id.Value = Request.QueryString["pending_mortgage_id"];
            }
            else
            */
            {

                ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                if (!mortgageAuthorization.Mortgage(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["mortgage_id"])))
                {
                    Session.Abandon();
                    Response.Redirect("~/login.aspx");
                }
                ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
        
                cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("mortgage_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["mortgage_id"]);

            }


                //  try
            {
                conn.Open();
                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                    lbl_home_name.Text = dr["home_name"].ToString();
                    lbl_home_name2.Text = lbl_home_name.Text;

                    // tiger.Mortgage hv = new tiger.Mortgage(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

                    // gv_mortgage_home_archive_list.DataSource = hv.getMortgageHomeArchiveList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(dr["home_id"]));
                    // gv_mortgage_home_archive_list.DataBind();


                    lbl_fi_name.Text = dr["fi_name"].ToString();


                    if (dr["mortgage_loan_amount"] == DBNull.Value)
                    {
                        lbl_mortgage_loan_amount.Text = String.Format("{0:0.00}", 0.00);
                        //***
                        balance = 0;
                    }
                    else
                    {
                        lbl_mortgage_loan_amount.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mortgage_loan_amount"]));
                        //***
                        balance = Convert.ToDouble(dr["mortgage_loan_amount"]);
                    }


                    if (dr["mortgage_payment_amount"] == DBNull.Value)
                        lbl_mortgage_payment_amount.Text = String.Format("{0:0.00}", 0.00);
                    else
                        lbl_mortgage_payment_amount.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["mortgage_payment_amount"]));




                    //***
                    interest_rate = Convert.ToDouble(dr["mortgage_interest_rate"]);
                    lbl_mortgage_interest_rate.Text = String.Format("{0:0.00}", interest_rate);


                    lbl_mortgage_term.Text = dr["mortgage_term"].ToString();
                    duration = Convert.ToDouble(dr["mortgage_term"]);

                    lbl_mortgage_current_term.Text = dr["mortgage_current_term"].ToString();

                    //***



                    date_begin = Convert.ToDateTime(dr["mortgage_date_begin"]);
                    lbl_mortgate_date_begin.Text = date_begin.Month.ToString() + " / " + date_begin.Day.ToString() + " / " + date_begin.Year.ToString();


                    if (dr["mortgage_date_end"] == DBNull.Value)
                    {
                        lbl_mortgate_date_end.Text = "";
                    }
                    else
                    {
                        date_end = Convert.ToDateTime(dr["mortgage_date_end"]);
                        lbl_mortgate_date_end.Text = date_end.Month.ToString() + " / " + date_end.Day.ToString() + " / " + date_end.Year.ToString();
                    }

                    pf_id = Convert.ToInt32(dr["pf_id"]);
                    pi_id = Convert.ToInt32(dr["pi_id"]);

                }

                if (pf_id == 12)
                {
                    lbl_pf_categ.Text = Resources.Resource.lbl_monthly;
                    frequency = 12;
                }

                if (pf_id == 24)
                {
                    lbl_pf_categ.Text = Resources.Resource.txt_bi_weekly;
                    frequency = 24;
                }

                if (pf_id == 241)
                {
                    lbl_pf_categ.Text = Resources.Resource.txt_acc_bi_weekly;
                    frequency = 241;
                }

                if (pf_id == 52)
                {
                    lbl_pf_categ.Text = Resources.Resource.txt_weekly;
                    frequency = 52;
                }

                if (pf_id == 521)
                {
                    lbl_pf_categ.Text = Resources.Resource.txt_acc_weekly;
                    frequency = 521;
                }


                if (pi_id == 12)
                {
                    lbl_pi_categ.Text = Resources.Resource.txt_bi_yearly;
                    interest_coumpounded = 12;
                }
                if (pi_id == 6)
                {
                    lbl_pi_categ.Text = Resources.Resource.txt_bi_yearly;
                    interest_coumpounded = 6;
                }


            }
            ///////////////////////////////////////////////////////////////////////////////////////////////////////

            tiger.Amortization hp = new tiger.Amortization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            // gv_amortization.DataSource = hp.getAmortizationCanTable(date_begin, balance, duration, frequency, interest_rate, 0.00, 0.00, 1);
            // gv_amortization.DataBind();




            // if the interest is compounded each year we use The America Amortization ( also Euro )


            if (interest_coumpounded == 12)
            {
                gv_amortization.DataSource = hp.getAmortizationPartialTable(date_begin, date_end, balance, duration, frequency, interest_rate, 0.00, 0.00, 1);
                gv_amortization.DataBind();

                // All the calculation of payment, interest, 50/50 point, end point, total payments, total interest etc...

                /*
                 r_amortizationCalculation.DataSource = hv.getAmortizationCalculation(from, balance, duration, 12.00, interest, 0.00,
                                                         0.00, Convert.ToInt32(ddl_eachYear.SelectedValue));
                 r_amortizationCalculation.DataBind();

                 // if the mortgage is paid by-weekly or weekly show the values and get the savigs repeater visible or of we add an additional payment
                 if (ddl_mortgage_paid_frequency.SelectedValue == "241" || ddl_mortgage_paid_frequency.SelectedValue == "521" || ddl_mortgage_paid_frequency.SelectedValue == "24" || ddl_mortgage_paid_frequency.SelectedValue == "52" || Convert.ToDouble(tbx_additionnal_eachYear.Text) > 0.00 || Convert.ToInt32(tbx_additionnal_payment.Text) > 0.00)
                 {
                     //All the calculation of payment, interest, 50/50 point, end point,total payments, total interest etc...
                     r_amortizationExtraCalculation.DataSource = hv.getAmortizationCalculation(from, balance, duration, frequency, interest, Convert.ToDouble(tbx_additionnal_payment.Text),
                                                            Convert.ToDouble(tbx_additionnal_eachYear.Text), Convert.ToInt32(ddl_eachYear.SelectedValue));
                     r_amortizationExtraCalculation.DataBind();
                 }
                */

            }




            // if the interest is compounded twice year we use The Canadian Amortization
            if (interest_coumpounded == 6)
            {
                gv_amortization.DataSource = hp.getAmortizationPartialCanTable(date_begin, date_end, balance, duration, frequency, interest_rate, 0.00, 0.00, 1); ;
                gv_amortization.DataBind();


                // All the calculation of Canadian payment, interest, 50/50 point, end point, total payments, total interest etc...
                /* 
                r_amortizationCalculation.DataSource = hv.getAmortizationCanCalculation(from, balance, duration, 12.00, interest, 0.00,
                                                                    0.00, Convert.ToInt32(ddl_eachYear.SelectedValue));
               r_amortizationCalculation.DataBind();

               // if the mortgage is paid by-weekly or weekly  show the values and get the savigs repeater visible or additional payments
                           
              if (ddl_mortgage_paid_frequency.SelectedValue == "241" || ddl_mortgage_paid_frequency.SelectedValue == "521" || ddl_mortgage_paid_frequency.SelectedValue == "24" || ddl_mortgage_paid_frequency.SelectedValue == "52" || Convert.ToDouble(tbx_additionnal_eachYear.Text) > 0.00 || Convert.ToInt32(tbx_additionnal_payment.Text) > 0.00)
              {


               // All the calculation of payment, interest, 50/50 point, end point, total payments, total interest etc...
               r_amortizationExtraCalculation.DataSource = hv.getAmortizationCanCalculation(from, balance, duration, frequency, interest, Convert.ToDouble(tbx_additionnal_payment.Text),
                                                                          Convert.ToDouble(tbx_additionnal_eachYear.Text), Convert.ToInt32(ddl_eachYear.SelectedValue));
               r_amortizationExtraCalculation.DataBind();
               }
              */
            }




            ///////////////////////////////////////////////////////////////////////////////////////////////////////////




        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chk_enable_button_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_enable_button.Checked == true)
            btn_delete.Enabled = true;
        else
            btn_delete.Enabled = false;
    }
    /// <summary>
    /// /
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_delete_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prMortgageDelete", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();


            if (h_mortgage_pending_id.Value != "")
            {

                cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("mortgage_id", SqlDbType.Int).Value = Convert.ToInt32(h_mortgage_pending_id.Value);
           
            }
            else
            {

                cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("mortgage_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["mortgage_id"]);
            }
            //execute the insert
            cmd.ExecuteReader();



        }
        catch (Exception error)
        {
           // tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
           // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }


        if (h_mortgage_pending_id.Value != "" )
        {
            // this mortgage_id is the current mortgage received
            // from the "mortgage_renew.aspx" page


            string url = "mortgage_renew.aspx?mortgage_id=" + Request.QueryString["mortgage_id"];

           // after the deletion of the pending mortgage we go back to renew the current mortgag
            // in the "mortgage_renew.aspx" page

            Response.Redirect(url);
        }

       // this is the case where the urlreferrer is the "mortgage_list.aspx" page,
            // ... from the pending mortgage tab
        else
        {
          
            string url = "mortgage_list.aspx";
            Response.Redirect(url);
        }
    }
}
