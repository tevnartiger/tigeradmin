﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn   
/// Date    : July 3, 2008
/// </summary>
public partial class manager_mortgage_mortgage_renew : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!RegEx.IsInteger(Request.QueryString["mortgage_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }


        reg_fi_city.ValidationExpression = RegEx.getText();
        reg_fi_addr.ValidationExpression = RegEx.getText();
        reg_fi_contact_email.ValidationExpression = RegEx.getEmail();
        reg_fi_contact_fname.ValidationExpression = RegEx.getText();
        reg_fi_contact_lname.ValidationExpression = RegEx.getText();
        reg_fi_contact_tel.ValidationExpression = RegEx.getText();
        reg_fi_name.ValidationExpression = RegEx.getText();
        reg_fi_pc.ValidationExpression = RegEx.getText();
        reg_fi_tel.ValidationExpression = RegEx.getText();
        reg_fi_website.ValidationExpression = RegEx.getText();
        reg_fi_prov.ValidationExpression = RegEx.getText();
        reg_fi_contact_fax.ValidationExpression = RegEx.getText();

        reg_mortgage_term.ValidationExpression = RegEx.getNumeric();
        reg_mortgage_current_term.ValidationExpression = RegEx.getNumeric();
        reg_mortgage_loan_amount.ValidationExpression = RegEx.getMoney();
        reg_mortgage_payment_amount.ValidationExpression = RegEx.getMoney();
        reg_mortgage_interest_rate.ValidationExpression = RegEx.getMoney();

        AccountObjectAuthorization mortgageAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!mortgageAuthorization.Mortgage(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["mortgage_id"])))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
        

        if (!Page.IsPostBack)
        {

            // ajoter le datareader

            // the table with the message "there's alredy a pending mortgage with the link to the mortgage page"
            tb_already_pending_mortgage.Visible = false;


            // home address
            tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_home_list.DataSource = hv.getHomeList(Convert.ToInt32(Session["schema_id"]));
            ddl_home_list.DataBind();
            //  ddl_home_list.Items.Insert(0,"home");


            tiger.Country ddl_fi = new tiger.Country(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_fi_country_list.DataSource = ddl_fi.getCountryList();
            ddl_fi_country_list.DataBind();

            tiger.FinancialInstitution fi = new tiger.FinancialInstitution(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_fi_list.DataSource = fi.getFinancialInstitutionList(Convert.ToInt32(Session["schema_id"]));
            ddl_fi_list.DataBind();
            //  ddl_fi_list.Items.Insert(0,"financial institution");

            panel_fi.Visible = false;

            lbl_link_pending_mortgage.Visible = false;
            lbl_already_pending_mortgage.Visible = false;
           //  link_pending_mortgage.NavigateUrl = "mortgage_pending_view.aspx?mortgage_id=" + Request.QueryString["mortgage_id"];
                    


            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prMortgageView", conn);
            SqlCommand cmd2 = new SqlCommand("prMortgagePendingId", conn);


            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("mortgage_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["mortgage_id"]);
            cmd.Parameters.Add("@pending_mortgage_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            try
            {
                conn.Open();
                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);


             
                
                while (dr.Read() == true)
                {
                    ddl_home_list.SelectedValue = Convert.ToString(dr["home_id"]);

                    ddl_home_list.Enabled = false;
                    ddl_fi_list.SelectedValue = Convert.ToString(dr["fi_id"]);
                  //  mortgage_loan_amount.Text = dr["mortgage_loan_amount"].ToString();
                  //  mortgage_payment_amount.Text = dr["mortgage_payment_amount"].ToString();
                  //  mortgage_interest_rate.Text = dr["mortgage_interest_rate"].ToString();
                  //  mortgage_term.Text = dr["mortgage_term"].ToString();

                  //  mortgage_current_term.Text = dr["mortgage_current_term"].ToString();

                    DateTime date_begin = new DateTime();
                    DateTime date_end = new DateTime();

                    if (dr["mortgage_date_end"] == DBNull.Value)
                    {
                        lbl_mortgate_date_end.Text = "";
                    }
                    else
                    {
                        date_end = Convert.ToDateTime(dr["mortgage_date_end"]);

                        date_end = date_end.AddDays(1);
                        lbl_mortgate_date_end.Text = date_end.Month.ToString() + " / " + date_end.Day.ToString() + " / " + date_end.Year.ToString();
                    }

                   // if (date_begin != null)
                    {
                        /// date_begin = Convert.ToDateTime(dr["mortgage_date_begin"]);
                        
                        ddl_mortgate_date_begin_y.SelectedValue = date_end.Year.ToString();
                        ddl_mortgate_date_begin_d.SelectedValue = date_end.Day.ToString();
                        ddl_mortgate_date_begin_m.SelectedValue = date_end.Month.ToString();


                         h_date_begin_y.Value = date_end.Year.ToString();
                         h_date_begin_d.Value = date_end.Day.ToString();
                         h_date_begin_m.Value = date_end.Month.ToString();
                    }

                    if (date_end >= DateTime.Now)
                    {
                     //btn_submit.Enabled = false;
                    }

                    ddl_pf_list.SelectedValue = dr["pf_id"].ToString();
                    ddl_pi_list.SelectedValue = dr["pi_id"].ToString();                  

                }
            }

            
            finally
            {
              //  conn.Close();
            }




            cmd2.CommandType = CommandType.StoredProcedure;

            int pending_mortgage_id = 0;

            try
            {
               // conn.Open();
                //Add the params
                cmd2.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd2.Parameters.Add("mortgage_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["mortgage_id"]);
                cmd2.Parameters.Add("@pending_mortgage_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            
                //execute the insert
                cmd2.ExecuteReader();
                pending_mortgage_id  = Convert.ToInt32(cmd2.Parameters["@pending_mortgage_id"].Value);
            }
            finally
            {
                conn.Close();
            }

         

            if (pending_mortgage_id > 0)
            {
                tb_mortgage_renew.Visible = false;
                btn_submit.Enabled = false;
                lbl_link_pending_mortgage.NavigateUrl = "mortgage_pending_view.aspx?mortgage_id=" + Request.QueryString["mortgage_id"]
                                                    + "&pending_mortgage_id=" + pending_mortgage_id.ToString();
                lbl_link_pending_mortgage.Visible = true;
                lbl_already_pending_mortgage.Visible = true;


                tb_already_pending_mortgage.Visible = true ;

                // disable the controls if there's a pending mortgage
                mortgage_loan_amount.Enabled = false;
                mortgage_payment_amount.Enabled = false;
                mortgage_interest_rate.Enabled = false;
                mortgage_term.Enabled = false;
                mortgage_current_term.Enabled = false;
                //ddl_fi.Enabled = false;
                ddl_mortgate_date_begin_d.Enabled = false;
                ddl_mortgate_date_begin_m.Enabled = false;
                ddl_mortgate_date_begin_y.Enabled = false;
                ddl_fi_list.Enabled = false;
                ddl_pf_list.Enabled = false;
                ddl_pi_list.Enabled = false;
                chk_fi_add.Visible = false;
            }






        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        Page.Validate("vg_mortgage");
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }


        DateTime min_date_begin = new DateTime();
        DateTime new_date_begin = new DateTime();
        

        tiger.Date df = new tiger.Date();

        new_date_begin =  Convert.ToDateTime(df.DateCulture(ddl_mortgate_date_begin_m.SelectedValue, ddl_mortgate_date_begin_d.SelectedValue, ddl_mortgate_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        min_date_begin = Convert.ToDateTime(df.DateCulture(h_date_begin_m.Value, h_date_begin_d.Value, h_date_begin_y.Value, Convert.ToString(Session["_lastCulture"])));

      // if the date of the renewal is greater or equal than the minimum date begin then enter the mortgage 
      // renewal into the database

      if(min_date_begin <= new_date_begin)
      {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prMortgageRenew", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params

            cmd.Parameters.Add("@update_successful", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@current_mortgage_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["mortgage_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.NVarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_list.SelectedValue);
            cmd.Parameters.Add("@fi_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_fi_list.SelectedValue);
            cmd.Parameters.Add("@mortgage_loan_amount", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(mortgage_loan_amount.Text));
            cmd.Parameters.Add("@mortgage_payment_amount", SqlDbType.Money).Value = Convert.ToDecimal(mortgage_payment_amount.Text);

            cmd.Parameters.Add("@mortgage_interest_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(mortgage_interest_rate.Text));
            cmd.Parameters.Add("@mortgage_term", SqlDbType.Int).Value = Convert.ToInt32(RegEx.getInteger(mortgage_term.Text));
            cmd.Parameters.Add("@mortgage_current_term", SqlDbType.Int).Value = Convert.ToInt32(RegEx.getInteger(mortgage_current_term.Text));

            cmd.Parameters.Add("@pi_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_pi_list.SelectedValue);

            cmd.Parameters.Add("@pf_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_pf_list.SelectedValue);

          //  tiger.Date df = new tiger.Date();
            cmd.Parameters.Add("@mortgage_date_begin", SqlDbType.DateTime).Value = Convert.ToDateTime(df.DateCulture(ddl_mortgate_date_begin_m.SelectedValue, ddl_mortgate_date_begin_d.SelectedValue, ddl_mortgate_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            int new_update = 0;
            new_update = Convert.ToInt32(cmd.Parameters["@update_successful"].Value);
            if (new_update > 0)
                result.InnerHtml = " update mortgage successful";


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }


     }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void chk_fi_add_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_fi_add.Checked == true)
        {


            panel_fi.Visible = true;

        }

        if (chk_fi_add.Checked == false)
        {
            panel_fi.Visible = false;
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_fi_Click(object sender, EventArgs e)
    {
        Page.Validate();
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prFinancialInstitutionAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@new_fi_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.NVarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@fi_name", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_name.Text);
            cmd.Parameters.Add("@fi_website", SqlDbType.NVarChar, 200).Value = RegEx.getText(fi_website.Text);

            cmd.Parameters.Add("@fi_addr", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_addr.Text);
            cmd.Parameters.Add("@fi_city", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_city.Text);
            cmd.Parameters.Add("@fi_prov", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_prov.Text);
            cmd.Parameters.Add("@fi_pc", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_pc.Text);
            cmd.Parameters.Add("@fi_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_tel.Text);
            cmd.Parameters.Add("@fi_contact_fname", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_contact_fname.Text);
            cmd.Parameters.Add("@fi_contact_lname", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_contact_lname.Text);
            cmd.Parameters.Add("@fi_contact_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_contact_tel.Text);
            cmd.Parameters.Add("@fi_contact_email", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_contact_email.Text);
            cmd.Parameters.Add("@fi_contact_fax", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_contact_fax.Text);
            cmd.Parameters.Add("@country_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_fi_country_list.SelectedValue);
            cmd.Parameters.Add("@fi_com", SqlDbType.Text).Value = RegEx.getText(fi_com.Text);




            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            int new_fi_id = 0;
            new_fi_id = Convert.ToInt32(cmd.Parameters["@new_fi_id"].Value);
            if (new_fi_id > 0)
                result.InnerHtml = " add successful";


            tiger.FinancialInstitution fi = new tiger.FinancialInstitution(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_fi_list.DataSource = fi.getFinancialInstitutionList(Convert.ToInt32(Session["schema_id"]));
            ddl_fi_list.DataBind();
            ddl_fi_list.SelectedValue = Convert.ToString(new_fi_id);
            panel_fi.Visible = false;
            chk_fi_add.Checked = false;


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }


    }
    
}
