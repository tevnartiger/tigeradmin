﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="mortgage_archive_view.aspx.cs" Inherits="manager_mortgage_mortgage_archive_view" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
       
 <br /><br /><br /><br /><br /><br /><br />       
        
  <cc1:TabContainer ID="TabContainer1" runat="server"   >
   <cc1:TabPanel ID="tab1" runat="server" HeaderText="MORTGAGE ARCHIVE"  >
    <ContentTemplate  >
    <asp:Label ID="Label11" runat="server" Text="<%$ Resources:Resource, lbl_u_archive_mortgage %>"></asp:Label><br /> 
        <asp:HyperLink ID="mortgage_update_link"
          Text="<%$ Resources:Resource, lbl_update %>"  runat="server"></asp:HyperLink>-&nbsp;   - delete mortgage
        &nbsp;<br />
        <br />
      <table  width="50%" bgcolor="#ffffcc">
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_home_name %>"></asp:Label></td>
                <td >
                    <asp:Label ID="lbl_home_name" runat="server">
                    </asp:Label></td>
            </tr>
            <tr>
                <td >
                 <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lbl_financial_institution %>"></asp:Label></td>
                <td >
                    <asp:Label ID="lbl_fi_name" runat="server">
                    </asp:Label></td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_loan_amount %>"></asp:Label></td>
                <td >
                    <asp:Label ID="lbl_mortgage_loan_amount" runat="server" ></asp:Label></td>
            </tr>
            <tr>
                <td>
                   <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lbl_interestrate %>"></asp:Label></td>
                <td>
                    <asp:Label ID="lbl_mortgage_interest_rate" runat="server" ></asp:Label></td>
            </tr>
            <tr>
                <td>
                     <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, lbl_term %>"></asp:Label></td>
                <td>
                    <asp:Label ID="lbl_mortgage_term" runat="server" ></asp:Label></td>
            </tr>
            <tr>
                <td>
                     <asp:Label ID="Label10" runat="server" Text="<%$ Resources:Resource, lbl_current_term %>"></asp:Label></td>
                <td>
                    <asp:Label ID="lbl_mortgage_current_term" runat="server" ></asp:Label></td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lbl_date_begin %>"></asp:Label></td>
                <td>
                    <asp:Label ID="lbl_mortgate_date_begin" runat="server" ></asp:Label>
                    </td>
            </tr>
             <tr>
                <td >
                    <asp:Label ID="Label12" runat="server" Text="<%$ Resources:Resource, lbl_date_end %>"></asp:Label></td>
                <td>
                    <asp:Label ID="lbl_mortgate_date_end" runat="server" ></asp:Label>
                    </td>
            </tr>
            <tr>
                <td >
                     <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lbl_payment_frequency %>"></asp:Label></td>
                <td >
                <asp:Label ID="lbl_pf_categ" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                      <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lbl_payment_amount %>"></asp:Label></td>
                <td>
                    <asp:Label ID="lbl_mortgage_payment_amount" runat="server" ></asp:Label></td>
            </tr>
            <tr>
                <td>
                      <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, lbl_interest_coumpound %>"></asp:Label></td>
                <td>
                <asp:Label ID="lbl_pi_categ" runat="server" ></asp:Label>
                   
                       </td>
            </tr>
        </table>
        <br />
         <br />
        <asp:CheckBox ID="chk_enable_button" Text="<%$ Resources:Resource, lbl_enable_button %>"  runat="server" 
         oncheckedchanged="chk_enable_button_CheckedChanged" AutoPostBack="true" />
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       <br />
&nbsp;<asp:Button ID="btn_delete" runat="server" 
         Text="<%$ Resources:Resource, lbl_delete_mortgage %>" 
         onclick="btn_delete_Click" />
       <br />
        
       
   
      </ContentTemplate  >
  </cc1:TabPanel>
  
    
    
    
    <cc1:TabPanel ID="tab2" runat="server" HeaderText="MORTGAGE ARCHIVE AMORTIZATION TABLE"  >
    <ContentTemplate  >
    <table  bgcolor="#ffffcc">
            <tr>
                <td>
                    <asp:Label ID="Label14" runat="server" Text="<%$ Resources:Resource, lbl_home_name %>"></asp:Label></td>
                <td >
                  &nbsp;&nbsp;&nbsp;  <asp:Label ID="lbl_home_name2" runat="server">
                    </asp:Label></td>
            </tr>
     </table>
      <br />
      <br />
   <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="83%" BorderColor="White" BorderWidth="3" ID="gv_amortization" runat="server" AlternatingRowStyle-BackColor="Beige"  AutoGenerateColumns="false">
     <Columns>
      <asp:BoundField DataField="ID"   HeaderText="#" />
      <asp:BoundField DataField="ladate"  HeaderText="<%$ Resources:Resource, txt_date %>" /> 
      <asp:BoundField DataField="OustandingBalance" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, txt_outstanding_balance %>"/>
      <asp:BoundField DataField="InterestPaid" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, txt_interest_paid %>" />
      <asp:BoundField DataField="CummInterestPaid" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, txt_cumm_interest %>"/>
      <asp:BoundField DataField="PrincipalPaid" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, txt_principal_paid %>" />
      <asp:BoundField DataField="CummPrincipalPaid" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, txt_cumm_principal_paid %>" />
      <asp:BoundField DataField="Balance" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, txt_balance %>" />  
    </Columns>
  </asp:GridView>
  </ContentTemplate  >
   </cc1:TabPanel>
   
   
   
 </cc1:TabContainer>
    
    
    
    
    

</asp:Content>

