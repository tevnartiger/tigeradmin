﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="mortgage_amortization.aspx.cs" Inherits="mortgage_mortgage_amortization" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <table bgcolor="#DEDEDE">
        <tr>
            <td valign="top">
    <table>
        <tr>
            <td style="height: 17px">
                <asp:Label ID="lbl_mortgage_balance" runat="server" Text="<%$ Resources:Resource, lbl_mortgage_balance %>"/></td>
            <td style="height: 17px">
                <asp:TextBox ID="tbx_balance" runat="server" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
               <asp:Label ID="lbl_interestrate" runat="server" Text="<%$ Resources:Resource, lbl_interestrate %>"/></td>
            <td>
                <asp:TextBox ID="tbx_interest" runat="server" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_duration" runat="server" Text="<%$ Resources:Resource, lbl_duration%>"/></td>
            <td>
                <asp:TextBox ID="tbx_duration_years" runat="server" >25</asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_mortgage_date_begin" runat="server" Text="<%$ Resources:Resource, lbl_mortgage_date_begin%>"/></td>
            <td>
               <asp:DropDownList   ID="ddl_mortgage_begin_m" runat="server">
                       <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>&nbsp;/&nbsp;
                    <asp:DropDownList ID="ddl_mortgage_begin_d" runat="server">
                         <asp:ListItem Value="0">Day</asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;/<asp:DropDownList  ID="ddl_mortgage_begin_y" runat="server" >
                     <asp:ListItem>1980</asp:ListItem>
                    <asp:ListItem>1981</asp:ListItem>
                    <asp:ListItem>1982</asp:ListItem>
                    <asp:ListItem>1983</asp:ListItem>
                    <asp:ListItem>1984</asp:ListItem>
                    <asp:ListItem>1985</asp:ListItem>
                    <asp:ListItem>1986</asp:ListItem>
                    <asp:ListItem>1987</asp:ListItem>
                    <asp:ListItem>1988</asp:ListItem>
                    <asp:ListItem>1989</asp:ListItem>
                    <asp:ListItem>1990</asp:ListItem>
                    <asp:ListItem>1991</asp:ListItem>
                    <asp:ListItem>1992</asp:ListItem>
                    <asp:ListItem>1993</asp:ListItem>
                    <asp:ListItem>1994</asp:ListItem>
                    <asp:ListItem>1995</asp:ListItem>
                    <asp:ListItem>1996</asp:ListItem>
                    <asp:ListItem>1997</asp:ListItem>
                    <asp:ListItem>1998</asp:ListItem>
                    <asp:ListItem>1999</asp:ListItem>                    
                    <asp:ListItem>2000</asp:ListItem>
                    <asp:ListItem>2001</asp:ListItem>
                    <asp:ListItem>2002</asp:ListItem>
                    <asp:ListItem>2003</asp:ListItem>
                    <asp:ListItem>2004</asp:ListItem>
                    <asp:ListItem>2005</asp:ListItem>
                    <asp:ListItem>2006</asp:ListItem>
                    <asp:ListItem>2007</asp:ListItem>
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                    <asp:ListItem>2011</asp:ListItem>
                    <asp:ListItem>2012</asp:ListItem>
                    <asp:ListItem>2013</asp:ListItem>
                    <asp:ListItem>2014</asp:ListItem>
                    <asp:ListItem>2015</asp:ListItem>
                 </asp:DropDownList></td>
        </tr>
        <tr>
            <td><asp:Label ID="lbl_mortgage_paid" runat="server" Text="<%$ Resources:Resource, lbl_mortgage_paid%>"/>
                </td>
            <td>
                   <asp:DropDownList ID="ddl_mortgage_paid_frequency" runat="server" >
                       <asp:ListItem Selected="True" Text="<%$ Resources:Resource, txt_monthly%>" Value="12"></asp:ListItem>
                        <asp:ListItem Value="24" Text="<%$ Resources:Resource, txt_bi_weekly%>"></asp:ListItem>
                        <asp:ListItem Value="241" Text="<%$ Resources:Resource, txt_acc_bi_weekly%>"></asp:ListItem>
                        <asp:ListItem Value="52" Text="<%$ Resources:Resource, txt_weekly%>"></asp:ListItem>
                        <asp:ListItem Value="521" Text="<%$ Resources:Resource, txt_acc_weekly%>"></asp:ListItem>
                    </asp:DropDownList></td>
        </tr>
        <tr>
            <td><asp:Label ID="lbl_interest_compounded" runat="server" Text="<%$ Resources:Resource, lbl_interest_compounded%>"/>
               </td>
            <td>
                   <asp:DropDownList ID="ddl_interest_compounded" runat="server" >
                       <asp:ListItem Value="12" Text="<%$ Resources:Resource, txt_yearly%>"></asp:ListItem>
                       <asp:ListItem Value="6" Text="<%$ Resources:Resource, txt_bi_yearly%>"></asp:ListItem>
                       
                   </asp:DropDownList>
                              </td>
        </tr>
    </table>
            </td>
            <td valign="top">
                &nbsp;</td>
        </tr>
    </table>
                      <br />
                      <br />
                    
                      <table bgcolor="#DEDEDE" >
                          <tr>
                              <td valign="top">
                <asp:Repeater ID="r_amortizationCalculation" runat="server">
                <ItemTemplate>
                <table >
                <tr>
                        <td bgcolor="#879EAA" style="font-weight: 700" colspan ="2">
                        
                           <asp:Label ID="lbl_regular_monthly_payments" runat="server" Text="<%$ Resources:Resource, lbl_regular_monthly_payments%>"/>
                        </td>
                       
                    </tr>
                    <tr>
                        <td><asp:Label ID="lbl_periodic_rate" runat="server" Text="<%$ Resources:Resource, lbl_periodic_rate%>"/>
                            </td>
                        <td>
                          <%#DataBinder.Eval(Container.DataItem, "PeriodRate", "{0:0.00}")%> &nbsp;&nbsp %
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbl_periodic_payments" runat="server" Text="<%$ Resources:Resource, lbl_periodic_payments%>"/> <br />(without extra payments)
                        </td>
                        <td valign="top">
                          <%#DataBinder.Eval(Container.DataItem, "Payments","{0:0.00}")%> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                         <asp:Label ID="lbl_number_payments" runat="server" Text="<%$ Resources:Resource, lbl_number_payments%>"/> 
                        </td>
                        <td>
                          <%#DataBinder.Eval(Container.DataItem, "ID")%> 
                        </td>
                    </tr>
                    <tr>
                        <td><asp:Label ID="lbl_avg_interest" runat="server" Text="<%$ Resources:Resource, lbl_avg_interest%>"/>
                            </td>
                        <td>
                             <%#DataBinder.Eval(Container.DataItem, "AvgPeriodInterest", "{0:0.00}")%>  
                        </td>
                    </tr>
                    <tr>
                        <td><asp:Label ID="lbl_total_interest" runat="server" Text="<%$ Resources:Resource, lbl_total_interest%>"/>
                            </td>
                        <td><asp:HiddenField ID="h_totalinterest1" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "CummInterestPaid")%>' />
                            <%#DataBinder.Eval(Container.DataItem, "CummInterestPaid", "{0:0.00}")%>  
                        </td>
                    </tr>
                    <tr>
                        <td><asp:Label ID="lbl_total_payments" runat="server" Text="<%$ Resources:Resource, lbl_total_payments%>"/>
                            </td>
                        <td>
                            <%#DataBinder.Eval(Container.DataItem, "TotalPaid", "{0:0.00}")%> 
                        </td>
                    </tr>
                     <tr>
                        <td><asp:Label ID="lbl_timetopayloan" runat="server" Text="<%$ Resources:Resource, lbl_timetopayloan%>"/>
                            </td>
                        <td><asp:HiddenField ID="h_timetopayloan1" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "TimeToPayLoan")%>' />
                            <%#DataBinder.Eval(Container.DataItem, "TimeToPayLoan", "{0:0.00}")%> &nbsp;years
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbl_5050" runat="server" Text="<%$ Resources:Resource, lbl_5050%>"/></td>
                        <td>
                          <%#DataBinder.Eval(Container.DataItem, "FiftyFiftyPoint_m")%> / <%#DataBinder.Eval(Container.DataItem, "FiftyFiftyPoint_y")%> 
                        </td>
                    </tr>
                    <tr>
                        <td><asp:Label ID="lbl_end_point" runat="server" Text="<%$ Resources:Resource, lbl_end_point%>"/>
                            </td>
                        <td>
                          <%#DataBinder.Eval(Container.DataItem, "themonth")%> / <%#DataBinder.Eval(Container.DataItem, "theyear")%> 
                        </td>
                    </tr>
                </table>
                
                </ItemTemplate>
                </asp:Repeater>
            
                              </td>
                              <td valign="top">
                                  <asp:Repeater ID="r_amortizationExtraCalculation" runat="server">
                <ItemTemplate>
                              <td valign="top">
                
                <table >
                <tr> 
                        
                        <td bgcolor="#879EAA" colspan="2">
                            <b> <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, txt_acc_payments%>"/>
                        </b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbl_periodic_rate" runat="server" Text="<%$ Resources:Resource, lbl_periodic_rate%>"/></td>
                        <td>
                          <%#DataBinder.Eval(Container.DataItem, "PeriodRate", "{0:0.00}")%> &nbsp;&nbsp %
                        </td>
                    </tr>
                    
                     <tr>
                        <td>
                            <asp:Label ID="lbl_periodic_paymentsextra" runat="server" Text="<%$ Resources:Resource, lbl_periodic_paymentsextra%>"/> 
                            <br />
                            <asp:Label ID="lbl_periodic_paymentsnoextra" runat="server" Text="<%$ Resources:Resource, lbl_periodic_paymentsnoextra%>"/> 
                           
                        </td>
                        <td valign="top">
                          <%#DataBinder.Eval(Container.DataItem, "ExtraPayments", "{0:0.00}")%> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <asp:Label ID="lbl_number_payments" runat="server" Text="<%$ Resources:Resource, lbl_number_payments%>"/> 
                        </td>
                        <td>
                          <%#DataBinder.Eval(Container.DataItem, "ID")%> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                         <asp:Label ID="lbl_avg_interest" runat="server" Text="<%$ Resources:Resource, lbl_avg_interest%>"/></td>
                        <td>
                             <%#DataBinder.Eval(Container.DataItem, "AvgPeriodInterest", "{0:0.00}")%>  
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbl_total_interest" runat="server" Text="<%$ Resources:Resource, lbl_total_interest%>"/></td>
                        <td><asp:HiddenField ID="h_totalinterest2" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "CummInterestPaid")%>' />
                            <%#DataBinder.Eval(Container.DataItem, "CummInterestPaid", "{0:0.00}")%>  
                        </td>
                    </tr>
                    <tr>
                        <td>
                           <asp:Label ID="lbl_total_payments" runat="server" Text="<%$ Resources:Resource, lbl_total_payments%>"/></td>
                        <td>
                            <%#DataBinder.Eval(Container.DataItem, "TotalPaid", "{0:0.00}")%> 
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <asp:Label ID="lbl_timetopayloan" runat="server" Text="<%$ Resources:Resource, lbl_timetopayloan%>"/></td>
                        <td><asp:HiddenField ID="h_timetopayloan2" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "TimeToPayLoan")%>' />
                            <%#DataBinder.Eval(Container.DataItem, "TimeToPayLoan", "{0:0.00}")%> &nbsp;years
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbl_5050" runat="server" Text="<%$ Resources:Resource, lbl_5050%>"/></td>
                        <td>
                          <%#DataBinder.Eval(Container.DataItem, "FiftyFiftyPoint_m")%> / <%#DataBinder.Eval(Container.DataItem, "FiftyFiftyPoint_y")%> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbl_end_point" runat="server" Text="<%$ Resources:Resource, lbl_end_point%>"/></td>
                        <td>
                          <%#DataBinder.Eval(Container.DataItem, "themonth")%> / <%#DataBinder.Eval(Container.DataItem, "theyear")%> 
                        </td>
                    </tr>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
              
                    <tr>
                        <td colspan="2" bgcolor="#FF3300">
                          <asp:Label ID="lbl_savings" runat="server" Text="<%$ Resources:Resource, lbl_savings%>"/>  Savings</td>
                        
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_amount%>"/> </td>
                        <td>
                            <asp:Label ID="lbl_amount_save" runat="server" Text='<%#GetMoneySave(Convert.ToDouble(Eval("CummInterestPaid")))%>'></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp; <asp:Label ID="lbl_years" runat="server" Text="<%$ Resources:Resource, lbl_years%>"/> &nbsp;</td>
                        <td>
                          <asp:Label ID="lbl_year_save" runat="server" Text='<%#GetYearSave(Convert.ToDouble(Eval("TimeToPayLoan")))%>'></asp:Label>
                            </td>
                    </tr>
            
                    
                    
                    
                    
                    
                    
                    
                </table>
                
                      </td></ItemTemplate>
                </asp:Repeater>
                              </td>
                          </tr>
                      </table>
                    
                      <br />
   
    <b><asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lbl_opt_extra_payments%>"/> </b><br />
    <br />
   
    <table>
        <tr>
            <td>
            <asp:Label ID="lbl_periodic_paymentsextra" runat="server" Text="<%$ Resources:Resource, lbl_extra_periodic_payments%>"/>
               </td>
                              <td>
                                  <asp:TextBox ID="tbx_additionnal_payment" runat="server"  >0</asp:TextBox>
                              </td>
                              <td>
                                  &nbsp;</td>
                          </tr>
                          <tr>
                              <td> <asp:Label ID="lbl_extra_early_payments" runat="server" Text="<%$ Resources:Resource, lbl_extra_early_payments%>"/>
                               
                              </td>
                              <td>
                                  <asp:TextBox ID="tbx_additionnal_eachYear" runat="server" 
                                      Width="65px">0</asp:TextBox>
                              </td>
                              <td>
&nbsp;<asp:DropDownList   ID="ddl_eachYear" runat="server">
                       <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                                  &nbsp;</td>
                          </tr>
                          </table>
                      <br />
                  &nbsp;<asp:Button ID="btn_calculate" runat="server" onclick="btn_calculate_Click" 
                          Text="<%$ Resources:Resource, btn_calculate_amortization %>" />
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <asp:Button ID="btn_calculate_extra" runat="server" 
                      onclick="btn_calculate_extra_Click" 
        Text="<%$ Resources:Resource, btn_amortization_table %>"/>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btn_graph" runat="server" 
        Text="<%$ Resources:Resource, lbl_graphs %>" onclick="btn_graph_Click" />
                      <br />
    
       <asp:Label ID="Label1" runat="server" ></asp:Label>
    <br />
<asp:GridView HeaderStyle-BackColor="AliceBlue" Width="100%" BorderColor="White" BorderWidth="3" ID="gv_amortization" runat="server" AlternatingRowStyle-BackColor="Beige"  AutoGenerateColumns="false">
    
    <Columns>
    
    <asp:BoundField DataField="ID"   HeaderText="#" />
    <asp:BoundField DataField="ladate"  HeaderText="<%$ Resources:Resource, txt_date %>" /> 
    <asp:BoundField DataField="OustandingBalance" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, txt_outstanding_balance %>"/>
    <asp:BoundField DataField="InterestPaid" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, txt_interest_paid %>" />
    <asp:BoundField DataField="CummInterestPaid" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, txt_cumm_interest %>"/>
    <asp:BoundField DataField="PrincipalPaid" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, txt_principal_paid %>" />
    <asp:BoundField DataField="CummPrincipalPaid" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, txt_cumm_principal_paid %>" />
    <asp:BoundField DataField="Balance" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, txt_balance %>" />  
    
   
  </Columns>
    </asp:GridView>
<br /><br />

  <asp:Chart ID="Chart1"   runat="server">
        <series>
          <asp:Series  ChartArea="ChartArea1" Name="<%$ Resources:Resource, txt_balance %>"></asp:Series>
          <asp:Series ChartArea="ChartArea2" Name="<%$ Resources:Resource, txt_interest_paid %>"></asp:Series>
          <asp:Series ChartArea="ChartArea2" Name="<%$ Resources:Resource, txt_principal_paid %>"></asp:Series>
        
        </series>
        <chartareas >
         
          <asp:ChartArea    Name="ChartArea1">
          <axisy LineColor="64, 64, 64, 64" >
					<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
					<MajorGrid LineColor="64, 64, 64, 64" />
				</axisy>
				<axisx  LineColor="64, 64, 64, 64">
					<LabelStyle  Font="Trebuchet MS, 8.25pt, style=Bold" IsEndLabelVisible="True" />
					<MajorGrid LineColor="64, 64, 64, 64" />        
				</axisx>
          </asp:ChartArea>
          <asp:ChartArea  Name="ChartArea2">
           <axisy LineColor="64, 64, 64, 64" >
					<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" />
					<MajorGrid LineColor="64, 64, 64, 64" />
				</axisy>
				<axisx  LineColor="64, 64, 64, 64">
					<LabelStyle  Font="Trebuchet MS, 8.25pt, style=Bold" IsEndLabelVisible="True" />
					<MajorGrid LineColor="64, 64, 64, 64" />        
				</axisx>
          </asp:ChartArea>
        </chartareas>
    </asp:Chart>
 
Information and interactive calculators are made available to you as self-help tools for your independent use and are not intended to provide investment advice. We can not and do not guarantee their applicability or accuracy in regards to your individual circumstances. All examples are hypothetical and are for illustrative purposes. We encourage you to seek personalized advice from qualified professionals regarding all personal finance issues.
</asp:Content>

