﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// Done by : Stanley Joocelyn
/// date    : jul 1 , 2008
/// </summary>

public partial class manager_mortgage_mortgage_add :BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        reg_fi_city.ValidationExpression = RegEx.getText();
        reg_fi_addr.ValidationExpression = RegEx.getText();
        reg_fi_contact_email.ValidationExpression = RegEx.getEmail();
        reg_fi_contact_fname.ValidationExpression = RegEx.getText();
        reg_fi_contact_lname.ValidationExpression = RegEx.getText();
        reg_fi_contact_tel.ValidationExpression = RegEx.getText();
        reg_fi_name.ValidationExpression = RegEx.getText();
        reg_fi_pc.ValidationExpression = RegEx.getText();
        reg_fi_tel.ValidationExpression = RegEx.getText();
        reg_fi_website.ValidationExpression = RegEx.getText();
        reg_fi_prov.ValidationExpression = RegEx.getText();
        reg_fi_contact_fax.ValidationExpression = RegEx.getText();

        reg_mortgage_term.ValidationExpression = RegEx.getNumeric();
        reg_mortgage_current_term.ValidationExpression = RegEx.getNumeric();
        reg_mortgage_loan_amount.ValidationExpression = RegEx.getMoney();
        reg_mortgage_payment_amount.ValidationExpression = RegEx.getMoney();
        reg_mortgage_interest_rate.ValidationExpression = RegEx.getMoney();

        if (!Page.IsPostBack)
        {

            // home address
            tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
          //  ddl_home_list.DataSource = hv.getHomeMortgageList(Convert.ToInt32(Session["schema_id"])); // hv.getHomeList(Convert.ToInt32(Session["schema_id"]));//
            ddl_home_list.DataSource = hv.getHomeList(Convert.ToInt32(Session["schema_id"]));//
            ddl_home_list.DataBind();
            //  ddl_home_list.Items.Insert(0,"home");


            tiger.Country ddl_fi = new tiger.Country(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_fi_country_list.DataSource = ddl_fi.getCountryList();
            ddl_fi_country_list.DataBind();

            tiger.FinancialInstitution fi = new tiger.FinancialInstitution(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_fi_list.DataSource = fi.getFinancialInstitutionList(Convert.ToInt32(Session["schema_id"]));
            ddl_fi_list.DataBind();
            //  ddl_fi_list.Items.Insert(0,"financial institution");

            panel_fi.Visible = false;
            lbl_all_property_mortgage.Visible = false;

            if (ddl_home_list.Items.Count < 1)
            {
                ddl_home_list.Enabled = false;
                btn_submit.Enabled = false;
                lbl_all_property_mortgage.Visible = true;
            }

        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        Page.Validate("vg_mortgage");
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prMortgageAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@new_mortgage_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.NVarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_list.SelectedValue);
            cmd.Parameters.Add("@fi_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_fi_list.SelectedValue);
            cmd.Parameters.Add("@mortgage_loan_amount", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(mortgage_loan_amount.Text));
            cmd.Parameters.Add("@mortgage_payment_amount", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(mortgage_payment_amount.Text));

            cmd.Parameters.Add("@mortgage_interest_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(mortgage_interest_rate.Text));
            cmd.Parameters.Add("@mortgage_term", SqlDbType.Int).Value = Convert.ToInt32(mortgage_term.Text);
            cmd.Parameters.Add("@mortgage_current_term", SqlDbType.Int).Value = Convert.ToInt32(RegEx.getInteger(mortgage_current_term.Text));

            cmd.Parameters.Add("@pi_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_pi_list.SelectedValue);

            cmd.Parameters.Add("@pf_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_pf_list.SelectedValue);



         //   DateTime expense_date_paid = new DateTime();
            tiger.Date df = new tiger.Date();
            cmd.Parameters.Add("@mortgage_date_begin", SqlDbType.DateTime).Value = Convert.ToDateTime(df.DateCulture(ddl_mortgate_date_begin_m.SelectedValue, ddl_mortgate_date_begin_d.SelectedValue, ddl_mortgate_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


            





            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            int new_mortgage_id = 0;
            new_mortgage_id = Convert.ToInt32(cmd.Parameters["@new_mortgage_id"].Value);
            if (new_mortgage_id > 0)
                result.InnerHtml = " add mortgage successful";


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void chk_fi_add_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_fi_add.Checked == true)
        {


            panel_fi.Visible = true;

        }

        if (chk_fi_add.Checked == false)
        {
            panel_fi.Visible = false;
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_fi_Click(object sender, EventArgs e)
    {

        Page.Validate();
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }


        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prFinancialInstitutionAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@new_fi_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@fi_name", SqlDbType.NVarChar, 50).Value = fi_name.Text;
            cmd.Parameters.Add("@fi_website", SqlDbType.NVarChar, 200).Value = fi_website.Text;

            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.Int).Value = Request.UserHostAddress.ToString();
            
            cmd.Parameters.Add("@fi_addr", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_addr.Text);
            cmd.Parameters.Add("@fi_city", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_city.Text);
            cmd.Parameters.Add("@fi_prov", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_prov.Text);
            cmd.Parameters.Add("@fi_pc", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_pc.Text);
            cmd.Parameters.Add("@fi_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_tel.Text);
            cmd.Parameters.Add("@fi_contact_fname", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_contact_fname.Text);
            cmd.Parameters.Add("@fi_contact_lname", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_contact_lname.Text);
            cmd.Parameters.Add("@fi_contact_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_contact_tel.Text);
            cmd.Parameters.Add("@fi_contact_email", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_contact_email.Text);
            cmd.Parameters.Add("@fi_contact_fax", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_contact_fax.Text);
            cmd.Parameters.Add("@country_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_fi_country_list.SelectedValue);
            cmd.Parameters.Add("@fi_com", SqlDbType.Text).Value = RegEx.getText(fi_com.Text);




            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            int new_fi_id = 0;
            new_fi_id = Convert.ToInt32(cmd.Parameters["@new_fi_id"].Value);
            if (new_fi_id > 0)
                result.InnerHtml = " add successful";


            tiger.FinancialInstitution fi = new tiger.FinancialInstitution(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_fi_list.DataSource = fi.getFinancialInstitutionList(Convert.ToInt32(Session["schema_id"]));
            ddl_fi_list.DataBind();
            ddl_fi_list.SelectedValue = Convert.ToString(new_fi_id);
            panel_fi.Visible = false;
            chk_fi_add.Checked = false;


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }


    }
}
