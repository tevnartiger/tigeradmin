﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : sept 7 , 2008
/// </summary>
/// 
public partial class manager_reports_reports_delequencies : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        tiger.Rent hp = new tiger.Rent(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_rent_delequency.DataSource = hp.getRentDelequencyList(Convert.ToInt32(Session["schema_id"]), 0);
        gv_rent_delequency.DataBind();
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="amount_of_warning_sent"></param>
    /// <returns></returns>
    protected string Get_AmountOfWarningSent(int amount_of_warning_sent)
    {

        string warning_sent = "";


        if (amount_of_warning_sent == 0)
        {
            warning_sent = Resources.Resource.lbl_none;
        }

        if (amount_of_warning_sent == 1)
        {
            warning_sent = Resources.Resource.lbl_first_notice;
        }

        if (amount_of_warning_sent == 2)
        {
            warning_sent = Resources.Resource.lbl_second_notice;
        }

        if (amount_of_warning_sent == 3)
        {
            warning_sent = Resources.Resource.lbl_third_notice;
        }

        return warning_sent;
    }
}
