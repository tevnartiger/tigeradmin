﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="reports_delequencies.aspx.cs" Inherits="manager_reports_reports_delequencies" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<br />
 <asp:GridView Width="83%" ID="gv_rent_delequency" runat="server" AutoGenerateColumns="false"
                   AllowPaging="true" AllowSorting="true"  AutoGenerateSelectButton="false">
    <Columns>
     <asp:BoundField DataField="home_name"  HeaderText="<%$ Resources:Resource, lbl_property%>"  />
     
      <asp:BoundField DataField="unit_door_no"  HeaderText="<%$ Resources:Resource, gv_unit%>"  />
      
      
      <asp:BoundField DataField="amount_owed"  HeaderText="<%$ Resources:Resource, gv_amount_owed%>"
             DataFormatString="{0:0.00}"   HtmlEncode="false"  />
    
     
      
      
      <asp:BoundField   DataField="rp_due_date" DataFormatString="{0:M-dd-yyyy}"  
       HtmlEncode="false" HeaderText="<%$ Resources:Resource, gv_due_date%>"  />
      <asp:BoundField  DataField="days" HeaderText="<%$ Resources:Resource, lbl_days %>"/> 
     
      <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_notice_sent%>"   >
      <ItemTemplate>
      <asp:Label  runat="server"  ID="notice_sent"    Text='<%#Get_AmountOfWarningSent(Convert.ToInt32(Eval("amount_of_warning_sent")))%>'    /> 
      </ItemTemplate>
      </asp:TemplateField>
     
      <asp:HyperLinkField  Text="<%$ Resources:Resource, gv_view %>" 
        DataNavigateUrlFields="home_id,tenant_id,tu_id,rl_rent_amount,re_id,unit_id" 
        DataNavigateUrlFormatString="~/manager/tenant/tenant_rent.aspx?h_id={0}&t_id={1}&tu_id={2}&ra_id={3}&re_id={4}&unit_id={5}" 
         HeaderText="<%$ Resources:Resource, gv_view %>"  />
    
      <asp:HyperLinkField  Text="<%$ Resources:Resource, lbl_send_notice %>" 
        DataNavigateUrlFields="rp_id,tenant_id,tu_id" 
        DataNavigateUrlFormatString="~/manager/notice/notice_delequency_send.aspx?rp_id={0}&tenant_id={1}&tu_id={2}" 
         HeaderText="<%$ Resources:Resource, lbl_send_notice %>"  />
    
   </Columns>
   </asp:GridView>
      
</asp:Content>

