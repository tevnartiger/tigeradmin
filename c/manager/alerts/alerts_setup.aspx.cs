﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : august 11 , 2008
/// </summary>
public partial class manager_alerts_alerts_setup : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {

            // set the regular expression in the validator

            reg_as_ip_expiration.ValidationExpression = RegEx.getNumeric();
            reg_as_ip_pending.ValidationExpression = RegEx.getNumeric();
            reg_as_lease_expiration.ValidationExpression = RegEx.getNumeric();
            reg_as_lease_pending.ValidationExpression = RegEx.getNumeric();
            reg_as_mortgage_expiration.ValidationExpression = RegEx.getNumeric();
            reg_as_mortgage_pending.ValidationExpression = RegEx.getNumeric();


            tb_successfull_confirmation.Visible = false;

            //the default number of days of alert we want is 90 days
            tbx_as_ip_expiration.Text = "90";
            tbx_as_ip_pending.Text ="90";
            tbx_as_lease_expiration.Text = "90";
            tbx_as_lease_pending.Text = "90";
            tbx_as_mortgage_expiration.Text = "90";
            tbx_as_mortgage_pending.Text = "90";


            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prAlertView", conn);

            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = 0;
            
            // this trace is not a query parameter , it only server to trace  a default insert in
            // the store procedure
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);


            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                    // amount of days for insurance policy pending and expiration
                    tbx_as_ip_expiration.Text = dr["as_ip_expiration"].ToString();
                    tbx_as_ip_pending.Text = dr["as_ip_pending"].ToString();

                    

                    // amount of days for lease pending and expiration
                    tbx_as_lease_expiration.Text = dr["as_lease_expiration"].ToString();
                    tbx_as_lease_pending.Text = dr["as_lease_pending"].ToString();



                    // amount of days for mortgage pending and expiration
                    tbx_as_mortgage_expiration.Text = dr["as_mortgage_expiration"].ToString();
                    tbx_as_mortgage_pending.Text = dr["as_mortgage_pending"].ToString();

                    }

            }
            finally
            {
                conn.Close();
            }



            


        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_update_Click(object sender, EventArgs e)
    {

        Page.Validate();
        // if page is not valid exit and delete the session
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prAlertSetup", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        conn.Open();
        //Add the params

        cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
        cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
        cmd.Parameters.Add("@as_ip_expiration", SqlDbType.Int).Value = Convert.ToInt32(tbx_as_ip_expiration.Text);
        cmd.Parameters.Add("@as_ip_pending", SqlDbType.Int).Value = Convert.ToInt32(tbx_as_ip_pending.Text);
        cmd.Parameters.Add("@as_lease_expiration", SqlDbType.Int).Value = Convert.ToInt32(tbx_as_lease_expiration.Text);
        cmd.Parameters.Add("@as_lease_pending", SqlDbType.Int).Value = Convert.ToInt32(tbx_as_lease_pending.Text);
        cmd.Parameters.Add("@as_mortgage_expiration", SqlDbType.Int).Value = Convert.ToInt32(tbx_as_mortgage_expiration.Text);
        cmd.Parameters.Add("@as_mortgage_pending", SqlDbType.Int).Value = Convert.ToInt32(tbx_as_mortgage_pending.Text);
        cmd.ExecuteNonQuery();


        int return_id = 0;
        return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);
        if (return_id == 0)
        {
            //   result.InnerHtml = " add mortgage successful";
            tb_successfull_confirmation.Visible = true;
        }

    }
}
