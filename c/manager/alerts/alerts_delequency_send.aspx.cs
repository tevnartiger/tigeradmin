﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : july 31 , 2008
/// </summary>
public partial class manager_alerts_alerts_delequency_send : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (RegEx.IsInteger(Request.QueryString["tu_id"]))
            h_tu_id.Value = Request.QueryString["tu_id"];
        else
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        if (RegEx.IsInteger(Request.QueryString["rp_id"]))
            h_rp_id.Value = Request.QueryString["rp_id"];
        else
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        if (RegEx.IsInteger(Request.QueryString["tenant_id"]))
            h_tenant_id.Value = Request.QueryString["tenant_id"];
        else
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        if (!Page.IsPostBack)
        {
           
            tb_successfull_confirmation.Visible = false;

            // the preview, before "printing" the notice , is not visible
            

            DateTime la_date = new DateTime();
            la_date = DateTime.Now;

            lbl_notice_date.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();
            lbl_notice_date_2.Text = lbl_notice_date.Text;
            lbl_notice_date_3.Text = lbl_notice_date_2.Text;


            String txt_first_notice = Resources.Resource.lbl_first_notice_text;
            tbx_first_notice.Value = txt_first_notice + "\r\n\r\n" + Resources.Resource.lbl_amount_owed + " : ";

            tbx_second_notice.Text = Resources.Resource.lbl_second_notice_text;
            tbx_third_notice.Text = Resources.Resource.lbl_third_notice_text;

            double amount_owed = 0;
            int days_late = 0;

            // by default will not send email
            radio_send_mail_1.SelectedValue = "0";
            radio_send_mail_2.SelectedValue = "0";
            radio_send_mail_3.SelectedValue = "0";

            ////////////////////////////////
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prTenantUnitNameList", conn);
            SqlCommand cmd2 = new SqlCommand("prRentDelequency", conn);
            SqlCommand cmd3 = new SqlCommand("prLateFee", conn);


            cmd.CommandType = CommandType.StoredProcedure;

            try
            {

                conn.Open();

                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tenant_id"]);


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);


                r_tenant_list.DataSource = dt;
                r_tenant_list_2.DataSource = dt;
                r_tenant_list_3.DataSource = dt;

                r_tenant_list.DataBind();
                r_tenant_list_2.DataBind();
                r_tenant_list_3.DataBind();
            }
            finally
            {
                //  conn.Close();
            }


            cmd2.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd2.Parameters.Add("@rp_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["rp_id"]);


                SqlDataReader dr = null;
                dr = cmd2.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                 {

                     amount_owed =Convert.ToDouble(dr["amount_owed"]);

                     tbx_first_notice.Value = tbx_first_notice.Value + String.Format("{0:0.00}", amount_owed);
                     
                    // the amount owed written in the second notice letter
                     lbl_amount_owed.Text = String.Format("{0:0.00}", amount_owed);

                     // the amount owed written in the third notice letter
                     lbl_amount_owed_2.Text = lbl_amount_owed.Text;

                    // the number of days late ( rent payment)
                     lbl_days_late_2.Text = dr["days"].ToString();
                     lbl_days_late_3.Text = lbl_days_late_2.Text ;
                }
            }
            finally
            {
               //  conn.Close();
            }




            cmd3.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd3.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);


                SqlDataReader dr3 = null;
                dr3 = cmd3.ExecuteReader(CommandBehavior.SingleRow);

                while (dr3.Read() == true)
                {

                    double late_fee= Convert.ToDouble(dr3["tt_late_fee"]);
                    double total = 0;
                    
                    // the late fee written in the second notice letter
                    lbl_late_fee.Text = String.Format("{0:0.00}", late_fee);

                    // the late fee written in the third notice letter
                    lbl_late_fee_2.Text = String.Format("{0:0.00}", late_fee);

                    total = late_fee + amount_owed;

                    lbl_total.Text = String.Format("{0:0.00}", total);
                    lbl_total_2.Text = lbl_total.Text;


                }
            }
            finally
            {
                conn.Close();
            }



            ///////////////////////////////


        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_confirm_Click(object sender, EventArgs e)
    {


        if (RegEx.IsInteger(Request.QueryString["tu_id"]))
            h_tu_id.Value = Request.QueryString["tu_id"];
        else
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        if (RegEx.IsInteger(Request.QueryString["rp_id"]))
            h_rp_id.Value = Request.QueryString["rp_id"];
        else
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        if (RegEx.IsInteger(Request.QueryString["tenant_id"]))
            h_tenant_id.Value = Request.QueryString["tenant_id"];
        else
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prLateRentNoticeAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        DateTime right_now = new DateTime();
        right_now = DateTime.Now;

        tiger.Date d = new tiger.Date();

        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));



        conn.Open();
        //Add the params

        cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
        cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);
        cmd.Parameters.Add("@rp_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["rp_id"]);
        cmd.Parameters.Add("@lrn_date", SqlDbType.VarChar, 50).Value = right_now;
        cmd.Parameters.Add("@lrn_number", SqlDbType.Int).Value = Convert.ToInt32(ddl_notice.SelectedValue);
        cmd.Parameters.Add("@lrn_email", SqlDbType.Bit).Value = Convert.ToByte(radio_send_mail_1.SelectedValue);
        cmd.Parameters.Add("@lrn_text", SqlDbType.NText).Value = tbx_first_notice.Value;
       
        cmd.ExecuteNonQuery();

        int return_id = 0;
        return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);
        if (return_id == 0)
        {
          //   result.InnerHtml = " add mortgage successful";
            tb_successfull_confirmation.Visible = true;
        }

        conn.Close();


        string tenant_email = "";

        if (radio_send_mail_1.SelectedValue == "1")
        {
            // send mail


            // first get the tenant's email

           // string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

           //   SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd2 = new SqlCommand("prTenantUnitNameList", conn);
            cmd2.CommandType = CommandType.StoredProcedure;

            try
            {

                conn.Open();

                //Add the params
                
                cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd2.Parameters.Add("@tenant_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tenant_id"]);


                SqlDataReader dr2 = null;
                dr2 = cmd2.ExecuteReader();

                while (dr2.Read())
                {
                    tenant_email = tenant_email + dr2["name_email"].ToString() +" ";
                    
                }
            }

            finally
            {
                conn.Close();
            }




            /// CONTINU HERE


        }


    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_confirm_2_Click(object sender, EventArgs e)
    {


        if (RegEx.IsInteger(Request.QueryString["tu_id"]))
            h_tu_id.Value = Request.QueryString["tu_id"];
        else
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        if (RegEx.IsInteger(Request.QueryString["rp_id"]))
            h_rp_id.Value = Request.QueryString["rp_id"];
        else
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        if (RegEx.IsInteger(Request.QueryString["tenant_id"]))
            h_tenant_id.Value = Request.QueryString["tenant_id"];
        else
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prLateRentNoticeAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        DateTime right_now = new DateTime();
        right_now = DateTime.Now;

        tiger.Date d = new tiger.Date();

        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));



        conn.Open();
        //Add the params

        cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
        cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);
        cmd.Parameters.Add("@rp_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["rp_id"]);
        cmd.Parameters.Add("@lrn_date", SqlDbType.VarChar, 50).Value = right_now;
        cmd.Parameters.Add("@lrn_number", SqlDbType.Int).Value = Convert.ToInt32(ddl_notice.SelectedValue);
        cmd.Parameters.Add("@lrn_email", SqlDbType.Bit).Value = Convert.ToByte(radio_send_mail_2.SelectedValue); 
        cmd.Parameters.Add("@lrn_text", SqlDbType.NText).Value = tbx_second_notice.Text;

        cmd.ExecuteNonQuery();

        int return_id = 0;
        return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);
        if (return_id == 0)
        {
            //   result.InnerHtml = " add mortgage successful";
            tb_successfull_confirmation.Visible = true;
        }

        conn.Close();

        if (radio_send_mail_2.SelectedValue == "1")
        {
            // send mail

             

        }


    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_confirm_3_Click(object sender, EventArgs e)
    {


        if (RegEx.IsInteger(Request.QueryString["tu_id"]))
            h_tu_id.Value = Request.QueryString["tu_id"];
        else
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        if (RegEx.IsInteger(Request.QueryString["rp_id"]))
            h_rp_id.Value = Request.QueryString["rp_id"];
        else
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        if (RegEx.IsInteger(Request.QueryString["tenant_id"]))
            h_tenant_id.Value = Request.QueryString["tenant_id"];
        else
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prLateRentNoticeAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        DateTime right_now = new DateTime();
        right_now = DateTime.Now;

        tiger.Date d = new tiger.Date();

        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));



        conn.Open();
        //Add the params
        cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
        cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);
        cmd.Parameters.Add("@rp_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["rp_id"]);
        cmd.Parameters.Add("@lrn_date", SqlDbType.VarChar, 50).Value = right_now;
        cmd.Parameters.Add("@lrn_number", SqlDbType.Int).Value = Convert.ToInt32(ddl_notice.SelectedValue);
        cmd.Parameters.Add("@lrn_email", SqlDbType.Bit).Value = Convert.ToByte(radio_send_mail_3.SelectedValue); 
        cmd.Parameters.Add("@lrn_text", SqlDbType.NText).Value = tbx_third_notice.Text;

        cmd.ExecuteNonQuery();

        int return_id = 0;
        return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);
        if (return_id == 0)
        {
            //   result.InnerHtml = " add mortgage successful";
            tb_successfull_confirmation.Visible = true;
        }

        conn.Close();



        if (radio_send_mail_3.SelectedValue == "1")
        {
            // send mail

        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_notice_SelectedIndexChanged(object sender, EventArgs e)
    {

        tb_successfull_confirmation.Visible = false;

        if (ddl_notice.SelectedValue == "1")
        {
            MultiView1.ActiveViewIndex = 0;
            

        }

        if (ddl_notice.SelectedValue == "2")
        {
            MultiView1.ActiveViewIndex = 1;
        }


        if (ddl_notice.SelectedValue == "3")
        {
            MultiView1.ActiveViewIndex = 2;
        }



    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_preview_1_Click(object sender, EventArgs e)
    {
        Server.Transfer("../notice/notice_delequency_preview.aspx", true);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_preview_2_Click(object sender, EventArgs e)
    {

        Server.Transfer("../notice/notice_delequency_preview.aspx", true);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_preview_3_Click(object sender, EventArgs e)
    {
        Server.Transfer("../notice/notice_delequency_preview.aspx", true);
    }
}
