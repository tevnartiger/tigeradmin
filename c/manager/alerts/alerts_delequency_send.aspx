﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="alerts_delequency_send.aspx.cs" Inherits="manager_alerts_alerts_delequency_send" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
        <b><asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_u_notice_letter %>"></asp:Label></b>
    <br /><br />


    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_letter_email %>"></asp:Label> :
    <asp:DropDownList ID="ddl_notice" AutoPostBack="true" runat="server" 
        onselectedindexchanged="ddl_notice_SelectedIndexChanged">
        <asp:ListItem Text="<%$ Resources:Resource, lbl_select_notice %>" Value="0"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:Resource, lbl_first_notice %>" Value="1"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:Resource, lbl_second_notice %>" Value="2"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:Resource, lbl_third_notice %>" Value="3"></asp:ListItem>
    </asp:DropDownList>
    <br />

  
    <table id="tb_successfull_confirmation" runat="server" style="width: 100%">
      <tr>
         <td style="color: #FF6600">
                            <b>
             <asp:Label ID="Label15" runat="server" Text="<%$ Resources:Resource, lbl_successfull_confirmation %>"></asp:Label></b></td>
      </tr>
   </table>
                

  
    
      
   <asp:MultiView ID="MultiView1" runat="server">  
   <asp:View ID="View1" runat="server">
    <table id="tb_notice_1" visible="true" runat="server" style="width: 100%">
    
    <tr>
            <td>
                <table style="width: 33%">
                    <tr>
                        <td>
                <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lbl_date_of_notice %>"></asp:Label>
                        </td>
                        <td>
                <asp:Label ID="lbl_notice_date" runat="server" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lbl_dear %>"></asp:Label>
                        </td>
                        <td>
                            <asp:Repeater ID="r_tenant_list" runat="server">
                            <ItemTemplate>
                            <%#Eval("name_lname")%> &nbsp;,&nbsp;
                            <%#Eval("name_fname")%><br />
                            </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
                
                
            </td>
        </tr>
        <tr>
            <td>
               
                <table style="width: 90%">
                    <tr>
                        <td valign="top">
                <asp:Button ID="btn_preview_1" runat="server" 
                    Text="<%$ Resources:Resource, btn_preview %>" onclick="btn_preview_1_Click" />
                        </td>
                        <td valign="top">
                            <asp:Label ID="Label22" runat="server" 
                    Text="<%$ Resources:Resource, lbl_send_email %>"></asp:Label>                
                            &nbsp;<asp:RadioButtonList ID="radio_send_mail_1" runat="server" 
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="0">No</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                </asp:RadioButtonList>
                        </td>
                        <td valign="top">
                            <asp:Button ID="btn_confirm" runat="server" onclick="btn_confirm_Click" Text="<%$ Resources:Resource, btn_confirm_notice %>" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <textarea id="tbx_first_notice"  runat="server" cols="20" rows="2"></textarea>
      
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
    </table>
&nbsp;

    
    </asp:View>
    
    
    
    
    <asp:View ID="View2" runat="server">
    <table id="tb_notice_2" visible="true" runat="server" style="width: 100%">
    
    <tr>
            <td>
                <table style="width: 100%">
                    <tr>
                        <td valign="top">
                <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lbl_date_of_notice %>"></asp:Label>
                        </td>
                        <td valign="top">
                <asp:Label ID="lbl_notice_date_2" runat="server" ></asp:Label>
                            <br />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:Label ID="Label10" runat="server" 
                                Text="<%$ Resources:Resource, lbl_important_notice %>" style="font-weight: 700"></asp:Label>
                            &nbsp;-<br />
                        </td>
                        <td valign="top">
                            <asp:Label ID="Label11" runat="server" 
                                Text="<%$ Resources:Resource, lbl_unpaid_amount %>" style="font-weight: 700"></asp:Label>  &nbsp;:&nbsp;<asp:Label ID="lbl_amount_owed" runat="server" Text="Label"></asp:Label> &nbsp;,&nbsp;<asp:Label 
                                ID="Label12" runat="server" Text="<%$ Resources:Resource, lbl_late_fee %>" 
                                style="font-weight: 700"></asp:Label>&nbsp;:&nbsp;<asp:Label
                                ID="lbl_late_fee" runat="server" ></asp:Label>&nbsp;,&nbsp;<asp:Label ID="Label8"
                                    runat="server" Text="<%$ Resources:Resource, lbl_total %>" 
                                style="font-weight: 700"></asp:Label>&nbsp;:&nbsp;<asp:Label
                                        ID="lbl_total" runat="server" Text="Label"></asp:Label>&nbsp;&nbsp;
                            <asp:Label ID="Label24" runat="server" 
                                Text="<%$ Resources:Resource, lbl_days_late%>" style="font-weight: 700"></asp:Label>
                        &nbsp;<asp:Label ID="lbl_days_late_2" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, lbl_dear %>"></asp:Label>
                        </td>
                        <td>
                            <asp:Repeater ID="r_tenant_list_2" runat="server">
                            <ItemTemplate>
                            <%#Eval("name_lname")%> &nbsp;,&nbsp;
                            <%#Eval("name_fname")%><br />
                            </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
            
            </td>
        </tr>
        
        
        <tr>
            <td>
                <table style="width: 100%">
                    <tr>
                        <td valign="top">
                <asp:Button ID="btn_preview_2" runat="server" 
                    Text="<%$ Resources:Resource,btn_preview %>" onclick="btn_preview_2_Click" />
                        </td>
                        <td valign="top">
                            <asp:Label ID="Label21" runat="server" 
                    Text="<%$ Resources:Resource, lbl_send_email %>"></asp:Label>                
                &nbsp;<asp:RadioButtonList ID="radio_send_mail_2" runat="server" 
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="0">No</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                </asp:RadioButtonList>
                        </td>
                        <td valign="top">
                            <asp:Button ID="btn_confirm_2" runat="server" onclick="btn_confirm_2_Click" Text="<%$ Resources:Resource, btn_confirm_notice %>" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        
        <tr>
            <td>
                <asp:TextBox ID="tbx_second_notice" runat="server" Height="320px" 
                    TextMode="MultiLine" Width="544px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
              
                </td>
        </tr>
    </table>

    
    </asp:View>
    
    
    
    
    
    <asp:View ID="View3" runat="server">
      <table id="tb_notice_3" visible="true" runat="server" style="width: 100%">
    
    <tr>
            <td>
                <table style="width: 100%">
                    <tr>
                        <td>
                <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lbl_date_of_notice %>"></asp:Label>
                        </td>
                        <td>
                <asp:Label ID="lbl_notice_date_3" runat="server" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:Label ID="Label13" runat="server" 
                                Text="<%$ Resources:Resource, lbl_urgent_notice %>" style="font-weight: 700"></asp:Label>
                            &nbsp;-<br />
                        </td>
                        <td valign="top">
                            <asp:Label ID="Label14" runat="server" 
                                Text="<%$ Resources:Resource, lbl_unpaid_amount %>" style="font-weight: 700"></asp:Label>  &nbsp;:&nbsp;<asp:Label ID="lbl_amount_owed_2" runat="server" ></asp:Label> &nbsp;,&nbsp;<asp:Label 
                                ID="Label16" runat="server" Text="<%$ Resources:Resource, lbl_late_fee %>" 
                                style="font-weight: 700"></asp:Label>&nbsp;:&nbsp;<asp:Label
                                ID="lbl_late_fee_2" runat="server" ></asp:Label>&nbsp;,&nbsp;<asp:Label ID="Label18"
                                    runat="server" Text="<%$ Resources:Resource, lbl_total %>" 
                                style="font-weight: 700"></asp:Label>&nbsp;:&nbsp;<asp:Label
                                        ID="lbl_total_2" runat="server" Text="Label"></asp:Label>&nbsp;
                            <asp:Label ID="Label23" runat="server" 
                                Text="<%$ Resources:Resource, lbl_days_late%>" style="font-weight: 700"></asp:Label>
                        &nbsp;<asp:Label ID="lbl_days_late_3" runat="server" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, lbl_dear %>"></asp:Label>
                        </td>
                        <td>
                            <asp:Repeater ID="r_tenant_list_3" runat="server">
                            <ItemTemplate>
                            <%#Eval("name_lname")%> &nbsp;,&nbsp;
                            <%#Eval("name_fname")%><br />
                            </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
                <br />
            </td>
        </tr>
        
        
         <tr>
            <td>
             
                <table style="width: 60%">
                    <tr>
                        <td valign="top">
                <asp:Button ID="btn_preview_3" runat="server" 
                    Text="<%$ Resources:Resource,btn_preview %>" onclick="btn_preview_3_Click" />
                        </td>
                        <td valign="top">
                            &nbsp;&nbsp;<asp:Label ID="Label20" runat="server" 
                    Text="<%$ Resources:Resource, lbl_send_email %>"></asp:Label>                
                &nbsp;<asp:RadioButtonList ID="radio_send_mail_3" runat="server" 
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="0">No</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                </asp:RadioButtonList>
                        </td>
                        <td valign="top">
                <asp:Button ID="btn_confirm_3" runat="server" onclick="btn_confirm_3_Click" Text="<%$ Resources:Resource, btn_confirm_notice %>" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        
        <tr>
            <td>
                <asp:TextBox ID="tbx_third_notice" runat="server" Height="320px" 
                    TextMode="MultiLine" Width="544px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
    </table>
  
    
    </asp:View>
    
    
    
    
    
    <asp:View ID="View4" runat="server">
    
    
        <table id="tb_preview" runat="server" style="width: 60%">
            <tr>
                <td>
                    <asp:Label ID="lbl_preview" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    
    </asp:View>

   
   </asp:MultiView> 
    


    <asp:HiddenField Visible="false"  ID="h_tu_id" runat="server" />
    <asp:HiddenField Visible="false"  ID="h_rp_id" runat="server" />
    <asp:HiddenField Visible="false"  ID="h_tenant_id" runat="server" />

    


</asp:Content>

