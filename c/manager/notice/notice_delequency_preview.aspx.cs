﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class manager_notice_notice_delequency_preview : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DateTime la_date = new DateTime();
            la_date = DateTime.Now;

            lbl_notice_date.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();
           

            string notice_number = "";
            notice_number = Request.Form["ctl00$ContentPlaceHolder1$ddl_notice"].ToString();

            int tu_id = Convert.ToInt32(Request.Form["ctl00$ContentPlaceHolder1$h_tu_id"]) ;
            int tenant_id = Convert.ToInt32(Request.Form["ctl00$ContentPlaceHolder1$h_tenant_id"]);
            int rp_id = Convert.ToInt32(Request.Form["ctl00$ContentPlaceHolder1$h_rp_id"]);


            if (notice_number == "1")
            {
                div_notice.InnerHtml = Request.Form["ctl00$ContentPlaceHolder1$tbx_first_notice"].ToString().Replace("&lt;", "<").Replace("&gt;", ">");

            }




            if (notice_number == "2")
            {
                div_notice.InnerHtml = Request.Form["ctl00$ContentPlaceHolder1$tbx_second_notice"].ToString().Replace("&lt;", "<").Replace("&gt;", ">");
                Label13.Text = Resources.Resource.lbl_important_notice;
            }




            if (notice_number == "3")
            {
                div_notice.InnerHtml = Request.Form["ctl00$ContentPlaceHolder1$tbx_third_notice"].ToString().Replace("&lt;", "<").Replace("&gt;", ">");
              Label13.Text = Resources.Resource.lbl_urgent_notice;


            }


            double amount_owed = 0;
            int days_late = 0;

            
            ////////////////////////////////
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prTenantUnitNameList", conn);
            SqlCommand cmd2 = new SqlCommand("prRentDelequency", conn);
            SqlCommand cmd3 = new SqlCommand("prLateFee", conn);


            cmd.CommandType = CommandType.StoredProcedure;

            try
            {

                conn.Open();

                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = tenant_id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);


                r_tenant_list.DataSource = dt;
                r_tenant_list.DataBind();
            }
            finally
            {
                //  conn.Close();
            }


            cmd2.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd2.Parameters.Add("@rp_id", SqlDbType.Int).Value = rp_id;


                SqlDataReader dr = null;
                dr = cmd2.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {

                    amount_owed = Convert.ToDouble(dr["amount_owed"]);

                   
                    // the amount owed written in the second notice letter
                    lbl_amount_owed.Text = String.Format("{0:0.00}", amount_owed);


                    // the number of days late ( rent payment)
                    lbl_days_late.Text = dr["days"].ToString();
                }
            }
            finally
            {
                //  conn.Close();
            }




            cmd3.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd3.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;


                SqlDataReader dr3 = null;
                dr3 = cmd3.ExecuteReader(CommandBehavior.SingleRow);

                while (dr3.Read() == true)
                {

                    double late_fee = Convert.ToDouble(dr3["tt_late_fee"]);
                    double total = 0;

                    // the late fee written in the second notice letter
                    lbl_late_fee.Text = String.Format("{0:0.00}", late_fee);


                    total = late_fee + amount_owed;

                    lbl_total.Text = String.Format("{0:0.00}", total);
                    

                }
            }
            finally
            {
                conn.Close();
            }



            ///////////////////////////////





        }
    }
}
