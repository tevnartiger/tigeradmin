using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class manager_uc_uc_content_menu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string menu = "";
        switch (System.IO.Path.GetFileName(Request.Path.ToString()))
        {
            case "man_default.aspx":
            case "man_mod_pwd.aspx":
                menu += "<h3>Dashboard <h3/>&nbsp;&nbsp;";
                menu += "<a style='color: #585880;' href='man_mod_pwd.aspx'>"+Resources.Resource.lbl_mod_pwd+" </a>&nbsp;&nbsp;";
            break;
        /*    case "group_add.aspx":
            case "group_modify.aspx":
            case "group_profile.aspx":
            case "group_remove.aspx":
                menu += "<a style='color: #585880;' href='group_profile.aspx'>" + Resources.Resource.lbl_group_profile + " </a>&nbsp&nbsp;";
                menu += "<a style='color: #585880;' href='group_add.aspx'>" + Resources.Resource.lbl_group_create + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #585880;' href='group_modify.aspx'>" + Resources.Resource.lbl_group_modify + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #585880;' href='group_remove.aspx'>" + Resources.Resource.lbl_gl_remove + " </a>&nbsp;&nbsp;";
          */


            /*----------------------------------------------------------------------------------------------------------*/
            /*----------------------------------------------------------------------------------------------------------*/


            case "appliance_default.aspx":
            case "appliance_moving.aspx":
            case "appliance_moving_wiz.aspx":
            case "appliance_add.aspx":
            case "appliance_update.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='appliance_default.aspx'>Inventory</a>&nbsp;&nbsp;";
            break;

            /*----------------------------------------------------------------------------------------------------------*/
            /*----------------------------------------------------------------------------------------------------------*/
 
            case "property_view.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_default.aspx'>Manage Properties</a>&nbsp;<b>&rang;<b/>&nbsp;";
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_list.aspx'>Property List</a>&nbsp;<b>&rang;<b/>&nbsp;";
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_view.aspx?h_id=" + Request.QueryString["h_id"] + "'>Property details </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;font-size:9pt;' href='property_update.aspx?h_id=" + Request.QueryString["h_id"] + "'>" + Resources.Resource.lbl_gl_update + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;font-size:9pt;' href='property_unit_list.aspx?h_id=" + Request.QueryString["h_id"] + "'>" + Resources.Resource.lbl_gl_unit_list + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;font-size:9pt;' href='property_unit_add.aspx?h_id=" + Request.QueryString["h_id"] + "'>" + Resources.Resource.lbl_gl_unit_add + " </a>&nbsp;&nbsp;";
            break;


            case "property_add.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_default.aspx'>" + Resources.Resource.lbl_property + "</a>&nbsp;<b>&rang;<b/>&nbsp;";
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_add.aspx'>New Property</a>&nbsp;&nbsp;";
            break;

            case "property_update.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_default.aspx'>Manage Properties</a>&nbsp;<b>&rang;<b/>&nbsp;";
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_list.aspx'>Property List</a>&nbsp;<b>&rang;<b/>&nbsp;";
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_update.aspx?h_id=" + Request.QueryString["h_id"] + "'>Property Update </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;font-size:9pt;' href='property_view.aspx?h_id=" + Request.QueryString["h_id"] + "'>Property details </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;font-size:9pt;' href='property_unit_list.aspx?h_id=" + Request.QueryString["h_id"] + "'>" + Resources.Resource.lbl_gl_unit_list + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;font-size:9pt;' href='property_unit_add.aspx?h_id=" + Request.QueryString["h_id"] + "'>" + Resources.Resource.lbl_gl_unit_add + " </a>&nbsp;&nbsp;";
            break;


            case "property_unit_add.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_default.aspx'>Manage Properties</a>&nbsp;<b>&rang;<b/>&nbsp;";
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_list.aspx'>Property List</a>&nbsp;<b>&rang;<b/>&nbsp;";
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_view.aspx?h_id=" + Request.QueryString["h_id"] + "'>Property details </a>&nbsp;<b>&rang;<b/>&nbsp;";
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_list.aspx'>New units</a>&nbsp;&nbsp;";
            break;
           
            case "property_list.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_default.aspx'>Manage Properties</a>&nbsp;<b>&rang;<b/>&nbsp;";
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_list.aspx'>Property List</a>&nbsp;&nbsp;";
            break;


            case "property_unit_list.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_default.aspx'>Manage Properties</a>&nbsp;<b>&rang;<b/>&nbsp;";
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_list.aspx'>Property list </a>&nbsp;<b>&rang;<b/>&nbsp;";
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_unit_list.aspx?h_id=" + Request.QueryString["h_id"] + "'>Unit List </a>&nbsp;";
            break;
            
            case "property_default.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_default.aspx'>Manage Properties</a>&nbsp;&nbsp;";
            break;

            case "property_profile.aspx":
                menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_default.aspx'>" + Resources.Resource.lbl_property + "</a>&nbsp;/&nbsp;";
                menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='property_profile.aspx'>Portfolio</a>&nbsp;&nbsp;:&nbsp;";
                menu += "<a style='color: #585880;font-size:9pt;' href='property_list.aspx'>Property list </a>&nbsp;&nbsp;";
                menu += "<a style='color: #585880;font-size:9pt;' href='property_add.aspx'>New Property </a>&nbsp;&nbsp;";
                menu += "<a style='color: #585880;font-size:9pt;' href='property_unit_list.aspx'>Unit List </a>&nbsp;&nbsp;";
                menu += "<a style='color: #585880;font-size:9pt;' href='property_unit_add.aspx'>New Unit</a>&nbsp;&nbsp;";
                menu += "<a style='color: #585880;font-size:9pt;' href='property_unit_add.aspx'>Mortgages</a>&nbsp;&nbsp;";
                menu += "<a style='color: #585880;font-size:9pt;' href='property_unit_add.aspx'>Insurances</a>&nbsp;&nbsp;";
               
            break;
      /*----------------------------------------------------------------------------------------------------------*/
      /*----------------------------------------------------------------------------------------------------------*/

            case "role_wiz.aspx":
            case "role_update.aspx":
            case "user_add.aspx":
            case "user_default.aspx":
            case "role_create_account.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/manager/user/user_default.aspx'>User</a>&nbsp;&nbsp;";
            break;


            case "tenant_default.aspx":
            case "tenant_view.aspx":
            case "tenant_search.aspx":
            case "tenant_prospect_list.aspx":
            case "tenant_prospect_add.aspx":
            case "tenant_list.aspx":
            case "tenant_update.aspx":
                menu += "<a style='color: #585880;' href='tenant_default.aspx'>" + Resources.Resource.lbl_gl_home + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #585880;' href='tenant_view.aspx?n_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_view + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #585880;' href='tenant_update.aspx?h_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_update + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #585880;' href='tenant_prospect_add.aspx'>" + Resources.Resource.lbl_gl_prospect_add + " </a>&nbsp;&nbsp;";
                menu += "<a style='color: #585880;' href='tenant_prospect_add.aspx'>" + Resources.Resource.lbl_gl_tenant_delinquant + " </a>&nbsp;&nbsp;";

                menu += "<a style='color: #585880;' href='tenant_search.aspx?h=0&l='>" + Resources.Resource.lbl_gl_search + " </a>&nbsp;&nbsp;";
                
                menu += "<a style='color: #585880;' href='manager/group/profile_view.aspx?n_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_profile + " </a>&nbsp;&nbsp;";
            break;
            case "lease_add.aspx":
            case "lease_view.aspx":
            case "lease_list.aspx":
            case "timeline_lease.aspx":
            case "lease_rent_update.aspx":
            case "lease_term_cond_update.aspx":
            case "lease_accommodation_update.aspx":

            menu += "<a style='color: #585880;' href='/manager/lease/lease_list.aspx?n_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_view + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/lease/lease_add.aspx?h_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/timeline/timeline_lease.aspx?n_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_timeline + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/lease/lease_accommodation_update.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_lease_update_accomodation + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/lease/lease_rent_update.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_lease_update_rent + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/lease/lease_term_cond_update.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_lease_update_terms + " </a>&nbsp;&nbsp;";
            break;

            /*----------------------------------------------------------------------------------------------------------*/
            /*----------------------------------------------------------------------------------------------------------*/

    
            case "alerts.aspx":
            case "alerts_setup.aspx":
            menu += "<h1>" + Resources.Resource.lbl_gl_alert + "</h1>";
            break;
            case "default2.aspx":
            menu += "<a style='color: #585880; font-size:11pt; font-weight:bold;";
            menu += "text-decoration:none ' href='/manager/scheduler/default2.aspx'>Event Calendar</a>&nbsp;&nbsp;";
            break;
            
            case "incident_list.aspx":
            case "appliance_list.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='appliance_default.aspx'>Inventory</a>&nbsp;<b>&rang;<b/>&nbsp;";
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='appliance_list.aspx'>Items</a>&nbsp;&nbsp;&nbsp;";
            menu += "<a style='color: #585880;font-size:9pt;' href='appliance_add.aspx'>New Item</a>&nbsp;&nbsp;";
            break;



            case "filemanager_default.aspx":
            case "filemanager_upload.aspx":
            case "filemanager.aspx":
            menu += "<a style='color: #585880; font-size:16px; font-weight:bold;";
            menu += "text-decoration:none ' href='/manager/filemanager/filemanager.aspx'>" + "Documents" + " </a>&nbsp;&nbsp;";
            break;
            case "mortgage_amortization.aspx":
            menu += "<a style='color: #585880;' href='/manager/mortgage/mortgage_amortization.aspx?n_id=" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_mortgage + " </a>&nbsp;&nbsp;";
             break;
             case "home_unit_rented_list.aspx":
            case "tenant_cancel_rent_payment.aspx":
            case "financial_income.aspx":
            case "tenant_rent_update.aspx":




            case "income_late_rent_fees.aspx":
            case "income_report.apsx":
            menu += "<a style='color: #585880;' href='/manager/home/home_unit_rented_list.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_rent_income + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/tenant/tenant_cancel_rent_payment.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_rent_income_update + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/income/income_late_rent_fees.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_rent_late + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/Financial/financial_income.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_income_report + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/Financial/financial_income.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_income_other + " </a>&nbsp;&nbsp;";
            break;
            case "role_member_list.aspx":
            case "role_wiz1.aspx":
            case "role_update1.aspx":
            menu += "<a style='color: #585880;' href='/manager/role/role_member_list.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_role_user + " </a>&nbsp;&nbsp;";
             menu += "<a style='color: #585880;' href='/manager/role/role_wiz.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_role_assign + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/role/role_update.aspx" + Request.QueryString["n_id"] + "'>" + Resources.Resource.lbl_gl_role_update + " </a>&nbsp;&nbsp;";
           
            break;


            case "income_default.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='income_default.aspx'>Income</a>&nbsp;&nbsp;";
            break;
            
            case "financial_expenses.aspx":
            case "financial_report.aspx":
            menu += "<a style='color: #585880;' href='/manager/financial/financial_expense_view.aspx'>" + Resources.Resource.lbl_gl_view + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/financial/financial_expenses.aspx'>" + Resources.Resource.lbl_gl_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/financial/expense_report.aspx'>" + Resources.Resource.lbl_gl_report + " </a>&nbsp;&nbsp;";
           
            break;
            case "company_add.aspx?c=c":
            case "company_add.aspx?c=s":
            case "financial_institution_add.aspx?c=":
            case "insurance_company_add.aspx?c=":
            case "wharehouse_add.aspx?c=":
            menu += "<a style='color: #585880;' href='/manager/contact/contact_list.aspx'>" + Resources.Resource.lbl_gl_view + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/company/company_add.aspx?c=c'>" + Resources.Resource.lbl_gl_contact_contractor_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/company/company_add.aspx?c=s'>" + Resources.Resource.lbl_gl_contact_vendor_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/financialinstitution/financial_institution_add.aspx'>" + Resources.Resource.lbl_gl_contact_fi_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/contact/contact_list.aspx'>" + Resources.Resource.lbl_gl_view + " </a>&nbsp;&nbsp;";
            
            break;
                //rent
            case "tenant_untreated_rent.aspx":
            case "rent_default.aspx":
            menu += "<a style='color: #585880;' href='/manager/rent/rent_default.aspx'>" + Resources.Resource.lbl_gl_home + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/tenant/tenant_untreated_rent.aspx'>" + Resources.Resource.lbl_gl_tenant_untreated + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/'>" + Resources.Resource.lbl_gl_contact_contractor_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/company/company_add.aspx?c=c'>" + Resources.Resource.lbl_gl_contact_contractor_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/company/company_add.aspx?c=c'>" + Resources.Resource.lbl_gl_contact_contractor_add + " </a>&nbsp;&nbsp;";
            break;
            case "company_list.aspx":
            case "company_add.aspx":
            case "warehouse_add.aspx":
            case "insurance_company_add.aspx":

            /*----------------------------------------------------------------------------------------------------------*/
            /*----------------------------------------------------------------------------------------------------------*/

    
            case "financial_institution_add.aspx":
            menu += "<a style='color: #585880;' href='/manager/company/company_add.aspx?c=c'>" + Resources.Resource.lbl_gl_contact_contractor_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/company/company_add.aspx?c=s'>" + Resources.Resource.lbl_gl_contact_contractor_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/financialinstitution/financial_institution_add.aspx?c=c'>Financial institution</a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/insurancecompany/insurance_company_add.aspx?c=c'>Insurance company </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/warehouse/warehouse_add.aspx?c=c'>Warehouse </a>&nbsp;&nbsp;";
            break;


            case "contact_list.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/manager/contact/contact_list.aspx?categ=0&l='>Business Contacts</a>&nbsp;&nbsp;&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/company/company_add.aspx?c=c'>" + Resources.Resource.lbl_gl_contact_contractor_add + " </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/company/company_add.aspx?c=s'>Vendor </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/financialinstitution/financial_institution_add.aspx?c=c'>Financial institution</a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/insurancecompany/insurance_company_add.aspx?c=c'>Insurance company </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/warehouse/warehouse_add.aspx?c=c'>Warehouse </a>&nbsp;&nbsp;";
            break;
            
         

            case "financial_scenario_list.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/manager/tools/tools_default.aspx'>Tools</a>&nbsp;<b>&rang;<b/>&nbsp;";
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/manager/financial/financial_scenario_list.aspx'>Financial Scenario</a>&nbsp;&nbsp;&nbsp;";
            menu += "<a style='color: #585880;font-size:9pt;' href='financial_scenario.aspx'>New Scenario</a>&nbsp;&nbsp;";
            break;


            case "financial_scenario.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/manager/tools/tools_default.aspx'>Tools</a>&nbsp;<b>&rang;<b/>&nbsp;";
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/manager/financial/financial_scenario_list.aspx'>Financial Scenario</a>&nbsp;<b>&rang;<b/>&nbsp;";
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/manager/financial/financial_scenario.aspx'>New Financial Scenario</a>&nbsp;&nbsp;&nbsp;";
            break;

            case "financial_analysis.aspx":
            case "financial_analysis_period.aspx":
            case "financial_group_analysis.aspx":
            case "financial_group_analysis_period.aspx":
            
            menu += "<a style='color: #585880;' href='/manager/financial/financial_analysis.aspx'>by property ( Monthly ) </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/financial/financial_analysis_period.aspx'>by property ( Date Range ) </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/financial/financial_group_analysis.aspx'>by group ( Monthly ) </a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/financial/financial_group_analysis_period.aspx'>by group ( Date Range ) </a>&nbsp;&nbsp;";
            
            menu += "<a style='color: #585880;' href='/manager/financial/financial_scenario_list.aspx'>" + Resources.Resource.lbl_gl_fscenario_list + " </a>&nbsp;&nbsp;";

            /*----------------------------------------------------------------------------------------------------------*/
            /*----------------------------------------------------------------------------------------------------------*/

    

            break;
            case "mortgage_list.aspx":
            case "insurance_policy_list.aspx":
            menu += "<a style='color: #585880;' href='/manager/mortgage/mortgage_list.aspx'>Property mortgage</a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/insurancepolicy/insurance_policy_list.aspx'>Property Insurance Policy </a>&nbsp;&nbsp;";
            break;
            case "name_profile.aspx":
            case "name_search.aspx":
            case "group_default.aspx":
            case "group_modify.aspx":
            case "group_profile.aspx":
            case "group_remove.aspx":
           
            menu += "<a style='color: #585880;' href='/manager/group/group_default.aspx'>Profiles home</a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/group/group_profile.aspx'>Property</a>&nbsp;&nbsp;";
            menu += "<a style='color: #585880;' href='/manager/name/name_profile.aspx'>Person </a>&nbsp;&nbsp;";
            //menu += "<a style='color: #585880;' href='/manager/name/name_search.aspx'>Search </a>&nbsp;&nbsp;";
            //menu += "<a style='color: #585880;' href='/manager/name/name_img_upload.aspx'>Add/Modify image</a>&nbsp;&nbsp;";
        
                break;




            /*----------------------------------------------------------------------------------------------------------*/
            /*----------------------------------------------------------------------------------------------------------*/

    
            case "tools_default.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='tools_default.aspx'>Tools</a>&nbsp;&nbsp;";
            break;

            case "tools_amortization.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='tools_default.aspx'>Tools</a>&nbsp;<b>&rang;<b/>&nbsp;";
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='tools_amortization.aspx'>Mortgage Amortization</a>&nbsp;&nbsp;";
            break;

            case "default.aspx":

            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='default.aspx'>Dashboard</a>&nbsp;";
            
                break;
         }
        //menu += "<FORM method='get'><INPUT type='button' value='Help' class='letter' onClick='window.open('http://www.familyship.com/private/familyship_help.php?name=member_lineage','mywindow','width=300,height=400,scrollbars=yes,screenX=0,screenY=0')'></FORM>";
        content_menu.InnerHtml = menu;

    }
}
