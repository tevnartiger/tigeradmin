﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_adm_lmenu.ascx.cs" Inherits="manager_uc_uc_adm_lmenu" %>
  
    
    <asp:TreeView ID="TreeView1" runat="server"    
    LineImagesFolder="~/TreeLineImages" Font-Size="Smaller" Font-Underline="False" 
    ImageSet="Msdn">
        <ParentNodeStyle Font-Bold="False" />
        <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
        <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" 
            HorizontalPadding="0px" VerticalPadding="0px" />
    <Nodes>
        <asp:TreeNode Text="WIZARD" Value="Wizard">
            <asp:TreeNode NavigateUrl="~/manager/property/property_add.aspx" 
                ShowCheckBox="False" Text="New Property" Value="New Property">
            </asp:TreeNode>
            <asp:TreeNode NavigateUrl="~/manager/lease/lease_add.aspx" Text="New Lease" 
                Value="New Lease"></asp:TreeNode>
            <asp:TreeNode NavigateUrl="~/manager/lease/lease_list.aspx" 
                Text="Commit Rent Payment" Value="Commit Rent Payment"></asp:TreeNode>
            <asp:TreeNode NavigateUrl="~/manager/income/income_default.aspx" 
                Text="Commit Expenses" Value="Commit Expenses"></asp:TreeNode>
        </asp:TreeNode>
        <asp:TreeNode Text="MANAGE" Value="MANAGE">
            <asp:TreeNode NavigateUrl="~/manager/property/property_default.aspx" 
                Text="Property" Value="Property"></asp:TreeNode>
            <asp:TreeNode NavigateUrl="~/manager/tenant/tenant_default.aspx" Text="Tenant" 
                Value="Tenant"></asp:TreeNode>
            <asp:TreeNode NavigateUrl="~/manager/lease/lease_list.aspx" Text="Lease" 
                Value="Lease"></asp:TreeNode>
            <asp:TreeNode NavigateUrl="~/manager/income/income_default.aspx" Text="Income" 
                Value="Income"></asp:TreeNode>
            <asp:TreeNode NavigateUrl="~/manager/Financial/financial_expenses.aspx" 
                Text="Expenses" Value="Expenses"></asp:TreeNode>
            <asp:TreeNode NavigateUrl="~/manager/rent/rent_default.aspx" 
                Text="Rent Payment" Value="Rent Payment"></asp:TreeNode>
           <asp:TreeNode NavigateUrl="~/manager/appliance/appliance_list.aspx" 
                Text="Inventory" Value="Inventory"></asp:TreeNode>
        </asp:TreeNode>
        <asp:TreeNode  Text="REPORTS" Value="REPORTS">
            <asp:TreeNode  NavigateUrl="~/manager/tenant/tenant_cancel_rent_payment.aspx" 
                Text="Rents" Value="Rents"></asp:TreeNode>
            <asp:TreeNode NavigateUrl="~/manager/Financial/financial_analysis.aspx" 
                Text="Cash Flow" Value="Cash Flow"></asp:TreeNode>
            <asp:TreeNode NavigateUrl="~/manager/Financial/financial_scenario_list.aspx" 
                Text="Financial Scenario" Value="Financial Scenario"></asp:TreeNode>
        </asp:TreeNode>
        <asp:TreeNode NavigateUrl="~/manager/alerts/alerts.aspx" Text="DAILY TASKS" 
            Value="DAILY TASKS">
            <asp:TreeNode Text="Alert" Value="Alert"></asp:TreeNode>
            <asp:TreeNode NavigateUrl="~/manager/Scheduler/default2.aspx" Text="Calendar" 
                Value="Calendar"></asp:TreeNode>
            <asp:TreeNode NavigateUrl="~/manager/incident/incident_list.aspx" 
                Text="Incident" Value="Incident"></asp:TreeNode>
            <asp:TreeNode NavigateUrl="~/manager/workorder/wo_list.aspx" Text="Work Order" 
                Value="Work Order"></asp:TreeNode>
            <asp:TreeNode NavigateUrl="~/manager/filemanager/filemanager.aspx" 
                Text="Files &amp; Documents" Value="Files &amp; Documents"></asp:TreeNode>
        </asp:TreeNode>
        <asp:TreeNode Text="OTHERS" Value="OTHERS">
            <asp:TreeNode NavigateUrl="~/manager/company/company_list.aspx" 
                Text="Companies" Value="Companies"></asp:TreeNode>
            <asp:TreeNode NavigateUrl="~/manager/mortgage/mortgage_amortization.aspx" 
                Text="Tools" Value="Tools"></asp:TreeNode>
            <asp:TreeNode NavigateUrl="~/manager/mortgage/mortgage_list.aspx" 
                Text="Mortgage &amp; Insurance" Value="Mortgage &amp; Insurance">
            </asp:TreeNode>
        </asp:TreeNode>
    </Nodes>
        <NodeStyle Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" 
            HorizontalPadding="5px" NodeSpacing="0px" VerticalPadding="0px" />
</asp:TreeView>

<br /><br />
    
    
<style type="text/css">
			body {
				font:11px Verdana;
				color:#333333;
			}
			a {
				font:11px Verdana;
				color:#315686;
				text-decoration:underline;
			}
			a:hover {
				color:#315686;
			}
		</style>
 		

<!--<div id="lm" runat="server" />-->
 
			


  
