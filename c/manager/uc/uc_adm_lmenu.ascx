<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_adm_lmenu.ascx.cs" Inherits="manager_uc_uc_adm_lmenu" %>
  
<asp:Panel ID="panel_default" runat="server">
<div  runat="server"  style="text-align: left; width: 170px;">
<h4>Quick Links</h4>
<div  runat="server"  class="LeftMNav"  >
<ul  runat="server" >
<li><a title="" href="/manager/Financial/financial_analysis.aspx">�&nbsp;Cash Flow</a></li>
<li><a title="" href="/manager/property/property_add.aspx">�&nbsp;New Property</a></li>
<li><a title="" href="/manager/lease/lease_add.aspx">�&nbsp;New Lease</a></li>
<li><a title="" href="/manager/tenant/tenant_cancel_rent_payment.aspx">�&nbsp;Paid rent List</a></li>
<li><a title="" href="/manager/reports/reports_delequencies.aspx">�&nbsp;Delequencies</a></li>
<li><a title="" href="/manager/property/property_available_units.aspx">�&nbsp;Vacancies</a></li>
</ul>    
</div>
</div>
<br runat="server"  />
<div runat="server"  style="text-align: left; width: 170px;">
<h4>Manage</h4>
<div  runat="server" class="LeftMNav" >
<ul   runat="server">
<li><asp:HyperLink ID="HyperLink4" NavigateUrl="~/manager/incident/incident_list.aspx" Text="�&nbsp;Incidents" runat="server" /></li>
<li><asp:HyperLink ID="HyperLink5" NavigateUrl="~/manager/workorder/wo_list.aspx" Text="�&nbsp;Work Order" runat="server" /></li>
<li><asp:HyperLink ID="HyperLink8" NavigateUrl="~/manager/appliance/appliance_default.aspx" Text="�&nbsp;Inventory" runat="server" /></li>
<li><asp:HyperLink ID="HyperLink9" NavigateUrl="~/manager/filemanager/filemanager_default.aspx" Text="�&nbsp;Documents" runat="server" /></li>
<li><asp:HyperLink ID="HyperLink2" NavigateUrl="~/manager/user/user_default.aspx" Text="�&nbsp;User" runat="server" /></li>
<li><asp:HyperLink ID="HyperLink6" NavigateUrl="~/manager/tools/tools_default.aspx" Text="�&nbsp;Tools" runat="server" /></li>                 
<li><asp:HyperLink ID="HyperLink7" NavigateUrl="~/manager/reports/reports_default.aspx" Text="�&nbsp;Reports" runat="server" /></li>




</ul>    
</div>
</div>
 </asp:Panel>
 
 
 
 
 
<asp:Panel ID="panel_property" runat="server">
<div runat="server" style="text-align: left; width: 170px;">
<h4>Property</h4>
<div runat="server" class="LeftMNav" >
<ul  runat="server">
<li><a title="" href="/manager/property/property_list.aspx">�&nbsp;<asp:Literal ID="Literal5" Text="Property List" runat="server" /></a></li>
<li><a title="" href="/manager/group/group_profile.aspx">�&nbsp;  <asp:Literal ID="Literal8" Text="Profile" runat="server" /></a></li>
<li><a title="" href="/manager/property/property_add.aspx">�&nbsp;  <asp:Literal ID="Literal4" Text="New Property" runat="server" /></a></li>
<li><a title="" href="/manager/mortgage/mortgage_list.aspx">�&nbsp; <asp:Literal ID="Literal3" Text="Mortgages" runat="server" /></a></li>
<li><a title="" href="/manager/insurancepolicy/insurance_policy_list.aspx">�&nbsp; <asp:Literal ID="Literal7" Text="Insurances" runat="server" /></a></li>
</ul>    
</div>
</div>
</asp:Panel>
 
 




<asp:Panel ID="panel_portfolio" runat="server">
<div   runat="server" style="text-align: left; width: 170px;">
<h4>Portfolio</h4>
<div   runat="server" class="LeftMNav" >
<ul    runat="server">
<li><a title="" href="/manager/portfolio/portfolio_add.aspx">�&nbsp; <asp:Literal ID="Literal6" Text="New Portfolio" runat="server" /></a></li>
<li><a title="" href="/manager/portfolio/portfolio_modify.aspx">�&nbsp;<asp:Literal ID="Literal1" Text="Edit Portfolio" runat="server" /></a></li>
<li><a title="" href="/manager/portfolio/portfolio_remove.aspx">�&nbsp;<asp:Literal ID="Literal2" Text="Delete Portfolio" runat="server" /></a></li>
<li><a title="" href="/manager/portfolio/portfolio_profile.aspx">�&nbsp;<asp:Literal ID="Literal38" Text="Portfolio Profile" runat="server" /></a></li>
</ul>    
</div>
</div>
</asp:Panel>


<asp:Panel ID="panel_tenant" runat="server">
<div runat="server" style="text-align: left; width: 170px;">
<h4>Tenant</h4>
<div runat="server" class="LeftMNav" >
<ul irunat="server">
<li><a title="" href="/manager/tenant/tenant_search.aspx?h=0&l=">�&nbsp;Tenant List</a></li>
<li><a title="" href="/manager/tenant/tenant_prospect_add.aspx">�&nbsp;Add prostective tenant</a></li>
<li><a title="" href="/manager/reports/reports_delequencies.aspx">�&nbsp;Delequencies</a></li>
</ul>    
</div>
</div>
</asp:Panel>



<asp:Panel ID="panel_lease" runat="server">
<div runat="server" style="text-align: left; width: 170px;">
<h4>Lease</h4>
<div runat="server" class="LeftMNav" >
<ul  runat="server">
<li><a title="" href="/manager/lease/lease_list.aspx">�&nbsp;<asp:Literal ID="Literal14" Text="Lease list" runat="server" /></a></li>
<li><a title="" href="/manager/lease/lease_add.aspx">�&nbsp;<asp:Literal ID="Literal9" Text="New Lease" runat="server" /></a></li>
<li><a title="" href="/manager/lease/lease_rent_update.aspx">�&nbsp;<asp:Literal ID="Literal10" Text="Edit rent amount" runat="server" /></a></li>
<li><a title="" href="/manager/lease/lease_accommodation_update.aspx">�&nbsp;<asp:Literal ID="Literal11" Text="Edit accomodations" runat="server" /></a></li>
<li><a title="" href="/manager/lease/lease_term_cond_update.aspx">�&nbsp;<asp:Literal ID="Literal12" Text="Edit terms & conditions" runat="server" /></a></li>
<li><a title="" href="/manager/timeline/timeline_lease.aspx">�&nbsp; <asp:Literal ID="Literal13" Text="View Time line" runat="server" /></a></li>
</ul>    
</div>
</div>
</asp:Panel>

<asp:Panel ID="panel_income" runat="server">
<div runat="server" style="text-align: left; width: 170px;">
<h4>Income</h4>
<div runat="server" class="LeftMNav" >
<ul runat="server">
<li><a title="" href="/manager/home/home_unit_rented_list.aspx">�&nbsp;<asp:Literal ID="Literal15" Text="Rent Payment" runat="server" /></a></li>
<li><a title="" href="/manager/financial/financial_income.aspx">�&nbsp;<asp:Literal ID="Literal16" Text="Other Income" runat="server" /></a></li>
<li><a title="" href="/manager/income/income_late_rent_fees.aspx">�&nbsp; <asp:Literal ID="Literal18" Text="Record Late Rent Fee" runat="server" /></a></li>
<li><a title="" href="/manager/income/income_received_late_rent_fees.aspx">�&nbsp;<asp:Literal ID="Literal17" Text="Late Rent Fee" runat="server" /></a></li>
<li><a title="" href="/manager/tenant/tenant_untreated_rent.aspx">�&nbsp;<asp:Literal ID="Literal19" Text="Untreated Rent" runat="server" /></a></li>
</ul>    
</div>
</div>

<br runat="server" />
<div runat="server" style="text-align: left; width: 170px;">
<h4>Expenses</h4>
<div runat="server" class="LeftMNav" >
<ul  runat="server">
<li><a title="" href="/manager/Financial/financial_expenses.aspx">�&nbsp;Record expenses</a></li>
</ul>    
</div>
</div>
</asp:Panel>



<asp:Panel ID="panel_expense" runat="server">
<div runat="server" style="text-align: left; width: 170px;">
<h4>Expenses</h4>
<div runat="server" class="LeftMNav" >
<ul  runat="server">
<li><a title="" href="/manager/Financial/financial_expenses.aspx">�&nbsp;Record expenses</a></li>
</ul>    
</div>
</div>
<br />
<div runat="server" style="text-align: left; width: 170px;">
<h4>Income</h4>
<div runat="server" class="LeftMNav" >
<ul  runat="server">
<li><a title="" href="/manager/home/home_unit_rented_list.aspx">�&nbsp;<asp:Literal ID="Literal24" Text="Rent Payment" runat="server" /></a></li>
<li><a title="" href="/manager/financial/financial_income.aspx">�&nbsp;<asp:Literal ID="Literal25" Text="Other Income" runat="server" /></a></li>
<li><a title="" href="/manager/income/income_late_rent_fees.aspx">�&nbsp; <asp:Literal ID="Literal26" Text="Record Late Rent Fee" runat="server" /></a></li>
<li><a title="" href="/manager/income/income_received_late_rent_fees.aspx">�&nbsp;<asp:Literal ID="Literal27" Text="Late Rent Fee" runat="server" /></a></li>
<li><a title="" href="/manager/tenant/tenant_untreated_rent.aspx">�&nbsp;<asp:Literal ID="Literal28" Text="Untreated Rent" runat="server" /></a></li>
</ul>    
</div>
</div>

</asp:Panel>







<asp:Panel ID="panel_inventory" runat="server">
<div runat="server" style="text-align: left; width: 170px;">
<h4>Inventory</h4>
<div  runat="server" class="LeftMNav" >
<ul id="ul1" runat="server">
<li><a title="" href="/manager/appliance/appliance_list.aspx">�&nbsp;<asp:Literal ID="Literal20" Text="Items list" runat="server" /></a></li>
<li><a title="" href="/manager/appliance/appliance_add.aspx">�&nbsp;<asp:Literal ID="Literal21" Text="New Item" runat="server" /></a></li>
<li><a title="" href="/manager/appliance/appliance_moving_wiz.aspx">�&nbsp; <asp:Literal ID="Literal22" Text="Move Items - Wizard" runat="server" /></a></li>
<li><a title="" href="/manager/appliance/appliance_moving.aspx">�&nbsp;<asp:Literal ID="Literal23" Text="Move Items" runat="server" /></a></li>
</ul>    
</div>
</div>
</asp:Panel>



<asp:Panel ID="panel_filemanager" runat="server">
<div runat="server" style="text-align: left; width: 170px;">
<h4>Documents</h4>
<div runat="server" class="LeftMNav" >
<ul  runat="server">
<li><a title="" href="/manager/filemanager/filemanager_myfiles.aspx">�&nbsp;<asp:Literal ID="Literal39" Text="My files" runat="server" /></a></li>
<li><a title="" href="/manager/filemanager/filemanager.aspx">�&nbsp;<asp:Literal ID="Literal29" Text="Shared files" runat="server" /></a></li>
<li><a title="" href="/manager/filemanager/filemanager_upload.aspx">�&nbsp;<asp:Literal ID="Literal30" Text="Upload files" runat="server" /></a></li>
</ul>    
</div>
</div>
</asp:Panel>
 		




<asp:Panel ID="panel_user" runat="server">
<div id="Div1" runat="server" style="text-align: left; width: 170px;">
<h4>User</h4>
<div id="Div3" runat="server" class="LeftMNav" >
<ul id="Ul2"  runat="server">
<li><a title="" href="/manager/user/user_add.aspx">�&nbsp;<asp:Literal ID="Literal31" Text="New User" runat="server" /></a></li>
<li><a title="" href="/manager/role/role_member_list.aspx?categ=0&l=">�&nbsp;  <asp:Literal ID="Literal32" Text="User Profile" runat="server" /></a></li>
<li><a title="" href="/manager/role/role_wiz.aspx">�&nbsp;  <asp:Literal ID="Literal33" Text="Assign a role" runat="server" /></a></li>
<li><a title="" href="/manager/role/role_update.aspx">�&nbsp; <asp:Literal ID="Literal34" Text="Edit User Profile" runat="server" /></a></li>
</ul>    
</div>
</div>
</asp:Panel>
 
 
 
 <asp:Panel ID="panel_tools" runat="server">
<div id="Div4" runat="server" style="text-align: left; width: 170px;">
<h4>Tools</h4>
<div id="Div6" runat="server" class="LeftMNav" >
<ul id="Ul3"  runat="server">
<li><a title="" href="/manager/tools/tools_amortization.aspx">�&nbsp;<asp:Literal ID="Literal35" Text="Mortage Calculator" runat="server" /></a></li>
<li><a title="" href="/manager/financial/financial_analysis_period_list.aspx">�&nbsp;  <asp:Literal ID="Literal36" Text="Range Financial scenario" runat="server" /></a></li>
<li><a title="" href="/manager/tools/financial_scenario_list.aspx">�&nbsp;  <asp:Literal ID="Literal37" Text="Financial scenario" runat="server" /></a></li>
</ul>    
</div>
</div>
</asp:Panel>
 
<!--<div id="lm" runat="server" />-->
 
			


  
