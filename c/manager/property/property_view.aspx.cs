using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.AccountObjectAuthorization;

public partial class property_property_view : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        lbl_url.Text = Request.Url.ToString();

        if (!Request.Url.ToString().Contains("h_id")) 
        {
         //   Session.Abandon();
         //   Response.Redirect("~/login.aspx");
        }

        if (!Request.Url.ToString().Contains("h_id=") || !RegEx.IsInteger(Request.QueryString["h_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        string home = Request.QueryString["h_id"];

        AccountObjectAuthorization homeAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["h_id"])))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
             


         
        // NOT Postback 
        if (!(Page.IsPostBack))
        {

            SetDefaultView();

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prHomeView", conn);
            SqlCommand cmd4 = new SqlCommand("prDimensionView", conn);
            SqlCommand cmd6 = new SqlCommand("prHomeCharView", conn);
            SqlCommand cmd7 = new SqlCommand("prHomeSiteView", conn);
            SqlCommand cmd8 = new SqlCommand("prHomeAllEvaluationList", conn);
            SqlCommand cmd9 = new SqlCommand("prHomeView", conn);
            
            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("home_id", SqlDbType.Int).Value = Convert.ToInt32(home);

            try
            {
                conn.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                rHomeImage.DataSource = ds;
                rHomeImage.DataBind();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                    home_name.Text = dr["home_name"].ToString();
                    home_type.Text = Convert.ToString(dr["home_type"]);
                    home_style.Text = Convert.ToString(dr["home_style"]);
                    home_unit.Text = Convert.ToString(dr["home_unit"]);
                    home_floor.Text = Convert.ToString(dr["home_floor"]);

                    home_matricule.Text = Convert.ToString(dr["home_matricule"]);
                    home_register.Text = Convert.ToString(dr["home_register"]);
                    home_localisation_certificat.Text = Convert.ToString(dr["home_localisation_certificat"]);

                    if (dr["home_date_built"] == DBNull.Value)
                        home_date_built.Text = "";
                    else
                    {
                        DateTime date_built = new DateTime();
                        date_built = Convert.ToDateTime(dr["home_date_built"]);
                        home_date_built.Text = date_built.Month.ToString() + "/" + date_built.Day.ToString() + "/" + date_built.Year.ToString();
                    }


                    if (dr["home_date_purchase"] == DBNull.Value)
                        home_date_built.Text = "";
                    else
                    {
                        DateTime date_purchase = new DateTime();
                        date_purchase = Convert.ToDateTime(dr["home_date_purchase"]);
                        home_date_purchase.Text = date_purchase.Month.ToString() + "/" + date_purchase.Day.ToString() + "/" + date_purchase.Year.ToString();
                    }

                    home_purchase_price.Text = Convert.ToString(dr["home_purchase_price"]);
                    home_living_space.Text = Convert.ToString(dr["home_living_space"]);
                    home_land_size.Text = Convert.ToString(dr["home_land_size"]);
                    home_desc.InnerHtml = Convert.ToString(dr["home_desc"]).Replace("\n", "<br>");
                    home_addr_no.Text = Convert.ToString(dr["home_addr_no"]);
                    home_addr_street.Text = Convert.ToString(dr["home_addr_street"]);

                    home_pc.Text = Convert.ToString(dr["home_pc"]);
                    home_city.Text = Convert.ToString(dr["home_city"]);
                    home_district.Text = Convert.ToString(dr["home_district"]);
                    home_prov.Text = Convert.ToString(dr["home_prov"]);
                }

            }
            finally
            {
              //  conn.Close();
            }

            
          
            cmd4.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd4.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd4.Parameters.Add("home_id", SqlDbType.Int).Value = Convert.ToInt32(home);

            try
            {
              //  conn4.Open();

                SqlDataReader dr4 = null;
                dr4 = cmd4.ExecuteReader(CommandBehavior.SingleRow);

                while (dr4.Read() == true)
                {

                    home_dimension.Text = Convert.ToString(dr4["home_dimension"]);
                    //home_register.Text  = Convert.ToString(dr4["home_register"]);
                    home_area.Text = Convert.ToString(dr4["home_area"]);
                    home_land_dimension.Text = Convert.ToString(dr4["home_land_dimension"]);
                    //home_localisation_certificat.Text = Convert.ToString(dr4["home_localisation_certificat"]);
                    home_land_surface.Text = Convert.ToString(dr4["home_land_surface"]);


                }

            }
            finally
            {
              //  conn4.Close();
            }

            

            cmd6.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd6.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd6.Parameters.Add("home_id", SqlDbType.Int).Value = Convert.ToInt32(home);

            try
            {
               // conn6.Open();

                SqlDataReader dr6 = null;
                dr6 = cmd6.ExecuteReader(CommandBehavior.SingleRow);

                while (dr6.Read() == true)
                {
                    home_fondation.Text = Convert.ToString(dr6["home_fondation"]);
                    home_frame.Text = Convert.ToString(dr6["home_frame"]);
                    home_roof.Text = Convert.ToString(dr6["home_roof"]);
                    home_windows.Text = Convert.ToString(dr6["home_windows"]);
                    home_floors.Text = Convert.ToString(dr6["home_floors"]);
                    home_under_floors.Text = Convert.ToString(dr6["home_under_floors"]);
                    home_wall.Text = Convert.ToString(dr6["home_wall"]);
                    home_water_heater.Text = Convert.ToString(dr6["home_water_heater"]);

                    home_lav_sech.Text = Convert.ToString(dr6["home_lav_sech"]);
                    home_fire_protection.Text = Convert.ToString(dr6["home_fire_protection"]);
                    home_laundry.Text = Convert.ToString(dr6["home_laundry"]);

                    home_parking_int.Text = Convert.ToString(dr6["home_parking_int"]);
                    home_parking_ext.Text = Convert.ToString(dr6["home_parking_ext"]);
                    home_prise_ext.Text = Convert.ToString(dr6["home_prise_ext"]);
                    home_water.Text = Convert.ToString(dr6["home_water"]);
                    home_heating.Text = Convert.ToString(dr6["home_heating"]);
                    home_combustible.Text = Convert.ToString(dr6["home_combustible"]);
                    home_sewage.Text = Convert.ToString(dr6["home_sewage"]);
                    home_ext_finition.Text = Convert.ToString(dr6["home_ext_finition"]);


                    home_parking.Text = Convert.ToString(dr6["home_parking"]);
                    home_particularity.InnerHtml = Convert.ToString(dr6["home_particularity"]).Replace("\n", "<br>");
                   


                }

            }
            finally
            {
            //    conn6.Close();
            }


            cmd7.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd7.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd7.Parameters.Add("home_id", SqlDbType.Int).Value = Convert.ToInt32(home);

            try
            {
             //   conn7.Open();

                SqlDataReader dr7 = null;
                dr7 = cmd7.ExecuteReader(CommandBehavior.SingleRow);

                while (dr7.Read() == true)
                {

                    if (Convert.ToString(dr7["home_site_corner"]) == "1")
                        home_site_corner.Checked = true;
                    if (Convert.ToString(dr7["home_site_highway"]) == "1")
                        home_site_highway.Checked = true;
                    if (Convert.ToString(dr7["home_site_school"]) == "1")
                        home_site_school.Checked = true;
                    if (Convert.ToString(dr7["home_site_services"]) == "1")
                        home_site_services.Checked = true;
                    if (Convert.ToString(dr7["home_site_transport"]) == "1")
                        home_site_transport.Checked = true;


                }

            }
            finally
            {
               // conn.Close();
            }


            cmd8.CommandType = CommandType.StoredProcedure;
         
            try
            {
                //Add the params
                cmd8.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd8.Parameters.Add("home_id", SqlDbType.Int).Value = Convert.ToInt32(home);

                SqlDataAdapter da = new SqlDataAdapter(cmd8);
                DataSet ds = new DataSet();
                da.Fill(ds);

                gv_home_year_evaluation.DataSource = ds;
                gv_home_year_evaluation.DataBind();
        
            }
            finally
            {
                conn.Close();
            }




         
        }
        // IF not postback       

    }


    protected string getUrl()
    {
        return "~/mediahandler/HomeImage.ashx?home_id="+getHomeId() + "&schema_id="+Session["schema_id"].ToString();
        
    }

    protected string getHomeId()
    {
       return Request.QueryString["h_id"];
    }

/// <summary>
/// 
/// </summary>
    private void SetDefaultView()
    {
        TabContainer1.ActiveTabIndex = 0 ;
    }

    protected void btn_sumbit_evaluation_Click(object sender, EventArgs e)
    {
        TabContainer1.ActiveTabIndex = 2;




        AccountObjectAuthorization homeAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["h_id"])))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////



        string home = Request.QueryString["h_id"];

    // adding an other evaluation of home
        if (ddl_year_evaluation.SelectedValue != "0" || home_land_evaluation.Text == "" || home_building_evaluation.Text == "")
       {

        SqlConnection conn8 = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        SqlCommand cmd8 = new SqlCommand("prHomeYearEvaluationAdd", conn8);
        cmd8.CommandType = CommandType.StoredProcedure;


        conn8.Open();
        //Add the params

        cmd8.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd8.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
        cmd8.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
        cmd8.Parameters.Add("home_id", SqlDbType.Int).Value = Convert.ToInt32(home);
        cmd8.Parameters.Add("@home_year_evaluation", SqlDbType.Int).Value = Convert.ToInt32(ddl_year_evaluation.SelectedValue);

       
        cmd8.Parameters.Add("@home_land_evaluation", SqlDbType.Int).Value = Convert.ToInt32(RegEx.IsInteger(home_land_evaluation.Text));
        cmd8.Parameters.Add("@home_building_evaluation", SqlDbType.Int).Value = Convert.ToInt32(RegEx.IsInteger(home_building_evaluation.Text));
        cmd8.Parameters.Add("@home_total_evaluation", SqlDbType.Int).Value = Convert.ToInt32(RegEx.IsInteger(home_total_evaluation.Text));
        //execute the insert
        cmd8.ExecuteNonQuery();
        conn8.Close();
       }


    // getting the new value that was added and put it in a gridview for displaying
        if (ddl_year_evaluation.SelectedValue != "0" || home_land_evaluation.Text == "" || home_building_evaluation.Text == "")
     {

      SqlConnection conn9 = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
      SqlCommand cmd9 = new SqlCommand("prHomeAllEvaluationList", conn9);
      cmd9.CommandType = CommandType.StoredProcedure;
      conn9.Open();
      cmd9.CommandType = CommandType.StoredProcedure;

      try
      {
          //Add the params
          cmd9.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
          cmd9.Parameters.Add("home_id", SqlDbType.Int).Value = Convert.ToInt32(home);

          SqlDataAdapter da = new SqlDataAdapter(cmd9);
          DataSet ds = new DataSet();
          da.Fill(ds);

          gv_home_year_evaluation.DataSource = ds;
          gv_home_year_evaluation.DataBind();

      }
      finally
      {
          conn9.Close();
      }
    }

    }
}
