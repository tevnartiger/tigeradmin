﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="property_available_units.aspx.cs" Inherits="manager_property_property_available_units" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<b><asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_vacancy %>"/></b>


    
        &nbsp;
    <br />


    
        <br />


    
        <asp:GridView Width="60%" ID="gv_HomeNumberOfUnitAvailable" 
          runat="server" AutoGenerateColumns="false"
          AllowPaging="true" AllowSorting="true"    AlternatingRowStyle-BackColor="#F0F0F6" 
         BorderColor="#CDCDCD"  BorderWidth="1" GridLines="Both" 
          HeaderStyle-BackColor="#F0F0F6" EmptyDataText="<%$ Resources:Resource, lbl_none %>"   >
    <Columns>
   <asp:BoundField HeaderText="<%$ Resources:Resource, lbl_property %>" DataField="home_name"  />
   <asp:BoundField HeaderText="<%$ Resources:Resource, lbl_nb_unit %>" DataField="number_of_units"  />
   </Columns>
   </asp:GridView>
  
    <br />
    <hr />
  
    <p>
    </p>
        <table >
            <tr>
                <td>
      <asp:Label ID="lbl_property" runat="server" Text="<%$ Resources:Resource, lbl_property %>"/>
                </td>
                <td>
       <asp:DropDownList ID="ddl_home_id" DataValueField="home_id" DataTextField="home_name"   runat="server" autopostback="true" OnSelectedIndexChanged="ddl_home_id_SelectedIndexChanged" />
    
    
    
    
                </td>
            </tr>
            </table>
        <br />
  
     <asp:GridView  Width="60%" ID="gv_unit_available_list" runat="server" AutoGenerateColumns="false"
       AllowSorting="true"  OnPageIndexChanging="gv_unit_available_list_PageIndexChanging"
        EmptyDataText="<%$ Resources:Resource, lbl_none %>"   
       AllowPaging="true"  AlternatingRowStyle-BackColor="#F0F0F6" 
      BorderColor="#CDCDCD"  BorderWidth="1" GridLines="Both" 
      HeaderStyle-BackColor="#F0F0F6"   PageSize="10"  >
    <Columns>
   <asp:BoundField HeaderText="<%$ Resources:Resource, lbl_door_number %>" DataField="unit_door_no"  />
   
  
      
  
       
       
    <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_since %>" >
    <ItemTemplate  >
    <asp:Label ID="lbl_since" runat="server" Text='<%#Since(Convert.ToDateTime(Eval("since")))%>' ></asp:Label>
    </ItemTemplate  >
    </asp:TemplateField>
       
       
       
    <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_number_of_days %>" >
    <ItemTemplate  >
    <asp:Label ID="lbl_number_of_days" runat="server" Text='<%#Get_NumberOfDays(Convert.ToInt32(Eval("number_of_days")))%>' ></asp:Label>
    </ItemTemplate  >
    </asp:TemplateField>      
   </Columns>
   </asp:GridView>




</asp:Content>

