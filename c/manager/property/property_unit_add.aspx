﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="property_unit_add.aspx.cs" Inherits="manager_property_property_unit_add" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

<b><asp:Label ID="Label21" runat="server" Text="<%$ Resources:Resource, lbl_add_property_4 %>"></asp:Label></b><br />
 <br />
<div>
        <strong>
        <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_add_units %>"></asp:Label></strong><br />
    
    <cc1:TabContainer ID="TabContainer1" runat="server">
    
  <cc1:TabPanel ID="tab1" runat="server" HeaderText="RESIDENTIAL UNITS"  >
   <ContentTemplate  >
           <div id="txt_message" runat="server"></div><br />
      <b> <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lbl_current_units %>"></asp:Label></b>&nbsp;(&nbsp;<asp:Label ID="Label11" runat="server" Text="<%$ Resources:Resource, lbl_door_number %>"/>&nbsp;)
        <br />
        
   
    
    <table id="tb_repeater_holder" runat="server" bgcolor="#ffffcc" width="60%">
    <tr runat="server"><td runat="server">
    
    
      <asp:Repeater  ID="r_unit_list" runat="server">
    <ItemTemplate  >
     <b ><%# Eval("unit_door_no") %> &nbsp,</b> 
    </ItemTemplate>
    </asp:Repeater>
    
    </td></tr> </table>
  
        <br />
         <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lbl_address %>"></asp:Label>&nbsp;
         <asp:Repeater runat="server" ID="rhome_view">
        <ItemTemplate>
        
        <table bgcolor="#ffffcc" >
        
        <tr>
          <td>
              <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_district %>"></asp:Label> &nbsp;:&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_district")%> 
                   </td>
            
              <td >,
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_no")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_street")%></td>
            
                 <td >,
                   <%#DataBinder.Eval(Container.DataItem, "home_city")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_prov")%></td>
               
            
                 <td >,
                   <%#DataBinder.Eval(Container.DataItem, "home_pc")%> 
                
                   </td>
               
            </tr>   
        </table>
        </ItemTemplate>
        </asp:Repeater> 
        <br /><table bgcolor="#ffffcc" style="width: 100%" border="1" cellpadding="3" cellspacing="1" rules=ROWS,cols frame=BOX 
     boredercolor="AliceBlue"  >
                <tr  >
                    <td bgcolor="AliceBlue" >
                    </td>
                    <td bgcolor="AliceBlue"  >
                      <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lbl_unit_level %>"/>
                    </td>
                    <td bgcolor="AliceBlue" >
                        <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lbl_door_number %>"/></td>
                    <td bgcolor="AliceBlue" >
                       <asp:Label ID="Label10" runat="server" Text="<%$ Resources:Resource, lbl_size %>"/>
                    </td>
                    <td bgcolor="AliceBlue">
                        <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, lbl_number_of_bedrooms %>"/></td>
                    <td bgcolor="AliceBlue">
                        <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lbl_number_of_bathrooms %>"/></td>
                    <td bgcolor="AliceBlue">
                        <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, lbl_maximum_tenants %>"/></td>
                </tr>
                <tr>
                    <td >
                        1</td>
                    <td >
                      <asp:DropDownList ID="ddl_unit_level1" runat="server" Height="20px">
                     <asp:ListItem Text="<%$ Resources:Resource, txt_basement %>" Value="0"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_first_floor %>" Value="1" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_second_floor %>" Value="2"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_third_floor %>" Value="3"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fourth_floor %>" Value="4"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fifth_floor %>" Value="5"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_sixth_floor %>" Value="6" ></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_seventh_floor %>" Value="7"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_eigth_floor %>" Value="8"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_nineth_floor %>" Value="9"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_tenth_floor %>" Value="10"></asp:ListItem>
                     </asp:DropDownList></td>
                    <td >
                        <asp:TextBox ID="tbx_door_no1" runat="server" Width="65px"></asp:TextBox></td>
                    <td >
                        <asp:TextBox ID="tbx_unit_size1" runat="server" Width="65px"></asp:TextBox>&nbsp;
                        <asp:DropDownList ID="ddl_unit_size_system1" runat="server">
                            <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_sqft %>" Value="0"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, lbl_sqm %>" Value="1"></asp:ListItem>
                        </asp:DropDownList></td>
                    <td >
                        <asp:DropDownList ID="ddl_nb_bedrooms1" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                     </asp:DropDownList></td>
                    <td >
                        <asp:DropDownList ID="ddl_nb_bathrooms1" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
             </asp:DropDownList></td>
                    <td >
                         <asp:DropDownList ID="ddl_max_tenants1" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                   
             </asp:DropDownList></td>
                </tr>
                <tr>
                    <td >
                        2</td>
                    <td >
                        <asp:DropDownList ID="ddl_unit_level2" runat="server" >
                           <asp:ListItem Text="<%$ Resources:Resource, txt_basement %>" Value="0"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_first_floor %>" Value="1" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_second_floor %>" Value="2"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_third_floor %>" Value="3"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fourth_floor %>" Value="4"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fifth_floor %>" Value="5"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_sixth_floor %>" Value="6" ></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_seventh_floor %>" Value="7"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_eigth_floor %>" Value="8"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_nineth_floor %>" Value="9"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_tenth_floor %>" Value="10"></asp:ListItem>
                     </asp:DropDownList>
                    </td>
                    <td >
                        <asp:TextBox ID="tbx_door_no2" runat="server" Width="65px"></asp:TextBox></td>
                    <td >
                        <asp:TextBox ID="tbx_unit_size2" runat="server" Width="65px"></asp:TextBox>&nbsp;
                        <asp:DropDownList ID="ddl_unit_size_system2" runat="server">
                          <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_sqft %>" Value="0"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, lbl_sqm %>" Value="1"></asp:ListItem>
                        </asp:DropDownList></td>
                    <td >
                        <asp:DropDownList ID="ddl_nb_bedrooms2" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                     </asp:DropDownList></td>
                    <td >
                        <asp:DropDownList ID="ddl_nb_bathrooms2" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
             </asp:DropDownList></td>
                    <td >
                          <asp:DropDownList ID="ddl_max_tenants2" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                   
             </asp:DropDownList></td>
                </tr>
                <tr>
                    <td >
                        3</td>
                    <td >
                        <asp:DropDownList ID="ddl_unit_level3" runat="server" Height="20px">
                        <asp:ListItem Text="<%$ Resources:Resource, txt_basement %>" Value="0"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_first_floor %>" Value="1" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_second_floor %>" Value="2"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_third_floor %>" Value="3"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fourth_floor %>" Value="4"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fifth_floor %>" Value="5"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_sixth_floor %>" Value="6" ></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_seventh_floor %>" Value="7"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_eigth_floor %>" Value="8"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_nineth_floor %>" Value="9"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_tenth_floor %>" Value="10"></asp:ListItem>
                      </asp:DropDownList>
                    </td>
                    <td >
                        <asp:TextBox ID="tbx_door_no3" runat="server" Width="65px"></asp:TextBox></td>
                    <td >
                        <asp:TextBox ID="tbx_unit_size3" runat="server" Width="65px"></asp:TextBox>&nbsp;
                        <asp:DropDownList ID="ddl_unit_size_system3" runat="server">
                          <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_sqft %>" Value="0"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, lbl_sqm %>" Value="1"></asp:ListItem>
                         </asp:DropDownList></td>
                    <td >
                        <asp:DropDownList ID="ddl_nb_bedrooms3" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                     </asp:DropDownList></td>
                    <td >
                       <asp:DropDownList ID="ddl_nb_bathrooms3" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
             </asp:DropDownList></td>
                    <td >
                          <asp:DropDownList ID="ddl_max_tenants3" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                   
             </asp:DropDownList></td>
                </tr>
                <tr>
                    <td >
                        4</td>
                    <td >
                        <asp:DropDownList ID="ddl_unit_level4" runat="server" Height="20px">
                          <asp:ListItem Text="<%$ Resources:Resource, txt_basement %>" Value="0"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_first_floor %>" Value="1" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_second_floor %>" Value="2"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_third_floor %>" Value="3"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fourth_floor %>" Value="4"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fifth_floor %>" Value="5"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_sixth_floor %>" Value="6" ></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_seventh_floor %>" Value="7"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_eigth_floor %>" Value="8"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_nineth_floor %>" Value="9"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_tenth_floor %>" Value="10"></asp:ListItem>
                     </asp:DropDownList>
                    </td>
                    <td >
                        <asp:TextBox ID="tbx_door_no4" runat="server" Width="65px"></asp:TextBox></td>
                    <td style="font-weight: 700" >
                        <asp:TextBox ID="tbx_unit_size4" runat="server" Width="65px"></asp:TextBox>&nbsp;
                        <asp:DropDownList ID="ddl_unit_size_system4" runat="server">
                           <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_sqft %>" Value="0"></asp:ListItem>
                           <asp:ListItem Text="<%$ Resources:Resource, lbl_sqm %>" Value="1"></asp:ListItem>
                        </asp:DropDownList></td>
                    <td >
                       <asp:DropDownList ID="ddl_nb_bedrooms4" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                     </asp:DropDownList></td>
                    <td >
                       <asp:DropDownList ID="ddl_nb_bathrooms4" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
             </asp:DropDownList></td>
                    <td >
                         <asp:DropDownList ID="ddl_max_tenants4" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                   
             </asp:DropDownList></td>
                </tr>
                <tr>
                    <td >
                        5</td>
                    <td >
                        <asp:DropDownList ID="ddl_unit_level5" runat="server" Height="20px">
                        <asp:ListItem Text="<%$ Resources:Resource, txt_basement %>" Value="0"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_first_floor %>" Value="1" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_second_floor %>" Value="2"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_third_floor %>" Value="3"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fourth_floor %>" Value="4"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fifth_floor %>" Value="5"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_sixth_floor %>" Value="6" ></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_seventh_floor %>" Value="7"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_eigth_floor %>" Value="8"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_nineth_floor %>" Value="9"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_tenth_floor %>" Value="10"></asp:ListItem>
                     </asp:DropDownList>
                    </td>
                    <td >
                        <asp:TextBox ID="tbx_door_no5" runat="server" Width="65px"></asp:TextBox></td>
                    <td >
                        <asp:TextBox ID="tbx_unit_size5" runat="server" Width="65px"></asp:TextBox>&nbsp;
                        <asp:DropDownList ID="ddl_unit_size_system5" runat="server">
                         <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_sqft %>" Value="0"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, lbl_sqm %>" Value="1"></asp:ListItem>
                        </asp:DropDownList></td>
                    <td >
                        <asp:DropDownList ID="ddl_nb_bedrooms5" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                     </asp:DropDownList></td>
                    <td >
                       <asp:DropDownList ID="ddl_nb_bathrooms5" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
             </asp:DropDownList></td>
                    <td >
                        <asp:DropDownList ID="ddl_max_tenants5" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                   
             </asp:DropDownList></td>
                </tr>
                <tr>
                    <td >
                        6</td>
                    <td >
                        <asp:DropDownList ID="ddl_unit_level6" runat="server" Height="20px">
                       <asp:ListItem Text="<%$ Resources:Resource, txt_basement %>" Value="0"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_first_floor %>" Value="1" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_second_floor %>" Value="2"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_third_floor %>" Value="3"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fourth_floor %>" Value="4"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fifth_floor %>" Value="5"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_sixth_floor %>" Value="6" ></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_seventh_floor %>" Value="7"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_eigth_floor %>" Value="8"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_nineth_floor %>" Value="9"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_tenth_floor %>" Value="10"></asp:ListItem>
                     </asp:DropDownList>
                    </td>
                    <td >
                        <asp:TextBox ID="tbx_door_no6" runat="server" Width="65px"></asp:TextBox></td>
                    <td >
                        <asp:TextBox ID="tbx_unit_size6" runat="server" Width="65px"></asp:TextBox>&nbsp;
                        <asp:DropDownList ID="ddl_unit_size_system6" runat="server">
                          <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_sqft %>" Value="0"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, lbl_sqm %>" Value="1"></asp:ListItem>
                        </asp:DropDownList></td>
                    <td >
                       <asp:DropDownList ID="ddl_nb_bedrooms6" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                     </asp:DropDownList></td>
                    <td >
                        <asp:DropDownList ID="ddl_nb_bathrooms6" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
             </asp:DropDownList></td>
                    <td >
                         <asp:DropDownList ID="ddl_max_tenants6" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                   
             </asp:DropDownList></td>
                </tr>
                <tr>
                    <td >
                        7</td>
                    <td >
                        <asp:DropDownList ID="ddl_unit_level7" runat="server" Height="20px">
                         <asp:ListItem Text="<%$ Resources:Resource, txt_basement %>" Value="0"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_first_floor %>" Value="1" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_second_floor %>" Value="2"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_third_floor %>" Value="3"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fourth_floor %>" Value="4"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fifth_floor %>" Value="5"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_sixth_floor %>" Value="6" ></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_seventh_floor %>" Value="7"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_eigth_floor %>" Value="8"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_nineth_floor %>" Value="9"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_tenth_floor %>" Value="10"></asp:ListItem>
                     </asp:DropDownList>
                    </td>
                    <td >
                        <asp:TextBox ID="tbx_door_no7" runat="server" Width="65px"></asp:TextBox></td>
                    <td >
                        <asp:TextBox ID="tbx_unit_size7" runat="server" Width="65px"></asp:TextBox>&nbsp;
                        <asp:DropDownList ID="ddl_unit_size_system7" runat="server">
                          <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_sqft %>" Value="0"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, lbl_sqm %>" Value="1"></asp:ListItem>
                         </asp:DropDownList></td>
                    <td >
                       <asp:DropDownList ID="ddl_nb_bedrooms7" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                     </asp:DropDownList></td>
                    <td >
                      <asp:DropDownList ID="ddl_nb_bathrooms7" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
             </asp:DropDownList></td>
                    <td >
                       <asp:DropDownList ID="ddl_max_tenants7" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                   
             </asp:DropDownList></td>
                </tr>
                <tr>
                    <td >
                        8</td>
                    <td >
                        <asp:DropDownList ID="ddl_unit_level8" runat="server" Height="20px">
                         <asp:ListItem Text="<%$ Resources:Resource, txt_basement %>" Value="0"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_first_floor %>" Value="1" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_second_floor %>" Value="2"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_third_floor %>" Value="3"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fourth_floor %>" Value="4"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fifth_floor %>" Value="5"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_sixth_floor %>" Value="6" ></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_seventh_floor %>" Value="7"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_eigth_floor %>" Value="8"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_nineth_floor %>" Value="9"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_tenth_floor %>" Value="10"></asp:ListItem>
                     </asp:DropDownList>
                    </td>
                    <td > <asp:TextBox ID="tbx_door_no8" runat="server" Width="65px"></asp:TextBox></td>
                    <td >
                        <asp:TextBox ID="tbx_unit_size8" runat="server" Width="65px"></asp:TextBox>&nbsp;
                        <asp:DropDownList ID="ddl_unit_size_system8" runat="server">
                           <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_sqft %>" Value="0"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, lbl_sqm %>" Value="1"></asp:ListItem>
                         </asp:DropDownList></td>
                    <td >
                      <asp:DropDownList ID="ddl_nb_bedrooms8" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                     </asp:DropDownList></td>
                    <td >
                       <asp:DropDownList ID="ddl_nb_bathrooms8" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
             </asp:DropDownList></td>
                    <td >
                        <asp:DropDownList ID="ddl_max_tenants8" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                   
             </asp:DropDownList></td>
                </tr>
                <tr>
                    <td >
                        9</td>
                    <td >
                        <asp:DropDownList ID="ddl_unit_level9" runat="server" Height="20px">
                        <asp:ListItem Text="<%$ Resources:Resource, txt_basement %>" Value="0"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_first_floor %>" Value="1" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_second_floor %>" Value="2"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_third_floor %>" Value="3"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fourth_floor %>" Value="4"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fifth_floor %>" Value="5"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_sixth_floor %>" Value="6" ></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_seventh_floor %>" Value="7"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_eigth_floor %>" Value="8"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_nineth_floor %>" Value="9"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_tenth_floor %>" Value="10"></asp:ListItem>
                     </asp:DropDownList>
                    </td>
                    <td >
                        <asp:TextBox ID="tbx_door_no9" runat="server" Width="65px"></asp:TextBox></td>
                    <td >
                        <asp:TextBox ID="tbx_unit_size9" runat="server" Width="65px"></asp:TextBox>&nbsp;
                        <asp:DropDownList ID="ddl_unit_size_system9" runat="server">
                          <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_sqft %>" Value="0"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, lbl_sqm %>" Value="1"></asp:ListItem>
                        </asp:DropDownList></td>
                    <td >
                        <asp:DropDownList ID="ddl_nb_bedrooms9" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                     </asp:DropDownList></td>
                    <td >
                       <asp:DropDownList ID="ddl_nb_bathrooms9" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
             </asp:DropDownList></td>
                    <td >
                         <asp:DropDownList ID="ddl_max_tenants9" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                   
             </asp:DropDownList></td>
                </tr>
                <tr>
                    <td >
                        10</td>
                    <td >
                        <asp:DropDownList ID="ddl_unit_level10" runat="server" Height="20px">
                         <asp:ListItem Text="<%$ Resources:Resource, txt_basement %>" Value="0"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_first_floor %>" Value="1" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_second_floor %>" Value="2"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_third_floor %>" Value="3"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fourth_floor %>" Value="4"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fifth_floor %>" Value="5"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_sixth_floor %>" Value="6" ></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_seventh_floor %>" Value="7"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_eigth_floor %>" Value="8"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_nineth_floor %>" Value="9"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_tenth_floor %>" Value="10"></asp:ListItem>
                     </asp:DropDownList>
                    </td>
                    <td >
                        <asp:TextBox ID="tbx_door_no10" runat="server" Width="65px"></asp:TextBox></td>
                    <td >
                        <asp:TextBox ID="tbx_unit_size10" runat="server" Width="65px"></asp:TextBox>&nbsp;
                        <asp:DropDownList ID="ddl_unit_size_system10" runat="server">
                          <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_sqft %>" Value="0"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, lbl_sqm %>" Value="1"></asp:ListItem>
                        </asp:DropDownList></td>
                    <td >
                       <asp:DropDownList ID="ddl_nb_bedrooms10" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                     </asp:DropDownList></td>
                    <td >
                        <asp:DropDownList ID="ddl_nb_bathrooms10" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
             </asp:DropDownList></td>
                    <td >
                          <asp:DropDownList ID="ddl_max_tenants10" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                   
             </asp:DropDownList></td>
                </tr>
            </table>
            <br />
          
       
        <asp:Button id="btn_submit" runat="server" Text="<%$ Resources:Resource, btn_submit_exit %>" OnClick="btn_submit_Click"  />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btn_submit_add_other" runat="server" 
            Text="<%$ Resources:Resource, btn_submit_add_other %>" 
            onclick="btn_submit_add_other_Click" />
        <br />
        
        <br />
        <asp:HyperLink ID="link_main" runat="server" NavigateUrl="~/home/home_main.aspx">go back to main</asp:HyperLink><br />
        <br />
   
    </ContentTemplate  >
   </cc1:TabPanel>
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   <cc1:TabPanel ID="tab2" runat="server" HeaderText="COMMERCIAL UNITS"  >
    <ContentTemplate  >
    <asp:Label ID="Label12" runat="server" Text="<%$ Resources:Resource, lbl_address %>"></asp:Label>&nbsp;
         
         <b> <asp:Label ID="Label19" runat="server" Text="<%$ Resources:Resource, lbl_current_units %>"></asp:Label></b>&nbsp;(&nbsp;<asp:Label ID="Label20" runat="server" Text="<%$ Resources:Resource, lbl_door_number %>"/>&nbsp;)
        <br />
        
   
    
    <table id="tb_repeater_holder2" runat="server" bgcolor="#ffffcc" width="60%">
    <tr runat="server"><td runat="server">
    
    
      <asp:Repeater  ID="r_unit_list2" runat="server">
    <ItemTemplate  >
     <b ><%# Eval("unit_door_no") %> &nbsp,</b> 
    </ItemTemplate>
    </asp:Repeater>
    
    </td></tr> </table>
  
        <br />
         <asp:Label ID="Label22" runat="server" Text="<%$ Resources:Resource, lbl_address %>"></asp:Label>&nbsp;
     
         
         
         
         <asp:Repeater runat="server" ID="rhome_view2">
        <ItemTemplate>
        
        <table bgcolor="#ffffcc" >
        
        <tr>
          <td>
              <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_district %>"></asp:Label> &nbsp;:&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_district")%> 
                   </td>
            
              <td >,
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_no")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_street")%></td>
            
                 <td >,
                   <%#DataBinder.Eval(Container.DataItem, "home_city")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_prov")%></td>
               
            
                 <td >,
                   <%#DataBinder.Eval(Container.DataItem, "home_pc")%> 
                
                   </td>
               
            </tr>   
        </table>
        </ItemTemplate>
        </asp:Repeater> 
        <br /><table bgcolor="#ffffcc" style="width: 100%" border="1" cellpadding="3" cellspacing="1" rules=ROWS,cols frame=BOX 
     BORDERCOLOR="AliceBlue">
                <tr  >
                    <td bgcolor="AliceBlue" >
                    </td>
                    <td bgcolor="AliceBlue"  >
                      <asp:Label ID="Label13" runat="server" Text="<%$ Resources:Resource, lbl_unit_level %>"/>
                    </td>
                    <td bgcolor="AliceBlue" >
                        <asp:Label ID="Label14" runat="server" Text="<%$ Resources:Resource, lbl_door_number %>"/></td>
                    <td bgcolor="AliceBlue" >
                       <asp:Label ID="Label15" runat="server" Text="<%$ Resources:Resource, lbl_size %>"/>
                    </td>
                    <td bgcolor="AliceBlue">
                        <asp:Label ID="Label16" runat="server" Text="NUM. OF ROOMS"/></td>
                    <td bgcolor="AliceBlue">
                        <asp:Label ID="Label17" runat="server" Text="<%$ Resources:Resource, lbl_number_of_bathrooms %>"/></td>
                    <td bgcolor="AliceBlue">
                        <asp:Label ID="Label18" runat="server" Text="<%$ Resources:Resource, lbl_maximum_tenants %>"/></td>
                </tr>
                <tr>
                    <td >
                        1</td>
                    <td >
                        <asp:DropDownList ID="ddl_unit_level11" runat="server" Height="20px">
                           <asp:ListItem Text="<%$ Resources:Resource, txt_basement %>" Value="0"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_first_floor %>" Value="1" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_second_floor %>" Value="2"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_third_floor %>" Value="3"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fourth_floor %>" Value="4"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fifth_floor %>" Value="5"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_sixth_floor %>" Value="6" ></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_seventh_floor %>" Value="7"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_eigth_floor %>" Value="8"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_nineth_floor %>" Value="9"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_tenth_floor %>" Value="10"></asp:ListItem>
                     </asp:DropDownList>
                    </td>
                    <td >
                          <asp:TextBox ID="tbx_door_no11" runat="server" Width="65px"></asp:TextBox></td>
                    <td >
                        <asp:TextBox ID="tbx_unit_size11" runat="server" Width="65px"></asp:TextBox>&nbsp;
                        <asp:DropDownList ID="ddl_unit_size_system11" runat="server">
                          <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_sqft %>" Value="0"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, lbl_sqm %>" Value="1"></asp:ListItem>
                         </asp:DropDownList></td>
                    <td >
                        <asp:DropDownList ID="ddl_nb_bedrooms11" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                     </asp:DropDownList></td>
                    <td >
                       <asp:DropDownList ID="ddl_nb_bathrooms11" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
             </asp:DropDownList></td>
                    <td >
                          <asp:DropDownList ID="ddl_max_tenants11" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                   
             </asp:DropDownList></td>
                </tr>
                <tr>
                    <td >
                        2</td>
                    <td >
                        <asp:DropDownList ID="ddl_unit_level12" runat="server" Height="20px">
                         <asp:ListItem Text="<%$ Resources:Resource, txt_basement %>" Value="0"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_first_floor %>" Value="1" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_second_floor %>" Value="2"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_third_floor %>" Value="3"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fourth_floor %>" Value="4"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fifth_floor %>" Value="5"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_sixth_floor %>" Value="6" ></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_seventh_floor %>" Value="7"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_eigth_floor %>" Value="8"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_nineth_floor %>" Value="9"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_tenth_floor %>" Value="10"></asp:ListItem>
                     </asp:DropDownList>
                    </td>
                    <td >
                        <asp:TextBox ID="tbx_door_no12" runat="server" Width="65px"></asp:TextBox></td>
                    <td >
                        <asp:TextBox ID="tbx_unit_size12" runat="server" Width="65px"></asp:TextBox>&nbsp;
                        <asp:DropDownList ID="ddl_unit_size_system12" runat="server">
                           <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_sqft %>" Value="0"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, lbl_sqm %>" Value="1"></asp:ListItem>
                        </asp:DropDownList></td>
                    <td >
                       <asp:DropDownList ID="ddl_nb_bedrooms12" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                     </asp:DropDownList></td>
                    <td >
                       <asp:DropDownList ID="ddl_nb_bathrooms12" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
             </asp:DropDownList></td>
                    <td >
                          <asp:DropDownList ID="ddl_max_tenants12" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                   
             </asp:DropDownList></td>
                </tr>
                <tr>
                    <td >
                        3</td>
                    <td >
                        <asp:DropDownList ID="ddl_unit_level13" runat="server" Height="20px">
                          <asp:ListItem Text="<%$ Resources:Resource, txt_basement %>" Value="0"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_first_floor %>" Value="1" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_second_floor %>" Value="2"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_third_floor %>" Value="3"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fourth_floor %>" Value="4"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_fifth_floor %>" Value="5"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_sixth_floor %>" Value="6" ></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_seventh_floor %>" Value="7"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_eigth_floor %>" Value="8"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_nineth_floor %>" Value="9"></asp:ListItem>
                     <asp:ListItem Text="<%$ Resources:Resource, txt_tenth_floor %>" Value="10"></asp:ListItem>
                     </asp:DropDownList>
                    </td>
                    <td >
                        <asp:TextBox ID="tbx_door_no13" runat="server" Width="65px"></asp:TextBox></td>
                    <td >
                        <asp:TextBox ID="tbx_unit_size13" runat="server" Width="65px"></asp:TextBox>&nbsp;
                        <asp:DropDownList ID="ddl_unit_size_system13" runat="server">
                           <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_sqft %>" Value="0"></asp:ListItem>
                            <asp:ListItem Text="<%$ Resources:Resource, lbl_sqm %>" Value="1"></asp:ListItem>
                        </asp:DropDownList></td>
                    <td >
                        <asp:DropDownList ID="ddl_nb_bedrooms13" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                     </asp:DropDownList></td>
                    <td >
                        <asp:DropDownList ID="ddl_nb_bathrooms13" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
             </asp:DropDownList></td>
                    <td >
                          <asp:DropDownList ID="ddl_max_tenants13" runat="server" Height="20px">
                     <asp:ListItem Value="0">0</asp:ListItem>
                     <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                     <asp:ListItem Value="2">2</asp:ListItem>
                     <asp:ListItem Value="3">3</asp:ListItem>
                     <asp:ListItem Value="4">4</asp:ListItem>
                     <asp:ListItem Value="5">5</asp:ListItem>
                      <asp:ListItem Value="6">6</asp:ListItem>
                       <asp:ListItem Value="7">7</asp:ListItem>
                        <asp:ListItem Value="8">8</asp:ListItem>
                         <asp:ListItem Value="9">9</asp:ListItem>
                          <asp:ListItem Value="10">10</asp:ListItem>
                   
             </asp:DropDownList></td>
                </tr>
             </table>
            <br />
          
       
        <asp:Button id="btn_submit2" runat="server" Text="<%$ Resources:Resource, btn_submit_exit %>" OnClick="btn_submit2_Click"  />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btn_submit_add_other2" runat="server" 
            Text="<%$ Resources:Resource, btn_submit_add_other %>" 
            onclick="btn_submit_add_other2_Click" />
        <br />
        
        <br />
        <asp:HyperLink ID="link_main2" runat="server" NavigateUrl="~/home/home_main.aspx">go back to main</asp:HyperLink><br />
        <br />
   
    </ContentTemplate  >
   </cc1:TabPanel>
     </cc1:TabContainer>
   
   
       
       
       
       
       
       
       
       
       
       
       

    
    </div>
</asp:Content>

