﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_property_default.ascx.cs" Inherits="c_manager_property_uc_property_default" %>

<br />


<table>


<tr><td> <asp:HyperLink ID="HyperLink5" NavigateUrl="~/manager/property/property_list.aspx" runat="server"><h2>  <asp:Literal ID="Literal5" Text="Property List" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to view and edit all properties from your account.</td></tr>

<tr><td> <asp:HyperLink ID="HyperLink1" NavigateUrl="~/manager/group/group_profile.aspx" runat="server"><h2>  <asp:Literal ID="Literal1" Text="<%$ Resources:Resource, lbl_gl_profile %>" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to view general information of one or more properties all in 1 page.</td></tr>


<tr><td> <asp:HyperLink ID="HyperLink4" NavigateUrl="~/manager/property/property_add.aspx" runat="server"><h2><asp:Literal ID="Literal4" Text="New Property" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to create a new property to your account.</td></tr>



<tr><td> <asp:HyperLink ID="HyperLink2" NavigateUrl="~/manager/mortgage/mortgage_list.aspx" runat="server"><h2>  <asp:Literal ID="Literal2" Text="Mortgages" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to manage the mortgages.</td></tr>

<tr><td> <asp:HyperLink ID="HyperLink3" NavigateUrl="~/manager/insurancepolicy/insurance_policy_list.aspx" runat="server"><h2>  <asp:Literal ID="Literal3" Text="Insurances" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to manage the insurances.</td></tr>



</table>
   