﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="Copy of property_add.aspx.cs" Inherits="manager_property_property_add" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<table>
 
<tr>
<td >
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
      <asp:View ID="View1" runat="server">
      <b><asp:Label ID="Label21" runat="server" Text="<%$ Resources:Resource, lbl_add_property_1 %>"></asp:Label></b><br />
    <br />
 <b><asp:Label ID="Label25" runat="server" 
        Text="<%$ Resources:Resource, lbl_mandatory_field %>" style="color: #FF3300"/></b><br />
    <br /><asp:Label ID="txt" runat="server" />
  <table >
   
<tr><td><span class="red">*</span><asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_home_name %>"></asp:Label></td>
    <td valign="top">  <asp:textbox id="home_name" CssClass="mandatory" runat="server"/>
<asp:RequiredFieldValidator Display="Dynamic" ID="as" ControlToValidate="home_name" runat="server" ErrorMessage="<%$ Resources:Resource, lbl_iv_validate %>" />
<asp:RegularExpressionValidator ID="reg_home_name" Display="Dynamic" ControlToValidate="home_name" runat="server" ErrorMessage="<%$ Resources:Resource, lbl_iv_invalid_char %>" />
</td></tr>
    
    
<tr><td><span class="red">*</span><asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_address_no %>"></asp:Label></td>
    <td valign="top"><asp:textbox id="home_addr_no" CssClass="mandatory" runat="server"/> 
<asp:RequiredFieldValidator Display="Dynamic" ID="fds" ControlToValidate="home_addr_no" runat="server" ErrorMessage="<%$ Resources:Resource, lbl_iv_validate %>" />
<asp:RegularExpressionValidator ID="reg_home_addr_no" Display="Dynamic" ControlToValidate="home_addr_no" runat="server" ErrorMessage="<%$ Resources:Resource, lbl_iv_invalid_char %>" />
       
 </td></tr>
<tr><td><span class="red">*</span><asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lbl_address_street %>"></asp:Label></td>
    <td valign="top"><asp:textbox id="home_addr_street" CssClass="mandatory" runat="server"/>
<asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator1" ControlToValidate="home_addr_street" runat="server" ErrorMessage="<%$ Resources:Resource, lbl_iv_validate %>" />
<asp:RegularExpressionValidator ID="reg_home_addr_street" Display="Dynamic" ControlToValidate="home_addr_street" runat="server" ErrorMessage="<%$ Resources:Resource, lbl_iv_invalid_char %>" />
    
    </td></tr>
<tr><td><asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lbl_pc %>"></asp:Label> </td>
    <td valign="top"><asp:textbox id="home_pc" runat="server"/> </td></tr>
<tr><td><span class="red">*</span><asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lbl_city %>"></asp:Label></td>
    <td valign="top"><asp:textbox id="home_city" CssClass="mandatory" runat="server"/> 
    <asp:RequiredFieldValidator Display="Dynamic" ID="RequiredFieldValidator2" ControlToValidate="home_city" runat="server" ErrorMessage="<%$ Resources:Resource, lbl_iv_validate %>" />
<asp:RegularExpressionValidator ID="reg_home_city" Display="Dynamic" ControlToValidate="home_city" runat="server" ErrorMessage="<%$ Resources:Resource, lbl_iv_invalid_char %>" />

    </td></tr>
<tr><td><asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, txt_district %>"></asp:Label></td>
    <td valign="top"><asp:textbox id="home_district" runat="server"/> </td></tr>
<tr><td><asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, lbl_province %>"></asp:Label> </td>
    <td valign="top"><asp:textbox id="home_prov" runat="server"/> </td></tr>
        
        <tr><td><asp:Label ID="Label18" runat="server" Text="<%$ Resources:Resource, lbl_gl_country %>"></asp:Label> </td>
    <td valign="top"><asp:textbox id="country" runat="server"/> </td></tr>

        
<tr><td><asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lbl_type %>"></asp:Label></td><td><asp:textbox id="home_type" runat="server"/></td></tr>
<tr><td><asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, lbl_style %>"></asp:Label></td><td><asp:textbox id="home_style" runat="server"/></td></tr>
<tr><td><asp:Label ID="Label10" runat="server" Text="<%$ Resources:Resource, lbl_nb_floor %>"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td><td><asp:textbox id="home_unit" runat="server" /></td></tr>
<tr><td><asp:Label ID="Label11" runat="server" Text="<%$ Resources:Resource, lbl_nb_unit %>"></asp:Label></td><td><asp:textbox id="home_floor" runat="server" /></td></tr>
<tr><td><asp:Label ID="Label12" runat="server" Text="<%$ Resources:Resource, lbl_date_built %>"></asp:Label> </td><td>

 <asp:DropDownList ID="ddl_home_date_built_m" runat="server">
                    <asp:ListItem Text="<%$ Resources:Resource, txt_month %>"  Value="0"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
&nbsp;/
                    <asp:DropDownList ID="ddl_home_date_built_d" runat="server">
                         <asp:ListItem Text="<%$ Resources:Resource, txt_day%>"  Value="0"></asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
&nbsp;/
                    <asp:DropDownList ID="ddl_home_date_built_y" runat="server">
                    <asp:ListItem Text="<%$ Resources:Resource, txt_year %>"  Value="0"></asp:ListItem>
                    <asp:ListItem>1980</asp:ListItem>
                    <asp:ListItem>1981</asp:ListItem>
                    <asp:ListItem>1982</asp:ListItem>
                    <asp:ListItem>1983</asp:ListItem>
                    <asp:ListItem>1984</asp:ListItem>
                    <asp:ListItem>1985</asp:ListItem>
                    <asp:ListItem>1986</asp:ListItem>
                    <asp:ListItem>1987</asp:ListItem>
                    <asp:ListItem>1988</asp:ListItem>
                    <asp:ListItem>1989</asp:ListItem>
                    <asp:ListItem>1990</asp:ListItem>
                    <asp:ListItem>1991</asp:ListItem>
                    <asp:ListItem>1992</asp:ListItem>
                    <asp:ListItem>1993</asp:ListItem>
                    <asp:ListItem>1994</asp:ListItem>
                    <asp:ListItem>1995</asp:ListItem>
                    <asp:ListItem>1996</asp:ListItem>
                    <asp:ListItem>1997</asp:ListItem>
                    <asp:ListItem>1998</asp:ListItem>
                    <asp:ListItem>1999</asp:ListItem>                    
                    <asp:ListItem>2000</asp:ListItem>
                    <asp:ListItem>2001</asp:ListItem>
                    <asp:ListItem>2002</asp:ListItem>
                    <asp:ListItem>2003</asp:ListItem>
                    <asp:ListItem>2004</asp:ListItem>
                    <asp:ListItem>2005</asp:ListItem>
                    <asp:ListItem>2006</asp:ListItem>
                    <asp:ListItem>2007</asp:ListItem>
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                    <asp:ListItem>2011</asp:ListItem>
                    <asp:ListItem>2012</asp:ListItem>
                    <asp:ListItem>2013</asp:ListItem>
                    <asp:ListItem>2014</asp:ListItem>
                    <asp:ListItem>2015</asp:ListItem>
                    </asp:DropDownList>
</td></tr>




<tr><td><asp:Label ID="Label13" runat="server" Text="<%$ Resources:Resource, lbl_date_purchase %>"></asp:Label></td>

<td>
<asp:DropDownList ID="ddl_home_date_purchase_m" runat="server">
                    <asp:ListItem Text="<%$ Resources:Resource, txt_month %>" Value="0"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
&nbsp;/
                    <asp:DropDownList ID="ddl_home_date_purchase_d" runat="server">
                         <asp:ListItem Text="<%$ Resources:Resource, txt_day %>" Value="0"></asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
&nbsp;/
                    <asp:DropDownList ID="ddl_home_date_purchase_y" runat="server">
                    <asp:ListItem Text="<%$ Resources:Resource, txt_year %>" Value="0"></asp:ListItem>
                    <asp:ListItem>1980</asp:ListItem>
                    <asp:ListItem>1981</asp:ListItem>
                    <asp:ListItem>1982</asp:ListItem>
                    <asp:ListItem>1983</asp:ListItem>
                    <asp:ListItem>1984</asp:ListItem>
                    <asp:ListItem>1985</asp:ListItem>
                    <asp:ListItem>1986</asp:ListItem>
                    <asp:ListItem>1987</asp:ListItem>
                    <asp:ListItem>1988</asp:ListItem>
                    <asp:ListItem>1989</asp:ListItem>
                    <asp:ListItem>1990</asp:ListItem>
                    <asp:ListItem>1991</asp:ListItem>
                    <asp:ListItem>1992</asp:ListItem>
                    <asp:ListItem>1993</asp:ListItem>
                    <asp:ListItem>1994</asp:ListItem>
                    <asp:ListItem>1995</asp:ListItem>
                    <asp:ListItem>1996</asp:ListItem>
                    <asp:ListItem>1997</asp:ListItem>
                    <asp:ListItem>1998</asp:ListItem>
                    <asp:ListItem>1999</asp:ListItem>                    
                    <asp:ListItem>2000</asp:ListItem>
                    <asp:ListItem>2001</asp:ListItem>
                    <asp:ListItem>2002</asp:ListItem>
                    <asp:ListItem>2003</asp:ListItem>
                    <asp:ListItem>2004</asp:ListItem>
                    <asp:ListItem>2005</asp:ListItem>
                    <asp:ListItem>2006</asp:ListItem>
                    <asp:ListItem>2007</asp:ListItem>
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                    <asp:ListItem>2011</asp:ListItem>
                    <asp:ListItem>2012</asp:ListItem>
                    <asp:ListItem>2013</asp:ListItem>
                    <asp:ListItem>2014</asp:ListItem>
                    <asp:ListItem>2015</asp:ListItem>
                    </asp:DropDownList>

</td></tr>
<tr><td><asp:Label ID="Label14" runat="server" Text="<%$ Resources:Resource, lbl_purchase_price %>"></asp:Label></td><td><asp:textbox id="home_purchase_price" runat="server"/></td></tr>
<tr><td><asp:Label ID="Label15" runat="server" Text="<%$ Resources:Resource, lbl_living_space %>"></asp:Label> </td><td><asp:textbox id="home_living_space" runat="server"/></td></tr>
<tr><td><asp:Label ID="Label16" runat="server" Text="<%$ Resources:Resource, lbl_land_size %>"></asp:Label> </td><td><asp:textbox id="home_land_size" runat="server"/></td></tr>
<tr><td valign="top"><asp:Label ID="Label17" runat="server" Text="<%$ Resources:Resource, lbl_description %>"></asp:Label></td><td>
    
    <asp:textbox id="home_desc" TextMode="MultiLine" runat="server" />
</td>
</tr>

    
  
    
    </table>

          <br />
    
    <asp:Button ID="btn_continu" runat="server" 
        Text="<%$ Resources:Resource, lbl_next %>" onclick="btn_continu_Click" />
      
      </asp:View>
      
      
      
      
      
      
      <asp:View ID="View2" runat="server">
      
      <b><asp:Label ID="Label22" runat="server" Text="<%$ Resources:Resource, lbl_add_property_2 %>"></asp:Label></b><br />
     <br />
 <b><asp:Label ID="Label23" runat="server" 
        Text="<%$ Resources:Resource, lbl_mandatory_field %>" style="color: #FF3300"/></b><br />
    <br />
 <br />
<table>
    
                <tr>
                    
                    
                    
                    <td >
                       <asp:Label ID="Label24" runat="server" Text="<%$ Resources:Resource, lbl_land_dimension %>"></asp:Label></td>
                    <td >
                        <asp:TextBox ID="home_land_dimension" runat="server" />
                    </td>
                    
                    <td >
                        &nbsp;</td>
                    
                    <td >
                        &nbsp; &nbsp;</td>
                    
                    <td >
                        <asp:Label ID="Label26" runat="server" Text="<%$ Resources:Resource, lbl_property_dimension %>"></asp:Label> 
                    
                    </td>
                    
                    
                    <td>
                    <asp:TextBox ID="home_dimension" runat="server" />
                    
                    </td>
                    
                </tr>
                <tr>
                <td >
                       <asp:Label ID="Label27" runat="server" Text="<%$ Resources:Resource, lbl_land_surface %>"></asp:Label></td>
                    <td >
                        <asp:TextBox ID="home_land_surface" runat="server" Height="16px" />
                    &nbsp;&nbsp;&nbsp;
                    </td>
                    <td >
                        <asp:DropDownList ID="ddl_land_surface_type" runat="server">
                            <asp:ListItem Value="0">Sq.-ft</asp:ListItem>
                            <asp:ListItem Value="1">Sq.-m</asp:ListItem>
                        </asp:DropDownList>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td >
                        &nbsp;</td>
                    <td  >
                        <asp:Label ID="Label35" runat="server" Text="<%$ Resources:Resource, lbl_living_area %>"></asp:Label></td>
                    <td >
                        <asp:TextBox ID="home_area" runat="server" Height="16px" />
                    &nbsp;&nbsp;&nbsp;
                        <asp:DropDownList ID="ddl_habitable_surface_type" runat="server">
                            <asp:ListItem Value="0">Sq.-ft</asp:ListItem>
                            <asp:ListItem Value="1">Sq.-m</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    
                    
                    
                    
                    
                </tr>
                <tr>
                    <td >
                       <asp:Label ID="Label36" runat="server" Text="<%$ Resources:Resource, lbl_localisation_certificat %>"></asp:Label></td>
                    <td >
                        <asp:TextBox ID="home_localisation_certificat" runat="server" 
                            />
                    </td>
                    
                    
                    
                    <td >
                        &nbsp;</td>
                    
                    
                    
                    <td >
                        &nbsp;</td>
                    
                    
                    
                    <td>
                    </td>
                    <td>
                    </td>
                    
                    
                    
                </tr>  
    
   
                <tr>
                    <td >
                        <asp:Label ID="Label37" runat="server" Text="<%$ Resources:Resource, lbl_matricule %>"></asp:Label> </td>
                    <td >
                        <asp:TextBox ID="home_matricule" runat="server" />
                    </td>
                    <td >
                        &nbsp;</td>
                    
                    
                    
                    <td >
                        &nbsp;</td>
                    
                    
                    
                    <td>
                    </td>
                    <td>
                    </td>
                    
                    
                    
                </tr>  
    
   
                <tr>
                    <td >
                        <asp:Label ID="Label38" runat="server" Text="<%$ Resources:Resource, lbl_register %>"></asp:Label></td>
                    <td >
                        <asp:TextBox ID="home_register" runat="server" />
                    </td>
                    <td >
                        &nbsp;</td>
                    
                    
                    
                    <td >
                        &nbsp;</td>
                    
                    
                    
                    <td>
                        &nbsp;</td>
                    <td>
                    </td>
                    
                    
                    
                </tr>  
    
   
                <tr>
                    <td >
                        &nbsp;</td>
                    <td >
                        &nbsp;</td>
                    <td >
                        &nbsp;</td>
                    
                    
                    
                    <td >
                        &nbsp;</td>
                    
                    
                    
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    
                    
                    
                </tr>  
    
   
    <tr>
                    <td >
                        <b><asp:Label ID="Label39" runat="server" Text="<%$ Resources:Resource, lbl_property_municipal_evaluation %>"></asp:Label></b></td>
                    <td>
                        
                        &nbsp;</td>
                    <td>
                        
                        &nbsp;</td>
                    <td>
                        
                        &nbsp;</td>
      </tr>              
    
   
    <tr>
                    <td >
                        &nbsp;</td>
                    <td>
                        
                        &nbsp;</td>
                    <td>
                        
                        &nbsp;</td>
                    <td>
                        
                        &nbsp;</td>
      </tr>              
    
   
    <tr>
                    <td >
                        <asp:Label ID="Label40" runat="server" Text="<%$ Resources:Resource, txt_year %>"></asp:Label> </td>
                    <td>
                        
                        <asp:DropDownList ID="ddl_year_evaluation" runat="server">
                            <asp:ListItem Text="<%$ Resources:Resource, txt_year %>" Value ="0" Selected="True"></asp:ListItem>
                            <asp:ListItem Value ="1981">1981</asp:ListItem>
                            <asp:ListItem Value ="1982">1982</asp:ListItem>
                            <asp:ListItem Value ="1983">1983</asp:ListItem>
                            <asp:ListItem Value ="1984">1984</asp:ListItem>
                            <asp:ListItem Value ="1985">1985</asp:ListItem>
                            <asp:ListItem Value ="1986">1986</asp:ListItem>
                            <asp:ListItem Value ="1987">1987</asp:ListItem>
                            <asp:ListItem Value ="1988">1998</asp:ListItem>
                            <asp:ListItem Value ="1989">1989</asp:ListItem>
                            <asp:ListItem Value ="1990">1990</asp:ListItem>
                            <asp:ListItem Value ="1991">1991</asp:ListItem>
                            <asp:ListItem Value ="1992">1992</asp:ListItem>
                            <asp:ListItem Value ="1993">1993</asp:ListItem>
                            <asp:ListItem Value ="1994">1994</asp:ListItem>
                            <asp:ListItem Value ="1995">1995</asp:ListItem>
                            <asp:ListItem Value ="1996">1996</asp:ListItem>
                            <asp:ListItem Value ="1997">1997</asp:ListItem>
                            <asp:ListItem Value ="1998">1998</asp:ListItem>
                            <asp:ListItem Value ="1999">1999</asp:ListItem>
                            <asp:ListItem Value ="2000">2000</asp:ListItem>
                            <asp:ListItem Value ="2001">2001</asp:ListItem>
                            <asp:ListItem Value ="2002">2002</asp:ListItem>
                            <asp:ListItem Value ="2003">2003</asp:ListItem>
                            <asp:ListItem Value ="2004">2004</asp:ListItem>
                            <asp:ListItem Value ="2005">2005</asp:ListItem>
                            <asp:ListItem Value ="2006">2006</asp:ListItem>
                            <asp:ListItem Value ="2007">2007</asp:ListItem>
                            <asp:ListItem Value ="2008">2008</asp:ListItem>
                            <asp:ListItem Value ="2009">2009</asp:ListItem>
                            <asp:ListItem Value ="2010">2010</asp:ListItem>
                           </asp:DropDownList></td>
                    <td>
                        
                        &nbsp;</td>
                    <td>
                        
                        &nbsp;</td>
      </tr>              
                <tr>  <td >
                        <asp:Label ID="Label41" runat="server" Text="<%$ Resources:Resource, lbl_land_evaluation %>"></asp:Label></td>
               
             
                    <td >
                        <asp:TextBox ID="home_land_evaluation" runat="server" Height="17px" 
                            /></td>
               
             
                    <td >
                        </td>
               
             
                    <td >
                        &nbsp;</td></tr>     
                    
                <tr>
                
                <td ><asp:Label ID="Label42" runat="server" Text="<%$ Resources:Resource, lbl_building_evaluation %>"></asp:Label></td>
                
                
                <td ><asp:TextBox ID="home_building_evaluation" runat="server" />
               </td>
                
                
                <td >&nbsp;</td>
                
                
                <td >&nbsp;</td>
               </tr>
               <tr>
                
                <td ><asp:Label ID="Label43" runat="server" Text="<%$ Resources:Resource, lbl_total_evaluation %>"></asp:Label></td>
                
                
                <td ><asp:TextBox ID="home_total_evaluation" runat="server" />
               </td>
                
                
                <td >&nbsp;</td>
                
                
                <td >&nbsp;</td>
               </tr>
 </table>
<br />
    
          &nbsp;<asp:Button ID="btn_previous_1" runat="server" 
              Text="<%$ Resources:Resource, lbl_previous %>" onclick="btn_previous_1_Click" />
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    
    <asp:Button ID="btn_continu_2" runat="server" 
        Text="<%$ Resources:Resource, lbl_next %>"  onclick="btn_continu_2_Click" />
      
      </asp:View>
      
      
      
      
      
      <asp:View ID="View3" runat="server"> 
      
      <b><asp:Label ID="Label44" runat="server" Text="<%$ Resources:Resource, lbl_add_property_3 %>"></asp:Label></b><br />
 <br />
 <b><asp:Label ID="Label45" runat="server" 
        Text="<%$ Resources:Resource, lbl_mandatory_field %>" style="color: #FF3300"/></b><br />
    <br />
 
  <table>
     
    <tr>
        <td >
            <asp:Label ID="Label52" runat="server" Text="<%$ Resources:Resource, lbl_fondation %>"></asp:Label></td>
        
        <td ><asp:TextBox ID="home_fondation" runat="server"/></td>
    </tr> 
        <tr>
        <td ><asp:Label ID="Label53" runat="server" Text="<%$ Resources:Resource, lbl_frame %>"/></td>
        
        <td ><asp:TextBox ID="home_frame" runat="server"/></td>
       </tr>
        <tr>
        <td ><asp:Label ID="Label54" runat="server" Text="<%$ Resources:Resource, lbl_roof %>"/></td>
        
        <td ><asp:TextBox ID="home_roof" runat="server"/></td>
        </tr>
         <tr>
        <td ><asp:Label ID="Label55" runat="server" Text="<%$ Resources:Resource, lbl_windows %>"/></td>
        
        <td ><asp:TextBox ID="home_windows" runat="server"/></td>
        </tr>
        <tr>
        <td ><asp:Label ID="Label56" runat="server" Text="<%$ Resources:Resource, lbl_floors %>"/></td>
        
        <td ><asp:TextBox ID="home_floors" runat="server"/></td>
        </tr>
        <tr>
        <td ><asp:Label ID="Label57" runat="server" Text="<%$ Resources:Resource, lbl_under_floors %>"/></td>
        
        <td ><asp:TextBox ID="home_under_floors" runat="server"/></td>
        </tr>
        <tr>
        <td ><asp:Label ID="Label58" runat="server" Text="<%$ Resources:Resource, lbl_walls %>"/></td>
        
        <td ><asp:TextBox ID="home_wall" runat="server"/></td>
        </tr>
        <tr>
        <td ><asp:Label ID="Label59" runat="server" Text="<%$ Resources:Resource, lbl_water_heater %>"/></td>
        
        <td ><asp:TextBox ID="home_water_heater" runat="server"/></td>
        </tr>
        <tr>
        <td valign="top" ><asp:Label ID="Label60" runat="server" Text="<%$ Resources:Resource, lbl_lav_sech %>"/></td>
        
        <td >
            <asp:RadioButtonList ID="home_lav_sech" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Text="<%$ Resources:Resource, lbl_no %>" Selected="True" Value="0"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:Resource, lbl_yes %>" Value="1"></asp:ListItem>
            </asp:RadioButtonList></td>
            </tr>
          <tr>
        <td valign="top" ><asp:Label ID="Label61" runat="server" Text="<%$ Resources:Resource, lbl_fire_protection %>"/></td>
        
        <td >
            <asp:RadioButtonList ID="home_fire_protection" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Text="<%$ Resources:Resource, lbl_no %>" Selected="True" Value="0"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:Resource, lbl_yes %>" Value="1"></asp:ListItem>
            </asp:RadioButtonList></td>
           </tr>
        <tr>
        <td valign="top" ><asp:Label ID="Label62" runat="server" Text="<%$ Resources:Resource, lbl_laundry %>"/></td>
        
        <td >
            <asp:RadioButtonList ID="home_laundry" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Text="<%$ Resources:Resource, lbl_no %>" Selected="True" Value="0"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:Resource, lbl_yes %>" Value="1"></asp:ListItem>
            </asp:RadioButtonList></td>
           </tr>
        <tr>
        <td ><asp:Label ID="Label63" runat="server" Text="<%$ Resources:Resource, lbl_parking %>"/></td>
        
        <td >
            <asp:Label ID="Label64" runat="server" Text="<%$ Resources:Resource, lbl_int %>"/>
            <asp:TextBox ID="home_parking_int" runat="server" Text="0" />&nbsp; <asp:Label ID="Label65" runat="server" Text="<%$ Resources:Resource, lbl_ext %>"/>
            :<asp:TextBox ID="home_parking_ext" runat="server" Text="0" /></td>
        <td>
            &nbsp;&nbsp;</td>
          </tr>

        <tr>
        <td valign="top" ><asp:Label ID="Label66" runat="server" Text="<%$ Resources:Resource, lbl_external_plugs %>"/></td>
        
        <td >
            <asp:RadioButtonList ID="home_prise_ext" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Text="<%$ Resources:Resource, lbl_no %>" Selected="True" Value="0"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:Resource, lbl_yes %>" Value="1"></asp:ListItem>
            </asp:RadioButtonList></td>
            </tr>
        <tr>
        <td ><asp:Label ID="Label67" runat="server" Text="<%$ Resources:Resource, lbl_water %>"/></td>
        
        <td ><asp:TextBox ID="home_water" runat="server"/></td>
        </tr>
        <tr>
        <td ><asp:Label ID="Label68" runat="server" Text="<%$ Resources:Resource, lbl_heating %>"/></td>
        
        
        
        <td ><asp:TextBox ID="home_heating" runat="server"/></td>
        </tr>
        <tr>
        <td><asp:Label ID="Label69" runat="server" Text="<%$ Resources:Resource, lbl_combustible %>"/></td>
        
        <td ><asp:TextBox ID="home_combustible" runat="server"/></td>
        </tr>
       <tr>
        <td ><asp:Label ID="Label70" runat="server" Text="<%$ Resources:Resource, lbl_sewage %>"/></td>
        
        <td ><asp:TextBox ID="home_sewage" runat="server"/></td>
        </tr>
        <tr>
        <td ><asp:Label ID="Label71" runat="server" Text="<%$ Resources:Resource, lbl_exterior_finition %>"/></td>
        
        <td ><asp:TextBox ID="home_ext_finition" runat="server"/></td>
        </tr>
        <tr>
        <td valign="top" ><asp:Label ID="Label72" runat="server" Text="<%$ Resources:Resource, lbl_site_influence %>"/></td>
        
        <td >
                
            <table>
                <tr>
                    <td>
            <asp:CheckBox ID="home_site_corner"   Text="<%$ Resources:Resource, lbl_corner %>"  runat="server" />
                    </td>
                    <td>
            <asp:CheckBox ID="home_site_school" Text="<%$ Resources:Resource, lbl_school %>"  runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
            <asp:CheckBox ID="home_site_highway" Text="<%$ Resources:Resource, lbl_highway %>"  runat="server" />
                    </td>
                    <td>
            <asp:CheckBox ID="home_site_services"  Text="<%$ Resources:Resource, lbl_services %>"  runat="server" />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
            <asp:CheckBox ID="home_site_transport"  Text="<%$ Resources:Resource, lbl_transport %>"  runat="server" />
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
&nbsp;</td>
            </tr>
            <tr>
        <td ><asp:Label ID="Label73" runat="server" Text="<%$ Resources:Resource, lbl_parking %>"/></td>
        
        
        <td ><asp:TextBox ID="home_parking" runat="server"/></td>
        </tr>
        <tr>
        <td valign="top"  ><asp:Label ID="Label74" runat="server" Text="<%$ Resources:Resource, lbl_particularity %>"/></td>
        
        <td>
            <asp:TextBox ID="home_particularity" TextMode="MultiLine" runat="server"/>
    
            </td>

</tr>
  
   
    
    </table>
 
    <br />
          <asp:Button ID="btn_previous_2" runat="server"  
              Text="<%$ Resources:Resource, lbl_previous %>" 
              onclick="btn_previous_2_Click"  />
          &nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btn_continu_3" runat="server" onclick="btn_continu_3_Click" 
        Text="<%$ Resources:Resource, lbl_next %>" />

      
      </asp:View>
     
     <asp:View ID="view_unit_add" runat="server">
     
     
     </asp:View>
     
     <asp:View ID="view_view_all" runat="server">
     
     
     </asp:View>
     
</asp:MultiView>
</td>
</tr>
</table>

</asp:Content>

