<%@ Page Language="VB" AutoEventWireup="true" MasterPageFile="vb_scheduler.master" %>
<%@ Register TagPrefix="osd" Namespace="OboutInc.Scheduler" Assembly="obout_Scheduler_NET"%>  

    <script runat="server" language="VB">
        Dim Scheduler As Scheduler
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
            Page.Title = "Scheduler Basic Example"
            scheduler.Login(1)        
        End Sub
        Protected Sub Page_Init(ByVal o As Object, ByVal e As EventArgs)
            scheduler = New Scheduler()
            scheduler.ID = "scheduler"
            scheduler.ProviderName = "System.Data.OleDb"
            scheduler.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Server.MapPath("../App_Data/scheduler.mdb")
            scheduler.EventsTableName = "Events"
            Scheduler.UserSettingsTableName = "UserSettings"
            Scheduler.CategoriesTableName = "Categories"
            scheduler.Width = "990"
            scheduler.Height = "500"
            scheduler.StyleFolder = "styles/default"              
            pnContent.Controls.Add(scheduler)
        End Sub
    </script>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
<asp:Panel ID="pnContent" runat="server"></asp:Panel>
</asp:Content>            