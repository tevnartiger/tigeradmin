<%@ Page Language="C#" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Web.UI" %>

<script runat="server" Language="C#">
	void Page_load(object sender, EventArgs e)
	{
	    try
		{
			XmlTextReader oXMLReader = null;
			oXMLReader = new XmlTextReader(Server.MapPath("examples.xml"));
			while (oXMLReader.Read())
			{
				if (oXMLReader.NodeType == XmlNodeType.Element && oXMLReader.Name == "path")
				{
					lblPath.Text = "<b>Path to folder with Scheduler examples: </b>" + oXMLReader.ReadString();
				}
			}
			oXMLReader.Close();
		}
		catch (Exception ex)
		{
		}
			
	}
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Scheduler Examples</title>
    <style type="text/css">
		td.link{
			padding-left:30px;
			width:250px;			
		}
		
		td.header {
			padding-top:20px;
			border-bottom:1px solid #eeeeee;
			color:#555555;
		}
		
		.tdText {
			font:11px Verdana;
			color:#333333;
		}
		.option2{
			font:11px Verdana;
			color:#0033cc;
			background-color___:#f6f9fc;
			padding-left:4px;
			padding-right:4px;
		}
		a {
			font:11px Verdana;
			color:#315686;
			text-decoration:underline;
		}

		a:hover {
			color:crimson;
		}
	</style>
	<script type="text/javascript">
	    function exChange(nameDIV)
	    {
		    document.getElementById("cs").style.display = "none";
            document.getElementById("vb").style.display = "none";
            document.getElementById("aspnet").style.display = "none";
            document.getElementById(nameDIV).style.display = "block";
            document.getElementById("rcs").checked = false;
            document.getElementById("raspnet").checked = false;
            document.getElementById("rvb").checked = false;  
            document.getElementById("r"+nameDIV).checked = true;                       
	    }
	</script>
</head>
<body>

 <div class="tdText">
        <a href="../Default.aspx">� Back to Suite Examples</a>
        <br /><br /><br />
        <span style="font-weight:bold;">obout Scheduler examples</span>
        <br />
        <br />
        Select the examples you want to see:&#160;&#160;&#160;
        <input type="radio" id="raspnet" name="exSelect" value ="asp" onclick="exChange('aspnet')"/> ASP.NET
        <input type="radio" id="rcs" name="exSelect" value ="cs" onclick="exChange('cs')"/> C#.NET
        <input type="radio" id ="rvb" name="exSelect" value ="vb" onclick="exChange('vb')"/> VB.NET
        <br /><br />
        <asp:Label runat="server" ID="lblPath"></asp:Label>
        <br /><br />
 		<div id="aspnet" style="display:block">
		    <span class="tdText"><b>ASP.NET</b></span>
			<table width="1024">
				<tr>
					<td valign="top">
						<table>
											
							<tr>
								<td class="header" colspan="2"><b>Basic examples</b></td>
							</tr>
							<tr>
								<td class="link">
									<a href="aspnet_scheduler_basic.aspx">BasicExample</a>
								</td>
								<td>aspnet_scheduler_basic.aspx</td>
							</tr>
							
							<tr>
								<td class="link">
									<a href="aspnet_scheduler_clientside_updateeventhandle.aspx">Client-side Update Event Handle</a>
								</td>
								<td>aspnet_scheduler_clientside_updateeventhandle.aspx</td>
							</tr>


							<tr>
								<td class="link">
									<a href="aspnet_scheduler_clientside_deleteeventhandle.aspx">Cancel Delete Event</a>
								</td>
								<td>aspnet_scheduler_clientside_deleteeventhandle.aspx</td>
							</tr>
															
							
						</table>
					</td>
				</tr>
			</table>
		</div>   		   
		        
		<div id="cs" style="display:none">
		    <span class="tdText"><b>C#.NET</b></span>
			<table width="1024">
				<tr>
					<td valign="top">
						<table>
																		
							<tr>
								<td class="header" colspan="2"><b>Basic examples</b></td>
							</tr>	
							<tr>
								<td class="link">
									<a href="cs_scheduler_basic.aspx">BasicExample</a>
								</td>
								<td>cs_scheduler_basic.aspx</td>
							</tr>						
							
															
														

						</table>
					</td>
				</tr>
			</table>
		</div>  
    
		<div id="vb" style="display:none">
		    <span class="tdText"><b>VB.NET</b></span>
			<table width="1024">
				<tr>
					<td valign="top">
						<table>

							<tr>
								<td class="header" colspan="2"><b>Basic examples</b></td>
							</tr>	
							
							<tr>
								<td class="link">
									<a href="vb_scheduler_basic.aspx">BasicExample</a>
								</td>
								<td>vb_scheduler_basic.aspx</td>
							</tr>																					

						</table>
					</td>
				</tr>
			</table>
		</div>     
    

		
		<script type="text/javascript">
		    exChange("<% Response.Write((Request.Params["div"]==null)?"aspnet":Request.Params["div"]); %>");
		</script>
		
		<br />
		<br />
		<div class="tdText" style="width: 525px;">
		 </div>
        <br />
        <br />
        <a href="http://www.obout.com/scheduler">obout Scheduler home</a>
        <br />
        <a href="http://www.obout.com">obout inc home</a>

      </div>       

</body>
</html>
