﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : july 29 , 2008
public partial class manager_contact_contact_list :BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // if the categ is not an integer logout
        if (!RegEx.IsInteger(Request.QueryString["categ"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

       // if the if "l" this is not a letter logout
       // "l" is the first letter of a last name
        if (Request.QueryString["l"] != "" && Request.QueryString["l"] != string.Empty)
        {
          if (!RegEx.IsAlpha(Request.QueryString["l"]))
          {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
          }
        }



        if (!Page.IsPostBack)
        {

            tiger.Contact contact = new tiger.Contact(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_contact_list.DataSource = contact.getContactSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["categ"]), Request.QueryString["l"]);
            gv_contact_list.DataBind();


            reg_TextBox1.ValidationExpression = RegEx.getName();
            
      
           // int category = Convert.ToInt32(ddl_contact_category.SelectedValue);

            if (Request.QueryString["categ"] == "0")
            {
                ddl_contact_category.SelectedValue = "0";

                link_a.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=a";
                link_b.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=b";
                link_c.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=c";
                link_d.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=d";
                link_e.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=e";
                link_f.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=f";
                link_g.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=g";
                link_h.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=h";
                link_i.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=i";
                link_j.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=j";
                link_k.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=k";
                link_l.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=l";
                link_m.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=m";
                link_n.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=n";
                link_o.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=o";
                link_p.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=p";
                link_q.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=q";
                link_r.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=r";
                link_s.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=s";
                link_t.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=t";
                link_u.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=u";
                link_v.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=v";
                link_w.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=w";
                link_x.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=x";
                link_y.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=y";
                link_z.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=z";

            }



            if (Request.QueryString["categ"] == "1")
            {
                ddl_contact_category.SelectedValue = "1";

                link_a.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=a";
                link_b.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=b";
                link_c.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=c";
                link_d.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=d";
                link_e.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=e";
                link_f.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=f";
                link_g.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=g";
                link_h.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=h";
                link_i.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=i";
                link_j.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=j";
                link_k.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=k";
                link_l.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=l";
                link_m.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=m";
                link_n.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=n";
                link_o.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=o";
                link_p.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=p";
                link_q.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=q";
                link_r.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=r";
                link_s.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=s";
                link_t.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=t";
                link_u.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=u";
                link_v.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=v";
                link_w.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=w";
                link_x.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=x";
                link_y.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=y";
                link_z.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=z";

            }

            if (Request.QueryString["categ"] == "2")
            {
                ddl_contact_category.SelectedValue = "2";

                link_a.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=a";
                link_b.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=b";
                link_c.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=c";
                link_d.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=d";
                link_e.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=e";
                link_f.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=f";
                link_g.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=g";
                link_h.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=h";
                link_i.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=i";
                link_j.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=j";
                link_k.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=k";
                link_l.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=l";
                link_m.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=m";
                link_n.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=n";
                link_o.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=o";
                link_p.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=p";
                link_q.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=q";
                link_r.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=r";
                link_s.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=s";
                link_t.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=t";
                link_u.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=u";
                link_v.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=v";
                link_w.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=w";
                link_x.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=x";
                link_y.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=y";
                link_z.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=z";

            }

            if (Request.QueryString["categ"] == "3")
            {
                ddl_contact_category.SelectedValue = "3";

                link_a.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=a";
                link_b.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=b";
                link_c.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=c";
                link_d.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=d";
                link_e.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=e";
                link_f.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=f";
                link_g.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=g";
                link_h.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=h";
                link_i.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=i";
                link_j.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=j";
                link_k.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=k";
                link_l.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=l";
                link_m.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=m";
                link_n.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=n";
                link_o.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=o";
                link_p.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=p";
                link_q.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=q";
                link_r.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=r";
                link_s.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=s";
                link_t.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=t";
                link_u.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=u";
                link_v.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=v";
                link_w.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=w";
                link_x.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=x";
                link_y.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=y";
                link_z.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=z";
            }

            if (Request.QueryString["categ"] == "4")
            {
                ddl_contact_category.SelectedValue = "4";

                link_a.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=a";
                link_b.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=b";
                link_c.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=c";
                link_d.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=d";
                link_e.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=e";
                link_f.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=f";
                link_g.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=g";
                link_h.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=h";
                link_i.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=i";
                link_j.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=j";
                link_k.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=k";
                link_l.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=l";
                link_m.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=m";
                link_n.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=n";
                link_o.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=o";
                link_p.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=p";
                link_q.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=q";
                link_r.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=r";
                link_s.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=s";
                link_t.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=t";
                link_u.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=u";
                link_v.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=v";
                link_w.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=w";
                link_x.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=x";
                link_y.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=y";
                link_z.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=z";

            }

            if (Request.QueryString["categ"] == "5")
            {
                ddl_contact_category.SelectedValue = "5";

                link_a.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=a";
                link_b.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=b";
                link_c.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=c";
                link_d.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=d";
                link_e.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=e";
                link_f.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=f";
                link_g.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=g";
                link_h.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=h";
                link_i.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=i";
                link_j.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=j";
                link_k.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=k";
                link_l.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=l";
                link_m.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=m";
                link_n.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=n";
                link_o.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=o";
                link_p.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=p";
                link_q.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=q";
                link_r.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=r";
                link_s.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=s";
                link_t.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=t";
                link_u.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=u";
                link_v.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=v";
                link_w.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=w";
                link_x.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=x";
                link_y.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=y";
                link_z.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=z";

            }
 
        }

    }


    protected void submit_Click(object sender, EventArgs e)
    {
        Page.Validate();
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        gv_contact_list.Visible = false;
        string name = "";
        tiger.Contact contact = new tiger.Contact(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_contact_search_list.DataSource = contact.getContactSearchList(Convert.ToInt32(Session["schema_id"]), 0, RegEx.getText(TextBox1.Text));
        gv_contact_search_list.DataBind();
       
        ddl_contact_category.SelectedIndex = 0;


        /* WebService2 ser = new WebService2();
         gv_contact_list.Visible = false;
         string name = "";
         tiger.Contact contact = new tiger.Contact(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
         gv_contact_search_list.DataSource = ser.getTenantCurrentSearchList(Convert.ToInt32(Session["schema_id"]), 0, TextBox1.Text); //tenant.getTenantCurrentSearchList(Convert.ToInt32(Session["schema_id"]), 0, TextBox1.Text);
         gv_contact_search_list.DataBind(); */
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_contact_search_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
        tiger.Contact contact = new tiger.Contact(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_contact_search_list.PageIndex = e.NewPageIndex;
        gv_contact_search_list.DataSource = contact.getContactSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_contact_category.SelectedValue), RegEx.getText(TextBox1.Text));
        gv_contact_search_list.DataBind();

    }
    protected void ddl_contact_category_SelectedIndexChanged(object sender, EventArgs e)
    {
        int category = Convert.ToInt32(ddl_contact_category.SelectedValue);

        if (category == 0)
        {
            link_a.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=a";
            link_b.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=b";
            link_c.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=c";
            link_d.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=d";
            link_e.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=e";
            link_f.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=f";
            link_g.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=g";
            link_h.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=h";
            link_i.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=i";
            link_j.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=j";
            link_k.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=k";
            link_l.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=l";
            link_m.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=m";
            link_n.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=n";
            link_o.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=o";
            link_p.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=p";
            link_q.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=q";
            link_r.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=r";
            link_s.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=s";
            link_t.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=t";
            link_u.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=u";
            link_v.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=v";
            link_w.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=w";
            link_x.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=x";
            link_y.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=y";
            link_z.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=0&l=z";

        }



        if (category == 1)
        {
            link_a.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=a";
            link_b.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=b";
            link_c.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=c";
            link_d.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=d";
            link_e.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=e";
            link_f.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=f";
            link_g.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=g";
            link_h.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=h";
            link_i.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=i";
            link_j.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=j";
            link_k.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=k";
            link_l.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=l";
            link_m.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=m";
            link_n.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=n";
            link_o.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=o";
            link_p.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=p";
            link_q.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=q";
            link_r.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=r";
            link_s.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=s";
            link_t.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=t";
            link_u.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=u";
            link_v.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=v";
            link_w.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=w";
            link_x.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=x";
            link_y.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=y";
            link_z.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=1&l=z";

        }

        if (category == 2)
        {
            link_a.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=a";
            link_b.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=b";
            link_c.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=c";
            link_d.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=d";
            link_e.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=e";
            link_f.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=f";
            link_g.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=g";
            link_h.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=h";
            link_i.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=i";
            link_j.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=j";
            link_k.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=k";
            link_l.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=l";
            link_m.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=m";
            link_n.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=n";
            link_o.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=o";
            link_p.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=p";
            link_q.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=q";
            link_r.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=r";
            link_s.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=s";
            link_t.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=t";
            link_u.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=u";
            link_v.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=v";
            link_w.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=w";
            link_x.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=x";
            link_y.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=y";
            link_z.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=2&l=z";

        }

        if (category == 3)
        {

            link_a.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=a";
            link_b.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=b";
            link_c.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=c";
            link_d.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=d";
            link_e.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=e";
            link_f.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=f";
            link_g.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=g";
            link_h.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=h";
            link_i.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=i";
            link_j.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=j";
            link_k.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=k";
            link_l.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=l";
            link_m.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=m";
            link_n.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=n";
            link_o.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=o";
            link_p.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=p";
            link_q.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=q";
            link_r.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=r";
            link_s.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=s";
            link_t.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=t";
            link_u.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=u";
            link_v.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=v";
            link_w.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=w";
            link_x.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=x";
            link_y.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=y";
            link_z.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=3&l=z";
        }

        if (category == 4)
        {
            link_a.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=a";
            link_b.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=b";
            link_c.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=c";
            link_d.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=d";
            link_e.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=e";
            link_f.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=f";
            link_g.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=g";
            link_h.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=h";
            link_i.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=i";
            link_j.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=j";
            link_k.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=k";
            link_l.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=l";
            link_m.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=m";
            link_n.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=n";
            link_o.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=o";
            link_p.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=p";
            link_q.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=q";
            link_r.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=r";
            link_s.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=s";
            link_t.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=t";
            link_u.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=u";
            link_v.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=v";
            link_w.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=w";
            link_x.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=x";
            link_y.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=y";
            link_z.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=4&l=z";

        }

        if (category == 5)
        {
            link_a.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=a";
            link_b.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=b";
            link_c.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=c";
            link_d.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=d";
            link_e.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=e";
            link_f.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=f";
            link_g.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=g";
            link_h.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=h";
            link_i.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=i";
            link_j.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=j";
            link_k.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=k";
            link_l.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=l";
            link_m.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=m";
            link_n.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=n";
            link_o.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=o";
            link_p.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=p";
            link_q.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=q";
            link_r.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=r";
            link_s.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=s";
            link_t.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=t";
            link_u.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=u";
            link_v.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=v";
            link_w.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=w";
            link_x.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=x";
            link_y.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=y";
            link_z.NavigateUrl = "~/manager/contact/contact_list.aspx?categ=5&l=z";

        }

      
        gv_contact_list.Visible = false;

        tiger.Contact contact = new tiger.Contact(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_contact_search_list.DataSource = contact.getContactSearchList(Convert.ToInt32(Session["schema_id"]), category, "");
        gv_contact_search_list.DataBind();

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_contact_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
        
        tiger.Contact contact = new tiger.Contact(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_contact_list.PageIndex = e.NewPageIndex;
        //gv_contact_list.DataSource = contact.getContactSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["categ"]), Request.QueryString["l"]);
        gv_contact_list.DataSource = contact.getContactSearchList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_contact_category.SelectedValue), Request.QueryString["l"]);
        gv_contact_list.DataBind();

    }


    protected string Get_ContactCateg(string categ)
    {

        string category = "";
        if (categ == "1")
        {
            category = Resources.Resource.lbl_contractor;
        }

        if (categ == "2")
        {
            category = Resources.Resource.lbl_supplier_vendor;
        }

        if (categ == "3")
        {
            category = Resources.Resource.lbl_financial_institution;
        }
        if (categ == "4")
        {
            category = Resources.Resource.lbl_insurance_company;
        }
        if (categ == "5")
        {
            category = Resources.Resource.lbl_warehouse;
        }

        return category;
    }


    protected string Get_Link(string id , string categ)
    {

        string link = "";

        if (categ == "1")
        {
           link = "~/manager/company/company_view.aspx?company_id=" + id;
        }

        if (categ == "2")
        {
            link = "~/manager/supplier/supplier_view.aspx?company_id=" + id;
        }

        if (categ == "3")
        {
            link = "~/manager/financialinstitution/financial_institution_view.aspx?fi_id=" + id;
        }
        if (categ == "4")
        {
            link = "~/manager/insurancecompany/insurance_company_view.aspx?ic_id=" + id;
        }
        if (categ == "5")
        {
            link = "~/manager/warehouse/warehouse_view.aspx?w_id=" + id;
        }

        return link ;
    }

       
}
