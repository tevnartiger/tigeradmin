<%@ Page Language="C#" MasterPageFile="../mp_manager.master" AutoEventWireup="true" CodeFile="company_list.aspx.cs" Inherits="company_company_list" Title="Company List" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<br />
    
    <asp:Button ID="btn_search" runat="server" Text="search" OnClick="btn_search_Click" /> : search by category<br /><br />
    
      
    <table  bgcolor="#ffffcc" id="TABLE1" language="javascript" onclick="return TABLE1_onclick()">
           
           <tr>
                <td ><asp:CheckBox ID="company_all" Text="all" runat="server" />
                    </td>
                <td >
                    </td>
                <td >
                    </td>
            </tr>
            <tr>
                <td    ><asp:CheckBox  Text="general contractor" ID="company_general_contractor" runat="server" />
                    </td>
                <td   >
                    <asp:CheckBox  Text="cleaning" ID="company_cleaning" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="HVAC" ID="company_hvac" runat="server" /></td>
            </tr>
            <tr>
                <td   >
                    <asp:CheckBox  Text="interior contractor" ID="company_interior_contractor" runat="server" /></td>
                <td   >
                    <asp:CheckBox  Text="decoration" ID="company_decoration" runat="server" /></td>
                <td   >
                    <asp:CheckBox  Text="inspection" ID="company_inspection" runat="server" /></td>
            </tr>
            <tr>
                <td   >
                    <asp:CheckBox  Text="exterior contractor" ID="company_exterior_contractor" runat="server" /></td>
                <td   >
                    <asp:CheckBox  Text="doors & windows" ID="company_doors_windows" runat="server" /></td>
                <td   >
                    <asp:CheckBox  Text="kitchen" ID="company_kitchen" runat="server" /></td>
            </tr>
            <tr>
                <td   >
                    <asp:CheckBox  Text="alarms & security systems" ID="company_alarms_security_systems" runat="server" /></td>
                <td   >
                    <asp:CheckBox  Text="electrical" ID="company_electrical" runat="server" /></td>
                <td   >
                    <asp:CheckBox  Text="locksmith" ID="company_locksmith" runat="server" /></td>
            </tr>
            <tr>
                <td   >
                   <asp:CheckBox  Text="architech" ID="company_architech" runat="server" /></td>
                <td   >
                    <asp:CheckBox  Text="engineer" ID="company_engineer" runat="server" /></td>
                <td   ><asp:CheckBox  Text="painting" ID="company_painting" runat="server" />
                    </td>
            </tr>
            <tr>
                <td   >
                    <asp:CheckBox  Text="basement" ID="company_basement" runat="server" /></td>
                <td   >
                    <asp:CheckBox  Text="flooring" ID="company_flooring" runat="server" /></td>
                <td   >
                    <asp:CheckBox  Text="paving" ID="company_paving" runat="server" /></td>
            </tr>
            <tr>
                <td   >
                    <asp:CheckBox  Text="bricks" ID="company_bricks" runat="server" /></td>
                <td   >
                   <asp:CheckBox  Text="foundation" ID="company_foundation" runat="server" /></td>
                <td   >
                    <asp:CheckBox  Text="plumbing" ID="company_plumbing" runat="server" /></td>
            </tr>
            <tr>
                <td   >
                    <asp:CheckBox  Text="cable , satellite , dish" ID="company_cable_satellite_dish" runat="server" /></td>
                <td   >
                    <asp:CheckBox  Text="gardening" ID="company_gardening" runat="server" /></td>
                <td   >
                    <asp:CheckBox  Text="roofs" ID="company_roofs" runat="server" /></td>
            </tr>
            <tr>
                <td   >
                    <asp:CheckBox  Text="ciment" ID="company_ciment" runat="server" /></td>
                <td   >
                    <asp:CheckBox   Text="gypse installation" ID="company_gypse_installation" runat="server" /></td>
                <td   >
                    <asp:CheckBox  Text="other" ID="company_other" runat="server" /></td>
            </tr>
        </table>
        
        <br />
    <div>
    <asp:GridView ID="dgcompany_list" runat="server" 
    AutoGenerateColumns="false" Width="83%"
     GridLines="Both"  PageSize="10"  AllowPaging="true" 
      AlternatingRowStyle-BackColor="#F0F0F6" 
      BorderColor="#CDCDCD"  BorderWidth="1"  
      HeaderStyle-BackColor="#F0F0F6" 
     OnPageIndexChanging="dgcompany_list_PageIndexChanging">
    <Columns>
    
    <asp:HyperLinkField  DataTextField="company_name" 
     DataNavigateUrlFields="company_id" 
     DataNavigateUrlFormatString="~/manager/company/company_view.aspx?company_id={0}" 
      HeaderText="Company name" />
    
    <asp:BoundField   DataField="company_addr_no"  HeaderText="Address #"/>
    <asp:BoundField  DataField="company_addr_street" HeaderText="Street" />
    <asp:BoundField  DataField="company_city" HeaderText="City"/>
    <asp:BoundField  DataField="company_tel" HeaderText="Telephone" />
    </Columns>
    
    
    </asp:GridView>
   
    </div>
   
   </asp:Content>