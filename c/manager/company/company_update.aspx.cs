using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.AccountObjectAuthorization;
/// <summary>
/// Done by : Stanley Jocelyn
/// date : sept 3, 2007
/// modified : oct 10 , 2007
/// Updating informations about a company
/// </summary>

public partial class company_company_update : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!RegEx.IsInteger(Request.QueryString["company_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        ////////
        //////// SECURITY CHECK BEGIN //////////////////
        ////////
        AccountObjectAuthorization companyAuthorization = new AccountObjectAuthorization(strconn);

        if (companyAuthorization.Company(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["company_id"])))
        { }
        else
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ////////
        //////// SECURITY CHECK END //////////////////
        ////////

        if (!(Page.IsPostBack))
        {

            reg_company_city.ValidationExpression = RegEx.getText();
            reg_company_addr_street.ValidationExpression = RegEx.getText();
            reg_company_contact_email.ValidationExpression = RegEx.getEmail();
            reg_company_contact_fname.ValidationExpression = RegEx.getText();
            reg_company_contact_lname.ValidationExpression = RegEx.getText();
            reg_company_contact_tel.ValidationExpression = RegEx.getText();
            reg_company_name.ValidationExpression = RegEx.getText();
            reg_company_pc.ValidationExpression = RegEx.getText();
            reg_company_tel.ValidationExpression = RegEx.getText();
            reg_company_website.ValidationExpression = RegEx.getText();
            reg_company_prov.ValidationExpression = RegEx.getText();


            // View yhe company info
           //   ( MARS ) Multiple Active resultsets
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prCompanyView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@company_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["company_id"]);
            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                    company_name.Text = dr["company_name"].ToString();
                    company_website.Text = dr["company_website"].ToString();
                   // company_addr_no.Text = dr["company_addr_no"].ToString();
                    company_addr_street.Text = dr["company_addr_street"].ToString();
                    company_city.Text = dr["company_city"].ToString();
                    company_prov.Text = dr["company_prov"].ToString();
                    company_pc.Text = dr["company_pc"].ToString();
                    company_tel.Text = dr["company_tel"].ToString();
                    company_contact_fname.Text = dr["company_contact_fname"].ToString();
                    company_contact_lname.Text = dr["company_contact_lname"].ToString();
                    company_contact_tel.Text = dr["company_contact_tel"].ToString();
                    company_contact_email.Text = dr["company_contact_email"].ToString();

                    if (dr["company_type"].ToString() == "b")
                        radio_contractor.SelectedValue = "1";
                    else
                        radio_contractor.SelectedValue = "0";
               
                }

            }
            finally
            {
             //   conn.Close();
            }


            // View company categories

            // view the company categ ( canoot do it with databinder --> bit (checkbox has no values ) )

           // SqlConnection conn2 = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd2 = new SqlCommand("prCompanyCategView", conn);
            cmd2.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@company_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["company_id"]);

            try
            {
               // conn.Open();

                SqlDataReader dr2 = null;
                dr2 = cmd2.ExecuteReader(CommandBehavior.SingleRow);

                while (dr2.Read() == true)
                {

                    if (Convert.ToInt32(dr2["company_general_contractor"]) == 1)
                        company_general_contractor.Checked = true;

                    if (Convert.ToInt32(dr2["company_interior_contractor"]) == 1)
                        company_interior_contractor.Checked = true;

                    if (Convert.ToInt32(dr2["company_cleaning"]) == 1)
                        company_cleaning.Checked = true;

                    if (Convert.ToInt32(dr2["company_decoration"]) == 1)
                        company_decoration.Checked = true;

                    if (Convert.ToInt32(dr2["company_gypse_installation"]) == 1)
                        company_gypse_installation.Checked = true;

                    if (Convert.ToInt32(dr2["company_painting"]) == 1)
                        company_painting.Checked = true;

                    if (Convert.ToInt32(dr2["company_plumbing"]) == 1)
                        company_plumbing.Checked = true;

                    if (Convert.ToInt32(dr2["company_basement"]) == 1)
                        company_basement.Checked = true;

                    if (Convert.ToInt32(dr2["company_exterior_contractor"]) == 1)
                        company_exterior_contractor.Checked = true;

                    if (Convert.ToInt32(dr2["company_inspection"]) == 1)
                        company_inspection.Checked = true;

                    if (Convert.ToInt32(dr2["company_hvac"]) == 1)
                        company_hvac.Checked = true;

                    if (Convert.ToInt32(dr2["company_kitchen"]) == 1)
                        company_kitchen.Checked = true;

                    if (Convert.ToInt32(dr2["company_electrical"]) == 1)
                        company_electrical.Checked = true;

                    if (Convert.ToInt32(dr2["company_doors_windows"]) == 1)
                        company_doors_windows.Checked = true;

                    if (Convert.ToInt32(dr2["company_alarms_security_systems"]) == 1)
                        company_alarms_security_systems.Checked = true;

                    if (Convert.ToInt32(dr2["company_paving"]) == 1)
                        company_paving.Checked = true;

                    if (Convert.ToInt32(dr2["company_flooring"]) == 1)
                        company_flooring.Checked = true;

                    if (Convert.ToInt32(dr2["company_roofs"]) == 1)
                        company_roofs.Checked = true;

                    if (Convert.ToInt32(dr2["company_gardening"]) == 1)
                        company_gardening.Checked = true;

                    if (Convert.ToInt32(dr2["company_locksmith"]) == 1)
                        company_locksmith.Checked = true;

                    if (Convert.ToInt32(dr2["company_architech"]) == 1)
                        company_architech.Checked = true;

                    if (Convert.ToInt32(dr2["company_engineer"]) == 1)
                        company_engineer.Checked = true;

                    if (Convert.ToInt32(dr2["company_bricks"]) == 1)
                        company_bricks.Checked = true;

                    if (Convert.ToInt32(dr2["company_foundation"]) == 1)
                        company_foundation.Checked = true;

                    if (Convert.ToInt32(dr2["company_cable_satellite_dish"]) == 1)
                        company_cable_satellite_dish.Checked = true;

                    if (Convert.ToInt32(dr2["company_other"]) == 1)
                        company_other.Checked = true;

                    if (Convert.ToInt32(dr2["company_ciment"]) == 1)
                        company_ciment.Checked = true;


                }

            }

            finally
            {
                conn.Close();
            }

            
        }

    }


    protected void btn_update_Click(object sender, EventArgs e)
    {

        Page.Validate();
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prCompanyUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

       
        
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return_update", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.NVarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@company_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["company_id"]);
            cmd.Parameters.Add("@company_name", SqlDbType.NVarChar, 50).Value = company_name.Text;
            cmd.Parameters.Add("@company_website", SqlDbType.NVarChar, 200).Value = company_website.Text;
         //   cmd.Parameters.Add("@company_addr_no", SqlDbType.NVarChar, 50).Value = company_addr_no.Text;
            cmd.Parameters.Add("@company_addr_street", SqlDbType.NVarChar, 200).Value = company_addr_street.Text;
            cmd.Parameters.Add("@company_city", SqlDbType.NVarChar, 50).Value = company_city.Text;
            cmd.Parameters.Add("@company_prov", SqlDbType.NVarChar, 50).Value = company_prov.Text;
            cmd.Parameters.Add("@company_pc", SqlDbType.NVarChar, 50).Value = company_pc.Text;
            cmd.Parameters.Add("@company_tel", SqlDbType.NVarChar, 50).Value = company_tel.Text;
            cmd.Parameters.Add("@company_contact_fname", SqlDbType.NVarChar, 50).Value = company_contact_fname.Text;
            cmd.Parameters.Add("@company_contact_lname", SqlDbType.NVarChar, 50).Value = company_contact_lname.Text;
            cmd.Parameters.Add("@company_contact_tel", SqlDbType.NVarChar, 50).Value = company_contact_tel.Text;
            cmd.Parameters.Add("@company_contact_email", SqlDbType.NVarChar, 50).Value = company_contact_email.Text;



            if (radio_contractor.SelectedValue == "1")
                cmd.Parameters.Add("@company_type", SqlDbType.Char, 1).Value = "b";
            else
                cmd.Parameters.Add("@company_type", SqlDbType.Char, 1).Value = "c";



            if (company_general_contractor.Checked == true)
                cmd.Parameters.Add("@company_general_contractor", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_general_contractor", SqlDbType.Bit).Value = 0;

            if (company_interior_contractor.Checked == true)
                cmd.Parameters.Add("@company_interior_contractor", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_interior_contractor", SqlDbType.Bit).Value = 0;

            if (company_exterior_contractor.Checked == true)
                cmd.Parameters.Add("@company_exterior_contractor", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_exterior_contractor", SqlDbType.Bit).Value = 0;

            if (company_cleaning.Checked == true)
                cmd.Parameters.Add("@company_cleaning", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_cleaning", SqlDbType.Bit).Value = 0;


            if (company_painting.Checked == true)
                cmd.Parameters.Add("@company_painting", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_painting", SqlDbType.Bit).Value = 0;

            if (company_paving.Checked == true)
                cmd.Parameters.Add("@company_paving", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_paving", SqlDbType.Bit).Value = 0;


            if (company_plumbing.Checked == true)
                cmd.Parameters.Add("@company_plumbing", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_plumbing", SqlDbType.Bit).Value = 0;


            if (company_decoration.Checked == true)
                cmd.Parameters.Add("@company_decoration", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_decoration", SqlDbType.Bit).Value = 0;

            if (company_doors_windows.Checked == true)
                cmd.Parameters.Add("@company_doors_windows", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_doors_windows", SqlDbType.Bit).Value = 0;

            if (company_bricks.Checked == true)
                cmd.Parameters.Add("@company_bricks", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_bricks", SqlDbType.Bit).Value = 0;

            if (company_foundation.Checked == true)
                cmd.Parameters.Add("@company_foundation", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_foundation", SqlDbType.Bit).Value = 0;

            if (company_alarms_security_systems.Checked == true)
                cmd.Parameters.Add("@company_alarms_security_systems", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_alarms_security_systems", SqlDbType.Bit).Value = 0;

            if (company_cable_satellite_dish.Checked == true)
                cmd.Parameters.Add("@company_cable_satellite_dish", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_cable_satellite_dish", SqlDbType.Bit).Value = 0;

            if (company_ciment.Checked == true)
                cmd.Parameters.Add("@company_ciment", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_ciment", SqlDbType.Bit).Value = 0;

            if (company_other.Checked == true)
                cmd.Parameters.Add("@company_other", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_other", SqlDbType.Bit).Value = 0;

            if (company_hvac.Checked == true)
                cmd.Parameters.Add("@company_hvac", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_hvac", SqlDbType.Bit).Value = 0;

            if (company_engineer.Checked == true)
                cmd.Parameters.Add("@company_engineer", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_engineer", SqlDbType.Bit).Value = 0;

            if (company_gypse_installation.Checked == true)
                cmd.Parameters.Add("@company_gypse_installation", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_gypse_installation", SqlDbType.Bit).Value = 0;

            if (company_architech.Checked == true)
                cmd.Parameters.Add("@company_architech", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_architech", SqlDbType.Bit).Value = 0;

            if (company_gardening.Checked == true)
                cmd.Parameters.Add("@company_gardening", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_gardening", SqlDbType.Bit).Value = 0;

            if (company_roofs.Checked == true)
                cmd.Parameters.Add("@company_roofs", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_roofs", SqlDbType.Bit).Value = 0;

            if (company_flooring.Checked == true)
                cmd.Parameters.Add("@company_flooring", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_flooring", SqlDbType.Bit).Value = 0;

            if (company_basement.Checked == true)
                cmd.Parameters.Add("@company_basement", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_basement", SqlDbType.Bit).Value = 0;

            if (company_inspection.Checked == true)
                cmd.Parameters.Add("@company_inspection", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_inspection", SqlDbType.Bit).Value = 0;

            if (company_kitchen.Checked == true)
                cmd.Parameters.Add("@company_kitchen", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_kitchen", SqlDbType.Bit).Value = 0;

            if (company_electrical.Checked == true)
                cmd.Parameters.Add("@company_electrical", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_electrical", SqlDbType.Bit).Value = 0;

            if (company_locksmith.Checked == true)
                cmd.Parameters.Add("@company_locksmith", SqlDbType.Bit).Value = 1;
            else
                cmd.Parameters.Add("@company_locksmith", SqlDbType.Bit).Value = 0;

            //execute the modification
            cmd.ExecuteNonQuery();


            if (Convert.ToInt32(cmd.Parameters["@return_update"].Value) == 0)
                lbl_update_success.Text = " UPDATE SUCCESSFULL";


            conn.Close();







    }
}

