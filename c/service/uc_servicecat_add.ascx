﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_servicecat_add.ascx.cs" Inherits="c_service_uc_servicecat_add" %>


<asp:Label CssClass="lead" ID="lit_cities_in" Text="<%$ Resources:service,service_add_servicecat%>" runat="server" /><span ID="span_zone" style=" color:Blue;font-size:large" clientidmode="Static" runat="server"/>
    <br />

<div style="float:left;width:100%;text-align:right;">
    
<a href="?p=catlist"><i class="icon-list-alt"></i><%= Resources.service.service_category%></a> &nbsp;
<a href="?p=list"><i class="icon-list-alt"></i><%= Resources.service.service_service%></a> &nbsp;
<a href="?p=catadd"><i class="icon-plus"></i><%= Resources.service.service_category%></a>&nbsp;
<a href="?p=add"><i class="icon-plus"></i><%= Resources.service.service_service%></a></div>


 <asp:Label ID="lbl_successful_add" ForeColor="Blue" runat="server" ></asp:Label><br />

    <dl class="dl-horizontal" runat="server" id="dlh1" >

      
      <dt> <%= Resources.service.service_name_en%></dt>
      <dd><asp:TextBox Height="28px" ID="tbx_sc_name_en" runat="server"></asp:TextBox>
      <asp:RegularExpressionValidator 
                                ID="reg_sc_name_en" runat="server" 
                                 ControlToValidate="tbx_sc_name_en"  Font-Bold="true" ForeColor="Red"
                                ErrorMessage="<%$ Resources:Resource, global_lb_invalid %>"> </asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator  ControlToValidate="tbx_sc_name_en"
                                   ID="req_sc_name_en" runat="server"  Font-Bold="true" ForeColor="Red"
                                ErrorMessage="<%$ Resources:Resource, global_lb_required %>"></asp:RequiredFieldValidator>
                     &nbsp;&nbsp;<asp:Label ID="lbl_sc_name_en_result" Font-Bold="true" ForeColor="Red" runat="server"></asp:Label>
 
      </dd>
     
      <dt> <%= Resources.service.service_name_fr%></dt>
      <dd><asp:TextBox Height="28px" ID="tbx_sc_name_fr" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator 
                                ID="reg_sc_name_fr" runat="server" 
                                 ControlToValidate="tbx_sc_name_fr"  Font-Bold="true" ForeColor="Red"
                                ErrorMessage="<%$ Resources:Resource, global_lb_invalid %>"> </asp:RegularExpressionValidator>
                        <asp:RequiredFieldValidator  ControlToValidate="tbx_sc_name_fr"
                                   ID="req_sc_name_fr" runat="server"  Font-Bold="true" ForeColor="Red"
                                ErrorMessage="<%$ Resources:Resource, global_lb_required %>"></asp:RequiredFieldValidator>
                               &nbsp;&nbsp;<asp:Label ID="lbl_sc_name_fr_result" Font-Bold="true" ForeColor="Red" runat="server"></asp:Label>
 
      </dd>
           
 </dl>
      <p>
      </p>
  <dl class="dl-horizontal"  runat="server" id="dl3">
     <dd> 
         <asp:Button ID="btn_add" ClientIDMode="Static" CssClass="btn btn-primary" 
             Text="<%$ Resources:service,service_add %>" runat="server" onclick="btn_add_Click" 
              /></dd>
   </dl>
<asp:Label ID="Label1" runat="server"></asp:Label>
