﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class c_service_uc_servicecat_list : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PopulateCateg();
        }
    }


    private void PopulateCateg()
    {
        sinfoca.tiger.Service crc = new sinfoca.tiger.Service();
        r_servicecate.DataSource = crc.getServiceCategList();
        r_servicecate.DataBind();
    }
}