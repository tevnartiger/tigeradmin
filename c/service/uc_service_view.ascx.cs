﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class c_tax_uc_tax_view : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            sinfoca.tiger.Service tfc = new sinfoca.tiger.Service();

           
            DataTable dt = tfc.getServiceView(Convert.ToInt32(Request.QueryString["srv_id"]));
               
            foreach (DataRow row in dt.Rows)
            {
                lbl_service_categ.Text = row["sc_name_en"].ToString();
                lbl_service_name_en.Text = row["service_title_en"].ToString();
                lbl_service_name_fr.Text = row["service_title_fr"].ToString();
                lbl_service_url.Text = row["service_url"].ToString();
                lbl_service_desc.Text = Convert.ToString(row["service_desc"]);
            
            }
        }
    }
}