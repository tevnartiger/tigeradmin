﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using sinfoca.login;

public partial class ProcessData : BasePageLite
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Clear();
        Response.ClearContent();

        List<string> queryStringList = new List<string>();
        queryStringList.Add("taxarchives");
        queryStringList.Add("taxpending");
        queryStringList.Add("servicecount");
        queryStringList.Add("taxpendingcount");
        queryStringList.Add("taxarchivescount");
        queryStringList.Add("region");
        queryStringList.Add("servicelist");
        queryStringList.Add("taxpendinglist");
        queryStringList.Add("taxarchiveslist");
        queryStringList.Add("city");


        int category_id = 0;
      

        string search = "";

        int a = 0;
        int b = 0;


     
        string CategoryID = Request.QueryString["CategoryID"];
        if (CategoryID != null && RegEx.IsInteger(CategoryID))
        {
            category_id = Convert.ToInt32(CategoryID);
        }

    

        if (Request.QueryString["ag"] != null && RegEx.IsInteger(Request.QueryString["ag"]))
        {
            a = Convert.ToInt32(Request.QueryString["ag"]);
        }

        if (Request.QueryString["bg"] != null && RegEx.IsInteger(Request.QueryString["bg"]))
        {
            b = Convert.ToInt32(Request.QueryString["bg"]);
        }

        if (queryStringList.Contains(Request.QueryString["fct"]))
        {
            switch (Request.QueryString["fct"])
            {

                case "servicecount": Response.Write(ServiceCount(a, b, category_id));
                    break;

                case "servicelist": Response.Write(ServiceList(a, b, category_id));
                    break;

            }
            Response.End();
        }



        if (Request.Form["ag"] != null && Request.Form["bg"] != null && Request.Form["cg"] != null && Request.Form["fct"] != null)
        {

            string sms = Request.Form["ag"].ToString().Replace(" ","+");
            string email = Request.Form["bg"].ToString();
            string person_sms_emailList = Request.Form["cg"].ToString();

           // string resp = sendEmailSMS(person_sms_emailList, sms,email);
            
            Response.Write("Success ");
            Response.End();
       }

      
    }




    public string ServiceCount(int maxRows, int startRowIndex, int category_id)
    {


        sinfoca.tiger.Service tfc = new sinfoca.tiger.Service();
        int list = tfc.getServiceCount( maxRows,startRowIndex, category_id);
       // return list.Replace("}]}", "}]").Replace("{\"tfr\" :", "");

        return list.ToString();
    }


    public string ServiceList(int maxRows, int startRowIndex, int category_id)
    {
        sinfoca.tiger.Service tfc = new sinfoca.tiger.Service();
        string list = Utils.GetJSONString(tfc.getServiceList( maxRows,startRowIndex, category_id));
        return list.Replace("}]}", "}]").Replace("{\"Service\" :", "");
    }


  

}