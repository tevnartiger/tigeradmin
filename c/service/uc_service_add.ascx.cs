﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class c_tax_uc_tax_add : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PopulateCateg();

            reg_service_name_en.ValidationExpression = RegEx.text;
            reg_service_name_fr.ValidationExpression = RegEx.text;
            reg_service_url.ValidationExpression = RegEx.text;
            reg_service_desc.ValidationExpression = RegEx.text;
        }
    }



    private void PopulateCateg()
    {
        sinfoca.tiger.Service crc = new sinfoca.tiger.Service();
        ddlCategory.DataSource = crc.getServiceCategList();
        ddlCategory.DataValueField = "sc_id";
        ddlCategory.DataTextField = "sc_name_en";
        ddlCategory.DataBind();
        ddlCategory.Items.Insert(0, new ListItem(Resources.service.service_select_category, "0"));
    }



    protected void btn_add_Click(object sender, EventArgs e)
    {

        lbl_category_result.Text = "";
        lbl_service_name_en_result.Text = "";
        lbl_service_name_fr_result.Text = "";


        bool response = true;

        if (ddlCategory.SelectedValue == "0")
        {
            lbl_category_result.Text = Resources.Resource.global_lb_required;
            response = false;

        }

        //lbl_successful_add.Text = Resources.service.service_saved;
       
        Page.Validate();
        if (Page.IsValid)
        {
      
          

            if (response == true)
            {
                sinfoca.tiger.Service tx = new sinfoca.tiger.Service();

                tx.Service_name = tbx_service_name_en.Text;
                tx.Service_name_fr = tbx_service_name_fr.Text;
                tx.Service_url = tbx_service_url.Text;
                tx.Service_comment =  tbx_service_desc.Text.Replace(System.Environment.NewLine, "<br/>");
                tx.Sc_id = Convert.ToInt32(ddlCategory.SelectedValue);




                int tc = tx.setServiceAdd();
                if (tc > 0)
                {
                    lbl_successful_add.Text = Resources.service.service_saved;
                }

                if (tc == -1)
                {
                    lbl_service_name_en_result.Text = Resources.service.service_already_exist;
                }

                if (tc == -2)
                {
                    lbl_service_name_fr_result.Text = Resources.service.service_already_exist;
                }

                if (tc == -3)
                {
                    lbl_service_name_en_result.Text = Resources.service.service_already_exist;
                    lbl_service_name_fr_result.Text = Resources.service.service_already_exist;
                }
            }
        }
    }


 
}