﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class c_service_uc_servicecat_add : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            reg_sc_name_en.ValidationExpression = RegEx.text;
            reg_sc_name_fr.ValidationExpression = RegEx.text;
        }
    }




    protected void btn_add_Click(object sender, EventArgs e)
    {

        lbl_sc_name_en_result.Text = "";
        lbl_sc_name_fr_result.Text = "";

        bool response = true;

        Page.Validate();
        if (Page.IsValid)
        {



            if (response == true)
            {
                sinfoca.tiger.Service tx = new sinfoca.tiger.Service();

                tx.Sc_name = tbx_sc_name_en.Text;
                tx.Sc_name_fr = tbx_sc_name_fr.Text;
                



                int tc = tx.setServiceCategAdd();
                if (tc > 0)
                {
                    lbl_successful_add.Text = Resources.service.service_saved;
                }

                if (tc == -1)
                {
                    lbl_sc_name_en_result.Text = Resources.service.service_already_exist;
                }

                if (tc == -2)
                {
                    lbl_sc_name_fr_result.Text = Resources.service.service_already_exist;
                }

                if (tc == -3)
                {
                    lbl_sc_name_en_result.Text = Resources.service.service_already_exist;
                    lbl_sc_name_fr_result.Text = Resources.service.service_already_exist;
                }
            }
        }
    }


 
}