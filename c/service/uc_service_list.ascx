﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_service_list.ascx.cs" Inherits="c_tax_uc_tax_list" %>
<script src="jquery.simplemodal.1.4.1.min.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="jsrender.js"></script>
<script language="javascript" type="text/javascript" src="json2.js"></script>
<script type="text/javascript"  src="jquery.pagination.js"></script>


<script type="text/javascript"  src="../../js/bootstrap.js"></script>

<link rel="stylesheet" href="pagination.css" />
 <script type="text/javascript">
     // When document has loaded, initialize pagination and form
     $(document).ready(function () {
         //--------------------------------------------------------------------------------------------------------------
         $("#ddlCategory").val("0");  // DEFAULT COUNTRY  /////////////////////////////////////////////////

         var CategoryID = $("#ddlCategory option:selected").val();
         $("#h_category_id").val(CategoryID);

         /////////////////////////////////////////     ONChange COUNTRY      //////////////////////////////////////////////////////////////////////

         $("#ddlCategory").change(function () {
             
             var CategoryID = $("#ddlCategory option:selected").val();
          

             $("#details2").html("");

             $.get("ProcessData.aspx", { ag: "10", bg: 0, CategoryID: CategoryID, fct: "servicelist" },
                   function (data) {
                       var mydata = jQuery.parseJSON(data);
                       // Render to string
                       // alert(data);

                       var html2 = $("#appTmpl2").render(mydata);
                       // Insert as HTML
                       $("#details2").html(html2);
                       //disable checkbox if no cell number
                   });


             $.get("ProcessData.aspx", { ag: "10", bg: 0, CategoryID: CategoryID, fct: "servicecount" },
                 function (data) {
                     //  alert($("#ddlState option:selected").val()); 
                     // alert(data);
                     $("#paginator").pagination(data, { items_per_page: 10, callback: handlePaginationClick });
                 });

                 $("#h_category_id").val(CategoryID);

         });

     })
    </script>  









     <script type="text/javascript">

         /**
         * Callback function that displays the content.
         *
         * Gets called every time the user clicks on a pagination link.
         *
         * @param {int}page_index New Page index
         * @param {jQuery} jq the container with the pagination links as a jQuery object
         */
         function handlePaginationClick(page_index, jq) {
             var item_page = 10;
           

             $.get("ProcessData.aspx", { ag: "10", bg: page_index * item_page, CategoryID: $("#h_category_id").val(), fct: "servicelist" },
                   function (data) {
                       var mydata = jQuery.parseJSON(data);
                       // Render to string
                       //alert(data);
                       var html2 = $("#appTmpl2").render(mydata);
                       // Insert as HTML
                       $("#details2").html(html2);
                       //disable checkbox if no cell number
                   });


             return false;
         }



         // The form contains fields for many pagiantion optiosn so you can
         // quickly see the resuluts of the different options.
         // This function creates an option object for the pagination function.
         // This will be be unnecessary in your application where you just set
         // the options once.


         // When document has loaded, initialize pagination and form
         $(document).ready(function () {
             // alert("document ready");
             // Create pagination element with options from form
             $.get("ProcessData.aspx", { ag: "10", bg: "0", CategoryID: $("#h_category_id").val(), fct: "servicecount" },
                 function (data) {
                     //  alert($("#ddlState option:selected").val()); 
                     $("#paginator").pagination(data, { items_per_page: 10, callback: handlePaginationClick });
                 });

         });

        </script>


      <dl class="dl-horizontal">
      <dt><%= Resources.service.service_category%></dt>
      <dd><asp:DropDownList ClientIDMode="Static"  CausesValidation="True" ID="ddlCategory" runat="server" /></dd>
          
      
      </dl>
  <div style="float:left;width:100%;text-align:right;">
    
<a href="?p=catlist"><i class="icon-list-alt"></i><%= Resources.service.service_category%></a> &nbsp;
<a href="?p=list"><i class="icon-list-alt"></i><%= Resources.service.service_service%></a> &nbsp;
<a href="?p=catadd"><i class="icon-plus"></i><%= Resources.service.service_category%></a>&nbsp;
<a href="?p=add"><i class="icon-plus"></i><%= Resources.service.service_service%></a></div>

      

 

<!--====== Template ======-->

        <script id="appTmpl2" type="text/x-jquery-tmpl">
        <tr>
         <td>{{=sc_name_en}}</td>
         <td>{{=service_title_en}}</td>
         <td>{{=service_url}}</td>
         <td> <a href="/c/service/service.aspx?p=view&srv_id={{=service_id}}" class="icon-search"    />
              &nbsp;&nbsp&nbsp;<a href="/c/service/service.aspx?p=upd&srv_id={{=service_id}}" class="icon-edit"/>..
         </td>     
      </tr>
        </script>
     <!--====== Container ======-->

        
        <table class="table table-bordered table-condensed table-hover">
        <thead>
        <tr>
         <th><strong><%= Resources.service.service_category %></strong></th>
         <th><strong><%= Resources.service.service_service%></strong></th>
         <th><strong><%= Resources.service.service_url%></strong></th>
         <th><strong>Action</strong></th>
         </tr>
        </thead>
        <tbody id="details2"></tbody>
        </table>

        <div id="paginator"  class="pagination2"></div>


 



        <br />
       
       
        <div id="h_ids"></div>
        <div id="h_state_id"></div>
        <div id="h_category_id"></div>
        <div id="h_city_id"></div>
        <div id="h_ids_pending"></div>
        <div id="h_state_id_pending"></div>
        <div id="h_country_id_pending"></div>
        <div id="h_ids_archives"></div>
        <div id="h_state_id_archives"></div>
        <div id="h_country_id_archives"></div>  
 