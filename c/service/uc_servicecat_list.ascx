﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_servicecat_list.ascx.cs" Inherits="c_service_uc_servicecat_list" %>

<h4><%= Resources.service.service_service_category%></h4>
  <div style="float:left;width:100%;text-align:right;">
    
<a href="?p=catlist"><i class="icon-list-alt"></i><%= Resources.service.service_category%></a> &nbsp;
<a href="?p=list"><i class="icon-list-alt"></i><%= Resources.service.service_service%></a> &nbsp;
<a href="?p=catadd"><i class="icon-plus"></i><%= Resources.service.service_category%></a>&nbsp;
<a href="?p=add"><i class="icon-plus"></i><%= Resources.service.service_service%></a></div>

     <table class="table table-bordered table-condensed table-hover">
        <thead>
             <tr>
                 <th><strong><%= Resources.service.service_category %> (en)</strong></th>
                 <th><strong><%= Resources.service.service_category%> (fr)</strong></th>
                 <th><strong>Action</strong></th>
             </tr>
        </thead>
           <asp:Repeater ID="r_servicecate" runat="server">
             <ItemTemplate>
               <tr>
                <td> <asp:Label ID="sc_name_en" runat="server"  Text='<%# Bind("sc_name_en")%>' /> </td>
                <td> <asp:Label ID="sc_name_fr" runat="server"  Text='<%# Bind("sc_name_fr")%>' /> </td>
                <td> 
                  <a href="/c/service/service.aspx?p=catupd&sc_id=<%# DataBinder.Eval(Container.DataItem, "sc_id") %>" class="icon-edit"/>
                </td>   
               </tr>
              </ItemTemplate>
            </asp:Repeater>
          </table>
 