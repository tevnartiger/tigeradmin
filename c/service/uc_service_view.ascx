﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_service_view.ascx.cs" Inherits="c_tax_uc_tax_view" %>

<asp:Label CssClass="lead" ID="lit_cities_in" Text="<%$ Resources:service,service_view %>" runat="server" />
    <br />  
<div style="float:left;width:100%;text-align:right;">
    
<a href="?p=catlist"><i class="icon-list-alt"></i><%= Resources.service.service_category%></a> &nbsp;
<a href="?p=list"><i class="icon-list-alt"></i><%= Resources.service.service_service%></a> &nbsp;
<a href="?p=catadd"><i class="icon-plus"></i><%= Resources.service.service_category%></a>&nbsp;
<a href="?p=add"><i class="icon-plus"></i><%= Resources.service.service_service%></a></div>
<br />
   <h4><asp:Literal ID="lit_header" runat="server"></asp:Literal></h4>
   
 <div class="row">

 
    <dl class="dl-horizontal" runat="server" id="dlh1" >
      
      <dt> <%= Resources.service.service_category%></dt>
      <dd> <asp:Label  ForeColor="Blue" Font-Bold="true" ID="lbl_service_categ" runat="server"/></dd>
      
      <dt id="tx_name_en" runat="server"> <%= Resources.service.service_name_en%></dt>
      <dd id="tx_name_en_lbl" runat="server"> <asp:Label ID="lbl_service_name_en" runat="server"/></dd>
    
      <dt id="tx_name_fr" runat="server"> <%= Resources.service.service_name_fr%></dt>
      <dd id="tx_name_ff_lbl" runat="server"> <asp:Label ID="lbl_service_name_fr" runat="server"/></dd>
    

      <dt> <%= Resources.service.service_url%></dt>
      <dd> <asp:Label  ID="lbl_service_url" runat="server"/></dd>
     
      <dt> <%= Resources.service.service_desc%></dt>
      <dd> <asp:Label ID="lbl_service_desc" runat="server"/></dd>
      
      
   
 </dl>

 
 </div>




   