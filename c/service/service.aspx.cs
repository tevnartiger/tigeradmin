﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class c_tax_tax :  BasePageLite
{
    protected void Page_Load(object sender, EventArgs e)
    {

        Control FeaturedProductUserControl;
        switch (Request.QueryString["p"])
        {
            case "list":
                FeaturedProductUserControl = LoadControl("uc_service_list.ascx");
                break;
          
            case "catlist":
                FeaturedProductUserControl = LoadControl("uc_servicecat_list.ascx");
                break;

            case "upd":
                FeaturedProductUserControl = LoadControl("uc_service_update.ascx");
                break;

            case "view":
                FeaturedProductUserControl = LoadControl("uc_service_view.ascx");
                break;
           
            case "add":
                FeaturedProductUserControl = LoadControl("uc_service_add.ascx");
                break;
           
            case "catadd":
                FeaturedProductUserControl = LoadControl("uc_servicecat_add.ascx");
                break;

            case "catupd":
                FeaturedProductUserControl = LoadControl("uc_servicecat_update.ascx");
                break;

            default:
                FeaturedProductUserControl = LoadControl("uc_service_list.ascx");
                break;
        }

        panel_service.Controls.Add(FeaturedProductUserControl);

    }

}