﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class c_service_uc_servicecat_update : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            sinfoca.tiger.Service tfc = new sinfoca.tiger.Service();


            DataTable dt = tfc.getServiceCategView(Convert.ToInt32(Request.QueryString["sc_id"]));

            foreach (DataRow row in dt.Rows)
            {
                tbx_sc_name_en.Text = row["sc_name_en"].ToString();
                tbx_sc_name_fr.Text = row["sc_name_fr"].ToString();


                if (Convert.ToInt32(Session["schema_id"]) != Convert.ToInt32(row["schema_id"].ToString()) && Convert.ToInt32(Session["schema_id"]) != 0)
                {
                    btn_add.Enabled = false;
                }
             }

            reg_sc_name_en.ValidationExpression = RegEx.text;
            reg_sc_name_fr.ValidationExpression = RegEx.text;
        }
    }




    protected void btn_add_Click(object sender, EventArgs e)
    {

        lbl_sc_name_en_result.Text = "";
        lbl_sc_name_fr_result.Text = "";

        bool response = true;

        Page.Validate();
        if (Page.IsValid)
        {



            if (response == true)
            {
                sinfoca.tiger.Service tx = new sinfoca.tiger.Service();

                tx.Sc_name = tbx_sc_name_en.Text;
                tx.Sc_name_fr = tbx_sc_name_fr.Text;




                int tc = tx.setServiceCategUpdate(Convert.ToInt32(Request.QueryString["sc_id"]));
                if (tc > 0)
                {
                    lbl_successful_add.Text = Resources.service.service_category_edited;
                }

                if (tc == -1)
                {
                    lbl_sc_name_en_result.Text = Resources.service.service_already_exist;
                }

                if (tc == -2)
                {
                    lbl_sc_name_fr_result.Text = Resources.service.service_already_exist;
                }

                if (tc == -3)
                {
                    lbl_sc_name_en_result.Text = Resources.service.service_already_exist;
                    lbl_sc_name_fr_result.Text = Resources.service.service_already_exist;
                }
            }
        }
    }


}