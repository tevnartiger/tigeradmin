﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class c_tax_uc_tax_list : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PopulateCateg();
        }
    }


    private void PopulateCateg()
    {
        sinfoca.tiger.Service crc = new sinfoca.tiger.Service();
        ddlCategory.DataSource = crc.getServiceCategList();
        ddlCategory.DataValueField = "sc_id";
        ddlCategory.DataTextField = "sc_name_en";
        ddlCategory.DataBind();
        ddlCategory.Items.Insert(0, new ListItem(Resources.service.service_select_category, "0"));
    }
}