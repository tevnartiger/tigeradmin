﻿<%@ Page Title="" Language="C#" Debug="true"  MasterPageFile="~/mp_main.master" AutoEventWireup="true" CodeFile="roles.aspx.cs" Inherits="c_roleservice_role_list" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="lead">Roles</div>
<div style="float:left;width:100%;text-align:right;">
  
<a href="?p=list"><i class="icon-list-alt"></i></a> &nbsp;

<a href="?p=add"><i class="icon-plus"></i></a></div>
<div style="float:left;width:100%; min-height:400px;">
 <asp:Panel ID="panel_roles" runat="server" />
</div>
</asp:Content>