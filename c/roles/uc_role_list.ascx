﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_role_list.ascx.cs" Inherits="c_uc_uc_role_list" %>

   <asp:GridView ID="gv_role" runat="server" GridLines="None" onrowdatabound="gv_role_RowDataBound"
         AutoGenerateColumns="false"  CssClass="table table-condensed table-hover" >    
           
           <Columns>
           
            <asp:BoundField DataField="role_name"     HeaderText="Roles" ReadOnly="True"  />
            <asp:BoundField DataField="role_status"     HeaderText="Status" ReadOnly="True"  />
            
              <asp:TemplateField  HeaderText="Details">
                 <ItemTemplate >
                     <asp:HiddenField ID="h_schema_id" Value='<%#Bind("schema_id")%>' visible="false" runat="server" />
                     <asp:HiddenField ID="h_role_id" Value='<%#Bind("role_id")%>' visible="false" runat="server" />
                     <asp:HyperLink ID="link_role" runat="server" />
                 </ItemTemplate >
              </asp:TemplateField> 
              
           </Columns>
       </asp:GridView>