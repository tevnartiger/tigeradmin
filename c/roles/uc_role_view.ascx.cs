﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class c_roles_uc_role_view : System.Web.UI.UserControl
{
    private GridViewHelper helper;

    protected void Page_Load(object sender, EventArgs e)
    {
        string lang = "";

        if (!string.IsNullOrEmpty(Request.QueryString["lang"]))
        {
            switch (Request.QueryString["lang"])
            {
                case "en": lang = "sc_name_en";
                    break;

                case "fr": lang = "sc_name_fr";
                    break;

                default: lang = "sc_name_fr";
                    break;

            }
        }
        else
            switch (Session["_lastCulture"].ToString().Substring(0, 2))
            {
                case "en": lang = "sc_name_en";
                    break;

                case "fr": lang = "sc_name_fr";
                    break;

                default: lang = "sc_name_fr";
                    break;

            }


        GridViewHelper helper = new GridViewHelper(this.gv_services);
        helper.RegisterGroup(lang, true, true);
        helper.GroupHeader += new GroupEvent(helper_GroupHeader);
        helper.ApplyGroupSort();

        Roles role = new Roles();
        gv_services.DataSource = role.getRoleServices(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["role_id"]));
        gv_services.DataBind();

        gv_services.HeaderRow.Visible = false;

        switch (Session["_lastCulture"].ToString().Substring(0, 2))
        {
            case "en": gv_services.Columns[0].Visible = false;
                gv_services.Columns[3].Visible = false;
                break;

            case "fr": gv_services.Columns[1].Visible = false;
                gv_services.Columns[2].Visible = false;
                break;

            default: lang = "sc_name_fr";
                break;

        }
        if (!Page.IsPostBack)
        {
            role.getRoleNameAndDesc(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["role_id"]));
            lbl_role_name.Text = role.Role_name;
            lbl_role_desc.Text = role.Role_desc;

        }
    }

    protected bool RoleHasService(int rolehaservice)
    {
        if (rolehaservice == 1)
            return true;
        else
            return false;
    }

    protected void gv_services_Sorting(object sender, GridViewSortEventArgs e)
    {

    }


    private void helper_GroupHeader(string groupName, object[] values, GridViewRow row)
    {

        if (groupName == "sc_name_en")
        {
            // row.BackColor = Color.Salmon;
            row.Cells[0].Text = "<h4>" + row.Cells[0].Text + "</h4>";
        }


        if (groupName == "sc_name_fr")
        {
            // row.BackColor = Color.Salmon;
            row.Cells[0].Text = "<h4>" + row.Cells[0].Text + "</h4>";
        }

    }


}