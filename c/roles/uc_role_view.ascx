﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_role_view.ascx.cs" Inherits="c_roles_uc_role_view" %>
<style type="text/css">
     .style1
     {
         width: 40%;
     }
     .style2
     {
         width: 40%;
     }
 </style>
 <div class="lead" style="display:inline-block"><%= Resources.role.role_role %></div> :  <asp:Label style="display:inline-block" ID="lbl_role_name" runat="server"></asp:Label>
 <table class="style2">
    
    <tr>
        <td valign="top">
           <%= Resources.role.role_desc %></td>
        <td>
         <asp:Label ID="lbl_role_desc" TextMode="MultiLine" Width="250px"  runat="server"></asp:Label>
        </td>
    </tr>
    </table>


<div style="padding-left:80px;">
 <asp:GridView ID="gv_services" runat="server" AllowPaging="True" 
           AllowSorting="True"  GridLines="None"  CellSpacing="5" 
           AutoGenerateColumns="false"  OnSorting="gv_services_Sorting"
           EmptyDataText="" PageSize="100" >    
           
           <Columns>
           
            <asp:BoundField DataField="service_title_fr"     HeaderText="fr" ReadOnly="True"  />
            <asp:BoundField DataField="service_title_en"     HeaderText="en" ReadOnly="True"  />
            <asp:BoundField DataField="sc_name_en"   HeaderText="Category" ReadOnly="True"  />
           <asp:BoundField DataField="sc_name_fr"   HeaderText="Category" ReadOnly="True"  />
            <asp:TemplateField>
            <ItemTemplate>
                <asp:CheckBox ID="chk_service"  Checked='<%#RoleHasService(Convert.ToInt32(Eval("hasservice")))%>' runat="server" />
                <asp:HiddenField ID="h_service_id" Value='<%# Bind("service_id")%>' runat="server" />
            </ItemTemplate>
            </asp:TemplateField>           
           </Columns>
   </asp:GridView>
 </div>


<br /><br />

<asp:Label ID="Label1" runat="server"></asp:Label>

<br /><br />