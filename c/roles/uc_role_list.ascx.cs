﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class c_uc_uc_role_list : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Roles role = new Roles();
            gv_role.DataSource = role.getRoleList( Convert.ToInt16(Session["schema_id"]),"en", 100);
            gv_role.DataBind();

        }
    }

    protected void gv_role_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField a = e.Row.FindControl("h_schema_id") as HiddenField;
            HiddenField b = e.Row.FindControl("h_role_id") as HiddenField;
            HyperLink   c = e.Row.FindControl("link_role") as HyperLink;
          
            if (a.Value == "0" && Session["schema_id"].ToString() != "0")
            {
              //  e.Row.Cells[2].Visible = false;
              //  e.Row.Cells[2].Text = "view";
                c.NavigateUrl = "~/c/roles/roles.aspx?p=view&role_id="+b.Value;
                c.Text = "view";
               
            }
            else
            {
              //  e.Row.Cells[2].Visible = true;
             //   e.Row.Cells[2].Text = "<div class='roleedit' >edit</div>";
                c.NavigateUrl = "~/c/roles/roles.aspx?p=upd&role_id=" + b.Value;
                c.Text = "update";
            }
        }
    }
}