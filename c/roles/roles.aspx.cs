﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class c_roleservice_role_list : BasePageLite
{
    protected void Page_Load(object sender, EventArgs e)
    {

        Control FeaturedProductUserControl;
        switch (Request.QueryString["p"])
        {
           
            case "list":
                FeaturedProductUserControl = LoadControl("uc_role_list.ascx");
                break;
            case "upd":
                FeaturedProductUserControl = LoadControl("uc_role_update.ascx");
                break;
            case "view":
                FeaturedProductUserControl = LoadControl("uc_role_view.ascx");
                break;
            case "add":
                FeaturedProductUserControl = LoadControl("uc_role_add.ascx");
                break;
            default://season
                FeaturedProductUserControl = LoadControl("uc_role_list.ascx");
                break;
        }

        panel_roles.Controls.Add(FeaturedProductUserControl);
    }
}