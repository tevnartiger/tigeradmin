﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class c_uc_uc_role_update : System.Web.UI.UserControl
{
    private GridViewHelper helper;

    protected void Page_Load(object sender, EventArgs e)
    {
        string lang = "";

        if (!string.IsNullOrEmpty(Request.QueryString["lang"]))
        {
            switch (Request.QueryString["lang"])
            {
                case "en": lang = "sc_name_en";
                    break;

                case "fr": lang = "sc_name_fr";
                    break;

                default: lang = "sc_name_fr";
                    break;

            }
        }
        else
            switch (Session["_lastCulture"].ToString().Substring(0, 2))
            {
                case "en": lang = "sc_name_en";
                    break;

                case "fr": lang = "sc_name_fr";
                    break;

                default: lang = "sc_name_fr";
                    break;

            }


        GridViewHelper helper = new GridViewHelper(this.gv_services);
        helper.RegisterGroup(lang, true, true);
        helper.GroupHeader += new GroupEvent(helper_GroupHeader);
        helper.ApplyGroupSort();

        Roles role = new Roles();
        gv_services.DataSource = role.getRoleServices(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["role_id"]));
        gv_services.DataBind();

        gv_services.HeaderRow.Visible = false;

        switch (Session["_lastCulture"].ToString().Substring(0, 2))
        {
            case "en": gv_services.Columns[0].Visible = false;
                       gv_services.Columns[3].Visible = false; 
                break;

            case "fr":  gv_services.Columns[1].Visible = false;
                        gv_services.Columns[2].Visible = false;
                break;

            default: lang = "sc_name_fr";
                break;

        }
        if (!Page.IsPostBack)
        {
            role.getRoleNameAndDesc(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["role_id"]));
            tbx_role_name.Text = role.Role_name;
            tbx_role_desc.Text = role.Role_desc;

            if (Convert.ToInt32(Session["schema_id"]) != role.Role_out_schema_id && Convert.ToInt32(Session["schema_id"]) != 0)
            {
                Response.Redirect("~/c/roles/roles.aspx");
            }
        }
        /*       
          <asp:BoundField DataField="service_title_fr"     HeaderText="fr" ReadOnly="True"  />
            <asp:BoundField DataField="service_title_en"     HeaderText="en" ReadOnly="True"  />
            <asp:BoundField DataField="sc_name_en"   HeaderText="Category" ReadOnly="True"  />
           <asp:BoundField DataField="sc_name_fr"   HeaderText="Category" ReadOnly="True"  />
            
         */


    }

    protected bool RoleHasService(int rolehaservice)
    {
        if (rolehaservice == 1)
            return true;
        else
            return false;
    }

    protected void gv_services_Sorting(object sender, GridViewSortEventArgs e)
    {

    }


    private void helper_GroupHeader(string groupName, object[] values, GridViewRow row)
    {

        if (groupName == "sc_name_en")
        {
           // row.BackColor = Color.Salmon;
            row.Cells[0].Text = "<h4>" + row.Cells[0].Text + "</h4>";
        }


        if (groupName == "sc_name_fr")
        {
           // row.BackColor = Color.Salmon;
            row.Cells[0].Text = "<h4>" + row.Cells[0].Text + "</h4>";
        }
       
    }

    protected void btn_update2_Click(object sender, EventArgs e)
    {
        int nb_services = 0;
        int compteur = 1;
        DataTable services = new DataTable();

        //– Add columns to the data table
        services.Columns.Add("ID", typeof(int));
        services.Columns.Add("service_id", typeof(int));
        services.Columns.Add("haseservice", typeof(int));


        for (int j = 0; j < gv_services.Rows.Count; j++)
        {
            HiddenField h_service_id = (HiddenField)gv_services.Rows[j].FindControl("h_service_id");
            CheckBox chk_process = (CheckBox)gv_services.Rows[j].FindControl("chk_service");

            // if the checkbox is checked the process
            if (chk_process.Checked)
            {
                services.Rows.Add(compteur, Convert.ToInt32(h_service_id.Value), 1);
                nb_services++;
            }
            else
                services.Rows.Add(compteur, Convert.ToInt32(h_service_id.Value), 0);
            compteur++;
        }

        Roles role = new Roles();

        role.Role_name = RegEx.getText(tbx_role_name.Text);
        role.Role_desc = RegEx.getText(tbx_role_desc.Text);
        role.setRoleServiceUpdate(Convert.ToInt32(Request.QueryString["role_id"]), services);

        if(role.Return_id==0)
            Label1.Text = nb_services.ToString();
    }
}