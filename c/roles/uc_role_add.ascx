﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_role_add.ascx.cs" Inherits="c_uc_uc_role_add" %>
 <style type="text/css">
     .style1
     {
         width: 43%;
     }
     .style2
     {
         width: 43%;
     }
 </style>
<asp:Panel ID="panel_role" runat="server">

<br />
<table class="style2">
    <tr>
        <td>
            <%= Resources.role.role_name %></td>
        <td>
            <asp:TextBox ID="tbx_role_name" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td valign="top">
           <%= Resources.role.role_desc %></td>
        <td>
         <asp:TextBox ID="tbx_role_desc" TextMode="MultiLine" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td valign="top">
            &nbsp;</td>
        <td>
            <asp:Button ID="btn_add_role" runat="server" 
                CssClass="btn btn-mini btn-primary" onclick="btn_add_role_Click" 
                Text="<%$Resources:Resource,app_save %>" />
        </td>
    </tr>
</table>






<br />
 <asp:GridView ID="gv_services" runat="server" AllowPaging="True" 
           AllowSorting="True"  GridLines="None"  CellSpacing="5" 
           AutoGenerateColumns="false"  Width="33%" OnSorting="gv_services_Sorting"
           EmptyDataText="" PageSize="100" >    
           
           <Columns>
           
            <asp:BoundField DataField="service_title_fr"     HeaderText="fr" ReadOnly="True"  />
            <asp:BoundField DataField="service_title_en"     HeaderText="en" ReadOnly="True"  />
            <asp:BoundField DataField="sc_name_en"   HeaderText="Category" ReadOnly="True"  />
           <asp:BoundField DataField="sc_name_fr"   HeaderText="Category" ReadOnly="True"  />
            <asp:TemplateField>
            <ItemTemplate>
                <asp:CheckBox ID="chk_service"  Checked='<%#RoleHasService(Convert.ToInt32(Eval("hasservice")))%>' runat="server" />
                <asp:HiddenField ID="h_service_id" Value='<%# Bind("service_id")%>' runat="server" />
            </ItemTemplate>
            </asp:TemplateField>
         
    
              
           </Columns>
   </asp:GridView>
<div  style="padding-left:80px;">
   <asp:Button ID="btn_add_role2" CssClass="btn btn-mini btn-primary" Text="<%$Resources:Resource,app_save %>" 
                runat="server" onclick="btn_add_role_Click" />
     
</div>
</asp:Panel>
<br /><br />

<asp:Label ID="lbl_success" Text="<%$Resources:role,role_saved %>" Font-Bold="true" ForeColor="Blue" runat="server" ></asp:Label><br />
<asp:Label ID="lbl_failure" Text="<%$Resources:role,role_already_exist %>" Font-Bold="true" ForeColor="Red" runat="server" ></asp:Label>
<asp:Label ID="Label1" runat="server" ></asp:Label>