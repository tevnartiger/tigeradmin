﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_role_update.ascx.cs" Inherits="c_uc_uc_role_update" %>

 <style type="text/css">
     .style1
     {
         width: 40%;
     }
     .style2
     {
         width: 40%;
     }
 </style>
 <p class="lead"><%= Resources.role.role_edit_role %></p>
 <table class="style2">
    <tr>
        <td>
            <%= Resources.role.role_name %></td>
        <td>
            <asp:TextBox ID="tbx_role_name" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td valign="top">
           <%= Resources.role.role_desc %></td>
        <td>
         <asp:TextBox ID="tbx_role_desc" TextMode="MultiLine" Width="250px" Height="100px" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td valign="top">
            &nbsp;</td>
        <td>
            <asp:Button ID="btn_update" Visible="false" CssClass="btn btn-primary" onclick="btn_update2_Click"  runat="server" Text="<%$Resources:Resource,app_save %>"  />
        </td>
    </tr>
</table>


<div style="padding-left:80px;">
 <asp:GridView ID="gv_services" runat="server" AllowPaging="True" 
           AllowSorting="True"  GridLines="None"  CellSpacing="5" 
           AutoGenerateColumns="false"  OnSorting="gv_services_Sorting"
           EmptyDataText="" PageSize="100" >    
           
           <Columns>
           
            <asp:BoundField DataField="service_title_fr"     HeaderText="fr" ReadOnly="True"  />
            <asp:BoundField DataField="service_title_en"     HeaderText="en" ReadOnly="True"  />
            <asp:BoundField DataField="sc_name_en"   HeaderText="Category" ReadOnly="True"  />
           <asp:BoundField DataField="sc_name_fr"   HeaderText="Category" ReadOnly="True"  />
            <asp:TemplateField>
            <ItemTemplate>
                <asp:CheckBox ID="chk_service"  Checked='<%#RoleHasService(Convert.ToInt32(Eval("hasservice")))%>' runat="server" />
                <asp:HiddenField ID="h_service_id" Value='<%# Bind("service_id")%>' runat="server" />
            </ItemTemplate>
            </asp:TemplateField>
         
    
              
           </Columns>
   </asp:GridView>
 </div>
<div  style="padding-left:80px;">
            <asp:Button ID="btn_update2" CssClass="btn btn-primary" Text="<%$Resources:Resource,app_save %>" 
                runat="server" onclick="btn_update2_Click" />
</div>

<br /><br />

<asp:Label ID="Label1" runat="server"></asp:Label>

<br /><br />