using System;
using System.Web.UI;


public partial class create_account : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            
         string temp_name = RegEx.name;
         string temp_pwd = RegEx.pwd;
         string temp_user = RegEx.alpha_numeric;
         string temp_email = RegEx.email;

         reg_name_fname.ValidationExpression = temp_name;
         reg_name_lname.ValidationExpression = temp_name;
         reg_name_email.ValidationExpression = temp_email; 
         reg_login_user.ValidationExpression = temp_name;
         reg_login_pwd.ValidationExpression = temp_pwd;
         reg_retype_login_pwd.ValidationExpression = temp_pwd;
        }
  }

    protected void bn_create_Click(object sender, EventArgs e)
    {

        string temp_login_user ="";
        string temp_login_pwd = "";
          Page.Validate();
          if (!Page.IsValid)
          {
            //input validation server side
            string str_val = "";
            bool bool_val = false;
           System.Web.HttpBrowserCapabilities browser = Request.Browser;
            sinfoca.tiger.security.Security sa = new sinfoca.tiger.security.Security(Request);
           
            sa.sendUserInfo(str_val, Request.Browser, Request);
            txt.Text = "Error occured <a href='http://sinfocatiger.com/login.aspx'>Back to main page</a>";
            bn_create.Enabled = true;
            }
            else
            {

                temp_login_user = login_user.Text;
                temp_login_pwd = login_pwd.Text;
            
                //insert home
                string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

                //validation code
                string schema_activation_code = tiger.RandomPassword.Generate(15, 20);

                /*****************************************************************/
                //check to see if dirty account exist 
                //SP will return dirty account schema_id 
                tiger.registration.Account a = new tiger.registration.Account(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                /*  int schema_id =  a.getDirtyAccount();

                  if (schema_id == -1)
                  {
                      txt.Text = "runtime error, cannot continue --RecycleReturn";
                  }
                  else
                  {
                                  if (schema_id > 0)
                      {

                          int replace_account_result = a.replaceAccount(name_lname.Text, name_fname.Text, name_email.Text,temp_login_user, temp_login_pwd, 1, Convert.ToInt32(schema_id), schema_activation_code);

                          if (replace_account_result == -1)
                          {
                              txt.Text = "runtime error, cannot continue --Replace_account_result";
                          }
                          else
                          {

                              switch (replace_account_result)
                              {
                                  case -1:
                                      txt.Text = "Runtime error occurred";
                                      break;
                                  case 0:
                                      //send email to user

                                      string str_to1 = name_email.Text;
                                      string str_from1 = "info@tiger.com";
                                      string str_subject1 = "tiger real estate project validation";
                                      string str_body1 = "Thank you for choosing tiger real estate<br/><br/>Validation code: " + schema_activation_code + "<br/><br/> click <a href='http://sinfocatiger.com/validate_account.aspx?email=" + name_email.Text + "'>here</a> to validate account";
                                      tiger.Email.sendMail(str_from1, str_to1, str_subject1, str_body1);
                                      txt.Text = "Your account has been created. An email has been sent to validate your account";
                                      break;
                                  case 1:
                                      txt.Text = "Stored procedure transaction failed";
                                      break;
                                  case 2:
                                      txt.Text = "Email already exist and is in Pending for validation";
                                      break;
                                  case 3:
                                      txt.Text = "<span class='letter_red'>Account is waiting for validation. To send validation key click  <a href='validate_account.aspx?email=" + name_email.Text + "' class='letter_link'>here</a></span>";
                                      break;
                                  case 4:
                                      txt.Text = "<span class='letter_red'>Account already exist. To re-activate your account click  <a href='activate_account.aspx?email=" + name_email.Text + "' class='letter_link'>here</a></span>";
                                      break;
                              }
                          }
                          txt.Text += "<br/><br/> schema_id:" + schema_id+"<br /><br /> replace account result:" + replace_account_result + " <br/><br/> ";

                      }
                      else
                      {
                       */
                //no dirty account found
                /**************************************************************************************/
                int create_account_result = a.createAccount(name_lname.Text, name_fname.Text, name_email.Text, temp_login_user, temp_login_pwd, 1, schema_activation_code);


                if (create_account_result == -1)
                {
                    txt.Text = "***runtime error, cannot continue --create_account_result";
                }
                else
                {
                    switch (create_account_result)
                    {
                        case -1:
                            txt.Text = "Runtime error occurred";
                            break;
                        case 0:
                            //get activation code
                            string temp_schema_activation_code = a.getSchemaActivationCode(temp_login_user);
                            string str_to2 = name_email.Text;
                            string str_from2 = "register@sinfoca.info";
                            string str_subject2 = "tiger real estate project validation";
                            string str_body2 = "Thank you for choosing tiger real estate.<br/> Please click <a href='https://sinfoca.info/account_validate.aspx?usr=" + temp_login_user + "&id=" + temp_schema_activation_code + "'>here</a> to validate account";
                            tiger.Email.sendMailRegistrationHtml(str_from2, str_to2, str_subject2, str_body2);
                                                     
                            txt.Text = "Your account has been created. An email has been sent to validate your account";
                            break;
                        case 1:
                            txt.Text = "Stored procedure transaction failed";
                            break;
                        case 2:
                            txt.Text = "<span class='letter_red'>User is already being used, click <a href='forget_pwd.aspx?email=" + name_email.Text + "' class='letter_link'>here</a>if you have forgotten your email.activation code</span>";
                            break;
                        case 3:
                            txt.Text = "<span class='letter_red'>Account already exist. To send validation key click  <a href='send_validation.aspx?email=" + name_email.Text + "' class='letter_link'>here</a></span>";
                            break;
                    }
                }


            }
        }
        }
    
 