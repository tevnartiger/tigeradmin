﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _default : BasePageLite
{
    private void Page_PreInit(object sender, EventArgs e)
    {
        if (Session["mobile"] != null)
            this.MasterPageFile = "~/mp_mobile_app.master";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Control FeaturedProductUserControl;
        switch (Session["role_name_abbr"].ToString())
        {
            case "EXDW":
            case "EX":
                FeaturedProductUserControl = LoadControl("uc/dash/uc_dash_exec.ascx");
                break;
            case "CS":
                FeaturedProductUserControl = LoadControl("uc/dash/uc_dash_cs.ascx");
                break;
            case "ACT":
                FeaturedProductUserControl = LoadControl("uc/dash/uc_dash_activation.ascx");
                break;
            case "BK":

                FeaturedProductUserControl = LoadControl("uc/dash/uc_dash_booking.ascx");
                break;
            case "ACC":
                FeaturedProductUserControl = LoadControl("uc/dash/uc_dash_accounting.ascx");
                break;


            case "SE":
            case "DW":
                FeaturedProductUserControl = LoadControl("uc/dash/uc_dash_dw.ascx");
                break;

            case "WPM":
                if (Session["role_name_abbr"].ToString() == "WPM")
                {
                    panel_event.Visible = true;
                }
                FeaturedProductUserControl = LoadControl("uc/dash/uc_dash_wpm.ascx");
                break;
            case "AO":
                FeaturedProductUserControl = LoadControl("~/c/orders_wpm/uc_order_list.ascx"); ;
                break;
            case "PM":
                FeaturedProductUserControl = LoadControl("uc/dash/uc_dash_pm.ascx");
                break;

            case "RO":
                FeaturedProductUserControl = LoadControl("uc/dash/uc_dash_ro.ascx");
                break;

            default:
                FeaturedProductUserControl = LoadControl("uc/dash/uc_dash_default.ascx");
                break;
        }
        panel_event.Controls.Add(FeaturedProductUserControl);
    }

    protected string LocalStorage()
    {
        string response = "";
        if (Session["mobile"] != null && Session["mobiletoken"] != null && Session["person_email"] != null)
        {
            response = "<script> $(function() { localStorage.usrname = '" + Session["person_email"].ToString() + "'; localStorage.token = '" + Session["mobiletoken"].ToString() + "'; localStorage.device = 'mobile';localStorage.useragent = '" + Request.UserAgent + Request.Browser.Win16.ToString() + Request.Browser.Win32.ToString() + Request.AcceptTypes.ToString() + Request.IsSecureConnection.ToString() + Request.Browser.Browser.ToString() + Request.Browser.Version + Request.UserLanguages + "';}); </script>";
        }

        if (Session["mobile"] == null)
        {
            response = "<script> $(function() { localStorage.device = 'web' ;}); </script>";
        }

        return response;
    }
}