﻿<%@ Page Language="C#" MasterPageFile="~/tenant/tenant.master" AutoEventWireup="true" CodeFile="tenant_main.aspx.cs" Inherits="tenant_tenant_tenant_main" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <b>  <asp:Label ID="lbl_hello" runat="server" Text="<%$ Resources:Resource, lbl_hello %>" ></asp:Label>
    &nbsp;&nbsp;
    <asp:Label ID="lbl_name" runat="server" ></asp:Label>&nbsp;! </b>
    <br /><br />
   
   
       <table width="100%">
           <tr>
               <td bgcolor="aliceblue">
                   <b>CURRENT LEASE</b></td>
           </tr>
       </table>
       &nbsp;<br />
       <asp:GridView ID="gv_lease_list" runat="server" AllowPaging="True" 
           AllowSorting="True" AlternatingRowStyle-BackColor="Beige" 
           AutoGenerateColumns="False" BorderColor="White" BorderWidth="3px" 
           DataKeyNames="tu_id" DataSourceID="SqlDataSource1" 
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" 
           HeaderStyle-BackColor="AliceBlue" PageSize="10" Width="90%">
           <Columns>
              
               <asp:BoundField DataField="home_name" 
                   HeaderText="<%$ Resources:Resource, lbl_property %>" 
                     />
               <asp:BoundField DataField="unit_door_no" 
                   HeaderText="<%$ Resources:Resource, lbl_door_no %>" 
                     />
               <asp:BoundField DataField="tu_date_begin" DataFormatString="{0:M-dd-yyyy}" 
                   HeaderText="<%$ Resources:Resource, lbl_lease_date_begin %>" HtmlEncode="false" 
                    />
               <asp:BoundField DataField="tu_date_end" DataFormatString="{0:M-dd-yyyy}" 
                   HeaderText="<%$ Resources:Resource, lbl_lease_date_end %>" HtmlEncode="false" 
                    />
            
               <asp:HyperLinkField DataNavigateUrlFields="tu_id,tenant_id,name_id" 
                   DataNavigateUrlFormatString="~/tenant/tenant/tenant_view.aspx?tu_id={0}&t_id={1}&n_id={2}" 
                   HeaderText="FILE" 
                   Text="FILE" />
           </Columns>
       </asp:GridView>
       
       <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
           ConnectionString="<%$ ConnectionStrings:sinfoca_tigerConnectionString2 %>" 
           SelectCommand="prTenantCurrentLeaseList" SelectCommandType="StoredProcedure">
           <SelectParameters>
               <asp:SessionParameter Name="schema_id" SessionField="schema_id" Type="Int32" />
               <asp:SessionParameter Name="name_id" SessionField="name_id"  Type="Int32" />
           </SelectParameters>
       </asp:SqlDataSource>
   
   




       <br />


   
   
       <table width="100%">
           <tr>
               <td bgcolor="aliceblue">
                   <b>MESSAGES FROM MANAGEMENT</b></td>
           </tr>
       </table>
       <br /> 
         <table id="tb_rent_delequency" runat="server" width="100%">
            <tr>
                <td>
                    <b><asp:Label ForeColor="Red" ID="Label3" runat="server" 
                        Text="<%$ Resources:Resource, lbl_u_delequency%>"/> </b>
                </td>
            </tr>
        </table>
    
     <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="100%" BorderColor="White" BorderWidth="3" ID="gv_rent_name_delequency" 
            runat="server" AutoGenerateColumns="false"
      AllowPaging="true" AllowSorting="true"  AutoGenerateSelectButton="false" 
        EmptyDataText="<%$ Resources:Resource, lbl_none%>" GridLines="Both"  
        AlternatingRowStyle-BackColor="Beige">
    <Columns>
     <asp:BoundField DataField="home_name"  HeaderText="<%$ Resources:Resource, lbl_property%>"  />
     
      <asp:BoundField DataField="unit_door_no"  HeaderText="<%$ Resources:Resource, gv_unit%>"  />
      
      
      <asp:BoundField DataField="amount_owed"  HeaderText="<%$ Resources:Resource, gv_amount_owed%>"
             DataFormatString="{0:0.00}"   HtmlEncode="false"  />
    
     
      
      
      <asp:BoundField   DataField="rp_due_date" DataFormatString="{0:M-dd-yyyy}"  
       HtmlEncode="false" HeaderText="<%$ Resources:Resource, gv_due_date%>"  />
      <asp:BoundField  DataField="days" HeaderText="<%$ Resources:Resource, lbl_days %>"/> 
     
      <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_notice_sent%>"   >
      <ItemTemplate>
      <asp:Label  runat="server"  ID="notice_sent"    Text='<%#Get_AmountOfWarningSent(Convert.ToInt32(Eval("amount_of_warning_sent")))%>'    /> 
      </ItemTemplate>
      </asp:TemplateField>
     
      
      <asp:HyperLinkField  Text="<%$ Resources:Resource, lbl_view_notice %>" 
        DataNavigateUrlFields="rp_id,tenant_id,tu_id" 
        DataNavigateUrlFormatString="~/owner/notice/notice_delequency_view.aspx?rp_id={0}&tu_id={2}" 
         HeaderText="<%$ Resources:Resource, lbl_view_notice %>"  />
    
   </Columns>
   </asp:GridView>

       <br />
       


   
   
       <table width="100%">
           <tr>
               <td bgcolor="aliceblue">
                   <b>AVAILABLE UNIT &amp; SOON TO BE AVAILABLE</b></td>
           </tr>
       </table>
          
   




                      <br />
                      <br />
                      <br />
                      <br />
                      <br />


   
   
       <table width="100%">
           <tr>
               <td bgcolor="aliceblue">
                   <b>REQUEST FOR ANOTHER UNIT</b></td>
           </tr>
       </table>
                 
   




</asp:Content>

