﻿<%@ Page Language="C#" MasterPageFile="~/owner/owner_mp.master" AutoEventWireup="true" CodeFile="owner_main.aspx.cs" Inherits="owner_owner_main" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    
       <h3>OWNER MODULE</h3> 
        <b>  <asp:Label ID="lbl_hello" runat="server" Text="<%$ Resources:Resource, lbl_hello %>" ></asp:Label>
    &nbsp;&nbsp;
    <asp:Label ID="lbl_name" runat="server" ></asp:Label>&nbsp;! </b>
    <br /><asp:HyperLink ID="lbl_files" Text="<%$ Resources:Resource, lbl_gl_files %>" runat="server" NavigateUrl="~/owner/files/owner_files.aspx"></asp:HyperLink>
   &nbsp; &nbsp; &nbsp;<asp:LinkButton ID="link_logout" OnClick="link_logout_Click"  runat="server">LogOut</asp:LinkButton>
   <br />
                      <div>
                          <span lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">
                          <br />
                          group :
                          <asp:Label ID="Label4" runat="server" Text=""></asp:Label>
                          <br />
                          URL :
                          <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                          <br />
                          <asp:HyperLink ID="HyperLink40" runat="server" 
                              NavigateUrl="~/owner/property/property_available_units.aspx"><span 
                              lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">AVAILABLE UNITS</span></asp:HyperLink>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
                          <asp:HyperLink ID="HyperLink16" runat="server" 
                              NavigateUrl="~/owner/mortgage/mortgage_amortization.aspx">Amortization</asp:HyperLink>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                          <br />
                          <asp:HyperLink ID="HyperLink24" runat="server" 
                              NavigateUrl="~/owner/Financial/financial_analysis.aspx">Financial analysis</asp:HyperLink>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <asp:HyperLink ID="HyperLink25" runat="server" 
                              NavigateUrl="~/owner/Financial/financial_analysis_period.aspx">Financial 
                          analysis for a period</asp:HyperLink>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                          <br />
                          <asp:HyperLink ID="HyperLink27" runat="server" 
                              NavigateUrl="~/owner/Financial/financial_income_graph.aspx">Income Graph</asp:HyperLink>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <asp:HyperLink ID="HyperLink28" runat="server" 
                              NavigateUrl="~/owner/Financial/financial_expenses_graph.aspx">Expenses 
                          Graph</asp:HyperLink>
                          <br />
                          <br />
                          <asp:HyperLink ID="HyperLink29" runat="server" 
                              NavigateUrl="~/owner/Financial/financial_analysis_period_expense_graph.aspx">Financial 
                          Analysis Expense Graph for a period</asp:HyperLink>
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          <asp:HyperLink ID="HyperLink30" runat="server" 
                              NavigateUrl="~/owner/Financial/financial_analysis_period_income_graph.aspx">Financial 
                          Analysis Income Graph for a period</asp:HyperLink>
                          <br />
                          <br />
                          <br />
                          <br />
                          * page temporaire</div>
    
        
        
                  <br />
                  <hr   id="hr_lease_up" runat="server" />
    
        
        
        <table id="tb_current_lease" runat="server" width="100%">
           <tr>
               <td bgcolor="aliceblue">
                  <h3>CURRENT LEASE</h3></td>
           </tr>
       </table>
       &nbsp;<br />
       <asp:GridView ID="gv_lease_list" runat="server" AllowPaging="True" 
           AllowSorting="True" AlternatingRowStyle-BackColor="Beige" 
           AutoGenerateColumns="False" BorderColor="White" BorderWidth="3px" 
           DataKeyNames="tu_id" DataSourceID="SqlDataSource1" 
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" 
           HeaderStyle-BackColor="AliceBlue" PageSize="10" Width="90%">
           <Columns>
              
               <asp:BoundField DataField="home_name" 
                   HeaderText="<%$ Resources:Resource, lbl_property %>" 
                     />
               <asp:BoundField DataField="unit_door_no" 
                   HeaderText="<%$ Resources:Resource, lbl_door_no %>" 
                     />
               <asp:BoundField DataField="tu_date_begin" DataFormatString="{0:M-dd-yyyy}" 
                   HeaderText="<%$ Resources:Resource, lbl_lease_date_begin %>" HtmlEncode="false" 
                    />
               <asp:BoundField DataField="tu_date_end" DataFormatString="{0:M-dd-yyyy}" 
                   HeaderText="<%$ Resources:Resource, lbl_lease_date_end %>" HtmlEncode="false" 
                    />
            
               <asp:HyperLinkField DataNavigateUrlFields="tu_id,tenant_id,name_id" 
                   DataNavigateUrlFormatString="~/owner/tenant/tenant_view.aspx?tu_id={0}&t_id={1}&n_id={2}" 
                   HeaderText="FILE" 
                   Text="FILE" />
           </Columns>
       </asp:GridView>
       
       <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
           ConnectionString="<%$ ConnectionStrings:sinfoca_tigerConnectionString2 %>" 
           SelectCommand="prTenantCurrentLeaseList" SelectCommandType="StoredProcedure">
           <SelectParameters>
               <asp:SessionParameter Name="schema_id" SessionField="schema_id" Type="Int32" />
               <asp:SessionParameter Name="name_id" SessionField="name_id"  Type="Int32" />
           </SelectParameters>
       </asp:SqlDataSource>
   
   




         <table id="tb_rent_delequency" runat="server" width="100%">
            <tr>
                <td>
                    <b><asp:Label ForeColor="Red" ID="Label3" runat="server" 
                        Text="<%$ Resources:Resource, lbl_u_delequency%>"/> </b>
                </td>
            </tr>
        </table>
    
     <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="100%" BorderColor="White" BorderWidth="3" ID="gv_rent_name_delequency" 
            runat="server" AutoGenerateColumns="false"
      AllowPaging="true" AllowSorting="true"  AutoGenerateSelectButton="false" 
        EmptyDataText="<%$ Resources:Resource, lbl_none%>" GridLines="Both"  
        AlternatingRowStyle-BackColor="Beige">
    <Columns>
     <asp:BoundField DataField="home_name"  HeaderText="<%$ Resources:Resource, lbl_property%>"  />
     
      <asp:BoundField DataField="unit_door_no"  HeaderText="<%$ Resources:Resource, gv_unit%>"  />
      
      
      <asp:BoundField DataField="amount_owed"  HeaderText="<%$ Resources:Resource, gv_amount_owed%>"
             DataFormatString="{0:0.00}"   HtmlEncode="false"  />
    
     
      
      
      <asp:BoundField   DataField="rp_due_date" DataFormatString="{0:M-dd-yyyy}"  
       HtmlEncode="false" HeaderText="<%$ Resources:Resource, gv_due_date%>"  />
      <asp:BoundField  DataField="days" HeaderText="<%$ Resources:Resource, lbl_days %>"/> 
     
      <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_notice_sent%>"   >
      <ItemTemplate>
      <asp:Label  runat="server"  ID="notice_sent"    Text='<%#Get_AmountOfWarningSent(Convert.ToInt32(Eval("amount_of_warning_sent")))%>'    /> 
      </ItemTemplate>
      </asp:TemplateField>
     
      
      <asp:HyperLinkField  Text="<%$ Resources:Resource, lbl_view_notice %>" 
        DataNavigateUrlFields="rp_id,tenant_id,tu_id" 
        DataNavigateUrlFormatString="~/tenant/notice/notice_delequency_view.aspx?rp_id={0}&tenant_id={1}&tu_id={2}" 
         HeaderText="<%$ Resources:Resource, lbl_view_notice %>"  />
    
   </Columns>
   </asp:GridView>

   




       <hr id="hr_lease_down" runat="server" />

   




       <br />


   
   
       <table width="100%">
           <tr>
               <td >
                 <h3>MESSAGES FROM MANAGEMENT</h3></td>
           </tr>
       </table>
       <br /> 
           <br /> 

       <br />
      

   
   
       <table width="100%">
           <tr>
               <td >
                   <h3>REQUEST FOR ANOTHER UNIT</h3></td>
           </tr>
       </table>
                 
   

</asp:Content>

