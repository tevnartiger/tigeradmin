﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.OwnerObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : may 20 , 2008
/// </summary>
public partial class manager_lease_lease_accommodation_archive_view : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        OwnerObjectAuthorization accArchiveAuthorization = new OwnerObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


            ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!accArchiveAuthorization.AccomodationArchive(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), Convert.ToInt32(Request.QueryString["tu_id"]), Convert.ToInt32(Request.QueryString["ta_id"])))
            {
                Session.Abandon();
                Response.Redirect("http://www.sinfocatiger.com/login.aspx");
            }
            ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
             


            if (!Page.IsPostBack)
            {

                SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                SqlCommand cmd = new SqlCommand("prAccommodationPreviousView", conn);
                SqlCommand cmd2 = new SqlCommand("prHomeUnitView", conn);


                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("ta_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["ta_id"]);
                


                try
                {
                     conn.Open();

                    SqlDataReader dr = null;
                    dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                    while (dr.Read() == true)
                    {

                        // lbl_current_ta_date_begin.Text = String.Format("{0:d}", dr["ta_date_begin"]);




                        DateTime date_begin = new DateTime();
                        DateTime date_end = new DateTime();

                        date_begin = Convert.ToDateTime(dr["ta_date_begin"]);
                        date_end = Convert.ToDateTime(dr["ta_date_end"]);

                        lbl_ta_date_begin.Text = date_begin.Month.ToString() + "-" + date_begin.Day.ToString() + "-" + date_begin.Year.ToString();
                        lbl_ta_date_end.Text = date_end.Month.ToString() + "-" + date_end.Day.ToString() + "-" + date_end.Year.ToString();



                        if (Convert.ToInt32(dr["ta_electricity_inc"]) == 1)
                            ta_electricity_inc.Checked = true;
                        else
                            ta_electricity_inc.Checked = false;

                        if (Convert.ToInt32(dr["ta_heat_inc"]) == 1)
                            ta_heat_inc.Checked = true;
                        else
                            ta_heat_inc.Checked = false;

                        if (Convert.ToInt32(dr["ta_water_inc"]) == 1)
                            ta_water_inc.Checked = true;
                        else
                            ta_water_inc.Checked = false;

                        if (Convert.ToInt32(dr["ta_water_tax_inc"]) == 1)
                            ta_water_tax_inc.Checked = true;
                        else
                            ta_water_tax_inc.Checked = false;

                        if (Convert.ToInt32(dr["ta_garage_inc"]) == 1)
                            ta_garage_inc.Checked = true;
                        else
                            ta_garage_inc.Checked = false;

                        if (Convert.ToInt32(dr["ta_parking_inc"]) == 1)
                            ta_parking_inc.Checked = true;
                        else
                            ta_parking_inc.Checked = false;

                        if (Convert.ToInt32(dr["ta_semi_furnished_inc"]) == 1)
                            ta_semi_furnished_inc.Checked = true;
                        else
                            ta_semi_furnished_inc.Checked = false;

                        if (Convert.ToInt32(dr["ta_furnished_inc"]) == 1)
                            ta_furnished_inc.Checked = true;
                        else
                            ta_furnished_inc.Checked = false;


                        ta_com.Text = Convert.ToString(dr["ta_com"]);
                    }



                }

                finally
                {

                  //  conn.Close();
                }




                cmd2.CommandType = CommandType.StoredProcedure;

                try
                {

                    cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                    cmd2.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);


                    SqlDataAdapter da = new SqlDataAdapter(cmd2);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    r_HomeUnitView.DataSource = ds;
                    r_HomeUnitView.DataBind();
                }
                finally
                {
                    conn.Close();
                }


               
            
            }

               
            else
              {
                Response.Redirect(tiger.security.Access.toLoginPage());
              }


    }
        
}
