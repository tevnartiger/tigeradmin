﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="lease_termsandconditions_archive_view.aspx.cs" Inherits="manager_lease_lease_termsandconditions_archive_view" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
    <span style="font-size: small"><b>TERMS AND CONDITIONS ARCHIVE </b></span>
    <br />
    <br />
<asp:Repeater runat="server" ID="r_HomeUnitView">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
        
        <tr>
          <td>
             <b>District&nbsp;:&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_district")%> 
                  </b> </td>
            </tr>
           <tr>
              <td valign="top" >
              <b><%#DataBinder.Eval(Container.DataItem, "home_addr_no")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_street")%></b></td>
            </tr>
            <tr>
                 <td valign="top" >
             <b>   <%#DataBinder.Eval(Container.DataItem, "home_city")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_prov")%>
                   &nbsp;,&nbsp;&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_pc")%> </b>
                   </td>
               
            </tr>
            <tr>
                 <td valign="top" >
                 <b>Unit</b>  &nbsp;:&nbsp;<%#DataBinder.Eval(Container.DataItem, "unit_door_no")%> 
                
                   </td>
               
            </tr>   
        </table>
        
        </ItemTemplate>
        </asp:Repeater>
  



  
  <br />
  
  <hr />
  
  
  
  
  <table>
  <tr>
  <td>
  Terms and conditions&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From&nbsp;&nbsp; :
  <asp:Label ID="lbl_tt_date_begin" runat=server></asp:Label>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; To :<asp:Label ID="lbl_tt_date_end" 
          runat=server></asp:Label>
  </td>
  </tr>
  </table>
 
 <hr />
     
  
          
          
          
          
          <table>
          
          
          <tr>
<td valign="top">
 Form of payment </td>
<td>
    <asp:Label ID="tt_form_of_payment" runat="server"  ></asp:Label>
                              </td>
</tr>
<tr>
<td valign="top">
Non-sufficient fund fee  &nbsp; </td>
<td>
<asp:Label ID="tt_nsf" runat="server" Width="128px"></asp:Label></td>
</tr>
        <tr>
        <td  valign=top>late fee</td>
        <td>
            <asp:Label ID="tt_late_fee" runat="server" Width="128px"></asp:Label>
        </td>
        
        
        </tr>
        <tr><td valign=top>Security deposit required</td><td>
            <asp:RadioButtonList CssClass="letter" ID="tt_security_deposit" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
                   <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
            
            <asp:Panel ID="Panel_security_deposit" Visible=true runat="server" >
            
            
                &nbsp;&nbsp;&nbsp;amount&nbsp;<asp:Label ID="tt_security_deposit_amount" runat="server"></asp:Label>
     </asp:Panel>       
            
            
            
            </td>
       </tr>
            
            
            
            
            
            
            
 
            
            
            
            
            
            
            
            
            
            
        <tr><td valign=top>Guarantor required </td><td>
            <asp:RadioButtonList CssClass="letter" ID="tt_guarantor" runat="server" 
                AutoPostBack="true" 
                
                RepeatDirection="Horizontal">
            <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
              <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
                              <br />
            <asp:Panel ID="panel_guarantor" runat="server" Visible="false">
                name :
                <asp:Label ID="guarantor_name" runat="server"></asp:Label>
            </asp:Panel>
                              <br />
            </td></tr>
        <tr>
        <td valign=top>Pets allowed</td>
        <td>
            <asp:RadioButtonList   CssClass="letter" ID="tt_pets" runat="server" 
                RepeatDirection="Horizontal" >
                <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
                <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
         </td>
        </tr>
        <tr valign="top">
            <td valign=top>Tenant is responsible for maintenance</td>
         <td>
          <asp:RadioButtonList   CssClass="letter" ID="tt_maintenance" runat="server" 
                   
                 RepeatDirection="Horizontal" >
            <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
            <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
            <asp:Panel Visible=true ID="panel_specify_maintenance" runat="server">
                &nbsp;&nbsp;&nbsp;specify maintenance allowed<br />&nbsp;<asp:Label runat=server 
                    ID="tt_specify_maintenance"  Width="300px" TextMode="MultiLine" ></asp:Label>
            </asp:Panel>
         
         </td> 
            
        </tr>
        <tr>
        <td valign=top>Can tenant make improvement</td>
        <td>
            <asp:RadioButtonList   CssClass="letter" ID="tt_improvement" runat="server" 
                
                RepeatDirection="Horizontal" >
                <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
                <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
             <asp:Panel Visible=true ID="panel_specify_improvement" runat="server">
                 &nbsp;&nbsp;&nbsp;specify improvement allowed<br />
                 &nbsp;<asp:Label runat=server Width="300px" ID="tt_specify_improvement" 
                     TextMode="MultiLine"  ></asp:Label>
            </asp:Panel>
         
        </td>
        </tr>
        <tr>
        <td valign=top>Notice to enter </td>
        <td>
            <asp:RadioButtonList ID="tt_notice_to_enter" runat="server" CssClass="letter" 
                 >
                <asp:ListItem Selected="True" Value="0">legal minimum</asp:ListItem>
                <asp:ListItem Value="1">number of hours</asp:ListItem>
            </asp:RadioButtonList>
            <asp:Panel ID="panel_specify_notice_to_enter" runat="server" Visible="true">
                &nbsp;specify number of hours notice&nbsp;<asp:Label ID="tt_specify_number_of_hours" 
                    runat="server" Width="100px"></asp:Label>
            </asp:Panel>
        </td>
        </tr>
        <tr><td valign=top>Who pay these insurances</td>
        
        <td>
            <table>
                <tr>
                    <td>
                        tenant content</td>
                    <td>
                        <asp:Label ID="tt_tenant_content_ins" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        landlord content</td>
                    <td>
                         <asp:Label ID="tt_landlord_content_ins" runat="server" ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        personal injury on property</td>
                    <td>
                        
                        
                        <asp:Label ID="tt_injury_ins" runat="server" ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        lease premises</td>
                    <td>
                         <asp:Label ID="tt_premises_ins" runat="server" ></asp:Label>
                    </td>
                </tr>
            </table>
            </td>
        </tr>
        
        
        
        <tr><td valign="top">
            Additional terms &amp; conditions</td>
            <td>
                <asp:Label ID="tt_additional_terms" runat="server" Height="200px" 
                    TextMode="MultiLine" Width="400px"></asp:Label>
            </td>
              </tr>
          
              <tr>
                  <td>
                      <div ID="txt_link" runat="server" />
                      </td>
                  </tr>
        </table>
          
 
  
  <div id="txt_message" runat="server" />  
       
   <asp:HiddenField ID="hd_min_begin_date_m" runat="server" />
             <asp:HiddenField ID="hd_min_begin_date_d" runat="server" />
             <asp:HiddenField ID="hd_min_begin_date_y" runat="server" />
             
    <!--Hidden fields BEGIN SECTION 1-->
         <input type="hidden" id="hd_home_id" runat="server" />
         <input type="hidden" id="hd_unit_id" runat="server" />
         <input type="hidden" id="hd_current_tu_id" runat="server" />
         <!--Hidden fields END SECTION 1-->
           <!--Hidden fields temporaire-->
          <input type="hidden" id="didden" runat="server" />
           

</asp:Content>

