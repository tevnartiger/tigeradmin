﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="lease_accommodation_archive_view.aspx.cs" Inherits="manager_lease_lease_accommodation_archive_view" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<span style="font-size: small"><b>ACCOMMODATION ARCHIVE
    </b></span><br /><br />
<asp:Repeater runat="server" ID="r_HomeUnitView">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
        
        <tr>
          <td>
             <b>District&nbsp;:&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_district")%> 
                  </b> </td>
            </tr>
           <tr>
              <td valign="top" >
              <b><%#DataBinder.Eval(Container.DataItem, "home_addr_no")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_street")%></b></td>
            </tr>
            <tr>
                 <td valign="top" >
             <b>   <%#DataBinder.Eval(Container.DataItem, "home_city")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_prov")%>
                   &nbsp;,&nbsp;&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_pc")%> </b>
                   </td>
               
            </tr>
            <tr>
                 <td valign="top" >
                 <b>Unit</b>  &nbsp;:&nbsp;<%#DataBinder.Eval(Container.DataItem, "unit_door_no")%> 
                
                   </td>
               
            </tr>   
        </table>
        
        </ItemTemplate>
        </asp:Repeater>
  



<table>
<tr>
<td><div id="txt_tenant_name" runat="server" />
</td>
</tr>

</table>

<hr />









<table>

 <tr>
 <td>
     Accommodation&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From&nbsp; :
 <asp:Label ID="lbl_ta_date_begin" runat="server"></asp:Label>
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; To&nbsp; :
     <asp:Label ID="lbl_ta_date_end" runat="server"></asp:Label>
</td>
 </tr>
</table>
   
  <hr />
     
           <table>
         
           <tr>
 <td valign="top">
     &nbsp;</td>
 </tr> 
           
       
          </table>
          <table>
     
     
     <tr>
     <td colspan="2">
         &nbsp;</td></tr>
     
         
              <tr>
                  <td colspan="2">
                      <strong><span style="font-size: 10pt">Utilities includes</span></strong><br />
                      <asp:CheckBox ID="ta_electricity_inc" runat="server" AutoPostBack="true" 
                          CssClass="letter"  
                          text="Electricity" />
                      <br />
                      <asp:CheckBox ID="ta_heat_inc" runat="server" AutoPostBack="true" 
                          CssClass="letter"   text="Heat" />
                      <br />
                      <asp:CheckBox ID="ta_water_inc" runat="server" AutoPostBack="true" 
                          CssClass="letter"  text="Water" />
                      <br />
                      <asp:CheckBox ID="ta_water_tax_inc" runat="server" AutoPostBack="true" 
                          CssClass="letter"   
                          text="Water tax" />
                      <br />
                      <asp:CheckBox ID="ta_none" runat="server" AutoPostBack="true" CssClass="letter" 
                           
                          text="None ( utilities are not included in the lease )" />
                      <br />
                      <br />
                      <span style="font-size: 10pt"><strong>Accomodations included</strong></span><br />
                      <asp:CheckBox ID="ta_parking_inc" runat="server" CssClass="letter" 
                          text="Parking" />
                      <br />
                      <asp:CheckBox ID="ta_garage_inc" runat="server" CssClass="letter" 
                          text="Garage" />
                      <br />
                      <asp:CheckBox ID="ta_furnished_inc" runat="server" CssClass="letter" 
                          text="Furnished" />
                      <br />
                      <asp:CheckBox ID="ta_semi_furnished_inc" runat="server" CssClass="letter" 
                          text="Semi-furnished" />
                      <br />
                  </td>
              </tr>
     
         
           <tr><td class="letter_bold" valign=top>Comments & extras</td>
                <td><asp:Label ID="ta_com" CssClass="letter" runat="server" TextMode="MultiLine" Width="389px"/></td>
            </tr>
       
      <tr><td colspan="2">&nbsp;</td></tr>
    <tr><td colspan="2">
     
      
     </td></tr>
          <tr><td><div ID="txt_link" runat="server" /></td></tr>
         <tr><td>
         </td></tr>
         
     </table>
    
    
    <div id="txt_message" runat="server" />
    
    <asp:HiddenField ID="hd_min_begin_date_m" runat="server" />
             <asp:HiddenField ID="hd_min_begin_date_d" runat="server" />
             <asp:HiddenField ID="hd_min_begin_date_y" runat="server" />
  
    <!--Hidden fields BEGIN SECTION 1-->
         <input type="hidden" id="hd_home_id" runat="server" />
         <input type="hidden" id="hd_unit_id" runat="server" />
         <input type="hidden" id="hd_current_tu_id" runat="server" />
         <!--Hidden fields END SECTION 1-->
           <!--Hidden fields temporaire-->
          <input type="hidden" id="didden" runat="server" />

</asp:Content>

