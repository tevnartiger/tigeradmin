﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.OwnerObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : may 10, 2008
/// </summary>

public partial class manager_property_property_update : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string home = Request.QueryString["h_id"];


        OwnerObjectAuthorization homeAuthorization = new OwnerObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), Convert.ToInt32(Request.QueryString["h_id"])))
        {
            Session.Abandon();
            Response.Redirect("http://www.sinfocatiger.com/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////


        // NOT Postback 
        if (!(Page.IsPostBack))
        {
            SetDefaultView();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            SqlCommand cmd = new SqlCommand("prHomeView", conn);
            SqlCommand cmd4 = new SqlCommand("prDimensionView", conn);
            SqlCommand cmd5 = new SqlCommand("prHomeEvaluationView", conn);
            SqlCommand cmd6 = new SqlCommand("prHomeCharView", conn);
            SqlCommand cmd7 = new SqlCommand("prHomeSiteView", conn);
            SqlCommand cmd8 = new SqlCommand("prHomeAllEvaluationList", conn);

            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(home);

            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                    home_name.Text = dr["home_name"].ToString();
                    home_type.Text = Convert.ToString(dr["home_type"]);
                    home_style.Text = Convert.ToString(dr["home_style"]);
                    home_unit.Text = Convert.ToString(dr["home_unit"]);
                    home_floor.Text = Convert.ToString(dr["home_floor"]);

                    home_matricule.Text = Convert.ToString(dr["home_matricule"]);
                    home_register.Text = Convert.ToString(dr["home_register"]);
                    home_localisation_certificat.Text = Convert.ToString(dr["home_localisation_certificat"]);

                    DateTime date_built = new DateTime();
                    date_built = Convert.ToDateTime(dr["home_date_built"]);
                    

                    ddl_home_date_built_y.Text = date_built.Year.ToString();
                    ddl_home_date_built_m.Text = date_built.Month.ToString();
                    ddl_home_date_built_d.Text = date_built.Day.ToString();


                    DateTime date_purchase = new DateTime();
                    date_purchase = Convert.ToDateTime(dr["home_date_purchase"]);
                    ddl_home_date_purchase_y.Text = date_purchase.Year.ToString();
                    ddl_home_date_purchase_m.Text = date_purchase.Month.ToString();
                    ddl_home_date_purchase_d.Text = date_purchase.Day.ToString();

                    home_purchase_price.Text = Convert.ToString(dr["home_purchase_price"]);
                    home_living_space.Text = Convert.ToString(dr["home_living_space"]);
                    home_land_size.Text = Convert.ToString(dr["home_land_size"]);
                    home_desc.Text = Convert.ToString(dr["home_desc"]);
                    home_addr_no.Text = Convert.ToString(dr["home_addr_no"]);
                    home_addr_street.Text = Convert.ToString(dr["home_addr_street"]);

                    home_pc.Text = Convert.ToString(dr["home_pc"]);
                    home_city.Text = Convert.ToString(dr["home_city"]);
                    home_district.Text = Convert.ToString(dr["home_district"]);
                    home_prov.Text = Convert.ToString(dr["home_prov"]);
                }

            }
            finally
            {
                // conn.Close();
            }


            //   SqlConnection conn2 = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            //   SqlCommand cmd2 = new SqlCommand("prHomeRevenueView", conn);
         
            cmd4.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd4.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd4.Parameters.Add("home_id", SqlDbType.Int).Value = Convert.ToInt32(home);

            try
            {
                //    conn.Open();

                SqlDataReader dr4 = null;
                dr4 = cmd4.ExecuteReader(CommandBehavior.SingleRow);

                while (dr4.Read() == true)
                {

                    home_dimension.Text = Convert.ToString(dr4["home_dimension"]);
                    //home_register.Text  = Convert.ToString(dr4["home_register"]);
                    home_area.Text = Convert.ToString(dr4["home_area"]);
                    home_land_dimension.Text = Convert.ToString(dr4["home_land_dimension"]);
                    // home_localisation_certificat.Text = Convert.ToString(dr4["home_localisation_certificat"]);
                    home_land_surface.Text = Convert.ToString(dr4["home_land_surface"]);


                }

            }
            finally
            {
                //  conn.Close();
            }

            cmd5.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd5.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd5.Parameters.Add("home_id", SqlDbType.Int).Value = Convert.ToInt32(home);

            try
            {
                // conn.Open();

                SqlDataReader dr5 = null;
                dr5 = cmd5.ExecuteReader(CommandBehavior.SingleRow);

                while (dr5.Read() == true)
                {
                    //home_year_evaluation.Text = Convert.ToString(dr["home_year_evaluation"]);
                    ddl_year_evaluation.SelectedValue = Convert.ToString(dr5["home_year_evaluation"]);
                    home_land_evaluation.Text = Convert.ToString(dr5["home_land_evaluation"]);
                    home_building_evaluation.Text = Convert.ToString(dr5["home_building_evaluation"]);
                    home_total_evaluation.Text = Convert.ToString(dr5["home_total_evaluation"]);

                }

            }
            finally
            {
                //   conn.Close();
            }
            cmd6.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd6.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd6.Parameters.Add("home_id", SqlDbType.Int).Value = Convert.ToInt32(home);

            try
            {
                //  conn.Open();

                SqlDataReader dr6 = null;
                dr6 = cmd6.ExecuteReader(CommandBehavior.SingleRow);

                while (dr6.Read() == true)
                {
                    home_fondation.Text = Convert.ToString(dr6["home_fondation"]);
                    home_frame.Text = Convert.ToString(dr6["home_frame"]);
                    home_roof.Text = Convert.ToString(dr6["home_roof"]);
                    home_windows.Text = Convert.ToString(dr6["home_windows"]);
                    home_floors.Text = Convert.ToString(dr6["home_floors"]);
                    home_under_floors.Text = Convert.ToString(dr6["home_under_floors"]);
                    home_wall.Text = Convert.ToString(dr6["home_wall"]);
                    home_water_heater.Text = Convert.ToString(dr6["home_water_heater"]);

                    home_lav_sech.Text = Convert.ToString(dr6["home_lav_sech"]);
                    home_fire_protection.Text = Convert.ToString(dr6["home_fire_protection"]);
                    home_laundry.Text = Convert.ToString(dr6["home_laundry"]);

                    home_parking_int.Text = Convert.ToString(dr6["home_parking_int"]);
                    home_parking_ext.Text = Convert.ToString(dr6["home_parking_ext"]);
                    home_prise_ext.Text = Convert.ToString(dr6["home_prise_ext"]);
                    home_water.Text = Convert.ToString(dr6["home_water"]);
                    home_heating.Text = Convert.ToString(dr6["home_heating"]);
                    home_combustible.Text = Convert.ToString(dr6["home_combustible"]);
                    home_sewage.Text = Convert.ToString(dr6["home_sewage"]);
                    home_ext_finition.Text = Convert.ToString(dr6["home_ext_finition"]);


                    home_parking.Text = Convert.ToString(dr6["home_parking"]);
                    home_particularity.Text = Convert.ToString(dr6["home_particularity"]);


                }

            }
            finally
            {
                //  conn.Close();
            }

            cmd7.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd7.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd7.Parameters.Add("home_id", SqlDbType.Int).Value = Convert.ToInt32(home);

            try
            {
                // conn.Open();

                SqlDataReader dr7 = null;
                dr7 = cmd7.ExecuteReader(CommandBehavior.SingleRow);

                while (dr7.Read() == true)
                {

                    if (Convert.ToString(dr7["home_site_corner"]) == "1")
                        home_site_corner.Checked = true;
                    if (Convert.ToString(dr7["home_site_highway"]) == "1")
                        home_site_highway.Checked = true;
                    if (Convert.ToString(dr7["home_site_school"]) == "1")
                        home_site_school.Checked = true;
                    if (Convert.ToString(dr7["home_site_services"]) == "1")
                        home_site_services.Checked = true;
                    if (Convert.ToString(dr7["home_site_transport"]) == "1")
                        home_site_transport.Checked = true;

                }

            }
            finally
            {
              //  conn.Close();
            }



              cmd8.CommandType = CommandType.StoredProcedure;
         
            try
            {
                //Add the params
                cmd8.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd8.Parameters.Add("home_id", SqlDbType.Int).Value = Convert.ToInt32(home);

                SqlDataAdapter da = new SqlDataAdapter(cmd8);
                DataSet ds = new DataSet();
                da.Fill(ds);

                gv_home_year_evaluation.DataSource = ds;
                gv_home_year_evaluation.DataBind();
        
            }
            finally
            {
                conn.Close();
            }
        }
        // IF not postback       

    }


    /// <summary>
    /// 
    /// </summary>
    private void SetDefaultView()
    {
        MultiView1.ActiveViewIndex = 0;
        tab1.BgColor = "#ffffcc";
        tab2.BgColor = "AliceBlue";
        tab3.BgColor = "AliceBlue";
        tab4.BgColor = "AliceBlue";
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkTab1_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
        tab1.BgColor = "#ffffcc";
        tab2.BgColor = "AliceBlue";
        tab3.BgColor = "AliceBlue";
        tab4.BgColor = "AliceBlue";

        
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkTab2_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
        tab1.BgColor = "AliceBlue";
        tab2.BgColor = "#ffffcc";
        tab3.BgColor = "AliceBlue";
        tab4.BgColor = "AliceBlue";
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkTab3_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;
        tab1.BgColor = "AliceBlue";
        tab2.BgColor = "AliceBlue";
        tab3.BgColor = "#ffffcc";
        tab4.BgColor = "AliceBlue";
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkTab4_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 3;
      
        tab1.BgColor = "AliceBlue";
        tab2.BgColor = "AliceBlue";
        tab3.BgColor = "AliceBlue";
        tab4.BgColor = "#ffffcc";
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_update_Click(object sender, EventArgs e)
    {

       /*
        //insert home
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        //resulta.InnerHtml = "home name : " + home_name.Text;
        SqlConnection conn8 = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        SqlCommand cmd8 = new SqlCommand("prHomeUpdate", conn8);
        cmd8.CommandType = CommandType.StoredProcedure;


        conn8.Open();
        //Add the params
        cmd8.Parameters.Add("return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd8.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd8.Parameters.Add("home_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["home_id"]);
        cmd8.Parameters.Add("home_name", SqlDbType.VarChar, 50).Value = home_name.Text.ToString();
        cmd8.Parameters.Add("home_type", SqlDbType.VarChar, 50).Value = home_type.Text;
        cmd8.Parameters.Add("home_style", SqlDbType.VarChar, 50).Value = home_style.Text;
        cmd8.Parameters.Add("home_unit", SqlDbType.Int).Value = Convert.ToInt32(home_unit.Text);
        cmd8.Parameters.Add("home_floor", SqlDbType.Int).Value = Convert.ToInt32(home_floor.Text);
        cmd8.Parameters.Add("home_date_built", SqlDbType.DateTime).Value = Convert.ToDateTime(home_date_built_y.Text + "/" + home_date_built_m.Text + "/" + home_date_built_d.Text);
        cmd8.Parameters.Add("home_date_purchase", SqlDbType.DateTime).Value = Convert.ToDateTime(home_date_purchase_y.Text + "/" + home_date_purchase_m.Text + "/" + home_date_purchase_d.Text);
        cmd8.Parameters.Add("home_purchase_price", SqlDbType.Money).Value = Convert.ToDecimal(home_purchase_price.Text);
        cmd8.Parameters.Add("home_living_space", SqlDbType.Int).Value = Convert.ToInt32(home_living_space.Text);
        cmd8.Parameters.Add("home_land_size", SqlDbType.VarChar, 50).Value = home_land_size.Text;


        cmd8.Parameters.Add("home_desc", SqlDbType.VarChar, 50).Value = home_desc.Text;
        cmd8.Parameters.Add("home_addr_no", SqlDbType.VarChar, 50).Value = home_addr_no.Text;
        cmd8.Parameters.Add("home_addr_street", SqlDbType.VarChar, 50).Value = home_addr_street.Text;

        cmd8.Parameters.Add("home_pc", SqlDbType.VarChar, 25).Value = home_pc.Text;
        cmd8.Parameters.Add("home_city", SqlDbType.VarChar, 50).Value = home_city.Text;
        cmd8.Parameters.Add("home_district", SqlDbType.VarChar, 50).Value = home_district.Text;
        cmd8.Parameters.Add("home_prov", SqlDbType.VarChar, 25).Value = home_prov.Text;
        cmd8.Parameters.Add("home_localisation_certificat", SqlDbType.VarChar, 50).Value = home_localisation_certificat.Text;
        cmd8.Parameters.Add("home_matricule", SqlDbType.VarChar, 50).Value = home_matricule.Text;
        cmd8.Parameters.Add("home_register", SqlDbType.VarChar, 50).Value = home_register.Text;


        //*******************

        cmd8.Parameters.Add("home_potential_revenue", SqlDbType.Money).Value = Convert.ToDecimal(home_potential_revenue.Text);
        cmd8.Parameters.Add("home_expected_expenses", SqlDbType.Money).Value = Convert.ToDecimal(home_expected_expenses.Text);
        cmd8.Parameters.Add("home_net_expected_revenue", SqlDbType.Money).Value = Convert.ToDecimal(home_net_revenue.Text);
        //  cmd8.Parameters.Add("home_matricule", SqlDbType.VarChar, 50).Value = home_matricule.Text;

        cmd8.Parameters.Add("home_dimension", SqlDbType.VarChar, 50).Value = home_dimension.Text;
        // cmd8.Parameters.Add("home_register", SqlDbType.VarChar, 50).Value = home_register.Text;
        cmd8.Parameters.Add("home_area", SqlDbType.Int).Value = home_area.Text;
        cmd8.Parameters.Add("home_land_dimension", SqlDbType.VarChar, 50).Value = home_land_dimension.Text;

        cmd8.Parameters.Add("home_land_surface", SqlDbType.Int).Value = Convert.ToInt32(home_land_surface.Text);


        cmd8.Parameters.Add("home_year_evaluation", SqlDbType.Int).Value = Convert.ToInt32(ddl_year_evaluation.SelectedValue);
        cmd8.Parameters.Add("home_land_evaluation", SqlDbType.Int).Value = Convert.ToInt32(home_land_evaluation.Text);
        cmd8.Parameters.Add("home_building_evaluation", SqlDbType.Int).Value = Convert.ToInt32(home_building_evaluation.Text);
        cmd8.Parameters.Add("home_total_evaluation", SqlDbType.Int).Value = Convert.ToInt32(home_total_evaluation.Text);


        cmd8.Parameters.Add("home_fondation", SqlDbType.VarChar, 50).Value = home_fondation.Text;
        cmd8.Parameters.Add("home_frame", SqlDbType.VarChar, 50).Value = home_frame.Text;
        cmd8.Parameters.Add("home_roof", SqlDbType.VarChar, 50).Value = home_roof.Text;
        cmd8.Parameters.Add("home_windows", SqlDbType.VarChar, 50).Value = home_windows.Text;
        cmd8.Parameters.Add("home_floors", SqlDbType.VarChar, 50).Value = home_floors.Text;
        cmd8.Parameters.Add("home_under_floors", SqlDbType.VarChar, 50).Value = home_under_floors.Text;
        cmd8.Parameters.Add("home_wall", SqlDbType.VarChar, 50).Value = home_wall.Text;


        cmd8.Parameters.Add("home_water_heater", SqlDbType.VarChar, 50).Value = home_water_heater.Text;

        cmd8.Parameters.Add("home_lav_sech", SqlDbType.Int).Value = Convert.ToInt32(home_lav_sech.Text);
        cmd8.Parameters.Add("home_fire_protection", SqlDbType.Int).Value = Convert.ToInt32(home_fire_protection.Text);
        cmd8.Parameters.Add("home_laundry", SqlDbType.Int).Value = Convert.ToInt32(home_laundry.Text);


        cmd8.Parameters.Add("home_parking_int", SqlDbType.Int).Value = Convert.ToInt32(home_parking_int.Text);
        cmd8.Parameters.Add("home_parking_ext", SqlDbType.Int).Value = Convert.ToInt32(home_parking_ext.Text);
        cmd8.Parameters.Add("home_prise_ext", SqlDbType.Int).Value = Convert.ToInt32(home_prise_ext.Text);
        cmd8.Parameters.Add("home_water", SqlDbType.VarChar, 50).Value = home_water.Text;
        cmd8.Parameters.Add("home_heating", SqlDbType.VarChar, 50).Value = home_heating.Text;
        cmd8.Parameters.Add("home_combustible", SqlDbType.VarChar, 50).Value = home_combustible.Text;
        cmd8.Parameters.Add("home_sewage", SqlDbType.VarChar, 50).Value = home_sewage.Text;
        cmd8.Parameters.Add("home_ext_finition", SqlDbType.VarChar, 50).Value = home_ext_finition.Text;


        if (home_site_corner.Checked == true)
            cmd8.Parameters.Add("home_site_corner", SqlDbType.Int).Value = 1;
        else
            cmd8.Parameters.Add("home_site_corner", SqlDbType.Int).Value = 0;


        if (home_site_highway.Checked == true)
            cmd8.Parameters.Add("home_site_highway", SqlDbType.Int).Value = 1;
        else
            cmd8.Parameters.Add("home_site_highway", SqlDbType.Int).Value = 0;

        if (home_site_school.Checked == true)
            cmd8.Parameters.Add("home_site_school", SqlDbType.Int).Value = 1;
        else
            cmd8.Parameters.Add("home_site_school", SqlDbType.Int).Value = 0;

        if (home_site_services.Checked == true)
            cmd8.Parameters.Add("home_site_services", SqlDbType.Int).Value = 1;
        else
            cmd8.Parameters.Add("home_site_services", SqlDbType.Int).Value = 0;


        if (home_site_transport.Checked == true)
            cmd8.Parameters.Add("home_site_transport", SqlDbType.Int).Value = 1;
        else
            cmd8.Parameters.Add("home_site_transport", SqlDbType.Int).Value = 0;





        cmd8.Parameters.Add("home_parking", SqlDbType.VarChar, 50).Value = home_parking.Text;
        cmd8.Parameters.Add("home_particularity", SqlDbType.VarChar, 50).Value = home_particularity.Text;






        //execute the insert
        cmd8.ExecuteNonQuery();


        Response.Redirect("unit_add.aspx");
 */
        

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_1_Click(object sender, EventArgs e)
    {
        //insert home
        //resulta.InnerHtml = "home name : " + home_name.Text;
        SqlConnection conn8 = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        SqlCommand cmd8 = new SqlCommand("prHomeUpdateSection1", conn8);
        cmd8.CommandType = CommandType.StoredProcedure;


        conn8.Open();
        //Add the params
        cmd8.Parameters.Add("return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd8.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd8.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["h_id"]);
        cmd8.Parameters.Add("@home_name", SqlDbType.VarChar, 50).Value = home_name.Text.ToString();
        cmd8.Parameters.Add("@home_type", SqlDbType.VarChar, 50).Value = home_type.Text;
        cmd8.Parameters.Add("@home_style", SqlDbType.VarChar, 50).Value = home_style.Text;
        cmd8.Parameters.Add("@home_unit", SqlDbType.Int).Value = Convert.ToInt32(home_unit.Text);
        cmd8.Parameters.Add("@home_floor", SqlDbType.Int).Value = Convert.ToInt32(home_floor.Text);


        DateTime date_built = new DateTime();
        DateTime date_purchase = new DateTime();

        tiger.Date c = new tiger.Date();
        date_built = Convert.ToDateTime(c.DateCulture(ddl_home_date_built_m.SelectedValue, ddl_home_date_built_d.SelectedValue, ddl_home_date_built_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        date_purchase = Convert.ToDateTime(c.DateCulture(ddl_home_date_purchase_m.SelectedValue, ddl_home_date_purchase_d.SelectedValue, ddl_home_date_purchase_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        cmd8.Parameters.Add("@home_date_built", SqlDbType.DateTime).Value = date_built;
        cmd8.Parameters.Add("@home_date_purchase", SqlDbType.DateTime).Value = date_purchase;
        
        
        
        
        cmd8.Parameters.Add("@home_purchase_price", SqlDbType.Money).Value = Convert.ToDecimal(home_purchase_price.Text);
        cmd8.Parameters.Add("@home_living_space", SqlDbType.Int).Value = Convert.ToInt32(home_living_space.Text);
        cmd8.Parameters.Add("@home_land_size", SqlDbType.VarChar, 50).Value = home_land_size.Text;


        cmd8.Parameters.Add("@home_desc", SqlDbType.VarChar, 50).Value = home_desc.Text;
        cmd8.Parameters.Add("@home_addr_no", SqlDbType.VarChar, 50).Value = home_addr_no.Text;
        cmd8.Parameters.Add("@home_addr_street", SqlDbType.VarChar, 50).Value = home_addr_street.Text;

        cmd8.Parameters.Add("@home_pc", SqlDbType.VarChar, 25).Value = home_pc.Text;
        cmd8.Parameters.Add("@home_city", SqlDbType.VarChar, 50).Value = home_city.Text;
        cmd8.Parameters.Add("@home_district", SqlDbType.VarChar, 50).Value = home_district.Text;
        cmd8.Parameters.Add("@home_prov", SqlDbType.VarChar, 25).Value = home_prov.Text;

        
        //execute the update
        cmd8.ExecuteNonQuery();

        //conn8.Close();


    }
    /// <summary>
    /// update home dimensiom
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_2_Click(object sender, EventArgs e)
    {


        //  cmd8.Parameters.Add("@home_matricule", SqlDbType.VarChar, 50).Value = home_matricule.Text;

        //insert home
        //resulta.InnerHtml = "home name : " + home_name.Text;
        SqlConnection conn8 = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        SqlCommand cmd8 = new SqlCommand("prHomeUpdateSection2", conn8);
        cmd8.CommandType = CommandType.StoredProcedure;


        conn8.Open();
        //Add the params
        cmd8.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd8.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd8.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["h_id"]);
        cmd8.Parameters.Add("@home_dimension", SqlDbType.VarChar, 50).Value = home_dimension.Text;
        cmd8.Parameters.Add("@home_register", SqlDbType.VarChar, 50).Value = home_register.Text;
        cmd8.Parameters.Add("@home_area", SqlDbType.Int).Value = home_area.Text;
        cmd8.Parameters.Add("@home_land_dimension", SqlDbType.VarChar, 50).Value = home_land_dimension.Text;
        cmd8.Parameters.Add("@home_localisation_certificat", SqlDbType.VarChar, 50).Value = home_localisation_certificat.Text;
        cmd8.Parameters.Add("@home_matricule", SqlDbType.Int).Value = Convert.ToInt32(home_matricule.Text) ;
        cmd8.Parameters.Add("@home_land_surface", SqlDbType.Int).Value = Convert.ToInt32(home_land_surface.Text);

        cmd8.Parameters.Add("@home_land_surface_type", SqlDbType.VarChar, 50).Value = ddl_land_surface_type.SelectedValue ;
        cmd8.Parameters.Add("@home_habitable_surface_type", SqlDbType.VarChar, 50).Value = ddl_habitable_surface_type.SelectedValue;


        //execute the update
        cmd8.ExecuteNonQuery();

        conn8.Close();


    }
    /// <summary>
    /// update home evaluation
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_3_Click(object sender, EventArgs e)
    {


        //insert home
        //resulta.InnerHtml = "home name : " + home_name.Text;
        SqlConnection conn8 = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        SqlCommand cmd8 = new SqlCommand("prHomeUpdateSection3", conn8);
        cmd8.CommandType = CommandType.StoredProcedure;


        conn8.Open();
        //Add the params
        cmd8.Parameters.Add("return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd8.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd8.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["h_id"]);
        cmd8.Parameters.Add("@home_year_evaluation", SqlDbType.Int).Value = Convert.ToInt32(ddl_year_evaluation.SelectedValue);
        cmd8.Parameters.Add("@home_land_evaluation", SqlDbType.Int).Value = Convert.ToInt32(home_land_evaluation.Text);
        cmd8.Parameters.Add("@home_building_evaluation", SqlDbType.Int).Value = Convert.ToInt32(home_building_evaluation.Text);
        cmd8.Parameters.Add("@home_total_evaluation", SqlDbType.Int).Value = Convert.ToInt32(home_total_evaluation.Text);

        //execute the update
        cmd8.ExecuteNonQuery();

        conn8.Close();






        string home = Request.QueryString["h_id"];


        // getting the new value that was added and put it in a gridview for displaying
        if (ddl_year_evaluation.SelectedValue != "0" || home_land_evaluation.Text == "" || home_building_evaluation.Text == "")
        {

            SqlConnection conn9 = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            SqlCommand cmd9 = new SqlCommand("prHomeAllEvaluationList", conn9);
            cmd9.CommandType = CommandType.StoredProcedure;
            conn9.Open();
            cmd9.CommandType = CommandType.StoredProcedure;

            try
            {
                //Add the params
                cmd9.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd9.Parameters.Add("home_id", SqlDbType.Int).Value = Convert.ToInt32(home);

                SqlDataAdapter da = new SqlDataAdapter(cmd9);
                DataSet ds = new DataSet();
                da.Fill(ds);

                gv_home_year_evaluation.DataSource = ds;
                gv_home_year_evaluation.DataBind();

            }
            finally
            {
                conn9.Close();
            }
        }









    }
    /// <summary>
    /// update home characteristics
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_4_Click(object sender, EventArgs e)
    {




        //insert home
        //resulta.InnerHtml = "home name : " + home_name.Text;
        SqlConnection conn8 = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        SqlCommand cmd8 = new SqlCommand("prHomeUpdateSection4", conn8);
        cmd8.CommandType = CommandType.StoredProcedure;


        conn8.Open();
        //Add the params
        cmd8.Parameters.Add("return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd8.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd8.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["h_id"]);
        cmd8.Parameters.Add("@home_fondation", SqlDbType.VarChar, 50).Value = home_fondation.Text;
        cmd8.Parameters.Add("@home_frame", SqlDbType.VarChar, 50).Value = home_frame.Text;
        cmd8.Parameters.Add("@home_roof", SqlDbType.VarChar, 50).Value = home_roof.Text;
        cmd8.Parameters.Add("@home_windows", SqlDbType.VarChar, 50).Value = home_windows.Text;
        cmd8.Parameters.Add("@home_floors", SqlDbType.VarChar, 50).Value = home_floors.Text;
        cmd8.Parameters.Add("@home_under_floors", SqlDbType.VarChar, 50).Value = home_under_floors.Text;
        cmd8.Parameters.Add("@home_wall", SqlDbType.VarChar, 50).Value = home_wall.Text;


        cmd8.Parameters.Add("@home_water_heater", SqlDbType.VarChar, 50).Value = home_water_heater.Text;

        cmd8.Parameters.Add("@home_lav_sech", SqlDbType.Int).Value = Convert.ToInt32(home_lav_sech.Text);
        cmd8.Parameters.Add("@home_fire_protection", SqlDbType.Int).Value = Convert.ToInt32(home_fire_protection.Text);
        cmd8.Parameters.Add("@home_laundry", SqlDbType.Int).Value = Convert.ToInt32(home_laundry.Text);


        cmd8.Parameters.Add("@home_parking_int", SqlDbType.Int).Value = Convert.ToInt32(home_parking_int.Text);
        cmd8.Parameters.Add("@home_parking_ext", SqlDbType.Int).Value = Convert.ToInt32(home_parking_ext.Text);
        cmd8.Parameters.Add("@home_prise_ext", SqlDbType.Int).Value = Convert.ToInt32(home_prise_ext.Text);
        cmd8.Parameters.Add("@home_water", SqlDbType.VarChar, 50).Value = home_water.Text;
        cmd8.Parameters.Add("@home_heating", SqlDbType.VarChar, 50).Value = home_heating.Text;
        cmd8.Parameters.Add("@home_combustible", SqlDbType.VarChar, 50).Value = home_combustible.Text;
        cmd8.Parameters.Add("@home_sewage", SqlDbType.VarChar, 50).Value = home_sewage.Text;
        cmd8.Parameters.Add("@home_ext_finition", SqlDbType.VarChar, 50).Value = home_ext_finition.Text;

        cmd8.Parameters.Add("@home_particularity", SqlDbType.VarChar, 50).Value = home_particularity.Text;

        if (home_site_corner.Checked == true)
            cmd8.Parameters.Add("@home_site_corner", SqlDbType.Int).Value = 1;
        else
            cmd8.Parameters.Add("@home_site_corner", SqlDbType.Int).Value = 0;


        if (home_site_highway.Checked == true)
            cmd8.Parameters.Add("@home_site_highway", SqlDbType.Int).Value = 1;
        else
            cmd8.Parameters.Add("@home_site_highway", SqlDbType.Int).Value = 0;

        if (home_site_school.Checked == true)
            cmd8.Parameters.Add("@home_site_school", SqlDbType.Int).Value = 1;
        else
            cmd8.Parameters.Add("@home_site_school", SqlDbType.Int).Value = 0;

        if (home_site_services.Checked == true)
            cmd8.Parameters.Add("@home_site_services", SqlDbType.Int).Value = 1;
        else
            cmd8.Parameters.Add("@home_site_services", SqlDbType.Int).Value = 0;


        if (home_site_transport.Checked == true)
            cmd8.Parameters.Add("@home_site_transport", SqlDbType.Int).Value = 1;
        else
            cmd8.Parameters.Add("@home_site_transport", SqlDbType.Int).Value = 0;





        cmd8.Parameters.Add("@home_parking", SqlDbType.VarChar, 50).Value = home_parking.Text;
      


        //execute the insert
        cmd8.ExecuteNonQuery();

        conn8.Close();
    }
}
