﻿<%@ Page Language="C#" MasterPageFile="~/owner/owner_mp.master" AutoEventWireup="true" CodeFile="property_unit_list.aspx.cs" Inherits="manager_property_property_unit_list" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<table>
<tr><td> <asp:Label ID="lbl_property" runat="server" Text="<%$ Resources:Resource, lbl_property %>"/>
                </td>
                <td>
       <asp:DropDownList ID="ddl_home_id" DataValueField="home_id" DataTextField="home_name"   runat="server" autopostback="true" OnSelectedIndexChanged="ddl_home_id_SelectedIndexChanged" />
       </td>  
</tr>
 </table><br />
    <table style="width: 100%">
        <tr>
            <td valign="top">
<asp:Repeater runat="server" ID="rhome_view">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
        
        <tr>
          <td>
                 District&nbsp;:&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_district")%> 
                   </td>
            </tr>
           <tr>
              <td valign="top" >
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_no")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_street")%></td>
            </tr>
            <tr>
                 <td valign="top" >
                   <%#DataBinder.Eval(Container.DataItem, "home_city")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_prov")%>&nbsp;,&nbsp;&nbsp;  <%#DataBinder.Eval(Container.DataItem, "home_pc")%> </td>
               
            </tr>
           
        </table>
        </ItemTemplate>
        </asp:Repeater> 
            </td>
            <td valign="top">
              
            </td>
        </tr>
    </table><br />
    
    <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="100%" 
        BorderColor="White" BorderWidth="3px" ID="GridView1" runat="server" AutoGenerateColumns="False" 
        AlternatingRowStyle-BackColor="Beige"  DataKeyNames="unit_id"  DataSourceID="SqlDataSource1" 
        PageSize="10" AllowPaging="True" >
        <Columns>
            
            
            
            <asp:BoundField DataField="unit_level" HeaderStyle-VerticalAlign="Top"  HeaderText="<%$ Resources:Resource, lbl_unit_level %>" 
                SortExpression="unit_level" />
            
            
           
            <asp:TemplateField HeaderStyle-VerticalAlign="Top"  HeaderText="<%$ Resources:Resource, lbl_door_number %>">
            <ItemTemplate>
              <asp:Label ID="lbl_unit_door_no" runat="server" Text='<%# Bind("unit_door_no") %>'></asp:Label>
            </ItemTemplate>
            <EditItemTemplate  >
              <asp:TextBox Width="50" id="lbl_unit_door_no2" Runat="Server" Text='<%# Bind("unit_door_no") %>'/>
            </EditItemTemplate>
            </asp:TemplateField>
            
            
            
            <asp:TemplateField HeaderStyle-VerticalAlign="Top"  HeaderText="<%$ Resources:Resource, lbl_size_sqft2 %>">
              <ItemTemplate>
              <asp:Label ID="lbl_unit_size_sqft" runat="server" Text='<%# Bind("unit_size_sqft") %>'></asp:Label>
              </ItemTemplate>
              <EditItemTemplate  >
              <asp:TextBox Width="50" id="lbl_unit_size_sqft" Runat="Server" Text='<%#Bind("unit_size_sqft") %>'/>
              </EditItemTemplate>
            </asp:TemplateField>
            
            
            <asp:TemplateField HeaderStyle-VerticalAlign="Top"  HeaderText="<%$ Resources:Resource, lbl_size_sqm2 %>">
            <ItemTemplate>
                <asp:Label ID="lbl_unit_size_sqm" runat="server" Text='<%# Bind("unit_size_sqm") %>'></asp:Label>
            </ItemTemplate>
           </asp:TemplateField>
            
               
            <asp:BoundField HeaderStyle-VerticalAlign="Top"  DataField="unit_bedroom_no" HeaderText="<%$ Resources:Resource, lbl_number_of_bedrooms %>" 
                SortExpression="unit_bedroom_no" />
            <asp:BoundField DataField="unit_bathroom_no" HeaderText="<%$ Resources:Resource, lbl_number_of_bathrooms %>"
                SortExpression="unit_bathroom_no" />
            
            <asp:BoundField HeaderStyle-VerticalAlign="Top"  DataField="unit_max_tenant" HeaderText="<%$ Resources:Resource, lbl_maximum_tenants %>" 
                SortExpression="unit_max_tenant" />
                
                <asp:CommandField CancelText='<%$Resources:Resource,lbl_cancel %>' 
                  EditText='<%$Resources:Resource,lbl_update %>'   UpdateText='<%$Resources:Resource,lbl_submit %>'  ShowEditButton="True" />
        </Columns>


    </asp:GridView>
    
      <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:sinfoca_tigerConnectionString2 %>" 
        SelectCommand="prHomeUnitList" SelectCommandType="StoredProcedure"
         UpdateCommand="prUnitUpdate" UpdateCommandType="StoredProcedure"  >
            
        <SelectParameters>
            <asp:ControlParameter ControlID="ddl_home_id" Name="home_id" 
                PropertyName="SelectedValue" Type="Int32" />
            <asp:SessionParameter Name="schema_id" SessionField="schema_id" Type="Int32" />
        </SelectParameters>
        
        
         <UpdateParameters>
            <asp:SessionParameter Name="schema_id" SessionField="schema_id" Type="Int32" />
          </UpdateParameters>
            
            
            
    </asp:SqlDataSource>
    <br />
</asp:Content>

