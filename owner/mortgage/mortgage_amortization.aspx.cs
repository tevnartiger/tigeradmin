﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : march 5 , 2008
/// </summary>
public partial class mortgage_mortgage_amortization : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {

            DateTime ladate = new DateTime();
            ladate = DateTime.Now;
            
            // the default selected month and year is now
            ddl_mortgage_begin_m.SelectedValue = ladate.Month.ToString();
            ddl_mortgage_begin_d.SelectedValue = ladate.Day.ToString();
            ddl_mortgage_begin_y.SelectedValue = ladate.Year.ToString();

            r_amortizationExtraCalculation.Visible = false;

        }
      
    }
    protected void btn_calculate_Click(object sender, EventArgs e)
    {
        string lang_session = Session["_lastCulture"].ToString();
        //frequency at wich the mortgage is paid : Monthly, bi-weekly or weekly
        double frequency = Convert.ToDouble(ddl_mortgage_paid_frequency.SelectedValue);
        //Duration in year of the mortgage ( years of amortization)
        double duration = Convert.ToDouble(tbx_duration_years.Text);
        double interest = Convert.ToDouble(tbx_interest.Text);
        double balance = Convert.ToDouble(tbx_balance.Text);


        DateTime from = new DateTime();
        
        tiger.Date d = new tiger.Date();
        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
       // from = Convert.ToDateTime(d.DateCulture(ddl_mortgage_begin_m.SelectedValue, "1", ddl_mortgage_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        from = Convert.ToDateTime(d.DateCulture(ddl_mortgage_begin_m.SelectedValue, ddl_mortgage_begin_d.SelectedValue, ddl_mortgage_begin_y.SelectedValue, lang_session));


        Label1.Text = lang_session;// ddl_mortgage_begin_m.SelectedValue + "/" + ddl_mortgage_begin_y.SelectedValue;

        tiger.Amortization hv = new tiger.Amortization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


        // if the interest is compounded each year we use The America Amortization ( also Euro )
        if (ddl_interest_compounded.SelectedValue == "12")
        {
          
            // All the calculation of payment, interest, 50/50 point, end point, total payments, total interest etc...
           r_amortizationCalculation.DataSource = hv.getAmortizationCalculation(from, balance, duration, 12.00, interest,0.00,
                                                  0.00, Convert.ToInt32(ddl_eachYear.SelectedValue));
            r_amortizationCalculation.DataBind();

            // if the mortgage is paid by-weekly or weekly show the values and get the savigs repeater visible or of we add an additional payment
            if (ddl_mortgage_paid_frequency.SelectedValue == "241" || ddl_mortgage_paid_frequency.SelectedValue == "521" || ddl_mortgage_paid_frequency.SelectedValue == "24" || ddl_mortgage_paid_frequency.SelectedValue == "52" || Convert.ToDouble(tbx_additionnal_eachYear.Text) > 0.00 || Convert.ToInt32(tbx_additionnal_payment.Text) > 0.00)
            {
                //All the calculation of payment, interest, 50/50 point, end point,total payments, total interest etc...
                 r_amortizationExtraCalculation.DataSource = hv.getAmortizationCalculation(from, balance, duration, frequency, interest, Convert.ToDouble(tbx_additionnal_payment.Text),
                                                        Convert.ToDouble(tbx_additionnal_eachYear.Text), Convert.ToInt32(ddl_eachYear.SelectedValue));
                r_amortizationExtraCalculation.DataBind();
            }
        }

        // if the interest is compounded twice year we use The Canadian Amortization
        if (ddl_interest_compounded.SelectedValue == "6")
        {
         
             // All the calculation of Canadian payment, interest, 50/50 point, end point, total payments, total interest etc...
             r_amortizationCalculation.DataSource = hv.getAmortizationCanCalculation(from, balance, duration, 12.00, interest, 0.00,
                                                    0.00, Convert.ToInt32(ddl_eachYear.SelectedValue));
             r_amortizationCalculation.DataBind();

             // if the mortgage is paid by-weekly or weekly  show the values and get the savigs repeater visible or additional payments
             if (ddl_mortgage_paid_frequency.SelectedValue == "241" || ddl_mortgage_paid_frequency.SelectedValue == "521" || ddl_mortgage_paid_frequency.SelectedValue == "24" || ddl_mortgage_paid_frequency.SelectedValue == "52" || Convert.ToDouble(tbx_additionnal_eachYear.Text) > 0.00 || Convert.ToInt32(tbx_additionnal_payment.Text) > 0.00)
             {
                 
                 
                 // All the calculation of payment, interest, 50/50 point, end point, total payments, total interest etc...
                  r_amortizationExtraCalculation.DataSource = hv.getAmortizationCanCalculation(from, balance, duration, frequency, interest, Convert.ToDouble(tbx_additionnal_payment.Text),
                                                           Convert.ToDouble(tbx_additionnal_eachYear.Text), Convert.ToInt32(ddl_eachYear.SelectedValue));
                  r_amortizationExtraCalculation.DataBind();
             }
        }

        gv_amortization.Visible = false;


        if (ddl_mortgage_paid_frequency.SelectedIndex > 0 || Convert.ToDouble(tbx_additionnal_eachYear.Text) > Convert.ToDouble(0.00) || Convert.ToDouble(tbx_additionnal_payment.Text) > Convert.ToDouble(0.00))
            r_amortizationExtraCalculation.Visible = true;
        else
            r_amortizationExtraCalculation.Visible = false;

        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_calculate_extra_Click(object sender, EventArgs e)
    {

        string lang_session = Session["_lastCulture"].ToString();
        //frequency at wich the mortgage is paid : Monthly, bi-weekly or weekly
        double frequency = Convert.ToDouble(ddl_mortgage_paid_frequency.SelectedValue);
        //Duration in year of the mortgage ( years of amortization)
        double duration = Convert.ToDouble(tbx_duration_years.Text);
        double interest = Convert.ToDouble(tbx_interest.Text);
        double balance = Convert.ToDouble(tbx_balance.Text);


        DateTime from = new DateTime();

        tiger.Date d = new tiger.Date();
        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime

        from = Convert.ToDateTime(d.DateCulture(ddl_mortgage_begin_m.SelectedValue, ddl_mortgage_begin_d.SelectedValue, ddl_mortgage_begin_y.SelectedValue, lang_session));


        Label1.Text = lang_session;// ddl_mortgage_begin_m.SelectedValue + "/" + ddl_mortgage_begin_y.SelectedValue;

        tiger.Amortization hv = new tiger.Amortization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


        // if the interest is compounded each year we use The America Amortization ( also Euro )
        if (ddl_interest_compounded.SelectedValue == "12")
        {
            gv_amortization.DataSource = hv.getAmortizationTable(from, balance, duration, frequency, interest, Convert.ToDouble(tbx_additionnal_payment.Text),
                                                     Convert.ToDouble(tbx_additionnal_eachYear.Text), Convert.ToInt32(ddl_eachYear.SelectedValue));
            gv_amortization.DataBind();

            // All the calculation of payment, interest, 50/50 point, end point, total payments, total interest etc...
            r_amortizationCalculation.DataSource = hv.getAmortizationCalculation(from, balance, duration, 12.00, interest, 0.00,
                                                    0.00, Convert.ToInt32(ddl_eachYear.SelectedValue));
            r_amortizationCalculation.DataBind();

            // if the mortgage is paid by-weekly or weekly show the values and get the savigs repeater visible or of we add an additional payment
            if (ddl_mortgage_paid_frequency.SelectedValue == "241" || ddl_mortgage_paid_frequency.SelectedValue == "521" || ddl_mortgage_paid_frequency.SelectedValue == "24" || ddl_mortgage_paid_frequency.SelectedValue == "52" || Convert.ToDouble(tbx_additionnal_eachYear.Text) > 0.00 || Convert.ToInt32(tbx_additionnal_payment.Text) > 0.00)
            {
                //All the calculation of payment, interest, 50/50 point, end point,total payments, total interest etc...
                r_amortizationExtraCalculation.DataSource = hv.getAmortizationCalculation(from, balance, duration, frequency, interest, Convert.ToDouble(tbx_additionnal_payment.Text),
                                                       Convert.ToDouble(tbx_additionnal_eachYear.Text), Convert.ToInt32(ddl_eachYear.SelectedValue));
                r_amortizationExtraCalculation.DataBind();
            }
        }

        // if the interest is compounded twice year we use The Canadian Amortization
        if (ddl_interest_compounded.SelectedValue == "6")
        {
            gv_amortization.DataSource = hv.getAmortizationCanTable(from, balance, duration, frequency, interest, Convert.ToDouble(tbx_additionnal_payment.Text),
                                                    Convert.ToDouble(tbx_additionnal_eachYear.Text), Convert.ToInt32(ddl_eachYear.SelectedValue));
            gv_amortization.DataBind();


            // All the calculation of Canadian payment, interest, 50/50 point, end point, total payments, total interest etc...
            r_amortizationCalculation.DataSource = hv.getAmortizationCanCalculation(from, balance, duration, 12.00, interest, 0.00,
                                                   0.00, Convert.ToInt32(ddl_eachYear.SelectedValue));
            r_amortizationCalculation.DataBind();

            // if the mortgage is paid by-weekly or weekly  show the values and get the savigs repeater visible or additional payments
            if (ddl_mortgage_paid_frequency.SelectedValue == "241" || ddl_mortgage_paid_frequency.SelectedValue == "521" || ddl_mortgage_paid_frequency.SelectedValue == "24" || ddl_mortgage_paid_frequency.SelectedValue == "52" || Convert.ToDouble(tbx_additionnal_eachYear.Text) > 0.00 || Convert.ToInt32(tbx_additionnal_payment.Text) > 0.00)
            {


                // All the calculation of payment, interest, 50/50 point, end point, total payments, total interest etc...
                r_amortizationExtraCalculation.DataSource = hv.getAmortizationCanCalculation(from, balance, duration, frequency, interest, Convert.ToDouble(tbx_additionnal_payment.Text),
                                                         Convert.ToDouble(tbx_additionnal_eachYear.Text), Convert.ToInt32(ddl_eachYear.SelectedValue));
                r_amortizationExtraCalculation.DataBind();
            }
        }

        gv_amortization.Visible = true;

        if (ddl_mortgage_paid_frequency.SelectedIndex > 0 || Convert.ToDouble(tbx_additionnal_eachYear.Text) > Convert.ToDouble(0.00) || Convert.ToDouble(tbx_additionnal_payment.Text) > Convert.ToDouble(0.00))
            r_amortizationExtraCalculation.Visible = true;
        else
            r_amortizationExtraCalculation.Visible = false;

    }



    protected string GetYearSave(double timepaywithextraloan )
    {

        double timesaved = 0;
        for (int i = 0; i < r_amortizationCalculation.Items.Count; i++)
        {
            HiddenField h_timetopayloan1 = (HiddenField)r_amortizationCalculation.Items[i].FindControl("h_timetopayloan1");

            timesaved = Convert.ToDouble(h_timetopayloan1.Value) - timepaywithextraloan;

        }

        return String.Format("{0:0.00}", timesaved);
    }


    protected string GetMoneySave(double interestwithextra)
    {
        double moneysaved = 0;
        for (int i = 0; i < r_amortizationCalculation.Items.Count ; i++)
        {
            
           HiddenField h_totalinterest1 = (HiddenField)r_amortizationCalculation.Items[i].FindControl("h_totalinterest1");
            moneysaved = Convert.ToDouble(h_totalinterest1.Value) - interestwithextra;
        }

        return String.Format("{0:0.00}", moneysaved);

        
    }
}
