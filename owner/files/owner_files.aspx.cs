﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Briefcase;
using System.IO;

public partial class owner_files_tenant_files : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {


            FillFolderTree(null);

            tiger.Owner o = new tiger.Owner(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = o.getOwnerHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

            if (home_count > 0)
            {
                int home_id = o.getOwnerHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

                ddl_home_owner_list.DataSource = o.getOwnerHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
              //  ddl_home_owner_list.SelectedValue = Convert.ToString(home_id);
                ddl_home_owner_list.DataBind();


                OwnerBindGrid();

            }


          //  BindGrid();
            TenantBindGrid();
        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="parent"></param>
    private void FillFolderTree(TreeNode parent)
    {
        int parentfolderid = 0;
        if (parent != null)
        {
            parentfolderid = int.Parse(parent.Value.ToString());
        }
                                                                                                        // who the folder belongs to ( 0: everybody , else : an individual)
        DataTable folders = Folders.GetSubFolders(parentfolderid, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

        if (parent != null)
        {
            parent.ChildNodes.Clear();
        }
        else
        {
            TreeView1.Nodes.Clear();
        }

        foreach (DataRow folder in folders.Rows)
        {
            TreeNode node = new TreeNode();
            node.Text = folder["FolderName"].ToString();
            node.Value = folder["Id"].ToString();
            if (parent == null)
            {
                TreeView1.Nodes.Add(node);
            }
            else
            {
                parent.ChildNodes.Add(node);
            }
        }
        if (TreeView1.SelectedNode != null)
        {
            TreeView1.SelectedNode.Expand();
        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
    {
        FillFolderTree(TreeView1.SelectedNode);
        TextBox2.Text = TreeView1.SelectedNode.Text;
        Label10.Text = TreeView1.SelectedNode.Text;
        BindGrid();

        TabContainer1.ActiveTabIndex = 0;
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Button1_Click(object sender, EventArgs e)
    {
        string name_id_ip = Request.UserHostAddress.ToString();

        try
        {

            // explaining the parameters
            // Convert.ToInt32(Session["name_id"])  : the Folder will belong to an individual owner and it can be acces this individual only
            // Convert.ToInt32(Session["name_id"]) : who ceated the folder 
            // name_id_ip : the ip of the user who created the folder


            if (TreeView1.SelectedNode != null && !CheckBox1.Checked)
            {                                                                                                                                           // who the folder belongs to          // who created the folder
                Folders.Create(TextBox1.Text, int.Parse(TreeView1.SelectedNode.Value.ToString()), DateTime.Now, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), Convert.ToInt32(Session["name_id"]), name_id_ip);
                FillFolderTree(TreeView1.SelectedNode);

            }
            else
            {                                                                                         // who the folder belongs to          // who created the folder
                Folders.Create(TextBox1.Text, 0, DateTime.Now, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), Convert.ToInt32(Session["name_id"]), name_id_ip);
                FillFolderTree(null);
            }
        }
        catch (Exception ex)
        {
            Label7.Text = ex.Message;
        }

    }




    protected void Button2_Click(object sender, EventArgs e)
    {
        try
        {
            int id = int.Parse(TreeView1.SelectedNode.Value.ToString());

            // explaining the parameters
            // Convert.ToInt32(Session["name_id"])  : the Folder to rename belong to an individual owner and it can be acces this individual only
            // Convert.ToInt32(Session["name_id"]) : who renamed the folder 
            // name_id_ip : the ip of the user who created the folder


            Folders.Rename(id, TextBox2.Text, Convert.ToInt32(Session["schema_id"])
                     , Convert.ToInt32(Session["name_id"]), Convert.ToInt32(Session["name_id"]),Request.UserHostAddress.ToString());
            //DataTable folders = Folders.GetSubFolders(id);
            TreeView1.SelectedNode.Text = TextBox2.Text;
        }
        catch (Exception ex)
        {
            Label7.Text = ex.Message;
        }

    }



    protected void Button3_Click(object sender, EventArgs e)
    {
        if (TreeView1.SelectedNode != null)
        {
            Folders.Delete(int.Parse(TreeView1.SelectedNode.Value.ToString()), Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
            Files.DeleteFromFolder(int.Parse(TreeView1.SelectedNode.Value.ToString()), Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

            foreach (TreeNode node in TreeView1.SelectedNode.ChildNodes)
            {
                Folders.Delete(int.Parse(node.Value.ToString()), Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                Files.DeleteFromFolder(int.Parse(node.Value.ToString()), Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
            }

            if (TreeView1.SelectedNode.Parent != null)
            {
                FillFolderTree(TreeView1.SelectedNode.Parent);
            }
            else
            {
                FillFolderTree(null);
            }
            GridView1.DataSource = null;
            GridView1.DataBind();
        }
        else
        {
            Label7.Text = Resources.Resource.lbl_folder_to_be_deleted;
        }

        // Button3.Enabled = false;
    }




    protected void Button4_Click(object sender, EventArgs e)
    {

        string name_id_ip = Request.UserHostAddress.ToString();

        try
        {

            if (TreeView1.SelectedNode != null)
            {
                int folderid = int.Parse(TreeView1.SelectedNode.Value);
                string filename = Path.GetFileName(FileUpload1.PostedFile.FileName);
                int filesize = 0;
                if (FileUpload1.HasFile)
                {

                    Stream stream = FileUpload1.PostedFile.InputStream;
                    filesize = FileUpload1.PostedFile.ContentLength;
                    byte[] filedata = new byte[filesize];
                    stream.Read(filedata, 0, filesize);


                    // explaining the parameters
                    // Convert.ToInt32(Session["name_id"])  : the File will belong to an individual owner and it can be acces this individual only
                    // Convert.ToInt32(Session["name_id"]) : who ceated the folder 
                    // name_id_ip : the ip of the user who created the folder

                    Files.Create(filename, filedata, filesize, folderid, DateTime.Now, Convert.ToInt32(Session["schema_id"]),
                                 Convert.ToInt32(Session["name_id"]), Convert.ToInt32(Session["name_id"]), name_id_ip);

                }
                BindGrid();
            }
            else
            {
                Label7.Text = Resources.Resource.lbl_destination_folder;
            }
        }
        catch (Exception ex)
        {
            Label7.Text = Resources.Resource.lbl_duplicate_file_name;
        }

    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int fileid = int.Parse(GridView1.DataKeys[int.Parse(e.CommandArgument.ToString())].Value.ToString());

        if (e.CommandName == "Download")
        {
            DataTable file = Files.GetFile(fileid, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
            byte[] filedata = (byte[])file.Rows[0]["filedata"];
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment;filename=" + file.Rows[0]["filename"]);
            Response.BinaryWrite(filedata);
            Response.End();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        BindGrid();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        BindGrid();
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int fileid = int.Parse(GridView1.DataKeys[e.RowIndex].Value.ToString());
        GridViewRow row = GridView1.Rows[e.RowIndex];

        Files.Rename(fileid, ((TextBox)row.Cells[0].FindControl("TextBox1")).Text, Convert.ToInt32(Session["schema_id"]),
            Convert.ToInt32(Session["name_id"]), Request.UserHostAddress.ToString());
        
        GridView1.EditIndex = -1;
        BindGrid();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Button1_Click1(object sender, EventArgs e)
    {
        int fileid = int.Parse(((Button)sender).CommandArgument);
        Files.Delete(fileid, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
        BindGrid();
    }


    /// <summary>
    /// 
    /// </summary>
    private void BindGrid()
    {

        DataTable files = Files.GetFilesFromFolder(int.Parse(TreeView1.SelectedNode.Value.ToString()), Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
        GridView1.DataSource = files;
        GridView1.DataBind();
    }



    /// <summary>
    /// 
    /// </summary>
    private void TenantBindGrid()
    {

        DataTable files = Files.GetTenantFileList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
        GridView3.DataSource = files;
        GridView3.DataBind();
    }
    /// <summary>
    /// 
    /// </summary>
    private void OwnerBindGrid()
    {

        DataTable files = Files.GetFilesForOwner(int.Parse(ddl_home_owner_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
        GridView2.DataSource = files;
        GridView2.DataBind();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView3_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int fileid = int.Parse(GridView3.DataKeys[int.Parse(e.CommandArgument.ToString())].Value.ToString());

        if (e.CommandName == "Download")
        {
            DataTable file = Files.TenantGetFile(fileid, Convert.ToInt32(Session["schema_id"]));
            byte[] filedata = (byte[])file.Rows[0]["filedata"];
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment;filename=" + file.Rows[0]["filename"]);
            Response.BinaryWrite(filedata);
            Response.End();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_owner_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable files = Files.GetFilesForOwner(int.Parse(ddl_home_owner_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
        GridView2.DataSource = files;
        GridView2.DataBind();

        TabContainer1.ActiveTabIndex = 1;

    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int fileid = int.Parse(GridView2.DataKeys[int.Parse(e.CommandArgument.ToString())].Value.ToString());

        if (e.CommandName == "Download")
        {
            DataTable file = Files.OwnerGetFile(fileid, Convert.ToInt32(Session["schema_id"]));
            byte[] filedata = (byte[])file.Rows[0]["filedata"];
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment;filename=" + file.Rows[0]["filename"]);
            Response.BinaryWrite(filedata);
            Response.End();
        }

        TabContainer1.ActiveTabIndex = 1;
    }



}
