﻿<%@ Page Title="" Language="C#" MasterPageFile="~/owner/owner_mp.master" AutoEventWireup="true" CodeFile="owner_files.aspx.cs" Inherits="owner_files_tenant_files" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asc" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asc:ScriptManager ID="ScriptManager1" runat="server">
</asc:ScriptManager>






<cc1:TabContainer  ID="TabContainer1"  runat="server" 
        BackColor="#CCCCFF"    >
   <cc1:TabPanel ID="tab1" runat="server"  HeaderText="My file uploads" >    
       <ContentTemplate>
       
       
       
    <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lbl_u_file_management %>" ></asp:Label>
      <br /><br /><table  cellpadding="3" cellspacing="0"><tr><td align="left" style="color: white; background-color: aliceblue; height: 24px;">
           <asp:Label ID="Label4" runat="server" Font-Bold="True"  
                    Text="<%$ Resources:Resource, lbl_folders %>" ForeColor="Black"></asp:Label>
           </td><td align="left" style="color: white; background-color: aliceblue; height: 24px;" valign="top"><asp:Label ID="Label5" runat="server" Font-Bold="True"  
                        Text="<%$ Resources:Resource, lbl_files %>" ForeColor="Black"></asp:Label>
         </td></tr><tr><td align="center" colspan="2" >
         <asp:Label ID="Label7" runat="server" BackColor="Yellow" EnableViewState="False"
            ForeColor="Red" Font-Bold="True" ></asp:Label>
        </td></tr><tr><td valign="top" >
        <asp:Panel ID="Panel1" runat="server" ScrollBars="Auto" >
            <asp:TreeView ID="TreeView1" runat="server"   Font-Bold="True"  
               OnSelectedNodeChanged="TreeView1_SelectedNodeChanged"  ForeColor="Black" ShowLines="True">
             <NodeStyle ChildNodesPadding="5px" HorizontalPadding="3px" />

             <SelectedNodeStyle BackColor="SteelBlue" ForeColor="White" />
            </asp:TreeView>
        </asp:Panel>
        </td><td valign="top">
  <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
       BorderColor="#CDCDCD"  BorderWidth="1px"   
       DataKeyNames="Id" OnRowCommand="GridView1_RowCommand"
       OnRowEditing="GridView1_RowEditing"  
       OnRowCancelingEdit="GridView1_RowCancelingEdit" 
      OnRowUpdating="GridView1_RowUpdating" OnRowDataBound="GridView1_RowDataBound"><Columns>
       <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_file_name %>">
         <ItemTemplate>
         <asp:Label ID="Label1" runat="server" Text='<%# Eval("FileName") %>'></asp:Label>
       </ItemTemplate>
       <EditItemTemplate>
       <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FileName") %>'></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1"
            Display="Dynamic" ErrorMessage="Please enter file name" Font-Bold="True">
         </asp:RequiredFieldValidator>
       </EditItemTemplate>
      </asp:TemplateField>
      <asp:BoundField DataField="FileSize" HeaderText="<%$ Resources:Resource, lbl_size_bytes %>" ReadOnly="True" />
      <asp:BoundField DataField="DateCreated" 
           HeaderText="<%$ Resources:Resource, lbl_created_on %>" ReadOnly="True" 
           DataFormatString="{0:MMM dd , yyyy}"  HtmlEncode="False"/>
      <asp:ButtonField ButtonType="Button" 
           CommandName="Download" Text="<%$ Resources:Resource, lbl_download %>" />
      <asp:TemplateField ShowHeader="False">
      <ItemTemplate>
      <asp:HiddenField ID="h_folder_id" runat="server" Value='<%# Bind("FolderId") %>' /><asp:HiddenField ID="h_file_id" runat="server" Value='<%# Bind("Id") %>'  /><asp:Button ID="Button1" runat="server" CausesValidation="False" CommandArgument='<%# Eval("Id") %>'
           CommandName="DeleteFile" OnClick="Button1_Click1" OnClientClick="return ConfirmDelete();"
           Text="<%$ Resources:Resource, lbl_delete %>" />
      </ItemTemplate>
      </asp:TemplateField>
      <asp:CommandField ButtonType="Button" 
       ShowEditButton="True" EditText="<%$ Resources:Resource, lbl_rename %>" 
       UpdateText="<%$ Resources:Resource, lbl_change %>" />
   </Columns>
  <AlternatingRowStyle   BackColor="White" />
  <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
  <PagerStyle  BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
  <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
</asp:GridView>


<br /><br /></td></tr>
    <tr><td colspan="2">
        <br /><br /><table cellpadding="5" style="background-color: whitesmoke">
            <tr><td colspan="3">
                <asp:Label ID="Label6" runat="server" Font-Bold="True" ForeColor="Red" 
                    Text="<%$ Resources:Resource, lbl_tasks %>"></asp:Label>
</td></tr><tr><td align="right" valign="top">
                <asp:Label ID="Label1" runat="server" Font-Bold="True" 
                    Text="new folder"></asp:Label>
</td><td align="left"><asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        ControlToValidate="TextBox1" Display="Dynamic" 
                        ErrorMessage="<%$ Resources:Resource, lbl_enter_folder_name %>" 
                        Font-Bold="True" ValidationGroup="createfolder"></asp:RequiredFieldValidator>
<br /><asp:CheckBox ID="CheckBox1" runat="server" Font-Bold="True" Font-Size="Small" 
                        Text="<%$ Resources:Resource, lbl_create_at_root_level %>"></asp:CheckBox>
</td><td align="left" valign="top"><asp:Button ID="Button1" runat="server" Font-Bold="True" 
                        OnClick="Button1_Click" Text="<%$ Resources:Resource, lbl_created_on %>" 
                        ValidationGroup="createfolder"></asp:Button>
</td></tr><tr><td align="right">
                <asp:Label ID="Label2" runat="server" Font-Bold="True" 
                    Text="rename folder"></asp:Label>
</td><td align="left"><asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ControlToValidate="TextBox2" Display="Dynamic" 
                        ErrorMessage="<%$ Resources:Resource, lbl_folder_new_name %>" Font-Bold="True" 
                        ValidationGroup="renamefolder"></asp:RequiredFieldValidator>
</td><td align="left"><asp:Button ID="Button2" runat="server" Font-Bold="True" 
                        OnClick="Button2_Click" Text="<%$ Resources:Resource, lbl_rename %>" 
                        ValidationGroup="renamefolder"></asp:Button>
</td></tr><tr><td align="right">
                <asp:Label ID="Label3" runat="server" Font-Bold="True" 
                    Text="<%$ Resources:Resource, lbl_delete_selected_folder %>"></asp:Label>
</td><td align="left"><asp:Label ID="Label10" runat="server" Font-Bold="True"></asp:Label>
</td><td align="left"><asp:Button ID="Button3" runat="server" Font-Bold="True" 
                        OnClick="Button3_Click" OnClientClick="return ConfirmDelete();" 
                        Text="<%$ Resources:Resource, lbl_delete %>"></asp:Button>
</td></tr><tr><td align="right">
                <asp:Label ID="Label9" runat="server" Font-Bold="True" 
                    Text="<%$ Resources:Resource, lbl_upload_file %>"></asp:Label>
</td><td align="left"><asp:FileUpload ID="FileUpload1" runat="server"></asp:FileUpload>
<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                        ControlToValidate="FileUpload1" Display="Dynamic" 
                        ErrorMessage="<%$ Resources:Resource, lbl_file_to_upload %>" Font-Bold="True" 
                        ValidationGroup="uploadfile"></asp:RequiredFieldValidator>
</td><td align="left"><asp:Button ID="Button4" runat="server" Font-Bold="True" 
                        OnClick="Button4_Click" Text="<%$ Resources:Resource, lbl_upload %>" 
                        ValidationGroup="uploadfile"></asp:Button>
</td></tr></table></td></tr><tr><td colspan="2"></td></tr></table><br />
<center>&#160;</center><br />
<br /><br />&#160;<br />

       
       
       

       
       
       
</ContentTemplate>
    
</cc1:TabPanel>
    
    
    
    
    
    
    
    
    
    
    
    <cc1:TabPanel ID="tab2" runat="server"  HeaderText="Property files" >
     <ContentTemplate  >
     
     <br /><table ><tr ><td  bgcolor="aliceblue"><B >VIEW FILE</B></td></tr><tr><td>Property &nbsp;&nbsp;&nbsp;
     <asp:DropDownList ID="ddl_home_owner_list" runat="server"  AutoPostBack="True"
                                   DataTextField="home_name" DataValueField="home_id" 
                       onselectedindexchanged="ddl_home_owner_list_SelectedIndexChanged" ></asp:DropDownList></td></tr></table><br />
     <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
          BorderColor="#CDCDCD"  BorderWidth="1px"
          DataKeyNames="ownerfiles_id" 
          OnRowCommand="GridView2_RowCommand" >
          <AlternatingRowStyle BackColor="White" />
          <Columns>
           <asp:BoundField DataField="home_name" HeaderText="Property" ReadOnly="True" />
          <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_file_name %>">
          <EditItemTemplate>
          <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FileName") %>'></asp:TextBox>
          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1"
               Display="Dynamic" ErrorMessage="Please enter file name" Font-Bold="True">
          </asp:RequiredFieldValidator>
          </EditItemTemplate>
           <ItemTemplate>
          <asp:Label ID="Label1" runat="server" Text='<%# Eval("FileName") %>'></asp:Label>
          </ItemTemplate>
          </asp:TemplateField>
            <asp:BoundField DataField="DateCreated" 
                 HeaderText="<%$ Resources:Resource, lbl_created_on %>" ReadOnly="True" 
                 DataFormatString="{0:MMM dd , yyyy}"  HtmlEncode="False"/>
          <asp:ButtonField ButtonType="Button" 
               CommandName="Download" Text="<%$ Resources:Resource, lbl_download %>" />
          </Columns>
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
           <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
           <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
           <RowStyle BackColor="#E3EAEB" />
           <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
           </asp:GridView><br /><br /><table ><tr ><td  bgcolor="aliceblue">
            <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label>
            </B></td></tr></table>
        
     
        
        
        
     
     
     
        
     
        
        
        
     
     
     
</ContentTemplate  >
    
</cc1:TabPanel>
     
   
     
     
     
     
     
     
     
     
     
     
     
     
     
    
  <cc1:TabPanel ID="tab3" runat="server"  HeaderText="Personnal lease files" >
    <ContentTemplate  >
    
    
    <asp:Label ID="lbl_tn_tenant_files" Text="<%$ Resources:Resource_Tenant, lbl_tn_tenant_files %>" runat="server"></asp:Label>

 <br />
 <br />
                    
  <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" 
       BorderColor="#CDCDCD"  BorderWidth="1px"
       DataKeyNames="tenantfiles_id" 
       OnRowCommand="GridView3_RowCommand"
       OnRowDataBound="GridView3_RowDataBound">
     <Columns>
     <asp:BoundField 
       DataField="home_name" 
       HeaderText="Property" ReadOnly="True" />
     <asp:TemplateField 
          HeaderText="<%$ Resources:Resource_Owner, lbl_file_name %>">
     
     <ItemTemplate>
     <asp:Label ID="Label1" runat="server" Text='<%# Eval("FileName") %>'></asp:Label>
     </ItemTemplate>
     </asp:TemplateField>
     <asp:BoundField 
       DataField="FileSize" 
       HeaderText="Size (bytes)" ReadOnly="True" />
     <asp:BoundField 
          DataField="DateCreated" 
          HeaderText="<%$ Resources:Resource_Owner, lbl_created_on %>" ReadOnly="True" 
          DataFormatString="{0:MMM dd , yyyy}"  
          HtmlEncode="False"/>
     <asp:ButtonField 
         ButtonType="Button"
        CommandName="Download" Text="<%$ Resources:Resource_Owner, lbl_download %>" />
     
     </Columns>
     <AlternatingRowStyle BackColor="White" />
     <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
     <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
     <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
     <RowStyle BackColor="#E3EAEB" />
     <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
     </asp:GridView><br /><br />

    
    
    
    
     

    
    
    
    
     
</ContentTemplate  >
  
</cc1:TabPanel>
     
     
     
    
    
    
    
    
    
    
     
     </cc1:TabContainer>
     









   
   
   

</asp:Content>

