﻿<%@ Page Language="C#" MasterPageFile="~/owner/owner_mp.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="financial_income.aspx.cs" Inherits="manager_Financial_financial_income" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:Label ID="Label2" runat="server" 
                    Text="<%$ Resources:Resource, lbl_other_income%>" /><br /><br />
<table>
<tr><td> <asp:Label ID="lbl_property" runat="server" Text="<%$ Resources:Resource, lbl_property %>"/>
                </td>
                <td>
       <asp:DropDownList ID="ddl_home_id" DataValueField="home_id" DataTextField="home_name"   runat="server" autopostback="true" OnSelectedIndexChanged="ddl_home_id_SelectedIndexChanged" />
       </td>  
</tr>
 </table><br />
 
 <table style="width: 100%">
        <tr>
            <td valign="top">
 <asp:Repeater runat="server" ID="rhome_view">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
        
        <tr>
          <td>
                 District&nbsp;:&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_district")%> 
                   </td>
            </tr>
           <tr>
              <td valign="top" >
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_no")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_street")%></td>
            </tr>
            <tr>
                 <td valign="top" >
                   <%#DataBinder.Eval(Container.DataItem, "home_city")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_prov")%></td>
               
            </tr>
            <tr>
                 <td valign="top" >
                   <%#DataBinder.Eval(Container.DataItem, "home_pc")%> 
                
                   </td>
               
            </tr>   
        </table>
        </ItemTemplate>
        </asp:Repeater>
            </td>
            <td valign="top">
                <table bgcolor="#ffffcc"  >
                    <tr>
                        <td>
                          <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, txt_month %>"></asp:Label>&nbsp;/&nbsp;<asp:Label ID="lbl_year" runat="server" Text="<%$ Resources:Resource, lbl_year %>"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_date_received_m" runat="server" 
                                AutoPostBack="true" onselectedindexchanged="ddl_date_received_m_SelectedIndexChanged" 
                                >
                                <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                            </asp:DropDownList>
                            &nbsp; / &nbsp;
                            <asp:DropDownList ID="ddl_date_received_y" runat="server" onselectedindexchanged="ddl_date_received_y_SelectedIndexChanged" 
                              AutoPostBack="true"   >
                                <asp:ListItem>2001</asp:ListItem>
                                <asp:ListItem>2002</asp:ListItem>
                                <asp:ListItem>2003</asp:ListItem>
                                <asp:ListItem>2004</asp:ListItem>
                                <asp:ListItem>2005</asp:ListItem>
                                <asp:ListItem>2006</asp:ListItem>
                                <asp:ListItem>2007</asp:ListItem>
                                <asp:ListItem>2008</asp:ListItem>
                                <asp:ListItem>2009</asp:ListItem>
                                <asp:ListItem>2010</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    </table>
               
                <br />
                <asp:Button ID="btn_view_graph" runat="server" onclick="btn_view_graph_Click" 
                    Text="View Graph" />
               
            </td>
        </tr>
    </table>
    <br />
     <br />

                <asp:Label ID="lbl_confirmation" runat="server" ForeColor="Red"></asp:Label>
                <br />
                <asp:Label ID="Label53" runat="server" style="font-weight: 700"></asp:Label>
&nbsp;&nbsp;<asp:Label ID="Label58" runat="server"></asp:Label>
    &nbsp;
                <asp:Label ID="Label55" runat="server" style="font-weight: 700"></asp:Label>
&nbsp;<asp:Label ID="Label56" runat="server"></asp:Label>
&nbsp;<asp:Label ID="lbl_amount" runat="server"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label57" runat="server" style="font-weight: 700"></asp:Label>
&nbsp;<asp:Label ID="Label7" runat="server"></asp:Label>
&nbsp;<asp:Label ID="lbl_date_paid" runat="server"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label59" runat="server" style="font-weight: 700"></asp:Label>
&nbsp;<asp:Label ID="Label60" runat="server"></asp:Label>
&nbsp;<asp:Label ID="lbl_income_reference" runat="server"></asp:Label>
&nbsp;<table style="width: 100%" >
           
        
        
        
        
        
        
        
        
        
        
   <tr>
   <td colspan="3">
   <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="100%" BorderColor="White" BorderWidth="3"  ID="gv_Income" runat="server" AutoGenerateColumns="false"
       AllowSorting="true"   EmptyDataText="" GridLines="Both"   
   BackColor="Beige">
        
     <Columns>
   <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_revenue%>"  >
    <ItemTemplate  >
   <asp:Label ID="income_categ"  Text='<%#GetIncomeCateg(Convert.ToInt32(Eval("incomecateg_id")))%>' runat="server" />
     
  </ItemTemplate  >
    </asp:TemplateField>
   
   
   <asp:BoundField HeaderText="<%$ Resources:Resource, txt_amount_received %>"    DataField="income_amount" DataFormatString="{0:0.00}"  />
   <asp:BoundField HeaderText="<%$ Resources:Resource, txt_date %>"    DataField="income_date_received"  DataFormatString="{0:M-dd-yyyy}"  />
   <asp:BoundField HeaderText="<%$ Resources:Resource, lbl_reference %>"   DataField="income_reference" />
   <asp:BoundField HeaderText="<%$ Resources:Resource, lbl_com %>"  DataField="income_comments" />
   
   <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_update %>"
     DataNavigateUrlFields="income_id" 
     DataNavigateUrlFormatString="financial_income_update.aspx?inc_id={0}" 
      HeaderText="<%$ Resources:Resource, lbl_update %>" />
      
      
      
       <asp:TemplateField >
    <ItemTemplate  >
   
    <asp:HiddenField ID="h_income_amount" Value='<%#Bind("income_amount","{0:0.00}")%>'  runat="server" />
    <asp:HiddenField ID="h_income_date_received" Value='<%#Bind("income_date_received","{0:M-dd-yyyy}")%>'  runat="server" />
    <asp:HiddenField ID="h_income_reference" Value='<%#Bind("income_reference")%>'  runat="server" />
    <asp:HiddenField ID="h_income_categ" Value='<%#GetIncomeCateg(Convert.ToInt32(Eval("incomecateg_id")))%>'  runat="server" />
    
    <asp:HiddenField ID="h_income_id" Value='<%#Bind("income_id")%>'  runat="server" />
   <asp:Button ID="btn_cancel" runat="server" OnClick="btn_cancel_Click"  Text="<%$ Resources:Resource, btn_cancel %>"/>
    </ItemTemplate  >
    </asp:TemplateField>
     
      </Columns>   
        
        
    </asp:GridView>

   </td>
   </tr>     
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
       <tr>
            <td valign="top" style="height: 18px; width: 121px;">
                &nbsp;</td>
            <td valign="top" style="height: 18px">
                &nbsp;</td>
            <td valign="top" style="height: 18px">
                &nbsp;</td>
        </tr>
        <tr>
            <td valign="top" style="height: 18px; width: 121px;">
                <asp:Label ID="Label52" runat="server" 
                    Text="<%$ Resources:Resource, lbl_total_expenses %>" 
                    style="font-weight: 700"/></td>
            <td valign="top"  >
                <asp:Label ID="lbl_total_month_income"  runat="server"></asp:Label>
            </td>
            <td valign="top"  >
                &nbsp;</td>
        </tr>
                                
    </table>


<br />

 <table style="width: 100%" >
        <tr>
            <td valign="top" bgcolor="AliceBlue">
                <asp:Label ID="Label5" runat="server" 
                    Text="<%$ Resources:Resource, lbl_revenue%>" style="font-weight: 700"/></td>
            <td valign="top" style="font-weight: 700" bgcolor="AliceBlue">
                <asp:Label ID="Label36" runat="server" 
                          Text="<%$ Resources:Resource, txt_amount_received %>" /></td>
            <td valign="top" bgcolor="AliceBlue">
                 <asp:Label ID="Label3" runat="server" 
                     Text="<%$ Resources:Resource, txt_date %>" style="font-weight: 700"/></td>
            <td valign="top" bgcolor="AliceBlue">
                 <asp:Label ID="Label6" runat="server" 
                    Text="<%$ Resources:Resource, lbl_reference%>" style="font-weight: 700"/></td>
            <td valign="top" bgcolor="AliceBlue">
                <asp:Label ID="Label4" runat="server" 
                    Text="<%$ Resources:Resource, lbl_com%>" style="font-weight: 700"/></td>
            <td valign="top" bgcolor="AliceBlue">
                 &nbsp;</td>
        </tr>
        <tr bgcolor="beige" >
            <td valign="top" style="height: 18px" >
                <asp:Label ID="Label8" runat="server" 
                    Text="<%$ Resources:Resource, lbl_garage %>" />&nbsp;</td>
            <td valign="top"  >
                <asp:TextBox ID="tbx_garage_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td valign="top"  >
                <asp:DropDownList ID="ddl_garage_date_received_m" runat="server" 
                    AutoPostBack="true" 
                    >
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                </asp:DropDownList>
                / &nbsp;<asp:DropDownList ID="ddl_garage_date_received_d" runat="server" 
                    >
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                </asp:DropDownList>
                &nbsp;&nbsp; / &nbsp;
                <asp:DropDownList ID="ddl_garage_date_received_y" runat="server" 
                    >
                    <asp:ListItem>2001</asp:ListItem>
                    <asp:ListItem>2002</asp:ListItem>
                    <asp:ListItem>2003</asp:ListItem>
                    <asp:ListItem>2004</asp:ListItem>
                    <asp:ListItem>2005</asp:ListItem>
                    <asp:ListItem>2006</asp:ListItem>
                    <asp:ListItem>2007</asp:ListItem>
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td valign="top" >
                <asp:TextBox ID="tbx_ref_garage" runat="server" Width="85px"></asp:TextBox>
            </td>
            <td valign="top" >
                <asp:TextBox ID="tbx_com_garage" runat="server" Height="50px" TextMode="MultiLine" 
                    Width="131px"></asp:TextBox>
            </td>
            <td valign="top" >
                <asp:Button ID="btn_garage" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" 
                    onclick="btn_garage_Click"  />
            </td>
        </tr>
        <tr>
            <td valign="top" style="height: 18px">
                <asp:Label ID="Label9" runat="server" 
                    Text="<%$ Resources:Resource, lbl_parking %>" />
            </td>
            <td valign="top" >
                <asp:TextBox ID="tbx_parking_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td valign="top" >
                <asp:DropDownList ID="ddl_parking_date_received_m" runat="server" 
                    AutoPostBack="true" 
                    >
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                </asp:DropDownList>
                / &nbsp;
                <asp:DropDownList ID="ddl_parking_date_received_d" runat="server" 
                    >
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                </asp:DropDownList>
                &nbsp; / &nbsp;
                <asp:DropDownList ID="ddl_parking_date_received_y" runat="server" 
                    >
                    <asp:ListItem>2001</asp:ListItem>
                    <asp:ListItem>2002</asp:ListItem>
                    <asp:ListItem>2003</asp:ListItem>
                    <asp:ListItem>2004</asp:ListItem>
                    <asp:ListItem>2005</asp:ListItem>
                    <asp:ListItem>2006</asp:ListItem>
                    <asp:ListItem>2007</asp:ListItem>
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td valign="top" >
                <asp:TextBox ID="tbx_ref_parking" runat="server" Width="85px"></asp:TextBox>
            </td>
            <td valign="top" >
                <asp:TextBox ID="tbx_com_parking" runat="server" Height="50px" TextMode="MultiLine" 
                    Width="130px"></asp:TextBox>
            </td>
            <td valign="top" >
                <asp:Button ID="btn_parking" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" 
                    onclick="btn_parking_Click" />
            </td>
        </tr>
        <tr bgcolor="beige">
            <td valign="top" style="height: 18px">
                <asp:Label ID="Label10" runat="server" 
                    Text="<%$ Resources:Resource, lbl_laundry %>" /></td>
            <td valign="top" >
                <asp:TextBox ID="tbx_laundry_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td valign="top" >
                <asp:DropDownList ID="ddl_laundry_date_received_m" runat="server" 
                    AutoPostBack="true" 
                    >
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                </asp:DropDownList>
                / &nbsp;
                <asp:DropDownList ID="ddl_laundry_date_received_d" runat="server" 
                    >
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                </asp:DropDownList>
                &nbsp; / &nbsp;
                <asp:DropDownList ID="ddl_laundry_date_received_y" runat="server" 
                    >
                    <asp:ListItem>2001</asp:ListItem>
                    <asp:ListItem>2002</asp:ListItem>
                    <asp:ListItem>2003</asp:ListItem>
                    <asp:ListItem>2004</asp:ListItem>
                    <asp:ListItem>2005</asp:ListItem>
                    <asp:ListItem>2006</asp:ListItem>
                    <asp:ListItem>2007</asp:ListItem>
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td valign="top" >
                <asp:TextBox ID="tbx_ref_laundry" runat="server" Width="85px"></asp:TextBox>
            </td>
            <td valign="top" >
                <asp:TextBox ID="tbx_com_laundry" runat="server" Height="50px" TextMode="MultiLine" 
                    Width="130px"></asp:TextBox>
            </td>
            <td valign="top" >
                <asp:Button ID="btn_laundry" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" onclick="btn_laundry_Click" />
            </td>
        </tr>
        <tr>
            <td valign="top" style="height: 18px">
                <asp:Label ID="Label11" runat="server" 
                    Text="<%$ Resources:Resource, lbl_storage %>" /></td>
            <td valign="top" >
                <asp:TextBox ID="tbx_storage_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td valign="top" >
                <asp:DropDownList ID="ddl_storage_date_received_m" runat="server" 
                    AutoPostBack="true" 
                    >
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                </asp:DropDownList>
                / &nbsp;
                <asp:DropDownList ID="ddl_storage_date_received_d" runat="server" 
                    >
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                </asp:DropDownList>
                &nbsp; / &nbsp;
                <asp:DropDownList ID="ddl_storage_date_received_y" runat="server" 
                    >
                    <asp:ListItem>2001</asp:ListItem>
                    <asp:ListItem>2002</asp:ListItem>
                    <asp:ListItem>2003</asp:ListItem>
                    <asp:ListItem>2004</asp:ListItem>
                    <asp:ListItem>2005</asp:ListItem>
                    <asp:ListItem>2006</asp:ListItem>
                    <asp:ListItem>2007</asp:ListItem>
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td valign="top" >
                <asp:TextBox ID="tbx_ref_storage" runat="server" Width="85px"></asp:TextBox>
            </td>
            <td valign="top" >
                <asp:TextBox ID="tbx_com_storage" runat="server" Height="50px" TextMode="MultiLine" 
                    Width="130px"></asp:TextBox>
            </td>
            <td valign="top" >
                <asp:Button ID="btn_storage" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" 
                    onclick="btn_storage_Click"  />
            </td>
        </tr>
        <tr bgcolor="beige">
            <td valign="top" style="height: 18px">
                <asp:Label ID="Label12" runat="server" 
                    Text="<%$ Resources:Resource, lbl_vending_machine %>"/> </td>
            <td valign="top" >
                <asp:TextBox ID="tbx_vending_machine_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td valign="top" >
                <asp:DropDownList ID="ddl_vending_machine_date_received_m" runat="server" 
                    AutoPostBack="true" 
                    >
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                </asp:DropDownList>
                / &nbsp;
                <asp:DropDownList ID="ddl_vending_machine_date_received_d" runat="server" 
                    >
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                </asp:DropDownList>
                &nbsp; / &nbsp;
                <asp:DropDownList ID="ddl_vending_machine_date_received_y" runat="server" 
                    >
                    <asp:ListItem>2001</asp:ListItem>
                    <asp:ListItem>2002</asp:ListItem>
                    <asp:ListItem>2003</asp:ListItem>
                    <asp:ListItem>2004</asp:ListItem>
                    <asp:ListItem>2005</asp:ListItem>
                    <asp:ListItem>2006</asp:ListItem>
                    <asp:ListItem>2007</asp:ListItem>
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td valign="top" >
                <asp:TextBox ID="tbx_ref_vending_machine" runat="server" Width="85px"></asp:TextBox>
            </td>
            <td valign="top" >
                <asp:TextBox ID="tbx_com_vending_machine" runat="server" Height="50px" TextMode="MultiLine" 
                    Width="130px"></asp:TextBox>
            </td>
            <td valign="top" >
               <asp:Button ID="btn_vending_machine" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" 
                    onclick="btn_vending_machine_Click"  />
            </td>
        </tr>
        <tr>
            <td valign="top" style="height: 18px">
                <asp:Label ID="Label13" runat="server" 
                    Text="<%$ Resources:Resource, lbl_cash_machine %>" /></td>
            <td valign="top" >
                <asp:TextBox ID="tbx_cash_machine_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td valign="top" >
                <asp:DropDownList ID="ddl_cash_machine_date_received_m" runat="server" 
                    AutoPostBack="true" 
                    >
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                </asp:DropDownList>
                / &nbsp;
                <asp:DropDownList ID="ddl_cash_machine_date_received_d" runat="server" 
                    >
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                </asp:DropDownList>
                &nbsp; / &nbsp;
                <asp:DropDownList ID="ddl_cash_machine_date_received_y" runat="server" 
                    >
                    <asp:ListItem>2001</asp:ListItem>
                    <asp:ListItem>2002</asp:ListItem>
                    <asp:ListItem>2003</asp:ListItem>
                    <asp:ListItem>2004</asp:ListItem>
                    <asp:ListItem>2005</asp:ListItem>
                    <asp:ListItem>2006</asp:ListItem>
                    <asp:ListItem>2007</asp:ListItem>
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td valign="top" >
                <asp:TextBox ID="tbx_ref_cash_machine" runat="server" Width="85px"></asp:TextBox>
            </td>
            <td valign="top" >
                <asp:TextBox ID="tbx_com_cash_machine" runat="server" Height="50px" TextMode="MultiLine" 
                    Width="130px"></asp:TextBox>
            </td>
            <td valign="top" >
                <asp:Button ID="btn_cash_machine" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" 
                    onclick="btn_cash_machine_Click"  />
            </td>
        </tr>
    
        <tr>
            <td valign="top" style="height: 18px" bgcolor="Beige">
                <asp:Label ID="Label26" runat="server" 
                    Text="<%$ Resources:Resource, lbl_other %>"/></td>
            <td valign="top" bgcolor="Beige" >
                <asp:TextBox ID="tbx_other_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td valign="top" bgcolor="Beige" >
                <asp:DropDownList ID="ddl_other_date_received_m" runat="server" 
                    AutoPostBack="true" 
                    >
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                </asp:DropDownList>
                / &nbsp;
                <asp:DropDownList ID="ddl_other_date_received_d" runat="server" 
                    >
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                </asp:DropDownList>
                &nbsp; / &nbsp;
                <asp:DropDownList ID="ddl_other_date_received_y" runat="server" 
                    >
                    <asp:ListItem>2001</asp:ListItem>
                    <asp:ListItem>2002</asp:ListItem>
                    <asp:ListItem>2003</asp:ListItem>
                    <asp:ListItem>2004</asp:ListItem>
                    <asp:ListItem>2005</asp:ListItem>
                    <asp:ListItem>2006</asp:ListItem>
                    <asp:ListItem>2007</asp:ListItem>
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td valign="top" bgcolor="Beige" >
                <asp:TextBox ID="tbx_ref_other" runat="server" Width="85px"></asp:TextBox>
            </td>
            <td valign="top" bgcolor="Beige" >
                <asp:TextBox ID="tbx_com_other" runat="server" Height="50px" TextMode="MultiLine" 
                    Width="130px"></asp:TextBox>
            </td>
            <td valign="top" bgcolor="Beige" >
               <asp:Button ID="btn_other" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit %>" onclick="btn_other_Click"  />
            </td>
        </tr>
                                
    </table>

<table cellpadding="0" cellspacing="0" style="width: 100%">
        <tr>
            <td align="right" bgcolor="#879EAA">
                <asp:Button ID="btn_submit_all" runat="server" 
                    Text="<%$ Resources:Resource, btn_submit_all %>" ForeColor="#0066FF" 
                    onclick="btn_submit_all_Click"  />
                </td>
        </tr>
    </table>

</asp:Content>

