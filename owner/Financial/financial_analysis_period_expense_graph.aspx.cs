﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.OwnerObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : april 21 , 2008
/// </summary>
/// 
public partial class manager_Financial_financial_analysis_period_expense_graph : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // dotnetCHARTING.DataEngine de = new dotnetCHARTING.DataEngine();

        if (!(Page.IsPostBack))
        {


            DateTime to = new DateTime();
            DateTime from = new DateTime();
            to = DateTime.Now; // the date in the to drop downlist

            tiger.Date d = new tiger.Date();
            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            to = Convert.ToDateTime(d.DateCulture(to.Month.ToString(), to.Day.ToString(), to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
            from = Convert.ToDateTime(d.DateCulture("1", "1", to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


            tiger.Date c = new tiger.Date();
            //if we are entering from the update page
            //-----------------------------------------------------
            if (Request.QueryString["h_id"] != "" && Request.QueryString["h_id"] != null && Request.QueryString["m"] != "" && Request.QueryString["m"] != null && Request.QueryString["y"] != "" && Request.QueryString["y"] != null)
            {
                OwnerObjectAuthorization homeAuthorization = new OwnerObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), Convert.ToInt32(Request.QueryString["h_id"])))
                {
                    Session.Abandon();
                    Response.Redirect("http://www.sinfocatiger.com/login.aspx");
                }
                ///////// SECURITY OBJECT CHECK  END ////////////////////////////////

                
                from = Convert.ToDateTime(c.DateCulture(Request.QueryString["m"], Request.QueryString["d"], Request.QueryString["y"], Convert.ToString(Session["_lastCulture"])));

            }
            //-----------------------------------------------------


            ddl_from_m.SelectedValue = from.Month.ToString();
            ddl_from_d.SelectedValue = from.Day.ToString();
            ddl_from_y.SelectedValue = from.Year.ToString();

            ddl_to_m.SelectedValue = to.Month.ToString();
            ddl_to_d.SelectedValue = to.Day.ToString();
            ddl_to_y.SelectedValue = to.Year.ToString();


            tiger.Owner o = new tiger.Owner(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = o.getOwnerHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
            string link_to_unit = "";
            if (home_count > 0)
            {
                int home_id = o.getOwnerHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

                //if we are entering from the update page
                //-----------------------------------------------------
                if (Request.QueryString["h_id"] != "" && Request.QueryString["h_id"] != null)
                {
                    home_id = Convert.ToInt32(Request.QueryString["h_id"]);


                    OwnerObjectAuthorization homeAuthorization = new OwnerObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                    if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), home_id))
                    {
                        Session.Abandon();
                        Response.Redirect("http://www.sinfocatiger.com/login.aspx");
                    }
                    ///////// SECURITY OBJECT CHECK  END ////////////////////////////////

                }
                //-----------------------------------------------------


                ddl_home_id.DataSource = o.getOwnerHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                ddl_home_id.SelectedValue = Convert.ToString(home_id);
                ddl_home_id.DataBind();



                //To view the address of the property

                tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
                rhome_view.DataBind();

                
                //set global properties

                /////////////////////////////////////////////////////////////////////////////////////////////
                Chart1.DefaultSeries.ConnectionString = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

                Chart1.Title = Resources.Resource.lbl_u_expense;
                Chart1.XAxis.Label.Text = Resources.Resource.lbl_expense_category;
                Chart1.YAxis.Label.Text = Resources.Resource.lbl_amount;
                Chart1.YAxis.FormatString = "{0:0.00}";
                Chart1.Type = dotnetCHARTING.ChartType.Pies;
                // Set default transparency
                Chart1.DefaultSeries.DefaultElement.Transparency = 20;

                // Set elements to explode
                Chart1.DefaultSeries.DefaultElement.ExplodeSlice = true;



                // Set labeling mode.
                Chart1.PieLabelMode = dotnetCHARTING.PieLabelMode.Outside;
                // Set show value
                // Chart1.DefaultSeries.DefaultElement.ShowValue = true;



                Chart1.Use3D = true;
                Chart1.TempDirectory = "temp";
                Chart1.Debug = true;
                //   Chart1.DateGrouping = TimeInterval.Day;
                Chart1.EmptyElementText = "N/A";
                // Set he chart size.
                Chart1.Width = 660;
                Chart1.Height = 412;

                Chart1.LegendBox.HeaderLabel.Text = Resources.Resource.lbl_category;

                //Add a series
                //  Chart1.Series.Name = GetExpenseCateg(Convert.ToInt32("<%YValue>"));
                Chart1.DefaultSeries.PaletteName = dotnetCHARTING.Palette.One;


                // relativeFreqSeries.DefaultElement.LabelTemplate = "%name\n%yPercentOfTotal";
                // Chart1.DefaultElement.LabelTemplate = "%name\n%yPercentOfTotal";

                Chart1.Series.StoredProcedure = "prExpensePeriodViewGroupByCategChart";
                //Chart1.Type = dotnetCHARTING.ChartType.Pie;
                //Set store procedure parameters, for third parameter other types are: Text,Date,Number,Currency,Double
                Chart1.Series.AddParameter("@schema_id", Convert.ToString(Session["schema_id"]), dotnetCHARTING.FieldType.Number);
                Chart1.Series.AddParameter("@home_id", home_id.ToString(), dotnetCHARTING.FieldType.Number);
                Chart1.Series.AddParameter("@lastCulture", Convert.ToString(Session["_lastCulture"]), dotnetCHARTING.FieldType.Text);
                Chart1.Series.AddParameter("@from", from.ToString(), dotnetCHARTING.FieldType.Date);
                Chart1.Series.AddParameter("@to", to.ToString(), dotnetCHARTING.FieldType.Date);

                // Dispalying the percentage
                Chart1.Series.DefaultElement.LabelTemplate = "%name\n%yPercentOfTotal";
                Chart1.Series.DefaultElement.ShowValue = true;

                Chart1.SeriesCollection.Add();

                Chart2.Visible = false;
                // by default we display the 3D pie chart
                ddl_dimension.SelectedValue = "3";
                //////////////////////////////////////////////////////////////////////////////////////////////
               
            }
            // if ther is no home

            else
            {
                //  txt_message.InnerHtml = "There is no property -- Add a property";

                //  txt_link.InnerHtml = home_count + " <a href='home_add.aspx'>Add Home</a>&nbsp;&nbsp;<a href='owner_add.aspx'>Add Owner</a>&nbsp;&nbsp;<a href='storage_add.aspx'>Add storage</a>&nbsp;&nbsp;";
            }



        }


        //Chart1.SeriesCollection.Add(getLiveData());

    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    dotnetCHARTING.SeriesCollection getLiveData()
    {
        dotnetCHARTING.DataEngine de = new dotnetCHARTING.DataEngine(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        //de.Series.Name = "Items";

        de.StoredProcedure = "prExpensePeriodViewGroupByCategChart";
        //Chart1.Type = dotnetCHARTING.ChartType.Pie;
        //Set store procedure parameters, for third parameter other types are: Text,Date,Number,Currency,Double
        de.AddParameter("@schema_id", "1", dotnetCHARTING.FieldType.Number);
        de.AddParameter("@home_id", "1", dotnetCHARTING.FieldType.Number);
        de.AddParameter("@lastCulture", Convert.ToString(Session["_lastCulture"]), dotnetCHARTING.FieldType.Text);
        de.AddParameter("@from", "4/10/08 12:00:00 AM", dotnetCHARTING.FieldType.Date);



        return de.GetSeries();
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        //To view the address of the property

        tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
        rhome_view.DataBind();


        // to view the expense in the  home for the date
        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        DateTime to = new DateTime();
        DateTime from = new DateTime();
        to = DateTime.Now; // the date in the to drop downlist

        tiger.Date d = new tiger.Date();
        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        to = Convert.ToDateTime(d.DateCulture(to.Month.ToString(), to.Day.ToString(), to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
        from = Convert.ToDateTime(d.DateCulture("1", "1", to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));



        ddl_from_m.SelectedValue = from.Month.ToString();
        ddl_from_d.SelectedValue = from.Day.ToString();
        ddl_from_y.SelectedValue = from.Year.ToString();

        ddl_to_m.SelectedValue = to.Month.ToString();
        ddl_to_d.SelectedValue = to.Day.ToString();
        ddl_to_y.SelectedValue = to.Year.ToString();

        //set global properties
       
        Chart1.DefaultSeries.ConnectionString = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        Chart1.Title = Resources.Resource.lbl_u_expense;
        Chart1.XAxis.Label.Text = Resources.Resource.lbl_expense_category;
        Chart1.YAxis.Label.Text = Resources.Resource.lbl_amount;
        Chart1.YAxis.FormatString = "{0:0.00}";
        Chart1.Type = dotnetCHARTING.ChartType.Pies;
        // Set default transparency
        Chart1.DefaultSeries.DefaultElement.Transparency = 20;

        // Set elements to explode
        Chart1.DefaultSeries.DefaultElement.ExplodeSlice = true;

        // Set show value
        // Chart1.DefaultSeries.DefaultElement.ShowValue = true;

        // Set labeling mode.

        Chart1.PieLabelMode = dotnetCHARTING.PieLabelMode.Outside;
        Chart1.Use3D = true;
        Chart1.TempDirectory = "temp";
        Chart1.Debug = true;
        //   Chart1.DateGrouping = TimeInterval.Day;
        Chart1.EmptyElementText = "N/A";
        // Set he chart size.
        Chart1.Width = 660;
        Chart1.Height = 412;

        Chart1.LegendBox.HeaderLabel.Text = Resources.Resource.lbl_category;

        //Add a series
        Chart1.Series.Name = "Items";
        Chart1.DefaultSeries.PaletteName = dotnetCHARTING.Palette.One;
        Chart1.Series.StoredProcedure = "prExpensePeriodViewGroupByCategChart";
        //Chart1.Type = dotnetCHARTING.ChartType.Pie;
        //Set store procedure parameters, for third parameter other types are: Text,Date,Number,Currency,Double
        Chart1.Series.AddParameter("@schema_id", Convert.ToString(Session["schema_id"]), dotnetCHARTING.FieldType.Number);
        Chart1.Series.AddParameter("@home_id", ddl_home_id.SelectedValue, dotnetCHARTING.FieldType.Number);
        Chart1.Series.AddParameter("@lastCulture", Convert.ToString(Session["_lastCulture"]), dotnetCHARTING.FieldType.Text);
        Chart1.Series.AddParameter("@from", from.ToString(), dotnetCHARTING.FieldType.Date);
        Chart1.Series.AddParameter("@to", to.ToString(), dotnetCHARTING.FieldType.Date);

        Chart1.Series.DefaultElement.LabelTemplate = "%name\n%yPercentOfTotal";
        Chart1.Series.DefaultElement.ShowValue = true;

        Chart1.SeriesCollection.Add();


        ddl_from_m.SelectedValue = from.Month.ToString();
        ddl_from_y.SelectedValue = from.Year.ToString();

        // we only display the 3D pie chart by default when we change property
        Chart2.Visible = false;

        ddl_dimension.SelectedValue = "3";
        ddl_chartype.SelectedValue = "1";

    }
  
   
    protected void btn_view_graph_Click(object sender, EventArgs e)
    {
        Response.Redirect("financial_analysis_period_income_graph.aspx?h_id=" + ddl_home_id.SelectedValue + "&m=" + ddl_from_m.SelectedValue
                          + "&d=" + ddl_from_d.SelectedValue + "&y=" + ddl_from_y.SelectedValue
                          + "&m2=" + ddl_to_m.SelectedValue
                          + "&d2=" + ddl_to_d.SelectedValue + "&y2=" + ddl_to_y.SelectedValue);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_view_Click(object sender, EventArgs e)
    {


        // to view the expense in the  home for the date
        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        DateTime to = new DateTime();
        DateTime from = new DateTime();
        

        tiger.Date d = new tiger.Date();
        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        ddl_from_m.SelectedValue = from.Month.ToString();
        ddl_from_y.SelectedValue = from.Year.ToString();

        //set global properties
        if (ddl_chartype.SelectedValue == "1" || ddl_chartype.SelectedValue == "2")
        {
            //set global properties
            Chart1.DefaultSeries.ConnectionString = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            Chart1.Title = Resources.Resource.lbl_u_expense;
            Chart1.XAxis.Label.Text = Resources.Resource.lbl_expense_category;
            Chart1.YAxis.Label.Text = Resources.Resource.lbl_amount;
            Chart1.YAxis.FormatString = "{0:0.00}";

            if (ddl_chartype.SelectedValue == "1")
            {
                // IF WE HAVE A PIE CHART   
                Chart1.Type = dotnetCHARTING.ChartType.Pies;
                // Set elements to explode
                Chart1.DefaultSeries.DefaultElement.ExplodeSlice = true;
                // Set show value
                // Set labeling mode.
                Chart1.PieLabelMode = dotnetCHARTING.PieLabelMode.Outside;
            }


            // Set default transparency
            Chart1.DefaultSeries.DefaultElement.Transparency = 20;

            // set the dimension of the chart
            if (ddl_dimension.SelectedValue == "3")
                Chart1.Use3D = true;


            Chart1.TempDirectory = "temp";
            Chart1.Debug = true;
            //   Chart1.DateGrouping = TimeInterval.Day;
            Chart1.EmptyElementText = "N/A";
            // Set he chart size.
            Chart1.Width = 660;
            Chart1.Height = 412;

            Chart1.LegendBox.HeaderLabel.Text = Resources.Resource.lbl_category;

            Chart1.DefaultElement.SmartLabel.Text = "<sqrt(%YValue)+10>";
            //Add a series
            Chart1.Series.Name = "Items";
            Chart1.DefaultSeries.PaletteName = dotnetCHARTING.Palette.One;
            Chart1.Series.StoredProcedure = "prExpensePeriodViewGroupByCategChart";
            //Chart1.Type = dotnetCHARTING.ChartType.Pie;
            //Set store procedure parameters, for third parameter other types are: Text,Date,Number,Currency,Double
            Chart1.Series.AddParameter("@schema_id", Convert.ToString(Session["schema_id"]), dotnetCHARTING.FieldType.Number);
            Chart1.Series.AddParameter("@home_id", ddl_home_id.SelectedValue, dotnetCHARTING.FieldType.Number);
            Chart1.Series.AddParameter("@lastCulture", Convert.ToString(Session["_lastCulture"]), dotnetCHARTING.FieldType.Text);
            Chart1.Series.AddParameter("@from", from.ToString(), dotnetCHARTING.FieldType.Date);
            Chart1.Series.AddParameter("@to", to.ToString(), dotnetCHARTING.FieldType.Date);

            Chart1.Series.DefaultElement.LabelTemplate = "%name\n%yPercentOfTotal";
            Chart1.Series.DefaultElement.ShowValue = true;

            Chart1.SeriesCollection.Add();

            ddl_from_m.SelectedValue = from.Month.ToString();
            ddl_from_y.SelectedValue = from.Year.ToString();

            Chart2.Visible = false;

        }
        //IF WE WANT TO DISPLAY THE LINE CHART OVER CERTAIN TIME PERIOD
        if (ddl_chartype.SelectedValue == "3")
        {
            Chart1.DefaultSeries.ConnectionString = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            Chart1.Title = Resources.Resource.lbl_u_expense;
            Chart1.XAxis.Label.Text = Resources.Resource.lbl_month_year;
            Chart1.YAxis.Label.Text = Resources.Resource.lbl_amount;
            Chart1.YAxis.FormatString = "{0:0.00}";

            Chart1.DefaultSeries.Type = dotnetCHARTING.SeriesType.Line;

            if (ddl_dimension.SelectedValue == "3")
                Chart1.Use3D = true;

            Chart1.TempDirectory = "temp";
            Chart1.Debug = true;
            //   Chart1.DateGrouping = TimeInterval.Day;
            Chart1.EmptyElementText = "N/A";
            // Set he chart size.
            Chart1.Width = 660;
            Chart1.Height = 412;

            Chart1.LegendBox.HeaderLabel.Text = Resources.Resource.lbl_category;
            //Add a series

            // Chart3.DefaultSeries.PaletteName = dotnetCHARTING.Palette.One;

            Chart1.Series.StoredProcedure = "prExpensePeriodViewGroupByCategLineChart";
            //Chart1.Type = dotnetCHARTING.ChartType.Pie;
            //Set store procedure parameters, for third parameter other types are: Text,Date,Number,Currency,Double
            Chart1.Series.AddParameter("@schema_id", Convert.ToString(Session["schema_id"]), dotnetCHARTING.FieldType.Number);
            Chart1.Series.AddParameter("@home_id", ddl_home_id.SelectedValue, dotnetCHARTING.FieldType.Number);
            Chart1.Series.AddParameter("@lastCulture", Convert.ToString(Session["_lastCulture"]), dotnetCHARTING.FieldType.Text);
            Chart1.Series.AddParameter("@from", from.ToString(), dotnetCHARTING.FieldType.Date);
            Chart1.Series.AddParameter("@to", to.ToString(), dotnetCHARTING.FieldType.Date);

            Chart1.SeriesCollection.Add();
            //////////////////////////////////////////////////////////////////////////////////////////////
            

        }
      

       
        
    }
}
