﻿<%@ Page Language="C#" MasterPageFile="~/owner/owner_mp.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="financial_scenario.aspx.cs" Inherits="manager_Financial_financial_scenario" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:Label ID="Label37" runat="server" 
                    Text="<%$ Resources:Resource, lbl_financial_analyses %>"/><br /><br />
                    
<table>
<tr><td> <asp:Label ID="lbl_property" runat="server" Text="<%$ Resources:Resource, lbl_property %>"/>
                </td>
                <td>
       <asp:DropDownList ID="ddl_home_id" DataValueField="home_id" DataTextField="home_name"   runat="server" autopostback="true" OnSelectedIndexChanged="ddl_home_id_SelectedIndexChanged" />
       </td>  
</tr>
 </table><br />
    <table style="width: 100%">
        <tr>
            <td valign="top">
<asp:Repeater runat="server" ID="rhome_view">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
        
        <tr>
          <td>
                 District&nbsp;:&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_district")%> 
                   </td>
            </tr>
           <tr>
              <td >
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_no")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_street")%></td>
            </tr>
            <tr>
                 <td >
                   <%#DataBinder.Eval(Container.DataItem, "home_city")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_prov")%></td>
               
            </tr>
            <tr>
                 <td >
                   <%#DataBinder.Eval(Container.DataItem, "home_pc")%> 
                
                   </td>
               
            </tr>   
        </table>
        </ItemTemplate>
        </asp:Repeater> 
            </td>
            <td valign="top">
                <table bgcolor="#ffffcc"  >
                    <tr>
                        <td>
                <asp:Label ID="lbl_scenario_name" runat="server" Text="<%$ Resources:Resource, lbl_scenario_name %>"></asp:Label>
                        </td>
                        <td>
                <asp:TextBox ID="tbx_name" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbl_year" runat="server" Text="<%$ Resources:Resource, lbl_year %>"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddl_year" runat="server">
                                <asp:ListItem>1950</asp:ListItem>
                                <asp:ListItem>1951</asp:ListItem>
                                <asp:ListItem>1952</asp:ListItem>
                                <asp:ListItem>1953</asp:ListItem>
                                <asp:ListItem>1954</asp:ListItem>
                                <asp:ListItem>1955</asp:ListItem>
                                <asp:ListItem>1956</asp:ListItem>
                                <asp:ListItem>1957</asp:ListItem>
                                <asp:ListItem>1958</asp:ListItem>
                                <asp:ListItem>1959</asp:ListItem>
                                <asp:ListItem>1960</asp:ListItem>
                                <asp:ListItem>1961</asp:ListItem>
                                <asp:ListItem>1962</asp:ListItem>
                                <asp:ListItem>1963</asp:ListItem>
                                <asp:ListItem>1964</asp:ListItem>
                                <asp:ListItem>1965</asp:ListItem>
                                <asp:ListItem>1966</asp:ListItem>
                                <asp:ListItem>1967</asp:ListItem>
                                <asp:ListItem>1968</asp:ListItem>
                                <asp:ListItem>1969</asp:ListItem>
                                <asp:ListItem>1970</asp:ListItem>
                                <asp:ListItem>1971</asp:ListItem>
                                <asp:ListItem>1972</asp:ListItem>
                                <asp:ListItem>1973</asp:ListItem>
                                <asp:ListItem>1974</asp:ListItem>
                                <asp:ListItem>1975</asp:ListItem>
                                <asp:ListItem>1976</asp:ListItem>
                                <asp:ListItem>1977</asp:ListItem>
                                <asp:ListItem>1978</asp:ListItem>
                                <asp:ListItem>1979</asp:ListItem>
                                <asp:ListItem>1980</asp:ListItem>
                                <asp:ListItem>1981</asp:ListItem>
                                <asp:ListItem>1982</asp:ListItem>
                                <asp:ListItem>1983</asp:ListItem>
                                <asp:ListItem>1984</asp:ListItem>
                                <asp:ListItem>1985</asp:ListItem>
                                <asp:ListItem>1986</asp:ListItem>
                                <asp:ListItem>1987</asp:ListItem>
                                <asp:ListItem>1988</asp:ListItem>
                                <asp:ListItem>1989</asp:ListItem>
                                <asp:ListItem>1990</asp:ListItem>
                                <asp:ListItem>1991</asp:ListItem>
                                <asp:ListItem>1992</asp:ListItem>
                                <asp:ListItem>1993</asp:ListItem>
                                <asp:ListItem>1994</asp:ListItem>
                                <asp:ListItem>1995</asp:ListItem>
                                <asp:ListItem>1996</asp:ListItem>
                                <asp:ListItem>1997</asp:ListItem>
                                <asp:ListItem>1998</asp:ListItem>
                                <asp:ListItem>1999</asp:ListItem>
                                <asp:ListItem>2001</asp:ListItem>
                                <asp:ListItem>2002</asp:ListItem>
                                <asp:ListItem>2003</asp:ListItem>
                                <asp:ListItem>2004</asp:ListItem>
                                <asp:ListItem>2005</asp:ListItem>
                                <asp:ListItem>2006</asp:ListItem>
                                <asp:ListItem>2007</asp:ListItem>
                                <asp:ListItem>2008</asp:ListItem>
                                <asp:ListItem>2009</asp:ListItem>
                                <asp:ListItem>2010</asp:ListItem>
                                <asp:ListItem>2011</asp:ListItem>
                                <asp:ListItem>2012</asp:ListItem>
                                <asp:ListItem>2013</asp:ListItem>
                                <asp:ListItem>2014</asp:ListItem>
                                <asp:ListItem>2015</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />
                <asp:Button ID="btn_save" runat="server"  
                    Text="<%$ Resources:Resource, btn_save %>" ForeColor="#0066FF" 
                                onclick="btn_save_Click" />
                        </td>
                        <td>
                            &nbsp;</td>
                    </tr>
                </table>
               
            </td>
        </tr>
    </table>
    <br />
    <table >
        <tr>
            <td>
                <asp:Label ID="lbl_property_value" runat="server" Text="<%$ Resources:Resource, lbl_property_value %>" ></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tbx_home_value" runat="server" Width="65px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_added_value" Text="<%$ Resources:Resource, lbl_added_value2 %>"  runat="server"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tbx_added_value" runat="server" Width="65px"></asp:TextBox>
            </td>
        </tr>
    </table>

    <table style="width: 100%">
        <tr>
            <td bgcolor="AliceBlue">
                <asp:Label ID="lbl_revenue" runat="server" 
                    Text="<%$ Resources:Resource, lbl_revenue %>" style="font-weight: 700"/> </td>
            <td bgcolor="AliceBlue">
                &nbsp;</td>
            <td bgcolor="AliceBlue">
                <asp:Label ID="lbl_monthly" runat="server" 
                    Text="<%$ Resources:Resource, lbl_monthly %>" style="font-weight: 700"/></td>
            <td bgcolor="AliceBlue">
                <asp:Label ID="lbl_annualy" runat="server" 
                    Text="<%$ Resources:Resource, lbl_annualy %>" style="font-weight: 700"/></td>
        </tr>
        
    <asp:Repeater ID="rNumberUnitbyBedroomNumber" runat="server">
    <ItemTemplate>
    <tr>
    <td> <%#DataBinder.Eval(Container.DataItem, "number_of_unit")%> &nbsp;X &nbsp;<%#DataBinder.Eval(Container.DataItem, "unit_bedroom_no")%>&nbsp;&nbsp; <asp:Label ID="lbl_bedroom" runat="server" 
                    Text="<%$ Resources:Resource, lbl_bedroom %>"/></td>
    <td></td>
    <td><asp:TextBox   Width="65" MaxLength="10"  runat="server"  ID="tbx_bedrooms_total_rent_m"   Text=''/></td>
    <td><asp:Label ID="lbl_bedrooms_total_rent_y" runat="server"  />
                     
        <asp:HiddenField ID="h_number_of_unit" Value='<%#DataBinder.Eval(Container.DataItem, "number_of_unit")%>' runat="server" /> 
        <asp:HiddenField ID="h_unit_bedroom_number" Value='<%#DataBinder.Eval(Container.DataItem, "unit_bedroom_no")%>' runat="server" />
        
                     </td>
    </tr>
    </ItemTemplate>
    </asp:Repeater>
        <tr>
            <td>
                <asp:Label ID="Label19" runat="server" 
                     Text="<%$ Resources:Resource, lbl_parking %>"/></td>
            <td>
                &nbsp;</td>
            <td>
                <asp:TextBox ID="tbx_parking_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td>
                 <asp:Label ID="lbl_parking_y" runat="server" 
                    /></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label20" runat="server" 
                     Text="<%$ Resources:Resource, lbl_laundry %>"/></td>
            <td>
                &nbsp;</td>
            <td>
                <asp:TextBox ID="tbx_laundry_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td>
               <asp:Label ID="lbl_laundry_y" runat="server" /></td>
        </tr>
        <tr>
            <td>
              <asp:Label ID="Label21" runat="server" 
                     Text="<%$ Resources:Resource, lbl_storage %>"/></td>
            <td>
                &nbsp;</td>
            <td>
                <asp:TextBox ID="tbx_storage_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="lbl_storage_y" runat="server" /></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label100" runat="server" 
                    Text="<%$ Resources:Resource, lbl_garage %>" /></td>
            <td>
                &nbsp;</td>
            <td>
                <asp:TextBox ID="tbx_garage_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="lbl_garage_y" runat="server" /></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label101" runat="server" 
                    Text="<%$ Resources:Resource, lbl_vending_machine %>"/></td>
            <td>
                &nbsp;</td>
            <td>
                <asp:TextBox ID="tbx_vending_machine_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="lbl_vending_machine_y" runat="server" /></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label102" runat="server" 
                    Text="<%$ Resources:Resource, lbl_cash_machine %>" /></td>
            <td>
                &nbsp;</td>
            <td>
                <asp:TextBox ID="tbx_cash_machine_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="lbl_cash_machine_y" runat="server" /></td>
        </tr>
        <tr>
            <td>
               <asp:Label ID="Label103" runat="server" 
                    Text="<%$ Resources:Resource, lbl_other %>" /></td>
            <td>
                &nbsp;</td>
            <td>
                <asp:TextBox ID="tbx_other_m2" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td>
                <asp:Label ID="lbl_other_y2" runat="server" /></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label17" runat="server" 
                     Text="<%$ Resources:Resource, lbl_pgi %>" style="font-weight: 700"/>&nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
               <asp:Label ID="lbl_pgi_m" runat="server" /></td>
            <td>
                <asp:Label ID="lbl_pgi_y" runat="server" /></td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btn_calculate_pgi" runat="server" 
                    Text="<%$ Resources:Resource, btn_calculate_pgi %>" ForeColor="#0066FF" 
                    onclick="btn_calculate_pgi_Click" />
                <br />
                <br />
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="AliceBlue">
                 <asp:Label ID="lbl_revenue_loss" runat="server" 
                     Text="<%$ Resources:Resource, lbl_revenue_loss %>" style="font-weight: 700"/></td>
            <td style="font-weight: 700" bgcolor="AliceBlue">
                <asp:Label ID="Label4" runat="server" 
                          Text="<%$ Resources:Resource, lbl_pgi_percent %>" /></td>
            <td bgcolor="AliceBlue">
               <asp:Label ID="Label7" runat="server" 
                    Text="<%$ Resources:Resource, lbl_monthly %>" style="font-weight: 700"/></td>

            <td bgcolor="AliceBlue">
                
                      <asp:Label ID="Label6" runat="server" 
                          Text="<%$ Resources:Resource, lbl_annualy %>" style="font-weight: 700" />
                </td>
        </tr>
        <tr>
            <td>
                 <asp:Label ID="Label27" runat="server" 
                          Text="<%$ Resources:Resource, lbl_vacancy_rate %>" /></td>
            <td>
                <asp:TextBox ID="tbx_vacancy_rate" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td>
               <asp:Label ID="lbl_vacancy_m" runat="server" /></td>
            <td>
                <asp:Label ID="lbl_vacancy_y" runat="server" /></td>
        </tr>
        <tr>
            <td style="height: 22px">
                 <asp:Label ID="Label28" runat="server" 
                          Text="<%$ Resources:Resource, lbl_bda %>" /></td>
            <td style="height: 22px">
                <asp:TextBox ID="tbx_bda_rate" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td style="height: 22px">
                <asp:Label ID="lbl_bda_m" runat="server" /></td>
            <td style="height: 22px">
                <asp:Label ID="lbl_bda_y" runat="server" /></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label18" runat="server" 
                     Text="<%$ Resources:Resource, lbl_egi %>" style="font-weight: 700"/>&nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                 <asp:Label ID="lbl_egi_m" runat="server" /></td>
            <td>
                 <asp:Label ID="lbl_egi_y" runat="server" /></td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btn_calculate_egi" runat="server" 
                    Text="<%$ Resources:Resource, btn_calculate_egi %>" ForeColor="#0066FF" 
                    onclick="btn_calculate_egi_Click" />
                <br />
                <br />
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="AliceBlue">
                <asp:Label ID="Label5" runat="server" 
                    Text="<%$ Resources:Resource, lbl_expenses%>" style="font-weight: 700"/></td>
            <td style="font-weight: 700" bgcolor="AliceBlue">
                <asp:Label ID="Label36" runat="server" 
                          Text="<%$ Resources:Resource, lbl_egi_percent %>" /></td>
            <td bgcolor="AliceBlue">
                 <asp:Label ID="Label3" runat="server" 
                     Text="<%$ Resources:Resource, lbl_monthly %>" style="font-weight: 700"/></td>
            <td bgcolor="AliceBlue">
                 <asp:Label ID="Label1" runat="server" 
                     Text="<%$ Resources:Resource, lbl_annualy %>" style="font-weight: 700"/></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label8" runat="server" 
                    Text="<%$ Resources:Resource, lbl_electricity %>" />&nbsp;</td>
            <td  >
                <asp:TextBox ID="tbx_electricity_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td >
                <asp:Label ID="lbl_electricity_m" runat="server" /></td>
            <td >
                <asp:Label ID="lbl_electricity_y" runat="server" /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label9" runat="server" 
                    Text="<%$ Resources:Resource, lbl_energy %>" />
            </td>
            <td >
                <asp:TextBox ID="tbx_energy_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td >
                <asp:Label ID="lbl_energy_m" runat="server" /></td>
            <td >
                <asp:Label ID="lbl_energy_y" runat="server"  /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label10" runat="server" 
                    Text="<%$ Resources:Resource, lbl_insurances %>" /></td>
            <td >
                <asp:TextBox ID="tbx_insurances_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td >
                <asp:Label ID="lbl_insurances_m" runat="server"/></td>
            <td >
               <asp:Label ID="lbl_insurances_y" runat="server"  /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label11" runat="server" 
                    Text="<%$ Resources:Resource, lbl_janitor %>" /></td>
            <td >
                <asp:TextBox ID="tbx_janitor_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td >
                <asp:Label ID="lbl_janitor_m" runat="server" /></td>
            <td  >
                 <asp:Label ID="lbl_janitor_y" runat="server"  /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label12" runat="server" 
                    Text="<%$ Resources:Resource, lbl_taxes %>"/> </td>
            <td >
                <asp:TextBox ID="tbx_taxes_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td >
                <asp:Label ID="lbl_taxes_m" runat="server" /></td>
            <td >
               <asp:Label ID="lbl_taxes_y" runat="server" /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label13" runat="server" 
                    Text="<%$ Resources:Resource, lbl_maintenance_repair %>" /></td>
            <td >
                <asp:TextBox ID="tbx_maintenance_repair_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td >
                <asp:Label ID="lbl_maintenance_repair_m" runat="server" /></td>
            <td >
                <asp:Label ID="lbl_maintenance_repair_y" runat="server" /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label14" runat="server" 
                    Text="<%$ Resources:Resource, lbl_school_taxes %>" /></td>
            <td  >
                <asp:TextBox ID="tbx_school_taxes_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td  >
                 <asp:Label ID="lbl_school_taxes_m" runat="server"  /></td>
            <td  >
                 <asp:Label ID="lbl_school_taxes_y" runat="server"  /></td>
        </tr>
        <tr>
            <td  >
            <asp:Label ID="Label22" runat="server" 
                    Text="<%$ Resources:Resource, lbl_management %>" />
            </td>
            <td  >
                <asp:TextBox ID="tbx_management_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td ><asp:Label ID="lbl_management_m" runat="server"  />
            </td>
            <td  ><asp:Label ID="lbl_management_y" runat="server"  />
            </td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label23" runat="server" 
                    Text="<%$ Resources:Resource, lbl_advertising %>" /></td>
            <td style="height: 18px">
                <asp:TextBox ID="tbx_advertising_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td  >
                <asp:Label ID="lbl_advertising_m" runat="server"  /></td>
            <td  >
                <asp:Label ID="lbl_advertising_y" runat="server"  /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label24" runat="server" 
                    Text="<%$ Resources:Resource, lbl_legal %>" /></td>
            <td >
                <asp:TextBox ID="tbx_legal_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td  >
                <asp:Label ID="lbl_legal_m" runat="server" /></td>
            <td  >
                <asp:Label ID="lbl_legal_y" runat="server" /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label25" runat="server" 
                    Text="<%$ Resources:Resource, lbl_accounting %>" /></td>
            <td >
                <asp:TextBox ID="tbx_accounting_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td >
               <asp:Label ID="lbl_accounting_m" runat="server"  /></td>
            <td >
                <asp:Label ID="lbl_accounting_y" runat="server" /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label26" runat="server" 
                    Text="<%$ Resources:Resource, lbl_other %>"/></td>
            <td >
                <asp:TextBox ID="tbx_other_m" runat="server" Width="65px"></asp:TextBox>
            </td>
            <td >
                <asp:Label ID="lbl_other_m" runat="server" /></td>
            <td  >
                <asp:Label ID="lbl_other_y" runat="server"  /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                &nbsp;</td>
            <td style="height: 18px">
                &nbsp;</td>
            <td style="height: 18px">
                &nbsp;</td>
            <td style="height: 18px">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Label ID="Label15" runat="server" 
                    Text="<%$ Resources:Resource, lbl_total_expenses %>" 
                    style="font-weight: 700"/></td>
            <td  >
                <asp:Label ID="lbl_total_exp_percent" runat="server"></asp:Label>
            </td>
            <td  >
                <asp:Label ID="lbl_total_expenses_m" runat="server"  /></td>
            <td  >
                <asp:Label ID="lbl_total_expenses_y" runat="server" /></td>
        </tr>
        <tr>
            <td style="height: 18px">
                <asp:Button ID="btn_calculate_expenses" runat="server" 
                    Text="<%$ Resources:Resource, btn_calculate_expenses %>" ForeColor="#0066FF" 
                    onclick="btn_calculate_expenses_Click"  />
                <br />
                <br />
            </td>
            <td style="height: 18px">
                &nbsp;</td>
            <td style="height: 18px">
                &nbsp;</td>
            <td style="height: 18px">
                &nbsp;</td>
        </tr>
        <tr>
            <td bgcolor="AliceBlue">
                <asp:Label ID="Label16" runat="server" 
                    Text="<%$ Resources:Resource, lbl_money_flow %>" 
                    style="font-weight: 700" /></td>
            <td bgcolor="AliceBlue">
                &nbsp;</td>
            <td bgcolor="AliceBlue">
                &nbsp;</td>
            <td bgcolor="AliceBlue">
                 <asp:Label ID="Label2" runat="server" 
                     Text="<%$ Resources:Resource, lbl_annualy %>" style="font-weight: 700"/></td>
        </tr>
        <tr>
            <td>
               <asp:Label ID="Label29" runat="server" 
                     Text="<%$ Resources:Resource, lbl_net_operating_inc %>" /></td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_net_operating_inc_y" runat="server"   /></td>
        </tr>
        <tr>
            <td>
               <asp:Label ID="Label30" runat="server" 
                     Text="<%$ Resources:Resource, lbl_ads %>"/></td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:TextBox ID="tbx_ads_y" runat="server" Width="65px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label31" runat="server" 
                     Text="<%$ Resources:Resource, lbl_liquidity %>" 
                    style="font-weight: 700" /></td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_liquidity_y" runat="server"  /></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                 <asp:Label ID="Label32" runat="server" 
                     Text="<%$ Resources:Resource, lbl_capitalisation %>" /></td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:TextBox ID="tbx_capitalisation_y" runat="server" Width="65px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label33" runat="server" 
                     Text="<%$ Resources:Resource, lbl_liquidity_capitalisation %>" 
                    style="font-weight: 700" /></td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_liquidity_capitalisation_y" runat="server" /></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
               <asp:Label ID="Label34" runat="server" 
                     Text="<%$ Resources:Resource, lbl_added_value %>" /></td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:TextBox ID="tbx_added_value_y" runat="server" Width="65px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label35" runat="server" 
                     Text="<%$ Resources:Resource, lbl_liq_cap_value %>" 
                    style="font-weight: 700" /></td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                <asp:Label ID="lbl_liq_cap_value_y" runat="server" /></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="Caculate" runat="server" onclick="Caculate_Click" 
                    Text="<%$ Resources:Resource, btn_calculate %>" ForeColor="#0066FF" />
                &nbsp;<br />
                </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center" bgcolor="AliceBlue" colspan="4">
                <b><asp:Label ID="Label39" runat="server" 
                     Text="<%$ Resources:Resource, lbl_analysis_return %>" /></b></td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label40" runat="server" 
                     Text="<%$ Resources:Resource, lbl_appartment_cost%>" /></td>
            <td colspan="2">
                <asp:Label ID="lbl_appartment_cost" runat="server"/></td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label41" runat="server" 
                     Text="<%$ Resources:Resource, lbl_exploitation_ratio%>" /></td>
            <td colspan="2">
                <asp:Label ID="lbl_rde" runat="server"/></td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label42" runat="server" 
                     Text="<%$ Resources:Resource, lbl_min_occupation %>" /></td>
            <td colspan="2">
                <asp:Label ID="lbl_tmo" runat="server"/></td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label43" runat="server" 
                     Text="<%$ Resources:Resource, lbl_performance_on_net %>" /></td>
            <td colspan="2">
                <asp:Label ID="lbl_trn" runat="server"/></td>
        </tr>
        
         <tr>
            <td colspan="2">
               <asp:Label ID="Label44" runat="server" 
                     Text="<%$ Resources:Resource, lbl_gi_multiplier %>" /></td>
            <td colspan="2">
                <asp:Label ID="lbl_mbre" runat="server"></asp:Label></td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label47" runat="server" 
                     Text="<%$ Resources:Resource, lbl_net_income_multiplier %>" /></td>
            <td colspan="2">
           <asp:Label ID="lbl_mrn" runat="server"></asp:Label>     
            </td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label46" runat="server" 
                     Text="<%$ Resources:Resource, lbl_debt_cover_ratio %>" /></td>
            <td colspan="2">
               <asp:Label ID="lbl_rcd" runat="server"></asp:Label></td>
        </tr>
    </table>


    <br />


    <asp:Button ID="Button1" ForeColor="#0066FF"  runat="server" Text="<%$ Resources:Resource, btn_return %>" onclick="Button1_Click" />


    <br />

<asp:HiddenField ID="h_total_number_of_unit"  runat="server" /> 
        
</asp:Content>

