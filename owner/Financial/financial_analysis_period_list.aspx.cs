﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : april 13, 2008
/// </summary>

public partial class manager_Financial_financial_analysis_period_list : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.IsPostBack))
        {
            tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gvFinancialAnalysisPeriodList.DataSource = hp.getOwnerFinancialAnalysisPeriodList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
            gvFinancialAnalysisPeriodList.DataBind();


        }
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_delete_Click(object sender, EventArgs e)
    {

        //*********************************************************************
        //*********************************************************************
        Button btn_cancel = (Button)sender;
        GridViewRow grdRow = (GridViewRow)btn_cancel.Parent.Parent;
        string strField1 = grdRow.Cells[6].Text;

        HiddenField h_pa_id = (HiddenField)grdRow.Cells[6].FindControl("h_pa_id");
 
        //------------------------------------------------------------------------------------

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prFinancialAnalysisPeriodDelete", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@period_analysis_id", SqlDbType.Int).Value = Convert.ToInt32(h_pa_id.Value);

            //execute the insert
            cmd.ExecuteReader();

        }
        catch (Exception error)
        {
            //  tiger.security.Error.errorAdd(conn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
        }
        
        //-------------------------------------------------------------------------------------
        
        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gvFinancialAnalysisPeriodList.DataSource = hp.getFinancialAnalysisPeriodList(Convert.ToInt32(Session["schema_id"]));
        gvFinancialAnalysisPeriodList.DataBind();


    }








}
