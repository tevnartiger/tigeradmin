﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : april 14 , 2008
/// </summary>
/// 

public partial class manager_Financial_financial_analysis_period_view : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!(Page.IsPostBack))
        {
            string maison = "";

            //------------------------------------------------------------------------------

            DateTime to = new DateTime();
            DateTime from = new DateTime();
            to = DateTime.Now; // the date in the to drop downlist

            tiger.Date d = new tiger.Date();
            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime

            int home_id = 0;
            //------------------------------------------------------------------------------
            //------------------------------------------------------------------------------



            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prFinancialAnalysisPeriodId", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@period_analysis_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["pa_id"]);

            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);



                while (dr.Read() == true)
                {

                    //tb = Convert.ToInt32(dr["home_id"]);
                    // h_h_id.Value = home_id.ToString();


                    from = Convert.ToDateTime(dr["period_analysis_date_begin"]);
                    to = Convert.ToDateTime(dr["period_analysis_date_end"]);

                    to = Convert.ToDateTime(d.DateCulture(to.Month.ToString(), to.Day.ToString(), to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
                    from = Convert.ToDateTime(d.DateCulture(from.Month.ToString(), from.Day.ToString(), from.Year.ToString(), Convert.ToString(Session["_lastCulture"])));

                    home_id = Convert.ToInt32(dr["home_id"]);

                    lbl_property_name.Text = dr["home_name"].ToString();

                    lbl_analysis_name.Text = dr["period_analysis_name"].ToString(); ;
                    lbl_analysis_comments.Text = dr["period_analysis_comments"].ToString(); 

                    lbl_debt_service_m.Text = dr["period_analysis_debt_service"].ToString();

                    lbl_capitalisation_m.Text = dr["period_analysis_capitalisation"].ToString();

                    lbl_added_value_m.Text = dr["period_analysis_added_value"].ToString();

                    lbl_home_value.Text = dr["period_analysis_property_value"].ToString();
                }

            }

            finally
            {
                conn.Close();
            }




            //------------------------------------------------------------------------------
            //------------------------------------------------------------------------------






            lbl_from2.Text = from.Month.ToString() + "-" + from.Day.ToString() + "-" + from.Year.ToString();
            lbl_to2.Text = to.Month.ToString() + "-" + to.Day.ToString() + "-" + to.Year.ToString();


            lbl_from3.Text = from.Month.ToString() + "-" + from.Day.ToString() + "-" + from.Year.ToString();
            lbl_to3.Text = to.Month.ToString() + "-" + to.Day.ToString() + "-" + to.Year.ToString();


            lbl_from4.Text = from.Month.ToString() + "-" + from.Day.ToString() + "-" + from.Year.ToString();
            lbl_to4.Text = to.Month.ToString() + "-" + to.Day.ToString() + "-" + to.Year.ToString();



            tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = h.getHomeCount(Convert.ToInt32(Session["schema_id"]));

            if (home_count > 0)
            {


                tiger.Financial f = new tiger.Financial(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                rNumberUnitbyBedroomNumber.DataSource = f.getNumberUnitbyBedroomNumberTotalMonthlyRentIncomePeriod(Convert.ToInt32(Session["schema_id"]), home_id, from, to);
                rNumberUnitbyBedroomNumber.DataBind();



                // tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rExpense.DataSource = f.getExpenseMonthViewGroupByCategPeriod(home_id, Convert.ToInt32(Session["schema_id"]), from, to);
                rExpense.DataBind();


                /////////////////////////////////////////////////////////////

                //To view the address of the property

                tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
                rhome_view.DataBind();





                // tiger.Financial hpp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rIncome.DataSource = f.getIncomeMonthViewGroupByCategPeriod(home_id, Convert.ToInt32(Session["schema_id"]), from, to);

                rIncome.DataBind();

                lbl_late_fee.Text = f.getPaidLateRentFeeHomeSumPeriod(home_id, Convert.ToInt32(Session["schema_id"]), from, to);


                //// CALCULATION OF TOTAL INCOME
                decimal month_total = 0;
                double number_of_unit = 0;

                for (int i = 0; i < rNumberUnitbyBedroomNumber.Items.Count; i++)
                {

                    HiddenField h_number_of_unit = (HiddenField)rNumberUnitbyBedroomNumber.Items[i].FindControl("h_number_of_unit");
                    Label bedrooms_total_rent_m = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_m");
                    month_total = month_total + Convert.ToDecimal(bedrooms_total_rent_m.Text);
                    number_of_unit = number_of_unit + Convert.ToDouble(h_number_of_unit.Value);
                }


                h_total_number_of_unit.Value = number_of_unit.ToString();






                for (int i = 0; i < rIncome.Items.Count; i++)
                {
                    HiddenField h_income_amount = (HiddenField)rIncome.Items[i].FindControl("h_income_amount");
                    month_total = month_total + Convert.ToDecimal(h_income_amount.Value);
                }



                month_total = month_total + Convert.ToDecimal(lbl_late_fee.Text);
                lbl_gi_m.Text = Convert.ToString(month_total);

                //// END OF CALCULATION OF TOTAL INCOME


                //// CALCULATION OF TOTAL EXPENSES

                decimal month_total_expense = 0;

                for (int i = 0; i < rExpense.Items.Count; i++)
                {
                    HiddenField h_expense_amount = (HiddenField)rExpense.Items[i].FindControl("h_expense_amount");
                    month_total_expense = month_total_expense + Convert.ToDecimal(h_expense_amount.Value);
                }

                lbl_total_expenses_m.Text = Convert.ToString(month_total_expense);

                //// END OF CALCULATION OF TOTAL EXPENSE

                decimal net_income = 0;

                net_income = month_total - month_total_expense;

                lbl_net_operating_inc_m.Text = Convert.ToString(net_income);

                lbl_liquidity_m.Text = "";

            }
            // if ther is no home

            else
            {
                //  txt_message.InnerHtml = "There is no property -- Add a property";

                //  txt_link.InnerHtml = home_count + " <a href='home_add.aspx'>Add Home</a>&nbsp;&nbsp;<a href='owner_add.aspx'>Add Owner</a>&nbsp;&nbsp;<a href='storage_add.aspx'>Add storage</a>&nbsp;&nbsp;";
            }


            //MONEY FLOW CALCULATION

            decimal liquidity = 0;

            if (lbl_debt_service_m.Text == "")
                lbl_debt_service_m.Text = "0";


            liquidity = Convert.ToDecimal(lbl_net_operating_inc_m.Text) - Convert.ToDecimal(lbl_debt_service_m.Text);

            lbl_liquidity_m.Text = Convert.ToString(liquidity);


            // Generated liquidity + capitalisation
            lbl_liquidity_capitalisation_m.Text = Convert.ToString(Convert.ToDecimal(lbl_liquidity_m.Text) + Convert.ToDecimal(lbl_capitalisation_m.Text));

            //Liquidity + Cap. +Added value
            lbl_liq_cap_value_m.Text = Convert.ToString(Convert.ToDecimal(lbl_liquidity_capitalisation_m.Text) + Convert.ToDecimal(lbl_added_value_m.Text));


            // ANALYSIS OF RETURN CALCULATION
             {

               

                double property_value = 0;
                double added_value = 0;
                double total_number_of_unit = Convert.ToDouble(h_total_number_of_unit.Value);
                double appartment_cost = 0;
                property_value = Convert.ToDouble(lbl_home_value.Text);


                property_value = property_value + Convert.ToDouble(lbl_added_value_m.Text);

                appartment_cost = (property_value / total_number_of_unit);

                lbl_appartment_cost.Text = appartment_cost.ToString();
                lbl_mbre.Text = Convert.ToString((property_value / Convert.ToDouble(lbl_gi_m.Text)));
                lbl_rde.Text = Convert.ToString((Convert.ToDouble(lbl_total_expenses_m.Text) / Convert.ToDouble(lbl_gi_m.Text)));

                //  lbl_tmo.Text = Convert.ToString((Convert.ToDouble(lbl_total_expenses_m.Text) + Convert.ToDouble(tbx_debt_service_m.Text)) / Convert.ToDouble(lbl_pgi_y.Text));

                lbl_trn.Text = Convert.ToString(Convert.ToDouble(lbl_net_operating_inc_m.Text) / property_value);

                lbl_mrn.Text = Convert.ToString(property_value / Convert.ToDouble(lbl_net_operating_inc_m.Text));

                lbl_rcd.Text = Convert.ToString((Convert.ToDouble(lbl_net_operating_inc_m.Text) / Convert.ToDouble(lbl_debt_service_m.Text)));
   
             }



        }

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetExpenseCateg(int expensecateg_id)
    {
        string expensecateg = "";

        switch (expensecateg_id)
        {
            case 1:
                expensecateg = Resources.Resource.lbl_electricity;
                break;
            case 2:
                expensecateg = Resources.Resource.lbl_energy;
                break;
            case 3:
                expensecateg = Resources.Resource.lbl_insurances;
                break;

            case 4:
                expensecateg = Resources.Resource.lbl_janitor;
                break;
            case 5:
                expensecateg = Resources.Resource.lbl_taxes;
                break;
            case 6:
                expensecateg = Resources.Resource.lbl_maintenance_repair;
                break;


            case 7:
                expensecateg = Resources.Resource.lbl_school_taxes;
                break;
            case 8:
                expensecateg = Resources.Resource.lbl_management;
                break;
            case 9:
                expensecateg = Resources.Resource.lbl_advertising;
                break;



            case 10:
                expensecateg = Resources.Resource.lbl_legal;
                break;
            case 11:
                expensecateg = Resources.Resource.lbl_accounting;
                break;
            case 12:
                expensecateg = Resources.Resource.lbl_other;
                break;

        }

        return expensecateg;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetIncomeCateg(int incomecateg_id)
    {
        string incomecateg = "";

        switch (incomecateg_id)
        {
            case 1:
                incomecateg = Resources.Resource.lbl_garage;
                break;
            case 2:
                incomecateg = Resources.Resource.lbl_parking;
                break;
            case 3:
                incomecateg = Resources.Resource.lbl_laundry;
                break;

            case 4:
                incomecateg = Resources.Resource.lbl_storage;
                break;
            case 5:
                incomecateg = Resources.Resource.lbl_vending_machine;
                break;
            case 6:
                incomecateg = Resources.Resource.lbl_cash_machine;
                break;

            case 7:
                incomecateg = Resources.Resource.lbl_other;
                break;

        }

        return incomecateg;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_expense_view_graph_Click(object sender, EventArgs e)
    {

        int home_id = 0;

        DateTime to = new DateTime();
        DateTime from = new DateTime();
        tiger.Date d = new tiger.Date();

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prFinancialAnalysisPeriodId", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        //Add the params
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@period_analysis_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["pa_id"]);

        try
        {
            conn.Open();

            SqlDataReader dr = null;
            dr = cmd.ExecuteReader(CommandBehavior.SingleRow);



            while (dr.Read() == true)
            {


                from = Convert.ToDateTime(dr["period_analysis_date_begin"]);
                to = Convert.ToDateTime(dr["period_analysis_date_end"]);
                home_id = Convert.ToInt32(dr["home_id"]);

                to = Convert.ToDateTime(d.DateCulture(to.Month.ToString(), to.Day.ToString(), to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
                from = Convert.ToDateTime(d.DateCulture(from.Month.ToString(), from.Day.ToString(), from.Year.ToString(), Convert.ToString(Session["_lastCulture"])));

                
            }

        }

        finally
        {
            conn.Close();
        }

        Response.Redirect("financial_analysis_period_expense_graph.aspx?h_id=" + home_id.ToString() + "&m=" + from.Month.ToString()
                           + "&d=" + from.Day.ToString() + "&y=" + from.Year.ToString()
                           + "&m2=" + to.Month.ToString()
                           + "&d2=" + to.Day.ToString() + "&y2=" + to.Year.ToString());
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_income_view_graph_Click(object sender, EventArgs e)
    {

        int home_id = 0;

        DateTime to = new DateTime();
        DateTime from = new DateTime();
        tiger.Date d = new tiger.Date();

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prFinancialAnalysisPeriodId", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        //Add the params
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@period_analysis_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["pa_id"]);
        

        try
        {
            conn.Open();

            SqlDataReader dr = null;
            dr = cmd.ExecuteReader(CommandBehavior.SingleRow);



            while (dr.Read() == true)
            {


                from = Convert.ToDateTime(dr["period_analysis_date_begin"]);
                to = Convert.ToDateTime(dr["period_analysis_date_end"]);
                home_id = Convert.ToInt32(dr["home_id"]);

                to = Convert.ToDateTime(d.DateCulture(to.Month.ToString(), to.Day.ToString(), to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
                from = Convert.ToDateTime(d.DateCulture(from.Month.ToString(), from.Day.ToString(), from.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


            }

        }

        finally
        {
            conn.Close();
        }

        Response.Redirect("financial_analysis_period_income_graph.aspx?h_id=" + home_id.ToString() + "&m=" + from.Month.ToString()
                          + "&d=" + from.Day.ToString() + "&y=" + from.Year.ToString()
                          + "&m2=" + to.Month.ToString()
                          + "&d2=" + to.Day.ToString() + "&y2=" + to.Year.ToString());
    }
}
