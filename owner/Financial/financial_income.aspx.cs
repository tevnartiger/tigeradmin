using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.OwnerObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : march 30, 2008
/// </summary>

public partial class manager_Financial_financial_income : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!(Page.IsPostBack))
        {
            string maison = "";



            DateTime date = new DateTime();
            date = DateTime.Now;
            tiger.Date c = new tiger.Date();
            //if we are entering from the update page
            //-----------------------------------------------------
            if (Request.QueryString["h_id"] != "" && Request.QueryString["h_id"] != null && Request.QueryString["m"] != "" && Request.QueryString["m"] != null && Request.QueryString["y"] != "" && Request.QueryString["y"] != null)
            {
                OwnerObjectAuthorization homeAuthorization = new OwnerObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), Convert.ToInt32(Request.QueryString["h_id"])))
                {
                    Session.Abandon();
                    Response.Redirect("http://www.sinfocatiger.com/login.aspx");
                }
                ///////// SECURITY OBJECT CHECK  END ////////////////////////////////

                date = Convert.ToDateTime(c.DateCulture(Request.QueryString["m"], Request.QueryString["d"], Request.QueryString["y"], Convert.ToString(Session["_lastCulture"])));

            }
            //-----------------------------------------------------




            // default Date receive of the incomes
            //   ddl_year.SelectedValue = date.Year.ToString();

            ddl_date_received_m.SelectedValue = date.Month.ToString();
            ddl_date_received_y.SelectedValue = date.Year.ToString();

            // garage
            ddl_garage_date_received_y.SelectedValue = date.Year.ToString();
            ddl_garage_date_received_d.SelectedValue = date.Day.ToString();
            ddl_garage_date_received_m.SelectedValue = date.Month.ToString();


            // parking
            ddl_parking_date_received_y.SelectedValue = date.Year.ToString();
            ddl_parking_date_received_d.SelectedValue = date.Day.ToString();
            ddl_parking_date_received_m.SelectedValue = date.Month.ToString();


            // laundry
            ddl_laundry_date_received_y.SelectedValue = date.Year.ToString();
            ddl_laundry_date_received_d.SelectedValue = date.Day.ToString();
            ddl_laundry_date_received_m.SelectedValue = date.Month.ToString();

            // storage
            ddl_storage_date_received_y.SelectedValue = date.Year.ToString();
            ddl_storage_date_received_d.SelectedValue = date.Day.ToString();
            ddl_storage_date_received_m.SelectedValue = date.Month.ToString();


            // vending_machine
            ddl_vending_machine_date_received_y.SelectedValue = date.Year.ToString();
            ddl_vending_machine_date_received_d.SelectedValue = date.Day.ToString();
            ddl_vending_machine_date_received_m.SelectedValue = date.Month.ToString();


            // cash_machine
            ddl_cash_machine_date_received_y.SelectedValue = date.Year.ToString();
            ddl_cash_machine_date_received_d.SelectedValue = date.Day.ToString();
            ddl_cash_machine_date_received_m.SelectedValue = date.Month.ToString();



            ddl_other_date_received_y.SelectedValue = date.Year.ToString();
            ddl_other_date_received_d.SelectedValue = date.Day.ToString();
            ddl_other_date_received_m.SelectedValue = date.Month.ToString();







            tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = h.getHomeCount(Convert.ToInt32(Session["schema_id"]));
            string link_to_unit = "";
            if (home_count > 0)
            {
                int home_id = h.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));

                //if we are entering from the update page
                //-----------------------------------------------------
                if (Request.QueryString["h_id"] != "" && Request.QueryString["h_id"] != null)
                {
                    home_id = Convert.ToInt32(Request.QueryString["h_id"]);

                    OwnerObjectAuthorization homeAuthorization = new OwnerObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                    if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), home_id))
                    {
                        Session.Abandon();
                        Response.Redirect("http://www.sinfocatiger.com/login.aspx");
                    }
                    ///////// SECURITY OBJECT CHECK  END ////////////////////////////////

                }
                //-----------------------------------------------------


                ddl_home_id.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                ddl_home_id.SelectedValue = Convert.ToString(home_id);
                ddl_home_id.DataBind();



                //To view the address of the property

                tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
                rhome_view.DataBind();

                // to view the incomes in the default home
                DateTime income_date_received = new DateTime();
                tiger.Date d = new tiger.Date();
                income_date_received = Convert.ToDateTime(d.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

                //if we are entering from the update page
                //-----------------------------------------------------
                if (Request.QueryString["h_id"] != "" && Request.QueryString["h_id"] != null && Request.QueryString["m"] != "" && Request.QueryString["m"] != null && Request.QueryString["y"] != "" && Request.QueryString["y"] != null)
                {
                    OwnerObjectAuthorization homeAuthorization = new OwnerObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    
                    ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                    if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), Convert.ToInt32(Request.QueryString["h_id"])))
                    {
                        Session.Abandon();
                        Response.Redirect("http://www.sinfocatiger.com/login.aspx");
                    }
                    ///////// SECURITY OBJECT CHECK  END ////////////////////////////////

                    income_date_received = Convert.ToDateTime(d.DateCulture(Request.QueryString["m"], "1", Request.QueryString["y"], Convert.ToString(Session["_lastCulture"])));

                }
                //-----------------------------------------------------

                tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

                //////////////////  tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                gv_Income.DataSource = hp.getIncomeMonthView(home_id, Convert.ToInt32(Session["schema_id"]), income_date_received);
                gv_Income.DataBind();

                lbl_total_month_income.Text = hp.getTotalMonthIncome(home_id, Convert.ToInt32(Session["schema_id"]), income_date_received);


            }
            // if ther is no home

            else
            {
                //  txt_message.InnerHtml = "There is no property -- Add a property";

                //  txt_link.InnerHtml = home_count + " <a href='home_add.aspx'>Add Home</a>&nbsp;&nbsp;<a href='owner_add.aspx'>Add Owner</a>&nbsp;&nbsp;<a href='storage_add.aspx'>Add storage</a>&nbsp;&nbsp;";
            }

        }

    }
    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {

        //To view the address of the property

        tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
        rhome_view.DataBind();


        // to view the incomes in the  home
        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        DateTime income_date_received = new DateTime();
        tiger.Date df = new tiger.Date();
        income_date_received = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        //////////////////  tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_Income.DataSource = hp.getIncomeMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);
        gv_Income.DataBind();




        lbl_total_month_income.Text = hp.getTotalMonthIncome(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_income_reference.Visible = false;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_all_Click(object sender, EventArgs e)
    {

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime date_received = new DateTime();
        tiger.Date d = new tiger.Date();
        date_received = Convert.ToDateTime(d.DateCulture(ddl_garage_date_received_m.SelectedValue, ddl_garage_date_received_d.SelectedValue, ddl_garage_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prIncomeBatchAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@incomecateg_id", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@income_date_received", SqlDbType.DateTime).Value = date_received;
            cmd.Parameters.Add("@income_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_garage_m.Text.Replace(",", "."));
            cmd.Parameters.Add("@income_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_garage.Text);
            cmd.Parameters.Add("@income_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_garage.Text);


            cmd.Parameters.Add("@incomecateg_id2", SqlDbType.Int).Value = 2;
            cmd.Parameters.Add("@income_date_received2", SqlDbType.DateTime).Value = date_received;
            cmd.Parameters.Add("@income_amount2", SqlDbType.Money).Value = Convert.ToDecimal(tbx_parking_m.Text.Replace(",", "."));

            cmd.Parameters.Add("@income_reference2", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_parking.Text);
            cmd.Parameters.Add("@income_comments2", SqlDbType.Text).Value = Convert.ToString(tbx_com_parking.Text);




            cmd.Parameters.Add("@incomecateg_id4", SqlDbType.Int).Value = 4;
            cmd.Parameters.Add("@income_date_received4", SqlDbType.DateTime).Value = date_received;
            cmd.Parameters.Add("@income_amount4", SqlDbType.Money).Value = Convert.ToDecimal(tbx_storage_m.Text.Replace(",", "."));


            cmd.Parameters.Add("@income_reference4", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_storage.Text);
            cmd.Parameters.Add("@income_comments4", SqlDbType.Text).Value = Convert.ToString(tbx_com_storage.Text);



            cmd.Parameters.Add("@incomecateg_id3", SqlDbType.Int).Value = 3;
            cmd.Parameters.Add("@income_date_received3", SqlDbType.DateTime).Value = date_received;
            cmd.Parameters.Add("@income_amount3", SqlDbType.Money).Value = Convert.ToDecimal(tbx_laundry_m.Text.Replace(",", "."));


            cmd.Parameters.Add("@income_reference3", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_laundry.Text);
            cmd.Parameters.Add("@income_comments3", SqlDbType.Text).Value = Convert.ToString(tbx_com_laundry.Text);



            cmd.Parameters.Add("@incomecateg_id5", SqlDbType.Int).Value = 5;
            cmd.Parameters.Add("@income_date_received5", SqlDbType.DateTime).Value = date_received;
            cmd.Parameters.Add("@income_amount5", SqlDbType.Money).Value = Convert.ToDecimal(tbx_vending_machine_m.Text.Replace(",", "."));


            cmd.Parameters.Add("@income_reference5", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_vending_machine.Text);
            cmd.Parameters.Add("@income_comments5", SqlDbType.Text).Value = Convert.ToString(tbx_com_vending_machine.Text);





            cmd.Parameters.Add("@incomecateg_id6", SqlDbType.Int).Value = 6;
            cmd.Parameters.Add("@income_date_received6", SqlDbType.DateTime).Value = date_received;
            cmd.Parameters.Add("@income_amount6", SqlDbType.Money).Value = Convert.ToDecimal(tbx_cash_machine_m.Text.Replace(",", "."));


            cmd.Parameters.Add("@income_reference6", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_cash_machine.Text);
            cmd.Parameters.Add("@income_comments6", SqlDbType.Text).Value = Convert.ToString(tbx_com_cash_machine.Text);


            cmd.Parameters.Add("@incomecateg_id7", SqlDbType.Int).Value = 7;
            cmd.Parameters.Add("@income_date_received7", SqlDbType.DateTime).Value = date_received;
            cmd.Parameters.Add("@income_amount7", SqlDbType.Money).Value = Convert.ToDecimal(tbx_other_m.Text.Replace(",", "."));

            cmd.Parameters.Add("@income_reference7", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_other.Text);
            cmd.Parameters.Add("@income_comments7", SqlDbType.Text).Value = Convert.ToString(tbx_com_other.Text);




            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }




        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        DateTime income_date_received = new DateTime();
        tiger.Date df = new tiger.Date();
        income_date_received = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        //////////////////  tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_Income.DataSource = hp.getIncomeMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);
        gv_Income.DataBind();




        lbl_total_month_income.Text = hp.getTotalMonthIncome(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);

        MaintainScrollPositionOnPostBack = false;


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_income_reference.Visible = false;

    }

    protected void ddl_date_received_y_SelectedIndexChanged(object sender, EventArgs e)
    {
        // garage
        ddl_garage_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_garage_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;


        // parking
        ddl_parking_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_parking_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // laundry
        ddl_laundry_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_laundry_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // storage
        ddl_storage_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_storage_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // vending_machine
        ddl_vending_machine_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_vending_machine_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;


        // cash_machine
        ddl_cash_machine_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_cash_machine_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;


        ddl_other_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_other_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // to view the incomes in the  home
        DateTime income_date_received = new DateTime();
        tiger.Date d = new tiger.Date();
        income_date_received = Convert.ToDateTime(d.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        //////////////////  tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_Income.DataSource = hp.getIncomeMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);
        gv_Income.DataBind();


        lbl_total_month_income.Text = hp.getTotalMonthIncome(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_income_reference.Visible = false;


    }
    protected void ddl_date_received_m_SelectedIndexChanged(object sender, EventArgs e)
    {
        // garage
        ddl_garage_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_garage_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;


        // parking
        ddl_parking_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_parking_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // laundry
        ddl_laundry_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_laundry_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // storage
        ddl_storage_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_storage_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // vending_machine
        ddl_vending_machine_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_vending_machine_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;


        // cash_machine
        ddl_cash_machine_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_cash_machine_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;


        ddl_other_date_received_y.SelectedValue = ddl_date_received_y.SelectedValue;
        ddl_other_date_received_m.SelectedValue = ddl_date_received_m.SelectedValue;

        // to view the incomes in the  home
        DateTime income_date_received = new DateTime();
        tiger.Date d = new tiger.Date();
        income_date_received = Convert.ToDateTime(d.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        //////////////////  tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_Income.DataSource = hp.getIncomeMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);
        gv_Income.DataBind();


        lbl_total_month_income.Text = hp.getTotalMonthIncome(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_income_reference.Visible = false;
    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_garage_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime date_received = new DateTime();
        tiger.Date d = new tiger.Date();
        date_received = Convert.ToDateTime(d.DateCulture(ddl_garage_date_received_m.SelectedValue, ddl_garage_date_received_d.SelectedValue, ddl_garage_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prIncomeAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@incomecateg_id", SqlDbType.Int).Value = 1;
            cmd.Parameters.Add("@income_date_received", SqlDbType.DateTime).Value = date_received;
            cmd.Parameters.Add("@income_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_garage_m.Text.Replace(",", "."));
            cmd.Parameters.Add("@income_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_garage.Text);
            cmd.Parameters.Add("@income_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_garage.Text);



            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            // tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }




        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        DateTime income_date_received = new DateTime();
        tiger.Date df = new tiger.Date();
        income_date_received = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        //////////////////  tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_Income.DataSource = hp.getIncomeMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);
        gv_Income.DataBind();



        lbl_total_month_income.Text = hp.getTotalMonthIncome(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_income_reference.Visible = false;


    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_parking_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime date_received = new DateTime();
        tiger.Date d = new tiger.Date();
        date_received = Convert.ToDateTime(d.DateCulture(ddl_parking_date_received_m.SelectedValue, ddl_parking_date_received_d.SelectedValue, ddl_parking_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prIncomeAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@incomecateg_id", SqlDbType.Int).Value = 2;
            cmd.Parameters.Add("@income_date_received", SqlDbType.DateTime).Value = date_received;
            cmd.Parameters.Add("@income_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_parking_m.Text.Replace(",", "."));

            cmd.Parameters.Add("@income_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_parking.Text);
            cmd.Parameters.Add("@income_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_parking.Text);



            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }


        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        DateTime income_date_received = new DateTime();
        tiger.Date df = new tiger.Date();
        income_date_received = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        //////////////////  tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_Income.DataSource = hp.getIncomeMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);
        gv_Income.DataBind();


        lbl_total_month_income.Text = hp.getTotalMonthIncome(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_income_reference.Visible = false;

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_storage_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime date_received = new DateTime();
        tiger.Date d = new tiger.Date();
        date_received = Convert.ToDateTime(d.DateCulture(ddl_storage_date_received_m.SelectedValue, ddl_storage_date_received_d.SelectedValue, ddl_storage_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prIncomeAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@incomecateg_id", SqlDbType.Int).Value = 4;
            cmd.Parameters.Add("@income_date_received", SqlDbType.DateTime).Value = date_received;
            cmd.Parameters.Add("@income_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_storage_m.Text.Replace(",", "."));


            cmd.Parameters.Add("@income_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_storage.Text);
            cmd.Parameters.Add("@income_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_storage.Text);

            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }




        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        DateTime income_date_received = new DateTime();
        tiger.Date df = new tiger.Date();
        income_date_received = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        //////////////////  tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_Income.DataSource = hp.getIncomeMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);
        gv_Income.DataBind();


        lbl_total_month_income.Text = hp.getTotalMonthIncome(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_income_reference.Visible = false;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_laundry_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime date_received = new DateTime();
        tiger.Date d = new tiger.Date();
        date_received = Convert.ToDateTime(d.DateCulture(ddl_laundry_date_received_m.SelectedValue, ddl_laundry_date_received_d.SelectedValue, ddl_laundry_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prIncomeAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@incomecateg_id", SqlDbType.Int).Value = 3;
            cmd.Parameters.Add("@income_date_received", SqlDbType.DateTime).Value = date_received;
            cmd.Parameters.Add("@income_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_laundry_m.Text.Replace(",", "."));


            cmd.Parameters.Add("@income_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_laundry.Text);
            cmd.Parameters.Add("@income_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_laundry.Text);

            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }



        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        DateTime income_date_received = new DateTime();
        tiger.Date df = new tiger.Date();
        income_date_received = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        //////////////////  tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_Income.DataSource = hp.getIncomeMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);
        gv_Income.DataBind();


        lbl_total_month_income.Text = hp.getTotalMonthIncome(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_income_reference.Visible = false;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_vending_machine_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime date_received = new DateTime();
        tiger.Date d = new tiger.Date();
        date_received = Convert.ToDateTime(d.DateCulture(ddl_vending_machine_date_received_m.SelectedValue, ddl_vending_machine_date_received_d.SelectedValue, ddl_vending_machine_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prIncomeAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@incomecateg_id", SqlDbType.Int).Value = 5;
            cmd.Parameters.Add("@income_date_received", SqlDbType.DateTime).Value = date_received;
            cmd.Parameters.Add("@income_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_vending_machine_m.Text.Replace(",", "."));


            cmd.Parameters.Add("@income_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_vending_machine.Text);
            cmd.Parameters.Add("@income_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_vending_machine.Text);

            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }



        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        DateTime income_date_received = new DateTime();
        tiger.Date df = new tiger.Date();
        income_date_received = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


        //////////////////  tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_Income.DataSource = hp.getIncomeMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);
        gv_Income.DataBind();


        lbl_total_month_income.Text = hp.getTotalMonthIncome(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_income_reference.Visible = false;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_cash_machine_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime date_received = new DateTime();
        tiger.Date d = new tiger.Date();
        date_received = Convert.ToDateTime(d.DateCulture(ddl_cash_machine_date_received_m.SelectedValue, ddl_cash_machine_date_received_d.SelectedValue, ddl_cash_machine_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prIncomeAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@incomecateg_id", SqlDbType.Int).Value = 6;
            cmd.Parameters.Add("@income_date_received", SqlDbType.DateTime).Value = date_received;
            cmd.Parameters.Add("@income_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_cash_machine_m.Text.Replace(",", "."));


            cmd.Parameters.Add("@income_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_cash_machine.Text);
            cmd.Parameters.Add("@income_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_cash_machine.Text);

            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }


        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        DateTime income_date_received = new DateTime();
        tiger.Date df = new tiger.Date();
        income_date_received = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        //////////////////  tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_Income.DataSource = hp.getIncomeMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);
        gv_Income.DataBind();


        lbl_total_month_income.Text = hp.getTotalMonthIncome(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_income_reference.Visible = false;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_other_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime date_received = new DateTime();
        tiger.Date d = new tiger.Date();
        date_received = Convert.ToDateTime(d.DateCulture(ddl_other_date_received_m.SelectedValue, ddl_other_date_received_d.SelectedValue, ddl_other_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prIncomeAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@incomecateg_id", SqlDbType.Int).Value = 7;
            cmd.Parameters.Add("@income_date_received", SqlDbType.DateTime).Value = date_received;
            cmd.Parameters.Add("@income_amount", SqlDbType.Money).Value = Convert.ToDecimal(tbx_other_m.Text.Replace(",", "."));

            cmd.Parameters.Add("@income_reference", SqlDbType.NVarChar, 50).Value = Convert.ToString(tbx_ref_other.Text);
            cmd.Parameters.Add("@income_comments", SqlDbType.Text).Value = Convert.ToString(tbx_com_other.Text);

            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }


        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        DateTime income_date_received = new DateTime();
        tiger.Date df = new tiger.Date();
        income_date_received = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        //////////////////  tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_Income.DataSource = hp.getIncomeMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);
        gv_Income.DataBind();


        lbl_total_month_income.Text = hp.getTotalMonthIncome(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);


        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_income_reference.Visible = false;
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetIncomeCateg(int incomecateg_id)
    {
        string incomecateg = "";

        switch (incomecateg_id)
        {
            case 1:
                incomecateg = Resources.Resource.lbl_garage;
                break;
            case 2:
                incomecateg = Resources.Resource.lbl_parking;
                break;
            case 3:
                incomecateg = Resources.Resource.lbl_laundry;
                break;

            case 4:
                incomecateg = Resources.Resource.lbl_storage;
                break;
            case 5:
                incomecateg = Resources.Resource.lbl_vending_machine;
                break;
            case 6:
                incomecateg = Resources.Resource.lbl_cash_machine;
                break;

            case 7:
                incomecateg = Resources.Resource.lbl_other;
                break;

        }

        return incomecateg;
    }


    protected void btn_view_graph_Click(object sender, EventArgs e)
    {
        lbl_confirmation.Visible = false;

        Label57.Visible = false;
        Label58.Visible = false;
        Label59.Visible = false;
        Label60.Visible = false;
        Label7.Visible = false;
        Label56.Visible = false;
        Label53.Visible = false;
        Label55.Visible = false;



        lbl_amount.Visible = false;
        lbl_date_paid.Visible = false;
        lbl_income_reference.Visible = false;

        Response.Redirect("financial_income_graph.aspx?h_id=" + ddl_home_id.SelectedValue + "&m=" + ddl_date_received_m.SelectedValue
                            + "&d=1" + "&y=" + ddl_date_received_y.SelectedValue);
    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_cancel_Click(object sender, EventArgs e)
    {

        //*********************************************************************
        //*********************************************************************
        Button btn_cancel = (Button)sender;
        GridViewRow grdRow = (GridViewRow)btn_cancel.Parent.Parent;

        //  string strField1 = grdRow.Cells[6].Text;

        HiddenField h_income_id = (HiddenField)grdRow.Cells[6].FindControl("h_income_id");
        HiddenField h_income_date_received = (HiddenField)grdRow.Cells[6].FindControl("h_income_date_received");
        HiddenField h_income_amount = (HiddenField)grdRow.Cells[6].FindControl("h_income_amount");
        HiddenField h_income_reference = (HiddenField)grdRow.Cells[6].FindControl("h_income_reference");
        HiddenField h_income_categ = (HiddenField)grdRow.Cells[6].FindControl("h_income_categ");


        lbl_confirmation.Text = Resources.Resource.lbl_cancel_confirmation;

        Label57.Text = Resources.Resource.txt_date;
        Label7.Text = ":";
        Label56.Text = ":";
        Label58.Text = ":";
        Label59.Text = Resources.Resource.lbl_reference;
        Label60.Text = ":";
        Label53.Text = h_income_categ.Value;

        Label55.Text = Resources.Resource.lbl_amount;



        lbl_amount.Text = h_income_amount.Value;
        lbl_date_paid.Text = h_income_date_received.Value;
        lbl_income_reference.Text = h_income_reference.Value;



        lbl_confirmation.Visible = true;

        Label57.Visible = true;
        Label58.Visible = true;
        Label59.Visible = true;
        Label60.Visible = true;
        Label7.Visible = true;
        Label56.Visible = true;
        Label53.Visible = true;
        Label55.Visible = true;



        lbl_amount.Visible = true;
        lbl_date_paid.Visible = true;
        lbl_income_reference.Visible = true;

        //------------------------------------------------------------------------------------

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prCancelIncome", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@income_id", SqlDbType.Int).Value = Convert.ToInt32(h_income_id.Value);

            //execute the insert
            cmd.ExecuteReader();

        }
        catch (Exception error)
        {
            //  tiger.security.Error.errorAdd(conn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
        }

        // to view the incomes in the  home
        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        DateTime income_date_received = new DateTime();
        tiger.Date df = new tiger.Date();
        income_date_received = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        //////////////////  tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_Income.DataSource = hp.getIncomeMonthView(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);
        gv_Income.DataBind();

        lbl_total_month_income.Text = hp.getTotalMonthIncome(Convert.ToInt32(ddl_home_id.SelectedValue), Convert.ToInt32(Session["schema_id"]), income_date_received);



    }

}


