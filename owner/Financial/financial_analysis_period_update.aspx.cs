﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : april 16, 2008
/// </summary>

public partial class manager_Financial_financial_analysis_period_update : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!(Page.IsPostBack))
        {
            string maison = "";

            //------------------------------------------------------------------------------

            DateTime to = new DateTime();
            DateTime from = new DateTime();
            to = DateTime.Now; // the date in the to drop downlist

            tiger.Date d = new tiger.Date();
            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime

            int home_id = 0;
            //------------------------------------------------------------------------------
            //------------------------------------------------------------------------------



            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prFinancialAnalysisPeriodId", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@period_analysis_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["pa_id"]);

            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);



                while (dr.Read() == true)
                {

                    //tb = Convert.ToInt32(dr["home_id"]);
                    // h_h_id.Value = home_id.ToString();


                    from = Convert.ToDateTime(dr["period_analysis_date_begin"]);
                    to = Convert.ToDateTime(dr["period_analysis_date_end"]);

                    to = Convert.ToDateTime(d.DateCulture(to.Month.ToString(), to.Day.ToString(), to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
                    from = Convert.ToDateTime(d.DateCulture(from.Month.ToString(), from.Day.ToString(), to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));

                    home_id = Convert.ToInt32(dr["home_id"]);
                    h_home_id.Value = home_id.ToString();

                    lbl_property_name.Text = String.Format("{0:0.00}", dr["home_name"]);

                    tbx_analysis_name.Text = String.Format("{0:0.00}", dr["period_analysis_name"]); ;
                    tbx_analysis_comments.Text = String.Format("{0:0.00}", dr["period_analysis_comments"]);

                    tbx_debt_service_m.Text = String.Format("{0:0.00}", dr["period_analysis_debt_service"]);

                    tbx_capitalisation_m.Text = String.Format("{0:0.00}", dr["period_analysis_capitalisation"]);

                    tbx_added_value_m.Text = String.Format("{0:0.00}", dr["period_analysis_added_value"]);

                    tbx_home_value.Text = String.Format("{0:0.00}",dr["period_analysis_property_value"]);
                }

            }

            finally
            {
                conn.Close();
            }




            //------------------------------------------------------------------------------
            //------------------------------------------------------------------------------




            ddl_from_m.SelectedValue = from.Month.ToString();
            ddl_from_d.SelectedValue = from.Day.ToString();
            ddl_from_y.SelectedValue = from.Year.ToString();

            ddl_to_m.SelectedValue = to.Month.ToString();
            ddl_to_d.SelectedValue = to.Day.ToString();
            ddl_to_y.SelectedValue = to.Year.ToString();



            lbl_from2.Text = ddl_from_m.SelectedValue + "-" + ddl_from_d.SelectedValue + "-" + ddl_from_y.SelectedValue;
            lbl_to2.Text = ddl_to_m.SelectedValue + "-" + ddl_to_d.SelectedValue + "-" + ddl_to_y.SelectedValue; ;


            lbl_from3.Text = ddl_from_m.SelectedValue + "-" + ddl_from_d.SelectedValue + "-" + ddl_from_y.SelectedValue;
            lbl_to3.Text = ddl_to_m.SelectedValue + "-" + ddl_to_d.SelectedValue + "-" + ddl_to_y.SelectedValue; ;


            lbl_from4.Text = ddl_from_m.SelectedValue + "-" + ddl_from_d.SelectedValue + "-" + ddl_from_y.SelectedValue;
            lbl_to4.Text = ddl_to_m.SelectedValue + "-" + ddl_to_d.SelectedValue + "-" + ddl_to_y.SelectedValue; ;



            tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = h.getHomeCount(Convert.ToInt32(Session["schema_id"]));

            if (home_count > 0)
            {


                tiger.Financial f = new tiger.Financial(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                rNumberUnitbyBedroomNumber.DataSource = f.getNumberUnitbyBedroomNumberTotalMonthlyRentIncomePeriod(Convert.ToInt32(Session["schema_id"]), home_id, from, to);
                rNumberUnitbyBedroomNumber.DataBind();


                ////////////////////////////////


                // tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rExpense.DataSource = f.getExpenseMonthViewGroupByCategPeriod(home_id, Convert.ToInt32(Session["schema_id"]), from, to);
                rExpense.DataBind();


                /////////////////////////////////////////////////////////////

                //To view the address of the property

                tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
                rhome_view.DataBind();





                // tiger.Financial hpp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rIncome.DataSource = f.getIncomeMonthViewGroupByCategPeriod(home_id, Convert.ToInt32(Session["schema_id"]), from, to);

                rIncome.DataBind();

                lbl_late_fee.Text = String.Format("{0:0.00}", Convert.ToDouble(f.getPaidLateRentFeeHomeSumPeriod(home_id, Convert.ToInt32(Session["schema_id"]), from, to)));


                //// CALCULATION OF TOTAL INCOME
                decimal month_total = 0;
                double number_of_unit = 0;

                for (int i = 0; i < rNumberUnitbyBedroomNumber.Items.Count; i++)
                {

                    HiddenField h_number_of_unit = (HiddenField)rNumberUnitbyBedroomNumber.Items[i].FindControl("h_number_of_unit");
                    Label bedrooms_total_rent_m = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_m");
                    month_total = month_total + Convert.ToDecimal(bedrooms_total_rent_m.Text);
                    number_of_unit = number_of_unit + Convert.ToDouble(h_number_of_unit.Value);
                }


                h_total_number_of_unit.Value = number_of_unit.ToString();






                for (int i = 0; i < rIncome.Items.Count; i++)
                {
                    HiddenField h_income_amount = (HiddenField)rIncome.Items[i].FindControl("h_income_amount");
                    month_total = month_total + Convert.ToDecimal(h_income_amount.Value);
                }



                month_total = month_total + Convert.ToDecimal(lbl_late_fee.Text);
                lbl_gi_m.Text = String.Format("{0:0.00}", month_total);

                //// END OF CALCULATION OF TOTAL INCOME


                //// CALCULATION OF TOTAL EXPENSES

                decimal month_total_expense = 0;

                for (int i = 0; i < rExpense.Items.Count; i++)
                {
                    HiddenField h_expense_amount = (HiddenField)rExpense.Items[i].FindControl("h_expense_amount");
                    month_total_expense = month_total_expense + Convert.ToDecimal(h_expense_amount.Value);
                }

                lbl_total_expenses_m.Text = String.Format("{0:0.00}", month_total_expense);

                //// END OF CALCULATION OF TOTAL EXPENSE

                decimal net_income = 0;

                net_income = month_total - month_total_expense;

                lbl_net_operating_inc_m.Text = String.Format("{0:0.00}", net_income);

                lbl_liquidity_m.Text = "";

            }
            // if ther is no home

            else
            {
                //  txt_message.InnerHtml = "There is no property -- Add a property";

                //  txt_link.InnerHtml = home_count + " <a href='home_add.aspx'>Add Home</a>&nbsp;&nbsp;<a href='owner_add.aspx'>Add Owner</a>&nbsp;&nbsp;<a href='storage_add.aspx'>Add storage</a>&nbsp;&nbsp;";
            }


            //MONEY FLOW CALCULATION

            decimal liquidity = 0;

            if (tbx_debt_service_m.Text == "")
                tbx_debt_service_m.Text = "0";


            liquidity = Convert.ToDecimal(lbl_net_operating_inc_m.Text) - Convert.ToDecimal(tbx_debt_service_m.Text);

            lbl_liquidity_m.Text = String.Format("{0:0.00}", liquidity);


            // Generated liquidity + capitalisation
            lbl_liquidity_capitalisation_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_liquidity_m.Text) + Convert.ToDecimal(tbx_capitalisation_m.Text)));

            //Liquidity + Cap. +Added value
            lbl_liq_cap_value_m.Text = String.Format("{0:0.00}", (Convert.ToDecimal(lbl_liquidity_capitalisation_m.Text) + Convert.ToDecimal(tbx_added_value_m.Text)));


            // ANALYSIS OF RETURN CALCULATION
            {



                double property_value = 0;
                double added_value = 0;
                double total_number_of_unit = Convert.ToDouble(h_total_number_of_unit.Value);
                double appartment_cost = 0;
                property_value = Convert.ToDouble(tbx_home_value.Text);


                property_value = property_value + Convert.ToDouble(tbx_added_value_m.Text);

                appartment_cost = (property_value / total_number_of_unit);

                lbl_appartment_cost.Text = String.Format("{0:0.00}", appartment_cost);
                lbl_mbre.Text = String.Format("{0:0.00}",((property_value / Convert.ToDouble(lbl_gi_m.Text))));
                lbl_rde.Text = String.Format("{0:0.00}",((Convert.ToDouble(lbl_total_expenses_m.Text) / Convert.ToDouble(lbl_gi_m.Text))));

                //  lbl_tmo.Text = Convert.ToString((Convert.ToDouble(lbl_total_expenses_m.Text) + Convert.ToDouble(tbx_debt_service_m.Text)) / Convert.ToDouble(lbl_pgi_y.Text));

                lbl_trn.Text = String.Format("{0:0.00}",(Convert.ToDouble(lbl_net_operating_inc_m.Text) / property_value));

                lbl_mrn.Text = String.Format("{0:0.00}",(property_value / Convert.ToDouble(lbl_net_operating_inc_m.Text)));

                lbl_rcd.Text = String.Format("{0:0.00}", ((Convert.ToDouble(lbl_net_operating_inc_m.Text) / Convert.ToDouble(tbx_debt_service_m.Text))));

            }



        }

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetExpenseCateg(int expensecateg_id)
    {
        string expensecateg = "";

        switch (expensecateg_id)
        {
            case 1:
                expensecateg = Resources.Resource.lbl_electricity;
                break;
            case 2:
                expensecateg = Resources.Resource.lbl_energy;
                break;
            case 3:
                expensecateg = Resources.Resource.lbl_insurances;
                break;

            case 4:
                expensecateg = Resources.Resource.lbl_janitor;
                break;
            case 5:
                expensecateg = Resources.Resource.lbl_taxes;
                break;
            case 6:
                expensecateg = Resources.Resource.lbl_maintenance_repair;
                break;


            case 7:
                expensecateg = Resources.Resource.lbl_school_taxes;
                break;
            case 8:
                expensecateg = Resources.Resource.lbl_management;
                break;
            case 9:
                expensecateg = Resources.Resource.lbl_advertising;
                break;



            case 10:
                expensecateg = Resources.Resource.lbl_legal;
                break;
            case 11:
                expensecateg = Resources.Resource.lbl_accounting;
                break;
            case 12:
                expensecateg = Resources.Resource.lbl_other;
                break;

        }

        return expensecateg;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetIncomeCateg(int incomecateg_id)
    {
        string incomecateg = "";

        switch (incomecateg_id)
        {
            case 1:
                incomecateg = Resources.Resource.lbl_garage;
                break;
            case 2:
                incomecateg = Resources.Resource.lbl_parking;
                break;
            case 3:
                incomecateg = Resources.Resource.lbl_laundry;
                break;

            case 4:
                incomecateg = Resources.Resource.lbl_storage;
                break;
            case 5:
                incomecateg = Resources.Resource.lbl_vending_machine;
                break;
            case 6:
                incomecateg = Resources.Resource.lbl_cash_machine;
                break;

            case 7:
                incomecateg = Resources.Resource.lbl_other;
                break;

        }

        return incomecateg;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Caculate_Click(object sender, EventArgs e)
    {
        decimal liquidity = 0;

        if (tbx_debt_service_m.Text == "")
            tbx_debt_service_m.Text = "0";


        liquidity = Convert.ToDecimal(lbl_net_operating_inc_m.Text) - Convert.ToDecimal(tbx_debt_service_m.Text);

        lbl_liquidity_m.Text = String.Format("{0:0.00}", liquidity);


        // Generated liquidity + capitalisation
        lbl_liquidity_capitalisation_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_liquidity_m.Text) + Convert.ToDecimal(tbx_capitalisation_m.Text)));

        //Liquidity + Cap. +Added value
        lbl_liq_cap_value_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_liquidity_capitalisation_m.Text) + Convert.ToDecimal(tbx_added_value_m.Text)));


        lbl_appartment_cost.Text = "";
        lbl_mbre.Text = "";
        lbl_rde.Text = "";
        lbl_trn.Text = "";
        lbl_mrn.Text = "";
        lbl_rcd.Text = "";

    }
    protected void btn_view_Click(object sender, EventArgs e)
    {

        lbl_from2.Text = ddl_from_m.SelectedValue + "-" + ddl_from_d.SelectedValue + "-" + ddl_from_y.SelectedValue;
        lbl_to2.Text = ddl_to_m.SelectedValue + "-" + ddl_to_d.SelectedValue + "-" + ddl_to_y.SelectedValue; ;


        lbl_from3.Text = ddl_from_m.SelectedValue + "-" + ddl_from_d.SelectedValue + "-" + ddl_from_y.SelectedValue;
        lbl_to3.Text = ddl_to_m.SelectedValue + "-" + ddl_to_d.SelectedValue + "-" + ddl_to_y.SelectedValue; ;


        lbl_from4.Text = ddl_from_m.SelectedValue + "-" + ddl_from_d.SelectedValue + "-" + ddl_from_y.SelectedValue;
        lbl_to4.Text = ddl_to_m.SelectedValue + "-" + ddl_to_d.SelectedValue + "-" + ddl_to_y.SelectedValue; ;


        //------------------------------------------------------------------------------

        DateTime to = new DateTime();
        DateTime from = new DateTime();
        // to = DateTime.Now; // the date in the to drop downlist

        tiger.Date d = new tiger.Date();
        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        // to view the incomes in the  home
        //////////////////////////////////////// // to view the total rent incomes in the  home  ////////////////////////////

        tiger.Financial f = new tiger.Financial(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        rNumberUnitbyBedroomNumber.DataSource = f.getNumberUnitbyBedroomNumberTotalMonthlyRentIncomePeriod(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(h_home_id.Value), from, to);

        rNumberUnitbyBedroomNumber.DataBind();

        /////////////////////////////////////////////////////////


        // tiger.Financial hp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rExpense.DataSource = f.getExpenseMonthViewGroupByCategPeriod(Convert.ToInt32(h_home_id.Value), Convert.ToInt32(Session["schema_id"]), from, to);

        rExpense.DataBind();

        ///////////////////////////////////////////////////

        //To view the address of the property

        tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(h_home_id.Value));
        rhome_view.DataBind();


        // to view the incomes in the  home
        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        // DateTime income_date_received = new DateTime();
        //// tiger.Date df = new tiger.Date();
        // income_date_received = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        // tiger.Financial hpp = new tiger.Financial(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rIncome.DataSource = f.getIncomeMonthViewGroupByCategPeriod(Convert.ToInt32(h_home_id.Value), Convert.ToInt32(Session["schema_id"]), from, to);
        rIncome.DataBind();




        // lbl_total_month_income.Text = hp.getTotalMonthIncome(, Convert.ToInt32(Session["schema_id"]), income_date_received);
        lbl_late_fee.Text = String.Format("{0:0.00}", Convert.ToDouble(f.getPaidLateRentFeeHomeSumPeriod(Convert.ToInt32(h_home_id.Value), Convert.ToInt32(Session["schema_id"]), from, to)));
        //// CALCULATION OF TOTAL INCOME
        decimal month_total = 0;

        double number_of_unit = 0;

        for (int i = 0; i < rNumberUnitbyBedroomNumber.Items.Count; i++)
        {

            HiddenField h_number_of_unit = (HiddenField)rNumberUnitbyBedroomNumber.Items[i].FindControl("h_number_of_unit");
            Label bedrooms_total_rent_m = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_m");
            month_total = month_total + Convert.ToDecimal(bedrooms_total_rent_m.Text);
            number_of_unit = number_of_unit + Convert.ToDouble(h_number_of_unit.Value);
        }


        h_total_number_of_unit.Value = number_of_unit.ToString();

        for (int i = 0; i < rIncome.Items.Count; i++)
        {
            HiddenField h_income_amount = (HiddenField)rIncome.Items[i].FindControl("h_income_amount");
            month_total = month_total + Convert.ToDecimal(h_income_amount.Value);
        }



        month_total = month_total + Convert.ToDecimal(lbl_late_fee.Text);
        lbl_gi_m.Text = String.Format("{0:0.00}", month_total);

        //// END OF CALCULATION OF TOTAL INCOME

        //// CALCULATION OF TOTAL EXPENSES

        decimal month_total_expense = 0;

        for (int i = 0; i < rExpense.Items.Count; i++)
        {
            HiddenField h_expense_amount = (HiddenField)rExpense.Items[i].FindControl("h_expense_amount");
            month_total_expense = month_total_expense + Convert.ToDecimal(h_expense_amount.Value);
        }

        lbl_total_expenses_m.Text = String.Format("{0:0.00}", month_total_expense);

        //// END OF CALCULATION OF TOTAL EXPENSE


        decimal net_income = 0;

        net_income = month_total - month_total_expense;

        lbl_net_operating_inc_m.Text = String.Format("{0:0.00}", net_income);

        lbl_liquidity_m.Text = "";

        lbl_liquidity_capitalisation_m.Text = "";
        //Liquidity + Cap. +Added value
        lbl_liq_cap_value_m.Text = "";
        tbx_added_value_m.Text = "";
        tbx_capitalisation_m.Text = "";
        tbx_debt_service_m.Text = "";
        lbl_appartment_cost.Text = "";
        lbl_mbre.Text = "";
        lbl_rde.Text = "";
        lbl_trn.Text = "";
        lbl_mrn.Text = "";
        lbl_rcd.Text = "";
    }
    protected void btn_return_Click(object sender, EventArgs e)
    {
        decimal month_total = 0;
        double number_of_unit = 0;

        for (int i = 0; i < rNumberUnitbyBedroomNumber.Items.Count; i++)
        {

            HiddenField h_number_of_unit = (HiddenField)rNumberUnitbyBedroomNumber.Items[i].FindControl("h_number_of_unit");
            Label bedrooms_total_rent_m = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_m");
            month_total = month_total + Convert.ToDecimal(bedrooms_total_rent_m.Text);
            number_of_unit = number_of_unit + Convert.ToDouble(h_number_of_unit.Value);
        }


        h_total_number_of_unit.Value = number_of_unit.ToString();

        // Analysis of Return calculations

        double property_value = 0;
        double added_value = 0;
        double total_number_of_unit = Convert.ToDouble(h_total_number_of_unit.Value);
        double appartment_cost = 0;
        property_value = Convert.ToDouble(tbx_home_value.Text);


        property_value = property_value + Convert.ToDouble(tbx_added_value_m.Text);

        appartment_cost = (property_value / total_number_of_unit);

        lbl_appartment_cost.Text = String.Format("{0:0.00}", appartment_cost);
        lbl_mbre.Text = String.Format("{0:0.00}", ((property_value / Convert.ToDouble(lbl_gi_m.Text))));
        lbl_rde.Text = String.Format("{0:0.00}",((Convert.ToDouble(lbl_total_expenses_m.Text) / Convert.ToDouble(lbl_gi_m.Text))));

        //  lbl_tmo.Text = Convert.ToString((Convert.ToDouble(lbl_total_expenses_m.Text) + Convert.ToDouble(tbx_debt_service_m.Text)) / Convert.ToDouble(lbl_pgi_y.Text));

        lbl_trn.Text = String.Format("{0:0.00}",(Convert.ToDouble(lbl_net_operating_inc_m.Text) / property_value));

        lbl_mrn.Text = String.Format("{0:0.00}",(property_value / Convert.ToDouble(lbl_net_operating_inc_m.Text)));

        lbl_rcd.Text = String.Format("{0:0.00}", ((Convert.ToDouble(lbl_net_operating_inc_m.Text) / Convert.ToDouble(tbx_debt_service_m.Text))));




    }
    protected void Update_Click(object sender, EventArgs e)
    {
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);


        DateTime date_begin = new DateTime();
        DateTime date_end = new DateTime();

        tiger.Date d = new tiger.Date();

        date_begin = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        date_end = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prFinancialAnalysisPeriodUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@period_analysis_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["pa_id"]);
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@period_analysis_name", SqlDbType.NVarChar, 50).Value = tbx_analysis_name.Text;
            cmd.Parameters.Add("@period_analysis_comments", SqlDbType.Text).Value = tbx_analysis_comments.Text;
            cmd.Parameters.Add("@period_analysis_property_value", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_home_value.Text);
            cmd.Parameters.Add("@period_analysis_date_begin", SqlDbType.DateTime).Value = date_begin;
            cmd.Parameters.Add("@period_analysis_date_end", SqlDbType.DateTime).Value = date_end;
            cmd.Parameters.Add("@period_analysis_debt_service", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_debt_service_m.Text);
            cmd.Parameters.Add("@period_analysis_capitalisation", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_capitalisation_m.Text);
            cmd.Parameters.Add("@period_analysis_added_value", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_added_value_m.Text);



            //execute the insert
            cmd.ExecuteReader();


        }
        catch (Exception error)
        {
            //  tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }

        // string url = "financial_analysis_period.aspx?h_id=" + h_h_id.Value + "&m=" + h_m.Value + "&d=" + h_d.Value + "&y=" + h_y.Value;

        lbl_appartment_cost.Text = "";
        lbl_mbre.Text = "";
        lbl_rde.Text = "";
        lbl_trn.Text = "";
        lbl_mrn.Text = "";
        lbl_rcd.Text = "";
    }



  
}
