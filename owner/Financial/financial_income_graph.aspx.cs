﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.OwnerObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : april 17 , 2008
/// </summary>
/// 
public partial class manager_Financial_financial_income_graph : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
       // dotnetCHARTING.DataEngine de = new dotnetCHARTING.DataEngine();

        if (!(Page.IsPostBack))
        {

            DateTime date = new DateTime();
            date = DateTime.Now;
            tiger.Date c = new tiger.Date();
            //if we are entering from the update page
            //-----------------------------------------------------
            if (Request.QueryString["h_id"] != "" && Request.QueryString["h_id"] != null && Request.QueryString["m"] != "" && Request.QueryString["m"] != null && Request.QueryString["y"] != "" && Request.QueryString["y"] != null)
            {
                OwnerObjectAuthorization homeAuthorization = new OwnerObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), Convert.ToInt32(Request.QueryString["h_id"])))
                {
                    Session.Abandon();
                    Response.Redirect("http://www.sinfocatiger.com/login.aspx");
                }
                ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
             

                date = Convert.ToDateTime(c.DateCulture(Request.QueryString["m"], Request.QueryString["d"], Request.QueryString["y"], Convert.ToString(Session["_lastCulture"])));

            }
            //-----------------------------------------------------


            ddl_date_received_m.SelectedValue = date.Month.ToString();
            ddl_date_received_y.SelectedValue = date.Year.ToString();


            ddl_dimension.SelectedValue = "3";

            tiger.Owner o = new tiger.Owner(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = o.getOwnerHomeCount(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
            string link_to_unit = "";
            if (home_count > 0)
            {
                int home_id = o.getOwnerHomeFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));

                //if we are entering from the update page
                //-----------------------------------------------------
                if (Request.QueryString["h_id"] != "" && Request.QueryString["h_id"] != null)
                {
                    home_id = Convert.ToInt32(Request.QueryString["h_id"]);

                    OwnerObjectAuthorization homeAuthorization = new OwnerObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


                    ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                    if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), home_id))
                    {
                        Session.Abandon();
                        Response.Redirect("http://www.sinfocatiger.com/login.aspx");
                    }
                    ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
             
                }
                //-----------------------------------------------------


                ddl_home_id.DataSource = o.getOwnerHomeList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
                ddl_home_id.SelectedValue = Convert.ToString(home_id);
                ddl_home_id.DataBind();



                //To view the address of the property

                tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
                rhome_view.DataBind();

                tiger.Date df = new tiger.Date();
                date = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

              /////////////////////////////////////////////////////////////////////////////////////////////
                Chart1.DefaultSeries.ConnectionString = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

                Chart1.Title = Resources.Resource.lbl_u_income;
                Chart1.XAxis.Label.Text = Resources.Resource.lbl_income_category;
                Chart1.YAxis.Label.Text = Resources.Resource.lbl_amount;
                Chart1.YAxis.FormatString = "{0:0.00}";
                Chart1.Type = dotnetCHARTING.ChartType.Pies;
                // Set default transparency
                Chart1.DefaultSeries.DefaultElement.Transparency = 20;

                // Set elements to explode
                Chart1.DefaultSeries.DefaultElement.ExplodeSlice = true;



                // Set labeling mode.
                Chart1.PieLabelMode = dotnetCHARTING.PieLabelMode.Outside;
                // Set show value
                // Chart1.DefaultSeries.DefaultElement.ShowValue = true;



                Chart1.Use3D = true;
                Chart1.TempDirectory = "temp";
                Chart1.Debug = true;
                //   Chart1.DateGrouping = TimeInterval.Day;
                Chart1.EmptyElementText = "N/A";
                // Set he chart size.
                Chart1.Width = 600;
                Chart1.Height = 350;

                Chart1.LegendBox.HeaderLabel.Text = Resources.Resource.lbl_category;

                //Add a series
                //  Chart1.Series.Name = GetExpenseCateg(Convert.ToInt32("<%YValue>"));
                Chart1.DefaultSeries.PaletteName = dotnetCHARTING.Palette.One;


                // relativeFreqSeries.DefaultElement.LabelTemplate = "%name\n%yPercentOfTotal";
                // Chart1.DefaultElement.LabelTemplate = "%name\n%yPercentOfTotal";

                Chart1.Series.StoredProcedure = "prIncomeMonthViewGroupByCategChart";
                //Chart1.Type = dotnetCHARTING.ChartType.Pie;
                //Set store procedure parameters, for third parameter other types are: Text,Date,Number,Currency,Double
                Chart1.Series.AddParameter("@schema_id", Convert.ToString(Session["schema_id"]), dotnetCHARTING.FieldType.Number);
                Chart1.Series.AddParameter("@home_id", home_id.ToString(), dotnetCHARTING.FieldType.Number);
                Chart1.Series.AddParameter("@lastCulture", Convert.ToString(Session["_lastCulture"]), dotnetCHARTING.FieldType.Text);
                Chart1.Series.AddParameter("@date_month_year", date.ToString(), dotnetCHARTING.FieldType.Date);

                // Dispalying the percentage
                Chart1.Series.DefaultElement.LabelTemplate = "%name\n%yPercentOfTotal";
                Chart1.Series.DefaultElement.ShowValue = true;

                Chart1.SeriesCollection.Add();





            /////////////////////////////////////////////////////////////////////////////////////////

            Label3.Text = Convert.ToString(Session["_lastCulture"]);
            }
            // if ther is no home

            else
            {
                //  txt_message.InnerHtml = "There is no property -- Add a property";

                //  txt_link.InnerHtml = home_count + " <a href='home_add.aspx'>Add Home</a>&nbsp;&nbsp;<a href='owner_add.aspx'>Add Owner</a>&nbsp;&nbsp;<a href='storage_add.aspx'>Add storage</a>&nbsp;&nbsp;";
            }

            
           
        }


        //Chart1.SeriesCollection.Add(getLiveData());
      
    }
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    dotnetCHARTING.SeriesCollection getLiveData()
    {
        dotnetCHARTING.DataEngine de = new dotnetCHARTING.DataEngine(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        //de.Series.Name = "Items";

        de.StoredProcedure = "prIncomeMonthViewGroupByCategChart";
        //Chart1.Type = dotnetCHARTING.ChartType.Pie;
        //Set store procedure parameters, for third parameter other types are: Text,Date,Number,Currency,Double
        de.AddParameter("@schema_id", "1", dotnetCHARTING.FieldType.Number);
        de.AddParameter("@home_id", "1", dotnetCHARTING.FieldType.Number);
        de.AddParameter("@lastCulture", Convert.ToString(Session["_lastCulture"]), dotnetCHARTING.FieldType.Text);
        de.AddParameter("@date_month_year", "4/10/08 12:00:00 AM", dotnetCHARTING.FieldType.Date);
     


        return de.GetSeries();
    }

/// <summary>
/// 
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        //To view the address of the property

        tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
        rhome_view.DataBind();

        ddl_dimension.SelectedValue = "3";


        // to view the incomes in the  home
        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        DateTime date = new DateTime();
        date = DateTime.Now;
        //set global properties

        ddl_date_received_m.SelectedValue = date.Month.ToString();
        ddl_date_received_y.SelectedValue = date.Year.ToString();

        tiger.Date df = new tiger.Date();
        date = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        /////////////////////////////////////////////////////////////////////////////////////////////
        Chart1.DefaultSeries.ConnectionString = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        Chart1.Title = Resources.Resource.lbl_u_income;
        Chart1.XAxis.Label.Text = Resources.Resource.lbl_income_category;
        Chart1.YAxis.Label.Text = Resources.Resource.lbl_amount;
        Chart1.YAxis.FormatString = "{0:0.00}";
        Chart1.Type = dotnetCHARTING.ChartType.Pies;
        // Set default transparency
        Chart1.DefaultSeries.DefaultElement.Transparency = 20;

        // Set elements to explode
        Chart1.DefaultSeries.DefaultElement.ExplodeSlice = true;



        // Set labeling mode.
        Chart1.PieLabelMode = dotnetCHARTING.PieLabelMode.Outside;
        // Set show value
        // Chart1.DefaultSeries.DefaultElement.ShowValue = true;



        Chart1.Use3D = true;
        Chart1.TempDirectory = "temp";
        Chart1.Debug = true;
        //   Chart1.DateGrouping = TimeInterval.Day;
        Chart1.EmptyElementText = "N/A";
        // Set he chart size.
        Chart1.Width = 600;
        Chart1.Height = 350;

        Chart1.LegendBox.HeaderLabel.Text = Resources.Resource.lbl_category;

        //Add a series
        //  Chart1.Series.Name = GetExpenseCateg(Convert.ToInt32("<%YValue>"));
        Chart1.DefaultSeries.PaletteName = dotnetCHARTING.Palette.One;


        // relativeFreqSeries.DefaultElement.LabelTemplate = "%name\n%yPercentOfTotal";
        // Chart1.DefaultElement.LabelTemplate = "%name\n%yPercentOfTotal";

        Chart1.Series.StoredProcedure = "prIncomeMonthViewGroupByCategChart";
        //Chart1.Type = dotnetCHARTING.ChartType.Pie;
        //Set store procedure parameters, for third parameter other types are: Text,Date,Number,Currency,Double
        Chart1.Series.AddParameter("@schema_id", Convert.ToString(Session["schema_id"]), dotnetCHARTING.FieldType.Number);
        Chart1.Series.AddParameter("@home_id", ddl_home_id.SelectedValue, dotnetCHARTING.FieldType.Number);
        Chart1.Series.AddParameter("@lastCulture", Convert.ToString(Session["_lastCulture"]), dotnetCHARTING.FieldType.Text);
        Chart1.Series.AddParameter("@date_month_year", date.ToString(), dotnetCHARTING.FieldType.Date);

        // Dispalying the percentage
        Chart1.Series.DefaultElement.LabelTemplate = "%name\n%yPercentOfTotal";
        Chart1.Series.DefaultElement.ShowValue = true;

        Chart1.SeriesCollection.Add();





        /////////////////////////////////////////////////////////////////////////////////////////

        Label3.Text = Convert.ToString(Session["_lastCulture"]);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
  /*  protected void ddl_date_received_m_SelectedIndexChanged(object sender, EventArgs e)
    {
         //To view the address of the property

        tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
        rhome_view.DataBind();


        // to view the incomes in the  home
        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        DateTime date = new DateTime();
        tiger.Date df = new tiger.Date();
        date = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        //set global properties
        Chart1.DefaultSeries.ConnectionString = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        Chart1.Title = Chart1.Title = Resources.Resource.lbl_u_income;
        Chart1.XAxis.Label.Text = Resources.Resource.lbl_income_category;
        Chart1.YAxis.Label.Text = Resources.Resource.lbl_amount;
        Chart1.YAxis.FormatString = "{0:0.00}";
        Chart1.Use3D = true;
        Chart1.TempDirectory = "temp";
        Chart1.Debug = true;
        //   Chart1.DateGrouping = TimeInterval.Day;
        Chart1.EmptyElementText = "N/A";
        // Set he chart size.
        Chart1.Width = 600;
        Chart1.Height = 350;

        Chart1.LegendBox.HeaderLabel.Text = Resources.Resource.lbl_category;

        Chart1.DefaultElement.SmartLabel.Text = "<sqrt(%YValue)+10>";
        //Add a series
        Chart1.Series.Name = "Items";
        Chart1.DefaultSeries.PaletteName = dotnetCHARTING.Palette.One;
        Chart1.Series.StoredProcedure = "prIncomeMonthViewGroupByCategChart";
        //Chart1.Type = dotnetCHARTING.ChartType.Pie;
        //Set store procedure parameters, for third parameter other types are: Text,Date,Number,Currency,Double
        Chart1.Series.AddParameter("@schema_id",Convert.ToString(Session["schema_id"]), dotnetCHARTING.FieldType.Number);
        Chart1.Series.AddParameter("@home_id", ddl_home_id.SelectedValue, dotnetCHARTING.FieldType.Number);
        Chart1.Series.AddParameter("@lastCulture", Convert.ToString(Session["_lastCulture"]), dotnetCHARTING.FieldType.Text);
        Chart1.Series.AddParameter("@date_month_year", date.ToString(), dotnetCHARTING.FieldType.Date);

        Chart1.SeriesCollection.Add();

        /////////////////////////////////////////////////////////////////////////////////////////////
        Chart2.DefaultSeries.ConnectionString = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        Chart2.Title = Resources.Resource.lbl_u_income;
        Chart2.XAxis.Label.Text = Resources.Resource.lbl_income_category;
        Chart2.YAxis.Label.Text = Resources.Resource.lbl_amount;
        Chart2.YAxis.FormatString = "{0:0.00}";
        Chart2.Type = dotnetCHARTING.ChartType.Pies;
        // Set default transparency
        Chart2.DefaultSeries.DefaultElement.Transparency = 20;

        // Set elements to explode
        Chart2.DefaultSeries.DefaultElement.ExplodeSlice = true;



        // Set labeling mode.
        Chart2.PieLabelMode = dotnetCHARTING.PieLabelMode.Outside;
        // Set show value
        // Chart1.DefaultSeries.DefaultElement.ShowValue = true;



        Chart2.Use3D = true;
        Chart2.TempDirectory = "temp";
        Chart2.Debug = true;
        //   Chart1.DateGrouping = TimeInterval.Day;
        Chart2.EmptyElementText = "N/A";
        // Set he chart size.
        Chart2.Width = 600;
        Chart2.Height = 350;

        Chart2.LegendBox.HeaderLabel.Text = Resources.Resource.lbl_category;

        //Add a series
        //  Chart1.Series.Name = GetExpenseCateg(Convert.ToInt32("<%YValue>"));
        Chart2.DefaultSeries.PaletteName = dotnetCHARTING.Palette.One;


        // relativeFreqSeries.DefaultElement.LabelTemplate = "%name\n%yPercentOfTotal";
        // Chart1.DefaultElement.LabelTemplate = "%name\n%yPercentOfTotal";

        Chart2.Series.StoredProcedure = "prIncomeMonthViewGroupByCategChart";
        //Chart1.Type = dotnetCHARTING.ChartType.Pie;
        //Set store procedure parameters, for third parameter other types are: Text,Date,Number,Currency,Double
        Chart2.Series.AddParameter("@schema_id", Convert.ToString(Session["schema_id"]), dotnetCHARTING.FieldType.Number);
        Chart2.Series.AddParameter("@home_id", ddl_home_id.SelectedValue, dotnetCHARTING.FieldType.Number);
        Chart2.Series.AddParameter("@lastCulture", Convert.ToString(Session["_lastCulture"]), dotnetCHARTING.FieldType.Text);
        Chart2.Series.AddParameter("@date_month_year", date.ToString(), dotnetCHARTING.FieldType.Date);

        // Dispalying the percentage
        Chart2.Series.DefaultElement.LabelTemplate = "%name\n%yPercentOfTotal";
        Chart2.Series.DefaultElement.ShowValue = true;

        Chart2.SeriesCollection.Add();





        /////////////////////////////////////////////////////////////////////////////////////////

        Label3.Text = Convert.ToString(Session["_lastCulture"]);

    }
    protected void ddl_date_received_y_SelectedIndexChanged(object sender, EventArgs e)
    {
        //To view the address of the property

        tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
        rhome_view.DataBind();


        // to view the incomes in the  home
        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        DateTime date = new DateTime();
        tiger.Date df = new tiger.Date();
        date = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        //set global properties
        Chart1.DefaultSeries.ConnectionString = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        Chart1.Title = Resources.Resource.lbl_u_income;
        Chart1.XAxis.Label.Text = Resources.Resource.lbl_category;
        Chart1.YAxis.Label.Text = Resources.Resource.lbl_amount;
        Chart1.YAxis.FormatString = "{0:0.00}";
        Chart1.Use3D = true;
        Chart1.TempDirectory = "temp";
        Chart1.Debug = true;
        //   Chart1.DateGrouping = TimeInterval.Day;
        Chart1.EmptyElementText = "N/A";
        // Set he chart size.
        Chart1.Width = 600;
        Chart1.Height = 350;

        Chart1.LegendBox.HeaderLabel.Text = Resources.Resource.lbl_category;

        //Add a series
        Chart1.Series.Name = "Items";
        Chart1.DefaultSeries.PaletteName = dotnetCHARTING.Palette.One;
        Chart1.Series.StoredProcedure = "prIncomeMonthViewGroupByCategChart";
        //Chart1.Type = dotnetCHARTING.ChartType.Pie;
        //Set store procedure parameters, for third parameter other types are: Text,Date,Number,Currency,Double
        Chart1.Series.AddParameter("@schema_id", Convert.ToString(Session["schema_id"]), dotnetCHARTING.FieldType.Number);
        Chart1.Series.AddParameter("@home_id", ddl_home_id.SelectedValue, dotnetCHARTING.FieldType.Number);
        Chart1.Series.AddParameter("@lastCulture", Convert.ToString(Session["_lastCulture"]), dotnetCHARTING.FieldType.Text);
        Chart1.Series.AddParameter("@date_month_year", date.ToString(), dotnetCHARTING.FieldType.Date);

        Chart1.SeriesCollection.Add();

        /////////////////////////////////////////////////////////////////////////////////////////////
        Chart2.DefaultSeries.ConnectionString = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        Chart2.Title = Resources.Resource.lbl_u_income;
        Chart2.XAxis.Label.Text = Resources.Resource.lbl_income_category;
        Chart2.YAxis.Label.Text = Resources.Resource.lbl_amount;
        Chart2.YAxis.FormatString = "{0:0.00}";
        Chart2.Type = dotnetCHARTING.ChartType.Pies;
        // Set default transparency
        Chart2.DefaultSeries.DefaultElement.Transparency = 20;

        // Set elements to explode
        Chart2.DefaultSeries.DefaultElement.ExplodeSlice = true;



        // Set labeling mode.
        Chart2.PieLabelMode = dotnetCHARTING.PieLabelMode.Outside;
        // Set show value
        // Chart1.DefaultSeries.DefaultElement.ShowValue = true;



        Chart2.Use3D = true;
        Chart2.TempDirectory = "temp";
        Chart2.Debug = true;
        //   Chart1.DateGrouping = TimeInterval.Day;
        Chart2.EmptyElementText = "N/A";
        // Set he chart size.
        Chart2.Width = 600;
        Chart2.Height = 350;

        Chart2.LegendBox.HeaderLabel.Text = Resources.Resource.lbl_category;

        //Add a series
        //  Chart1.Series.Name = GetExpenseCateg(Convert.ToInt32("<%YValue>"));
        Chart2.DefaultSeries.PaletteName = dotnetCHARTING.Palette.One;


        // relativeFreqSeries.DefaultElement.LabelTemplate = "%name\n%yPercentOfTotal";
        // Chart1.DefaultElement.LabelTemplate = "%name\n%yPercentOfTotal";

        Chart2.Series.StoredProcedure = "prIncomeMonthViewGroupByCategChart";
        //Chart1.Type = dotnetCHARTING.ChartType.Pie;
        //Set store procedure parameters, for third parameter other types are: Text,Date,Number,Currency,Double
        Chart2.Series.AddParameter("@schema_id", Convert.ToString(Session["schema_id"]), dotnetCHARTING.FieldType.Number);
        Chart2.Series.AddParameter("@home_id", ddl_home_id.SelectedValue, dotnetCHARTING.FieldType.Number);
        Chart2.Series.AddParameter("@lastCulture", Convert.ToString(Session["_lastCulture"]), dotnetCHARTING.FieldType.Text);
        Chart2.Series.AddParameter("@date_month_year", date.ToString(), dotnetCHARTING.FieldType.Date);

        // Dispalying the percentage
        Chart2.Series.DefaultElement.LabelTemplate = "%name\n%yPercentOfTotal";
        Chart2.Series.DefaultElement.ShowValue = true;

        Chart2.SeriesCollection.Add();





        /////////////////////////////////////////////////////////////////////////////////////////
     
    }
   * 
   */


   /// <summary>
   /// 
   /// </summary>
   /// <param name="sender"></param>
   /// <param name="e"></param>
    protected void btn_view_Click(object sender, EventArgs e)
    {

        
        // to view the incomes in the  home
        //////////////////////////////////////// // to view the incomes in the  home  ////////////////////////////
        DateTime date = new DateTime();
        tiger.Date df = new tiger.Date();
        date = Convert.ToDateTime(df.DateCulture(ddl_date_received_m.SelectedValue, "1", ddl_date_received_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        Chart1.DefaultSeries.ConnectionString = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        Chart1.Title = Resources.Resource.lbl_u_income;
        Chart1.XAxis.Label.Text = Resources.Resource.lbl_income_category;
        Chart1.YAxis.Label.Text = Resources.Resource.lbl_amount;
        Chart1.YAxis.FormatString = "{0:0.00}";

        if (ddl_chartype.SelectedValue == "1")
        {
            // IF WE HAVE A PIE CHART   
            Chart1.Type = dotnetCHARTING.ChartType.Pies;
            // Set elements to explode
            Chart1.DefaultSeries.DefaultElement.ExplodeSlice = true;
            // Set show value
            // Set labeling mode.
            Chart1.PieLabelMode = dotnetCHARTING.PieLabelMode.Outside;
        }

        // set the dimension of the chart
        if (ddl_dimension.SelectedValue == "3")
         Chart1.Use3D = true;
        
        Chart1.TempDirectory = "temp";
        Chart1.Debug = true;
        //   Chart1.DateGrouping = TimeInterval.Day;
        Chart1.EmptyElementText = "N/A";
        // Set he chart size.
        Chart1.Width = 600;
        Chart1.Height = 350;

        Chart1.LegendBox.HeaderLabel.Text = Resources.Resource.lbl_category;

        // Chart1.DefaultElement.SmartLabel.Text = "<sqrt(%YValue)+10>";


        //Add a series
        Chart1.Series.Name = "Items";
        Chart1.DefaultSeries.PaletteName = dotnetCHARTING.Palette.One;
        Chart1.Series.StoredProcedure = "prIncomeMonthViewGroupByCategChart";
        //Chart1.Type = dotnetCHARTING.ChartType.Pie;
        //Set store procedure parameters, for third parameter other types are: Text,Date,Number,Currency,Double
        Chart1.Series.AddParameter("@schema_id", Convert.ToString(Session["schema_id"]), dotnetCHARTING.FieldType.Number);
        Chart1.Series.AddParameter("@home_id", ddl_home_id.SelectedValue, dotnetCHARTING.FieldType.Number);
        Chart1.Series.AddParameter("@lastCulture", Convert.ToString(Session["_lastCulture"]), dotnetCHARTING.FieldType.Text);
        Chart1.Series.AddParameter("@date_month_year", date.ToString(), dotnetCHARTING.FieldType.Date);

        Chart1.Series.DefaultElement.LabelTemplate = "%name\n%yPercentOfTotal";
        Chart1.Series.DefaultElement.ShowValue = true;

        Chart1.SeriesCollection.Add();
    }
    protected void btn_view_graph_Click(object sender, EventArgs e)
    {

        Response.Redirect("financial_expenses_graph.aspx?h_id=" + ddl_home_id.SelectedValue + "&m=" + ddl_date_received_m.SelectedValue
                           + "&d=1" + "&y=" + ddl_date_received_y.SelectedValue);
    }
}
