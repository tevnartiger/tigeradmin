﻿<%@ Page Language="C#"  MaintainScrollPositionOnPostback="true" MasterPageFile="~/owner/owner_mp.master" AutoEventWireup="true" CodeFile="financial_analysis_period_update.aspx.cs" Inherits="manager_Financial_financial_analysis_period_update" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


UPDATE &nbsp;<asp:Label ID="Label2" runat="server" 
                    Text="<%$ Resources:Resource, lbl_financial_analysis%>" /> &nbsp;FOR A PERIOD<br /><br />
<table>
<tr> <asp:Label ID="lbl_property" runat="server" Text="<%$ Resources:Resource, lbl_property %>"/>
               :&nbsp;
    <asp:Label ID="lbl_property_name" runat="server" Text="Label"></asp:Label>
               </td>
                <td></td> 
</tr>
 </table><br />
 
  <table style="width: 100%">
        <tr>
            <td valign="top">
 <asp:Repeater runat="server" ID="rhome_view">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
        
        <tr>
          <td>
                 District&nbsp;:&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_district")%> 
                   </td>
            </tr>
           <tr>
              <td valign="top" >
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_no")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_street")%></td>
            </tr>
            <tr>
                 <td valign="top" >
                   <%#DataBinder.Eval(Container.DataItem, "home_city")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_prov")%></td>
               
            </tr>
            <tr>
                 <td valign="top" >
                   <%#DataBinder.Eval(Container.DataItem, "home_pc")%> 
                
                   </td>
               
            </tr>   
        </table>
        </ItemTemplate>
        </asp:Repeater>
            </td>
            <td valign="top">
                <table bgcolor="#ffffcc"  >
                    
                    <tr>
                    
                    
                      <td>
      &nbsp;<asp:Label ID="lbl_from" runat="server" Text="<%$ Resources:Resource, lbl_from %>"/>
                </td>
                <td>
                    <asp:DropDownList ID="ddl_from_m" runat="server">
                    <asp:ListItem Value="0">Month</asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                           
                    </asp:DropDownList>
&nbsp;/
                    <asp:DropDownList ID="ddl_from_d" runat="server">
                         <asp:ListItem Value="0">Day</asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
&nbsp;/
                    <asp:DropDownList ID="ddl_from_y" runat="server">
                    <asp:ListItem Value="0">Year</asp:ListItem>
                    <asp:ListItem>1980</asp:ListItem>
                    <asp:ListItem>1981</asp:ListItem>
                    <asp:ListItem>1982</asp:ListItem>
                    <asp:ListItem>1983</asp:ListItem>
                    <asp:ListItem>1984</asp:ListItem>
                    <asp:ListItem>1985</asp:ListItem>
                    <asp:ListItem>1986</asp:ListItem>
                    <asp:ListItem>1987</asp:ListItem>
                    <asp:ListItem>1988</asp:ListItem>
                    <asp:ListItem>1989</asp:ListItem>
                    <asp:ListItem>1990</asp:ListItem>
                    <asp:ListItem>1991</asp:ListItem>
                    <asp:ListItem>1992</asp:ListItem>
                    <asp:ListItem>1993</asp:ListItem>
                    <asp:ListItem>1994</asp:ListItem>
                    <asp:ListItem>1995</asp:ListItem>
                    <asp:ListItem>1996</asp:ListItem>
                    <asp:ListItem>1997</asp:ListItem>
                    <asp:ListItem>1998</asp:ListItem>
                    <asp:ListItem>1999</asp:ListItem>                    
                    <asp:ListItem>2000</asp:ListItem>
                    <asp:ListItem>2001</asp:ListItem>
                    <asp:ListItem>2002</asp:ListItem>
                    <asp:ListItem>2003</asp:ListItem>
                    <asp:ListItem>2004</asp:ListItem>
                    <asp:ListItem>2005</asp:ListItem>
                    <asp:ListItem>2006</asp:ListItem>
                    <asp:ListItem>2007</asp:ListItem>
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                    <asp:ListItem>2011</asp:ListItem>
                    <asp:ListItem>2012</asp:ListItem>
                    <asp:ListItem>2013</asp:ListItem>
                    <asp:ListItem>2014</asp:ListItem>
                    <asp:ListItem>2015</asp:ListItem>
                    </asp:DropDownList>
                </td>
                     
                    
                    </tr>
                    
                    <tr>
                    
                    
                       <td>
      <asp:Label ID="lbl_to" runat="server" Text="<%$ Resources:Resource, lbl_to %>"/>
                </td>
                <td>
                    <asp:DropDownList ID="ddl_to_m" runat="server">
                  
                    <asp:ListItem Value="0">Month</asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    
                    </asp:DropDownList>
&nbsp;/
                    <asp:DropDownList ID="ddl_to_d" runat="server">
                        <asp:ListItem Value="0">Day</asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
&nbsp;/
                    <asp:DropDownList ID="ddl_to_y" runat="server">
                    <asp:ListItem Value="0">Year</asp:ListItem>
                    <asp:ListItem>1980</asp:ListItem>
                    <asp:ListItem>1981</asp:ListItem>
                    <asp:ListItem>1982</asp:ListItem>
                    <asp:ListItem>1983</asp:ListItem>
                    <asp:ListItem>1984</asp:ListItem>
                    <asp:ListItem>1985</asp:ListItem>
                    <asp:ListItem>1986</asp:ListItem>
                    <asp:ListItem>1987</asp:ListItem>
                    <asp:ListItem>1988</asp:ListItem>
                    <asp:ListItem>1989</asp:ListItem>
                    <asp:ListItem>1990</asp:ListItem>
                    <asp:ListItem>1991</asp:ListItem>
                    <asp:ListItem>1992</asp:ListItem>
                    <asp:ListItem>1993</asp:ListItem>
                    <asp:ListItem>1994</asp:ListItem>
                    <asp:ListItem>1995</asp:ListItem>
                    <asp:ListItem>1996</asp:ListItem>
                    <asp:ListItem>1997</asp:ListItem>
                    <asp:ListItem>1998</asp:ListItem>
                    <asp:ListItem>1999</asp:ListItem>                    
                    <asp:ListItem>2000</asp:ListItem>
                    <asp:ListItem>2001</asp:ListItem>
                    <asp:ListItem>2002</asp:ListItem>
                    <asp:ListItem>2003</asp:ListItem>
                    <asp:ListItem>2004</asp:ListItem>
                    <asp:ListItem>2005</asp:ListItem>
                    <asp:ListItem>2006</asp:ListItem>
                    <asp:ListItem>2007</asp:ListItem>
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                    <asp:ListItem>2011</asp:ListItem>
                    <asp:ListItem>2012</asp:ListItem>
                    <asp:ListItem>2013</asp:ListItem>
                    <asp:ListItem>2014</asp:ListItem>
                    <asp:ListItem>2015</asp:ListItem>
                    </asp:DropDownList>
                </td>
                  <td>
&nbsp;&nbsp;
                                <asp:Button ID="btn_view" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_view %>" 
                          onclick="btn_view_Click"   />
                            </td>
                    
                    </tr>
                    
                    <tr>
                    
                    
                       <td>
                           Analysis name</td>
                <td>
                    <asp:TextBox ID="tbx_analysis_name" runat="server"></asp:TextBox>
                </td>
                  <td>
                      &nbsp;</td>
                    
                    </tr>
                    
                    <tr>
                    
                    
                       <td valign="top">
                           Analysis comments</td>
                <td>
                    <b>
                    <asp:TextBox ID="tbx_analysis_comments" runat="server" Height="73px" TextMode="MultiLine" 
                         Width="100%"></asp:TextBox>
                    </b>
                </td>
                  <td valign="top">
                      &nbsp;&nbsp;&nbsp;
                      <asp:Button ID="btn_update" runat="server" Text="<%$ Resources:Resource, lbl_update_analysis %>"
                          onclick="Update_Click"  />
                        </td>
                    
                    </tr>
                    
                    </table>
               
            </td>
        </tr>
    </table>
    <br />
    
    
    <table >
        <tr>
            <td>
                <asp:Label ID="lbl_property_value" runat="server" 
                    Text="<%$ Resources:Resource, lbl_property_value %>" style="font-weight: 700" ></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="tbx_home_value" runat="server" Width="65px"></asp:TextBox>
            </td>
        </tr>
        
    </table><br />
    
     <table style="width: 100%">
        <tr>
            <td bgcolor="AliceBlue">
                <asp:Label ID="lbl_revenue" runat="server" 
                    Text="<%$ Resources:Resource, lbl_revenue %>" style="font-weight: 700"/> </td>
            
            <td bgcolor="AliceBlue">
                <asp:Label ID="lbl_monthly" runat="server" 
                    Text="<%$ Resources:Resource, lbl_from %>" style="font-weight: 700"/>&nbsp;:&nbsp; 
                <asp:Label ID="lbl_from2" runat="server" Text="Label"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label34" runat="server" 
                    Text="<%$ Resources:Resource, lbl_to %>" style="font-weight: 700"/>&nbsp;:&nbsp;
                     <asp:Label ID="lbl_to2" runat="server" Text="Label"></asp:Label>
                    
                </td>
           
        </tr>
        
        
        <asp:Repeater ID="rNumberUnitbyBedroomNumber" runat="server">
    <ItemTemplate>
    <tr>
    <td> <%#DataBinder.Eval(Container.DataItem, "number_of_unit")%> &nbsp;X &nbsp;<%#DataBinder.Eval(Container.DataItem, "unit_bedroom_no")%>&nbsp;&nbsp; <asp:Label ID="lbl_bedroom" runat="server" 
                    Text="<%$ Resources:Resource, lbl_bedroom %>"/></td>
    
   
    <td><asp:Label ID="lbl_bedrooms_total_rent_m" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "total_rent_received","{0:0.00}")%>'  />
                     
        <asp:HiddenField ID="h_number_of_unit" Value='<%#DataBinder.Eval(Container.DataItem, "number_of_unit")%>' runat="server" /> 
        <asp:HiddenField ID="h_unit_bedroom_number" Value='<%#DataBinder.Eval(Container.DataItem, "unit_bedroom_no")%>' runat="server" />
        
                     </td>
    </tr>
    </ItemTemplate>
    </asp:Repeater>
    
  
   <asp:Repeater ID="rIncome"   runat="server">
    <ItemTemplate  >
        <tr  >
            <td valign="top" style="height: 18px">
                <asp:Label ID="Label40" runat="server" 
                    Text='<%#GetIncomeCateg(Convert.ToInt32(Eval("incomecateg_id")))%>'   />&nbsp;</td>
            <td valign="top"  >
                <%#DataBinder.Eval(Container.DataItem, "income_amount","{0:0.00}")%>
                <asp:HiddenField ID="h_income_amount" Value='<%#DataBinder.Eval(Container.DataItem, "income_amount")%>' runat="server" />
        
            </td>
           
           
        </tr>
        
        </ItemTemplate>
    </asp:Repeater>
  
  
        
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" 
                    Text="<%$ Resources:Resource, lbl_late_fee %>"/></td>
            
            <td>
                <asp:Label ID="lbl_late_fee" runat="server" Width="65px"></asp:Label>
            </td>
           
        </tr>
        
          <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            
        </tr>
        
        <tr>
            <td>
                <asp:Label ID="Label17" runat="server" 
                     Text="<%$ Resources:Resource, lbl_total_income %>" style="font-weight: 700"/>&nbsp;</td>
           
            <td>
               <asp:Label ID="lbl_gi_m" runat="server" /></td>
            
        </tr>
        
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
           
        </tr>
        
        <tr>
            <td bgcolor="AliceBlue">
                <asp:Label ID="Label5" runat="server" 
                    Text="<%$ Resources:Resource, lbl_expenses%>" style="font-weight: 700"/></td>
            
            <td bgcolor="AliceBlue">
                 <asp:Label ID="Label3" runat="server" 
                     Text="<%$ Resources:Resource, lbl_from %>" style="font-weight: 700"/>&nbsp;:&nbsp; 
                <asp:Label ID="lbl_from3" runat="server" Text="Label"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label7" runat="server" 
                    Text="<%$ Resources:Resource, lbl_to %>" style="font-weight: 700"/>&nbsp;:&nbsp;
                     <asp:Label ID="lbl_to3" runat="server" Text="Label"></asp:Label></td>
            
        </tr>
        
             
    <asp:Repeater ID="rExpense"   runat="server">
    <ItemTemplate  >
        <tr  >
            <td valign="top" style="height: 18px">
                <asp:Label ID="Label40" runat="server" 
                    Text='<%#GetExpenseCateg(Convert.ToInt32(Eval("expensecateg_id")))%>'   />&nbsp;</td>
            <td valign="top"  >
                <%#DataBinder.Eval(Container.DataItem, "expense_amount","{0:0.00}")%>
                
                <asp:HiddenField ID="h_expense_amount" Value='<%#DataBinder.Eval(Container.DataItem, "expense_amount")%>' runat="server" />
        
            </td>
          
            
        </tr>
        
        </ItemTemplate>
    </asp:Repeater>
       
        <tr>
            <td style="height: 18px">
                &nbsp;</td>
            <td style="height: 18px">
                &nbsp;</td>
            
        </tr>
    
     <tr>
            <td style="height: 18px">
                <asp:Label ID="Label15" runat="server" 
                    Text="<%$ Resources:Resource, lbl_total_expenses %>" 
                    style="font-weight: 700"/></td>
            
            <td  >
                <asp:Label ID="lbl_total_expenses_m" runat="server" 
                    Text="lbl_total_expenses_m" /></td>
            
        </tr>
        <tr>
            <td style="height: 18px">
                <br />
            </td>
            <td style="height: 18px">
                &nbsp;</td>
            
        </tr>
        
        
        
        <tr>
            <td bgcolor="AliceBlue">
                <asp:Label ID="Label16" runat="server" 
                    Text="<%$ Resources:Resource, lbl_money_flow %>" 
                    style="font-weight: 700" /></td>
           
            <td bgcolor="AliceBlue">
                 <asp:Label ID="Label4" runat="server" 
                     Text="<%$ Resources:Resource, lbl_from %>" style="font-weight: 700"/> &nbsp;:&nbsp;
                     <asp:Label ID="lbl_from4" runat="server" Text="" />&nbsp;&nbsp;
                <asp:Label ID="Label1" runat="server" 
                    Text="<%$ Resources:Resource, lbl_to %>" style="font-weight: 700"/>&nbsp;:&nbsp;
                     <asp:Label ID="lbl_to4" runat="server" Text=""></asp:Label></td>
        </tr>
        <tr>
            <td>
               <asp:Label ID="Label29" runat="server" 
                     Text="<%$ Resources:Resource, lbl_net_operating_inc %>" /></td>
            
            <td>
                <asp:Label ID="lbl_net_operating_inc_m" runat="server"   /></td>
        </tr>
    
        <tr>
            <td>
               <asp:Label ID="Label30" runat="server" 
                     Text="<%$ Resources:Resource, lbl_mortgage %>"/></td>
           
            <td>
                <asp:TextBox ID="tbx_debt_service_m" runat="server" Width="65px"></asp:TextBox>
                <hr />
            </td>
        </tr>
    
         <tr>
            <td>
                <asp:Label ID="Label31" runat="server" 
                     Text="<%$ Resources:Resource, lbl_liquidity %>" 
                    style="font-weight: 700" /></td>
            
            <td>
                <asp:Label ID="lbl_liquidity_m" runat="server"  /></td>
        </tr>
    
    
         <tr>
            <td>
                &nbsp;</td>
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                <td>
                &nbsp;</td>
           
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                 <asp:Label ID="Label32" runat="server" 
                     Text="<%$ Resources:Resource, lbl_capitalisation2 %>" /></td>
            
            <td>
                <asp:TextBox ID="tbx_capitalisation_m" runat="server" Width="65px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label33" runat="server" 
                     Text="<%$ Resources:Resource, lbl_liquidity_capitalisation %>" 
                    style="font-weight: 700" /></td>
            
            <td>
                <asp:Label ID="lbl_liquidity_capitalisation_m" runat="server" /></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
               <asp:Label ID="Label8" runat="server" 
                     Text="<%$ Resources:Resource, lbl_added_value %>" /></td>
           
            <td>
                <asp:TextBox ID="tbx_added_value_m" runat="server" Width="65px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label35" runat="server" 
                     Text="<%$ Resources:Resource, lbl_liq_cap_value %>" 
                    style="font-weight: 700" /></td>
            
            <td>
                <asp:Label ID="lbl_liq_cap_value_m" runat="server" /></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
           
            <td>
                &nbsp;</td>
               
                
            
            <td>
                &nbsp;</td>
        </tr>
    
   
    
    
    <tr>
            <td colspan="2">
                <asp:Button ID="Caculate" runat="server" 
                    Text="<%$ Resources:Resource, btn_calculate_liquidity %>" 
                    ForeColor="#0066FF" onclick="Caculate_Click" />
                &nbsp;<br />
                </td>
            <td colspan="2">
                &nbsp;</td>
            
        </tr>
        
         <tr>
            <td align="center" bgcolor="AliceBlue" colspan="3">
                <b><asp:Label ID="Label39" runat="server" 
                     Text="<%$ Resources:Resource, lbl_analysis_return %>" /></b></td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label40" runat="server" 
                     Text="<%$ Resources:Resource, lbl_appartment_cost%>" /></td>
            <td colspan="2">
                <asp:Label ID="lbl_appartment_cost" runat="server"/></td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label41" runat="server" 
                     Text="<%$ Resources:Resource, lbl_exploitation_ratio%>" /></td>
            <td>
                <asp:Label ID="lbl_rde" runat="server"/></td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label43" runat="server" 
                     Text="<%$ Resources:Resource, lbl_performance_on_net %>" /></td>
            <td>
                <asp:Label ID="lbl_trn" runat="server"/></td>
        </tr>
        
         <tr>
            <td colspan="2">
               <asp:Label ID="Label44" runat="server" 
                     Text="<%$ Resources:Resource, lbl_gi_multiplier %>" /></td>
            <td >
                <asp:Label ID="lbl_mbre" runat="server"></asp:Label></td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label47" runat="server" 
                     Text="<%$ Resources:Resource, lbl_net_income_multiplier %>" /></td>
            <td colspan="2">
           <asp:Label ID="lbl_mrn" runat="server"></asp:Label>     
            </td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label46" runat="server" 
                     Text="<%$ Resources:Resource, lbl_debt_cover_ratio %>" /></td>
            <td>
               <asp:Label ID="lbl_rcd" runat="server"></asp:Label></td>
        </tr>
    </table>
    
     <asp:Button ID="Button2" ForeColor="#0066FF"  runat="server" 
        Text="<%$ Resources:Resource, btn_return %>" onclick="btn_return_Click" />

<asp:HiddenField ID="h_total_number_of_unit"  runat="server" />
<asp:HiddenField ID="h_home_id"  runat="server" />
    <br />


</asp:Content>

