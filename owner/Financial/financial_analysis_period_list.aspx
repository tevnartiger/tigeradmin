﻿<%@ Page Language="C#" MasterPageFile="~/owner/owner_mp.master" AutoEventWireup="true" CodeFile="financial_analysis_period_list.aspx.cs" Inherits="manager_Financial_financial_analysis_period_list" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


 <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_financial_analysis_list %>"></asp:Label><br /><br />

    <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="100%" BorderColor="White" BorderWidth="3"  ID="gvFinancialAnalysisPeriodList" runat="server" AutoGenerateColumns="false"
     H  AllowPaging="false" AlternatingRowStyle-BackColor="Beige" >
    <Columns>
    
    <asp:BoundField   DataField="period_analysis_name"  HeaderText="<%$ Resources:Resource, lbl_period_name %>"/>
    <asp:BoundField   DataField="home_name"  HeaderText="<%$ Resources:Resource, lbl_property %>"/>
     <asp:BoundField  DataField="period_analysis_date_begin" DataFormatString="{0:M-dd-yyyy}" HeaderText="<%$ Resources:Resource, lbl_from %>" />
    <asp:BoundField  DataField="period_analysis_date_end" DataFormatString="{0:M-dd-yyyy}" HeaderText="<%$ Resources:Resource, lbl_to %>" />
    <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_view %>"
     DataNavigateUrlFields="period_analysis_id" 
     DataNavigateUrlFormatString="financial_analysis_period_view.aspx?pa_id={0}" 
      HeaderText="<%$ Resources:Resource, lbl_view %>" />

   <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_update %>"
     DataNavigateUrlFields="period_analysis_id" 
     DataNavigateUrlFormatString="financial_analysis_period_update.aspx?pa_id={0}" 
      HeaderText="<%$ Resources:Resource, lbl_update %>" />      
      
      
      
        <asp:TemplateField >
    <ItemTemplate  >
    
    <asp:HiddenField ID="h_pa_id" Value='<%#Bind("period_analysis_id")%>'  runat="server" />
   <asp:Button ID="btn_cancel" runat="server" OnClick="btn_delete_Click"  Text="<%$ Resources:Resource, btn_cancel %>"/>
    </ItemTemplate  >
    </asp:TemplateField>
      
      
    </Columns>
    
    
    </asp:GridView>


</asp:Content>

