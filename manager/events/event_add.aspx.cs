﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
/// <summary>
/// done by : Stanley Jocelyn
/// date    : july 23 , 2008
/// desc    : adding an event
/// </summary>
public partial class manager_events_event_add : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            DateTime date = new DateTime();
            date = DateTime.Now;


            ddl_event_date_begin_y.SelectedValue = date.Year.ToString();
            ddl_event_date_begin_d.SelectedValue = date.Day.ToString();
            ddl_event_date_begin_m.SelectedValue = date.Month.ToString();


            ddl_event_date_end_y.SelectedValue = date.Year.ToString();
            ddl_event_date_end_d.SelectedValue = date.Day.ToString();
            ddl_event_date_end_m.SelectedValue = date.Month.ToString();

            lbl_event_confirmation.Visible = false;
            tb_confirmation.Visible = false;

        }
    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        lbl_event_confirmation.Visible = false;
        tb_confirmation.Visible = false;
        
        //*********************************************************************
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prEventAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        int success = 0;

        string event_date_begin;
        string event_date_end;
        string event_header;


        tiger.Date df = new tiger.Date();
        DateTime date_begin = Convert.ToDateTime(df.DateCulture(ddl_event_date_begin_m.SelectedValue, ddl_event_date_begin_d.SelectedValue, ddl_event_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        DateTime date_end = Convert.ToDateTime(df.DateCulture(ddl_event_date_end_m.SelectedValue, ddl_event_date_end_d.SelectedValue, ddl_event_date_end_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        date_begin = date_begin.AddMinutes(Convert.ToDouble(ddl_date_begin_hours.SelectedValue)).AddMinutes(Convert.ToDouble(ddl_date_begin_minutes.SelectedValue));
        date_end =  date_end.AddMinutes(Convert.ToDouble(ddl_date_end_hours.SelectedValue)).AddMinutes(Convert.ToDouble(ddl_date_end_minutes.SelectedValue));
        

       if( date_end >= date_begin )
       {
        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
        

            cmd.Parameters.Add("@event_date_begin", SqlDbType.DateTime).Value = date_begin;
            event_date_begin = ddl_event_date_begin_m.SelectedValue+"-"+ ddl_event_date_begin_d.SelectedValue+"-"+ ddl_event_date_begin_y.SelectedValue ;

            cmd.Parameters.Add("@event_date_end", SqlDbType.DateTime).Value = date_end ;
            event_date_end = ddl_event_date_end_m.SelectedValue + "-" + ddl_event_date_end_d.SelectedValue + "-" + ddl_event_date_end_y.SelectedValue;
            
            
            cmd.Parameters.Add("@event_header", SqlDbType.NVarChar, 50).Value = tbx_event.Text;
            event_header = tbx_event.Text;

            cmd.Parameters.Add("@event_description", SqlDbType.NText).Value = tbx_description.Text;
            
            //execute the insert
            cmd.ExecuteNonQuery();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            success = Convert.ToInt32(cmd.Parameters["@return"].Value);
            if (success == 0)
            {
                lbl_event_confirmation.Visible = true;
                tb_confirmation.Visible = true;

                lbl_event.Text = event_header ;
                lbl_event_date_begin.Text = event_date_begin ;
                lbl_event_date_end.Text = event_date_end ;

                tbx_description.Text = "";
                tbx_event.Text = "";

            }


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "event_add.aspx", error.ToString());
        }

       }
    }
}
