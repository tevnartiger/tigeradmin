<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="insurance_company_view.aspx.cs" Inherits="insurancecompany_insurance_company_view" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div>
    INSURANCE COMPANY VIEW <br />
        <asp:HyperLink ID="ic_add_link" 
            runat="server">add</asp:HyperLink> -&nbsp;  <asp:HyperLink ID="ic_update_link"
            runat="server">update</asp:HyperLink> - delete
        &nbsp;<br />
        <br />
            <asp:Repeater runat=server ID="r_insurance_company">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
            <tr>
                <td>
                    name</td>
                <td >
                    : 
                   
                    <%#DataBinder.Eval(Container.DataItem, "ic_name")%></td>
            </tr>
            <tr>
                <td>
                    website</td>
                <td >
                    :
                    <asp:HyperLink ID="ic_website_link"   NavigateUrl=<%#DataBinder.Eval(Container.DataItem, "ic_website")%> Target=_blank runat="server"><%#DataBinder.Eval(Container.DataItem, "ic_website")%></asp:HyperLink>
                   
                   </td>
            </tr>
            
            <tr>
                <td  >
                    address </td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "ic_addr")%></td>
            </tr>
            
            <tr>
                <td  >
                    country</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "country_name")%></td>
            </tr>
            
            <tr>
                <td >
                    prov/state</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "ic_prov")%></td>
            </tr>
            <tr>
                <td>
                    city</td>
                <td>
                    :
                    <%#DataBinder.Eval(Container.DataItem, "ic_city")%>
                </td>
            </tr>
            <tr>
                <td >
                    postal code</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "ic_pc")%></td>
            </tr>
            <tr>
                <td >
                    telephone</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "ic_tel")%>&nbsp;</td>
            </tr>
            <tr>
                
                <td  >
                    contact first name</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "ic_contact_fname")%></td>
            </tr>
            <tr>
                <td >
                    contact last name</td>
                <td  >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "ic_contact_lname")%></td>
            </tr>
            <tr>
                <td >
                    contact tel.</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "ic_contact_tel")%></td>
            </tr>
            <tr>
                <td >
                    contact email</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "ic_contact_email")%></td>
            </tr>
            <tr>
                <td >
                    comments</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "ic_com")%></td>
            </tr>

        </table>
        </ItemTemplate>
        </asp:Repeater>
        <br />
         <asp:HyperLink ID="link_go_back_to_list" runat="server" NavigateUrl="~/insurancecompany/insurance_company_list.aspx">GO back to list</asp:HyperLink>
        
    </div>
    </asp:Content>
