using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class insurancecompany_insurance_company_list : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        tiger.InsuranceCompany ic = new tiger.InsuranceCompany(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_ic_list.DataSource = ic.getInsuranceCompanyList(Convert.ToInt32(Session["schema_id"]));
        gv_ic_list.DataBind();
    }
}
