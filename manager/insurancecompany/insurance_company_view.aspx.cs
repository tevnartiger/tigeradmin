using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.AccountObjectAuthorization;
/// <summary>
/// Done by : Stanley Jocelyn
/// date    : oct 22 , 2007
/// </summary>

public partial class insurancecompany_insurance_company_view : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RegEx.IsInteger(Request.QueryString["ic_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }


        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        /////////////
        /////////////// SECURITY CHECK BEGIN   //////////////
        //////////////
        AccountObjectAuthorization icAuthorization = new AccountObjectAuthorization(strconn);

        if (!icAuthorization.InsuranceCompany(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["ic_id"])))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        /////////////
        /////////////// SECURITY CHECK END  //////////////
        //////////////



        ic_add_link.NavigateUrl = "insurance_company_add.aspx?ic_id=" + Request.QueryString["ic_id"];
        ic_update_link.NavigateUrl = "insurance_company_update.aspx?ic_id=" + Request.QueryString["ic_id"];

        // view the company infos
        tiger.InsuranceCompany h = new tiger.InsuranceCompany(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        r_insurance_company.DataSource = h.getInsuranceCompanyView(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["ic_id"]));
        r_insurance_company.DataBind();
    }
}
