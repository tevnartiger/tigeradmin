using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.AccountObjectAuthorization;
/// <summary>
/// Done by : Stanley Jocelyn
/// date    : oct 23 ,2007
/// </summary>
public partial class insurancecompany_insurance_company_update : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RegEx.IsInteger(Request.QueryString["ic_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }


        reg_ic_city.ValidationExpression = RegEx.getText();
        reg_ic_addr.ValidationExpression = RegEx.getText();
        reg_ic_contact_email.ValidationExpression = RegEx.getEmail();
        reg_ic_contact_fname.ValidationExpression = RegEx.getText();
        reg_ic_contact_lname.ValidationExpression = RegEx.getText();
        reg_ic_contact_tel.ValidationExpression = RegEx.getText();
        reg_ic_name.ValidationExpression = RegEx.getText();
        reg_ic_pc.ValidationExpression = RegEx.getText();
        reg_ic_tel.ValidationExpression = RegEx.getText();
        reg_ic_website.ValidationExpression = RegEx.getText();
        reg_ic_prov.ValidationExpression = RegEx.getText();
        reg_ic_contact_fax.ValidationExpression = RegEx.getText();

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        /////////////
        /////////////// SECURITY CHECK BEGIN   //////////////
        //////////////
        AccountObjectAuthorization icAuthorization = new AccountObjectAuthorization(strconn);

        if (!icAuthorization.InsuranceCompany(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["ic_id"])))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        /////////////
        /////////////// SECURITY CHECK END  //////////////
        //////////////

        if (!Page.IsPostBack)
        {
            // The country list
            tiger.Country ic = new tiger.Country(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_ic_country_list.DataSource = ic.getCountryList();
            ddl_ic_country_list.DataBind();

            //  Information about a financial institution ( ic_id )
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prInsuranceCompanyView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@ic_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["ic_id"]);
            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                    ic_name.Text = dr["ic_name"].ToString();
                    ic_website.Text = dr["ic_website"].ToString();
                    ic_addr.Text = dr["ic_addr"].ToString();

                    //lbl.Text = dr["country_id"].ToString();
                    if (dr["country_id"].ToString() == "")
                    { }
                    else
                        ddl_ic_country_list.SelectedValue = dr["country_id"].ToString();

                    ic_city.Text = dr["ic_city"].ToString();
                    ic_prov.Text = dr["ic_prov"].ToString();
                    ic_pc.Text = dr["ic_pc"].ToString();
                    ic_tel.Text = dr["ic_tel"].ToString();
                    ic_contact_fname.Text = dr["ic_contact_fname"].ToString();
                    ic_contact_lname.Text = dr["ic_contact_lname"].ToString();
                    ic_contact_tel.Text = dr["ic_contact_tel"].ToString();
                    ic_contact_email.Text = dr["ic_contact_email"].ToString();
                    ic_com.Text = dr["ic_com"].ToString();

                }

            }
            finally
            {
                conn.Close();
            }
        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {

        Page.Validate();
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prInsuranceCompanyUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@update_ic_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.NVarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@ic_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["ic_id"]);
            cmd.Parameters.Add("@ic_name", SqlDbType.NVarChar, 50).Value = ic_name.Text;
            cmd.Parameters.Add("@ic_website", SqlDbType.NVarChar, 200).Value = ic_website.Text;

            cmd.Parameters.Add("@ic_addr", SqlDbType.NVarChar, 50).Value = ic_addr.Text;
            cmd.Parameters.Add("@ic_city", SqlDbType.NVarChar, 50).Value = ic_city.Text;
            cmd.Parameters.Add("@ic_prov", SqlDbType.NVarChar, 50).Value = ic_prov.Text;
            cmd.Parameters.Add("@ic_pc", SqlDbType.NVarChar, 50).Value = ic_pc.Text;
            cmd.Parameters.Add("@ic_tel", SqlDbType.NVarChar, 50).Value = ic_tel.Text;
            cmd.Parameters.Add("@ic_contact_fname", SqlDbType.NVarChar, 50).Value = ic_contact_fname.Text;
            cmd.Parameters.Add("@ic_contact_lname", SqlDbType.NVarChar, 50).Value = ic_contact_lname.Text;
            cmd.Parameters.Add("@ic_contact_tel", SqlDbType.NVarChar, 50).Value = ic_contact_tel.Text;
            cmd.Parameters.Add("@ic_contact_email", SqlDbType.NVarChar, 50).Value = ic_contact_email.Text;
            cmd.Parameters.Add("@ic_contact_fax", SqlDbType.NVarChar, 50).Value = ic_contact_fax.Text;
            cmd.Parameters.Add("@country_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_ic_country_list.SelectedValue);
            cmd.Parameters.Add("@ic_com", SqlDbType.Text).Value = RegEx.getText(ic_com.Text);




            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            if (Convert.ToInt32(cmd.Parameters["@update_ic_id"].Value) == 0)
                result.InnerHtml = " UPDATE successful";


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }
    }
}
