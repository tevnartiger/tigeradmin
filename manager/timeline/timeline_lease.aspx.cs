﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.DataVisualization;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : fev 28 , 2008
/// </summary>
/// 
public partial class manager_timeline_timeline_lease : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((!Page.IsPostBack))
        {
            DateTime test_date = new DateTime();
            test_date = DateTime.Now;

            DateTime right_now = new DateTime();
            right_now = DateTime.Now;

            tiger.Date d = new tiger.Date();


            string[] from;
            string[] to;

            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


            tiger.Home l = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getHomeCount(Convert.ToInt32(Session["schema_id"]));

            // first we check if there's any property available
            if (home_count > 0)
            {
                int home_id = l.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));


                tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ddl_home_list2.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                ddl_home_list2.DataBind();
               

                // DropDownList pour les Unit
                //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

                if (unit_count > 0)
                {

                    int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), home_id);

                    ddl_unit_id2.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id); 
                    ddl_unit_id2.DataBind();
                    ddl_unit_id2.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
                    ddl_unit_id2.SelectedIndex = 0;


                    //------------------------ TIMELINE CHART BEGIN -----------------------------------

                    SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    SqlCommand cmd = new SqlCommand("prRangeBarChartLease", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                    cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id ;


                    DateTime datebegin = new DateTime();
                    DateTime dateend = new DateTime();


                    DateTime datemin = new DateTime();
                    DateTime datemax = new DateTime();


                    // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
                    // and convert it in Datetime
                    datemin = Convert.ToDateTime(d.DateCulture("1", "1", "3000", Convert.ToString(Session["_lastCulture"])));
                    datemax = Convert.ToDateTime(d.DateCulture("1", "1", "1900", Convert.ToString(Session["_lastCulture"])));


                    try
                    {
                        conn.Open();

                        SqlDataReader dr = null;
                        dr = cmd.ExecuteReader();

                        int i = 0;
                        int j = 1;
                        string daterangestring = "";
                        string[] datestring;

                        while (dr.Read() == true)
                        {
                            // Populate series data
                            daterangestring = dr["daterange"].ToString();

                            // Split string on spaces.
                            // This will separate all the words.

                            datestring = daterangestring.Split(',');

                            for (int k = 0; k < datestring.Length - 1; k = k + 2)
                            {

                                //daterangestring=datestring[k];
                                from = datestring[k].Split('/');
                                to = datestring[k + 1].Split('/');

                                if (datestring[k] != "")
                                    datebegin = Convert.ToDateTime(d.DateCulture(from[0], from[1], from[2], Convert.ToString(Session["_lastCulture"])));


                                if (datestring[k + 1] != "")
                                    dateend = Convert.ToDateTime(d.DateCulture(to[0], to[1], to[2], Convert.ToString(Session["_lastCulture"])));


                                if (datebegin < datemin)
                                    datemin = datebegin;

                                if (dateend > datemax)
                                    datemax = dateend;

                                Chart1.Series["Series1"].Points.AddXY(j, datebegin, dateend);
                            }

                            // Set axis labels
                            CustomLabel sd = new CustomLabel();
                            sd.Text = dr["unit_door_no"].ToString();

                            sd.FromPosition = i + 4;
                            sd.ToPosition = i - 2;

                            Chart1.ChartAreas["ChartArea1"].AxisX.CustomLabels.Add(sd);

                            Chart1.ChartAreas[0].AxisX.LabelStyle.Interval = 1;
                            Chart1.Series["Series1"].ToolTip = Resources.Resource.lbl_lease_date_begin + " : #VALY  ,\n " + Resources.Resource.lbl_lease_date_end + " :\n#VALY2";


                            foreach (DataPoint point in Chart1.Series[0].Points)
                            {
                                point.YValues[0] = point.YValues[0];
                                Chart1.Series["Series1"].Palette = ChartColorPalette.BrightPastel;//assigning colors to datapoint  
                            }

                            j++;
                            i++;

                        }


                        // Set Range Bar chart type
                        Chart1.Series["Series1"].ChartType = SeriesChartType.RangeBar;

                        // Set point width
                        Chart1.Series["Series1"]["PointWidth"] = "0.2";

                        // Set Y axis Minimum and Maximum
                        Chart1.ChartAreas["ChartArea1"].AxisY.Minimum = datemin.AddDays(-30).ToOADate();
                        Chart1.ChartAreas["ChartArea1"].AxisY.Maximum = datemax.AddDays(30).ToOADate();
                        Chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Format = "MMM dd yyyy";
                        Chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Angle = -45;

                        Chart1.Width = 650;
                        Chart1.Height = 400;


                        ddl_timeline_begin_m.SelectedValue = datemin.Month.ToString();
                        ddl_timeline_begin_d.SelectedValue = datemin.Day.ToString();
                        ddl_timeline_begin_y.SelectedValue = datemin.Year.ToString();


                        ddl_timeline_end_m.SelectedValue = datemax.Month.ToString();
                        ddl_timeline_end_d.SelectedValue = datemax.Day.ToString();
                        ddl_timeline_end_y.SelectedValue = datemax.Year.ToString();


                    }
                    finally
                    {
                        conn.Close();
                    }

                    Chart1.Visible = true;

                    //------------------------ TIMELINE CHART END -------------------------------------

                   
                }

            }
      }
        
    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_unit_id2_SelectedIndexChanged(object sender, EventArgs e)
    {
        DateTime right_now = new DateTime();
        right_now = DateTime.Now;
        tiger.Date d = new tiger.Date();

        string[] from;
        string[] to;
    
        if (Convert.ToInt32(ddl_unit_id2.SelectedValue) > 0)
        {
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prUnitLeaseArchiveList", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_unit_id2.SelectedValue);

            DateTime datemin = new DateTime();
            DateTime datemax = new DateTime();


            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            datemin = Convert.ToDateTime(d.DateCulture("1", "1", "3000", Convert.ToString(Session["_lastCulture"])));
            datemax = Convert.ToDateTime(d.DateCulture("1", "1", "1900", Convert.ToString(Session["_lastCulture"])));


            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader();

                while (dr.Read() == true)
                {

                    // Populate series data
                    Chart1.Series["Series1"].Points.AddXY(1, Convert.ToDateTime(dr["tu_date_begin"]), Convert.ToDateTime(dr["tu_date_end"]));



                    if (Convert.ToDateTime(dr["tu_date_begin"]) < datemin)
                        datemin = Convert.ToDateTime(dr["tu_date_begin"]);

                    if (Convert.ToDateTime(dr["tu_date_end"]) > datemax)
                        datemax = Convert.ToDateTime(dr["tu_date_end"]);


                    // Set axis labels
                    Chart1.Series["Series1"].Points[0].AxisLabel = dr["unit_door_no"].ToString();



                }

                // Set Range Bar chart type
                Chart1.Series["Series1"].ChartType = SeriesChartType.RangeBar;

                Chart1.Series["Series1"].ToolTip = Resources.Resource.lbl_lease_date_begin + " : #VALY  ,\n " + Resources.Resource.lbl_lease_date_end + " :\n#VALY2";

                // Set point width
                Chart1.Series["Series1"]["PointWidth"] = "0.25";

                // Set Y axis Minimum and Maximum
                Chart1.ChartAreas["ChartArea1"].AxisY.Minimum = datemin.AddDays(-30).ToOADate();
                Chart1.ChartAreas["ChartArea1"].AxisY.Maximum = datemax.AddDays(30).ToOADate();
                Chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Format = "MMM dd yyyy";


                ddl_timeline_begin_m.SelectedValue = datemin.Month.ToString();
                ddl_timeline_begin_d.SelectedValue = datemin.Day.ToString();
                ddl_timeline_begin_y.SelectedValue = datemin.Year.ToString();


                ddl_timeline_end_m.SelectedValue = datemax.Month.ToString();
                ddl_timeline_end_d.SelectedValue = datemax.Day.ToString();
                ddl_timeline_end_y.SelectedValue = datemax.Year.ToString();



                foreach (DataPoint point in Chart1.Series[0].Points)
                {
                    point.YValues[0] = point.YValues[0];
                    Chart1.Series["Series1"].Palette = ChartColorPalette.BrightPastel;//assigning colors to datapoint  
                }

                Chart1.Width = 650;
                Chart1.Height = 400;

                Chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Angle = -45;
            }
            finally
            {
                conn.Close();
            }

            Chart1.Visible = true;
        }
        else
        {
            Chart1.Visible = false;
        }






        if (Convert.ToInt32(ddl_unit_id2.SelectedIndex) == 0)
        {
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prRangeBarChartLease", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_list2.SelectedValue);


            DateTime datebegin = new DateTime();
            DateTime dateend = new DateTime();


            DateTime datemin = new DateTime();
            DateTime datemax = new DateTime();


            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            datemin = Convert.ToDateTime(d.DateCulture("1", "1", "3000", Convert.ToString(Session["_lastCulture"])));
            datemax = Convert.ToDateTime(d.DateCulture("1", "1", "1900", Convert.ToString(Session["_lastCulture"])));


            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader();

                int i = 0;
                int j = 1;
                string daterangestring = "";
                string[] datestring;

                while (dr.Read() == true)
                {
                    // Populate series data
                    daterangestring = dr["daterange"].ToString();

                    // Split string on spaces.
                    // This will separate all the words.

                    datestring = daterangestring.Split(',');

                    for (int k = 0; k < datestring.Length - 1; k = k + 2)
                    {

                        //daterangestring=datestring[k];
                        from = datestring[k].Split('/');
                        to = datestring[k + 1].Split('/');

                        if (datestring[k] != "")
                            datebegin = Convert.ToDateTime(d.DateCulture(from[0], from[1], from[2], Convert.ToString(Session["_lastCulture"])));


                        if (datestring[k + 1] != "")
                            dateend = Convert.ToDateTime(d.DateCulture(to[0], to[1], to[2], Convert.ToString(Session["_lastCulture"])));



                        if (datebegin < datemin)
                            datemin = datebegin;

                        if (dateend > datemax)
                            datemax = dateend;

                        Chart1.Series["Series1"].Points.AddXY(j, datebegin, dateend);
                    }

                    // Set axis labels
                    CustomLabel sd = new CustomLabel();
                    sd.Text = dr["unit_door_no"].ToString();

                    sd.FromPosition = i + 4;
                    sd.ToPosition = i - 2;

                    Chart1.ChartAreas["ChartArea1"].AxisX.CustomLabels.Add(sd);

                    Chart1.ChartAreas[0].AxisX.LabelStyle.Interval = 1;
                    Chart1.Series["Series1"].ToolTip = Resources.Resource.lbl_lease_date_begin + " : #VALY  ,\n " + Resources.Resource.lbl_lease_date_end + " :\n#VALY2";


                    foreach (DataPoint point in Chart1.Series[0].Points)
                    {
                        point.YValues[0] = point.YValues[0];
                        Chart1.Series["Series1"].Palette = ChartColorPalette.BrightPastel;//assigning colors to datapoint  
                    }

                    j++;
                    i++;

                }


                // Set Range Bar chart type
                Chart1.Series["Series1"].ChartType = SeriesChartType.RangeBar;

                // Set point width
                Chart1.Series["Series1"]["PointWidth"] = "0.2";

                // Set Y axis Minimum and Maximum
                Chart1.ChartAreas["ChartArea1"].AxisY.Minimum = datemin.AddDays(-30).ToOADate();
                Chart1.ChartAreas["ChartArea1"].AxisY.Maximum = datemax.AddDays(30).ToOADate();
                Chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Format = "MMM dd yyyy";
                Chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Angle = -45;

                Chart1.Width = 650;
                Chart1.Height = 400;

                ddl_timeline_begin_m.SelectedValue = datemin.Month.ToString();
                ddl_timeline_begin_d.SelectedValue = datemin.Day.ToString();
                ddl_timeline_begin_y.SelectedValue = datemin.Year.ToString();


                ddl_timeline_end_m.SelectedValue = datemax.Month.ToString();
                ddl_timeline_end_d.SelectedValue = datemax.Day.ToString();
                ddl_timeline_end_y.SelectedValue = datemax.Year.ToString();

            }
            finally
            {
                conn.Close();
            }

            Chart1.Visible = true;
        }


    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_list2_SelectedIndexChanged(object sender, EventArgs e)
    {

        string[] from;
        string[] to ;


        int home_id = Convert.ToInt32(ddl_home_list2.SelectedValue);

        // here we check if there's any unit in this property
        tiger.Unit uc = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        int unit_count = uc.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

        //  if (unit_count > 0)
        // here we get the list of unit that were already added 
        {
            // repopulating the ddl for the property selected
            ddl_unit_id2.Dispose();
            ddl_unit_id2.DataSource = uc.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
            ddl_unit_id2.DataBind();
            ddl_unit_id2.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
            ddl_unit_id2.SelectedIndex = 0;


        }



        tiger.Date d = new tiger.Date();

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prRangeBarChartLease", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_list2.SelectedValue);


        DateTime datebegin = new DateTime();
        DateTime dateend = new DateTime();


        DateTime datemin = new DateTime();
        DateTime datemax = new DateTime();


        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        datemin = Convert.ToDateTime(d.DateCulture("1", "1", "3000", Convert.ToString(Session["_lastCulture"])));
        datemax = Convert.ToDateTime(d.DateCulture("1", "1", "1900", Convert.ToString(Session["_lastCulture"])));


        try
        {
            conn.Open();

            SqlDataReader dr = null;
            dr = cmd.ExecuteReader();

            int i = 0;
            int j = 1;
            string daterangestring = "";
            string[] datestring;

            while (dr.Read() == true)
            {
                // Populate series data
                daterangestring = dr["daterange"].ToString();

                // Split string on spaces.
                // This will separate all the words.

                datestring = daterangestring.Split(',');

                for (int k = 0; k < datestring.Length - 1; k = k + 2)
                {

                    //daterangestring=datestring[k];

                    from = datestring[k].Split('/');
                    to = datestring[k + 1].Split('/');

                    if (datestring[k] != "")
                        datebegin = Convert.ToDateTime(d.DateCulture(from[0], from[1], from[2], Convert.ToString(Session["_lastCulture"])));


                    if (datestring[k + 1] != "")
                        dateend = Convert.ToDateTime(d.DateCulture(to[0], to[1], to[2], Convert.ToString(Session["_lastCulture"])));



                    if (datebegin < datemin)
                        datemin = datebegin;

                    if (dateend > datemax)
                        datemax = dateend;

                    Chart1.Series["Series1"].Points.AddXY(j, datebegin, dateend);
                }

                // Set axis labels
                CustomLabel sd = new CustomLabel();
                sd.Text = dr["unit_door_no"].ToString();

                sd.FromPosition = i + 4;
                sd.ToPosition = i - 2;

                Chart1.ChartAreas["ChartArea1"].AxisX.CustomLabels.Add(sd);

                Chart1.ChartAreas[0].AxisX.LabelStyle.Interval = 1;
                Chart1.Series["Series1"].ToolTip = Resources.Resource.lbl_lease_date_begin + " : #VALY  ,\n " + Resources.Resource.lbl_lease_date_end + " :\n#VALY2";


                foreach (DataPoint point in Chart1.Series[0].Points)
                {
                    point.YValues[0] = point.YValues[0];
                    Chart1.Series["Series1"].Palette = ChartColorPalette.BrightPastel;//assigning colors to datapoint  
                }

                j++;
                i++;

            }


            // Set Range Bar chart type
            Chart1.Series["Series1"].ChartType = SeriesChartType.RangeBar;

            // Set point width
            Chart1.Series["Series1"]["PointWidth"] = "0.2";

            // Set Y axis Minimum and Maximum
            Chart1.ChartAreas["ChartArea1"].AxisY.Minimum = datemin.AddDays(-30).ToOADate();
            Chart1.ChartAreas["ChartArea1"].AxisY.Maximum = datemax.AddDays(30).ToOADate();
            Chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Format = "MMM dd yyyy";
            Chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Angle = -45;

            Chart1.Width = 650;
            Chart1.Height = 400;


            ddl_timeline_begin_m.SelectedValue = datemin.Month.ToString();
            ddl_timeline_begin_d.SelectedValue = datemin.Day.ToString();
            ddl_timeline_begin_y.SelectedValue = datemin.Year.ToString();


            ddl_timeline_end_m.SelectedValue = datemax.Month.ToString();
            ddl_timeline_end_d.SelectedValue = datemax.Day.ToString();
            ddl_timeline_end_y.SelectedValue = datemax.Year.ToString();


        }
        finally
        {
            conn.Close();
        }

        Chart1.Visible = true;

    
    }

    protected void btn_view_Click(object sender, EventArgs e)
    {

        string[] from;
        string[] to;

        tiger.Date d = new tiger.Date();

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prRangeBarChartLeaseHomeUnit", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_list2.SelectedValue);
        cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_unit_id2.SelectedValue);
        cmd.Parameters.Add("@timeline_begin", SqlDbType.DateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_timeline_begin_m.SelectedValue, ddl_timeline_begin_d.SelectedValue, ddl_timeline_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
        cmd.Parameters.Add("@timeline_end", SqlDbType.DateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_timeline_end_m.SelectedValue, ddl_timeline_end_d.SelectedValue, ddl_timeline_end_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


        DateTime datebegin = new DateTime();
        DateTime dateend = new DateTime();


        DateTime datemin = new DateTime();
        DateTime datemax = new DateTime();


        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        datemin = Convert.ToDateTime(d.DateCulture("1", "1", "3000", Convert.ToString(Session["_lastCulture"])));
        datemax = Convert.ToDateTime(d.DateCulture("1", "1", "1900", Convert.ToString(Session["_lastCulture"])));


        try
        {
            conn.Open();

            SqlDataReader dr = null;
            dr = cmd.ExecuteReader();

            int i = 0;
            int j = 1;
            string daterangestring = "";
            string[] datestring;

            while (dr.Read() == true)
            {
                // Populate series data
                daterangestring = dr["daterange"].ToString();

                // Split string on spaces.
                // This will separate all the words.

                datestring = daterangestring.Split(',');

                for (int k = 0; k < datestring.Length - 1; k = k + 2)
                {

                    //daterangestring=datestring[k];
                    from = datestring[k].Split('/');
                    to = datestring[k + 1].Split('/');

                    if (datestring[k] != "")
                        datebegin = Convert.ToDateTime(d.DateCulture(from[0], from[1], from[2], Convert.ToString(Session["_lastCulture"])));


                    if (datestring[k + 1] != "")
                        dateend = Convert.ToDateTime(d.DateCulture(to[0], to[1], to[2], Convert.ToString(Session["_lastCulture"])));


                    if (datebegin < datemin)
                        datemin = datebegin;

                    if (dateend > datemax)
                        datemax = dateend;

                    Chart1.Series["Series1"].Points.AddXY(j, datebegin, dateend);
                }

                // Set axis labels
                CustomLabel sd = new CustomLabel();
                sd.Text = dr["unit_door_no"].ToString();

                sd.FromPosition = i + 4;
                sd.ToPosition = i - 2;

                Chart1.ChartAreas["ChartArea1"].AxisX.CustomLabels.Add(sd);

                Chart1.ChartAreas[0].AxisX.LabelStyle.Interval = 1;
                Chart1.Series["Series1"].ToolTip = Resources.Resource.lbl_lease_date_begin + " : #VALY  ,\n " + Resources.Resource.lbl_lease_date_end + " :\n#VALY2";


                foreach (DataPoint point in Chart1.Series[0].Points)
                {
                    point.YValues[0] = point.YValues[0];
                    Chart1.Series["Series1"].Palette = ChartColorPalette.BrightPastel;//assigning colors to datapoint  
                }

                j++;
                i++;

            }


            // Set Range Bar chart type
            Chart1.Series["Series1"].ChartType = SeriesChartType.RangeBar;

            // Set point width
            Chart1.Series["Series1"]["PointWidth"] = "0.2";

            // Set Y axis Minimum and Maximum

            datemin = Convert.ToDateTime(d.DateCulture(ddl_timeline_begin_m.SelectedValue, ddl_timeline_begin_d.SelectedValue, ddl_timeline_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            datemax = Convert.ToDateTime(d.DateCulture(ddl_timeline_end_m.SelectedValue, ddl_timeline_end_d.SelectedValue, ddl_timeline_end_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


            Chart1.ChartAreas["ChartArea1"].AxisY.Minimum = datemin.AddDays(-30).ToOADate();
            Chart1.ChartAreas["ChartArea1"].AxisY.Maximum = datemax.AddDays(30).ToOADate();
            Chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Format = "MMM dd yyyy";
            Chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Angle = -45;

            Chart1.Width = 650;
            Chart1.Height = 400;

           

          

        }
        finally
        {
            conn.Close();
        }

        Chart1.Visible = true;
    }
}
