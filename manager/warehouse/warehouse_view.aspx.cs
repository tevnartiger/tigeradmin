﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : may 28, 2008
/// </summary>
public partial class manager_warehouse_warehouse_view : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RegEx.IsInteger(Request.QueryString["w_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        //---------------------------------------------------------------------------------------------------
        AccountObjectAuthorization warehouseAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!warehouseAuthorization.Warehouse(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["w_id"])))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
        //---------------------------------------------------------------------------------------------------
      
        warehouse_add_link.NavigateUrl = "warehouse_add.aspx";
        warehouse_update_link.NavigateUrl = "warehouse_update.aspx?w_id=" + Request.QueryString["w_id"];

        // view the company infos
        tiger.Warehouse h = new tiger.Warehouse(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        r_warehouse.DataSource = h.getWarehouseView(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["w_id"]));
        r_warehouse.DataBind();

    }
}
