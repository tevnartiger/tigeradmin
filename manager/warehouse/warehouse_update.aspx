﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="warehouse_update.aspx.cs" Inherits="manager_warehouse_warehouse_update" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<div>
         <asp:Label ID="Label4" runat="server" 
             Text="<%$ Resources:Resource, lbl_update_warehouse %>" 
             style="font-weight: 700; font-size: small"/><br /><br />
        <table bgcolor="#ffffcc" >
            <tr>
                <td >
                    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_name %>"/></td>
                <td  >
                    : 
                    <asp:TextBox ID="warehouse_name" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator 
                        ID="reg_warehouse_name" runat="server" 
                         ControlToValidate="warehouse_name"
                        ErrorMessage="invalid name">
                        </asp:RegularExpressionValidator>
                &nbsp;<asp:RequiredFieldValidator  ControlToValidate="warehouse_name"
                           ID="req_warehouse_name" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lbl_website %>"/></td>
                <td >
                    : 
                    <asp:TextBox ID="warehouse_website" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_warehouse_website" runat="server" 
                         ControlToValidate="warehouse_website"
                        ErrorMessage="invalid link">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td  >
                    <asp:Label ID="lbl_address" runat="server" Text="<%$ Resources:Resource, lbl_address %>"/></td>
                <td  >
                    :
                    <asp:TextBox ID="warehouse_addr" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator ID="reg_warehouse_addr" runat="server" 
                        ControlToValidate="warehouse_addr"
                        ErrorMessage="invalid address">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="lbl_tel" runat="server" Text="<%$ Resources:Resource, lbl_tel %>"/></td>
                <td >
                    :
                    <asp:TextBox ID="warehouse_tel" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator 
                        ID="reg_warehouse_tel" runat="server" 
                        ControlToValidate="warehouse_tel"
                        ErrorMessage="invalid telephone">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td >
                   <asp:Label ID="lbl_country" runat="server" Text="<%$ Resources:Resource, lbl_country %>"/></td>
                <td >
                    :
                    <asp:DropDownList ID="ddl_warehouse_country_list" DataTextField="country_name" DataValueField="country_id" runat="server">
                    
                    </asp:DropDownList></td>
            </tr>
            
            <tr>
                <td  >
                   <asp:Label ID="lbl_prov" runat="server" Text="<%$ Resources:Resource, lbl_prov %>"/></td>
                <td  >
                    :
                    <asp:TextBox ID="warehouse_prov" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_warehouse_prov" runat="server" 
                        ControlToValidate="warehouse_prov"
                        ErrorMessage="enter letters only ">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="lbl_city" runat="server" Text="<%$ Resources:Resource, lbl_city %>"/></td>
                <td >
                    :
                    <asp:TextBox ID="warehouse_city" runat="server"></asp:TextBox>
                &nbsp;<asp:RegularExpressionValidator ID="reg_warehouse_city" runat="server" 
                        ControlToValidate="warehouse_city"
                        ErrorMessage="enter letters only">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td  >
                    <asp:Label ID="lbl_pc" runat="server" Text="<%$ Resources:Resource, lbl_pc %>"/></td>
                <td  >
                    :
                    <asp:TextBox ID="warehouse_pc" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_warehouse_pc" runat="server" 
                        ControlToValidate="warehouse_pc"
                        ErrorMessage="invalid postal code">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td   >
                  <asp:Label ID="lbl_fname" runat="server" Text="<%$ Resources:Resource, lbl_contact_fname %>"/></td>
                <td  >
                    :
                    <asp:TextBox ID="warehouse_contact_fname" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_warehouse_contact_fname" runat="server" 
                        ControlToValidate="warehouse_contact_fname"
                        ErrorMessage="invalid name">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td  >
                   <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_contact_lname %>"/></td>
                <td   >
                    :
                    <asp:TextBox ID="warehouse_contact_lname" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_warehouse_contact_lname" runat="server" 
                        ControlToValidate="warehouse_contact_lname"
                        ErrorMessage="invalid last name">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td  >
                    contact tel.</td>
                <td  >
                    :
                    <asp:TextBox ID="warehouse_contact_tel" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_warehouse_contact_tel" 
                        ControlToValidate="warehouse_contact_tel"
                        runat="server" ErrorMessage="invalid telephone">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td  >
                   <asp:Label ID="lbl_fax" runat="server" Text="<%$ Resources:Resource, lbl_fax %>"/></td>
                <td  >
                    :
                    <asp:TextBox ID="warehouse_contact_fax" runat="server" ></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator ID="reg_warehouse_contact_fax" 
                        ControlToValidate="warehouse_contact_fax"
                        runat="server" ErrorMessage="invalid fax">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            
            <tr>
                <td  >
                  <asp:Label ID="lbl_email" runat="server" Text="<%$ Resources:Resource, lbl_email %>"/></td>
                <td  >
                    :
                    <asp:TextBox ID="warehouse_contact_email" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_warehouse_contact_email" 
                        ControlToValidate="warehouse_contact_email"
                        runat="server" ErrorMessage="invalid email">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td valign=top>
                  <asp:Label ID="lbl_com" Visible="false" runat="server" Text="<%$ Resources:Resource, lbl_com %>"/></td>
                <td>
                    
                    <asp:TextBox ID="warehouse_com" runat="server" Height="100px" TextMode="MultiLine" Width="300px"></asp:TextBox></td>
            </tr>
        
        </table>
        <br />
       
        
        <asp:Button ID="btn_submit" runat="server" Text="submit"  OnClick="btn_submit_Click"/>
        &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
        <br />
    
  
    <div id="result" runat="server"></div>
   
    </div>


</asp:Content>

