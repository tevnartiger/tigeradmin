﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="warehouse_view.aspx.cs" Inherits="manager_warehouse_warehouse_view" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div>
    WAREHOUSE VIEW <br />
        <asp:HyperLink ID="warehouse_add_link" 
            runat="server">add</asp:HyperLink> -&nbsp;  <asp:HyperLink ID="warehouse_update_link"
            runat="server">update</asp:HyperLink> - delete
        &nbsp;<br />
        <br />
            <asp:Repeater runat=server ID="r_warehouse">
        <ItemTemplate>
        <table width="60%" bgcolor="#ffffcc" >
            <tr>
                <td>
                    name</td>
                <td valign="top" >
                    : 
                   
                    <%#DataBinder.Eval(Container.DataItem, "warehouse_name")%></td>
            </tr>
            <tr>
                <td>
                    website</td>
                <td valign="top"  >
                    :
                    <asp:HyperLink ID="warehouse_website_link"   NavigateUrl=<%#DataBinder.Eval(Container.DataItem, "warehouse_website")%> Target="_blank" runat="server"><%#DataBinder.Eval(Container.DataItem, "warehouse_website")%></asp:HyperLink>
                   
                   </td>
            </tr>
            
            <tr>
                <td  >
                    address </td>
                <td valign="top"  >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "warehouse_addr")%></td>
            </tr>
            
            <tr>
                <td  >
                    country</td>
                <td  valign="top" >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "country_name")%></td>
            </tr>
            
            <tr>
                <td >
                    prov/state</td>
                <td  valign="top" >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "warehouse_prov")%></td>
            </tr>
            <tr>
                <td>
                    city</td>
                <td valign="top" >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "warehouse_city")%>
                </td>
            </tr>
            <tr>
                <td >
                    postal code</td>
                <td valign="top"  >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "warehouse_pc")%></td>
            </tr>
            <tr>
                <td >
                    telephone</td>
                <td valign="top"  >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "warehouse_tel")%>&nbsp;</td>
            </tr>
            <tr>
                
                <td  >
                    contact first name</td>
                <td valign="top" >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "warehouse_contact_fname")%></td>
            </tr>
            <tr>
                <td >
                    contact last name</td>
                <td valign="top"  >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "warehouse_contact_lname")%></td>
            </tr>
            <tr>
                <td >
                    contact tel.</td>
                <td valign="top" >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "warehouse_tel")%></td>
            </tr>
            <tr>
                <td valign="top" >
                    contact email</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "warehouse_email")%></td>
            </tr>
            <tr>
                <td >
                    comments</td>
                <td valign="top" >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "warehouse_com")%></td>
            </tr>

        </table>
        </ItemTemplate>
        </asp:Repeater>
        <br />
         <asp:HyperLink ID="link_go_back_to_list" runat="server" NavigateUrl="~/manager/warehouse/warehouse_list.aspx">GO back to list</asp:HyperLink>
        
    </div>


</asp:Content>

