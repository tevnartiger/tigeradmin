using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class manager_uc_uc_adm_lmenu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string curDir = Page.TemplateSourceDirectory.ToString();
        switch (curDir)
        {
            case "/manager/property": panel_default.Visible = false ;
                                   
                                      panel_property.Visible = true ;
                                   
                                      panel_portfolio.Visible = false ;
                                   
                                      panel_tenant.Visible = false ;
                                   
                                      panel_lease.Visible = false ;
                                    

                                      panel_income.Visible = false ;

                                      panel_expense.Visible = false;
                                   

                                      panel_inventory.Visible = false;

                                      panel_filemanager.Visible = false;

                                      panel_user.Visible = false;

                                      panel_tools.Visible = false;
                                    break;

            case "/manager/filemanager": panel_default.Visible = false;

                                    panel_property.Visible = false;

                                    panel_portfolio.Visible = false;

                                    panel_tenant.Visible = false;

                                    panel_lease.Visible = false;

                                    panel_income.Visible = false;

                                    panel_expense.Visible = false;

                                    panel_inventory.Visible = false;

                                    panel_filemanager.Visible = true;

                                    panel_user.Visible = false;

                                    panel_tools.Visible = false;

                                    break;


            case "/manager/portfolio": panel_default.Visible = false;
                                    
                                       panel_property.Visible = false;
                                   
                                       panel_portfolio.Visible = true;
                                    
                                       panel_tenant.Visible = false;
                                    
                                       panel_lease.Visible = false;
                                   

                                       panel_income.Visible = false;

                                       panel_expense.Visible = false;
                                   
                                       panel_inventory.Visible = false;

                                       panel_filemanager.Visible = false;

                                       panel_user.Visible = false;

                                       panel_tools.Visible = false;
                                    break;


            case "/manager/tenant": panel_default.Visible = false;
                                   
                                    panel_property.Visible = false;
                                   
                                    panel_portfolio.Visible = false;
                                    
                                    panel_tenant.Visible = true;
                                    
                                    panel_lease.Visible = false;
                                   
                                    panel_income.Visible = false;

                                    panel_expense.Visible = false;
                                    
                                    panel_inventory.Visible = false;

                                    panel_filemanager.Visible = false;

                                    panel_user.Visible = false;

                                    panel_tools.Visible = false;
                                    break;

            case "/manager/timeline":
            case "/manager/lease": panel_default.Visible = false;
                                  
                                   panel_property.Visible = false;
                                   
                                   panel_portfolio.Visible = false;
                                    
                                   panel_tenant.Visible = false;
                                  
                                   panel_lease.Visible = true;
                                   
                                   panel_income.Visible = false;

                                   panel_expense.Visible = false;
                                
                                   panel_inventory.Visible = false;

                                   panel_filemanager.Visible = false;

                                   panel_user.Visible = false;

                                   panel_tools.Visible = false;
                                    break;

            case "/manager/expense":
            case "/manager/income":
            case "/manager/financial":
            case "/manager/Financial":
                                    if(Request.Url.ToString().Contains("income"))
                                    {
                                        panel_default.Visible = false;

                                        panel_property.Visible = false;

                                        panel_portfolio.Visible = false;

                                        panel_tenant.Visible = false;

                                        panel_lease.Visible = false;

                                        panel_income.Visible = true;

                                        panel_expense.Visible = false;

                                        panel_inventory.Visible = false;

                                        panel_filemanager.Visible = false;

                                        panel_user.Visible = false;

                                        panel_tools.Visible = false;
                                    }


                                    if (Request.Url.ToString().Contains("expense"))
                                    {
                                        panel_default.Visible = false;

                                        panel_property.Visible = false;

                                        panel_portfolio.Visible = false;

                                        panel_tenant.Visible = false;

                                        panel_lease.Visible = false;

                                        panel_income.Visible = false;

                                        panel_expense.Visible = true;

                                        panel_inventory.Visible = false;

                                        panel_filemanager.Visible = false;

                                        panel_user.Visible = false;

                                        panel_tools.Visible = false;
                                    }
                                    break;

            case "/manager/appliance": panel_default.Visible = false;
                                   
                                       panel_property.Visible = false;
                              
                                       panel_portfolio.Visible = false;
                                 
                                       panel_tenant.Visible = false;
                                 
                                       panel_lease.Visible = false;
                                   
                                       panel_income.Visible = false;

                                       panel_expense.Visible = false;
                                    
                                       panel_inventory.Visible = true;

                                       panel_filemanager.Visible = false;

                                       panel_user.Visible = false;

                                       panel_tools.Visible = false;
                                    break;


            case "/manager/user":
            case "/manager/role": panel_default.Visible = false;

                                    panel_property.Visible = false;

                                    panel_portfolio.Visible = false;

                                    panel_tenant.Visible = false;

                                    panel_lease.Visible = false;

                                    panel_income.Visible = false;

                                    panel_expense.Visible = false;

                                    panel_inventory.Visible = false;

                                    panel_filemanager.Visible = false;

                                    panel_user.Visible = true;

                                    panel_tools.Visible = false;
                                    break;


            case "/manager/tools": panel_default.Visible = false;

                                    panel_property.Visible = false;

                                    panel_portfolio.Visible = false;

                                    panel_tenant.Visible = false;

                                    panel_lease.Visible = false;

                                    panel_income.Visible = false;

                                    panel_expense.Visible = false;

                                    panel_inventory.Visible = false;

                                    panel_filemanager.Visible = false;

                                    panel_user.Visible = false;

                                    panel_tools.Visible = true;
                                    break;


                         default : panel_default.Visible = true;
                                 
                                   panel_property.Visible = false;
                              
                                   panel_portfolio.Visible = false;
                                   
                                   panel_tenant.Visible = false;
                                   
                                   panel_lease.Visible = false;
                                   
                                   panel_income.Visible = false;

                                   panel_expense.Visible = false;
                                  
                                   panel_inventory.Visible = false;

                                   panel_filemanager.Visible = false;

                                   panel_user.Visible = false;

                                   panel_tools.Visible = false;
                                    break;



                         
        }


    }

}

