﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="lease_view.aspx.cs" Inherits="manager_lease_lease_view" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<br />
<span style="font-size: small"><b>LEASE VIEW<br /></b></span> 
  
<asp:Repeater runat="server" ID="r_HomeUnitView">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
        
        <tr>
          <td>
             <b>District&nbsp;:&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_district")%> 
                  </b> </td>
            </tr>
           <tr>
              <td valign="top" >
              <b><%#DataBinder.Eval(Container.DataItem, "home_addr_no")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_street")%></b></td>
            </tr>
            <tr>
                 <td valign="top" >
             <b>   <%#DataBinder.Eval(Container.DataItem, "home_city")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_prov")%>
                   &nbsp;,&nbsp;&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_pc")%> </b>
                   </td>
               
            </tr>
            <tr>
                 <td valign="top" >
                 <b>Unit</b>  &nbsp;:&nbsp;<%#DataBinder.Eval(Container.DataItem, "unit_door_no")%> 
                
                   </td>
               
            </tr>   
        </table>
        
        </ItemTemplate>
        </asp:Repeater>
&nbsp;<br />
&nbsp;<table >
        <tr id="tr_lease_type" runat="server">
            <td>
               <b>Lease type</b> </td>
            <td>
                &nbsp;:&nbsp;&nbsp;<asp:Label ID="lbl_lease_type" runat="server" ></asp:Label></td>
        </tr>
        <tr id="tr_company" runat="server">
            <td>
               <b> Company</b></td>
            <td>
               &nbsp;:&nbsp;&nbsp; <asp:Label ID="lbl_company" runat="server" ></asp:Label></td>
        </tr>
    </table>
    <table  width="100%"><tr><td style="color: #D3D3D3">
        ______________________________________________________________________________________________</td></tr></table>
    <br />
    <table>
<tr>
<td colspan='2'><div id="txt_current_tenant_name" runat="server" />
</td>
</tr>

</table>
<br />

<table >
<tr>
<td valign="top"><table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td>
            <b>Current Rent amount</b>&nbsp;</td>
                                          <td>
                                              : <asp:Label ID="lbl_rent_amount" runat="server" />
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
    <b>Since</b></td>
                                          <td>
                                              :
<asp:Label ID="lbl_current_rl_date_begin" runat="server" style="color: #FF3300"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          </td>
                                      </tr>
                                  </table>
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              </td>
    <td valign="top">
        <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="100%" BorderColor="White" BorderWidth="3"
        GridLines="Both"   
          AutoGenerateColumns="false"   AlternatingRowStyle-BackColor="Beige" ID="gv_rent_list" runat="server">
        
         <Columns>
   <asp:BoundField   DataField="rl_rent_amount" DataFormatString="{0:0.00}"  
     HeaderText="Previous Rent amount" HtmlEncode="false" /> 
   <asp:BoundField   DataField="rl_date_begin" DataFormatString="{0:M-dd-yyyy}"  
     HeaderText="Date Begin" HtmlEncode="false" />
   <asp:BoundField   DataField="rl_date_end" DataFormatString="{0:M-dd-yyyy}"  
     HeaderText="Date End" HtmlEncode="false" />
      <asp:BoundField DataField="rl_id" HeaderText="rl_id"   />
      
   </Columns>
        
        
        </asp:GridView>
                              </td>
</tr> 
   
 </table >
<br />

<table width="100%">
     
     
     <tr>
     <td bgcolor="aliceblue" colspan="2">
         <b>UTILITIES &amp; ACCOMMODATIONS</b><br />
    
     </td></tr>
     
         
     <tr>
     <td valign="top" >
     <b>Accommodation Since </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :
 <asp:Label ID="lbl_current_ta_date_begin" runat=server style="color: #FF3300"></asp:Label>
     
     </td>
     
      <td valign="top" rowspan="2">
          <asp:GridView  HeaderStyle-BackColor="AliceBlue" Width="100%" BorderColor="White" BorderWidth="3"
        GridLines="Both"  
          AutoGenerateColumns="false"   AlternatingRowStyle-BackColor="Beige"   ID="gv_accommodation_list" runat="server">
          
          
           <Columns>
   
   <asp:BoundField   DataField="ta_date_begin" DataFormatString="{0:M-dd-yyyy}"  
     HeaderText="Previous date Begin" HtmlEncode="false" />
   <asp:BoundField   DataField="ta_date_end" DataFormatString="{0:M-dd-yyyy}"  
     HeaderText="Previous date End" HtmlEncode="false" />
      <asp:BoundField DataField="ta_id" HeaderText="ta_id"   />
      
      <asp:HyperLinkField   Text="View"
     DataNavigateUrlFields="tu_id,ta_id" 
     DataNavigateUrlFormatString="~/manager/lease/lease_accommodation_archive_view.aspx?tu_id={0}&ta_id={1}" 
      HeaderText="View" />
      
   </Columns>
          
          
          </asp:GridView>
     
     
          <br />
     
     
     </td></tr>
     
         
     <tr>
     <td >
         <strong><span style="font-size: 10pt">
         Utilities includes</span></strong><br />
     <asp:CheckBox AutoPostBack=true CssClass="letter" ID="ta_electricity_inc" text="Electricity" runat="server"  />
     <br />
     <asp:CheckBox AutoPostBack=true CssClass="letter" ID="ta_heat_inc" text="Heat" runat="server"  />
     <br />
     <asp:CheckBox AutoPostBack=true CssClass="letter" ID="ta_water_inc" text="Water" runat="server"   />
     <br />
     <asp:CheckBox AutoPostBack=true CssClass="letter" ID="ta_water_tax_inc" text="Water tax" runat="server"   />
     <br />
     <asp:CheckBox AutoPostBack=true CssClass="letter" ID="ta_none" text="None ( utilities are not included in the lease )" runat="server"   />
    
     
     
     
     <br /><br />
     
     
     
       
         <span style="font-size: 10pt"><strong>Accomodations included</strong></span><br /><br />
     <asp:CheckBox CssClass="letter" ID="ta_parking_inc" text="Parking" runat="server" />
     <br />
     <asp:CheckBox CssClass="letter" ID="ta_garage_inc" text="Garage" runat="server" />
     <br />
     <asp:CheckBox CssClass="letter" ID="ta_furnished_inc" text="Furnished" runat="server" />
     <br />
     <asp:CheckBox CssClass="letter" ID="ta_semi_furnished_inc" text="Semi-furnished" runat="server" />
     <br />
     
     </td>
     
      </tr>
     
         
           <tr><td class="letter_bold" valign=top style="font-weight: bold">Comments & extras :</td>
                <td>&nbsp; <asp:Label ID="lbl_ta_com" CssClass="letter" runat="server"  Width="389px"/>
                    <br />
                              </td>
            </tr>
    <tr><td colspan="2" style="height: 17px">
     
     <table>

 <tr>
 <td>
     &nbsp;</td>
 </tr>
</table>
     
     
      <br />
        <table width="100%">
        <tr>
        <td bgcolor="aliceblue"><b>TERMS &amp; CONDITIONS</b><br /></td>
        </tr>
        </table>
     
      <br />
     </td></tr>
          
         </table>
    
  
  <table width="100%">
          
          
          <tr>
<td valign="top" style="width: 234px">
    <b>Form of payment </b> </td>
<td colspan="2">
    <asp:Label ID="tt_form_of_payment" runat="server" ></asp:Label>
</td>
</tr>
<tr>
<td valign="top" colspan="3">
    
    <table width="100%"><tr><td style="color: #D3D3D3">
        ____________________________________________________________________________________________________________________________</td></tr></table>
    </td>
</tr>
<tr>
<td valign="top" style="width: 234px">
    <b>Non-sufficient fund fee</b>  &nbsp; </td>
<td colspan="2">
<asp:Label ID="tt_nsf" runat="server" Width="128px"/></td>
</tr>
        <tr>
        <td  valign=top colspan="3"> 
    
    <table width="100%"><tr><td style="color: #D3D3D3">
        ____________________________________________________________________________________________________________________________</td></tr></table>
            </td>
        
        
        </tr>
        <tr>
        <td  valign=top style="width: 234px"><b>late fee</b></td>
        <td>
            <asp:Label ID="tt_late_fee" runat="server" Width="128px"/>
        </td>
        
        
        </tr>
        <tr><td valign=top colspan="3">
    
    <table width="100%"><tr><td style="color: #D3D3D3">
        ____________________________________________________________________________________________________________________________</td></tr></table>
            </td>
       </tr>
            
            
          
            
            
        <tr><td valign=top style="width: 234px"><b>Security deposit required</b></td><td>
            <asp:RadioButtonList CssClass="letter" ID="tt_security_deposit" runat="server" 
                                  RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
                   <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
            
            <asp:Panel ID="Panel_security_deposit" Visible=true runat="server" >
            
            
                &nbsp;&nbsp;&nbsp;amount&nbsp;: <asp:Label ID="tt_security_deposit_amount" runat="server"/>
     </asp:Panel>       
            
            
            
            </td>
       </tr>
            
             
            
            
        <tr><td valign=top colspan="3"> 
    
    <table width="100%"><tr><td style="color: #D3D3D3">
        ____________________________________________________________________________________________________________________________</td></tr></table>
            </td></tr>
           
        <tr><td valign=top style="width: 234px"><b>Guarantor required</b> </td><td>
            <asp:RadioButtonList CssClass="letter" ID="tt_guarantor" runat="server" 
                AutoPostBack="true" RepeatDirection="Horizontal"   >
            <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
              <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
            <asp:Panel ID="panel_guarantor" runat="server" Visible="false">
                <b>&nbsp; name</b>&nbsp;: 
                <asp:Label ID="guarantor_name" runat="server" ></asp:Label>
                <br />
                
                <!--  BEGIN GUARANTOR INFORMATIONS    -->
                
                <!-- END GUARANTOR INFORMATIONS      -->
            </asp:Panel>
            </td></tr>
            
            
            
            
            
            
            
 
            
            
            
            
            
            
            
            
            
            
        <tr><td valign=top colspan="3"> 
    
    <table width="100%"><tr><td style="color: #D3D3D3">
        ____________________________________________________________________________________________________________________________</td></tr></table>
            </td></tr>
        <tr>
        <td valign=top style="width: 234px"><b>Pets allowed</b></td>
        <td>
            <asp:RadioButtonList   CssClass="letter" ID="tt_pets" runat="server" 
                RepeatDirection="Horizontal" >
                <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
                <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
         </td>
        </tr>
        <tr valign="top">
            <td valign=top colspan="3"> 
    
    <table width="100%"><tr><td style="color: #D3D3D3">
        ____________________________________________________________________________________________________________________________</td></tr></table>
            </td>
            
        </tr>
        <tr valign="top">
            <td valign=top style="width: 234px"><b>Tenant is responsible for maintenance</b></td>
         <td>
          <asp:RadioButtonList   CssClass="letter" ID="tt_maintenance" runat="server" RepeatDirection="Horizontal" 
                  >
            <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
            <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
           <br />
                &nbsp;&nbsp;&nbsp;<b>Maintenance allowed</b><br />&nbsp;<asp:Label runat=server 
                     ID="tt_specify_maintenance"  Width="300px" />
           
         
         </td> 
            
        </tr>
        <tr>
        <td valign=top colspan="3"> 
    
    <table width="100%"><tr><td style="color: #D3D3D3">
        ____________________________________________________________________________________________________________________________</td></tr></table>
            </td>
        </tr>
        <tr>
        <td valign=top style="width: 234px"><b>Can tenant make improvement</b></td>
        <td>
            <asp:RadioButtonList   CssClass="letter" ID="tt_improvement" runat="server" RepeatDirection="Horizontal" 
                 >
                <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
                <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
             <asp:Panel Visible=true ID="panel_specify_improvement" runat="server">
                 &nbsp;&nbsp;&nbsp;<b>Improvement allowed</b><br />
                 &nbsp;<asp:Label runat=server Width="300px" ID="tt_specify_improvement" 
                      />
            </asp:Panel>
         
        </td>
        </tr>
        <tr>
        <td valign=top colspan="3"> 
    
    <table width="100%"><tr><td style="color: #D3D3D3">
        ____________________________________________________________________________________________________________________________</td></tr></table>
            </td>
        </tr>
        <tr>
        <td valign=top style="width: 234px"><b>Notice to enter</b> </td>
        <td>
            <asp:RadioButtonList ID="tt_notice_to_enter" runat="server" CssClass="letter" RepeatDirection="Horizontal" 
               >
                <asp:ListItem Selected="True" Value="0">legal minimum</asp:ListItem>
                <asp:ListItem Value="1">number of hours</asp:ListItem>
            </asp:RadioButtonList>
            <br />
                &nbsp;<b>Number of hours notice</b>&nbsp;: <asp:Label ID="tt_specify_number_of_hours" 
                    runat="server" Width="100px"/>
   
        </td>
        </tr>
        <tr><td valign=top colspan="3"> 
    
    <table width="100%"><tr><td style="color: #D3D3D3">
        ____________________________________________________________________________________________________________________________</td></tr></table>
            </td>
        
        </tr>
        
        
        
        <tr><td valign=top style="width: 234px"><b>Who pay these insurances</b></td>
        
        <td>
            <table>
                <tr>
                    <td>
                        <b>tenant content</b></td>
                    <td valign="top">
                        :
                        <asp:Label ID="tt_tenant_content_ins" runat="server" ></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>landlord content</b></td>
                    <td valign="top">
                        :<asp:Label ID="tt_landlord_content_ins" runat="server" ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>personal injury on property</b></td>
                    <td valign="top">
                        :<asp:Label ID="tt_injury_ins" runat="server" ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>lease premises</b></td>
                    <td valign="top">
                        :
                        <asp:Label ID="tt_premises_ins" runat="server" ></asp:Label>
                    </td>
                </tr>
            </table>
            </td>
        
        </tr>
        
        
        
        <tr><td valign="top" colspan="3">
    
    <table width="100%"><tr><td style="color: #D3D3D3">
        ____________________________________________________________________________________________________________________________</td></tr></table>
             </td>
              </tr>
          
        
        
        <tr><td valign="top" style="width: 234px">
            <b>Additional terms &amp; conditions</b></td>
            <td>
                <asp:Label ID="tt_additional_terms" runat="server" Height="200px" 
                    Width="400px"/> 
                <br />
                <br />
            </td>
            <td valign="top">
                &nbsp;</td>
              </tr>
          <tr>
          <td colspan="3" valign="top" >
              <table cellpadding="0" cellspacing="0" style="width: 100%">
                  <tr>
                      <td valign="top">
              <b>Terms and conditions since : </b>
  
   <asp:Label ID="lbl_current_tt_date_begin" runat="server" style="color: #FF3300"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </td>
                      <td valign="top">
          
          
     &nbsp;
          
          
     <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="100%" BorderColor="White" BorderWidth="3"
        GridLines="Both"  
          AutoGenerateColumns="false"   AlternatingRowStyle-BackColor="Beige"  ID="gv_terms_list" runat="server"  >
         
           <Columns>
   
   <asp:BoundField   DataField="tt_date_begin" DataFormatString="{0:M-dd-yyyy}"  
     HeaderText="Previous date Begin" HtmlEncode="false" />
   <asp:BoundField   DataField="tt_date_end" DataFormatString="{0:M-dd-yyyy}"  
     HeaderText="Previous date End" HtmlEncode="false" />
      <asp:BoundField DataField="tt_id" HeaderText="ta_id"   />
      
      <asp:HyperLinkField   Text="View"
     DataNavigateUrlFields="tu_id,tt_id" 
     DataNavigateUrlFormatString="~/manager/lease/lease_termsandconditions_archive_view.aspx?tu_id={0}&tt_id={1}" 
      HeaderText="View" />
      
   </Columns>           
                    
                    
     </asp:GridView>
     
     
     
                      </td>
                  </tr>
              </table>
              <br />
              </td>
          </tr>
              <tr>
                  <td style="width: 234px">
                      <div ID="txt_link" runat="server" />
                      </td>
                  </tr>
        </table>
         
</asp:Content>

