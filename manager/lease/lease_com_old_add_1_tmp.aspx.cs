﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : jan 3 , 2009
/// </summary>

public partial class manager_lease_lease_com_old_add_1_tmp : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        

        if (!Page.IsPostBack)
        {
            reg_rl_rent_amount.ValidationExpression = RegEx.getMoney();
            reg_rl_rent_amount_no.ValidationExpression = RegEx.getMoney();

            reg_company_city.ValidationExpression = RegEx.getText();
            reg_company_addr.ValidationExpression = RegEx.getText();
            reg_company_contact_email.ValidationExpression = RegEx.getEmail();
            reg_company_name.ValidationExpression = RegEx.getText();
            reg_company_tel.ValidationExpression = RegEx.getText();
            reg_company_website.ValidationExpression = RegEx.getText();
            reg_company_com.ValidationExpression = RegEx.getText();
            reg_company_fax.ValidationExpression = RegEx.getText();
            reg_new_rl_rent_amount.ValidationExpression = RegEx.getMoney();
           

            panel_new_company.Visible = false;
            panel_company.Visible = false;

            var currentYear = DateTime.Today.Year;
            for (int i = 60; i >= 0; i--)
            {
                // Now just add an entry that's the current year minus the counter
                ddl_to_y.Items.Add((currentYear - i).ToString());
            }

            tiger.Country ic = new tiger.Country(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_country_id_1.DataSource = ic.getCountryList();
            ddl_country_id_1.DataBind();


            //Names in the system
            tiger.Name n = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_name_id_1.DataSource = n.getNameList(Convert.ToInt32(Session["schema_id"]), 0);
            ddl_name_id_1.DataBind();
            ddl_name_id_1.Items.Insert(0, "Select a name");
            ddl_name_id_1.SelectedIndex = 0;


            // list of tenant in the lease we are going to create
            rTenantTempoList.Visible = false;
            lbl_tenants.Visible = false;


            // list of company in the "leaser" category
            tiger.Company comp = new tiger.Company(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_company.DataSource = comp.getCompanyLeaseList(Convert.ToInt32(Session["schema_id"]));
            ddl_company.DataBind();


            // if there is no company yet in the system , render the control not visible
           if (ddl_company.Items.Count == 0)
            {
               ddl_company.Visible = false;
               chb_company.Visible = false;
            }



            DateTime to = new DateTime();
            DateTime from = new DateTime();
            to = DateTime.Now; // the date in the to drop downlist

            tiger.Date d = new tiger.Date();
            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            to = Convert.ToDateTime(d.DateCulture(to.Month.ToString(), to.Day.ToString(), to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
            from = Convert.ToDateTime(d.DateCulture("1", "1", to.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


            // First we check if there's home available
            tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = h.getHomeComCount(Convert.ToInt32(Session["schema_id"]));
            string link_to_unit = "";
            if (home_count > 0)
            {
                int home_id = h.getHomeComFirstId(Convert.ToInt32(Session["schema_id"]));
                if (!string.IsNullOrEmpty(Request.QueryString["h_id"]))
                {
                    home_id = Convert.ToInt32(Request.QueryString["h_id"]);
                }
                // link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                //Session["schema_id"]));


                ddl_home_id.Visible = true;


                ddl_home_id.DataSource = h.getHomeComList(Convert.ToInt32(Session["schema_id"]));
                ddl_home_id.SelectedValue = Convert.ToString(home_id);
                ddl_home_id.DataBind();



                //To view the address of the property

                tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
                rhome_view.DataBind();



                //*********************************************
                // DropDownList pour les Unit
                //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitComCount(Convert.ToInt32(Session["schema_id"]), home_id);
                int unit_id = 0;

                if (unit_count > 0)
                {

                    unit_id = u.getUnitComFirstId(Convert.ToInt32(Session["schema_id"]), home_id);
                    if (!string.IsNullOrEmpty(Request.QueryString["unit_id"]))
                    {
                        unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                    }

                    ddl_unit_id.DataSource = u.getUnitComList(Convert.ToInt32(Session["schema_id"]), home_id);
                    ddl_unit_id.SelectedValue = Convert.ToString(unit_id);
                    ddl_unit_id.DataBind();
                }

                 // if ther is no unit
                else
                {

                    //   txt_message.InnerHtml = "There is no unit in this property -- add unit";

                }

                //hidden fields



            }
            // if ther is no home

            else
            {
                //  txt_message.InnerHtml = "There is no property -- Add a property";

                //  txt_link.InnerHtml = home_count + " <a href='home_add.aspx'>Add Home</a>&nbsp;&nbsp;<a href='owner_add.aspx'>Add Owner</a>&nbsp;&nbsp;<a href='janitor_add.aspx'>Add Janitor</a>&nbsp;&nbsp;";
            }



            // by default the tenant_id we are going to create is = 0 , 
            if (hd_tenant_id.Value == null || hd_tenant_id.Value == "")
            {
                hd_tenant_id.Value = "0";
            }


            if (hd_tenant_unit_id.Value == null || hd_tenant_unit_id.Value == "")
            {
                hd_tenant_unit_id.Value = "0";
            }

        }
    }
    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {
       
            ddl_home_id.Visible = true;

            tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
            rhome_view.DataBind();

            tiger.Unit u = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            int unit_id = u.getUnitComFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
            //change list of unit id

            if (unit_id > 0)
            {
                ddl_unit_id.Visible = true;
                //first dispose of old ddl_unit
                ddl_unit_id.Dispose();
                ddl_unit_id.DataSource = u.getUnitComList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
                ddl_unit_id.DataBind();
                ddl_unit_id.SelectedValue = Convert.ToString(unit_id);


            }
            else
            {
                // txt_message.InnerHtml = "There is no unit in this house -- add unit(s)";

                //  txt_link.InnerHtml = "<a href='unit_add.aspx?home_id='>Add Unit</a>";


            }
            //   }// fin if not postback
       
    }

    protected void btn_next_Click(object sender, EventArgs e)
    {
        DateTime to = new DateTime();
        DateTime from = new DateTime();


        tiger.Date d = new tiger.Date();
        // if lease date begin and lease date end are valid date and date_to is greater then date_from
        if (ddl_to_m.SelectedIndex > 0 && ddl_to_d.SelectedIndex > 0 && ddl_to_y.SelectedIndex > 0 && ddl_from_m.SelectedIndex > 0 && ddl_from_d.SelectedIndex > 0 && ddl_from_y.SelectedIndex > 0)
        {
            to = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            from = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

            if (tiger.Date.isDate(Convert.ToString(to)) && tiger.Date.isDate(Convert.ToString(from)) && (to > from))
            {
                MultiView1.ActiveViewIndex = 1;
            }
        }

        lbl_home.Text = ddl_home_id.SelectedItem.Text;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chb_company_CheckedChanged(object sender, EventArgs e)
    {
        if (chb_company.Checked)
        {
            panel_company.Visible = true;
            panel_new_company.Visible = false;
            chb_newcompany.Checked = false;
        }
        else
        {
            panel_company.Visible = false;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chb_newcompany_CheckedChanged(object sender, EventArgs e)
    {
        if (chb_newcompany.Checked)
        {

            panel_new_company.Visible = true;
            panel_company.Visible = false;
            chb_company.Checked = false;
        }
        else
        {
            panel_new_company.Visible = false;

        }
    }
   
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_next_1_Click(object sender, EventArgs e)
    {
 
        
        tiger.Company c = new tiger.Company(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

         if (chb_newcompany.Checked == true)
          {
              Page.Validate("vg_company");
              if (!Page.IsValid)
              {
                  Session.Abandon();
                  Response.Redirect("~/login.aspx");
              }

            if ((company_name.Text == String.Empty || company_tel.Text == String.Empty))
            { }
            else
                if (!c.CompanyExist(Convert.ToInt32(Session["schema_id"]),company_name.Text))       
                  MultiView1.ActiveViewIndex = 2;
          }

         if(chb_company.Checked)
            MultiView1.ActiveViewIndex = 2;
    
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_previous_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chb_name_add_1_CheckedChanged(object sender, EventArgs e)
    {
        if (chb_name_add_1.Checked)
        {
            panel_name_add_1.Visible = true;

        }
        else
        {
            panel_name_add_1.Visible = false;

        }
    }

  
    protected void btn_previous_1_Click(object sender, EventArgs e)
    {

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_other_Click(object sender, EventArgs e)
    {

        AccountObjectAuthorization tenantTempoAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
     //   if (!tenantTempoAuthorization.TenantTempo(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_tenant_id.Value)) && Convert.ToInt32(hd_tenant_id.Value) != 0)
        {
     //       Session.Abandon();
     //       Response.Redirect("~/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////



        int new_tenant_id = 0;
        int new_tenant_unit_id = 0;
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prTenantAddTempo", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return_new_tenant_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = Convert.ToInt32(hd_tenant_id.Value);



            if (chb_1.Checked == true && ddl_name_id_1.SelectedIndex > 0)
                cmd.Parameters.Add("@name_id_1", SqlDbType.Int).Value = Convert.ToInt32(ddl_name_id_1.SelectedValue);
            else
                cmd.Parameters.Add("@name_id_1", SqlDbType.Int).Value = 0;

            // new tenants paramater - new person add to the database   
            //name 1
            cmd.Parameters.Add("@name_lname_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_lname_1.Text);
            cmd.Parameters.Add("@name_fname_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_fname_1.Text);


            cmd.Parameters.Add("@name_addr_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_1.Text);
            cmd.Parameters.Add("@name_addr_city_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_city_1.Text);
            cmd.Parameters.Add("@name_addr_pc_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_pc_1.Text);
            cmd.Parameters.Add("@name_addr_state_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_state_1.Text);

            if (chb_name_add_1.Checked == false)
                cmd.Parameters.Add("@country_id_1", SqlDbType.Int).Value = System.DBNull.Value;
            else
                cmd.Parameters.Add("@country_id_1", SqlDbType.Int).Value = Convert.ToInt32(ddl_country_id_1.SelectedValue);

            cmd.Parameters.Add("@name_tel_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_tel_1.Text);
            cmd.Parameters.Add("@name_tel_work_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_tel_work_1.Text);
            cmd.Parameters.Add("@name_tel_work_ext_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_tel_work_ext_1.Text);

            tiger.Date d = new tiger.Date();

            cmd.Parameters.Add("@name_cell_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_cell_1.Text);
            cmd.Parameters.Add("@name_fax_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_fax_1.Text);
            cmd.Parameters.Add("@name_email_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_email_1.Text);
            cmd.Parameters.Add("@name_com_1", SqlDbType.Text).Value = RegEx.getText(name_com_1.Text);

            if (chb_name_add_1.Checked == false)
                cmd.Parameters.Add("@name_dob_1", SqlDbType.DateTime).Value = System.DBNull.Value;
            else
                cmd.Parameters.Add("@name_dob_1", SqlDbType.DateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_name_1_dob_m.SelectedValue, ddl_name_1_dob_d.SelectedValue, ddl_name_1_dob_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            if (Convert.ToInt32(cmd.Parameters["@return_new_tenant_id"].Value) > 0)
            {
                new_tenant_id = Convert.ToInt32(cmd.Parameters["@return_new_tenant_id"].Value);
                hd_tenant_id.Value = Convert.ToString(new_tenant_id);


                // If we have a new tenant_id , display the tenants
                tiger.Tenant g = new tiger.Tenant(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                rTenantTempoList.DataSource = g.getTenantTempoList(Convert.ToInt32(Session["schema_id"]), new_tenant_id);
                rTenantTempoList.DataBind();
                rTenantTempoList.Visible = true;
                lbl_tenants.Visible = true;

            }

        }
        catch (Exception error)
        {
            //   tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }

        conn.Close();

        //  Server.Transfer("lease_add_2_tmp.aspx",true);   
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_yes_Click(object sender, EventArgs e)
    {
        panel_rent.Visible = true;
        panel_rent_no.Visible = false;
        btn_no.Visible = false;
        btn_yes.Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_no_Click(object sender, EventArgs e)
    {
        panel_rent_no.Visible = true;
        panel_rent.Visible = false;
        btn_no.Visible = false;
        btn_yes.Visible = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_next_3_Click(object sender, EventArgs e)
    {

        Page.Validate("vg_acc");
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prRentTempoInitialUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            //cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_tenant_unit_id.Value);
            cmd.Parameters.Add("@rl_rent_amount", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(rl_rent_amount.Text));
            cmd.Parameters.Add("@rl_rent_paid_every", SqlDbType.Int).Value = Convert.ToInt32(ddl_rl_rent_paid_every.SelectedValue);
            cmd.ExecuteReader();
        }
        catch
        {
            // TODO

        }

        finally
        {

            conn.Close();
        }


        // Create an archive lease ( only including the rent logs and tenants)
        SqlCommand cmd2 = new SqlCommand("prCreateComOldLease", conn);
        cmd2.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params

            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd2.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd2.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_tenant_unit_id.Value);



            if (chb_company.Checked)
            {

                cmd2.Parameters.Add("@company_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_company.SelectedValue);

                cmd2.Parameters.Add("@company_name", SqlDbType.NVarChar, 50).Value = "";
                cmd2.Parameters.Add("@company_website", SqlDbType.NVarChar, 200).Value = "";
                cmd2.Parameters.Add("@company_addr_street", SqlDbType.NVarChar, 200).Value = "";
                cmd2.Parameters.Add("@company_city", SqlDbType.NVarChar, 50).Value = "";
                cmd2.Parameters.Add("@company_tel", SqlDbType.NVarChar, 50).Value = "";
                cmd2.Parameters.Add("@company_contact_email", SqlDbType.NVarChar, 50).Value = "";
                cmd2.Parameters.Add("@company_contact_fax", SqlDbType.NVarChar, 50).Value = "";
                cmd2.Parameters.Add("@company_com", SqlDbType.Text).Value = "";

            }

            if (chb_newcompany.Checked)
            {
                cmd2.Parameters.Add("@company_id", SqlDbType.Int).Value = 0;

                cmd2.Parameters.Add("@company_name", SqlDbType.NVarChar, 50).Value = RegEx.getText(company_name.Text);
                cmd2.Parameters.Add("@company_website", SqlDbType.NVarChar, 200).Value = RegEx.getText(company_website.Text);
                cmd2.Parameters.Add("@company_addr_street", SqlDbType.NVarChar, 200).Value = RegEx.getText(company_addr.Text);
                cmd2.Parameters.Add("@company_city", SqlDbType.NVarChar, 50).Value = RegEx.getText(company_city.Text);
                cmd2.Parameters.Add("@company_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(company_tel.Text);
                cmd2.Parameters.Add("@company_contact_email", SqlDbType.NVarChar, 50).Value = RegEx.getText(company_email.Text);
                cmd2.Parameters.Add("@company_contact_fax", SqlDbType.NVarChar, 50).Value = RegEx.getText(company_fax.Text);
                cmd2.Parameters.Add("@company_com", SqlDbType.Text).Value = RegEx.getText(company_com.Text);

            }

            
            
            
            
            
            cmd2.ExecuteReader();
        }

        finally
        {

            conn.Close();
        }

     //   Response.Redirect("lease_add.aspx");


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        // dropdownlist rent frequency
        tiger.Home v = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ddl_rent_frequency.DataSource = v.getHomeUnitRentedSumListArchives(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
        ddl_rent_frequency.DataBind();

        int rent_frequency = Convert.ToInt32(ddl_rent_frequency.SelectedItem.Value);

        // here we get the list of untreated rented unit ( the chlid gridview )
        tiger.Home uv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_parent_nested_unpaid_rent_month.DataSource = uv.getHomeUnitRentedListArchives(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), rent_frequency);
        gv_parent_nested_unpaid_rent_month.DataBind();

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        MultiView1.ActiveViewIndex = 5 ;

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_next_4_Click(object sender, EventArgs e)
    {


        Page.Validate("vg_acc_2");
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        //To view the address of the property

       /// tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
       /// rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
       /// rhome_view.DataBind();
        
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prRentTempoInitialUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            //cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_tenant_unit_id.Value);
            cmd.Parameters.Add("@rl_rent_amount", SqlDbType.Money).Value = Convert.ToDecimal(rl_rent_amount_no.Text.Replace(",", "."));
            cmd.Parameters.Add("@rl_rent_paid_every", SqlDbType.Int).Value = Convert.ToInt32(ddl_rl_rent_paid_every_no.SelectedValue);
            cmd.ExecuteReader();
        }
        catch
        {
            // TODO

        }

        finally
        {

            conn.Close();
        }

        MultiView1.ActiveViewIndex = 4;


        tiger.Rent h = new tiger.Rent(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        rRentlog.DataSource = h.getRentlogTempoView(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_tenant_unit_id.Value));
        rRentlog.DataBind();
    }
    protected void btn_add_other_rent_Onclick(object sender, EventArgs e)
    {

        Page.Validate("vg_acc_3");
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        
        // add to a new rent amount in the rent log ,for the archives rents ( leases ) 
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prRentTempoUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_tenant_unit_id.Value);
            cmd.Parameters.Add("@rl_rent_amount", SqlDbType.Money).Value = Convert.ToDecimal(new_rl_rent_amount.Text.Replace(",", "."));
            cmd.Parameters.Add("@rl_rent_paid_every", SqlDbType.Int).Value = Convert.ToInt32(ddl_rl_rent_paid_every.SelectedValue);

            tiger.Date d = new tiger.Date();

            cmd.Parameters.Add("@rl_date_begin", SqlDbType.SmallDateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_rl_date_begin_m.SelectedValue, ddl_rl_date_begin_d.SelectedValue, ddl_rl_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            cmd.Parameters.Add("@current_rl_date_end", SqlDbType.SmallDateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_rl_date_begin_m.SelectedValue, ddl_rl_date_begin_d.SelectedValue, ddl_rl_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

            cmd.ExecuteReader();
        }
        catch
        {
            // TODO

        }

        finally
        {

            conn.Close();
        }


        //**************************************************************************************************

        if (!RegEx.IsInteger(Request.QueryString["tu_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        // View the tempo rent INFO
        SqlCommand cmd2 = new SqlCommand("prRentTempoView", conn);
        cmd2.CommandType = CommandType.StoredProcedure;

        //Add the params
        cmd2.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd2.Parameters.Add("tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);
        try
        {
            conn.Open();

            SqlDataReader dr2 = null;
            dr2 = cmd2.ExecuteReader(CommandBehavior.SingleRow);

            while (dr2.Read() == true)
            {
                lbl_rent_amount.Text = Convert.ToString(Convert.ToInt32(dr2["rl_rent_amount"]));
                ddl_rl_rent_paid_every.SelectedValue = dr2["re_id"].ToString();
                lbl_current_rl_date_begin.Text = dr2["rl_date_begin"].ToString();
            }
        }

        finally
        {

            conn.Close();
        }

        tiger.Rent h = new tiger.Rent(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        rRentlog.DataSource = h.getRentlogTempoView(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_tenant_unit_id.Value));
        rRentlog.DataBind();
        rRentlog.Visible = true;

    }
   

    protected string GetDate(DateTime date)
    {
        string ladate = date.Month.ToString() + "/" + date.Day.ToString() + "/" + date.Year.ToString();

        if (ladate == "1/1/1900")
            ladate = "";

        return ladate;

    }


    protected void btn_finish_Onclick(object sender, EventArgs e)
    {
        Page.Validate("vg_acc_3");
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        if (!RegEx.IsInteger(Request.QueryString["tu_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        
        // add to a new rent amount in the rent log ,for the previous rent 
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prRentTempoUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);
            cmd.Parameters.Add("@rl_rent_amount", SqlDbType.Money).Value = Convert.ToDecimal(new_rl_rent_amount.Text.Replace(",", "."));
            cmd.Parameters.Add("@rl_rent_paid_every", SqlDbType.Int).Value = Convert.ToInt32(ddl_rl_rent_paid_every.SelectedValue);

            tiger.Date d = new tiger.Date();

            cmd.Parameters.Add("@rl_date_begin", SqlDbType.SmallDateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_rl_date_begin_m.SelectedValue, ddl_rl_date_begin_d.SelectedValue, ddl_rl_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            cmd.Parameters.Add("@current_rl_date_end", SqlDbType.SmallDateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_rl_date_begin_m.SelectedValue, ddl_rl_date_begin_d.SelectedValue, ddl_rl_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

            cmd.ExecuteReader();
        }
        catch
        {
            // TODO

        }

        finally
        {

            conn.Close();
        }



        // Create an archive lease ( only including the rent logs and tenants)
        SqlCommand cmd2 = new SqlCommand("prCreateComOldLease", conn);
        cmd2.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params

            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd2.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd2.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_tenant_unit_id.Value);



            if (chb_company.Checked)
            {

                cmd2.Parameters.Add("@company_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_company.SelectedValue);

                cmd2.Parameters.Add("@company_name", SqlDbType.NVarChar, 50).Value = "";
                cmd2.Parameters.Add("@company_website", SqlDbType.NVarChar, 200).Value = "";
                cmd2.Parameters.Add("@company_addr_street", SqlDbType.NVarChar, 200).Value = "";
                cmd2.Parameters.Add("@company_city", SqlDbType.NVarChar, 50).Value = "";
                cmd2.Parameters.Add("@company_tel", SqlDbType.NVarChar, 50).Value = "";
                cmd2.Parameters.Add("@company_contact_email", SqlDbType.NVarChar, 50).Value = "";
                cmd2.Parameters.Add("@company_contact_fax", SqlDbType.NVarChar, 50).Value = "";
                cmd2.Parameters.Add("@company_com", SqlDbType.Text).Value = "";

            }

            if (chb_newcompany.Checked)
            {
                cmd2.Parameters.Add("@company_id", SqlDbType.Int).Value = 0;

                cmd2.Parameters.Add("@company_name", SqlDbType.NVarChar, 50).Value = RegEx.getText(company_name.Text);
                cmd2.Parameters.Add("@company_website", SqlDbType.NVarChar, 200).Value = RegEx.getText(company_website.Text);
                cmd2.Parameters.Add("@company_addr_street", SqlDbType.NVarChar, 200).Value = RegEx.getText(company_addr.Text);
                cmd2.Parameters.Add("@company_city", SqlDbType.NVarChar, 50).Value = RegEx.getText(company_city.Text);
                cmd2.Parameters.Add("@company_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(company_tel.Text);
                cmd2.Parameters.Add("@company_contact_email", SqlDbType.NVarChar, 50).Value = RegEx.getText(company_email.Text);
                cmd2.Parameters.Add("@company_contact_fax", SqlDbType.NVarChar, 50).Value = RegEx.getText(company_fax.Text);
                cmd2.Parameters.Add("@company_com", SqlDbType.Text).Value = RegEx.getText(company_com.Text);

            }

              cmd2.ExecuteReader();
        }

        finally
        {

            conn.Close();
        }


       
       // Response.Redirect("../tenant/tenant_untreated_rent.aspx");


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // dropdownlist rent frequency
        tiger.Home v = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ddl_rent_frequency.DataSource = v.getHomeUnitRentedSumListArchives(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
        ddl_rent_frequency.DataBind();

        int rent_frequency = Convert.ToInt32(ddl_rent_frequency.SelectedItem.Value);

        // here we get the list of untreated rented unit ( the chlid gridview )
        tiger.Home uv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_parent_nested_unpaid_rent_month.DataSource = uv.getHomeUnitRentedListArchives(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), rent_frequency);
        gv_parent_nested_unpaid_rent_month.DataBind();

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        MultiView1.ActiveViewIndex = 5;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chb_1_CheckedChanged(object sender, EventArgs e)
    {
        if (chb_1.Checked)
        {
            panel_1.Visible = true;
        }
        else
        {
            panel_1.Visible = false;
        }
    }
    protected void btn_next_2_Click(object sender, EventArgs e)
    {


        panel_rent_no.Visible = true;
         panel_rent.Visible = true;

         btn_no.Visible = true;
         btn_yes.Visible = true;

        AccountObjectAuthorization tenantTempoAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
       // if (!tenantTempoAuthorization.TenantTempo(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_tenant_id.Value)) && Convert.ToInt32(hd_tenant_id.Value) != 0)
        {
          //  Session.Abandon();
          //  Response.Redirect("~/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////


        int new_tenant_id = 0;
        int new_tenant_unit_id = 0;
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
  
        SqlConnection conn = new SqlConnection(strconn);
    

        //  string strconn2 = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        // SqlConnection conn2 = new SqlConnection(strconn2);
        SqlCommand cmd2 = new SqlCommand("prTenantUnitAddTempo", conn);
        cmd2.CommandType = CommandType.StoredProcedure;


        //  try
        {
            conn.Open();
            //Add the params     @return_new_tenant_unit_id
            cmd2.Parameters.Add("@return_new_tenant_unit_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@new_tenant_id", SqlDbType.Int).Value = Convert.ToInt32(hd_tenant_id.Value);

            // new tenants paramater - new person add to the database   
            //name 1
            cmd2.Parameters.Add("@unit_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_unit_id.SelectedValue);


            tiger.Date d = new tiger.Date();
            DateTime ladate = new DateTime();
            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            ladate = Convert.ToDateTime(d.DateCulture(ddl_from_m.SelectedValue, ddl_from_d.SelectedValue, ddl_from_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));         
            cmd2.Parameters.Add("@tu_date_begin", SqlDbType.DateTime).Value = ladate;

            ladate = Convert.ToDateTime(d.DateCulture(ddl_to_m.SelectedValue, ddl_to_d.SelectedValue, ddl_to_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
            cmd2.Parameters.Add("@tu_date_end", SqlDbType.DateTime).Value = ladate;

            //execute the insert
            cmd2.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            if (Convert.ToInt32(cmd2.Parameters["@return_new_tenant_unit_id"].Value) > 0)
            {
                new_tenant_unit_id = Convert.ToInt32(cmd2.Parameters["@return_new_tenant_unit_id"].Value);
                hd_tenant_unit_id.Value = Convert.ToString(new_tenant_unit_id);
            }

        }
        // catch (Exception error)
        {
            //  tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }

        //-----------------------------------------------------------------------------------------------
       // Response.Redirect("lease_add_3_tmp.aspx?tu_id=" + new_tenant_unit_id + "&tenant_id=" + new_tenant_id);
        conn.Close();


        if (rTenantTempoList.Items.Count > 0)
        {
            MultiView1.ActiveViewIndex = 3;

            panel_rent_no.Visible = false;
            panel_rent.Visible = false;

            tiger.Tenant g = new tiger.Tenant(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            rTenantTempoList_1.DataSource = g.getTenantTempoList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_tenant_id.Value));
            rTenantTempoList_1.DataBind();
        }       

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_previous_2_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_previous_3_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;
    }




    protected DataSet GetTrasnl(int tu_id, int rl_id)
    {
        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prRentUntreatedList", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        DateTime the_date = new DateTime();
        DateTime right_now = new DateTime();
        right_now = DateTime.Now;

        //   the_date= Convert.ToDateTime( ddl_date_received_m.SelectedValue  +"/"+ ddl_date_received_d.SelectedValue  +"/"+ ddl_date_received_y.SelectedValue) ;
        //  Label7.Text = Convert.ToString( the_date);
        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;
            cmd.Parameters.Add("@rl_id", SqlDbType.Int).Value = rl_id;
            cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = right_now;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="thedate"></param>
    /// <returns></returns>
    protected string Get_UntreatedRent(DateTime thedate)
    {
        return thedate.Month.ToString() + "/" + thedate.Day.ToString() + "/" + thedate.Year.ToString(); ;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="thedate"></param>
    /// <returns></returns>
    protected string Get_UntreatedRent_Month(DateTime thedate)
    {
        return thedate.Month.ToString();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="thedate"></param>
    /// <returns></returns>
    protected string Get_UntreatedRent_Day(DateTime thedate)
    {
        return thedate.Day.ToString();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="thedate"></param>
    /// <returns></returns>
    protected string Get_UntreatedRent_Year(DateTime thedate)
    {
        return thedate.Year.ToString();
    }

    /// <summary>
    /// This method erase empty gridview row within "gv_parent_nested_unpaid_rent_month"
    /// i.e. : "gv_child_nested_unpaid_rent_month" is empty
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_parent_nested_unpaid_rent_month_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int count = 0;

        for (int i = 0; i < gv_parent_nested_unpaid_rent_month.Rows.Count; i++)
        {
            // create an instance of a GridRow and get a row of the parent GridView
            GridViewRow row = gv_parent_nested_unpaid_rent_month.Rows[i];

            //From that row we get the child GridView and count the number of row in the nested GridView

            GridView grv = row.Cells[1].FindControl("gv_child_nested_unpaid_rent_month") as GridView;
            if (grv.Rows.Count == 0)
            {
                row.Visible = false;
            }
            else
                count++;
        }

        if (count == 0)
            gv_parent_nested_unpaid_rent_month.Visible = false;
        else
            gv_parent_nested_unpaid_rent_month.Visible = true;


        Label1.Text = gv_parent_nested_unpaid_rent_month.Rows.Count.ToString();
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        
        
        String compare_rl_rent_amount = "";
        String rp_amount = "";
        String rp_paid_date = "";
        String rp_due_date = "";
        String tu_id = "";
        String rl_id = "";
        String rp_notes = "";
        int number_of_insert = 0;
        String no_delinquent = "";


        // For each row of the parent GridView get the child nested GridView
        // here we are counting the number of row of the parent GridView
        for (int i = 0; i < gv_parent_nested_unpaid_rent_month.Rows.Count; i++)
        {
            // create an instance of a GridRow and get a row of the parent GridView
            GridViewRow row = gv_parent_nested_unpaid_rent_month.Rows[i];

            //From that row we get the child GridView and count the number of row in the nested GridView

            GridView grv = row.Cells[1].FindControl("gv_child_nested_unpaid_rent_month") as GridView;
            for (int j = 0; j < grv.Rows.Count; j++)
            {



                CheckBox chk_process = (CheckBox)grv.Rows[j].Cells[0].FindControl("chk_process");
                // if the checkbox is checked the process
                if (chk_process.Checked)
                {
                    TextBox tbx_rl_rent_amount = (TextBox)grv.Rows[j].Cells[2].FindControl("gvparent_child_textbox");
                    // Label rent_due_date = (Label)grv.Rows[j].Cells[2].FindControl("rent_due_date");
                    HiddenField h_rent_due_date = (HiddenField)grv.Rows[j].Cells[6].FindControl("h_rent_due_date");

                    DropDownList ddl_child_unpaid_rent_m = (DropDownList)grv.Rows[j].Cells[6].FindControl("ddl_child_unpaid_rent_m");
                    DropDownList ddl_child_unpaid_rent_d = (DropDownList)grv.Rows[j].Cells[6].FindControl("ddl_child_unpaid_rent_d");
                    DropDownList ddl_child_unpaid_rent_y = (DropDownList)grv.Rows[j].Cells[6].FindControl("ddl_child_unpaid_rent_y");

                    CheckBox chk_no_delequency = (CheckBox)grv.Rows[j].Cells[4].FindControl("chk_no_delequency");

                    TextBox tbx_notes = (TextBox)grv.Rows[j].Cells[6].FindControl("tbx_notes");

                    HiddenField h_tu_id = (HiddenField)grv.Rows[j].Cells[6].FindControl("h_tu_id");
                    HiddenField h_rl_rent_amount = (HiddenField)grv.Rows[j].Cells[6].FindControl("h_rl_rent_amount");
                    HiddenField h_rl_id = (HiddenField)grv.Rows[j].Cells[6].FindControl("h_rl_id");



                    // if h_tu_id or hd_rl_id are note integer logout
                    if (!RegEx.IsInteger(h_tu_id.Value) ||
                        !RegEx.IsInteger(h_rl_id.Value) ||
                        !RegEx.IsMoney(h_rl_rent_amount.Value))
                    {
                        Session.Abandon();
                        Response.Redirect("~/login.aspx");
                    }

                    if (RegEx.IsMoney(tbx_rl_rent_amount.Text))
                    {
                        rp_amount = rp_amount + tbx_rl_rent_amount.Text + "|";
                        compare_rl_rent_amount = compare_rl_rent_amount + h_rl_rent_amount.Value + "|";
                        rp_paid_date = rp_paid_date + ddl_child_unpaid_rent_m.SelectedValue + "/" + ddl_child_unpaid_rent_d.SelectedValue + "/" + ddl_child_unpaid_rent_y.SelectedValue + "|";
                        rp_due_date = rp_due_date + h_rent_due_date.Value + "|";
                        tu_id = tu_id + h_tu_id.Value + "|";
                        rl_id = rl_id + h_rl_id.Value + "|";
                        rp_notes = rp_notes + tbx_notes.Text + "|";


                        if (chk_no_delequency.Checked)
                            no_delinquent = no_delinquent + "1|";
                        else
                            no_delinquent = no_delinquent + "0|";

                        number_of_insert++;
                   }

                }


            }
        }


        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        AccountObjectAuthorization rentAuthorization = new AccountObjectAuthorization(strconn);

        if (!rentAuthorization.RentPaymentBatch(Convert.ToInt32(Session["schema_id"]), tu_id, rl_id, number_of_insert))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        //////////
        ////////// SECURITY CHECK END //////////////
        /////////


        if (rl_id != "" && number_of_insert > 0) //
        {
            //Here we send the values to the stored procedure

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prRentPaymentBatchAdd", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            // try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);

                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();

                // we replace the commas  by dots because SQL "CAST" will convert $100,00 by 100000.00
                // we cast the string "100,00" in money
                cmd.Parameters.Add("@rp_amount", SqlDbType.VarChar, 80000).Value = rp_amount.Replace(",", ".");
                cmd.Parameters.Add("@compare_rl_rent_amount", SqlDbType.VarChar, 80000).Value = compare_rl_rent_amount.Replace(",", ".");
                cmd.Parameters.Add("@rp_paid_date", SqlDbType.VarChar, 80000).Value = rp_paid_date;
                cmd.Parameters.Add("@rp_due_date", SqlDbType.VarChar, 80000).Value = rp_due_date;
                cmd.Parameters.Add("@tu_id", SqlDbType.VarChar, 80000).Value = tu_id;
                cmd.Parameters.Add("@rl_id", SqlDbType.VarChar, 80000).Value = rl_id;
                cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;
                cmd.Parameters.Add("@rp_no_delenquent", SqlDbType.VarChar, 80000).Value = no_delinquent;
                cmd.Parameters.Add("@rp_notes", SqlDbType.NVarChar, 4000).Value = rp_notes;

                //execute the insert
                cmd.ExecuteReader();

            }
            //  finally
            {
                conn.Close();
            }


        }

        // dropdownlist rent frequency
        tiger.Home v = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ddl_rent_frequency.DataSource = v.getHomeUnitRentedSumListArchives(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
        ddl_rent_frequency.DataBind();


        int rent_frequency = 0;
        if(ddl_rent_frequency.Items.Count >0 )
          rent_frequency = Convert.ToInt32(ddl_rent_frequency.SelectedItem.Value);

        // here we get the list of untreated rented unit ( the chlid gridview )
        tiger.Home uv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_parent_nested_unpaid_rent_month.DataSource = uv.getHomeUnitRentedListArchives(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue), rent_frequency);
        gv_parent_nested_unpaid_rent_month.DataBind();

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_rent_frequency_SelectedIndexChanged(object sender, EventArgs e)
    {
        DateTime right_now = new DateTime();
        right_now = DateTime.Now;

        int home_id = Convert.ToInt32(ddl_home_id.SelectedValue);
        int rent_frequency = Convert.ToInt32(ddl_rent_frequency.SelectedValue);

        // here we get the list of untreated rented unit ( the chlid gridview )
        tiger.Home uv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_parent_nested_unpaid_rent_month.DataSource = uv.getHomeUnitRentedListArchives(Convert.ToInt32(Session["schema_id"]), home_id, rent_frequency);
        gv_parent_nested_unpaid_rent_month.DataBind();

    }
}
