using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    :
/// </summary>
public partial class home_lease_rent_update : BasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            reg_new_rl_rent_amount.ValidationExpression = RegEx.getMoney();
            reg_new_rl_rent_amount_2.ValidationExpression = RegEx.getMoney();

            txt_message.InnerHtml = "";
            //int home_id = 1;// Convert.ToInt32(Request.QueryString["home_id"]);

            r_pendingrentlist.Visible = true;
            txt_pending.InnerHtml = "";
            btn_continue.Enabled = true;
            panel_current_tenant.Visible = true;


            //--------- Commercial
            txt_message_2.InnerHtml = "";
            //int home_id = 1;// Convert.ToInt32(Request.QueryString["home_id"]);

            r_pendingrentlist_2.Visible = true;
            txt_pending_2.InnerHtml = "";
            btn_continue_2.Enabled = true;
            panel_current_tenant_2.Visible = true;
            //---------


            tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = h.getHomeCount(Convert.ToInt32(Session["schema_id"]));
            string link_to_unit = "";
            if (home_count > 0)
            {
                int home_id = h.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));
                if (!string.IsNullOrEmpty(Request.QueryString["h_id"]))
                {
                    home_id = Convert.ToInt32(Request.QueryString["h_id"]);
                }

                link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                //Session["schema_id"]));


                ddl_home_id.Visible = true;
                //hidden fields
                hd_home_id.Value = Convert.ToString(home_id);



                ddl_home_id.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                ddl_home_id.SelectedValue = Convert.ToString(home_id);
                ddl_home_id.DataBind();

                //*********************************************
                // DropDownList pour les Unit
                //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

                if (unit_count > 0)
                {

                    int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), home_id);
                    if (!string.IsNullOrEmpty(Request.QueryString["unit_id"]))
                    {
                        unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                    }

                    //link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                    //Session["schema_id"]));                    
                    // unit_id hiddenfield
                    hd_unit_id.Value = Convert.ToString(unit_id);

                    ddl_unit_id.DataSource = u.getUnitResList(Convert.ToInt32(Session["schema_id"]), home_id);
                    ddl_unit_id.SelectedValue = Convert.ToString(unit_id);
                    ddl_unit_id.DataBind();

                    //get current tenant id
                    int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), unit_id);
                    //if there is a current tenant in the unit then get name(s)
                    if (temp_tenant_id > 0)
                    {
                        panel_current_tenant.Visible = true;
                        txt_current_tenant_name.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);

                        tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                        hd_current_tu_id.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), unit_id));

                        panel_rent_update.Visible = true;


                        // here we check the amount of pending rent for this tenant unit
                        int count = 0;

                        tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                        count = p.getCountPendingRent(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id.Value));


                        if (count > 0)
                        {
                            btn_continue.Enabled = false;
                            txt_pending.InnerHtml = "<strong><span style='color: #ff3300'>You already have a pending rent ,<br /> please remove pending before creating another rent, ( link goes here)</span></strong> ";

                        }

                        //-----  pending rent in this tenant unit
                        tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                        r_pendingrentlist.DataSource = v.getPendingRentList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id.Value));
                        r_pendingrentlist.DataBind();

                        ///------------------- 

                        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                        SqlCommand cmd = new SqlCommand("prRentView", conn);
                        SqlCommand cmd2 = new SqlCommand("prLastRentPaidDate", conn);

                        cmd.CommandType = CommandType.StoredProcedure;

                        DateTime the_date = new DateTime();


                        the_date = DateTime.Now; // the date in the to drop downlist
                        DateTime la_date = new DateTime(); // Date begin of Rent
                        DateTime last_rp_date = new DateTime(); // Last paid rent date ( or current )

                        tiger.Date d = new tiger.Date();
                        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
                        // and convert it in Datetime
                        the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


                        //Add the params
                        cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                        cmd.Parameters.Add("tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id.Value);
                        cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;




                        try
                        {
                            conn.Open();

                            SqlDataReader dr = null;
                            dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                            while (dr.Read() == true)
                            {
                                lbl_rent_amount.Text = Convert.ToString(Convert.ToInt32(dr["rl_rent_amount"]));
                                ddl_rl_rent_paid_every.SelectedValue = dr["re_id"].ToString();


                                //  lbl_current_rl_date_begin.Text = String.Format("{0:d}", dr["rl_date_begin"]);



                                la_date = Convert.ToDateTime(dr["rl_date_begin"]);

                                lbl_current_rl_date_begin.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();

                                lbl_current_rl_date_begin2.Text = lbl_current_rl_date_begin.Text;


                                DateTime new_date_begin = new DateTime();
                                new_date_begin = Convert.ToDateTime(dr["rl_date_begin"]);

                                hd_min_rent_begin_date_m.Value = new_date_begin.Month.ToString();
                                hd_min_rent_begin_date_d.Value = new_date_begin.Day.ToString();
                                hd_min_rent_begin_date_y.Value = new_date_begin.Year.ToString();

                                ddl_rl_date_begin_m.SelectedValue = new_date_begin.AddDays(1).Month.ToString();
                                ddl_rl_date_begin_d.SelectedValue = new_date_begin.AddDays(1).Day.ToString();
                                ddl_rl_date_begin_y.SelectedValue = new_date_begin.AddDays(1).Year.ToString();



                            }
                        }

                        finally
                        {

                            //  conn.Close();
                        }


                        ////// last paid rent date

                        cmd2.CommandType = CommandType.StoredProcedure;


                        try
                        {
                            // conn.Open();
                            //Add the params
                            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                            cmd2.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id.Value);
                            cmd2.Parameters.Add("@last_rp_date", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;

                            //execute the insert
                            cmd2.ExecuteReader();

                            if (cmd2.Parameters["@last_rp_date"].Value != System.DBNull.Value)
                                last_rp_date = Convert.ToDateTime(cmd2.Parameters["@last_rp_date"].Value);
                            else
                                last_rp_date = Convert.ToDateTime("1/1/1900");

                        }
                        finally
                        {
                            conn.Close();
                        }



                        if (la_date < last_rp_date)
                        {

                            lbl_current_rl_date_begin2.Text = last_rp_date.Month.ToString() + "-" + last_rp_date.Day.ToString() + "-" + last_rp_date.Year.ToString();

                            hd_min_rent_begin_date_m.Value = last_rp_date.Month.ToString();
                            hd_min_rent_begin_date_d.Value = last_rp_date.Day.ToString();
                            hd_min_rent_begin_date_y.Value = last_rp_date.Year.ToString();

                            ddl_rl_date_begin_m.SelectedValue = last_rp_date.AddDays(1).Month.ToString();
                            ddl_rl_date_begin_d.SelectedValue = last_rp_date.AddDays(1).Day.ToString();
                            ddl_rl_date_begin_y.SelectedValue = last_rp_date.AddDays(1).Year.ToString();
                        }



                    }

                    else
                    {
                        txt_message.InnerHtml = "This  Unit is not rented";
                        r_pendingrentlist.Visible = false;
                        panel_current_tenant.Visible = false;
                        panel_rent_update.Visible = false;
                        hd_current_tu_id.Value = "0";
                    }

                }

                // if ther is no unit
                else
                {

                    txt_message.InnerHtml = "There is no unit in this property -- add unit";
                    panel_rent_update.Visible = false;
                    r_pendingrentlist.Visible = false;
                    hd_current_tu_id.Value = "0";
                    hd_unit_id.Value = "0";
                }

                //hidden fields
                hd_home_id.Value = Convert.ToString(home_id);

                /************************Now get home information*****************************/

                //txt_link.InnerHtml = h.getHomeViewInfo(Session["schema_id"], home_id, Convert.ToChar(Session["user_lang"]));
                // txt_link.InnerHtml = link_to_unit;
            }
            // if ther is no home
            else
            {
                txt_message.InnerHtml = "There is no property -- Add a property";
                panel_rent_update.Visible = false;
                hd_home_id.Value = "0";
                hd_current_tu_id.Value = "0";
                hd_unit_id.Value = "0";
                ddl_home_id.Visible = false;
                txt_link.InnerHtml = home_count + " <a href='home_add.aspx'>Add Home</a>&nbsp;&nbsp;<a href='owner_add.aspx'>Add Owner</a>&nbsp;&nbsp;<a href='janitor_add.aspx'>Add Janitor</a>&nbsp;&nbsp;";
            }


            int homecom_count = h.getHomeCount(Convert.ToInt32(Session["schema_id"]));
            if (homecom_count > 0)
            {
                int home_id = h.getHomeComFirstId(Convert.ToInt32(Session["schema_id"]));
                link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                //Session["schema_id"]));


                ddl_home_id_2.Visible = true;
                //hidden fields
                hd_home_id_2.Value = Convert.ToString(home_id);

                ddl_home_id_2.DataSource = h.getHomeComList(Convert.ToInt32(Session["schema_id"]));
                if (home_id > 0)
                    ddl_home_id_2.SelectedValue = Convert.ToString(home_id);
                ddl_home_id_2.DataBind();

                //*********************************************
                // DropDownList pour les Unit
                //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitComCount(Convert.ToInt32(Session["schema_id"]), home_id);

                if (unit_count > 0)
                {

                    int unit_id = u.getUnitComFirstId(Convert.ToInt32(Session["schema_id"]), home_id);
                    //link_to_unit = "<a href='unit_add.aspx?home_id=" + home_id + "'>Add a unit</a>";
                    //Session["schema_id"]));                    
                    // unit_id hiddenfield
                    hd_unit_id_2.Value = Convert.ToString(unit_id);

                    ddl_unit_id_2.DataSource = u.getUnitComList(Convert.ToInt32(Session["schema_id"]), home_id);
                    ddl_unit_id_2.SelectedValue = Convert.ToString(unit_id);
                    ddl_unit_id_2.DataBind();

                    //get current tenant id
                    int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), unit_id);
                    //if there is a current tenant in the unit then get name(s)
                    if (temp_tenant_id > 0)
                    {
                        panel_current_tenant_2.Visible = true;
                        txt_current_tenant_name_2.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);

                        tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                        //  hd_current_tu_id_2.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), unit_id));

                        panel_rent_update_2.Visible = true;


                        // here we check the amount of pending rent for this tenant unit
                        int count = 0;

                        tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                        count = p.getCountPendingRent(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id_2.Value));


                        if (count > 0)
                        {
                            btn_continue_2.Enabled = false;
                            txt_pending_2.InnerHtml = "<strong><span style='color: #ff3300'>You already have a pending rent ,<br /> please remove pending before creating another rent, ( link goes here)</span></strong> ";

                        }

                        //-----  pending rent in this tenant unit
                        tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                        r_pendingrentlist_2.DataSource = v.getPendingRentList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id_2.Value));
                        r_pendingrentlist_2.DataBind();

                        ///------------------- 

                        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                        SqlCommand cmd = new SqlCommand("prRentView", conn);
                        SqlCommand cmd2 = new SqlCommand("prLastRentPaidDate", conn);

                        cmd.CommandType = CommandType.StoredProcedure;

                        DateTime the_date = new DateTime();


                        the_date = DateTime.Now; // the date in the to drop downlist
                        DateTime la_date = new DateTime(); // Date begin of Rent
                        DateTime last_rp_date = new DateTime(); // Last paid rent date ( or current )

                        tiger.Date d = new tiger.Date();
                        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
                        // and convert it in Datetime
                        the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


                        //Add the params
                        cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                        cmd.Parameters.Add("tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id_2.Value);
                        cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;




                        try
                        {
                            conn.Open();

                            SqlDataReader dr = null;
                            dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                            while (dr.Read() == true)
                            {
                                lbl_rent_amount_2.Text = Convert.ToString(Convert.ToInt32(dr["rl_rent_amount"]));
                                ddl_rl_rent_paid_every_2.SelectedValue = dr["re_id"].ToString();


                                //  lbl_current_rl_date_begin.Text = String.Format("{0:d}", dr["rl_date_begin"]);



                                la_date = Convert.ToDateTime(dr["rl_date_begin"]);

                                lbl_current_rl_date_begin_2.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();

                                lbl_current_rl_date_begin_3.Text = lbl_current_rl_date_begin_2.Text;


                                DateTime new_date_begin = new DateTime();
                                new_date_begin = Convert.ToDateTime(dr["rl_date_begin"]);

                                hd_min_rent_begin_date_m2.Value = new_date_begin.Month.ToString();
                                hd_min_rent_begin_date_d2.Value = new_date_begin.Day.ToString();
                                hd_min_rent_begin_date_y2.Value = new_date_begin.Year.ToString();

                                ddl_rl_date_begin_m2.SelectedValue = new_date_begin.AddDays(1).Month.ToString();
                                ddl_rl_date_begin_d2.SelectedValue = new_date_begin.AddDays(1).Day.ToString();
                                ddl_rl_date_begin_y2.SelectedValue = new_date_begin.AddDays(1).Year.ToString();

                                if (dr["unit_type"].ToString() == "C" && Convert.ToInt32(dr["company_id"]) > 0)
                                {
                                    lbl_company.Text = dr["company_name"].ToString();
                                }
                                else
                                {
                                    tr_company.Visible = false;
                                }

                            }
                        }

                        finally
                        {

                            //  conn.Close();
                        }


                        ////// last paid rent date

                        cmd2.CommandType = CommandType.StoredProcedure;


                        try
                        {
                            // conn.Open();
                            //Add the params
                            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                            cmd2.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id_2.Value);
                            cmd2.Parameters.Add("@last_rp_date", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;

                            //execute the insert
                            cmd2.ExecuteReader();

                            if (cmd2.Parameters["@last_rp_date"].Value != System.DBNull.Value)
                                last_rp_date = Convert.ToDateTime(cmd2.Parameters["@last_rp_date"].Value);
                            else
                                last_rp_date = Convert.ToDateTime("1/1/1900");

                        }
                        finally
                        {
                            conn.Close();
                        }



                        if (la_date < last_rp_date)
                        {

                            lbl_current_rl_date_begin_3.Text = last_rp_date.Month.ToString() + "-" + last_rp_date.Day.ToString() + "-" + last_rp_date.Year.ToString();

                            hd_min_rent_begin_date_m2.Value = last_rp_date.Month.ToString();
                            hd_min_rent_begin_date_d2.Value = last_rp_date.Day.ToString();
                            hd_min_rent_begin_date_y2.Value = last_rp_date.Year.ToString();

                            ddl_rl_date_begin_m2.SelectedValue = last_rp_date.AddDays(1).Month.ToString();
                            ddl_rl_date_begin_d2.SelectedValue = last_rp_date.AddDays(1).Day.ToString();
                            ddl_rl_date_begin_y2.SelectedValue = last_rp_date.AddDays(1).Year.ToString();
                        }



                    }

                    else
                    {
                        txt_message_2.InnerHtml = "This  Unit is not rented";
                        r_pendingrentlist_2.Visible = false;
                        panel_current_tenant_2.Visible = false;
                        panel_rent_update_2.Visible = false;
                        hd_current_tu_id_2.Value = "0";
                    }

                }

                // if ther is no unit
                else
                {

                    txt_message_2.InnerHtml = "There is no unit in this property -- add unit";
                    panel_rent_update_2.Visible = false;
                    r_pendingrentlist_2.Visible = false;
                    hd_current_tu_id_2.Value = "0";
                    hd_unit_id_2.Value = "0";
                }

                //hidden fields
                hd_home_id_2.Value = Convert.ToString(home_id);
            }
            else
            {
                txt_message_2.InnerHtml = "There is no property -- Add a property";
                panel_rent_update_2.Visible = false;
                hd_home_id_2.Value = "0";
                hd_current_tu_id_2.Value = "0";
                hd_unit_id_2.Value = "0";
                ddl_home_id_2.Visible = false;
                btn_continue_2.Visible = false;
                txt_link_2.InnerHtml = homecom_count + " <a href='home_add.aspx'>Add Home</a>&nbsp;&nbsp;<a href='owner_add.aspx'>Add Owner</a>&nbsp;&nbsp;<a href='janitor_add.aspx'>Add Janitor</a>&nbsp;&nbsp;";
            }


        }
    }

    //----------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        //  if (!Page.IsPostBack)
        //  { 

        txt_message.InnerHtml = "";
        ddl_home_id.Visible = true;
        r_pendingrentlist.Visible = true;
        txt_pending.InnerHtml = "";
        btn_continue.Enabled = true;
        panel_current_tenant.Visible = true;

        hd_home_id.Value = Convert.ToString(ddl_home_id.SelectedValue);
        tiger.Unit u = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
        //change list of unit id

        if (unit_id > 0)
        {
            ddl_unit_id.Visible = true;
            //first dispose of old ddl_unit
            ddl_unit_id.Dispose();
            ddl_unit_id.DataSource = u.getUnitResList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
            ddl_unit_id.DataBind();

            hd_unit_id.Value = Convert.ToString(unit_id);

            //----- Now we check if there is any pending rent )
            // tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            // dgpendinglist.DataSource = v.getPendingLeasesList(Convert.ToInt32(Session["schema_id"]), unit_id);
            // dgpendinglist.DataBind();

            ///------------------- 
            //get current tenant id
            int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), unit_id);
            //if there is a current tenant in the unit then get name(s)
            if (temp_tenant_id > 0)
            {
                panel_current_tenant.Visible = true;
                txt_current_tenant_name.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);

                tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                hd_current_tu_id.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), unit_id));

                panel_rent_update.Visible = true;


                //---Here we count the number of pending rent in this tenant unit
                int count = 0;

                tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                count = p.getCountPendingRent(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id.Value));


                if (count > 0)
                {
                    btn_continue.Enabled = false;
                    txt_pending.InnerHtml = "<strong><span style='color: #ff3300'>You already have a pending rent ,<br /> please remove pending before creating another rent, ( link goes here)</span></strong> ";

                }

                //----- Now we show there is any pending rent in this tenant unit
                tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                r_pendingrentlist.DataSource = v.getPendingRentList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id.Value));

                r_pendingrentlist.DataBind();

                ///------------------- 
                ///
                SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                SqlCommand cmd = new SqlCommand("prRentView", conn);
                SqlCommand cmd2 = new SqlCommand("prLastRentPaidDate", conn);

                cmd.CommandType = CommandType.StoredProcedure;


                DateTime the_date = new DateTime();
                DateTime la_date = new DateTime();//date begin of rent
                DateTime last_rp_date = new DateTime(); // Last paid rent date ( or current )
                the_date = DateTime.Now; // the date in the to drop downlist

                tiger.Date d = new tiger.Date();
                // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
                // and convert it in Datetime
                the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


                //Add the params
                cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id.Value);
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;



                try
                {
                    conn.Open();

                    SqlDataReader dr = null;
                    dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                    while (dr.Read() == true)
                    {
                        lbl_rent_amount.Text = Convert.ToString(Convert.ToInt32(dr["rl_rent_amount"]));
                        ddl_rl_rent_paid_every.SelectedValue = dr["re_id"].ToString();


                        //  lbl_current_rl_date_begin.Text = String.Format("{0:d}", dr["rl_date_begin"]);


                        la_date = Convert.ToDateTime(dr["rl_date_begin"]);

                        lbl_current_rl_date_begin.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();

                        lbl_current_rl_date_begin2.Text = lbl_current_rl_date_begin.Text;


                        DateTime new_date_begin = new DateTime();
                        new_date_begin = Convert.ToDateTime(dr["rl_date_begin"]);

                        hd_min_rent_begin_date_m.Value = new_date_begin.Month.ToString();
                        hd_min_rent_begin_date_d.Value = new_date_begin.Day.ToString();
                        hd_min_rent_begin_date_y.Value = new_date_begin.Year.ToString();


                        ddl_rl_date_begin_m.SelectedValue = new_date_begin.AddDays(1).Month.ToString();
                        ddl_rl_date_begin_d.SelectedValue = new_date_begin.AddDays(1).Day.ToString();
                        ddl_rl_date_begin_y.SelectedValue = new_date_begin.AddDays(1).Year.ToString();
                    }
                }


                finally
                {

                    // conn.Close();
                }


                ////// last paid rent date

                cmd2.CommandType = CommandType.StoredProcedure;
                try
                {
                    // conn.Open();
                    //Add the params
                    cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                    cmd2.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id.Value);
                    cmd2.Parameters.Add("@last_rp_date", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;

                    //execute the insert
                    cmd2.ExecuteReader();

                    if (cmd2.Parameters["@last_rp_date"].Value != System.DBNull.Value)
                        last_rp_date = Convert.ToDateTime(cmd2.Parameters["@last_rp_date"].Value);
                    else
                        last_rp_date = Convert.ToDateTime("1/1/1900");

                }
                finally
                {
                    conn.Close();
                }

                if (la_date < last_rp_date)
                {

                    lbl_current_rl_date_begin2.Text = last_rp_date.Month.ToString() + "-" + last_rp_date.Day.ToString() + "-" + last_rp_date.Year.ToString();

                    hd_min_rent_begin_date_m.Value = last_rp_date.Month.ToString();
                    hd_min_rent_begin_date_d.Value = last_rp_date.Day.ToString();
                    hd_min_rent_begin_date_y.Value = last_rp_date.Year.ToString();

                    ddl_rl_date_begin_m.SelectedValue = last_rp_date.AddDays(1).Month.ToString();
                    ddl_rl_date_begin_d.SelectedValue = last_rp_date.AddDays(1).Day.ToString();
                    ddl_rl_date_begin_y.SelectedValue = last_rp_date.AddDays(1).Year.ToString();
                }

            }
            else
            {
                txt_message.InnerHtml = "This  Unit is not rented ";
                r_pendingrentlist.Visible = false;
                panel_current_tenant.Visible = false;
                panel_rent_update.Visible = false;

                hd_current_tu_id.Value = "0";
            }
            //hidden fields
        }
        else
        {
            txt_message.InnerHtml = "There is no unit in this house -- add unit(s)";
            panel_rent_update.Visible = false;
            r_pendingrentlist.Visible = false;
            ddl_unit_id.Visible = false;
            //  txt_link.InnerHtml = "<a href='unit_add.aspx?home_id='>Add Unit</a>";
            hd_current_tu_id.Value = "0";
            hd_unit_id.Value = "0";

        }
        //   }// fin if not postback
    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_id_2_SelectedIndexChanged(object sender, EventArgs e)
    {
        //  if (!Page.IsPostBack)
        //  { 

        txt_message_2.InnerHtml = "";
        ddl_home_id_2.Visible = true;
        r_pendingrentlist_2.Visible = true;
        txt_pending_2.InnerHtml = "";
        btn_continue_2.Enabled = true;
        panel_current_tenant_2.Visible = true;

        hd_home_id_2.Value = Convert.ToString(ddl_home_id_2.SelectedValue);
        tiger.Unit u = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        int unit_id = u.getUnitComFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id_2.SelectedValue));
        //change list of unit id

        if (unit_id > 0)
        {
            ddl_unit_id_2.Visible = true;
            //first dispose of old ddl_unit
            ddl_unit_id_2.Dispose();
            ddl_unit_id_2.DataSource = u.getUnitComList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id_2.SelectedValue));
            ddl_unit_id.DataBind();

            hd_unit_id_2.Value = Convert.ToString(unit_id);

            //----- Now we check if there is any pending rent )
            // tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            // dgpendinglist.DataSource = v.getPendingLeasesList(Convert.ToInt32(Session["schema_id"]), unit_id);
            // dgpendinglist.DataBind();

            ///------------------- 
            //get current tenant id
            int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), unit_id);
            //if there is a current tenant in the unit then get name(s)
            if (temp_tenant_id > 0)
            {
                panel_current_tenant_2.Visible = true;
                txt_current_tenant_name_2.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);

                tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                hd_current_tu_id_2.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), unit_id));

                panel_rent_update_2.Visible = true;


                //---Here we count the number of pending rent in this tenant unit
                int count = 0;

                tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                count = p.getCountPendingRent(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id_2.Value));


                if (count > 0)
                {
                    btn_continue_2.Enabled = false;
                    txt_pending_2.InnerHtml = "<strong><span style='color: #ff3300'>You already have a pending rent ,<br /> please remove pending before creating another rent, ( link goes here)</span></strong> ";

                }

                //----- Now we show there is any pending rent in this tenant unit
                tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                r_pendingrentlist_2.DataSource = v.getPendingRentList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id_2.Value));

                r_pendingrentlist_2.DataBind();

                ///------------------- 
                ///
                SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                SqlCommand cmd = new SqlCommand("prRentView", conn);
                SqlCommand cmd2 = new SqlCommand("prLastRentPaidDate", conn);

                cmd.CommandType = CommandType.StoredProcedure;


                DateTime the_date = new DateTime();
                DateTime la_date = new DateTime();//date begin of rent
                DateTime last_rp_date = new DateTime(); // Last paid rent date ( or current )
                the_date = DateTime.Now; // the date in the to drop downlist

                tiger.Date d = new tiger.Date();
                // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
                // and convert it in Datetime
                the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


                //Add the params
                cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id_2.Value);
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;



                try
                {
                    conn.Open();

                    SqlDataReader dr = null;
                    dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                    while (dr.Read() == true)
                    {
                        lbl_rent_amount_2.Text = Convert.ToString(Convert.ToInt32(dr["rl_rent_amount"]));
                        ddl_rl_rent_paid_every_2.SelectedValue = dr["re_id"].ToString();


                        //  lbl_current_rl_date_begin.Text = String.Format("{0:d}", dr["rl_date_begin"]);


                        la_date = Convert.ToDateTime(dr["rl_date_begin"]);

                        lbl_current_rl_date_begin_2.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();

                        lbl_current_rl_date_begin_3.Text = lbl_current_rl_date_begin_2.Text;


                        DateTime new_date_begin = new DateTime();
                        new_date_begin = Convert.ToDateTime(dr["rl_date_begin"]);

                        hd_min_rent_begin_date_m2.Value = new_date_begin.Month.ToString();
                        hd_min_rent_begin_date_d2.Value = new_date_begin.Day.ToString();
                        hd_min_rent_begin_date_y2.Value = new_date_begin.Year.ToString();


                        ddl_rl_date_begin_m2.SelectedValue = new_date_begin.AddDays(1).Month.ToString();
                        ddl_rl_date_begin_d2.SelectedValue = new_date_begin.AddDays(1).Day.ToString();
                        ddl_rl_date_begin_y2.SelectedValue = new_date_begin.AddDays(1).Year.ToString();

                        if (dr["unit_type"].ToString() == "C" && Convert.ToInt32(dr["company_id"]) > 0)
                        {
                            lbl_company.Text = dr["company_name"].ToString();
                        }
                        else
                        {
                            tr_company.Visible = false;
                        }
                    }
                }


                finally
                {

                    // conn.Close();
                }


                ////// last paid rent date

                cmd2.CommandType = CommandType.StoredProcedure;
                try
                {
                    // conn.Open();
                    //Add the params
                    cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                    cmd2.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id_2.Value);
                    cmd2.Parameters.Add("@last_rp_date", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;

                    //execute the insert
                    cmd2.ExecuteReader();

                    if (cmd2.Parameters["@last_rp_date"].Value != System.DBNull.Value)
                        last_rp_date = Convert.ToDateTime(cmd2.Parameters["@last_rp_date"].Value);
                    else
                        last_rp_date = Convert.ToDateTime("1/1/1900");

                }
                finally
                {
                    conn.Close();
                }

                if (la_date < last_rp_date)
                {

                    lbl_current_rl_date_begin_3.Text = last_rp_date.Month.ToString() + "-" + last_rp_date.Day.ToString() + "-" + last_rp_date.Year.ToString();

                    hd_min_rent_begin_date_m2.Value = last_rp_date.Month.ToString();
                    hd_min_rent_begin_date_d2.Value = last_rp_date.Day.ToString();
                    hd_min_rent_begin_date_y2.Value = last_rp_date.Year.ToString();

                    ddl_rl_date_begin_m2.SelectedValue = last_rp_date.AddDays(1).Month.ToString();
                    ddl_rl_date_begin_d2.SelectedValue = last_rp_date.AddDays(1).Day.ToString();
                    ddl_rl_date_begin_y2.SelectedValue = last_rp_date.AddDays(1).Year.ToString();
                }

            }
            else
            {
                txt_message_2.InnerHtml = "This  Unit is not rented ";
                r_pendingrentlist_2.Visible = false;
                panel_current_tenant_2.Visible = false;
                panel_rent_update_2.Visible = false;

                hd_current_tu_id_2.Value = "0";
            }
            //hidden fields
        }
        else
        {
            txt_message_2.InnerHtml = "There is no unit in this house -- add unit(s)";
            panel_rent_update_2.Visible = false;
            r_pendingrentlist_2.Visible = false;
            ddl_unit_id_2.Visible = false;
            //  txt_link.InnerHtml = "<a href='unit_add.aspx?home_id='>Add Unit</a>";
            hd_current_tu_id_2.Value = "0";
            hd_unit_id_2.Value = "0";

        }
        //   }// fin if not postback

        // TabContainer1.ActiveTabIndex = 1;
    }
    //----------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_unit_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        txt_message.InnerHtml = "";

        r_pendingrentlist.Visible = true;
        txt_pending.InnerHtml = "";
        btn_continue.Enabled = true;
        panel_current_tenant.Visible = true;

        hd_unit_id.Value = Convert.ToString(ddl_unit_id.SelectedValue);
        // label_added_unit.Text = ddl_unit_id.SelectedItem.Text;
        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_unit_id.SelectedValue));
        //if there is a current tenant in the unit then get name(s)
        if (temp_tenant_id > 0)
        {
            panel_current_tenant.Visible = true;
            txt_current_tenant_name.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);


            //tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            hd_current_tu_id.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), Convert.ToInt32(ddl_unit_id.SelectedValue)));


            panel_rent_update.Visible = true;

            //---Here we count the number of pending rent in this tenant unit
            int count = 0;

            tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            count = p.getCountPendingRent(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id.Value));


            if (count > 0)
            {
                btn_continue.Enabled = false;
                txt_pending.InnerHtml = "<strong><span style='color: #ff3300'>You already have a pending rent ,<br /> please remove pending before creating another rent, ( link goes here)</span></strong> ";

            }

            //----- Now we show there is any pending rent in this tenant unit
            tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            r_pendingrentlist.DataSource = v.getPendingRentList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id.Value));

            r_pendingrentlist.DataBind();

            ///------------------- 
            ///


            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prRentView", conn);
            SqlCommand cmd2 = new SqlCommand("prLastRentPaidDate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            DateTime the_date = new DateTime();
            the_date = DateTime.Now; // the date in the to drop downlist

            DateTime la_date = new DateTime();//date begin of rent
            DateTime last_rp_date = new DateTime(); // Last paid rent date ( or current )

            tiger.Date d = new tiger.Date();
            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


            //Add the params
            cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id.Value);
            cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;

            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                    lbl_rent_amount.Text = Convert.ToString(Convert.ToInt32(dr["rl_rent_amount"]));
                    ddl_rl_rent_paid_every.SelectedValue = dr["re_id"].ToString();


                    //  lbl_current_rl_date_begin.Text = String.Format("{0:d}", dr["rl_date_begin"]);


                    la_date = Convert.ToDateTime(dr["rl_date_begin"]);

                    lbl_current_rl_date_begin.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();

                    lbl_current_rl_date_begin2.Text = lbl_current_rl_date_begin.Text;


                    DateTime new_date_begin = new DateTime();
                    new_date_begin = Convert.ToDateTime(dr["rl_date_begin"]);

                    hd_min_rent_begin_date_m.Value = new_date_begin.Month.ToString();
                    hd_min_rent_begin_date_d.Value = new_date_begin.Day.ToString();
                    hd_min_rent_begin_date_y.Value = new_date_begin.Year.ToString();


                    ddl_rl_date_begin_m.SelectedValue = hd_min_rent_begin_date_m.Value;
                    ddl_rl_date_begin_d.SelectedValue = hd_min_rent_begin_date_d.Value;
                    ddl_rl_date_begin_y.SelectedValue = hd_min_rent_begin_date_y.Value;
                }
            }

            finally
            {

                // conn.Close();
            }


            cmd2.CommandType = CommandType.StoredProcedure;


            try
            {
                // conn.Open();
                //Add the params
                cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd2.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id.Value);
                cmd2.Parameters.Add("@last_rp_date", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;

                //execute the insert
                cmd2.ExecuteReader();

                if (cmd2.Parameters["@last_rp_date"].Value != System.DBNull.Value)
                    last_rp_date = Convert.ToDateTime(cmd2.Parameters["@last_rp_date"].Value);
                else
                    last_rp_date = Convert.ToDateTime("1/1/1900");

            }
            finally
            {
                conn.Close();
            }

            if (la_date < last_rp_date)
            {

                lbl_current_rl_date_begin2.Text = last_rp_date.Month.ToString() + "-" + last_rp_date.Day.ToString() + "-" + last_rp_date.Year.ToString();

                hd_min_rent_begin_date_m.Value = last_rp_date.Month.ToString();
                hd_min_rent_begin_date_d.Value = last_rp_date.Day.ToString();
                hd_min_rent_begin_date_y.Value = last_rp_date.Year.ToString();


                ddl_rl_date_begin_m.SelectedValue = last_rp_date.AddDays(1).Month.ToString();
                ddl_rl_date_begin_d.SelectedValue = last_rp_date.AddDays(1).Day.ToString();
                ddl_rl_date_begin_y.SelectedValue = last_rp_date.AddDays(1).Year.ToString();
            }

            // hd_home_id.Value = ddl_home_id.Text;
            // hd_unit_id.Value = ddl_unit_id.Text;

        }
        else
        {
            txt_message.InnerHtml = "This  Unit is not rented";
            r_pendingrentlist.Visible = false;

            panel_current_tenant.Visible = false;
            panel_rent_update.Visible = false;
            hd_current_tu_id.Value = "0";
        }

        // TabContainer1.ActiveTabIndex = 0 ;
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_unit_id_2_SelectedIndexChanged(object sender, EventArgs e)
    {
        txt_message_2.InnerHtml = "";

        r_pendingrentlist_2.Visible = true;
        txt_pending_2.InnerHtml = "";
        btn_continue_2.Enabled = true;
        panel_current_tenant_2.Visible = true;

        hd_unit_id_2.Value = Convert.ToString(ddl_unit_id_2.SelectedValue);
        // label_added_unit.Text = ddl_unit_id.SelectedItem.Text;
        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        int temp_tenant_id = u.getCurrentTenantId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_unit_id_2.SelectedValue));
        //if there is a current tenant in the unit then get name(s)
        if (temp_tenant_id > 0)
        {
            panel_current_tenant_2.Visible = true;
            txt_current_tenant_name_2.InnerHtml = u.getTenantUnitName(Convert.ToInt32(Session["schema_id"]), temp_tenant_id);


            //tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            tiger.Unit unit = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            hd_current_tu_id_2.Value = Convert.ToString(unit.getCurrentTenantUnitId(Convert.ToInt32(temp_tenant_id), Convert.ToInt32(ddl_unit_id_2.SelectedValue)));


            panel_rent_update_2.Visible = true;

            //---Here we count the number of pending rent in this tenant unit
            int count = 0;

            tiger.Lease p = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            count = p.getCountPendingRent(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id_2.Value));


            if (count > 0)
            {
                btn_continue_2.Enabled = false;
                txt_pending_2.InnerHtml = "<strong><span style='color: #ff3300'>You already have a pending rent ,<br /> please remove pending before creating another rent, ( link goes here)</span></strong> ";

            }

            //----- Now we show there is any pending rent in this tenant unit
            tiger.Lease v = new tiger.Lease(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            r_pendingrentlist_2.DataSource = v.getPendingRentList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(hd_current_tu_id_2.Value));

            r_pendingrentlist_2.DataBind();

            ///------------------- 
            ///


            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prRentView", conn);
            SqlCommand cmd2 = new SqlCommand("prLastRentPaidDate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            DateTime the_date = new DateTime();
            the_date = DateTime.Now; // the date in the to drop downlist

            DateTime la_date = new DateTime();//date begin of rent
            DateTime last_rp_date = new DateTime(); // Last paid rent date ( or current )

            tiger.Date d = new tiger.Date();
            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            the_date = Convert.ToDateTime(d.DateCulture(the_date.Month.ToString(), the_date.Day.ToString(), the_date.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


            //Add the params
            cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id_2.Value);
            cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;

            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                    lbl_rent_amount_2.Text = Convert.ToString(Convert.ToInt32(dr["rl_rent_amount"]));
                    ddl_rl_rent_paid_every.SelectedValue = dr["re_id"].ToString();


                    //  lbl_current_rl_date_begin.Text = String.Format("{0:d}", dr["rl_date_begin"]);


                    la_date = Convert.ToDateTime(dr["rl_date_begin"]);

                    lbl_current_rl_date_begin_2.Text = la_date.Month.ToString() + "-" + la_date.Day.ToString() + "-" + la_date.Year.ToString();

                    lbl_current_rl_date_begin_3.Text = lbl_current_rl_date_begin_2.Text;


                    DateTime new_date_begin = new DateTime();
                    new_date_begin = Convert.ToDateTime(dr["rl_date_begin"]);

                    hd_min_rent_begin_date_m2.Value = new_date_begin.Month.ToString();
                    hd_min_rent_begin_date_d2.Value = new_date_begin.Day.ToString();
                    hd_min_rent_begin_date_y2.Value = new_date_begin.Year.ToString();


                    ddl_rl_date_begin_m2.SelectedValue = hd_min_rent_begin_date_m2.Value;
                    ddl_rl_date_begin_d2.SelectedValue = hd_min_rent_begin_date_d2.Value;
                    ddl_rl_date_begin_y2.SelectedValue = hd_min_rent_begin_date_y2.Value;

                    if (dr["unit_type"].ToString() == "C" && Convert.ToInt32(dr["company_id"]) > 0)
                    {
                        lbl_company.Text = dr["company_name"].ToString();
                    }
                    else
                    {
                        tr_company.Visible = false;
                    }
                }
            }

            finally
            {

                // conn.Close();
            }


            cmd2.CommandType = CommandType.StoredProcedure;


            try
            {
                // conn.Open();
                //Add the params
                cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd2.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id_2.Value);
                cmd2.Parameters.Add("@last_rp_date", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;

                //execute the insert
                cmd2.ExecuteReader();

                if (cmd2.Parameters["@last_rp_date"].Value != System.DBNull.Value)
                    last_rp_date = Convert.ToDateTime(cmd2.Parameters["@last_rp_date"].Value);
                else
                    last_rp_date = Convert.ToDateTime("1/1/1900");

            }
            finally
            {
                conn.Close();
            }

            if (la_date < last_rp_date)
            {

                lbl_current_rl_date_begin_3.Text = last_rp_date.Month.ToString() + "-" + last_rp_date.Day.ToString() + "-" + last_rp_date.Year.ToString();

                hd_min_rent_begin_date_m2.Value = last_rp_date.Month.ToString();
                hd_min_rent_begin_date_d2.Value = last_rp_date.Day.ToString();
                hd_min_rent_begin_date_y2.Value = last_rp_date.Year.ToString();


                ddl_rl_date_begin_m2.SelectedValue = last_rp_date.AddDays(1).Month.ToString();
                ddl_rl_date_begin_d2.SelectedValue = last_rp_date.AddDays(1).Day.ToString();
                ddl_rl_date_begin_y2.SelectedValue = last_rp_date.AddDays(1).Year.ToString();
            }

            // hd_home_id.Value = ddl_home_id.Text;
            // hd_unit_id.Value = ddl_unit_id.Text;

        }
        else
        {
            txt_message_2.InnerHtml = "This  Unit is not rented";
            r_pendingrentlist_2.Visible = false;

            panel_current_tenant_2.Visible = false;
            panel_rent_update_2.Visible = false;
            hd_current_tu_id_2.Value = "0";
        }

        //TabContainer1.ActiveTabIndex = 1;
    }


    //------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_continue_Onclick(object sender, EventArgs e)
    {

        Page.Validate("vg_res");
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        DateTime min_date_begin = new DateTime();
        DateTime date_begin = new DateTime();

        tiger.Date d = new tiger.Date();
        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        min_date_begin = Convert.ToDateTime(d.DateCulture(hd_min_rent_begin_date_m.Value, hd_min_rent_begin_date_d.Value, hd_min_rent_begin_date_y.Value, Convert.ToString(Session["_lastCulture"])));
        date_begin = Convert.ToDateTime(d.DateCulture(ddl_rl_date_begin_m.SelectedValue, ddl_rl_date_begin_d.SelectedValue, ddl_rl_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        if (date_begin > min_date_begin)
        {
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prRentModify", conn); // OU prRentModify
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id.Value);
                cmd.Parameters.Add("@rl_rent_amount", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(new_rl_rent_amount.Text));

                //tiger.Date d = new tiger.Date();

                cmd.Parameters.Add("@rl_rent_paid_every", SqlDbType.Int).Value = Convert.ToInt32(ddl_rl_rent_paid_every.SelectedValue);
                cmd.Parameters.Add("@rl_date_begin", SqlDbType.SmallDateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_rl_date_begin_m.SelectedValue, ddl_rl_date_begin_d.SelectedValue, ddl_rl_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
                cmd.Parameters.Add("@current_rl_date_end", SqlDbType.SmallDateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_rl_date_begin_m.SelectedValue, ddl_rl_date_begin_d.SelectedValue, ddl_rl_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

                cmd.ExecuteReader();
            }
            catch
            {
                // TODO

            }

            finally
            {

                conn.Close();
            }

        }

        //TabContainer1.ActiveTabIndex = 0 ;
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_continue_2_Onclick(object sender, EventArgs e)
    {
        Page.Validate("vg_com");
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        DateTime min_date_begin = new DateTime();
        DateTime date_begin = new DateTime();

        tiger.Date d = new tiger.Date();
        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        min_date_begin = Convert.ToDateTime(d.DateCulture(hd_min_rent_begin_date_m2.Value, hd_min_rent_begin_date_d2.Value, hd_min_rent_begin_date_y2.Value, Convert.ToString(Session["_lastCulture"])));
        date_begin = Convert.ToDateTime(d.DateCulture(ddl_rl_date_begin_m2.SelectedValue, ddl_rl_date_begin_d2.SelectedValue, ddl_rl_date_begin_y2.SelectedValue, Convert.ToString(Session["_lastCulture"])));



        if (date_begin > min_date_begin)
        {
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prRentModify", conn); // OU prRentModify
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id_2.Value);
                cmd.Parameters.Add("@rl_rent_amount", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(new_rl_rent_amount_2.Text));

                //tiger.Date d = new tiger.Date();

                cmd.Parameters.Add("@rl_rent_paid_every", SqlDbType.Int).Value = Convert.ToInt32(ddl_rl_rent_paid_every_2.SelectedValue);
                cmd.Parameters.Add("@rl_date_begin", SqlDbType.SmallDateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_rl_date_begin_m2.SelectedValue, ddl_rl_date_begin_d2.SelectedValue, ddl_rl_date_begin_y2.SelectedValue, Convert.ToString(Session["_lastCulture"])));
                cmd.Parameters.Add("@current_rl_date_end", SqlDbType.SmallDateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_rl_date_begin_m2.SelectedValue, ddl_rl_date_begin_d2.SelectedValue, ddl_rl_date_begin_y2.SelectedValue, Convert.ToString(Session["_lastCulture"])));

                cmd.ExecuteReader();
            }
            catch
            {
                // TODO

            }

            finally
            {

                conn.Close();
            }

            //TabContainer1.ActiveTabIndex = 1;

        }
    }
}
