﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : july 23 , 2008
/// </summary>
public partial class manager_lease_lease_expiration : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {



            tiger.Home l = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getHomeCount(Convert.ToInt32(Session["schema_id"]));

            int home_id = l.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));
            
            string referrer = "";

            if(Request.UrlReferrer != null)
              referrer = Request.UrlReferrer.AbsolutePath.ToString();
           // Label4.Text = referrer ;

            // first we check if there's any property available
            if (home_count > 0)
            {
                
                //if the url referer is from the reminder page
                if (referrer == "/sinfo/manager/alerts/alerts.aspx" || referrer == "/manager/alerts/alerts.aspx")
                {
                    if (Request.QueryString["home_id"] != string.Empty)
                    {
                        if (!RegEx.IsInteger(Request.QueryString["home_id"]))
                        {
                            Session.Abandon();
                            Response.Redirect("~/login.aspx");
                        }

                        home_id = Convert.ToInt32(Request.QueryString["home_id"]);
                    }

                    AccountObjectAuthorization homeAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    
                    ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
                    if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), home_id) && home_id != 0)
                    {
                        Session.Abandon();
                        Response.Redirect("~/login.aspx");
                    }
                    ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
             

                    tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    ddl_home_list.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                    ddl_home_list.DataBind();
                    ddl_home_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
                    ddl_home_list.SelectedValue = home_id.ToString();


                   

                    tiger.Lease lease = new tiger.Lease(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    gv_lease_list1.DataSource = lease.getLeaseExpirationList(Convert.ToInt32(Session["schema_id"]), home_id, 1);
                    gv_lease_list2.DataSource = lease.getLeaseExpirationList(Convert.ToInt32(Session["schema_id"]), home_id, 2);
                    gv_lease_list3.DataSource = lease.getLeaseExpirationList(Convert.ToInt32(Session["schema_id"]), home_id, 3);

                    gv_lease_list1.DataBind();
                    gv_lease_list2.DataBind();
                    gv_lease_list3.DataBind();

                }
                //if the url referer is not  from the reminder page
                else
                {
                    tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    ddl_home_list.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                    ddl_home_list.DataBind();
                    ddl_home_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));


                    tiger.Lease lease = new tiger.Lease(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    gv_lease_list1.DataSource = lease.getLeaseExpirationList(Convert.ToInt32(Session["schema_id"]), 0, 1);
                    gv_lease_list2.DataSource = lease.getLeaseExpirationList(Convert.ToInt32(Session["schema_id"]), 0, 2);
                    gv_lease_list3.DataSource = lease.getLeaseExpirationList(Convert.ToInt32(Session["schema_id"]), 0, 3);

                    gv_lease_list1.DataBind();
                    gv_lease_list2.DataBind();
                    gv_lease_list3.DataBind();
                }





            }


        }

    }



    protected void ddl_home_list_SelectedIndexChanged(object sender, EventArgs e)
    {

        int home_id = 0;

        home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        tiger.Lease lease = new tiger.Lease(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_lease_list1.DataSource = lease.getLeaseExpirationList(Convert.ToInt32(Session["schema_id"]), home_id, 1);
        gv_lease_list2.DataSource = lease.getLeaseExpirationList(Convert.ToInt32(Session["schema_id"]), home_id, 2);
        gv_lease_list3.DataSource = lease.getLeaseExpirationList(Convert.ToInt32(Session["schema_id"]), home_id, 3);

        gv_lease_list1.DataBind();
        gv_lease_list2.DataBind();
        gv_lease_list3.DataBind();


    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_lease_list1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {


        int home_id = 0;

        home_id = Convert.ToInt32(ddl_home_list.SelectedValue);
        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.Lease lease = new tiger.Lease(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_lease_list1.PageIndex = e.NewPageIndex;
        gv_lease_list1.DataSource = lease.getLeaseExpirationList(Convert.ToInt32(Session["schema_id"]), home_id, 1);
        gv_lease_list1.DataBind();

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_lease_list2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {


        int home_id = 0;

        home_id = Convert.ToInt32(ddl_home_list.SelectedValue);
        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.Lease lease = new tiger.Lease(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_lease_list2.PageIndex = e.NewPageIndex;
        gv_lease_list2.DataSource = lease.getLeaseExpirationList(Convert.ToInt32(Session["schema_id"]), home_id, 2);
        gv_lease_list2.DataBind();

    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_lease_list3_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {


        int home_id = 0;

        home_id = Convert.ToInt32(ddl_home_list.SelectedValue);
        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.Lease lease = new tiger.Lease(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_lease_list3.PageIndex = e.NewPageIndex;
        gv_lease_list3.DataSource = lease.getLeaseExpirationList(Convert.ToInt32(Session["schema_id"]), home_id, 3);
        gv_lease_list3.DataBind();

    }

}
