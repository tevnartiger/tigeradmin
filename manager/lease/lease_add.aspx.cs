﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : dec 29 , 2008
/// </summary>
public partial class manager_home_lease_add : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    
    protected void btn_next_Click(object sender, EventArgs e)
    {
        if (radio_lease_time.SelectedValue == "1") //residentiel
        {
            if (radio_lease_type.SelectedValue == "1")  // new residential lease
                Response.Redirect("lease_res_add_1.aspx");
            if (radio_lease_type.SelectedValue == "2")  
                Response.Redirect("lease_com_add_1.aspx");  // new commercial
              

        }

        if (radio_lease_time.SelectedValue == "2") //commercial
        {
            if (radio_lease_type.SelectedValue == "1")          
               Response.Redirect("lease_res_old_add_1_tmp.aspx");  // old residential
            if (radio_lease_type.SelectedValue == "2")
                Response.Redirect("lease_com_old_add_1_tmp.aspx");  // old commercial

            //lease_com_old_add_1_tmp.aspx"
        }
    }
}
