﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="lease_com_add_1.aspx.cs" Inherits="manager_lease_lease_com_add_1" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <B>CREATE COMMERCIAL LEASE - STEP 1 </B><br /><br />
   
         <table>
            <tr><td>
            <asp:Panel ID="panel_add_home" runat="server" Height="40px" Width="330px">
          <asp:DropDownList ID="ddl_home_id" DataValueField="home_id" DataTextField="home_name"   runat="server" autopostback="true" OnSelectedIndexChanged="ddl_home_id_SelectedIndexChanged" />
                </asp:Panel>
           </td></tr>
           
           <tr><td><div id="txt_pending" runat="server"></div></td></tr>
           <tr><td><div id="txt_message" runat="server"></div></td></tr>
         
         <tr>
         
         <td>
         
    <asp:Repeater ID="r_pendingleaseslist"  runat="server">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
            <tr>
                <td>
                   <b>Pending lease date begin</b> </td>
                <td >
                    : 
                    <%#DataBinder.Eval(Container.DataItem, "tu_date_begin")%>
                    <input type="hidden" id="hd_pending_tu_date_begin" value ='<%#DataBinder.Eval(Container.DataItem, "tu_date_begin")%>' runat="server" />
                    
               </td>
            </tr>
         </table>
        </ItemTemplate>
        
    </asp:Repeater>
            
         
         
         </td>
         </tr>
        
           <tr><td>
        <asp:Panel ID="panel_add_unit"  runat="server" Height="5px" Width="328px">
            <strong><span style="font-size: 10pt">select unit</span></strong> :
            <asp:DropDownList ID="ddl_unit_id" DataValueField="unit_id" DataTextField="unit_door_no"   runat="server" OnSelectedIndexChanged="ddl_unit_id_SelectedIndexChanged" AutoPostBack="true" />
                </asp:Panel>
            
           </td></tr>
           
           <asp:Panel ID="panel_create_lease" runat="server">
           <tr><td><br />
             <asp:Panel ID="panel_current_tenant"  Visible="false" runat="server"><br />
           <b> Company :</b> &nbsp;&nbsp; <asp:Label ID="lbl_company_name" runat="server" ></asp:Label><br /><br />
             <table><tr><td colspan='2'><div id="txt_current_tenant_name" runat="server" /></td></tr><tr><td>
                 Date of departure(mm-dd-yyy)</td><td>
                     <asp:DropDownList ID="ddl_tu_date_end_m" runat="server">
                     <asp:ListItem Value="0">Month</asp:ListItem>
                     <asp:ListItem Value="1">January</asp:ListItem>
                     <asp:ListItem Value="2">February</asp:ListItem>
                     <asp:ListItem Value="3">March</asp:ListItem>
                     <asp:ListItem Value="4">April</asp:ListItem>
                     <asp:ListItem Value="5">May</asp:ListItem>
                     <asp:ListItem Selected="True" Value="6">June</asp:ListItem>
                     <asp:ListItem Value="7">July</asp:ListItem>
                     <asp:ListItem Value="8">August</asp:ListItem>
                     <asp:ListItem Value="9">September</asp:ListItem>
                     <asp:ListItem Value="10">October</asp:ListItem>
                     <asp:ListItem Value="11">November</asp:ListItem>
                     <asp:ListItem Value="12">December</asp:ListItem>
                 </asp:DropDownList>&nbsp; / &nbsp;
                 <asp:DropDownList ID="ddl_tu_date_end_d" runat="server">
                     <asp:ListItem Value="0">Day</asp:ListItem>
                     <asp:ListItem Selected="True">1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>&nbsp; / &nbsp;
                 <asp:DropDownList ID="ddl_tu_date_end_y" runat="server" >
                    
                 </asp:DropDownList></td></tr></table>
             </asp:Panel>
           </td></tr>
           <tr><td>
           <table><tr><td colspan="2" class="nameb">New tenant</td></tr><tr><td class="letter">
               Lease starting date(mm-dd-yyy)</td><td>
           <asp:DropDownList ID="ddl_tu_date_begin_m" runat="server">
                     <asp:ListItem Value="0">Month</asp:ListItem>
                     <asp:ListItem Value="1">January</asp:ListItem>
                     <asp:ListItem Value="2">February</asp:ListItem>
                     <asp:ListItem Value="3">March</asp:ListItem>
                     <asp:ListItem Value="4">April</asp:ListItem>
                     <asp:ListItem Value="5">May</asp:ListItem>
                     <asp:ListItem  Value="6">June</asp:ListItem>
                     <asp:ListItem Value="7">July</asp:ListItem>
                     <asp:ListItem Value="8">August</asp:ListItem>
                     <asp:ListItem Value="9">September</asp:ListItem>
                     <asp:ListItem Value="10">October</asp:ListItem>
                     <asp:ListItem Value="11">November</asp:ListItem>
                     <asp:ListItem Value="12">December</asp:ListItem>
                 </asp:DropDownList>&nbsp; / &nbsp;
                 <asp:DropDownList ID="ddl_tu_date_begin_d" runat="server">
                     <asp:ListItem Value="0">Day</asp:ListItem>
                     <asp:ListItem >1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>&nbsp; / &nbsp;
                 <asp:DropDownList ID="ddl_tu_date_begin_y" runat="server" >
                 </asp:DropDownList></td></tr></table>
           </td></tr>
           </asp:Panel>
           
           
           
           
           <tr><td>
               
               <table   cellpadding="0" cellspacing="0" style="width: 100%">
                   <tr>
                       <td valign="top">
                          
                           <table runat="server" id="tb_last_paid_rent" bgcolor="#ffffcc" cellpadding="0" cellspacing="0" style="width: 100%">
                               <tr>
                                   <td>
                                       Last rent due date :
                                       <asp:Label ID="lbl_min_date_begin" runat="server" style="font-weight: 700"></asp:Label>
                                       <br />
                                       <br />
                                   </td>
                               </tr>
                               <tr>
                                   <td>
                                       Last rent payment :&nbsp;&nbsp;
                                       <asp:Label ID="lbl_last_date_paid" runat="server" style="font-weight: 700"></asp:Label>
                                       <br />
                                       <br />
                                       the date of departure and the&nbsp; new lease start date<br />
                                       must be greater than
                                       <asp:Label ID="lbl_min_date_begin2" runat="server" style="font-weight: 700"></asp:Label>
                                   </td>
                               </tr>
                           </table>
                           <br />
                       </td>
                   </tr>
               </table>
               <br />
           <asp:Button ID="btn_continue" runat="server" Text="Continue"  OnClick="btn_continue_Onclick"/>
          </td></tr>
          <tr><td><div ID="txt_link" runat="server" /></td></tr>
         <tr><td>
         </td></tr>
          </table>
          
         
    <!--Hidden fields BEGIN SECTION 1-->
         <asp:HiddenField Visible="false" id="hd_home_id" runat="server" />
         <asp:HiddenField Visible="false" id="hd_unit_id" runat="server" />
         <asp:HiddenField Visible="false" id="hd_current_tu_id" runat="server" />
         <!--Hidden fields END SECTION 1-->
           <!--Hidden fields temporaire-->
          <asp:HiddenField Visible="false" id="didden" runat="server" />
  
    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label><br />
          

</asp:Content>

