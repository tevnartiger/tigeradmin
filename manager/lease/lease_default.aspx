﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="lease_default.aspx.cs" Inherits="manager_lease_lease_default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<br />

<table>

<tr><td> <asp:HyperLink ID="HyperLink2" NavigateUrl="~/manager/lease/lease_list.aspx" runat="server"><h2>  <asp:Literal ID="Literal6" Text="Lease List" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to view and edit all properties from your account.</td></tr>

<tr><td> <asp:HyperLink ID="HyperLink5" NavigateUrl="~/manager/lease/lease_add.aspx" runat="server"><h2>  <asp:Literal ID="Literal5" Text="New Lease" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to view and edit all properties from your account.</td></tr>

<tr><td> <asp:HyperLink ID="HyperLink1" NavigateUrl="~/manager/lease/lease_rent_update.aspx" runat="server"><h2>  <asp:Literal ID="Literal1" Text="Edit rent amount" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to view general information of one or more properties all in 1 page.</td></tr>


<tr><td> <asp:HyperLink ID="HyperLink4" NavigateUrl="~/manager/lease/lease_accommodation_update.aspx" runat="server"><h2><asp:Literal ID="Literal4" Text="Edit accomodations" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to create a new property to your account.</td></tr>


<tr><td> <asp:HyperLink ID="HyperLink6" NavigateUrl="~/manager/lease/lease_term_cond_update.aspx" runat="server"><h2><asp:Literal ID="Literal2" Text="Edit terms & conditions" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to create a new property to your account.</td></tr>


<tr><td> <asp:HyperLink ID="HyperLink3" NavigateUrl="~/manager/timeline/timeline_lease.aspx" runat="server"><h2>  <asp:Literal ID="Literal3" Text="View Time line" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to manage the insurances.</td></tr>

</table>
</asp:Content>

