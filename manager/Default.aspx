﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="manager_default" Title="Untitled Page" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
 
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="dashboard_container">

        <div id="top_row" class="row">
            <div class="col98"></div>
        </div>

        <asp:Panel ID="panel_alerts" runat="server">
            <div class="divborder" style="text-align: left; width: 100%;">
                <table runat="server" class="padded-table" width="100%" bgcolor="#FFFFcc">
                    <tr>
                        <td>
                            <asp:Label ID="Label6" Text="<%$ Resources:Resource, lbl_u_alerts %>"
                                runat="server" Style="font-weight: 900; font-size: small"></asp:Label>
                        </td>
                    </tr>
                    <tr id="tr_lease_expiration" runat="server">
                        <td>&nbsp;
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_lease_expiration" runat="server"></asp:Label>
                            &nbsp;<asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lbl_lease_expire %>"></asp:Label>
                            <asp:Label ID="lbl_as_lease_expiration" runat="server"></asp:Label>
                            &nbsp;<asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>"></asp:Label>
                        </td>

                    </tr>
                    <tr id="tr_lease_pending" runat="server">
                        <td>&nbsp;
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_lease_pending" runat="server"></asp:Label>
                            &nbsp;<asp:Label ID="Label13" runat="server" Text="<%$ Resources:Resource, lbl_lease_pending %>"></asp:Label>
                            <asp:Label ID="lbl_as_lease_pending" runat="server"></asp:Label>
                            &nbsp;<asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>"></asp:Label>
                        </td>

                    </tr>
                    <tr id="tr_mortgage_expiration" runat="server">
                        <td>&nbsp;
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_mortgage_expiration" runat="server"></asp:Label>
                            &nbsp;<asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, lbl_mortgage_expire %>"></asp:Label>
                            <asp:Label ID="lbl_as_mortgage_expiration" runat="server"></asp:Label>
                            &nbsp;<asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>"></asp:Label>
                        </td>

                    </tr>
                    <tr id="tr_mortgage_pending" runat="server">
                        <td>&nbsp;
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_mortgage_pending" runat="server"></asp:Label>
                            &nbsp;<asp:Label ID="Label12" runat="server" Text="<%$ Resources:Resource, lbl_mortgage_pending %>"></asp:Label>
                            <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_as_mortgage_pending" runat="server"></asp:Label>
                            &nbsp;<asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>"></asp:Label>
                        </td>

                    </tr>
                    <tr id="tr_ip_expiration" runat="server">
                        <td>&nbsp;
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_ip_expiration" runat="server"></asp:Label>
                            &nbsp;<asp:Label ID="Label10" runat="server" Text="<%$ Resources:Resource, lbl_ip_expire %>"></asp:Label>
                            <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_as_ip_expiration" runat="server"></asp:Label>
                            &nbsp;<asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>"></asp:Label>
                        </td>

                    </tr>
                    <tr id="tr_ip_pending" runat="server">
                        <td>&nbsp;
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_ip_pending" runat="server"></asp:Label>
                            &nbsp;<asp:Label ID="Label11" runat="server" Text="<%$ Resources:Resource, lbl_ip_PENDING %>"></asp:Label>
                            <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_as_ip_pending" runat="server"></asp:Label>
                            &nbsp;<asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>"></asp:Label>
                        </td>

                    </tr>
                    <tr id="tr_rent_delequency" runat="server">
                        <td>&nbsp;
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_rent_delequency" runat="server"></asp:Label>
                            &nbsp;<asp:Label ID="Label14" runat="server" Text="<%$ Resources:Resource, lbl_late_rent_not_received %>"></asp:Label></td>

                    </tr>

                    <tr id="tr_wo_overdue" runat="server">
                        <td>&nbsp;
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_wo_overdue" runat="server"></asp:Label>
                            &nbsp;<asp:Label ID="Label16" runat="server" Text="<%$ Resources:Resource, lbl_wo_overdue %>"></asp:Label></td>

                    </tr>
                    <tr id="tr_wo_pending" runat="server">
                        <td>&nbsp;
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_wo_pending" runat="server"></asp:Label>
                            &nbsp;<asp:Label ID="Label18" runat="server" Text="<%$ Resources:Resource, lbl_wo_pending%>"></asp:Label></td>

                    </tr>
                    <tr id="tr_untreated_rent" runat="server">
                        <td>&nbsp;
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_untreated_rent" runat="server"></asp:Label>
                            &nbsp;<asp:Label ID="Label17" runat="server" Text="<%$ Resources:Resource, lbl_alert_untreated_rent%>"></asp:Label></td>

                    </tr>
                    <tr id="tr1" runat="server">
                        <td>&nbsp;&nbsp;
                                    <asp:HyperLink Text="<%$ Resources:Resource, lbl_setup %>"
                                        ID="HyperLink1" runat="server" NavigateUrl="~/manager/alerts/alerts_setup.aspx"></asp:HyperLink>

                            &nbsp;&nbsp;
                                    <asp:HyperLink Text="<%$ Resources:Resource, lbl_details %>"
                                        ID="HyperLink46" runat="server"
                                        NavigateUrl="~/manager/alerts/alerts.aspx"></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br />

                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <br />
        </asp:Panel>


        <div class="divborder" style="text-align: left; width: 100%;">
            <table width="100%">
                <tr>
                    <td colspan="2" style="border-bottom: 3px solid #000000; background: url(../App_Themes/SinfoTiger/images/grid_bg.png) repeat-x;">
                        <b>CURRENT MONTH RENT PAYMENTS</b></td>
                    <td style="border-bottom: 3px solid #000000; background: url(../App_Themes/SinfoTiger/images/grid_bg.png) repeat-x;">&nbsp;&nbsp; ( % )</td>
                </tr>
                <tr>
                    <td><a href="home/home_unit_rented_list.aspx"># Leases</a></td>
                    <td>
                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_lease_count2" runat="server"></asp:Label>
                    </td>
                    <td>&nbsp; -&nbsp;</td>
                </tr>
                <tr>
                    <td>Paid rent</td>
                    <td>
                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_paid_rent_count" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_paid_rent_count_percent" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Unpaid rent</td>
                    <td>
                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_unpaid_rent_count" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_unpaid_rent_count_percent" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Partial payment </td>
                    <td>
                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_partial_rent_count" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_partial_rent_count_percent" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <br />
        <br />

        <div class="divborder" style="text-align: left; width: 100%;">
            <table class="padded-table" width="100%">
                <tr>
                    <td><b>CASH FLOW</b></td>
                    <td>&nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td>Total income for all the propertie</td>
                    <td>
                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_income" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Total expense for all the properties</td>
                    <td>
                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_expense" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>Cash flow ( Income - Expense )</td>
                    <td>
                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_cash_flow" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <br />

        <div id="second_row" class="row">
            <div id="second_row_left" class="col59">

                <div id="second_row_left_bottom" class="row">
                    <div id="second_row_left_bottom_left" class="col30">

                        <div class="divborder" style="text-align: left; width: 100%;">
                            <div class="dashboarddivheader">Properties</div>
                            <table class="padded-table" width="100%">
                                <tr>
                                    <td><a href="/manager/property/property_list.aspx">Total Properties</a></td>
                                    <td>
                                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_home_count" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>Total Units</td>
                                    <td>
                                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_unit_count" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>Occupied units</td>
                                    <td>
                                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_occ_unit_count" runat="server"></asp:Label></td>
                                </tr>
                                <tr>
                                    <td>Vacant units</td>
                                    <td>
                                        <asp:Label ID="lbl_vacant_unit_count" Font-Size="16px" runat="server"></asp:Label></td>
                                </tr>
                            </table>
                        </div>
                        <br />
                        <br />
                        <div class="divborder" style="text-align: left; width: 100%;">
                            <b>
                                <asp:Label ID="Label15" runat="server" Text="<%$ Resources:Resource, lbl_vacancy %>" /></b>
                            <br />
                            <br />
                            <asp:GridView Width="60%" ID="gv_HomeNumberOfUnitAvailable"
                                runat="server" AutoGenerateColumns="false"
                                AllowPaging="true" AllowSorting="true" AlternatingRowStyle-BackColor="#F0F0F6"
                                BorderColor="#CDCDCD" BorderWidth="1" GridLines="Both"
                                HeaderStyle-BackColor="#F0F0F6" EmptyDataText="<%$ Resources:Resource, lbl_none %>">
                                <Columns>
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, lbl_property %>" DataField="home_name" />
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, lbl_nb_unit %>" DataField="number_of_units" />
                                </Columns>
                            </asp:GridView>
                            <br />
                            <br />
                        </div>


                        <div class="divborder" style="text-align: left; width: 100%;display:none">
                            <div class="dashboarddivheader">BUSINESS CONTACTS</div>
                            <table class="padded-table" width="100%">
                                <tr>
                                    <td>Contractors</td>
                                    <td>
                                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_company_count" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Vendor / supplier</td>
                                    <td>
                                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_supplier_vendor_count" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Financial Institution</td>
                                    <td>
                                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_fi_count" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Insurance companie</td>
                                    <td>
                                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_ic_count" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Warehouse</td>
                                    <td>
                                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_warehouse_count" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>

                    </div>


                    <div id="second_row_left_bottom_right" style="display: none" class="col29">

                        <div class="divborder" style="text-align: left; width: 100%;">
                            <div class="dashboarddivheader">USER</div>
                            <table class="padded-table" width="100%">
                                <tr>
                                    <td>Tenants</td>
                                    <td>
                                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_tenant_count" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Owner</td>
                                    <td>
                                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_owner_count" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Janitor</td>
                                    <td>
                                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_janitor_count" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Property Manager</td>
                                    <td>
                                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_pm_count" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br />
                        <br />

                        <div class="divborder" style="text-align: left; width: 100%;">
                            <div class="dashboarddivheader">INVENTORY</div>
                            <table class="padded-table" width="100%">
                                <tr>
                                    <td>Items in the properties</td>
                                    <td>
                                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_home_item_count" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Items in external storage ( warehouse )</td>
                                    <td>
                                        <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_warehouse_item_count" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>


                    </div>
                </div>
            </div>

            <div id="second_row_right" class="col39">
                <div class="divborder" style="text-align: left; width: 100%;">
                    <div class="dashboarddivheader">DAILY TASK</div>
                    <table class="padded-table" width="100%">
                        <tr>
                            <td>Incident in the last 48 hours</td>
                            <td>
                                <asp:Label EnableTheming="false" ID="lbl_incident_last48hrs_count" Font-Size="14px" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="/manager/incident/incident_list.aspx">Incident in the last 30 days</a></td>
                            <td>
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_incident_last30days_count" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="/manager/workorder/wo_list.aspx">Open work order</a></td>
                            <td>
                                <asp:Label EnableTheming="false" ID="lbl_wo_count" Font-Size="14px" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td><a href="/manager/Scheduler/default2.aspx">Calendar message +7 days</a></td>
                            <td>
                                <asp:Label EnableTheming="false" ID="lbl_event_within7days_count" Font-Size="14px" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <br />



                <div class="divborder" style="text-align: left; width: 100%;">
                    <div class="dashboarddivheader">LEASE</div>
                    <table class="padded-table" width="100%">
                        <tr>
                            <td><a href="/manager/lease/lease_list.aspx">Current Lease</a></td>
                            <td>
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_lease_count" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Pending Lease</td>
                            <td>
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_pending_lease_count" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Archived Lease</td>
                            <td>
                                <asp:Label EnableTheming="false" Font-Size="14px" ID="lbl_archive_lease_count" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                </div>


            </div>



        </div>

    </div>



</asp:Content>



