<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="financial_institution_update.aspx.cs" Inherits="financialinstitution_financial_institution_update" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

   FINANCIAL INSTITUTION UPDATE<br /><br />
        
        <table bgcolor="#ffffcc" >
            <tr>
                <td >
                    name</td>
                <td  >
                    : 
                    <asp:TextBox ID="fi_name" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator 
                        ID="reg_fi_name" runat="server" 
                         ControlToValidate="fi_name"
                        ErrorMessage="invalid name">
                        </asp:RegularExpressionValidator>
                &nbsp;<asp:RequiredFieldValidator  ControlToValidate="fi_name"
                           ID="req_fi_name" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    website</td>
                <td >
                    : 
                    <asp:TextBox ID="fi_website" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_fi_website" runat="server" 
                         ControlToValidate="fi_website"
                        ErrorMessage="invalid link">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td  >
                    address </td>
                <td  >
                    :
                    <asp:TextBox ID="fi_addr" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator ID="reg_fi_addr" runat="server" 
                        ControlToValidate="fi_addr"
                        ErrorMessage="invalid address">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td >
                    telephone</td>
                <td >
                    :
                    <asp:TextBox ID="fi_tel" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator 
                        ID="reg_fi_tel" runat="server" 
                        ControlToValidate="fi_tel"
                        ErrorMessage="invalid telephone">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td >
                    country</td>
                <td >
                    :
                    <asp:DropDownList ID="ddl_fi_country_list" DataTextField="country_name" DataValueField="country_id" runat="server">
                    
                    </asp:DropDownList></td>
            </tr>
            
            <tr>
                <td  >
                    prov/state</td>
                <td  >
                    :
                    <asp:TextBox ID="fi_prov" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator ID="reg_fi_prov" runat="server" 
                        ControlToValidate="fi_prov"
                        ErrorMessage="enter letters only ">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td >
                    city</td>
                <td >
                    :
                    <asp:TextBox ID="fi_city" runat="server"></asp:TextBox>
                &nbsp;<asp:RegularExpressionValidator ID="reg_fi_city" runat="server" 
                        ControlToValidate="fi_city"
                        ErrorMessage="enter letters only">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td  >
                    postal code</td>
                <td  >
                    :
                    <asp:TextBox ID="fi_pc" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator ID="reg_fi_pc" runat="server" 
                        ControlToValidate="fi_pc"
                        ErrorMessage="invalid postal code">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td   >
                    contact first name</td>
                <td  >
                    :
                    <asp:TextBox ID="fi_contact_fname" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator ID="reg_fi_contact_fname" runat="server" 
                        ControlToValidate="fi_contact_fname"
                        ErrorMessage="invalid name">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td  >
                    contact last name</td>
                <td   >
                    :
                    <asp:TextBox ID="fi_contact_lname" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator ID="reg_fi_contact_lname" runat="server" 
                        ControlToValidate="fi_contact_lname"
                        ErrorMessage="invalid last name">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td  >
                    contact tel.</td>
                <td  >
                    :
                    <asp:TextBox ID="fi_contact_tel" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator ID="reg_fi_contact_tel" 
                        ControlToValidate="fi_contact_tel"
                        runat="server" ErrorMessage="invalid telephone">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td  >
                    contact fax.</td>
                <td  >
                    :
                    <asp:TextBox ID="fi_contact_fax" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator ID="reg_fi_contact_fax" 
                        ControlToValidate="fi_contact_fax"
                        runat="server" ErrorMessage="invalid fax">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            
            <tr>
                <td  >
                    contact email</td>
                <td  >
                    :
                    <asp:TextBox ID="fi_contact_email" runat="server">
                    </asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_fi_contact_email" 
                        ControlToValidate="fi_contact_email"
                        runat="server" ErrorMessage="invalid email">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td valign=top>
                    comments</td>
                <td>
                    
                    <asp:TextBox ID="fi_com" runat="server" Height="100px" TextMode="MultiLine" Width="300px"></asp:TextBox>
                   
                </td>
            </tr>
        
        </table>
        
        <br />
      
        <asp:Button ID="btn_submit" runat="server" Text="submit"  OnClick="btn_submit_Click"/>
        &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <asp:Label ID="lbl" runat="server" ></asp:Label>
  
    <div id="result" runat=server></div>
   
    </asp:Content>
