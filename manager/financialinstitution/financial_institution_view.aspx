<%@ Page Language="C#" MasterPageFile="../mp_manager.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="financial_institution_view.aspx.cs" Inherits="financialinstitution_financial_institution_view" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 
    
    <div>
    FINANCIAL INSTITUTION VIEW <br />
        <asp:HyperLink ID="fi_add_link" 
            runat="server">add</asp:HyperLink> -&nbsp;  <asp:HyperLink ID="fi_update_link"
            runat="server">update</asp:HyperLink> - delete
        &nbsp;<br />
        <br />
            <asp:Repeater runat=server ID="r_financial_institution">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
            <tr>
                <td>
                    name</td>
                <td >
                    : 
                   
                    <%#DataBinder.Eval(Container.DataItem, "fi_name")%></td>
            </tr>
            <tr>
                <td>
                    website</td>
                <td >
                    :
                    <asp:HyperLink ID="fi_website_link"   NavigateUrl=<%#DataBinder.Eval(Container.DataItem, "fi_website")%> Target=_blank runat="server"><%#DataBinder.Eval(Container.DataItem, "fi_website")%></asp:HyperLink>
                   
                   </td>
            </tr>
            
            <tr>
                <td  >
                    address </td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "fi_addr")%></td>
            </tr>
            
            <tr>
                <td  >
                    country</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "country_name")%></td>
            </tr>
            
            <tr>
                <td >
                    prov/state</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "fi_prov")%></td>
            </tr>
            <tr>
                <td>
                    city</td>
                <td>
                    :
                    <%#DataBinder.Eval(Container.DataItem, "fi_city")%>
                </td>
            </tr>
            <tr>
                <td >
                    postal code</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "fi_pc")%></td>
            </tr>
            <tr>
                <td >
                    telephone</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "fi_tel")%>&nbsp;</td>
            </tr>
            <tr>
                
                <td  >
                    contact first name</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "fi_contact_fname")%></td>
            </tr>
            <tr>
                <td >
                    contact last name</td>
                <td  >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "fi_contact_lname")%></td>
            </tr>
            <tr>
                <td >
                    contact tel.</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "fi_contact_tel")%></td>
            </tr>
            <tr>
                <td >
                    contact email</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "fi_contact_email")%></td>
            </tr>
            <tr>
                <td >
                    comments</td>
                <td >
                    :
                    <%#DataBinder.Eval(Container.DataItem, "fi_com")%></td>
            </tr>

        </table>
        </ItemTemplate>
        </asp:Repeater>
        <br />
         <asp:HyperLink ID="link_go_back_to_list" runat="server" NavigateUrl="~/financialinstitution/financial_institution_list.aspx">GO back to list</asp:HyperLink>
        
    </div>
    </asp:Content>
