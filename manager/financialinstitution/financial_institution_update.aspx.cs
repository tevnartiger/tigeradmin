using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.AccountObjectAuthorization;
/// <summary>
/// Done by : Stanley Jocelyn
/// date    : oct 22 , 2007
/// </summary>
public partial class financialinstitution_financial_institution_update : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RegEx.IsInteger(Request.QueryString["fi_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        reg_fi_city.ValidationExpression = RegEx.getText();
        reg_fi_addr.ValidationExpression = RegEx.getText();
        reg_fi_contact_email.ValidationExpression = RegEx.getEmail();
        reg_fi_contact_fname.ValidationExpression = RegEx.getText();
        reg_fi_contact_lname.ValidationExpression = RegEx.getText();
        reg_fi_contact_tel.ValidationExpression = RegEx.getText();
        reg_fi_name.ValidationExpression = RegEx.getText();
        reg_fi_pc.ValidationExpression = RegEx.getText();
        reg_fi_tel.ValidationExpression = RegEx.getText();
        reg_fi_website.ValidationExpression = RegEx.getText();
        reg_fi_prov.ValidationExpression = RegEx.getText();
        reg_fi_contact_fax.ValidationExpression = RegEx.getText();


        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        ////////
        //////// SECURITY CHECK BEGIN //////////////////
        ////////
        AccountObjectAuthorization fiAuthorization = new AccountObjectAuthorization(strconn);

        if (!fiAuthorization.FinancialInstitution(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["fi_id"])))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ////////
        //////// SECURITY CHECK END //////////////////
        ////////

        if (!Page.IsPostBack)
        {
            // The country list
            tiger.Country fi = new tiger.Country(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_fi_country_list.DataSource = fi.getCountryList();
            ddl_fi_country_list.DataBind();

            //  Information about a financial institution ( fi_id )
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prFinancialInstitutionView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@fi_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["fi_id"]);
            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                    fi_name.Text = dr["fi_name"].ToString();
                    fi_website.Text = dr["fi_website"].ToString();
                    fi_addr.Text = dr["fi_addr"].ToString();

                    //lbl.Text = dr["country_id"].ToString();
                    if (dr["country_id"].ToString() == "")
                    { }
                    else
                        ddl_fi_country_list.SelectedValue = dr["country_id"].ToString();

                    fi_city.Text = dr["fi_city"].ToString();
                    fi_prov.Text = dr["fi_prov"].ToString();
                    fi_pc.Text = dr["fi_pc"].ToString();
                    fi_tel.Text = dr["fi_tel"].ToString();
                    fi_contact_fname.Text = dr["fi_contact_fname"].ToString();
                    fi_contact_lname.Text = dr["fi_contact_lname"].ToString();
                    fi_contact_tel.Text = dr["fi_contact_tel"].ToString();
                    fi_contact_email.Text = dr["fi_contact_email"].ToString();
                    fi_com.Text = dr["fi_com"].ToString();

                }

            }
            finally
            {
                conn.Close();
            }
        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {

        Page.Validate();
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }


        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prFinancialInstitutionUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.NVarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@fi_name", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_name.Text);
            cmd.Parameters.Add("@fi_website", SqlDbType.NVarChar, 200).Value = RegEx.getText(fi_website.Text);
            cmd.Parameters.Add("@fi_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["fi_id"]);
            cmd.Parameters.Add("@fi_addr", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_addr.Text);
            cmd.Parameters.Add("@fi_city", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_city.Text);
            cmd.Parameters.Add("@fi_prov", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_prov.Text);
            cmd.Parameters.Add("@fi_pc", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_pc.Text);
            cmd.Parameters.Add("@fi_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_tel.Text);
            cmd.Parameters.Add("@fi_contact_fname", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_contact_fname.Text);
            cmd.Parameters.Add("@fi_contact_lname", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_contact_lname.Text);
            cmd.Parameters.Add("@fi_contact_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_contact_tel.Text);
            cmd.Parameters.Add("@fi_contact_email", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_contact_email.Text);
            cmd.Parameters.Add("@fi_contact_fax", SqlDbType.NVarChar, 50).Value = RegEx.getText(fi_contact_fax.Text);
            cmd.Parameters.Add("@country_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_fi_country_list.SelectedValue);
            cmd.Parameters.Add("@fi_com", SqlDbType.Text).Value = RegEx.getText(fi_com.Text);




            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 0)
                result.InnerHtml = " add successful";


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }
    }
}
