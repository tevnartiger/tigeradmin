using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.AccountObjectAuthorization;
/// <summary>
/// Done by : Stanley Jocelyn
/// date    : oct 22 , 2007
/// </summary>

public partial class financialinstitution_financial_institution_view : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RegEx.IsInteger(Request.QueryString["fi_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        ////////
        //////// SECURITY CHECK BEGIN //////////////////
        ////////
        AccountObjectAuthorization fiAuthorization = new AccountObjectAuthorization(strconn);

        if (!fiAuthorization.FinancialInstitution(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["fi_id"])))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ////////
        //////// SECURITY CHECK END //////////////////
        ////////

        fi_add_link.NavigateUrl = "financial_institution_add.aspx?fi_id=" + Request.QueryString["fi_id"];
        fi_update_link.NavigateUrl = "financial_institution_update.aspx?fi_id=" + Request.QueryString["fi_id"];

        // view the company infos
        tiger.FinancialInstitution h = new tiger.FinancialInstitution(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        r_financial_institution.DataSource = h.getFinancialInstitutionView(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["fi_id"]));
        r_financial_institution.DataBind();

    }
}
