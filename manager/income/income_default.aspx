﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="income_default.aspx.cs" Inherits="manager_income_income_default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<br />
<table>

<tr><td> <asp:HyperLink ID="HyperLink4" NavigateUrl="~/manager/home/home_unit_rented_list.aspx" runat="server">
<h2>  <asp:Literal ID="Literal4" Text="Rent Payment" runat="server" /></h2></asp:HyperLink>
</td><td>Enables you to view and record monthly rent payment..</td></tr>

<tr><td> <asp:HyperLink ID="HyperLink5" NavigateUrl="~/manager/financial/financial_income.aspx" runat="server">
<h2>  <asp:Literal ID="Literal5" Text="Other Income" runat="server" /></h2></asp:HyperLink>
</td><td>Enables you to view and record income other then rents</td></tr>

<tr><td> <asp:HyperLink ID="HyperLink1" NavigateUrl="~/manager/income/income_late_rent_fees.aspx" runat="server">
<h2>  <asp:Literal ID="Literal1" Text="Record Late Rent Fee" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to record a fee to a bounced check for example.</td></tr>

<tr><td> <asp:HyperLink ID="HyperLink6" NavigateUrl="~/manager/income/income_received_late_rent_fees.aspx" runat="server">
<h2>  <asp:Literal ID="Literal6" Text="Late Rent Fee" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to view the list of paid late rent fees.</td></tr>


<tr><td> <asp:HyperLink ID="HyperLink2" NavigateUrl="~/manager/tenant/tenant_untreated_rent.aspx" runat="server">
<h2>  <asp:Literal ID="Literal2" Text="Untreated Rent" runat="server" /></h2></asp:HyperLink>
</td><td>Enable you to view a list of unpaid tenant's rent.</td></tr>

</table>
</asp:Content>

