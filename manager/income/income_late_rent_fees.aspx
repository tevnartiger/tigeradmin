﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="income_late_rent_fees.aspx.cs" Inherits="manager_income_income_late_rent_fees" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_late_rent_fees %>"/>

    <div>
                &nbsp;&nbsp;
        <br />
        <table >
            <tr>
                <td>
      <asp:Label ID="lbl_property" runat="server" Text="<%$ Resources:Resource, lbl_property %>"/>
                </td>
                <td>
       <asp:DropDownList ID="ddl_home_id" DataValueField="home_id" DataTextField="home_name"   runat="server" autopostback="true" OnSelectedIndexChanged="ddl_home_id_SelectedIndexChanged" />
    
    
    
    
                </td>
                <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;</td>
                <td>
      &nbsp;<asp:Label ID="lbl_from" runat="server" Text="<%$ Resources:Resource, lbl_from %>"/>
                </td>
                <td>
                    <asp:DropDownList ID="ddl_from_m" runat="server">
                      <asp:ListItem Text="<%$ Resources:Resource, txt_month %>" Value="0"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
&nbsp;/
                    <asp:DropDownList ID="ddl_from_d" runat="server">
                         <asp:ListItem Text="<%$ Resources:Resource, txt_day %>" Value="0"></asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
&nbsp;/
                    <asp:DropDownList ID="ddl_from_y" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
      <asp:Label ID="lbl_unit" runat="server" Text="<%$ Resources:Resource, lbl_unit %>"/>
                </td>
                <td>
        <asp:DropDownList ID="ddl_unit_id" DataValueField="unit_id" DataTextField="unit_door_no"   runat="server" OnSelectedIndexChanged="ddl_unit_id_SelectedIndexChanged" AutoPostBack="true" />
    
    
    
    
                </td>
                <td>
                    &nbsp;</td>
                <td>
      <asp:Label ID="lbl_to" runat="server" Text="<%$ Resources:Resource, lbl_to %>"/>
                </td>
                <td>
                    <asp:DropDownList ID="ddl_to_m" runat="server">
                    
                      <asp:ListItem Text="<%$ Resources:Resource, txt_month %>" Value="0"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
&nbsp;/
                    <asp:DropDownList ID="ddl_to_d" runat="server">
                        <asp:ListItem Text="<%$ Resources:Resource, txt_day %>" Value="0"></asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
&nbsp;/
                    <asp:DropDownList ID="ddl_to_y" runat="server">
                    </asp:DropDownList>
                </td>
                            <td>
&nbsp;&nbsp;
                                <asp:Button ID="Button1" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_view %>" onclick="Button1_Click"/>
                            </td>
            </tr>
        </table>
        <br />
     
    <br />
    <asp:Label ID="lbl_confirmation" runat="server" ForeColor="Red"></asp:Label><br />
    <asp:Label ID="lbl_unit2" runat="server" ></asp:Label>&nbsp;&nbsp;<asp:Label ID="Label3" runat="server" Text=":" />&nbsp;&nbsp;<asp:Label ID="lbl_unit_door_no" runat="server" Text=""></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="lbl_amount" runat="server" ></asp:Label>&nbsp;&nbsp;<asp:Label ID="Label2" runat="server" Text=":" />&nbsp;&nbsp; <asp:Label ID="lbl_late_fee_amount" runat="server" Text=""></asp:Label>

    <br /><br />
   <asp:GridView ID="gv_rent_payment_archive" runat="server" AutoGenerateColumns="false"
     AllowSorting="true"   Width="83%"  DataKeyNames="rp_id"  >
        
     <Columns>
    <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_home_name %>" DataField="home_name"   />
   <asp:BoundField HeaderText="<%$ Resources:Resource,gv_unit %>" DataField="unit_door_no"   />
   <asp:BoundField HeaderText="<%$ Resources:Resource,gv_rent %>"   DataField="rl_rent_amount" DataFormatString="{0:0.00}"  />
   <asp:BoundField HeaderText="<%$ Resources:Resource,gv_amount_paid %>"   DataField="rp_amount" DataFormatString="{0:0.00}"  />
   <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_due_date %>"  DataField="rp_due_date" DataFormatString="{0:MMM-dd-yyyy}"  
     HtmlEncode="false" />
   <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_date_received %>"  DataField="rp_paid_date" DataFormatString="{0:MMM-dd-yyyy}"  
     HtmlEncode="false" />
      <asp:BoundField DataField="tu_id" HeaderText="tu_id"   />
     
      <asp:TemplateField HeaderText='<%$ Resources:Resource,lbl_late_fees%>' >
    <ItemTemplate  >
    <asp:TextBox  Width="100" MaxLength="10"  runat="server"  ID="gv_tbx_late_fee"   Text='<%# Bind("tt_late_fee","{0:0.00}")%>'/> 
   <asp:HiddenField Visible="false" ID="h_unit_door_no" Value='<%#Bind("unit_door_no")%>'  runat="server" />
    <asp:HiddenField Visible="false" ID="h_rp_id" Value='<%#Bind("rp_id")%>'  runat="server" />
    <asp:HiddenField Visible="false" ID="h_unit_id" Value='<%#Bind("unit_id")%>'  runat="server" />
   </ItemTemplate  >
    </asp:TemplateField>
    
         
    
      <asp:TemplateField >
    <ItemTemplate  >
   <asp:Button ID="btn_paid" runat="server" OnClick="btn_paid_Click"  Text="<%$ Resources:Resource, lbl_paid %>"/>
    </ItemTemplate  >
    </asp:TemplateField>
    
   
    
      <asp:TemplateField >
    <ItemTemplate  >
    <asp:Button  ID="btn_ignore" OnClick="btn_ignore_Click"  runat="server" Text="<%$ Resources:Resource, lbl_ignore %>"/>
    </ItemTemplate  >
    </asp:TemplateField>
    
    
  
   </Columns>   
        
        
       
        
        
    </asp:GridView>
    </div><br /><br />
    
</asp:Content>

