<%@ Page Language="C#" MasterPageFile="../mp_manager.master" AutoEventWireup="true" CodeFile="company_update.aspx.cs" Inherits="company_company_update" Title="Update Company" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <div>
       UPDATE COMPANY<br />
        <br />
        <asp:Label ID="lbl_update_success" runat="server" Text=""></asp:Label>
        <br />
     
        
        <table id="tb_contractor_question" runat="server">
        <tr>
        <td valign="top">Is this contractor is also a vendor/supplier </td>
        <td>
            <asp:RadioButtonList ID="radio_contractor" runat="server" >
                <asp:ListItem  Value="0">no</asp:ListItem>
                <asp:ListItem Value="1">yes</asp:ListItem>
            </asp:RadioButtonList>
            </td>
        </tr>
        </table>
         <br />
         
         
        CATEGORIES
        <br />
        <table  bgcolor="#ffffcc" id="TABLE1" language="javascript" onclick="return TABLE1_onclick()">
            <tr>
                <td   ><asp:CheckBox  Text="general contractor" ID="company_general_contractor" runat="server" />
                    </td>
                <td  >
                    <asp:CheckBox  Text="cleaning" ID="company_cleaning" runat="server" /></td>
                <td >
                    <asp:CheckBox  Text="HVAC" ID="company_hvac" runat="server" /></td>
            </tr>
            <tr>
                <td  >
                    <asp:CheckBox  Text="interior contractor" ID="company_interior_contractor" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="decoration" ID="company_decoration" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="inspection" ID="company_inspection" runat="server" /></td>
            </tr>
            <tr>
                <td  >
                    <asp:CheckBox  Text="exterior contractor" ID="company_exterior_contractor" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="doors & windows" ID="company_doors_windows" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="kitchen" ID="company_kitchen" runat="server" /></td>
            </tr>
            <tr>
                <td  >
                    <asp:CheckBox  Text="alarms & security systems" ID="company_alarms_security_systems" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="electrical" ID="company_electrical" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="locksmith" ID="company_locksmith" runat="server" /></td>
            </tr>
            <tr>
                <td  >
                   <asp:CheckBox  Text="architech" ID="company_architech" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="engineer" ID="company_engineer" runat="server" /></td>
                <td  ><asp:CheckBox  Text="painting" ID="company_painting" runat="server" />
                    </td>
            </tr>
            <tr>
                <td  >
                    <asp:CheckBox  Text="basement" ID="company_basement" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="flooring" ID="company_flooring" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="paving" ID="company_paving" runat="server" /></td>
            </tr>
            <tr>
                <td  >
                    <asp:CheckBox  Text="bricks" ID="company_bricks" runat="server" /></td>
                <td  >
                   <asp:CheckBox  Text="foundation" ID="company_foundation" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="plumbing" ID="company_plumbing" runat="server" /></td>
            </tr>
            <tr>
                <td  >
                    <asp:CheckBox  Text="cable , satellite , dish" ID="company_cable_satellite_dish" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="gardening" ID="company_gardening" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="roofs" ID="company_roofs" runat="server" /></td>
            </tr>
            <tr>
                <td  >
                    <asp:CheckBox  Text="ciment" ID="company_ciment" runat="server" /></td>
                <td  >
                    <asp:CheckBox   Text="gypse installation" ID="company_gypse_installation" runat="server" /></td>
                <td  >
                    <asp:CheckBox  Text="other" ID="company_other" runat="server" /></td>
            </tr>
        </table><br />
        COMPANY
        <br />
        <table bgcolor="#ffffcc" width="65%" >
            <tr>
                <td>
                    name</td>
                <td >
                    : 
                    <asp:TextBox ID="company_name" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator 
                        ID="reg_company_name" runat="server" 
                         ControlToValidate="company_name"
                        ErrorMessage="invalid name">
                        </asp:RegularExpressionValidator>
                &nbsp;<asp:RequiredFieldValidator  ControlToValidate="company_name"
                           ID="req_company_name" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    website</td>
                <td >
                    : 
                    <asp:TextBox ID="company_website" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_company_website" runat="server" 
                         ControlToValidate="company_website"
                        ErrorMessage="invalid link">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            
            <tr>
                <td  >
                                        address</td>
                <td >
                    :
                    <asp:TextBox ID="company_addr_street" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_company_addr_street" runat="server" 
                        ControlToValidate="company_addr_street"
                        ErrorMessage="invalid address">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            
            <tr>
                <td >
                    prov/state</td>
                <td >
                    :
                    <asp:TextBox ID="company_prov" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_company_prov" runat="server" 
                        ControlToValidate="company_prov"
                        ErrorMessage="enter letters only ">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    city</td>
                <td>
                    :
                    <asp:TextBox ID="company_city" runat="server"></asp:TextBox>
                &nbsp;<asp:RegularExpressionValidator ID="reg_company_city" runat="server" 
                        ControlToValidate="company_city"
                        ErrorMessage="enter letters only">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td >
                    postal code</td>
                <td >
                    :
                    <asp:TextBox ID="company_pc" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_company_pc" runat="server" 
                        ControlToValidate="company_pc"
                        ErrorMessage="invalid postal code">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td >
                    telephone</td>
                <td >
                    :
                    <asp:TextBox ID="company_tel" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator 
                        ID="reg_company_tel" runat="server" 
                        ControlToValidate="company_tel"
                        ErrorMessage="invalid telephone">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td  >
                    contact first name</td>
                <td >
                    :
                    <asp:TextBox ID="company_contact_fname" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_company_contact_fname" runat="server" 
                        ControlToValidate="company_contact_fname"
                        ErrorMessage="invalid name">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td >
                    contact last name</td>
                <td  >
                    :
                    <asp:TextBox ID="company_contact_lname" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_company_contact_lname" runat="server" 
                        ControlToValidate="company_contact_lname"
                        ErrorMessage="invalid last name">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td >
                    contact tel.</td>
                <td >
                    :
                    <asp:TextBox ID="company_contact_tel" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_company_contact_tel" 
                        ControlToValidate="company_contact_tel"
                        runat="server" ErrorMessage="invalid telephone">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
       
            <tr>
                <td >
                    contact email</td>
                <td >
                    :
                    <asp:TextBox ID="company_contact_email" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_company_contact_email" 
                        ControlToValidate="company_contact_email"
                        runat="server" ErrorMessage="invalid email">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
       
        </table>
        <br />
        <asp:Button ID="btn_update" runat="server" Text="update"  OnClick="btn_update_Click"/></div>
    </asp:Content>