using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.AccountObjectAuthorization;
using log4net;
using Owasp.Esapi;

public partial class company_company_view : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!RegEx.IsInteger(Request.QueryString["company_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

      
        ////////
        //////// SECURITY CHECK BEGIN //////////////////
        ////////
        AccountObjectAuthorization companyAuthorization = new AccountObjectAuthorization(strconn);

        if (!companyAuthorization.Company(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["company_id"])))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ////////
        //////// SECURITY CHECK END //////////////////
        ////////


        company_add_link.NavigateUrl = "company_add.aspx?company_id=" + Request.QueryString["company_id"];
        company_update_link.NavigateUrl = "company_update.aspx?company_id=" + Request.QueryString["company_id"];


        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prCompanyCategView", conn);
        SqlCommand cmd2 = new SqlCommand("prCompanyView", conn);
        
        cmd.CommandType = CommandType.StoredProcedure;
        cmd2.CommandType = CommandType.StoredProcedure;

        //Add the params
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@company_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["company_id"]);

        try
        {
            conn.Open();

            SqlDataReader dr = null;
            dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

            while (dr.Read() == true)
            {
                   
              if (Convert.ToInt32(dr["company_general_contractor"]) == 1 )
              company_general_contractor.Checked = true;
               
              if (Convert.ToInt32(dr["company_interior_contractor"]) == 1 )
              company_interior_contractor.Checked = true;

               if (Convert.ToInt32(dr["company_cleaning"]) == 1 )
              company_cleaning.Checked = true;

              if (Convert.ToInt32(dr["company_decoration"]) == 1 )
              company_decoration.Checked = true;

              if (Convert.ToInt32(dr["company_gypse_installation"]) == 1 )
              company_gypse_installation.Checked = true;

              if (Convert.ToInt32(dr["company_painting"]) == 1 )
              company_painting.Checked = true;

              if (Convert.ToInt32(dr["company_plumbing"]) == 1 )
              company_plumbing.Checked = true;

              if (Convert.ToInt32(dr["company_basement"]) == 1 )
              company_basement.Checked = true;

             if (Convert.ToInt32(dr["company_exterior_contractor"]) == 1 )
              company_exterior_contractor.Checked = true;

              if (Convert.ToInt32(dr["company_inspection"]) ==  1 )
              company_inspection.Checked = true;

              if (Convert.ToInt32(dr["company_hvac"]) == 1 )
              company_hvac.Checked = true;

              if (Convert.ToInt32(dr["company_kitchen"]) == 1 )
              company_kitchen.Checked = true;

              if (Convert.ToInt32(dr["company_electrical"]) == 1 )
              company_electrical.Checked = true;

              if (Convert.ToInt32(dr["company_doors_windows"]) == 1 )
              company_doors_windows.Checked = true;

              if (Convert.ToInt32(dr["company_alarms_security_systems"]) == 1 )
              company_alarms_security_systems.Checked = true;

              if (Convert.ToInt32(dr["company_paving"]) == 1 )
              company_paving.Checked = true;

              if (Convert.ToInt32(dr["company_flooring"]) == 1 )
              company_flooring.Checked = true;

              if (Convert.ToInt32(dr["company_roofs"]) == 1 )
              company_roofs.Checked = true;

              if (Convert.ToInt32(dr["company_gardening"]) == 1 )
              company_gardening.Checked = true;

              if (Convert.ToInt32(dr["company_locksmith"]) == 1 )
              company_locksmith.Checked = true;

              if (Convert.ToInt32(dr["company_architech"]) == 1 )
              company_architech.Checked = true;

              if (Convert.ToInt32(dr["company_engineer"]) == 1 )
              company_engineer.Checked = true;

              if (Convert.ToInt32(dr["company_bricks"]) == 1 )
              company_bricks.Checked = true;

              if (Convert.ToInt32(dr["company_foundation"]) == 1 )
              company_foundation.Checked = true;

              if (Convert.ToInt32(dr["company_cable_satellite_dish"]) == 1 )
              company_cable_satellite_dish.Checked = true;

              if (Convert.ToInt32(dr["company_other"]) ==  1 )
              company_other.Checked = true;

              if (Convert.ToInt32(dr["company_ciment"]) == 1 )
              company_ciment.Checked = true;


            }

        }
            
        finally
        {
          //  conn.Close();
        }


        //Add the params
        cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd2.Parameters.Add("@company_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["company_id"]);
        try
        {
         //   conn.Open();

            SqlDataReader dr = null;
            dr = cmd2.ExecuteReader(CommandBehavior.SingleRow);

            while (dr.Read() == true)
            {
                company_name.Text = dr["company_name"].ToString();
                company_website.Text = dr["company_website"].ToString();
               // company_addr_no.Text = dr["company_addr_no"].ToString();
                company_addr_street.Text = dr["company_addr_street"].ToString();
                company_city.Text = dr["company_city"].ToString();
                company_prov.Text = dr["company_prov"].ToString();
                company_pc.Text = dr["company_pc"].ToString();
                company_tel.Text = dr["company_tel"].ToString();
                company_contact_fname.Text = dr["company_contact_fname"].ToString();
                company_contact_lname.Text = dr["company_contact_lname"].ToString();
                company_contact_tel.Text = dr["company_contact_tel"].ToString();
                company_contact_email.Text = dr["company_contact_email"].ToString();

                if (dr["company_type"].ToString() == "b")
                    radio_contractor.SelectedValue = "1";
                else
                    radio_contractor.SelectedValue = "0";

            }

        }

        finally
        {
            conn.Close();
        }
 
     }

}
