using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
/// <summary>
/// Done by : Stanley Jocelyn
/// date    : sept 7, 2007
/// </summary>

public partial class supplier_supplier_list : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        tiger.Supplier h = new tiger.Supplier(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        dgsupplier_list.DataSource = h.getSupplierList(Convert.ToInt32(Session["schema_id"]));
        dgsupplier_list.DataBind();
        
    }
}
