using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.AccountObjectAuthorization;

public partial class supplier_supplier_view : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        ////////
        //////// SECURITY CHECK BEGIN //////////////////
        ////////
        AccountObjectAuthorization companyAuthorization = new AccountObjectAuthorization(strconn);

        if (!companyAuthorization.Company(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["company_id"])))
        {
            Session.Abandon();
            Response.Redirect("http://www.sinfocatiger.com/login.aspx");
        }
        ////////
        //////// SECURITY CHECK END //////////////////
        ////////

        supplier_add_link.NavigateUrl= "supplier_add.aspx?company_id=" + Request.QueryString["company_id"];
        supplier_update_link.NavigateUrl = "supplier_update.aspx?company_id=" + Request.QueryString["company_id"];
       
        tiger.Company h = new tiger.Company(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        rsupplierView.DataSource = h.getCompanyView(Convert.ToInt32(Session["schema_id"]),Convert.ToInt32(Request.QueryString["company_id"]));
        rsupplierView.DataBind();

    }

}
