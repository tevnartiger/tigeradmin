<%@ Page Language="C#" AutoEventWireup="true" Inherits="OboutInc.Scheduler.Templates.Settings" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link rel="Stylesheet" media="all" href="res/Settings.css" />
</head>
<body>

    <div>
    <table style="width:100%;background-color:#C6DBFF;">
        <tr>
            <td valign="top">
            <input id="btnBack" class="btnBack" type="button" value="Back" />
            <input id="btnSave" class="btnSave" type="button" value="Save Settings" />
            <input id="btnCancel" class="btnCancel" type="button" value="Cancel" />
            </td>
        </tr>
    </table>
    <div id="divContent" style="width:100%;overflow:auto;">
    <table style="width:100%;" cellpadding="0" cellspacing="0">
        <tr>
            <td style="width:5px;height:5px;"></td>
            <td></td>            
            <td style="width:5px;height:5px;"></td>            
        </tr>
        <tr>
            <td></td>
            <td>
                <table style="width:100%;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                             <table style="width:100%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td style="width:1px;"><img src="res/SettingsTopLeft.gif" /></td>
                                    <td class="tdBackground" style="width:*;"></td>
                                    <td style="width:1px;" align="right"><img src="res/SettingsTopRight.gif" /></td>
                                </tr>
                                <tr>
                                    <td class="tdBackground"></td>
                                    <td class="tdBackground"><b>Scheduler Settings</b></td>
                                    <td class="tdBackground"></td>                                    
                                </tr>
                                <tr>
                                    <td class="tdBackground"></td>
                                    <td class="tdBackground"  style="padding-left:0px;">
                                        <table cellpadding="0" cellspacing="1">
                                            <tr>
                                                <td id="tabGeneral" class="MenuSelected">General</td>
                                                <td id="tabCategory" class="MenuUnselected">Categories</td>
                                            </tr>
                                        </table>
                                                                                
                                    </td>
                                    <td class="tdBackground"></td>                                    
                                </tr>
                                
                             </table>

                        
                        
                        </td>
                    </tr>
                    <tr>
                        <td class="tdInnerBackground" style="padding-left:5px;padding-right:5px;padding-top:10px;padding-bottom:10px;">
                            <div id="divCategory"  style="display:none;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">                                
                                    <tr>
                                        <td class="tdHeader"><b>category</b></td>
                                        <td class="tdHeader tdHideShow"><b>show</b></td>
                                        <td class="tdHeader tdTheme"><b>theme</b></td>
                                        <td class="tdHeader tdDelete"><b>delete</b></td>
                                    </tr>
                                    <tr><td colspan="4" class="tdBackground" style="height:2px;"></td></tr>                                
                                </table>
                                
                                
                                <table id="tableCategories" style="width:100%;" cellpadding="0" cellspacing="0">                                
                                </table>
                                <br />
                                <span style="font-size:10pt;font-weight:bold;">Create a new category:</span>
                                <br />
                                <input id="txtNewCategory" type="text" style="width:200px" />
                                <input id="btnCreate" type="button" value="Create" />
                                <input id="txtCatName" type="text" class="txtCatName" style="display:none;"/>                                
                                
                            </div>                        
                            <div id="divGeneral" style="display:block;">
                                <table style="width:100%;" cellpadding="0" cellspacing="0">
                                    
                                    <tr style="display:none;">
                                        <td class="tdOption" style="width:30%;">
                                            <b>Language:</b>
                                        </td>
                                        <td class="tdOption">
                                            <select id="ddLanguage" class="ddLanguage">
                                                <option value="us-en">English-US</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr  style="display:none;"><td colspan="2" class="tdBackground" style="height:2px;"></td></tr>

                                    <tr  style="display:none;">
                                        <td class="tdOption" style="width:30%;">
                                            <b>Timezone:</b>
                                        </td>
                                        <td class="tdOption">
                                            <select id="ddTimezone" class="ddTimezone">
                                                <option value="7">(GMT +07:00)</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr  style="display:none;"><td colspan="2" class="tdBackground" style="height:2px;"></td></tr>
                                    
                                    <tr>
                                        <td class="tdOption">
                                            <b>Date format:</b>
                                        </td>
                                        <td class="tdOption">
                                            <select id="ddDateFormat" class="ddDateFormat">
                                                <option value="dd/mm/yyyy">31/12/2008</option>
                                                <option value="mm/dd/yyyy">12/31/2008</option>
                                                <option value="yyyy-mm-dd">2008-12-31</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr><td colspan="2" class="tdBackground" style="height:2px;"></td></tr>
                                    

                                    <tr>
                                        <td class="tdOption">
                                            <b>Time format:</b>
                                        </td>
                                        <td class="tdOption">
                                            <select id="ddTimeFormat" class="ddTimeFormat">
                                                <option value="TwelveHours">1:00am</option>
                                                <option value="TwentyFourHours">13:00</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr><td colspan="2" class="tdBackground" style="height:2px;"></td></tr>

                                    <tr>
                                        <td class="tdOption">
                                            <b>Week start on:</b>
                                        </td>
                                        <td class="tdOption">
                                            <select id="ddWeekStart" class="ddWeekStart">
                                                <option value="0">Sunday</option>
                                                <option value="1">Monday</option>
                                                <option value="6">Saturday</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr><td colspan="2" class="tdBackground" style="height:2px;"></td></tr>
                                
                                    <tr>
                                        <td class="tdOption">
                                            <b>Show weekend:</b>
                                        </td>
                                        <td class="tdOption">
                                            <select id="ddShowWeekend" class="ddShowWeekend">
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr><td colspan="2" class="tdBackground" style="height:2px;"></td></tr>



                                    <tr>
                                        <td class="tdOption">
                                            <b>Default view:</b>
                                        </td>
                                        <td class="tdOption">
                                            <select id="ddDefaultView" class="ddDefaultView">
                                                <option value="0">Day</option>
                                                <option value="1">Week</option>
                                                <option value="2">Month</option>
                                                <option value="3">Custom View</option>
                                                <option value="5">Settings</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr><td colspan="2" class="tdBackground" style="height:2px;"></td></tr>

                                    <tr>
                                        <td class="tdOption">
                                            <b>Custom view:</b>
                                        </td>
                                        <td class="tdOption">
                                            <select id="ddCustomViewDayNumber" class="ddCustomViewDayNumber">
                                                <option value="2">Next 2 Days</option>
                                                <option value="3">Next 3 Days</option>
                                                <option value="4">Next 4 Days</option>
                                                <option value="5">Next 5 Days</option>
                                                <option value="6">Next 6 Days</option>                                            
                                                <option value="7">Next 7 Days</option>
                                            </select>
                                        </td>
                                    </tr>                            
                                </table>
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td>
                             <table style="width:100%;" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="tdBackground" style="height:20px;"></td>
                                    <td class="tdBackground">
                                    &nbsp;
                                    </td>
                                    <td class="tdBackground"></td>
                                </tr>
                                <tr>
                                    <td style="width:1px;"><img src="res/SettingsBottomLeft.gif" /></td>
                                    <td class="tdBackground"></td>
                                    <td style="width:1px;" align="right"><img src="res/SettingsBottomRight.gif" /></td>
                                </tr>
                             </table>
                        
                        </td>
                    </tr>
                    
                </table>
            </td>
            <td></td>
        </tr>        
    </table>                
    <br />
    </div>
    
    <div style="display:none ">
        <select id="selTheme" style="width:100%;">
            <option value="default">Default</option>        
            <option value="blue">Blue</option>
            <option value="orange">Orange</option> 
            <option value="green">Green</option>                                                            
            <option value="violet">Violet</option>                                                            

        </select>
    </div>
    
    </div>
    <%=EmbededScript%>    
</body>
</html>
