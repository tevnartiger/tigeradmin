<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="cs_scheduler.master" %>
<%@ Register TagPrefix="osd" Namespace="OboutInc.Scheduler" Assembly="obout_Scheduler_NET"%>

    <script runat="server">
        Scheduler scheduler;
        void Page_Load(Object o, EventArgs e)
        {
            Page.Title = "Scheduler Basic Example";
            scheduler.Login(1);            
        }
        void Page_Init(Object o, EventArgs e)
        {
            scheduler = new Scheduler();
            scheduler.ID = "scheduler";
            scheduler.ProviderName = "System.Data.OleDb";
            scheduler.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("../App_Data/scheduler.mdb");
            scheduler.EventsTableName = "Events";
            scheduler.UserSettingsTableName = "UserSettings";
            scheduler.CategoriesTableName = "Categories";
            scheduler.Width = "990";
            scheduler.Height = "500";
            scheduler.StyleFolder = "styles/default";                
            pnContent.Controls.Add(scheduler);
        }
    </script>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
<asp:Panel ID="pnContent" runat="server"></asp:Panel>
</asp:Content>            