﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class manager_Scheduler_Default3 : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            // create schedule user settings if it doesn't exist
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prOboutScheduleUserSettingsAdd", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();

            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@lang", SqlDbType.NVarChar, 20).Value = Session["_lastCulture"].ToString();

            cmd.ExecuteNonQuery();

            conn.Close();

            schedule.Login(Convert.ToInt32(Session["schema_id"]));

        }



        schedule.Login(Convert.ToInt32(Session["schema_id"]));
    }
}
