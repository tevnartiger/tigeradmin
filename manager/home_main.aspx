<%@ Page Language="C#" MasterPageFile="../mp_manager.master" AutoEventWireup="true" CodeFile="home_main.aspx.cs" Inherits="home_home_main"   Title="Prospective tenant view" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div>
    <asp:Label ID="lbl_success" runat="server" />
        <br />
        <br />
        <asp:Calendar ID="Calendar1" runat="server" SelectedDayStyle-BackColor="Red">
        </asp:Calendar>
        <span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA"> 
        <br />
        <asp:HyperLink ID="HyperLink32" runat="server" 
            NavigateUrl="~/home/home_main.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Home 2</span></asp:HyperLink>
        <br />
        <br />
        <asp:HyperLink ID="HyperLink13" runat="server" 
            NavigateUrl="~/manager/property/property_add.aspx?h_id=0">PROPERTY ADD</asp:HyperLink>
        &nbsp;&nbsp;
        <asp:HyperLink ID="HyperLink36" runat="server" 
            NavigateUrl="~/manager/warehouse/warehouse_add.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">ADD A WAREHOUSE 
        (ENTREP�T )</span></asp:HyperLink>
        &nbsp;
        <asp:HyperLink ID="HyperLink37" runat="server" 
            NavigateUrl="~/manager/warehouse/warehouse_list.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Warehouse list</span></asp:HyperLink>
        &nbsp;
        <asp:HyperLink ID="HyperLink38" runat="server" 
            NavigateUrl="~/manager/appliance/appliance_moving.aspx">APPLIANCE MOVE</asp:HyperLink>
        &nbsp;
        <br />
        <br />
        <asp:HyperLink ID="HyperLink39" runat="server" 
            NavigateUrl="~/manager/appliance/appliance_list.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Appliance list</span></asp:HyperLink>
        <br />
        </span><br />
        <asp:HyperLink ID="HyperLink16" runat="server" 
            NavigateUrl="~/mortgage/mortgage_amortization.aspx">Amortization</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:HyperLink ID="HyperLink34" runat="server" 
            NavigateUrl="~/manager/property/property_unit_list.aspx">Update Unit</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:HyperLink ID="HyperLink35" runat="server" 
            NavigateUrl="~/manager/tenant/tenant_search.aspx?h=0&l=a">Tenant Search</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;
        <br />
        <br />
        <asp:HyperLink ID="HyperLink19" runat="server"
        NavigateUrl="~/manager/Financial/financial_income.aspx" >Income</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:HyperLink ID="HyperLink17" runat="server" 
            NavigateUrl="~/manager/Financial/financial_expenses.aspx">Expenses</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <asp:HyperLink ID="HyperLink20" runat="server" 
            NavigateUrl="~/manager/income/income_late_rent_fees.aspx">Late rent fees 
        payments</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;
        <asp:HyperLink ID="HyperLink21" runat="server" 
            NavigateUrl="~/manager/income/income_received_late_rent_fees.aspx">Late rent 
        payment fees received</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:HyperLink ID="HyperLink22" runat="server" 
            NavigateUrl="~/manager/tenant/tenant_cancel_rent_payment.aspx">Cancel Rent 
        Payment</asp:HyperLink>
        <br />
        <br />
        <asp:HyperLink ID="HyperLink24" runat="server" 
            NavigateUrl="~/manager/Financial/financial_analysis.aspx">Financial analysis</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:HyperLink ID="HyperLink25" runat="server" 
            NavigateUrl="~/manager/Financial/financial_analysis_period.aspx">Financial 
        analysis for a period</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
&nbsp;<asp:HyperLink ID="HyperLink26" runat="server" 
            NavigateUrl="~/manager/Financial/financial_analysis_period_list.aspx">List of Financial 
        analysis for a period (views ,update,delete )</asp:HyperLink>
    
        <br />
    
        <br />
        <asp:HyperLink ID="HyperLink27" runat="server" 
            NavigateUrl="~/manager/Financial/financial_income_graph.aspx">Income Graph</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:HyperLink ID="HyperLink28" runat="server" 
            NavigateUrl="~/manager/Financial/financial_expenses_graph.aspx">Expenses Graph</asp:HyperLink>
        <br />
        <br />
        <asp:HyperLink ID="HyperLink29" runat="server" 
            NavigateUrl="~/manager/Financial/financial_analysis_period_expense_graph.aspx">Financial Analysis Expense Graph for a period</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:HyperLink ID="HyperLink30" runat="server" 
            NavigateUrl="~/manager/Financial/financial_analysis_period_income_graph.aspx">Financial Analysis Income Graph for a period</asp:HyperLink>
        
        <br />
        
        <br />
        <asp:HyperLink ID="HyperLink31" runat="server" 
            NavigateUrl="~/manager/lease/lease_list.aspx">Lease List</asp:HyperLink>
        <br />
        <br />
        <asp:HyperLink ID="HyperLink33" runat="server" 
            NavigateUrl="~/manager/lease/lease_archive_list.aspx">Lease Archive List</asp:HyperLink>
        <br />
            <br />
       
        <strong>
        <br />
            <br />
           
            <br />
        </strong>
        <br />
        <br />
        * page
         temporaire</div>
 </asp:Content>
