﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="reports_default.aspx.cs" Inherits="manager_reports_reports_default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<STYLE type="text/css">
 
A:link {color:#8a2528 ; text-decoration:none}
A:visited {color:#6e6042 ; text-decoration:none}
A:active {color:#996600 ; text-decoration:underline}
A:hover {color:#996600 ; text-decoration:underline}
A:link {color:#8a2528 ; text-decoration:none}

#nav a:link {display:block;color:#000066 ; text-decoration:none}
#nav a:visited {display:block;color:#444f51 ; text-decoration:none}
#nav a:active {display:block;color:#996600 ; text-decoration:none}
#nav a:hover {display:block;color:#996600 ; text-decoration:none}
#nav a:link {display:block;color:#000066 ; text-decoration:none}

#rcLink a:link {display:block;color:#ffffff ; text-decoration:none}
#rcLink a:visited {display:block;color:#ffffff ; text-decoration:none}
#rcLink a:active {display:block;color:#ffffff ; text-decoration:underline}
#rcLink a:hover {display:block;color:#ffffff ; text-decoration:underline}
#rcLink a:link {display:block;color:#ffffff ; text-decoration:none}
		
.statusLink{color:#00457c; text-decoration:underline}

 .iixred   {font-style: normal; 
 		font-variant: normal; 
 		font-weight: normal; 
 		font-size: 8pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: red} 
		
 .iixredbi   {font-style: italic; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 8pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: red} 
 
 .red   {font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 10pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: red} 
		
 .xredb   {font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 10pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: red} 
		
 .xiiredb   {font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 12pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: red} 
		
 .xred   {font-style: normal; 
 		font-variant: normal; 
 		font-weight: normal; 
 		font-size: 10pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: red} 
		
 .xivredb   {font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 14pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: red} 
		
 .xivblueb   {font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 14pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #000066} 
		
		
		
.white  {font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 12pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #ffffff}
		
.xiwhiteb  {font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 11pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #ffffff}
		
.xwhiteb {font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 10pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #ffffff}
		
.ixwhite {font-style: normal; 
 		font-variant: normal; 
 		font-weight: normal; 
 		font-size: 9pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #ffffff}
		
.ixwhiteb {font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 9pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #ffffff}
		
		
		
.wcopyright {font-family: Arial, Helvetica, sans-serif;
		 font-size: 8pt;
		color: #ffffff}

.uwcopyright {font-family: Arial, Helvetica, sans-serif;
		 font-size: 7pt;
		color: #ffffff}
		
.wbcopyright {font-family: Arial, Helvetica, sans-serif;
		 font-size: 8pt;
		 font-weight: bold; 
		color: #ffffff}
		 
.gcopyright {font-family: Arial, Helvetica, sans-serif;
		 font-size: 8pt;
 		color: #666666}

		
.udcopyright {font-family: Arial, Helvetica, sans-serif;
		 font-size: 7pt;
 		color: #999999}
		
.ubcopyright {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 7pt;
	color: #000066;}
		 
.copyright {font-family: Arial, Helvetica, sans-serif;
		 font-size: 8pt}
		 
.copyrightb {font-family: Arial, Helvetica, sans-serif;
		 font-size: 8pt;
		  font-weight: bold;}
		 
.ixblueb {font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 9pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #000066}
		
.xiiblueb {font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 12pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #000066}
		
		
.rrxiredb {font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 11pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #990100}
		
.rrxiiredb {font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 12pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #990100}
		
.rrxiiiredb {font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 13pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #990100}
		
.xiblueb {  font-family: Arial, Helvetica, sans-serif; font-size: 11pt; font-weight: bold; color: #000066}

.xblueb {  font-family: Arial, Helvetica, sans-serif; font-size: 10pt; font-weight: bold; color: #000066}

.xblue {  font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000066}

.xitipb {  font-family: Arial, Helvetica, sans-serif; font-size: 11pt; font-weight: bold; color: #333399}

.xtipb {  font-family: Arial, Helvetica, sans-serif; font-size: 10pt; font-weight: bold; color: #333399}

.xtip {  font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #333399}
 
.distextbox {border-right: #999999 1px solid; border-top: #999999 1px solid; border-left: #999999 1px solid; border-bottom: #999999 1px solid; background-color: #dddddd }

.disradio {background-color: #dddddd}

.searchbox {
			font-size:8pt;
			border-right: #000000 1px solid; 
            border-top: #000000 3px solid; 
            border-left: #000000 1px solid; 
            border-bottom: #000000 1px solid;}
		 
.textbox   {border-right: #999999 1px solid; 
            border-top: #999999 1px solid; 
            border-left: #999999 1px solid; 
            border-bottom: #999999 1px solid}	
			

			
.textboxbl   {border-right: #000066 1px solid; 
            border-top: #000066 1px solid; 
            border-left: #000066 1px solid; 
            border-bottom: #000066 1px solid}

.textboxgr   {border-right: #dddddd 1px solid; 
            border-top: #dddddd 1px solid; 
            border-left: #dddddd 1px solid; 
            border-bottom: #dddddd 1px solid}
		


.ixblack {font-style: normal; 
 		font-variant: normal; 
 		font-weight: normal; 
 		font-size: 9pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #000000} 
		
.ixblackb {font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 9pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #000000} 

.xblack{font-style: normal; 
 		font-variant: normal; 
 		font-weight: normal; 
 		font-size: 10pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #000000} 
		
.xblackb{font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 10pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #000000}
		
.xiblack{font-style: normal; 
 		font-variant: normal; 
 		font-weight: normal; 
 		font-size: 11pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #000000}
		
.xiblackb {font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 11pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #000000}
		
.xiiblack {font-style: normal; 
 		font-variant: normal; 
 		font-weight: normal; 
 		font-size: 12pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #000000}
		
.xiiblackb {font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 12pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #000000}

		
.xiiiblack {font-style: normal; 
 		font-variant: normal; 
 		font-weight: normal; 
 		font-size: 13pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #000000}
		
.xiiiblackb{font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 13pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #000000}
		
		
		
.xgray {font-style: normal; 
 		font-variant: normal; 
 		font-weight: normal; 
 		font-size: 10pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #666666}
.xgrayb {font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 10pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #666666}
.ixgray {font-style: normal; 
 		font-variant: normal; 
 		font-weight: normal; 
 		font-size: 9pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #666666} 
		
.ixgrayb {font-style: normal; 
 		font-variant: normal; 
 		font-weight: bold; 
 		font-size: 9pt;
 		font-family: arial, helvetica, sans-serif; 
 		color: #666666} 


.horizontalcssmenu ul{
margin: 0;
padding: 0;
list-style-type: none;
z-index: 8;
}

/*Top level list items*/
.horizontalcssmenu ul li{
position: relative;
display: inline;
float: left;
z-index: 8;
}

/*Top level menu link items style*/
.horizontalcssmenu ul li a{
display: block;
width: 120px; /*Width of top level menu link items*/
padding: 2px 8px;

border-left-width: 0;
text-decoration: none;

color: black;
font: bold 13px Arial #ffffff;
z-index: 8;

}
	
/*Sub level menu*/
.horizontalcssmenu ul li ul{
left: 0;
top: 0;
border-top: 1px solid #202020;
position: absolute;
display: block;
visibility: hidden;
z-index: 100;

}

/*Sub level menu list items*/
.horizontalcssmenu ul li ul li{
display: inline;
float: none;
z-index: 18;
}


/* Sub level menu links style */
.horizontalcssmenu ul li ul li a{
width: 160px; /*width of sub menu levels*/
position: relative;

font-weight: normal;
padding: 2px 5px;
background: #d3cbb4;
border-width: 0 1px 1px 1px;
z-index: 8;
}

.horizontalcssmenu ul li a:hover{
background: url(menubgover.gif) center center repeat-x;
font-weight: bold;

}

.horizontalcssmenu ul li ul li a:hover{
background: #e5e1d7;
}

.horizontalcssmenu .arrowdiv{
position: absolute;
right: 0;
background: red center left;
}



.bottom{
z-index: -15;
}
	
/* Holly Hack for IE \*/
* html .horizontalcssmenu ul li { float: left; height: 1%; }
* html .horizontalcssmenu ul li a { height: 1%; }
/* End */



 </STYLE>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<div class="ixblack" align="center">

<table border="1" cellpadding="8" cellspacing="1" width="100%" class="ixblack">

            <tr>
              <td class="ixblack" colspan="2">
                <p align="center"><b>RentRight Report List</b></p>
                <p align="left">You choose how broad or narrow you want your
                reports to be. Most reports can be run for a unit, building, one
                owner or complex, or for all properties combined. You can also
                run reports for <strong>any date range</strong> you choose. Then
                choose if you want the report with or without details.&nbsp;
                Because of this flexibility, RentRight has unlimited ways to
                report your management information.</p>
                <p align="left">Below are just a few of the possibilities.
                Please call or <a href="mailto:sales@rent-right.com">e mail us</a>

                if a report you need is not listed, so we can send or fax you
                samples.</td>
            </tr>
            <tr>
              <td width="50%">
                <table border="0" width="100%" class="ixblack">
                  <tr>
                    <td colspan="2" bgcolor="#dddddd"><b>Income Reports</b><br></td>
                  </tr>

                  <tr>
                    <td colspan="2">Income Categories</td>
                  </tr>
                  <tr>
                    <td width="52"></td>
                    <td>x<a href="Reportimages/inchisbl.htm">By Building</a></td>
                  </tr>
                  <tr>

                    <td width="50"></td>
                    <td>By Unit</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>xBy All Properties</td>
                  </tr>
                  <tr>

                    <td></td>
                    <td><a href="Reportimages/inchisow.htm">By Owner</a></td>
                  </tr>
                  <tr>
                    <td colspan="2">Net Income</td>
                  </tr>
                  <tr>
                    <td></td>

                    <td>x<a href="Reportimages/netincbl.htm">By Building</a></td>
                  </tr>
                  <tr>
                    <td></td>

                    <td>x<a href="Reportimages/netincal.htm">By All Properties</a></td>
                  </tr>
                  <tr>
                    <td colspan="2">Net Income Ledger</td>

                  </tr>
                  <tr>
                    <td colspan="2">x<a href="../renthistory.htm">Rent History</a></td>
                  </tr>
                  <tr>
                    <td colspan="2"><a href="Reportimages/bank.htm">Bank Deposit
                      Report</a></td>
                  </tr>
                  <tr>

                    <td colspan="2"><a href="Reportimages/summten.htm">Summary
                      Income By Tenant</a></td>
                  </tr>
                  <tr>
                    <td colspan="2">x<a href="Reportimages/summbldg.htm">Summary
                      Income By Building</a></td>
                  </tr>
                  <tr>
                    <td colspan="2">xProfit/Loss Statement</td>

                  </tr>
                  <tr>
                    <td></td>
                    <td>x<a href="Reportimages/profloss.htm">By Building</a></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>By Unit</td>

                  </tr>
                  <tr>
                    <td></td>
                    <td>x<a href="../sales/plallprop.htm">By All Properties</a></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>&nbsp;</td>

                  </tr>
                  <tr>
                    <th align="left" colspan="2"><b>Expense Reports</b></th>
                  </tr>
                  <tr>
                    <td colspan="2">Expenses</td>
                  </tr>
                  <tr>

                    <td></td>
                    <td>x<a href="Reportimages/expenses.htm">By Building</a></td>
                  </tr>
                  <tr>

                    <td></td>
                    <td>x<a href="../sales/expallprop.htm">By All Properties</a></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td><a href="Reportimages/expbyown.htm">By Owner</a></td>
                  </tr>
                  <tr>

                    <td colspan="2">xExpenses By Category</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>x<a href="Reportimages/expcatbl.htm">By Building</a></td>
                  </tr>
                  <tr>
                    <td></td>

                    <td>By Unit</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>x<a href="Reportimages/expcatal.htm">By All Properties</a></td>
                  </tr>
                  <tr>
                    <td></td>

                    <td><a href="Reportimages/exptotow.htm">By Owner</a></td>
                  </tr>
                  <tr>
                    <td colspan="2">Depreciating Expenses</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>By Building</td>

                  </tr>
                  <tr>
                    <td></td>
                    <td>By Unit</td>
                  </tr>
                  <tr>
                    <td></td>
                    <td><a href="Reportimages/depexblg.htm">By All Properties</a></td>

                  </tr>
                  <tr>
                    <td></td>
                    <td><a href="Reportimages/depexpow.htm">By Owner</a></td>
                  </tr>
                  <tr>
                    <td colspan="2"><a href="../sales/CheckRegister.htm">Check
                      Register</a></td>
                  </tr>

                  <tr>
                    <td colspan="2"><b>Invoices and Receipts</b></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>x<a href="Reportimages/laterent.htm">Late Rent Invoice</a></td>
                  </tr>
                  <tr>

                    <td></td>
                    <td><a href="Reportimages/nexmoinv.htm">Next Month Invoice</a></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td>Rent Receipt</td>
                  </tr>
                  <tr>

                    <td></td>
                    <td><a href="../sales/SecDepReceipt.htm">Security Deposit
                      Receipt</a></td>
                  </tr>
                  <tr>
                    <td></td>
                    <td><a href="../secstatement.htm">Security Deposit Statement</a></td>
                  </tr>
                  <tr>

                    <td></td>
                    <td>x<a href="Reportimages/tenstate.htm">Tenant Statement</a></td>
                  </tr>
                </table>


              </td>
              <td valign="top" width="50%">
			  
                
                  <table border="0" width="100%" class="ixblack">

                    <tr>
                      <td colspan="2" bgcolor="#dddddd"><b>Building Reports</b><br></td>
                    </tr>
                    <tr>
                      <td colspan="2">Rent Roll</td>
                    </tr>
                    <tr>
                      <td width="30"></td>

                      <td>xBy Unit</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>x<a href="Reportimages/rrolbybl.htm">By Building</a></td>
                    </tr>
                    <tr>
                      <td></td>

                      <td>x<a href="Reportimages/rollbyal.htm">By All Properties</a></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td><a href="Reportimages/rrolbyow.htm">By Owner</a></td>
                    </tr>
                    <tr>
                      <td colspan="2">xBuilding List</td>

                    </tr>
                    <tr>
                      <td colspan="2"><a href="Reportimages/bldginfo.htm">Building
                        Information Report</a></td>
                    </tr>
                    <tr>
                      <td colspan="2">To Do List</td>
                    </tr>
                    <tr>

                      <td></td>
                      <td>xBy Building</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>xBy Unit</td>
                    </tr>
                    <tr>

                      <td colspan="2">x<a href="../sales/Vacancy.htm">Vacancy
                        Loss Report</a></td>
                    </tr>
                    <tr>
                      <td colspan="2">Appliance Report</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>xBy Unit</td>

                    </tr>
                    <tr>
                      <td></td>
                      <td>xBy Building</td>
                    </tr>
                    <tr>
                      <td colspan="2">Rooms</td>
                    </tr>

                    <tr>
                      <td></td>
                      <td>By Unit</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>By Building</td>
                    </tr>

                    <tr>
                      <td colspan="2">Room Contents</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>xBy Unit</td>
                    </tr>
                    <tr>

                      <td></td>
                      <td>xBy Building</td>
                    </tr>
                    <tr>
                      <td colspan="2">xInsurance Policies</td>
                    </tr>
                    <tr>
                      <td colspan="2">xMortgage Companies</td>

                    </tr>
                    <tr>
                      <td colspan="2">Classified Ads</td>
                    </tr>
                    <tr>
                      <td colspan="2">Notes</td>
                    </tr>
                    <tr>

                      <td></td>
                      <td>By Unit</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>By Building</td>
                    </tr>
                    <tr>

                      <td colspan="2"><b>Tenant Reports</b></td>
                    </tr>
                    <tr>
                      <td colspan="2">x<a href="Reportimages/leaseexp.htm">Lease
                        Expiration Report</a></td>
                    </tr>
                    <tr>
                      <td colspan="2">xLate Rent Report</td>

                    </tr>
                    <tr>
                      <td colspan="2"><a href="Reportimages/security.htm">Security
                        Deposit Report</a></td>
                    </tr>
                    <tr>
                      <td colspan="2"><strong>Other Reports</strong></td>

                    </tr>
                    <tr>
                      <td colspan="2">x<a href="Reportimages/contrlst.htm">Contractor
                        List</a></td>
                    </tr>
                    <tr>
                      <td colspan="2"><b>Forms and Letters</b></td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>&nbsp;</td>

                    </tr>
                    <tr>
                      <td></td>
                      <td>Rental Application</td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>Mailing Labels</td>

                    </tr>
                  </table>


				  
              </td>
            </tr>
          </table>

</div>


    <br />



    <p>
        <span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA"> 
        <asp:HyperLink ID="HyperLink47" runat="server" NavigateUrl="~/manager/events/default.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Events Calendar</span></asp:HyperLink>
        &nbsp;
                          <asp:HyperLink ID="HyperLink48" runat="server" 
                              NavigateUrl="~/manager/events/event_add.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Add an event</span></asp:HyperLink>
        &nbsp;
                          <asp:HyperLink ID="HyperLink49" runat="server" 
                              NavigateUrl="~/manager/Scheduler/default2.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">Event cal. 2</span></span></asp:HyperLink>
        &nbsp;&nbsp;<asp:HyperLink ID="HyperLink56" runat="server" 
            NavigateUrl="~/tiger/numberofuser.aspx"># of logged in user</asp:HyperLink>
                          &nbsp;<asp:HyperLink ID="HyperLink51" runat="server" 
                              NavigateUrl="~/EncDecViaCode.aspx"><span
                lang="EN-US" style="font-size: 10pt; line-height: 115%; font-family: 'Courier New';
                mso-ansi-language: EN-US; mso-fareast-font-family: 'Times New Roman'; mso-fareast-theme-font: minor-fareast;
                mso-fareast-language: EN-CA; mso-bidi-language: AR-SA">webconfig encrypt/decrypt</span></asp:HyperLink>
        &nbsp;
                               <br />
        </span><strong>REPORTS (todo) , </strong><span lang="EN-US" style="font-size: 10pt; font-family: 'Courier New';
            mso-ansi-language: EN-US">tenant invoice,tenant financial,Building financials</span></p>
<p>
        &nbsp;UNTREATED RENT EST EN FAIT E AGED RECEIVABLE REPORT advanced management 
        report<br />
        incExp reports<br />
        aged receiveble reports<br />
        lease expration reports<br />
        operating reserve summary reports<br />
        advanced lease report<br />
        advanced portfolio report<br />
        liabiliti summary report<br />
        maintenance report<br />
        detail income report revenu analysis OWNER REPORt<br />
        FINANCE REPORT<br />
        <br />
        advanced money out
        <br />
        refund
        <br />
        incexp
        <br />
        aged rceiveble
        <br />
        detail income<br />
        <br />
        revenue analysis
        <br />
        PROPERTY REPORT
        <br />
        advanced property
        <br />
        unit summary
        <br />
        vacancy MAINTENANCE REPORT
        <br />
        maintenance summary
        <br />
        maintenance expense
        <br />
        service request
        <br />
        work order category LEASE REPORT
        <br />
        advanced lease
        <br />
        rent roll
        <br />
        lease expiration
        <br />
        notice of intent to vacate
        <br />
        liabiliti summary
        <br />
        lease charges
        <br />
        <br />
        LETTERS AND STATEMENTS</p>


</asp:Content>

