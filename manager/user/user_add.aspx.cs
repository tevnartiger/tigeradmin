﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : sept 23 , 2009
/// </summary>
public partial class manager_user_user_add : BasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_home_list.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
            gv_home_list.DataBind();

            reg_name_addr.ValidationExpression = RegEx.getText();
            reg_name_addr_city.ValidationExpression = RegEx.getText();
            reg_name_addr_pc.ValidationExpression = RegEx.getText();
            reg_name_fname.ValidationExpression = RegEx.getText();
            reg_name_lname.ValidationExpression = RegEx.getText();
            reg_name_tel.ValidationExpression = RegEx.getText();
            reg_name_email.ValidationExpression = RegEx.getEmail();

            reg_name_password2.ValidationExpression = RegEx.getText();
            reg_name_password.ValidationExpression = RegEx.getText();
            reg_name_login.ValidationExpression = RegEx.getText();

            MultiView1.ActiveViewIndex = 0;
        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_next_Click(object sender, EventArgs e)
    {

        lbl_person.Text = RegEx.getText(name_fname.Text + " " + name_lname.Text);
        MultiView1.ActiveViewIndex = 1;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_Click(object sender, EventArgs e)
    {


        int number_of_insert = 0;
        string home_id = "";
        //---------------------------------------------------------
        for (int j = 0; j < gv_home_list.Rows.Count; j++)
        {

            CheckBox chk_process = (CheckBox)gv_home_list.Rows[j].FindControl("chk_process");
            // if the checkbox is checked the process
            if (chk_process.Checked)
            {
                HiddenField h_id = (HiddenField)gv_home_list.Rows[j].FindControl("h_home_id");

                home_id = home_id + h_id.Value + "|";
                number_of_insert++;
            }
        }

        //----------------------------------------------------------


        //generate hash password
        string temp_salt = sinfoca.tiger.security.Hash.createSalt(30);
        string temp_hash = sinfoca.tiger.security.Hash.createPasswordHash(RegEx.getText(name_password.Text), temp_salt);
        Page.Validate();
        if (Page.IsValid)
        {
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prRoleNewWebAccount", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //  try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = 0;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;
                cmd.Parameters.Add("@home_id", SqlDbType.VarChar, 8000).Value = RegEx.getText(home_id);
                cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_role.SelectedValue);

                //----------------------------------------------------------------------------------------


                cmd.Parameters.Add("@name_lname", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_lname.Text);
                cmd.Parameters.Add("@name_fname", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_fname.Text);


                cmd.Parameters.Add("@name_addr", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr.Text);
                cmd.Parameters.Add("@name_addr_city", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_city.Text);
                cmd.Parameters.Add("@name_addr_pc", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_pc.Text);
                cmd.Parameters.Add("@name_addr_state", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_state.Text);


                cmd.Parameters.Add("@name_tel", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_tel.Text);

                tiger.Date d = new tiger.Date();

                cmd.Parameters.Add("@name_email", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_email.Text);
                cmd.Parameters.Add("@name_dob", SqlDbType.DateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_name_dob_m.SelectedValue, ddl_name_dob_d.SelectedValue, ddl_name_dob_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

                cmd.Parameters.Add("@login_user", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_login.Text);
                cmd.Parameters.Add("@login_salt", SqlDbType.NVarChar, 50).Value = RegEx.getText(temp_salt);
                cmd.Parameters.Add("@login_hash", SqlDbType.NVarChar, 50).Value = RegEx.getText(temp_hash);



                //execute the insert
                cmd.ExecuteReader();

                if (Convert.ToInt32(cmd.Parameters["@return_id"].Value) == 0)
                    lbl_success.Text = Resources.Resource.lbl_successfull_add;
                conn.Close();

            }

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_previous_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
}
