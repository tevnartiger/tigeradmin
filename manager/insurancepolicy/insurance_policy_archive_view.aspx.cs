﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn   
/// date    : july 11, 2008
/// </summary>
public partial class manager_insurancepolicy_insurance_policy_archive_view : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RegEx.IsInteger(Request.QueryString["ip_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        /////////////
        /////////////// SECURITY CHECK BEGIN   //////////////
        //////////////
        string strconn = (Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
       
        AccountObjectAuthorization ipAuthorization = new AccountObjectAuthorization(strconn);

        if (!ipAuthorization.InsurancePolicy(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["ip_id"])))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        /////////////
        /////////////// SECURITY CHECK END  //////////////
        //////////


        if (!Page.IsPostBack)
        {

            btn_delete.Enabled = false;

            // insurance_policy_add_link.NavigateUrl = "insurance_policy_add.aspx";
             insurance_policy_update_link.NavigateUrl = "insurance_policy_update.aspx?ip_id=" + Request.QueryString["ip_id"];

            
            int insurance_type = 0;
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prInsurancePolicyView", conn);
            cmd.CommandType = CommandType.StoredProcedure;



            //Add the params
            cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("ip_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["ip_id"]);
            //  try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);



                while (dr.Read() == true)
                {
                    lbl_home_name.Text = dr["home_name"].ToString();

                    // tiger.InsurancePolicy hv = new tiger.InsurancePolicy(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

                    //------------------------------------------------
                    //------------------------------------------------
                    // gv_ip_home_archive_list.DataSource = hv.getInsurancePolicyHomeArchiveList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(dr["home_id"]));
                    //  gv_ip_home_archive_list.DataBind();
                    //------------------------------------------------
                    //------------------------------------------------


                    lbl_ip_name.Text = dr["ip_name"].ToString();

                    insurance_type = Convert.ToInt32(dr["ip_insurance_type"]);

                    lbl_ip_number.Text = dr["ip_number"].ToString();


                    if (dr["ip_amount_covered"] == DBNull.Value)
                        lbl_ip_amount_covered.Text = String.Format("{0:0.00}", 0.00);
                    else
                        lbl_ip_amount_covered.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["ip_amount_covered"]));


                    if (dr["ip_deductible"] == DBNull.Value)
                        lbl_ip_deductible.Text = String.Format("{0:0.00}", 0.00);
                    else
                        lbl_ip_deductible.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["ip_deductible"]));


                    if (dr["ip_year_premium"] == DBNull.Value)
                        lbl_ip_year_premium.Text = String.Format("{0:0.00}", 0.00);
                    else
                        lbl_ip_year_premium.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["ip_year_premium"]));


                    if (dr["ip_monthly_payment"] == DBNull.Value)
                        lbl_ip_monthly_payment.Text = String.Format("{0:0.00}", 0.00);
                    else
                        lbl_ip_monthly_payment.Text = String.Format("{0:0.00}", Convert.ToDouble(dr["ip_monthly_payment"]));




                    DateTime date_begin = new DateTime();
                    date_begin = Convert.ToDateTime(dr["ip_date_begin"]);
                    lbl_ip_date_begin.Text = date_begin.Month.ToString() + "/" + date_begin.Day.ToString() + "/" + date_begin.Year.ToString();


                    DateTime date_end = new DateTime();
                    date_end = Convert.ToDateTime(dr["ip_date_end"]);
                    lbl_ip_date_end.Text = date_end.Month.ToString() + "/" + date_end.Day.ToString() + "/" + date_end.Year.ToString();



                }

            }



            //finally
            {
                conn.Close();
            }



            if (insurance_type == 1)
            {
                lbl_insurance_type.Text = Resources.Resource.lbl_property_insurance;
            }
            if (insurance_type == 2)
            {
                lbl_insurance_type.Text = Resources.Resource.lbl_mortgage_insurance;
            }

            if (insurance_type == 3)
            {
                lbl_insurance_type.Text = Resources.Resource.lbl_mortgage_life_insurance;
            }

        }
    }
    protected void btn_delete_Click(object sender, EventArgs e)
    {

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prInsurancePolicyDelete", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@ip_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["ip_id"]);
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            //execute the insert
            cmd.ExecuteReader();

            // int new_mortgage_id = 0;
            // new_mortgage_id = Convert.ToInt32(cmd.Parameters["@new_mortgage_id"].Value);
            // if (new_mortgage_id > 0)
            //    result.InnerHtml = " add mortgage successful";


        }
        catch (Exception error)
        {
            // tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }

        string url = "insurance_policy_list.aspx";
        Response.Redirect(url);


    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void chk_enable_button_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_enable_button.Checked == true)
            btn_delete.Enabled = true;
        else
            btn_delete.Enabled = false;
    }

}
