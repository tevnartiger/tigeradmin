<%@ Page Language="C#" MasterPageFile="../mp_manager.master" AutoEventWireup="true" CodeFile="group_removeqq.aspx.cs" Inherits="group_group_add" Title="New group" %>
<%@ Register Src="~/manager/uc/uc_content_menu.ascx" TagName="uc_content_menu" TagPrefix="uc_content_menu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc_content_menu:uc_content_menu id="uc_content_menu" runat="server" /><br />

<div style="float:left;width:100%;">
<div style="float:left;width:100%;">
<h2><asp:Literal ID="lt" runat="server" Text="<%$ resources:resource, lbl_home_group %>" /></h2>
</div>


<div style="float:left;width:100%;text-align:left">
<asp:MultiView ID="mv" runat="server" ActiveViewIndex="0"> 
<asp:View ID="view1" runat="server">
1/3&nbsp;&nsbp;<asp:Literal runat="server"  ID="Literal1" Text="<%$ resources:resource, lbl_group_type %>" />
<br /><br /><br />
<asp:TextBox ID="tb_group_name" MaxLength="15" width="100"  runat="server"></asp:TextBox><br />
       <asp:RequiredFieldValidator ID="ada" runat="server" ControlToValidate="tb_group_name" Display="dynamic" Text="<%$ resources:resource, iv_required %>" />
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" OnClick="btn_to2_Click"
            Text="<%$ resources:resource,lbl_next %>" />
 </asp:View>  
 
 <asp:View ID="view2" runat="server">
 2/3&nbsp;&nsbp;<asp:Literal runat="server"  ID="Literal2" Text="<%$ resources:resource, lbl_group_select_property %>" />
<br /><br /><br />
     <asp:CheckBoxList RepeatDirection="Vertical" ID="cbl_home_list" DataTextField="home_name" DataValueField="home_id" runat="server" />
 <br /><br />           
 <asp:Button ID="btn_back1" runat="server"  OnClick="btn_back1_Click" Text="<%$ resources:resource,lbl_previous %>" />&nbsp;&nbsp;&nbsp;&nbsp;  
            <asp:Button ID="btn_to3" runat="server" OnClick="btn_to3_Click" Text="<%$ resources:resource,lbl_next %>" />
 </asp:View>
  <asp:View ID="view3" runat="server">
 3/3
 <br /><br />
     <asp:CheckBoxList RepeatDirection="Vertical" ID="CheckBoxList1" DataTextField="home_name" DataValueField="home_id" runat="server" />
 <br /><br />
 
 <asp:Literal ID="lt_group" runat="server" /><br /><br />
   <asp:Button ID="btn_back3" runat="server" OnClick="btn_back2_Click"  Text="<%$ resources:resource,lbl_previous %>" />&nbsp;&nbsp;&nbsp;&nbsp;  
 <asp:Button ID="btn_create" runat="server" OnClick="btn_create_Click"  Text="<%$ resources:resource,lbl_group_create %>" />
 <br /><br />
 <asp:Literal ID="lt_message" runat="server" />

 </asp:View>
 </asp:MultiView>
 </div>
<div style="float:left;width:60%;text-align:left;">
</div>


<!--<div id="txt_message" runat="server" />
 <asp:GridView ID="group_list" runat="server" />
 
 </div>
    -->
</asp:Content>