﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="alerts_setup.aspx.cs" Inherits="manager_alerts_alerts_setup" Title="Untitled Page" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
 <style type="text/css"  >
 div.row {float: left; margin:  0px 0px 8px 0px; padding: 0; width: 98%;}
 div.col400px {float: left; width: 400px; margin: 0 3px 0 0; padding: 0;}
 div.col500px {float: left; width: 500px; margin: 0 3px 0 0; padding: 0;}
 </style>
</asp:Content>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="top_row" class="row">
 <div class="col500px">     
  <div id="basic"   class="myform">
     <h1><asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:Resource, lbl_u_alert_setup %>"/></h1>
        
      <hr />
      
       
        
        
   </div>
 </div>
</div>







<table id="tb_successfull_confirmation" runat="server" style="width: 100%">
      <tr>
         <td style="color: #FF6600">
                            <b>
             <asp:Literal ID="Literal14" runat="server" Text="<%$ Resources:Resource, lbl_successfull_modification %>"></asp:Literal></b></td>
      </tr>
   </table>
    
  <table >
                    <tr bgcolor="aliceblue">
                         <td colspan="2"  >
                           <b><asp:Literal ID="Literal15" runat="server" Text="<%$ Resources:Resource, lbl_alert_when %>"/> </b>
                         </td>
                         
                    </tr>
                     <tr runat="server">
                            <td align="right">
                                &nbsp;
                                <asp:Literal ID="lbl_lease_expiration" runat="server"></asp:Literal>
&nbsp;<asp:Literal ID="Literal8" runat="server" Text="<%$ Resources:Resource, lbl_lease_expire %>"></asp:Literal>
                            </td>
                            <td>
                         
                                <asp:Textbox ID="tbx_as_lease_expiration" runat="server" Width="60px"></asp:Textbox>
                            &nbsp;<asp:Literal ID="Literal7" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>" ></asp:Literal>
                                  <asp:RegularExpressionValidator ID="reg_as_lease_expiration" ControlToValidate="tbx_as_lease_expiration" runat="server" ErrorMessage="invalid number"></asp:RegularExpressionValidator>
           
                         </td>
                            
                        </tr>
                        <tr align="right" id="tr_lease_pending" runat="server">
                            <td>
                                &nbsp;
                                <asp:Literal ID="lbl_lease_pending" runat="server"></asp:Literal>
                                &nbsp;<asp:Literal ID="Literal13" runat="server" Text="<%$ Resources:Resource, lbl_lease_pending %>"></asp:Literal>
                                </td>
                                
                                <td>
                         
                                <asp:Textbox ID="tbx_as_lease_pending" runat="server" Width="60px"></asp:Textbox>
                                &nbsp;<asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>" ></asp:Literal>
                               <asp:RegularExpressionValidator ID="reg_as_lease_pending" ControlToValidate="tbx_as_lease_pending" runat="server" ErrorMessage="invalid number"></asp:RegularExpressionValidator>       
                         </td>
                            
                        </tr>
                        <tr align="right" id="tr_mortgage_expiration" runat="server">
                            <td >
                                &nbsp;
                                <asp:Literal ID="lbl_mortgage_expiration" runat="server"></asp:Literal>
&nbsp;<asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Resource, lbl_mortgage_expire %>"></asp:Literal>
                                              </td>
                                              
                          <td>
                         
                                <asp:Textbox ID="tbx_as_mortgage_expiration" runat="server" Width="60px"></asp:Textbox>
                                      &nbsp;<asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>" ></asp:Literal> 
                              <asp:RegularExpressionValidator ID="reg_as_mortgage_expiration" ControlToValidate="tbx_as_mortgage_expiration" runat="server" ErrorMessage="invalid number"></asp:RegularExpressionValidator>       
                          </td>
                            
                        </tr>
                        <tr align="right" id="tr_mortgage_pending" runat="server">
                            <td >
                                &nbsp;
                                <asp:Literal ID="lbl_mortgage_pending" runat="server"></asp:Literal>
&nbsp;<asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:Resource, lbl_mortgage_pending %>"></asp:Literal>
                                              </td>
                                              
                           <td>
                         
                                <asp:Textbox ID="tbx_as_mortgage_pending" runat="server" Width="60px"></asp:Textbox>
                                    &nbsp;<asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>" ></asp:Literal>
                              <asp:RegularExpressionValidator ID="reg_as_mortgage_pending" ControlToValidate="tbx_as_mortgage_pending" runat="server" ErrorMessage="invalid number"></asp:RegularExpressionValidator>       
                         </td>
                            
                        </tr>
                        <tr align="right" id="tr_ip_expiration" runat="server">
                            <td>
                                &nbsp;
                                <asp:Literal ID="lbl_ip_expiration" runat="server"></asp:Literal>
&nbsp;<asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Resource, lbl_ip_expire %>"></asp:Literal>
                                              </td>
                                              
                          <td>
                         
                                <asp:Textbox ID="tbx_as_ip_expiration" runat="server" Width="60px"></asp:Textbox>
                                &nbsp;<asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>" ></asp:Literal>
                            <asp:RegularExpressionValidator ID="reg_as_ip_expiration" ControlToValidate="tbx_as_ip_expiration" runat="server" ErrorMessage="invalid number"></asp:RegularExpressionValidator>       
                         </td>
                            
                        </tr>
                        <tr align="right" id="tr_ip_pending" runat="server">
                            <td >
                                &nbsp;
                                <asp:Literal ID="lbl_ip_pending" runat="server"></asp:Literal>
&nbsp;<asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:Resource, lbl_ip_PENDING %>"></asp:Literal>
                                              </td>
                                              
                            <td>
                         
                                <asp:Textbox ID="tbx_as_ip_pending" runat="server" Width="60px"></asp:Textbox>
                                &nbsp;<asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:Resource, lbl_l_days %>" ></asp:Literal>
                           <asp:RegularExpressionValidator ID="reg_as_ip_pending" ControlToValidate="tbx_as_ip_pending" runat="server" ErrorMessage="invalid number"></asp:RegularExpressionValidator>       
                         
                         </td>
                            
                        </tr>
                        
                        
                        
                        <tr align="right" runat="server">
                            <td >&nbsp;
                                <br />
                            </td>
                                              
                            <td align="left"> 
                                <br />
                             <asp:Button ID="btn_update" runat="server" 
                              Text="<%$ Resources:Resource, lbl_update %>" 
                              onclick="btn_update_Click" />
                            </td>
                            
                        </tr>
                                                
                    </table>
                
    <br />
   
                
</asp:Content>

