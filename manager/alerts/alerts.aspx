﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="alerts.aspx.cs" Inherits="manager_alerts_alerts" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<br />

<table>
 <tr>
     <td  >
       <asp:Label ID="lbl_property" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_property %>" style="font-weight: 700" />
      </td>
         <td  >
                               :
                               <asp:DropDownList ID="ddl_home_list" runat="server" AutoPostBack="true" 
                                   DataTextField="home_name" DataValueField="home_id" 
                                   OnSelectedIndexChanged="ddl_home_list_SelectedIndexChanged">
                               </asp:DropDownList>
                               &nbsp;</td>
                               
        
   </tr>
                      
</table>
    <br />
        <table width="100%" id="tb_untreated_rent" runat="server">
            <tr id="tr_untreated_rent" runat="server">
                <td bgcolor="aliceblue">
                    <b><asp:Label ID="Label8" runat="server" Text="UNTREATED RENT(S)"/> </b>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView Width="50%" BorderColor="White" BorderWidth="3"  
                                  ID="gv_untreated_rent" runat="server" AutoGenerateColumns="false">
        
                        <Columns>
                         <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_home_name %>" 
                                         DataField="home_name"   />
                         <asp:BoundField HeaderText="amount" 
                                         DataField="amount"   />
                                         
                         <asp:HyperLinkField  Text="<%$ Resources:Resource,lbl_view %>" 
                                         DataNavigateUrlFields="home_id" 
                                         DataNavigateUrlFormatString="../tenant/tenant_untreated_rent.aspx?h_id={0}" 
                                         HeaderText="<%$ Resources:Resource,lbl_view %>"  />               
                         
                                         
                       </Columns>          
                   </asp:GridView> 
      
                </td>
            </tr>
            </table>
         <br />
        <table width="100%" id="tb_wo_pending" runat="server">
            <tr id="tr_wo_pending" runat="server">
                <td bgcolor="aliceblue">
                    <b><asp:Label ID="Label5" runat="server" Text="SCHEDULE MAINTENANCE & WORK ORDERS"/> </b>
                </td>
            </tr>
            <tr>
                <td >
                 <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="83%" BorderColor="White" BorderWidth="3"  ID="gv_wo_pending" runat="server" AutoGenerateColumns="false"
                      GridLines="Both"   AllowPaging="true" >
        
                        <Columns>
                         <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_work_order %>" 
                                         DataField="wo_title"   />
                         <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_property %>" 
                                         DataField="home_name"   />
                         <asp:BoundField HeaderText="<%$ Resources:Resource,gv_unit %>" 
                                         DataField="unit_door_no"   />
                         <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_date_begin %>" 
                                         DataField="wo_date_begin" DataFormatString="{0:MMM-dd-yyyy}"  
                                         HtmlEncode="false" />
      
      
                         <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_due_date %>"  DataField="wo_exp_date_end" DataFormatString="{0:MMM-dd-yyyy}"  
                                 HtmlEncode="false" />
      
                         <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_status%>"  >
                         <ItemTemplate  >
                         <asp:Label ID="lbl_wo_status"  
                             Text='<%#GetStatus(Convert.ToInt32(Eval("wo_status")))%>' 
                           runat="server" />   
                         </ItemTemplate  >
                         </asp:TemplateField>
       
       
                         <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_priority%>"  >
                         <ItemTemplate  >
                         <asp:Label ID="lbl_task_priority"  
                                       Text='<%#GetPriority(Convert.ToInt32(Eval("wo_priority")))%>' 
                                       runat="server" />   
                         </ItemTemplate  >
                         </asp:TemplateField>
       
       
                         <asp:HyperLinkField  Text="<%$ Resources:Resource,lbl_view %>" 
                                         DataNavigateUrlFields="wo_id,home_id,incident_id" 
                                         DataNavigateUrlFormatString="../workorder/wo_view.aspx?wo_id={0}&h_id={1}&inc={2}" 
                                         HeaderText="<%$ Resources:Resource,lbl_view %>"  />
      
    
    
                         <asp:HyperLinkField  Text="<%$ Resources:Resource,lbl_update %>" 
                                         DataNavigateUrlFields="wo_id,home_id,incident_id" 
                                         DataNavigateUrlFormatString="../workorder/wo_update.aspx?wo_id={0}&h_id={1}&inc={2}" 
                                         HeaderText="<%$ Resources:Resource,lbl_update %>"  />
    
    
    
      
    
                    </Columns>   
        
        
    </asp:GridView> 
                </td>
            </tr>
        </table>
    <br />
     <table width="100%" id="tb_wo_overdue" runat="server">
            <tr>
                <td bgcolor="aliceblue">
                    <b><asp:Label ID="Label7" runat="server" Text="OVERDUE MAINTENANCE & WORK ORDERS"/> </b>
                </td>
            </tr>
            <tr>
                <td >
                  <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="83%" BorderColor="White" BorderWidth="3"  ID="gv_wo_overdue" runat="server"
                       AutoGenerateColumns="false" AllowPaging="true">
        
                        <Columns>
                         <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_work_order %>" 
                                         DataField="wo_title"   />
                         <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_property %>" 
                                         DataField="home_name"   />
                         <asp:BoundField HeaderText="<%$ Resources:Resource,gv_unit %>" 
                                         DataField="unit_door_no"   />
                         <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_date_begin %>" 
                                         DataField="wo_date_begin" DataFormatString="{0:MMM-dd-yyyy}"  
                                         HtmlEncode="false" />
      
      
                         <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_due_date %>"  DataField="wo_exp_date_end" DataFormatString="{0:MMM-dd-yyyy}"  
                                 HtmlEncode="false" />
      
                         <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_status%>"  >
                         <ItemTemplate  >
                         <asp:Label ID="lbl_wo_status"  
                             Text='<%#GetStatus(Convert.ToInt32(Eval("wo_status")))%>' 
                           runat="server" />   
                         </ItemTemplate  >
                         </asp:TemplateField>
       
       
                         <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_priority%>"  >
                         <ItemTemplate  >
                         <asp:Label ID="lbl_task_priority"  
                                       Text='<%#GetPriority(Convert.ToInt32(Eval("wo_priority")))%>' 
                                       runat="server" />   
                         </ItemTemplate  >
                         </asp:TemplateField>
       
       
                         <asp:HyperLinkField  Text="<%$ Resources:Resource,lbl_view %>" 
                                         DataNavigateUrlFields="wo_id,home_id,incident_id" 
                                         DataNavigateUrlFormatString="../workorder/wo_view.aspx?wo_id={0}&h_id={1}&inc={2}" 
                                         HeaderText="<%$ Resources:Resource,lbl_view %>"  />
      
    
    
                         <asp:HyperLinkField  Text="<%$ Resources:Resource,lbl_update %>" 
                                         DataNavigateUrlFields="wo_id,home_id,incident_id" 
                                         DataNavigateUrlFormatString="../workorder/wo_update.aspx?wo_id={0}&h_id={1}&inc={2}" 
                                         HeaderText="<%$ Resources:Resource,lbl_update %>"  />
    
    
    
      
    
                    </Columns>   
        
        
    </asp:GridView> 
        
  
                </td>
            </tr>
        </table>
    <br />
    <table style="width: 100%">
        <tr>
      <td>
      <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b><asp:Label ID="Label46" runat="server" 
                        Text="<%$ Resources:Resource, lbl_u_lease_expiration%>"/> </b>
                </td>
            </tr>
        </table>
        <br />
        <asp:Repeater ID="r_lease_expiration" runat="server">
            <ItemTemplate>
                <asp:Label ID="Label42" runat="server" Text="<%$ Resources:Resource, lbl_less31%>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <%# Eval("less31")%>
                <br />
                <asp:Label ID="Label44" runat="server" Text="<%$ Resources:Resource, lbl_between_31and60%>"/>&nbsp; : <%# Eval("between31and60")%>
                <br />
                <asp:Label ID="Label45" runat="server" Text="<%$ Resources:Resource, lbl_between_61and90%>"/>&nbsp; :&nbsp;<%# Eval("between61and90")%>
               
             </ItemTemplate>
        </asp:Repeater>
       <br />
                <asp:HyperLink ID="lease_expiration_link" Text="<%$ Resources:Resource, lbl_view%>" runat="server"></asp:HyperLink>
               

          </td>
      
      
      
      <td>
      
      
      <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b><asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_u_mortgage_expiration%>"/> </b>
                </td>
            </tr>
        </table>
        
        
        <br />
        <asp:Repeater ID="r_mortgage_expiration" runat="server">
            <ItemTemplate>
                <asp:Label ID="Label42" runat="server" Text="<%$ Resources:Resource, lbl_less31%>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <%# Eval("less31")%>
                <br />
                <asp:Label ID="Label44" runat="server" Text="<%$ Resources:Resource, lbl_between_31and60%>"/>&nbsp; : <%# Eval("between31and60")%>
                <br />
                <asp:Label ID="Label45" runat="server" Text="<%$ Resources:Resource, lbl_between_61and90%>"/>&nbsp; :&nbsp;<%# Eval("between61and90")%>
               </ItemTemplate>
        </asp:Repeater>
       <br />
                <asp:HyperLink ID="mortgage_expiration_link" Text="<%$ Resources:Resource, lbl_view%>" runat="server"></asp:HyperLink>
               
      </td>
            
            
            
 <td>
                <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b><asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_u_insurance_policy_expiration%>"/> </b>
                </td>
            </tr>
        </table>
        
        
        <br />
        <asp:Repeater ID="r_insurance_policy_expiration" runat="server">
            <ItemTemplate>
                <asp:Label ID="Label42" runat="server" Text="<%$ Resources:Resource, lbl_less31%>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <%# Eval("less31")%>
                <br />
                <asp:Label ID="Label44" runat="server" Text="<%$ Resources:Resource, lbl_between_31and60%>"/>&nbsp; : <%# Eval("between31and60")%>
                <br />
                <asp:Label ID="Label45" runat="server" Text="<%$ Resources:Resource, lbl_between_61and90%>"/>&nbsp; :&nbsp;<%# Eval("between61and90")%>
                
             </ItemTemplate>
        </asp:Repeater>
<br />
                <asp:HyperLink ID="insurance_policy_expiration_link" Text="<%$ Resources:Resource, lbl_view%>" runat="server"></asp:HyperLink>
               
                
     </td>
        </tr>
    </table>
      
       


    <br />
    <br />
    <table style="width: 100%">
        <tr>
      <td>
      <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b><asp:Label ID="Label47" runat="server" 
                        Text="<%$ Resources:Resource, lbl_u_lease_pending%>"/> </b>
                </td>
            </tr>
        </table>
        <br />
        <asp:Repeater ID="r_lease_pending" runat="server">
            <ItemTemplate>
                <asp:Label ID="Label48" runat="server" 
                    Text="<%$ Resources:Resource, lbl_less31%>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <%# Eval("less31")%>
                <br />
                <asp:Label ID="Label49" runat="server" 
                    Text="<%$ Resources:Resource, lbl_between_31and60%>"/>&nbsp; : <%# Eval("between31and60")%>
                <br />
                <asp:Label ID="Label50" runat="server" 
                    Text="<%$ Resources:Resource, lbl_between_61and90%>"/>&nbsp; :&nbsp;<%# Eval("between61and90")%>
                
             </ItemTemplate>
        </asp:Repeater>
      
     <br />
                <asp:HyperLink ID="lease_pending_link" Text="<%$ Resources:Resource, lbl_view%>" runat="server"></asp:HyperLink>
               
      </td>
      
      
      
      <td>
      
      
      <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b><asp:Label ID="Label51" runat="server" 
                        Text="<%$ Resources:Resource, lbl_u_pending_mortgage%>"/> </b>
                </td>
            </tr>
        </table>
        
        
        <br />
        <asp:Repeater ID="r_mortgage_pending" runat="server">
            <ItemTemplate>
                <asp:Label ID="Label52" runat="server" 
                    Text="<%$ Resources:Resource, lbl_less31%>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <%# Eval("less31")%>
                <br />
                <asp:Label ID="Label53" runat="server" 
                    Text="<%$ Resources:Resource, lbl_between_31and60%>"/>&nbsp; : <%# Eval("between31and60")%>
                <br />
                <asp:Label ID="Label54" runat="server" 
                    Text="<%$ Resources:Resource, lbl_between_61and90%>"/>&nbsp; :&nbsp;<%# Eval("between61and90")%>
                
               </ItemTemplate>
        </asp:Repeater>
      <br />
                <asp:HyperLink ID="mortgage_pending_link" Text="<%$ Resources:Resource, lbl_view%>" runat="server"></asp:HyperLink>
               
      </td>
            
            
            
 <td>
                <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b><asp:Label ID="Label55" runat="server" 
                        Text="<%$ Resources:Resource,lbl_u_pending_insurance_policy %>"/> </b>
                </td>
            </tr>
        </table>
        
        
        <br />
        <asp:Repeater ID="r_insurance_policy_pending" runat="server">
            <ItemTemplate>
                <asp:Label ID="Label56" runat="server" 
                    Text="<%$ Resources:Resource, lbl_less31%>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <%# Eval("less31")%>
                <br />
                <asp:Label ID="Label57" runat="server" 
                    Text="<%$ Resources:Resource, lbl_between_31and60%>"/>&nbsp; : <%# Eval("between31and60")%>
                <br />
                <asp:Label ID="Label58" runat="server" 
                    Text="<%$ Resources:Resource, lbl_between_61and90%>"/>&nbsp; :&nbsp;<%# Eval("between61and90")%>
                
             </ItemTemplate>
        </asp:Repeater>
              
            <br />
                <asp:HyperLink ID="insurance_policy_pending_link"  Text="<%$ Resources:Resource, lbl_view%>" runat="server"></asp:HyperLink>
                   
     </td>
        </tr>
    </table>
    <br />
    <table id="tb_rent_delequency" runat="server" width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b><asp:Label ID="Label3" runat="server" 
                        Text="<%$ Resources:Resource, lbl_u_delequency%>"/> </b>
                </td>
            </tr>
        </table>
    
     <asp:GridView Width="83%" ID="gv_rent_delequency" runat="server" AutoGenerateColumns="false"
                   AllowPaging="true" AllowSorting="true"  AutoGenerateSelectButton="false">
    <Columns>
     <asp:BoundField DataField="home_name"  HeaderText="<%$ Resources:Resource, lbl_property%>"  />
     
      <asp:BoundField DataField="unit_door_no"  HeaderText="<%$ Resources:Resource, gv_unit%>"  />
      
      
      <asp:BoundField DataField="amount_owed"  HeaderText="<%$ Resources:Resource, gv_amount_owed%>"
             DataFormatString="{0:0.00}"   HtmlEncode="false"  />
    
     
      
      
      <asp:BoundField   DataField="rp_due_date" DataFormatString="{0:M-dd-yyyy}"  
       HtmlEncode="false" HeaderText="<%$ Resources:Resource, gv_due_date%>"  />
      <asp:BoundField  DataField="days" HeaderText="<%$ Resources:Resource, lbl_days %>"/> 
     
      <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_notice_sent%>"   >
      <ItemTemplate>
      <asp:Label  runat="server"  ID="notice_sent"    Text='<%#Get_AmountOfWarningSent(Convert.ToInt32(Eval("amount_of_warning_sent")))%>'    /> 
      </ItemTemplate>
      </asp:TemplateField>
     
      <asp:HyperLinkField  Text="<%$ Resources:Resource, gv_view %>" 
        DataNavigateUrlFields="home_id,tenant_id,tu_id,rl_rent_amount,re_id,unit_id" 
        DataNavigateUrlFormatString="~/manager/tenant/tenant_rent.aspx?h_id={0}&t_id={1}&tu_id={2}&ra_id={3}&re_id={4}&unit_id={5}" 
         HeaderText="<%$ Resources:Resource, gv_view %>"  />
    
      <asp:HyperLinkField  Text="<%$ Resources:Resource, lbl_send_notice %>" 
        DataNavigateUrlFields="rp_id,tenant_id,tu_id" 
        DataNavigateUrlFormatString="~/manager/notice/notice_delequency_send.aspx?rp_id={0}&tenant_id={1}&tu_id={2}" 
         HeaderText="<%$ Resources:Resource, lbl_send_notice %>"  />
    
   </Columns>
   </asp:GridView>
      
       <br />
       
<br />
    


    <br />
    pending work order rows :
    <asp:Label ID="Label59" runat="server" Text="Label"></asp:Label>
    <br />
    overdue work order rows :
    <asp:Label ID="Label9" runat="server" Text="Label"></asp:Label>
    <br />
    untreated :
    <asp:Label ID="Label10" runat="server" Text="Label"></asp:Label>


</asp:Content>

