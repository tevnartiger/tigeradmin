﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : july 17 , 2008
/// </summary>
public partial class manager_alerts_alerts : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {

            
            tiger.Home l = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getHomeCount(Convert.ToInt32(Session["schema_id"]));

            int home_id = l.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));

            // first we check if there's any property available
            if (home_count > 0)
            {



                tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ddl_home_list.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                ddl_home_list.DataBind();
                ddl_home_list.Items.Insert(0,new ListItem(Resources.Resource.lbl_all, "0"));
            }


            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prLeaseExpiration", conn);
            SqlCommand cmd2 = new SqlCommand("prMortgageExpiration", conn);
            SqlCommand cmd3 = new SqlCommand("prInsurancePolicyExpiration", conn);

            SqlCommand cmd4 = new SqlCommand("prLeasePending", conn);
            SqlCommand cmd5 = new SqlCommand("prMortgagePending", conn);
            SqlCommand cmd6 = new SqlCommand("prInsurancePolicyPending", conn);
            SqlCommand cmd7 = new SqlCommand("prWorkOrderPending", conn);
            SqlCommand cmd8 = new SqlCommand("prWorkOrderOverdue", conn);
            SqlCommand cmd9 = new SqlCommand("prHomeUntreatedRentQuantity", conn);


            cmd.CommandType = CommandType.StoredProcedure;

            try
            {

                 conn.Open();

                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = 0;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);


                r_lease_expiration.DataSource = dt;
                r_lease_expiration.DataBind();
            }
            finally
            {
                //  conn.Close();
            }


            cmd2.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd2.Parameters.Add("@home_id", SqlDbType.Int).Value = 0;


                SqlDataAdapter da = new SqlDataAdapter(cmd2);
                DataTable dt = new DataTable();
                da.Fill(dt);


                r_mortgage_expiration.DataSource = dt;
                r_mortgage_expiration.DataBind();
            }
            finally
            {
                // conn.Close();
            }



            cmd3.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd3.Parameters.Add("@home_id", SqlDbType.Int).Value = 0;


                SqlDataAdapter da = new SqlDataAdapter(cmd3);
                DataTable dt = new DataTable();
                da.Fill(dt);


                r_insurance_policy_expiration.DataSource = dt;
                r_insurance_policy_expiration.DataBind();
            }
            finally
            {
                // conn.Close();
            }


            cmd4.CommandType = CommandType.StoredProcedure;

            try
            {
                cmd4.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd4.Parameters.Add("@home_id", SqlDbType.Int).Value = 0;


                SqlDataAdapter da = new SqlDataAdapter(cmd4);
                DataTable dt = new DataTable();
                da.Fill(dt);


                r_lease_pending.DataSource = dt;
                r_lease_pending.DataBind();
            }
            finally
            {
                //  conn.Close();
            }


            cmd5.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd5.Parameters.Add("@schema_id", SqlDbType.Int).Value = 1; // Convert.ToInt32(Session["schema_id"]);
                cmd5.Parameters.Add("@home_id", SqlDbType.Int).Value = 0;


                SqlDataAdapter da = new SqlDataAdapter(cmd5);
                DataTable dt = new DataTable();
                da.Fill(dt);


                r_mortgage_pending.DataSource = dt;
                r_mortgage_pending.DataBind();
            }
            finally
            {
                // conn.Close();
            }



            cmd6.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd6.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd6.Parameters.Add("@home_id", SqlDbType.Int).Value = 0;


                SqlDataAdapter da = new SqlDataAdapter(cmd6);
                DataTable dt = new DataTable();
                da.Fill(dt);


                r_insurance_policy_pending.DataSource = dt;
                r_insurance_policy_pending.DataBind();
            }

            finally
            {
                // conn.Close();
            }


            cmd7.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd7.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd7.Parameters.Add("@home_id", SqlDbType.Int).Value = 0;


                SqlDataAdapter da = new SqlDataAdapter(cmd7);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_wo_pending.DataSource = dt;
                gv_wo_pending.DataBind();

                if (gv_wo_pending.Rows.Count < 1)
                {
                    tb_wo_pending.Visible = false;
                }
            }

            finally
            {
                
                Label59.Text = gv_wo_pending.Rows.Count.ToString();
              //  conn.Close();
            }

            cmd8.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd8.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd8.Parameters.Add("@home_id", SqlDbType.Int).Value = 0;


                SqlDataAdapter da = new SqlDataAdapter(cmd8);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_wo_overdue.DataSource = dt;
                gv_wo_overdue.DataBind();
            }

            finally
            {
                if (gv_wo_overdue.Rows.Count < 1)
                {
                    tb_wo_overdue.Visible = false;
                }
                //conn.Close();
                Label9.Text = gv_wo_overdue.Rows.Count.ToString();
            }


            cmd9.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd9.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd9.Parameters.Add("@home_id", SqlDbType.Int).Value = 0;


                SqlDataAdapter da = new SqlDataAdapter(cmd9);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_untreated_rent.DataSource = dt;
                gv_untreated_rent.DataBind();
            }

            finally
            {
                if (gv_untreated_rent.Rows.Count < 1)
                {
                    tb_untreated_rent.Visible = false;
                }
                Label10.Text = gv_untreated_rent.Rows.Count.ToString();
                conn.Close();
            }




            // setting the URL values

            lease_expiration_link.NavigateUrl = "~/manager/lease/lease_expiration.aspx";
            mortgage_expiration_link.NavigateUrl = "~/manager/mortgage/mortgage_expiration.aspx";
            insurance_policy_expiration_link.NavigateUrl = "~/manager/insurancepolicy/insurance_policy_expiration.aspx";


            lease_pending_link.NavigateUrl = "~/manager/lease/lease_list.aspx";
            mortgage_pending_link.NavigateUrl = "~/manager/mortgage/mortgage_list.aspx";
            insurance_policy_pending_link.NavigateUrl = "~/manager/insurancepolicy/insurance_policy_list.aspx";


            tiger.Rent hp = new tiger.Rent(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_rent_delequency.DataSource = hp.getRentDelequencyList(Convert.ToInt32(Session["schema_id"]), 0);
            gv_rent_delequency.DataBind();

            if (gv_rent_delequency.Rows.Count < 1)
            {
                tb_rent_delequency.Visible = false;
            }
        
        }
    }
    protected void ddl_home_list_SelectedIndexChanged(object sender, EventArgs e)
    {

        int home_id = 0;

        home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        tiger.Rent hp = new tiger.Rent(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_rent_delequency.DataSource = hp.getRentDelequencyList(Convert.ToInt32(Session["schema_id"]), home_id);
        gv_rent_delequency.DataBind();


        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prLeaseExpiration", conn);
        SqlCommand cmd2 = new SqlCommand("prMortgageExpiration", conn);
        SqlCommand cmd3 = new SqlCommand("prInsurancePolicyExpiration", conn);

        SqlCommand cmd4 = new SqlCommand("prLeasePending", conn);
        SqlCommand cmd5 = new SqlCommand("prMortgagePending", conn);
        SqlCommand cmd6 = new SqlCommand("prInsurancePolicyPending", conn);
        SqlCommand cmd7 = new SqlCommand("prWorkOrderPending", conn);
        SqlCommand cmd8 = new SqlCommand("prWorkOrderOverdue", conn);
        SqlCommand cmd9 = new SqlCommand("prHomeUntreatedRentQuantity", conn);

        cmd.CommandType = CommandType.StoredProcedure;

        try
        {

            conn.Open();

            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;


            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);


            r_lease_expiration.DataSource = dt;
            r_lease_expiration.DataBind();
        }
        finally
        {
            //  conn.Close();
        }


        cmd2.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;


            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);


            r_mortgage_expiration.DataSource = dt;
            r_mortgage_expiration.DataBind();
        }
        finally
        {
            // conn.Close();
        }



        cmd3.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd3.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;


            SqlDataAdapter da = new SqlDataAdapter(cmd3);
            DataTable dt = new DataTable();
            da.Fill(dt);


            r_insurance_policy_expiration.DataSource = dt;
            r_insurance_policy_expiration.DataBind();
        }
        finally
        {
            // conn.Close();
        }


        cmd4.CommandType = CommandType.StoredProcedure;

        try
        {
            cmd4.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd4.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;


            SqlDataAdapter da = new SqlDataAdapter(cmd4);
            DataTable dt = new DataTable();
            da.Fill(dt);


            r_lease_pending.DataSource = dt;
            r_lease_pending.DataBind();
        }
        finally
        {
            //  conn.Close();
        }


        cmd5.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd5.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd5.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;


            SqlDataAdapter da = new SqlDataAdapter(cmd5);
            DataTable dt = new DataTable();
            da.Fill(dt);


            r_mortgage_pending.DataSource = dt;
            r_mortgage_pending.DataBind();
        }
        finally
        {
            // conn.Close();
        }



        cmd6.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd6.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd6.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;


            SqlDataAdapter da = new SqlDataAdapter(cmd6);
            DataTable dt = new DataTable();
            da.Fill(dt);


            r_insurance_policy_pending.DataSource = dt;
            r_insurance_policy_pending.DataBind();
        }
        finally
        {
          //  conn.Close();
        }

        cmd7.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd7.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd7.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id ;


            SqlDataAdapter da = new SqlDataAdapter(cmd7);
            DataTable dt = new DataTable();
            da.Fill(dt);


            gv_wo_pending.DataSource = dt;
            gv_wo_pending.DataBind();
        }

        finally
        {
           // conn.Close();
        }

        cmd8.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd8.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd8.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;


            SqlDataAdapter da = new SqlDataAdapter(cmd8);
            DataTable dt = new DataTable();
            da.Fill(dt);


            gv_wo_overdue.DataSource = dt;
            gv_wo_overdue.DataBind();
        }

        finally
        {
           // conn.Close();
        }


        cmd9.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd9.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd9.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;


            SqlDataAdapter da = new SqlDataAdapter(cmd9);
            DataTable dt = new DataTable();
            da.Fill(dt);


            gv_untreated_rent.DataSource = dt;
            gv_untreated_rent.DataBind();
        }

        finally
        {
            conn.Close();
        }


        lease_expiration_link.NavigateUrl = "~/manager/lease/lease_expiration.aspx?home_id=" + home_id.ToString();
        mortgage_expiration_link.NavigateUrl = "~/manager/mortgage/mortgage_expiration.aspx?home_id=" + home_id.ToString();
        insurance_policy_expiration_link.NavigateUrl = "~/manager/insurancepolicy/insurance_policy_expiration.aspx?home_id=" + home_id.ToString();


        lease_pending_link.NavigateUrl = "~/manager/lease/lease_list.aspx?home_id=" + home_id.ToString()+"&categ=p";
        mortgage_pending_link.NavigateUrl = "~/manager/mortgage/mortgage_list.aspx?home_id=" + home_id.ToString() + "&categ=p";
        insurance_policy_pending_link.NavigateUrl = "~/manager/insurancepolicy/insurance_policy_list.aspx?home_id=" + home_id.ToString()+"&categ=p";


    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetPriority(int priority_id)
    {
        string priority = "";

        switch (priority_id)
        {
            case 1:
                priority = Resources.Resource.lbl_urgent;
                break;
            case 2:
                priority = Resources.Resource.lbl_high;
                break;
            case 3:
                priority = Resources.Resource.lbl_medium;
                break;

            case 4:
                priority = Resources.Resource.lbl_low;
                break;
        }

        return priority;
    }




    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected string GetStatus(int status_id)
    {
        string status = "";

        switch (status_id)
        {
            case 1:
                status = Resources.Resource.lbl_pending;
                break;
            case 2:
                status = Resources.Resource.lbl_in_progress;
                break;
            case 3:
                status = Resources.Resource.lbl_completed;
                break;

            case 4:
                status = Resources.Resource.lbl_closed;
                break;
        }

        return status;
    }

   
    /// <summary>
    /// 
    /// </summary>
    /// <param name="amount_of_warning_sent"></param>
    /// <returns></returns>
    protected string Get_AmountOfWarningSent(int amount_of_warning_sent)
    {

        string warning_sent = "";


        if (amount_of_warning_sent == 0)
        {
            warning_sent = Resources.Resource.lbl_none;
        }

        if( amount_of_warning_sent == 1)
        {
            warning_sent = Resources.Resource.lbl_first_notice;
        }

        if (amount_of_warning_sent == 2)
        {
            warning_sent = Resources.Resource.lbl_second_notice;
        }

        if (amount_of_warning_sent == 3)
        {
            warning_sent = Resources.Resource.lbl_third_notice;
        }

        return warning_sent;
    }
}
