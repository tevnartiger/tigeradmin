﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : feb 29, 2008
/// </summary>

public partial class manager_Financial_financial_scenario : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!(Page.IsPostBack))
        {

            reg_name.ValidationExpression = RegEx.getText();
            
            
            DateTime date = new DateTime();
            date = DateTime.Now;

            // default Year of scenario is the current year
            ddl_year.SelectedValue = date.Year.ToString();

          tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
          int home_count = h.getHomeCount(Convert.ToInt32(Session["schema_id"]));
          string link_to_unit = "";
          if (home_count > 0)
          {
              int home_id = h.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));
              
                tiger.Financial f = new tiger.Financial(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
                rNumberUnitbyBedroomNumber.DataSource = f.getNumberUnitbyBedroomNumber(home_id);
                rNumberUnitbyBedroomNumber.DataBind();

                rNumberComUnit.DataSource = f.getNumberComUnit(Convert.ToInt32(Session["schema_id"]),home_id);
                rNumberComUnit.DataBind();
  

              ddl_home_id.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
              ddl_home_id.SelectedValue = Convert.ToString(home_id);
              ddl_home_id.DataBind();



              //To view the address of the property

              tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
              rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
              rhome_view.DataBind();



             


          }
          // if ther is no home

          else
          {
              //  txt_message.InnerHtml = "There is no property -- Add a property";

              //  txt_link.InnerHtml = home_count + " <a href='home_add.aspx'>Add Home</a>&nbsp;&nbsp;<a href='owner_add.aspx'>Add Owner</a>&nbsp;&nbsp;<a href='janitor_add.aspx'>Add Janitor</a>&nbsp;&nbsp;";
          }

        }

    }

    protected void ddl_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {


        tiger.Financial f = new tiger.Financial(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        rNumberUnitbyBedroomNumber.DataSource = f.getNumberUnitbyBedroomNumber(Convert.ToInt32(ddl_home_id.SelectedValue));
        rNumberUnitbyBedroomNumber.DataBind();


        rNumberComUnit.DataSource = f.getNumberComUnit(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
        rNumberComUnit.DataBind();

        //To view the address of the property

        tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_id.SelectedValue));
        rhome_view.DataBind();







        //******************************************************************************************
        tbx_name.Text = "";

        lbl_pgi_m.Text = "";
        lbl_pgi_y.Text = "";

        // get the income for parking, laundry and storage
        lbl_parking_y.Text = "";
        lbl_storage_y.Text = "";
        lbl_laundry_y.Text = "";

        lbl_garage_y.Text = "";
        lbl_vending_machine_y.Text = "";
        lbl_cash_machine_y.Text = "";
        lbl_other_y2.Text = "";



        // Income Loss , for vacancy
        lbl_vacancy_m.Text = "";
        lbl_vacancy_y.Text = "";


        // Income Loss , for bad debt allowance
        lbl_bda_m.Text = "";
        lbl_bda_y.Text = "";




        // total effective gross income for month and year
        lbl_egi_m.Text = "";
        lbl_egi_y.Text = "";


        lbl_electricity_m.Text = "";
        lbl_electricity_y.Text = "";

        // energy expenses
        lbl_energy_m.Text = "";
        lbl_energy_y.Text = "";

        // insurance expenses
        lbl_insurances_m.Text = "";
        lbl_insurances_y.Text = "";

        // janitor expenses
        lbl_janitor_m.Text = "";
        lbl_janitor_y.Text = "";

        // property taxes expenses
        lbl_taxes_m.Text = "";
        lbl_taxes_y.Text = "";

        // maintenance & repair expenses
        lbl_maintenance_repair_m.Text = "";
        lbl_maintenance_repair_y.Text = "";

        // school taxes expenses
        lbl_school_taxes_m.Text = "";
        lbl_school_taxes_y.Text = "";

        // management expenses
        lbl_management_m.Text = "";
        lbl_management_y.Text = "";

        // advertising expenses
        lbl_advertising_m.Text = "";
        lbl_advertising_y.Text = "";

        // legal expenses
        lbl_legal_m.Text = "";
        lbl_legal_y.Text = "";


        // accounting expenses
        lbl_accounting_m.Text = "";
        lbl_accounting_y.Text = "";

        // Other expenses
        lbl_other_m.Text = "";
        lbl_other_y.Text = "";




        lbl_total_expenses_m.Text = "";
        lbl_total_expenses_y.Text = "";


        lbl_total_exp_percent.Text = "";



        // Net gross exploiation income
        lbl_net_operating_inc_y.Text = "";

        // Generated liquidity ( before income taxes )
        lbl_liquidity_y.Text = "";

        // Generated liquidity + capitalisation
        lbl_liquidity_capitalisation_y.Text = "";

        //Liquidity + Cap. +Added value
        lbl_liq_cap_value_y.Text = "";

        //******************************************************************************************

    }
    protected void Caculate_Click(object sender, EventArgs e)
    {

        decimal month_total = 0;
        decimal year_total = 0;
        decimal vacancy_rate = 0;
        decimal bad_debt_allowance_rate = 0;

        decimal electricity_rate = 0;
        decimal energy_rate = 0;
        decimal insurance_rate = 0;
        decimal janitor_rate = 0;
        decimal property_taxe_rate = 0;
        decimal maintenance_repair_rate = 0;
        decimal school_taxe_rate = 0;
        decimal management_rate = 0;
        decimal advertising_rate = 0;
        decimal legal_rate = 0;
        decimal accounting_rate = 0;
        decimal other_rate = 0;


        decimal total_expenses_m = 0;
        decimal total_expenses_y = 0;




        for (int i = 0; i < rNumberUnitbyBedroomNumber.Items.Count; i++)
        {
            TextBox bedrooms_total_rent_m = (TextBox)rNumberUnitbyBedroomNumber.Items[i].FindControl("tbx_bedrooms_total_rent_m");
            Label lbl_bedrooms_total_rent_y = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_y");

            lbl_bedrooms_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text))) * 12);

            month_total = month_total + Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text));
        }


        for (int i = 0; i < rNumberComUnit.Items.Count; i++)
        {
            TextBox commercial_total_rent_m = (TextBox)rNumberComUnit.Items[i].FindControl("tbx_commercial_total_rent_m");
            Label lbl_commercial_total_rent_y = (Label)rNumberComUnit.Items[i].FindControl("lbl_commercial_total_rent_y");

            lbl_commercial_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(commercial_total_rent_m.Text)) * 12);

            month_total = month_total + Convert.ToDecimal(commercial_total_rent_m.Text);
        }

        month_total = month_total + Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text));        

        year_total = month_total * 12;

        lbl_pgi_m.Text = String.Format("{0:0.00}", month_total);
        lbl_pgi_y.Text = String.Format("{0:0.00}", year_total);

        // get the income for parking, laundry and storage, vending & cash machine + other
        lbl_parking_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text)) * 12));
        lbl_storage_y.Text =  String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text))* 12));
        lbl_laundry_y.Text =  String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text)) * 12));

        lbl_garage_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text)) * 12));
        lbl_vending_machine_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text)) * 12));
        lbl_cash_machine_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text)) * 12));
        lbl_other_y2.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text)) * 12));

        // get the vacancy_rate and bad_debt_allowance_rate
        vacancy_rate =  Convert.ToDecimal(RegEx.getMoney(tbx_vacancy_rate.Text)) / 100;
        bad_debt_allowance_rate = Convert.ToDecimal(RegEx.getMoney(tbx_bda_rate.Text)) / 100;

         
      
        // Income Loss , for vacancy
        lbl_vacancy_m.Text = String.Format("{0:0.00}",(vacancy_rate * month_total));
        lbl_vacancy_y.Text = String.Format("{0:0.00}", (vacancy_rate * year_total));


        // Income Loss , for bad debt allowance
        lbl_bda_m.Text = String.Format("{0:0.00}",(bad_debt_allowance_rate * month_total));
        lbl_bda_y.Text = String.Format("{0:0.00}", (bad_debt_allowance_rate * year_total));


        // total effective gross income for month and year
        lbl_egi_m.Text = String.Format("{0:0.00}",(month_total - Convert.ToDecimal(lbl_vacancy_m.Text) - Convert.ToDecimal(lbl_bda_m.Text)));
        lbl_egi_y.Text = String.Format("{0:0.00}", (year_total - Convert.ToDecimal(lbl_vacancy_y.Text) - Convert.ToDecimal(lbl_bda_y.Text)));


        /// get the rate of utilities , management ,advertisement , taxes etc...
        /// 
        electricity_rate = Convert.ToDecimal(RegEx.getMoney(tbx_electricity_m.Text)) / 100;
        energy_rate = Convert.ToDecimal(RegEx.getMoney(tbx_energy_m.Text)) / 100;
        insurance_rate = Convert.ToDecimal(RegEx.getMoney(tbx_insurances_m.Text)) / 100;
        
        janitor_rate = Convert.ToDecimal(RegEx.getMoney(tbx_janitor_m.Text)) / 100;
        property_taxe_rate = Convert.ToDecimal(RegEx.getMoney(tbx_taxes_m.Text)) / 100;
        maintenance_repair_rate = Convert.ToDecimal(RegEx.getMoney(tbx_maintenance_repair_m.Text)) / 100;

        school_taxe_rate = Convert.ToDecimal(RegEx.getMoney(tbx_school_taxes_m.Text)) / 100;
        management_rate = Convert.ToDecimal(RegEx.getMoney(tbx_management_m.Text)) / 100;
        advertising_rate = Convert.ToDecimal(RegEx.getMoney(tbx_advertising_m.Text)) / 100;
        legal_rate = Convert.ToDecimal(RegEx.getMoney(tbx_legal_m.Text)) / 100;
        accounting_rate = Convert.ToDecimal(RegEx.getMoney(tbx_accounting_m.Text)) / 100;
        other_rate = Convert.ToDecimal(RegEx.getMoney(tbx_other_m.Text)) / 100;
       
        // eletricity expenses
        lbl_electricity_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text) * electricity_rate)) ;
        lbl_electricity_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * electricity_rate)) ;

        // energy expenses
        lbl_energy_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text) * energy_rate));
        lbl_energy_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * energy_rate));

       // insurance expenses
        lbl_insurances_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text) * insurance_rate));
        lbl_insurances_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * insurance_rate));

        // janitor expenses
        lbl_janitor_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text) * janitor_rate));
        lbl_janitor_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * janitor_rate));

        // property taxes expenses
        lbl_taxes_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text) * property_taxe_rate));
        lbl_taxes_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * property_taxe_rate));

        // maintenance & repair expenses
        lbl_maintenance_repair_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text) * maintenance_repair_rate));
        lbl_maintenance_repair_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * maintenance_repair_rate));

        // school taxes expenses
        lbl_school_taxes_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text) * school_taxe_rate));
        lbl_school_taxes_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * school_taxe_rate));

        // management expenses
        lbl_management_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text) * management_rate));
        lbl_management_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * management_rate));

        // advertising expenses
        lbl_advertising_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text) * advertising_rate));
        lbl_advertising_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * advertising_rate));

        // legal expenses
        lbl_legal_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text) * legal_rate));
        lbl_legal_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * legal_rate));


        // accounting expenses
        lbl_accounting_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text) * accounting_rate));
        lbl_accounting_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * accounting_rate));

        // Other expenses
        lbl_other_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text) * other_rate));
        lbl_other_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * other_rate));



        // Montly Gross total expenses



        total_expenses_m = Convert.ToDecimal(lbl_electricity_m.Text) + Convert.ToDecimal(lbl_energy_m.Text) +
                                   Convert.ToDecimal(lbl_insurances_m.Text) + Convert.ToDecimal(lbl_janitor_m.Text) + Convert.ToDecimal(lbl_taxes_m.Text) +
                                   Convert.ToDecimal(lbl_maintenance_repair_m.Text) + Convert.ToDecimal(lbl_school_taxes_m.Text) + Convert.ToDecimal(lbl_management_m.Text) + Convert.ToDecimal(lbl_advertising_m.Text) +
                                  Convert.ToDecimal(lbl_legal_m.Text) + Convert.ToDecimal(lbl_accounting_m.Text) +Convert.ToDecimal(lbl_other_m.Text);

       total_expenses_y = Convert.ToDecimal(lbl_electricity_y.Text) + Convert.ToDecimal(lbl_energy_y.Text) +
                                    Convert.ToDecimal(lbl_insurances_y.Text) + Convert.ToDecimal(lbl_janitor_y.Text) + Convert.ToDecimal(lbl_taxes_y.Text) +
                                    Convert.ToDecimal(lbl_maintenance_repair_y.Text) + Convert.ToDecimal(lbl_school_taxes_y.Text) + Convert.ToDecimal(lbl_management_y.Text) + Convert.ToDecimal(lbl_advertising_y.Text) +
                                   Convert.ToDecimal(lbl_legal_y.Text) + Convert.ToDecimal(lbl_accounting_y.Text) + Convert.ToDecimal(lbl_other_y.Text);

        lbl_total_expenses_m.Text = String.Format("{0:0.00}",(total_expenses_m));
        lbl_total_expenses_y.Text = String.Format("{0:0.00}",(total_expenses_y));


        lbl_total_exp_percent.Text = String.Format("{0:0.00}",((electricity_rate + energy_rate + insurance_rate + janitor_rate + property_taxe_rate + maintenance_repair_rate + school_taxe_rate +
                                      management_rate + advertising_rate + legal_rate + accounting_rate + other_rate) * 100));





       double  property_value = Convert.ToDouble(RegEx.getMoney(tbx_home_value.Text));

        if (tbx_added_value_y.Text == "")
        {
            tbx_added_value_y.Text = String.Format("{0:0.00}", (property_value * (Convert.ToDouble(RegEx.getMoney(tbx_added_value.Text)) / 100)));
            property_value = property_value * ((Convert.ToDouble(RegEx.getMoney(tbx_added_value.Text)) / 100) + 1);


        }
        else
        {
            tbx_added_value_y.Text = String.Format("{0:0.00}", (property_value * (Convert.ToDouble(RegEx.getMoney(tbx_added_value.Text)) / 100)));
            property_value = property_value + Convert.ToDouble(RegEx.getMoney(tbx_added_value_y.Text));
        }



        // Net gross exploiation income
        lbl_net_operating_inc_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) - Convert.ToDecimal(lbl_total_expenses_y.Text)));

        // Generated liquidity ( before income taxes )
        lbl_liquidity_y.Text =  String.Format("{0:0.00}",(Convert.ToDecimal(lbl_net_operating_inc_y.Text) - Convert.ToDecimal(RegEx.getMoney(tbx_ads_y.Text))));

        // Generated liquidity + capitalisation
        lbl_liquidity_capitalisation_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_liquidity_y.Text) + Convert.ToDecimal(RegEx.getMoney(tbx_capitalisation_y.Text))));

        //Liquidity + Cap. +Added value
        lbl_liq_cap_value_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_liquidity_capitalisation_y.Text) + Convert.ToDecimal(RegEx.getMoney(tbx_added_value_y.Text))));


        lbl_appartment_cost.Text = "";
        lbl_mbre.Text = "";
        lbl_rde.Text = "";
        lbl_tmo.Text = "";
        lbl_trn.Text = "";
        lbl_mrn.Text = "";

        lbl_rcd.Text = "";
    }
    protected void btn_calculate_expenses_Click(object sender, EventArgs e)
    {
        decimal month_total = 0;
        decimal year_total = 0;
        decimal vacancy_rate = 0;
        decimal bad_debt_allowance_rate = 0;

        decimal electricity_rate = 0;
        decimal energy_rate = 0;
        decimal insurance_rate = 0;
        decimal janitor_rate = 0;
        decimal property_taxe_rate = 0;
        decimal maintenance_repair_rate = 0;
        decimal school_taxe_rate = 0;
        decimal management_rate = 0;
        decimal advertising_rate = 0;
        decimal legal_rate = 0;
        decimal accounting_rate = 0;
        decimal other_rate = 0;


        decimal total_expenses_m = 0;
        decimal total_expenses_y = 0;
        double number_of_unit = 0;


        //GO TROUGH THE REPEATER

        for (int i = 0; i < rNumberUnitbyBedroomNumber.Items.Count; i++)
        {
            TextBox bedrooms_total_rent_m = (TextBox)rNumberUnitbyBedroomNumber.Items[i].FindControl("tbx_bedrooms_total_rent_m");
            Label lbl_bedrooms_total_rent_y = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_y");
            HiddenField h_number_of_unit = (HiddenField)rNumberUnitbyBedroomNumber.Items[i].FindControl("h_number_of_unit");

            lbl_bedrooms_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text)) * 12));

            month_total = month_total + Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text));

            number_of_unit = number_of_unit + Convert.ToDouble(h_number_of_unit.Value);
        }


        for (int i = 0; i < rNumberComUnit.Items.Count; i++)
        {
            TextBox commercial_total_rent_m = (TextBox)rNumberComUnit.Items[i].FindControl("tbx_commercial_total_rent_m");
            Label lbl_commercial_total_rent_y = (Label)rNumberComUnit.Items[i].FindControl("lbl_commercial_total_rent_y");

            HiddenField h_number_of_unit = (HiddenField)rNumberComUnit.Items[i].FindControl("h_number_of_unit");

            lbl_commercial_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(commercial_total_rent_m.Text) * 12));

            month_total = month_total + Convert.ToDecimal(commercial_total_rent_m.Text);

            number_of_unit = number_of_unit + Convert.ToDouble(h_number_of_unit.Value);

        }


        h_total_number_of_unit.Value = number_of_unit.ToString();

        month_total = month_total + Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text))  +Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text)); 

        year_total = month_total * 12;

        lbl_pgi_m.Text = String.Format("{0:0.00}",month_total);
        lbl_pgi_y.Text = String.Format("{0:0.00}",year_total);

        // get the income for parking, laundry and storage
        lbl_parking_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text)) * 12));
        lbl_storage_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text)) * 12));
        lbl_laundry_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text)) * 12));

        lbl_garage_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text)) * 12));
        lbl_vending_machine_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text)) * 12));
        lbl_cash_machine_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text)) * 12));
        lbl_other_y2.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text)) * 12));


        // get the vacancy_rate and bad_debt_allowance_rate
        vacancy_rate = Convert.ToDecimal(RegEx.getMoney(tbx_vacancy_rate.Text)) / 100;
        bad_debt_allowance_rate = Convert.ToDecimal(RegEx.getMoney(tbx_bda_rate.Text)) / 100;

        // Income Loss , for vacancy
        lbl_vacancy_m.Text = String.Format("{0:0.00}",(vacancy_rate * month_total));
        lbl_vacancy_y.Text = String.Format("{0:0.00}",(vacancy_rate * year_total));


        // Income Loss , for bad debt allowance
        lbl_bda_m.Text = String.Format("{0:0.00}",(bad_debt_allowance_rate * month_total));
        lbl_bda_y.Text = String.Format("{0:0.00}",(bad_debt_allowance_rate * year_total));


        // total effective gross income for month and year
        lbl_egi_m.Text = String.Format("{0:0.00}",(month_total - Convert.ToDecimal(lbl_vacancy_m.Text)) - Convert.ToDecimal(RegEx.getMoney(lbl_bda_m.Text)));
        lbl_egi_y.Text = String.Format("{0:0.00}",(year_total - Convert.ToDecimal(lbl_vacancy_y.Text)) - Convert.ToDecimal(RegEx.getMoney(lbl_bda_y.Text)));


        /// get the rate of utilities , management ,advertisement , taxes etc...
        /// 
        electricity_rate = Convert.ToDecimal(RegEx.getMoney(tbx_electricity_m.Text)) / 100;
        energy_rate = Convert.ToDecimal(RegEx.getMoney(tbx_energy_m.Text)) / 100;
        insurance_rate = Convert.ToDecimal(RegEx.getMoney(tbx_insurances_m.Text)) / 100;

        janitor_rate = Convert.ToDecimal(RegEx.getMoney(tbx_janitor_m.Text)) / 100;
        property_taxe_rate = Convert.ToDecimal(RegEx.getMoney(tbx_taxes_m.Text)) / 100;
        maintenance_repair_rate = Convert.ToDecimal(RegEx.getMoney(tbx_maintenance_repair_m.Text)) / 100;

        school_taxe_rate = Convert.ToDecimal(RegEx.getMoney(tbx_school_taxes_m.Text)) / 100;
        management_rate = Convert.ToDecimal(RegEx.getMoney(tbx_management_m.Text)) / 100;
        advertising_rate = Convert.ToDecimal(RegEx.getMoney(tbx_advertising_m.Text)) / 100;
        legal_rate = Convert.ToDecimal(RegEx.getMoney(tbx_legal_m.Text)) / 100;
        accounting_rate = Convert.ToDecimal(RegEx.getMoney(tbx_accounting_m.Text)) / 100;
        other_rate = Convert.ToDecimal(RegEx.getMoney(tbx_other_m.Text)) / 100;

        // eletricity expenses
        lbl_electricity_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text)) * electricity_rate);
        lbl_electricity_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * electricity_rate));

        // energy expenses
        lbl_energy_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text)) * energy_rate);
        lbl_energy_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * energy_rate));

        // insurance expenses
        lbl_insurances_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text)) * insurance_rate);
        lbl_insurances_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * insurance_rate));

        // janitor expenses
        lbl_janitor_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text)) * janitor_rate);
        lbl_janitor_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * janitor_rate));

        // property taxes expenses
        lbl_taxes_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text)) * property_taxe_rate);
        lbl_taxes_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * property_taxe_rate));

        // maintenance & repair expenses
        lbl_maintenance_repair_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text)) * maintenance_repair_rate);
        lbl_maintenance_repair_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * maintenance_repair_rate));

        // school taxes expenses
        lbl_school_taxes_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text)) * school_taxe_rate);
        lbl_school_taxes_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * school_taxe_rate));

        // management expenses
        lbl_management_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text)) * management_rate);
        lbl_management_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * management_rate));

        // advertising expenses
        lbl_advertising_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text)) * advertising_rate);
        lbl_advertising_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * advertising_rate));

        // legal expenses
        lbl_legal_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text)) * legal_rate);
        lbl_legal_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * legal_rate));


        // accounting expenses
        lbl_accounting_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text)) * accounting_rate);
        lbl_accounting_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * accounting_rate));

        // Other expenses
        lbl_other_m.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_m.Text)) * other_rate);
        lbl_other_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) * other_rate));



        // Montly Gross total expenses

  
        total_expenses_m = Convert.ToDecimal(lbl_electricity_m.Text) + Convert.ToDecimal(lbl_energy_m.Text) +
                                   Convert.ToDecimal(lbl_insurances_m.Text) + Convert.ToDecimal(lbl_janitor_m.Text) + Convert.ToDecimal(lbl_taxes_m.Text) +
                                   Convert.ToDecimal(lbl_maintenance_repair_m.Text) + Convert.ToDecimal(lbl_school_taxes_m.Text) + Convert.ToDecimal(lbl_management_m.Text) + Convert.ToDecimal(lbl_advertising_m.Text) +
                                  Convert.ToDecimal(lbl_legal_m.Text) + Convert.ToDecimal(lbl_accounting_m.Text) + Convert.ToDecimal(lbl_other_m.Text);


        total_expenses_y = Convert.ToDecimal(lbl_electricity_y.Text) + Convert.ToDecimal(lbl_energy_y.Text) +
                                    Convert.ToDecimal(lbl_insurances_y.Text) + Convert.ToDecimal(lbl_janitor_y.Text) + Convert.ToDecimal(lbl_taxes_y.Text) +
                                    Convert.ToDecimal(lbl_maintenance_repair_y.Text) + Convert.ToDecimal(lbl_school_taxes_y.Text) + Convert.ToDecimal(lbl_management_y.Text) + Convert.ToDecimal(lbl_advertising_y.Text) +
                                   Convert.ToDecimal(lbl_legal_y.Text) + Convert.ToDecimal(lbl_accounting_y.Text) + Convert.ToDecimal(lbl_other_y.Text);

        lbl_total_expenses_m.Text = String.Format("{0:0.00}", total_expenses_m);
        lbl_total_expenses_y.Text = String.Format("{0:0.00}", total_expenses_y);

        // Net gross exploiation income
        lbl_net_operating_inc_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(lbl_egi_y.Text) - Convert.ToDecimal(lbl_total_expenses_y.Text)));


        lbl_total_exp_percent.Text = String.Format("{0:0.00}",((electricity_rate + energy_rate + insurance_rate + janitor_rate + property_taxe_rate + maintenance_repair_rate + school_taxe_rate +
                                   management_rate + advertising_rate + legal_rate + accounting_rate + other_rate) * 100));



        // Analysis of Return calculations

        double property_value = 0 ;
        double added_value = 0;
        double total_number_of_unit = Convert.ToDouble(h_total_number_of_unit.Value);
        double appartment_cost = 0;
        property_value = Convert.ToDouble(RegEx.getMoney(tbx_home_value.Text));

        if (tbx_added_value_y.Text == "")
        {
            tbx_added_value_y.Text = String.Format("{0:0.00}", (property_value * (Convert.ToDouble(RegEx.getMoney(tbx_added_value.Text)) / 100)));
            property_value = property_value * ((Convert.ToDouble(RegEx.getMoney(tbx_added_value.Text)) / 100) + 1);


        }
        else
        {
           tbx_added_value_y.Text = String.Format("{0:0.00}", (property_value * (Convert.ToDouble(RegEx.getMoney(tbx_added_value.Text)) / 100)));
            property_value = property_value + Convert.ToDouble(RegEx.getMoney(tbx_added_value_y.Text));
        }

        appartment_cost = (property_value / total_number_of_unit);

       // lbl_appartment_cost.Text = appartment_cost.ToString();
        //lbl_mbre.Text = Convert.ToString((property_value / Convert.ToDouble(lbl_egi_y.Text)));
       // lbl_rde.Text = Convert.ToString((Convert.ToDouble(lbl_total_expenses_y.Text) / Convert.ToDouble(lbl_egi_y.Text)));


        // Net gross exploiation income
        lbl_net_operating_inc_y.Text = "";
        // Generated liquidity ( before income taxes )
        lbl_liquidity_y.Text = "";
        // Generated liquidity + capitalisation
        lbl_liquidity_capitalisation_y.Text = "";
        //Liquidity + Cap. +Added value
        lbl_liq_cap_value_y.Text = "";

        lbl_appartment_cost.Text = "";
        lbl_mbre.Text = "";
        lbl_rde.Text = "";
        lbl_tmo.Text = "";
        lbl_trn.Text = "";
        lbl_mrn.Text = "";
        lbl_rcd.Text = "";

      
    }
    protected void btn_calculate_egi_Click(object sender, EventArgs e)
    {
        decimal month_total = 0;
        decimal year_total = 0;
        decimal vacancy_rate = 0;
        decimal bad_debt_allowance_rate = 0;

       



        for (int i = 0; i < rNumberUnitbyBedroomNumber.Items.Count; i++)
        {
            TextBox bedrooms_total_rent_m = (TextBox)rNumberUnitbyBedroomNumber.Items[i].FindControl("tbx_bedrooms_total_rent_m");
            Label lbl_bedrooms_total_rent_y = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_y");

            lbl_bedrooms_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text)) * 12));

            month_total = month_total + Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text));
        }


        for (int i = 0; i < rNumberComUnit.Items.Count; i++)
        {
            TextBox commercial_total_rent_m = (TextBox)rNumberComUnit.Items[i].FindControl("tbx_commercial_total_rent_m");
            Label lbl_commercial_total_rent_y = (Label)rNumberComUnit.Items[i].FindControl("lbl_commercial_total_rent_y");

            lbl_commercial_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(commercial_total_rent_m.Text)) * 12));

            month_total = month_total + Convert.ToDecimal(RegEx.getMoney(commercial_total_rent_m.Text));

        }

        month_total = month_total + Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text)); 

        year_total = month_total * 12;

        lbl_pgi_m.Text = String.Format("{0:0.00}", month_total);
        lbl_pgi_y.Text = String.Format("{0:0.00}", year_total);

        // get the income for parking, laundry and storage
        lbl_parking_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text)) * 12));
        lbl_storage_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text)) * 12));
        lbl_laundry_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text)) * 12));

        lbl_garage_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text)) * 12));
        lbl_vending_machine_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text)) * 12));
        lbl_cash_machine_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text)) * 12));
        lbl_other_y2.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text)) * 12));

        // get the vacancy_rate and bad_debt_allowance_rate
        vacancy_rate = Convert.ToDecimal(RegEx.getMoney(tbx_vacancy_rate.Text)) / 100;
        bad_debt_allowance_rate = Convert.ToDecimal(RegEx.getMoney(tbx_bda_rate.Text)) / 100;

        // Income Loss , for vacancy
        lbl_vacancy_m.Text = String.Format("{0:0.00}",(vacancy_rate * month_total));
        lbl_vacancy_y.Text = String.Format("{0:0.00}",(vacancy_rate * year_total));


        // Income Loss , for bad debt allowance
        lbl_bda_m.Text = String.Format("{0:0.00}",(bad_debt_allowance_rate * month_total));
        lbl_bda_y.Text = String.Format("{0:0.00}",(bad_debt_allowance_rate * year_total));


        // total effective gross income for month and year
        lbl_egi_m.Text = String.Format("{0:0.00}",(month_total - Convert.ToDecimal(lbl_vacancy_m.Text) - Convert.ToDecimal(lbl_bda_m.Text)));
        lbl_egi_y.Text = String.Format("{0:0.00}",(year_total - Convert.ToDecimal(lbl_vacancy_y.Text) - Convert.ToDecimal(lbl_bda_y.Text)));



        // Net gross exploiation income
        lbl_net_operating_inc_y.Text = "";
        // Generated liquidity ( before income taxes )
        lbl_liquidity_y.Text = "";
        // Generated liquidity + capitalisation
        lbl_liquidity_capitalisation_y.Text = "";
        //Liquidity + Cap. +Added value
        lbl_liq_cap_value_y.Text = "";

        lbl_appartment_cost.Text = "";
        lbl_mbre.Text = "";
        lbl_rde.Text = "";
        lbl_tmo.Text = "";
        lbl_trn.Text = "";
        lbl_mrn.Text = "";
        lbl_rcd.Text = "";
    }
    protected void btn_calculate_pgi_Click(object sender, EventArgs e)
    {

        decimal month_total = 0;
        decimal year_total = 0;

        for (int i = 0; i < rNumberUnitbyBedroomNumber.Items.Count; i++)
        {
            TextBox bedrooms_total_rent_m = (TextBox)rNumberUnitbyBedroomNumber.Items[i].FindControl("tbx_bedrooms_total_rent_m");
            Label lbl_bedrooms_total_rent_y = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_y");

            lbl_bedrooms_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text)) * 12));

            month_total = month_total + Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text));
        }


        for (int i = 0; i < rNumberComUnit.Items.Count; i++)
        {
            TextBox commercial_total_rent_m = (TextBox)rNumberComUnit.Items[i].FindControl("tbx_commercial_total_rent_m");
            Label lbl_commercial_total_rent_y = (Label)rNumberComUnit.Items[i].FindControl("lbl_commercial_total_rent_y");

            lbl_commercial_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(commercial_total_rent_m.Text)) * 12);

            month_total = month_total + Convert.ToDecimal(commercial_total_rent_m.Text);

         
        }

        month_total = month_total + Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text)) + Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text))
                      + Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text)); 

        year_total = month_total * 12;

        lbl_pgi_m.Text = String.Format("{0:0.00}",(month_total));
        lbl_pgi_y.Text = String.Format("{0:0.00}",(year_total));

        // get the income for parking, laundry and storage
        lbl_parking_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text)) * 12));
        lbl_storage_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text)) * 12));
        lbl_laundry_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text)) * 12));

        lbl_garage_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text)) * 12));
        lbl_vending_machine_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text)) * 12));
        lbl_cash_machine_y.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text)) * 12));
        lbl_other_y2.Text = String.Format("{0:0.00}",(Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text)) * 12));



        // Net gross exploiation income
        lbl_net_operating_inc_y.Text = "";
        // Generated liquidity ( before income taxes )
        lbl_liquidity_y.Text = "";
        // Generated liquidity + capitalisation
        lbl_liquidity_capitalisation_y.Text = "";
        //Liquidity + Cap. +Added value
        lbl_liq_cap_value_y.Text = "";

        lbl_appartment_cost.Text = "";
        lbl_mbre.Text = "";
        lbl_rde.Text = "";
        lbl_tmo.Text = "";
        lbl_trn.Text = "";
        lbl_mrn.Text = "";
        lbl_rcd.Text = "";
    }
    protected void btn_save_Click(object sender, EventArgs e)
    {


        Page.Validate("vg_name");
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }


        String bedrooms_total_rent = "";
        String  number_of_unit = "";
        String  unit_bedroom_no = "";

        decimal commercial_total_rent = 0;
        int number_of_com_unit = 0;
       
        int number_of_insert = 0;
     

        for (int i = 0; i < rNumberUnitbyBedroomNumber.Items.Count; i++)
        {
            TextBox bedrooms_total_rent_m = (TextBox)rNumberUnitbyBedroomNumber.Items[i].FindControl("tbx_bedrooms_total_rent_m");
            HiddenField h_number_of_unit = (HiddenField)rNumberUnitbyBedroomNumber.Items[i].FindControl("h_number_of_unit");
            HiddenField h_unit_bedroom_no = (HiddenField)rNumberUnitbyBedroomNumber.Items[i].FindControl("h_unit_bedroom_number");


            bedrooms_total_rent = bedrooms_total_rent + bedrooms_total_rent_m.Text + "|"; 
            number_of_unit = number_of_unit + h_number_of_unit.Value + "|";
            unit_bedroom_no = unit_bedroom_no + h_unit_bedroom_no.Value + "|";
            
            number_of_insert++;
        }


        for (int i = 0; i < rNumberComUnit.Items.Count; i++)
        {
            TextBox commercial_total_rent_m = (TextBox)rNumberComUnit.Items[i].FindControl("tbx_commercial_total_rent_m");
            HiddenField h_number_of_com_unit = (HiddenField)rNumberComUnit.Items[i].FindControl("h_number_of_unit");
            
            commercial_total_rent = Convert.ToDecimal(RegEx.getMoney(commercial_total_rent_m.Text));
            number_of_com_unit = Convert.ToInt32(h_number_of_com_unit.Value);
        }


 
        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prMoneyFlowScenarioAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);

            // we replace the commas  by dots because SQL "CAST" will convert $100,00 by 100000.00
            // we cast the string "100,00" in money

            cmd.Parameters.Add("@bedrooms_total_rent", SqlDbType.VarChar, 80000).Value = bedrooms_total_rent.Replace(",", ".");
            cmd.Parameters.Add("@number_of_unit", SqlDbType.VarChar, 80000).Value = number_of_unit.Replace(",", ".");
            cmd.Parameters.Add("@unit_bedroom_no", SqlDbType.VarChar, 80000).Value = unit_bedroom_no.Replace(",", ".");
            cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;

            cmd.Parameters.Add("@commercial_total_rent", SqlDbType.Money).Value = commercial_total_rent;
            cmd.Parameters.Add("@number_of_com_unit", SqlDbType.Int).Value = number_of_com_unit;
                       
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_id.SelectedValue);
            cmd.Parameters.Add("@home_value", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_home_value.Text));
            cmd.Parameters.Add("@year", SqlDbType.Int).Value = Convert.ToInt32(ddl_year.SelectedValue);
            cmd.Parameters.Add("@name", SqlDbType.NVarChar, 500).Value = RegEx.getText(tbx_name.Text);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            
            cmd.Parameters.Add("@parking", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_parking_m.Text));
            cmd.Parameters.Add("@laundry", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_laundry_m.Text));
            cmd.Parameters.Add("@storage", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_storage_m.Text));

            cmd.Parameters.Add("@garage", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_garage_m.Text));
            cmd.Parameters.Add("@vending_machine", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_vending_machine_m.Text));
            cmd.Parameters.Add("@cash_machine", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_cash_machine_m.Text));
            cmd.Parameters.Add("@other", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_other_m2.Text));


            cmd.Parameters.Add("@liqcapvalue", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(lbl_liq_cap_value_y.Text));

            cmd.Parameters.Add("@ads", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_ads_y.Text.Replace(",", ".")));
            cmd.Parameters.Add("@capitalisation", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_capitalisation_y.Text));
            cmd.Parameters.Add("@added_value", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(tbx_added_value_y.Text));


            cmd.Parameters.Add("@vacancy_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_vacancy_rate.Text));
            cmd.Parameters.Add("@bda_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_bda_rate.Text));
            cmd.Parameters.Add("@electricity_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_electricity_m.Text));
            cmd.Parameters.Add("@energy_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_energy_m.Text));
            cmd.Parameters.Add("@insurance_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_insurances_m.Text));
            cmd.Parameters.Add("@janitor_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_janitor_m.Text));
            cmd.Parameters.Add("@property_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_taxes_m.Text));
            cmd.Parameters.Add("@maintenance_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_maintenance_repair_m.Text));
            cmd.Parameters.Add("@school_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_school_taxes_m.Text));
            cmd.Parameters.Add("@management_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_management_m.Text));
            cmd.Parameters.Add("@advertise_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_advertising_m.Text));
            cmd.Parameters.Add("@legal_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_legal_m.Text));
            cmd.Parameters.Add("@accounting_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_accounting_m.Text));
            cmd.Parameters.Add("@other_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(tbx_other_m.Text));

            //execute the insert
            cmd.ExecuteReader();

        }
        finally
        {
            conn.Close();
            if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 0)
            {
                lbl_success.Text = Resources.Resource.lbl_successfull_add;
            }
        }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {

        decimal month_total = 0;
        decimal number_of_unit = 0;

        //GO TROUGH THE REPEATER

        for (int i = 0; i < rNumberUnitbyBedroomNumber.Items.Count; i++)
        {
            TextBox bedrooms_total_rent_m = (TextBox)rNumberUnitbyBedroomNumber.Items[i].FindControl("tbx_bedrooms_total_rent_m");
            Label lbl_bedrooms_total_rent_y = (Label)rNumberUnitbyBedroomNumber.Items[i].FindControl("lbl_bedrooms_total_rent_y");
            HiddenField h_number_of_unit = (HiddenField)rNumberUnitbyBedroomNumber.Items[i].FindControl("h_number_of_unit");

            lbl_bedrooms_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text)) * 12));

            month_total = month_total + Convert.ToDecimal(RegEx.getMoney(bedrooms_total_rent_m.Text));

            number_of_unit = number_of_unit + Convert.ToDecimal(h_number_of_unit.Value);
        }




        for (int i = 0; i < rNumberComUnit.Items.Count; i++)
        {
            TextBox commercial_total_rent_m = (TextBox)rNumberComUnit.Items[i].FindControl("tbx_commercial_total_rent_m");
            Label lbl_commercial_total_rent_y = (Label)rNumberComUnit.Items[i].FindControl("lbl_commercial_total_rent_y");

            HiddenField h_number_of_unit = (HiddenField)rNumberComUnit.Items[i].FindControl("h_number_of_unit");

            lbl_commercial_total_rent_y.Text = String.Format("{0:0.00}", (Convert.ToDecimal(RegEx.getMoney(commercial_total_rent_m.Text)) * 12));

            month_total = month_total + Convert.ToDecimal(RegEx.getMoney(commercial_total_rent_m.Text));

            number_of_unit = number_of_unit + Convert.ToDecimal(h_number_of_unit.Value);

        }


        h_total_number_of_unit.Value = number_of_unit.ToString();

        // Analysis of Return calculations

        double property_value = 0;
        double added_value = 0;
        double total_number_of_unit = Convert.ToDouble(h_total_number_of_unit.Value);
        double appartment_cost = 0;
        property_value = Convert.ToDouble(RegEx.getMoney(tbx_home_value.Text));

        if (tbx_added_value_y.Text == "")
        {
            RegEx.getMoney(tbx_added_value_y.Text = String.Format("{0:0.00}", (property_value * (Convert.ToDouble(RegEx.getMoney(tbx_added_value.Text)) / 100))));
            property_value = property_value * ((Convert.ToDouble(RegEx.getMoney(tbx_added_value.Text)) / 100) + 1);


        }
        else
        {
            RegEx.getMoney(tbx_added_value_y.Text = String.Format("{0:0.00}", (property_value * (Convert.ToDouble(RegEx.getMoney(tbx_added_value.Text)) / 100))));
            property_value = property_value + Convert.ToDouble(RegEx.getMoney(tbx_added_value_y.Text));
        }

        
        appartment_cost = (property_value / total_number_of_unit);

        lbl_appartment_cost.Text = String.Format("{0:0.00}", appartment_cost);
        lbl_mbre.Text = String.Format("{0:0.00}",((property_value / Convert.ToDouble(RegEx.getMoney(lbl_egi_y.Text)))));
        lbl_rde.Text = String.Format("{0:0.00}",((Convert.ToDouble(RegEx.getMoney(lbl_total_expenses_y.Text)) / Convert.ToDouble(RegEx.getMoney(lbl_egi_y.Text)))));

        lbl_tmo.Text = String.Format("{0:0.00}",((Convert.ToDouble(RegEx.getMoney(lbl_total_expenses_y.Text)) + Convert.ToDouble(RegEx.getMoney(tbx_ads_y.Text))) / Convert.ToDouble(RegEx.getMoney(lbl_pgi_y.Text))));

        lbl_trn.Text =  String.Format("{0:0.00}",(Convert.ToDouble(RegEx.getMoney(lbl_net_operating_inc_y.Text)) / property_value));

        lbl_mrn.Text = String.Format("{0:0.00}",(property_value / Convert.ToDouble(RegEx.getMoney(lbl_net_operating_inc_y.Text))));

        lbl_rcd.Text = String.Format("{0:0.00}", ((Convert.ToDouble(RegEx.getMoney(lbl_net_operating_inc_y.Text)) / Convert.ToDouble(RegEx.getMoney(tbx_ads_y.Text)))));
 
    }
}
