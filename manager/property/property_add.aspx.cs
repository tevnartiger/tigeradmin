﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.IO;


/// <summary>
/// Done by : Stanley Jocelyn
/// date    : may 10, 2008
/// </summary>
/// 
public partial class manager_property_property_add : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {


       // MultiView1.EnableViewState = false;
        //GET number of units in temporary units
        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        u.cleanTempUnit(Convert.ToInt32(Session["schema_id"]),15);
        int temp_table_unit_count = u.getTempUnitCount(Convert.ToInt32(Session["name_id"]), Convert.ToInt32(Session["schema_id"]), Convert.ToString(Session.SessionID));
        lbl_count.Text = temp_table_unit_count.ToString();
        //  lbl_count.Text = "ad";
        int temp_unit_max = u.getUnitAllowed(Convert.ToInt32(Session["schema_id"]));
        int temp_unit_used = u.getUnitMax(Convert.ToInt32(Session["schema_id"]));
        int temp_free_unit = (temp_unit_max - temp_unit_used);
       //check for overload 
        int unit_overload = (temp_unit_max - (temp_table_unit_count + temp_free_unit));
        //get total units in temp folder
        //get maximum units allowed
        lbl_max_unit.Text = temp_unit_max.ToString();
        //current
        lbl_free_unit.Text = temp_free_unit.ToString();
        //get maximum units available
        lbl_unit_used.Text = temp_unit_used.ToString()+"---free unit: "+temp_free_unit;

        if (temp_free_unit < 1)
        {
            panel_form.Enabled = false;
            panel_form.Visible = false;
           lbl_message.Text += "You have more units then your maximum allowed. You must remove some units";
        }
           //  Server.Transfer("property_add.aspx?nomorefreeunits", false);
         //   Response.Redirect("http://yahoo.com");

        if (unit_overload < 0)
        {
            Server.Transfer("property_add.aspx?overload", false);
        }
        if (Convert.ToInt32(temp_free_unit) == 0)
        {
            panel_form.Visible = false;
            lb_more_unit.Enabled = false;
            lb_more_unit.Visible = false;
            lbl_message.Text = "No More units available to add. Add <a href='more_units.aspx'>MORE UNITS</a>";
        }

        //if MultiView1.GetActiveView
        //panel to open
        if (Convert.ToInt32(temp_free_unit) >= 1)
            panel_1.Visible = true;

        if (Convert.ToInt32(temp_free_unit) >= 1)
            panel_1.Visible = true;

        if (Convert.ToInt32(temp_free_unit) >= 2)
            panel_2.Visible = true;

        if (Convert.ToInt32(temp_free_unit) >= 3)
            panel_3.Visible = true;

        if (Convert.ToInt32(temp_free_unit) >= 4)
            panel_4.Visible = true;

        if (Convert.ToInt32(temp_free_unit) >= 4)
            panel_4.Visible = true;

        if (Convert.ToInt32(temp_free_unit) >= 5)
            panel_5.Visible = true;
        if (Convert.ToInt32(temp_free_unit) >= 5)
            panel_5.Visible = true;

        if (Convert.ToInt32(temp_free_unit) >= 6)
            panel_6.Visible = true;

        if (Convert.ToInt32(temp_free_unit) >= 7)
            panel_7.Visible = true;
        
        if (Convert.ToInt32(temp_free_unit) >= 8)
            panel_8.Visible = true;
        
        if (Convert.ToInt32(temp_free_unit) >= 9)
            panel_9.Visible = true;

        if (Convert.ToInt32(temp_free_unit) >= 10)
            panel_10.Visible = true;




        if (!Page.IsPostBack)
        {
            // txt.Text = Session.SessionID.ToString();
            SetDefaultView();
            string temp_name = RegEx.getName();
            string temp_address = RegEx.getAddress();
            string temp_alphanumeric = RegEx.getAlphaNumeric();

            reg_home_name.ValidationExpression = temp_name;
            reg_home_addr_no.ValidationExpression = temp_address;
            reg_home_addr_street.ValidationExpression = temp_address;
            reg_home_city.ValidationExpression = temp_address;

            //GET number of units in temporary units
            /*       tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                   lbl_count.Text = Convert.ToString(u.getTempUnitCount(Convert.ToInt32(Session["name_id"]), Convert.ToInt32(Session["schema_id"]),Convert.ToString(Session.SessionID)));
                   lbl_count.Text = "ad";
               */
            /*reg_login_user.ValidationExpression = temp_name;
          reg_login_pwd.ValidationExpression = RegEx.getPwd();
          reg_retype_login_pwd.ValidationExpression = temp_pwd;*/

        }


    }

    /// <summary>
    /// 
    /// </summary>
    private void SetDefaultView()
    {
        MultiView1.ActiveViewIndex = 0;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_continu_Click(object sender, EventArgs e)
    {
        lbl_home_name.Text = home_name.Text;
        MultiView1.ActiveViewIndex = 1;

    }
    protected void lb_more_unit_Click(object sender, EventArgs e)
    {
        MultiView1.EnableViewState = false;
        MultiView1.ActiveViewIndex = 1;
    }
    protected void btn_list_Click(object sender, EventArgs e)
    {
        int temp_name_id = Convert.ToInt32(Session["name_id"]);
        int temp_schema_id = Convert.ToInt32(Session["schema_id"]);// Session["schema_id"]);
        string temp_page_session = Convert.ToString(Session.SessionID);


        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
       // u.cleanTempUnit(temp_schema_id);
        int count = (Convert.ToInt32(u.getTempUnitCount(Convert.ToInt32(Session["name_id"]), Convert.ToInt32(Session["schema_id"]), Convert.ToString(Session.SessionID))));
        //add new rows if there is some available
        if ((Convert.ToInt32(tbx_door_no1.Text.Length) > 0))
        {   //1st
            u.addTempUnit(temp_name_id, temp_schema_id, temp_page_session, tbx_door_no1.Text,
           Convert.ToChar(ddl_unit_type1.SelectedValue), Convert.ToInt32(ddl_nb_bedrooms1.Text), Convert.ToInt32(ddl_nb_bathrooms1.Text), Convert.ToInt32(ddl_unit_level1.Text), Convert.ToInt32(ddl_max_tenants1.Text));
            //2nd

            if ((Convert.ToInt32(tbx_door_no2.Text.Length) > 0))
            {
                u.addTempUnit(temp_name_id, temp_schema_id, temp_page_session,RegEx.getText(tbx_door_no2.Text),
                Convert.ToChar(ddl_unit_type2.SelectedValue), Convert.ToInt32(ddl_nb_bedrooms2.Text),
                Convert.ToInt32(ddl_nb_bathrooms2.Text), Convert.ToInt32(ddl_unit_level2.Text),
                Convert.ToInt32(ddl_max_tenants2.Text));
            }
            //3rd
            if ((Convert.ToInt32(tbx_door_no3.Text.Length) > 0))
            {
                u.addTempUnit(temp_name_id, temp_schema_id, temp_page_session, RegEx.getText(tbx_door_no3.Text),
                Convert.ToChar(ddl_unit_type3.SelectedValue), Convert.ToInt32(ddl_nb_bedrooms3.Text),
                Convert.ToInt32(ddl_nb_bathrooms3.Text), Convert.ToInt32(ddl_unit_level3.Text),
                Convert.ToInt32(ddl_max_tenants3.Text));
            }
            //4rd
            if ((Convert.ToInt32(tbx_door_no4.Text.Length) > 0))
            {
                u.addTempUnit(temp_name_id, temp_schema_id, temp_page_session, RegEx.getText(tbx_door_no4.Text),
                Convert.ToChar(ddl_unit_type4.SelectedValue), Convert.ToInt32(ddl_nb_bedrooms4.Text),
                Convert.ToInt32(ddl_nb_bathrooms4.Text), Convert.ToInt32(ddl_unit_level4.Text),
                Convert.ToInt32(ddl_max_tenants4.Text));
            }
            //5th
            if ((Convert.ToInt32(tbx_door_no5.Text.Length) > 0))
            {
                u.addTempUnit(temp_name_id, temp_schema_id, temp_page_session, RegEx.getText(tbx_door_no5.Text),
                Convert.ToChar(ddl_unit_type5.SelectedValue), Convert.ToInt32(ddl_nb_bedrooms5.Text),
                Convert.ToInt32(ddl_nb_bathrooms5.Text), Convert.ToInt32(ddl_unit_level5.Text),
                Convert.ToInt32(ddl_max_tenants5.Text));
            }
            
            //6th
            if ((Convert.ToInt32(tbx_door_no6.Text.Length) > 0))
            {
                u.addTempUnit(temp_name_id, temp_schema_id, temp_page_session, RegEx.getText(tbx_door_no6.Text),
                Convert.ToChar(ddl_unit_type6.SelectedValue), Convert.ToInt32(ddl_nb_bedrooms6.Text),
                Convert.ToInt32(ddl_nb_bathrooms6.Text), Convert.ToInt32(ddl_unit_level6.Text),
                Convert.ToInt32(ddl_max_tenants6.Text));
            }
            //7th
            if ((Convert.ToInt32(tbx_door_no7.Text.Length) > 0))
            {
                u.addTempUnit(temp_name_id, temp_schema_id, temp_page_session, RegEx.getText(tbx_door_no7.Text),
                Convert.ToChar(ddl_unit_type7.SelectedValue), Convert.ToInt32(ddl_nb_bedrooms7.Text),
                Convert.ToInt32(ddl_nb_bathrooms7.Text), Convert.ToInt32(ddl_unit_level7.Text),
                Convert.ToInt32(ddl_max_tenants7.Text));
            }
            //8th
            if ((Convert.ToInt32(tbx_door_no8.Text.Length) > 0))
            {
                u.addTempUnit(temp_name_id, temp_schema_id, temp_page_session, RegEx.getText(tbx_door_no8.Text),
                   Convert.ToChar(ddl_unit_type8.SelectedValue), Convert.ToInt32(ddl_nb_bedrooms8.Text),
                   Convert.ToInt32(ddl_nb_bathrooms8.Text), Convert.ToInt32(ddl_unit_level8.Text),
                   Convert.ToInt32(ddl_max_tenants8.Text));
            }
            //9th
            if ((Convert.ToInt32(tbx_door_no9.Text.Length) > 0))
            {
                u.addTempUnit(temp_name_id, temp_schema_id, temp_page_session, RegEx.getText(tbx_door_no9.Text),
                   Convert.ToChar(ddl_unit_type9.SelectedValue), Convert.ToInt32(ddl_nb_bedrooms9.Text),
                   Convert.ToInt32(ddl_nb_bathrooms9.Text), Convert.ToInt32(ddl_unit_level9.Text),
                   Convert.ToInt32(ddl_max_tenants9.Text));
            }
            //10th
            if ((Convert.ToInt32(tbx_door_no10.Text.Length) > 0))
            {
                u.addTempUnit(temp_name_id, temp_schema_id, temp_page_session, RegEx.getText(tbx_door_no10.Text),
                   Convert.ToChar(ddl_unit_type10.SelectedValue), Convert.ToInt32(ddl_nb_bedrooms10.Text),
                   Convert.ToInt32(ddl_nb_bathrooms10.Text), Convert.ToInt32(ddl_unit_level10.Text),
                   Convert.ToInt32(ddl_max_tenants10.Text));
            }
            gv_list.DataSource = u.getTempUnitList(temp_name_id, Convert.ToInt32(Session["schema_id"]), temp_page_session);
            gv_list.DataBind();
            MultiView1.ActiveViewIndex = 2;
        }

        else
        {
            lbl_message.Text += "Failed";
            lbl_message.Text += "<br/><br/>You need to have inputed at least 1 unit";
            MultiView1.ActiveViewIndex = 1;

        }


    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {

        Page.Validate();
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        bool status_add_user = false;
         
        tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        int temp_unit_max = u.getUnitAllowed(Convert.ToInt32(Session["schema_id"]));
        int temp_unit_used = u.getUnitMax(Convert.ToInt32(Session["schema_id"]));
        int temp_free_unit = (temp_unit_max - temp_unit_used);
      //tunit must be greater then 1 or there is tampering

        if (temp_free_unit > 0)
        {
            //get home_id from property add
            int temp_home_id = u.addUnitProperty(Session["schema_id"].ToString(), home_name.Text, home_addr_no.Text, home_addr_street.Text, home_city.Text, RegEx.getText(home_desc.Text));
            //   lbl_message.Text += "TEMP_HOME_ID VALUE : " + temp_home_id + "<br/>";


            //-----------  add picture ( image )modification  begin -- Stanley Jocelyn ----------------//
          
            string filename = Path.GetFileName(FileUpload1.PostedFile.FileName);
            int filesize = 0;
            if (FileUpload1.HasFile)
            {
             
                Stream stream = FileUpload1.PostedFile.InputStream;
                filesize = FileUpload1.PostedFile.ContentLength;
                byte[] filedata = new byte[filesize];
                stream.Read(filedata, 0, filesize);

                int success = 0;
                string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

                tiger.Media upload = new tiger.Media(strconn);

                success = upload.HomeImageUpdate(filedata, Convert.ToInt32(Session["schema_id"]), temp_home_id,
                                            Convert.ToInt32(Session["name_id"]), Request.UserHostAddress.ToString());
            }

            
            //-----------  add picture ( image )modification   end -- Stanley Jocelyn ----------------//





            if (temp_home_id > 0)
            {
                SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"].ToString());
                SqlCommand cmd = new SqlCommand("prTempUnitParamList", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    conn.Open();
                    //Add the params
                    cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                    cmd.Parameters.Add("@page_session", SqlDbType.NVarChar, 50).Value = Convert.ToString(Session.SessionID);
                    //maximum amount of units is 99 by choosing char(2)
                    cmd.Parameters.Add("@top", SqlDbType.NChar, 2).Value = temp_free_unit;

                    // lbl_message.Text += "i: " + i + "<br/>";
                    SqlDataReader dr = null;
                    dr = cmd.ExecuteReader();
                    //lbl_message.Text += "Count unit :  "+Convert.ToString(dr.FieldCount) + "<br/><br/>";
                    //lbl_message.Text += "prTempUnitList " + Convert.ToString(Session["name_id"]) + ", " + Convert.ToString(Session["schema_id"]) + ", " + Convert.ToString(Session.SessionID)+"<br/>";
                    while (dr.Read() == true)
                    {
                        //  lbl_message.Text += "Loop: " + i + "<br/>";

                        if (!u.addUnit(Convert.ToInt32(Session["schema_id"]), temp_home_id, Convert.ToString(dr["unit_door_no"]), Convert.ToChar(dr["unit_type"]),
                             Convert.ToInt32(dr["unit_bedroom_no"]), Convert.ToInt32(dr["unit_bathroom_no"]), Convert.ToInt32(dr["unit_level"]),
                             Convert.ToInt32(dr["unit_max_tenant"]), Convert.ToInt32(Session["name_id"]), Request.UserHostName.ToString()))
                        {
                            status_add_user = false;
                        }

                    }
                }
                finally
                {
                    conn.Close();
                }

            }
            else
            {
                lbl_message.Text += "Cannot complete this action";
            }
        }
          
        //now remove units in temp

        u.cleanTempUnit(Convert.ToInt32(Session["schema_id"]), 0);
    }

    protected void btn_back_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
}
 
