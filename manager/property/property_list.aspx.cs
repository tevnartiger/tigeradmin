using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date   : unkown , 2008
/// </summary>

public partial class property_property_list : BasePage
{
   

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            tiger.Home h = new tiger.Home(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            DataTable dth= h.getHomeList(Convert.ToInt32(Session["schema_id"]));
            if(dth != null && dth.Rows.Count > 0)
            {
                dghome_list.DataSource = dth;
                dghome_list.DataBind();
            }
            else
            {
                AlertMessageusercontrol.ShowWarning("There are no homes listed! Add a new home");
            }

            
        }
        

    }


    protected void dghome_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
        tiger.Home h = new tiger.Home(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        dghome_list.PageIndex = e.NewPageIndex;
        dghome_list.DataSource =  h.getHomeList(Convert.ToInt32(Session["schema_id"]));
        dghome_list.DataBind();
    }
    protected void dghome_list_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
