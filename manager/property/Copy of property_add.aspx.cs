﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;


/// <summary>
/// Done by : Stanley Jocelyn
/// date    : may 10, 2008
/// </summary>
/// 
public partial class manager_property_property_add : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            txt.Text = Session.SessionID.ToString();
            SetDefaultView();
            string temp_name = RegEx.getName();
            string temp_address = RegEx.getAddress();
            string temp_alphanumeric = RegEx.getAlphaNumeric();

            reg_home_name.ValidationExpression = temp_name;
            reg_home_addr_no.ValidationExpression = temp_address;
            reg_home_addr_street.ValidationExpression = temp_address;
            reg_home_city.ValidationExpression = temp_address;

            /*reg_login_user.ValidationExpression = temp_name;
            reg_login_pwd.ValidationExpression = RegEx.getPwd();
            reg_retype_login_pwd.ValidationExpression = temp_pwd;*/
     
        }


    }

    /// <summary>
    /// 
    /// </summary>
    private void SetDefaultView()
    {
        MultiView1.ActiveViewIndex = 0;
       
    }
  
/// <summary>
/// 
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
    protected void btn_continu_Click(object sender, EventArgs e)
    {

        MultiView1.ActiveViewIndex = 1;
        
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_continu_2_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;
        
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_previous_2_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_previous_1_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_continu_3_Click(object sender, EventArgs e)
    {



        SqlConnection conn8 = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        SqlCommand cmd8 = new SqlCommand("prHomeAdd", conn8);
        cmd8.CommandType = CommandType.StoredProcedure;


        conn8.Open();
        //Add the params
        cmd8.Parameters.Add("@new_home_id", SqlDbType.Int).Direction = ParameterDirection.Output;
        cmd8.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        //cmd8.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["home_id"]);
        cmd8.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
        cmd8.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
        cmd8.Parameters.Add("@home_name", SqlDbType.VarChar, 50).Value = home_name.Text.ToString();
        cmd8.Parameters.Add("@home_type", SqlDbType.VarChar, 50).Value = home_type.Text;
        cmd8.Parameters.Add("@home_style", SqlDbType.VarChar, 50).Value = home_style.Text;

        if (home_unit.Text == "")
            home_unit.Text = "0";
        cmd8.Parameters.Add("@home_unit", SqlDbType.Int).Value = Convert.ToInt32(home_unit.Text);

        if (home_floor.Text == "")
            home_floor.Text = "0";
        cmd8.Parameters.Add("@home_floor", SqlDbType.Int).Value = Convert.ToInt32(home_floor.Text);



        DateTime date_built = new DateTime();
        DateTime date_purchase = new DateTime();
        tiger.Date c = new tiger.Date();

        if (ddl_home_date_built_m.SelectedValue == "0" || ddl_home_date_built_d.SelectedValue == "0" || ddl_home_date_built_y.SelectedValue == "0")
            date_built = Convert.ToDateTime(c.DateCulture("1", "1", "1900", Convert.ToString(Session["_lastCulture"])));
        else
            date_built = Convert.ToDateTime(c.DateCulture(ddl_home_date_built_m.SelectedValue, ddl_home_date_built_d.SelectedValue, ddl_home_date_built_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        if (ddl_home_date_purchase_m.SelectedValue == "0" || ddl_home_date_purchase_d.SelectedValue == "0" || ddl_home_date_purchase_y.SelectedValue == "0")
            date_purchase = Convert.ToDateTime(c.DateCulture("1", "1", "1900", Convert.ToString(Session["_lastCulture"])));
        else
            date_purchase = Convert.ToDateTime(c.DateCulture(ddl_home_date_purchase_m.SelectedValue, ddl_home_date_purchase_d.SelectedValue, ddl_home_date_purchase_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

        cmd8.Parameters.Add("@home_date_built", SqlDbType.DateTime).Value = date_built;
        cmd8.Parameters.Add("@home_date_purchase", SqlDbType.DateTime).Value = date_purchase;



        if (home_purchase_price.Text == "")
            home_purchase_price.Text = "0";

        if (home_living_space.Text == "")
            home_living_space.Text = "0";

        if (home_land_size.Text == "")
            home_land_size.Text = "0";



        
        cmd8.Parameters.Add("@home_purchase_price", SqlDbType.Money).Value = Convert.ToDecimal(home_purchase_price.Text);
        cmd8.Parameters.Add("@home_living_space", SqlDbType.Int).Value = Convert.ToInt32(home_living_space.Text);
        cmd8.Parameters.Add("@home_land_size", SqlDbType.VarChar, 50).Value = home_land_size.Text;


        cmd8.Parameters.Add("@home_desc", SqlDbType.VarChar, 50).Value = home_desc.Text;
        cmd8.Parameters.Add("@home_addr_no", SqlDbType.VarChar, 50).Value = home_addr_no.Text;
        cmd8.Parameters.Add("@home_addr_street", SqlDbType.VarChar, 50).Value = home_addr_street.Text;

        cmd8.Parameters.Add("@home_pc", SqlDbType.VarChar, 25).Value = home_pc.Text;
        cmd8.Parameters.Add("@home_city", SqlDbType.VarChar, 50).Value = home_city.Text;
        cmd8.Parameters.Add("@home_district", SqlDbType.VarChar, 50).Value = home_district.Text;
        cmd8.Parameters.Add("@home_prov", SqlDbType.VarChar, 25).Value = home_prov.Text;
        cmd8.Parameters.Add("@home_localisation_certificat", SqlDbType.VarChar, 50).Value = home_localisation_certificat.Text;
        cmd8.Parameters.Add("@home_matricule", SqlDbType.VarChar, 50).Value = home_matricule.Text;
        cmd8.Parameters.Add("@home_register", SqlDbType.VarChar, 50).Value = home_register.Text;


        //*******************

       // cmd8.Parameters.Add("@home_potential_revenue", SqlDbType.Money).Value = Convert.ToDecimal(home_potential_revenue.Text);
       // cmd8.Parameters.Add("@home_expected_expenses", SqlDbType.Money).Value = Convert.ToDecimal(home_expected_expenses.Text);
       // cmd8.Parameters.Add("@home_net_expected_revenue", SqlDbType.Money).Value = Convert.ToDecimal(home_net_revenue.Text);
        //  cmd8.Parameters.Add("home_matricule", SqlDbType.VarChar, 50).Value = home_matricule.Text;

        cmd8.Parameters.Add("@home_dimension", SqlDbType.VarChar, 50).Value = home_dimension.Text;

        if (home_area.Text == "")
            home_area.Text = "0";

        cmd8.Parameters.Add("@home_area", SqlDbType.Int).Value = Convert.ToInt32(home_area.Text);

        cmd8.Parameters.Add("@home_land_dimension", SqlDbType.VarChar, 50).Value = home_land_dimension.Text;


        if (home_land_surface.Text == "")
            home_land_surface.Text = "0";

        cmd8.Parameters.Add("@home_land_surface", SqlDbType.Int).Value = Convert.ToInt32(home_land_surface.Text);


        cmd8.Parameters.Add("@home_habitable_surface_type", SqlDbType.Int).Value = Convert.ToInt32(ddl_habitable_surface_type.SelectedValue);
        cmd8.Parameters.Add("@home_land_surface_type", SqlDbType.Int).Value = Convert.ToInt32(ddl_land_surface_type.SelectedValue);









        if (ddl_year_evaluation.SelectedValue == "0")
            cmd8.Parameters.Add("@home_year_evaluation", SqlDbType.Int).Value = "1900";
      
        else
        cmd8.Parameters.Add("@home_year_evaluation", SqlDbType.Int).Value = Convert.ToInt32(ddl_year_evaluation.SelectedValue);

        if (home_land_evaluation.Text=="")
         home_land_evaluation.Text="0";

        if (home_building_evaluation.Text == "")
            home_building_evaluation.Text = "0";

        if (home_total_evaluation.Text == "")
            home_total_evaluation.Text = "0";
        
        cmd8.Parameters.Add("@home_land_evaluation", SqlDbType.Int).Value = Convert.ToInt32(home_land_evaluation.Text);
        cmd8.Parameters.Add("@home_building_evaluation", SqlDbType.Int).Value = Convert.ToInt32(home_building_evaluation.Text);
        cmd8.Parameters.Add("@home_total_evaluation", SqlDbType.Int).Value = Convert.ToInt32(home_total_evaluation.Text);


        cmd8.Parameters.Add("@home_fondation", SqlDbType.VarChar, 50).Value = home_fondation.Text;
        cmd8.Parameters.Add("@home_frame", SqlDbType.VarChar, 50).Value = home_frame.Text;
        cmd8.Parameters.Add("@home_roof", SqlDbType.VarChar, 50).Value = home_roof.Text;
        cmd8.Parameters.Add("@home_windows", SqlDbType.VarChar, 50).Value = home_windows.Text;
        cmd8.Parameters.Add("@home_floors", SqlDbType.VarChar, 50).Value = home_floors.Text;
        cmd8.Parameters.Add("@home_under_floors", SqlDbType.VarChar, 50).Value = home_under_floors.Text;
        cmd8.Parameters.Add("@home_wall", SqlDbType.VarChar, 50).Value = home_wall.Text;


        cmd8.Parameters.Add("@home_water_heater", SqlDbType.VarChar, 50).Value = home_water_heater.Text;





        cmd8.Parameters.Add("@home_lav_sech", SqlDbType.Int).Value = Convert.ToInt32(home_lav_sech.Text);
        cmd8.Parameters.Add("@home_fire_protection", SqlDbType.Int).Value = Convert.ToInt32(home_fire_protection.Text);
        cmd8.Parameters.Add("@home_laundry", SqlDbType.Int).Value = Convert.ToInt32(home_laundry.Text);
        cmd8.Parameters.Add("@home_parking_int", SqlDbType.Int).Value = Convert.ToInt32(home_parking_int.Text);
        cmd8.Parameters.Add("@home_parking_ext", SqlDbType.Int).Value = Convert.ToInt32(home_parking_ext.Text);
        cmd8.Parameters.Add("@home_prise_ext", SqlDbType.Int).Value = Convert.ToInt32(home_prise_ext.Text);








        cmd8.Parameters.Add("@home_water", SqlDbType.VarChar, 50).Value = home_water.Text;
        cmd8.Parameters.Add("@home_heating", SqlDbType.VarChar, 50).Value = home_heating.Text;
        cmd8.Parameters.Add("@home_combustible", SqlDbType.VarChar, 50).Value = home_combustible.Text;
        cmd8.Parameters.Add("@home_sewage", SqlDbType.VarChar, 50).Value = home_sewage.Text;
        cmd8.Parameters.Add("@home_ext_finition", SqlDbType.VarChar, 50).Value = home_ext_finition.Text;


        if (home_site_corner.Checked == true)
            cmd8.Parameters.Add("@home_site_corner", SqlDbType.Int).Value = 1;
        else
            cmd8.Parameters.Add("@home_site_corner", SqlDbType.Int).Value = 0;


        if (home_site_highway.Checked == true)
            cmd8.Parameters.Add("@home_site_highway", SqlDbType.Int).Value = 1;
        else
            cmd8.Parameters.Add("@home_site_highway", SqlDbType.Int).Value = 0;

        if (home_site_school.Checked == true)
            cmd8.Parameters.Add("@home_site_school", SqlDbType.Int).Value = 1;
        else
            cmd8.Parameters.Add("home_site_school", SqlDbType.Int).Value = 0;

        if (home_site_services.Checked == true)
            cmd8.Parameters.Add("@home_site_services", SqlDbType.Int).Value = 1;
        else
            cmd8.Parameters.Add("@home_site_services", SqlDbType.Int).Value = 0;


        if (home_site_transport.Checked == true)
            cmd8.Parameters.Add("@home_site_transport", SqlDbType.Int).Value = 1;
        else
            cmd8.Parameters.Add("@home_site_transport", SqlDbType.Int).Value = 0;





        cmd8.Parameters.Add("@home_parking", SqlDbType.VarChar, 50).Value = home_parking.Text;
       cmd8.Parameters.Add("@home_particularity", SqlDbType.VarChar, 50).Value = home_particularity.Text;






        //execute the insert
        cmd8.ExecuteNonQuery();

        conn8.Close();

        string home_id = Convert.ToString(cmd8.Parameters["@new_home_id"].Value);



        Response.Redirect("property_add_2.aspx?h_id=" + home_id, true);

    }
}
