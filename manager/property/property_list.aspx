<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master"  AutoEventWireup="true" CodeFile="property_list.aspx.cs" Inherits="property_property_list" Title="property list" %>

<%@ Register Src="~/shared/userControls/AlertMessage/AlertMessage.usercontrol.ascx" TagPrefix="uc1" TagName="AlertMessageusercontrol" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <a href="property_add.aspx" class="simpleButton  primary">+ add property</a>
    <uc1:AlertMessageusercontrol runat="server" ID="AlertMessageusercontrol" />
     <asp:GridView ID="dghome_list" runat="server" AutoGenerateColumns="false" 
           AllowPaging="true" AllowSorting="true" Width="100%"
      HeaderStyle-BackColor="#F0F0F6" PageSize="15"  OnPageIndexChanging="dghome_list_PageIndexChanging"
         onselectedindexchanged="dghome_list_SelectedIndexChanged" 
           >
    <Columns>
    
    <asp:BoundField   DataField="home_name"   HeaderText="Property name"/>
    <asp:BoundField   DataField="home_addr_no"  HeaderText="Address #"/>
    <asp:BoundField  DataField="home_addr_street" HeaderText="Street" />
    <asp:BoundField  DataField="units" HeaderText="Unit(s)" />
    <asp:BoundField  DataField="home_prov" HeaderText="Prov/State" />
    <asp:BoundField  DataField="home_city" HeaderText="City"/>
    <asp:BoundField  DataField="home_district" HeaderText="District"/>
    
     <asp:HyperLinkField   Text="View"
     DataNavigateUrlFields="home_id" 
     DataNavigateUrlFormatString="~/manager/property/property_view.aspx?h_id={0}" 
      HeaderText="View" />
      
      <asp:HyperLinkField   Text="Edit"
     DataNavigateUrlFields="home_id" 
     DataNavigateUrlFormatString="~/manager/property/property_update.aspx?h_id={0}" 
      HeaderText="Edit" />
    </Columns>
    
    
    </asp:GridView>
    
   </asp:Content>
