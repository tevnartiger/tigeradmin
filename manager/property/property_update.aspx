﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="property_update.aspx.cs" Inherits="manager_property_property_update" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager> 
                       
 <b><asp:Label ID="Label25" runat="server" 
        Text="<%$ Resources:Resource, lbl_mandatory_field %>" style="color: #FF3300"/></b>
       <br />
    <br />



<br />   <br />   
           <h1>Address</h1> 
    <br />   
     
  <table border="0" cellpadding="1" cellspacing="3"  width="100%">
   
<tr>
    <td style="width: 190px">  
        &nbsp;</td><td valign="top" >
        <asp:Label ID="lbl_successful_1" runat="server"></asp:Label>
    </td>
    <td valign="top" >&nbsp;</td></tr>
<tr><td style="width: 190px" >
        <asp:Label ID="Label1" runat="server" 
            Text="<%$ Resources:Resource, lbl_home_name %>"></asp:Label>
    </td>
    <td valign="top"><asp:textbox id="home_name" runat="server"/>&nbsp;<asp:Label ID="Label29"
        runat="server" Text="*" ForeColor="#FF3300"/></td>
    <td rowspan="18" 
        valign="top">
        <asp:Repeater ID="rHomeImage" runat="server">
            <ItemTemplate>
                <asp:Image ID="Image2" runat="server" Height="270" 
                    ImageUrl='<%# "~/mediahandler/HomeImage.ashx?h_id="+ Eval("home_id") %>' 
                    Width="300" />
            </ItemTemplate>
        </asp:Repeater>
    </td>
      </tr>
<tr><td style="width: 190px">Image</td>
    <td valign="top">
        <asp:FileUpload ID="FileUpload1" runat="server" />
    </td></tr>
<tr><td style="width: 190px">
    <asp:Label ID="Label2" runat="server" 
        Text="<%$ Resources:Resource, lbl_address_no %>"></asp:Label> </td>
    <td valign="top"><asp:textbox id="home_addr_no" runat="server"/>&nbsp;<asp:Label ID="Label30"
        runat="server" Text="*" ForeColor="#FF3300"/></td>
      </tr>
<tr><td style="width: 190px">
    <asp:Label ID="Label3" runat="server" 
        Text="<%$ Resources:Resource, lbl_address_street %>"></asp:Label></td>
    <td valign="top"><asp:textbox id="home_addr_street" runat="server"/>&nbsp;<asp:Label ID="Label31"
        runat="server" Text="*" ForeColor="#FF3300"/></td></tr>
<tr><td style="width: 190px">
    <asp:Label ID="Label4" runat="server" 
        Text="<%$ Resources:Resource, lbl_pc %>"></asp:Label></td>
    <td valign="top"><asp:textbox id="home_pc" runat="server"/>&nbsp;<asp:Label ID="Label32"
        runat="server" Text="*" ForeColor="#FF3300"/></td></tr>
<tr><td style="width: 190px">
    <asp:Label ID="Label5" runat="server" 
        Text="<%$ Resources:Resource, lbl_city %>"></asp:Label> </td>
    <td valign="top"><asp:textbox id="home_city" runat="server"/>&nbsp;<asp:Label ID="Label33"
        runat="server" Text="*" ForeColor="#FF3300"/></td></tr>
<tr><td style="width: 190px">
    <asp:Label ID="Label6" runat="server" 
        Text="<%$ Resources:Resource, txt_district %>"></asp:Label></td>
    <td valign="top"><asp:textbox id="home_district" runat="server"/>&nbsp;<asp:Label 
            ID="Label34" runat="server" ForeColor="#FF3300" Text="*" />
    </td></tr>
<tr><td style="width: 190px">
        <asp:Label ID="Label7" runat="server" 
            Text="<%$ Resources:Resource, lbl_province %>"></asp:Label>
    </td><td valign="top">
        <asp:TextBox ID="home_prov" runat="server"></asp:TextBox>
        &nbsp;<asp:Label ID="Label28" runat="server" ForeColor="#FF3300" Text="*"></asp:Label>
    </td></tr>
<tr><td style="width: 190px">
        <asp:Label ID="Label8" runat="server" 
            Text="<%$ Resources:Resource, lbl_type %>"></asp:Label>
    </td><td>
        <asp:TextBox ID="home_type" runat="server"></asp:TextBox>
    </td></tr>
<tr><td style="width: 190px">
        <asp:Label ID="Label9" runat="server" 
            Text="<%$ Resources:Resource, lbl_style %>"></asp:Label>
    </td><td>
        <asp:TextBox ID="home_style" runat="server"></asp:TextBox>
    </td></tr>
<tr><td style="width: 190px">

        <asp:Label ID="Label10" runat="server" 
            Text="<%$ Resources:Resource, lbl_nb_floor %>"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td><td>
        <asp:TextBox ID="home_unit" runat="server" Width="35px"></asp:TextBox>
    </td></tr>




<tr>

<td style="width: 190px">
    <asp:Label ID="Label11" runat="server" 
        Text="<%$ Resources:Resource, lbl_nb_unit %>"></asp:Label>

</td><td>
        <asp:TextBox ID="home_floor" runat="server" Width="35px"></asp:TextBox>
    </td>

      </tr>
<tr><td style="width: 190px">
        <asp:Label ID="Label12" runat="server" 
            Text="<%$ Resources:Resource, lbl_date_built %>"></asp:Label>
    </td><td>
        <asp:DropDownList ID="ddl_home_date_built_m" runat="server">
            <asp:ListItem Text="<%$ Resources:Resource, txt_month %>" Value="0"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
           </asp:DropDownList>
        &nbsp;/
        <asp:DropDownList ID="ddl_home_date_built_d" runat="server">
            <asp:ListItem Text="<%$ Resources:Resource, txt_day %>" Value="0"></asp:ListItem>
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>
            <asp:ListItem>3</asp:ListItem>
            <asp:ListItem>4</asp:ListItem>
            <asp:ListItem>5</asp:ListItem>
            <asp:ListItem>6</asp:ListItem>
            <asp:ListItem>7</asp:ListItem>
            <asp:ListItem>8</asp:ListItem>
            <asp:ListItem>9</asp:ListItem>
            <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>11</asp:ListItem>
            <asp:ListItem>12</asp:ListItem>
            <asp:ListItem>13</asp:ListItem>
            <asp:ListItem>14</asp:ListItem>
            <asp:ListItem>15</asp:ListItem>
            <asp:ListItem>16</asp:ListItem>
            <asp:ListItem>17</asp:ListItem>
            <asp:ListItem>18</asp:ListItem>
            <asp:ListItem>19</asp:ListItem>
            <asp:ListItem>20</asp:ListItem>
            <asp:ListItem>21</asp:ListItem>
            <asp:ListItem>22</asp:ListItem>
            <asp:ListItem>23</asp:ListItem>
            <asp:ListItem>24</asp:ListItem>
            <asp:ListItem>25</asp:ListItem>
            <asp:ListItem>26</asp:ListItem>
            <asp:ListItem>27</asp:ListItem>
            <asp:ListItem>28</asp:ListItem>
            <asp:ListItem>29</asp:ListItem>
            <asp:ListItem>30</asp:ListItem>
            <asp:ListItem>31</asp:ListItem>
        </asp:DropDownList>
        &nbsp;/
        <asp:DropDownList ID="ddl_home_date_built_y" runat="server">
            
        </asp:DropDownList>
    </td></tr>
<tr><td style="width: 190px">
        <asp:Label ID="Label13" runat="server" 
            Text="<%$ Resources:Resource, lbl_date_purchase %>"></asp:Label>
    </td><td>
        <asp:DropDownList ID="ddl_home_date_purchase_m" runat="server">
           <asp:ListItem Text="<%$ Resources:Resource, txt_month %>" Value="0"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
           </asp:DropDownList>
        &nbsp;/
        <asp:DropDownList ID="ddl_home_date_purchase_d" runat="server">
            <asp:ListItem Text="<%$ Resources:Resource, txt_day %>" Value="0"></asp:ListItem>
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>
            <asp:ListItem>3</asp:ListItem>
            <asp:ListItem>4</asp:ListItem>
            <asp:ListItem>5</asp:ListItem>
            <asp:ListItem>6</asp:ListItem>
            <asp:ListItem>7</asp:ListItem>
            <asp:ListItem>8</asp:ListItem>
            <asp:ListItem>9</asp:ListItem>
            <asp:ListItem>10</asp:ListItem>
            <asp:ListItem>11</asp:ListItem>
            <asp:ListItem>12</asp:ListItem>
            <asp:ListItem>13</asp:ListItem>
            <asp:ListItem>14</asp:ListItem>
            <asp:ListItem>15</asp:ListItem>
            <asp:ListItem>16</asp:ListItem>
            <asp:ListItem>17</asp:ListItem>
            <asp:ListItem>18</asp:ListItem>
            <asp:ListItem>19</asp:ListItem>
            <asp:ListItem>20</asp:ListItem>
            <asp:ListItem>21</asp:ListItem>
            <asp:ListItem>22</asp:ListItem>
            <asp:ListItem>23</asp:ListItem>
            <asp:ListItem>24</asp:ListItem>
            <asp:ListItem>25</asp:ListItem>
            <asp:ListItem>26</asp:ListItem>
            <asp:ListItem>27</asp:ListItem>
            <asp:ListItem>28</asp:ListItem>
            <asp:ListItem>29</asp:ListItem>
            <asp:ListItem>30</asp:ListItem>
            <asp:ListItem>31</asp:ListItem>
        </asp:DropDownList>
        &nbsp;/
        <asp:DropDownList ID="ddl_home_date_purchase_y" runat="server">
        </asp:DropDownList>
    </td></tr>
<tr><td style="width: 190px">
        <asp:Label ID="Label14" runat="server" 
            Text="<%$ Resources:Resource, lbl_purchase_price %>"></asp:Label>
    </td><td>
        <asp:TextBox ID="home_purchase_price" runat="server"></asp:TextBox>
    </td></tr>
<tr><td style="width: 190px">
    
    <asp:Label ID="Label15" runat="server" 
        Text="<%$ Resources:Resource, lbl_living_space %>"></asp:Label>
</td>
    <td>
        <asp:TextBox ID="home_living_space" runat="server"></asp:TextBox>
    </td>
</tr>

    
  
    
      <tr>
          <td style="width: 190px">
              <asp:Label ID="Label16" runat="server" 
                  Text="<%$ Resources:Resource, lbl_land_size %>"></asp:Label>
          </td>
          <td>
              <asp:TextBox ID="home_land_size" runat="server"></asp:TextBox>
          </td>
      </tr>

    
  
    
      <tr>
          <td style="width: 190px" valign="top">
              <asp:Label ID="Label17" runat="server" 
                  Text="<%$ Resources:Resource, lbl_description %>"></asp:Label>
          </td>
          <td valign="top">
              <asp:TextBox ID="home_desc" runat="server" Height="50px" TextMode="MultiLine" 
                  Width="200px"></asp:TextBox>
          </td>
      </tr>

    
  
    
    </table>

    <br />
    
    <asp:Button ID="btn_submit_1" runat="server" 
        Text="<%$ Resources:Resource, btn_submit %>" onclick="btn_submit_1_Click"  />
      
      

     
     
     
<br />  <br />     
<h1>Dimension , matricule , certificat</h1>
     
         <table border="0" width="100%" cellpadding="1" cellspacing="3" >
             <tr>
                 <td style="width: 151px">
                     &nbsp;</td>
                 <td>
                     <asp:Label ID="lbl_successful_2" runat="server"></asp:Label>
                 </td>
                 <td>
                     &nbsp;</td>
                 <td>
                     &nbsp;</td>
                 <td>
                     &nbsp;</td>
                 <td>
                     &nbsp;</td>
             </tr>
             <tr>
                 <td style="width: 151px">
                     <asp:Label ID="Label24" runat="server" 
                         Text="<%$ Resources:Resource, lbl_land_dimension %>"></asp:Label>
                 </td>
                 <td>
                     <asp:TextBox ID="home_land_dimension" runat="server" Width="80px"></asp:TextBox>
                 </td>
                 <td>
                     &nbsp;</td>
                 <td>
                     &nbsp; &nbsp;</td>
                 <td>
                     <asp:Label ID="Label26" runat="server" 
                         Text="<%$ Resources:Resource, lbl_property_dimension %>"></asp:Label>
                 </td>
                 <td>
                     <asp:TextBox ID="home_dimension" runat="server" Height="18px" Width="80px"></asp:TextBox>
                 </td>
             </tr>
             <tr>
                 <td style="width: 151px">
                     <asp:Label ID="Label27" runat="server" 
                         Text="<%$ Resources:Resource, lbl_land_surface %>"></asp:Label>
                 </td>
                 <td>
                     <asp:TextBox ID="home_land_surface" runat="server" Height="16px" 
                         Width="80px"></asp:TextBox>
                     &nbsp;&nbsp;&nbsp;
                 </td>
                 <td>
                     <asp:DropDownList ID="ddl_land_surface_type" runat="server">
                         <asp:ListItem Value="0">Sq.-ft</asp:ListItem>
                         <asp:ListItem Value="1">Sq.-m</asp:ListItem>
                     </asp:DropDownList>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                 <td>
                     &nbsp;</td>
                 <td>
                     <asp:Label ID="Label35" runat="server" 
                         Text="<%$ Resources:Resource, lbl_living_area %>"></asp:Label>
                 </td>
                 <td>
                     <asp:TextBox ID="home_area" runat="server" Height="16px" Width="80px"></asp:TextBox>
                     &nbsp;&nbsp;&nbsp;
                     <asp:DropDownList ID="ddl_habitable_surface_type" runat="server">
                         <asp:ListItem Value="0">Sq.-ft</asp:ListItem>
                         <asp:ListItem Value="1">Sq.-m</asp:ListItem>
                     </asp:DropDownList>
                 </td>
             </tr>
             <tr>
                 <td style="width: 151px">
                     <asp:Label ID="Label36" runat="server" 
                         Text="<%$ Resources:Resource, lbl_localisation_certificat %>"></asp:Label>
                 </td>
                 <td>
                     <asp:TextBox ID="home_localisation_certificat" runat="server" Width="80px" 
                         Height="18px"></asp:TextBox>
                 </td>
                 <td>
                     &nbsp;</td>
                 <td>
                     &nbsp;</td>
                 <td>
                 </td>
                 <td>
                 </td>
             </tr>
             <tr>
                 <td style="width: 151px">
                     <asp:Label ID="Label37" runat="server" 
                         Text="<%$ Resources:Resource, lbl_matricule %>"></asp:Label>
                 </td>
                 <td>
                     <asp:TextBox ID="home_matricule" runat="server" Width="80px"></asp:TextBox>
                 </td>
                 <td>
                     &nbsp;</td>
                 <td>
                     &nbsp;</td>
                 <td>
                     </td>
                 <td>
                 </td>
             </tr>
             <tr>
                 <td style="width: 151px">
                     <asp:Label ID="Label38" runat="server" 
                         Text="<%$ Resources:Resource, lbl_register %>"></asp:Label>
                 </td>
                 <td>
                     <asp:TextBox ID="home_register" runat="server" Width="80px"></asp:TextBox>
                 </td>
                 <td>
                     &nbsp;</td>
                 <td>
                     &nbsp;</td>
                 <td>
                     &nbsp;</td>
                 <td>
                     </td>
             </tr>
             <tr>
                 <td style="width: 151px">
                     &nbsp;</td>
                 <td>
                     &nbsp;</td>
                 <td>
                     &nbsp;</td>
                 <td>
                     &nbsp;</td>
                 <td>
                     &nbsp;</td>
                 <td>
                     &nbsp;</td>
             </tr>
         </table>
 <br />
    
    <asp:Button ID="btn_submit_2" runat="server" 
        Text="<%$ Resources:Resource, btn_submit %>" onclick="btn_submit_2_Click"  />
        

    
    
    
    
     
<h1>Municipal evaluation</h1>
       
        <table width="100%" border="0" cellpadding="1" cellspacing="3">
        
         <tr>
                    <td style="width: 178px" >
                        &nbsp;</td>
                    <td style="width: 191px">
                        
                        <asp:Label ID="lbl_successful_3" runat="server"></asp:Label>
                    </td>
                    <td>
                        
                        &nbsp;</td>
                    <td>
                        
                        &nbsp;</td>
      </tr>              
    
   
    <tr>
                    <td colspan="2" >
                        <b>
                        <asp:Label ID="Label39" runat="server" 
                            Text="<%$ Resources:Resource, lbl_update_property_evaluation %>"></asp:Label>
                        </b></td>
                    <td>
                        
                        &nbsp;</td>
                    <td>
                        
                        &nbsp;</td>
      </tr>              
    
   
    <tr>
                    <td style="width: 178px" >
                        &nbsp;</td>
                    <td style="width: 191px">
                        
                        &nbsp;</td>
                    <td colspan="2">
                        
                        <asp:Label ID="lbl_confirmation" runat="server" ForeColor="Red"></asp:Label>
                        &nbsp;
                        <br />
                        <asp:Label ID="Label74" runat="server"></asp:Label>
                        &nbsp;
                        <asp:Label ID="Label75" runat="server"></asp:Label>
                        &nbsp;
                        <asp:Label ID="lbl_amount" runat="server"></asp:Label>
                    </td>
      </tr>              
                <tr>  <td style="width: 178px" >
                        <asp:Label ID="Label40" runat="server" 
                            Text="<%$ Resources:Resource, txt_year %>"></asp:Label></td>
               
             
                    <td style="width: 191px" >
                        <asp:DropDownList ID="ddl_year_evaluation" runat="server">
                            <asp:ListItem Selected="True" Text="<%$ Resources:Resource, txt_year %>" 
                                Value=""></asp:ListItem>
                            <asp:ListItem Value="1981">1981</asp:ListItem>
                            <asp:ListItem Value="1982">1982</asp:ListItem>
                            <asp:ListItem Value="1983">1983</asp:ListItem>
                            <asp:ListItem Value="1984">1984</asp:ListItem>
                            <asp:ListItem Value="1985">1985</asp:ListItem>
                            <asp:ListItem Value="1986">1986</asp:ListItem>
                            <asp:ListItem Value="1987">1987</asp:ListItem>
                            <asp:ListItem Value="1988">1998</asp:ListItem>
                            <asp:ListItem Value="1989">1989</asp:ListItem>
                            <asp:ListItem Value="1990">1990</asp:ListItem>
                            <asp:ListItem Value="1991">1991</asp:ListItem>
                            <asp:ListItem Value="1992">1992</asp:ListItem>
                            <asp:ListItem Value="1993">1993</asp:ListItem>
                            <asp:ListItem Value="1994">1994</asp:ListItem>
                            <asp:ListItem Value="1995">1995</asp:ListItem>
                            <asp:ListItem Value="1996">1996</asp:ListItem>
                            <asp:ListItem Value="1997">1997</asp:ListItem>
                            <asp:ListItem Value="1998">1998</asp:ListItem>
                            <asp:ListItem Value="1999">1999</asp:ListItem>
                            <asp:ListItem Value="2000">2000</asp:ListItem>
                            <asp:ListItem Value="2001">2001</asp:ListItem>
                            <asp:ListItem Value="2002">2002</asp:ListItem>
                            <asp:ListItem Value="2003">2003</asp:ListItem>
                            <asp:ListItem Value="2004">2004</asp:ListItem>
                            <asp:ListItem Value="2005">2005</asp:ListItem>
                            <asp:ListItem Value="2006">2006</asp:ListItem>
                            <asp:ListItem Value="2007">2007</asp:ListItem>
                            <asp:ListItem Value="2008">2008</asp:ListItem>
                            <asp:ListItem Value="2009">2009</asp:ListItem>
                            <asp:ListItem Value="2010">2010</asp:ListItem>
                            <asp:ListItem Value="2011">2011</asp:ListItem>
                            <asp:ListItem Value="2012">2012</asp:ListItem>
                            <asp:ListItem Value="2013">2013</asp:ListItem>
                            <asp:ListItem Value="2014">2014</asp:ListItem>
                            <asp:ListItem>2015</asp:ListItem>
                            <asp:ListItem>2016</asp:ListItem>
                            <asp:ListItem>2017</asp:ListItem>
                            <asp:ListItem>2018</asp:ListItem>
                            <asp:ListItem>2019</asp:ListItem>
                            <asp:ListItem>2020</asp:ListItem>
                            <asp:ListItem>2022</asp:ListItem>
                            <asp:ListItem>2023</asp:ListItem>
                            <asp:ListItem>2024</asp:ListItem>
                            <asp:ListItem>2025</asp:ListItem>
                            <asp:ListItem>2026</asp:ListItem>
                        </asp:DropDownList>
                    </td>
               
             
                    <td colspan="2" rowspan="4" valign="top" >
                        <asp:GridView ID="gv_home_year_evaluation" runat="server" AllowSorting="true" 
                            AlternatingRowStyle-BackColor="Beige" AutoGenerateColumns="false" 
                            BorderColor="White" BorderWidth="3" EmptyDataText="no unit rented" 
                            GridLines="Both" HeaderStyle-BackColor="AliceBlue" Width="100%">
                            <Columns>
                                <asp:BoundField DataField="home_year_evaluation" 
                                    HeaderText="<%$ Resources:Resource, txt_year %>" />
                                <asp:BoundField DataField="home_land_evaluation" DataFormatString="{0:0.00}" 
                                    HeaderText="<%$ Resources:Resource, lbl_land_evaluation %>" />
                                <asp:BoundField DataField="home_building_evaluation" 
                                    DataFormatString="{0:0.00}" 
                                    HeaderText="<%$ Resources:Resource, lbl_building_evaluation %>" />
                                <asp:BoundField DataField="home_total_evaluation" 
                                    HeaderText="<%$ Resources:Resource, lbl_total_evaluation %>" />
                                    
                           <asp:TemplateField >
                          <ItemTemplate  >
                          <asp:HiddenField Visible="false" ID="h_he_id" Value='<%#Bind("he_id")%>'  runat="server" />
                          <asp:HiddenField Visible="false" ID="h_home_year_evaluation" Value='<%#Bind("home_year_evaluation")%>'  runat="server" />
                          <asp:Button ID="btn_cancel" runat="server" OnClick="btn_cancel_Click"  Text="<%$ Resources:Resource, btn_cancel %>"/>
                          </ItemTemplate  >
                          </asp:TemplateField>
                                    
                                    
                            </Columns>
                        </asp:GridView>
                    </td>
               
             
                    </tr>     
                    
                <tr>
                
                <td style="width: 178px" ><asp:Label ID="Label41" runat="server" 
                        Text="<%$ Resources:Resource, lbl_land_evaluation %>"></asp:Label></td>
                
                
                <td style="width: 191px" ><asp:TextBox ID="home_land_evaluation" runat="server" Width="80px" 
                        Height="17px"></asp:TextBox>
               </td>
                
                
               </tr>
               <tr>
                
                <td style="width: 178px" ><asp:Label ID="Label42" runat="server" 
                        Text="<%$ Resources:Resource, lbl_building_evaluation %>"></asp:Label></td>
                
                
                <td style="width: 191px" ><asp:TextBox ID="home_building_evaluation" runat="server" Width="80px"></asp:TextBox>
               </td>
                
                
               </tr>
        
            <tr>
                <td style="width: 178px">
                    <asp:Label ID="Label43" runat="server" 
                        Text="<%$ Resources:Resource, lbl_total_evaluation %>"></asp:Label>
                </td>
                <td style="width: 191px">
                    <asp:TextBox ID="home_total_evaluation" runat="server" Width="80px"></asp:TextBox>
                </td>
            </tr>
        
        </table>
        <br />
    <asp:Button ID="btn_submit_3" runat="server" 
        Text="<%$ Resources:Resource, lbl_update %>" onclick="btn_submit_3_Click" />

    
    
    
    
    
    
    <h1>Characteristics</h1>
         
   <table border="0" width="100%" cellpadding="1" cellspacing="3" >
     
    <tr>
        <td style="width: 208px" >
            &nbsp;</td>
        
        <td >
            <asp:Label ID="lbl_successful_4" runat="server"></asp:Label>
        </td>
    </tr> 
        <tr>
        <td style="width: 208px" ><asp:Label ID="Label51" runat="server" 
                Text="<%$ Resources:Resource, lbl_fondation %>"/></td>
        
        <td ><asp:TextBox ID="home_fondation" runat="server"></asp:TextBox></td>
       </tr>
        <tr>
        <td style="width: 208px" ><asp:Label ID="Label52" runat="server" 
                Text="<%$ Resources:Resource, lbl_frame %>"/></td>
        
        <td ><asp:TextBox ID="home_frame" runat="server"></asp:TextBox></td>
        </tr>
         <tr>
        <td style="width: 208px" ><asp:Label ID="Label53" runat="server" 
                Text="<%$ Resources:Resource, lbl_roof %>"/></td>
        
        <td ><asp:TextBox ID="home_roof" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
        <td style="width: 208px" ><asp:Label ID="Label54" runat="server" 
                Text="<%$ Resources:Resource, lbl_windows %>"/></td>
        
        <td ><asp:TextBox ID="home_windows" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
        <td style="width: 208px" ><asp:Label ID="Label55" runat="server" 
                Text="<%$ Resources:Resource, lbl_floors %>"/></td>
        
        <td ><asp:TextBox ID="home_floors" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
        <td style="width: 208px" ><asp:Label ID="Label56" runat="server" 
                Text="<%$ Resources:Resource, lbl_under_floors %>"/></td>
        
        <td ><asp:TextBox ID="home_under_floors" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
        <td style="width: 208px" ><asp:Label ID="Label57" runat="server" 
                Text="<%$ Resources:Resource, lbl_walls %>"/></td>
        
        <td ><asp:TextBox ID="home_wall" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
        <td style="width: 208px" ><asp:Label ID="Label58" runat="server" 
                Text="<%$ Resources:Resource, lbl_water_heater %>"/></td>
        
        <td >
            <asp:TextBox ID="home_water_heater" runat="server"></asp:TextBox>
            </td>
            </tr>
          <tr>
        <td valign="top" style="width: 208px" ><asp:Label ID="Label59" runat="server" 
                Text="<%$ Resources:Resource, lbl_lav_sech %>"/></td>
        
        <td >
            <asp:RadioButtonList ID="home_lav_sech" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Text="<%$ Resources:Resource, lbl_no %>" Selected="True" Value="0"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:Resource, lbl_yes %>" Value="1"></asp:ListItem>
            </asp:RadioButtonList></td>
           </tr>
        <tr>
        <td valign="top" style="width: 208px" ><asp:Label ID="Label60" runat="server" 
                Text="<%$ Resources:Resource, lbl_fire_protection %>"/></td>
        
        <td >
            <asp:RadioButtonList ID="home_fire_protection" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Text="<%$ Resources:Resource, lbl_no %>" Selected="True" Value="0"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:Resource, lbl_yes %>" Value="1"></asp:ListItem>
            </asp:RadioButtonList></td>
           </tr>
        <tr>
        <td valign="top" style="width: 208px" ><asp:Label ID="Label61" runat="server" 
                Text="<%$ Resources:Resource, lbl_laundry %>"/></td>
        
        <td >
            <asp:RadioButtonList ID="home_laundry" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_no %>" 
                    Value="0"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:Resource, lbl_yes %>" Value="1"></asp:ListItem>
            </asp:RadioButtonList>
            </td>
          </tr>

        <tr>
        <td style="width: 208px" ><asp:Label ID="Label62" runat="server" 
                Text="<%$ Resources:Resource, lbl_parking %>"/></td>
        
        <td >
            <asp:Label ID="Label63" runat="server" 
                Text="<%$ Resources:Resource, lbl_int %>" />
            <asp:TextBox ID="home_parking_int" runat="server" Text="0" Width="70px"></asp:TextBox>
            &nbsp;
            <asp:Label ID="Label64" runat="server" 
                Text="<%$ Resources:Resource, lbl_ext %>" />
            :<asp:TextBox ID="home_parking_ext" runat="server" Text="0" Width="68px"></asp:TextBox>
            </td>
            <td>
                &nbsp;&nbsp;</td>
            </tr>
        <tr>
        <td valign="top" style="width: 208px" ><asp:Label ID="Label65" runat="server" 
                Text="<%$ Resources:Resource, lbl_external_plugs %>"/></td>
        
        <td >
            <asp:RadioButtonList ID="home_prise_ext" runat="server" 
                RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_no %>" 
                    Value="0"></asp:ListItem>
                <asp:ListItem Text="<%$ Resources:Resource, lbl_yes %>" Value="1"></asp:ListItem>
            </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
        <td style="width: 208px" ><asp:Label ID="Label66" runat="server" 
                Text="<%$ Resources:Resource, lbl_water %>"/></td>
        
        
        
        <td ><asp:TextBox ID="home_water" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
        <td style="width: 208px"><asp:Label ID="Label67" runat="server" 
                Text="<%$ Resources:Resource, lbl_heating %>"/></td>
        
        <td ><asp:TextBox ID="home_heating" runat="server"></asp:TextBox></td>
        </tr>
       <tr>
        <td style="width: 208px" ><asp:Label ID="Label68" runat="server" 
                Text="<%$ Resources:Resource, lbl_combustible %>"/></td>
        
        <td ><asp:TextBox ID="home_combustible" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
        <td style="width: 208px" ><asp:Label ID="Label69" runat="server" 
                Text="<%$ Resources:Resource, lbl_sewage %>"/></td>
        
        <td ><asp:TextBox ID="home_sewage" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
        <td style="width: 208px" ><asp:Label ID="Label70" runat="server" 
                Text="<%$ Resources:Resource, lbl_exterior_finition %>"/></td>
        
        <td >
                
            <asp:TextBox ID="home_ext_finition" runat="server"></asp:TextBox>
            </td>
            </tr>
            <tr>
        <td valign="top" style="width: 208px" ><asp:Label ID="Label71" runat="server" 
                Text="<%$ Resources:Resource, lbl_site_influence %>"/></td>
        
        
        <td >
            <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td>
                        <asp:CheckBox ID="home_site_corner" runat="server" 
                            Text="<%$ Resources:Resource, lbl_corner %>" />
                    </td>
                    <td>
                        <asp:CheckBox ID="home_site_school" runat="server" 
                            Text="<%$ Resources:Resource, lbl_school %>" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox ID="home_site_highway" runat="server" 
                            Text="<%$ Resources:Resource, lbl_highway %>" />
                    </td>
                    <td>
                        <asp:CheckBox ID="home_site_services" runat="server" 
                            Text="<%$ Resources:Resource, lbl_services %>" />
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <asp:CheckBox ID="home_site_transport" runat="server" 
                            Text="<%$ Resources:Resource, lbl_transport %>" />
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
            &nbsp;</td>
        </tr>
        <tr>
        <td style="width: 208px"  ><asp:Label ID="Label72" runat="server" 
                Text="<%$ Resources:Resource, lbl_parking %>"/></td>
        
        <td><asp:TextBox ID="home_parking" runat="server"></asp:TextBox></td>

</tr>
  
   
    
      <tr>
          <td valign="top" style="width: 208px">
              <asp:Label ID="Label73" runat="server" 
                  Text="<%$ Resources:Resource, lbl_particularity %>" />
          </td>
          <td  >
              <asp:TextBox ID="home_particularity" runat="server" Height="149px" 
                  TextMode="MultiLine" Width="219px"></asp:TextBox>
          </td>
      </tr>
  
   
    
    </table>
 
    <br />
    <asp:Button ID="btn_submit_4" runat="server" 
        Text="<%$ Resources:Resource, btn_submit %>" onclick="btn_submit_4_Click" />
    <br />

    


</asp:Content>

