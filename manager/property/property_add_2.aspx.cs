﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : may 7 , 2008
/// </summary>

public partial class manager_property_property_add_2 : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {  
        if (!Page.IsPostBack)
        {


          if (!RegEx.IsInteger(Request.QueryString["h_id"]))
          {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
          }

           // here we get the list of unit that were already added 

          int home_id = Convert.ToInt32(Request.QueryString["h_id"]);
          //---------------------------------------------------------------------------------------------------
          AccountObjectAuthorization homeAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

          ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
          if (!homeAuthorization.Home(Convert.ToInt32(Session["schema_id"]), home_id))
          {
              Session.Abandon();
              Response.Redirect("~/login.aspx");
          }
          ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
          //---------------------------------------------------------------------------------------------------


          tiger.Home u = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
          r_unit_list.DataSource = u.getHomeUnitList(home_id, Convert.ToInt32(Session["schema_id"]));
          r_unit_list.DataBind();

          //To view the address of the property

          tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
          rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
          rhome_view.DataBind();


           



        }
         
    }



    protected void btn_submit_Click(object sender, EventArgs e)
    {
        // panel_home_unit_add.Visible = true;


        if (tbx_unit_size1.Text == "")
            tbx_unit_size1.Text = "0";

        if (tbx_nb_bedrooms1.Text == "")
            tbx_nb_bedrooms1.Text = "0";

        if (tbx_nb_bathrooms1.Text == "")
            tbx_nb_bathrooms1.Text = "0";

        if (tbx_unit_level1.Text == "")
            tbx_unit_level1.Text = "0";

        if (tbx_max_tenants1.Text == "")
            tbx_max_tenants1.Text = "0";

        //---

        if (tbx_unit_size2.Text == "")
            tbx_unit_size2.Text = "0";

        if (tbx_nb_bedrooms2.Text == "")
            tbx_nb_bedrooms2.Text = "0";

        if (tbx_nb_bathrooms2.Text == "")
            tbx_nb_bathrooms2.Text = "0";

        if (tbx_unit_level2.Text == "")
            tbx_unit_level2.Text = "0";

        if (tbx_max_tenants2.Text == "")
            tbx_max_tenants2.Text = "0";

        //---

        if (tbx_unit_size3.Text == "")
            tbx_unit_size3.Text = "0";

        if (tbx_nb_bedrooms3.Text == "")
            tbx_nb_bedrooms3.Text = "0";

        if (tbx_nb_bathrooms3.Text == "")
            tbx_nb_bathrooms3.Text = "0";

        if (tbx_unit_level3.Text == "")
            tbx_unit_level3.Text = "0";

        if (tbx_max_tenants3.Text == "")
            tbx_max_tenants3.Text = "0";

        //--

        if (tbx_unit_size4.Text == "")
            tbx_unit_size4.Text = "0";

        if (tbx_nb_bedrooms4.Text == "")
            tbx_nb_bedrooms4.Text = "0";

        if (tbx_nb_bathrooms4.Text == "")
            tbx_nb_bathrooms4.Text = "0";

        if (tbx_unit_level4.Text == "")
            tbx_unit_level4.Text = "0";

        if (tbx_max_tenants4.Text == "")
            tbx_max_tenants4.Text = "0";

        //--


        if (tbx_unit_size5.Text == "")
            tbx_unit_size5.Text = "0";

        if (tbx_nb_bedrooms5.Text == "")
            tbx_nb_bedrooms5.Text = "0";

        if (tbx_nb_bathrooms5.Text == "")
            tbx_nb_bathrooms5.Text = "0";

        if (tbx_unit_level5.Text == "")
            tbx_unit_level5.Text = "0";

        if (tbx_max_tenants5.Text == "")
            tbx_max_tenants5.Text = "0";

        //--

        if (tbx_unit_size6.Text == "")
            tbx_unit_size6.Text = "0";

        if (tbx_nb_bedrooms6.Text == "")
            tbx_nb_bedrooms6.Text = "0";

        if (tbx_nb_bathrooms6.Text == "")
            tbx_nb_bathrooms6.Text = "0";

        if (tbx_unit_level6.Text == "")
            tbx_unit_level6.Text = "0";

        if (tbx_max_tenants6.Text == "")
            tbx_max_tenants6.Text = "0";

        //--


        if (tbx_unit_size7.Text == "")
            tbx_unit_size7.Text = "0";

        if (tbx_nb_bedrooms7.Text == "")
            tbx_nb_bedrooms7.Text = "0";

        if (tbx_nb_bathrooms7.Text == "")
            tbx_nb_bathrooms7.Text = "0";

        if (tbx_unit_level7.Text == "")
            tbx_unit_level7.Text = "0";

        if (tbx_max_tenants7.Text == "")
            tbx_max_tenants7.Text = "0";

        //--

        if (tbx_unit_size8.Text == "")
            tbx_unit_size8.Text = "0";

        if (tbx_nb_bedrooms8.Text == "")
            tbx_nb_bedrooms8.Text = "0";

        if (tbx_nb_bathrooms8.Text == "")
            tbx_nb_bathrooms8.Text = "0";

        if (tbx_unit_level8.Text == "")
            tbx_unit_level8.Text = "0";

        if (tbx_max_tenants8.Text == "")
            tbx_max_tenants8.Text = "0";

            //--


            if (tbx_unit_size9.Text == "")
                tbx_unit_size9.Text = "0";

        if (tbx_nb_bedrooms9.Text == "")
            tbx_nb_bedrooms9.Text = "0";

        if (tbx_nb_bathrooms9.Text == "")
            tbx_nb_bathrooms9.Text = "0";

        if (tbx_unit_level9.Text == "")
            tbx_unit_level9.Text = "0";

        if (tbx_max_tenants9.Text == "")
            tbx_max_tenants9.Text = "0";

        //--



        if (tbx_unit_size10.Text == "")
            tbx_unit_size10.Text = "0";

        if (tbx_nb_bedrooms10.Text == "")
            tbx_nb_bedrooms10.Text = "0";

        if (tbx_nb_bathrooms10.Text == "")
            tbx_nb_bathrooms10.Text = "0";

        if (tbx_unit_level10.Text == "")
            tbx_unit_level10.Text = "0";

        if (tbx_max_tenants10.Text == "")
            tbx_max_tenants10.Text = "0";


        
                string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

                SqlConnection conn = new SqlConnection(strconn);
                SqlCommand cmd = new SqlCommand("prUnitBatchAdd", conn);
                cmd.CommandType = CommandType.StoredProcedure;



                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);

                int home_id = Convert.ToInt32(Request.QueryString["h_id"]);
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
                
                cmd.Parameters.Add("@unit_door_no1", SqlDbType.VarChar, 50).Value = RegEx.getText(tbx_door_no1.Text);
                cmd.Parameters.Add("@unit_size_system1", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system1.SelectedValue);

                // if the unit size is in sq-ft
                if (ddl_unit_size_system1.SelectedValue == "0")
                {
                    cmd.Parameters.Add("@unit_size_sqft1", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size1.Text);
                    cmd.Parameters.Add("@unit_size_sqm1", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size1.Text) * Convert.ToDecimal(0.0929034);

                }

                // if the unit size is in sq-m
                else
                {
                    cmd.Parameters.Add("@unit_size_sqm1", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size1.Text);
                    cmd.Parameters.Add("@unit_size_sqft1", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size1.Text) * Convert.ToDecimal(10.7639104);


                }

                cmd.Parameters.Add("@unit_bedroom_no1", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms1.Text);
                cmd.Parameters.Add("@unit_bathroom_no1", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms1.Text);
                cmd.Parameters.Add("@unit_level1", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level1.Text);

                cmd.Parameters.Add("@unit_max_tenant1", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants1.Text);
       
        /// second unit to be added
        /// 
                cmd.Parameters.Add("@unit_door_no2", SqlDbType.VarChar,50).Value =  RegEx.getText(tbx_door_no2.Text) ;
                cmd.Parameters.Add("@unit_size_system2", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system2.SelectedValue);

                // if the unit size is in sq-ft
                if (ddl_unit_size_system2.SelectedValue == "0")
                {
                    cmd.Parameters.Add("@unit_size_sqft2", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size2.Text);
                    cmd.Parameters.Add("@unit_size_sqm2", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size2.Text) * Convert.ToDecimal(0.0929034);

                }

                // if the unit size is in sq-m
                else
                {
                    cmd.Parameters.Add("@unit_size_sqm2", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size2.Text);
                    cmd.Parameters.Add("@unit_size_sqft2", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size2.Text) * Convert.ToDecimal(10.7639104);


                }

                cmd.Parameters.Add("@unit_bedroom_no2", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms2.Text);
                cmd.Parameters.Add("@unit_bathroom_no2", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms2.Text);
                cmd.Parameters.Add("@unit_level2", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level2.Text);

                cmd.Parameters.Add("@unit_max_tenant2", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants2.Text);



          /// third unit to be added
          ///

                cmd.Parameters.Add("@unit_door_no3", SqlDbType.VarChar, 50).Value = RegEx.getText(tbx_door_no3.Text);
                cmd.Parameters.Add("@unit_size_system3", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system3.SelectedValue);

                // if the unit size is in sq-ft
                if (ddl_unit_size_system3.SelectedValue == "0")
                {
                    cmd.Parameters.Add("@unit_size_sqft3", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size3.Text);
                    cmd.Parameters.Add("@unit_size_sqm3", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size3.Text) * Convert.ToDecimal(0.0939034);

                }

                // if the unit size is in sq-m
                else
                {
                    cmd.Parameters.Add("@unit_size_sqm3", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size3.Text);
                    cmd.Parameters.Add("@unit_size_sqft3", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size3.Text) * Convert.ToDecimal(10.7639104);


                }

                cmd.Parameters.Add("@unit_bedroom_no3", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms3.Text);
                cmd.Parameters.Add("@unit_bathroom_no3", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms3.Text);
                cmd.Parameters.Add("@unit_level3", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level3.Text);

                cmd.Parameters.Add("@unit_max_tenant3", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants3.Text);




                /// fourth unit to be added
                ///

                cmd.Parameters.Add("@unit_door_no4", SqlDbType.VarChar, 50).Value = RegEx.getText(tbx_door_no4.Text);
                cmd.Parameters.Add("@unit_size_system4", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system4.SelectedValue);

                // if the unit size is in sq-ft
                if (ddl_unit_size_system4.SelectedValue == "0")
                {
                    cmd.Parameters.Add("@unit_size_sqft4", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size4.Text);
                    cmd.Parameters.Add("@unit_size_sqm4", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size4.Text) * Convert.ToDecimal(0.0939034);

                }

                // if the unit size is in sq-m
                else
                {
                    cmd.Parameters.Add("@unit_size_sqm4", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size4.Text);
                    cmd.Parameters.Add("@unit_size_sqft4", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size4.Text) * Convert.ToDecimal(10.7639104);


                }

                cmd.Parameters.Add("@unit_bedroom_no4", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms4.Text);
                cmd.Parameters.Add("@unit_bathroom_no4", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms4.Text);
                cmd.Parameters.Add("@unit_level4", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level4.Text);

                cmd.Parameters.Add("@unit_max_tenant4", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants4.Text);






                /// fifth unit to be added
                ///

                cmd.Parameters.Add("@unit_door_no5", SqlDbType.VarChar, 50).Value = RegEx.getText(tbx_door_no5.Text);
                cmd.Parameters.Add("@unit_size_system5", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system5.SelectedValue);

                // if the unit size is in sq-ft
                if (ddl_unit_size_system5.SelectedValue == "0")
                {
                    cmd.Parameters.Add("@unit_size_sqft5", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size5.Text);
                    cmd.Parameters.Add("@unit_size_sqm5", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size5.Text) * Convert.ToDecimal(0.0939034);

                }

                // if the unit size is in sq-m
                else
                {
                    cmd.Parameters.Add("@unit_size_sqm5", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size5.Text);
                    cmd.Parameters.Add("@unit_size_sqft5", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size5.Text) * Convert.ToDecimal(10.7639104);


                }

                cmd.Parameters.Add("@unit_bedroom_no5", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms5.Text);
                cmd.Parameters.Add("@unit_bathroom_no5", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms5.Text);
                cmd.Parameters.Add("@unit_level5", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level5.Text);

                cmd.Parameters.Add("@unit_max_tenant5", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants5.Text);




                /// 6 th unit to be added
                ///

                cmd.Parameters.Add("@unit_door_no6", SqlDbType.VarChar, 50).Value = RegEx.getText(tbx_door_no6.Text);
                cmd.Parameters.Add("@unit_size_system6", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system6.SelectedValue);

                // if the unit size is in sq-ft
                if (ddl_unit_size_system6.SelectedValue == "0")
                {
                    cmd.Parameters.Add("@unit_size_sqft6", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size6.Text);
                    cmd.Parameters.Add("@unit_size_sqm6", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size6.Text) * Convert.ToDecimal(0.0939034);

                }

                // if the unit size is in sq-m
                else
                {
                    cmd.Parameters.Add("@unit_size_sqm6", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size6.Text);
                    cmd.Parameters.Add("@unit_size_sqft6", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size6.Text) * Convert.ToDecimal(10.7639104);


                }

                cmd.Parameters.Add("@unit_bedroom_no6", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms6.Text);
                cmd.Parameters.Add("@unit_bathroom_no6", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms6.Text);
                cmd.Parameters.Add("@unit_level6", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level6.Text);

                cmd.Parameters.Add("@unit_max_tenant6", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants6.Text);


                /// 7 th unit to be added
                ///

                cmd.Parameters.Add("@unit_door_no7", SqlDbType.VarChar, 50).Value = RegEx.getText(tbx_door_no7.Text);
                cmd.Parameters.Add("@unit_size_system7", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system7.SelectedValue);

                // if the unit size is in sq-ft
                if (ddl_unit_size_system7.SelectedValue == "0")
                {
                    cmd.Parameters.Add("@unit_size_sqft7", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size7.Text);
                    cmd.Parameters.Add("@unit_size_sqm7", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size7.Text) * Convert.ToDecimal(0.0939034);

                }

                // if the unit size is in sq-m
                else
                {
                    cmd.Parameters.Add("@unit_size_sqm7", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size7.Text);
                    cmd.Parameters.Add("@unit_size_sqft7", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size7.Text) * Convert.ToDecimal(10.7639104);


                }

                cmd.Parameters.Add("@unit_bedroom_no7", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms7.Text);
                cmd.Parameters.Add("@unit_bathroom_no7", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms7.Text);
                cmd.Parameters.Add("@unit_level7", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level7.Text);

                cmd.Parameters.Add("@unit_max_tenant7", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants7.Text);



                /// 8 th unit to be added
                ///

                cmd.Parameters.Add("@unit_door_no8", SqlDbType.VarChar, 50).Value = RegEx.getText(tbx_door_no8.Text);
                cmd.Parameters.Add("@unit_size_system8", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system8.SelectedValue);

                // if the unit size is in sq-ft
                if (ddl_unit_size_system8.SelectedValue == "0")
                {
                    cmd.Parameters.Add("@unit_size_sqft8", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size8.Text);
                    cmd.Parameters.Add("@unit_size_sqm8", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size8.Text) * Convert.ToDecimal(0.0939034);

                }

                // if the unit size is in sq-m
                else
                {
                    cmd.Parameters.Add("@unit_size_sqm8", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size8.Text);
                    cmd.Parameters.Add("@unit_size_sqft8", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size8.Text) * Convert.ToDecimal(10.7639104);


                }

                cmd.Parameters.Add("@unit_bedroom_no8", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms8.Text);
                cmd.Parameters.Add("@unit_bathroom_no8", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms8.Text);
                cmd.Parameters.Add("@unit_level8", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level8.Text);

                cmd.Parameters.Add("@unit_max_tenant8", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants8.Text);


                /// 9 th unit to be added
                ///

                cmd.Parameters.Add("@unit_door_no9", SqlDbType.VarChar, 50).Value = RegEx.getText(tbx_door_no9.Text);
                cmd.Parameters.Add("@unit_size_system9", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system9.SelectedValue);

                // if the unit size is in sq-ft
                if (ddl_unit_size_system9.SelectedValue == "0")
                {
                    cmd.Parameters.Add("@unit_size_sqft9", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size9.Text);
                    cmd.Parameters.Add("@unit_size_sqm9", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size9.Text) * Convert.ToDecimal(0.0939034);

                }

                // if the unit size is in sq-m
                else
                {
                    cmd.Parameters.Add("@unit_size_sqm9", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size9.Text);
                    cmd.Parameters.Add("@unit_size_sqft9", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size9.Text) * Convert.ToDecimal(10.7639104);


                }

                cmd.Parameters.Add("@unit_bedroom_no9", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms9.Text);
                cmd.Parameters.Add("@unit_bathroom_no9", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms9.Text);
                cmd.Parameters.Add("@unit_level9", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level9.Text);

                cmd.Parameters.Add("@unit_max_tenant9", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants9.Text);

                /// 10 th unit to be added
                ///

                cmd.Parameters.Add("@unit_door_no10", SqlDbType.VarChar, 50).Value = RegEx.getText(tbx_door_no10.Text);
                cmd.Parameters.Add("@unit_size_system10", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system10.SelectedValue);

                // if the unit size is in sq-ft
                if (ddl_unit_size_system10.SelectedValue == "0")
                {
                    cmd.Parameters.Add("@unit_size_sqft10", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size10.Text);
                    cmd.Parameters.Add("@unit_size_sqm10", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size10.Text) * Convert.ToDecimal(0.0939034);

                }

                // if the unit size is in sq-m
                else
                {
                    cmd.Parameters.Add("@unit_size_sqm10", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size10.Text);
                    cmd.Parameters.Add("@unit_size_sqft10", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size10.Text) * Convert.ToDecimal(10.7639104);


                }

                cmd.Parameters.Add("@unit_bedroom_no10", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms10.Text);
                cmd.Parameters.Add("@unit_bathroom_no10", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms10.Text);
                cmd.Parameters.Add("@unit_level10", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level10.Text);

                cmd.Parameters.Add("@unit_max_tenant10", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants10.Text);


                cmd.ExecuteNonQuery();

       //reseting all the field in the page


                tbx_unit_size1.Text ="";
                tbx_nb_bedrooms1.Text ="";
                tbx_nb_bathrooms1.Text ="";
                tbx_unit_level1.Text ="";
                tbx_max_tenants1.Text ="";
                tbx_door_no1.Text = "";

                //---

                tbx_unit_size2.Text ="";
                tbx_nb_bedrooms2.Text ="";
                tbx_nb_bathrooms2.Text ="";
                tbx_unit_level2.Text ="";
                tbx_max_tenants2.Text ="";
                tbx_door_no2.Text = "";

                //---

                tbx_unit_size3.Text ="";
                tbx_nb_bedrooms3.Text ="";
                tbx_nb_bathrooms3.Text ="";
                tbx_unit_level3.Text ="";
                tbx_max_tenants3.Text ="";
                tbx_door_no3.Text = "";

                //--

                tbx_unit_size4.Text ="";
                tbx_nb_bedrooms4.Text ="";
                tbx_nb_bathrooms4.Text ="";
                tbx_unit_level4.Text ="";
                tbx_max_tenants4.Text ="";
                tbx_door_no4.Text = "";

                //--

                tbx_unit_size5.Text ="";
                tbx_nb_bedrooms5.Text ="";
                tbx_nb_bathrooms5.Text ="";
                tbx_unit_level5.Text ="";
                tbx_max_tenants5.Text ="";
                tbx_door_no5.Text = "";

                //--

                tbx_unit_size6.Text ="";
                tbx_nb_bedrooms6.Text ="";
                tbx_nb_bathrooms6.Text ="";
                tbx_unit_level6.Text ="";
                tbx_max_tenants6.Text ="";
                tbx_door_no6.Text = "";

                //--

                tbx_unit_size7.Text ="";
                tbx_nb_bedrooms7.Text ="";
                tbx_nb_bathrooms7.Text ="";
                tbx_unit_level7.Text ="";
                tbx_max_tenants7.Text ="";
                tbx_door_no7.Text = "";

                //--

                tbx_unit_size8.Text ="";
                tbx_nb_bedrooms8.Text ="";
                tbx_nb_bathrooms8.Text ="";
                tbx_unit_level8.Text ="";
                tbx_max_tenants8.Text ="";
                tbx_door_no8.Text = "";

                //--
 
                tbx_unit_size9.Text ="";
                tbx_nb_bedrooms9.Text ="";
                tbx_nb_bathrooms9.Text ="";
                tbx_unit_level9.Text ="";
                tbx_max_tenants9.Text ="";
                tbx_door_no9.Text = "";

                //--
 
                tbx_unit_size10.Text ="";
                tbx_nb_bedrooms10.Text ="";
                tbx_nb_bathrooms10.Text ="";
                tbx_unit_level10.Text ="";
                tbx_max_tenants10.Text ="";
                tbx_door_no10.Text = "";



              
                tiger.Home u = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                r_unit_list.DataSource = u.getHomeUnitList(home_id, Convert.ToInt32(Session["schema_id"]));
                r_unit_list.DataBind();

                //To view the address of the property

                tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
                rhome_view.DataBind();

                Response.Redirect("property_add2.aspx");


    }

/// <summary>
/// 
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
    protected void btn_submit_add_other_Click(object sender, EventArgs e)
    {


        // panel_home_unit_add.Visible = true;


        if (tbx_unit_size1.Text == "")
            tbx_unit_size1.Text = "0";

        if (tbx_nb_bedrooms1.Text == "")
            tbx_nb_bedrooms1.Text = "0";

        if (tbx_nb_bathrooms1.Text == "")
            tbx_nb_bathrooms1.Text = "0";

        if (tbx_unit_level1.Text == "")
            tbx_unit_level1.Text = "0";

        if (tbx_max_tenants1.Text == "")
            tbx_max_tenants1.Text = "0";

        //---

        if (tbx_unit_size2.Text == "")
            tbx_unit_size2.Text = "0";

        if (tbx_nb_bedrooms2.Text == "")
            tbx_nb_bedrooms2.Text = "0";

        if (tbx_nb_bathrooms2.Text == "")
            tbx_nb_bathrooms2.Text = "0";

        if (tbx_unit_level2.Text == "")
            tbx_unit_level2.Text = "0";

        if (tbx_max_tenants2.Text == "")
            tbx_max_tenants2.Text = "0";

        //---

        if (tbx_unit_size3.Text == "")
            tbx_unit_size3.Text = "0";

        if (tbx_nb_bedrooms3.Text == "")
            tbx_nb_bedrooms3.Text = "0";

        if (tbx_nb_bathrooms3.Text == "")
            tbx_nb_bathrooms3.Text = "0";

        if (tbx_unit_level3.Text == "")
            tbx_unit_level3.Text = "0";

        if (tbx_max_tenants3.Text == "")
            tbx_max_tenants3.Text = "0";

        //--

        if (tbx_unit_size4.Text == "")
            tbx_unit_size4.Text = "0";

        if (tbx_nb_bedrooms4.Text == "")
            tbx_nb_bedrooms4.Text = "0";

        if (tbx_nb_bathrooms4.Text == "")
            tbx_nb_bathrooms4.Text = "0";

        if (tbx_unit_level4.Text == "")
            tbx_unit_level4.Text = "0";

        if (tbx_max_tenants4.Text == "")
            tbx_max_tenants4.Text = "0";

        //--


        if (tbx_unit_size5.Text == "")
            tbx_unit_size5.Text = "0";

        if (tbx_nb_bedrooms5.Text == "")
            tbx_nb_bedrooms5.Text = "0";

        if (tbx_nb_bathrooms5.Text == "")
            tbx_nb_bathrooms5.Text = "0";

        if (tbx_unit_level5.Text == "")
            tbx_unit_level5.Text = "0";

        if (tbx_max_tenants5.Text == "")
            tbx_max_tenants5.Text = "0";

        //--

        if (tbx_unit_size6.Text == "")
            tbx_unit_size6.Text = "0";

        if (tbx_nb_bedrooms6.Text == "")
            tbx_nb_bedrooms6.Text = "0";

        if (tbx_nb_bathrooms6.Text == "")
            tbx_nb_bathrooms6.Text = "0";

        if (tbx_unit_level6.Text == "")
            tbx_unit_level6.Text = "0";

        if (tbx_max_tenants6.Text == "")
            tbx_max_tenants6.Text = "0";

        //--


        if (tbx_unit_size7.Text == "")
            tbx_unit_size7.Text = "0";

        if (tbx_nb_bedrooms7.Text == "")
            tbx_nb_bedrooms7.Text = "0";

        if (tbx_nb_bathrooms7.Text == "")
            tbx_nb_bathrooms7.Text = "0";

        if (tbx_unit_level7.Text == "")
            tbx_unit_level7.Text = "0";

        if (tbx_max_tenants7.Text == "")
            tbx_max_tenants7.Text = "0";

        //--

        if (tbx_unit_size8.Text == "")
            tbx_unit_size8.Text = "0";

        if (tbx_nb_bedrooms8.Text == "")
            tbx_nb_bedrooms8.Text = "0";

        if (tbx_nb_bathrooms8.Text == "")
            tbx_nb_bathrooms8.Text = "0";

        if (tbx_unit_level8.Text == "")
            tbx_unit_level8.Text = "0";

        if (tbx_max_tenants8.Text == "")
            tbx_max_tenants8.Text = "0";

        //--


        if (tbx_unit_size9.Text == "")
            tbx_unit_size9.Text = "0";

        if (tbx_nb_bedrooms9.Text == "")
            tbx_nb_bedrooms9.Text = "0";

        if (tbx_nb_bathrooms9.Text == "")
            tbx_nb_bathrooms9.Text = "0";

        if (tbx_unit_level9.Text == "")
            tbx_unit_level9.Text = "0";

        if (tbx_max_tenants9.Text == "")
            tbx_max_tenants9.Text = "0";

        //--



        if (tbx_unit_size10.Text == "")
            tbx_unit_size10.Text = "0";

        if (tbx_nb_bedrooms10.Text == "")
            tbx_nb_bedrooms10.Text = "0";

        if (tbx_nb_bathrooms10.Text == "")
            tbx_nb_bathrooms10.Text = "0";

        if (tbx_unit_level10.Text == "")
            tbx_unit_level10.Text = "0";

        if (tbx_max_tenants10.Text == "")
            tbx_max_tenants10.Text = "0";



        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prUnitBatchAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;



        conn.Open();
        //Add the params
        cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);


        int home_id = Convert.ToInt32(Request.QueryString["h_id"]);
        cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

        cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
        cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();

        cmd.Parameters.Add("@unit_door_no1", SqlDbType.VarChar, 50).Value = RegEx.getText(tbx_door_no1.Text);
        cmd.Parameters.Add("@unit_size_system1", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system1.SelectedValue);

        // if the unit size is in sq-ft
        if (ddl_unit_size_system1.SelectedValue == "0")
        {
            cmd.Parameters.Add("@unit_size_sqft1", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size1.Text);
            cmd.Parameters.Add("@unit_size_sqm1", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size1.Text) * Convert.ToDecimal(0.0929034);

        }

        // if the unit size is in sq-m
        else
        {
            cmd.Parameters.Add("@unit_size_sqm1", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size1.Text);
            cmd.Parameters.Add("@unit_size_sqft1", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size1.Text) * Convert.ToDecimal(10.7639104);


        }

        cmd.Parameters.Add("@unit_bedroom_no1", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms1.Text);
        cmd.Parameters.Add("@unit_bathroom_no1", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms1.Text);
        cmd.Parameters.Add("@unit_level1", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level1.Text);

        cmd.Parameters.Add("@unit_max_tenant1", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants1.Text);

        /// second unit to be added
        /// 
        cmd.Parameters.Add("@unit_door_no2", SqlDbType.VarChar, 50).Value = RegEx.getText(tbx_door_no2.Text);
        cmd.Parameters.Add("@unit_size_system2", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system2.SelectedValue);

        // if the unit size is in sq-ft
        if (ddl_unit_size_system2.SelectedValue == "0")
        {
            cmd.Parameters.Add("@unit_size_sqft2", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size2.Text);
            cmd.Parameters.Add("@unit_size_sqm2", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size2.Text) * Convert.ToDecimal(0.0929034);

        }

        // if the unit size is in sq-m
        else
        {
            cmd.Parameters.Add("@unit_size_sqm2", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size2.Text);
            cmd.Parameters.Add("@unit_size_sqft2", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size2.Text) * Convert.ToDecimal(10.7639104);


        }

        cmd.Parameters.Add("@unit_bedroom_no2", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms2.Text);
        cmd.Parameters.Add("@unit_bathroom_no2", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms2.Text);
        cmd.Parameters.Add("@unit_level2", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level2.Text);

        cmd.Parameters.Add("@unit_max_tenant2", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants2.Text);



        /// third unit to be added
        ///

        cmd.Parameters.Add("@unit_door_no3", SqlDbType.VarChar, 50).Value = RegEx.getText(tbx_door_no3.Text);
        cmd.Parameters.Add("@unit_size_system3", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system3.SelectedValue);

        // if the unit size is in sq-ft
        if (ddl_unit_size_system3.SelectedValue == "0")
        {
            cmd.Parameters.Add("@unit_size_sqft3", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size3.Text);
            cmd.Parameters.Add("@unit_size_sqm3", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size3.Text) * Convert.ToDecimal(0.0939034);

        }

        // if the unit size is in sq-m
        else
        {
            cmd.Parameters.Add("@unit_size_sqm3", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size3.Text);
            cmd.Parameters.Add("@unit_size_sqft3", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size3.Text) * Convert.ToDecimal(10.7639104);


        }

        cmd.Parameters.Add("@unit_bedroom_no3", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms3.Text);
        cmd.Parameters.Add("@unit_bathroom_no3", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms3.Text);
        cmd.Parameters.Add("@unit_level3", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level3.Text);

        cmd.Parameters.Add("@unit_max_tenant3", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants3.Text);




        /// fourth unit to be added
        ///

        cmd.Parameters.Add("@unit_door_no4", SqlDbType.VarChar, 50).Value = RegEx.getText(tbx_door_no4.Text);
        cmd.Parameters.Add("@unit_size_system4", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system4.SelectedValue);

        // if the unit size is in sq-ft
        if (ddl_unit_size_system4.SelectedValue == "0")
        {
            cmd.Parameters.Add("@unit_size_sqft4", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size4.Text);
            cmd.Parameters.Add("@unit_size_sqm4", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size4.Text) * Convert.ToDecimal(0.0939034);

        }

        // if the unit size is in sq-m
        else
        {
            cmd.Parameters.Add("@unit_size_sqm4", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size4.Text);
            cmd.Parameters.Add("@unit_size_sqft4", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size4.Text) * Convert.ToDecimal(10.7639104);


        }

        cmd.Parameters.Add("@unit_bedroom_no4", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms4.Text);
        cmd.Parameters.Add("@unit_bathroom_no4", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms4.Text);
        cmd.Parameters.Add("@unit_level4", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level4.Text);

        cmd.Parameters.Add("@unit_max_tenant4", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants4.Text);






        /// fifth unit to be added
        ///

        cmd.Parameters.Add("@unit_door_no5", SqlDbType.VarChar, 50).Value = RegEx.getText(tbx_door_no5.Text);
        cmd.Parameters.Add("@unit_size_system5", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system5.SelectedValue);

        // if the unit size is in sq-ft
        if (ddl_unit_size_system5.SelectedValue == "0")
        {
            cmd.Parameters.Add("@unit_size_sqft5", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size5.Text);
            cmd.Parameters.Add("@unit_size_sqm5", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size5.Text) * Convert.ToDecimal(0.0939034);

        }

        // if the unit size is in sq-m
        else
        {
            cmd.Parameters.Add("@unit_size_sqm5", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size5.Text);
            cmd.Parameters.Add("@unit_size_sqft5", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size5.Text) * Convert.ToDecimal(10.7639104);


        }

        cmd.Parameters.Add("@unit_bedroom_no5", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms5.Text);
        cmd.Parameters.Add("@unit_bathroom_no5", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms5.Text);
        cmd.Parameters.Add("@unit_level5", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level5.Text);

        cmd.Parameters.Add("@unit_max_tenant5", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants5.Text);




        /// 6 th unit to be added
        ///

        cmd.Parameters.Add("@unit_door_no6", SqlDbType.VarChar, 50).Value = RegEx.getText(tbx_door_no6.Text);
        cmd.Parameters.Add("@unit_size_system6", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system6.SelectedValue);

        // if the unit size is in sq-ft
        if (ddl_unit_size_system6.SelectedValue == "0")
        {
            cmd.Parameters.Add("@unit_size_sqft6", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size6.Text);
            cmd.Parameters.Add("@unit_size_sqm6", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size6.Text) * Convert.ToDecimal(0.0939034);

        }

        // if the unit size is in sq-m
        else
        {
            cmd.Parameters.Add("@unit_size_sqm6", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size6.Text);
            cmd.Parameters.Add("@unit_size_sqft6", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size6.Text) * Convert.ToDecimal(10.7639104);


        }

        cmd.Parameters.Add("@unit_bedroom_no6", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms6.Text);
        cmd.Parameters.Add("@unit_bathroom_no6", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms6.Text);
        cmd.Parameters.Add("@unit_level6", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level6.Text);

        cmd.Parameters.Add("@unit_max_tenant6", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants6.Text);


        /// 7 th unit to be added
        ///

        cmd.Parameters.Add("@unit_door_no7", SqlDbType.VarChar, 50).Value = RegEx.getText(tbx_door_no7.Text);
        cmd.Parameters.Add("@unit_size_system7", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system7.SelectedValue);

        // if the unit size is in sq-ft
        if (ddl_unit_size_system7.SelectedValue == "0")
        {
            cmd.Parameters.Add("@unit_size_sqft7", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size7.Text);
            cmd.Parameters.Add("@unit_size_sqm7", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size7.Text) * Convert.ToDecimal(0.0939034);

        }

        // if the unit size is in sq-m
        else
        {
            cmd.Parameters.Add("@unit_size_sqm7", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size7.Text);
            cmd.Parameters.Add("@unit_size_sqft7", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size7.Text) * Convert.ToDecimal(10.7639104);


        }

        cmd.Parameters.Add("@unit_bedroom_no7", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms7.Text);
        cmd.Parameters.Add("@unit_bathroom_no7", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms7.Text);
        cmd.Parameters.Add("@unit_level7", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level7.Text);

        cmd.Parameters.Add("@unit_max_tenant7", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants7.Text);



        /// 8 th unit to be added
        ///

        cmd.Parameters.Add("@unit_door_no8", SqlDbType.VarChar, 50).Value = RegEx.getText(tbx_door_no8.Text);
        cmd.Parameters.Add("@unit_size_system8", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system8.SelectedValue);

        // if the unit size is in sq-ft
        if (ddl_unit_size_system8.SelectedValue == "0")
        {
            cmd.Parameters.Add("@unit_size_sqft8", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size8.Text);
            cmd.Parameters.Add("@unit_size_sqm8", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size8.Text) * Convert.ToDecimal(0.0939034);

        }

        // if the unit size is in sq-m
        else
        {
            cmd.Parameters.Add("@unit_size_sqm8", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size8.Text);
            cmd.Parameters.Add("@unit_size_sqft8", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size8.Text) * Convert.ToDecimal(10.7639104);


        }

        cmd.Parameters.Add("@unit_bedroom_no8", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms8.Text);
        cmd.Parameters.Add("@unit_bathroom_no8", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms8.Text);
        cmd.Parameters.Add("@unit_level8", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level8.Text);

        cmd.Parameters.Add("@unit_max_tenant8", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants8.Text);


        /// 9 th unit to be added
        ///

        cmd.Parameters.Add("@unit_door_no9", SqlDbType.VarChar, 50).Value = RegEx.getText(tbx_door_no9.Text);
        cmd.Parameters.Add("@unit_size_system9", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system9.SelectedValue);

        // if the unit size is in sq-ft
        if (ddl_unit_size_system9.SelectedValue == "0")
        {
            cmd.Parameters.Add("@unit_size_sqft9", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size9.Text);
            cmd.Parameters.Add("@unit_size_sqm9", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size9.Text) * Convert.ToDecimal(0.0939034);

        }

        // if the unit size is in sq-m
        else
        {
            cmd.Parameters.Add("@unit_size_sqm9", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size9.Text);
            cmd.Parameters.Add("@unit_size_sqft9", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size9.Text) * Convert.ToDecimal(10.7639104);


        }

        cmd.Parameters.Add("@unit_bedroom_no9", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms9.Text);
        cmd.Parameters.Add("@unit_bathroom_no9", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms9.Text);
        cmd.Parameters.Add("@unit_level9", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level9.Text);

        cmd.Parameters.Add("@unit_max_tenant9", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants9.Text);

        /// 10 th unit to be added
        ///

        cmd.Parameters.Add("@unit_door_no10", SqlDbType.VarChar, 50).Value = RegEx.getText(tbx_door_no10.Text);
        cmd.Parameters.Add("@unit_size_system10", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system10.SelectedValue);

        // if the unit size is in sq-ft
        if (ddl_unit_size_system10.SelectedValue == "0")
        {
            cmd.Parameters.Add("@unit_size_sqft10", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size10.Text);
            cmd.Parameters.Add("@unit_size_sqm10", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size10.Text) * Convert.ToDecimal(0.0939034);

        }

        // if the unit size is in sq-m
        else
        {
            cmd.Parameters.Add("@unit_size_sqm10", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size10.Text);
            cmd.Parameters.Add("@unit_size_sqft10", SqlDbType.Decimal).Value = Convert.ToDecimal(tbx_unit_size10.Text) * Convert.ToDecimal(10.7639104);


        }

        cmd.Parameters.Add("@unit_bedroom_no10", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bedrooms10.Text);
        cmd.Parameters.Add("@unit_bathroom_no10", SqlDbType.Int).Value = Convert.ToInt32(tbx_nb_bathrooms10.Text);
        cmd.Parameters.Add("@unit_level10", SqlDbType.Int).Value = Convert.ToInt32(tbx_unit_level10.Text);

        cmd.Parameters.Add("@unit_max_tenant10", SqlDbType.Money).Value = Convert.ToInt32(tbx_max_tenants10.Text);


        cmd.ExecuteNonQuery();

        //reseting all the field in the page


        tbx_unit_size1.Text = "";
        tbx_nb_bedrooms1.Text = "";
        tbx_nb_bathrooms1.Text = "";
        tbx_unit_level1.Text = "";
        tbx_max_tenants1.Text = "";
        tbx_door_no1.Text = "";

        //---

        tbx_unit_size2.Text = "";
        tbx_nb_bedrooms2.Text = "";
        tbx_nb_bathrooms2.Text = "";
        tbx_unit_level2.Text = "";
        tbx_max_tenants2.Text = "";
        tbx_door_no2.Text = "";

        //---

        tbx_unit_size3.Text = "";
        tbx_nb_bedrooms3.Text = "";
        tbx_nb_bathrooms3.Text = "";
        tbx_unit_level3.Text = "";
        tbx_max_tenants3.Text = "";
        tbx_door_no3.Text = "";

        //--

        tbx_unit_size4.Text = "";
        tbx_nb_bedrooms4.Text = "";
        tbx_nb_bathrooms4.Text = "";
        tbx_unit_level4.Text = "";
        tbx_max_tenants4.Text = "";
        tbx_door_no4.Text = "";

        //--

        tbx_unit_size5.Text = "";
        tbx_nb_bedrooms5.Text = "";
        tbx_nb_bathrooms5.Text = "";
        tbx_unit_level5.Text = "";
        tbx_max_tenants5.Text = "";
        tbx_door_no5.Text = "";

        //--

        tbx_unit_size6.Text = "";
        tbx_nb_bedrooms6.Text = "";
        tbx_nb_bathrooms6.Text = "";
        tbx_unit_level6.Text = "";
        tbx_max_tenants6.Text = "";
        tbx_door_no6.Text = "";

        //--

        tbx_unit_size7.Text = "";
        tbx_nb_bedrooms7.Text = "";
        tbx_nb_bathrooms7.Text = "";
        tbx_unit_level7.Text = "";
        tbx_max_tenants7.Text = "";
        tbx_door_no7.Text = "";

        //--

        tbx_unit_size8.Text = "";
        tbx_nb_bedrooms8.Text = "";
        tbx_nb_bathrooms8.Text = "";
        tbx_unit_level8.Text = "";
        tbx_max_tenants8.Text = "";
        tbx_door_no8.Text = "";

        //--

        tbx_unit_size9.Text = "";
        tbx_nb_bedrooms9.Text = "";
        tbx_nb_bathrooms9.Text = "";
        tbx_unit_level9.Text = "";
        tbx_max_tenants9.Text = "";
        tbx_door_no9.Text = "";

        //--

        tbx_unit_size10.Text = "";
        tbx_nb_bedrooms10.Text = "";
        tbx_nb_bathrooms10.Text = "";
        tbx_unit_level10.Text = "";
        tbx_max_tenants10.Text = "";
        tbx_door_no10.Text = "";



        
        tiger.Home u = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        r_unit_list.DataSource = u.getHomeUnitList(home_id, Convert.ToInt32(Session["schema_id"]));
        r_unit_list.DataBind();

        //To view the address of the property

        tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
        rhome_view.DataBind();
   
    }
}
