﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="property_add.aspx.cs" Inherits="manager_property_property_add" Title="Untitled Page" %>

<%@ Register Src="~/shared/userControls/AlertMessage/AlertMessage.usercontrol.ascx" TagPrefix="uc1" TagName="AlertMessageusercontrol" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <asp:GridView ID="gv_list" runat="server"></asp:GridView>

    <uc1:AlertMessageusercontrol runat="server" ID="AlertMessageusercontrol" />
    Maximum units allowed:
    <asp:Label ID="lbl_max_unit" runat="server" /><br />
    #units used :
    <asp:Label ID="lbl_unit_used" runat="server" /><br />
    Free units:
    <asp:Label ID="lbl_free_unit" Font-Bold="true" Font-Size="XX-Large" runat="server" /><br />



    <asp:Label ID="lbl_count" runat="server" />
    Units entered
   <br />
    <br />
    <asp:Label ID="lbl_home_name" runat="server" />
    <asp:Label ID="lbl_message" runat="server" />
    <span style="width: 100%; text-align: right;">
        <asp:LinkButton ID="lb_more_unit" Text="Add more units" runat="server" OnClick="lb_more_unit_Click" /></span>
    <br />

    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">
            <asp:Panel ID="panel_form" runat="server" Visible="true">
                <div style="width: 700px; text-align: left; margin-left: 200px; font-size: small; font-weight: lighter">

                    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_home_name %>"></asp:Label>
                    <br />
                    <asp:TextBox ID="home_name" Width="300px" ValidationGroup="val_property" MaxLength="50" Height="15px" CssClass="mandatory" runat="server" />
                    <asp:RequiredFieldValidator Display="Dynamic" ID="as" ControlToValidate="home_name" runat="server" ErrorMessage="<%$ Resources:Resource, lbl_iv_validate %>" />
                    <asp:RegularExpressionValidator ID="reg_home_name" Display="Dynamic" ControlToValidate="home_name" runat="server" ErrorMessage="<%$ Resources:Resource, lbl_iv_invalid_char %>" />
                    <br />
                    <br />
                    <asp:Label ID="Label19" runat="server" Text="<%$ Resources:Resource, lbl_address_no %>"></asp:Label>
                    <br />
                    <asp:TextBox ID="home_addr_no" Width="50px" ValidationGroup="val_property" MaxLength="50" Height="15px" CssClass="mandatory" runat="server" />
                    <asp:RequiredFieldValidator Display="Dynamic" ID="fds" ControlToValidate="home_addr_no" runat="server" ErrorMessage="<%$ Resources:Resource, lbl_iv_validate %>" />
                    <asp:RegularExpressionValidator ID="reg_home_addr_no" Display="Dynamic" ControlToValidate="home_addr_no" runat="server" ErrorMessage="<%$ Resources:Resource, lbl_iv_invalid_char %>" />
                    <br />
                    <asp:Label ID="Label3" runat="server" ValidationGroup="val_property" Text="<%$ Resources:Resource, lbl_address_street %>"></asp:Label>
                    <br />
                    <asp:TextBox ID="home_addr_street" Width="300px" MaxLength="50" Height="15px" CssClass="mandatory" runat="server" />
                    <asp:RequiredFieldValidator ValidationGroup="val_property" Display="Dynamic" ID="RequiredFieldValidator1" ControlToValidate="home_addr_street" runat="server" ErrorMessage="<%$ Resources:Resource, lbl_iv_validate %>" />
                    <asp:RegularExpressionValidator ValidationGroup="val_property" ID="reg_home_addr_street" Display="Dynamic" ControlToValidate="home_addr_street" runat="server" ErrorMessage="<%$ Resources:Resource, lbl_iv_invalid_char %>" />
                    <br />
                    <asp:Label ID="Label5" runat="server" ValidationGroup="val_property" Text="<%$ Resources:Resource, lbl_city %>"></asp:Label>
                    <br />
                    <asp:TextBox ID="home_city" Width="300px" MaxLength="45" Height="15px" CssClass="mandatory" runat="server" />
                    <asp:RequiredFieldValidator ValidationGroup="val_property" Display="Dynamic" ID="RequiredFieldValidator2" ControlToValidate="home_city" runat="server" ErrorMessage="<%$ Resources:Resource, lbl_iv_validate %>" />
                    <asp:RegularExpressionValidator ValidationGroup="val_property" ID="reg_home_city" Display="Dynamic" ControlToValidate="home_city" runat="server" ErrorMessage="<%$ Resources:Resource, lbl_iv_invalid_char %>" />
                    <br />
                    Comments (optional)<br />
                    <asp:TextBox ID="home_desc" TextMode="MultiLine" Rows="3" Columns="50" runat="server" />


                    <br />

                    <asp:Button ID="btn_continu" runat="server"
                        Text="<%$ Resources:Resource, lbl_next %>" OnClick="btn_continu_Click" />
                </div>
            </asp:Panel>
        </asp:View>






        <asp:View ID="View2" runat="server">

            <table
                style="width: 700px" border="1" cellpadding="3" cellspacing="1" rules="ROWS,cols" frame="BOX"
                bordercolor="AliceBlue">

                <tr bgcolor="AliceBlue">
                    <td></td>
                    <td>
                        <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lbl_unit_level %>" />
                    </td>
                    <td bgcolor="AliceBlue">
                        <asp:Label ID="Labels4" runat="server" Text="<%$ Resources:Resource, lbl_door_number %>" /></td>
                    <td bgcolor="AliceBlue">
                        <asp:Label ID="Label10" runat="server" Text="<%$ Resources:Resource, lbl_unit_type %>" />
                    </td>
                    <td bgcolor="AliceBlue">
                        <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, lbl_number_of_bedrooms %>" /></td>
                    <td bgcolor="AliceBlue">
                        <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lbl_number_of_bathrooms %>" /></td>
                    <td bgcolor="AliceBlue">
                        <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, lbl_maximum_tenants %>" /></td>
                </tr>
                <asp:Panel ID="panel_1" runat="server" Visible="false">
                    <tr>
                        <td valign="top">1</td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_unit_level1" runat="server" Height="20px">
                                <asp:ListItem Value="0">Basement</asp:ListItem>
                                <asp:ListItem Value="1" Selected>First floor</asp:ListItem>
                                <asp:ListItem Value="2">Second floor</asp:ListItem>
                                <asp:ListItem Value="3">Third floor</asp:ListItem>
                                <asp:ListItem Value="4">Fourth floor</asp:ListItem>
                                <asp:ListItem Value="5">Fifth floor</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:TextBox ID="tbx_door_no1" runat="server" Width="60px" Height="15px"></asp:TextBox></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_unit_type1" runat="server">
                                <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_gl_residential %>" Value="R"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, lbl_gl_commercial %>" Value="C"></asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bedrooms1" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bathrooms1" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_max_tenants1" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>

                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="tbx_door_no1" runat="server"
                                ErrorMessage="The first record must not be empty"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </asp:Panel>
                <asp:Panel ID="panel_2" runat="server" Visible="false">

                    <tr>
                        <td>2</td>
                        <td>
                            <asp:DropDownList ID="ddl_unit_level2" runat="server" Height="20px">
                                <asp:ListItem Value="0">Basement</asp:ListItem>
                                <asp:ListItem Value="1" Selected>First floor</asp:ListItem>
                                <asp:ListItem Value="2">Second floor</asp:ListItem>
                                <asp:ListItem Value="3">Third floor</asp:ListItem>
                                <asp:ListItem Value="4">Fourth floor</asp:ListItem>
                                <asp:ListItem Value="5">Fifth floor</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:TextBox ID="tbx_door_no2" runat="server" Width="60px" Height="15px"></asp:TextBox></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_unit_type2" runat="server">
                                <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_gl_residential %>" Value="R"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, lbl_gl_commercial %>" Value="C"></asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bedrooms2" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bathrooms2" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_max_tenants2" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>

                            </asp:DropDownList>
                        </td>
                    </tr>
                </asp:Panel>
                <asp:Panel ID="panel_3" runat="server" Visible="false">


                    <tr>
                        <td>3</td>
                        <td>
                            <asp:DropDownList ID="ddl_unit_level3" runat="server" Height="20px">
                                <asp:ListItem Value="0">Basement</asp:ListItem>
                                <asp:ListItem Value="1" Selected>First floor</asp:ListItem>
                                <asp:ListItem Value="2">Second floor</asp:ListItem>
                                <asp:ListItem Value="3">Third floor</asp:ListItem>
                                <asp:ListItem Value="4">Fourth floor</asp:ListItem>
                                <asp:ListItem Value="5">Fifth floor</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:TextBox ID="tbx_door_no3" runat="server" Width="60px" Height="15px"></asp:TextBox></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_unit_type3" runat="server">
                                <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_gl_residential %>" Value="R"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, lbl_gl_commercial %>" Value="C"></asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bedrooms3" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bathrooms3" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_max_tenants3" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>

                            </asp:DropDownList>
                        </td>
                    </tr>
                </asp:Panel>
                <asp:Panel ID="panel_4" runat="server" Visible="false">


                    <tr>
                        <td>4</td>
                        <td>
                            <asp:DropDownList ID="ddl_unit_level4" runat="server" Height="20px">
                                <asp:ListItem Value="0">Basement</asp:ListItem>
                                <asp:ListItem Value="1" Selected>First floor</asp:ListItem>
                                <asp:ListItem Value="2">Second floor</asp:ListItem>
                                <asp:ListItem Value="3">Third floor</asp:ListItem>
                                <asp:ListItem Value="4">Fourth floor</asp:ListItem>
                                <asp:ListItem Value="5">Fifth floor</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:TextBox ID="tbx_door_no4" runat="server" Width="60px" Height="15px"></asp:TextBox></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_unit_type4" runat="server">
                                <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_gl_residential %>" Value="R"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, lbl_gl_commercial %>" Value="C"></asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bedrooms4" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bathrooms4" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_max_tenants4" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>

                            </asp:DropDownList>
                        </td>
                    </tr>
                </asp:Panel>
                <asp:Panel ID="panel_5" runat="server" Visible="false">


                    <tr>
                        <td>5</td>
                        <td>
                            <asp:DropDownList ID="ddl_unit_level5" runat="server" Height="20px">
                                <asp:ListItem Value="0">Basement</asp:ListItem>
                                <asp:ListItem Value="1" Selected>First floor</asp:ListItem>
                                <asp:ListItem Value="2">Second floor</asp:ListItem>
                                <asp:ListItem Value="3">Third floor</asp:ListItem>
                                <asp:ListItem Value="4">Fourth floor</asp:ListItem>
                                <asp:ListItem Value="5">Fifth floor</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:TextBox ID="tbx_door_no5" runat="server" Width="60px" Height="15px"></asp:TextBox></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_unit_type5" runat="server">
                                <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_gl_residential %>" Value="R"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, lbl_gl_commercial %>" Value="C"></asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bedrooms5" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bathrooms5" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_max_tenants5" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>

                            </asp:DropDownList>
                        </td>
                    </tr>
                </asp:Panel>
                <asp:Panel ID="panel_6" runat="server" Visible="false">


                    <tr>
                        <td>6</td>
                        <td>
                            <asp:DropDownList ID="ddl_unit_level6" runat="server" Height="20px">
                                <asp:ListItem Value="0">Basement</asp:ListItem>
                                <asp:ListItem Value="1" Selected>First floor</asp:ListItem>
                                <asp:ListItem Value="2">Second floor</asp:ListItem>
                                <asp:ListItem Value="3">Third floor</asp:ListItem>
                                <asp:ListItem Value="4">Fourth floor</asp:ListItem>
                                <asp:ListItem Value="5">Fifth floor</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:TextBox ID="tbx_door_no6" runat="server" Width="60px" Height="15px"></asp:TextBox></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_unit_type6" runat="server">
                                <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_gl_residential %>" Value="R"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, lbl_gl_commercial %>" Value="C"></asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bedrooms6" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bathrooms6" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_max_tenants6" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>

                            </asp:DropDownList>
                        </td>
                    </tr>
                </asp:Panel>
                <asp:Panel ID="panel_7" runat="server" Visible="false">



                    <tr>
                        <td valign="top">7</td>
                        <td>
                            <asp:DropDownList ID="ddl_unit_level7" runat="server" Height="20px">
                                <asp:ListItem Value="0">Basement</asp:ListItem>
                                <asp:ListItem Value="1" Selected>First floor</asp:ListItem>
                                <asp:ListItem Value="2">Second floor</asp:ListItem>
                                <asp:ListItem Value="3">Third floor</asp:ListItem>
                                <asp:ListItem Value="4">Fourth floor</asp:ListItem>
                                <asp:ListItem Value="5">Fifth floor</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:TextBox ID="tbx_door_no7" runat="server" Width="60px" Height="15px"></asp:TextBox></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_unit_type7" runat="server">
                                <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_gl_residential %>" Value="R"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, lbl_gl_commercial %>" Value="C"></asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bedrooms7" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bathrooms7" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_max_tenants7" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>

                            </asp:DropDownList>
                        </td>
                    </tr>
                </asp:Panel>
                <asp:Panel ID="panel_8" runat="server" Visible="false">
                    <tr>
                        <td valign="top">7</td>
                        <td>
                            <asp:DropDownList ID="ddl_unit_level8" runat="server" Height="20px">
                                <asp:ListItem Value="0">Basement</asp:ListItem>
                                <asp:ListItem Value="1" Selected>First floor</asp:ListItem>
                                <asp:ListItem Value="2">Second floor</asp:ListItem>
                                <asp:ListItem Value="3">Third floor</asp:ListItem>
                                <asp:ListItem Value="4">Fourth floor</asp:ListItem>
                                <asp:ListItem Value="5">Fifth floor</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:TextBox ID="tbx_door_no8" runat="server" Width="60px" Height="15px"></asp:TextBox></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_unit_type8" runat="server">
                                <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_gl_residential %>" Value="R"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, lbl_gl_commercial %>" Value="C"></asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bedrooms8" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bathrooms8" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_max_tenants8" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>

                            </asp:DropDownList>
                        </td>
                    </tr>
                </asp:Panel>
                <asp:Panel ID="panel_9" runat="server" Visible="false">

                    <tr>
                        <td valign="top">9</td>
                        <td>
                            <asp:DropDownList ID="ddl_unit_level9" runat="server" Height="20px">
                                <asp:ListItem Value="0">Basement</asp:ListItem>
                                <asp:ListItem Value="1" Selected>First floor</asp:ListItem>
                                <asp:ListItem Value="2">Second floor</asp:ListItem>
                                <asp:ListItem Value="3">Third floor</asp:ListItem>
                                <asp:ListItem Value="4">Fourth floor</asp:ListItem>
                                <asp:ListItem Value="5">Fifth floor</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:TextBox ID="tbx_door_no9" runat="server" Width="60px" Height="15px"></asp:TextBox></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_unit_type9" runat="server">
                                <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_gl_residential %>" Value="R"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, lbl_gl_commercial %>" Value="C"></asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bedrooms9" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bathrooms9" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_max_tenants9" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>

                            </asp:DropDownList>
                        </td>
                    </tr>
                </asp:Panel>
                <asp:Panel ID="panel_10" runat="server" Visible="false">

                    <tr>
                        <td valign="top">7</td>
                        <td>
                            <asp:DropDownList ID="ddl_unit_level10" runat="server" Height="20px">
                                <asp:ListItem Value="0">Basement</asp:ListItem>
                                <asp:ListItem Value="1" Selected>First floor</asp:ListItem>
                                <asp:ListItem Value="2">Second floor</asp:ListItem>
                                <asp:ListItem Value="3">Third floor</asp:ListItem>
                                <asp:ListItem Value="4">Fourth floor</asp:ListItem>
                                <asp:ListItem Value="5">Fifth floor</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:TextBox ID="tbx_door_no10" runat="server" Width="60px" Height="15px"></asp:TextBox></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_unit_type10" runat="server">
                                <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_gl_residential %>" Value="R"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, lbl_gl_commercial %>" Value="C"></asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bedrooms10" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_nb_bathrooms10" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                            </asp:DropDownList></td>
                        <td valign="top">
                            <asp:DropDownList ID="ddl_max_tenants10" runat="server" Height="20px">
                                <asp:ListItem Value="0">0</asp:ListItem>
                                <asp:ListItem Value="1" Selected>1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>

                            </asp:DropDownList>
                        </td>
                    </tr>
                </asp:Panel>
            </table>
            <br />


            <asp:Button ID="btn_back" runat="server" Text="<%$ Resources:Resource, lbl_gl_back%>" OnClick="btn_back_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btn_list" runat="server"
            Text="<%$ Resources:Resource, lbl_next %>"
            OnClick="btn_list_Click" />



        </asp:View>

        <asp:View ID="view_summary" runat="server">
            <h3>View</h3>
            Property Image &nbsp; &nbsp; &nbsp;
         <asp:FileUpload ID="FileUpload1" runat="server" EnableViewState="true" />
            <br />
             <table>
                        <tr>
                            <th>door #</th>
                            <th>level</th>
                            <th>bedroom</th>
                            <th>max tenant</th>

                        </tr>
            <asp:Repeater ID="gv_repeater_list" runat="server">
                <ItemTemplate>
                   
                        <tr>
                            <td><%# Eval("unit_door_no") %></td>
                            <td><%# Eval("unit_level") %></td>
                            <td><%# Eval("unit_bedroom_no") %></td>
                            <td><%# Eval("unit_max_tenant") %></td>

                        </tr>

                    
                </ItemTemplate>
            </asp:Repeater></table>
             
            <asp:Button ID="btn_submit" runat="server" Text="Submit" OnClick="btn_submit_Click" />
        </asp:View>

    </asp:MultiView>


</asp:Content>

