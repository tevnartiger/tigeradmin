﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="financial_analysis_period_view.aspx.cs" Inherits="manager_Financial_financial_analysis_period_view" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<asp:Label ID="Label2" runat="server" 
                    Text="<%$ Resources:Resource, lbl_financial_analysis%>" /> &nbsp;FOR A PERIOD<br /><br />
<table>
<tr><td> <asp:Label ID="lbl_property" runat="server" Text="<%$ Resources:Resource, lbl_property %>"/>
               :&nbsp;
    <asp:Label ID="lbl_property_name" runat="server" Text="Label"></asp:Label>
               </td>
                <td>
       </td>  
    <td>
        <asp:HyperLink ID="HyperLink1" runat="server" Text="<%$ Resources:Resource, lbl_back_to_list %>"
            NavigateUrl="~/manager/Financial/financial_analysis_period_list.aspx"></asp:HyperLink>
    </td>
</tr>
 </table><br />
 
  <table style="width: 100%">
        <tr>
            <td valign="top">
 <asp:Repeater runat="server" ID="rhome_view">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
        
        <tr>
          <td>
                 District&nbsp;:&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_district")%> 
                   </td>
            </tr>
           <tr>
              <td valign="top" >
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_no")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_street")%></td>
            </tr>
            <tr>
                 <td valign="top" >
                   <%#DataBinder.Eval(Container.DataItem, "home_city")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_prov")%></td>
               
            </tr>
            <tr>
                 <td valign="top" >
                   <%#DataBinder.Eval(Container.DataItem, "home_pc")%> 
                
                   </td>
               
            </tr>   
        </table>
        </ItemTemplate>
        </asp:Repeater>
            </td>
            <td valign="top">
                <table bgcolor="#ffffcc"  >
                    
                    <tr>
                    
                    
                       <td>
                           Analysis name&nbsp;</td>
                <td>
                    <asp:Label  BackColor="White" ID="lbl_analysis_name" runat="server"></asp:Label>
                </td>
                  <td>
                      &nbsp;</td>
                    
                    </tr>
                    
                    <tr>
                    
                    
                       <td valign="top">
                           Analysis comments&nbsp;</td>
                <td>
                    <b>
                    <asp:Label BackColor="White" ID="lbl_analysis_comments" runat="server" Height="73px" TextMode="MultiLine" 
                         Width="100%"></asp:Label>
                    </b>
                </td>
                  <td valign="top">
                      &nbsp;&nbsp;&nbsp;
                        </td>
                    
                    </tr>
                    
                    </table>
               
            </td>
        </tr>
    </table>
    <br />
    
    
    <table >
        <tr>
            <td>
                <asp:Label ID="lbl_property_value" runat="server" 
                    Text="<%$ Resources:Resource, lbl_property_value %>" style="font-weight: 700" ></asp:Label>
            &nbsp;:</td>
            <td>
                <asp:Label ID="lbl_home_value" runat="server" Width="65px"></asp:Label>
            </td>
             <td>
               </td>
            <td>
                
            &nbsp;&nbsp;&nbsp;
            </td>
            
            <td> 
                <asp:Button ID="btn_expense_view_graph" runat="server" onclick="btn_expense_view_graph_Click" 
                    Text="<%$ Resources:Resource, lbl_view_expense_graph %>" Visible="False" />
               </td>
            <td>
                
            &nbsp;<asp:Button ID="btn_income_view_graph" runat="server" onclick="btn_income_view_graph_Click" 
                    Text="<%$ Resources:Resource, lbl_view_income_graph %>" Visible="False" />
            </td>
        </tr>
        
    </table><br />
    
     <table style="width: 100%">
        <tr>
            <td bgcolor="AliceBlue">
                <asp:Label ID="lbl_revenue" runat="server" 
                    Text="<%$ Resources:Resource, lbl_revenue %>" style="font-weight: 700"/> </td>
            
            <td bgcolor="AliceBlue">
                <asp:Label ID="lbl_monthly" runat="server" 
                    Text="<%$ Resources:Resource, lbl_from %>" style="font-weight: 700"/>&nbsp;:&nbsp; 
                <asp:Label ID="lbl_from2" runat="server" Text="Label"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label34" runat="server" 
                    Text="<%$ Resources:Resource, lbl_to %>" style="font-weight: 700"/>&nbsp;:&nbsp;
                     <asp:Label ID="lbl_to2" runat="server" Text="Label"></asp:Label>
                    
                </td>
           
        </tr>
        
        
        <asp:Repeater ID="rNumberUnitbyBedroomNumber" runat="server">
    <ItemTemplate>
    <tr>
    <td> <%#DataBinder.Eval(Container.DataItem, "number_of_unit")%> &nbsp;X &nbsp;<%#DataBinder.Eval(Container.DataItem, "unit_bedroom_no")%>&nbsp;&nbsp; <asp:Label ID="lbl_bedroom" runat="server" 
                    Text="<%$ Resources:Resource, lbl_bedroom %>"/></td>
    
   
    <td><asp:Label ID="lbl_bedrooms_total_rent_m" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "total_rent_received")%>'  />
                     
        <asp:HiddenField ID="h_number_of_unit" Value='<%#DataBinder.Eval(Container.DataItem, "number_of_unit")%>' runat="server" /> 
        <asp:HiddenField ID="h_unit_bedroom_number" Value='<%#DataBinder.Eval(Container.DataItem, "unit_bedroom_no")%>' runat="server" />
        
                     </td>
    </tr>
    </ItemTemplate>
    </asp:Repeater>
    
  
   <asp:Repeater ID="rIncome"   runat="server">
    <ItemTemplate  >
        <tr  >
            <td valign="top" style="height: 18px">
                <asp:Label ID="Label40" runat="server" 
                    Text='<%#GetIncomeCateg(Convert.ToInt32(Eval("incomecateg_id")))%>'   />&nbsp;</td>
            <td valign="top"  >
                <%#DataBinder.Eval(Container.DataItem, "income_amount","{0:0.00}")%>
                <asp:HiddenField ID="h_income_amount" Value='<%#DataBinder.Eval(Container.DataItem, "income_amount")%>' runat="server" />
        
            </td>
           
           
        </tr>
        
        </ItemTemplate>
    </asp:Repeater>
  
  
        
        <tr>
            <td>
                <asp:Label ID="Label6" runat="server" 
                    Text="<%$ Resources:Resource, lbl_late_fee %>"/></td>
            
            <td>
                <asp:Label ID="lbl_late_fee" runat="server" Width="65px"></asp:Label>
            </td>
           
        </tr>
        
          <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
            
        </tr>
        
        <tr>
            <td>
                <asp:Label ID="Label17" runat="server" 
                     Text="<%$ Resources:Resource, lbl_total_income %>" style="font-weight: 700"/>&nbsp;</td>
           
            <td>
               <asp:Label ID="lbl_gi_m" runat="server" /></td>
            
        </tr>
        
        <tr>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
           
        </tr>
        
        <tr>
            <td bgcolor="AliceBlue">
                <asp:Label ID="Label5" runat="server" 
                    Text="<%$ Resources:Resource, lbl_expenses%>" style="font-weight: 700"/></td>
            
            <td bgcolor="AliceBlue">
                 <asp:Label ID="Label3" runat="server" 
                     Text="<%$ Resources:Resource, lbl_from %>" style="font-weight: 700"/>&nbsp;:&nbsp; 
                <asp:Label ID="lbl_from3" runat="server" Text="Label"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label7" runat="server" 
                    Text="<%$ Resources:Resource, lbl_to %>" style="font-weight: 700"/>&nbsp;:&nbsp;
                     <asp:Label ID="lbl_to3" runat="server" Text="Label"></asp:Label></td>
            
        </tr>
        
             
    <asp:Repeater ID="rExpense"   runat="server">
    <ItemTemplate  >
        <tr  >
            <td valign="top" style="height: 18px">
                <asp:Label ID="Label40" runat="server" 
                    Text='<%#GetExpenseCateg(Convert.ToInt32(Eval("expensecateg_id")))%>'   />&nbsp;</td>
            <td valign="top"  >
                <%#DataBinder.Eval(Container.DataItem, "expense_amount","{0:0.00}")%>
                
                <asp:HiddenField ID="h_expense_amount" Value='<%#DataBinder.Eval(Container.DataItem, "expense_amount")%>' runat="server" />
        
            </td>
          
            
        </tr>
        
        </ItemTemplate>
    </asp:Repeater>
       
        <tr>
            <td style="height: 18px">
                &nbsp;</td>
            <td style="height: 18px">
                &nbsp;</td>
            
        </tr>
    
     <tr>
            <td style="height: 18px">
                <asp:Label ID="Label15" runat="server" 
                    Text="<%$ Resources:Resource, lbl_total_expenses %>" 
                    style="font-weight: 700"/></td>
            
            <td  >
                <asp:Label ID="lbl_total_expenses_m" runat="server" 
                    Text="lbl_total_expenses_m" /></td>
            
        </tr>
        <tr>
            <td style="height: 18px">
                <br />
            </td>
            <td style="height: 18px">
                &nbsp;</td>
            
        </tr>
        
        
        
        <tr>
            <td bgcolor="AliceBlue">
                <asp:Label ID="Label16" runat="server" 
                    Text="<%$ Resources:Resource, lbl_money_flow %>" 
                    style="font-weight: 700" /></td>
           
            <td bgcolor="AliceBlue">
                 <asp:Label ID="Label4" runat="server" 
                     Text="<%$ Resources:Resource, lbl_from %>" style="font-weight: 700"/> &nbsp;:&nbsp;
                     <asp:Label ID="lbl_from4" runat="server" Text="" />&nbsp;&nbsp;
                <asp:Label ID="Label1" runat="server" 
                    Text="<%$ Resources:Resource, lbl_to %>" style="font-weight: 700"/>&nbsp;:&nbsp;
                     <asp:Label ID="lbl_to4" runat="server" Text=""></asp:Label></td>
        </tr>
        <tr>
            <td>
               <asp:Label ID="Label29" runat="server" 
                     Text="<%$ Resources:Resource, lbl_net_operating_inc %>" /></td>
            
            <td>
                <asp:Label ID="lbl_net_operating_inc_m" runat="server"   /></td>
        </tr>
    
        <tr>
            <td>
               <asp:Label ID="Label30" runat="server" 
                     Text="<%$ Resources:Resource, lbl_mortgage %>"/></td>
           
            <td>
                <asp:Label ID="lbl_debt_service_m" runat="server" Width="65px"></asp:Label>
                <hr />
            </td>
        </tr>
    
         <tr>
            <td>
                <asp:Label ID="Label31" runat="server" 
                     Text="<%$ Resources:Resource, lbl_liquidity %>" 
                    style="font-weight: 700" /></td>
            
            <td>
                <asp:Label ID="lbl_liquidity_m" runat="server"  /></td>
        </tr>
    
    
         <tr>
            <td>
                &nbsp;</td>
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                <td>
                &nbsp;</td>
           
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                 <asp:Label ID="Label32" runat="server" 
                     Text="<%$ Resources:Resource, lbl_capitalisation2 %>" /></td>
            
            <td>
                <asp:Label ID="lbl_capitalisation_m" runat="server" Width="65px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label33" runat="server" 
                     Text="<%$ Resources:Resource, lbl_liquidity_capitalisation %>" 
                    style="font-weight: 700" /></td>
            
            <td>
                <asp:Label ID="lbl_liquidity_capitalisation_m" runat="server" /></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
            
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
               <asp:Label ID="Label8" runat="server" 
                     Text="<%$ Resources:Resource, lbl_added_value %>" /></td>
           
            <td>
                <asp:Label ID="lbl_added_value_m" runat="server" Width="65px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label35" runat="server" 
                     Text="<%$ Resources:Resource, lbl_liq_cap_value %>" 
                    style="font-weight: 700" /></td>
            
            <td>
                <asp:Label ID="lbl_liq_cap_value_m" runat="server" /></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
           
            <td>
                &nbsp;</td>
               
                
            
            <td>
                &nbsp;</td>
        </tr>
    
   
    
    
    <tr>
            <td colspan="2">
                &nbsp;<br />
                </td>
            <td colspan="2">
                &nbsp;</td>
            
        </tr>
        
         <tr>
            <td align="center" bgcolor="AliceBlue" colspan="3">
                <b><asp:Label ID="Label39" runat="server" 
                     Text="<%$ Resources:Resource, lbl_analysis_return %>" /></b></td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label40" runat="server" 
                     Text="<%$ Resources:Resource, lbl_appartment_cost%>" /></td>
            <td colspan="2">
                <asp:Label ID="lbl_appartment_cost" runat="server"/></td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label41" runat="server" 
                     Text="<%$ Resources:Resource, lbl_exploitation_ratio%>" /></td>
            <td>
                <asp:Label ID="lbl_rde" runat="server"/></td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label43" runat="server" 
                     Text="<%$ Resources:Resource, lbl_performance_on_net %>" /></td>
            <td>
                <asp:Label ID="lbl_trn" runat="server"/></td>
        </tr>
        
         <tr>
            <td colspan="2">
               <asp:Label ID="Label44" runat="server" 
                     Text="<%$ Resources:Resource, lbl_gi_multiplier %>" /></td>
            <td >
                <asp:Label ID="lbl_mbre" runat="server"></asp:Label></td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label47" runat="server" 
                     Text="<%$ Resources:Resource, lbl_net_income_multiplier %>" /></td>
            <td colspan="2">
           <asp:Label ID="lbl_mrn" runat="server"></asp:Label>     
            </td>
        </tr>
        
        <tr>
            <td colspan="2">
               <asp:Label ID="Label46" runat="server" 
                     Text="<%$ Resources:Resource, lbl_debt_cover_ratio %>" /></td>
            <td>
               <asp:Label ID="lbl_rcd" runat="server"></asp:Label></td>
        </tr>
    </table>
    
<asp:HiddenField ID="h_total_number_of_unit"  runat="server" />

    <br />


</asp:Content>

