<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="portfolio_remove.aspx.cs" Inherits="portfolio_group_group_profile" Title="Untitled Page" %>
<%@ Register Src="~/manager/uc/uc_content_menu_group.ascx" TagName="uc_content_menu_group" TagPrefix="uc_content_menu_group" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <table width="800">
<tr><td><h2>Remove Property Group</h2></td></tr>
<tr><td><asp:DropDownList ID="ddl_group" runat="server" DataTextField="group_name" DataValueField="group_id" AutoPostBack="true" OnSelectedIndexChanged="ddl_group_SelectedIndexChanged" />
&nbsp;&nbsp;<asp:Button ID="bn_remove" runat="server" Text="Remove" 
        onclick="bn_remove_Click" /><br />
        <asp:Literal ID="lt_response" runat="server" /></td></tr>
        <tr><td align="center"><uc_content_menu_group:uc_content_menu_group ID="uc_group" runat="server" /></uc_content_menu_group:uc_content_menu_group><br />
<br /></td></tr>
<tr><td>
 <asp:GridView ID="gv_home" runat="server" AutoGenerateColumns="false" DataKeyNames="home_id" Width="800">
 <Columns>
      <asp:HyperLinkField  DataNavigateUrlFields="home_id" DataNavigateUrlFormatString="/manager/home/home_profile?home_id={0}"  HeaderText="" DataTextField="home_name" />
  
   <asp:TemplateField HeaderText="Owner">
 <ItemTemplate>
 <asp:Label ID="lbl_b" runat="server" Text='<%# getOwnerProfile(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "home_id"))) %>' />
 </ItemTemplate>
 </asp:TemplateField>
 <asp:TemplateField HeaderText="Property Manager">
 <ItemTemplate>
 <asp:Label ID="lbl_b" runat="server" Text='<%# getManagerProfile(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "home_id"))) %>' />
 </ItemTemplate>
 </asp:TemplateField>
 <asp:TemplateField HeaderText="Janitor">
 <ItemTemplate>
 <asp:Label ID="lbl_b" runat="server" Text='<%# getJanitorProfile(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "home_id"))) %>' />
 </ItemTemplate>
 </asp:TemplateField>
 </Columns>
 </asp:GridView>
</td></tr>
</table>
 
</asp:Content>

