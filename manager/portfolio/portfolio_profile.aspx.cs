using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class portfolio_group_group_profile : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        { 
        //filter 

        }

        if (!Page.IsPostBack)
        {
              int temp_group_id = 0;
              tiger.Group g = new tiger.Group(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));


              if (Convert.ToInt32(Request.QueryString["group_id"]) > 0)
              {
                  temp_group_id = Convert.ToInt32(Request.QueryString["group_id"]);
              }
              else
              {
                  //parameter group exist find 1st occurence of a group
                  int first_group_id = g.groupFindFirst(Convert.ToInt32(Session["schema_id"]));
                  if (first_group_id > 0)
                  {
                          temp_group_id = first_group_id;
                  }
                  else
                  {
                  //    lbl_profile.Text = "No group exist, please create a group";
                  }
              }
            //validate group_id
              //lbl_profile.Text = temp_group_id +"----"+ddl_group.SelectedValue;
                
              if (temp_group_id > 0)
              {
                  ddl_group.DataSource = g.getGroupHomeList(Convert.ToInt32(Session["schema_id"]));
                  ddl_group.DataBind();
                 // ddl_group.Items.Insert(0, new ListItem(Resources.Resource.lbl_group_profile, "0"));
                  ddl_group.SelectedValue = Convert.ToString(temp_group_id);


                  //group.InnerHtml = g.getGroupProfileHomeView(Convert.ToInt32(Session["schema_id"]), temp_group_id);
                  gv_home.DataSource = g.getGroupProfileSingleHomeList(Convert.ToInt32(Session["schema_id"]), temp_group_id);
                  gv_home.DataBind();
                  //gv_home.Rows[1].ToString();

              }
              //txt.Text = temp_group_id.ToString();
        }
    }

    protected void ddl_group_SelectedIndexChanged(object sender, EventArgs e)
    {
        //filter group 
        int temp_group_id = Convert.ToInt32(ddl_group.SelectedValue);
        tiger.Group g = new tiger.Group(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        //lbl_profile.Text = temp_group_id.ToString();

       // group.InnerHtml = g.getGroupProfileHomeView(Convert.ToInt32(Session["schema_id"]), temp_group_id); 

     //   ddl_group.DataSource = g.getGroupProfileHomeView(Convert.ToInt32(Session["schema_id"]), temp_group_id);
      //      ddl_group.DataBind();
            // ddl_group.Items.Insert(0, new ListItem(Resources.Resource.lbl_group_profile, "0"));
            //ddl_group.SelectedValue = Convert.ToString(temp_group_id);


           // group.InnerHtml = g.getGroupProfileHomeView(Convert.ToInt32(Session["schema_id"]), temp_group_id);
             gv_home.DataSource = g.getGroupProfileSingleHomeList(Convert.ToInt32(Session["schema_id"]), temp_group_id);
             gv_home.DataBind();
            // gv_home.Rows[1].ToString();

       // txt.Text = temp_group_id.ToString();
    }

    protected string getOwnerProfile(int home_id)
    {
        string str_view = "";
        //get owner
        SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]); 
        SqlCommand cmd = new SqlCommand("prProfileOwnerInfo", conn);
        cmd.CommandType = CommandType.StoredProcedure;


        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

        try
        {
            conn.Open();

            SqlDataReader dr = null;
            dr = cmd.ExecuteReader();

            while (dr.Read() == true)
            {
                str_view += "<a href='/manager/name/name_profile.aspx?name_id=" + Convert.ToString(dr["name_id"]) + "'>" + Convert.ToString(dr["owner"]) + "</a><br/>" + Convert.ToString(dr["owner_tel"]) + "<br/>" + Convert.ToString(dr["owner_email"]) + "<br/>";
                //get owner
            }

            return str_view;
        }
        finally
        {
            conn.Close();
        }

    }

     protected string getManagerProfile(int home_id)
        {
            string str_view = "";
            //get owner
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]); 
            SqlCommand cmd = new SqlCommand("prProfileManagerInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader();

                while (dr.Read() == true)
                {
                    str_view += "<a href='/manager/name/name_profile.aspx?name_id=" + Convert.ToString(dr["name_id"]) + "'>" + Convert.ToString(dr["manager"]) + "</a><br/>" + Convert.ToString(dr["manager_tel"]) + "<br/>" + Convert.ToString(dr["manager_email"]) + "<br/>";
                    //get owner
                }

                return str_view;
            }
            finally
            {
                conn.Close();
            }



        }

        protected string getJanitorProfile(int home_id)
        {
            //get Janitor
            string str_view = "";
            //get owner
            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]); 
            SqlCommand cmd = new SqlCommand("prProfileJanitorInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader();

                while (dr.Read() == true)
                {
                    str_view += "<a href='/manager/name/name_profile.aspx?name_id=" + Convert.ToString(dr["name_id"]) + "'>" + Convert.ToString(dr["janitor"]) + "</a><br/>" + Convert.ToString(dr["janitor_tel"]) + "<br/>" + Convert.ToString(dr["janitor_email"]);
                    //get owner
                }

                return str_view;
            }
            finally
            {
                conn.Close();
            }

        }

}
     
 
 
