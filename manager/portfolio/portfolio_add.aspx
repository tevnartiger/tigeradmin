<%@ Page Language="C#" MasterPageFile="../mp_manager.master" AutoEventWireup="true" CodeFile="portfolio_add.aspx.cs" Inherits="portfolio_group_add" Title="New group" %>
 <%@ Register Src="~/manager/uc/uc_content_menu_group.ascx" TagName="uc_content_menu_group" TagPrefix="uc_content_menu_group" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 
<asp:MultiView ID="mv" runat="server" ActiveViewIndex="0"> 
<asp:View ID="view1" runat="server">
<table width="800">
<tr><td><h2>New Portfolio</h2>
</td></tr>
 <tr><td align="center"><uc_content_menu_group:uc_content_menu_group ID="uc_group" runat="server" /></uc_content_menu_group:uc_content_menu_group><br />
</td></tr>
<tr><td>
(1)<asp:Literal runat="server"  ID="Literal1" Text="<%$ resources:resource, lbl_group_type %>" />
<br /><br />
<asp:TextBox ID="tb_group_name" MaxLength="15" width="100"  runat="server"></asp:TextBox><br />
       <asp:RequiredFieldValidator ID="ada" runat="server" ControlToValidate="tb_group_name" Display="dynamic" Text="<%$ resources:resource, iv_required %>" />
       <asp:RegularExpressionValidator runat="server" ID="reg_group_name" Display="dynamic" ControlToValidate="tb_group_name" Text="<%$ resources:resource, iv_invalid_char %>" />
      <asp:Literal ID="lt_message" runat="server" />
    <br />
        <br />
 
(2)<asp:Literal runat="server"  ID="Literal2" Text="<%$ resources:resource, lbl_group_select_property %>" />
<br /><br /><br />
     <asp:CheckBoxList RepeatDirection="Vertical" ID="cbl_home_list" DataTextField="home_name" DataValueField="home_id" runat="server" />
 <br /> 
  <asp:Button ID="btn_create" runat="server" OnClick="btn_create_Click"  Text="<%$ resources:resource,lbl_group_create %>" />

</td></tr>
 
</table>
 
     </asp:View>
 
 
  
 </asp:MultiView>
 

<!--<div id="txt_message" runat="server" />
 <asp:GridView ID="group_list" runat="server" />
 
 </div>
    -->
</asp:Content>