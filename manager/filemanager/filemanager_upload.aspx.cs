﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Briefcase;
using System.IO;
//
// Done by : Stanley Jocelyn
// Date : sept 22 , 2009
//
public partial class manager_filemanager_filemanager_upload : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        { 

            tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = h.getHomeCount(Convert.ToInt32(Session["schema_id"]));

            if (home_count > 0)
            {

                gv_home_list.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                gv_home_list.DataBind();

            
            }
        
        
        }

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_upload_Click(object sender, EventArgs e)
    {

        Page.Validate();
        if (Page.IsValid)
        {
            int number_of_insert = 0;
            string home_id = "";
            string name_id_ip = "";

            try
            {

                string filename = Path.GetFileName(FileUpload2.PostedFile.FileName);
                int filesize = 0;
                if (FileUpload2.HasFile)
                {

                    Briefcase.Files f = new Files();
                    Stream stream = FileUpload2.PostedFile.InputStream;
                    filesize = FileUpload2.PostedFile.ContentLength;
                    byte[] filedata = new byte[filesize];
                    stream.Read(filedata, 0, filesize);

                    //---------------------------------------------------------

                    for (int j = 0; j < gv_home_list.Rows.Count; j++)
                    {

                        CheckBox chk_process = (CheckBox)gv_home_list.Rows[j].FindControl("chk_process");
                        // if the checkbox is checked the process
                        if (chk_process.Checked)
                        {
                            HiddenField h_id = (HiddenField)gv_home_list.Rows[j].FindControl("h_home_id");

                            home_id = home_id + h_id.Value + "|";
                            number_of_insert++;
                        }
                    }


                    //----------------------------------------------------------




                    name_id_ip = Request.UserHostAddress.ToString();
                    // this is the trace name_id

                    string selected_group = radio_group.SelectedValue;
                    switch (selected_group)
                    {
                        case "3": f.TenantCreate(filename, filedata, filesize, DateTime.Now, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), name_id_ip, home_id, number_of_insert);
                            break;
                        case "4": f.OwnerCreate(filename, filedata, filesize, DateTime.Now, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), name_id_ip, home_id, number_of_insert);
                            break;
                        case "5": f.JanitorCreate(filename, filedata, filesize, DateTime.Now, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), name_id_ip, home_id, number_of_insert);
                            break;
                        case "6": f.PMCreate(filename, filedata, filesize, DateTime.Now, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), name_id_ip, home_id, number_of_insert);
                            break;

                    }
                }


            }
            catch (Exception ex)
            {
                // Label11.Text = ex.Message;// Resources.Resource.lbl_duplicate_file_name;
            }
        }
    }
}
