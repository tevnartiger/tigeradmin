<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="filemanager.aspx.cs" Inherits="_default" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asc" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">

   <style type="text/css">
body
{
	font-family:Verdana;
}
.text
{
	background-color:#ebe9ed;
	font-size:11px;
	text-align:center;
}
.textContent
{
	font-size:11px;
	text-align:center;
}
.modalBackground 
{
   background-color:#000;
   filter:alpha(opacity=70);
   opacity:0.7;
}
           .style1
           {
               width: 100%;
           }
       </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asc:ScriptManager ID="ScriptManager1" runat="server">
</asc:ScriptManager>
    
    <script language="javascript" type="text/javascript">



        function fnClickUpdate(sender, e) {

            var tbx1 = document.getElementById('ctl00_ContentPlaceHolder1_TabContainer1_tab1_TextBox1').value;


            // alert(tbx1);
            if (tbx1.length > 0) {
                __doPostBack(sender, e);
            }


        }


        function ConfirmDelete() {
            if (confirm('Do you wish to delete this file/folder?'))
                return true;
            else
                return false;
        }



        function CheckAllDataViewCheckBoxes(checkVal) {

            for (i = 0; i < document.forms[0].elements.length; i++) {

                elm = document.forms[0].elements[i];
                if (elm.type == 'checkbox' && elm.name.substring(elm.name.length - 11, elm.name.length) == 'chk_process') {
                    { elm.checked = checkVal }
                }
            }
        }


        function CheckAllDataViewCheckBoxes2(checkVal) {

            for (i = 0; i < document.forms[0].elements.length; i++) {

                elm = document.forms[0].elements[i];
                if (elm.type == 'checkbox' && elm.name.substring(elm.name.length - 12, elm.name.length) == 'chk_process2') {
                    { elm.checked = checkVal }
                }
            }
        }

        function CheckAllDataViewCheckBoxes3(checkVal) {

            for (i = 0; i < document.forms[0].elements.length; i++) {

                elm = document.forms[0].elements[i];
                if (elm.type == 'checkbox' && elm.name.substring(elm.name.length - 12, elm.name.length) == 'chk_process3') {
                    { elm.checked = checkVal }
                }
            }
        }


        function CheckAllDataViewCheckBoxes4(checkVal) {

            for (i = 0; i < document.forms[0].elements.length; i++) {

                elm = document.forms[0].elements[i];
                if (elm.type == 'checkbox' && elm.name.substring(elm.name.length - 12, elm.name.length) == 'chk_process4') {
                    { elm.checked = checkVal }
                }
            }
        }
</script>

 

    
  
    
   <h2>OWNER</h2>
   <table ><tr ><td  bgcolor="#003366"><B  >
                    <asp:Label ID="Label4"  EnableTheming="False" ForeColor="White"  
            runat="server" Text="VIEW FILE"></asp:Label>

                    </B></td>
        <td bgcolor="#003366">
            &nbsp;</td>
        </tr><tr><td>Shared With owner(s) of&nbsp;&nbsp;&nbsp;
     <asp:DropDownList ID="ddl_home_owner_list" runat="server"  AutoPostBack="True"
                                   DataTextField="home_name" DataValueField="home_id" 
                       onselectedindexchanged="ddl_home_owner_list_SelectedIndexChanged" ></asp:DropDownList></td>
            <td valign="top">
                <table class="style1">
                    <tr>
                        <td valign="top">
                            Owner(s) name :</td>
                        <td valign="top">
                             <asp:Label ID="lbl_owner_list" runat="server"/>
                          </td>
                    </tr>
                </table>
            </td>
        </tr></table><br />
                       <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
                         BorderColor="#CDCDCD"  BorderWidth="1px"
                        DataKeyNames="ownerfiles_id" OnRowCommand="GridView2_RowCommand"
                        OnRowEditing="GridView2_RowEditing"   Width="83%"
                        OnRowCancelingEdit="GridView2_RowCancelingEdit" 
                        OnRowUpdating="GridView2_RowUpdating" OnRowDataBound="GridView2_RowDataBound"><AlternatingRowStyle BackColor="White" /><Columns><asp:BoundField DataField="home_name" HeaderText="Property" ReadOnly="True" /><asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_file_name %>"><EditItemTemplate><asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FileName") %>'></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1"
                                        Display="Dynamic" ErrorMessage="Please enter file name" Font-Bold="True"></asp:RequiredFieldValidator></EditItemTemplate><ItemTemplate><asp:Label ID="Label1" runat="server" Text='<%# Eval("FileName") %>'></asp:Label></ItemTemplate></asp:TemplateField><asp:BoundField DataField="DateCreated" 
                                HeaderText="<%$ Resources:Resource, lbl_created_on %>" ReadOnly="True" 
                                DataFormatString="{0:MMM dd , yyyy}"  HtmlEncode="False"/><asp:ButtonField ButtonType="Image" ImageUrl="Earth-Download-24x24.png" CommandName="Download" Text="<%$ Resources:Resource, lbl_download %>" /><asp:TemplateField ShowHeader="False"><ItemTemplate><asp:HiddenField Visible="false" ID="h_file_id" runat="server" Value='<%# Bind("ownerfiles_id") %>'  />
                                <asp:ImageButton ImageUrl="Symbols-Delete-16x16.png" ID="btn_delete" runat="server" CausesValidation="False" CommandArgument='<%# Eval("ownerfiles_id") %>'
                                        CommandName="DeleteFile" OnClick="btn_delete_Click" OnClientClick="return ConfirmDelete();"
                                        Text="<%$ Resources:Resource, lbl_delete %>" /></ItemTemplate></asp:TemplateField>
                                        <asp:CommandField ButtonType="Button" ShowEditButton="True"
                                                         EditText="<%$ Resources:Resource, lbl_rename %>" 
                                                         UpdateText="<%$ Resources:Resource, lbl_change %>" >
                               <ControlStyle BackColor="#003366" ForeColor="White" />
                               </asp:CommandField>
                           </Columns><FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" /><PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" /><RowStyle BackColor="#E3EAEB" /><SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" /></asp:GridView><br /><br /><table ><tr ><td  bgcolor="#003366"><B >
    
          
            <asp:Label ID="Label11" runat="server"></asp:Label>

            </td></tr></table>


    <h2>TENANTS</h2>
        
            <table>
                    <tr>
                        <td bgcolor="#003366">
                            <b>
                        <asp:Label ID="Label6" runat="server" EnableTheming="False" ForeColor="White" 
                                Text="VIEW FILE"></asp:Label></b></td></tr><tr>
                        <td>
                            Shared With tenants of &nbsp;&nbsp;&nbsp;
                            <asp:DropDownList ID="ddl_home_tenant_list" runat="server" AutoPostBack="True" 
                                DataTextField="home_name" DataValueField="home_id" 
                                onselectedindexchanged="ddl_home_tenant_list_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <br />
                <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" 
                    DataKeyNames="tenantfiles_id" OnRowCancelingEdit="GridView3_RowCancelingEdit" 
                    OnRowCommand="GridView3_RowCommand" OnRowDataBound="GridView3_RowDataBound" 
                    OnRowEditing="GridView3_RowEditing" OnRowUpdating="GridView3_RowUpdating" 
                    Width="83%">
                    <Columns>
                        <asp:BoundField DataField="home_name" HeaderText="Property" ReadOnly="True" />
                        <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_file_name %>">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FileName") %>'></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="TextBox1" Display="Dynamic" 
                                    ErrorMessage="Please enter file name" Font-Bold="True">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("FileName") %>'></asp:Label></ItemTemplate></asp:TemplateField><asp:BoundField DataField="DateCreated" DataFormatString="{0:MMMM dd , yyyy}" 
                            HeaderText="<%$ Resources:Resource, lbl_created_on %>" HtmlEncode="False" 
                            ReadOnly="True" />
                        <asp:ButtonField ButtonType="Image" CommandName="Download" 
                            ImageUrl="Earth-Download-24x24.png" 
                            Text="<%$ Resources:Resource, lbl_download %>" />
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:HiddenField Visible="false" ID="h_file_id" runat="server" 
                                    Value='<%# Bind("tenantfiles_id") %>' />
                                <asp:ImageButton ID="btn_tenant_delete" runat="server" CausesValidation="False" 
                                    CommandArgument='<%# Eval("tenantfiles_id") %>' CommandName="DeleteFile" 
                                    ImageUrl="Symbols-Delete-16x16.png" OnClick="btn_tenant_delete_Click" 
                                    OnClientClick="return ConfirmDelete();" 
                                    Text="<%$ Resources:Resource, lbl_delete %>" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ButtonType="Button" EditText="<%$ Resources:Resource, lbl_rename %>" 
                            ShowEditButton="True" UpdateText="<%$ Resources:Resource, lbl_change %>" >
                        <ControlStyle BackColor="#003366" ForeColor="White" />
                        </asp:CommandField>
                    </Columns>
                </asp:GridView>
                <br />
                <br />
                  
               
                
                
                
                
                
                
                
        <h2>
            PROPERTY MANAGER</h2>
         <table>
                    <tr>
                        <td bgcolor="#003366">
                            <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label8" runat="server" EnableTheming="False" ForeColor="White" 
                                Text="VIEW FILE"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </b></td>
                        <td bgcolor="#003366">
                            &nbsp;</td></tr><tr>
                        <td>
                            Shared With property manager(s) of &nbsp;&nbsp;&nbsp;
                            <asp:DropDownList ID="ddl_home_pm_list" runat="server" AutoPostBack="True" 
                                DataTextField="home_name" DataValueField="home_id" 
                                OnSelectedIndexChanged="ddl_home_pm_list_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <table class="style1">
                                <tr>
                                    <td valign="top">
                                        Property manager(s) name :</td><td valign="top">
                                        <asp:Label ID="lbl_pm_list" runat="server"></asp:Label></td></tr></table></td></tr></table><br />
                <asp:GridView ID="GridView4" runat="server" AutoGenerateColumns="False" 
                    DataKeyNames="pmfiles_id" OnRowCancelingEdit="GridView4_RowCancelingEdit" 
                    OnRowCommand="GridView4_RowCommand" OnRowDataBound="GridView4_RowDataBound" 
                    OnRowEditing="GridView4_RowEditing" OnRowUpdating="GridView4_RowUpdating" 
                    Width="83%">
                    <Columns>
                        <asp:BoundField DataField="home_name" HeaderText="Property" ReadOnly="True" />
                        <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_file_name %>">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FileName") %>'></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="TextBox1" Display="Dynamic" 
                                    ErrorMessage="Please enter file name" Font-Bold="True">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("FileName") %>'></asp:Label></ItemTemplate></asp:TemplateField><asp:BoundField DataField="DateCreated" DataFormatString="{0:MMM dd , yyyy}" 
                            HeaderText="<%$ Resources:Resource, lbl_created_on %>" HtmlEncode="False" 
                            ReadOnly="True" />
                        <asp:ButtonField ButtonType="Image" CommandName="Download" 
                            ImageUrl="Earth-Download-24x24.png" 
                            Text="<%$ Resources:Resource, lbl_download %>" />
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:HiddenField Visible="false" ID="h_file_id" runat="server" 
                                    Value='<%# Bind("pmfiles_id") %>' />
                                <asp:ImageButton ID="btn_pm_delete" runat="server" CausesValidation="False" 
                                    CommandArgument='<%# Eval("pmfiles_id") %>' CommandName="DeleteFile" 
                                    ImageUrl="Symbols-Delete-16x16.png" OnClick="btn_pm_delete_Click" 
                                    OnClientClick="return ConfirmDelete();" 
                                    Text="<%$ Resources:Resource, lbl_delete %>" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ButtonType="Button" EditText="<%$ Resources:Resource, lbl_rename %>" 
                            ShowEditButton="True" UpdateText="<%$ Resources:Resource, lbl_change %>" >
                        <ControlStyle BackColor="#003366" ForeColor="White" />
                        </asp:CommandField>
                    </Columns>
                </asp:GridView>
                <br />
                <br />

                
                
                
                
     <h2>JANITOR</h2>
            
            
            
            
            
            
        
         <table>
                    <tr>
                        <td bgcolor="#003366">
                            <b>
                            
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label22" runat="server" EnableTheming="False" ForeColor="White" 
                                Text="VIEW FILE"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </b>
                        </td>
                        <td bgcolor="#003366">
                            &nbsp;</td></tr><tr>
                        <td>
                            Shared With property janitor(s) of &nbsp;&nbsp;&nbsp;
                            <asp:DropDownList ID="ddl_home_janitor_list" runat="server" AutoPostBack="True" 
                                DataTextField="home_name" DataValueField="home_id" 
                                onselectedindexchanged="ddl_home_janitor_list_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td valign="top">
                            <table class="style1">
                               <tr>
                             <td valign="top">
                            Janitor(s) name :</td><td valign="top">
                            <asp:Label ID="lbl_janitor_list" runat="server"></asp:Label></td></tr></table></td></tr></table><br />
                <asp:GridView ID="GridView5" runat="server" AutoGenerateColumns="False" 
                    DataKeyNames="janitorfiles_id" OnRowCancelingEdit="GridView5_RowCancelingEdit" 
                    OnRowCommand="GridView5_RowCommand" OnRowDataBound="GridView5_RowDataBound" 
                    OnRowEditing="GridView5_RowEditing" OnRowUpdating="GridView5_RowUpdating" 
                    Width="83%">
                    <Columns>
                        <asp:BoundField DataField="home_name" HeaderText="Property" ReadOnly="True" />
                        <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_file_name %>">
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FileName") %>'></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                    ControlToValidate="TextBox1" Display="Dynamic" 
                                    ErrorMessage="Please enter file name" Font-Bold="True">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </asp:RequiredFieldValidator>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("FileName") %>'></asp:Label></ItemTemplate></asp:TemplateField><asp:BoundField DataField="DateCreated" DataFormatString="{0:MMM dd , yyyy}" 
                            HeaderText="<%$ Resources:Resource, lbl_created_on %>" HtmlEncode="False" 
                            ReadOnly="True" />
                        <asp:ButtonField ButtonType="Image" CommandName="Download" 
                            ImageUrl="Earth-Download-24x24.png" 
                            Text="<%$ Resources:Resource, lbl_download %>" />
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:HiddenField Visible="false" ID="h_file_id" runat="server" 
                                    Value='<%# Bind("janitorfiles_id") %>' />
                                <asp:ImageButton ID="btn_janitor_delete" runat="server" CausesValidation="False" 
                                    CommandArgument='<%# Eval("janitorfiles_id") %>' CommandName="DeleteFile" 
                                    ImageUrl="Symbols-Delete-16x16.png" OnClick="btn_janitor_delete_Click" 
                                    OnClientClick="return ConfirmDelete();" 
                                    Text="<%$ Resources:Resource, lbl_delete %>" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField ButtonType="Button" EditText="<%$ Resources:Resource, lbl_rename %>" 
                            ShowEditButton="True" UpdateText="<%$ Resources:Resource, lbl_change %>" >
                        <ControlStyle BackColor="#003366" ForeColor="White" />
                        </asp:CommandField>
                    </Columns>
                </asp:GridView>
                
  
     
   </asp:Content>