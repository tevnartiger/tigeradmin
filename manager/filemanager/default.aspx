﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="default.aspx.cs" Inherits="_default" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="spl" Namespace="OboutInc.Splitter2" Assembly="obout_Splitter2_Net" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asc" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asc:ScriptManager ID="ScriptManager1" runat="server">
</asc:ScriptManager>
       <style type="text/css">
body
{
	font-family:Verdana;
}
.text
{
	background-color:#ebe9ed;
	font-size:11px;
	text-align:center;
}
.textContent
{
	font-size:11px;
	text-align:center;
}
</style>
    <script language="javascript" type="text/javascript">
        function ConfirmDelete() {
            if (confirm('Do you wish to delete this file/folder?'))
                return true;
            else
                return false;
        }



        function CheckAllDataViewCheckBoxes(checkVal) {

            for (i = 0; i < document.forms[0].elements.length; i++) {

                elm = document.forms[0].elements[i];
                if (elm.type == 'checkbox' && elm.name.substring(elm.name.length - 11, elm.name.length) == 'chk_process') {
                    { elm.checked = checkVal }
                }
            }
        }


        function CheckAllDataViewCheckBoxes2(checkVal) {

            for (i = 0; i < document.forms[0].elements.length; i++) {

                elm = document.forms[0].elements[i];
                if (elm.type == 'checkbox' && elm.name.substring(elm.name.length - 12, elm.name.length) == 'chk_process2') {
                    { elm.checked = checkVal }
                }
            }
        }
</script>

 

    
 
<br /><br />
     
     
     <h1>OFFICE</h1>
   
     <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lbl_u_file_management %>">
     </asp:Label><br /><br />
    

			
		
			
			
			
			
			<table>
			
			
			<tr>
			
			<td valign="top">			
			<div style="width:100%;height:100%;">
				 
				 <asp:TreeView ID="TreeView1" runat="server"   
                             Font-Bold="True"  
                             OnSelectedNodeChanged="TreeView1_SelectedNodeChanged"  
                             ForeColor="Black" ShowLines="True">
                  <NodeStyle ChildNodesPadding="5px" 
                             HorizontalPadding="3px" />
                 <SelectedNodeStyle BackColor="SteelBlue" ForeColor="White" />
                </asp:TreeView>
        
			</div>		
			</td>
			
			
			
			<td valign="top">
			
			<div style="width:100%;height:100%;">
			 
				  <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
                         BorderColor="#CDCDCD"  BorderWidth="1px"   
                        DataKeyNames="Id" OnRowCommand="GridView1_RowCommand"
                        OnRowEditing="GridView1_RowEditing"  
                        OnRowCancelingEdit="GridView1_RowCancelingEdit" 
                        OnRowUpdating="GridView1_RowUpdating" OnRowDataBound="GridView1_RowDataBound"><AlternatingRowStyle   BackColor="White" /><Columns><asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_file_name %>"><EditItemTemplate><asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FileName") %>'></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1"
                                        Display="Dynamic" ErrorMessage="Please enter file name" Font-Bold="True"></asp:RequiredFieldValidator></EditItemTemplate><ItemTemplate><asp:Label ID="Label1" runat="server" Text='<%# Eval("FileName") %>'></asp:Label></ItemTemplate></asp:TemplateField><asp:BoundField DataField="FileSize" HeaderText="<%$ Resources:Resource, lbl_size_bytes %>" ReadOnly="True" /><asp:BoundField DataField="DateCreated" 
                                HeaderText="<%$ Resources:Resource, lbl_created_on %>" ReadOnly="True" 
                                DataFormatString="{0:MMM dd , yyyy}"  HtmlEncode="False"/><asp:ButtonField ButtonType="Button" CommandName="Download" Text="<%$ Resources:Resource, lbl_download %>" /><asp:TemplateField ShowHeader="False"><ItemTemplate><asp:HiddenField ID="h_folder_id" runat="server" Value='<%# Bind("FolderId") %>' /><asp:HiddenField ID="h_file_id" runat="server" Value='<%# Bind("Id") %>'  /><asp:Button ID="Button1" runat="server" CausesValidation="False" CommandArgument='<%# Eval("Id") %>'
                                        CommandName="DeleteFile" OnClick="Button1_Click1" OnClientClick="return ConfirmDelete();"
                                        Text="<%$ Resources:Resource, lbl_delete %>" /></ItemTemplate></asp:TemplateField><asp:CommandField ButtonType="Button" ShowEditButton="True" EditText="<%$ Resources:Resource, lbl_rename %>" UpdateText="<%$ Resources:Resource, lbl_change %>" /></Columns><FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" /><PagerStyle  BackColor="#666666" ForeColor="White" HorizontalAlign="Center" /><SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
               </asp:GridView>
             
               <br />
                <table  cellpadding="3" cellspacing="0"><tr><td align="center" colspan="2" ><asp:Label ID="Label7" runat="server" BackColor="Yellow" EnableViewState="False"
            ForeColor="Red" Font-Bold="True" ></asp:Label></td></tr><tr><td valign="top" >
              </td>
              <td valign="top">
              </td></tr><tr><td colspan="2"><br /><br /><table cellpadding="5" style="background-color: whitesmoke"><tr><td colspan="3"><asp:Label ID="Label6" runat="server" Font-Bold="True"  ForeColor="Red"
                        Text="Tasks"></asp:Label></td></tr><tr><td align="right" valign="top"><asp:Label ID="Label1" runat="server" Font-Bold="True"  Text="New folders"></asp:Label></td><td align="left"><asp:TextBox ID="TextBox1" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1"
                        Display="Dynamic" ErrorMessage="<%$ Resources:Resource, lbl_enter_folder_name %>" Font-Bold="True" ValidationGroup="createfolder"></asp:RequiredFieldValidator><br /><asp:CheckBox ID="CheckBox1" runat="server" Font-Bold="True" 
                        Text="<%$ Resources:Resource, lbl_create_at_root_level %>" Font-Size="Small" /></td><td align="left" valign="top"><asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="<%$ Resources:Resource, lbl_created_on %>" Font-Bold="True"  ValidationGroup="createfolder" /></td></tr><tr><td align="right"><asp:Label ID="Label2" runat="server" Text="rename selected root folder" Font-Bold="True" ></asp:Label></td><td align="left"><asp:TextBox ID="TextBox2" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox2"
                        Display="Dynamic" ErrorMessage="<%$ Resources:Resource, lbl_folder_new_name %>" Font-Bold="True"
                        ValidationGroup="renamefolder"></asp:RequiredFieldValidator></td><td align="left"><asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="<%$ Resources:Resource, lbl_rename %>" Font-Bold="True"  ValidationGroup="renamefolder" /></td></tr><tr><td align="right"><asp:Label ID="Label3" runat="server" Font-Bold="True"  Text="<%$ Resources:Resource, lbl_delete_selected_folder %>"></asp:Label></td><td align="left"><asp:Label ID="Label10" runat="server" Font-Bold="True" ></asp:Label></td><td align="left"><asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="<%$ Resources:Resource, lbl_delete %>" Font-Bold="True" OnClientClick="return ConfirmDelete();" /></td></tr><tr><td align="right"><asp:Label ID="Label9" runat="server" Font-Bold="True"  Text="<%$ Resources:Resource, lbl_upload_file %>"></asp:Label></td><td align="left"><asp:FileUpload ID="FileUpload1" runat="server" /><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="FileUpload1"
                        Display="Dynamic" ErrorMessage="<%$ Resources:Resource, lbl_file_to_upload %>" Font-Bold="True"
                        ValidationGroup="uploadfile"></asp:RequiredFieldValidator></td><td align="left"><asp:Button ID="Button4" runat="server"    
                        Text="<%$ Resources:Resource, lbl_upload %>" Font-Bold="True" 
                        ValidationGroup="uploadfile"  OnClick="Button4_Click" /></td></tr></table></td></tr><tr><td colspan="2"></td></tr>
   </table>
    
			</div>
			
			
			</td>
			
			
			</tr>
			
			
			</table>
		
     

     
    
   
  
  
  
  
  
  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    <h2>OWNER</h2>
  <br /><br /><table ><tr ><td  bgcolor="aliceblue"><B >VIEW FILE</B></td></tr><tr><td>Shared With &nbsp;&nbsp;&nbsp;
     <asp:DropDownList ID="ddl_home_owner_list" runat="server"  AutoPostBack="True"
                                   DataTextField="home_name" DataValueField="home_id" 
                       onselectedindexchanged="ddl_home_owner_list_SelectedIndexChanged" ></asp:DropDownList></td></tr></table><br />
                       <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" 
                         BorderColor="#CDCDCD"  BorderWidth="1px"
                        DataKeyNames="ownerfiles_id" OnRowCommand="GridView2_RowCommand"
                        OnRowEditing="GridView2_RowEditing"  
                        OnRowCancelingEdit="GridView2_RowCancelingEdit" 
                        OnRowUpdating="GridView2_RowUpdating" OnRowDataBound="GridView2_RowDataBound"><AlternatingRowStyle BackColor="White" /><Columns><asp:BoundField DataField="home_name" HeaderText="Property" ReadOnly="True" /><asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_file_name %>"><EditItemTemplate><asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FileName") %>'></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1"
                                        Display="Dynamic" ErrorMessage="Please enter file name" Font-Bold="True"></asp:RequiredFieldValidator></EditItemTemplate><ItemTemplate><asp:Label ID="Label1" runat="server" Text='<%# Eval("FileName") %>'></asp:Label></ItemTemplate></asp:TemplateField><asp:BoundField DataField="DateCreated" 
                                HeaderText="<%$ Resources:Resource, lbl_created_on %>" ReadOnly="True" 
                                DataFormatString="{0:MMMM dd , yyyy}"  HtmlEncode="False"/><asp:ButtonField ButtonType="Button" CommandName="Download" Text="<%$ Resources:Resource, lbl_download %>" /><asp:TemplateField ShowHeader="False"><ItemTemplate><asp:HiddenField ID="h_file_id" runat="server" Value='<%# Bind("ownerfiles_id") %>'  /><asp:Button ID="btn_delete" runat="server" CausesValidation="False" CommandArgument='<%# Eval("ownerfiles_id") %>'
                                        CommandName="DeleteFile" OnClick="btn_delete_Click" OnClientClick="return ConfirmDelete();"
                                        Text="<%$ Resources:Resource, lbl_delete %>" /></ItemTemplate></asp:TemplateField><asp:CommandField ButtonType="Button" ShowEditButton="True" EditText="<%$ Resources:Resource, lbl_rename %>" UpdateText="<%$ Resources:Resource, lbl_change %>" /></Columns><FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" /><HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" /><PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" /><RowStyle BackColor="#E3EAEB" /><SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" /></asp:GridView><br /><br /><table ><tr ><td  bgcolor="aliceblue"><B >UPLOAD A FILE   (CLIC HERE)
            <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label>
            </B></td></tr></table><table cellpadding="5" style="background-color: beige"><tr><td>Share with Owner of wich properties </td><td colspan="2">
                 
            
              
                </td></tr><tr><td>&#160;</td><td colspan="2"><asp:GridView 
                    ID="gv_home_list" runat="server" AllowPaging="True" 
                        AllowSorting="True" 
                        AutoGenerateColumns="False" BorderColor="#CDCDCD" BorderWidth="1px"   PageSize="100" 
                        ><AlternatingRowStyle BackColor="#F0F0F6" />
                <Columns>
                        
                             <asp:TemplateField HeaderText="<input id='chkAllItems' type='checkbox' onclick='CheckAllDataViewCheckBoxes(document.forms[0].chkAllItems.checked)' />"  >
                            <ItemTemplate>
                            <asp:CheckBox  runat="server"   ID="chk_process"   /> 
                                <asp:HiddenField ID="h_home_id" Value='<%# Bind("home_id")%>' runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:BoundField DataField="home_name" HeaderText="Property name" />                          
                            <asp:HyperLinkField DataNavigateUrlFields="home_id" 
                                DataNavigateUrlFormatString="~/manager/property/property_view.aspx?h_id={0}" 
                                HeaderText="View" Text="View"  />
                        </Columns><HeaderStyle BackColor="#F0F0F6" />
                </asp:GridView></td></tr><tr><td align="right"><asp:Label ID="Label16" runat="server" Font-Bold="True" 
                            Text="<%$ Resources:Resource, lbl_upload_file %>"></asp:Label></td><td align="left"><asp:FileUpload ID="FileUpload2" runat="server" /><asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                            ControlToValidate="FileUpload2" Display="Dynamic" 
                            ErrorMessage="<%$ Resources:Resource, lbl_file_to_upload %>" Font-Bold="True" 
                            ValidationGroup="uploadfile2"></asp:RequiredFieldValidator></td><td align="left"><asp:Button ID="Button8" runat="server" Font-Bold="True" 
                            OnClick="Button8_Click" Text="<%$ Resources:Resource, lbl_upload %>" 
                            ValidationGroup="uploadfile2"  /></td></tr></table>
        
      
        
     
        
        
        
        
        
    
    
    
         <br /><br />
        <asp:Label ID="Label15" runat="server" ></asp:Label><br />
        <asp:Label ID="Label19" runat="server" ></asp:Label>
        
      
        
     
        
        
        
        
        
    
    
  
   
     
     
     
     
     
     
    <h2>TENANTS</h2>
    <table ><tr ><td  bgcolor="aliceblue"><B >VIEW FILE</B></td></tr><tr><td>Shared With &nbsp;&nbsp;&nbsp; 
    <asp:DropDownList ID="ddl_home_tenant_list" runat="server"  AutoPostBack="True"
                                   DataTextField="home_name" DataValueField="home_id" 
                       onselectedindexchanged="ddl_home_tenant_list_SelectedIndexChanged" ></asp:DropDownList>
                    
                
                       </td></tr></table><br /><asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" 
                         BorderColor="#CDCDCD"  BorderWidth="1px"
                        DataKeyNames="tenantfiles_id" OnRowCommand="GridView3_RowCommand"
                        OnRowEditing="GridView3_RowEditing"  
                        OnRowCancelingEdit="GridView3_RowCancelingEdit" 
                        OnRowUpdating="GridView3_RowUpdating" OnRowDataBound="GridView3_RowDataBound"><Columns><asp:BoundField DataField="home_name" HeaderText="Property" ReadOnly="True" /><asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_file_name %>"><EditItemTemplate><asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FileName") %>'></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1"
                                        Display="Dynamic" ErrorMessage="Please enter file name" Font-Bold="True"></asp:RequiredFieldValidator></EditItemTemplate><ItemTemplate><asp:Label ID="Label1" runat="server" Text='<%# Eval("FileName") %>'></asp:Label></ItemTemplate></asp:TemplateField><asp:BoundField DataField="DateCreated" 
                                HeaderText="<%$ Resources:Resource, lbl_created_on %>" ReadOnly="True" 
                                DataFormatString="{0:MMMM dd , yyyy}"  HtmlEncode="False"/><asp:ButtonField ButtonType="Button" CommandName="Download" Text="<%$ Resources:Resource, lbl_download %>" /><asp:TemplateField ShowHeader="False"><ItemTemplate><asp:HiddenField ID="h_file_id" runat="server" Value='<%# Bind("tenantfiles_id") %>'  /><asp:Button ID="btn_tenant_delete" runat="server" CausesValidation="False" CommandArgument='<%# Eval("tenantfiles_id") %>'
                                        CommandName="DeleteFile" OnClick="btn_tenant_delete_Click" OnClientClick="return ConfirmDelete();"
                                        Text="<%$ Resources:Resource, lbl_delete %>" /></ItemTemplate></asp:TemplateField><asp:CommandField ButtonType="Button" ShowEditButton="True" EditText="<%$ Resources:Resource, lbl_rename %>" UpdateText="<%$ Resources:Resource, lbl_change %>" /></Columns><AlternatingRowStyle BackColor="White" /><FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" /><HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" /><RowStyle BackColor="#E3EAEB" /><SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" /></asp:GridView><br /><br />
  <table ><tr ><td  bgcolor="aliceblue"><B >UPLOAD A FILE</B></td></tr></table>
  <table cellpadding="5" style="background-color: beige"><tr><td valign="top">Share with Tenant of wich properties </td>
  <td colspan="2">
                                   
                                   
                                   
                                   
                                   
                                   
                                   
                                    
                       <asp:GridView 
                    ID="gv_home_tenant_list" runat="server" AllowPaging="True" 
                        AllowSorting="True" 
                        AutoGenerateColumns="False" BorderColor="#CDCDCD" BorderWidth="1px"   PageSize="100" 
                        ><AlternatingRowStyle BackColor="#F0F0F6" />
                <Columns>
                        
                             <asp:TemplateField HeaderText="<input id='chkAllItems2' type='checkbox' onclick='CheckAllDataViewCheckBoxes2(document.forms[0].chkAllItems2.checked)' />"  >
                            <ItemTemplate>
                            <asp:CheckBox  runat="server"   ID="chk_process2"   /> 
                                <asp:HiddenField ID="h_home_id" Value='<%# Bind("home_id")%>' runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:BoundField DataField="home_name" HeaderText="Property name" />                          
                            <asp:HyperLinkField DataNavigateUrlFields="home_id" 
                                DataNavigateUrlFormatString="~/manager/property/property_view.aspx?h_id={0}" 
                                HeaderText="View" Text="View" />
                        </Columns><HeaderStyle BackColor="#F0F0F6" />
                </asp:GridView>  
                                   
                                   
                                   
                                   
                                   
                                   
                                   
                                   </td>
                      </tr><tr><td align="right"><asp:Label ID="Label12" runat="server" Font-Bold="True" 
                         Text="<%$ Resources:Resource, lbl_upload_file %>"></asp:Label></td><td align="left"><asp:FileUpload ID="FileUpload3" runat="server" /><asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                        ControlToValidate="FileUpload3" Display="Dynamic" 
                        ErrorMessage="<%$ Resources:Resource, lbl_file_to_upload %>" Font-Bold="True" 
                        ValidationGroup="uploadfile3"></asp:RequiredFieldValidator></td><td align="left"><asp:Button ID="Button9" runat="server"
                        Text="<%$ Resources:Resource, lbl_upload %>" Font-Bold="True" 
                      OnClick="Button9_Click"  ValidationGroup="uploadfile3" /></td></tr></table>
    
    
    
    
    
         <br /><br />
        <asp:Label ID="Label13" runat="server" ></asp:Label><br />
        <asp:Label ID="Label14" runat="server" ></asp:Label>
        

     
   
     
     
    
</asp:Content>