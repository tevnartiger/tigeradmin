using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Briefcase;
using System.IO;

// Done by : Stanley Jocelyn
// Date : sept 12 , 2008
// Briefcase
public partial class _default : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            FillFolderTree(null);
            //Button3.Enabled = false;

            
            tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
           // ddl_home_list.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
           // ddl_home_list.DataBind();
           // ddl_home_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
            int home_count = h.getHomeCount(Convert.ToInt32(Session["schema_id"]));

            if (home_count > 0)
            {
                int home_id = h.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));

                ddl_home_owner_list.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                ddl_home_owner_list.DataBind();
               ddl_home_owner_list.SelectedValue = Convert.ToString(home_id);

                ddl_home_tenant_list.DataSource = ddl_home_owner_list.DataSource;
                ddl_home_tenant_list.DataBind();
                ddl_home_tenant_list.SelectedValue = Convert.ToString(home_id);


             //   ddl_home_tenant_list2.DataSource = ddl_home_owner_list.DataSource;
             //   ddl_home_tenant_list2.DataBind();
             //   ddl_home_tenant_list2.SelectedValue = Convert.ToString(home_id);



                gv_home_list.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                gv_home_list.DataBind();

                gv_home_tenant_list.DataSource  = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                gv_home_tenant_list.DataBind();



                TenantBindGrid();
                OwnerBindGrid();


              //  DataTable files = Files.GetFilesForTenant(int.Parse(ddl_home_tenant_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
               // Grid1.DataSource = files;
               // Grid1.DataBind();
            }

        }
    }

    private void BindGrid()
    {
       
        DataTable files = Files.GetFilesFromFolder(int.Parse(TreeView1.SelectedNode.Value.ToString()), Convert.ToInt32(Session["schema_id"]),0);
        GridView1.DataSource = files;
        GridView1.DataBind();
    }


    private void OwnerBindGrid()
    {

        DataTable files = Files.GetFilesForOwner(int.Parse(ddl_home_owner_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
        GridView2.DataSource = files;
        GridView2.DataBind();
    }


    private void TenantBindGrid()
    {

        DataTable files = Files.GetFilesForTenant(int.Parse(ddl_home_tenant_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
        GridView3.DataSource = files;
        GridView3.DataBind();
    }

    
    private void FillFolderTree(TreeNode parent)
    {
        int parentfolderid = 0;
        if (parent != null)
        {
            parentfolderid = int.Parse(parent.Value.ToString());
        }
                                                           // who the folder belongs to ( 0: everybody , else : an individual)
        DataTable folders = Folders.GetSubFolders(parentfolderid,Convert.ToInt32(Session["schema_id"]),0);

        if (parent != null)
        {
            parent.ChildNodes.Clear();
        }
        else
        {
            TreeView1.Nodes.Clear();
        }

        foreach (DataRow folder in folders.Rows)
        {
            TreeNode node = new TreeNode();
            node.Text = folder["FolderName"].ToString();
            node.Value = folder["Id"].ToString();
            if (parent == null)
            {
                TreeView1.Nodes.Add(node);
            }
            else
            {
                parent.ChildNodes.Add(node);
            }
        }
        if (TreeView1.SelectedNode != null)
        {
            TreeView1.SelectedNode.Expand();
        }
    }

    protected void TreeView1_SelectedNodeChanged(object sender, EventArgs e)
    {
        FillFolderTree(TreeView1.SelectedNode);
        TextBox2.Text = TreeView1.SelectedNode.Text;
        Label10.Text = TreeView1.SelectedNode.Text;
        BindGrid();

        //TabContainer1.ActiveTabIndex = 0;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string name_id_ip = Request.UserHostAddress.ToString();

        try
        {
            if (TreeView1.SelectedNode != null && !CheckBox1.Checked)
            {                                                                                                              
                // explaining the parameters
                // 0 : the folder was created by management and it can be acces by everybody
                // Convert.ToInt32(Session["name_id"]) : who ceated the folder
                // name_id_ip : the ip of the user who created the folder

                Folders.Create(TextBox1.Text, int.Parse(TreeView1.SelectedNode.Value.ToString()), DateTime.Now, Convert.ToInt32(Session["schema_id"]), 0, Convert.ToInt32(Session["name_id"]),name_id_ip);
                FillFolderTree(TreeView1.SelectedNode);

            }
            else
            {
                Folders.Create(TextBox1.Text, 0, DateTime.Now, Convert.ToInt32(Session["schema_id"]), 0, Convert.ToInt32(Session["name_id"]), name_id_ip);
                FillFolderTree(null);
            }
        }
        catch (Exception ex)
        {
            Label7.Text = ex.Message;
        }

    }



    protected void Button2_Click(object sender, EventArgs e)
    {
        try  
        {
            int id = int.Parse(TreeView1.SelectedNode.Value.ToString());

            // explaining the parameters
            // 0 : it can be acces by everybody
            // Convert.ToInt32(Session["name_id"]) : who is trying to rename the folder
            // Request.UserHostAddress.ToString() : the ip of the user who created the folder

               

            Folders.Rename(id, TextBox2.Text, Convert.ToInt32(Session["schema_id"]), 0, Convert.ToInt32(Session["name_id"]),
                            Request.UserHostAddress.ToString());
            //DataTable folders = Folders.GetSubFolders(id);
            TreeView1.SelectedNode.Text = TextBox2.Text;
        }
        catch (Exception ex)
        {
            Label7.Text = ex.Message;
        }

    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Button3_Click(object sender, EventArgs e)
    {
        if (TreeView1.SelectedNode != null)
        {
            Folders.Delete(int.Parse(TreeView1.SelectedNode.Value.ToString()), Convert.ToInt32(Session["schema_id"]),0);
            Files.DeleteFromFolder(int.Parse(TreeView1.SelectedNode.Value.ToString()), Convert.ToInt32(Session["schema_id"]),0);

            foreach (TreeNode node in TreeView1.SelectedNode.ChildNodes)
            {
                Folders.Delete(int.Parse(node.Value.ToString()), Convert.ToInt32(Session["schema_id"]),0);
                Files.DeleteFromFolder(int.Parse(node.Value.ToString()), Convert.ToInt32(Session["schema_id"]),0);
            }

            if (TreeView1.SelectedNode.Parent != null)
            {
                FillFolderTree(TreeView1.SelectedNode.Parent);
            }
            else
            {
                FillFolderTree(null);
            }
            GridView1.DataSource = null;
            GridView1.DataBind();
        }
        else
        {
            Label7.Text = Resources.Resource.lbl_folder_to_be_deleted;
        }

       // Button3.Enabled = false;
    }
    

    protected void Button4_Click(object sender, EventArgs e)
    {

        string name_id_ip = Request.UserHostAddress.ToString();

         try
        {
           
            if (TreeView1.SelectedNode != null)
            {
                int folderid = int.Parse(TreeView1.SelectedNode.Value);
                string filename = Path.GetFileName(FileUpload1.PostedFile.FileName);
                int filesize = 0;
                if (FileUpload1.HasFile)
                {
                   
                    Stream stream = FileUpload1.PostedFile.InputStream;
                    filesize = FileUpload1.PostedFile.ContentLength;
                    byte[] filedata = new byte[filesize];
                    stream.Read(filedata, 0, filesize);

                    // explaining the parameters
                    // 0 : the File will belong to everybody in the account (schema)
                    // Convert.ToInt32(Session["name_id"]) : who ceated the folder
                    // name_id_ip : the ip of the user who created the folder


                    Files.Create(filename, filedata, filesize, folderid, DateTime.Now, Convert.ToInt32(Session["schema_id"]),
                                 0, Convert.ToInt32(Session["name_id"]),name_id_ip);
                  
                }
                BindGrid();
            }
            else
            {
                Label7.Text = Resources.Resource.lbl_destination_folder;
            }
        }
        catch (Exception ex)
        {
            Label7.Text = ex.ToString(); // Resources.Resource.lbl_duplicate_file_name;
        }

    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int fileid = int.Parse(GridView1.DataKeys[int.Parse(e.CommandArgument.ToString())].Value.ToString());

        if (e.CommandName == "Download")
        {
            DataTable file = Files.GetFile(fileid, Convert.ToInt32(Session["schema_id"]),0);
            byte[] filedata = (byte[])file.Rows[0]["filedata"];
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment;filename=" + file.Rows[0]["filename"]);
            Response.BinaryWrite(filedata);
            Response.End();
        }
    }
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        BindGrid();
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        BindGrid();
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int fileid = int.Parse(GridView1.DataKeys[e.RowIndex].Value.ToString());
        GridViewRow row = GridView1.Rows[e.RowIndex];



        Files.Rename(fileid,((TextBox)row.Cells[0].FindControl("TextBox1")).Text, Convert.ToInt32(Session["schema_id"]),
            Convert.ToInt32(Session["name_id"]), Request.UserHostAddress.ToString());
       
        GridView1.EditIndex = -1;
        BindGrid();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }
    protected void Button1_Click1(object sender, EventArgs e)
    {
        int fileid = int.Parse(((Button)sender).CommandArgument);
        Files.Delete(fileid, Convert.ToInt32(Session["schema_id"]),0);
        BindGrid();
    }





    protected void Button8_Click(object sender, EventArgs e)
    {

        int number_of_insert = 0;
        string home_id = "";
        string name_id_ip = "";

       try
        {
            
                string filename = Path.GetFileName(FileUpload2.PostedFile.FileName);
                int filesize = 0;
                if (FileUpload2.HasFile)
                {
                    Label11.Text = "test insert has file ";

                    Briefcase.Files f = new Files(); 
                    Stream stream = FileUpload2.PostedFile.InputStream;
                    filesize = FileUpload2.PostedFile.ContentLength;
                    byte[] filedata = new byte[filesize];
                    stream.Read(filedata, 0, filesize);


                    //---------------------------------------------------------



                    for (int j = 0; j < gv_home_list.Rows.Count; j++)
                    {

                        CheckBox chk_process = (CheckBox)gv_home_list.Rows[j].FindControl("chk_process");
                        // if the checkbox is checked the process
                        if (chk_process.Checked)
                        {
                            HiddenField h_id = (HiddenField)gv_home_list.Rows[j].FindControl("h_home_id");

                            home_id = home_id + h_id.Value + "|";
                            number_of_insert++;
                        }
                    }


                    //----------------------------------------------------------




                    name_id_ip = Request.UserHostAddress.ToString();
        


                   // f.OwnerCreate(filename, filedata, filesize, DateTime.Now, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]),int.Parse(ddl_home_list.SelectedValue));
                                                                                                                   
                                                                                                                 // this is the trace name_id
                 f.OwnerCreate(filename, filedata, filesize, DateTime.Now, Convert.ToInt32(Session["schema_id"]),Convert.ToInt32(Session["name_id"]),name_id_ip , home_id ,number_of_insert);

                    Label15.Text = home_id;
                    Label19.Text = number_of_insert.ToString();
                }

               
           
        }
        catch (Exception ex)
        {
            Label11.Text = ex.Message;// Resources.Resource.lbl_duplicate_file_name;
        }


     //   ddl_home_owner_list.SelectedValue = ddl_home_list.SelectedValue;

        OwnerBindGrid();
    }




    protected void Button9_Click(object sender, EventArgs e)
    {

        int number_of_insert = 0;
        string home_id = "";
        string name_id_ip = "";

       // try
        {

            string filename = Path.GetFileName(FileUpload3.PostedFile.FileName);
            int filesize = 0;
            if (FileUpload3.HasFile)
            {
               // Label11.Text = "test insert has file ";

                Briefcase.Files f = new Files();
                Stream stream = FileUpload3.PostedFile.InputStream;
                filesize = FileUpload3.PostedFile.ContentLength;
                byte[] filedata = new byte[filesize];
                stream.Read(filedata, 0, filesize);

                //---------------------------------------------------------



                for (int j = 0; j < gv_home_tenant_list.Rows.Count; j++)
                {

                    CheckBox chk_process = (CheckBox)gv_home_tenant_list.Rows[j].FindControl("chk_process2");
                    // if the checkbox is checked the process
                    if (chk_process.Checked)
                    {
                        HiddenField h_id = (HiddenField)gv_home_tenant_list.Rows[j].FindControl("h_home_id");

                        home_id = home_id + h_id.Value + "|";
                        number_of_insert++;
                    }
                }


                //----------------------------------------------------------

                name_id_ip = Request.UserHostAddress.ToString();
                                                                                                                  // trace name_id who created the file
                f.TenantCreate(filename, filedata, filesize, DateTime.Now, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]),name_id_ip ,home_id,number_of_insert);

                Label13.Text = home_id;
                Label14.Text = number_of_insert.ToString();

            }



        }
      //  catch (Exception ex)
        {
         //   Label11.Text = ex.Message;// Resources.Resource.lbl_duplicate_file_name;
        }


      //  ddl_home_tenant_list.SelectedValue = ddl_home_tenant_list2.SelectedValue;

        TenantBindGrid();
    }








    protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int fileid = int.Parse(GridView2.DataKeys[int.Parse(e.CommandArgument.ToString())].Value.ToString());

        if (e.CommandName == "Download")
        {
            DataTable file = Files.OwnerGetFile(fileid, Convert.ToInt32(Session["schema_id"]));
            byte[] filedata = (byte[])file.Rows[0]["filedata"];
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment;filename=" + file.Rows[0]["filename"]);
            Response.BinaryWrite(filedata);
            Response.End();
        }

        //TabContainer1.ActiveTabIndex = 1;
    }
    protected void GridView2_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView2.EditIndex = e.NewEditIndex;
        OwnerBindGrid();
        //TabContainer1.ActiveTabIndex = 1;
    }
    protected void GridView2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView2.EditIndex = -1;
        OwnerBindGrid();
        //TabContainer1.ActiveTabIndex = 1;
    }
    protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int fileid = int.Parse(GridView2.DataKeys[e.RowIndex].Value.ToString());
        GridViewRow row = GridView2.Rows[e.RowIndex];
        Files.OwnerRename(fileid, ((TextBox)row.Cells[0].FindControl("TextBox1")).Text, Convert.ToInt32(Session["schema_id"])
                       , Convert.ToInt32(Session["name_id"]) , Request.UserHostAddress.ToString());
        GridView2.EditIndex = -1;
        OwnerBindGrid();
        //TabContainer1.ActiveTabIndex = 1;
    }

    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }

    protected void btn_delete_Click(object sender, EventArgs e)
    {
        int fileid = int.Parse(((Button)sender).CommandArgument);
        Files.OwnerDelete(fileid, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_owner_list.SelectedValue));
        OwnerBindGrid();
        //TabContainer1.ActiveTabIndex = 1;
    }

    protected void ddl_home_owner_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable files = Files.GetFilesForOwner(int.Parse(ddl_home_owner_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
        GridView2.DataSource = files;
        GridView2.DataBind();

        //TabContainer1.ActiveTabIndex = 1 ;

    }




    protected void GridView3_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int fileid = int.Parse(GridView3.DataKeys[int.Parse(e.CommandArgument.ToString())].Value.ToString());

        if (e.CommandName == "Download")
        {
            DataTable file = Files.TenantGetFile(fileid, Convert.ToInt32(Session["schema_id"]));
            byte[] filedata = (byte[])file.Rows[0]["filedata"];
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment;filename=" + file.Rows[0]["filename"]);
            Response.BinaryWrite(filedata);
            Response.End();
        }
    }
    protected void GridView3_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView3.EditIndex = e.NewEditIndex;
        TenantBindGrid();
    }
    protected void GridView3_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView3.EditIndex = -1;
        TenantBindGrid();
    }
    protected void GridView3_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int fileid = int.Parse(GridView3.DataKeys[e.RowIndex].Value.ToString());
        GridViewRow row = GridView3.Rows[e.RowIndex];
        Files.TenantRename(fileid, ((TextBox)row.Cells[0].FindControl("TextBox1")).Text, Convert.ToInt32(Session["schema_id"])
                            , Convert.ToInt32(Session["name_id"]) , Request.UserHostAddress.ToString());
        GridView3.EditIndex = -1;
        TenantBindGrid();
    }

    protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }

    protected void btn_tenant_delete_Click(object sender, EventArgs e)
    {
        int fileid = int.Parse(((Button)sender).CommandArgument);
        Files.TenantDelete(fileid, Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_tenant_list.SelectedValue));
        TenantBindGrid();
        //TabContainer1.ActiveTabIndex = 2;
    }



    protected void ddl_home_tenant_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable files = Files.GetFilesForTenant(int.Parse(ddl_home_tenant_list.SelectedValue), Convert.ToInt32(Session["schema_id"]));
        GridView3.DataSource = files;
        GridView3.DataBind();

        //TabContainer1.ActiveTabIndex = 2;

    }

    protected void chk_all_CheckedChanged(object sender, EventArgs e)
    {

    /*
        for (int j = 0; j < gv_rented_unit_list.Rows.Count; j++)
        {

            CheckBox chk_process = (CheckBox)gv_rented_unit_list.Rows[j].FindControl("chk_process");
            // if the checkbox is checked the process
            if (chk_process.Checked)
            {


                CheckBox chk_no_delequency = (CheckBox)gv_rented_unit_list.Rows[j].FindControl("chk_no_delequency");
                TextBox tbx_rl_rent_amount = (TextBox)gv_rented_unit_list.Rows[j].FindControl("txtbx");
                Label rent_due_date = (Label)gv_rented_unit_list.Rows[j].FindControl("rent_due_date");


                TextBox tbx_notes = (TextBox)gv_rented_unit_list.Rows[j].FindControl("tbx_notes");

                DropDownList date_received_m = (DropDownList)gv_rented_unit_list.Rows[j].FindControl("ddl_unpaid_rent_m");
                DropDownList date_received_d = (DropDownList)gv_rented_unit_list.Rows[j].FindControl("ddl_unpaid_rent_d");
                DropDownList date_received_y = (DropDownList)gv_rented_unit_list.Rows[j].FindControl("ddl_unpaid_rent_y");

                HiddenField h_tu_id = (HiddenField)gv_rented_unit_list.Rows[j].FindControl("h_tu_id");
                HiddenField h_rl_id = (HiddenField)gv_rented_unit_list.Rows[j].FindControl("h_rl_id");
                HiddenField h_rl_rent_amount = (HiddenField)gv_rented_unit_list.Rows[j].FindControl("h_rl_rent_amount");

                if (chk_no_delequency.Checked)
                    no_delinquent = no_delinquent + "1|";
                else
                    no_delinquent = no_delinquent + "0|";



                rp_amount = rp_amount + tbx_rl_rent_amount.Text + "|";
                compare_rl_rent_amount = compare_rl_rent_amount + h_rl_rent_amount.Value + "|";
                rp_paid_date = rp_paid_date + date_received_m.SelectedValue + "/" + date_received_d.SelectedValue + "/" + date_received_y.SelectedValue + "|";
                rp_due_date = rp_due_date + rent_due_date.Text + "|";
                tu_id = tu_id + h_tu_id.Value + "|";
                rl_id = rl_id + h_rl_id.Value + "|";
                rp_notes = rp_notes + tbx_notes.Text + "|";

                number_of_insert++;
            }
        }
     

        */


    }
}
