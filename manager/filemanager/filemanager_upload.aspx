﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="filemanager_upload.aspx.cs" Inherits="manager_filemanager_filemanager_upload" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script language="javascript" type="text/javascript">


        function CheckAllDataViewCheckBoxes(checkVal) {

            for (i = 0; i < document.forms[0].elements.length; i++) {

                elm = document.forms[0].elements[i];
                if (elm.type == 'checkbox' && elm.name.substring(elm.name.length - 11, elm.name.length) == 'chk_process') {
                    { elm.checked = checkVal }
                }
            }
        }
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



    <p>
        &nbsp;</p>
    <p>
        <b>Step
        1</b>- select the file you want to upload</p>
    <p>
        <b>Step
        2</b>- select the group of users with whom you want to share the file with</p>
    <p>
        <b>Step
        3</b>- select the property of the users</p>
    <p>
        &nbsp;</p>
    <p>
        ex : if you want to share the file income_statement.doc with the Owners of the 
        <b>Property A</b></p>
    <p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; first , click on browse and select &nbsp; 
        income_statement.doc on your computer</p>
    <p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; second , select share with&nbsp;Owner from the radio list</p>
    <p>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; third , check <b>Property A</b> from the list of 
        property&nbsp;</p>
      <br />  <br />  
    <hr />
    <br />
        <b>Step
        1</b>- select the file you want to upload<br />

    <br />

<table cellpadding="5" ><tr>
                
                <td align="right">
                <asp:Label ID="Label16" runat="server" Font-Bold="True"  EnableTheming="False"
                           Text="<%$ Resources:Resource, lbl_upload_file %>"></asp:Label></td><td align="left" >
                           <asp:FileUpload ID="FileUpload2" runat="server" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" 
                            ControlToValidate="FileUpload2" Display="Dynamic" 
                            ErrorMessage="<%$ Resources:Resource, lbl_file_to_upload %>" 
                            ValidationGroup="uploadfile"></asp:RequiredFieldValidator></td>
                            </tr></table>
                            
    <br />
    <br />  <b>Step 2</b> - select the group of users with whom you want to share the file 
    with<br />  <br />
      
      
<asp:RequiredFieldValidator ID="req_radio_group" runat="server" 
                            ControlToValidate="radio_group" Display="Dynamic" 
                            ErrorMessage="please select a group" 
                            ValidationGroup="uploadfile"></asp:RequiredFieldValidator>
    <asp:RadioButtonList ID="radio_group" ValidationGroup="uploadfile" runat="server">
        <asp:ListItem Value="4">Owner</asp:ListItem>
        <asp:ListItem Value="3">Tenant</asp:ListItem>
        <asp:ListItem Value="6">Property Manager</asp:ListItem>
        <asp:ListItem Value="5">Janitor</asp:ListItem>
    </asp:RadioButtonList>


    <br />
    <b>Step 3</b>- select the property of the users<br />
    <br />
    &nbsp;Share with Owner of wich properties&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btn_upload" runat="server"  
                              Text="<%$ Resources:Resource, lbl_upload %>" 
                            ValidationGroup="uploadfile" onclick="btn_upload_Click"  /><br />

<table cellpadding="5" ><tr><td><asp:GridView 
                    ID="gv_home_list" runat="server" AllowPaging="True" 
                        AllowSorting="True"  PageSize="100"
                        AutoGenerateColumns="False" BorderColor="#CDCDCD" BorderWidth="1px"  
                        ><AlternatingRowStyle BackColor="#F0F0F6" />
                <Columns>
                        
                             <asp:TemplateField HeaderText="<input id='chkAllItems' type='checkbox' onclick='CheckAllDataViewCheckBoxes(document.forms[0].chkAllItems.checked)' />"  >
                            <ItemTemplate>
                            <asp:CheckBox  runat="server"   ID="chk_process"   /> 
                                <asp:HiddenField Visible="false" ID="h_home_id" Value='<%# Bind("home_id")%>' runat="server" />
                            </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:BoundField DataField="home_name" HeaderText="Property" />                          
                            
                        </Columns><HeaderStyle BackColor="#F0F0F6" />
                </asp:GridView></td></tr><tr>
                
                <td align="right" >
                    <br />
                            </td></tr></table>
                            
  </asp:Content>

