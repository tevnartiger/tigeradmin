﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="grid.aspx.cs" Inherits="manager_home_grid" %>

<%@ Register Assembly="obout_Calendar2_Net" Namespace="OboutInc.Calendar2" TagPrefix="obout" %>

<%@ Register Assembly="obout_Grid_NET" Namespace="Obout.Grid" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <script language="javascript" type="text/javascript">
      function ConfirmDelete() {
          if (confirm('Do you wish to delete this file/folder?'))
              return true;
          else
              return false;
      }



      function CheckAllDataViewCheckBoxes(checkVal) {

          for (i = 0; i < document.forms[0].elements.length; i++) {

              elm = document.forms[0].elements[i];
              if (elm.type == 'checkbox' && elm.name.substring(elm.name.length - 11, elm.name.length) == 'chk_process') {
                  { elm.checked = checkVal }
              }
          }
      }

</script>

  <asp:Label ID="lbl_rent_unit_list" runat="server" Text="<%$ Resources:Resource, lbl_rent_unit_list %>"/>
        <br /><div id="txt_message" runat="server"></div>
    <table>
        <tr>
            <td>
      
   <asp:Label ID="lbl_property" runat="server" Text="<%$ Resources:Resource, lbl_property %>"/> 
            </td>
            <td>
                :
                <asp:DropDownList AutoPostBack="true" DataTextField="home_name" DataValueField="home_id" ID="ddl_home_list" runat="server" OnSelectedIndexChanged="ddl_home_list_SelectedIndexChanged">
        </asp:DropDownList>
            &nbsp;</td>
        </tr>
        <tr>
            <td>
        <asp:Label ID="lbl_rent_paid_every" runat="server" Text="<%$ Resources:Resource, lbl_rent_paid_every %>"/> 
            </td>
            <td>
                :
        <asp:DropDownList ID="ddl_rent_frequency" AutoPostBack="true" DataTextField="frequency" DataValueField="re_id" runat="server" OnSelectedIndexChanged="ddl_rent_frequency_SelectedIndexChanged">
        </asp:DropDownList>
            </td>
        </tr>
    </table>
        <br />
         <table>
                <tr>
                <td>
                    <asp:Label ID="lbl_date_received" runat="server"  Text="<%$ Resources:Resource, lbl_date_received %>"/> ( mm / dd / yyyy&nbsp; )</td>
                <td> 
                    :<asp:DropDownList ID="ddl_date_received_m"  AutoPostBack="true" runat="server" 
                        onselectedindexchanged="ddl_date_received_m_SelectedIndexChanged">
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    / &nbsp;
                    <asp:DropDownList ID="ddl_date_received_d" runat="server" 
                        onselectedindexchanged="ddl_date_received_d_SelectedIndexChanged">
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>&nbsp; / &nbsp;
                 <asp:DropDownList ID="ddl_date_received_y" runat="server" 
                        onselectedindexchanged="ddl_date_received_y_SelectedIndexChanged" >
                        <asp:ListItem>2001</asp:ListItem>
                     <asp:ListItem>2002</asp:ListItem>
                     <asp:ListItem>2003</asp:ListItem>
                     <asp:ListItem>2004</asp:ListItem>
                     <asp:ListItem>2005</asp:ListItem>
                     <asp:ListItem>2006</asp:ListItem>
                     <asp:ListItem>2007</asp:ListItem>
                     <asp:ListItem>2008</asp:ListItem>
                     <asp:ListItem>2009</asp:ListItem>
                     <asp:ListItem>2010</asp:ListItem>
                 </asp:DropDownList>
                   </td>
            </tr>    
      </table><br />
      
     <b> <asp:Label ID="lbl_delequency" runat="server" Text="DELEQUENCY"></asp:Label></b>
      <table id="tb_delequency" width="100%" runat="server">
        <tr>
            <td bgcolor="White">
    <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="100%" BorderColor="White" BorderWidth="3" ID="gv_rent_delequency" runat="server" AutoGenerateColumns="false"
      AllowPaging="true" AllowSorting="true"  AutoGenerateSelectButton="false" 
        EmptyDataText="no unit rented" GridLines="Both"  
        AlternatingRowStyle-BackColor="Beige">
    <Columns>
      <asp:TemplateField HeaderText='<%$ Resources:Resource, gv_to_process %>' >
    <ItemTemplate>
    <asp:CheckBox  runat="server"  Checked="true" ID="chk_process_1"   /> 
     </ItemTemplate>
    </asp:TemplateField>
   <asp:BoundField DataField="unit_door_no"   />
   
  <asp:BoundField   DataField="rl_rent_amount" DataFormatString="{0:0.00}"   />
      <asp:BoundField   DataField="rp_amount" DataFormatString="{0:0.00}"  />
      
      
      <asp:TemplateField   >
    <ItemTemplate  >
    
    <asp:Label  runat="server"  ID="rd_amount_owed"    Text='<%#RentDelequencyAmount(Convert.ToInt32(Eval("rp_id")))%>'    /> 
    
    </ItemTemplate>
    </asp:TemplateField>
    
    <asp:TemplateField ItemStyle-BackColor="Aqua"  ItemStyle-Width="50px" >
    <ItemTemplate>
    
    <asp:TextBox  runat="server"  ID="tbx_delequency"  Width="70"    Text=""/> 
    
    </ItemTemplate>
    </asp:TemplateField>
         
   <asp:BoundField   DataField="rp_due_date" DataFormatString="{0:M-dd-yyyy}"  
     HtmlEncode="false" />
  
    
    <asp:TemplateField >
    <ItemTemplate>
    
    <asp:DropDownList ID="ddl_date_delequency_m" SelectedValue='<%#Set_Delequency_Month()%>'  AutoPostBack="true" runat="server">
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;
                    <asp:DropDownList ID="ddl_date_delequency_d" SelectedValue='<%#Set_Delequency_Day()%>'  runat="server">
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>&nbsp;
                 <asp:DropDownList ID="ddl_date_delequency_y" SelectedValue='<%#Set_Delequency_Year()%>'  runat="server">
                     <asp:ListItem>2001</asp:ListItem>
                     <asp:ListItem>2002</asp:ListItem>
                     <asp:ListItem>2003</asp:ListItem>
                     <asp:ListItem>2004</asp:ListItem>
                     <asp:ListItem>2005</asp:ListItem>
                     <asp:ListItem>2006</asp:ListItem>
                     <asp:ListItem>2007</asp:ListItem>
                     <asp:ListItem>2008</asp:ListItem>
                     <asp:ListItem>2009</asp:ListItem>
                     <asp:ListItem>2010</asp:ListItem>
                 </asp:DropDownList> 
                 
   <asp:HiddenField runat="server"  ID="h_rp_id"   
        Value='<%# Bind("rp_id")%>'/>
   
    </ItemTemplate>
    </asp:TemplateField>
    
    
   
      
      
    
   </Columns>
   </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align="right" bgcolor="#FFFFCC">
                <asp:Button ID="btn_delequency" runat="server" Text="submit" 
                    onclick="btn_delequency_Click" />
            </td>
        </tr>
    </table>
    <br />
    <asp:Label ID="lbl_u_paid_rent" runat="server" 
        Text="<%$ Resources:Resource, lbl_u_paid_rent %>" style="font-weight: 700" />
 
    <br />
    
    <table  id="tb_paid_rent" width="100%" runat="server">
        <tr>
            <td>
     <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="100%" BorderColor="White" BorderWidth="3"  ID="gv_rented_paid_unit_list" runat="server" AutoGenerateColumns="false"
      AllowPaging="true" AllowSorting="true"  AutoGenerateSelectButton="false" 
        EmptyDataText="no unit rented" GridLines="Both"  
        AlternatingRowStyle-BackColor="Beige">
    <Columns>
   <asp:BoundField DataField="unit_door_no"   />
   <asp:BoundField   DataField="rl_rent_amount" DataFormatString="{0:0.00}"  />
   <asp:BoundField   DataField="rp_amount" DataFormatString="{0:0.00}"  />
   <asp:BoundField   DataField="rp_due_date" DataFormatString="{0:M-dd-yyyy}"  
     HtmlEncode="false" />
   <asp:BoundField   DataField="rp_paid_date" DataFormatString="{0:M-dd-yyyy}"  
     HtmlEncode="false" />
      <asp:BoundField DataField="tu_id" HeaderText="tu_id"   />
       <asp:BoundField DataField="rl_id" HeaderText="rl_id"   />
   </Columns>
   </asp:GridView>
                <br />
      
   
   </td>
   </tr>
   </table>
   
   
   
        <br />
   
   <asp:Label ID="Label1" runat="server" 
        Text="<%$ Resources:Resource, lbl_u_unpaid_rent %>" 
        style="font-weight: 700"></asp:Label>    
    <br />
 <asp:Label ID="lbl_late_rent" runat="server" Text="<%$ Resources:Resource, lbl_late_rent %>" ></asp:Label> <br />
  <asp:Label ID="lbl_no_delequency" runat="server" Text="<%$ Resources:Resource, lbl_no_delequency %>" ></asp:Label> <br />
    <br />
    <table  id="tb_rented_unit_list" runat="server">
        <tr>
            <td>
       <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="100%" BorderColor="White" BorderWidth="3"  ID="gv_rented_unit_list" runat="server" AutoGenerateColumns="false"
      AllowPaging="true" AllowSorting="true" GridLines="Both"  EmptyDataText="<%$ Resources:Resource, gv_no_payment %>"  AlternatingRowStyle-BackColor="Beige">
    <Columns>
    
    <asp:TemplateField HeaderText='<%$ Resources:Resource, gv_to_process %>' >
    <ItemTemplate>
    <asp:CheckBox  runat="server"  Checked="true" ID="chk_process"   /> 
     </ItemTemplate>
    </asp:TemplateField>
   <asp:BoundField DataField="unit_door_no"   />
   <asp:BoundField   DataField="rl_rent_amount" DataFormatString="{0:0.00}"   />
    <asp:TemplateField >
    <ItemTemplate>
    
    <asp:TextBox  runat="server" Width="80"  ID="txtbx" DataFormatString="{0:0.00}"    Text='<%# Bind("rl_rent_amount","{0:0.00}")%>'/> 
    
    </ItemTemplate>
    </asp:TemplateField>
    
    <asp:TemplateField HeaderText="*" >
    <ItemTemplate>
    <asp:CheckBox  runat="server"   ID="chk_no_delequency"   /> 
     </ItemTemplate>
    </asp:TemplateField>
    
    <asp:TemplateField >
    <ItemTemplate>
    
    <asp:TextBox  runat="server"  ID="tbx_notes"   /> 
    
    </ItemTemplate>
    </asp:TemplateField>
    
     <asp:TemplateField HeaderText='<%$ Resources:Resource, lbl_due_date %>' >
    <ItemTemplate>
    
    <asp:Label runat="server"  ID="rent_due_date"  DataFormatString="{0:M-dd-yyyy}"  
     HtmlEncode="false"  Text='<%#Get_UnpaidRent_Due_Date(Convert.ToInt32(Eval("rl_id")))%>'/> 
    </ItemTemplate>
    </asp:TemplateField>
    
    
    <asp:TemplateField  HeaderText='<%$ Resources:Resource, lbl_date_received %>'  >
    <ItemTemplate>
    
    <ASP:TextBox runat="server" ID="tbx_unpaid_rent" ReadOnly="true" Text='<%#Get_UnpaidRent_Due_Date(Convert.ToInt32(Eval("rl_id")))%>'></ASP:TextBox>

     <obout:Calendar ID="Calendar1" runat="server"
                          DatePickerMode="true"
                          DatePickerSynchronize="true"
                          CultureName='<%#Get_CultureName()%>'
                          DateFormat="MMM/dd/yyyy"
                          SelectedDate='<%#Get_UnpaidRent_Due_Date2(Convert.ToInt32(Eval("rl_id")))%>'
                          DatePickerImagePath="../../App_Themes/Obout/Calendar/styles/icon2.gif"
                         TextBoxId="tbx_unpaid_rent">
     </obout:Calendar> 
     
     <%-- 
    &nbsp;<asp:DropDownList  SelectedValue='<%#Get_UnpaidRent_Month(Convert.ToInt32(Eval("rl_id")))%>'  ID="ddl_unpaid_rent_m" runat="server">
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    / &nbsp;
                    <asp:DropDownList SelectedValue='<%#Get_UnpaidRent_Day(Convert.ToInt32(Eval("rl_id")))%>' ID="ddl_unpaid_rent_d" runat="server">
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>&nbsp; / &nbsp;
                 <asp:DropDownList SelectedValue='<%#Get_UnpaidRent_Year(Convert.ToInt32(Eval("rl_id")))%>' ID="ddl_unpaid_rent_y" runat="server" >
                     <asp:ListItem>2007</asp:ListItem>
                     <asp:ListItem>2008</asp:ListItem>
                     <asp:ListItem>2009</asp:ListItem>
                     <asp:ListItem>2010</asp:ListItem>
                 </asp:DropDownList>
                 
                 --%>
    <asp:HiddenField runat="server"  ID="h_due_date"   
        Value='<%#Get_UnpaidRent_Due_Date(Convert.ToInt32(Eval("rl_id")))%>'/>
        
    <asp:HiddenField runat="server"  ID="h_rl_rent_amount"   
        Value='<%# Bind("rl_rent_amount","{0:0.00}")%>'/> 
        
    <asp:HiddenField runat="server"  ID="h_rl_id"   
        Value='<%# Bind("rl_id")%>'/>
        
    <asp:HiddenField runat="server"  ID="h_tu_id"   
        Value='<%# Bind("tu_id")%>'/>
     </ItemTemplate>
    </asp:TemplateField>
  
  <%--  <asp:HyperLinkField  Text="<%$ Resources:Resource, gv_view %>" 
     DataNavigateUrlFields="home_id,tenant_id,tu_id,rl_rent_amount,re_id,unit_id" 
      DataNavigateUrlFormatString="~/manager/tenant/tenant_rent.aspx?h_id={0}&t_id={1}&tu_id={2}&ra_id={3}&re_id={4}&unit_id={5}" 
      HeaderText="View details" />   
      
      
       --%>
      <asp:BoundField  DataField="tu_id" HeaderText="tu_id"/> 
      <asp:BoundField  DataField="rl_id" HeaderText="rl_id"/> 
    </Columns>
    </asp:GridView>
    
    
    <cc1:Grid id="grid_rented_unit_list" runat="server" CallbackMode="true" Serialize="true" 
             AutoGenerateColumns="false"
			 FolderStyle="../App_Themes/Obout/grid/styles/style_12" PageSize="10" 
			  
			 AllowMultiRecordSelection="false"
			 GenerateRecordIds="true" 
			 AllowAddingRecords="false" AllowPaging="true" AllowFiltering="false" >
			 <Columns>
			    <cc1:Column DataField="tu_id" Width="5%" ReadOnly="true" HeaderText="SELECT"  runat="server">
				    <TemplateSettings TemplateID="TemplateWithCheckbox" />
				</cc1:Column>
			    <cc1:Column ID="unit_door_no" Width="8%" DataField="unit_door_no" HeaderText="<%$ Resources:Resource, gv_unit %>"  runat="server"/>
				 <cc1:Column ID="Column1" Width="8%" DataField="rl_rent_amount" HeaderText="<%$ Resources:Resource, gv_rent %>"  runat="server"/>
				
		 <cc1:Column ID="Column2" Width="8%" DataField="rl_rent_amount" HeaderText="<%$ Resources:Resource, gv_rent %>"  runat="server"/>
				
				
				</Columns>					
		</cc1:Grid>
  
            </td><td></td>
        </tr>
        <tr>
            <td align="right" bgcolor="#FFFFCC">
        <asp:Button ID="btn_submit" runat="server" Text="Submit" 
        onclick="btn_submit_Click" />
            </td><td></td>
        </tr>
    </table>
    <br />
   
 
    <asp:HiddenField ID="h_btn_submit" Value="0" runat="server" />


</asp:Content>

