using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
/// <summary>
/// Created by : Stanley Jocelyn
/// date       : oct 9 , 2007
/// </summary>
public partial class home_home_unit_add : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

          tiger.Home l = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
          int home_count = l.getHomeCount(Convert.ToInt32(Session["schema_id"]));
          
         // first we check if there's any property available
          if (home_count > 0)
          {
              int home_id = l.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));

              tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
              ddl_home_list.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
              ddl_home_list.DataBind();

             // here we get the list of unit that were already added 

              tiger.Home u = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
              r_unit_list.DataSource = u.getHomeUnitList(home_id, Convert.ToInt32(Session["schema_id"]));
              r_unit_list.DataBind();




          }

          else
          {
              panel_home_unit_add.Visible = false;
              txt_message.InnerHtml = "<b><a href='~/home/home_add.aspx'>Please add a property</a></b>";
          }


          

        }
        
    }


  
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        panel_home_unit_add.Visible = true ;
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prUnitAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

       

            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
        
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_list.SelectedValue);
            cmd.Parameters.Add("@unit_door_no", SqlDbType.Int).Value = Convert.ToInt32(unit_door_no.Text);
            cmd.Parameters.Add("@unit_size_system", SqlDbType.Bit).Value = Convert.ToByte(ddl_unit_size_system.SelectedValue);

            // if the unit size is in sq-ft
            if (ddl_unit_size_system.SelectedValue == "0")
            {
                cmd.Parameters.Add("@unit_size_sqft", SqlDbType.Decimal).Value = Convert.ToDecimal(unit_size.Text);
                cmd.Parameters.Add("@unit_size_sqm", SqlDbType.Decimal).Value = Convert.ToDecimal(unit_size.Text) * Convert.ToDecimal(0.0929034) ;

            }

            // if the unit size is in sq-m
            else
            {
                cmd.Parameters.Add("@unit_size_sqm", SqlDbType.Decimal).Value = Convert.ToDecimal(unit_size.Text);
                cmd.Parameters.Add("@unit_size_sqft", SqlDbType.Decimal).Value = Convert.ToDecimal(unit_size.Text) * Convert.ToDecimal(10.7639104) ;

            
            }

            cmd.Parameters.Add("@unit_bedroom_no", SqlDbType.Int).Value = Convert.ToInt32(unit_bedroom_no.Text);
            cmd.Parameters.Add("@unit_bathroom_no", SqlDbType.Int).Value = Convert.ToInt32(unit_bathroom_no.Text);
            cmd.Parameters.Add("@unit_level", SqlDbType.Int).Value = Convert.ToInt32(unit_level.Text);

            cmd.Parameters.Add("@unit_expected_rent", SqlDbType.Money).Value = Convert.ToDecimal(unit_expected_rent.Text);
            cmd.Parameters.Add("@unit_max_tenant", SqlDbType.Money).Value = Convert.ToInt32(unit_max_tenant.Text);
            

            cmd.ExecuteNonQuery();


            if (Convert.ToInt32(cmd.Parameters["@return_id"].Value) == 0)
            {
                lbl_success.Text = Resources.Resource.lbl_successfull_add;
            }

       


    }
     
    protected void ddl_home_list_SelectedIndexChanged(object sender, EventArgs e)
    {

        lbl_success.Text = "";


        r_unit_list.Visible = true ;
        int home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        // here we check if there's any unit in this property
        tiger.Unit uc = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        int unit_count = uc.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);


        if (unit_count > 0)
        // here we get the list of unit that were already added 
        {
            tiger.Home u = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            r_unit_list.DataSource = u.getHomeUnitList(home_id ,Convert.ToInt32(Session["schema_id"]));
            r_unit_list.DataBind();

        }
        else
        {
            r_unit_list.Visible = false;
        }
    }
}
