<%@ Page Language="C#" MasterPageFile="../mp_manager.master" AutoEventWireup="true" CodeFile="home_unit_rented_list.aspx.cs" Inherits="home_home_unit_rented_list" Title="Prospective tenant view" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function OnChanged(sender, args) {
            sender.get_clientStateField().value = sender.saveClientState();
        }
    </script>

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>


    <br />
    <div id="txt_message" runat="server"></div>

    <table>
        <tr>
            <td>

                <asp:Label ID="lbl_property" runat="server" Text="<%$ Resources:Resource, lbl_property %>"
                    Style="font-size: small" />
            </td>
            <td>:
                <asp:DropDownList AutoPostBack="true" DataTextField="home_name" DataValueField="home_id" ID="ddl_home_list" runat="server" OnSelectedIndexChanged="ddl_home_list_SelectedIndexChanged">
                </asp:DropDownList>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_rent_paid_every" runat="server"
                    Text="<%$ Resources:Resource, lbl_rent_paid_every %>" Style="font-size: small" />
                &nbsp;&nbsp;&nbsp;
            </td>
            <td>:
        <asp:DropDownList ID="ddl_rent_frequency" AutoPostBack="true" DataTextField="frequency" DataValueField="re_id" runat="server" OnSelectedIndexChanged="ddl_rent_frequency_SelectedIndexChanged">
        </asp:DropDownList>
            </td>
        </tr>
    </table>
    <br />
    <table>
        <tr>
            <td>
                <asp:Label ID="lbl_date_received" runat="server" Text="<%$ Resources:Resource, lbl_date %>" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
            <td>:
                <asp:DropDownList ID="ddl_date_received_m" AutoPostBack="true" runat="server"
                    OnSelectedIndexChanged="ddl_date_received_m_SelectedIndexChanged">
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                </asp:DropDownList>
                &nbsp; / &nbsp;
                 <asp:DropDownList ID="ddl_date_received_y" runat="server"
                     OnSelectedIndexChanged="ddl_date_received_y_SelectedIndexChanged">
                     <asp:ListItem>2001</asp:ListItem>
                     <asp:ListItem>2002</asp:ListItem>
                     <asp:ListItem>2003</asp:ListItem>
                     <asp:ListItem>2004</asp:ListItem>
                     <asp:ListItem>2005</asp:ListItem>
                     <asp:ListItem>2006</asp:ListItem>
                     <asp:ListItem>2007</asp:ListItem>
                     <asp:ListItem>2008</asp:ListItem>
                     <asp:ListItem>2009</asp:ListItem>
                     <asp:ListItem>2010</asp:ListItem>
                     <asp:ListItem>2011</asp:ListItem>
                     <asp:ListItem>2012</asp:ListItem>
                     <asp:ListItem>2013</asp:ListItem>
                     <asp:ListItem>2014</asp:ListItem>
                     <asp:ListItem>2015</asp:ListItem>
                     <asp:ListItem>2016</asp:ListItem>
                     <asp:ListItem>2017</asp:ListItem>
                     <asp:ListItem>2018</asp:ListItem>
                     <asp:ListItem>2019</asp:ListItem>
                     <asp:ListItem>2020</asp:ListItem>
                     <asp:ListItem>2021</asp:ListItem>
                     <asp:ListItem>2022</asp:ListItem>
                     <asp:ListItem>2023</asp:ListItem>
                     <asp:ListItem>2024</asp:ListItem>
                     <asp:ListItem>2025</asp:ListItem>
                     <asp:ListItem>2026</asp:ListItem>
                     <asp:ListItem>2027</asp:ListItem>
                     <asp:ListItem>2028</asp:ListItem>
                     <asp:ListItem>2029</asp:ListItem>
                 </asp:DropDownList>
                &nbsp;</td>
        </tr>
    </table>
    <br />


    <h2>RENT PAYMENTS</h2>
            
                <asp:Panel ID="Panel1" runat="server">
                    <asp:Label ID="Label1" runat="server" Style="font-weight: 700"
                        Text="<%$ Resources:Resource, lbl_u_unpaid_rent %>"></asp:Label>
                    <br />
                    <asp:Label ID="lbl_late_rent" runat="server"
                        Text="<%$ Resources:Resource, lbl_late_rent %>"></asp:Label>
                    <br />
                    <asp:Label ID="lbl_no_delequency" runat="server"
                        Text="<%$ Resources:Resource, lbl_no_delequency %>"></asp:Label>
                    <br />
                    <br />
                    <table id="tb_rented_unit_list" runat="server" width="100%">
                        <tr runat="server">
                            <td runat="server">
                                <asp:GridView ID="gv_rented_unit_list" runat="server" AllowPaging="True"
                                    AllowSorting="True" AutoGenerateColumns="False" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_gl_select %>">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk_process" runat="server" Checked="true" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="unit_door_no" />
                                        <asp:BoundField DataField="rl_rent_amount" DataFormatString="{0:0.00}" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtbx" runat="server" DataFormatString="{0:0.00}"
                                                    Text='<%# Bind("rl_rent_amount","{0:0.00}")%>' Width="80" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="*">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk_no_delequency" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:TextBox ID="tbx_notes" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_due_date %>">
                                            <ItemTemplate>
                                                <asp:Label ID="rent_due_date" runat="server" DataFormatString="{0:MMM-dd-yyyy}"
                                                    HtmlEncode="false"
                                                    Text='<%#Get_UnpaidRent_Due_Date(Convert.ToInt32(Eval("rl_id")))%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_date_received %>">
                                            <ItemTemplate>
                                                &nbsp;<asp:DropDownList ID="ddl_unpaid_rent_m" runat="server"
                                                    SelectedValue='<%#Get_UnpaidRent_Month(Convert.ToInt32(Eval("rl_id")))%>'>
                                                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                                                </asp:DropDownList>
                                                / &nbsp;
                                        <asp:DropDownList ID="ddl_unpaid_rent_d" runat="server"
                                            SelectedValue='<%#Get_UnpaidRent_Day(Convert.ToInt32(Eval("rl_id")))%>'>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                            <asp:ListItem>6</asp:ListItem>
                                            <asp:ListItem>7</asp:ListItem>
                                            <asp:ListItem>8</asp:ListItem>
                                            <asp:ListItem>9</asp:ListItem>
                                            <asp:ListItem>10</asp:ListItem>
                                            <asp:ListItem>11</asp:ListItem>
                                            <asp:ListItem>12</asp:ListItem>
                                            <asp:ListItem>13</asp:ListItem>
                                            <asp:ListItem>14</asp:ListItem>
                                            <asp:ListItem>15</asp:ListItem>
                                            <asp:ListItem>16</asp:ListItem>
                                            <asp:ListItem>17</asp:ListItem>
                                            <asp:ListItem>18</asp:ListItem>
                                            <asp:ListItem>19</asp:ListItem>
                                            <asp:ListItem>20</asp:ListItem>
                                            <asp:ListItem>21</asp:ListItem>
                                            <asp:ListItem>22</asp:ListItem>
                                            <asp:ListItem>23</asp:ListItem>
                                            <asp:ListItem>24</asp:ListItem>
                                            <asp:ListItem>25</asp:ListItem>
                                            <asp:ListItem>26</asp:ListItem>
                                            <asp:ListItem>27</asp:ListItem>
                                            <asp:ListItem>28</asp:ListItem>
                                            <asp:ListItem>29</asp:ListItem>
                                            <asp:ListItem>30</asp:ListItem>
                                            <asp:ListItem>31</asp:ListItem>
                                        </asp:DropDownList>
                                                &nbsp; / &nbsp;
                                        <asp:DropDownList ID="ddl_unpaid_rent_y" runat="server"
                                            SelectedValue='<%#Get_UnpaidRent_Year(Convert.ToInt32(Eval("rl_id")))%>'>
                                            <asp:ListItem>2001</asp:ListItem>
                                            <asp:ListItem>2002</asp:ListItem>
                                            <asp:ListItem>2003</asp:ListItem>
                                            <asp:ListItem>2004</asp:ListItem>
                                            <asp:ListItem>2005</asp:ListItem>
                                            <asp:ListItem>2006</asp:ListItem>
                                            <asp:ListItem>2007</asp:ListItem>
                                            <asp:ListItem>2008</asp:ListItem>
                                            <asp:ListItem>2009</asp:ListItem>
                                            <asp:ListItem>2010</asp:ListItem>
                                            <asp:ListItem>2011</asp:ListItem>
                                            <asp:ListItem>2012</asp:ListItem>
                                            <asp:ListItem>2013</asp:ListItem>
                                            <asp:ListItem>2014</asp:ListItem>
                                            <asp:ListItem>2015</asp:ListItem>
                                            <asp:ListItem>2016</asp:ListItem>
                                            <asp:ListItem>2017</asp:ListItem>
                                            <asp:ListItem>2018</asp:ListItem>
                                            <asp:ListItem>2019</asp:ListItem>
                                            <asp:ListItem>2020</asp:ListItem>
                                            <asp:ListItem>2021</asp:ListItem>
                                            <asp:ListItem>2022</asp:ListItem>
                                            <asp:ListItem>2023</asp:ListItem>
                                            <asp:ListItem>2024</asp:ListItem>
                                            <asp:ListItem>2025</asp:ListItem>
                                            <asp:ListItem>2026</asp:ListItem>
                                            <asp:ListItem>2027</asp:ListItem>
                                            <asp:ListItem>2028</asp:ListItem>
                                            <asp:ListItem>2029</asp:ListItem>

                                        </asp:DropDownList>
                                                <asp:HiddenField Visible="false" ID="h_due_date" runat="server"
                                                    Value='<%#Get_UnpaidRent_Due_Date(Convert.ToInt32(Eval("rl_id")))%>' />
                                                <asp:HiddenField Visible="false" ID="h_rl_rent_amount" runat="server"
                                                    Value='<%# Bind("rl_rent_amount","{0:0.00}")%>' />
                                                <asp:HiddenField Visible="false" ID="h_rl_id" runat="server" Value='<%# Bind("rl_id")%>' />
                                                <asp:HiddenField Visible="false" ID="h_tu_id" runat="server" Value='<%# Bind("tu_id")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="AliceBlue" />
                                </asp:GridView>
                            </td>
                            <td runat="server"></td>
                        </tr>
                        <tr runat="server">
                            <td runat="server" align="right" bgcolor="#FFFFCC">
                                <asp:Button ID="btn_submit" runat="server" OnClick="btn_submit_Click"
                                    Text="Submit" />
                            </td>
                            <td runat="server"></td>
                        </tr>
                    </table>
                </asp:Panel>
                <br />
                <asp:Label ID="lbl_u_paid_rent" runat="server"
                    Text="<%$ Resources:Resource, lbl_u_paid_rent %>" Style="font-weight: 700" />

                <br />

                <table id="tb_paid_rent" width="100%" runat="server">
                    <tr runat="server">
                        <td runat="server">
                            <asp:GridView Width="83%" ID="gv_rented_paid_unit_list" runat="server" AutoGenerateColumns="False"
                                AllowPaging="True" AllowSorting="True">
                                <Columns>
                                    <asp:BoundField DataField="unit_door_no" />
                                    <asp:BoundField DataField="rl_rent_amount" DataFormatString="{0:0.00}" />
                                    <asp:BoundField DataField="rp_amount" DataFormatString="{0:0.00}" />
                                    <asp:BoundField DataField="rp_due_date" DataFormatString="{0:MMM-dd-yyyy}"
                                        HtmlEncode="False" />
                                    <asp:BoundField DataField="rp_paid_date" DataFormatString="{0:MMM-dd-yyyy}"
                                        HtmlEncode="False" />
                                    <asp:BoundField DataField="tu_id" HeaderText="tu_id" />
                                    <asp:BoundField DataField="rl_id" HeaderText="rl_id" />
                                </Columns>
                                <HeaderStyle BackColor="AliceBlue" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>









                <b>
                    <asp:Label ID="lbl_delequency" runat="server" Text="DELEQUENCY"></asp:Label></b>
                <table id="tb_delequency" width="100%" runat="server">
                    <tr>
                        <td bgcolor="White">
                            <asp:GridView Width="83%" BorderColor="White" BorderWidth="3" ID="gv_rent_delequency" runat="server" AutoGenerateColumns="false"
                                AllowPaging="true" AllowSorting="true" PageSize="500" AutoGenerateSelectButton="false" OnPageIndexChanging="gv_rent_delequency_PageIndexChanging">
                                <Columns>
                                    <asp:TemplateField HeaderText='<%$ Resources:Resource, gv_to_process %>'>
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" Checked="true" ID="chk_process_1" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="unit_door_no" />

                                    <asp:BoundField DataField="rl_rent_amount" DataFormatString="{0:0.00}" />
                                    <asp:BoundField DataField="rp_amount" DataFormatString="{0:0.00}" />


                                    <asp:TemplateField>
                                        <ItemTemplate>

                                            <asp:Label runat="server" ID="rd_amount_owed" Text='<%#RentDelequencyAmount(Convert.ToInt32(Eval("rp_id")))%>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="50px">
                                        <ItemTemplate>

                                            <asp:TextBox runat="server" ID="tbx_delequency" Width="70" Text="" />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="rp_due_date" DataFormatString="{0:MMM-dd-yyyy}"
                                        HtmlEncode="false" />


                                    <asp:TemplateField>
                                        <ItemTemplate>

                                            <asp:DropDownList ID="ddl_date_delequency_m" SelectedValue='<%#Set_Delequency_Month()%>' AutoPostBack="true" runat="server">
                                                <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                                                <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                                            </asp:DropDownList>
                                            &nbsp;
                    <asp:DropDownList ID="ddl_date_delequency_d" SelectedValue='<%#Set_Delequency_Day()%>' runat="server">
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>&nbsp;
                 <asp:DropDownList ID="ddl_date_delequency_y" SelectedValue='<%#Set_Delequency_Year()%>' runat="server">
                     <asp:ListItem>2001</asp:ListItem>
                     <asp:ListItem>2002</asp:ListItem>
                     <asp:ListItem>2003</asp:ListItem>
                     <asp:ListItem>2004</asp:ListItem>
                     <asp:ListItem>2005</asp:ListItem>
                     <asp:ListItem>2006</asp:ListItem>
                     <asp:ListItem>2007</asp:ListItem>
                     <asp:ListItem>2008</asp:ListItem>
                     <asp:ListItem>2009</asp:ListItem>
                     <asp:ListItem>2010</asp:ListItem>
                     <asp:ListItem>2011</asp:ListItem>
                     <asp:ListItem>2012</asp:ListItem>
                     <asp:ListItem>2013</asp:ListItem>
                     <asp:ListItem>2014</asp:ListItem>
                     <asp:ListItem>2015</asp:ListItem>
                     <asp:ListItem>2016</asp:ListItem>
                     <asp:ListItem>2017</asp:ListItem>
                     <asp:ListItem>2018</asp:ListItem>
                     <asp:ListItem>2019</asp:ListItem>
                     <asp:ListItem>2020</asp:ListItem>
                     <asp:ListItem>2021</asp:ListItem>
                     <asp:ListItem>2022</asp:ListItem>
                     <asp:ListItem>2023</asp:ListItem>
                     <asp:ListItem>2024</asp:ListItem>
                     <asp:ListItem>2025</asp:ListItem>
                     <asp:ListItem>2026</asp:ListItem>
                     <asp:ListItem>2027</asp:ListItem>
                     <asp:ListItem>2028</asp:ListItem>
                     <asp:ListItem>2029</asp:ListItem>

                 </asp:DropDownList>

                                            <asp:HiddenField Visible="false" runat="server" ID="h_rp_id"
                                                Value='<%# Bind("rp_id")%>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>






                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" bgcolor="#FFFFCC">
                            <asp:Button ID="btn_delequency" runat="server" Text="submit"
                                OnClick="btn_delequency_Click" />
                        </td>
                    </tr>
                </table>

                <br />



    <asp:HiddenField Visible="false" ID="h_btn_submit" Value="0" runat="server" />


</asp:Content>
