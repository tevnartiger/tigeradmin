<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true"CodeFile="home_unit_add.aspx.cs" Inherits="home_home_unit_add" Title="ADD UNIT" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
        <strong>
        ADD UNIT </strong><br />
       
        <div id="txt_message" runat="server"></div><br />
        Current unit in this property
        <asp:Repeater runat="server" ID="r_unit_list">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
            <tr>
                <td>
                    door number/ app#</td>
                <td >:&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "unit_door_no")%></td>
            </tr>
        </table>
        </ItemTemplate>
        </asp:Repeater>
        <asp:Label ID="lbl_success" runat="server" Text="Label"></asp:Label>
        <br />
        
        <asp:Panel ID="panel_home_unit_add" runat="server">
        <table>
            <tr>
                <td>
        Home&nbsp;
                </td>
                <td>
                    &nbsp;
        <asp:DropDownList AutoPostBack=true DataTextField="home_name" DataValueField="home_id" ID="ddl_home_list" runat="server" OnSelectedIndexChanged="ddl_home_list_SelectedIndexChanged">
        </asp:DropDownList></td>
                <td >
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <br />
                </td>
                <td >
                </td>
            </tr>
            <tr>
                <td >
                    expected rent</td>
                <td >:
                    <asp:TextBox ID="unit_expected_rent" runat="server"></asp:TextBox></td>
                <td  >
                </td>
            </tr>
            <tr>
                <td >
                    unit level (floor)</td>
                <td >:
                    <asp:TextBox ID="unit_level" runat="server"></asp:TextBox></td>
                <td  >
                </td>
            </tr>
            <tr>
                <td >
                    door/app # number</td>
                <td >:
                    <asp:TextBox ID="unit_door_no" runat="server" ></asp:TextBox></td>
                <td  >
                </td>
            </tr>
            <tr>
                <td >
                    size (sq. ft - sq. m. )</td>
                <td >:
                    <asp:TextBox ID="unit_size" runat="server"></asp:TextBox></td>
                <td  >
                    <asp:DropDownList ID="ddl_unit_size_system" runat="server">
                        <asp:ListItem Selected=True Value="0">sq. -  ft</asp:ListItem>
                        <asp:ListItem Value="1">sq. - m</asp:ListItem>
                       
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td >
                    number of bedrooms</td>
                <td >:
                    <asp:TextBox ID="unit_bedroom_no" runat="server"></asp:TextBox></td>
                <td  >
                </td>
            </tr>
            <tr>
                <td>
                    number of bathrooms</td>
                <td >:
                    <asp:TextBox ID="unit_bathroom_no" runat="server"></asp:TextBox></td>
                <td >
                </td>
            </tr>
            <tr>
                <td >
                    maximum tenants</td>
                <td >:
                    <asp:TextBox ID="unit_max_tenant" runat="server"></asp:TextBox></td>
                <td  >
                </td>
            </tr>
            <tr>
                <td >
                </td>
                <td >
                </td>
                <td  >
                </td>
            </tr>
        </table>  
        <br />
        <asp:Button id="btn_submit" runat="server" Text="submit" OnClick="btn_submit_Click"  /><br />
        </asp:Panel>
        <br />
        <asp:HyperLink ID="link_main" runat="server" NavigateUrl="~/home/home_main.aspx">go back to main</asp:HyperLink><br />
        <br />
    
    </div>
   </asp:Content>