﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manager_role_role_wiz : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            tiger.Country ic = new tiger.Country(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_country_id_1.DataSource = ic.getCountryList();
            ddl_country_id_1.DataBind();

            tiger.Name n = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_name_id_1.DataSource = n.getNameList(Convert.ToInt32(Session["schema_id"]), 0);
            ddl_name_id_1.DataBind();
            ddl_name_id_1.Items.Insert(0, Resources.Resource.lbl_select_name);
            ddl_name_id_1.SelectedIndex = 0;


            tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_home_list.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
            gv_home_list.DataBind();



        }





    }



    protected void chb_1_CheckedChanged(object sender, EventArgs e)
    {
        if (chb_1.Checked)
        {
            panel_1.Visible = true;
            //  hd_panel1.Value = "true";
            chb_name_add_1.Checked = false;
        }
        else
        {
            panel_1.Visible = false;
            //hd_panel1.Value = "false";
        }
    }
    protected void chb_name_add_1_CheckedChanged(object sender, EventArgs e)
    {
        if (chb_name_add_1.Checked)
        {
            panel_name_add_1.Visible = true;
            chb_1.Checked = false;

        }
        else
        {
            panel_name_add_1.Visible = false;
        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_continue1_Click(object sender, EventArgs e)
    {
        if (chb_1.Checked)
        {
            tiger.Name n = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            gv_person_role.DataSource = n.getNameRoleAssignementList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_name_id_1.SelectedValue));
            gv_person_role.DataBind();

            lbl_person.Text = ddl_name_id_1.SelectedItem.Text;

        }



        if (chb_name_add_1.Checked)
        {
            lbl_person.Text = RegEx.getText(name_fname_1.Text + " , " + name_lname_1.Text);
        }



        MultiView1.ActiveViewIndex = 2;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_step1(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_continue2_Click(object sender, EventArgs e)
    {

        if (chb_1.Checked)
        {
            tiger.Name n = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_home_list.DataSource = n.getPropertyNotForNameRole(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_name_id_1.SelectedValue), Convert.ToInt32(ddl_role.SelectedValue));
            gv_home_list.DataBind();

            lbl_person2.Text = RegEx.getText(ddl_name_id_1.SelectedItem.Text);
        }



        if (chb_name_add_1.Checked)
        {
            lbl_person2.Text = RegEx.getText(name_fname_1.Text + " , " + name_lname_1.Text);
        }

        lbl_role.Text = ddl_role.SelectedItem.Text;

        MultiView1.ActiveViewIndex = 3;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="module_id"></param>
    /// <returns></returns>
    protected string GetRoleName(int module_id)
    {
        string Module = "";

        switch (module_id)
        {
            case 5: Module = Resources.Resource.lbl_maintenance_worker;
                              break;
            case 6: Module = Resources.Resource.lbl_property_manager;
                              break;
            case 4: Module = Resources.Resource.lbl_owner;
                              break;   
        }
        return Module ;  
    }




    protected DataTable GetPropertyForNameRole(int role_id)
    {
        tiger.Name n = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        return n.getPropertyForNameRole(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_name_id_1.SelectedValue), role_id);
        
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_previous_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_previous1_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;
    }
    protected void btn_continu3_Click(object sender, EventArgs e)
    {



        int number_of_insert = 0;
        string home_id = "";
      

        //---------------------------------------------------------



        for (int j = 0; j < gv_home_list.Rows.Count; j++)
        {

            CheckBox chk_process = (CheckBox)gv_home_list.Rows[j].FindControl("chk_process");
            // if the checkbox is checked the process
            if (chk_process.Checked)
            {
                HiddenField h_id = (HiddenField)gv_home_list.Rows[j].FindControl("h_home_id");

                home_id = home_id + h_id.Value + "|";
                number_of_insert++;
            }
        }
   
        //----------------------------------------------------------

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prRoleNameAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        //  try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;
            cmd.Parameters.Add("@home_id", SqlDbType.VarChar, 8000).Value = RegEx.getText(home_id);
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_role.SelectedValue);

            //----------------------------------------------------------------------------------------

            // new tenants paramater - new person add to the database   
            //name 1

            if (chb_1.Checked)
            {
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_name_id_1.SelectedValue);
            }
            else
            {
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = 0;
            }


            cmd.Parameters.Add("@name_lname_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_lname_1.Text);
            cmd.Parameters.Add("@name_fname_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_fname_1.Text);


            cmd.Parameters.Add("@name_addr_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_1.Text);
            cmd.Parameters.Add("@name_addr_city_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_city_1.Text);
            cmd.Parameters.Add("@name_addr_pc_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_pc_1.Text);
            cmd.Parameters.Add("@name_addr_state_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_addr_state_1.Text);

            if (chb_name_add_1.Checked == false)
                cmd.Parameters.Add("@country_id_1", SqlDbType.Int).Value = System.DBNull.Value;
            else
                cmd.Parameters.Add("@country_id_1", SqlDbType.Int).Value = Convert.ToInt32(ddl_country_id_1.SelectedValue);

            cmd.Parameters.Add("@name_tel_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_tel_1.Text);
            cmd.Parameters.Add("@name_tel_work_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_tel_work_1.Text);
            cmd.Parameters.Add("@name_tel_work_ext_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_tel_work_ext_1.Text);

            tiger.Date d = new tiger.Date();

            cmd.Parameters.Add("@name_cell_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_cell_1.Text);
            cmd.Parameters.Add("@name_fax_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_fax_1.Text);
            cmd.Parameters.Add("@name_email_1", SqlDbType.NVarChar, 50).Value = RegEx.getText(name_email_1.Text);
            cmd.Parameters.Add("@name_com_1", SqlDbType.Text).Value = RegEx.getText(name_com_1.Text);

            if (chb_name_add_1.Checked == false)
                cmd.Parameters.Add("@name_dob_1", SqlDbType.DateTime).Value = System.DBNull.Value;
            else
                cmd.Parameters.Add("@name_dob_1", SqlDbType.DateTime).Value = Convert.ToDateTime(d.DateCulture(ddl_name_1_dob_m.SelectedValue, ddl_name_1_dob_d.SelectedValue, ddl_name_1_dob_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

            
            //execute the insert
            cmd.ExecuteReader();
            
            if (Convert.ToInt32(cmd.Parameters["@return_id"].Value) == 0)
                   lbl_success.Text = Resources.Resource.lbl_successfull_add;
            conn.Close();

        }

    }
}
