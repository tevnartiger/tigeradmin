﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
/// <summary>
/// Done by : Stanley Jocelyn
/// june 2009
/// </summary>
public partial class manager_role_role_update : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            tiger.Name n = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_name_id_1.DataSource = n.getNameList(Convert.ToInt32(Session["schema_id"]), 0);
            ddl_name_id_1.DataBind();
            ddl_name_id_1.Items.Insert(0,new ListItem(Resources.Resource.lbl_select_name, "0"));
            ddl_name_id_1.SelectedIndex = 0;

            

            tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_home_list.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
            gv_home_list.DataBind();

            //--------------------------------------------------------

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prPropertyForNameRole", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value =  Convert.ToInt32(ddl_name_id_1.SelectedValue);
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_role.SelectedValue);

            int home_id = 0;

             try
               {
                   conn.Open();

                   SqlDataReader dr = null;
                   dr = cmd.ExecuteReader();


                   while (dr.Read() == true)
                   {
                       home_id = Convert.ToInt32(dr["home_id"]);
                       
                       for (int j = 0; j < gv_home_list.Rows.Count; j++)
                       {

                           CheckBox chk_process = (CheckBox)gv_home_list.Rows[j].FindControl("chk_process");
                           HiddenField h_id = (HiddenField)gv_home_list.Rows[j].FindControl("h_home_id");

                           // if the checkbox is checked the process
                           if (home_id == Convert.ToInt32(h_id.Value))
                           {
                               chk_process.Checked = true;
                           }
                       }
                   }
            }
            finally
            {
                conn.Close();
            }

            //-------------------------------------------------------------

        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="module_id"></param>
    /// <returns></returns>
    protected string GetRoleName(int module_id)
    {
        string Module = "";

        switch (module_id)
        {
            case 4: Module = Resources.Resource.lbl_owner;
                break;
            case 5: Module = Resources.Resource.lbl_maintenance_worker;
                break;
            case 6: Module = Resources.Resource.lbl_property_manager;
                break;
        }
        return Module;
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="role_id"></param>
    /// <returns></returns>
    protected DataTable GetPropertyForNameRole(int role_id)
    {
        tiger.Name n = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        return n.getPropertyForNameRole(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_name_id_1.SelectedValue), role_id);

    }


    protected void ddl_name_id_1_SelectedIndexChanged(object sender, EventArgs e)
    {
        lbl_person.Text = ddl_name_id_1.SelectedItem.Text;

        tiger.Name n = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_person_role.DataSource = n.getNameRoleAssignementList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_name_id_1.SelectedValue));
        gv_person_role.DataBind();



        for (int j = 0; j < gv_home_list.Rows.Count; j++)
        {
            CheckBox chk_process = (CheckBox)gv_home_list.Rows[j].FindControl("chk_process");
            chk_process.Checked = false;
        }

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prPropertyForNameRole", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_name_id_1.SelectedValue);
        cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_role.SelectedValue);

        int home_id = 0;

        try
        {
            conn.Open();

            SqlDataReader dr = null;
            dr = cmd.ExecuteReader();


            while (dr.Read() == true)
            {
                home_id = Convert.ToInt32(dr["home_id"]);

                for (int j = 0; j < gv_home_list.Rows.Count; j++)
                {

                    CheckBox chk_process = (CheckBox)gv_home_list.Rows[j].FindControl("chk_process");
                    HiddenField h_id = (HiddenField)gv_home_list.Rows[j].FindControl("h_home_id");

                    // if the checkbox is checked the process
                    if (home_id == Convert.ToInt32(h_id.Value))
                    {
                        chk_process.Checked = true;
                    }
                }
            }
        }
        finally
        {
            conn.Close();
        }

        //-------------------------------------------------------------


    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_role_SelectedIndexChanged(object sender, EventArgs e)
    {


        for (int j = 0; j < gv_home_list.Rows.Count; j++)
        {
            CheckBox chk_process = (CheckBox)gv_home_list.Rows[j].FindControl("chk_process");
            chk_process.Checked = false;           
        }

        //--------------------------------------------------------
        tiger.Name n = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_person_role.DataSource = n.getNameRoleAssignementList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_name_id_1.SelectedValue));
        gv_person_role.DataBind();

        //--------------------------------------------------------

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prPropertyForNameRole", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_name_id_1.SelectedValue);
        cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_role.SelectedValue);

        int home_id = 0;

        try
        {
            conn.Open();

            SqlDataReader dr = null;
            dr = cmd.ExecuteReader();


            while (dr.Read() == true)
            {
                home_id = Convert.ToInt32(dr["home_id"]);

                for (int j = 0; j < gv_home_list.Rows.Count; j++)
                {

                    CheckBox chk_process = (CheckBox)gv_home_list.Rows[j].FindControl("chk_process");
                    HiddenField h_id = (HiddenField)gv_home_list.Rows[j].FindControl("h_home_id");

                    // if the checkbox is checked the process
                    if (home_id == Convert.ToInt32(h_id.Value))
                    {
                        chk_process.Checked = true;
                    }
                }
            }
        }
        finally
        {
            conn.Close();
        }

        //-------------------------------------------------------------

    }
    protected void btn_update_Click(object sender, EventArgs e)
    {

        int number_of_insert = 0;
        string home_id = "";


        //---------------------------------------------------------



        for (int j = 0; j < gv_home_list.Rows.Count; j++)
        {

            CheckBox chk_process = (CheckBox)gv_home_list.Rows[j].FindControl("chk_process");
            // if the checkbox is checked the process
            if (chk_process.Checked)
            {
                HiddenField h_id = (HiddenField)gv_home_list.Rows[j].FindControl("h_home_id");

                home_id = home_id + h_id.Value + "|";
                number_of_insert++;
            }
        }

        //----------------------------------------------------------

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prRoleNameUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        //  try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;
            cmd.Parameters.Add("@home_id", SqlDbType.VarChar, 8000).Value = home_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_role.SelectedValue);

            //----------------------------------------------------------------------------------------

            // new tenants paramater - new person add to the database   
            //name 1
            
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_name_id_1.SelectedValue);
            //execute the insert
            cmd.ExecuteReader();

            if (Convert.ToInt32(cmd.Parameters["@return_id"].Value) == 0)
                lbl_success.Text = Resources.Resource.lbl_successfull_add;
            conn.Close();

        }

        //--------------------------------------------------------
        tiger.Name n = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_person_role.DataSource = n.getNameRoleAssignementList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_name_id_1.SelectedValue));
        gv_person_role.DataBind();

        //--------------------------------------------------------

    }
}
