﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="role_wiz.aspx.cs" Inherits="manager_role_role_wiz" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script language="javascript" type="text/javascript">
        function ConfirmDelete()
        {
            if(confirm('Do you wish to delete this file/folder?'))
            return true;
           else
           return false;
        }
  
   

function CheckAllDataViewCheckBoxes( checkVal)
{

 for(i = 0; i < document.forms[0].elements.length; i++)
  {
    
    elm = document.forms[0].elements[i];
    if (elm.type == 'checkbox' && elm.name.substring(elm.name.length-11,elm.name.length) == 'chk_process' )
    {
     {elm.checked = checkVal }
    }
  }
}

</script>

<asp:MultiView ID="MultiView1" runat="server"  ActiveViewIndex="0">
  
  
  
 <asp:View ID="View1" runat="server">
   
     <b><span style="font-size: small">Role Assignement Wizard
     <br />
     </span></b>
     <br /><br /><br />
     Step 1 - Select the person for whom a role will be assigned<br />
     <br />
     Step 2 - Select the role to be assigned to the person ( janitor ,property 
     manager etc .... )<br />
     <br />
     Step 3 - Select the property assigned to the person for his/her role
   <br /><br />
   
   
     <br />
  <table bgcolor="aliceblue" style="width: 50%;">
            <tr>
                <td align="right">
                    <asp:Button ID="btn_next" runat="server" onclick="btn_step1" 
                        Text="<%$ Resources:Resource,lbl_goto_step1 %>" />
                </td>
            </tr>
        </table>
      

 </asp:View>
  
  
  
  <asp:View ID="View2"  runat="server">
   
   
      <b><span style="font-size: small">Role Assignement Wizard<br />
      <br />
      </span></b>
      <br />
   
   Step 1 - Select the person for whom a role will be assigned<br />
    <br /><br />
     <table><tr><td>
     <table>
     <tr><td><asp:CheckBox ID="chb_1" Text="<%$ Resources:Resource, lbl_select_name %>" runat="server" OnCheckedChanged="chb_1_CheckedChanged" AutoPostBack="true" /></td></tr>
     <asp:Panel ID="panel_1" runat="server" Visible="false">
     <tr><td ><asp:Label ID="lbl_tenant" runat="server" Text="<%$ Resources:Resource, lbl_name %>"/></td><td><asp:DropDownList ID="ddl_name_id_1"   DataValueField="name_id" DataTextField="name_name" runat="server" />
        </td></tr>
        </asp:Panel>
            </table>
     </td></tr>
     <tr><td>
         <br />
         <br />
      <asp:CheckBox ID="chb_name_add_1" Visible="false"  Text="<%$ Resources:Resource, lbl_new_name %>" runat="server" OnCheckedChanged="chb_name_add_1_CheckedChanged"  AutoPostBack="true" /></td></tr>
     <asp:Panel ID="panel_name_add_1" Visible="false" runat="server">
     <tr><td> 
     <table border="0" cellpadding="1" cellspacing="3" bgcolor="#ffffcc">
            <tr><td ><asp:Label ID="lbl_fname" runat="server" Text="<%$ Resources:Resource, lbl_fname %>"/></td>
                <td><asp:TextBox ID="name_fname_1"  runat="server"/></td>
                <td ><asp:Label ID="lbl_lname" runat="server" Text="<%$ Resources:Resource, lbl_lname %>"/></td>
                <td><asp:TextBox ID="name_lname_1"  runat="server"/></td>
            </tr>
            <tr>
             <td >
                 <asp:Label ID="lbl_dob" runat="server" Text="<%$ Resources:Resource, lbl_dob %>"/></td>
             <td colspan="3">
                 <asp:DropDownList ID="ddl_name_1_dob_m" runat="server">
                    <asp:ListItem Text="<%$ Resources:Resource, txt_month %>" Value="0"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    
                 </asp:DropDownList>&nbsp; / &nbsp;
                 <asp:DropDownList ID="ddl_name_1_dob_d" runat="server">
                     <asp:ListItem Text="<%$ Resources:Resource, txt_day %>" Value="0"></asp:ListItem>
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>&nbsp; / &nbsp;
                 <asp:DropDownList ID="ddl_name_1_dob_y" runat="server" >
                     <asp:ListItem Text="<%$ Resources:Resource, txt_year %>"  Value="0"></asp:ListItem>
                     <asp:ListItem>1960</asp:ListItem>
                     <asp:ListItem>1961</asp:ListItem>
                     <asp:ListItem>1962</asp:ListItem>
                     <asp:ListItem>1963</asp:ListItem>
                     <asp:ListItem>1964</asp:ListItem>
                     <asp:ListItem>1965</asp:ListItem>
                     <asp:ListItem>1966</asp:ListItem>
                     <asp:ListItem>1967</asp:ListItem>
                     <asp:ListItem>1968</asp:ListItem>
                     <asp:ListItem>1969</asp:ListItem>
                     <asp:ListItem>1970</asp:ListItem>
                     <asp:ListItem>1972</asp:ListItem>
                     <asp:ListItem>1973</asp:ListItem>
                     <asp:ListItem>1974</asp:ListItem>
                     <asp:ListItem>1975</asp:ListItem>
                     <asp:ListItem>1976</asp:ListItem>
                     <asp:ListItem>1977</asp:ListItem>
                     <asp:ListItem>1978</asp:ListItem>
                     <asp:ListItem>1979</asp:ListItem>
                     <asp:ListItem>1980</asp:ListItem>
                     <asp:ListItem>1981</asp:ListItem>
                     <asp:ListItem>1982</asp:ListItem>
                     <asp:ListItem>1983</asp:ListItem>
                     <asp:ListItem>1984</asp:ListItem>
                     <asp:ListItem>1985</asp:ListItem>
                     <asp:ListItem>1986</asp:ListItem>
                     <asp:ListItem>1987</asp:ListItem>
                     <asp:ListItem>1988</asp:ListItem>
                     <asp:ListItem>1989</asp:ListItem>
                     <asp:ListItem>1990</asp:ListItem>
                     <asp:ListItem>1991</asp:ListItem>
                     <asp:ListItem>1992</asp:ListItem>
                     <asp:ListItem>1993</asp:ListItem>
                     <asp:ListItem>1994</asp:ListItem>
                     <asp:ListItem>1995</asp:ListItem>
                     <asp:ListItem>1996</asp:ListItem>
                     <asp:ListItem>1997</asp:ListItem>
                     <asp:ListItem>1998</asp:ListItem>
                     <asp:ListItem>1999</asp:ListItem>
                     <asp:ListItem>2000</asp:ListItem>
                     <asp:ListItem>2001</asp:ListItem>
                     <asp:ListItem>2002</asp:ListItem>
                     <asp:ListItem>2003</asp:ListItem>
                     <asp:ListItem>2004</asp:ListItem>
                     <asp:ListItem>2005</asp:ListItem>
                     <asp:ListItem>2006</asp:ListItem>
                     <asp:ListItem>2007</asp:ListItem>
                     <asp:ListItem>2008</asp:ListItem>
                 </asp:DropDownList></td>
         </tr>
            <tr><td ><asp:Label ID="lbl_address" runat="server" Text="<%$ Resources:Resource, lbl_address %>"/></td>
                <td><asp:TextBox ID="name_addr_1"  runat="server"></asp:TextBox></td>
                <td ><asp:Label ID="lbl_city" runat="server" Text="<%$ Resources:Resource, lbl_city %>"/></td>
                <td><asp:TextBox ID="name_addr_city_1"  runat="server"/></td>
            </tr>
            <tr><td ><asp:Label ID="lbl_pc" runat="server" Text="<%$ Resources:Resource, lbl_pc %>"/></td>
                <td><asp:TextBox ID="name_addr_pc_1"  runat="server"/></td>
                <td ><asp:Label ID="lbl_prov" runat="server" Text="<%$ Resources:Resource, lbl_prov %>"/></td>
                <td>
                    <asp:TextBox ID="name_addr_state_1"  runat="server"/></td>
            </tr>
            <tr><td  ><asp:Label ID="lbl_country" runat="server" Text="<%$ Resources:Resource, lbl_country %>"/></td>
                <td colspan="3"><asp:DropDownList ID="ddl_country_id_1"  runat="server" DataTextField="country_name"
                        DataValueField="country_id"/></td>
            </tr>
            <tr><td ><asp:Label ID="lbl_tel" runat="server" Text="<%$ Resources:Resource, lbl_tel %>"/></td>
                <td><asp:TextBox ID="name_tel_1"  runat="server"/></td>
            <td ><asp:Label ID="lbl_tel_work" runat="server" Text="<%$ Resources:Resource, lbl_tel_work %>"/></td>
                <td><asp:TextBox ID="name_tel_work_1"  runat="server"/></td>
            </tr>
            <tr><td ><asp:Label ID="lbl_tel_work_ext" runat="server" Text="<%$ Resources:Resource, lbl_tel_work_ext %>"/></td>
                <td><asp:TextBox ID="name_tel_work_ext_1"  runat="server"/></td>
		<td ><asp:Label ID="lbl_cell" runat="server" Text="<%$ Resources:Resource, lbl_cell %>"/></td>
                <td><asp:TextBox ID="name_cell_1"  runat="server"/></td>
            </tr>
            <tr><td ><asp:Label ID="lbl_fax" runat="server" Text="<%$ Resources:Resource, lbl_fax %>"/></td>
                <td><asp:TextBox ID="name_fax_1"  runat="server"/></td>
                <td ><asp:Label ID="lbl_email" runat="server" Text="<%$ Resources:Resource, lbl_email %>"/></td>
                <td><asp:TextBox ID="name_email_1"  runat="server"/></td>
            </tr>
            <tr><td ><asp:Label ID="lbl_com" Visible="false" runat="server" Text="<%$ Resources:Resource, lbl_com %>"/></td>
                <td colspan="3"><asp:TextBox ID="name_com_1" Visible="false" runat="server" TextMode="MultiLine" Width="389px"/></td>
            </tr>
        </table>
     </td></tr>
     </asp:Panel>
    <tr><td><br />&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btn_continue1" runat="server"  OnClick="btn_continue1_Click" Text="<%$ Resources:Resource, lbl_next %>" /></td></tr>
    <tr><td>
     
     </td></tr>
     </table>

     
     
  </asp:View>
      
      
      
      
      
      
 <asp:View ID="View3" runat="server">
      

     <b><span style="font-size: small">Role Assignement Wizard<br />
     </span></b>
     <br />
     Step 2 - Select the role to be assigned to the person ( janitor ,property 
     manager etc .... )<br />
     <br />
     <br />
     &nbsp;Person :<asp:Label ID="lbl_person" runat="server" ></asp:Label>
     <br />
     <br />
     <asp:GridView ID="gv_person_role" runat="server" AutoGenerateColumns="false" 
         Width="40%">
         <Columns>
             <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_current_role_assignement %>">
                 <ItemTemplate>
                     <asp:Label ID="lbl_role_name" runat="server" 
                         Text='<%#GetRoleName(Convert.ToInt32(Eval("role_id")))%>' />
                 </ItemTemplate>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_property %>">
                 <ItemTemplate>
                     <asp:Repeater ID="r_property" runat="server" 
                         DataSource='<%#GetPropertyForNameRole(Convert.ToInt32(Eval("role_id")))%>'>
                         <ItemTemplate>
                             <%# Eval("home_name")%><br />
                         </ItemTemplate>
                     </asp:Repeater>
                 </ItemTemplate>
             </asp:TemplateField>
         </Columns>
     </asp:GridView>
     <hr />
     &nbsp;Select New Role Assignement :&nbsp;
     <asp:DropDownList ID="ddl_role" runat="server">
         <asp:ListItem Value="6">Property Manager</asp:ListItem>
         <asp:ListItem Value="5">Maintenance</asp:ListItem>
         <asp:ListItem Text="<%$ Resources:Resource, lbl_owner %>" Value="4"></asp:ListItem>
     </asp:DropDownList>
     <br />
     <br />
     <br />
     &nbsp;&nbsp;
     <asp:Button ID="btn_previous" runat="server" 
         Text="<%$ Resources:Resource, lbl_previous %>" 
         onclick="btn_previous_Click" />
     &nbsp;&nbsp;
     <asp:Button ID="btn_continue2" runat="server" onclick="btn_continue2_Click" 
         Text="<%$ Resources:Resource, lbl_next %>" />
     <br />
     <br />
    

 </asp:View>
      
      
      
      
      
  <asp:View ID="View4" runat="server">
      
       
        <b><span style="font-size: small">Role Assignement Wizard<br />
        </span></b>
        <br />
      
       
        Step 3 - Select the property assigned to the person for his/her role
     <br />
        <br />
        &nbsp;Person :
        <asp:Label ID="lbl_person2" runat="server" Text="Label"></asp:Label>
        
     <br />
       
     <br />
        &nbsp;New Role Assignement&nbsp;&nbsp;&nbsp;&nbsp; :
        <asp:Label ID="lbl_role" runat="server" Text="Label"></asp:Label>
     <br />
        <br />
        <hr />
        Select the properties assigned to the person for his/her role<br /><br />
        <asp:GridView ID="gv_home_list" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" BorderColor="#CDCDCD" 
            BorderWidth="1px" PageSize="20">
            <AlternatingRowStyle BackColor="#F0F0F6" />
            <Columns>
                <asp:TemplateField HeaderText="&lt;input id='chkAllItems' type='checkbox' onclick='CheckAllDataViewCheckBoxes(document.forms[0].chkAllItems.checked)' /&gt;">
                    <ItemTemplate>
                        <asp:CheckBox ID="chk_process" runat="server" />
                        <asp:HiddenField ID="h_home_id" runat="server" Value='<%# Bind("home_id")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="home_name" HeaderText="Property name" />
                <asp:HyperLinkField DataNavigateUrlFields="home_id" 
                    DataNavigateUrlFormatString="~/manager/property/property_view.aspx?h_id={0}" 
                    HeaderText="View" Text="View" />
            </Columns>
            <HeaderStyle BackColor="#F0F0F6" />
        </asp:GridView>
      
      
        <br />
        <asp:Button ID="btn_previous1" runat="server" 
             Text="<%$ Resources:Resource, lbl_previous %>" 
            onclick="btn_previous1_Click" />
        &nbsp;
        <asp:Button ID="btn_continu3" runat="server" 
            Text="<%$ Resources:Resource, btn_submit %>" 
            onclick="btn_continu3_Click" />
      
      
        <br />
        <br />
        <asp:Label ID="lbl_success" runat="server" Text=""></asp:Label>
      
      
  </asp:View>
     
</asp:MultiView>



</asp:Content>

