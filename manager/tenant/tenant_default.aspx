﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="tenant_default.aspx.cs" Inherits="manager_tenant_tenant_default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<br />
<table>
<tr><td> 
<asp:HyperLink ID="HyperLink6" NavigateUrl="~/manager/tenant/tenant_search.aspx?h=0&l=" runat="server"><h2>
<asp:Literal ID="Literal6" Text="Tenant list" runat="server" /></h2>
</asp:HyperLink>
</td><td>Enable you to view ,search and edit for all current, prospective and previous tenant on your account.</td></tr>

<tr><td>
<asp:HyperLink ID="HyperLink1" NavigateUrl="~/manager/tenant/tenant_prospect_add.aspx" runat="server"><h2>
<asp:Literal ID="Literal1" Text="Add Prospective tenant" runat="server" /></h2>
</asp:HyperLink>
</td><td>Enable you to add a possible futur tenant to your account.</td></tr>

<tr><td>
<asp:HyperLink ID="HyperLink2" NavigateUrl="~/manager/reports/reports_delequencies.aspx" runat="server"><h2>
<asp:Literal ID="Literal2" Text="Delequencies" runat="server" /></h2>
</asp:HyperLink>
</td><td>Enable you to view delinqencies and send notice letters to tenants.</td></tr>


</table>
</asp:Content>

