<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="tenant_prospect_update.aspx.cs" Inherits="tenant_tenant_prospect_update" Title="Tenant Prospect Update" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div>
        TENANT PROSPECT - UPDATE<br /><asp:HyperLink ID="tenantapplication_add_link" 
            runat="server">add </asp:HyperLink> -&nbsp;  <asp:HyperLink ID="tenantapplication_update_link"
            runat="server">update</asp:HyperLink> - delete mortgage
        &nbsp;<br />
    
     <table border="0" cellpadding="1" cellspacing="3" bgcolor="#ffffcc">
            <tr><td class="letter_bold">first name <asp:RegularExpressionValidator 
                        ID="reg_name_fname" runat="server" 
                         ControlToValidate="name_fname"
                        ErrorMessage="invalid">
                        </asp:RegularExpressionValidator>
                &nbsp;<asp:RequiredFieldValidator  ControlToValidate="name_fname"
                           ID="req_name_fname" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                </td>
                <td><asp:TextBox ID="name_fname" CssClass="letter" runat="server"/></td>
                <td class="letter_bold">last name 
                    <asp:RegularExpressionValidator 
                        ID="reg_name_lname" runat="server" 
                         ControlToValidate="name_lname"
                        ErrorMessage="invalid">
                        </asp:RegularExpressionValidator>
                &nbsp;<asp:RequiredFieldValidator  ControlToValidate="name_lname"
                           ID="req_name_lname" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                </td>
                <td><asp:TextBox ID="name_lname" CssClass="letter" runat="server"/></td>
            </tr>
            <tr>
             <td class="letter_bold">
                 date of birth ( mm / dd / yyyy )</td>
             <td colspan="3">
                 <asp:DropDownList ID="ddl_name_dob_m" runat="server">
                     <asp:ListItem Value="1">January</asp:ListItem>
                     <asp:ListItem Value="2">February</asp:ListItem>
                     <asp:ListItem Value="3">March</asp:ListItem>
                     <asp:ListItem Value="4">April</asp:ListItem>
                     <asp:ListItem Value="4">May</asp:ListItem>
                     <asp:ListItem Value="6">June</asp:ListItem>
                     <asp:ListItem Value="7">July</asp:ListItem>
                     <asp:ListItem Value="8">August</asp:ListItem>
                     <asp:ListItem Value="9">September</asp:ListItem>
                     <asp:ListItem Value="10">October</asp:ListItem>
                     <asp:ListItem Value="11">November</asp:ListItem>
                     <asp:ListItem Value="12">December</asp:ListItem>
                 </asp:DropDownList>&nbsp; / &nbsp;
                 <asp:DropDownList ID="ddl_name_dob_d" runat="server">
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>&nbsp; / &nbsp;
                 <asp:DropDownList ID="ddl_name_dob_y" runat="server" >
                     <asp:ListItem>1960</asp:ListItem>
                     <asp:ListItem>1961</asp:ListItem>
                     <asp:ListItem>1962</asp:ListItem>
                     <asp:ListItem>1963</asp:ListItem>
                     <asp:ListItem>1964</asp:ListItem>
                     <asp:ListItem>1965</asp:ListItem>
                     <asp:ListItem>1966</asp:ListItem>
                     <asp:ListItem>1967</asp:ListItem>
                     <asp:ListItem>1968</asp:ListItem>
                     <asp:ListItem>1969</asp:ListItem>
                     <asp:ListItem>1970</asp:ListItem>
                     <asp:ListItem>1972</asp:ListItem>
                     <asp:ListItem>1973</asp:ListItem>
                     <asp:ListItem>1974</asp:ListItem>
                     <asp:ListItem>1975</asp:ListItem>
                     <asp:ListItem>1976</asp:ListItem>
                     <asp:ListItem>1977</asp:ListItem>
                     <asp:ListItem>1978</asp:ListItem>
                     <asp:ListItem>1979</asp:ListItem>
                     <asp:ListItem>1980</asp:ListItem>
                     <asp:ListItem>1981</asp:ListItem>
                     <asp:ListItem>1982</asp:ListItem>
                     <asp:ListItem>1983</asp:ListItem>
                     <asp:ListItem>1984</asp:ListItem>
                     <asp:ListItem>1985</asp:ListItem>
                     <asp:ListItem>1986</asp:ListItem>
                     <asp:ListItem>1987</asp:ListItem>
                     <asp:ListItem>1988</asp:ListItem>
                     <asp:ListItem>1989</asp:ListItem>
                     <asp:ListItem>1990</asp:ListItem>
                     <asp:ListItem>1991</asp:ListItem>
                     <asp:ListItem>1992</asp:ListItem>
                     <asp:ListItem>1993</asp:ListItem>
                     <asp:ListItem>1994</asp:ListItem>
                     <asp:ListItem>1995</asp:ListItem>
                     <asp:ListItem>1996</asp:ListItem>
                     <asp:ListItem>1997</asp:ListItem>
                     <asp:ListItem>1998</asp:ListItem>
                     <asp:ListItem>1999</asp:ListItem>
                     <asp:ListItem>2000</asp:ListItem>
                     <asp:ListItem>2001</asp:ListItem>
                     <asp:ListItem>2002</asp:ListItem>
                     <asp:ListItem>2003</asp:ListItem>
                     <asp:ListItem>2004</asp:ListItem>
                     <asp:ListItem>2005</asp:ListItem>
                     <asp:ListItem>2006</asp:ListItem>
                     <asp:ListItem>2007</asp:ListItem>
                     <asp:ListItem>2008</asp:ListItem>
                 </asp:DropDownList></td>
         </tr>
            <tr><td class="letter_bold">address 
                <asp:RegularExpressionValidator 
                        ID="reg_name_addr" runat="server" 
                         ControlToValidate="name_addr"
                        ErrorMessage="invalid">
                        </asp:RegularExpressionValidator>
                &nbsp;<asp:RequiredFieldValidator  ControlToValidate="name_addr"
                           ID="req_name_addr" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                </td>
                <td><asp:TextBox ID="name_addr" CssClass="letter" runat="server"></asp:TextBox></td>
                <td class="letter_bold">City 
                <asp:RegularExpressionValidator 
                        ID="reg_name_addr_city" runat="server" 
                         ControlToValidate="name_addr_city"
                        ErrorMessage="invalid">
                        </asp:RegularExpressionValidator>
                &nbsp;<asp:RequiredFieldValidator  ControlToValidate="name_addr_city"
                           ID="req_name_addr_city" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                </td>
                <td><asp:TextBox ID="name_addr_city" CssClass="letter" runat="server"/></td>
            </tr>
            <tr><td class="letter_bold">Pc 
                <asp:RegularExpressionValidator 
                        ID="reg_name_addr_pc" runat="server" 
                         ControlToValidate="name_addr_pc"
                        ErrorMessage="invalid">
                        </asp:RegularExpressionValidator>
                &nbsp;<asp:RequiredFieldValidator  ControlToValidate="name_addr_pc"
                           ID="req_name_addr_pc" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                </td>
                <td><asp:TextBox ID="name_addr_pc" CssClass="letter" runat="server"/></td>
                <td class="letter_bold">State/Prov</td>
                <td>
                    <asp:TextBox ID="name_addr_state" CssClass="letter" runat="server"/></td>
            </tr>
         <tr>
             <td class="letter_bold">
                 Current employer 
                <asp:RegularExpressionValidator 
                        ID="reg_tenantapplication_current_employer" runat="server" 
                         ControlToValidate="tenantapplication_current_employer"
                        ErrorMessage="invalid">
                        </asp:RegularExpressionValidator>
                &nbsp;<asp:RequiredFieldValidator  ControlToValidate="tenantapplication_current_employer"
                           ID="req_tenantapplication_current_employer" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                </td><td>
                 <asp:TextBox ID="tenantapplication_current_employer" runat="server"></asp:TextBox></td><td>Monthly Income 
                <asp:RegularExpressionValidator 
                        ID="reg_tenantapplication_monthly_income" runat="server" 
                         ControlToValidate="tenantapplication_monthly_income"
                        ErrorMessage="invalid">
                        </asp:RegularExpressionValidator>
                &nbsp;<asp:RequiredFieldValidator  ControlToValidate="tenantapplication_monthly_income"
                           ID="req_tenantapplication_monthly_income" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                </td><td>
                 <asp:TextBox ID="tenantapplication_monthly_income" runat="server"></asp:TextBox></td>
         </tr>
         <tr>
             <td class="letter_bold">
                 Current employer since ( mm / dd / yyyy )</td>
             <td colspan="3">
             <asp:DropDownList ID="ddl_tenantapplication_employer_since_m" runat="server">
                        <asp:ListItem Value="1">January</asp:ListItem>
                        <asp:ListItem Value="2">February</asp:ListItem>
                        <asp:ListItem Value="3">March</asp:ListItem>
                        <asp:ListItem Value="4">April</asp:ListItem>
                        <asp:ListItem Value="5">May</asp:ListItem>
                        <asp:ListItem Value="6">June</asp:ListItem>
                        <asp:ListItem Value="7">July</asp:ListItem>
                        <asp:ListItem Value="7">August</asp:ListItem>
                        <asp:ListItem Value="9">September</asp:ListItem>
                        <asp:ListItem Value="10">October</asp:ListItem>
                        <asp:ListItem Value="11">November</asp:ListItem>
                        <asp:ListItem Value="12">December</asp:ListItem>
                   </asp:DropDownList> 
                    /
                    <asp:DropDownList ID="ddl_tenantapplication_employer_since_d" runat="server" >
    <asp:ListItem>1</asp:ListItem>
    <asp:ListItem>2</asp:ListItem>
    <asp:ListItem>3</asp:ListItem>
    <asp:ListItem>4</asp:ListItem>
    <asp:ListItem>5</asp:ListItem>
    <asp:ListItem>6</asp:ListItem>
    <asp:ListItem>7</asp:ListItem>
    <asp:ListItem>8</asp:ListItem>
    <asp:ListItem>9</asp:ListItem>
    <asp:ListItem>10</asp:ListItem>
    <asp:ListItem>11</asp:ListItem>
    <asp:ListItem>12</asp:ListItem>
    <asp:ListItem>13</asp:ListItem>
    <asp:ListItem>14</asp:ListItem>
    <asp:ListItem>15</asp:ListItem>
    <asp:ListItem>16</asp:ListItem>
    <asp:ListItem>17</asp:ListItem>
    <asp:ListItem>18</asp:ListItem>
    <asp:ListItem>19</asp:ListItem>
    <asp:ListItem>20</asp:ListItem>
    <asp:ListItem>21</asp:ListItem>
    <asp:ListItem>22</asp:ListItem>
    <asp:ListItem>23</asp:ListItem>
    <asp:ListItem>24</asp:ListItem>
    <asp:ListItem>25</asp:ListItem>
    <asp:ListItem>26</asp:ListItem>
    <asp:ListItem>27</asp:ListItem>
    <asp:ListItem>28</asp:ListItem>
    <asp:ListItem>29</asp:ListItem>
    <asp:ListItem>30</asp:ListItem>
    <asp:ListItem>31</asp:ListItem>
    </asp:DropDownList>
                    /
                    <asp:DropDownList ID="ddl_tenantapplication_employer_since_y" runat="server">
                                           <asp:ListItem>1960</asp:ListItem>
     <asp:ListItem>1961</asp:ListItem>
     <asp:ListItem>1962</asp:ListItem>
     <asp:ListItem>1963</asp:ListItem>
     <asp:ListItem>1964</asp:ListItem>
     <asp:ListItem>1965</asp:ListItem>
     <asp:ListItem>1966</asp:ListItem>
     <asp:ListItem>1967</asp:ListItem>
     <asp:ListItem>1968</asp:ListItem>
     <asp:ListItem>1969</asp:ListItem>
     <asp:ListItem>1970</asp:ListItem>
     <asp:ListItem>1972</asp:ListItem>
     <asp:ListItem>1973</asp:ListItem>
     <asp:ListItem>1974</asp:ListItem>
     <asp:ListItem>1975</asp:ListItem>
     <asp:ListItem>1976</asp:ListItem>
     <asp:ListItem>1977</asp:ListItem>
     <asp:ListItem>1978</asp:ListItem>
     <asp:ListItem>1979</asp:ListItem>
     <asp:ListItem>1980</asp:ListItem>
     <asp:ListItem>1981</asp:ListItem>
     <asp:ListItem>1982</asp:ListItem>
     <asp:ListItem>1983</asp:ListItem>
     <asp:ListItem>1984</asp:ListItem>
     <asp:ListItem>1985</asp:ListItem>
     <asp:ListItem>1986</asp:ListItem>
     <asp:ListItem>1987</asp:ListItem>
     <asp:ListItem>1988</asp:ListItem>
     <asp:ListItem>1989</asp:ListItem>
     <asp:ListItem>1990</asp:ListItem>
     <asp:ListItem>1991</asp:ListItem>
      <asp:ListItem>1992</asp:ListItem>
     <asp:ListItem>1993</asp:ListItem>
     <asp:ListItem>1994</asp:ListItem>
     <asp:ListItem>1995</asp:ListItem>
     <asp:ListItem>1996</asp:ListItem>
     <asp:ListItem>1997</asp:ListItem>
     <asp:ListItem>1998</asp:ListItem>
     <asp:ListItem>1999</asp:ListItem>
     <asp:ListItem>2000</asp:ListItem>
     <asp:ListItem>2001</asp:ListItem>
      <asp:ListItem>2002</asp:ListItem>
     <asp:ListItem>2003</asp:ListItem>
     <asp:ListItem>2004</asp:ListItem>
     <asp:ListItem>2005</asp:ListItem>
     <asp:ListItem>2006</asp:ListItem>
<asp:ListItem>2007</asp:ListItem>
<asp:ListItem>2008</asp:ListItem>
                    </asp:DropDownList>
                 </td>
         </tr>
         <tr>
             <td class="letter_bold" valign="top">
                 People who will live in the property with you</td>
             <td colspan="3">
                 <asp:TextBox ID="tenantapplication_people_with_prospect" runat="server" Width="350px" Height="50px" TextMode="MultiLine"></asp:TextBox></td>
         </tr>
         <tr>
             <td class="letter_bold">
                 Current landlord`s name</td>
             <td colspan="3">
                 <asp:TextBox ID="tenantapplication_landlord_name" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator 
                        ID="reg_tenantapplication_landlord_name" runat="server" 
                         ControlToValidate="tenantapplication_landlord_name"
                        ErrorMessage="invalid">
                        </asp:RegularExpressionValidator>
                &nbsp;<asp:RequiredFieldValidator  ControlToValidate="tenantapplication_landlord_name"
                           ID="req_tenantapplication_landlord_name" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                </td>
         </tr>
         <tr>
             <td class="letter_bold">
                 Current landlord &nbsp;telephone</td>
             <td colspan="3">
                 <asp:TextBox ID="tenantapplication_landlord_tel" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator 
                        ID="reg_tenantapplication_landlord_tel" runat="server" 
                         ControlToValidate="tenantapplication_landlord_tel"
                        ErrorMessage="invalid">
                        </asp:RegularExpressionValidator>
                &nbsp;<asp:RequiredFieldValidator  ControlToValidate="tenantapplication_landlord_tel"
                           ID="req_tenantapplication_landlord_tel" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                </td>
         </tr>
         <tr>
             <td class="letter_bold" valign="top">
                 Reason of departure</td>
             <td colspan="3">
                 <asp:TextBox ID="tenantapplication_reason_depart"  runat="server" Height="50px" TextMode="MultiLine" Width="350px"></asp:TextBox></td>
         </tr>
         <tr>
             <td class="letter_bold">
                 SSN</td>
             <td colspan="3">
                 <asp:TextBox ID="name_ssn" runat="server"></asp:TextBox></td>
         </tr>
            <tr><td  class="letter_bold">Country</td>
                <td colspan="3">
                    <asp:DropDownList ID="ddl_country_list" DataTextField="country_name" DataValueField="country_id" runat="server">
                    </asp:DropDownList></td>
            </tr>
            <tr><td class="letter_bold">Telephone
                <asp:RegularExpressionValidator 
                        ID="reg_name_tel" runat="server" 
                         ControlToValidate="name_tel"
                        ErrorMessage="invalid">
                        </asp:RegularExpressionValidator>
                &nbsp;<asp:RequiredFieldValidator  ControlToValidate="name_tel"
                           ID="req_name_tel" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                </td>
                <td><asp:TextBox ID="name_tel" CssClass="letter" runat="server"/></td>
            <td class="letter_bold">Tel. Work</td>
                <td><asp:TextBox ID="name_tel_work" CssClass="letter" runat="server"/></td>
            </tr>
            <tr><td class="letter_bold">Tel. Work ext.</td>
                <td><asp:TextBox ID="name_tel_work_ext" CssClass="letter" runat="server"/></td>
		<td class="letter_bold">Cell</td>
                <td><asp:TextBox ID="name_cell" CssClass="letter" runat="server"/></td>
            </tr>
            <tr><td class="letter_bold">Fax</td>
                <td><asp:TextBox ID="name_fax" CssClass="letter" runat="server"/></td>
                <td class="letter_bold">Email</td>
                <td><asp:TextBox ID="name_email" CssClass="letter" runat="server"/></td>
            </tr>
            <tr><td class="letter_bold" valign="top">Comments</td>
                <td colspan="3"><asp:TextBox ID="name_com" CssClass="letter" runat="server" TextMode="MultiLine" Width="389px"/></td>
            </tr>
        </table>
        <br />
        LOOKING FOR<br />
        <br />
        <table border="0" cellpadding="1" cellspacing="3" bgcolor="#ffffcc">
            <tr>
                <td class="letter_bold" >
                    Minimum price</td>
                <td><asp:DropDownList ID="ddl_pt_rent_min" runat="server">
                    <asp:ListItem Value="0">Any</asp:ListItem>
                    <asp:ListItem Value="250">250</asp:ListItem>
                    <asp:ListItem Value="300">300</asp:ListItem>
                    <asp:ListItem Value="350">350</asp:ListItem>
                    <asp:ListItem Value="400">400</asp:ListItem>
                    <asp:ListItem Value="450">450</asp:ListItem>
                    <asp:ListItem Value="500">500</asp:ListItem>
                    <asp:ListItem Value="550">550</asp:ListItem>
                    <asp:ListItem Value="600">600</asp:ListItem>
                    <asp:ListItem Value="650">650</asp:ListItem>
                    <asp:ListItem Value="700">700</asp:ListItem>
                    <asp:ListItem Value="750">750</asp:ListItem>
                    <asp:ListItem Value="800">800</asp:ListItem>
                    <asp:ListItem Value="850">850</asp:ListItem>
                    <asp:ListItem Value="900">900</asp:ListItem>
                    <asp:ListItem Value="950">950</asp:ListItem>
                    <asp:ListItem Value="1000">1000</asp:ListItem>
                    <asp:ListItem Value="1100">1100</asp:ListItem>
                    <asp:ListItem Value="1200">1200</asp:ListItem>
                    <asp:ListItem Value="1300">1300</asp:ListItem>
                    <asp:ListItem Value="1400">1400</asp:ListItem>
                    <asp:ListItem Value="1500">1500</asp:ListItem>
                    <asp:ListItem Value="1600">1600</asp:ListItem>
                    <asp:ListItem Value="170">1700</asp:ListItem>
                    <asp:ListItem Value="1800">1800</asp:ListItem>
                    <asp:ListItem Value="1900">1900</asp:ListItem>
                    <asp:ListItem Value="2000">2000</asp:ListItem>
                    <asp:ListItem Value="2100">2100</asp:ListItem>
                    <asp:ListItem Value="2200">2200</asp:ListItem>
                    <asp:ListItem Value="2300">2300</asp:ListItem>
                    <asp:ListItem Value="2400">2400</asp:ListItem>
                    <asp:ListItem Value="2500">2500</asp:ListItem>
                    <asp:ListItem Value="2600">2600</asp:ListItem>
                    <asp:ListItem Value="2700">2700</asp:ListItem >
                    <asp:ListItem Value="2800">2800</asp:ListItem >
                    <asp:ListItem Value="2900">2900</asp:ListItem >
                    <asp:ListItem Value="300">3000</asp:ListItem >
                    <asp:ListItem Value="3100">3100</asp:ListItem>
                    <asp:ListItem  Value="3200">3200</asp:ListItem>
                    <asp:ListItem Value="3300">3300</asp:ListItem>
                    <asp:ListItem Value="3400">3400</asp:ListItem>
                    <asp:ListItem Value="3500">3500</asp:ListItem>
                    <asp:ListItem Value="3600">3600</asp:ListItem>
                    <asp:ListItem Value="3700">3700</asp:ListItem>
                    <asp:ListItem Value="3800">3800</asp:ListItem>
                    <asp:ListItem Value="3900">3900</asp:ListItem>
                    <asp:ListItem Value="4000">4000+</asp:ListItem>
                </asp:DropDownList></td>
                <td class="letter_bold">
                    Maximum price</td>
                <td><asp:DropDownList ID="ddl_pt_rent_max" runat="server">
                    <asp:ListItem Value="0">Any</asp:ListItem>
                    <asp:ListItem Value="250">250</asp:ListItem>
                    <asp:ListItem Value="300">300</asp:ListItem>
                    <asp:ListItem Value="350">350</asp:ListItem>
                    <asp:ListItem Value="400">400</asp:ListItem>
                    <asp:ListItem Value="450">450</asp:ListItem>
                    <asp:ListItem Value="500">500</asp:ListItem>
                    <asp:ListItem Value="550">550</asp:ListItem>
                    <asp:ListItem Value="600">600</asp:ListItem>
                    <asp:ListItem Value="650">650</asp:ListItem>
                    <asp:ListItem Value="700">700</asp:ListItem>
                    <asp:ListItem Value="750">750</asp:ListItem>
                    <asp:ListItem Value="800">800</asp:ListItem>
                    <asp:ListItem Value="850">850</asp:ListItem>
                    <asp:ListItem Value="900">900</asp:ListItem>
                    <asp:ListItem Value="950">950</asp:ListItem>
                    <asp:ListItem Value="1000">1000</asp:ListItem>
                    <asp:ListItem Value="1100">1100</asp:ListItem>
                    <asp:ListItem Value="1200">1200</asp:ListItem>
                    <asp:ListItem Value="1300">1300</asp:ListItem>
                    <asp:ListItem Value="1400">1400</asp:ListItem>
                    <asp:ListItem Value="1500">1500</asp:ListItem>
                    <asp:ListItem Value="1600">1600</asp:ListItem>
                    <asp:ListItem Value="170">1700</asp:ListItem>
                    <asp:ListItem Value="1800">1800</asp:ListItem>
                    <asp:ListItem Value="1900">1900</asp:ListItem>
                    <asp:ListItem Value="2000">2000</asp:ListItem>
                    <asp:ListItem Value="2100">2100</asp:ListItem>
                    <asp:ListItem Value="2200">2200</asp:ListItem>
                    <asp:ListItem Value="2300">2300</asp:ListItem>
                    <asp:ListItem Value="2400">2400</asp:ListItem>
                    <asp:ListItem Value="2500">2500</asp:ListItem>
                    <asp:ListItem Value="2600">2600</asp:ListItem>
                    <asp:ListItem Value="2700">2700</asp:ListItem >
                    <asp:ListItem Value="2800">2800</asp:ListItem >
                    <asp:ListItem Value="2900">2900</asp:ListItem >
                    <asp:ListItem Value="300">3000</asp:ListItem >
                    <asp:ListItem Value="3100">3100</asp:ListItem>
                    <asp:ListItem  Value="3200">3200</asp:ListItem>
                    <asp:ListItem Value="3300">3300</asp:ListItem>
                    <asp:ListItem Value="3400">3400</asp:ListItem>
                    <asp:ListItem Value="3500">3500</asp:ListItem>
                    <asp:ListItem Value="3600">3600</asp:ListItem>
                    <asp:ListItem Value="3700">3700</asp:ListItem>
                    <asp:ListItem Value="3800">3800</asp:ListItem>
                    <asp:ListItem Value="3900">3900</asp:ListItem>
                    <asp:ListItem Value="4000">4000+</asp:ListItem>
                </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="letter_bold">
                    Bedrooms</td>
                <td>
                    <asp:DropDownList ID="ddl_pt_bedroom_no" runat="server">
                        <asp:ListItem Value="0">No Minimum</asp:ListItem>
                        <asp:ListItem Value="1">1+</asp:ListItem>
                        <asp:ListItem Value="2">2+</asp:ListItem>
                        <asp:ListItem Value="3">3+</asp:ListItem>
                        <asp:ListItem Value="4">4+</asp:ListItem>
                        <asp:ListItem Value="5">5+</asp:ListItem>
                    </asp:DropDownList></td>
                <td class="letter_bold">
                    Bathrooms</td>
                <td>
                    <asp:DropDownList ID="ddl_pt_bathroom_no" runat="server">
                    <asp:ListItem Value="0">No Maximum</asp:ListItem>
                        <asp:ListItem Value="1">1+</asp:ListItem>
                        <asp:ListItem Value="2">2+</asp:ListItem>
                        <asp:ListItem Value="3">3+</asp:ListItem>
                        <asp:ListItem Value="4">4+</asp:ListItem>
                        <asp:ListItem Value="5">5+</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="letter_bold">
                    Min. square foot</td>
                <td colspan="3">
                    <asp:DropDownList ID="ddl_pt_min_sqft" runat="server">
                        <asp:ListItem Value="0">Any</asp:ListItem>
                        <asp:ListItem>600</asp:ListItem>
                        <asp:ListItem>800</asp:ListItem>
                        <asp:ListItem>1000</asp:ListItem>
                        <asp:ListItem>1200</asp:ListItem>
                        <asp:ListItem>1400</asp:ListItem>
                        <asp:ListItem>1600</asp:ListItem>
                        <asp:ListItem>1800</asp:ListItem>
                        <asp:ListItem>2000</asp:ListItem>
                        <asp:ListItem>2200</asp:ListItem>
                        <asp:ListItem>2400</asp:ListItem>
                        <asp:ListItem>2600</asp:ListItem>
                        <asp:ListItem>2800</asp:ListItem>
                        <asp:ListItem>3000</asp:ListItem>
                        <asp:ListItem>3200</asp:ListItem>
                        <asp:ListItem>3400</asp:ListItem>
                        <asp:ListItem>3600</asp:ListItem>
                        <asp:ListItem>3800</asp:ListItem>
                        <asp:ListItem Value="4000">4000+</asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td class="letter_bold">
                    Begining date (mm/dd/yyyy )
                </td>
                <td colspan="3"><asp:DropDownList ID="ddl_pt_date_begin_m" runat="server">
                        <asp:ListItem Value="1">January</asp:ListItem>
                        <asp:ListItem Value="2">February</asp:ListItem>
                        <asp:ListItem Value="3">March</asp:ListItem>
                        <asp:ListItem Value="4">April</asp:ListItem>
                        <asp:ListItem Value="5">May</asp:ListItem>
                        <asp:ListItem Value="6">June</asp:ListItem>
                        <asp:ListItem Value="7">July</asp:ListItem>
                        <asp:ListItem Value="7">August</asp:ListItem>
                        <asp:ListItem Value="9">September</asp:ListItem>
                        <asp:ListItem Value="10">October</asp:ListItem>
                        <asp:ListItem Value="11">November</asp:ListItem>
                        <asp:ListItem Value="12">December</asp:ListItem>
                    </asp:DropDownList>
                    /
                    <asp:DropDownList ID="ddl_pt_date_begin_d" runat="server" >
    <asp:ListItem>1</asp:ListItem>
    <asp:ListItem>2</asp:ListItem>
    <asp:ListItem>3</asp:ListItem>
    <asp:ListItem>4</asp:ListItem>
    <asp:ListItem>5</asp:ListItem>
    <asp:ListItem>6</asp:ListItem>
    <asp:ListItem>7</asp:ListItem>
    <asp:ListItem>8</asp:ListItem>
    <asp:ListItem>9</asp:ListItem>
    <asp:ListItem>10</asp:ListItem>
    <asp:ListItem>11</asp:ListItem>
    <asp:ListItem>12</asp:ListItem>
    <asp:ListItem>13</asp:ListItem>
    <asp:ListItem>14</asp:ListItem>
    <asp:ListItem>15</asp:ListItem>
    <asp:ListItem>16</asp:ListItem>
    <asp:ListItem>17</asp:ListItem>
    <asp:ListItem>18</asp:ListItem>
    <asp:ListItem>19</asp:ListItem>
    <asp:ListItem>20</asp:ListItem>
    <asp:ListItem>21</asp:ListItem>
    <asp:ListItem>22</asp:ListItem>
    <asp:ListItem>23</asp:ListItem>
    <asp:ListItem>24</asp:ListItem>
    <asp:ListItem>25</asp:ListItem>
    <asp:ListItem>26</asp:ListItem>
    <asp:ListItem>27</asp:ListItem>
    <asp:ListItem>28</asp:ListItem>
    <asp:ListItem>29</asp:ListItem>
    <asp:ListItem>30</asp:ListItem>
    <asp:ListItem>31</asp:ListItem>
    </asp:DropDownList>
                    /
                    <asp:DropDownList ID="ddl_pt_date_begin_y" runat="server">
     <asp:ListItem>1961</asp:ListItem>
     <asp:ListItem>1962</asp:ListItem>
     <asp:ListItem>1963</asp:ListItem>
     <asp:ListItem>1964</asp:ListItem>
     <asp:ListItem>1965</asp:ListItem>
     <asp:ListItem>1966</asp:ListItem>
     <asp:ListItem>1967</asp:ListItem>
     <asp:ListItem>1968</asp:ListItem>
     <asp:ListItem>1969</asp:ListItem>
     <asp:ListItem>1970</asp:ListItem>
     <asp:ListItem>1972</asp:ListItem>
     <asp:ListItem>1973</asp:ListItem>
     <asp:ListItem>1974</asp:ListItem>
     <asp:ListItem>1975</asp:ListItem>
     <asp:ListItem>1976</asp:ListItem>
     <asp:ListItem>1977</asp:ListItem>
     <asp:ListItem>1978</asp:ListItem>
     <asp:ListItem>1979</asp:ListItem>
     <asp:ListItem>1980</asp:ListItem>
     <asp:ListItem>1981</asp:ListItem>
     <asp:ListItem>1982</asp:ListItem>
     <asp:ListItem>1983</asp:ListItem>
     <asp:ListItem>1984</asp:ListItem>
     <asp:ListItem>1985</asp:ListItem>
     <asp:ListItem>1986</asp:ListItem>
     <asp:ListItem>1987</asp:ListItem>
     <asp:ListItem>1988</asp:ListItem>
     <asp:ListItem>1989</asp:ListItem>
     <asp:ListItem>1990</asp:ListItem>
     <asp:ListItem>1991</asp:ListItem>
      <asp:ListItem>1992</asp:ListItem>
     <asp:ListItem>1993</asp:ListItem>
     <asp:ListItem>1994</asp:ListItem>
     <asp:ListItem>1995</asp:ListItem>
     <asp:ListItem>1996</asp:ListItem>
     <asp:ListItem>1997</asp:ListItem>
     <asp:ListItem>1998</asp:ListItem>
     <asp:ListItem>1999</asp:ListItem>
     <asp:ListItem>2000</asp:ListItem>
     <asp:ListItem>2001</asp:ListItem>
      <asp:ListItem>2002</asp:ListItem>
     <asp:ListItem>2003</asp:ListItem>
     <asp:ListItem>2004</asp:ListItem>
     <asp:ListItem>2005</asp:ListItem>
     <asp:ListItem>2006</asp:ListItem>
<asp:ListItem>2007</asp:ListItem>
<asp:ListItem>2008</asp:ListItem>

                    </asp:DropDownList>
                    </td>
            </tr>
            <tr>
                <td>
                 <strong><span >Utilities</span></strong><br />
     <asp:CheckBox  CssClass="letter" ID="pt_electricity_inc" text="Electricity" runat="server"  />
     <br />
     <asp:CheckBox  CssClass="letter" ID="pt_heat_inc" text="Heat" runat="server"  />
     <br />
     <asp:CheckBox CssClass="letter" ID="pt_water_inc" text="Water" runat="server"  />
     <br />
     <asp:CheckBox  CssClass="letter" ID="pt_water_tax_inc" text="Water tax" runat="server"  />
     
     </td><td></td><td valign=top >
       <span ><strong>Accomodations</strong></span><br />
     <asp:CheckBox CssClass="letter" ID="pt_parking_inc" text="Parking" runat="server" />
     <br />
     <asp:CheckBox CssClass="letter" ID="pt_garage_inc" text="Garage" runat="server" />
     <br />
     <asp:CheckBox CssClass="letter" ID="pt_furnished_inc" text="Furnished" runat="server" />
     <br />
     <asp:CheckBox CssClass="letter" ID="pt_semi_furnished_inc" text="Semi-furnished" runat="server" />
     <br />
                </td>
            </tr>
            <tr>
                <td class="letter_bold" >
                    Comments</td>
                <td colspan="3">
                    <asp:TextBox ID="pt_com" CssClass="letter" runat="server" TextMode="MultiLine" Width="389px"/></td>
            </tr>
        </table>
        <br />
        <asp:Button ID="btn_submit" runat="server" OnClick="btn_submit_Click" Text="Submit" /></div>
    </asp:Content>