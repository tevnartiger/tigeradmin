﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="tenant_view.aspx.cs" Inherits="manager_tenant_tenant_view" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>


    <h4><b>
        <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lbl_tenant %>"></asp:Label>&nbsp; -
        <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lbl_folder %>"></asp:Label></b></h4>
    <asp:HyperLink ID="tenantapplication_add_link"
        runat="server">
        <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lbl_add %>"></asp:Label>
    </asp:HyperLink>
    -&nbsp; 
    <asp:HyperLink ID="tenantapplication_update_link"
        runat="server">
        <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_update %>"></asp:Label>
    </asp:HyperLink>

    <br />
    <b>
        <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lbl_u_name %>"></asp:Label>&nbsp; </b>: <b>
            <asp:Label ID="lbl_name_lname" runat="server" />&nbsp;,
            <asp:Label ID="lbl_name_fname" runat="server" />
        </b>
    <br />
    <b>
        <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_application_date %>"></asp:Label>
        :&nbsp;&nbsp;<asp:Label ID="lbl_tenantapplication_date_insert" runat="server" Text=""></asp:Label>
    </b>
    <br />
    <br />




    <h1><%= Resources.Resource.lbl_roomate_and_application %>  </h1>



    <table width="100%">
        <tr>
            <td bgcolor="aliceblue">
                <b>
                    <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, lbl_current_roomate %>"></asp:Label></b></td>
        </tr>
    </table>
    <asp:GridView ID="gv_roomates_list" runat="server" AllowSorting="True"
        AutoGenerateColumns="False" BorderColor="White"
        EmptyDataText="NONE"
        Width="40%">
        <Columns>
            <asp:BoundField DataField="tenant_name" />
            <asp:HyperLinkField DataNavigateUrlFields="tenant_id,name_id,tu_id,unit_id,home_id"
                DataNavigateUrlFormatString="~/manager/tenant/tenant_view.aspx?t_id={0}&amp;n_id={1}&amp;tu_id={2}&amp;unit_id={3}&amp;h_id={4}"
                Text="View" />
        </Columns>
    </asp:GridView>
    <br />
    <table width="100%">
        <tr>
            <td bgcolor="aliceblue">
                <b>
                    <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lbl_application_information %>"></asp:Label></b></td>
        </tr>
    </table>
    <br />

    <table border="0" cellpadding="1" cellspacing="3">
        <tr>
            <td>
                <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, lbl_dob %>"
                    Style="font-weight: 700"></asp:Label></td>
            <td colspan="2">
                <asp:Label ID="lbl_name_dob" runat="server" />
            </td>
            <td></td>
            <td rowspan="12" valign="top">
                <asp:Repeater ID="rNameImage" runat="server">
                    <ItemTemplate>
                        <asp:Image ID="name_photo" runat="server" Height="179"
                            ImageUrl='<%# "~/mediahandler/NameImage.ashx?n_id="+ Eval("name_id") %>'
                            Width="196" />
                    </ItemTemplate>
                </asp:Repeater>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label10" runat="server"
                    Text="<%$ Resources:Resource, lbl_address %>" Style="font-weight: 700"></asp:Label></td>
            <td>
                <asp:Label ID="lbl_name_addr" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Label11" runat="server"
                    Text="<%$ Resources:Resource, lbl_city %>" Style="font-weight: 700" /></td>
            <td>
                <asp:Label ID="lbl_name_addr_city" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label12" runat="server" Text="<%$ Resources:Resource, lbl_pc %>"
                    Style="font-weight: 700" /></td>
            <td>
                <asp:Label ID="lbl_name_addr_pc" runat="server" />
            </td>
            <td>
                <asp:Label ID="Label13" runat="server"
                    Text="<%$ Resources:Resource, lbl_prov %>" Style="font-weight: 700" /></td>
            <td>
                <asp:Label ID="lbl_name_addr_state" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label14" runat="server"
                    Text="<%$ Resources:Resource, lbl_current_employer %>"
                    Style="font-weight: 700" /></td>
            <td>
                <asp:Label ID="lbl_tenantapplication_current_employer" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Label15" runat="server"
                    Text="<%$ Resources:Resource, lbl_monthly_income %>" Style="font-weight: 700" /></td>
            <td>
                <asp:Label ID="lbl_tenantapplication_monthly_income" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label16" runat="server"
                    Text="<%$ Resources:Resource, lbl_current_employer_since %>"
                    Style="font-weight: 700" /></td>
            <td colspan="3">
                <asp:Label ID="lbl_tenantapplication_employer_since" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Label ID="Label17" runat="server"
                    Text="<%$ Resources:Resource, lbl_live_with_you %>" Style="font-weight: 700" />
            </td>
            <td colspan="3">&nbsp;
                   <asp:Label ID="lbl_tenantapplication_people_with_prospect" runat="server"
                       Height="50px" TextMode="MultiLine" Width="350px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label18" runat="server"
                    Text="<%$ Resources:Resource, lbl_current_landlord_name %>"
                    Style="font-weight: 700" /></td>
            <td colspan="3">
                <asp:Label ID="lbl_tenantapplication_landlord_name" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label19" runat="server"
                    Text="<%$ Resources:Resource, lbl_current_landlord_tel %>"
                    Style="font-weight: 700" /></td>
            <td colspan="3">
                <asp:Label ID="lbl_tenantapplication_landlord_tel" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Label ID="Label20" runat="server"
                    Text="<%$ Resources:Resource, lbl_reason_departure %>"
                    Style="font-weight: 700" /></td>
            <td colspan="3">&nbsp;
                   <asp:Label ID="lbl_tenantapplication_reason_depart" runat="server"
                       Height="50px" TextMode="MultiLine" Width="350px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label21" runat="server"
                    Text="<%$ Resources:Resource, lbl_ssn %>" Style="font-weight: 700" /></td>
            <td colspan="3">
                <asp:Label ID="lbl_name_ssn" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label22" runat="server"
                    Text="<%$ Resources:Resource, lbl_country %>" Style="font-weight: 700" /></td>
            <td colspan="3">
                <asp:Label ID="lbl_country_name" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label23" runat="server"
                    Text="<%$ Resources:Resource, lbl_tel %>" Style="font-weight: 700" /></td>
            <td>
                <asp:Label ID="lbl_name_tel" runat="server" />
            </td>
            <td>
                <asp:Label ID="Label24" runat="server"
                    Text="<%$ Resources:Resource, lbl_tel_work %>" Style="font-weight: 700" /></td>
            <td>
                <asp:Label ID="lbl_name_tel_work" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label25" runat="server"
                    Text="<%$ Resources:Resource, lbl_tel_work_ext %>" Style="font-weight: 700" /></td>
            <td>
                <asp:Label ID="lbl_name_tel_work_ext" runat="server" />
            </td>
            <td>
                <asp:Label ID="Label26" runat="server"
                    Text="<%$ Resources:Resource, lbl_cell %>" Style="font-weight: 700" /></td>
            <td>
                <asp:Label ID="lbl_name_cell" runat="server" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label27" runat="server" Text="<%$ Resources:Resource, lbl_fax %>"
                    Style="font-weight: 700" /></td>
            <td>
                <asp:Label ID="lbl_name_fax" runat="server" />
            </td>
            <td>
                <asp:Label ID="Label28" runat="server"
                    Text="<%$ Resources:Resource, lbl_email %>" Style="font-weight: 700" /></td>
            <td>
                <asp:Label ID="lbl_name_email" runat="server" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Label ID="Label29" runat="server"
                    Text="<%$ Resources:Resource, lbl_comments %>" Style="font-weight: 700" /></td>
            <td colspan="3">
                <asp:Label ID="lbl_name_com" runat="server" TextMode="MultiLine"
                    Width="389px" />
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <br />
    <br />
    <table border="0" cellpadding="1" cellspacing="3">
        <tr>
            <td>
                <asp:Label ID="Label30" runat="server"
                    Text="<%$ Resources:Resource, lbl_min_price %>" Style="font-weight: 700" /></td>
            <td>
                <asp:Label ID="lbl_pt_rent_min" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Label31" runat="server"
                    Text="<%$ Resources:Resource, lbl_max_price %>" Style="font-weight: 700" /></td>
            <td>
                <asp:Label ID="lbl_pt_rent_max" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label32" runat="server"
                    Text="<%$ Resources:Resource, lbl_bedrooms %>" Style="font-weight: 700" /></td>
            <td>
                <asp:Label ID="lbl_pt_bedroom_no" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="Label33" runat="server"
                    Text="<%$ Resources:Resource, lbl_bathrooms %>" Style="font-weight: 700" /></td>
            <td>
                <asp:Label ID="lbl_pt_bathroom_no" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label34" runat="server"
                    Text="<%$ Resources:Resource, lbl_min_sqft %>" Style="font-weight: 700" /></td>
            <td colspan="3">
                <asp:Label ID="lbl_pt_min_sqft" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label35" runat="server"
                    Text="<%$ Resources:Resource, lbl_begining_date %>" Style="font-weight: 700" />
            </td>
            <td colspan="3">
                <asp:Label ID="lbl_pt_date_begin" runat="server" Width="57px"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <strong><span>
                    <asp:Label ID="Label37" runat="server"
                        Text="<%$ Resources:Resource, lbl_utilities %>" /></span></strong><br />
                <asp:CheckBox ID="pt_electricity_inc" runat="server"
                    Text="<%$ Resources:Resource, lbl_electricity %>" />
                <br />
                <asp:CheckBox ID="pt_heat_inc" runat="server"
                    Text="<%$ Resources:Resource, lbl_heat %>" />
                <br />
                <asp:CheckBox ID="pt_water_inc" runat="server"
                    Text="<%$ Resources:Resource, lbl_water %>" />
                <br />
                <asp:CheckBox ID="pt_water_tax_inc" runat="server"
                    Text="<%$ Resources:Resource, lbl_water_tax %>" />
            </td>
            <td></td>
            <td valign="top">
                <span><strong>
                    <asp:Label ID="Label38" runat="server"
                        Text="<%$ Resources:Resource, lbl_accommodations %>" /></strong></span><br />
                <asp:CheckBox ID="pt_parking_inc" runat="server"
                    Text="<%$ Resources:Resource, lbl_parking %>" />
                <br />
                <asp:CheckBox ID="pt_garage_inc" runat="server"
                    Text="<%$ Resources:Resource, lbl_garage %>" />
                <br />
                <asp:CheckBox ID="pt_furnished_inc" runat="server"
                    Text="<%$ Resources:Resource, lbl_furnished %>" />
                <br />
                <asp:CheckBox ID="pt_semi_furnished_inc" runat="server"
                    Text="<%$ Resources:Resource, lbl_semi_furnished %>" />
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label36" runat="server"
                    Text="<%$ Resources:Resource, lbl_comments %>" /></td>
            <td colspan="3">
                <asp:Label ID="pt_com" runat="server" TextMode="MultiLine" Width="389px" />
            </td>
        </tr>
    </table>

    <br />
    <br />
    <hr />
    <br />
    <br />

    <h1><%= Resources.Resource.lbl_payment_history %></h1>
    <table width="100%">
        <tr>
            <td bgcolor="aliceblue">
                <b>
                    <asp:Label ID="Label39" runat="server" Text="<%$ Resources:Resource, lbl_u_payment_history%>" /></b>
            </td>
        </tr>
    </table>
    <asp:GridView ID="gv_tenant_payment_archive" runat="server" AllowPaging="true"
        AllowSorting="true"
        AlternatingRowStyle-BackColor="#F0F0F6"
        BorderColor="#CDCDCD" BorderWidth="1"
        HeaderStyle-BackColor="#F0F0F6" AutoGenerateColumns="false"
        EmptyDataText="<%$ Resources:Resource, lbl_none%>" GridLines="Both"
        OnPageIndexChanging="gv_tenant_payment_archive_PageIndexChanging" PageSize="10"
        Width="70%">
        <Columns>
            <asp:BoundField DataField="home_name" HeaderText="<%$ Resources:Resource, lbl_property%>" />
            <asp:BoundField DataField="unit_door_no" HeaderText="<%$ Resources:Resource, lbl_door_number%>" />
            <asp:TemplateField HeaderText="Lease type">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lbl_lease_type" Text='<%#GetLeaseType(Convert.ToString(Eval("unit_type")))%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="rp_due_date" DataFormatString="{0:M-dd-yyyy}"
                HeaderText="<%$ Resources:Resource, lbl_due_date%>" />
            <asp:BoundField DataField="rp_paid_date" DataFormatString="{0:M-dd-yyyy}"
                HeaderText="<%$ Resources:Resource, gv_paid_date%>" />
        </Columns>
    </asp:GridView>

    <br />


    <table width="100%">
        <tr>
            <td bgcolor="aliceblue">
                <b>
                    <asp:Label ID="Label40" runat="server" Text="<%$ Resources:Resource, lbl_u_rent_paid_on_time%>" /></b>
            </td>
        </tr>
    </table>
    <br />

    <asp:Repeater ID="r_ontime_rent" runat="server">
        <ItemTemplate>
            <asp:Label ID="Label40" runat="server" Text="<%$ Resources:Resource, lbl_rent_paid_on_time%>" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <%# Eval("ontime")%>
            <br />
            <asp:Label ID="Label41" runat="server" Text="<%$ Resources:Resource, lbl_number_of_payment%>" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <%# Eval("total")%>
            <br />
            <asp:Label ID="Label42" runat="server" Text="<%$ Resources:Resource, lbl_percentage%>" />
            &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<%# Eval("percentage")%>
        </ItemTemplate>
    </asp:Repeater>
    <br />


    <table width="100%">
        <tr>
            <td bgcolor="aliceblue">
                <b>
                    <asp:Label ID="Label42" runat="server" Text="<%$ Resources:Resource, lbl_late_rent_since_application%>" />
                </b>
            </td>
        </tr>
    </table>


    <br />
    <br />
    <asp:Repeater ID="Repeater1" runat="server">
        <ItemTemplate>
            <asp:Label ID="Label42" runat="server" Text="<%$ Resources:Resource, lbl_less31%>" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <%# Eval("less31")%>
            <br />
            <asp:Label ID="Label44" runat="server" Text="<%$ Resources:Resource, lbl_between_31and60%>" />&nbsp; : <%# Eval("between31and60")%>
            <br />
            <asp:Label ID="Label45" runat="server" Text="<%$ Resources:Resource, lbl_between_61and90%>" />&nbsp; :&nbsp;<%# Eval("between61and90")%>
            <br />
            <asp:Label ID="Label46" runat="server" Text="<%$ Resources:Resource, lbl_more90%>" />&&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;<%# Eval("more91")%>
        </ItemTemplate>
    </asp:Repeater>
    <br />
    <br />
    <br />
    <table width="100%">
        <tr>
            <td bgcolor="aliceblue">
                <b>
                    <asp:Label ID="Label43" runat="server" Text="<%$ Resources:Resource, lbl_late_rent_list%>" /></b>
            </td>
        </tr>
    </table>
    <br />
    <asp:GridView ID="gv_tenant_late_payment_archive" runat="server"
        AutoGenerateColumns="false"
        EmptyDataText="<%$ Resources:Resource, lbl_none%>"
        GridLines="Both" AlternatingRowStyle-BackColor="#F0F0F6"
        BorderColor="#CDCDCD" BorderWidth="1"
        HeaderStyle-BackColor="#F0F0F6" Width="70%">
        <Columns>
            <asp:BoundField DataField="home_name" HeaderText="<%$ Resources:Resource, lbl_property%>" />
            <asp:BoundField DataField="unit_door_no" HeaderText="<%$ Resources:Resource, lbl_door_number%>" />
            <asp:TemplateField HeaderText="Lease type">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lbl_lease_type" Text='<%#GetLeaseType(Convert.ToString(Eval("unit_type")))%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="rp_due_date" DataFormatString="{0:M-dd-yyyy}"
                HeaderText="<%$ Resources:Resource, lbl_due_date%>" />
            <asp:BoundField DataField="rp_paid_date" DataFormatString="{0:M-dd-yyyy}"
                HeaderText="<%$ Resources:Resource, gv_paid_date%>" />
        </Columns>
    </asp:GridView>
    <br />





    <br />
    <br />
    <hr />
    <br />
    <br />




    <h1><%= Resources.Resource.lbl_current_lease %></h1>

    <table width="100%">
        <tr>
            <td>


                <table width="100%">
                    <tr>
                        <td bgcolor="aliceblue" style="height: 18px"><b>
                            <asp:Label ID="Label47"
                                runat="server" Text="<%$ Resources:Resource, lbl_current_lease %>" /></b> </td>
                    </tr>
                </table>
                <br />

                <br />
                <asp:Repeater runat="server" ID="r_HomeUnitView">
                    <ItemTemplate>
                        <table bgcolor="#ffffcc">

                            <tr>
                                <td>
                                    <b>
                                        <asp:Label ID="Label51" runat="server" Text="<%$ Resources:Resource, lbl_district%>"></asp:Label>&nbsp;:&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_district")%> 
                                    </b></td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <b><%#DataBinder.Eval(Container.DataItem, "home_addr_no")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_street")%></b></td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <b><%#DataBinder.Eval(Container.DataItem, "home_city")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_prov")%>
                   &nbsp;,&nbsp;&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_pc")%> </b>
                                </td>

                            </tr>
                            <tr>
                                <td valign="top">
                                    <b>
                                        <asp:Label ID="Label80" runat="server" Text="<%$ Resources:Resource, lbl_unit%>"></asp:Label></b>  &nbsp;:&nbsp;<%#DataBinder.Eval(Container.DataItem, "unit_door_no")%> 
                
                                </td>

                            </tr>
                        </table>

                    </ItemTemplate>
                </asp:Repeater>
                &nbsp;<br />

                <table>
                    <tr id="tr_lease_type" runat="server">
                        <td>
                            <b>Lease type</b> </td>
                        <td>&nbsp;:&nbsp;&nbsp;<asp:Label ID="lbl_lease_type" runat="server"></asp:Label></td>
                    </tr>
                    <tr id="tr_company" runat="server">
                        <td>
                            <b>Company</b></td>
                        <td>&nbsp;:&nbsp;&nbsp;
                                    <asp:Label ID="lbl_company" runat="server"></asp:Label></td>
                    </tr>
                </table>
                &nbsp;<table width="90%">
                    <tr>
                        <td style="color: #D3D3D3">__________________________________________________________________________________________</td>
                    </tr>
                </table>
                <br />
                <table>
                    <tr>
                        <td>
                            <div id="txt_current_tenant_name" runat="server" />
                        </td>
                    </tr>

                </table>
                <br />

                <table width="100%">
                    <tr>
                        <td valign="top" style="width: 411px">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td valign="top">
                                        <b>
                                            <asp:Label ID="Label53" runat="server"
                                                Text="<%$ Resources:Resource, lbl_rent_amount %>"></asp:Label></b>&nbsp;</td>
                                    <td valign="top">:
                                                <asp:Label ID="lbl_rent_amount" runat="server" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 125px">
                                        <b>
                                            <asp:Label ID="Label52" runat="server" Text="<%$ Resources:Resource, lbl_since %>"></asp:Label></b></td>
                                    <td>:
                                                <asp:Label ID="lbl_current_rl_date_begin" runat="server" Style="color: #FF3300"></asp:Label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td valign="top">
                            <asp:GridView
                                AlternatingRowStyle-BackColor="#F0F0F6"
                                BorderColor="#CDCDCD" BorderWidth="1" Width="100%"
                                HeaderStyle-BackColor="#F0F0F6"
                                AutoGenerateColumns="False" ID="gv_rent_list" runat="server">

                                <Columns>
                                    <asp:BoundField DataField="rl_rent_amount" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, lbl_previous_rent_amount %>"
                                        HtmlEncode="False" />
                                    <asp:BoundField DataField="rl_date_begin" DataFormatString="{0:M-dd-yyyy}"
                                        HeaderText="<%$ Resources:Resource, lbl_date_begin %>" HtmlEncode="False" />
                                    <asp:BoundField DataField="rl_date_end" DataFormatString="{0:M-dd-yyyy}"
                                        HeaderText="<%$ Resources:Resource, lbl_date_end %>" HtmlEncode="False" />
                                    <asp:BoundField DataField="rl_id" HeaderText="rl_id" />

                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>

                </table>
                <br />

                <table width="100%">


                    <tr>
                        <td bgcolor="aliceblue" colspan="2">
                            <b>
                                <asp:Label ID="Label54" runat="server"
                                    Text="<%$ Resources:Resource, lbl_u_utilities_accommodations %>"></asp:Label></b><br />

                        </td>
                    </tr>


                    <tr>
                        <td valign="top">
                            <b>
                                <asp:Label ID="Label55" runat="server"
                                    Text="<%$ Resources:Resource, lbl_accommodation_since %>"></asp:Label>&nbsp;</b>:
 <asp:Label ID="lbl_current_ta_date_begin" runat="server" Style="color: #FF3300"></asp:Label>

                        </td>

                        <td valign="top" rowspan="2" align="right">
                            <asp:GridView Width="80%"
                                AlternatingRowStyle-BackColor="#F0F0F6"
                                BorderColor="#CDCDCD" BorderWidth="1"
                                HeaderStyle-BackColor="#F0F0F6"
                                AutoGenerateColumns="False" ID="gv_accommodation_list" runat="server">


                                <Columns>

                                    <asp:BoundField DataField="ta_date_begin" DataFormatString="{0:M-dd-yyyy}"
                                        HeaderText="<%$ Resources:Resource, lbl_previous_accommodation_begin %>"
                                        HtmlEncode="False" />
                                    <asp:BoundField DataField="ta_date_end" DataFormatString="{0:M-dd-yyyy}"
                                        HeaderText="<%$ Resources:Resource, lbl_previous_accommodation_end %>"
                                        HtmlEncode="False" />


                                    <asp:HyperLinkField Text="<%$ Resources:Resource, lbl_view %>"
                                        DataNavigateUrlFields="tu_id,ta_id"
                                        DataNavigateUrlFormatString="~/manager/lease/lease_accommodation_archive_view.aspx?tu_id={0}&ta_id={1}"
                                        HeaderText="<%$ Resources:Resource, lbl_view %>" />

                                </Columns>
                            </asp:GridView>


                            <br />


                        </td>
                    </tr>


                    <tr>
                        <td>
                            <strong><span style="font-size: 10pt">
                                <asp:Label ID="Label56" runat="server"
                                    Text="<%$ Resources:Resource, lbl_utilities_included %>"></asp:Label></span></strong><br />
                            <asp:CheckBox AutoPostBack="True" ID="ta_electricity_inc"
                                Text="<%$ Resources:Resource, lbl_electricity %>" runat="server" />
                            <br />
                            <asp:CheckBox AutoPostBack="True" ID="ta_heat_inc"
                                Text="<%$ Resources:Resource, lbl_heat %>" runat="server" />
                            <br />
                            <asp:CheckBox AutoPostBack="True" ID="ta_water_inc"
                                Text="<%$ Resources:Resource, lbl_water %>" runat="server" />
                            <br />
                            <asp:CheckBox AutoPostBack="True" ID="ta_water_tax_inc"
                                Text="<%$ Resources:Resource, lbl_water_tax %>" runat="server" />
                            <br />
                            <asp:CheckBox AutoPostBack="True" ID="ta_none"
                                Text="<%$ Resources:Resource, lbl_none %>" runat="server" />




                            <br />
                            <br />




                            <span style="font-size: 10pt"><strong>
                                <asp:Label ID="Label57" runat="server"
                                    Text="<%$ Resources:Resource, lbl_accommodation_included %>" /></strong></span><br />
                            <br />
                            <asp:CheckBox ID="ta_parking_inc" Text="<%$ Resources:Resource, lbl_parking %>"
                                runat="server" />
                            <br />
                            <asp:CheckBox ID="ta_garage_inc" Text="<%$ Resources:Resource, lbl_garage %>"
                                runat="server" />
                            <br />
                            <asp:CheckBox ID="ta_furnished_inc"
                                Text="<%$ Resources:Resource, lbl_furnished %>" runat="server" />
                            <br />
                            <asp:CheckBox ID="ta_semi_furnished_inc"
                                Text="<%$ Resources:Resource, lbl_semi_furnished %>" runat="server" />
                            <br />

                        </td>

                    </tr>


                    <tr>
                        <td valign="top" style="font-weight: bold">
                            <asp:Label ID="Label58" runat="server"
                                Text="<%$ Resources:Resource, lbl_comment_extra %>"></asp:Label>&nbsp;:</td>
                        <td>&nbsp;
                
                
                    <div id="lbl_ta_com" runat="server" />



                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="height: 17px">

                            <table>

                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>


                            <br />
                            <table width="90%">
                                <tr>
                                    <td bgcolor="aliceblue"><b>
                                        <asp:Label ID="Label59" runat="server"
                                            Text="<%$ Resources:Resource, lbl_u_term_and_condition %>"></asp:Label></b><br />
                                    </td>
                                </tr>
                            </table>

                            <br />
                        </td>
                    </tr>

                </table>


                <table width="100%">


                    <tr>
                        <td valign="top" style="width: 234px">
                            <b>
                                <asp:Label ID="Label60" runat="server"
                                    Text="<%$ Resources:Resource, lbl_form_of_payment %>"></asp:Label>
                            </b></td>
                        <td colspan="2">
                            <asp:Label ID="tt_form_of_payment" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="3">

                            <table width="100%">
                                <tr>
                                    <td style="color: #D3D3D3">_______________________________________________________________________________________</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 234px">
                            <b>
                                <asp:Label ID="Label61" runat="server"
                                    Text="<%$ Resources:Resource, lbl_nsf_fee %>" /></b>  &nbsp; </td>
                        <td colspan="2">
                            <asp:Label ID="tt_nsf" runat="server" Width="128px" /></td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="3">

                            <table width="100%">
                                <tr>
                                    <td style="color: #D3D3D3">_________________________________________________________________________________________</td>
                                </tr>
                            </table>
                        </td>


                    </tr>
                    <tr>
                        <td valign="top" style="width: 234px"><b>
                            <asp:Label ID="Label62" runat="server"
                                Text="<%$ Resources:Resource, lbl_late_fee %>" /></b></td>
                        <td>
                            <asp:Label ID="tt_late_fee" runat="server" Width="128px" />
                        </td>


                    </tr>
                    <tr>
                        <td valign="top" colspan="3">

                            <table width="100%">
                                <tr>
                                    <td style="color: #D3D3D3">________________________________________________________________________________________</td>
                                </tr>
                            </table>
                        </td>
                    </tr>





                    <tr>
                        <td valign="top" style="width: 234px"><b>
                            <asp:Label ID="Label63" runat="server"
                                Text="<%$ Resources:Resource, lbl_security_deposit_required %>" /></b></td>
                        <td>
                            <asp:RadioButtonList ID="tt_security_deposit" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Text="No" Value="0"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                            </asp:RadioButtonList>

                            <asp:Panel ID="Panel_security_deposit" runat="server">
                                &nbsp;&nbsp;&nbsp;<asp:Label ID="Label64" runat="server"
                                    Text="<%$ Resources:Resource, lbl_amount %>"></asp:Label>&nbsp;:
                                        <asp:Label ID="tt_security_deposit_amount" runat="server" />
                            </asp:Panel>



                        </td>
                    </tr>




                    <tr>
                        <td valign="top" colspan="3">

                            <table width="100%">
                                <tr>
                                    <td style="color: #D3D3D3">________________________________________________________________________________________</td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td valign="top" style="width: 234px"><b>
                            <asp:Label ID="Label65" runat="server"
                                Text="<%$ Resources:Resource, lbl_guarantor_required %>"></asp:Label></b> </td>
                        <td>
                            <asp:RadioButtonList ID="tt_guarantor" runat="server"
                                AutoPostBack="True" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Text="No" Value="0"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:Panel ID="panel_guarantor" runat="server" Visible="False">
                                <b>&nbsp; name</b>&nbsp;: 
                <asp:Label ID="guarantor_name" runat="server"></asp:Label>
                                <br />

                                <!--  BEGIN GUARANTOR INFORMATIONS    -->

                                <!-- END GUARANTOR INFORMATIONS      -->
                            </asp:Panel>
                        </td>
                    </tr>



                    <tr>
                        <td valign="top" colspan="3">

                            <table width="90%">
                                <tr>
                                    <td style="color: #D3D3D3">________________________________________________________________________________________</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 234px"><b>
                            <asp:Label ID="Label66" runat="server"
                                Text="<%$ Resources:Resource, lbl_pets_allowed %>"></asp:Label></b></td>
                        <td>
                            <asp:RadioButtonList ID="tt_pets" runat="server"
                                RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Text="No" Value="0"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td valign="top" colspan="3">

                            <table width="100%">
                                <tr>
                                    <td style="color: #D3D3D3">________________________________________________________________________________________</td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                    <tr valign="top">
                        <td valign="top" style="width: 234px"><b>
                            <asp:Label ID="Label68" runat="server"
                                Text="<%$ Resources:Resource, lbl_tenant_maintenance %>"></asp:Label></b></td>
                        <td>
                            <asp:RadioButtonList ID="tt_maintenance" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Text="No" Value="0"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                            </asp:RadioButtonList>
                            <br />
                            &nbsp;&nbsp;&nbsp;<b><asp:Label ID="Label67" runat="server"
                                Text="<%$ Resources:Resource, lbl_maintenance_allowed %>"></asp:Label></b><br />
                            &nbsp;<asp:Label runat="server"
                                ID="tt_specify_maintenance" Width="300px" />


                        </td>

                    </tr>
                    <tr>
                        <td valign="top" colspan="3">

                            <table width="90%">
                                <tr>
                                    <td style="color: #D3D3D3">________________________________________________________________________________________</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 234px"><b>
                            <asp:Label ID="Label70" runat="server"
                                Text="<%$ Resources:Resource, lbl_tenant_improvement %>"></asp:Label></b></td>
                        <td>
                            <asp:RadioButtonList ID="tt_improvement" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Text="No" Value="0"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:Panel ID="panel_specify_improvement" runat="server">
                                &nbsp;&nbsp;&nbsp;<b><asp:Label ID="Label69" runat="server"
                                    Text="<%$ Resources:Resource, lbl_improvement_allowed %>"></asp:Label></b><br />
                                &nbsp;<asp:Label runat="server" Width="300px" ID="tt_specify_improvement" />
                            </asp:Panel>

                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="3">

                            <table width="90%">
                                <tr>
                                    <td style="color: #D3D3D3">________________________________________________________________________________________</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="width: 234px"><b>
                            <asp:Label ID="Label71" runat="server"
                                Text="<%$ Resources:Resource, lbl_notice_to_enter %>"></asp:Label></b> </td>
                        <td>
                            <asp:RadioButtonList ID="tt_notice_to_enter" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Selected="True" Text="No" Value="0"></asp:ListItem>
                                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                            </asp:RadioButtonList>
                            <br />
                            &nbsp;<b><asp:Label ID="Label72" runat="server"
                                Text="<%$ Resources:Resource, lbl_number_of_hours_notice %>"></asp:Label></b>&nbsp;:
                                    <asp:Label ID="tt_specify_number_of_hours"
                                        runat="server" Width="100px" />

                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="3">

                            <table width="90%">
                                <tr>
                                    <td style="color: #D3D3D3">________________________________________________________________________________________</td>
                                </tr>
                            </table>
                        </td>

                    </tr>



                    <tr>
                        <td valign="top" style="width: 234px"><b>
                            <asp:Label ID="Label73" runat="server"
                                Text="<%$ Resources:Resource, lbl_who_pay_insurances %>"></asp:Label></b></td>

                        <td>
                            <table>
                                <tr>
                                    <td>
                                        <b>
                                            <asp:Label ID="Label74" runat="server"
                                                Text="<%$ Resources:Resource, lbl_tenant_content %>"></asp:Label></b></td>
                                    <td valign="top">:
                        <asp:Label ID="tt_tenant_content_ins" runat="server"></asp:Label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>
                                            <asp:Label ID="Label75" runat="server"
                                                Text="<%$ Resources:Resource, lbl_landlord_content %>"></asp:Label></b></td>
                                    <td valign="top">:<asp:Label ID="tt_landlord_content_ins" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>
                                            <asp:Label ID="Label76" runat="server"
                                                Text="<%$ Resources:Resource, lbl_personal_injury %>"></asp:Label></b></td>
                                    <td valign="top">:<asp:Label ID="tt_injury_ins" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>
                                            <asp:Label ID="Label77" runat="server"
                                                Text="<%$ Resources:Resource, lbl_lease_premises %>"></asp:Label></b></td>
                                    <td valign="top">:
                        <asp:Label ID="tt_premises_ins" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>



                    <tr>
                        <td valign="top" colspan="3">

                            <table width="90%">
                                <tr>
                                    <td style="color: #D3D3D3">________________________________________________________________________________________</td>
                                </tr>
                            </table>
                        </td>
                    </tr>



                    <tr>
                        <td valign="top" style="width: 234px">
                            <b>
                                <asp:Label ID="Label78" runat="server"
                                    Text="<%$ Resources:Resource, lbl_additional_terms %>"></asp:Label></b></td>
                        <td>

                            <div id="tt_additional_terms" runat="server" />

                            <br />
                            <br />
                        </td>
                        <td valign="top">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3" valign="top">
                            <table cellpadding="0" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td valign="top">
                                        <b>
                                            <asp:Label ID="Label79" runat="server"
                                                Text="<%$ Resources:Resource, lbl_terms_since %>"></asp:Label>
                                            : </b>

                                        <asp:Label ID="lbl_current_tt_date_begin" runat="server" Style="color: #FF3300"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td valign="top">&nbsp;
          
          
     <asp:GridView Width="100%"
         AlternatingRowStyle-BackColor="#F0F0F6"
         BorderColor="#CDCDCD" BorderWidth="1"
         HeaderStyle-BackColor="#F0F0F6"
         AutoGenerateColumns="False" ID="gv_terms_list" runat="server">

         <Columns>

             <asp:BoundField DataField="tt_date_begin" DataFormatString="{0:MM-dd-yyyy}"
                 HeaderText="<%$ Resources:Resource, lbl_previous_date_begin %>" HtmlEncode="False" />
             <asp:BoundField DataField="tt_date_end" DataFormatString="{0:MM-dd-yyyy}"
                 HeaderText="<%$ Resources:Resource, lbl_previous_date_end %>" HtmlEncode="False" />
             <asp:BoundField DataField="tt_id" HeaderText="ta_id" />

             <asp:HyperLinkField Text="<%$ Resources:Resource, lbl_view %>"
                 DataNavigateUrlFields="tu_id,tt_id"
                 DataNavigateUrlFormatString="~/manager/lease/lease_termsandconditions_archive_view.aspx?tu_id={0}&tt_id={1}"
                 HeaderText="<%$ Resources:Resource, lbl_view %>" />

         </Columns>
     </asp:GridView>



                                    </td>
                                </tr>
                            </table>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 234px">
                            <div id="txt_link" runat="server" />
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>



    <br />
    <br />
    <hr />
    <br />
    <br />




    <h1><%= Resources.Resource.lbl_lease_archive %></h1>


    <table width="100%">
        <tr>
            <td bgcolor="aliceblue">
                <b>
                    <asp:Label ID="Label48" runat="server" Text="<%$ Resources:Resource, lbl_lease_archive%>" /></b>
            </td>
        </tr>
    </table>
    <br />
    <asp:GridView ID="gv_tenant_lease_archives" runat="server" AllowSorting="true"
        AutoGenerateColumns="false"
        AlternatingRowStyle-BackColor="#F0F0F6"
        BorderColor="#CDCDCD" BorderWidth="1" Width="100%"
        HeaderStyle-BackColor="#F0F0F6"
        EmptyDataText="NONE" GridLines="Both">
        <Columns>
            <asp:BoundField DataField="home_name" HeaderText="<%$ Resources:Resource, lbl_property%>" />
            <asp:BoundField DataField="unit_door_no" HeaderText="<%$ Resources:Resource, lbl_door_number%>" />
            <asp:TemplateField HeaderText="Lease type">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lbl_lease_type" Text='<%#GetLeaseType(Convert.ToString(Eval("unit_type")))%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="tu_date_begin" DataFormatString="{0:M-dd-yyyy}"
                HeaderText="<%$ Resources:Resource, gv_lease_date_begin%>" />
            <asp:BoundField DataField="tu_date_end" DataFormatString="{0:M-dd-yyyy}"
                HeaderText="<%$ Resources:Resource, lbl_lease_date_end%>" />
            <asp:HyperLinkField DataNavigateUrlFields="tu_id, tenant_id"
                DataNavigateUrlFormatString="~/manager/lease/lease_archive_view.aspx?tu_id={0}&t_id={1}"
                Text="<%$ Resources:Resource, lbl_view%>" />
        </Columns>
    </asp:GridView>


    <br />
    <br />
    <hr />
    <br />
    <br />




   <h1> <%= Resources.Resource.lbl_request_history %></h1>
        

            <table width="100%">
                <tr>
                    <td bgcolor="aliceblue">
                        <b>
                            <asp:Label ID="Label49" runat="server" Text="<%$ Resources:Resource, lbl_u_request_history%>" /></b>
                    </td>
                </tr>
            </table>
            request 1 request 2 ... 2nnnnnn<br />



     <br /> <br />
    <hr />
    <br /> <br />


    <h1><%= Resources.Resource.lbl_incident_history %></h1>
        


            <table width="100%">
                <tr>
                    <td bgcolor="aliceblue">
                        <b>
                            <asp:Label ID="Label50" runat="server" Text="<%$ Resources:Resource, lbl_u_incident_history%>" /></b>
                    </td>
                </tr>
            </table>
            <br />
            <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="83%" BorderColor="White" BorderWidth="3" ID="gv_incident_list" runat="server" AutoGenerateColumns="false"
                AllowSorting="true" GridLines="Both" AllowPaging="true" PageSize="10"
                OnPageIndexChanging="gv_incident_list_PageIndexChanging" AlternatingRowStyle-BackColor="Beige">

                <Columns>
                    <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_title %>" DataField="incident_title" />
                    <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_property %>" DataField="home_name" />
                    <asp:BoundField HeaderText="<%$ Resources:Resource,gv_unit %>" DataField="unit_door_no" />
                    <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_date %>" DataField="incident_date" DataFormatString="{0:MMM-dd-yyyy}"
                        HtmlEncode="false" />

                    <asp:HyperLinkField Text="<%$ Resources:Resource,lbl_view %>"
                        DataNavigateUrlFields="incident_id"
                        DataNavigateUrlFormatString="~/manager/incident/incident_view.aspx?inc_id={0}"
                        HeaderText="<%$ Resources:Resource,lbl_view %>" />


                    <asp:TemplateField HeaderText="<%$ Resources:Resource,lbl_update %>">
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink1" Text="<%$ Resources:Resource, lbl_update %>" runat="server" NavigateUrl='<%#"~/manager/incident/incident_update.aspx?inc_id="+DataBinder.Eval(Container.DataItem,"incident_id")%>'></asp:HyperLink>

                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>


            </asp:GridView>
            <br />


 
</asp:Content>

