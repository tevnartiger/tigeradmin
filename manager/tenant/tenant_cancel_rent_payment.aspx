﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="tenant_cancel_rent_payment.aspx.cs" Inherits="manager_tenant_tenant_cancel_rent_payment" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div>
                &nbsp;&nbsp;<asp:Label ID="Label1" runat="server" Text="Edit Rent Payment" 
                    style="font-size: small; font-weight: 700"></asp:Label>
        <br /><br />
        <table >
            <tr>
                <td>
      <asp:Label ID="lbl_property" runat="server" Text="<%$ Resources:Resource, lbl_property %>"/>
                </td>
                <td>
       <asp:DropDownList ID="ddl_home_id" DataValueField="home_id" DataTextField="home_name"   runat="server" autopostback="true" OnSelectedIndexChanged="ddl_home_id_SelectedIndexChanged" />
    
    
    
    
                </td>
                <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;</td>
                <td>
      &nbsp;<asp:Label ID="lbl_from" runat="server" Text="<%$ Resources:Resource, lbl_from %>"/>
                </td>
                <td>
                    <asp:DropDownList ID="ddl_from_m" runat="server">
                    <asp:ListItem Text="<%$ Resources:Resource, txt_month %>" Value="0"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
&nbsp;/
                    <asp:DropDownList ID="ddl_from_d" runat="server">
                         <asp:ListItem Text="<%$ Resources:Resource, txt_day %>" Value="0"></asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
&nbsp;/
                    <asp:DropDownList ID="ddl_from_y" runat="server">
                    <asp:ListItem Value="0">Year</asp:ListItem>
                    <asp:ListItem>1980</asp:ListItem>
                    <asp:ListItem>1981</asp:ListItem>
                    <asp:ListItem>1982</asp:ListItem>
                    <asp:ListItem>1983</asp:ListItem>
                    <asp:ListItem>1984</asp:ListItem>
                    <asp:ListItem>1985</asp:ListItem>
                    <asp:ListItem>1986</asp:ListItem>
                    <asp:ListItem>1987</asp:ListItem>
                    <asp:ListItem>1988</asp:ListItem>
                    <asp:ListItem>1989</asp:ListItem>
                    <asp:ListItem>1990</asp:ListItem>
                    <asp:ListItem>1991</asp:ListItem>
                    <asp:ListItem>1992</asp:ListItem>
                    <asp:ListItem>1993</asp:ListItem>
                    <asp:ListItem>1994</asp:ListItem>
                    <asp:ListItem>1995</asp:ListItem>
                    <asp:ListItem>1996</asp:ListItem>
                    <asp:ListItem>1997</asp:ListItem>
                    <asp:ListItem>1998</asp:ListItem>
                    <asp:ListItem>1999</asp:ListItem>                    
                    <asp:ListItem>2000</asp:ListItem>
                    <asp:ListItem>2001</asp:ListItem>
                    <asp:ListItem>2002</asp:ListItem>
                    <asp:ListItem>2003</asp:ListItem>
                    <asp:ListItem>2004</asp:ListItem>
                    <asp:ListItem>2005</asp:ListItem>
                    <asp:ListItem>2006</asp:ListItem>
                    <asp:ListItem>2007</asp:ListItem>
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                    <asp:ListItem>2011</asp:ListItem>
                    <asp:ListItem>2012</asp:ListItem>
                    <asp:ListItem>2013</asp:ListItem>
                    <asp:ListItem>2014</asp:ListItem>
                    <asp:ListItem>2015</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
      <asp:Label ID="lbl_unit" runat="server" Text="<%$ Resources:Resource, lbl_unit %>"/>
                </td>
                <td>
        <asp:DropDownList ID="ddl_unit_id" DataValueField="unit_id" DataTextField="unit_door_no"   runat="server" OnSelectedIndexChanged="ddl_unit_id_SelectedIndexChanged" AutoPostBack="true" />
    
    
    
    
                </td>
                <td>
                    &nbsp;</td>
                <td>
      <asp:Label ID="lbl_to" runat="server" Text="<%$ Resources:Resource, lbl_to %>"/>
                </td>
                <td>
                    <asp:DropDownList ID="ddl_to_m" runat="server">
                    
                      <asp:ListItem Text="<%$ Resources:Resource, txt_month %>" Value="0"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
&nbsp;/
                    <asp:DropDownList ID="ddl_to_d" runat="server">
                        <asp:ListItem Value="0">Day</asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
&nbsp;/
                    <asp:DropDownList ID="ddl_to_y" runat="server">
                    <asp:ListItem Value="0">Year</asp:ListItem>
                    <asp:ListItem>1980</asp:ListItem>
                    <asp:ListItem>1981</asp:ListItem>
                    <asp:ListItem>1982</asp:ListItem>
                    <asp:ListItem>1983</asp:ListItem>
                    <asp:ListItem>1984</asp:ListItem>
                    <asp:ListItem>1985</asp:ListItem>
                    <asp:ListItem>1986</asp:ListItem>
                    <asp:ListItem>1987</asp:ListItem>
                    <asp:ListItem>1988</asp:ListItem>
                    <asp:ListItem>1989</asp:ListItem>
                    <asp:ListItem>1990</asp:ListItem>
                    <asp:ListItem>1991</asp:ListItem>
                    <asp:ListItem>1992</asp:ListItem>
                    <asp:ListItem>1993</asp:ListItem>
                    <asp:ListItem>1994</asp:ListItem>
                    <asp:ListItem>1995</asp:ListItem>
                    <asp:ListItem>1996</asp:ListItem>
                    <asp:ListItem>1997</asp:ListItem>
                    <asp:ListItem>1998</asp:ListItem>
                    <asp:ListItem>1999</asp:ListItem>                    
                    <asp:ListItem>2000</asp:ListItem>
                    <asp:ListItem>2001</asp:ListItem>
                    <asp:ListItem>2002</asp:ListItem>
                    <asp:ListItem>2003</asp:ListItem>
                    <asp:ListItem>2004</asp:ListItem>
                    <asp:ListItem>2005</asp:ListItem>
                    <asp:ListItem>2006</asp:ListItem>
                    <asp:ListItem>2007</asp:ListItem>
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                    <asp:ListItem>2011</asp:ListItem>
                    <asp:ListItem>2012</asp:ListItem>
                    <asp:ListItem>2013</asp:ListItem>
                    <asp:ListItem>2014</asp:ListItem>
                    <asp:ListItem>2015</asp:ListItem>
                    </asp:DropDownList>
                </td>
                            <td>
&nbsp;&nbsp;
                                <asp:Button ID="Button1" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_view %>" onclick="Button1_Click"/>
                            </td>
            </tr>
        </table>
        <br />
                <br />
                <asp:Label ID="lbl_confirmation" runat="server" ForeColor="Red"></asp:Label>
                <br />
                <asp:Label ID="Label2" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label3" runat="server"></asp:Label>
&nbsp;<asp:Label ID="lbl_unit_door_no" runat="server"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label4" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label5" runat="server"></asp:Label>
&nbsp;<asp:Label ID="lbl_amount" runat="server"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label6" runat="server"></asp:Label>
&nbsp;<asp:Label ID="Label7" runat="server"></asp:Label>
&nbsp;<asp:Label ID="lbl_date_paid" runat="server"></asp:Label>
&nbsp;<br />
    <br />
    
    
   <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="83%" BorderColor="White" BorderWidth="3"  ID="gv_rent_payment_archive" runat="server" AutoGenerateColumns="false"
       AllowSorting="true"  AllowPaging="true" PageSize="10"
      OnPageIndexChanging="gv_rent_payment_archive_PageIndexChanging"  >
        
     <Columns>
   <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_home_name %>" DataField="home_name"   />
   <asp:BoundField HeaderText="<%$ Resources:Resource,gv_unit %>" DataField="unit_door_no"   />
   <asp:BoundField HeaderText="<%$ Resources:Resource,gv_rent %>"   DataField="rl_rent_amount" DataFormatString="{0:0.00}"  />
   <asp:BoundField HeaderText="<%$ Resources:Resource,gv_amount_paid %>"   DataField="rp_amount" DataFormatString="{0:0.00}"  />
   <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_due_date %>"  DataField="rp_due_date" DataFormatString="{0:M-dd-yyyy}"  
     HtmlEncode="false" />
   <asp:BoundField HeaderText="<%$ Resources:Resource,lbl_date_received %>"  DataField="rp_paid_date" DataFormatString="{0:M-dd-yyyy}"  
     HtmlEncode="false" />  
       <asp:TemplateField >
    <ItemTemplate  >
   <asp:HyperLink ID="HyperLink1"  Text="<%$ Resources:Resource, lbl_update %>"  runat="server" NavigateUrl='<%#"tenant_rent_update.aspx?rpp_id="+DataBinder.Eval(Container.DataItem,"rp_id")+"&h_id="+DataBinder.Eval(Container.DataItem,"home_id")+GetTrasnl()%>'></asp:HyperLink>
     
  </ItemTemplate  >
    </asp:TemplateField>
      
        <asp:TemplateField >
    <ItemTemplate  >
    <asp:HiddenField ID="h_unit_door_no" Value='<%#Bind("unit_door_no")%>'  runat="server"   />
   <asp:HiddenField  ID="h_rp_amount" Value='<%#Bind("rp_amount","{0:0.00}" )%>'  runat="server" />
   
   <asp:HiddenField ID="h_rp_paid_date" Value='<%#Bind("rp_paid_date","{0:M-dd-yyyy}" )%>' runat="server" 
      />
    <asp:HiddenField ID="h_rp_id" Value='<%#Bind("rp_id")%>'  runat="server" />
   <asp:Button ID="btn_cancel" runat="server" OnClick="btn_cancel_Click"  Text="<%$ Resources:Resource, btn_cancel %>"/>
    </ItemTemplate  >
    </asp:TemplateField>
    
    
    
    
    
      </Columns>   
        
        
    </asp:GridView>
    </div>  <br />
    
    
    
   
    
    
    <asp:HiddenField ID="h_rp_id2"  runat="server" />
  

</asp:Content>

