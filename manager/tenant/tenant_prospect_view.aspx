<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="tenant_prospect_view.aspx.cs" Inherits="tenant_tenant_prospect_view" Title="Prospective tenant view" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div>
        TENANT - VIEW<br /><asp:HyperLink ID="tenantapplication_add_link" 
            runat="server">add </asp:HyperLink> -&nbsp;  <asp:HyperLink ID="tenantapplication_update_link"
            runat="server">update</asp:HyperLink> - delete mortgage
        &nbsp;<br />
        <br />
        <asp:Button ID="btn_delete" runat="server" Text="Reject prospect" 
            onclick="btn_delete_Click" />
&nbsp;
        <asp:Button ID="btn_insert" runat="server" Text="Accept prospect" 
            onclick="btn_insert_Click" />
        &nbsp; ( insert prospect into the system )<br />
        <br />
    
     <table border="0" cellpadding="1" cellspacing="3" >
            <tr><td class="letter_bold" style="font-weight: bold">first name</td>
                <td><asp:Label ID="lbl_name_fname" CssClass="letter" runat="server"/></td>
                <td class="letter_bold" style="font-weight: bold">last name</td>
                <td><asp:Label ID="lbl_name_lname" CssClass="letter" runat="server"/></td>
            </tr>
            
            <tr><td  class="letter_bold" style="font-weight: bold">date of birth (mm / dd /yyyy )</td>
                
                <td colspan ="2" ><asp:Label ID="lbl_name_dob" CssClass="letter" runat="server"/></td>
            </tr>
            <tr><td class="letter_bold" style="font-weight: bold">address</td>
                <td><asp:Label ID="lbl_name_addr" CssClass="letter" runat="server"></asp:Label></td>
                <td class="letter_bold" style="font-weight: bold">City</td>
                <td><asp:Label ID="lbl_name_addr_city" CssClass="letter" runat="server"/></td>
            </tr>
            <tr><td class="letter_bold" style="font-weight: bold">Pc</td>
                <td><asp:Label ID="lbl_name_addr_pc" CssClass="letter" runat="server"/></td>
                <td class="letter_bold" style="font-weight: bold">State/Prov</td>
                <td>
                    <asp:Label ID="lbl_name_addr_state" CssClass="letter" runat="server"/></td>
            </tr>
         <tr>
             <td class="letter_bold" style="font-weight: bold">
                 Current employer</td><td>
                 <asp:Label ID="lbl_tenantapplication_current_employer" runat="server"></asp:Label></td><td>
                 <b>Monthly Income</b></td><td>
                 <asp:Label ID="lbl_tenantapplication_monthly_income" runat="server"></asp:Label></td>
         </tr>
         <tr>
             <td class="letter_bold" style="font-weight: bold">
                 Current employer since ( mm / dd / yyyy )</td>
             <td colspan="3">
             <asp:Label ID="lbl_tenantapplication_employer_since" runat="server"></asp:Label>
                 </td>
         </tr>
         <tr>
             <td class="letter_bold" valign="top" style="font-weight: bold">
                 People who will live in the property with you</td>
             <td colspan="3">&nbsp;
                 <asp:Label ID="lbl_tenantapplication_people_with_prospect" runat="server" Width="350px" Height="50px" TextMode="MultiLine"></asp:Label></td>
         </tr>
         <tr>
             <td class="letter_bold" style="font-weight: bold">
                 Current landlord`s name</td>
             <td colspan="3">
                 <asp:Label ID="lbl_tenantapplication_landlord_name" runat="server"></asp:Label></td>
         </tr>
         <tr>
             <td class="letter_bold" style="font-weight: bold">
                 Current landlord &nbsp;telephone</td>
             <td colspan="3">
                 <asp:Label ID="lbl_tenantapplication_landlord_tel" runat="server"></asp:Label></td>
         </tr>
         <tr>
             <td class="letter_bold" valign="top" style="font-weight: bold">
                 Reason of departure</td>
             <td colspan="3">&nbsp;
                 <asp:Label ID="lbl_tenantapplication_reason_depart"  runat="server" Height="50px" TextMode="MultiLine" Width="350px"></asp:Label></td>
         </tr>
         <tr>
             <td class="letter_bold" style="font-weight: bold">
                 SSN</td>
             <td colspan="3">
                 <asp:Label ID="lbl_name_ssn" runat="server"></asp:Label></td>
         </tr>
            <tr><td  class="letter_bold" style="font-weight: bold">Country</td>
                <td colspan="3"><asp:Label ID="lbl_country_name" CssClass="letter" runat="server" /></td>
            </tr>
            <tr><td class="letter_bold" style="font-weight: bold">Telephone</td>
                <td><asp:Label ID="lbl_name_tel" CssClass="letter" runat="server"/></td>
            <td class="letter_bold" style="font-weight: bold">Tel. Work</td>
                <td><asp:Label ID="lbl_name_tel_work" CssClass="letter" runat="server"/></td>
            </tr>
            <tr><td class="letter_bold" style="font-weight: bold">Tel. Work ext.</td>
                <td><asp:Label ID="lbl_name_tel_work_ext" CssClass="letter" runat="server"/></td>
		<td class="letter_bold" style="font-weight: bold">Cell</td>
                <td><asp:Label ID="lbl_name_cell" CssClass="letter" runat="server"/></td>
            </tr>
            <tr><td class="letter_bold" style="font-weight: bold">Fax</td>
                <td><asp:Label ID="lbl_name_fax" CssClass="letter" runat="server"/></td>
                <td class="letter_bold" style="font-weight: 700">Email</td>
                <td><asp:Label ID="lbl_name_email" CssClass="letter" runat="server"/></td>
            </tr>
            <tr><td class="letter_bold" valign="top" style="font-weight: bold">Comments</td>
                <td colspan="3"><asp:Label ID="lbl_name_com" CssClass="letter" runat="server" TextMode="MultiLine" Width="389px"/></td>
            </tr>
        </table>
        <br />
        <hr />
        <br />
        <span style="font-size: small; font-weight: bold">LOOKING FOR</span><br />
        <br />
        <table border="0" cellpadding="1" cellspacing="3" >
            <tr>
                <td class="letter_bold" style="font-weight: bold" >
                    Minimum price</td>
                <td><asp:Label ID="lbl_pt_rent_min" runat="server">
                     </asp:Label></td>
                <td class="letter_bold" style="font-weight: bold">
                    Maximum price</td>
                <td><asp:Label ID="lbl_pt_rent_max" runat="server">
                  </asp:Label></td>
            </tr>
            <tr>
                <td class="letter_bold" style="font-weight: bold">
                    Bedrooms</td>
                <td>
                    <asp:Label ID="lbl_pt_bedroom_no" runat="server">
                    </asp:Label></td>
                <td class="letter_bold" style="font-weight: bold">
                    Bathrooms</td>
                <td>
                    <asp:Label ID="lbl_pt_bathroom_no" runat="server">
                    </asp:Label></td>
            </tr>
            <tr>
                <td class="letter_bold" style="font-weight: bold">
                    Min. square foot</td>
                <td colspan="3">
                    <asp:Label ID="lbl_pt_min_sqft" runat="server">
                    </asp:Label></td>
            </tr>
            <tr>
                <td class="letter_bold" style="font-weight: bold">
                    Begining date (mm/dd/yyyy )
                </td>
                <td colspan="3"><asp:Label ID="lbl_pt_date_begin" runat="server" Width="57px"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td>
                 <strong><span >Utilities</span></strong><br />
     <asp:CheckBox  CssClass="letter" ID="pt_electricity_inc" text="Electricity" runat="server"  />
     <br />
     <asp:CheckBox  CssClass="letter" ID="pt_heat_inc" text="Heat" runat="server"  />
     <br />
     <asp:CheckBox CssClass="letter" ID="pt_water_inc" text="Water" runat="server"  />
     <br />
     <asp:CheckBox  CssClass="letter" ID="pt_water_tax_inc" text="Water tax" runat="server"  />
     
     </td><td></td><td valign=top >
       <span ><strong>Accomodations</strong></span><br />
     <asp:CheckBox CssClass="letter" ID="pt_parking_inc" text="Parking" runat="server" />
     <br />
     <asp:CheckBox CssClass="letter" ID="pt_garage_inc" text="Garage" runat="server" />
     <br />
     <asp:CheckBox CssClass="letter" ID="pt_furnished_inc" text="Furnished" runat="server" />
     <br />
     <asp:CheckBox CssClass="letter" ID="pt_semi_furnished_inc" text="Semi-furnished" runat="server" />
     <br />
                </td>
            </tr>
            <tr>
                <td class="letter_bold" >
                    Comments</td>
                <td colspan="3">
                    <asp:Label ID="pt_com" CssClass="letter" runat="server" TextMode="MultiLine" Width="389px"/></td>
            </tr>
        </table>
      
        </div>
  

</asp:Content>