﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.AccountObjectAuthorization;

/// <summary>
/// Created by : Stanley Jocelyn
/// date       : jan 21, 2008
/// </summary>

public partial class tenant_tenant_untreated_rent : BasePage
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        AccountObjectAuthorization homeAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        if (!Page.IsPostBack)
        {
            DateTime right_now = new DateTime();
            right_now = DateTime.Now;
           
            tiger.Home l = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getHomeCount(Convert.ToInt32(Session["schema_id"]));

            // first we check if there's any property available
            if (home_count > 0)
            {
                int home_id ;

                
                tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ddl_home_list.DataSource = h.getHomeUntreatedRentList(Convert.ToInt32(Session["schema_id"]));
                ddl_home_list.DataBind();

              if (Request.UrlReferrer.ToString().Contains("alerts.aspx"))
                //********************************************************************
                if (Request.QueryString["h_id"] != null )
                {
                    if (!RegEx.IsInteger(Request.QueryString["h_id"]))
                    {
                        Session.Abandon();
                        Response.Redirect("~/login.aspx");
                    }

                    ddl_home_list.SelectedValue = Convert.ToString(Request.QueryString["h_id"]);
                    home_id = Convert.ToInt32(Request.QueryString["h_id"]);
                }
                //********************************************************************


                if (ddl_home_list.Items.Count > 0)
                {
                    // dropdownlist rent frequency
                    tiger.Home v = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    ddl_rent_frequency.DataSource = v.getHomeUnitRentedSumListArchives(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_list.SelectedValue));
                    ddl_rent_frequency.DataBind();
                }
               
                int rent_frequency = 0;

                if(ddl_rent_frequency.Items.Count > 0)
                {
                    rent_frequency = Convert.ToInt32(ddl_rent_frequency.SelectedItem.Value);
                }
                

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                if (ddl_home_list.Items.Count > 0)
                {
                    // here we get the list of untreated rented unit ( the chlid gridview )
                    tiger.Home uv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    gv_parent_nested_unpaid_rent_month.DataSource = uv.getHomeUnitRentedListArchives(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_list.SelectedValue), rent_frequency);
                    gv_parent_nested_unpaid_rent_month.DataBind();
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                //To view the address of the property

                
            }

            else
            {
                // panel_home_unit_add.Visible = false;
                txt_message.InnerHtml = "<b><a href='home/home_add.aspx'>Please add a property</a></b>";
            }

        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_list_SelectedIndexChanged(object sender, EventArgs e)
    {

        DateTime the_date = new DateTime();
        DateTime right_now = new DateTime();
        right_now = DateTime.Now;

       // Label8.Text = Request.Form["ddl_date_received_m"] + "/" + Request.Form["ddl_date_received_d"] + "/" + Request.Form["ddl_date_received_y"];
        int home_id = Convert.ToInt32(ddl_home_list.SelectedValue);


      

        // dropdownlist rent frequency
        tiger.Home v = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        ddl_rent_frequency.DataSource = v.getHomeUnitRentedSumListArchives(Convert.ToInt32(Session["schema_id"]), home_id);
        ddl_rent_frequency.DataBind();

        // here we check if there's any unit in this property
        tiger.Unit uc = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        int unit_count = uc.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

        if (unit_count > 0)
        // here we get the list of unit that were already added 
        {
            int rent_frequency = Convert.ToInt32(ddl_rent_frequency.SelectedItem.Value);
            tiger.Home u = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            // here we get the list of untreated rented unit ( the chlid gridview )
            tiger.Home uv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_parent_nested_unpaid_rent_month.DataSource = uv.getHomeUnitRentedListArchives(Convert.ToInt32(Session["schema_id"]), home_id, rent_frequency);
            gv_parent_nested_unpaid_rent_month.DataBind();

        }
        else
        {
           
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_rent_frequency_SelectedIndexChanged(object sender, EventArgs e)
    {
        DateTime right_now = new DateTime();
        right_now = DateTime.Now;
     
        int home_id = Convert.ToInt32(ddl_home_list.SelectedValue);
        int rent_frequency = Convert.ToInt32(ddl_rent_frequency.SelectedValue);

        // here we get the list of untreated rented unit ( the chlid gridview )
        tiger.Home uv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_parent_nested_unpaid_rent_month.DataSource = uv.getHomeUnitRentedListArchives(Convert.ToInt32(Session["schema_id"]), home_id, rent_frequency);
        gv_parent_nested_unpaid_rent_month.DataBind();

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="tu_id"></param>
    /// <returns></returns>
    protected DataSet GetTrasnl(int tu_id,int rl_id)
    {
        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prRentUntreatedList", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        DateTime the_date = new DateTime();
        DateTime right_now = new DateTime();
        right_now = DateTime.Now;

       //   the_date= Convert.ToDateTime( ddl_date_received_m.SelectedValue  +"/"+ ddl_date_received_d.SelectedValue  +"/"+ ddl_date_received_y.SelectedValue) ;
      //  Label7.Text = Convert.ToString( the_date);
        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;
            cmd.Parameters.Add("@rl_id", SqlDbType.Int).Value = rl_id;
            cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = right_now; 

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="thedate"></param>
    /// <returns></returns>
    protected string Get_UntreatedRent(DateTime thedate)
    {
        return thedate.Month.ToString() + "/" + thedate.Day.ToString()+"/"+thedate.Year.ToString();;
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="thedate"></param>
    /// <returns></returns>
    protected string Get_UntreatedRent_Month(DateTime thedate)
    {
        return thedate.Month.ToString();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="thedate"></param>
    /// <returns></returns>
    protected string Get_UntreatedRent_Day(DateTime thedate)
    {
        return thedate.Day.ToString();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="thedate"></param>
    /// <returns></returns>
    protected string Get_UntreatedRent_Year(DateTime thedate)
    {
        return thedate.Year.ToString();
    }


    /// <summary>
    /// This method erase empty gridview row within "gv_parent_nested_unpaid_rent_month"
    /// i.e. : "gv_child_nested_unpaid_rent_month" is empty
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gv_parent_nested_unpaid_rent_month_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        int count = 0;
       
        for (int i = 0; i < gv_parent_nested_unpaid_rent_month.Rows.Count; i++)
        {
            // create an instance of a GridRow and get a row of the parent GridView
            GridViewRow row = gv_parent_nested_unpaid_rent_month.Rows[i];

            //From that row we get the child GridView and count the number of row in the nested GridView

            GridView grv = row.Cells[1].FindControl("gv_child_nested_unpaid_rent_month") as GridView;
            if (grv.Rows.Count == 0)
            {
                row.Visible = false;
            }
            else
                count++;
        }

        if (count == 0)
            gv_parent_nested_unpaid_rent_month.Visible = false;
        else
            gv_parent_nested_unpaid_rent_month.Visible = true;


      Label1.Text =  gv_parent_nested_unpaid_rent_month.Rows.Count.ToString();
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        Session["home_id"] = ddl_home_list.SelectedValue;
        Session["rent_frequency"] = ddl_rent_frequency.SelectedValue;

        String compare_rl_rent_amount = "";
        String rp_amount = "";
        String rp_paid_date = "";
        String rp_due_date = "";
        String tu_id = "";
        String rl_id = "";
        String rp_notes = "";
        int number_of_insert = 0;
        String no_delinquent = "";


        // For each row of the parent GridView get the child nested GridView
        // here we are counting the number of row of the parent GridView
        for (int i = 0; i < gv_parent_nested_unpaid_rent_month.Rows.Count; i++)
        {
            // create an instance of a GridRow and get a row of the parent GridView
            GridViewRow row = gv_parent_nested_unpaid_rent_month.Rows[i];
            
            //From that row we get the child GridView and count the number of row in the nested GridView

            GridView grv = row.Cells[1].FindControl("gv_child_nested_unpaid_rent_month") as GridView;
            for (int j = 0; j < grv.Rows.Count; j++)
            {



                CheckBox chk_process = (CheckBox)grv.Rows[j].Cells[0].FindControl("chk_process");
                // if the checkbox is checked the process
                if (chk_process.Checked)
                {
                  TextBox tbx_rl_rent_amount = (TextBox)grv.Rows[j].Cells[2].FindControl("gvparent_child_textbox");
                 // Label rent_due_date = (Label)grv.Rows[j].Cells[2].FindControl("rent_due_date");
                  HiddenField h_rent_due_date = (HiddenField)grv.Rows[j].Cells[6].FindControl("h_rent_due_date");

                  DropDownList ddl_child_unpaid_rent_m = (DropDownList)grv.Rows[j].Cells[6].FindControl("ddl_child_unpaid_rent_m");
                  DropDownList ddl_child_unpaid_rent_d = (DropDownList)grv.Rows[j].Cells[6].FindControl("ddl_child_unpaid_rent_d");
                  DropDownList ddl_child_unpaid_rent_y = (DropDownList)grv.Rows[j].Cells[6].FindControl("ddl_child_unpaid_rent_y");

                  CheckBox chk_no_delequency = (CheckBox)grv.Rows[j].Cells[4].FindControl("chk_no_delequency");

                  TextBox tbx_notes = (TextBox)grv.Rows[j].Cells[6].FindControl("tbx_notes");

                  HiddenField h_tu_id = (HiddenField)grv.Rows[j].Cells[6].FindControl("h_tu_id");
                  HiddenField h_rl_rent_amount = (HiddenField)grv.Rows[j].Cells[6].FindControl("h_rl_rent_amount");
                  HiddenField h_rl_id = (HiddenField)grv.Rows[j].Cells[6].FindControl("h_rl_id");


                  // if h_tu_id or hd_rl_id are note integer logout
                  if (!RegEx.IsInteger(h_tu_id.Value) ||
                      !RegEx.IsInteger(h_rl_id.Value) ||
                      !RegEx.IsMoney(h_rl_rent_amount.Value))
                  {
                      Session.Abandon();
                      Response.Redirect("~/login.aspx");
                  }

                  if (RegEx.IsMoney(tbx_rl_rent_amount.Text))
                  {
                      rp_amount = rp_amount + tbx_rl_rent_amount.Text + "|";
                      compare_rl_rent_amount = compare_rl_rent_amount + h_rl_rent_amount.Value + "|";
                      rp_paid_date = rp_paid_date + ddl_child_unpaid_rent_m.SelectedValue + "/" + ddl_child_unpaid_rent_d.SelectedValue + "/" + ddl_child_unpaid_rent_y.SelectedValue + "|";
                      rp_due_date = rp_due_date + h_rent_due_date.Value+ "|";
                      tu_id = tu_id + h_tu_id.Value + "|";
                      rl_id = rl_id + h_rl_id.Value + "|";
                      rp_notes = rp_notes + RegEx.getText(tbx_notes.Text) + "|";

      
                      if (chk_no_delequency.Checked)
                          no_delinquent = no_delinquent + "1|";
                      else
                          no_delinquent = no_delinquent + "0|";

                      number_of_insert++;
                 }

              }


            }
        }
      

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        AccountObjectAuthorization rentAuthorization = new AccountObjectAuthorization(strconn);

        if (!rentAuthorization.RentPaymentBatch(Convert.ToInt32(Session["schema_id"]), tu_id, rl_id, number_of_insert))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        //////////
        ////////// SECURITY CHECK END //////////////
        /////////


        if (rl_id != "" && number_of_insert > 0) //
        {
            //Here we send the values to the stored procedure

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prRentPaymentBatchAdd", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);

                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar,15).Value = Request.UserHostAddress.ToString();

                // we replace the commas  by dots because SQL "CAST" will convert $100,00 by 100000.00
                // we cast the string "100,00" in money
                cmd.Parameters.Add("@rp_amount", SqlDbType.VarChar, 80000).Value = rp_amount.Replace(",", ".");
                cmd.Parameters.Add("@compare_rl_rent_amount", SqlDbType.VarChar, 80000).Value = compare_rl_rent_amount.Replace(",", ".");
                cmd.Parameters.Add("@rp_paid_date", SqlDbType.VarChar, 80000).Value = rp_paid_date;
                cmd.Parameters.Add("@rp_due_date", SqlDbType.VarChar, 80000).Value = rp_due_date;
                cmd.Parameters.Add("@tu_id", SqlDbType.VarChar, 80000).Value = tu_id;
                cmd.Parameters.Add("@rl_id", SqlDbType.VarChar, 80000).Value = rl_id;
                cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;
                cmd.Parameters.Add("@rp_no_delenquent", SqlDbType.VarChar, 80000).Value = no_delinquent;
                cmd.Parameters.Add("@rp_notes", SqlDbType.VarChar, 80000).Value = rp_notes;

                //execute the insert
                cmd.ExecuteReader();

            }
            finally
            {
                conn.Close();
            }


        }

      Response.Redirect("tenant_untreated_rent.aspx?h_id="+ddl_home_list.SelectedValue);
      
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_date_received_m_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_date_received_d_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_date_received_y_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
