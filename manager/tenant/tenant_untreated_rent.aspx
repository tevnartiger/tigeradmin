﻿<%@ Page Language="C#" MasterPageFile="../mp_manager.master" AutoEventWireup="true" CodeFile="tenant_untreated_rent.aspx.cs" Inherits="tenant_tenant_untreated_rent"Title="Prospective tenant view" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <h2> <b> <asp:Label ID="lbl_rent_unit_list" runat="server" Text="<%$ Resources:Resource, lbl_untreted_rent_list %>"/></b></h2>
        <br /><div id="txt_message" runat="server"></div><br />
      
      
        <table style="width: 45%">
            <tr>
                <td>
                     <asp:Label ID="lbl_property" runat="server" Text="<%$ Resources:Resource, lbl_property %>"/> </td>
                <td>
                    <asp:DropDownList AutoPostBack="true" DataTextField="home_name" DataValueField="home_id" ID="ddl_home_list" runat="server" OnSelectedIndexChanged="ddl_home_list_SelectedIndexChanged">
        </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                   <asp:Label ID="lbl_rent_paid_every" runat="server" Text="<%$ Resources:Resource, lbl_rent_paid_every %>"/> :
                </td>
                <td>
        <asp:DropDownList ID="ddl_rent_frequency" AutoPostBack="true" DataTextField="frequency" DataValueField="re_id" runat="server" OnSelectedIndexChanged="ddl_rent_frequency_SelectedIndexChanged">
        </asp:DropDownList>
                </td>
            </tr>
    </table>
        <br />
    
     
    <br />
&nbsp;row count :<asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
    <br />
  
       
        <br />
         <asp:GridView ID="gv_parent_nested_unpaid_rent_month" 
                       OnRowDataBound="gv_parent_nested_unpaid_rent_month_RowDataBound"
                       runat="server"  Width="87%"
                       AutoGenerateColumns="false">
    <Columns>
    
   
   <asp:BoundField DataField="unit_door_no" HeaderText="<%$ Resources:Resource,gv_unit %>"    />
  
    <asp:TemplateField HeaderText='<%$ Resources:Resource, lbl_untreated_rent %>' >
    <ItemTemplate>
    <asp:HiddenField Visible="false" ID="HiddenField2" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "tu_id")%>' />
     <asp:GridView ID="gv_child_nested_unpaid_rent_month" 
      DataSource='<%#GetTrasnl(Convert.ToInt32(Eval("tu_id")),Convert.ToInt32(Eval("rl_id")))%>' 
      runat="server" AutoGenerateColumns="false"
       AutoGenerateSelectButton="false"  Width="100%" >
    <Columns>
    
    <asp:TemplateField HeaderText='<%$ Resources:Resource, gv_to_process %>' >
    <ItemTemplate>
    <asp:CheckBox  runat="server"  Checked="true" ID="chk_process"   /> 
     </ItemTemplate>
    </asp:TemplateField>
   <asp:BoundField   DataField="rl_rent_amount"  DataFormatString="{0:0.00}"   HeaderText='<%$ Resources:Resource,gv_rent %>'/>
   
   <asp:TemplateField HeaderText='<%$ Resources:Resource,gv_amount_paid %>' >
    <ItemTemplate  >
    <asp:TextBox  Width="100" MaxLength="10"  runat="server"  ID="gvparent_child_textbox"   Text='<%# Bind("rl_rent_amount","{0:0.00}")%>'/> 
   </ItemTemplate  >
    </asp:TemplateField>
    
     <asp:TemplateField HeaderText="*" >
    <ItemTemplate>
    <asp:CheckBox  runat="server"   ID="chk_no_delequency"   /> 
     </ItemTemplate>
    </asp:TemplateField>
    
    <asp:TemplateField  HeaderText='<%$ Resources:Resource,gv_notes%>' >
    <ItemTemplate>
    
    <asp:TextBox  runat="server"  ID="tbx_notes"    Text=""/> 
    
    </ItemTemplate>
    </asp:TemplateField>
    
   <asp:BoundField    DataField="rp_due_date" DataFormatString="{0:M-dd-yyyy}"  
     HtmlEncode="false" HeaderText='<%$ Resources:Resource,gv_due_date %>' />


<asp:TemplateField HeaderText='<%$ Resources:Resource,lbl_date_received %>'   >
    <ItemTemplate>
        <asp:DropDownList SelectedValue='<%#Get_UntreatedRent_Month(Convert.ToDateTime(Eval("rp_due_date")))%>'  ID="ddl_child_unpaid_rent_m" runat="server">
        <asp:ListItem  Value="1">January</asp:ListItem>
        <asp:ListItem Value="2">February</asp:ListItem>
                        <asp:ListItem Value="3">March</asp:ListItem>
                        <asp:ListItem Value="4">April</asp:ListItem>
                        <asp:ListItem Value="5">May</asp:ListItem>
                        <asp:ListItem Value="6">June</asp:ListItem>
                        <asp:ListItem Value="7">July</asp:ListItem>
                        <asp:ListItem Value="8">August</asp:ListItem>
                        <asp:ListItem Value="9">September</asp:ListItem>
                        <asp:ListItem Value="10">October</asp:ListItem>
                        <asp:ListItem Value="11">November</asp:ListItem>
                        <asp:ListItem Value="12">December</asp:ListItem>
                       
           </asp:DropDownList>

           &nbsp; / &nbsp;
             <asp:DropDownList SelectedValue='<%#Get_UntreatedRent_Day(Convert.ToDateTime(Eval("rp_due_date")))%>' ID="ddl_child_unpaid_rent_d" runat="server">
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>
                 &nbsp; / &nbsp;

                  <asp:DropDownList SelectedValue='<%#Get_UntreatedRent_Year(Convert.ToDateTime(Eval("rp_due_date")))%>' ID="ddl_child_unpaid_rent_y" runat="server" >
                     <asp:ListItem>1980</asp:ListItem>
                     <asp:ListItem>1981</asp:ListItem>
                     <asp:ListItem>1982</asp:ListItem>
                     <asp:ListItem>1983</asp:ListItem>
                     <asp:ListItem>1984</asp:ListItem>
                     <asp:ListItem>1985</asp:ListItem>
                     <asp:ListItem>1986</asp:ListItem>
                     <asp:ListItem>1987</asp:ListItem>
                     <asp:ListItem>1988</asp:ListItem>
                     <asp:ListItem>1989</asp:ListItem>
                     <asp:ListItem>1990</asp:ListItem>
                     <asp:ListItem>1991</asp:ListItem>
                     <asp:ListItem>1992</asp:ListItem>
                     <asp:ListItem>1993</asp:ListItem>
                     <asp:ListItem>1994</asp:ListItem>
                     <asp:ListItem>1995</asp:ListItem>
                     <asp:ListItem>1996</asp:ListItem>
                     <asp:ListItem>1997</asp:ListItem>
                     <asp:ListItem>1998</asp:ListItem>
                     <asp:ListItem>1999</asp:ListItem>
                     <asp:ListItem>2000</asp:ListItem>
                     <asp:ListItem>2001</asp:ListItem>
                     <asp:ListItem>2002</asp:ListItem>
                     <asp:ListItem>2003</asp:ListItem>
                     <asp:ListItem>2004</asp:ListItem>
                     <asp:ListItem>2005</asp:ListItem>
                     <asp:ListItem>2006</asp:ListItem>
                     <asp:ListItem>2007</asp:ListItem>
                     <asp:ListItem>2008</asp:ListItem>
                     <asp:ListItem>2009</asp:ListItem>
                     <asp:ListItem>2010</asp:ListItem>
                     <asp:ListItem>2011</asp:ListItem>
                     <asp:ListItem>2012</asp:ListItem>
                     <asp:ListItem>2013</asp:ListItem>
                     <asp:ListItem>2014</asp:ListItem>
                     <asp:ListItem>2015</asp:ListItem>
                     <asp:ListItem>2016</asp:ListItem>
                     <asp:ListItem>2017</asp:ListItem>
                     <asp:ListItem>2018</asp:ListItem>
                     <asp:ListItem>2019</asp:ListItem>
                     <asp:ListItem>2020</asp:ListItem>
                 </asp:DropDownList>

                 <asp:HiddenField Visible="false"    runat="server"  ID="h_rl_id"   Value='<%# Bind("rl_id")%>' /> 
                 <asp:HiddenField Visible="false"    runat="server"  ID="h_re_id"   Value='<%# Bind("re_id")%>'  />
                 <asp:HiddenField Visible="false"    runat="server"  ID="h_tu_id"   Value='<%# Bind("tu_id")%>'  /> 
                 <asp:HiddenField Visible="false"    runat="server"  ID="h_rl_rent_amount"   Value='<%# Bind("rl_rent_amount")%>' /> 
                  <asp:HiddenField Visible="false"   runat="server"  ID="h_rent_due_date" Value='<%#Get_UntreatedRent(Convert.ToDateTime(Eval("rp_due_date")))%>'  />
   
     </ItemTemplate>
    </asp:TemplateField>
    
    <asp:HyperLinkField  Text="<%$ Resources:Resource, gv_view %>" 
     DataNavigateUrlFields="tu_id,rl_rent_amount,re_id" 
     DataNavigateUrlFormatString="~/tenant/tenant_rent.aspx?tu_id={0}&ra_id={1}&re_id={2}" 
      HeaderText="View" />
      
  
   </Columns>
   </asp:GridView>
    
    </ItemTemplate>
    </asp:TemplateField>
 
   
    </Columns>
    </asp:GridView>

        <br />

        <asp:Button ID="btn_submit" runat="server" Text="Submit" 
        onclick="btn_submit_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  
</asp:Content>