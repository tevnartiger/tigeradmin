<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="wo_add.aspx.cs" Inherits="manager_workorder_wo_add" Title="Untitled Page" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <asp:MultiView ID="MultiView1" runat="server">
    <asp:View ID="View1" runat="server">
    
        <b>
          <asp:Label ID="Label1" runat="server" 
        Text="<%$ Resources:Resource, lbl_wo_creation %>" style="font-size: small"></asp:Label></b>
     
    <br /><br /><br />
    <table width="83%" >
        <tr>
            <td valign="top" style="width: 248px">
              <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_title %>"></asp:Label></td>
            <td >
                <asp:TextBox ID="tbx_wo_title" runat="server" Height="16px" Width="228px"></asp:TextBox>
                &nbsp;<asp:RegularExpressionValidator ID="reg_tbx_wo_title" runat="server" 
                    ControlToValidate="tbx_wo_title" ErrorMessage="invalid name" 
                    ValidationGroup="vg_wo">
                        </asp:RegularExpressionValidator>
                &nbsp;<asp:RequiredFieldValidator ID="req_tbx_wo_title" runat="server" 
                    ControlToValidate="tbx_wo_title" ErrorMessage="required" 
                    ValidationGroup="vg_wo">
                    </asp:RequiredFieldValidator>
              <br /><hr />      
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 248px" >
                <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lbl_property %>"></asp:Label></td>
            <td >
                <asp:DropDownList DataValueField="home_id" DataTextField="home_name"  
                    ID="ddl_home_id" runat="server"  AutoPostBack="true"
                    onselectedindexchanged="ddl_home_id_SelectedIndexChanged">
                </asp:DropDownList>
                <br /><hr />
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 248px" >
                <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lbl_unit_if_applicable %>"></asp:Label></td>
            <td >
                <asp:DropDownList DataValueField="unit_id" DataTextField="unit_door_no"  ID="ddl_unit_id" runat="server">
                </asp:DropDownList>
                <br /><hr />
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 248px" >
                <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lbl_exact_location %>"></asp:Label></td>
            <td >
                <asp:TextBox ID="tbx_wo_location" runat="server" Height="16px" Width="229px"></asp:TextBox><br />
                <hr />
            </td>
        </tr>
        <tr>
            <td valign="top" style="width: 248px" >
                <asp:Label ID="Label29" runat="server" 
                    Text="<%$ Resources:Resource, lbl_priority%>"></asp:Label></td>
            <td >
                <asp:DropDownList ID="ddl_wo_priority" runat="server">
                    <asp:ListItem Text="<%$ Resources:Resource, lbl_urgent%>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, lbl_high%>" Value="2"></asp:ListItem>
                    <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_medium%>"  Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, lbl_low%>" Value="4"></asp:ListItem>
                </asp:DropDownList>
                    <hr />
                    </td>
        </tr>
              
        <tr>
            <td valign="top" style="width: 248px" >
                <asp:Label ID="Label31" runat="server" Text="<%$ Resources:Resource, lbl_status%>"></asp:Label>
            </td>
            <td >
                <asp:DropDownList ID="ddl_wo_status" runat="server">
                    <asp:ListItem Text="<%$ Resources:Resource, lbl_pending%>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, lbl_in_progress%>" Value="2"></asp:ListItem>
                   <asp:ListItem Text="<%$ Resources:Resource, lbl_closed%>" Value="4"></asp:ListItem>
                </asp:DropDownList>
                    <hr />
                    </td>
        </tr>
              
        <tr>
            <td valign="top" style="width: 248px" >
                <asp:Label ID="Label12" runat="server" 
                    Text="<%$ Resources:Resource, lbl_date_begin %>"></asp:Label>&nbsp;</td>
            <td >
                <asp:DropDownList ID="ddl_wo_date_begin_m" runat="server">
                        <asp:ListItem Text="<%$ Resources:Resource, txt_month %>" Value="0"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;/&nbsp;
                    <asp:DropDownList ID="ddl_wo_date_begin_d" runat="server" >
                     <asp:ListItem Text="<%$ Resources:Resource, txt_day %>" Value="0"></asp:ListItem>
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>&nbsp;/&nbsp;    
                    

                     <asp:DropDownList ID="ddl_wo_date_begin_y" runat="server">
                            <asp:ListItem Text="<%$ Resources:Resource, txt_year %>" Value="0"></asp:ListItem>
                            <asp:ListItem>1950</asp:ListItem>
                            <asp:ListItem>1951</asp:ListItem>
                            <asp:ListItem>1952</asp:ListItem>
                            <asp:ListItem>1953</asp:ListItem>
                            <asp:ListItem>1954</asp:ListItem>
                            <asp:ListItem>1955</asp:ListItem>
                            <asp:ListItem>1956</asp:ListItem>
                            <asp:ListItem>1957</asp:ListItem>
                            <asp:ListItem>1958</asp:ListItem>
                            <asp:ListItem>1959</asp:ListItem>
                            <asp:ListItem>1960</asp:ListItem>
                            <asp:ListItem>1961</asp:ListItem>
                            <asp:ListItem>1962</asp:ListItem>
                            <asp:ListItem>1963</asp:ListItem>
                            <asp:ListItem>1964</asp:ListItem>
                            <asp:ListItem>1965</asp:ListItem>
                            <asp:ListItem>1966</asp:ListItem>
                            <asp:ListItem>1967</asp:ListItem>
                            <asp:ListItem>1968</asp:ListItem>
                            <asp:ListItem>1969</asp:ListItem>
                            <asp:ListItem>1970</asp:ListItem>
                            <asp:ListItem>1971</asp:ListItem>
                            <asp:ListItem>1972</asp:ListItem>
                            <asp:ListItem>1973</asp:ListItem>
                            <asp:ListItem>1974</asp:ListItem>
                            <asp:ListItem>1975</asp:ListItem>
                            <asp:ListItem>1976</asp:ListItem>
                            <asp:ListItem>1977</asp:ListItem>
                            <asp:ListItem>1978</asp:ListItem>
                            <asp:ListItem>1979</asp:ListItem>
                            <asp:ListItem>1980</asp:ListItem>
                            <asp:ListItem>1981</asp:ListItem>
                            <asp:ListItem>1982</asp:ListItem>
                            <asp:ListItem>1983</asp:ListItem>
                            <asp:ListItem>1984</asp:ListItem>
                            <asp:ListItem>1985</asp:ListItem>
                            <asp:ListItem>1986</asp:ListItem>
                            <asp:ListItem>1987</asp:ListItem>
                            <asp:ListItem>1988</asp:ListItem>
                            <asp:ListItem>1989</asp:ListItem>
                            <asp:ListItem>1990</asp:ListItem>
                            <asp:ListItem>1991</asp:ListItem>
                            <asp:ListItem>1992</asp:ListItem>
                            <asp:ListItem>1993</asp:ListItem>
                            <asp:ListItem>1994</asp:ListItem>
                            <asp:ListItem>1995</asp:ListItem>
                            <asp:ListItem>1996</asp:ListItem>
                            <asp:ListItem>1997</asp:ListItem>
                            <asp:ListItem>1998</asp:ListItem>
                            <asp:ListItem>1999</asp:ListItem>
                            <asp:ListItem>2000</asp:ListItem>
                            <asp:ListItem>2001</asp:ListItem>
                            <asp:ListItem>2002</asp:ListItem>
                            <asp:ListItem>2003</asp:ListItem>
                            <asp:ListItem>2004</asp:ListItem>
                            <asp:ListItem>2005</asp:ListItem>
                            <asp:ListItem>2006</asp:ListItem>
                            <asp:ListItem>2007</asp:ListItem>
                            <asp:ListItem>2008</asp:ListItem>
                            <asp:ListItem>2009</asp:ListItem>
                            <asp:ListItem>2010</asp:ListItem>
                            <asp:ListItem>2011</asp:ListItem>
                            <asp:ListItem>2012</asp:ListItem>
                            <asp:ListItem>2013</asp:ListItem>
                            <asp:ListItem>2014</asp:ListItem>
                            <asp:ListItem>2015</asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>
                    
                    
                    
                    
                    
                    
                &nbsp;<hr />
                    </td>
        </tr>
        
        <tr>
            <td valign="top" style="width: 248px">
                <asp:Label ID="Label3" runat="server" 
                    Text="<%$ Resources:Resource, lbl_expected_date_end %>"></asp:Label>
            </td>
            <td>
                
                <asp:DropDownList ID="ddl_wo_exp_date_end_m" runat="server">
                    <asp:ListItem Text="<%$ Resources:Resource, txt_month %>" Value="0"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                </asp:DropDownList>
                &nbsp;/&nbsp;
                <asp:DropDownList ID="ddl_wo_exp_date_end_d" runat="server">
                    <asp:ListItem Text="<%$ Resources:Resource, txt_day %>" Value="0"></asp:ListItem>
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>5</asp:ListItem>
                    <asp:ListItem>6</asp:ListItem>
                    <asp:ListItem>7</asp:ListItem>
                    <asp:ListItem>8</asp:ListItem>
                    <asp:ListItem>9</asp:ListItem>
                    <asp:ListItem>10</asp:ListItem>
                    <asp:ListItem>11</asp:ListItem>
                    <asp:ListItem>12</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>14</asp:ListItem>
                    <asp:ListItem>15</asp:ListItem>
                    <asp:ListItem>16</asp:ListItem>
                    <asp:ListItem>17</asp:ListItem>
                    <asp:ListItem>18</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>20</asp:ListItem>
                    <asp:ListItem>21</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>23</asp:ListItem>
                    <asp:ListItem>24</asp:ListItem>
                    <asp:ListItem>25</asp:ListItem>
                    <asp:ListItem>26</asp:ListItem>
                    <asp:ListItem>27</asp:ListItem>
                    <asp:ListItem>28</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                    <asp:ListItem>30</asp:ListItem>
                    <asp:ListItem>31</asp:ListItem>
                </asp:DropDownList>
                &nbsp;/&nbsp;
                <asp:DropDownList ID="ddl_wo_exp_date_end_y" runat="server">
                    <asp:ListItem Text="<%$ Resources:Resource, txt_year %>" Value="0"></asp:ListItem>
                    <asp:ListItem>1950</asp:ListItem>
                    <asp:ListItem>1951</asp:ListItem>
                    <asp:ListItem>1952</asp:ListItem>
                    <asp:ListItem>1953</asp:ListItem>
                    <asp:ListItem>1954</asp:ListItem>
                    <asp:ListItem>1955</asp:ListItem>
                    <asp:ListItem>1956</asp:ListItem>
                    <asp:ListItem>1957</asp:ListItem>
                    <asp:ListItem>1958</asp:ListItem>
                    <asp:ListItem>1959</asp:ListItem>
                    <asp:ListItem>1960</asp:ListItem>
                    <asp:ListItem>1961</asp:ListItem>
                    <asp:ListItem>1962</asp:ListItem>
                    <asp:ListItem>1963</asp:ListItem>
                    <asp:ListItem>1964</asp:ListItem>
                    <asp:ListItem>1965</asp:ListItem>
                    <asp:ListItem>1966</asp:ListItem>
                    <asp:ListItem>1967</asp:ListItem>
                    <asp:ListItem>1968</asp:ListItem>
                    <asp:ListItem>1969</asp:ListItem>
                    <asp:ListItem>1970</asp:ListItem>
                    <asp:ListItem>1971</asp:ListItem>
                    <asp:ListItem>1972</asp:ListItem>
                    <asp:ListItem>1973</asp:ListItem>
                    <asp:ListItem>1974</asp:ListItem>
                    <asp:ListItem>1975</asp:ListItem>
                    <asp:ListItem>1976</asp:ListItem>
                    <asp:ListItem>1977</asp:ListItem>
                    <asp:ListItem>1978</asp:ListItem>
                    <asp:ListItem>1979</asp:ListItem>
                    <asp:ListItem>1980</asp:ListItem>
                    <asp:ListItem>1981</asp:ListItem>
                    <asp:ListItem>1982</asp:ListItem>
                    <asp:ListItem>1983</asp:ListItem>
                    <asp:ListItem>1984</asp:ListItem>
                    <asp:ListItem>1985</asp:ListItem>
                    <asp:ListItem>1986</asp:ListItem>
                    <asp:ListItem>1987</asp:ListItem>
                    <asp:ListItem>1988</asp:ListItem>
                    <asp:ListItem>1989</asp:ListItem>
                    <asp:ListItem>1990</asp:ListItem>
                    <asp:ListItem>1991</asp:ListItem>
                    <asp:ListItem>1992</asp:ListItem>
                    <asp:ListItem>1993</asp:ListItem>
                    <asp:ListItem>1994</asp:ListItem>
                    <asp:ListItem>1995</asp:ListItem>
                    <asp:ListItem>1996</asp:ListItem>
                    <asp:ListItem>1997</asp:ListItem>
                    <asp:ListItem>1998</asp:ListItem>
                    <asp:ListItem>1999</asp:ListItem>
                    <asp:ListItem>2000</asp:ListItem>
                    <asp:ListItem>2001</asp:ListItem>
                    <asp:ListItem>2002</asp:ListItem>
                    <asp:ListItem>2003</asp:ListItem>
                    <asp:ListItem>2004</asp:ListItem>
                    <asp:ListItem>2005</asp:ListItem>
                    <asp:ListItem>2006</asp:ListItem>
                    <asp:ListItem>2007</asp:ListItem>
                    <asp:ListItem>2008</asp:ListItem>
                    <asp:ListItem>2009</asp:ListItem>
                    <asp:ListItem>2010</asp:ListItem>
                    <asp:ListItem>2011</asp:ListItem>
                    <asp:ListItem>2012</asp:ListItem>
                    <asp:ListItem>2013</asp:ListItem>
                    <asp:ListItem>2014</asp:ListItem>
                    <asp:ListItem>2015</asp:ListItem>
                    <asp:ListItem></asp:ListItem>
                </asp:DropDownList>
                &nbsp;
              
                <asp:Label ID="lbl_exp_date_end_greater0" runat="server" 
                    Text="<%$ Resources:Resource, lbl_exp_date_end_greater %>" Visible="false"></asp:Label>
              
            </td>
        </tr>
                
        <tr>
            <td style="width: 250px" valign="top">
                &nbsp;</td>
            <td align="right" bgcolor="aliceblue">
                <asp:Button ID="btn_step_2" runat="server" onclick="btn_step_2_Click" 
                    Text="<%$ Resources:Resource, btn_continu %>" ValidationGroup="vg_wo" />
            </td>
        </tr>
                
        </table>
        
       
        <table  width="83%">
        <tr>
        <td>
            &nbsp;</td>
        </tr>
        </table>
       
       
       
          
        
            </asp:View>
    
    
    
    
    
    
    <asp:View ID="View2" runat="server">
    
    
    
       <b>
          <asp:Label ID="Label27" runat="server" 
        Text="<%$ Resources:Resource, lbl_work_order_tasks %>" style="font-size: small"></asp:Label></b>
       
       
       
          
        
        <br />
        <br />
        <asp:GridView ID="gv_task" runat="server" AutoGenerateColumns="false" Width="83%">
      <Columns>   
       <asp:BoundField   DataField="task_title"  HeaderText="<%$ Resources:Resource, lbl_tasks %>"/>
       <asp:BoundField   DataField="company_name"  HeaderText="<%$ Resources:Resource, lbl_company %>"/>
       
       <asp:BoundField  DataField="task_date_begin" 
                        HeaderText="<%$ Resources:Resource, lbl_date_begin %>"
                        DataFormatString="{0:MMM dd , yyyy}"  
                        HtmlEncode="false" />
       <asp:BoundField  DataField="task_exp_date_end" 
                        HeaderText="<%$ Resources:Resource, lbl_due_date %>" 
                        DataFormatString="{0:MMM dd , yyyy}"  
                        HtmlEncode="false" />
                        
        <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_status%>"  >
       <ItemTemplate  >
       <asp:Label ID="lbl_task_status"  
                  Text='<%#GetStatus(Convert.ToInt32(Eval("task_status")))%>' 
                  runat="server" />   
       </ItemTemplate  >
      </asp:TemplateField>
                        
       <asp:TemplateField HeaderText="<%$ Resources:Resource, lbl_priority%>"  >
       <ItemTemplate  >
       <asp:Label ID="lbl_task_priority"  
                  Text='<%#GetPriority(Convert.ToInt32(Eval("task_priority")))%>' 
                  runat="server" />   
       </ItemTemplate  >
       </asp:TemplateField>
       
       
       <asp:BoundField  DataField="task_cost_estimation"
                        HeaderText="<%$ Resources:Resource, lbl_cost_estimation %>"
                        DataFormatString="{0:0.00}" />
                        
       <asp:TemplateField >
       <ItemTemplate  >
    
       <asp:HiddenField ID="h_task_id" Value='<%#Bind("task_id")%>'  runat="server" />
       <asp:Button ID="btn_cancel" runat="server" OnClick="btn_cancel_task_Click"  Text="<%$ Resources:Resource, btn_cancel %>"/>
       </ItemTemplate  >
       </asp:TemplateField>
     </Columns>
    </asp:GridView>
    
    
        <br />
        <b>
        <asp:Label ID="Label30" runat="server" style="font-size: small" 
            Text="<%$ Resources:Resource, lbl_work_order %>"></asp:Label>
        </b>
    <br />
    
        <br />
        <table width="83%">
            <tr>
                <td style="width: 248px" valign="top">
                    <asp:Label ID="Label21" runat="server" 
                        Text="<%$ Resources:Resource, lbl_title %>"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lbl_wo_title" runat="server" Height="16px" 
                        style="font-weight: 700" Width="228px"></asp:Label>
                    <br />
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 248px" valign="top">
                    <asp:Label ID="Label22" runat="server" 
                        Text="<%$ Resources:Resource, lbl_property %>"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lbl_home_name" runat="server" style="font-weight: 700"></asp:Label>
                    <br />
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 248px" valign="top">
                    <asp:Label ID="Label23" runat="server" 
                        Text="<%$ Resources:Resource, lbl_unit_if_applicable %>"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lbl_unit_door_no" runat="server" style="font-weight: 700"></asp:Label>
                    <br />
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 248px" valign="top">
                    <asp:Label ID="Label32" runat="server" 
                        Text="<%$ Resources:Resource, lbl_priority%>"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lbl_wo_priority" runat="server" style="font-weight: 700"></asp:Label>
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 248px" valign="top">
                    <asp:Label ID="Label33" runat="server" 
                        Text="<%$ Resources:Resource, lbl_status%>"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lbl_wo_status" runat="server" style="font-weight: 700"></asp:Label>
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 248px" valign="top">
                    <asp:Label ID="Label24" runat="server" 
                        Text="<%$ Resources:Resource, lbl_exact_location %>"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="lbl_exact_location" runat="server" Height="16px" 
                        style="font-weight: 700" Width="229px"></asp:Label>
                    <br />
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 248px" valign="top">
                    <asp:Label ID="Label25" runat="server" 
                        Text="<%$ Resources:Resource, lbl_date_begin %>"></asp:Label>
                    &nbsp;</td>
                <td>
                    <asp:Label ID="lbl_wo_date_begin" runat="server" style="font-weight: 700"></asp:Label>
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 248px" valign="top">
                    <asp:Label ID="Label26" runat="server" 
                        Text="<%$ Resources:Resource, lbl_expected_date_end %>"></asp:Label>
                </td>
                <td>
                    &nbsp;<asp:Label ID="lbl_wo_exp_date_end" runat="server" style="font-weight: 700"></asp:Label>
                    <hr />
                </td>
            </tr>
        </table>
        <table width="83%">
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label17" runat="server" 
                        style="font-size: small; font-weight: 700" 
                        Text="<%$ Resources:Resource, lbl_tasks %>"></asp:Label>
                    <br />
                    <br />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label18" runat="server" 
                        Text="<%$ Resources:Resource, lbl_title %>"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="tbx_task_title" runat="server" Height="16px" Width="228px"></asp:TextBox>
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label19" runat="server" 
                        Text="<%$ Resources:Resource, lbl_task_date_begin %>"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddl_task_date_begin_m" runat="server">
                        <asp:ListItem Text="<%$ Resources:Resource, txt_month %>" Value="0"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;/&nbsp;
                    <asp:DropDownList ID="ddl_task_date_begin_d" runat="server">
                        <asp:ListItem Text="<%$ Resources:Resource, txt_day %>" Value="0"></asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;/&nbsp;
                    <asp:DropDownList ID="ddl_task_date_begin_y" runat="server">
                        <asp:ListItem Text="<%$ Resources:Resource, txt_year %>" Value="0"></asp:ListItem>
                        <asp:ListItem>1950</asp:ListItem>
                        <asp:ListItem>1951</asp:ListItem>
                        <asp:ListItem>1952</asp:ListItem>
                        <asp:ListItem>1953</asp:ListItem>
                        <asp:ListItem>1954</asp:ListItem>
                        <asp:ListItem>1955</asp:ListItem>
                        <asp:ListItem>1956</asp:ListItem>
                        <asp:ListItem>1957</asp:ListItem>
                        <asp:ListItem>1958</asp:ListItem>
                        <asp:ListItem>1959</asp:ListItem>
                        <asp:ListItem>1960</asp:ListItem>
                        <asp:ListItem>1961</asp:ListItem>
                        <asp:ListItem>1962</asp:ListItem>
                        <asp:ListItem>1963</asp:ListItem>
                        <asp:ListItem>1964</asp:ListItem>
                        <asp:ListItem>1965</asp:ListItem>
                        <asp:ListItem>1966</asp:ListItem>
                        <asp:ListItem>1967</asp:ListItem>
                        <asp:ListItem>1968</asp:ListItem>
                        <asp:ListItem>1969</asp:ListItem>
                        <asp:ListItem>1970</asp:ListItem>
                        <asp:ListItem>1971</asp:ListItem>
                        <asp:ListItem>1972</asp:ListItem>
                        <asp:ListItem>1973</asp:ListItem>
                        <asp:ListItem>1974</asp:ListItem>
                        <asp:ListItem>1975</asp:ListItem>
                        <asp:ListItem>1976</asp:ListItem>
                        <asp:ListItem>1977</asp:ListItem>
                        <asp:ListItem>1978</asp:ListItem>
                        <asp:ListItem>1979</asp:ListItem>
                        <asp:ListItem>1980</asp:ListItem>
                        <asp:ListItem>1981</asp:ListItem>
                        <asp:ListItem>1982</asp:ListItem>
                        <asp:ListItem>1983</asp:ListItem>
                        <asp:ListItem>1984</asp:ListItem>
                        <asp:ListItem>1985</asp:ListItem>
                        <asp:ListItem>1986</asp:ListItem>
                        <asp:ListItem>1987</asp:ListItem>
                        <asp:ListItem>1988</asp:ListItem>
                        <asp:ListItem>1989</asp:ListItem>
                        <asp:ListItem>1990</asp:ListItem>
                        <asp:ListItem>1991</asp:ListItem>
                        <asp:ListItem>1992</asp:ListItem>
                        <asp:ListItem>1993</asp:ListItem>
                        <asp:ListItem>1994</asp:ListItem>
                        <asp:ListItem>1995</asp:ListItem>
                        <asp:ListItem>1996</asp:ListItem>
                        <asp:ListItem>1997</asp:ListItem>
                        <asp:ListItem>1998</asp:ListItem>
                        <asp:ListItem>1999</asp:ListItem>
                        <asp:ListItem>2000</asp:ListItem>
                        <asp:ListItem>2001</asp:ListItem>
                        <asp:ListItem>2002</asp:ListItem>
                        <asp:ListItem>2003</asp:ListItem>
                        <asp:ListItem>2004</asp:ListItem>
                        <asp:ListItem>2005</asp:ListItem>
                        <asp:ListItem>2006</asp:ListItem>
                        <asp:ListItem>2007</asp:ListItem>
                        <asp:ListItem>2008</asp:ListItem>
                        <asp:ListItem>2009</asp:ListItem>
                        <asp:ListItem>2010</asp:ListItem>
                        <asp:ListItem>2011</asp:ListItem>
                        <asp:ListItem>2012</asp:ListItem>
                        <asp:ListItem>2013</asp:ListItem>
                        <asp:ListItem>2014</asp:ListItem>
                        <asp:ListItem>2015</asp:ListItem>
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;<hr />
                </td>
            </tr>
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label20" runat="server" 
                        Text="<%$ Resources:Resource, lbl_task_exp_date_end %>"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddl_task_est_date_end_m" runat="server">
                        <asp:ListItem Text="<%$ Resources:Resource, txt_month %>" Value="0"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;/&nbsp;
                    <asp:DropDownList ID="ddl_task_est_date_end_d" runat="server">
                        <asp:ListItem Text="<%$ Resources:Resource, txt_day %>" Value="0"></asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                        <asp:ListItem>5</asp:ListItem>
                        <asp:ListItem>6</asp:ListItem>
                        <asp:ListItem>7</asp:ListItem>
                        <asp:ListItem>8</asp:ListItem>
                        <asp:ListItem>9</asp:ListItem>
                        <asp:ListItem>10</asp:ListItem>
                        <asp:ListItem>11</asp:ListItem>
                        <asp:ListItem>12</asp:ListItem>
                        <asp:ListItem>13</asp:ListItem>
                        <asp:ListItem>14</asp:ListItem>
                        <asp:ListItem>15</asp:ListItem>
                        <asp:ListItem>16</asp:ListItem>
                        <asp:ListItem>17</asp:ListItem>
                        <asp:ListItem>18</asp:ListItem>
                        <asp:ListItem>19</asp:ListItem>
                        <asp:ListItem>20</asp:ListItem>
                        <asp:ListItem>21</asp:ListItem>
                        <asp:ListItem>22</asp:ListItem>
                        <asp:ListItem>23</asp:ListItem>
                        <asp:ListItem>24</asp:ListItem>
                        <asp:ListItem>25</asp:ListItem>
                        <asp:ListItem>26</asp:ListItem>
                        <asp:ListItem>27</asp:ListItem>
                        <asp:ListItem>28</asp:ListItem>
                        <asp:ListItem>29</asp:ListItem>
                        <asp:ListItem>30</asp:ListItem>
                        <asp:ListItem>31</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;/&nbsp;
                    <asp:DropDownList ID="ddl_task_est_date_end_y" runat="server">
                        <asp:ListItem Text="<%$ Resources:Resource, txt_year %>" Value="0"></asp:ListItem>
                        <asp:ListItem>1950</asp:ListItem>
                        <asp:ListItem>1951</asp:ListItem>
                        <asp:ListItem>1952</asp:ListItem>
                        <asp:ListItem>1953</asp:ListItem>
                        <asp:ListItem>1954</asp:ListItem>
                        <asp:ListItem>1955</asp:ListItem>
                        <asp:ListItem>1956</asp:ListItem>
                        <asp:ListItem>1957</asp:ListItem>
                        <asp:ListItem>1958</asp:ListItem>
                        <asp:ListItem>1959</asp:ListItem>
                        <asp:ListItem>1960</asp:ListItem>
                        <asp:ListItem>1961</asp:ListItem>
                        <asp:ListItem>1962</asp:ListItem>
                        <asp:ListItem>1963</asp:ListItem>
                        <asp:ListItem>1964</asp:ListItem>
                        <asp:ListItem>1965</asp:ListItem>
                        <asp:ListItem>1966</asp:ListItem>
                        <asp:ListItem>1967</asp:ListItem>
                        <asp:ListItem>1968</asp:ListItem>
                        <asp:ListItem>1969</asp:ListItem>
                        <asp:ListItem>1970</asp:ListItem>
                        <asp:ListItem>1971</asp:ListItem>
                        <asp:ListItem>1972</asp:ListItem>
                        <asp:ListItem>1973</asp:ListItem>
                        <asp:ListItem>1974</asp:ListItem>
                        <asp:ListItem>1975</asp:ListItem>
                        <asp:ListItem>1976</asp:ListItem>
                        <asp:ListItem>1977</asp:ListItem>
                        <asp:ListItem>1978</asp:ListItem>
                        <asp:ListItem>1979</asp:ListItem>
                        <asp:ListItem>1980</asp:ListItem>
                        <asp:ListItem>1981</asp:ListItem>
                        <asp:ListItem>1982</asp:ListItem>
                        <asp:ListItem>1983</asp:ListItem>
                        <asp:ListItem>1984</asp:ListItem>
                        <asp:ListItem>1985</asp:ListItem>
                        <asp:ListItem>1986</asp:ListItem>
                        <asp:ListItem>1987</asp:ListItem>
                        <asp:ListItem>1988</asp:ListItem>
                        <asp:ListItem>1989</asp:ListItem>
                        <asp:ListItem>1990</asp:ListItem>
                        <asp:ListItem>1991</asp:ListItem>
                        <asp:ListItem>1992</asp:ListItem>
                        <asp:ListItem>1993</asp:ListItem>
                        <asp:ListItem>1994</asp:ListItem>
                        <asp:ListItem>1995</asp:ListItem>
                        <asp:ListItem>1996</asp:ListItem>
                        <asp:ListItem>1997</asp:ListItem>
                        <asp:ListItem>1998</asp:ListItem>
                        <asp:ListItem>1999</asp:ListItem>
                        <asp:ListItem>2000</asp:ListItem>
                        <asp:ListItem>2001</asp:ListItem>
                        <asp:ListItem>2002</asp:ListItem>
                        <asp:ListItem>2003</asp:ListItem>
                        <asp:ListItem>2004</asp:ListItem>
                        <asp:ListItem>2005</asp:ListItem>
                        <asp:ListItem>2006</asp:ListItem>
                        <asp:ListItem>2007</asp:ListItem>
                        <asp:ListItem>2008</asp:ListItem>
                        <asp:ListItem>2009</asp:ListItem>
                        <asp:ListItem>2010</asp:ListItem>
                        <asp:ListItem>2011</asp:ListItem>
                        <asp:ListItem>2012</asp:ListItem>
                        <asp:ListItem>2013</asp:ListItem>
                        <asp:ListItem>2014</asp:ListItem>
                        <asp:ListItem>2015</asp:ListItem>
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;<asp:Label ID="lbl_exp_date_end_greater" runat="server" 
                        Text="<%$ Resources:Resource, lbl_exp_date_end_greater %>" Visible="false"></asp:Label>
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label7" runat="server" 
                        Text="<%$ Resources:Resource, lbl_type_of_work %>" />
                </td>
                <td>
                    <table ID="tb_contractor" runat="server" bgcolor="#ffffcc" 
                        language="javascript" onclick="return TABLE1_onclick()">
                        <tr>
                            <td>
                                <asp:CheckBox ID="company_hvac" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_hvac %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_decoration" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_decoration %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_inspection" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_inspection %>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="company_cleaning" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_cleaning %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_doors_windows" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_doors %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_kitchen" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_kitchen %>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="company_alarms_security_systems" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_alarm_security %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_electrical" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_electrical %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_locksmith" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_locksmith %>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="company_architech" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_architech %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_engineer" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_engineer %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_painting" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_painting %>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="company_basement" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_basement %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_flooring" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_flooring %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_paving" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_paving %>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="company_bricks" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_bricks %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_foundation" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_foundation %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_plumbing" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_plumbing %>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="company_cable_satellite_dish" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_cable %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_gardening" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_gardening %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_roofs" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_roofs %>" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="company_ciment" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_ciment %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_gypse_installation" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_gypse_installation %>" />
                            </td>
                            <td>
                                <asp:CheckBox ID="company_other" runat="server" 
                                    Text="<%$ Resources:Resource, lbl_other %>" />
                            </td>
                        </tr>
                    </table>
                    <br />
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label8" runat="server" 
                        Text="<%$ Resources:Resource, lbl_priority%>"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="ddl_priority" runat="server">
                        <asp:ListItem Text="<%$ Resources:Resource, lbl_urgent%>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, lbl_high%>" Value="2"></asp:ListItem>
                        <asp:ListItem Selected="True" Text="<%$ Resources:Resource, lbl_medium%>" 
                            Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, lbl_low%>" Value="4"></asp:ListItem>
                    </asp:DropDownList>
                    <hr />
                </td>
            </tr>
            
            <tr>
            <td valign="top" style="width: 248px" >
                <asp:Label ID="Label16" runat="server" Text="<%$ Resources:Resource, lbl_status%>"></asp:Label>
            </td>
            <td >
                <asp:DropDownList ID="ddl_task_status" runat="server">
                    <asp:ListItem Text="<%$ Resources:Resource, lbl_pending%>" Value="1"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, lbl_in_progress%>" Value="2"></asp:ListItem>
                    <asp:ListItem Text="<%$ Resources:Resource, lbl_closed%>" Value="4"></asp:ListItem>
                </asp:DropDownList>
                    <hr />
                    </td>
        </tr>
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label9" runat="server" 
                        Text="<%$ Resources:Resource, lbl_description %>" />
                </td>
                <td>
                    <asp:TextBox ID="tbx_task_description" runat="server" Height="141px" 
                        TextMode="MultiLine" Width="360px"></asp:TextBox>
                    <br />
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label10" runat="server" 
                        Text="<%$ Resources:Resource, lbl_notes %>" />
                </td>
                <td>
                    <asp:TextBox ID="tbx_task_notes" runat="server" Height="140px" 
                        TextMode="MultiLine" Width="360px"></asp:TextBox>
                    <br />
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label13" runat="server" 
                        Text="<%$ Resources:Resource, lbl_contractor %>"></asp:Label>
                    &nbsp;</td>
                <td>
                    <asp:DropDownList ID="ddl_task_contractor" runat="server" 
                        DataTextField="company_name" DataValueField="company_id">
                    </asp:DropDownList>
                    <br />
                    <br />
                    <asp:LinkButton ID="link_company_add" runat="server" 
                        onclick="link_company_add_Click">Add a company</asp:LinkButton>
                    <br />
                    <br />
                    <asp:Panel ID="Panel1" runat="server" Wrap="true">
                        <div>
                            <asp:Label ID="lbl_company" runat="server" 
                                Text="<%$ Resources:Resource, lbl_company %>"></asp:Label>
                            <br />
                            <table ID="tb_contractor_question" runat="server">
                                <tr>
                                    <td valign="top">
                                        Is this contractor is also a vendor/supplier
                                    </td>
                                    <td>
                                        <asp:RadioButtonList ID="radio_contractor" runat="server">
                                            <asp:ListItem Selected="True" Value="0">no</asp:ListItem>
                                            <asp:ListItem Value="1">yes</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:Label ID="lbl_category" runat="server" 
                                Text="<%$ Resources:Resource, lbl_category %>"></asp:Label>
                            <br />
                            <br />
                            <table ID="Table1" runat="server" bgcolor="#ffffcc" language="javascript" 
                                onclick="return TABLE1_onclick()">
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="company_general_contractor" runat="server" 
                                            Text="general contractor" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox1" runat="server" Text="cleaning" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox2" runat="server" Text="HVAC" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="company_interior_contractor" runat="server" 
                                            Text="interior contractor" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox3" runat="server" Text="decoration" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox4" runat="server" Text="inspection" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="company_exterior_contractor" runat="server" 
                                            Text="exterior contractor" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox5" runat="server" Text="doors &amp; windows" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox6" runat="server" Text="kitchen" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="CheckBox7" runat="server" 
                                            Text="alarms &amp; security systems" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox8" runat="server" Text="electrical" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox9" runat="server" Text="locksmith" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="CheckBox10" runat="server" Text="architech" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox11" runat="server" Text="engineer" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox12" runat="server" Text="painting" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="CheckBox13" runat="server" Text="basement" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox14" runat="server" Text="flooring" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox15" runat="server" Text="paving" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="CheckBox16" runat="server" Text="bricks" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox17" runat="server" Text="foundation" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox18" runat="server" Text="plumbing" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="CheckBox19" runat="server" Text="cable , satellite , dish" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox20" runat="server" Text="gardening" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox21" runat="server" Text="roofs" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="CheckBox22" runat="server" Text="ciment" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox23" runat="server" Text="gypse installation" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox24" runat="server" Text="other" />
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:Label ID="Label15" runat="server" 
                                Text="<%$ Resources:Resource, lbl_company %>"></asp:Label>
                            <br />
                            <br />
                            <table bgcolor="#ffffcc">
                                <tr>
                                    <td style="width: 109px">
                                        <asp:Label ID="lbl_name_name" runat="server" 
                                            Text="<%$ Resources:Resource, lbl_name_name %>"></asp:Label>
                                    </td>
                                    <td style="width: 169px">
                                        :
                                        <asp:TextBox ID="company_name" runat="server"></asp:TextBox>
                                        &nbsp;<asp:RegularExpressionValidator ID="reg_company_name" runat="server" 
                                            ControlToValidate="company_name" ErrorMessage="invalid name" 
                                            ValidationGroup="vg_company">
                        </asp:RegularExpressionValidator>
                                        &nbsp;<asp:RequiredFieldValidator ID="req_company_name" runat="server" 
                                            ControlToValidate="company_name" ErrorMessage="required" 
                                            ValidationGroup="vg_company">
                                            </asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        website</td>
                                    <td>
                                        :
                                        <asp:TextBox ID="company_website" runat="server"></asp:TextBox>
                                        &nbsp;<asp:RegularExpressionValidator ID="reg_company_website" runat="server" 
                                            ControlToValidate="company_website" ErrorMessage="invalid link" 
                                            ValidationGroup="vg_company">
                                       </asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 109px">
                                        address</td>
                                    <td style="width: 169px">
                                        :
                                        <asp:TextBox ID="company_addr_street" runat="server"></asp:TextBox>
                                        &nbsp;<asp:RegularExpressionValidator ID="reg_company_addr_street" runat="server" 
                                            ControlToValidate="company_addr_street" ErrorMessage="invalid address" 
                                            ValidationGroup="vg_company">
                                      </asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 109px">
                                        prov/state</td>
                                    <td style="width: 169px">
                                        :
                                        <asp:TextBox ID="company_prov" runat="server"></asp:TextBox>
                                        &nbsp;<asp:RegularExpressionValidator ID="reg_company_prov" runat="server" 
                                            ControlToValidate="company_prov" ErrorMessage="enter letters only " 
                                            ValidationGroup="vg_company">
                        </asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 109px">
                                        city</td>
                                    <td style="width: 169px">
                                        :
                                        <asp:TextBox ID="company_city" runat="server"></asp:TextBox>
                                        &nbsp;<asp:RegularExpressionValidator ID="reg_company_city" runat="server" 
                                            ControlToValidate="company_city" ErrorMessage="enter letters only" 
                                            ValidationGroup="vg_company">
                        </asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 109px">
                                        postal code</td>
                                    <td style="width: 169px">
                                        :
                                        <asp:TextBox ID="company_pc" runat="server"></asp:TextBox>
                                        &nbsp;<asp:RegularExpressionValidator ID="reg_company_pc" runat="server" 
                                            ControlToValidate="company_pc" ErrorMessage="invalid postal code" 
                                            ValidationGroup="vg_company">
                        </asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 109px">
                                        telephone</td>
                                    <td style="width: 169px">
                                        :
                                        <asp:TextBox ID="company_tel" runat="server"></asp:TextBox>
                                        &nbsp;<asp:RegularExpressionValidator ID="reg_company_tel" runat="server" 
                                            ControlToValidate="company_tel" ErrorMessage="invalid telephone" 
                                            ValidationGroup="vg_company">
                        </asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 109px">
                                        contact first name</td>
                                    <td style="width: 169px">
                                        :
                                        <asp:TextBox ID="company_contact_fname" runat="server"></asp:TextBox>
                                        &nbsp;<asp:RegularExpressionValidator ID="reg_company_contact_fname" runat="server" 
                                            ControlToValidate="company_contact_fname" ErrorMessage="invalid name" 
                                            ValidationGroup="vg_company">
                        </asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 109px">
                                        contact last name</td>
                                    <td style="width: 169px">
                                        :
                                        <asp:TextBox ID="company_contact_lname" runat="server"></asp:TextBox>
                                        &nbsp;<asp:RegularExpressionValidator ID="reg_company_contact_lname" runat="server" 
                                            ControlToValidate="company_contact_lname" ErrorMessage="invalid last name" 
                                            ValidationGroup="vg_company">
                        </asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 109px">
                                        contact tel.</td>
                                    <td style="width: 169px">
                                        :
                                        <asp:TextBox ID="company_contact_tel" runat="server"></asp:TextBox>
                                        &nbsp;<asp:RegularExpressionValidator ID="reg_company_contact_tel" runat="server" 
                                            ControlToValidate="company_contact_tel" ErrorMessage="invalid telephone" 
                                            ValidationGroup="vg_company">
                        </asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 109px">
                                        contact email</td>
                                    <td style="width: 169px">
                                        :
                                        <asp:TextBox ID="company_contact_email" runat="server"></asp:TextBox>
                                        &nbsp;<asp:RegularExpressionValidator ID="reg_company_contact_email" runat="server" 
                                            ControlToValidate="company_contact_email" ErrorMessage="invalid email" 
                                            ValidationGroup="vg_company">
                        </asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:Button ID="Button2" runat="server" onclick="Button1_Click" Text="submit" 
                                ValidationGroup="vg_company" />
                            &nbsp; &nbsp;&nbsp;
                            <asp:Button ID="btn_cancel0" runat="server" onclick="btn_cancel_Click" 
                                Text="Cancel" />
                            <br />
                        </div>
                        <div ID="result" runat="server">
                        </div>
                    </asp:Panel>
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 250px" valign="top">
                    <asp:Label ID="Label14" runat="server" 
                        Text="<%$ Resources:Resource, lbl_cost_estimation %>"></asp:Label>
                    &nbsp;</td>
                <td>
                    <asp:TextBox ID="tbx_task_cost_estimation" runat="server"></asp:TextBox>
                    &nbsp;<asp:RegularExpressionValidator ID="reg_tbx_task_cost_estimation" 
                        runat="server" ControlToValidate="tbx_task_cost_estimation" ErrorMessage="enter an amount">
                        </asp:RegularExpressionValidator>
                    <hr />
                </td>
            </tr>
            <tr>
                <td style="width: 250px" valign="top">
                    &nbsp;</td>
                <td align="right" bgcolor="aliceblue">
                    <asp:RegularExpressionValidator ID="reg_tbx_task_title" runat="server" 
                        ControlToValidate="tbx_task_title" ErrorMessage="invalid name" 
                        ValidationGroup="vg_task">
                        </asp:RegularExpressionValidator>
                    &nbsp;<asp:RequiredFieldValidator ID="req_tbx_task_title" runat="server" 
                        ControlToValidate="tbx_task_title" ErrorMessage="task name required" 
                        ValidationGroup="vg_task">
                    </asp:RequiredFieldValidator>
                    &nbsp;
                    <asp:Button ID="btn_submit" runat="server" onclick="btn_submit_Click" 
                     ValidationGroup="vg_task"
                        Text="<%$ Resources:Resource, btn_submit %>" style="height: 26px" />
                </td>
            </tr>
        </table>

    <asp:HiddenField  Visible="false" ID="hd_wo_id" runat="server" />
 
    
     <asp:Label ID="Label11" runat="server" Text="Label"></asp:Label>
    </asp:View> 
    </asp:MultiView>
   
<br />
    
</asp:Content>

