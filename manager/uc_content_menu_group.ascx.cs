using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class manager_uc_uc_content_menu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string group_menu = "";
        switch (System.IO.Path.GetFileName(Request.Path.ToString()))
        {
            case "group_modify.aspx":
            case "group_add.aspx":
              case "group_profile.aspx":
            case "group_remove.aspx":
                  group_menu += "<a href='group_add.aspx' class='small_link'>" + Resources.Resource.lbl_group_create + " </a>&nbsp;&nbsp;";
                  group_menu += "<a href='group_modify.aspx' class='small_link'>" + Resources.Resource.lbl_group_modify + " </a>&nbsp;&nbsp;";
                group_menu += "<a href='group_remove.aspx' class='small_link'>" + Resources.Resource.lbl_gl_remove + " </a>&nbsp;&nbsp;";
                break;
                case "name_profile.aspx":
                group_menu += "<a href='name_add.aspx' class='small_link'>" + Resources.Resource.lbl_group_create + " </a>&nbsp;&nbsp;";
                break;
                
                 }
        //menu += "<FORM method='get'><INPUT type='button' value='Help' class='letter' onClick='window.open('http://www.familyship.com/private/familyship_help.php?name=member_lineage','mywindow','width=300,height=400,scrollbars=yes,screenX=0,screenY=0')'></FORM>";
        content_group_menu.InnerHtml = group_menu;

    }
}
