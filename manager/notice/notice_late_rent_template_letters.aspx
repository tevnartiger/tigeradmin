﻿<%@ Page Title="" MaintainScrollPositionOnPostback="true" Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="notice_late_rent_template_letters.aspx.cs" Inherits="manager_notice_notice_late_rent_template_letters" %>

<%@ Register Assembly="obout_Editor" Namespace="OboutInc.Editor" TagPrefix="obout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <b><asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource,lbl_u_unpaid_rent_template%>" ></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

    </b>
    <br />
    <br />

     <table id="tb_successfull_confirmation" runat="server" style="width: 100%">
      <tr>
         <td style="color: #FF6600">
                            <b>
             <asp:Label ID="Label15" runat="server" Text="<%$ Resources:Resource, lbl_successfull_confirmation %>"></asp:Label></b></td>
      </tr>
   </table>
    
    <br />


    &nbsp;<br />
    <table >
        <tr>
            <td>


    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_letter_email %>"></asp:Label>
    :</td>
            <td>
    <asp:DropDownList ID="ddl_notice" AutoPostBack="true" runat="server" 
        onselectedindexchanged="ddl_notice_SelectedIndexChanged">
        <asp:ListItem Text="<%$ Resources:Resource, lbl_select_notice %>" Value="0"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:Resource, lbl_first_notice %>" Value="1"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:Resource, lbl_second_notice %>" Value="2"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:Resource, lbl_third_notice %>" Value="3"></asp:ListItem>
    </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_language" runat="server" Text="<%$ Resources:Resource, lbl_gl_language %>"></asp:Label>
            &nbsp;&nbsp;&nbsp;&nbsp; :</td>
            <td>
                <asp:DropDownList ID="ddl_language" runat="server"  AutoPostBack="true"
                    onselectedindexchanged="ddl_language_SelectedIndexChanged">
                    <asp:ListItem Value="en-US">English</asp:ListItem>
                    <asp:ListItem Value="fr-FR">Francais</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
    </table>
&nbsp;
  <table style="width: 90%">
                    <tr>
                        <td valign="top">
                <asp:Button ID="btn_save" runat="server" 
                    Text="<%$ Resources:Resource, btn_sauve %>" onclick="btn_save_Click"  />
                        </td>
                        <td valign="top">
                            <asp:Button ID="btn_revert" runat="server" 
                                Text="<%$ Resources:Resource, btn_revert %>" onclick="btn_revert_Click" />
          </td>
          <td valign="top">
          <asp:Button ID="btn_update" runat="server" onclick="btn_update_Click" Text="<%$ Resources:Resource, lbl_update %>" />
     </td>
    </tr>
 </table>

    <br />
    <obout:Editor  ModeSwitch="false" Submit="false"  ID="tbx_notice" Appearance="lite" runat="server">
    </obout:Editor>

</asp:Content>

