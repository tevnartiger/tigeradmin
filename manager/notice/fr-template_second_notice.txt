﻿Deuxieme avis

Date de l'avis : ((6))

Madame/Monsieur : ((5))

Votre loyer n'a pas été reçu à la date du présent avis. Comme indiqué sur votre contrat de location,
des frais de retard pourrait être sont ajoutés à la somme due .

Somme due         : ((1))
Frais de retard   : ((2))
Total             : ((3))

Cette question est grave et votre attention urgente est nécessaire. Le fait de ne pas agir promptement
pourrait conduire a des procédures d'expulsion. Si l'expulsion est demandée, vous pourriez pour être 
responsable de frais supplémentaires, tels les frais de cour et les honoraires d'avocat,votre cote de 
crédit pourrait aussi être touchée.

Cordialement, ((7))

PS. Ne pas tenir compte de ce préavis, si vous avez déjà envoyé votre paiement