﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="notice_delequency_send.aspx.cs" Inherits="manager_notice_notice_delequency_send" Title="Untitled Page" %>
<%@ Register Assembly="obout_Editor" Namespace="OboutInc.Editor" TagPrefix="obout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">




   
        <b><asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_u_notice_letter %>"></asp:Label></b>
    <br /><br />
    <asp:HyperLink ID="HyperLink1" Text="<%$ Resources:Resource, lbl_change_template %>" runat="server" 
            NavigateUrl="~/manager/notice/notice_late_rent_template_letters.aspx"></asp:HyperLink>
<br />
  
        <asp:Label ID="lbl_mail" runat="server"></asp:Label>
  
        <hr />
        


    <table >
        <tr>
            <td>


    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_letter_email %>"></asp:Label>
    :</td>
            <td>
    <asp:DropDownList ID="ddl_notice" AutoPostBack="true" runat="server" 
        onselectedindexchanged="ddl_notice_SelectedIndexChanged">
        <asp:ListItem Text="<%$ Resources:Resource, lbl_select_notice %>" Value="0"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:Resource, lbl_first_notice %>" Value="1"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:Resource, lbl_second_notice %>" Value="2"></asp:ListItem>
        <asp:ListItem Text="<%$ Resources:Resource, lbl_third_notice %>" Value="3"></asp:ListItem>
    </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_language" runat="server" Text="<%$ Resources:Resource, lbl_gl_language %>"></asp:Label>
            &nbsp;&nbsp;&nbsp;&nbsp; :</td>
            <td>
                <asp:DropDownList ID="ddl_language" runat="server"  AutoPostBack="true"
                    onselectedindexchanged="ddl_language_SelectedIndexChanged">
                    <asp:ListItem Value="en-US">English</asp:ListItem>
                    <asp:ListItem Value="fr-FR">Francais</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    &nbsp;
  
    <table id="tb_successfull_confirmation" runat="server" style="width: 100%">
      <tr>
         <td style="color: #FF6600">
                            <b>
             <asp:Label ID="Label15" runat="server" Text="<%$ Resources:Resource, lbl_successfull_confirmation %>"></asp:Label></b></td>
      </tr>
   </table>
                

  
    
      
   <asp:MultiView ID="MultiView1" runat="server">  
   <asp:View ID="View1" runat="server">
    <table id="tb_notice_1" visible="true" runat="server" style="width: 100%">
    
    <tr>
            <td>
                <table style="width: 60%">
                    <tr>
                        
                        <td>
                <asp:Label ID="lbl_notice_date" runat="server" ></asp:Label>
                        </td>
                    </tr>
                </table>
                
                
            </td>
        </tr>
        <tr>
            <td>
               
                <table style="width: 90%">
                    <tr>
                        <td valign="top">
                <asp:Button ID="btn_preview_1" runat="server" 
                    Text="<%$ Resources:Resource, btn_preview %>" onclick="btn_preview_1_Click" />
                        </td>
                        <td valign="top">
                            <asp:Label ID="Label22" runat="server" 
                    Text="<%$ Resources:Resource, lbl_send_email %>"></asp:Label>                
                            &nbsp;<asp:RadioButtonList ID="radio_send_mail_1" runat="server" 
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="0">No</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                </asp:RadioButtonList>
                        </td>
                        <td valign="top">
                            <asp:Button ID="btn_confirm" runat="server" onclick="btn_confirm_Click" Text="<%$ Resources:Resource, btn_confirm_notice %>" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
      
                <obout:Editor   Submit="false" ID="tbx_first_notice" Appearance="lite"  runat="server">
                </obout:Editor>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
    </table>
&nbsp;

    
    </asp:View>
    
    
    
    
    <asp:View ID="View2" runat="server">
    <table id="tb_notice_2" visible="true" runat="server" style="width: 100%">
    
    <tr>
            <td>
                <table style="width: 100%">
                    <tr>
                        
                        <td valign="top">
                <asp:Label ID="lbl_notice_date_2" runat="server" ></asp:Label>
                           
                        </td>
                    </tr>
                </table>
            
            </td>
        </tr>
        
        
        <tr>
            <td>
                <table style="width: 100%">
                    <tr>
                        <td valign="top">
                <asp:Button ID="btn_preview_2" runat="server" 
                    Text="<%$ Resources:Resource,btn_preview %>" onclick="btn_preview_2_Click" />
                        </td>
                        <td valign="top">
                            <asp:Label ID="Label21" runat="server" 
                    Text="<%$ Resources:Resource, lbl_send_email %>"></asp:Label>                
                &nbsp;<asp:RadioButtonList ID="radio_send_mail_2" runat="server" 
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="0">No</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                </asp:RadioButtonList>
                        </td>
                        <td valign="top">
                            <asp:Button ID="btn_confirm_2" runat="server" onclick="btn_confirm_2_Click" Text="<%$ Resources:Resource, btn_confirm_notice %>" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        
        <tr>
            <td>
               
                 <obout:Editor   Submit="false" ID="tbx_second_notice" Appearance="lite"  runat="server">
                </obout:Editor>
            </td>
        </tr>
        <tr>
            <td valign="top">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
              
                </td>
        </tr>
    </table>

    
    </asp:View>
    
    
    
    
    
    <asp:View ID="View3" runat="server">
      <table id="tb_notice_3" visible="true" runat="server" style="width: 100%">
    
    <tr>
            <td>
                <table style="width: 100%">
                    <tr>
                        <td>
                <asp:Label ID="lbl_notice_date_3" runat="server" ></asp:Label>
                        </td>
                    </tr>
                </table> 
            </td>
        </tr>
        
        
         <tr>
            <td>
             
                <table style="width: 60%">
                    <tr>
                        <td valign="top">
                <asp:Button ID="btn_preview_3" runat="server" 
                    Text="<%$ Resources:Resource,btn_preview %>" onclick="btn_preview_3_Click" />
                        </td>
                        <td valign="top">
                            &nbsp;&nbsp;<asp:Label ID="Label20" runat="server" 
                    Text="<%$ Resources:Resource, lbl_send_email %>"></asp:Label>                
                &nbsp;<asp:RadioButtonList ID="radio_send_mail_3" runat="server" 
                    RepeatDirection="Horizontal">
                    <asp:ListItem Value="0">No</asp:ListItem>
                    <asp:ListItem Value="1">Yes</asp:ListItem>
                </asp:RadioButtonList>
                
                        </td>
                        <td valign="top">
                <asp:Button ID="btn_confirm_3" runat="server" onclick="btn_confirm_3_Click" Text="<%$ Resources:Resource, btn_confirm_notice %>" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        
        
        <tr>
            <td>
            
                 <obout:Editor   Submit="false" ID="tbx_third_notice" Appearance="lite"  runat="server">
                </obout:Editor>
            </td>
        </tr>
        <tr>
            <td valign="top">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
    </table>
  
    
    </asp:View>
    
    
    
    
    
    <asp:View ID="View4" runat="server">
    
    <asp:Label ID="Label17" runat="server" Text="<%$ Resources:Resource, lbl_notice_rent %>"
     Font-Underline="true" ForeColor="Red" Font-Bold="false" Font-Size="Large"
      ></asp:Label>
    <br />
    
        <table id="tb_preview" runat="server" style="width: 60%">
            <tr>
                <td>
                    <asp:Label ID="lbl_preview" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    
    </asp:View>

   
   </asp:MultiView> 
    


    <asp:HiddenField ID="h_tu_id" runat="server" />
    <asp:HiddenField ID="h_rp_id" runat="server" />
    <asp:HiddenField ID="h_tenant_id" runat="server" />

    


</asp:Content>

