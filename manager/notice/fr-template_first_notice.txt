﻿Premier avis

Date de l'avis : ((6))

Madame/Monsieur : ((5))

Il s'agit d'un rappel amical que la totalité de votre loyer n'a pas été reçu à la date ci-dessus. 
Si c'est un oubli, s’il vous plaît envoyer votre paiement immédiatement afin d'éviter tout frais de
retard.

Rappelez-vous que le fait de payer votre loyer à temps est d'une grande importance, et que votre loyer
doit être reçu par mon bureau à la date d'échéance afin d'être à l'heure.

Merci pour votre prompte attention .

N'hésitez pas à me contacter si vous avez des questions ou des préoccupations.

((7))