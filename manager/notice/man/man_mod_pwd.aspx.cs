using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
    
public partial class manager_man_man_mod_pwd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        

    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        string temp_pwd = pwd.Text;
        string temp_user = Convert.ToString(Session["login_user"]);
        int temp_name_id = Convert.ToInt32(Session["name_id"]);
        string temp_salt = sinfoca.tiger.security.Hash.createSalt(50);
        string temp_hash = sinfoca.tiger.security.Hash.createPasswordHash(temp_pwd, temp_salt);
        
        //edit tlogin
        tiger.security.Conn c = new tiger.security.Conn(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        switch(c.modifyPassword(temp_user,temp_pwd,temp_name_id,Convert.ToString(Request.UserHostAddress)))
        {
            case 0:
                lbl_message.Text = "Password has been modified";
                break;
            case 1:
                lbl_message.Text = "Password was not changeg";
                break;
            case 2:
                lbl_message.Text = "User doesn't exist";
                break;
        
        }
    }
}
