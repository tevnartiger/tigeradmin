<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="man_mod_pwd.aspx.cs" Inherits="manager_man_man_mod_pwd" Title="Untitled Page" %>
<%@ Register Src="~/manager/uc/uc_content_menu.ascx" TagName="uc_content_menu" TagPrefix="uc_content_menu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h2><asp:Literal ID="lt" runat="server" Text="<%$ resources:resource, lbl_mod_pwd %>" /></h2>

    
  <div style="float:left;width:200;text-align:left;">
  <asp:Literal ID="sf" runat="server" Text="<%$ resources:resource, lbl_type_pwd %>" /><br /><asp:TextBox ID="pwd" TextMode="Password" runat="server" />
  <asp:RequiredFieldValidator ID="req_pwd" runat="server" ControlToValidate="pwd" ErrorMessage="<%$ Resources:resource, iv_required %>"></asp:RequiredFieldValidator>
    <br /><br />
    <asp:Literal ID="Literal1" runat="server" Text="<%$ resources:resource, lbl_retype_pwd %>" /><br /><asp:TextBox ID="pwd_repeat" TextMode="password" runat="server" />
    
    <asp:CompareValidator ID="comp_pwd_repeat" ControlToValidate="pwd_repeat" ControlToCompare="pwd" runat="server" ErrorMessage="<%$ Resources:resource, iv_pwd_compare %>"></asp:CompareValidator>
    <br /><br />
    <asp:Button ID="btn_submit" runat="server" 
         Text="<%$ resources:resource, btn_mod_pwd %>" onclick="btn_submit_Click" />
    <br />
    <br />
    <asp:Label ID="lbl_message" runat="server" />
    </div>
    
</asp:Content>

