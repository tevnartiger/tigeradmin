﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.IO;
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : oct , 2007
/// </summary>
public partial class manager_appliance_appliance_add : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if ((!Page.IsPostBack))
        {
            // setting the validation expression

            reg_appliance_desc.ValidationExpression = RegEx.getText();
            reg_appliance_invoice_no.ValidationExpression = RegEx.getAlphaNumeric();
            reg_appliance_name.ValidationExpression = RegEx.getText();
            reg_appliance_purchase_cost.ValidationExpression = RegEx.getMoney();
            reg_appliance_serial_no.ValidationExpression = RegEx.getAlphaNumeric();
           

            // we get the list of warehouse available
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            lbl_select_warehouse.Visible = false;

            //get the list of warehouse 
            tiger.Warehouse warehouse = new tiger.Warehouse(strconn);
            ddl_warehouse_list.DataSource = warehouse.getWarehouseList(Convert.ToInt32(Session["schema_id"]));
            ddl_warehouse_list.DataBind();

            // if there is no warehouse in the database render the control not visible
            if (ddl_warehouse_list.Items.Count == 0)
            {
                ddl_warehouse_list.Visible = false;
                chk_warehouse.Visible = false;
            }
            else
            {
                ddl_warehouse_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_select, "-1"));
                ddl_warehouse_list.SelectedIndex = 0;
            }


            //by default we wanna put a new appliance in a property
            ddl_warehouse_list.Enabled = false;

            //by default we put new appliance in property therefore the property checkbox is selected
            chk_home.Checked = true;

            DateTime date = new DateTime();
            date = DateTime.Now;

            ddl_ua_dateadd_m.SelectedValue = date.Month.ToString();
            ddl_ua_dateadd_d.SelectedValue = date.Day.ToString();
            ddl_ua_dateadd_y.SelectedValue = date.Year.ToString();



            ddl_appliance_date_purchase_m.SelectedValue = date.Month.ToString();
            ddl_appliance_date_purchase_d.SelectedValue = date.Day.ToString();
            ddl_appliance_date_purchase_y.SelectedValue = date.Year.ToString();


            
            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prApplianceShortCategList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                //    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                ddl_appliance_categ.DataSource = ds;

                string lang = "";

                if (Session["_lastCulture"].ToString() == "fr-FR")
                    lang = "ac_name_fr";

                if (Session["_lastCulture"].ToString() == "en-US")
                    lang = "ac_name_en";

                ddl_appliance_categ.DataTextField = lang;
                ddl_appliance_categ.DataBind();
                ddl_appliance_categ.Items.Insert(0, Resources.Resource.lbl_select);
            }
            finally
            {
                conn.Close();
            }
            // ----------------------------------------------------------------------

            tiger.Supplier s = new tiger.Supplier(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            ddl_supplier_list.DataSource = s.getSupplierList(Convert.ToInt32(Session["schema_id"]));

            ddl_supplier_list.DataBind();
            ddl_supplier_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_select, "0"));
            

            /// -------------------------------------------------------------------------------------------------

            // List of Houses and Units in each house
            tiger.Home h = new tiger.Home(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            int home_count = h.getHomeCount(Convert.ToInt32(Session["schema_id"]));

            ddl_appliance_home_id.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
            ddl_appliance_home_id.DataBind();

            if (home_count > 0)
            {
                int home_id = h.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));


                ddl_appliance_home_id.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                ddl_appliance_home_id.SelectedValue = Convert.ToString(home_id);
                //  ddl_appliance_home_id.DataBind();

                //ddl_appliance_categ.Items.

                //Manage request unit_id
                //if unit_id doesn't exist
                //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);

                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

                if (unit_count > 0)
                {

                    int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), home_id);
                  
                    ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
                    ddl_unit_id.DataBind();
                    ddl_unit_id.Items.Add(new ListItem(Resources.Resource.lbl_storage_unit, "-1"));
                    // ddl_unit_id.SelectedValue = Convert.ToString(unit_id);


                }


            }
     

        }

    }
    protected void ddl_appliance_home_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        //get home_id
        //    temp_home_id.Value = ddl_appliance_home_id.SelectedValue;
        //get first unit_id of home

        tiger.Unit u = new tiger.Unit(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        int unit_id = u.getUnitFirstId(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_appliance_home_id.SelectedValue));
        //change list of unit id

        if (unit_id > 0)
        {
            ddl_unit_id.Visible = true;
            //first dispose of old ddl_unit
            ddl_unit_id.Dispose();
            ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_appliance_home_id.SelectedValue));
            ddl_unit_id.DataBind();
            ddl_unit_id.Items.Add(new ListItem(Resources.Resource.lbl_storage_unit, "-1"));
            //ddl_unit_id.SelectedValue = Convert.ToString(unit_id);

        }
    }
    protected void btn_add_appliance_Click(object sender, EventArgs e)
    {
      if( chk_warehouse.Checked == true || chk_home.Checked == true)    
     { 
          string invoice_photo_name ="";
          string appliance_photo_name ="";
          bool continu = true;

          // if the warehouse checkbox is selected , the user must choose a warehouse from
          // the warehouse dropdownlist
          if (chk_warehouse.Checked == true)
          {
              if (ddl_warehouse_list.SelectedIndex == 0)
              {
                  continu = false;
                  lbl_select_warehouse.Visible = true;
              }
          }

          Page.Validate();
          if (Page.IsValid && continu == true)         
          {
                lbl_select_warehouse.Visible = false;

                string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

                SqlConnection conn = new SqlConnection(strconn);
                SqlCommand cmd = new SqlCommand("prApplianceAdd", conn);
                cmd.CommandType = CommandType.StoredProcedure;

              //  try
                {
                    conn.Open();
                    //Add the params

                    DateTime date_purchase = new DateTime();
                    DateTime date_add = new DateTime();
                    tiger.Date df = new tiger.Date();

                    cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                    cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
                    cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
        

                    if (chk_home.Checked == true)
                    {
                        cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_appliance_home_id.SelectedValue);
                        cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_unit_id.SelectedValue);
                        cmd.Parameters.Add("@warehouse_id", SqlDbType.Int).Value = DBNull.Value;
                    }


                    if (chk_warehouse.Checked == true)
                    {
                        cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = DBNull.Value;
                        cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = DBNull.Value;
                        cmd.Parameters.Add("@warehouse_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_warehouse_list.SelectedValue);
                    }

                    date_add = Convert.ToDateTime(df.DateCulture(ddl_ua_dateadd_m.SelectedValue, ddl_ua_dateadd_d.SelectedValue, ddl_ua_dateadd_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));

                    cmd.Parameters.Add("@ua_dateadd", SqlDbType.SmallDateTime).Value = date_add;

                     cmd.Parameters.Add("@ac_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_appliance_categ.SelectedValue);

 
                    cmd.Parameters.Add("@company_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_supplier_list.SelectedValue);
                    cmd.Parameters.Add("@appliance_name", SqlDbType.VarChar, 50).Value = appliance_name.Text;
                    cmd.Parameters.Add("@appliance_invoice_no", SqlDbType.VarChar, 50).Value = appliance_invoice_no.Text;
                    cmd.Parameters.Add("@appliance_invoice_photo_name", SqlDbType.VarChar, 300).Value = invoice_photo_name;


                    string filename = Path.GetFileName(appliance_photo.PostedFile.FileName);
                    int filesize = 0;
                    if (appliance_photo.HasFile)
                    {

                        Stream stream = appliance_photo.PostedFile.InputStream;
                        filesize = appliance_photo.PostedFile.ContentLength;
                        byte[] filedata = new byte[filesize];
                        stream.Read(filedata, 0, filesize);

                     //   if(filesize <= 5000) // max file size is 50K
                         cmd.Parameters.Add("@appliance_photo", SqlDbType.Image).Value = filedata;
                       // else
                        //    cmd.Parameters.Add("@appliance_photo", SqlDbType.Image).Value = DBNull.Value;

                    }
                    else
                        cmd.Parameters.Add("@appliance_photo", SqlDbType.Image).Value = DBNull.Value;







                    if (appliance_invoice_photo.HasFile)
                    {
                        invoice_photo_name = appliance_invoice_photo.PostedFile.FileName;
                        int last_index_a = invoice_photo_name.LastIndexOf('\\');
                        invoice_photo_name = invoice_photo_name.Substring(last_index_a + 1);

                        Stream stream = appliance_invoice_photo.PostedFile.InputStream;
                        filesize = appliance_invoice_photo.PostedFile.ContentLength;
                        byte[] filedata = new byte[filesize];
                        stream.Read(filedata, 0, filesize);

                       // if (filesize <= 5000) // max file size is 50K
                            cmd.Parameters.Add("@appliance_invoice_photo", SqlDbType.Image).Value = filedata;
                       // else
                         //   cmd.Parameters.Add("@appliance_invoice_photo", SqlDbType.Image).Value = DBNull.Value;
                    }
                    else
                        cmd.Parameters.Add("@appliance_invoice_photo", SqlDbType.Image).Value = DBNull.Value;

                                  
                    
                    cmd.Parameters.Add("@appliance_desc", SqlDbType.VarChar, 50).Value = appliance_desc.Text;
                    cmd.Parameters.Add("@appliance_purchase_cost", SqlDbType.Money).Value = Convert.ToDecimal(appliance_purchase_cost.Text);
                    cmd.Parameters.Add("@appliance_serial_no", SqlDbType.VarChar, 50).Value = appliance_serial_no.Text;


                   
                    date_purchase= Convert.ToDateTime(df.DateCulture(ddl_appliance_date_purchase_m.SelectedValue,ddl_appliance_date_purchase_d.SelectedValue,ddl_appliance_date_purchase_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));
                    cmd.Parameters.Add("@appliance_date_purchase", SqlDbType.DateTime).Value = date_purchase;


                    cmd.Parameters.Add("@appliance_guarantee_year", SqlDbType.Int).Value = Convert.ToInt32(ddl_appliance_guarantee_year.SelectedValue) ;
                    cmd.Parameters.Add("@appliance_photo_name", SqlDbType.VarChar, 300).Value = appliance_photo_name;

            
            
            
            
                    //execute the insert
                    cmd.ExecuteReader();
                    //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

                    if (Convert.ToInt32(cmd.Parameters["@return_id"].Value) == 0)
                    {
                        lbl_success.Text =Resources.Resource.lbl_successfull_add;
                    }
          

                }
                conn.Close();

        }

     }
    }
    protected void chk_warehouse_CheckedChanged(object sender, EventArgs e)
    {
        lbl_success.Text = "";
                   
        if (chk_warehouse.Checked == true)
        {
            chk_home.Checked = false;
            ddl_appliance_home_id.Enabled = false;
            ddl_unit_id.Enabled = false;

            ddl_warehouse_list.Enabled = true;
        }

    }
    protected void chk_home_CheckedChanged(object sender, EventArgs e)
    {
        lbl_success.Text = "";
        lbl_select_warehouse.Visible = false;

        if (chk_home.Checked == true)
        {
            chk_warehouse.Checked = false;
            ddl_warehouse_list.Enabled = false;

            ddl_appliance_home_id.Enabled =true;
            ddl_unit_id.Enabled = true;
        }
    }
}
