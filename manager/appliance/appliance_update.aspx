﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="appliance_update.aspx.cs" Inherits="manager_appliance_appliance_update" Title="Untitled Page" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
<style type="text/css">

 div.row {float: left; margin:  0px 0px 8px 0px; padding: 0; width: 98%;}
 div.col500px {float: left; width: 500px; margin: 0 3px 0 0; padding: 0;}
 div.col65em {float: left; width:65em; margin: 0 3px 0 0; padding: 0;}


</style>
</asp:Content>
 
 
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="top_row" class="row">
<div class="col65em">     
 <div class="myform">
        <h1>Edit Item</h1>
        <hr/>
   </div>
  </div>
</div>


<table style="width: 100%">
                         <tr>
                             <td>

<table >
        <tr>
                <td style="width: 167px" align="right" >
                    <asp:Label ID="Label11" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_supplier %>"  />  </td>
                <td style="width: 279px">
                    <asp:DropDownList ID="ddl_supplier_list"  DataTextField="company_name" DataValueField="company_id" runat="server">
                    </asp:DropDownList>
                    <span style="font-size: 7pt">&nbsp; 
                        </span></td>
            </tr>
            
            <tr>
        <td "align="right" style="width: 167px" align="right"><strong><asp:Label ID="Label18" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_property %>"  /></strong></td>
        <td style="width: 279px">
            <asp:DropDownList ID="ddl_appliance_home_id" runat="server"  
                DataTextField="home_name" DataValueField="home_id" AutoPostBack="True" 
                onselectedindexchanged="ddl_appliance_home_id_SelectedIndexChanged">
        </asp:DropDownList>&nbsp;
            <asp:CheckBox ID="chk_home" Text="" runat="server" 
               AutoPostBack="true"  oncheckedchanged="chk_home_CheckedChanged" />
       </td>
        </tr>
            <tr>
        <td valign="top" align="right" style="width: 167px"><strong><asp:Label ID="Label4" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_unit %>"  />  </strong></td>
        <td valign="top" style="width: 279px">
            <asp:DropDownList ID="ddl_unit_id" DataTextField="unit_door_no" DataValueField="unit_id" runat="server">
            </asp:DropDownList>
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        </td>
        </tr>
        
        
        <tr>
        <td align="right" style="width: 167px"><b><asp:Label ID="Label5" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_external_storage %>"  />
            <br />
            <asp:Label 
                    ID="lbl_select_warehouse" runat="server" Text="select a warehouse" 
                    style="color: #FF0000"></asp:Label>
            </b></td>
        <td style="width: 279px">
            <asp:DropDownList    ID="ddl_warehouse_list" DataValueField="warehouse_id"
                                DataTextField="warehouse_name" runat="server">
            </asp:DropDownList>
            &nbsp;
            <asp:CheckBox ID="chk_warehouse" runat="server" 
               AutoPostBack="true"  oncheckedchanged="chk_warehouse_CheckedChanged" />
            &nbsp;</td>
        <td>
            &nbsp;</td>
        </tr>  
        
            <tr>
        <td style="width: 167px" align="right"><asp:Label ID="Label6" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_date_add %>"  /></td>
        <td style="width: 279px">
             
                 <asp:DropDownList ID="ddl_ua_dateadd_m" runat="server">
                 <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                                     <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                
                 </asp:DropDownList>&nbsp; /&nbsp;<asp:DropDownList ID="ddl_ua_dateadd_d" runat="server">
                     <asp:ListItem Text="<%$ Resources:Resource, txt_day %>" Value="0"></asp:ListItem>
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>&nbsp; / 
                 <asp:DropDownList ID="ddl_ua_dateadd_y" runat="server" >
                     <asp:ListItem Text="<%$ Resources:Resource, txt_year %>" Value="0"></asp:ListItem>
                     <asp:ListItem>1960</asp:ListItem>
                     <asp:ListItem>1961</asp:ListItem>
                     <asp:ListItem>1962</asp:ListItem>
                     <asp:ListItem>1963</asp:ListItem>
                     <asp:ListItem>1964</asp:ListItem>
                     <asp:ListItem>1965</asp:ListItem>
                     <asp:ListItem>1966</asp:ListItem>
                     <asp:ListItem>1967</asp:ListItem>
                     <asp:ListItem>1968</asp:ListItem>
                     <asp:ListItem>1969</asp:ListItem>
                     <asp:ListItem>1970</asp:ListItem>
                     <asp:ListItem>1972</asp:ListItem>
                     <asp:ListItem>1973</asp:ListItem>
                     <asp:ListItem>1974</asp:ListItem>
                     <asp:ListItem>1975</asp:ListItem>
                     <asp:ListItem>1976</asp:ListItem>
                     <asp:ListItem>1977</asp:ListItem>
                     <asp:ListItem>1978</asp:ListItem>
                     <asp:ListItem>1979</asp:ListItem>
                     <asp:ListItem>1980</asp:ListItem>
                     <asp:ListItem>1981</asp:ListItem>
                     <asp:ListItem>1982</asp:ListItem>
                     <asp:ListItem>1983</asp:ListItem>
                     <asp:ListItem>1984</asp:ListItem>
                     <asp:ListItem>1985</asp:ListItem>
                     <asp:ListItem>1986</asp:ListItem>
                     <asp:ListItem>1987</asp:ListItem>
                     <asp:ListItem>1988</asp:ListItem>
                     <asp:ListItem>1989</asp:ListItem>
                     <asp:ListItem>1990</asp:ListItem>
                     <asp:ListItem>1991</asp:ListItem>
                     <asp:ListItem>1992</asp:ListItem>
                     <asp:ListItem>1993</asp:ListItem>
                     <asp:ListItem>1994</asp:ListItem>
                     <asp:ListItem>1995</asp:ListItem>
                     <asp:ListItem>1996</asp:ListItem>
                     <asp:ListItem>1997</asp:ListItem>
                     <asp:ListItem>1998</asp:ListItem>
                     <asp:ListItem>1999</asp:ListItem>
                     <asp:ListItem>2000</asp:ListItem>
                     <asp:ListItem>2001</asp:ListItem>
                     <asp:ListItem>2002</asp:ListItem>
                     <asp:ListItem>2003</asp:ListItem>
                     <asp:ListItem>2004</asp:ListItem>
                     <asp:ListItem>2005</asp:ListItem>
                     <asp:ListItem>2006</asp:ListItem>
                     <asp:ListItem>2007</asp:ListItem>
                     <asp:ListItem>2008</asp:ListItem>
                     <asp:ListItem>2009</asp:ListItem>
                     <asp:ListItem>2010</asp:ListItem>
                     <asp:ListItem>2011</asp:ListItem>
                     <asp:ListItem>2012</asp:ListItem>
                     <asp:ListItem>2013</asp:ListItem>
                     <asp:ListItem>2014</asp:ListItem>
                     <asp:ListItem>2015</asp:ListItem>
                 </asp:DropDownList>
        </td>
        </tr>
            </table>
                                   </td>
                                   <td valign="top">
            <asp:GridView ID="gv_appliance_previous_storage" runat="server" 
            AlternatingRowStyle-BackColor="Beige" 
                AutoGenerateColumns="false" BorderColor="White" BorderWidth="3" 
                EmptyDataText='<%$ Resources:Resource, lbl_no_data %>' GridLines="Both" HeaderStyle-BackColor="AliceBlue" 
                 Width="100%" >
                <Columns>
                  <asp:BoundField DataField="storage_name" HeaderText="Previously Stored"/>  
               
                 <asp:BoundField DataField="unit_door_no" HeaderText='<%$ Resources:Resource, lbl_unit %>' />
                <asp:BoundField DataField="ua_dateadd" HeaderText="Add" DataFormatString="{0:M-dd-yyyy}"  
                   HtmlEncode="false" />
               <asp:BoundField DataField="ua_date_remove" HeaderText="Removed" DataFormatString="{0:M-dd-yyyy}"  
                     HtmlEncode="false"/>
                    
                        
                </Columns>
            </asp:GridView>
                    
                    
                                   </td>
                               </tr>
                           </table>
                      <asp:Label ID="Label1" runat="server"></asp:Label>
        <br />
        
        <br />
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
 <div id="Div1" class="row">
<div class="col65em">     
 <div class="myform">
 
 
 
 
       <div id="Div3" class="row">
        <div class="col65em">
        <label ><asp:Label ID="Label7" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_category %>"  />
        <span class="small"><asp:RequiredFieldValidator ID="req_appliance_categ"  ControlToValidate="ddl_appliance_categ"
                InitialValue="<%$ Resources:Resource, lbl_select %>"
                runat="server" ErrorMessage="select a category">
                </asp:RequiredFieldValidator>
        </span>
        </label>&nbsp; 
         <asp:DropDownList ID="ddl_appliance_categ" runat="server"   DataValueField="ac_id">
        </asp:DropDownList>
        </div>       
        </div>

 
 
 
 
 
 
 
 
        <div id="Div2" class="row">
        <div class="col65em">
        <label ><asp:Label ID="Label8" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_appliance_name %>"  />
        <span class="small"><asp:RegularExpressionValidator ID="reg_appliance_name" runat="server"  ControlToValidate="appliance_name"
                    ErrorMessage="invalid name"></asp:RegularExpressionValidator>&nbsp;<asp:RequiredFieldValidator 
                    ID="req_appliance_name" runat="server"  ControlToValidate="appliance_name"
                    ErrorMessage="required"></asp:RequiredFieldValidator>
        </span>
        </label>&nbsp; 
          <asp:TextBox ID="appliance_name" runat="server"></asp:TextBox>
        </div>       
        </div>
        
        
        
        
        
        
        
        <div id="Div4" class="row">
        <div class="col65em">
        <label ><asp:Label ID="Label9" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_invoice_no %>"  />
        <span class="small"><asp:RegularExpressionValidator 
                    ID="reg_appliance_invoice_no" runat="server"  ControlToValidate="appliance_invoice_no"
                    ErrorMessage="invalid invoice # , only numbers or letters"></asp:RegularExpressionValidator>
        </span>
        </label>&nbsp; 
         <asp:TextBox ID="appliance_invoice_no" runat="server"></asp:TextBox>
        </div>       
        </div>
 
 
 
 
 
 
 
       <div id="Div5" class="row">
        <div class="col65em">
        <label ><asp:Label ID="Label10" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_invoice_photo %>"  />
        <span class="small">
        </span>
        </label>&nbsp; 
        &nbsp;<asp:Repeater runat="server" ID="rApplianceInvoiceImage"><ItemTemplate>
                                        <asp:Image ID="appliance_invoice_photo0" runat="server" Height="270" 
                                            ImageUrl='<%# "~/mediahandler/ApplianceInvoiceImage.ashx?app_id="+ Eval("appliance_id") %>' 
                                            Width="300" /></ItemTemplate>
               </asp:Repeater>
        </div>       
        </div>
 
 
 
 
 
 
 
 
       <div id="Div6" class="row">
        <div class="col65em">
        <label ><asp:Label ID="Label20" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_invoice_photo %>"  />
        <span class="small">
        </span>
        </label>&nbsp; 
           <asp:FileUpload ID="appliance_invoice_photo" runat="server" />
        </div>       
        </div>
 
 
 
 
 
 
        <div id="Div7" class="row">
        <div class="col65em">
        <label ><asp:Label ID="Label12" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_cost %>"  />
        <span class="small"> <asp:RegularExpressionValidator 
                    ID="reg_appliance_purchase_cost" runat="server"  ControlToValidate="appliance_purchase_cost"
                    ErrorMessage="invalid , must be numeric"></asp:RegularExpressionValidator>
        </span>
        </label>&nbsp; 
           <asp:TextBox ID="appliance_purchase_cost" runat="server"></asp:TextBox>
        </div>       
        </div>
 
 
 
 
 
 
        <div id="Div8" class="row">
        <div class="col65em">
        <label ><asp:Label ID="Label13" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_description %>"  />
        <span class="small"> <asp:RegularExpressionValidator 
                    ID="reg_appliance_desc" runat="server"  ControlToValidate="appliance_desc"
                    ErrorMessage="only text allowed"></asp:RegularExpressionValidator>
        </span>
        </label>&nbsp; 
           <asp:TextBox ID="appliance_desc" runat="server" TextMode="MultiLine" Height="90px" Width="281px"></asp:TextBox>
        </div>       
        </div>
        
        
        
        
        
        
        
        
        
        
        
        <div id="Div9" class="row">
        <div class="col65em">
        <label ><asp:Label ID="Label14" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_serial_no %>"  />
        <span class="small">  <asp:RegularExpressionValidator 
                    ID="reg_appliance_serial_no" runat="server"  ControlToValidate="appliance_serial_no"
                    ErrorMessage="invalid invoice # , only numbers or letters"></asp:RegularExpressionValidator>
        </span>
        </label>&nbsp; 
            <asp:TextBox ID="appliance_serial_no" runat="server"></asp:TextBox>
        </div>       
        </div>
        
        
        
        
        
        
        
        
        
        
        <div id="Div10" class="row">
        <div class="col65em">
        <label > <asp:Label ID="Label15" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_purchase_date %>"  />
        <span class="small">  
        </span>
        </label>&nbsp; 
             <asp:DropDownList ID="ddl_appliance_date_purchase_m" runat="server">
                 <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                                     <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                
                 </asp:DropDownList>&nbsp; /&nbsp;<asp:DropDownList 
                        ID="ddl_appliance_date_purchase_d" runat="server">
                     <asp:ListItem Text="<%$ Resources:Resource, txt_day %>"  Value="0"></asp:ListItem>
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>&nbsp; / 
                 <asp:DropDownList ID="ddl_appliance_date_purchase_y" runat="server" >
                     <asp:ListItem Text="<%$ Resources:Resource, txt_year %>"  Value="0"></asp:ListItem>
                     <asp:ListItem>1960</asp:ListItem>
                     <asp:ListItem>1961</asp:ListItem>
                     <asp:ListItem>1962</asp:ListItem>
                     <asp:ListItem>1963</asp:ListItem>
                     <asp:ListItem>1964</asp:ListItem>
                     <asp:ListItem>1965</asp:ListItem>
                     <asp:ListItem>1966</asp:ListItem>
                     <asp:ListItem>1967</asp:ListItem>
                     <asp:ListItem>1968</asp:ListItem>
                     <asp:ListItem>1969</asp:ListItem>
                     <asp:ListItem>1970</asp:ListItem>
                     <asp:ListItem>1972</asp:ListItem>
                     <asp:ListItem>1973</asp:ListItem>
                     <asp:ListItem>1974</asp:ListItem>
                     <asp:ListItem>1975</asp:ListItem>
                     <asp:ListItem>1976</asp:ListItem>
                     <asp:ListItem>1977</asp:ListItem>
                     <asp:ListItem>1978</asp:ListItem>
                     <asp:ListItem>1979</asp:ListItem>
                     <asp:ListItem>1980</asp:ListItem>
                     <asp:ListItem>1981</asp:ListItem>
                     <asp:ListItem>1982</asp:ListItem>
                     <asp:ListItem>1983</asp:ListItem>
                     <asp:ListItem>1984</asp:ListItem>
                     <asp:ListItem>1985</asp:ListItem>
                     <asp:ListItem>1986</asp:ListItem>
                     <asp:ListItem>1987</asp:ListItem>
                     <asp:ListItem>1988</asp:ListItem>
                     <asp:ListItem>1989</asp:ListItem>
                     <asp:ListItem>1990</asp:ListItem>
                     <asp:ListItem>1991</asp:ListItem>
                     <asp:ListItem>1992</asp:ListItem>
                     <asp:ListItem>1993</asp:ListItem>
                     <asp:ListItem>1994</asp:ListItem>
                     <asp:ListItem>1995</asp:ListItem>
                     <asp:ListItem>1996</asp:ListItem>
                     <asp:ListItem>1997</asp:ListItem>
                     <asp:ListItem>1998</asp:ListItem>
                     <asp:ListItem>1999</asp:ListItem>
                     <asp:ListItem>2000</asp:ListItem>
                     <asp:ListItem>2001</asp:ListItem>
                     <asp:ListItem>2002</asp:ListItem>
                     <asp:ListItem>2003</asp:ListItem>
                     <asp:ListItem>2004</asp:ListItem>
                     <asp:ListItem>2005</asp:ListItem>
                     <asp:ListItem>2006</asp:ListItem>
                     <asp:ListItem>2007</asp:ListItem>
                     <asp:ListItem>2008</asp:ListItem>
                     <asp:ListItem>2009</asp:ListItem>
                     <asp:ListItem>2010</asp:ListItem>
                     <asp:ListItem>2011</asp:ListItem>
                     <asp:ListItem>2012</asp:ListItem>
                     <asp:ListItem>2013</asp:ListItem>
                     <asp:ListItem>2014</asp:ListItem>
                     <asp:ListItem>2015</asp:ListItem>
                 </asp:DropDownList>
                             
        </div>       
        </div>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        <div id="Div11" class="row">
        <div class="col65em">
        <label > <asp:Label ID="Label16" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_garanty_year %>"  />
        <span class="small">
        </span>
        </label>&nbsp; 
            <asp:DropDownList ID="ddl_appliance_guarantee_year" runat="server">
                        <asp:ListItem Text="<%$ Resources:Resource, lbl_select %>"  Value="0"></asp:ListItem>
                        <asp:ListItem>1960</asp:ListItem>
                        <asp:ListItem>1961</asp:ListItem>
                        <asp:ListItem>1962</asp:ListItem>
                        <asp:ListItem>1963</asp:ListItem>
                        <asp:ListItem>1964</asp:ListItem>
                        <asp:ListItem>1965</asp:ListItem>
                        <asp:ListItem>1966</asp:ListItem>
                        <asp:ListItem>1967</asp:ListItem>
                        <asp:ListItem>1968</asp:ListItem>
                        <asp:ListItem>1969</asp:ListItem>
                        <asp:ListItem>1970</asp:ListItem>
                        <asp:ListItem>1971</asp:ListItem>
                        <asp:ListItem>1972</asp:ListItem>
                        <asp:ListItem>1973</asp:ListItem>
                        <asp:ListItem>1974</asp:ListItem>
                        <asp:ListItem>1975</asp:ListItem>
                        <asp:ListItem>1976</asp:ListItem>
                        <asp:ListItem>1977</asp:ListItem>
                        <asp:ListItem>1978</asp:ListItem>
                        <asp:ListItem>1979</asp:ListItem>
                        <asp:ListItem>1980</asp:ListItem>
                        <asp:ListItem>1981</asp:ListItem>
                        <asp:ListItem>1982</asp:ListItem>
                        <asp:ListItem>1983</asp:ListItem>
                        <asp:ListItem>1984</asp:ListItem>
                        <asp:ListItem>1985</asp:ListItem>
                        <asp:ListItem>1986</asp:ListItem>
                        <asp:ListItem>1987</asp:ListItem>
                        <asp:ListItem>1988</asp:ListItem>
                        <asp:ListItem>1989</asp:ListItem>
                        <asp:ListItem>1990</asp:ListItem>
                        <asp:ListItem>1991</asp:ListItem>
                        <asp:ListItem>1992</asp:ListItem>
                        <asp:ListItem>1992</asp:ListItem>
                        <asp:ListItem>1994</asp:ListItem>
                        <asp:ListItem>1995</asp:ListItem>
                        <asp:ListItem>1996</asp:ListItem>
                        <asp:ListItem>1997</asp:ListItem>
                        <asp:ListItem>1998</asp:ListItem>
                        <asp:ListItem>1999</asp:ListItem>
                        <asp:ListItem>2000</asp:ListItem>
                        <asp:ListItem>2001</asp:ListItem>
                        <asp:ListItem>2002</asp:ListItem>
                        <asp:ListItem>2003</asp:ListItem>
                        <asp:ListItem>2004</asp:ListItem>
                        <asp:ListItem>2005</asp:ListItem>
                        <asp:ListItem>2006</asp:ListItem>
                        <asp:ListItem>2007</asp:ListItem>
                        <asp:ListItem>2008</asp:ListItem>
                        <asp:ListItem>2009</asp:ListItem>
                        <asp:ListItem>2010</asp:ListItem>
                        <asp:ListItem>2012</asp:ListItem>
                        <asp:ListItem>2013</asp:ListItem>
                        <asp:ListItem>2014</asp:ListItem>
                        <asp:ListItem>2015</asp:ListItem>
                        <asp:ListItem>2016</asp:ListItem>
                        <asp:ListItem>2017</asp:ListItem>
                        <asp:ListItem>2018</asp:ListItem>
                        <asp:ListItem>2019</asp:ListItem>
                        <asp:ListItem>2020</asp:ListItem>
                    </asp:DropDownList>
        </div>       
        </div>
        
 
 
 
 
 
 
 
 
 
 
 
 
     
        <div id="Div12" class="row">
        <div class="col65em">
        <label ><asp:Label ID="Label17" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_appliance_picture %>"  />
        <span class="small"> 
        </span>
        </label>&nbsp;<asp:Repeater runat="server" 
                        ID="rApplianceImage"><ItemTemplate>
                                        <asp:Image ID="appliance_photo" 
                                            runat="server" Height="270" 
                                            ImageUrl='<%# "~/mediahandler/ApplianceImage.ashx?app_id="+ Eval("appliance_id")%>' 
                                            Width="300" /></ItemTemplate>
                               </asp:Repeater>
        </div>       
        </div>
        
 
 
 
       <div id="Div13" class="row">
        <div class="col65em">
        <label ><asp:Label ID="Label2" runat="server" 
                                   Text="<%$ Resources:Resource, lbl_appliance_picture %>"  />
        <span class="small"> 
        </span>
        </label>&nbsp;<asp:FileUpload ID="appliance_photo" runat="server" />
        </div>       
        </div>
 
 
 
 
   </div>
  </div>
</div>
        
        
        
        
        
        
        
        
        
        <table  >
      
            <tr>
                <td valign=top >
                   </td>
                <td>
                    &nbsp;
                    </td>
            </tr>
        </table>


    <br />
    <asp:Label ID="lbl_success" runat="server" Text=""></asp:Label>
    <br />


    <br />
    <asp:Button ID="btn_update" runat="server"  Text="<%$ Resources:Resource, btn_submit%>" onclick="btn_update_Click"  />

 <asp:HiddenField ID="ua_id" runat="server" />
    <br />
    <asp:Label ID="Label21" runat="server" Text="Label"></asp:Label>
</asp:Content>

