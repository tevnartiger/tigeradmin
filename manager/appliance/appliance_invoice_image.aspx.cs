﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.AccountObjectAuthorization;

public partial class manager_appliance_appliance_invoice_image : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!RegEx.IsInteger(Request.QueryString["appliance_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        // MARS ACTIVE MULTIPLE RESULT SET
        // VIEW  AN APPLIANCE
        
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        ////////
        //////// SECURITY CHECK BEGIN //////////////////
        ////////
        AccountObjectAuthorization applianceAuthorization = new AccountObjectAuthorization(strconn);

        if (applianceAuthorization.Appliance(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["appliance_id"])))
        {
        }
        else
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ////////
        //////// SECURITY CHECK END //////////////////
        ////////

        link_appliance_invoice.NavigateUrl = "appliance_view.aspx?appliance_id=" + Request.QueryString["appliance_id"];

        SqlConnection conn1 = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prApplianceView2", conn1);
      

        cmd.CommandType = CommandType.StoredProcedure;
        
        //Add the params
        cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
        cmd.Parameters.Add("appliance_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["appliance_id"]);
        try
        {
            conn1.Open();

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            rApplianceInvoiceImage.DataSource = ds;
            rApplianceInvoiceImage.DataBind();

        }
        finally
        {
            conn1.Close();
        }

    }
}
