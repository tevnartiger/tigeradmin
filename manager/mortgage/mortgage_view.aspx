<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true"  CodeFile="mortgage_view.aspx.cs" Inherits="mortgage_mortgage_view"  Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
             
  <cc1:TabContainer ID="TabContainer1" runat="server"   >
   <cc1:TabPanel ID="tab1" runat="server" HeaderText="MORTGAGE"  >
    <ContentTemplate  >
      <asp:Label ID="Label11" runat="server" Text="<%$ Resources:Resource, lbl_u_mortgage %>"></asp:Label><br /><asp:HyperLink ID="mortgage_add_link" 
          Text="<%$ Resources:Resource, lbl_add %>"   runat="server"> </asp:HyperLink> -&nbsp;  <asp:HyperLink ID="mortgage_update_link"
          Text="<%$ Resources:Resource, lbl_update %>"  runat="server"></asp:HyperLink>-&nbsp;  <asp:HyperLink ID="mortgage_renew_link"
          Text="<%$ Resources:Resource, lbl_renew %>"  runat="server"></asp:HyperLink>  - delete mortgage
        &nbsp;<br />
        <br />
   <table style="width: 100%">
            <tr>
                <td valign="top">
      <table  bgcolor="#ffffcc">
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_home_name %>"></asp:Label></td>
                <td >
                    <asp:Label ID="lbl_home_name" runat="server">
                    </asp:Label></td>
            </tr>
            <tr>
                <td >
                 <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lbl_financial_institution %>"></asp:Label></td>
                <td >
                    <asp:Label ID="lbl_fi_name" runat="server">
                    </asp:Label></td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_loan_amount %>"></asp:Label></td>
                <td >
                    <asp:Label ID="lbl_mortgage_loan_amount" runat="server" Width="115px"></asp:Label></td>
            </tr>
            <tr>
                <td>
                   <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lbl_interestrate %>"></asp:Label></td>
                <td>
                    <asp:Label ID="lbl_mortgage_interest_rate" runat="server" Width="115px"></asp:Label></td>
            </tr>
            <tr>
                <td>
                     <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, lbl_term %>"></asp:Label></td>
                <td>
                    <asp:Label ID="lbl_mortgage_term" runat="server" Width="115px"></asp:Label></td>
            </tr>
            <tr>
                <td>
                     <asp:Label ID="Label10" runat="server" Text="<%$ Resources:Resource, lbl_current_term %>"></asp:Label></td>
                <td>
                    <asp:Label ID="lbl_mortgage_current_term" runat="server" Width="114px"></asp:Label></td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lbl_date_begin %>"></asp:Label></td>
                <td>
                    <asp:Label ID="lbl_mortgate_date_begin" runat="server" Width="65px"></asp:Label>
                    </td>
            </tr>
             <tr>
                <td >
                    <asp:Label ID="Label12" runat="server" Text="<%$ Resources:Resource, lbl_date_end %>"></asp:Label></td>
                <td>
                    <asp:Label ID="lbl_mortgate_date_end" runat="server" Width="65px"></asp:Label>
                    </td>
            </tr>
            <tr>
                <td >
                     <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lbl_payment_frequency %>"></asp:Label></td>
                <td >
                <asp:Label ID="lbl_pf_categ" runat="server" Width="65px"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                      <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lbl_payment_amount %>"></asp:Label></td>
                <td>
                    <asp:Label ID="lbl_mortgage_payment_amount" runat="server" Width="115px"></asp:Label></td>
            </tr>
            <tr>
                <td>
                      <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, lbl_interest_coumpound %>"></asp:Label></td>
                <td>
                <asp:Label ID="lbl_pi_categ" runat="server" Width="65px"></asp:Label>
                   
                       </td>
            </tr>
        </table>
                </td>
                <td valign="top" align="left">
                     
         <table width="100%">
           <tr>
               <td bgcolor="aliceblue">
          <b><asp:Label ID="Label13" runat="server" Text="<%$ Resources:Resource, lbl_u_archive_mortgage %>"></asp:Label></b></td>
           </tr>
           
           
           
             <tr>
               <td >
                                       <asp:GridView ID="gv_mortgage_home_archive_list" 
    runat="server" AutoGenerateColumns="false" GridLines="Both"  
        AlternatingRowStyle-BackColor="Beige"  HeaderStyle-BackColor="AliceBlue" 
         Width="100%" BorderColor="AliceBlue" BorderWidth="3"
       AllowPaging="true"  PageSize="10" EmptyDataText="<%$ Resources:Resource, lbl_none %>"  >
           
    
    <Columns>
    
    <asp:BoundField DataField="home_name"  HeaderText="<%$ Resources:Resource, lbl_home_name %>" />
    <asp:BoundField DataField="mortgage_interest_rate" DataFormatString="{0:0.00}"  HeaderText="<%$ Resources:Resource, lbl_interestrate %>" />
    <asp:BoundField DataField="mortgage_payment_amount" DataFormatString="{0:0.00}"  HeaderText="<%$ Resources:Resource, lbl_mortgage_payment %>"/>
    
    <asp:BoundField DataField="mortgage_date_begin" DataFormatString="{0:M-dd-yyyy}"  HtmlEncode="false" HeaderText="<%$ Resources:Resource, lbl_date_begin %>" />
    <asp:BoundField DataField="mortgage_date_end" DataFormatString="{0:M-dd-yyyy}"  HtmlEncode="false"  HeaderText="<%$ Resources:Resource, lbl_date_end %>"/>
    
   
   
    <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_view %>"
     DataNavigateUrlFields="mortgage_id" 
     DataNavigateUrlFormatString="~/manager/mortgage/mortgage_archive_view.aspx?mortgage_id={0}" 
      HeaderText="<%$ Resources:Resource, gv_view_detail %>" />
    </Columns>  
    </asp:GridView>
               
            </td>
           </tr>
       </table>

    </td>
            </tr>
        </table>
         <br />
        <asp:CheckBox ID="chk_enable_button" Text="<%$ Resources:Resource, lbl_enable_button %>"  runat="server" 
         oncheckedchanged="chk_enable_button_CheckedChanged" AutoPostBack="true" />
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       <br />
&nbsp;<asp:Button ID="btn_delete" runat="server" 
         Text="<%$ Resources:Resource, lbl_delete_mortgage %>" 
         onclick="btn_delete_Click" />
       <br />
        
       
    
   
    </ContentTemplate  >
  </cc1:TabPanel>
  
  
   <cc1:TabPanel ID="tab2" runat="server" HeaderText="MORTGAGE AMORTIZATION TABLE"  >
    <ContentTemplate  >
   
      <table  bgcolor="#ffffcc">
            <tr>
                <td>
                    <asp:Label ID="Label14" runat="server" Text="<%$ Resources:Resource, lbl_home_name %>"></asp:Label></td>
                <td >
                  &nbsp;&nbsp;&nbsp;  <asp:Label ID="lbl_home_name2" runat="server">
                    </asp:Label></td>
            </tr>
            </table>
            <br /><br />
      <br />
   <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="100%" BorderColor="White" BorderWidth="3" ID="gv_amortization" runat="server" AlternatingRowStyle-BackColor="Beige"  AutoGenerateColumns="false">
     <Columns>
      <asp:BoundField DataField="ID"   HeaderText="#" />
      <asp:BoundField DataField="ladate"  HeaderText="<%$ Resources:Resource, txt_date %>" /> 
      <asp:BoundField DataField="OustandingBalance" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, txt_outstanding_balance %>"/>
      <asp:BoundField DataField="InterestPaid" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, txt_interest_paid %>" />
      <asp:BoundField DataField="CummInterestPaid" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, txt_cumm_interest %>"/>
      <asp:BoundField DataField="PrincipalPaid" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, txt_principal_paid %>" />
      <asp:BoundField DataField="CummPrincipalPaid" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, txt_cumm_principal_paid %>" />
      <asp:BoundField DataField="Balance" DataFormatString="{0:0.00}" HeaderText="<%$ Resources:Resource, txt_balance %>" />  
    </Columns>
  </asp:GridView>
<br />
   
   
   
   
   
   
   
   
    </ContentTemplate  >
   </cc1:TabPanel>
   
   
   
 </cc1:TabContainer>
  
  
        
       
 </asp:Content>