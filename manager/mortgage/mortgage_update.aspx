<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="mortgage_update.aspx.cs" Inherits="mortgage_mortgage_update"  Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


 <div>
        <b><asp:Label ID="Label11" runat="server" Text="<%$ Resources:Resource, lbl_u_mortgage %>"></asp:Label></b><br />
        <br />
        <asp:CheckBox ID="chk_fi_add" runat="server" AutoPostBack="True" OnCheckedChanged="chk_fi_add_CheckedChanged"
            Text="<%$ Resources:Resource, lbl_add_financial_institution %>" /><br />
      <table>
       <tr>
       <td valign = "top"><br />
        <table  bgcolor="#ffffcc">
            <tr>
                <td>
                     <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_home_name %>"></asp:Label></td>
                <td >
                    <asp:DropDownList DataTextField="home_name" DataValueField="home_id" ID="ddl_home_list" runat="server">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lbl_financial_institution %>"></asp:Label></td>
                <td >
                    <asp:DropDownList ID="ddl_fi_list" DataTextField="fi_name" DataValueField="fi_id"  runat="server">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td >
                   <asp:Label ID="Label2" runat="server" Text="<%$ Resources:Resource, lbl_loan_amount %>"></asp:Label></td>
                <td >
                    <asp:TextBox ID="mortgage_loan_amount" runat="server" Width="115px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator 
                        ID="req_mortgage_loan_amount" runat="server" 
                         ValidationGroup="vg_mortgage"
                         ControlToValidate="mortgage_loan_amount"
                        ErrorMessage="required">
                        </asp:RequiredFieldValidator>
                &nbsp;<asp:RegularExpressionValidator ID="reg_mortgage_loan_amount" 
                          ValidationGroup="vg_mortgage"
                         ControlToValidate="mortgage_loan_amount"
                        runat="server" ErrorMessage="invalid input">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lbl_interestrate %>"></asp:Label></td>
                <td>
                    <asp:TextBox ID="mortgage_interest_rate" runat="server" Width="115px"></asp:TextBox>&nbsp;
                    <asp:RequiredFieldValidator 
                        ID="req_mortgage_interest_rate" runat="server" 
                         ValidationGroup="vg_mortgage"
                         ControlToValidate="mortgage_interest_rate"
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                &nbsp;<asp:RegularExpressionValidator ID="reg_mortgage_interest_rate" 
                          ValidationGroup="vg_mortgage"
                         ControlToValidate="mortgage_interest_rate"
                        runat="server" ErrorMessage="invalid input">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                   <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, lbl_term %>"></asp:Label></td>
                <td>
                    <asp:TextBox ID="mortgage_term" runat="server" Width="115px"></asp:TextBox>&nbsp;
                    <asp:RequiredFieldValidator 
                        ID="req_mortgage_term" runat="server" 
                          ValidationGroup="vg_mortgage"
                         ControlToValidate="mortgage_term"
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                &nbsp;<asp:RegularExpressionValidator ID="reg_mortgage_term" 
                          ValidationGroup="vg_mortgage"
                         ControlToValidate="mortgage_term"
                        runat="server" ErrorMessage="invalid input">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                  <asp:Label ID="Label10" runat="server" Text="<%$ Resources:Resource, lbl_current_term %>"></asp:Label></td>
                <td>
                    <asp:TextBox ID="mortgage_current_term" runat="server" Width="114px"></asp:TextBox>&nbsp;
                    <asp:RequiredFieldValidator 
                        ID="req_mortgage_current_term" runat="server" 
                          ValidationGroup="vg_mortgage"
                         ControlToValidate="mortgage_current_term"
                        ErrorMessage="required"></asp:RequiredFieldValidator>
                &nbsp;<asp:RegularExpressionValidator ID="reg_mortgage_current_term" 
                          ValidationGroup="vg_mortgage"
                         ControlToValidate="mortgage_current_term"
                        runat="server" ErrorMessage="invalid input">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td >
                   <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lbl_date_begin %>"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="ddl_mortgate_date_begin_m" runat="server">
                        <asp:ListItem Text="<%$ Resources:Resource, txt_january %>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_february %>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_march %>" Value="3"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_april %>" Value="4"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_may %>" Value="5"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_june %>" Value="6"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_july %>" Value="7"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_august %>" Value="8"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_september %>" Value="9"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_october %>" Value="10"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_november %>" Value="11"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_december %>" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;/&nbsp;
                    <asp:DropDownList ID="ddl_mortgate_date_begin_d" runat="server" >
                     <asp:ListItem>1</asp:ListItem>
                     <asp:ListItem>2</asp:ListItem>
                     <asp:ListItem>3</asp:ListItem>
                     <asp:ListItem>4</asp:ListItem>
                     <asp:ListItem>5</asp:ListItem>
                     <asp:ListItem>6</asp:ListItem>
                     <asp:ListItem>7</asp:ListItem>
                     <asp:ListItem>8</asp:ListItem>
                     <asp:ListItem>9</asp:ListItem>
                     <asp:ListItem>10</asp:ListItem>
                     <asp:ListItem>11</asp:ListItem>
                     <asp:ListItem>12</asp:ListItem>
                     <asp:ListItem>13</asp:ListItem>
                     <asp:ListItem>14</asp:ListItem>
                     <asp:ListItem>15</asp:ListItem>
                     <asp:ListItem>16</asp:ListItem>
                     <asp:ListItem>17</asp:ListItem>
                     <asp:ListItem>18</asp:ListItem>
                     <asp:ListItem>19</asp:ListItem>
                     <asp:ListItem>20</asp:ListItem>
                     <asp:ListItem>21</asp:ListItem>
                     <asp:ListItem>22</asp:ListItem>
                     <asp:ListItem>23</asp:ListItem>
                     <asp:ListItem>24</asp:ListItem>
                     <asp:ListItem>25</asp:ListItem>
                     <asp:ListItem>26</asp:ListItem>
                     <asp:ListItem>27</asp:ListItem>
                     <asp:ListItem>28</asp:ListItem>
                     <asp:ListItem>29</asp:ListItem>
                     <asp:ListItem>30</asp:ListItem>
                     <asp:ListItem>31</asp:ListItem>
                 </asp:DropDownList>&nbsp; &nbsp;/&nbsp;    
                    

                     <asp:DropDownList ID="ddl_mortgate_date_begin_y" runat="server">
                            <asp:ListItem>1950</asp:ListItem>
                            <asp:ListItem>1951</asp:ListItem>
                            <asp:ListItem>1952</asp:ListItem>
                            <asp:ListItem>1953</asp:ListItem>
                            <asp:ListItem>1954</asp:ListItem>
                            <asp:ListItem>1955</asp:ListItem>
                            <asp:ListItem>1956</asp:ListItem>
                            <asp:ListItem>1957</asp:ListItem>
                            <asp:ListItem>1958</asp:ListItem>
                            <asp:ListItem>1959</asp:ListItem>
                            <asp:ListItem>1960</asp:ListItem>
                            <asp:ListItem>1961</asp:ListItem>
                            <asp:ListItem>1962</asp:ListItem>
                            <asp:ListItem>1963</asp:ListItem>
                            <asp:ListItem>1964</asp:ListItem>
                            <asp:ListItem>1965</asp:ListItem>
                            <asp:ListItem>1966</asp:ListItem>
                            <asp:ListItem>1967</asp:ListItem>
                            <asp:ListItem>1968</asp:ListItem>
                            <asp:ListItem>1969</asp:ListItem>
                            <asp:ListItem>1970</asp:ListItem>
                            <asp:ListItem>1971</asp:ListItem>
                            <asp:ListItem>1972</asp:ListItem>
                            <asp:ListItem>1973</asp:ListItem>
                            <asp:ListItem>1974</asp:ListItem>
                            <asp:ListItem>1975</asp:ListItem>
                            <asp:ListItem>1976</asp:ListItem>
                            <asp:ListItem>1977</asp:ListItem>
                            <asp:ListItem>1978</asp:ListItem>
                            <asp:ListItem>1979</asp:ListItem>
                            <asp:ListItem>1980</asp:ListItem>
                            <asp:ListItem>1981</asp:ListItem>
                            <asp:ListItem>1982</asp:ListItem>
                            <asp:ListItem>1983</asp:ListItem>
                            <asp:ListItem>1984</asp:ListItem>
                            <asp:ListItem>1985</asp:ListItem>
                            <asp:ListItem>1986</asp:ListItem>
                            <asp:ListItem>1987</asp:ListItem>
                            <asp:ListItem>1988</asp:ListItem>
                            <asp:ListItem>1989</asp:ListItem>
                            <asp:ListItem>1990</asp:ListItem>
                            <asp:ListItem>1991</asp:ListItem>
                            <asp:ListItem>1992</asp:ListItem>
                            <asp:ListItem>1993</asp:ListItem>
                            <asp:ListItem>1994</asp:ListItem>
                            <asp:ListItem>1995</asp:ListItem>
                            <asp:ListItem>1996</asp:ListItem>
                            <asp:ListItem>1997</asp:ListItem>
                            <asp:ListItem>1998</asp:ListItem>
                            <asp:ListItem>1999</asp:ListItem>
                            <asp:ListItem>2000</asp:ListItem>
                            <asp:ListItem>2001</asp:ListItem>
                            <asp:ListItem>2002</asp:ListItem>
                            <asp:ListItem>2003</asp:ListItem>
                            <asp:ListItem>2004</asp:ListItem>
                            <asp:ListItem>2005</asp:ListItem>
                            <asp:ListItem>2006</asp:ListItem>
                            <asp:ListItem>2007</asp:ListItem>
                            <asp:ListItem>2008</asp:ListItem>
                            <asp:ListItem>2009</asp:ListItem>
                            <asp:ListItem>2010</asp:ListItem>
                            <asp:ListItem>2011</asp:ListItem>
                            <asp:ListItem>2012</asp:ListItem>
                            <asp:ListItem>2013</asp:ListItem>
                            <asp:ListItem>2014</asp:ListItem>
                            <asp:ListItem>2015</asp:ListItem>
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList></td>
            </tr>
            <tr>
                <td >
                    <asp:Label ID="Label5" runat="server" Text="<%$ Resources:Resource, lbl_payment_frequency %>"></asp:Label></td>
                <td >
                    <asp:DropDownList ID="ddl_pf_list" runat="server">
                        <asp:ListItem Selected="True" Text="<%$ Resources:Resource, txt_monthly%>" Value="12"></asp:ListItem>
                        <asp:ListItem Value="24" Text="<%$ Resources:Resource, txt_bi_weekly%>"></asp:ListItem>
                        <asp:ListItem Value="241" Text="<%$ Resources:Resource, txt_acc_bi_weekly%>"></asp:ListItem>
                        <asp:ListItem Value="52" Text="<%$ Resources:Resource, txt_weekly%>"></asp:ListItem>
                        <asp:ListItem Value="521" Text="<%$ Resources:Resource, txt_acc_weekly%>"></asp:ListItem>
                    </asp:DropDownList></td>
                    
                    
              
         
            </tr>
         
            <tr>
                <td>
                     <asp:Label ID="Label6" runat="server" Text="<%$ Resources:Resource, lbl_payment_amount %>"></asp:Label></td>
                <td>
                    <asp:TextBox ID="mortgage_payment_amount" runat="server" Width="115px"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator ID="reg_mortgage_payment_amount" 
                          ValidationGroup="vg_mortgage"
                         ControlToValidate="mortgage_payment_amount"
                        runat="server" ErrorMessage="invalid input">
                        </asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, lbl_interest_coumpound %>"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="ddl_pi_list" runat="server">
                        <asp:ListItem Text="<%$ Resources:Resource, lbl_annualy %>" Selected="True" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:Resource, txt_bi_yearly %>"  Value="6"></asp:ListItem>
                    </asp:DropDownList></td>
            </tr>
        </table>
           <br />
     <asp:Button ID="btn_submit" runat="server" 
        ValidationGroup="vg_mortgage"
      Text="<%$ Resources:Resource, btn_submit %>"
       OnClick="btn_submit_Click" />
       </td>
       <td valign = "top">
       <table>
       <asp:Panel runat="server" ID="panel_fi">
                        <tr>
                            <td>
                                <asp:Label ID="Label12" runat="server" Text="<%$ Resources:Resource, lbl_u_financial_institution %>"></asp:Label>
                    <table bgcolor="#ffffcc">
                        <tr>
                            <td>
                                <asp:Label ID="Label13" runat="server" Text="<%$ Resources:Resource, lbl_lname %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_name" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator 
                        ID="reg_fi_name" runat="server" 
                         ControlToValidate="fi_name"
                        ErrorMessage="invalid name">
                        </asp:RegularExpressionValidator>
                &nbsp;<asp:RequiredFieldValidator  ControlToValidate="fi_name"
                           ID="req_fi_name" runat="server" 
                        ErrorMessage="required"></asp:RequiredFieldValidator></td>
                        </tr>
                        <tr>
                            <td>
                <asp:Label ID="Label14" runat="server" Text="<%$ Resources:Resource, lbl_website %>"></asp:Label></td>
                              </td>
                            <td>
                                :
                                <asp:TextBox ID="fi_website" runat="server"></asp:TextBox>
                                &nbsp;<asp:RegularExpressionValidator ID="reg_fi_website" runat="server" 
                         ControlToValidate="fi_website"
                        ErrorMessage="invalid link">
                        </asp:RegularExpressionValidator></td>
                        </tr>
                        <tr>
                            <td>
                         <asp:Label ID="Label15" runat="server" Text="<%$ Resources:Resource, lbl_address %>"></asp:Label></td>
                            
                            </td>
                            <td>
                                :
                                <asp:TextBox ID="fi_addr" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator ID="reg_fi_addr" runat="server" 
                        ControlToValidate="fi_addr"
                        ErrorMessage="invalid address">
                        </asp:RegularExpressionValidator></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label16" runat="server" Text="<%$ Resources:Resource, lbl_tel %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_tel" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator 
                        ID="reg_fi_tel" runat="server" 
                        ControlToValidate="fi_tel"
                        ErrorMessage="invalid telephone">
                        </asp:RegularExpressionValidator></td>
                        </tr>
                        <tr>
                            <td>
                                 <asp:Label ID="Label17" runat="server" Text="<%$ Resources:Resource, lbl_country %>"></asp:Label></td>
                            <td>
                                :
                                <asp:DropDownList ID="ddl_fi_country_list" runat="server" DataTextField="country_name"
                                    DataValueField="country_id">
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td>
                             <asp:Label ID="Label18" runat="server" Text="<%$ Resources:Resource, lbl_prov %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_prov" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator ID="reg_fi_prov" runat="server" 
                        ControlToValidate="fi_prov"
                        ErrorMessage="enter letters only ">
                        </asp:RegularExpressionValidator></td>
                        </tr>
                        <tr>
                            <td>
                               <asp:Label ID="Label19" runat="server" Text="<%$ Resources:Resource, lbl_city %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_city" runat="server"></asp:TextBox>
                                &nbsp;<asp:RegularExpressionValidator ID="reg_fi_city" runat="server" 
                        ControlToValidate="fi_city"
                        ErrorMessage="enter letters only">
                        </asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label20" runat="server" Text="<%$ Resources:Resource, lbl_pc %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_pc" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator ID="reg_fi_pc" runat="server" 
                        ControlToValidate="fi_pc"
                        ErrorMessage="invalid postal code">
                        </asp:RegularExpressionValidator></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label21" runat="server" Text="<%$ Resources:Resource, lbl_contact_fname %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_contact_fname" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator ID="reg_fi_contact_fname" runat="server" 
                        ControlToValidate="fi_contact_fname"
                        ErrorMessage="invalid name">
                        </asp:RegularExpressionValidator></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label22" runat="server" Text="<%$ Resources:Resource, lbl_contact_lname %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_contact_lname" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator ID="reg_fi_contact_lname" runat="server" 
                        ControlToValidate="fi_contact_lname"
                        ErrorMessage="invalid last name">
                        </asp:RegularExpressionValidator></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label23" runat="server" Text="<%$ Resources:Resource, lbl_contact_tel %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_contact_tel" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator ID="reg_fi_contact_tel" 
                        ControlToValidate="fi_contact_tel"
                        runat="server" ErrorMessage="invalid telephone">
                        </asp:RegularExpressionValidator></td>
                        </tr>
                        <tr>
                            <td>
                              <asp:Label ID="Label24" runat="server" Text="<%$ Resources:Resource, lbl_fax %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_contact_fax" runat="server"></asp:TextBox>&nbsp;
                    <asp:RegularExpressionValidator ID="reg_fi_contact_fax" 
                        ControlToValidate="fi_contact_fax"
                        runat="server" ErrorMessage="invalid fax">
                        </asp:RegularExpressionValidator></td>
                        </tr>
                        <tr>
                            <td>
                             <asp:Label ID="Label25" runat="server" Text="<%$ Resources:Resource, lbl_email %>"></asp:Label></td>
                            <td>
                                :
                                <asp:TextBox ID="fi_contact_email" runat="server"></asp:TextBox>&nbsp;<asp:RegularExpressionValidator ID="reg_fi_contact_email" 
                        ControlToValidate="fi_contact_email"
                        runat="server" ErrorMessage="invalid email">
                        </asp:RegularExpressionValidator></td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Label ID="Label26" runat="server" Text="<%$ Resources:Resource, lbl_comments %>"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="fi_com" runat="server" Height="100px" TextMode="MultiLine" Width="300px"></asp:TextBox></td>
                        </tr>
                    </table>
                    
                    <asp:Button ID="btn_submit_fi" runat="server" OnClick="btn_submit_fi_Click" Text="<%$ Resources:Resource, btn_submit %>" />
                    <br /> 
                            </td>
                        </tr>
                    </asp:Panel>
       </table>
       </td>
       </tr>
       </table>
       
       <div id="result" runat="server">
       </div>
       
    </div>
       


</asp:Content>

