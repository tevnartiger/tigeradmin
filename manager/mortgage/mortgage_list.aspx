<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" CodeFile="mortgage_list.aspx.cs" Inherits="mortgage_mortgage_list" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    
   
<br />
 <cc1:TabContainer ID="TabContainer1" runat="server"   >
    
  <cc1:TabPanel ID="tab1" runat="server" HeaderText="<%$ Resources:Resource, lbl_u_mortgage %>"  >
   <ContentTemplate  >
   
  
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label8" runat="server" style="font-weight: 700" 
                        Text="<%$ Resources:Resource, lbl_property %>" />
                </td>
                <td>
                    :
                    <asp:DropDownList ID="ddl_home_list" runat="server" AutoPostBack="true" 
                        DataTextField="home_name" DataValueField="home_id" 
                        OnSelectedIndexChanged="ddl_home_list_SelectedIndexChanged">
                    </asp:DropDownList>
                    &nbsp;</td>
            </tr>
        </table>
    
  
    <br />
    <asp:GridView ID="gv_mortgage_list" 
    runat="server" AutoGenerateColumns="false" GridLines="Both"  
        AlternatingRowStyle-BackColor="#F0F0F6" 
      BorderColor="#CDCDCD"  BorderWidth="1"  Width="83%"
      HeaderStyle-BackColor="#F0F0F6"
       AllowPaging="true"  PageSize="10"  OnPageIndexChanging="gv_mortgage_list_PageIndexChanging"
         EmptyDataText="<%$ Resources:Resource, lbl_none %>"  
           >
    
    <Columns>
    
    <asp:BoundField DataField="home_name"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"  HeaderText="<%$ Resources:Resource, lbl_home_name %>" />
    <asp:BoundField DataField="home_city"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top" HeaderText="<%$ Resources:Resource, lbl_city %>"/>
     <asp:BoundField DataField="mortgage_interest_rate"  ItemStyle-VerticalAlign="Top" HeaderStyle-Font-Size="Small"  HeaderStyle-VerticalAlign="Top" DataFormatString="{0:0.00}"  HeaderText="<%$ Resources:Resource, lbl_interestrate %>" />
  
    <asp:BoundField DataField="mortgage_payment_amount" DataFormatString="{0:0.00}"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"  HeaderText="<%$ Resources:Resource, lbl_mortgage_payment %>"/>
    
   <asp:BoundField DataField="mortgage_date_begin" DataFormatString="{0:M-dd-yyyy}"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"  HtmlEncode="false" HeaderText="<%$ Resources:Resource, lbl_date_begin %>" />
    <asp:BoundField DataField="mortgage_date_end" DataFormatString="{0:M-dd-yyyy}"  HtmlEncode="false"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top" HeaderText="<%$ Resources:Resource, lbl_date_end %>"/>
    
    <asp:HyperLinkField  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"   Text="<%$ Resources:Resource, lbl_view %>"
     DataNavigateUrlFields="mortgage_id" 
     DataNavigateUrlFormatString="~/manager/mortgage/mortgage_view.aspx?mortgage_id={0}" 
      HeaderText="<%$ Resources:Resource, gv_view_detail %>" />
    </Columns>  
    </asp:GridView>
       
   </ContentTemplate  >
   </cc1:TabPanel>
   
   
   
   
   
   
   
   
  <cc1:TabPanel ID="tab2" runat="server" HeaderText="<%$ Resources:Resource, lbl_u_pending_mortgage %>"  >
    
    
    <ContentTemplate  >
        <table>
            <tr>
                <td>
                    <asp:Label ID="lbl_property" runat="server" style="font-weight: 700" 
                        Text="<%$ Resources:Resource, lbl_property %>" />
                </td>
                <td>
                    :
                    <asp:DropDownList ID="ddl_home_pending_list" runat="server" AutoPostBack="true" 
                        DataTextField="home_name" DataValueField="home_id" 
                        OnSelectedIndexChanged="ddl_home_pending_list_SelectedIndexChanged">
                    </asp:DropDownList>
                    &nbsp;</td>
            </tr>
        </table>
    
    <br />
       <table width="83%">
           <tr>
               <td bgcolor="aliceblue">
                   <b>
                   <asp:Label ID="Label42" runat="server" 
                       Text="<%$ Resources:Resource, lbl_less31%>" />
                   </b>
               </td>
           </tr>
       </table>
    <br />
       <asp:GridView ID="gv_mortgage_pending_list1" runat="server" 
           AllowPaging="true"  
           AutoGenerateColumns="false" 
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" GridLines="Both" 
           AlternatingRowStyle-BackColor="#F0F0F6" 
             BorderColor="#CDCDCD"  BorderWidth="1" 
              HeaderStyle-BackColor="#F0F0F6"
           OnPageIndexChanging="gv_mortgage_pending_list1_PageIndexChanging" 
           PageSize="10" Width="83%">
           <Columns>
            <asp:BoundField  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top" DataField="home_name"  
             HeaderText="<%$ Resources:Resource, lbl_home_name %>" />
            <asp:BoundField  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top" DataField="home_city" 
             HeaderText="<%$ Resources:Resource, lbl_city %>"/>
            <asp:BoundField  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top" DataField="mortgage_interest_rate" DataFormatString="{0:0.00}" 
             HeaderText="<%$ Resources:Resource, lbl_interestrate %>" />
            <asp:BoundField  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top" DataField="mortgage_payment_amount" DataFormatString="{0:0.00}" 
             HeaderText="<%$ Resources:Resource, lbl_mortgage_payment %>"/>
            <asp:BoundField   ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top" DataField="mortgage_date_begin" DataFormatString="{0:M-dd-yyyy}"  HtmlEncode="false" 
             HeaderText="<%$ Resources:Resource, lbl_date_begin %>" />
            <asp:BoundField  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top" DataField="amount_days_left" 
              HeaderText="<%$ Resources:Resource, lbl_days %>"/>
            <asp:HyperLinkField  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"  Text="<%$ Resources:Resource, lbl_view %>"
              DataNavigateUrlFields="mortgage_id" 
              DataNavigateUrlFormatString="~/manager/mortgage/mortgage_pending_view.aspx?mortgage_id={0}" 
              HeaderText="<%$ Resources:Resource, gv_view_detail %>" />
           </Columns>
       </asp:GridView>
     <br />
     
  <br />
       <table width="83%">
           <tr>
               <td bgcolor="aliceblue">
                   <b>
                   <asp:Label ID="Label4" runat="server" 
                       Text="<%$ Resources:Resource, lbl_between_31and60%>" />
                   </b>
               </td>
           </tr>
       </table>
    <br />
       <asp:GridView ID="gv_mortgage_pending_list2" runat="server" 
           AllowPaging="true" AutoGenerateColumns="false" 
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" GridLines="Both" 
          AlternatingRowStyle-BackColor="#F0F0F6" 
           BorderColor="#CDCDCD"  BorderWidth="1"  Width="83%"
          HeaderStyle-BackColor="#F0F0F6" 
           OnPageIndexChanging="gv_mortgage_pending_list2_PageIndexChanging" 
           PageSize="10" >
           <Columns>
               <asp:BoundField DataField="home_name"   ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
             HeaderText="<%$ Resources:Resource, lbl_home_name %>" />
            <asp:BoundField DataField="home_city"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
             HeaderText="<%$ Resources:Resource, lbl_city %>"/>
            <asp:BoundField DataField="mortgage_interest_rate"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
             DataFormatString="{0:0.00}" 
             HeaderText="<%$ Resources:Resource, lbl_interestrate %>" />
            <asp:BoundField DataField="mortgage_payment_amount"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top" DataFormatString="{0:0.00}" 
             HeaderText="<%$ Resources:Resource, lbl_mortgage_payment %>"/>
            <asp:BoundField DataField="mortgage_date_begin"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top" DataFormatString="{0:M-dd-yyyy}"  HtmlEncode="false" 
             HeaderText="<%$ Resources:Resource, lbl_date_begin %>" />
            <asp:BoundField DataField="amount_days_left"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top" 
              HeaderText="<%$ Resources:Resource, lbl_days %>"/>
            <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_view %>"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
              DataNavigateUrlFields="mortgage_id" 
              DataNavigateUrlFormatString="~/manager/mortgage/mortgage_pending_view.aspx?mortgage_id={0}" 
              HeaderText="<%$ Resources:Resource, gv_view_detail %>" />
           
           </Columns>
       </asp:GridView>
       <br />
    
    
        <br />
        <table width="83%">
            <tr>
                <td bgcolor="aliceblue">
                    <b>
                    <asp:Label ID="Label5" runat="server" 
                        Text="<%$ Resources:Resource, lbl_between_61and90%>" />
                    </b>
                </td>
            </tr>
       </table>
    <br />
       <asp:GridView ID="gv_mortgage_pending_list3" runat="server" 
           AllowPaging="true" 
           AutoGenerateColumns="false" 
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" GridLines="Both" 
           AlternatingRowStyle-BackColor="#F0F0F6" 
           BorderColor="#CDCDCD"  BorderWidth="1" 
           HeaderStyle-BackColor="#F0F0F6"
           OnPageIndexChanging="gv_mortgage_pending_list3_PageIndexChanging" 
           PageSize="10" Width="83%">
           <Columns>
               <asp:BoundField DataField="home_name"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
             HeaderText="<%$ Resources:Resource, lbl_home_name %>" />
            <asp:BoundField DataField="home_city"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
             HeaderText="<%$ Resources:Resource, lbl_city %>"/>
            <asp:BoundField DataField="mortgage_interest_rate"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
             DataFormatString="{0:0.00}" 
             HeaderText="<%$ Resources:Resource, lbl_interestrate %>" />
            <asp:BoundField DataField="mortgage_payment_amount"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
             DataFormatString="{0:0.00}" 
             HeaderText="<%$ Resources:Resource, lbl_mortgage_payment %>"/>
            <asp:BoundField DataField="mortgage_date_begin" DataFormatString="{0:M-dd-yyyy}"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
             HtmlEncode="false" 
             HeaderText="<%$ Resources:Resource, lbl_date_begin %>" />
            <asp:BoundField DataField="amount_days_left"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
              HeaderText="<%$ Resources:Resource, lbl_days %>"/>
            <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_view %>"
              DataNavigateUrlFields="mortgage_id"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
              DataNavigateUrlFormatString="~/manager/mortgage/mortgage_pending_view.aspx?mortgage_id={0}" 
              HeaderText="<%$ Resources:Resource, gv_view_detail %>" />
           
           </Columns>
       </asp:GridView>
       <br />
    
    
        <br />
        <table width="83%">
            <tr>
                <td bgcolor="aliceblue">
                    <b>
                    <asp:Label ID="Label6" runat="server" 
                        Text="<%$ Resources:Resource, lbl_more90%>" />
                    </b>
                </td>
            </tr>
       </table>
       <br />
       <asp:GridView ID="gv_mortgage_pending_list4" runat="server" 
           AllowPaging="true"  
           AutoGenerateColumns="false" 
           EmptyDataText="<%$ Resources:Resource, lbl_none %>" GridLines="Both" 
           AlternatingRowStyle-BackColor="#F0F0F6" 
           BorderColor="#CDCDCD"  BorderWidth="1"  Width="83%"
           HeaderStyle-BackColor="#F0F0F6"
           OnPageIndexChanging="gv_mortgage_pending_list4_PageIndexChanging" 
           PageSize="10" >
           <Columns>
              <asp:BoundField DataField="home_name"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top" 
             HeaderText="<%$ Resources:Resource, lbl_home_name %>" />
            <asp:BoundField DataField="home_city"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
             HeaderText="<%$ Resources:Resource, lbl_city %>"/>
            <asp:BoundField DataField="mortgage_interest_rate"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
             DataFormatString="{0:0.00}" 
             HeaderText="<%$ Resources:Resource, lbl_interestrate %>" />
            <asp:BoundField DataField="mortgage_payment_amount" DataFormatString="{0:0.00}"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top" 
             HeaderText="<%$ Resources:Resource, lbl_mortgage_payment %>"/>
            <asp:BoundField DataField="mortgage_date_begin" DataFormatString="{0:M-dd-yyyy}"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
             HtmlEncode="false" 
             HeaderText="<%$ Resources:Resource, lbl_date_begin %>" />
            <asp:BoundField DataField="amount_days_left"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
              HeaderText="<%$ Resources:Resource, lbl_days %>"/>
            <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_view %>"
              DataNavigateUrlFields="mortgage_id"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
              DataNavigateUrlFormatString="~/manager/mortgage/mortgage_pending_view.aspx?mortgage_id={0}" 
              HeaderText="<%$ Resources:Resource, gv_view_detail %>" />
           
           </Columns>
       </asp:GridView>
       
       </ContentTemplate  >
       
   </cc1:TabPanel >
   
 
 
 
 
 
 
 
 
 
 
   
   
   
   
   
    <cc1:TabPanel ID="tab3" runat="server"  HeaderText="<%$ Resources:Resource, lbl_u_archive_mortgage %>" >
    
     <ContentTemplate  >
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label7" runat="server" style="font-weight: 700" 
                        Text="<%$ Resources:Resource, lbl_property %>" />
                </td>
                <td>
                    :
                    <asp:DropDownList ID="ddl_home_archive_list" runat="server" AutoPostBack="true" 
                        DataTextField="home_name" DataValueField="home_id" 
                        OnSelectedIndexChanged="ddl_home_archive_list_SelectedIndexChanged">
                    </asp:DropDownList>
                    &nbsp;</td>
            </tr>
        </table>
    
    <br />
    
    
    <br />
    <asp:GridView ID="gv_mortgage_archive_list" 
    runat="server" AutoGenerateColumns="false" GridLines="Both"  
       AlternatingRowStyle-BackColor="#F0F0F6" 
      BorderColor="#CDCDCD"  BorderWidth="1"  Width="83%"
      HeaderStyle-BackColor="#F0F0F6" 
       AllowPaging="true"  PageSize="10"  OnPageIndexChanging="gv_mortgage_archive_list_PageIndexChanging"
           EmptyDataText="<%$ Resources:Resource, lbl_none %>"  
           >
    
    <Columns>
    
    <asp:BoundField DataField="home_name"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
      HeaderText="<%$ Resources:Resource, lbl_home_name %>" />
    
     <asp:BoundField DataField="mortgage_interest_rate" DataFormatString="{0:0.00}"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
       HeaderText="<%$ Resources:Resource, lbl_interestrate %>" />
  
    <asp:BoundField DataField="mortgage_loan_amount" DataFormatString="{0:0.00}"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
     HeaderText="<%$ Resources:Resource, lbl_loan_amount %>" />
  
    
    <asp:BoundField DataField="mortgage_payment_amount" DataFormatString="{0:0.00}"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
     HeaderText="<%$ Resources:Resource, lbl_mortgage_payment %>"/>
    
   <asp:BoundField DataField="mortgage_date_begin" DataFormatString="{0:M-dd-yyyy}"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
     HtmlEncode="false" HeaderText="<%$ Resources:Resource, lbl_date_begin %>" />
    <asp:BoundField DataField="mortgage_date_end" DataFormatString="{0:M-dd-yyyy}"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
      HtmlEncode="false"  HeaderText="<%$ Resources:Resource, lbl_date_end %>"/>
    
   
   
    <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_view %>"
     DataNavigateUrlFields="mortgage_id"  ItemStyle-VerticalAlign="Top"  HeaderStyle-VerticalAlign="Top"
     DataNavigateUrlFormatString="~/manager/mortgage/mortgage_archive_view.aspx?mortgage_id={0}" 
      HeaderText="<%$ Resources:Resource, gv_view_detail %>" />
    </Columns>  
    </asp:GridView>
    </ContentTemplate  >
   </cc1:TabPanel>
   
    
    
   </cc1:TabContainer>
   
    
    
    
    
    </asp:Content>