using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.AccountObjectAuthorization;

/// <summary>
/// Done by : Stanley jocelyn
/// Date    : dec 20, 2008
/// </summary>

public partial class mortgage_mortgage_update : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!RegEx.IsInteger(Request.QueryString["mortgage_id"]))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        reg_fi_city.ValidationExpression = RegEx.getText();
        reg_fi_addr.ValidationExpression = RegEx.getText();
        reg_fi_contact_email.ValidationExpression = RegEx.getEmail();
        reg_fi_contact_fname.ValidationExpression = RegEx.getText();
        reg_fi_contact_lname.ValidationExpression = RegEx.getText();
        reg_fi_contact_tel.ValidationExpression = RegEx.getText();
        reg_fi_name.ValidationExpression = RegEx.getText();
        reg_fi_pc.ValidationExpression = RegEx.getText();
        reg_fi_tel.ValidationExpression = RegEx.getText();
        reg_fi_website.ValidationExpression = RegEx.getText();
        reg_fi_prov.ValidationExpression = RegEx.getText();
        reg_fi_contact_fax.ValidationExpression = RegEx.getText();

        reg_mortgage_term.ValidationExpression = RegEx.getNumeric();
        reg_mortgage_current_term.ValidationExpression = RegEx.getNumeric();
        reg_mortgage_loan_amount.ValidationExpression = RegEx.getMoney();
        reg_mortgage_payment_amount.ValidationExpression = RegEx.getMoney();
        reg_mortgage_interest_rate.ValidationExpression = RegEx.getMoney();

        AccountObjectAuthorization mortgageAuthorization = new AccountObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
        if (!mortgageAuthorization.Mortgage(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Request.QueryString["mortgage_id"])))
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////
           
        if (!Page.IsPostBack)
        {

            // ajoter le datareader



            // home address
            tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_home_list.DataSource = hv.getHomeList(Convert.ToInt32(Session["schema_id"]));
            ddl_home_list.DataBind();
            //  ddl_home_list.Items.Insert(0,"home");


            tiger.Country ddl_fi = new tiger.Country(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_fi_country_list.DataSource = ddl_fi.getCountryList();
            ddl_fi_country_list.DataBind();

            tiger.FinancialInstitution fi = new tiger.FinancialInstitution(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_fi_list.DataSource = fi.getFinancialInstitutionList(Convert.ToInt32(Session["schema_id"]));
            ddl_fi_list.DataBind();
            //  ddl_fi_list.Items.Insert(0,"financial institution");

            panel_fi.Visible = false;



            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prMortgageView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("mortgage_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["mortgage_id"]);
            //  try
            {
                conn.Open();
                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                    ddl_home_list.SelectedValue = Convert.ToString(dr["home_id"]);

                    ddl_home_list.Enabled = false;

                    ddl_fi_list.SelectedValue = Convert.ToString(dr["fi_id"]);
                    mortgage_loan_amount.Text = dr["mortgage_loan_amount"].ToString();
                    mortgage_payment_amount.Text = dr["mortgage_payment_amount"].ToString();
                    mortgage_interest_rate.Text = dr["mortgage_interest_rate"].ToString();
                    mortgage_term.Text = dr["mortgage_term"].ToString();

                    mortgage_current_term.Text = dr["mortgage_current_term"].ToString();

                    DateTime date_begin = new DateTime();
                    if (date_begin != null)
                    {
                        date_begin = Convert.ToDateTime(dr["mortgage_date_begin"]);
                        ddl_mortgate_date_begin_y.SelectedValue = date_begin.Year.ToString();
                        ddl_mortgate_date_begin_d.SelectedValue = date_begin.Day.ToString();
                        ddl_mortgate_date_begin_m.SelectedValue = date_begin.Month.ToString();
                    }


                    ddl_pf_list.SelectedValue = dr["pf_id"].ToString();
                    ddl_pi_list.SelectedValue = dr["pi_id"].ToString();

                }
            }



        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {

        Page.Validate("vg_mortgage");
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prMortgageUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params

            cmd.Parameters.Add("@update_successful", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@mortgage_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["mortgage_id"]);
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_home_list.SelectedValue);
            cmd.Parameters.Add("@fi_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_fi_list.SelectedValue);
            cmd.Parameters.Add("@mortgage_loan_amount", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(mortgage_loan_amount.Text));
            cmd.Parameters.Add("@mortgage_payment_amount", SqlDbType.Money).Value = Convert.ToDecimal(RegEx.getMoney(mortgage_payment_amount.Text));

            cmd.Parameters.Add("@mortgage_interest_rate", SqlDbType.Decimal).Value = Convert.ToDecimal(RegEx.getMoney(mortgage_interest_rate.Text));
            cmd.Parameters.Add("@mortgage_term", SqlDbType.Int).Value = Convert.ToInt32(RegEx.getInteger(mortgage_term.Text));
            cmd.Parameters.Add("@mortgage_current_term", SqlDbType.Int).Value = Convert.ToInt32(RegEx.getInteger(mortgage_current_term.Text));

            cmd.Parameters.Add("@pi_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_pi_list.SelectedValue);

            cmd.Parameters.Add("@pf_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_pf_list.SelectedValue);

            tiger.Date df = new tiger.Date();
            cmd.Parameters.Add("@mortgage_date_begin", SqlDbType.DateTime).Value = Convert.ToDateTime(df.DateCulture(ddl_mortgate_date_begin_m.SelectedValue, ddl_mortgate_date_begin_d.SelectedValue, ddl_mortgate_date_begin_y.SelectedValue, Convert.ToString(Session["_lastCulture"])));


            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            int new_update = 0;
            new_update= Convert.ToInt32(cmd.Parameters["@update_successful"].Value);
            if (new_update > 0)
                result.InnerHtml = " update mortgage successful";


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>

    protected void chk_fi_add_CheckedChanged(object sender, EventArgs e)
    {
        if (chk_fi_add.Checked == true)
        {


            panel_fi.Visible = true;

        }

        if (chk_fi_add.Checked == false)
        {
            panel_fi.Visible = false;
        }

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_fi_Click(object sender, EventArgs e)
    {

        Page.Validate();
        if (!Page.IsValid)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx");
        }

        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prFinancialInstitutionAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@new_fi_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@fi_name", SqlDbType.VarChar, 50).Value = fi_name.Text;
            cmd.Parameters.Add("@fi_website", SqlDbType.VarChar, 200).Value = fi_website.Text;

            cmd.Parameters.Add("@fi_addr", SqlDbType.VarChar, 50).Value = RegEx.getText(fi_addr.Text);
            cmd.Parameters.Add("@fi_city", SqlDbType.VarChar, 50).Value = RegEx.getText(fi_city.Text);
            cmd.Parameters.Add("@fi_prov", SqlDbType.VarChar, 50).Value = RegEx.getText(fi_prov.Text);
            cmd.Parameters.Add("@fi_pc", SqlDbType.VarChar, 50).Value = RegEx.getText(fi_pc.Text);
            cmd.Parameters.Add("@fi_tel", SqlDbType.VarChar, 50).Value = RegEx.getText(fi_tel.Text);
            cmd.Parameters.Add("@fi_contact_fname", SqlDbType.VarChar, 50).Value = RegEx.getText(fi_contact_fname.Text);
            cmd.Parameters.Add("@fi_contact_lname", SqlDbType.VarChar, 50).Value = RegEx.getText(fi_contact_lname.Text);
            cmd.Parameters.Add("@fi_contact_tel", SqlDbType.VarChar, 50).Value = RegEx.getText(fi_contact_tel.Text);
            cmd.Parameters.Add("@fi_contact_email", SqlDbType.VarChar, 50).Value = RegEx.getText(fi_contact_email.Text);
            cmd.Parameters.Add("@fi_contact_fax", SqlDbType.VarChar, 50).Value = RegEx.getText(fi_contact_fax.Text);
            cmd.Parameters.Add("@country_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_fi_country_list.SelectedValue);
            cmd.Parameters.Add("@fi_com", SqlDbType.Text).Value = RegEx.getText(fi_com.Text);




            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            int new_fi_id = 0;
            new_fi_id = Convert.ToInt32(cmd.Parameters["@new_fi_id"].Value);
            if (new_fi_id > 0)
                result.InnerHtml = " add successful";


            tiger.FinancialInstitution fi = new tiger.FinancialInstitution(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            ddl_fi_list.DataSource = fi.getFinancialInstitutionList(Convert.ToInt32(Session["schema_id"]));
            ddl_fi_list.DataBind();
            ddl_fi_list.SelectedValue = Convert.ToString(new_fi_id);
            panel_fi.Visible = false;
            chk_fi_add.Checked = false;


        }
        catch (Exception error)
        {
            tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
            //Response.Redirect("home_main.aspx");
            //  error.ToString();
            result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
        }


    }
}
