using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.tiger.security.AccountObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : nov 1, 2007
/// </summary>
public partial class mortgage_mortgage_list : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string referrer = "";

            //  referrer = Request.UrlReferrer.ToString();
            // the absolute path is "/sinfo/alerts/alerts.aspx "



            //if the url referer is from the reminder page
            if (referrer == "/sinfo/manager/alerts/alerts.aspx" || referrer == "/manager/alerts/alerts.aspx")
            {

                //this is the pending tab
                TabContainer1.ActiveTabIndex = 1;
               
            }
            else
            {
                SetDefaultView();
            }


          /*  tiger.Mortgage hv = new tiger.Mortgage(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_mortgage_list.DataSource = hv.getMortgageList(Convert.ToInt32(Session["schema_id"]),0);
            gv_mortgage_list.DataBind();

            gv_mortgage_archive_list.DataSource = hv.getMortgageArchiveList(Convert.ToInt32(Session["schema_id"]),0);
            gv_mortgage_archive_list.DataBind();

            gv_mortgage_pending_list.DataSource = hv.getMortgagePendingList(Convert.ToInt32(Session["schema_id"]),0);
            gv_mortgage_pending_list.DataBind();
         */

            tiger.Home l = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getHomeCount(Convert.ToInt32(Session["schema_id"]));

            int home_id = l.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));

            // first we check if there's any property available
            if (home_count > 0)
            {

                  referrer = Request.UrlReferrer.AbsolutePath.ToString();
              //if the url referer is from the reminder page
               if (referrer == "/sinfo/manager/alerts/alerts.aspx" || referrer == "/manager/alerts/alerts.aspx")
               {
                   if (!RegEx.IsInteger(Request.QueryString["home_id"]))
                   {
                       Session.Abandon();
                       Response.Redirect("~/login.aspx");
                   }
                   home_id = Convert.ToInt32(Request.QueryString["home_id"]);

               }


                //   tiger.Home l = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ddl_home_pending_list.DataSource = l.getHomeList(Convert.ToInt32(Session["schema_id"]));
                ddl_home_list.DataSource = ddl_home_pending_list.DataSource;
                ddl_home_archive_list.DataSource = ddl_home_pending_list.DataSource;

                ddl_home_pending_list.DataBind();
                ddl_home_pending_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));
                ddl_home_pending_list.SelectedValue = home_id.ToString();

                ddl_home_list.DataBind();
                ddl_home_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));

                ddl_home_archive_list.DataBind();
                ddl_home_archive_list.Items.Insert(0, new ListItem(Resources.Resource.lbl_all, "0"));

            }

            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd1 = new SqlCommand("prMortgagePendingList", conn);
            SqlCommand cmd2 = new SqlCommand("prMortgagePendingList", conn);
            SqlCommand cmd3 = new SqlCommand("prMortgagePendingList", conn);
            SqlCommand cmd4 = new SqlCommand("prMortgagePendingList", conn);

            SqlCommand cmd5 = new SqlCommand("prMortgageList", conn);
            SqlCommand cmd6 = new SqlCommand("prMortgageArchiveList", conn);


            cmd1.CommandType = CommandType.StoredProcedure;

            try
            {

                conn.Open();

                cmd1.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd1.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id ;
                cmd1.Parameters.Add("@time_frame", SqlDbType.Int).Value = 1;


                SqlDataAdapter da = new SqlDataAdapter(cmd1);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_mortgage_pending_list1.DataSource = dt;
                gv_mortgage_pending_list1.DataBind();
            }
            finally
            {
                //  conn.Close();
            }



            cmd2.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd2.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd2.Parameters.Add("@time_frame", SqlDbType.Int).Value = 2;


                SqlDataAdapter da = new SqlDataAdapter(cmd2);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_mortgage_pending_list2.DataSource = dt;
                gv_mortgage_pending_list2.DataBind();
            }
            finally
            {
                //  conn.Close();
            }




            cmd3.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd3.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd3.Parameters.Add("@time_frame", SqlDbType.Int).Value = 3;


                SqlDataAdapter da = new SqlDataAdapter(cmd3);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_mortgage_pending_list3.DataSource = dt;
                gv_mortgage_pending_list3.DataBind();
            }
            finally
            {
                //  conn.Close();
            }


            cmd4.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd4.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd4.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd4.Parameters.Add("@time_frame", SqlDbType.Int).Value = 4;


                SqlDataAdapter da = new SqlDataAdapter(cmd4);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_mortgage_pending_list4.DataSource = dt;
                gv_mortgage_pending_list4.DataBind();
            }
            finally
            {
                //    conn.Close();
            }


            cmd5.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd5.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd5.Parameters.Add("@home_id", SqlDbType.Int).Value = 0;
           
                SqlDataAdapter da = new SqlDataAdapter(cmd5);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_mortgage_list.DataSource = dt;
                gv_mortgage_list.DataBind();
            }
            finally
            {
               // conn.Close();
            }



            cmd6.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd6.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd6.Parameters.Add("@home_id", SqlDbType.Int).Value = 0;

                SqlDataAdapter da = new SqlDataAdapter(cmd6);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_mortgage_archive_list.DataSource = dt;
                gv_mortgage_archive_list.DataBind();
            }
            finally
            {
                // conn.Close();
            }

        }
    }



/// <summary>
/// 
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
    protected void gv_mortgage_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        TabContainer1.ActiveTabIndex = 0;
           int home_id = Convert.ToInt32(ddl_home_list.SelectedValue);
        
            // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
          
            tiger.Mortgage hv = new tiger.Mortgage(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_mortgage_list.PageIndex = e.NewPageIndex;
            gv_mortgage_list.DataSource = hv.getMortgageList(Convert.ToInt32(Session["schema_id"]),home_id);
            gv_mortgage_list.DataBind();

    }
/// <summary>
/// 
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
    protected void gv_mortgage_archive_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        int home_id = Convert.ToInt32(ddl_home_archive_list.SelectedValue);

        TabContainer1.ActiveTabIndex = 2;

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.Mortgage hv = new tiger.Mortgage(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_mortgage_archive_list.PageIndex = e.NewPageIndex;
        gv_mortgage_archive_list.DataSource = hv.getMortgageArchiveList(Convert.ToInt32(Session["schema_id"]),home_id);
        gv_mortgage_archive_list.DataBind();

    }


    protected void gv_mortgage_pending_list1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        TabContainer1.ActiveTabIndex = 1;

        int home_id = Convert.ToInt32(ddl_home_pending_list.SelectedValue);

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.Mortgage hv = new tiger.Mortgage(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_mortgage_pending_list1.PageIndex = e.NewPageIndex;
        gv_mortgage_pending_list1.DataSource = hv.getMortgagePendingList(Convert.ToInt32(Session["schema_id"]),home_id,1);
        gv_mortgage_pending_list1.DataBind();

    }


    protected void gv_mortgage_pending_list2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        TabContainer1.ActiveTabIndex = 1;

        int home_id = Convert.ToInt32(ddl_home_pending_list.SelectedValue);

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.Mortgage hv = new tiger.Mortgage(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_mortgage_pending_list2.PageIndex = e.NewPageIndex;
        gv_mortgage_pending_list2.DataSource = hv.getMortgagePendingList(Convert.ToInt32(Session["schema_id"]), home_id, 2);
        gv_mortgage_pending_list2.DataBind();

    }


    protected void gv_mortgage_pending_list3_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        TabContainer1.ActiveTabIndex = 1;

        int home_id = Convert.ToInt32(ddl_home_pending_list.SelectedValue);

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.Mortgage hv = new tiger.Mortgage(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_mortgage_pending_list3.PageIndex = e.NewPageIndex;
        gv_mortgage_pending_list3.DataSource = hv.getMortgagePendingList(Convert.ToInt32(Session["schema_id"]), home_id, 3);
        gv_mortgage_pending_list3.DataBind();

    }


    protected void gv_mortgage_pending_list4_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        TabContainer1.ActiveTabIndex = 1;

        int home_id = Convert.ToInt32(ddl_home_pending_list.SelectedValue);

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.Mortgage hv = new tiger.Mortgage(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_mortgage_pending_list4.PageIndex = e.NewPageIndex;
        gv_mortgage_pending_list4.DataSource = hv.getMortgagePendingList(Convert.ToInt32(Session["schema_id"]), home_id, 4);
        gv_mortgage_pending_list4.DataBind();

    }


    protected string GetInterestRate(decimal interest_rate)
    {


       // interest_rate = interest_rate * 100;

        return String.Format("{0:0.00}", interest_rate); 

    }

    /// <summary>
    /// 
    /// </summary>
    private void SetDefaultView()
    {
        TabContainer1.ActiveTabIndex = 0;

    }

    protected void ddl_home_pending_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        int home_id = Convert.ToInt32(ddl_home_pending_list.SelectedValue);

        TabContainer1.ActiveTabIndex = 1;

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd1 = new SqlCommand("prMortgagePendingList", conn);
        SqlCommand cmd2 = new SqlCommand("prMortgagePendingList", conn);
        SqlCommand cmd3 = new SqlCommand("prMortgagePendingList", conn);
        SqlCommand cmd4 = new SqlCommand("prMortgagePendingList", conn);

        cmd1.CommandType = CommandType.StoredProcedure;

        try
        {

            conn.Open();

            cmd1.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd1.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd1.Parameters.Add("@time_frame", SqlDbType.Int).Value = 1;


            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da.Fill(dt);


            gv_mortgage_pending_list1.DataSource = dt;
            gv_mortgage_pending_list1.DataBind();
        }
        finally
        {
            //  conn.Close();
        }



        cmd2.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd2.Parameters.Add("@time_frame", SqlDbType.Int).Value = 2;


            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);


            gv_mortgage_pending_list2.DataSource = dt;
            gv_mortgage_pending_list2.DataBind();
        }
        finally
        {
            //  conn.Close();
        }




        cmd3.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd3.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd3.Parameters.Add("@time_frame", SqlDbType.Int).Value = 3;


            SqlDataAdapter da = new SqlDataAdapter(cmd3);
            DataTable dt = new DataTable();
            da.Fill(dt);


            gv_mortgage_pending_list3.DataSource = dt;
            gv_mortgage_pending_list3.DataBind();
        }
        finally
        {
            //  conn.Close();
        }


        cmd4.CommandType = CommandType.StoredProcedure;

        try
        {

            cmd4.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd4.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd4.Parameters.Add("@time_frame", SqlDbType.Int).Value = 4;


            SqlDataAdapter da = new SqlDataAdapter(cmd4);
            DataTable dt = new DataTable();
            da.Fill(dt);


            gv_mortgage_pending_list4.DataSource = dt;
            gv_mortgage_pending_list4.DataBind();
        }
        finally
        {
                conn.Close();
        }

    }


    protected void ddl_home_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        int home_id = Convert.ToInt32(ddl_home_list.SelectedValue);

        TabContainer1.ActiveTabIndex = 0 ;

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.Mortgage hv = new tiger.Mortgage(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_mortgage_list.DataSource = hv.getMortgageList(Convert.ToInt32(Session["schema_id"]), home_id);
        gv_mortgage_list.DataBind();

    }


    protected void ddl_home_archive_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        int home_id = Convert.ToInt32(ddl_home_archive_list.SelectedValue);

        TabContainer1.ActiveTabIndex = 2;

        // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS

        tiger.Mortgage hv = new tiger.Mortgage(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        gv_mortgage_archive_list.DataSource = hv.getMortgageArchiveList(Convert.ToInt32(Session["schema_id"]), home_id);
        gv_mortgage_archive_list.DataBind();

    }
}
