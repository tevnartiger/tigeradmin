using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class manager_mp_manager : System.Web.UI.MasterPage 
{

    //By doing so, each session is ensuring a unique view state key and thus preventing the 
    //Cross-Site Request Forgery attacks. You can read more about that here 
    // http://en.wikipedia.org/wiki/Cross-site_request_forgery.

    void Page_Init(object sender, EventArgs e)
    {
        Page.ViewStateUserKey = Session.SessionID;

    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        HyperLink2.CssClass = "inactive";
        HyperLink3.CssClass = "inactive";
        HyperLink4.CssClass = "inactive";
        HyperLink5.CssClass = "inactive";
        HyperLink6.CssClass = "inactive";
        HyperLink7.CssClass = "inactive";
        HyperLink1.CssClass = "inactive";
        ds.CssClass = "inactive";


    }

    protected void bn_search_Click(object sender, EventArgs e)
    {
      //  Response.Redirect("~/manager/name/name_search.aspx?v=" + Convert.ToString(tb_search.Text), false);
    }
}
