﻿<%@ Page Language="C#" MaintainScrollPositionOnPostback="true"  MasterPageFile="~/tenant/tenant.master" AutoEventWireup="true" CodeFile="tenant_view.aspx.cs" Inherits="manager_tenant_tenant_view" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asv" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asv:ScriptManager ID="ScriptManager1" runat="server">
    </asv:ScriptManager>
    
 

<h4><b>TENANT&nbsp; - FOLDER</b></h4>
      <br />
       
        <b>NAME&nbsp; </b>: <b><asp:Label ID="lbl_name_lname"  runat="server"/>&nbsp;, <asp:Label ID="lbl_name_fname"  runat="server"/>
        </b>
        <br />
        <b>APPLICATION DATE :&nbsp;&nbsp;<asp:Label ID="lbl_tenantapplication_date_insert" runat="server"  Text=""></asp:Label>
        </b>
        <br />
  


<cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="2"  >
   <cc1:TabPanel ID="tab1" runat="server" HeaderText="<%$ Resources:Resource, lbl_roomate_and_application %>"  >
   <ContentTemplate  >
        
           <table width="100%">
               <tr>
                   <td bgcolor="aliceblue">
                       <b>        <asp:Label ID="Label7" runat="server" Text="<%$ Resources:Resource, lbl_current_roomate %>"></asp:Label></b></td>
               </tr>
           </table>
           <asp:GridView ID="gv_roomates_list" runat="server" AllowSorting="true" 
               AutoGenerateColumns="false" BorderColor="White" EmptyDataText="NONE" 
               GridLines="Both" Width="20%">
               <Columns>
                   <asp:BoundField DataField="tenant_name" />
                  
               </Columns>
           </asp:GridView>
           <br />
           <table width="100%">
               <tr>
                   <td bgcolor="aliceblue">
                       <b> <asp:Label ID="Label8" runat="server" Text="<%$ Resources:Resource, lbl_application_information %>"></asp:Label></b></td>
               </tr>
           </table>
           <br />

    <table bgcolor="#ffffcc" border="0" cellpadding="1" cellspacing="3">
           <tr>
               <td>
                   <asp:Label ID="Label9" runat="server" Text="<%$ Resources:Resource, lbl_dob %>"></asp:Label></td>
               <td colspan="2">
                   <asp:Label ID="lbl_name_dob" runat="server" />
               </td>
           </tr>
           <tr>
               <td>
                   <asp:Label ID="Label10" runat="server" Text="<%$ Resources:Resource, lbl_address %>"></asp:Label></td>
               <td>
                   <asp:Label ID="lbl_name_addr" runat="server"></asp:Label>
               </td>
               <td>
                  <asp:Label ID="Label11" runat="server" Text="<%$ Resources:Resource, lbl_city %>"/></td>
               <td>
                   <asp:Label ID="lbl_name_addr_city" runat="server" />
               </td>
           </tr>
           <tr>
               <td>
                  <asp:Label ID="Label12" runat="server" Text="<%$ Resources:Resource, lbl_pc %>"/></td>
               <td>
                   <asp:Label ID="lbl_name_addr_pc" runat="server" />
               </td>
               <td>
                   <asp:Label ID="Label13" runat="server" Text="<%$ Resources:Resource, lbl_prov %>"/></td>
               <td>
                   <asp:Label ID="lbl_name_addr_state" runat="server" />
               </td>
           </tr>
           <tr>
               <td>
                  <asp:Label ID="Label14" runat="server" Text="<%$ Resources:Resource, lbl_current_employer %>"/></td>
               <td>
                   <asp:Label ID="lbl_tenantapplication_current_employer" runat="server"></asp:Label>
               </td>
               <td>
                   <asp:Label ID="Label15" runat="server" Text="<%$ Resources:Resource, lbl_monthly_income %>"/></td>
               <td>
                   <asp:Label ID="lbl_tenantapplication_monthly_income" runat="server"></asp:Label>
               </td>
           </tr>
           <tr>
               <td>
                   <asp:Label ID="Label16" runat="server" Text="<%$ Resources:Resource, lbl_current_employer_since %>"/></td>
               <td colspan="3">
                   <asp:Label ID="lbl_tenantapplication_employer_since" runat="server"></asp:Label>
               </td>
           </tr>
           <tr>
               <td valign="top">
                 <asp:Label ID="Label17" runat="server" Text="<%$ Resources:Resource, lbl_live_with_you %>"/>  </td>
               <td colspan="3">
                   &nbsp;
                   <asp:Label ID="lbl_tenantapplication_people_with_prospect" runat="server" 
                       Height="50px" TextMode="MultiLine" Width="350px"></asp:Label>
               </td>
           </tr>
           <tr>
               <td>
                   <asp:Label ID="Label18" runat="server" Text="<%$ Resources:Resource, lbl_current_landlord_name %>"/></td>
               <td colspan="3">
                   <asp:Label ID="lbl_tenantapplication_landlord_name" runat="server"></asp:Label>
               </td>
           </tr>
           <tr>
               <td>
                   <asp:Label ID="Label19" runat="server" Text="<%$ Resources:Resource, lbl_current_landlord_tel %>"/></td>
               <td colspan="3">
                   <asp:Label ID="lbl_tenantapplication_landlord_tel" runat="server"></asp:Label>
               </td>
           </tr>
           <tr>
               <td valign="top">
                  <asp:Label ID="Label20" runat="server" Text="<%$ Resources:Resource, lbl_reason_departure %>"/></td>
               <td colspan="3">
                   &nbsp;
                   <asp:Label ID="lbl_tenantapplication_reason_depart" runat="server" 
                       Height="50px" TextMode="MultiLine" Width="350px"></asp:Label>
               </td>
           </tr>
           <tr>
               <td>
                   <asp:Label ID="Label21" runat="server" Text="<%$ Resources:Resource, lbl_ssn %>"/></td>
               <td colspan="3">
                   <asp:Label ID="lbl_name_ssn" runat="server"></asp:Label>
               </td>
           </tr>
           <tr>
               <td>
                   <asp:Label ID="Label22" runat="server" Text="<%$ Resources:Resource, lbl_country %>"/></td>
               <td colspan="3">
                   <asp:Label ID="lbl_country_name" runat="server" />
               </td>
           </tr>
           <tr>
               <td>
                   <asp:Label ID="Label23" runat="server" Text="<%$ Resources:Resource, lbl_tel%>"/></td>
               <td>
                   <asp:Label ID="lbl_name_tel" runat="server" />
               </td>
               <td>
                  <asp:Label ID="Label24" runat="server" Text="<%$ Resources:Resource, lbl_tel_work%>"/></td>
               <td>
                   <asp:Label ID="lbl_name_tel_work" runat="server" />
               </td>
           </tr>
           <tr>
               <td>
                   <asp:Label ID="Label25" runat="server" Text="<%$ Resources:Resource, lbl_tel_work_ext%>"/></td>
               <td>
                   <asp:Label ID="lbl_name_tel_work_ext" runat="server" />
               </td>
               <td>
                   <asp:Label ID="Label26" runat="server" Text="<%$ Resources:Resource, lbl_cell%>"/></td>
               <td>
                   <asp:Label ID="lbl_name_cell" runat="server" />
               </td>
           </tr>
           <tr>
               <td>
                  <asp:Label ID="Label27" runat="server" Text="<%$ Resources:Resource, lbl_fax%>"/></td>
               <td>
                   <asp:Label ID="lbl_name_fax" runat="server" />
               </td>
               <td>
                    <asp:Label ID="Label28" runat="server" Text="<%$ Resources:Resource, lbl_email%>"/></td>
               <td>
                   <asp:Label ID="lbl_name_email" runat="server" />
               </td>
           </tr>
           <tr>
               <td valign="top">
                    <asp:Label ID="Label29" runat="server" Text="<%$ Resources:Resource, lbl_comments%>"/></td>
               <td colspan="3">
                   <asp:Label ID="lbl_name_com" runat="server" TextMode="MultiLine" 
                       Width="389px" />
               </td>
           </tr>
       </table>       
       <br />
       <br />
       <table bgcolor="#ffffcc" border="0" cellpadding="1" cellspacing="3">
           <tr>
               <td>
                   <asp:Label ID="Label30" runat="server" Text="<%$ Resources:Resource, lbl_min_price%>"/></td>
               <td>
                   <asp:Label ID="lbl_pt_rent_min" runat="server"> </asp:Label>
               </td>
               <td>
                   <asp:Label ID="Label31" runat="server" Text="<%$ Resources:Resource, lbl_max_price%>"/></td>
               <td>
                   <asp:Label ID="lbl_pt_rent_max" runat="server"> </asp:Label>
               </td>
           </tr>
           <tr>
               <td>
                    <asp:Label ID="Label32" runat="server" Text="<%$ Resources:Resource, lbl_bedrooms%>"/></td>
               <td>
                   <asp:Label ID="lbl_pt_bedroom_no" runat="server"> </asp:Label>
               </td>
               <td>
                   <asp:Label ID="Label33" runat="server" Text="<%$ Resources:Resource, lbl_bathrooms%>"/></td>
               <td>
                   <asp:Label ID="lbl_pt_bathroom_no" runat="server"> </asp:Label>
               </td>
           </tr>
           <tr>
               <td>
                    <asp:Label ID="Label34" runat="server" Text="<%$ Resources:Resource, lbl_min_sqft%>"/></td>
               <td colspan="3">
                   <asp:Label ID="lbl_pt_min_sqft" runat="server"> </asp:Label>
               </td>
           </tr>
           <tr>
               <td>
                  <asp:Label ID="Label35" runat="server" Text="<%$ Resources:Resource, lbl_begining_date%>"/>
               </td>
               <td colspan="3">
                   <asp:Label ID="lbl_pt_date_begin" runat="server" Width="57px"></asp:Label>
               </td>
           </tr>
           <tr>
               <td>
                   <strong><span> <asp:Label ID="Label37" runat="server" Text="<%$ Resources:Resource, lbl_utilities%>"/></span></strong><br />
                   <asp:CheckBox ID="pt_electricity_inc" runat="server" text="<%$ Resources:Resource, lbl_electricity%>" />
                   <br />
                   <asp:CheckBox ID="pt_heat_inc" runat="server" text="<%$ Resources:Resource, lbl_heat%>" />
                   <br />
                   <asp:CheckBox ID="pt_water_inc" runat="server" Text="<%$ Resources:Resource, lbl_water%>" />
                   <br />
                   <asp:CheckBox ID="pt_water_tax_inc" runat="server" Text="<%$ Resources:Resource, lbl_water_tax%>" />
               </td>
               <td>
               </td>
               <td valign="top">
                   <span><strong> <asp:Label ID="Label38" runat="server" Text="<%$ Resources:Resource, lbl_accommodations%>"/></strong></span><br />
                   <asp:CheckBox ID="pt_parking_inc" runat="server" Text="<%$ Resources:Resource, lbl_parking%>" />
                   <br />
                   <asp:CheckBox ID="pt_garage_inc" runat="server" Text="<%$ Resources:Resource, lbl_garage%>" />
                   <br />
                   <asp:CheckBox ID="pt_furnished_inc" runat="server" Text="<%$ Resources:Resource, lbl_furnished%>" />
                   <br />
                   <asp:CheckBox ID="pt_semi_furnished_inc" runat="server" text="<%$ Resources:Resource, lbl_semi_furnished%>" />
                   <br />
               </td>
           </tr>
           <tr>
               <td>
                    <asp:Label ID="Label36" runat="server" Text="<%$ Resources:Resource, lbl_comments%>"/></td>
               <td colspan="3">
                   <asp:Label ID="pt_com" runat="server" TextMode="MultiLine" Width="389px" />
               </td>
           </tr>
       </table>
 
        

      </ContentTemplate  >
</cc1:TabPanel>






   <cc1:TabPanel ID="tab2" runat="server"  HeaderText="<%$ Resources:Resource, lbl_payment_history %>"  >
    <ContentTemplate  >     
     
         <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b><asp:Label ID="Label39" runat="server" Text="<%$ Resources:Resource, lbl_u_payment_history%>"/></b>
                </td>
            </tr>
        </table>
        
        <asp:GridView ID="gv_tenant_payment_archive" runat="server" AllowPaging="true" 
            AllowSorting="true" AlternatingRowStyle-BackColor="Beige" 
            AutoGenerateColumns="false" BorderColor="White" BorderWidth="3" 
            EmptyDataText="<%$ Resources:Resource, lbl_none%>" GridLines="Both" HeaderStyle-BackColor="AliceBlue" 
            OnPageIndexChanging="gv_tenant_payment_archive_PageIndexChanging" PageSize="10" 
            Width="70%">
            <Columns>
                <asp:BoundField DataField="home_name" HeaderText="<%$ Resources:Resource, lbl_property%>" />
                <asp:BoundField DataField="unit_door_no" HeaderText="<%$ Resources:Resource, lbl_door_number%>" />
                
                <asp:TemplateField HeaderText="Lease type"   >
                <ItemTemplate  >
                <asp:Label  runat="server"  ID="lbl_lease_type"    Text='<%#GetLeaseType(Convert.ToString(Eval("unit_type")))%>'    /> 
                </ItemTemplate>
                </asp:TemplateField>
                
                <asp:BoundField DataField="rp_due_date" DataFormatString="{0:M-dd-yyyy}" 
                    HeaderText="<%$ Resources:Resource, lbl_due_date%>" />
                <asp:BoundField DataField="rp_paid_date" DataFormatString="{0:M-dd-yyyy}" 
                    HeaderText="<%$ Resources:Resource, gv_paid_date%>" />
            </Columns>
        </asp:GridView>

               
        <br />
       
        
       <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b><asp:Label ID="Label40" runat="server" Text="<%$ Resources:Resource, lbl_u_rent_paid_on_time%>"/></b>
                </td>
            </tr>
        </table>
        <br />
        </b>
        <asp:Repeater ID="r_ontime_rent" runat="server">
            <ItemTemplate>
               <asp:Label ID="Label40" runat="server" Text="<%$ Resources:Resource, lbl_rent_paid_on_time%>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <%# Eval("ontime")%>
                <br />
                <asp:Label ID="Label41" runat="server" Text="<%$ Resources:Resource, lbl_number_of_payment%>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <%# Eval("total")%>
                <br />
                <asp:Label ID="Label42" runat="server" Text="<%$ Resources:Resource, lbl_percentage%>"/> &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<%# Eval("percentage")%>
            </ItemTemplate>
        </asp:Repeater>
        <br />
        
        
         <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b><asp:Label ID="Label42" runat="server" Text="<%$ Resources:Resource, lbl_late_rent_since_application%>"/> </b>
                </td>
            </tr>
        </table>
        
        
        
        <br />
        <br />
        <asp:Repeater ID="Repeater1" runat="server">
            <ItemTemplate>
                <asp:Label ID="Label42" runat="server" Text="<%$ Resources:Resource, lbl_less31%>"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : <%# Eval("less31")%>
                <br />
                <asp:Label ID="Label44" runat="server" Text="<%$ Resources:Resource, lbl_between_31and60%>"/>&nbsp; : <%# Eval("between31and60")%>
                <br />
                <asp:Label ID="Label45" runat="server" Text="<%$ Resources:Resource, lbl_between_61and90%>"/>&nbsp; :&nbsp;<%# Eval("between61and90")%>
                <br />
                <asp:Label ID="Label46" runat="server" Text="<%$ Resources:Resource, lbl_more90%>"/>&&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;<%# Eval("more91")%>
            </ItemTemplate>
        </asp:Repeater>
        <br />
        <br />
        <br />
        <table width="100%">
            <tr>
                <td bgcolor="aliceblue">
                    <b><asp:Label ID="Label43" runat="server" Text="<%$ Resources:Resource, lbl_late_rent_list%>"/></b>
                </td>
            </tr>
        </table>
        <br />
        <asp:GridView ID="gv_tenant_late_payment_archive" runat="server" 
            AlternatingRowStyle-BackColor="Beige" AutoGenerateColumns="false" 
            BorderColor="White" BorderWidth="3" EmptyDataText="<%$ Resources:Resource, lbl_none%>" GridLines="Both" 
            HeaderStyle-BackColor="AliceBlue" Width="70%">
            <Columns>
                <asp:BoundField DataField="home_name" HeaderText="<%$ Resources:Resource, lbl_property%>" />
                <asp:BoundField DataField="unit_door_no" HeaderText="<%$ Resources:Resource, lbl_door_number%>" />
               
                <asp:TemplateField HeaderText="Lease type"   >
                <ItemTemplate  >
                <asp:Label  runat="server"  ID="lbl_lease_type"    Text='<%#GetLeaseType(Convert.ToString(Eval("unit_type")))%>'    /> 
                </ItemTemplate>
                </asp:TemplateField>
                
                <asp:BoundField DataField="rp_due_date" DataFormatString="{0:M-dd-yyyy}" 
                    HeaderText="<%$ Resources:Resource, lbl_due_date%>" />
                <asp:BoundField DataField="rp_paid_date" DataFormatString="{0:M-dd-yyyy}" 
                    HeaderText="<%$ Resources:Resource, gv_paid_date%>" />
            </Columns>
        </asp:GridView>
        <br />
  </ContentTemplate  >
</cc1:TabPanel>
  

    
    
    
    
 <cc1:TabPanel ID="TabPanel2"  HeaderText="<%$ Resources:Resource, lbl_current_lease %>" runat="server"   >
    <ContentTemplate  >
       
     <table>
       <tr>
        <td>


       <table width="100%">
        <tr>
        <td bgcolor="aliceblue" style="height: 18px"> <b> <asp:Label ID="Label47" 
                runat="server" Text="<%$ Resources:Resource, lbl_current_lease %>"/></b></td>
        </tr>
      </table>

<br />
     
    <br />
<asp:Repeater runat="server" ID="r_HomeUnitView">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
        
        <tr>
          <td>
             <b><asp:Label ID="Label51" runat="server" Text="<%$ Resources:Resource, lbl_district%>"></asp:Label>&nbsp;:&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_district")%> 
                  </b> </td>
            </tr>
           <tr>
              <td valign="top" >
              <b><%#DataBinder.Eval(Container.DataItem, "home_addr_no")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_street")%></b></td>
            </tr>
            <tr>
                 <td valign="top" >
             <b>   <%#DataBinder.Eval(Container.DataItem, "home_city")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_prov")%>
                   &nbsp;,&nbsp;&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_pc")%> </b>
                   </td>
               
            </tr>
            <tr>
                 <td valign="top" >
                 <b><asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_unit%>"></asp:Label></b>  &nbsp;:&nbsp;<%#DataBinder.Eval(Container.DataItem, "unit_door_no")%> 
                
                   </td>
               
            </tr>   
        </table>
        
        </ItemTemplate>
        </asp:Repeater>
&nbsp;<br />

<table >
        <tr id="tr_lease_type" runat="server">
            <td>
               <b>Lease type</b> </td>
            <td>
                &nbsp;:&nbsp;&nbsp;<asp:Label ID="lbl_lease_type" runat="server" ></asp:Label></td>
        </tr>
        <tr id="tr_company" runat="server">
            <td>
               <b> Company</b></td>
            <td>
               &nbsp;:&nbsp;&nbsp; <asp:Label ID="lbl_company" runat="server" ></asp:Label></td>
        </tr>
    </table>
&nbsp;<table  width="100%"><tr><td style="color: #D3D3D3">
        ___________________________________________________________________________________________</td></tr></table>
    <br />
    <table>
<tr>
<td><div id="txt_current_tenant_name" runat="server" />
</td>
</tr>

</table>
<br />

<table >
<tr>
<td valign="top"><table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td>
            <b>   <asp:Label ID="Label53" runat="server" 
                Text="<%$ Resources:Resource, lbl_rent_amount %>"></asp:Label></b>&nbsp;</td>
                                          <td>
                                              : <asp:Label ID="lbl_rent_amount" runat="server" />
                                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
    <b>   <asp:Label ID="Label2" runat="server" 
                Text="<%$ Resources:Resource, lbl_since %>"></asp:Label></b></td>
                                          <td>
                                              :
<asp:Label ID="lbl_current_rl_date_begin" runat="server" style="color: #FF3300"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                          </td>
                                      </tr>
                                  </table>
                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                              </td>
    <td valign="top">
        <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="100%" BorderColor="White" BorderWidth="3"
        GridLines="Both"   
          AutoGenerateColumns="false"   AlternatingRowStyle-BackColor="Beige" ID="gv_rent_list" runat="server">
        
         <Columns>
   <asp:BoundField   DataField="rl_rent_amount" DataFormatString="{0:0.00}"  
     HeaderText="Previous Rent amount" HtmlEncode="false" /> 
   <asp:BoundField   DataField="rl_date_begin" DataFormatString="{0:M-dd-yyyy}"  
     HeaderText="Date Begin" HtmlEncode="false" />
   <asp:BoundField   DataField="rl_date_end" DataFormatString="{0:M-dd-yyyy}"  
     HeaderText="Date End" HtmlEncode="false" />
      <asp:BoundField DataField="rl_id" HeaderText="rl_id"   />
      
   </Columns>
        
        
        </asp:GridView>
                              </td>
</tr> 
   
 </table >
<br />

<table width="100%">
     
     
     <tr>
     <td bgcolor="aliceblue" colspan="2">
         <b><asp:Label ID="Label54" runat="server" Text="<%$ Resources:Resource, lbl_u_utilities_accommodations %>"></asp:Label></b><br />
    
     </td></tr>
     
         
     <tr>
     <td valign="top" >
     <b><asp:Label ID="Label55" runat="server" 
             Text="<%$ Resources:Resource, lbl_accommodation_since %>"></asp:Label> </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :
 <asp:Label ID="lbl_current_ta_date_begin" runat="server" style="color: #FF3300"></asp:Label>
     
     </td>
     
      <td valign="top" rowspan="2">
          <asp:GridView  HeaderStyle-BackColor="AliceBlue" Width="100%" BorderColor="White" BorderWidth="3"
        GridLines="Both"  
          AutoGenerateColumns="false"   AlternatingRowStyle-BackColor="Beige"   ID="gv_accommodation_list" runat="server">
          
          
           <Columns>
   
   <asp:BoundField   DataField="ta_date_begin" DataFormatString="{0:M-dd-yyyy}"  
     HeaderText="Previous date Begin" HtmlEncode="false" />
   <asp:BoundField   DataField="ta_date_end" DataFormatString="{0:M-dd-yyyy}"  
     HeaderText="Previous date End" HtmlEncode="false" />
      <asp:BoundField DataField="ta_id" HeaderText="ta_id"   />
      
      <asp:HyperLinkField   Text="View"
     DataNavigateUrlFields="tu_id,ta_id" 
     DataNavigateUrlFormatString="~/tenant/lease/lease_accommodation_archive_view.aspx?tu_id={0}&ta_id={1}" 
      HeaderText="View" />
      
   </Columns>
          
          
          </asp:GridView>
     
     
          <br />
     
     
     </td></tr>
     
         
     <tr>
     <td >
          <strong><span style="font-size: 10pt">
        <asp:Label ID="Label56" runat="server"
             Text="<%$ Resources:Resource, lbl_utilities_included %>"></asp:Label></span></strong><br />
     <asp:CheckBox AutoPostBack=True  ID="ta_electricity_inc" 
             text="<%$ Resources:Resource, lbl_electricity %>" runat="server"  />
     <br />
     <asp:CheckBox AutoPostBack=True  ID="ta_heat_inc" 
             text="<%$ Resources:Resource, lbl_heat %>" runat="server"  />
     <br />
     <asp:CheckBox AutoPostBack=True  ID="ta_water_inc" 
             text="<%$ Resources:Resource, lbl_water %>" runat="server"   />
     <br />
     <asp:CheckBox AutoPostBack=True  ID="ta_water_tax_inc" 
             text="<%$ Resources:Resource, lbl_water_tax %>" runat="server"   />
     <br />
     <asp:CheckBox AutoPostBack=True  ID="ta_none" 
             text="<%$ Resources:Resource, lbl_none %>" runat="server"   />
    
     
     
     
     <br /><br />
     
     
     
       
         <span style="font-size: 10pt"><strong>
         <asp:Label ID="Label57" runat="server"
             Text="<%$ Resources:Resource, lbl_accommodation_included %>"/></strong></span><br /><br />
     <asp:CheckBox  ID="ta_parking_inc" text="<%$ Resources:Resource, lbl_parking %>" 
             runat="server" />
     <br />
     <asp:CheckBox  ID="ta_garage_inc" text="<%$ Resources:Resource, lbl_garage %>" 
             runat="server" />
     <br />
     <asp:CheckBox  ID="ta_furnished_inc" 
             text="<%$ Resources:Resource, lbl_furnished %>" runat="server" />
     <br />
     <asp:CheckBox  ID="ta_semi_furnished_inc" 
             text="<%$ Resources:Resource, lbl_semi_furnished %>" runat="server" />
     <br />
     
     </td>
     
      </tr>
     
         
           <tr><td  valign=top style="font-weight: bold"><asp:Label ID="Label58" runat="server" 
                   Text="<%$ Resources:Resource, lbl_comment_extra %>"></asp:Label></td>
                <td>&nbsp; <div   ID="lbl_ta_com"  runat="server" />
                    
                    <br />
                              </td>
            </tr>
    <tr><td colspan="2" style="height: 17px">
     
     <table>

 <tr>
 <td>
     &nbsp;</td>
 </tr>
</table>
     
     
      <br />
         <table width="90%">
        <tr>
        <td bgcolor="aliceblue"><b>
            <asp:Label ID="Label59" runat="server" 
                Text="<%$ Resources:Resource, lbl_u_term_and_condition %>"></asp:Label></b><br /></td>
        </tr>
        </table>
     
      <br />
     </td></tr>
          
         </table>
    
  
  <table width="100%">
     
     
     <tr>
     <td bgcolor="aliceblue" colspan="2">
         <b>
             <asp:Label ID="Label3" runat="server" 
             Text="<%$ Resources:Resource, lbl_u_utilities_accommodations %>"></asp:Label></b><br />
    
     </td></tr>
     
         
     <tr>
     <td valign="top" >
     <b><asp:Label ID="Label4" runat="server" 
             Text="<%$ Resources:Resource, lbl_accommodation_since %>"></asp:Label>&nbsp;</b>:
 <asp:Label ID="Label5" runat=server style="color: #FF3300"></asp:Label>
     
     </td>
     
      <td valign="top" rowspan="2" align="right">
          <asp:GridView Width="80%" BorderColor="White" BorderWidth="3px"  
          AutoGenerateColumns="False"   ID="GridView1" runat="server">
          
          
           <Columns>
   
   <asp:BoundField   DataField="ta_date_begin" DataFormatString="{0:M-dd-yyyy}"  
     HeaderText="<%$ Resources:Resource, lbl_previous_accommodation_begin %>" 
                   HtmlEncode="False" />
   <asp:BoundField   DataField="ta_date_end" DataFormatString="{0:M-dd-yyyy}"  
     HeaderText="<%$ Resources:Resource, lbl_previous_accommodation_end %>" 
                   HtmlEncode="False" />
    
      
      <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_view %>"
     DataNavigateUrlFields="tu_id,ta_id" 
     DataNavigateUrlFormatString="~/tenant/lease/lease_accommodation_archive_view.aspx?tu_id={0}&ta_id={1}" 
      HeaderText="<%$ Resources:Resource, lbl_view %>" />
      
   </Columns>
          
          
              <AlternatingRowStyle BackColor="Beige" />
              <HeaderStyle BackColor="AliceBlue" />
          
          
          </asp:GridView>
     
     
          <br />
     
     
     </td></tr>
     
         
     <tr>
     <td >
         <strong><span style="font-size: 10pt">
        <asp:Label ID="Label6" runat="server"
             Text="<%$ Resources:Resource, lbl_utilities_included %>"></asp:Label></span></strong><br />
     <asp:CheckBox AutoPostBack=True  ID="CheckBox1" 
             text="<%$ Resources:Resource, lbl_electricity %>" runat="server"  />
     <br />
     <asp:CheckBox AutoPostBack=True  ID="CheckBox2" 
             text="<%$ Resources:Resource, lbl_heat %>" runat="server"  />
     <br />
     <asp:CheckBox AutoPostBack=True  ID="CheckBox3" 
             text="<%$ Resources:Resource, lbl_water %>" runat="server"   />
     <br />
     <asp:CheckBox AutoPostBack=True  ID="CheckBox4" 
             text="<%$ Resources:Resource, lbl_water_tax %>" runat="server"   />
     <br />
     <asp:CheckBox AutoPostBack=True  ID="CheckBox5" 
             text="<%$ Resources:Resource, lbl_none %>" runat="server"   />
    
     
     
     
     <br /><br />
     
     
     
       
         <span style="font-size: 10pt"><strong>
         <asp:Label ID="Label48" runat="server"
             Text="<%$ Resources:Resource, lbl_accommodation_included %>"/></strong></span><br /><br />
     <asp:CheckBox  ID="CheckBox6" text="<%$ Resources:Resource, lbl_parking %>" 
             runat="server" />
     <br />
     <asp:CheckBox  ID="CheckBox7" text="<%$ Resources:Resource, lbl_garage %>" 
             runat="server" />
     <br />
     <asp:CheckBox  ID="CheckBox8" 
             text="<%$ Resources:Resource, lbl_furnished %>" runat="server" />
     <br />
     <asp:CheckBox  ID="CheckBox9" 
             text="<%$ Resources:Resource, lbl_semi_furnished %>" runat="server" />
     <br />
     
     </td>
     
      </tr>
     
         
           <tr><td  valign="top" style="font-weight: bold">
               <asp:Label ID="Label49" runat="server" 
                   Text="<%$ Resources:Resource, lbl_comment_extra %>"></asp:Label>&nbsp;:</td>
                <td>&nbsp;
                
                
                    <div   ID="Div1"  runat="server"      />
                    
                 
                 
                    <br />
                              </td>
            </tr>
    <tr><td colspan="2" style="height: 17px">
     
     <table>

 <tr>
 <td>
     &nbsp;</td>
 </tr>
</table>
     
     
      <br />
        <table width="90%">
        <tr>
        <td bgcolor="aliceblue"><b>
            <asp:Label ID="Label50" runat="server" 
                Text="<%$ Resources:Resource, lbl_u_term_and_condition %>"></asp:Label></b><br /></td>
        </tr>
        </table>
     
      <br />
     </td></tr>
          
         </table>
    
  
  <table width="100%">
          
          
          <tr>
<td valign="top" style="width: 234px">
    <b><asp:Label ID="Label60" runat="server" 
        Text="<%$ Resources:Resource, lbl_form_of_payment %>"></asp:Label> </b> </td>
<td colspan="2">
    <asp:Label ID="tt_form_of_payment" runat="server" ></asp:Label>
</td>
</tr>
<tr>
<td valign="top" colspan="3">
    
    <table width="100%"><tr><td style="color: #D3D3D3">
        _______________________________________________________________________________________</td></tr></table>
    </td>
</tr>
<tr>
<td valign="top" style="width: 234px">
    <b><asp:Label ID="Label61" runat="server" 
        Text="<%$ Resources:Resource, lbl_nsf_fee %>"/></b>  &nbsp; </td>
<td colspan="2">
<asp:Label ID="tt_nsf" runat="server" Width="128px"/></td>
</tr>
        <tr>
        <td  valign=top colspan="3"> 
    
    <table width="100%"><tr><td style="color: #D3D3D3">
        _________________________________________________________________________________________</td></tr></table>
            </td>
        
        
        </tr>
        <tr>
        <td  valign=top style="width: 234px"><b><asp:Label ID="Label62" runat="server" 
                Text="<%$ Resources:Resource, lbl_late_fee %>"/></b></td>
        <td>
            <asp:Label ID="tt_late_fee" runat="server" Width="128px"/>
        </td>
        
        
        </tr>
        <tr><td valign=top colspan="3">
    
    <table width="100%"><tr><td style="color: #D3D3D3">
        ________________________________________________________________________________________</td></tr></table>
            </td>
       </tr>
            
            
          
            
            
        <tr><td valign=top style="width: 234px"><b><asp:Label ID="Label63" runat="server" 
                Text="<%$ Resources:Resource, lbl_security_deposit_required %>"/></b></td><td>
            <asp:RadioButtonList  ID="tt_security_deposit" runat="server" 
                                  RepeatDirection="Horizontal">
                <asp:ListItem Selected="True" Text="No" Value="0"></asp:ListItem>
                   <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
            </asp:RadioButtonList>
            
            <asp:Panel ID="Panel_security_deposit" runat="server" >
            
            
                &nbsp;&nbsp;&nbsp;<asp:Label ID="Label64" runat="server" 
                    Text="<%$ Resources:Resource, lbl_amount %>"></asp:Label>&nbsp;: <asp:Label ID="tt_security_deposit_amount" runat="server"/>
     </asp:Panel>       
            
            
            
            </td>
       </tr>
            
             
            
            
        <tr><td valign=top colspan="3"> 
    
    <table width="100%"><tr><td style="color: #D3D3D3">
        ________________________________________________________________________________________</td></tr></table>
            </td></tr>
           
        <tr><td valign=top style="width: 234px"><b>
            <asp:Label ID="Label65" runat="server" 
                Text="<%$ Resources:Resource, lbl_guarantor_required %>"></asp:Label></b> </td><td>
            <asp:RadioButtonList  ID="tt_guarantor" runat="server" 
                AutoPostBack="True" RepeatDirection="Horizontal"   >
            <asp:ListItem Selected="True" Text="No" Value="0"></asp:ListItem>
              <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
            </asp:RadioButtonList>
            <asp:Panel ID="panel_guarantor" runat="server" Visible="False">
                <b>&nbsp; name</b>&nbsp;: 
                <asp:Label ID="guarantor_name" runat="server" ></asp:Label>
                <br />
                
                <!--  BEGIN GUARANTOR INFORMATIONS    -->
                
                <!-- END GUARANTOR INFORMATIONS      -->
            </asp:Panel>
            </td></tr>
            
                     
            
        <tr><td valign=top colspan="3"> 
    
    <table width="90%"><tr><td style="color: #D3D3D3">
        ________________________________________________________________________________________</td></tr></table>
            </td></tr>
        <tr>
        <td valign=top style="width: 234px"><b>
            <asp:Label ID="Label66" runat="server" 
                Text="<%$ Resources:Resource, lbl_pets_allowed %>"></asp:Label></b></td>
        <td>
            <asp:RadioButtonList    ID="tt_pets" runat="server" 
                RepeatDirection="Horizontal" >
                <asp:ListItem Selected="True" Text="No" Value="0"></asp:ListItem>
                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
            </asp:RadioButtonList>
         </td>
        </tr>
        <tr valign="top">
            <td valign=top colspan="3"> 
    
    <table width="100%"><tr><td style="color: #D3D3D3">
        ________________________________________________________________________________________</td></tr></table>
            </td>
            
        </tr>
        <tr valign="top">
            <td valign=top style="width: 234px"><b><asp:Label ID="Label68" runat="server" 
                    Text="<%$ Resources:Resource, lbl_tenant_maintenance %>"></asp:Label></b></td>
         <td>
          <asp:RadioButtonList    ID="tt_maintenance" runat="server" RepeatDirection="Horizontal" 
                  >
            <asp:ListItem Selected="True" Text="No" Value="0"></asp:ListItem>
            <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
            </asp:RadioButtonList>
           <br />
                &nbsp;&nbsp;&nbsp;<b><asp:Label ID="Label67" runat="server" 
                 Text="<%$ Resources:Resource, lbl_maintenance_allowed %>"></asp:Label></b><br />&nbsp;<asp:Label runat=server 
                     ID="tt_specify_maintenance"  Width="300px" /> 
           
         
         </td> 
            
        </tr>
        <tr>
        <td valign=top colspan="3"> 
    
    <table width="90%"><tr><td style="color: #D3D3D3">
        ________________________________________________________________________________________</td></tr></table>
            </td>
        </tr>
        <tr>
        <td valign=top style="width: 234px"><b><asp:Label ID="Label70" runat="server" 
                Text="<%$ Resources:Resource, lbl_tenant_improvement %>"></asp:Label></b></td>
        <td>
            <asp:RadioButtonList    ID="tt_improvement" runat="server" RepeatDirection="Horizontal" 
                 >
                <asp:ListItem Selected="True" Text="No" Value="0"></asp:ListItem>
                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
            </asp:RadioButtonList>
             <asp:Panel ID="panel_specify_improvement" runat="server">
                 &nbsp;&nbsp;&nbsp;<b><asp:Label ID="Label69" runat="server" 
                     Text="<%$ Resources:Resource, lbl_improvement_allowed %>"></asp:Label></b><br />
                 &nbsp;<asp:Label runat=server Width="300px" ID="tt_specify_improvement" 
                      />
            </asp:Panel>
         
        </td>
        </tr>
        <tr>
        <td valign=top colspan="3"> 
    
    <table width="90%"><tr><td style="color: #D3D3D3">
        ________________________________________________________________________________________</td></tr></table>
            </td>
        </tr>
        <tr>
        <td valign=top style="width: 234px"><b>
            <asp:Label ID="Label71" runat="server" 
                Text="<%$ Resources:Resource, lbl_notice_to_enter %>"></asp:Label></b> </td>
        <td>
            <asp:RadioButtonList ID="tt_notice_to_enter" runat="server"  RepeatDirection="Horizontal" 
               >
                <asp:ListItem Selected="True" Text="No" Value="0"></asp:ListItem>
                <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
            </asp:RadioButtonList>
            <br />
                &nbsp;<b><asp:Label ID="Label72" runat="server" 
                Text="<%$ Resources:Resource, lbl_number_of_hours_notice %>"></asp:Label></b>&nbsp;: <asp:Label ID="tt_specify_number_of_hours" 
                    runat="server" Width="100px"/>
   
        </td>
        </tr>
        <tr><td valign=top colspan="3"> 
    
    <table width="90%"><tr><td style="color: #D3D3D3">
        ________________________________________________________________________________________</td></tr></table>
            </td>
        
        </tr>
        
        
        
        <tr><td valign=top style="width: 234px"><b>
            <asp:Label ID="Label73" runat="server" 
                Text="<%$ Resources:Resource, lbl_who_pay_insurances %>"></asp:Label></b></td>
        
        <td>
            <table>
                <tr>
                    <td>
                        <b>
                            <asp:Label ID="Label74" runat="server" 
                            Text="<%$ Resources:Resource, lbl_tenant_content %>"></asp:Label></b></td>
                    <td valign="top">
                        :
                        <asp:Label ID="tt_tenant_content_ins" runat="server" ></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <b><asp:Label ID="Label75" runat="server" 
                            Text="<%$ Resources:Resource, lbl_landlord_content %>"></asp:Label></b></td>
                    <td valign="top">
                        :<asp:Label ID="tt_landlord_content_ins" runat="server" ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>
                            <asp:Label ID="Label76" runat="server" 
                            Text="<%$ Resources:Resource, lbl_personal_injury %>"></asp:Label></b></td>
                    <td valign="top">
                        :<asp:Label ID="tt_injury_ins" runat="server" ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>
                            <asp:Label ID="Label77" runat="server" 
                            Text="<%$ Resources:Resource, lbl_lease_premises %>"></asp:Label></b></td>
                    <td valign="top">
                        :
                        <asp:Label ID="tt_premises_ins" runat="server" ></asp:Label>
                    </td>
                </tr>
            </table>
            </td>
        
        </tr>
        
        
        
        <tr><td valign="top" colspan="3">
    
    <table width="90%"><tr><td style="color: #D3D3D3">
        ________________________________________________________________________________________</td></tr></table>
             </td>
              </tr>
          
        
        
        <tr><td valign="top" style="width: 234px">
            <b>
                <asp:Label ID="Label78" runat="server"  
                Text="<%$ Resources:Resource, lbl_additional_terms %>"></asp:Label></b></td>
            <td> 
                    
                <div   ID="tt_additional_terms" runat="server"   />
                
                <br />
                <br />
            </td>
            <td valign="top">
                &nbsp;</td>
              </tr>
          <tr>
          <td colspan="3" valign="top" >
              <table cellpadding="0" cellspacing="0" style="width: 100%">
                  <tr>
                      <td valign="top">
              <b><asp:Label ID="Label79" runat="server"  
                              Text="<%$ Resources:Resource, lbl_terms_since %>" ></asp:Label> : </b>
  
   <asp:Label ID="lbl_current_tt_date_begin" runat="server" style="color: #FF3300"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      </td>
                      <td valign="top">
          
          
     &nbsp;
          
          
     <asp:GridView Width="100%" BorderColor="White" BorderWidth="3px"  
          AutoGenerateColumns="False"  ID="gv_terms_list" runat="server"  >
         
           <Columns>
   
   <asp:BoundField   DataField="tt_date_begin" DataFormatString="{0:M-dd-yyyy}"  
     HeaderText="<%$ Resources:Resource, lbl_previous_date_begin %>" HtmlEncode="False" />
   <asp:BoundField   DataField="tt_date_end" DataFormatString="{0:M-dd-yyyy}"  
     HeaderText="<%$ Resources:Resource, lbl_previous_date_end %>" HtmlEncode="False" />
      <asp:BoundField DataField="tt_id" HeaderText="ta_id"   />
      
      <asp:HyperLinkField   Text="<%$ Resources:Resource, lbl_view %>"
     DataNavigateUrlFields="tu_id,tt_id" 
     DataNavigateUrlFormatString="~/tenant/lease/lease_termsandconditions_archive_view.aspx?tu_id={0}&tt_id={1}" 
      HeaderText="<%$ Resources:Resource, lbl_view %>" />
      
   </Columns>           
                    
                    
           <AlternatingRowStyle BackColor="Beige" />
           <HeaderStyle BackColor="AliceBlue" />
                    
                    
     </asp:GridView>
     
     
     
                      </td>
                  </tr>
              </table>
              <br />
              </td>
          </tr>
              <tr>
                  <td style="width: 234px">
                      <div ID="txt_link" runat="server" />
                      </td>
                  </tr>
        </table>



</td>
</tr>
</table> 
        
        
  </ContentTemplate  >
</cc1:TabPanel>
    
    
  
  <cc1:TabPanel ID="tab4" HeaderText="<%$ Resources:Resource, lbl_lease_archive %>" runat="server">
      <ContentTemplate>
  
  
     <table width="100%">
          <tr>
              <td bgcolor="aliceblue">
                  <b> <asp:Label ID="Label52" runat="server" Text="<%$ Resources:Resource, lbl_lease_archive%>"/></b>
              </td>
          </tr>
      </table>
      <br />
      <asp:GridView ID="gv_tenant_lease_archives" runat="server" AllowSorting="true" 
          AlternatingRowStyle-BackColor="Beige" AutoGenerateColumns="false" 
          BorderColor="White" BorderWidth="3" EmptyDataText="NONE" GridLines="Both" 
          HeaderStyle-BackColor="AliceBlue" Width="100%">
          <Columns>
              <asp:BoundField DataField="home_name" HeaderText="<%$ Resources:Resource, lbl_property%>" />
              <asp:BoundField DataField="unit_door_no" HeaderText="<%$ Resources:Resource, lbl_door_number%>" />
              
               <asp:TemplateField HeaderText="Lease type"   >
               <ItemTemplate  >
               <asp:Label  runat="server"  ID="lbl_lease_type"    Text='<%#GetLeaseType(Convert.ToString(Eval("unit_type")))%>'    /> 
               </ItemTemplate>
               </asp:TemplateField>
              
              <asp:BoundField DataField="tu_date_begin" DataFormatString="{0:M-dd-yyyy}" 
                  HeaderText="<%$ Resources:Resource, gv_lease_date_begin%>" />
              <asp:BoundField DataField="tu_date_end" DataFormatString="{0:M-dd-yyyy}" 
                  HeaderText="<%$ Resources:Resource, lbl_lease_date_end%>" />
              <asp:HyperLinkField DataNavigateUrlFields="tu_id,tenant_id" 
                  DataNavigateUrlFormatString="~/tenant/lease/lease_archive_view.aspx?tu_id={0}&t_id={1}" 
                  Text="<%$ Resources:Resource, lbl_view%>" />
          </Columns>
      </asp:GridView> 
  
 </ContentTemplate  >
</cc1:TabPanel>
    
    
   <cc1:TabPanel ID="tab5" runat="server" HeaderText="<%$ Resources:Resource, lbl_request_history %>"  >
    <ContentTemplate  >
  
      <table width="100%">
           <tr>
               <td bgcolor="aliceblue">
                   <b><asp:Label ID="Label80" runat="server" Text="<%$ Resources:Resource, lbl_u_request_history%>"/></b>
               </td>
           </tr>
       </table>
       request 1 request 2 ... 2nnnnnn<br />
  
  
  
   </ContentTemplate  >
</cc1:TabPanel>
  
  
  
  
   
   <cc1:TabPanel ID="tab6" HeaderText="<%$ Resources:Resource, lbl_incident_history %>" runat="server"   >
    <ContentTemplate  >
  
  
  
       <table width="100%">
           <tr>
               <td bgcolor="aliceblue">
                   <b><asp:Label ID="Label81" runat="server" Text="<%$ Resources:Resource, lbl_u_incident_history%>"/></b>
               </td>
           </tr>
       </table>
       <br /><br />
  
  
  
  </ContentTemplate  >
</cc1:TabPanel>
    
    
</cc1:TabContainer>

 
</asp:Content>

