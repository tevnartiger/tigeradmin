﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class tenant_tenant_tenant_main : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            /*
            tiger.Rent hp = new tiger.Rent(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_rent_name_delequency.DataSource = hp.getRentNameDelequencyList(Convert.ToInt32(Session["schema_id"]),Convert.ToInt32(Session["name_id"]));
            gv_rent_name_delequency.DataBind();
             * 
             * */



            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prRentNameDelequencyList", conn);
            SqlCommand cmd2 = new SqlCommand("prNameView", conn);

            cmd.CommandType = CommandType.StoredProcedure;

            try
            {

                conn.Open();

                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);


                gv_rent_name_delequency.DataSource = dt;
                gv_rent_name_delequency.DataBind();
            }
            finally
            {
                //  conn.Close();
            }



            cmd2.CommandType = CommandType.StoredProcedure;

            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);

            SqlDataReader dr = null;
            dr = cmd2.ExecuteReader(CommandBehavior.SingleRow);


            while (dr.Read() == true)
            {
                lbl_name.Text = dr["name"].ToString();
               
            }
            
            conn.Close();

        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="amount_of_warning_sent"></param>
    /// <returns></returns>
    protected string Get_AmountOfWarningSent(int amount_of_warning_sent)
    {

        string warning_sent = "";


        if (amount_of_warning_sent == 0)
        {
            warning_sent = Resources.Resource.lbl_none;
        }

        if (amount_of_warning_sent == 1)
        {
            warning_sent = Resources.Resource.lbl_first_notice;
        }

        if (amount_of_warning_sent == 2)
        {
            warning_sent = Resources.Resource.lbl_second_notice;
        }

        if (amount_of_warning_sent == 3)
        {
            warning_sent = Resources.Resource.lbl_third_notice;
        }

        return warning_sent;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    protected string GetLeaseType(string type)
    {
        string lease_type = "";

        if (type == "R")
            lease_type = "Residential";
        else
            lease_type = "Commercial";

        return lease_type;

    }


    protected void link_logout_Click(object sender, EventArgs e)
    {
        Session.Abandon();
        Response.Redirect("~/login.aspx");
    }
}
