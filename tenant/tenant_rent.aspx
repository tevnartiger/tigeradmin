<%@ Page Language="C#" AutoEventWireup="true" CodeFile="tenant_rent.aspx.cs" Inherits="tenant_tenant_rent" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Tenant rent</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        TENANT RENT<br />
        <br />
        <strong><span style="text-decoration: underline">
           Address</span> :</strong><br />
     <asp:Repeater runat=server ID="rhome_view">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
        
        <tr>
            
                 <td>
                 District&nbsp;:&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_district")%> 
                   </td>
               
            </tr>
        
            <tr>
            
                 <td >
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_no")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_street")%></td>
               
            </tr>
            <tr>
                 <td >
                   <%#DataBinder.Eval(Container.DataItem, "home_city")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_prov")%></td>
               
            </tr>
            <tr>
                 <td >
                   <%#DataBinder.Eval(Container.DataItem, "home_pc")%> 
                
                   </td>
               
            </tr>
            
        
                
           
        </table>
        </ItemTemplate>
        </asp:Repeater>
            <br />
      
        <span style="text-decoration: underline">Tenant<br />
        
        <asp:Repeater runat=server ID="rtenant_view">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
        
        <tr>
            
                 <td>
                  <%#DataBinder.Eval(Container.DataItem, "name")%> 
                   </td>
               
            </tr>
        
         
      </table>
        </ItemTemplate>
        </asp:Repeater>
            <br />
        </span>
        
        <span style="text-decoration: underline">
        Paid rent</span><span><br />
            <br />
            <span style="text-decoration: underline">
            Late rent<br />
            <br />
            </span>
        </span>
        <table border="0" cellpadding="1" cellspacing="3" bgcolor="#ffffcc">
            <tr>
                <td >
                    due date</td>
                <td style="width: 116px">
                    :
                    <asp:Label ID="lbl_due_date" runat="server"></asp:Label></td>
                <td style="width: 4px">
                </td>
            </tr>
            <tr>
                <td>
                    due amount</td>
                <td style="width: 116px">
                    :
                    <asp:Label ID="lbl_amount_due" runat="server"></asp:Label></td>
                <td style="width: 4px">
                </td>
            </tr>
            <tr>
                <td>
                    late fee</td>
                <td style="width: 116px">
                    :
                    <asp:TextBox ID="TextBox3" runat="server" Width="88px"></asp:TextBox></td>
                <td style="width: 4px">
                    <asp:Button ID="Button1" runat="server" Text="submit" /></td>
            </tr>
            
        </table>
       
            <hr />
       Rent<br />
            <table border="0" cellpadding="1" cellspacing="3" bgcolor="#ffffcc">
            <tr>
                    <td >
                        rent amount</td>
                    <td>
                        :
                    <asp:Label ID="lbl_rent_amount" runat="server" ></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        due date</td>
                    <td >
                        :
                        <asp:DropDownList ID="DropDownList5" runat="server">
                            <asp:ListItem Value="0">Month</asp:ListItem>
                            <asp:ListItem Value="1">January</asp:ListItem>
                            <asp:ListItem Value="2">February</asp:ListItem>
                            <asp:ListItem Value="3">March</asp:ListItem>
                            <asp:ListItem Value="4">April</asp:ListItem>
                            <asp:ListItem Value="4">May</asp:ListItem>
                            <asp:ListItem Value="6">June</asp:ListItem>
                            <asp:ListItem Value="7">July</asp:ListItem>
                            <asp:ListItem Value="8">August</asp:ListItem>
                            <asp:ListItem Value="9">September</asp:ListItem>
                            <asp:ListItem Value="10">October</asp:ListItem>
                            <asp:ListItem Value="11">November</asp:ListItem>
                            <asp:ListItem Value="12">December</asp:ListItem>
                        </asp:DropDownList>&nbsp; /
                        <asp:TextBox ID="TextBox2" runat="server" Width="65px"></asp:TextBox>
                        /&nbsp;
                        <asp:DropDownList ID="DropDownList6" runat="server">
                            <asp:ListItem Value="0">Year</asp:ListItem>
                            <asp:ListItem>2007</asp:ListItem>
                            <asp:ListItem>2008</asp:ListItem>
                            <asp:ListItem>2009</asp:ListItem>
                            <asp:ListItem>2010</asp:ListItem>
                            <asp:ListItem>2011</asp:ListItem>
                            <asp:ListItem>2012</asp:ListItem>
                            <asp:ListItem>2013</asp:ListItem>
                            <asp:ListItem>2014</asp:ListItem>
                            <asp:ListItem>2015</asp:ListItem>
                           
                        </asp:DropDownList></td>
                </tr>
                <tr>
                <td style="height: 26px">
                    date received ( mm / dd / yyyy&nbsp; )</td>
                <td style="height: 26px">
                    :&nbsp;<asp:DropDownList ID="DropDownList1" runat="server">
                        <asp:ListItem Value="0">Month</asp:ListItem>
                        <asp:ListItem Value="1">January</asp:ListItem>
                        <asp:ListItem Value="2">February</asp:ListItem>
                        <asp:ListItem Value="3">March</asp:ListItem>
                        <asp:ListItem Value="4">April</asp:ListItem>
                        <asp:ListItem Value="4">May</asp:ListItem>
                        <asp:ListItem Value="6">June</asp:ListItem>
                        <asp:ListItem Value="7">July</asp:ListItem>
                        <asp:ListItem Value="8">August</asp:ListItem>
                        <asp:ListItem Value="9">September</asp:ListItem>
                        <asp:ListItem Value="10">October</asp:ListItem>
                        <asp:ListItem Value="11">November</asp:ListItem>
                        <asp:ListItem Value="12">December</asp:ListItem>
                    </asp:DropDownList>
                    / &nbsp;
                    <asp:TextBox ID="TextBox5" runat="server" Width="65px"></asp:TextBox>
                    /
                    <asp:DropDownList ID="DropDownList2" runat="server">
                        <asp:ListItem Value="0">Year</asp:ListItem>
                        <asp:ListItem>2007</asp:ListItem>
                        <asp:ListItem>2008</asp:ListItem>
                        <asp:ListItem>2009</asp:ListItem>
                        <asp:ListItem>2010</asp:ListItem>
                        <asp:ListItem>2011</asp:ListItem>
                        <asp:ListItem>2012</asp:ListItem>
                        <asp:ListItem>2013</asp:ListItem>
                        <asp:ListItem>2014</asp:ListItem>
                        <asp:ListItem>2015</asp:ListItem>
                   
                    </asp:DropDownList></td>
            </tr>
            </table>
        <br />
        <table border="0" cellpadding="1" cellspacing="3" bgcolor="#ffffcc">
            <tr>
                <td style="width: 199px" >
                    fully paid</td>
                <td style="width: 190px" >
                    :</td>
                <td >
                    <asp:Button ID="Button2" runat="server" Text="submit" /></td>
            </tr>
            <tr>
                <td style="width: 199px">
                    partially paid</td>
                <td style="width: 190px">
                    :<asp:TextBox ID="TextBox1" runat="server" Width="95px"></asp:TextBox></td>
                <td >
                    <asp:Button ID="submit" runat="server" Text="submit" /></td>
            </tr>
        </table>
        
    
    </div>
    </form>
</body>
</html>
