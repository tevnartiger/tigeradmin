﻿using System;
using System.Text;
using System.IO;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.tiger.security.TenantObjectAuthorization;

/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : aug 6 , 2008
/// </summary>
public partial class tenant_notice_notice_delequency_view : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        TenantObjectAuthorization rentpaidAuthorization = new TenantObjectAuthorization(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

        // CHECK IF HOME AND UNIT IS AUTHORIZED
        ///////// SECURITY OBJECT CHECK  BEGIN ////////////////////////////////
      /*
        
        if (!rentpaidAuthorization.RentPaid(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]), Convert.ToInt32(Request.QueryString["rp_id"]), Convert.ToInt32(Request.QueryString["tu_id"])))
        {
          //  Session.Abandon();
          //  Response.Redirect("http://www.sinfocatiger.com/login.aspx");
        }
        ///////// SECURITY OBJECT CHECK  END ////////////////////////////////

    */
        if (!Page.IsPostBack)
        {
            /*
            tiger.Rent hp = new tiger.Rent(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_rent_name_delequency.DataSource = hp.getRentNameDelequencyList(Convert.ToInt32(Session["schema_id"]),Convert.ToInt32(Session["name_id"]));
            gv_rent_name_delequency.DataBind();
             * 
             * */


            double amount_owed = 0;


            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prRentNameDelequencyView", conn);
            SqlCommand cmd2 = new SqlCommand("prNameView", conn);
            SqlCommand cmd3 = new SqlCommand("prRentDelequency", conn);
            SqlCommand cmd4 = new SqlCommand("prLateFee", conn);

            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();

            try
            {

                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd.Parameters.Add("@rp_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["rp_id"]);


                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);
               

                while (dr.Read() == true)
                {

                    DateTime notice_date = new DateTime();
                    notice_date = Convert.ToDateTime(dr["lrn_date"]);

                    lbl_notice_date.Text = notice_date.Month.ToString() + "-" + notice_date.Day.ToString() + "-" + notice_date.Year.ToString();

                    notice.InnerHtml = Server.HtmlDecode(dr["lrn_text"].ToString());

                   // lbl_notice.Text = Server.            //parsetext(dr["lrn_text"].ToString(),true);
                }
            }
            finally
            {
                //  conn.Close();
            }



            cmd2.CommandType = CommandType.StoredProcedure;

            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd2.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);

            SqlDataReader dr2 = null;
            dr2 = cmd2.ExecuteReader(CommandBehavior.SingleRow);


            while (dr2.Read() == true)
            {
                lbl_name.Text = dr2["name"].ToString();

            }


            cmd3.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd3.Parameters.Add("@rp_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["rp_id"]);


                SqlDataReader dr3 = null;
                dr3 = cmd3.ExecuteReader(CommandBehavior.SingleRow);
                
               
                while (dr3.Read() == true)
                {
                   

                    amount_owed = Convert.ToDouble(dr3["amount_owed"]);

                    // the amount owed
                    lbl_amount_owed.Text = String.Format("{0:0.00}", amount_owed);

                    // the number of days late ( rent payment)
                    lbl_days_late.Text = dr3["days"].ToString();
                   
                }
            }
            finally
            {
                //  conn.Close();
            }


            cmd4.CommandType = CommandType.StoredProcedure;

            try
            {

                cmd4.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
                cmd4.Parameters.Add("@tu_id", SqlDbType.Int).Value = Convert.ToInt32(Request.QueryString["tu_id"]);


                SqlDataReader dr4 = null;
                dr4 = cmd4.ExecuteReader(CommandBehavior.SingleRow);

                while (dr4.Read() == true)
                {

                    double late_fee = Convert.ToDouble(dr4["tt_late_fee"]);
                    double total = 0;

                    // the late fee written in the second notice letter
                    lbl_late_fee.Text = String.Format("{0:0.00}", late_fee);

                    // the late fee written in the third notice letter
                    lbl_late_fee.Text = String.Format("{0:0.00}", late_fee);

                    total = late_fee + amount_owed;

                    lbl_total.Text = String.Format("{0:0.00}", total);

                }
            }
            finally
            {
                conn.Close();
            }


        } 

    }



    public string parsetext(string text, bool allow)
    {
        //Create a StringBuilder object from the string intput
        //parameter
        StringBuilder sb = new StringBuilder(text);
        //Replace all double white spaces with a single white space
        //and &nbsp;
        sb.Replace(" ", " &nbsp;");
        //Check if HTML tags are not allowed
        if (!allow)
        {
            //Convert the brackets into HTML equivalents
            sb.Replace("<", "&lt;");
            sb.Replace(">", "&gt;");
            //Convert the double quote
            sb.Replace("\"", "&quot;");
        }
        //Create a StringReader from the processed string of
        //the StringBuilder
        StringReader sr = new StringReader(sb.ToString());
        StringWriter sw = new StringWriter();
        //Loop while next character exists
        while (sr.Peek() > -1)
        {
            //Read a line from the string and store it to a temp
            //variable
            string temp = sr.ReadLine();
            //write the string with the HTML break tag
            //Note here write method writes to a Internal StringBuilder
            //object created automatically
            sw.Write(temp + "<br>");
        }
        //Return the final processed text
        return sw.GetStringBuilder().ToString();
    }


}
