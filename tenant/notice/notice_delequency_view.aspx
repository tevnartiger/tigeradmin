﻿<%@ Page Language="C#" MasterPageFile="~/tenant/tenant.master" AutoEventWireup="true" CodeFile="notice_delequency_view.aspx.cs" Inherits="tenant_notice_notice_delequency_view" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
   
    <asp:Label ID="Label1" runat="server" Text="<%$ Resources:Resource, lbl_notice_rent %>"
     Font-Underline="true" ForeColor="Red" Font-Bold="false" Font-Size="Large"
      ></asp:Label>
    <br />

<table id="tb_notice_1" visible="true" runat="server" style="width: 100%">
    
    <tr>
            <td style="height: 112px">
                <table style="width: 90%">
                    <tr>
                        <td align="right">
                <asp:Label ID="Label3" runat="server" Text="<%$ Resources:Resource, lbl_date_of_notice %>" 
                                style="font-weight: 700"></asp:Label>
                        </td>
                        <td>
                <asp:Label ID="lbl_notice_date" runat="server" ></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right">
                            <asp:Label ID="Label10" runat="server" 
                                Text="<%$ Resources:Resource, lbl_important_notice %>" style="font-weight: 700"></asp:Label>
                            &nbsp;-<br />
                        </td>
                        <td valign="top">
                            <asp:Label ID="Label11" runat="server" 
                                Text="<%$ Resources:Resource, lbl_unpaid_amount %>" style="font-weight: 700"></asp:Label>  &nbsp;:&nbsp;<asp:Label ID="lbl_amount_owed" runat="server" Text="Label"></asp:Label> &nbsp;,&nbsp;<asp:Label 
                                ID="Label12" runat="server" Text="<%$ Resources:Resource, lbl_late_fee %>" 
                                style="font-weight: 700"></asp:Label>&nbsp;:&nbsp;<asp:Label
                                ID="lbl_late_fee" runat="server" ></asp:Label>&nbsp;,&nbsp;<asp:Label ID="Label8"
                                    runat="server" Text="<%$ Resources:Resource, lbl_total %>" 
                                style="font-weight: 700"></asp:Label>&nbsp;:&nbsp;<asp:Label
                                        ID="lbl_total" runat="server" Text="Label"></asp:Label>&nbsp;&nbsp;
                            <asp:Label ID="Label24" runat="server" 
                                Text="<%$ Resources:Resource, lbl_days_late%>" style="font-weight: 700"></asp:Label>
                        &nbsp;<asp:Label ID="lbl_days_late" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="right">
                <asp:Label ID="Label4" runat="server" Text="<%$ Resources:Resource, lbl_dear %>" 
                                style="font-weight: 700"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lbl_name" runat="server" ></asp:Label>
                        </td>
                    </tr>
                </table>
                
                
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
            <td><div id="notice" runat="server"></div>
                
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        </tr>
    </table>
</asp:Content>

