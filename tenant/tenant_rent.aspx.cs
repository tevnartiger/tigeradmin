using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class tenant_tenant_rent : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            // home address
            tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]),Convert.ToInt32(Request.QueryString["h_id"]));
            rhome_view.DataBind();

            lbl_rent_amount.Text = Request.QueryString["ra_id"];

            // tenant view
            tiger.Tenant tv = new tiger.Tenant(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            rtenant_view.DataSource = tv.getTenantNameList(Convert.ToInt32(Session["schema_id"]),Convert.ToInt32(Request.QueryString["t_id"]));
            rtenant_view.DataBind();


         }

     
        

    }
}
