<%@ Page Language="C#" MasterPageFile="../mp_manager.master" MaintainScrollPositionOnPostback="true"  AutoEventWireup="true" CodeFile="lease_add_acc_clause.aspx.cs" Inherits="home_lease_add_acc_clause"  %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    
<!--Hidden fields BEGIN SECTION 1-->
 <b>CREATE LEASE - STEP 4</b>    
<br>
<h3>Lease terms and conditions</h3>
    <asp:HyperLink ID="main" runat="server" NavigateUrl="~/manager/home/home_main.aspx">main</asp:HyperLink><br />
<br />
<div>
<table>
<tr>
<td valign="top">
 Form of payment </td>
<td>
<asp:DropDownList ID="ddl_tt_form_of_payment" runat="server">
<asp:ListItem Value="0">Not specified</asp:ListItem>
<asp:ListItem Value="1">Personal check</asp:ListItem>
<asp:ListItem Value="1">Cashier's check </asp:ListItem>
<asp:ListItem Value="3">Cash</asp:ListItem>
<asp:ListItem Value="4">Money order</asp:ListItem>
<asp:ListItem Value="5">Credit card</asp:ListItem>
 <asp:ListItem Value="6">Other</asp:ListItem>
</asp:DropDownList></td>
</tr>
<tr>
<td valign="top">
Non-sufficient fund fee  &nbsp;</td>
<td>
<asp:TextBox ID="tt_nsf" runat="server" Width="128px"></asp:TextBox></td>
</tr>
<tr>
<td  valign=top>Security deposit required</td>
<td>
<asp:RadioButtonList  AutoPostBack=true CssClass="letter" ID="tt_security_deposit" runat="server" OnSelectedIndexChanged="tt_security_deposit_SelectedIndexChanged">
<asp:ListItem Selected="True" Value="0">no</asp:ListItem>
<asp:ListItem Value="1">yes</asp:ListItem>
</asp:RadioButtonList>
<asp:Panel Visible=false ID="Panel_security_deposit" runat="server" >
&nbsp;&nbsp;&nbsp;amount <asp:TextBox runat=server ID="security_deposit_amount"></asp:TextBox></asp:Panel>
</td>
 </tr>
<tr><td valign=top>Guarantor required </td><td>
<asp:RadioButtonList  AutoPostBack=true CssClass="letter" ID="tt_guarantor" runat="server" OnSelectedIndexChanged="tt_guarantor_SelectedIndexChanged">
<asp:ListItem Selected="True" Value="0">no</asp:ListItem>
<asp:ListItem Value="1">yes</asp:ListItem>
</asp:RadioButtonList>
 <asp:Panel ID="panel_guarantor" Visible=false runat="server" >
 from the system &nbsp;<asp:DropDownList ID="ddl_guarantor_name_id"  AutoPostBack=true CssClass="letter" DataValueField="name_id" DataTextField="name_name" runat="server" OnSelectedIndexChanged="ddl_guarantor_name_id_SelectedIndexChanged" />
<br ><b>Or</b><br > add a guarantor <asp:Label Visible=false ID="txt" runat=server Font-Bold=true> * Disabled *</asp:Label>
 <!--  BEGIN GUARANTOR INFORMATIONS    -->
<table border="0" cellpadding="1" cellspacing="3" bgcolor="#ffffcc">
<tr><td class="letter_bold">first name</td>
<td><asp:TextBox ID="guarantor_fname" CssClass="letter" runat="server"/></td>
</tr>
 <tr>
<td class="letter_bold">last name</td>
<td><asp:TextBox ID="guarantor_lname" CssClass="letter" runat="server"/></td>
</tr>
<tr><td class="letter_bold">address</td>
<td><asp:TextBox ID="guarantor_addr" CssClass="letter" runat="server"></asp:TextBox></td>
</tr>
 <tr>
<td class="letter_bold">City</td>
<td><asp:TextBox ID="guarantor_addr_city" CssClass="letter" runat="server"/></td>
</tr>
<tr><td class="letter_bold">Pc</td>
<td><asp:TextBox ID="guarantor_addr_pc" CssClass="letter" runat="server"/></td>
 </tr>
<tr>
<td class="letter_bold">State/Prov</td>
<td>
<asp:TextBox ID="guarantor_addr_state" CssClass="letter" runat="server"/></td>
</tr>
<tr><td  class="letter_bold">Country</td>
<td colspan="3"><asp:DropDownList ID="ddl_guarantor_country_id" CssClass="letter" runat="server" DataTextField="country_name"
DataValueField="country_id"/></td>
</tr>
<tr><td class="letter_bold">Telephone</td>
<td><asp:TextBox ID="guarantor_tel" CssClass="letter" runat="server"/></td>
</tr>      
<tr> 
<td class="letter_bold">Tel. Work</td>
<td><asp:TextBox ID="guarantor_tel_work" CssClass="letter" runat="server"/></td>
</tr>
<tr><td class="letter_bold">Tel. Work ext.</td>
<td><asp:TextBox ID="guarantor_tel_work_ext" CssClass="letter" runat="server"/>                                   </td>
</tr>
<tr>
<td class="letter_bold">Cell</td>
 <td><asp:TextBox ID="guarantor_cell" CssClass="letter" runat="server"/></td>
</tr>
<tr><td class="letter_bold">Fax</td>
<td><asp:TextBox ID="guarantor_fax" CssClass="letter" runat="server"/></td>
</tr>
<tr>
<td class="letter_bold">Email</td>
<td><asp:TextBox ID="guarantor_email" CssClass="letter" runat="server"/></td>
</tr>
<tr><td class="letter_bold">Comments</td>
<td colspan="3"><asp:TextBox ID="guarantor_com" CssClass="letter" runat="server" TextMode="MultiLine" Width="200px"/></td>
</tr>
</table>  
            <!-- END GUARANTOR INFORMATIONS      -->
</asp:Panel>  
</td>
</tr>
<tr><td valign=top>Pets allowed</td><td>
<asp:RadioButtonList CssClass="letter" ID="tt_pets" runat="server">
<asp:ListItem Selected="True" Value="0">no</asp:ListItem>
<asp:ListItem Value="1">yes</asp:ListItem>
</asp:RadioButtonList></td></tr>
<tr valign="top">
<td valign=top>Tenant is responsible for maintenance</td>
<td>
<asp:RadioButtonList   CssClass="letter" ID="tt_maintenance" runat="server"  >
<asp:ListItem Selected="True" Value="0">no</asp:ListItem>
<asp:ListItem Value="1">yes</asp:ListItem>
</asp:RadioButtonList>
<asp:Panel  Visible=true ID="panel_specify_maintenance" runat="server">
&nbsp;&nbsp;&nbsp;specify maintenance allowed<br />&nbsp;<asp:TextBox runat=server ID="tt_specify_maintenance"  Width="300px" TextMode="MultiLine" ></asp:TextBox></asp:Panel>
 </td>
</tr>
<tr>
<td valign=top>Can tenant make improvement</td>
<td>
<asp:RadioButtonList   CssClass="letter" ID="tt_improvement" runat="server"  >
<asp:ListItem Selected="True" Value="0">no</asp:ListItem>
<asp:ListItem Value="1">yes</asp:ListItem>
</asp:RadioButtonList>
<asp:Panel Visible=true ID="panel_specify_improvement" runat="server">
&nbsp;&nbsp;&nbsp;specify improvement allowed<br />&nbsp;<asp:TextBox runat=server ID="tt_specify_improvement"  Width="300px" TextMode="MultiLine" ></asp:TextBox></asp:Panel>         
</td>             
</tr>
<tr>
<td valign=top>Notice to enter
</td>
<td>
<asp:RadioButtonList   CssClass="letter" ID="tt_notice_to_enter" runat="server">
<asp:ListItem Selected="True" Value="0">legal minimum</asp:ListItem>
<asp:ListItem Value="1">number of hours</asp:ListItem>
</asp:RadioButtonList>
<asp:Panel Visible=true ID="panel_specify_notice_to_enter" runat="server">
&nbsp;specify number of hours notice&nbsp;<asp:TextBox runat=server Width="100px" ID="tt_specify_number_of_hours"  ></asp:TextBox></asp:Panel>
</td>
</tr>
<tr>
<td valign=top>Who pay these insurances</td>
<td>
<table> 
<tr><td>tenant content</td> <td><asp:DropDownList CssClass="letter" ID="ddl_tt_tenant_content_ins" runat="server">
<asp:ListItem Selected="True" Value="0">not specified</asp:ListItem>
<asp:ListItem  Value="1">landlord</asp:ListItem>
<asp:ListItem  Value="2">tenant</asp:ListItem>
</asp:DropDownList></td></tr>
<tr><td>landlord content</td> <td><asp:DropDownList CssClass="letter" ID="ddl_tt_landlord_content_ins" runat="server">
<asp:ListItem Selected="True" Value="0">not specified</asp:ListItem>
<asp:ListItem  Value="1">landlord</asp:ListItem>
<asp:ListItem  Value="2">tenant</asp:ListItem>
</asp:DropDownList></td></tr>
<tr><td>personal injury on property</td> <td><asp:DropDownList CssClass="letter" ID="ddl_tt_injury_ins" runat="server">
<asp:ListItem Selected="True" Value="0">not specified</asp:ListItem>
<asp:ListItem  Value="1">landlord</asp:ListItem>
<asp:ListItem  Value="2">tenant</asp:ListItem>
</asp:DropDownList></td></tr>
<tr><td>lease premises</td> <td><asp:DropDownList ID="ddl_tt_premises_ins" CssClass="letter" runat="server">
<asp:ListItem Selected="True" Value="0">not specified</asp:ListItem>
<asp:ListItem  Value="1">landlord</asp:ListItem>
<asp:ListItem  Value="2">tenant</asp:ListItem>
</asp:DropDownList></td></tr>
</table>    
 </td>
</tr>
<tr><td valign=top>Additional Rules and Regulations</td>
<td><asp:TextBox runat=server Width="400px"  Height="200px" ID="tt_additional_terms" TextMode=MultiLine></asp:TextBox></td>
</tr>
</table>
</div>
<br>
<asp:Button ID="btn_submit" runat="server" OnClick="btn_submit_Click" Text="Submit" /><br />
<br>
<input type="hidden" id="hd_home_id" runat="server" />
<input type="hidden" id="hd_unit_id" runat="server" />
<input type="hidden" id="hd_current_tu_id" runat="server" />
<input type="hidden" id="hd_tu_date_begin" runat="server" />
<input type="hidden" id="hd_tu_date_end" runat="server" />
<input type="hidden" runat=server id=hd_name_id_1 />  
<input type="hidden" runat=server id=hd_name_id_2 /> 
<input type="hidden" runat=server id=hd_name_id_3 /> 
<input type="hidden" runat=server id=hd_name_lname_1 />
<input type="hidden" runat=server id=hd_name_fname_1 />
<input type="hidden" runat=server id=hd_name_addr_1/>
<input type="hidden" runat=server id=hd_name_addr_city_1 />
<input type="hidden" runat=server id=hd_name_addr_pc_1 />
<input type="hidden" runat=server id=hd_name_addr_state_1 />
<input type="hidden" runat=server id=hd_country_id_1  />
<input type="hidden" runat=server id=hd_name_tel_1 />
<input type="hidden" runat=server id=hd_name_tel_work_1 />
<input type="hidden" runat=server id=hd_name_tel_work_ext_1 />
<input type="hidden" runat=server id=hd_name_cell_1 />
<input type="hidden" runat=server id=hd_name_fax_1 />
<input type="hidden" runat=server id=hd_name_email_1 />
<input type="hidden" runat=server id=hd_name_com_1 />
<input type="hidden" runat=server id=hd_name_dob_1 />
<input type="hidden" runat=server id=hd_name_lname_2 />
<input type="hidden" runat=server id=hd_name_fname_2 />
<input type="hidden" runat=server id=hd_name_addr_2 />
<input type="hidden" runat=server id=hd_name_addr_city_2/>
<input type="hidden" runat=server id=hd_name_addr_pc_2 />
<input type="hidden" runat=server id=hd_name_addr_state_2 />
<input type="hidden" runat=server id=hd_country_id_2 />
<input type="hidden" runat=server id=hd_name_tel_2 />
<input type="hidden" runat=server id=hd_name_tel_work_2 />
<input type="hidden" runat=server id=hd_name_tel_work_ext_2 />
<input type="hidden" runat=server id=hd_name_cell_2 />
<input type="hidden" runat=server id=hd_name_fax_2 />
<input type="hidden" runat=server id=hd_name_email_2 />
<input type="hidden" runat=server id=hd_name_com_2 />
<input type="hidden" runat=server id=hd_name_dob_2 />
<input type="hidden" runat=server id=hd_name_lname_3 />
<input type="hidden" runat=server id=hd_name_fname_3 />
<input type="hidden" runat=server id=hd_name_addr_3 />
<input type="hidden" runat=server id=hd_name_addr_city_3 />
<input type="hidden" runat=server id=hd_name_addr_pc_3 />
<input type="hidden" runat=server id=hd_name_addr_state_3 />
<input type="hidden" runat=server id=hd_country_id_3 />
<input type="hidden" runat=server id=hd_name_tel_3 />
<input type="hidden" runat=server id=hd_name_tel_work_3 />
<input type="hidden" runat=server id=hd_name_tel_work_ext_3 />
<input type="hidden" runat=server id=hd_name_cell_3 />
<input type="hidden" runat=server id=hd_name_fax_3 />
<input type="hidden" runat=server id=hd_name_email_3 />
<input type="hidden" runat=server id=hd_name_com_3 />
<input type="hidden" runat=server id=hd_name_dob_3  />
<!--Hidden fields END SECTION 1-->
<!--Hidden fields BEGIN SECTION 3-->
<input type="hidden" runat="server" id="hd_rl_rent_amount" />
<input type="hidden" runat="server" id="hd_rl_rent_paid_every" />
<input type="hidden" runat="server" id="hd_ta_electricity_inc" />
<input type="hidden" ID="hd_ta_heat_inc"  runat="server" />
<input type="hidden" ID="hd_ta_water_inc"  runat="server" />
<input type="hidden" ID="hd_ta_water_tax_inc"  runat="server" />
<input type="hidden" ID="hd_ta_parking_inc"  runat="server" />
<input type="hidden" ID="hd_ta_garage_inc"  runat="server" />
<input type="hidden" ID="hd_ta_furnished_inc"  runat="server" />
<input type="hidden" ID="hd_ta_semi_furnished_inc"  runat="server" />
<input type=hidden ID="hd_ta_com" runat=server />
<input type=hidden ID="hd_ta_none" runat=server />
<!--Hidden fields END SECTION 3-->
 </asp:Content>
