﻿<%@ Page Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="lease_list.aspx.cs" Inherits="manager_home_lease_list" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <span  style="font-size: medium"><b>Lease list</b>
 
        <br /></span><div id="txt_message" runat="server"></div>
    <table>
        <tr>
            <td>
      
   <asp:Label ID="lbl_property" runat="server" Text="<%$ Resources:Resource, lbl_property %>"/> 
            </td>
            <td>
                :
                <asp:DropDownList AutoPostBack="true" DataTextField="home_name" DataValueField="home_id" ID="ddl_home_list" runat="server" OnSelectedIndexChanged="ddl_home_list_SelectedIndexChanged">
        </asp:DropDownList>
            &nbsp;</td>
        </tr>
        <tr>
        
                  <td>
      <asp:Label ID="lbl_unit" runat="server" Text="<%$ Resources:Resource, lbl_unit %>"/>
                </td>
                <td valign="top">
                    :
        <asp:DropDownList ID="ddl_unit_id" DataValueField="unit_id" DataTextField="unit_door_no"   runat="server" OnSelectedIndexChanged="ddl_unit_id_SelectedIndexChanged" AutoPostBack="true" />
    
    
    
    
                </td>
        </tr>
        
        </table>
        <br />
        <asp:Label ID="lbl_address" runat="server" Text="<%$ Resources:Resource, lbl_address %>"/>
    :<br />
        <asp:Repeater runat="server" ID="rhome_view">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
        
        <tr>
          <td>
                 District&nbsp;:&nbsp; <%#DataBinder.Eval(Container.DataItem, "home_district")%> 
                   </td>
            </tr>
           <tr>
              <td >
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_no")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_addr_street")%></td>
            </tr>
            <tr>
                 <td >
                   <%#DataBinder.Eval(Container.DataItem, "home_city")%> &nbsp;,&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_prov")%></td>
               
            </tr>
            <tr>
                 <td >
                   <%#DataBinder.Eval(Container.DataItem, "home_pc")%> 
                
                   </td>
               
            </tr>   
        </table>
        </ItemTemplate>
        </asp:Repeater> 
    <br />
         
      
    
    <br />
     <asp:GridView HeaderStyle-BackColor="AliceBlue" Width="100%" BorderColor="White" BorderWidth="3"  ID="gv_rented_paid_unit_list" runat="server" AutoGenerateColumns="false"
       AllowSorting="true"  OnPageIndexChanging="gv_rented_paid_unit_list_PageIndexChanging"
         EmptyDataText="no unit rented" GridLines="Both"  PageSize="10" AllowPaging="true"
        AlternatingRowStyle-BackColor="Beige" OnSorting="gv_rented_paid_unit_list_Sorting">
    <Columns>
   <asp:BoundField DataField="unit_door_no" SortExpression="Unit"   />
   <asp:BoundField   DataField="tu_date_begin" DataFormatString="{0:M-dd-yyyy}"  
     HeaderText="Date Begin" HtmlEncode="false" SortExpression="Date Begin" />
   <asp:BoundField   DataField="tu_date_end" DataFormatString="{0:M-dd-yyyy}"  
     HeaderText="Date End" HtmlEncode="false" SortExpression="Date End" />
      <asp:BoundField DataField="tu_id" HeaderText="tu_id"   />
      
      
      
      
       <asp:HyperLinkField   Text="View"
     DataNavigateUrlFields="tu_id,unit_id,home_id" 
     DataNavigateUrlFormatString="~/manager/lease/lease_view.aspx?tu_id={0}&unit_id={1}&h_id={2}" 
      HeaderText="View" />
       
   </Columns>
   </asp:GridView>
        <br />
   
    <br />
    <br />
    <br />
    <br />
    <br />
   
 
    <asp:HiddenField ID="h_btn_submit" Value="0" runat="server" />
</asp:Content>

