﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// Created by : Stanley Jocelyn
/// date       : May 18, 2008
/// </summary>

public partial class manager_lease_lease_archive_list : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((!Page.IsPostBack))
        {
            String referer = "";
            referer = Request.UrlReferrer.ToString();

            //  Label2.Text = referer;
            // if value = 0 form was not submit
            h_btn_submit.Value = "0";


            DateTime test_date = new DateTime();
            test_date = DateTime.Now;


            String the_date = "";
            DateTime right_now = new DateTime();
            right_now = DateTime.Now;

            tiger.Date d = new tiger.Date();

            // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
            // and convert it in Datetime
            right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


            tiger.Home l = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getHomeCount(Convert.ToInt32(Session["schema_id"]));




            // first we check if there's any property available
            if (home_count > 0)
            {
                int home_id = l.getHomeFirstId(Convert.ToInt32(Session["schema_id"]));


                tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ddl_home_list.DataSource = h.getHomeList(Convert.ToInt32(Session["schema_id"]));
                ddl_home_list.DataBind();


                // here we get the list of paid rented unit (gridview of the paid rent for the current month )

                tiger.Lease paid = new tiger.Lease(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                gv_archive_lease_list.DataSource = paid.getLeaseArchiveList(Convert.ToInt32(Session["schema_id"]), home_id,0, right_now);
                gv_archive_lease_list.Columns[0].HeaderText = Resources.Resource.gv_unit;
                gv_archive_lease_list.DataBind();


                //To view the address of the property

                tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
                rhome_view.DataBind();

                // DropDownList pour les Unit
                //int unit_id = Convert.ToInt32(Request.QueryString["unit_id"]);
                tiger.Unit u = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                int unit_count = u.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id); 
                if (unit_count > 0)
                {

                    ddl_unit_id.DataSource = u.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
                    ddl_unit_id.DataBind();
                    ddl_unit_id.Items.Insert(0, "-- All Units --");
                    ddl_unit_id.SelectedIndex = 0;

                }    


              

            }

            else
            {
                // panel_home_unit_add.Visible = false;
                txt_message.InnerHtml = "<b><a href='home/home_add.aspx'>Please add a property</a></b>";
            }

        }
        // if the referer is self i.e : from the respon redirect of the submit button
        //********************************************************************
        /*  if (Request.UrlReferrer.ToString() == Request.Url.ToString())
          {
              Session.Contents.Remove("home_id");
              Session.Contents.Remove("rent_frequency");
              Session.Contents.Remove("date_received_m");
              Session.Contents.Remove("date_received_d");
              Session.Contents.Remove("date_received_y");
          }
       */
        //********************************************************************



    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void ddl_home_list_SelectedIndexChanged(object sender, EventArgs e)
    {
        gv_archive_lease_list.Visible = true;

        // if value = 0 form was not submit
        h_btn_submit.Value = "0";

        DateTime right_now = new DateTime();
        right_now = DateTime.Now;

        tiger.Date d = new tiger.Date();

        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        // right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));
        // get the date in the good format according to Session["_lastCulture"] , with method DateCulture( string month,string day,string year,string _lastCulture)
        // and convert it in Datetime
        // right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));

        // tiger.Date d = new tiger.Date();

        //  the_date = 
        right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));


        int home_id = Convert.ToInt32(ddl_home_list.SelectedValue);



        // Here we are getting the address of the selected home/property
        tiger.Home hv = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        rhome_view.DataSource = hv.getHomeView(Convert.ToInt32(Session["schema_id"]), home_id);
        rhome_view.DataBind();



        // here we check if there's any unit in this property
        tiger.Unit uc = new tiger.Unit(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        int unit_count = uc.getUnitCount(Convert.ToInt32(Session["schema_id"]), home_id);

        if (unit_count > 0)
        // here we get the list of unit that were already added 
        {


            // here we get the list of paid rented unit (gridview of the paid rent for the current month )

            tiger.Lease paid = new tiger.Lease(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_archive_lease_list.DataSource = paid.getLeaseArchiveList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_list.SelectedValue),0, right_now);
            gv_archive_lease_list.DataBind();


            // repopulating the ddl for the property selected
            ddl_unit_id.Dispose();
            ddl_unit_id.DataSource = uc.getUnitList(Convert.ToInt32(Session["schema_id"]), home_id);
            // ddl_unit_id.SelectedValue = Convert.ToString(unit_id);
            ddl_unit_id.DataBind();
            ddl_unit_id.Items.Insert(0, "-- All Units --");
            ddl_unit_id.SelectedIndex = 0;
            

        }
        else
        {

        }

        if (gv_archive_lease_list.Rows.Count == 0)
            gv_archive_lease_list.Visible = false;
    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btn_submit_Click(object sender, EventArgs e)
    {



    }
   

    protected void gv_archive_lease_list_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        DateTime right_now = new DateTime();
        right_now = DateTime.Now;
        tiger.Date d = new tiger.Date();

        right_now = Convert.ToDateTime(d.DateCulture(right_now.Month.ToString(), right_now.Day.ToString(), right_now.Year.ToString(), Convert.ToString(Session["_lastCulture"])));

        if (ddl_unit_id.SelectedIndex == 0)
        {

            // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
            tiger.Lease paid = new tiger.Lease(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_archive_lease_list.PageIndex = e.NewPageIndex;
            gv_archive_lease_list.DataSource = paid.getLeaseArchiveList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_list.SelectedValue), 0, right_now);
            gv_archive_lease_list.DataBind();
        }

        else
        {

            // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
            tiger.Lease paid = new tiger.Lease(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_archive_lease_list.PageIndex = e.NewPageIndex;
            gv_archive_lease_list.DataSource = paid.getLeaseArchiveList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_list.SelectedValue), Convert.ToInt32(ddl_unit_id.SelectedValue), right_now);
            gv_archive_lease_list.DataBind();
        }



    }



    protected void ddl_unit_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        DateTime right_now = new DateTime();
        right_now = DateTime.Now;
        tiger.Date d = new tiger.Date();


        if (ddl_unit_id.SelectedIndex == 0)
        {

            // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
            tiger.Lease paid = new tiger.Lease(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_archive_lease_list.DataSource = paid.getLeaseArchiveList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_list.SelectedValue), 0, right_now);
            gv_archive_lease_list.DataBind();
        }

        else
        {
            // TO CHECK THIS IS MAY BE NOT THE MOST EFFICIENT WAY TO DO THIS
            tiger.Lease paid = new tiger.Lease(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            gv_archive_lease_list.DataSource = paid.getLeaseArchiveList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(ddl_home_list.SelectedValue), Convert.ToInt32(ddl_unit_id.SelectedValue), right_now);
            gv_archive_lease_list.DataBind();
        }
    }
}
