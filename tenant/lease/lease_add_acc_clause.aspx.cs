using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
/// <summary>
/// Done by              : Stanley Jocelyn
/// date                 : sept 12 , 2007
/// last modifidication  : sept 20 , 2007
/// </summary>

public partial class home_lease_add_acc_clause : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (tiger.security.Access.hasAccess("1100000000", Convert.ToInt32(Session["group_id"]), Convert.ToInt32(Session["schema_id"])))
        {
            if (!(Page.IsPostBack))
            {
                tiger.Name n = new tiger.Name(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ddl_guarantor_name_id.DataSource = n.getNameList(Convert.ToInt32(Session["schema_id"]), 0);
                ddl_guarantor_name_id.DataBind();
                ddl_guarantor_name_id.Items.Insert(0, "Select a name");



                /*Begin hidden files*/
                hd_home_id.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_home_id"]);
                hd_unit_id.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_unit_id"]);
                hd_current_tu_id.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_current_tu_id"]);
                hd_tu_date_begin.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_tu_date_begin"]);
                hd_tu_date_end.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_tu_date_end"]);
                hd_name_id_1.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_id_1"]);
                hd_name_id_2.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_id_2"]);
                hd_name_id_3.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_id_3"]);

                //name 1
                hd_name_lname_1.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_lname_1"]);
                hd_name_fname_1.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_fname_1"]);            /*End Hidden files*/
                hd_name_addr_1.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_addr_1"]);
                hd_name_addr_city_1.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_addr_city_1"]);
                hd_name_addr_pc_1.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_addr_pc_1"]);
                hd_name_addr_state_1.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_addr_state_1"]);
                hd_country_id_1.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_country_id_1"]);
                hd_name_tel_1.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_tel_1"]);
                hd_name_tel_work_1.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_tel_work_1"]);
                hd_name_tel_work_ext_1.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_tel_work_ext_1"]);
                hd_name_cell_1.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_cell_1"]);
                hd_name_fax_1.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_fax_1"]);
                hd_name_email_1.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_email_1"]);
                hd_name_com_1.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_com_1"]);
                hd_name_dob_1.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_dob_1"]);

                //name 2
                hd_name_lname_2.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_lname_2"]);
                hd_name_fname_2.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_fname_2"]);            /*End Hidden files*/
                hd_name_addr_2.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_addr_2"]);
                hd_name_addr_city_2.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_addr_city_2"]);
                hd_name_addr_pc_2.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_addr_pc_2"]);
                hd_name_addr_state_2.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_addr_state_2"]);
                hd_country_id_2.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_country_id_2"]);
                hd_name_tel_2.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_tel_2"]);
                hd_name_tel_work_2.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_tel_work_2"]);
                hd_name_tel_work_ext_2.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_tel_work_ext_2"]);
                hd_name_cell_2.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_cell_2"]);
                hd_name_fax_2.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_fax_2"]);
                hd_name_email_2.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_email_2"]);
                hd_name_com_2.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_com_2"]);
                hd_name_dob_2.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_dob_2"]);
                //name 3
                hd_name_lname_3.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_lname_3"]);
                hd_name_fname_3.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_fname_3"]);            /*End Hidden files*/
                hd_name_addr_3.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_addr_3"]);
                hd_name_addr_city_3.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_addr_city_3"]);
                hd_name_addr_pc_3.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_addr_pc_3"]);
                hd_name_addr_state_3.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_addr_state_3"]);
                hd_country_id_3.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_country_id_3"]);
                hd_name_tel_3.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_tel_3"]);
                hd_name_tel_work_3.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_tel_work_3"]);
                hd_name_tel_work_ext_3.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_tel_work_ext_3"]);
                hd_name_cell_3.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_cell_3"]);
                hd_name_fax_3.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_fax_3"]);
                hd_name_email_3.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_email_3"]);
                hd_name_com_3.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_com_3"]);
                hd_name_dob_3.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$hd_name_dob_3"]);

                hd_rl_rent_amount.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$rl_rent_amount"]);
                hd_rl_rent_paid_every.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$ddl_rl_rent_paid_every"]);
                hd_ta_electricity_inc.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$ta_electricity_inc"]);
                hd_ta_heat_inc.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$ta_heat_inc"]);



                hd_ta_water_inc.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$ta_water_inc"]);
                hd_ta_water_tax_inc.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$ta_water_tax_inc"]);
                hd_ta_parking_inc.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$ta_parking_inc"]);
                hd_ta_garage_inc.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$ta_garage_inc"]);
                hd_ta_furnished_inc.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$ta_furnished_inc"]);
                hd_ta_semi_furnished_inc.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$ta_semi_furnished_inc"]);
                hd_ta_com.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$ta_com"]);
                hd_ta_none.Value = Convert.ToString(Request.Form["ctl00$ContentPlaceHolder1$ta_none"]);
            }
        }
        else
        {
            Response.Redirect(tiger.security.Access.toLoginPage());
        }

    }
    protected void tt_security_deposit_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(tt_security_deposit.SelectedValue) == "1")
            Panel_security_deposit.Visible = true;
        else
            Panel_security_deposit.Visible = false ;

    }
    protected void tt_guarantor_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(tt_guarantor.SelectedValue) == "1")
            panel_guarantor.Visible = true;
        else
           panel_guarantor.Visible = false;

    }
    
   
    protected void ddl_guarantor_name_id_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(ddl_guarantor_name_id.SelectedIndex) > 0)
        {
            guarantor_fname.Enabled = false;
            guarantor_lname.Enabled = false;
            guarantor_addr.Enabled = false;
            guarantor_addr_city.Enabled = false;
            guarantor_addr_pc.Enabled = false;
            guarantor_addr_state.Enabled = false;
            ddl_guarantor_country_id.Enabled = false;
            guarantor_tel.Enabled = false;
            guarantor_tel_work.Enabled = false;
            guarantor_tel_work_ext.Enabled = false;
            guarantor_cell.Enabled = false;
            guarantor_fax.Enabled = false;
            guarantor_email.Enabled = false;
            guarantor_com.Enabled = false;
            txt.Visible = true;
        }
        else
        {
            guarantor_fname.Enabled = true;
            guarantor_lname.Enabled = true;
            guarantor_addr.Enabled = true;
            guarantor_addr_city.Enabled = true;
            guarantor_addr_pc.Enabled = true;
            guarantor_addr_state.Enabled = true;
            ddl_guarantor_country_id.Enabled = true;
            guarantor_tel.Enabled = true;
            guarantor_tel_work.Enabled = true;
            guarantor_tel_work_ext.Enabled = true;
            guarantor_cell.Enabled = true;
            guarantor_fax.Enabled = true;
            guarantor_email.Enabled = true;
            guarantor_com.Enabled = true;
            txt.Visible = false;

        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        if (tiger.security.Access.hasAccess("1100000000", Convert.ToInt32(Session["group_id"]), Convert.ToInt32(Session["schema_id"])))
        {
           
           string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

           SqlConnection conn = new SqlConnection(strconn);
           SqlCommand cmd = new SqlCommand("prCreateLease", conn);
           cmd.CommandType = CommandType.StoredProcedure;

           try
           {
               conn.Open();
         
               // new tenant parameter - tenant from the database
               cmd.Parameters.Add("@return_success", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
               cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
               cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = Convert.ToInt32(hd_home_id.Value);
               cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = Convert.ToInt32(hd_unit_id.Value);
               cmd.Parameters.Add("@current_tu_id", SqlDbType.Int).Value = Convert.ToInt32(hd_current_tu_id.Value);
             
               
               cmd.Parameters.Add("@tu_date_begin", SqlDbType.DateTime).Value = Convert.ToDateTime(hd_tu_date_begin.Value);


           

               if (Convert.ToString(hd_tu_date_end.Value) == "//")
                   cmd.Parameters.Add("@tu_date_end", SqlDbType.DateTime).Value = Convert.ToDateTime("1/1/1900");
               else
                   cmd.Parameters.Add("@tu_date_end", SqlDbType.DateTime).Value = Convert.ToDateTime(hd_tu_date_end.Value);


  if ((Convert.ToString(hd_name_id_1.Value) != "Select a name") && (Convert.ToString(hd_name_id_1.Value) != "0"))
 cmd.Parameters.Add("@name_id_1", SqlDbType.Int).Value = Convert.ToInt32(hd_name_id_1.Value);
 else
 cmd.Parameters.Add("@name_id_1", SqlDbType.Int).Value = 0 ;

if ((Convert.ToString(hd_name_id_2.Value) != "Select a name") && (Convert.ToString(hd_name_id_2.Value) != "0"))
 cmd.Parameters.Add("@name_id_2", SqlDbType.Int).Value = Convert.ToInt32(hd_name_id_2.Value);
 else
 cmd.Parameters.Add("@name_id_2", SqlDbType.Int).Value = 0 ;



if ((Convert.ToString(hd_name_id_3.Value) != "Select a name") && (Convert.ToString(hd_name_id_3.Value) != "0"))
 cmd.Parameters.Add("@name_id_3", SqlDbType.Int).Value = Convert.ToInt32(hd_name_id_3.Value);
 else
 cmd.Parameters.Add("@name_id_3", SqlDbType.Int).Value = 0 ;



if (Convert.ToString(hd_name_dob_1.Value) == "//")
    cmd.Parameters.Add("@name_dob_1", SqlDbType.DateTime).Value = System.DBNull.Value;
else
   cmd.Parameters.Add("@name_dob_1", SqlDbType.DateTime).Value = Convert.ToDateTime(hd_name_dob_1.Value);

if (Convert.ToString(hd_name_dob_2.Value) == "//")
cmd.Parameters.Add("@name_dob_2", SqlDbType.DateTime).Value =  System.DBNull.Value;
else
cmd.Parameters.Add("@name_dob_2", SqlDbType.DateTime).Value = Convert.ToDateTime(hd_name_dob_2.Value);

if (Convert.ToString(hd_name_dob_3.Value) == "//")
    cmd.Parameters.Add("@name_dob_3", SqlDbType.DateTime).Value = System.DBNull.Value;
else
  cmd.Parameters.Add("@name_dob_3", SqlDbType.DateTime).Value = Convert.ToDateTime(hd_name_dob_2.Value);




               // new tenants paramater - new person add to the database   
               //name 1
               cmd.Parameters.Add("@name_lname_1", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_lname_1.Value);
               cmd.Parameters.Add("@name_fname_1", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_fname_1.Value);


               cmd.Parameters.Add("@name_addr_1", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_addr_1.Value);
               cmd.Parameters.Add("@name_addr_city_1", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_addr_city_1.Value);
               cmd.Parameters.Add("@name_addr_pc_1", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_addr_pc_1.Value);
               cmd.Parameters.Add("@name_addr_state_1", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_addr_state_1.Value);

               if (hd_country_id_1.Value == null || hd_country_id_1.Value == "")
                cmd.Parameters.Add("@country_id_1", SqlDbType.Int).Value = System.DBNull.Value;
               else
                 cmd.Parameters.Add("@country_id_1", SqlDbType.Int).Value =  Convert.ToInt32(hd_country_id_1.Value);

               cmd.Parameters.Add("@name_tel_1", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_tel_1.Value);
               cmd.Parameters.Add("@name_tel_work_1", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_tel_work_1.Value);
               cmd.Parameters.Add("@name_tel_work_ext_1", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_tel_work_ext_1.Value);

               cmd.Parameters.Add("@name_cell_1", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_cell_1.Value);
               cmd.Parameters.Add("@name_fax_1", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_fax_1.Value);
               cmd.Parameters.Add("@name_email_1", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_email_1.Value);
               cmd.Parameters.Add("@name_com_1", SqlDbType.Text).Value = Convert.ToString(hd_name_com_1.Value);

               //name 2
               cmd.Parameters.Add("@name_lname_2", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_lname_2.Value);
               cmd.Parameters.Add("@name_fname_2", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_fname_2.Value);
               cmd.Parameters.Add("@name_addr_2", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_addr_2.Value);
               cmd.Parameters.Add("@name_addr_city_2", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_addr_city_2.Value);
               cmd.Parameters.Add("@name_addr_pc_2", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_addr_pc_2.Value);
               cmd.Parameters.Add("@name_addr_state_2", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_addr_state_2.Value);

               if (hd_country_id_2.Value == null || hd_country_id_2.Value == "")
                   cmd.Parameters.Add("@country_id_2", SqlDbType.Int).Value = System.DBNull.Value;
               else
                   cmd.Parameters.Add("@country_id_2", SqlDbType.Int).Value = Convert.ToInt32(hd_country_id_2.Value);

               
               
               
               cmd.Parameters.Add("@name_tel_2", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_tel_2.Value);
               cmd.Parameters.Add("@name_tel_work_2", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_tel_work_2.Value);
               cmd.Parameters.Add("@name_tel_work_ext_2", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_tel_work_ext_2.Value);
               cmd.Parameters.Add("@name_cell_2", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_cell_2.Value);
               cmd.Parameters.Add("@name_fax_2", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_fax_2.Value);
               cmd.Parameters.Add("@name_email_2", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_email_2.Value);
               cmd.Parameters.Add("@name_com_2", SqlDbType.Text).Value = Convert.ToString(hd_name_com_2.Value);
               //name 3
               cmd.Parameters.Add("@name_lname_3", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_lname_3.Value);
               cmd.Parameters.Add("@name_fname_3", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_fname_3.Value);
               cmd.Parameters.Add("@name_addr_3", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_addr_3.Value);
               cmd.Parameters.Add("@name_addr_city_3", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_addr_city_3.Value);
               cmd.Parameters.Add("@name_addr_pc_3", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_addr_pc_3.Value);
               cmd.Parameters.Add("@name_addr_state_3", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_addr_state_3.Value);


               if (hd_country_id_3.Value == null || hd_country_id_3.Value == "")
                   cmd.Parameters.Add("@country_id_3", SqlDbType.Int).Value = System.DBNull.Value;
               else
                   cmd.Parameters.Add("@country_id_3", SqlDbType.Int).Value = Convert.ToInt32(hd_country_id_3.Value);

               
               
               
               cmd.Parameters.Add("@name_tel_3", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_tel_3.Value);
               cmd.Parameters.Add("@name_tel_work_3", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_tel_work_3.Value);
               cmd.Parameters.Add("@name_tel_work_ext_3", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_tel_work_ext_3.Value);
               cmd.Parameters.Add("@name_cell_3", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_cell_3.Value);
               cmd.Parameters.Add("@name_fax_3", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_fax_3.Value);
               cmd.Parameters.Add("@name_email_3", SqlDbType.VarChar, 50).Value = Convert.ToString(hd_name_email_3.Value);
               cmd.Parameters.Add("@name_com_3", SqlDbType.Text).Value = Convert.ToString(hd_name_com_3.Value);

               if (ddl_guarantor_name_id.SelectedIndex == 0)
                   cmd.Parameters.Add("@guarantor_name_id", SqlDbType.Int).Value = 0;
               else
                   cmd.Parameters.Add("@guarantor_name_id", SqlDbType.Int).Value = Convert.ToInt32(ddl_guarantor_name_id.SelectedValue);
               
             
               
               
               // if guarantor is not in the system we create him
               cmd.Parameters.Add("@guarantor_name_fname", SqlDbType.VarChar, 50).Value = guarantor_fname.Text;
               cmd.Parameters.Add("@guarantor_name_lname", SqlDbType.VarChar, 50).Value = guarantor_lname.Text;
               cmd.Parameters.Add("@guarantor_name_addr", SqlDbType.VarChar, 50).Value = guarantor_addr.Text;
               cmd.Parameters.Add("@guarantor_name_addr_city", SqlDbType.VarChar, 50).Value = guarantor_addr_city.Text;
               cmd.Parameters.Add("@guarantor_name_addr_pc", SqlDbType.VarChar, 50).Value = guarantor_addr_pc.Text;
               cmd.Parameters.Add("@guarantor_name_addr_state", SqlDbType.VarChar, 50).Value = guarantor_addr_state.Text;
               cmd.Parameters.Add("@guarantor_country_id", SqlDbType.Int).Value = 2; // Convert.ToInt32(ddl_guarantor_country_id.Text);
               cmd.Parameters.Add("@guarantor_name_tel", SqlDbType.VarChar, 50).Value = guarantor_tel.Text;
               cmd.Parameters.Add("@guarantor_name_tel_work", SqlDbType.VarChar, 50).Value = guarantor_tel_work.Text;
               cmd.Parameters.Add("@guarantor_name_tel_work_ext", SqlDbType.VarChar, 50).Value = guarantor_tel_work_ext.Text;
               cmd.Parameters.Add("@guarantor_name_cell", SqlDbType.VarChar, 50).Value = guarantor_cell.Text;
               cmd.Parameters.Add("@guarantor_name_fax", SqlDbType.VarChar, 50).Value = guarantor_fax.Text;
               cmd.Parameters.Add("@guarantor_name_email", SqlDbType.VarChar, 50).Value = guarantor_email.Text;
               cmd.Parameters.Add("@guarantor_name_com", SqlDbType.Text).Value = guarantor_com.Text;
              
                     // Rent amount and accomodations

                        cmd.Parameters.Add("@rl_rent_amount",SqlDbType.Money).Value     = Convert.ToDecimal(hd_rl_rent_amount.Value)  ;
                        cmd.Parameters.Add("@rl_rent_paid_every",SqlDbType.Int).Value = Convert.ToInt32(hd_rl_rent_paid_every.Value);
             
                  
               
               if(Convert.ToString(hd_ta_none.Value) == "on")
                   {
                       cmd.Parameters.Add("@ta_electricity_inc", SqlDbType.Int).Value = 0;
                       cmd.Parameters.Add("@ta_heat_inc", SqlDbType.Int).Value = 0;
                       cmd.Parameters.Add("@ta_water_inc", SqlDbType.Int).Value = 0;
                       cmd.Parameters.Add("@ta_water_tax_inc", SqlDbType.Int).Value = 0;
                       cmd.Parameters.Add("@ta_none", SqlDbType.Int).Value = 1;
                   }
                   else
                  {
                    if(Convert.ToString(hd_ta_electricity_inc.Value) == "on")
                        cmd.Parameters.Add("@ta_electricity_inc", SqlDbType.Int).Value = 1;
                    else
                        cmd.Parameters.Add("@ta_electricity_inc", SqlDbType.Int).Value = 0;




                    if(Convert.ToString(hd_ta_heat_inc.Value) == "on")
                        cmd.Parameters.Add("@ta_heat_inc", SqlDbType.Int).Value = 1;
                    else
                        cmd.Parameters.Add("@ta_heat_inc", SqlDbType.Int).Value = 0;





                    if(Convert.ToString(hd_ta_water_inc.Value) == "on")
                        cmd.Parameters.Add("@ta_water_inc", SqlDbType.Int).Value = 1;
                    else
                        cmd.Parameters.Add("@ta_water_inc", SqlDbType.Int).Value = 0;


                    if(Convert.ToString(hd_ta_water_tax_inc.Value) == "on")
                        cmd.Parameters.Add("@ta_water_tax_inc", SqlDbType.Int).Value = 1;
                    else
                        cmd.Parameters.Add("@ta_water_tax_inc", SqlDbType.Int).Value = 0;

                    cmd.Parameters.Add("@ta_none", SqlDbType.Int).Value = 0;
                   }



                 if(Convert.ToString(hd_ta_parking_inc.Value) == "on")
                     cmd.Parameters.Add("@ta_parking_inc", SqlDbType.Int).Value = 1;
                    else
                     cmd.Parameters.Add("@ta_parking_inc", SqlDbType.Int).Value = 0;

      

      

                 if(Convert.ToString(hd_ta_garage_inc.Value) == "on")
                     cmd.Parameters.Add("@ta_garage_inc", SqlDbType.Int).Value = 1;
                    else
                     cmd.Parameters.Add("@ta_garage_inc", SqlDbType.Int).Value = 0;




                 if(Convert.ToString(hd_ta_furnished_inc.Value) == "on")
                     cmd.Parameters.Add("@ta_furnished_inc", SqlDbType.Int).Value = 1;
                    else
                     cmd.Parameters.Add("@ta_furnished_inc", SqlDbType.Int).Value = 0;



                if(Convert.ToString(hd_ta_semi_furnished_inc.Value) == "on")
                    cmd.Parameters.Add("@ta_semi_furnished_inc", SqlDbType.Int).Value = 1;
                    else
                    cmd.Parameters.Add("@ta_semi_furnished_inc", SqlDbType.Int).Value = 0;
              

                cmd.Parameters.Add("@ta_com", SqlDbType.Text).Value = hd_ta_com.Value;

                 
                          // Terms and Conditions parameters

                if (Convert.ToString(tt_security_deposit.SelectedValue) == "1")
                    cmd.Parameters.Add("@tt_security_deposit", SqlDbType.Int).Value = 1;
                else
                    cmd.Parameters.Add("@tt_security_deposit", SqlDbType.Int).Value = 0;        
                         
               
             if (Convert.ToString(tt_security_deposit.SelectedValue) == "1")
              cmd.Parameters.Add("@tt_security_deposit_amount",SqlDbType.SmallMoney).Value= Convert.ToDecimal(security_deposit_amount.Text);
          else
              cmd.Parameters.Add("@tt_security_deposit_amount", SqlDbType.SmallMoney).Value = 0 ;


         // if (tt_guarantor.SelectedIndex == 0)
          //    cmd.Parameters.Add("@tt_guarantor", SqlDbType.Int).Value = 1;
 if(Convert.ToString(tt_guarantor.SelectedValue) == "1")
cmd.Parameters.Add("@tt_guarantor", SqlDbType.Int).Value = 1;
 else
cmd.Parameters.Add("@tt_guarantor", SqlDbType.Int).Value = 0;


if(Convert.ToString(tt_pets.SelectedValue) == "1")
 cmd.Parameters.Add("@tt_pets", SqlDbType.Int).Value = 1;
else
 cmd.Parameters.Add("@tt_pets", SqlDbType.Int).Value = 0;


if(Convert.ToString(tt_maintenance.SelectedValue) == "1")
  cmd.Parameters.Add("@tt_maintenance", SqlDbType.Int).Value = 1;
else
 cmd.Parameters.Add("@tt_maintenance", SqlDbType.Int).Value = 0;

cmd.Parameters.Add("@tt_specify_maintenance", SqlDbType.Text).Value = tt_specify_maintenance.Text;


if(Convert.ToString(tt_improvement.SelectedValue) == "1")                             
 cmd.Parameters.Add("@tt_improvement", SqlDbType.Int).Value = 1 ;
else
 cmd.Parameters.Add("@tt_improvement", SqlDbType.Int).Value = 0 ;

cmd.Parameters.Add("@tt_specify_improvement", SqlDbType.Text).Value = tt_specify_improvement.Text;


if(Convert.ToString(tt_notice_to_enter.SelectedValue) == "1")
 cmd.Parameters.Add("@tt_notice_to_enter", SqlDbType.Int).Value = 1;
else
 cmd.Parameters.Add("@tt_notice_to_enter", SqlDbType.Int).Value = 0;

if (tt_specify_number_of_hours.Text == "")
    cmd.Parameters.Add("@tt_specify_number_of_hours", SqlDbType.Int).Value = 0;
else
   cmd.Parameters.Add("@tt_specify_number_of_hours",SqlDbType.Int).Value= Convert.ToInt32(tt_specify_number_of_hours.Text);

 if(Convert.ToString(ddl_tt_tenant_content_ins.SelectedValue) == "0")
  cmd.Parameters.Add("@tt_tenant_content_ins", SqlDbType.Int).Value = 0; 

 if(Convert.ToString(ddl_tt_tenant_content_ins.SelectedValue) == "1")
 cmd.Parameters.Add("@tt_tenant_content_ins", SqlDbType.Int).Value = 1;

 if(Convert.ToString(ddl_tt_tenant_content_ins.SelectedValue) == "2")
     cmd.Parameters.Add("@tt_tenant_content_ins", SqlDbType.Int).Value = 2;




if(Convert.ToString(ddl_tt_landlord_content_ins.SelectedValue)== "0" )
    cmd.Parameters.Add("@tt_landlord_content_ins", SqlDbType.Int).Value = 0;
if(Convert.ToString(ddl_tt_landlord_content_ins.SelectedValue)== "1" )
    cmd.Parameters.Add("@tt_landlord_content_ins", SqlDbType.Int).Value = 1;
if(Convert.ToString(ddl_tt_landlord_content_ins.SelectedValue)== "2" )
    cmd.Parameters.Add("@tt_landlord_content_ins", SqlDbType.Int).Value = 2;



if(Convert.ToString(ddl_tt_injury_ins.SelectedValue) == "0")
    cmd.Parameters.Add("@tt_injury_ins", SqlDbType.Int).Value = 0;
if(Convert.ToString(ddl_tt_injury_ins.SelectedValue) == "1")
    cmd.Parameters.Add("@tt_injury_ins", SqlDbType.Int).Value = 1;
if(Convert.ToString(ddl_tt_injury_ins.SelectedValue) == "2")
    cmd.Parameters.Add("@tt_injury_ins", SqlDbType.Int).Value = 2;
                    


 if(Convert.ToString(ddl_tt_premises_ins.SelectedValue) == "0" )
     cmd.Parameters.Add("@tt_premises_ins", SqlDbType.Int).Value = 0;
if(Convert.ToString(ddl_tt_premises_ins.SelectedValue) == "1" )
    cmd.Parameters.Add("@tt_premises_ins", SqlDbType.Int).Value = 1;
if(Convert.ToString(ddl_tt_premises_ins.SelectedValue) == "2" )
    cmd.Parameters.Add("@tt_premises_ins", SqlDbType.Int).Value = 2;



           cmd.Parameters.Add("@tt_additional_terms", SqlDbType.Text).Value = tt_additional_terms.Text;

           cmd.Parameters.Add("@tt_nsf", SqlDbType.SmallMoney).Value = Convert.ToDecimal(tt_nsf.Text);
           cmd.Parameters.Add("@tt_form_of_payment", SqlDbType.Int).Value = Convert.ToInt32(ddl_tt_form_of_payment.SelectedValue);

                 cmd.ExecuteReader();
           }
           finally
           {
               conn.Close();
           }

     }
     else
     {
         Response.Redirect(tiger.security.Access.toLoginPage());
     }
    }
}
