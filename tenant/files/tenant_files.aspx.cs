﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Briefcase;
using System.IO;

public partial class tenant_tenant_tenant_files : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            TenantBindGrid();
        }
    }


    private void TenantBindGrid()
    {

        DataTable files = Files.GetTenantFileList(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["name_id"]));
        GridView3.DataSource = files;
        GridView3.DataBind();
    }

    protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }

    protected void GridView3_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int fileid = int.Parse(GridView3.DataKeys[int.Parse(e.CommandArgument.ToString())].Value.ToString());

        if (e.CommandName == "Download")
        {
            DataTable file = Files.TenantGetFile(fileid, Convert.ToInt32(Session["schema_id"]));
            byte[] filedata = (byte[])file.Rows[0]["filedata"];
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment;filename=" + file.Rows[0]["filename"]);
            Response.BinaryWrite(filedata);
            Response.End();
        }
    }


}
