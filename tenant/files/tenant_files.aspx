﻿<%@ Page Title="" Language="C#" MasterPageFile="~/tenant/tenant.master" AutoEventWireup="true" CodeFile="tenant_files.aspx.cs" Inherits="tenant_tenant_tenant_files" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:Label ID="lbl_tn_tenant_files" Text="<%$ Resources:Resource_Tenant, lbl_tn_tenant_files %>" runat="server"></asp:Label>

 <br />
 <br />
                    
  <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="False" 
       BorderColor="#CDCDCD"  BorderWidth="1px"
       DataKeyNames="tenantfiles_id" 
       OnRowCommand="GridView3_RowCommand"
       OnRowDataBound="GridView3_RowDataBound">
     <Columns>
     <asp:BoundField 
       DataField="home_name" 
       HeaderText="Property" ReadOnly="True" />
     <asp:TemplateField 
          HeaderText="<%$ Resources:Resource_Tenant, lbl_file_name %>">
     
     <ItemTemplate>
     <asp:Label ID="Label1" runat="server" Text='<%# Eval("FileName") %>'></asp:Label>
     </ItemTemplate>
     </asp:TemplateField>
     <asp:BoundField 
       DataField="FileSize" 
       HeaderText="Size (bytes)" ReadOnly="True" />
     <asp:BoundField 
          DataField="DateCreated" 
          HeaderText="<%$ Resources:Resource_Tenant, lbl_created_on %>" ReadOnly="True" 
          DataFormatString="{0:MMM dd , yyyy}"  
          HtmlEncode="False"/>
     <asp:ButtonField 
         ButtonType="Button"
        CommandName="Download" Text="<%$ Resources:Resource_Tenant, lbl_download %>" />
     
     </Columns>
     <AlternatingRowStyle BackColor="White" />
     <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
     <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
     <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
     <RowStyle BackColor="#E3EAEB" />
     <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
     </asp:GridView><br /><br />

   
   
   

</asp:Content>

