<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EncDecViaCode.aspx.cs" Inherits="EncDecViaCode" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 321px">
            <tr>
                <td align="center" style="width: 444px">
                    <asp:Label ID="Label1" runat="server" Font-Size="X-Large" ForeColor="Blue" Text="Protect or Unprotect Connection String"
                        Width="385px"></asp:Label></td>
            </tr>
            <tr>
                <td align="center" style="width: 444px">
                    <br />
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Encrypt Connection String" /><br />
                </td>
            </tr>
            <tr>
                <td align="center" style="width: 444px">
                    <br />
                    <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Decrypt Connection String" /><br />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
