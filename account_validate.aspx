﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="account_validate.aspx.cs" Inherits="account_validate" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css"  >
    div.row {float: left; margin:  0px 0px 8px 0px; padding: 0; width: 98%;}
    div.col500px {float: left; width: 500px; margin: 0 3px 0 0; padding: 0;}
    div.col35em {float: left; width:35em; margin: 0 3px 0 0; padding: 0;}
    
           
        body{
        font-family:"Lucida Grande", "Lucida Sans Unicode", Verdana, Arial, Helvetica, sans-serif;
        font-size:12px;
         background-color:#ECF4FB;
        }
        p, h1, form, button{border:0; margin:0; padding:0;}
        .spacer{clear:both; height:1px;}
        /* ----------- My Form ----------- */
        .myform{
        margin:0 auto;
        width:400px;
        padding:14px;
        }

        /* ----------- stylized ----------- */
        #stylized{
        border:solid 2px #b7ddf2;
        background:#fff;
        }
        #stylized h1 {
        font-size:14px;
        font-weight:bold;
        margin-bottom:8px;
        }
        #stylized p{
        font-size:11px;
        color:#666666;
        margin-bottom:20px;
        border-bottom:solid 1px #b7ddf2;
        padding-bottom:10px;
        }
        #stylized label{
        display:block;
        font-weight:bold;
        text-align:right;
        width:140px;
        float:left;
        }
        #stylized .small{
        color:#666666;
        display:block;
        font-size:11px;
        font-weight:normal;
        text-align:right;
        width:140px;
        }
        #stylized input{
        float:left;
        font-size:12px;
        padding:4px 2px;
        border:solid 1px #aacfe4;
        width:200px;
        margin:2px 0 20px 10px;
        }
        #stylized button{
        clear:both;
        margin-left:150px;
        width:125px;
        height:31px;
        background:#666666 url(img/button.png) no-repeat;
        text-align:center;
        line-height:31px;
        color:#FFFFFF;
        font-size:11px;
        font-weight:bold;
        }
    
    </style>
    <script type="text/javascript">
    
      function pageLoad() {
      }
    
    </script>
</head>
<body>
     <br /><br /><br /><br /><br />
    <div id="stylized" class="myform">
    <form id="form"  runat="server">
    
    <h1>
        <asp:Image ID="Image1" runat="server" ImageUrl="~/login2_files/logo.png" />
    </h1>
    <h1>Account validation ( Dev. )</h1>
    <p>&nbsp;</p>

        <div id="Div3" class="row">
        <div class="col35em">
        <label >User name
            :
        <span class="small">
        </span>
        </label>&nbsp;
        <asp:Label ID="usr" runat="server" />
        </div>  
        </div> 

    <label>Validation Key
    <span class="small">
    </span>
    </label>
    <asp:TextBox id="schema_activation_code"  runat="server"/>
   <br />
   
        <div id="Div5" class="row">
        <div class="col35em">
        <label >Activation code
            :
        <span class="small">
        </span>
        </label>&nbsp;
        <asp:TextBox id="activation_code"  runat="server"/>
        </div>  
        </div> 
   
       <div id="Div1" class="row">
        <div class="col35em">
        <label >&nbsp;&nbsp;
        <span class="small">
            </span></label>&nbsp;
         <asp:Button ID="btn_activate" Width="100px" runat="server" Text="Activate" 
                onclick="btn_activate_Click" />
       &nbsp;&nbsp;
            <asp:HyperLink ID="link_login" runat="server" NavigateUrl="~/login.aspx" 
                style="font-size: large">Login</asp:HyperLink>
       </div>  
        </div> 
   
    
    
    
       <div id="Div2" class="row">
        <div class="col35em">
        <label >&nbsp;
        <span class="small">
        </span>
        </label>&nbsp;
        <asp:Label ID="txt" runat="server" />
        </div>  
        </div> 
        
         <div id="Div4" class="row">
        <div class="col35em">
        <label >&nbsp;
        <span class="small">
        </span>
        </label>&nbsp;
        <asp:Label ID="message" runat="server" />
        </div>  
        </div> 
 <div class="spacer"></div>
    </form>
    </div>
</body>
</html>
