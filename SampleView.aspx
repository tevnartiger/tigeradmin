<%@ Page Language="c#" Debug="true" EnableSessionState="False" EnableViewState="False" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head runat="server">
    <title>.net Charting Sample</title>
    <script runat="server">
        string CurrentVersion = "44";
        string LastVersion = "43";

        void Page_Load(Object sender, EventArgs e)
        {
            string name = "";
            if (Page.Request.QueryString["id"] != null)
                name = Page.Request.QueryString["id"].ToString();
            string lang = "";
            if (Page.Request.QueryString["lang"] != null)
                lang = Page.Request.QueryString["lang"];

            for (int i = 0; i < samples.Length; i++)
            {
                if (samples[i] == name)
                {
                    HttpCookie cookie = null;
                    if (lang != null && lang.Length > 0)
                    {
                        cookie = new HttpCookie("dncSampleLanguage");
                        lang = cookie.Value = lang;
                        Response.Cookies.Add(cookie);
                    }
                    else
                    {
                        cookie = Request.Cookies["dncSampleLanguage"];
                        if (cookie != null)
                        {
                            if (cookie.Value == "VB")
                                lang = "VB";
                            else lang = "CS";
                        }
                        else
                        {
                            cookie = new HttpCookie("dncSampleLanguage");
                            lang = cookie.Value = "CS";
                        }
                    }

                    SName.Text += "Sample:";
                    if (versions[i] == CurrentVersion)
                        SName.Text += "<img align=absmiddle src=\"images/new2.gif\">";
                    else if (versions[i] == LastVersion)
                        SName.Text += "<img align=absmiddle src=\"images/new.gif\">";
                    myIFrame.Visible = true;
                    if (lang == "VB")
                    {
                        myIFrame.Text = "<iframe width=\"100%\" height =\"800\" src=\"Samples/VisualBasic/" + name + ".aspx\"></iframe>";
                        SName.Text += " <b>" + name + "</b>   <br>Source: <b>[Visual Basic]</b><BR>" + Server.MapPath("Samples/VisualBasic/" + name + ".aspx");
                        myCode.Text = getSampleCode("Samples\\VisualBasic\\" + name);
                        LangSelect.Text = "<a href=\"Samples/VisualBasic/" + name + ".aspx\"><img align=absmiddle title=\"Open Sample in this window.\" border=0 src=\"images/nvopensample.gif\"></a> <a href=\"SampleView.aspx?id="
                        + samples[i] + "&lang=CS\" ><img border=\"0\" src=\"images/nvcsharp.gif\" align=\"absmiddle\" title=\"Run C Sharp Version\"></a>";
                    }
                    else
                    {
                        myIFrame.Text = "<iframe width=\"100%\" height =\"800\" src=\"Samples/CSharp/" + name + ".aspx\"></iframe>";
                        SName.Text += "<b>" + name + "</b>   <br>Source: <b> [C#]</b><br>" + Server.MapPath("Samples/CSharp/" + name + ".aspx");

                        myCode.Text = getSampleCode("Samples\\CSharp\\" + name);
                        LangSelect.Text = "<a href=\"Samples/CSharp/" + name + ".aspx\"><img align=absmiddle title=\"Open Sample in this window.\" border=0 src=\"images/nvopensample.gif\"></a> <a href=\"SampleView.aspx?id="
                        + samples[i] + "&lang=VB\" ><img border=\"0\" src=\"images/nvvbasic.gif\" align=\"absmiddle\"  title=\"Run Visual Basic Version\"></a>";
                    }

                    int p = (i + samples.Length - 1) % samples.Length; int n = (i + 1) % samples.Length;
                    NavLabel0.Text = "";
                    NavLabel1.Text = "<a href=\"SampleView.aspx?id=" + samples[p] + "\"><img border=\"0\" src=\"images/thumbs/" + samples[p] + ".gif\" height=80><br>" + samples[p] + "</a>";
                    NavLabel2.Text = "<a href=\"SampleView.aspx?id=" + samples[n] + "\"><img border=\"0\" src=\"images/thumbs/" + samples[n] + ".gif\" height=80><br>" + samples[n] + "</a>";
                    NavLabel3.Text = "";

                    TopPNLabel.Text = "<a href=\"SampleView.aspx?id=" + samples[p] + "\"><img border=\"0\" src=\"images/nvprevioustop.gif\" align=\"absmiddle\"></a><img height=\"1\" alt=\"\" src=\"images/s.gif\" width=\"90\" border=\"0\"><a href=\"SampleView.aspx?id="
                    + samples[n] + "\"><img border=\"0\" src=\"images/nvnexttop.gif\" align=\"absmiddle\"></a><img height=\"1\" alt=\"\" src=\"images/s.gif\" width=\"50\" border=\"0\">";
                    continue;
                }
            }
        }



        string[] samples = {
"AxisCaps","AxisDualScales",
"AxisGridLinePerTick","AxisMinimumInterval","AxisOrientation","AxisScaleBreaks","AxisScaleBreaksManual","SmartScaleBreakLimit","AxisScaleRange","AxisScaleSynchronization","AffectingRanges","CalculatedTimeAxis","CalendarPattern","InteractiveUnitConversion","InvertedAxis","InvisibleAxis","SeriesAxisBinding","SeriesAxisBindingdb","TimeInterval","Unitconversion","AdvancedTimeInterval","ScaleBreakCalendarPattern",
"ScaleBreakStyling","AxisLogBases","AxisScaleLogRange","GetAxisRange","LineCapScale","SmartLogMinorTicks","AxisMarkerAdvanced","AxisMarkers","AxisMarkerTicks","AxisHorizontalColorBands","DynamicAxisMarkers","RadarMarkers","RadarMarkersInFront","AxisSmartScaleBreakWithMarker","XAxisLabelAngles","AxisLabelTokens","AxisLabelAlignment","AxisMinorTicks","AxisSmartMinorTicks","AxisMinorTicksTrick",
"AxisShadowCalculatedTicks","AxisShadowImportantTicks","AxisShadowSeriesLastValues","AxisShadowSeriesNames","AxisShadowSeriesValues","AxisShadowSeriesValues2","AxisShadowSeriesValues3","AxisTickAlignment","AxisTickAutoAddRangeTimeLabels","AxisTickAutoRangeTimeLabels","AxisTickAutoTimeLabels","AxisTickOverrides","AxisTickSmartTimeLabels","AxisTickToken","AxisTickTypes","ElementTicks","ArbitraryTicks","ArbitraryTicks2","DynamicTicks","AxisRangeTextTicks",
"Ticklabelmodes","tickSeparators","RangedTicks","CustomFunction","AxisLabelOverrides","AxisTickExpressions","AutoTimeRangeRowLimit","annotationAlignment","AnnotationDataSource","AnnotationLink","annotationOrientation","DynamicAnnotation","HeaderAnnotation","ElementAnnotation","DataSourceAnnotation","FinancialMA","AroonIndicators","ChaikinIndicators","CommodityChannelIndex","Correlation",
"Filters","ZScore","Trendline","TrendlineScatter","DateTrendLine","MarketStrength","FinancialOneValueIndicators","FrequencyTable","Oscillators","RateCharts","RunningSum","CrossingSignal","ExtremeValueSignal","GeneralLinear","GeneralLinearFit","LogarithmicFitting","NonLinearPolynomial","PolynomialFitting","PolynomialFittingExtended","Residuals",
"SineFitting","Stochastic","TrendLineExponential","TrendLineExponentialExt","TrendLineLogarithmic","TrendLinePolynomial","TrendLinePolynomialExt","TrendLinePower","VolumeIndicators","ChartAreaDataDistribution","ExponentialDistribution","FiniteImpulseResponse","LogNormalDistribution","NormalDistribution","PriceActionIndicator","RChart","RunChart","SChart","MarketFacilitationIndex","SeriesDataDistribution",
"AgeHistogram","DirectionalMovement","DMISignal","ExponentialDistribution1","ExponentialDistribution2","FinancialKurtosisIndicator","ForecastTraffic","Histogram","NormalDistribution1","NormalDistribution2","SoftDrinksHistogram","TrueRange","Pearson","BesselI","BesselJ","BesselK","BesselY","Beta","GammaP","GammaQ",
"ChartAreaCustomLayout","ChartAreaDB","ChartAreaDB2","ChartAreaFinancial","ChartAreaRelationships","ChartAreaSizing","AlignChartAreas","AlignChartAreasAdvanced","CloseChartAreas","DietDB","ColorBlindPie","ColorByElements","MiniStackedBar","MultiColorArea","DashboardLinearGauge","DashboardLinearGauge2","FunnelArea","ElementErrorBars","ElementMarkers","ElementSubValueStyling",
"EmptyElements1","EmptyElements2","ProgressBar","Pyramid","RadarAngles","RadarBackground","RadarMode","DifferenceSeries","Padding","Pareto","TakeColorFrom","VersionHistory","VersionHistoryGantt","GanttAdvanced","GanttAdvanced2","InvisibleElementMarkers","InvisibleElements","SmartPalette","SmartPaletteSaving","SmartPaletteLoading",
"SmartPaletteAutoSync","Palettes","SplineTension","BoxAndWhisker","SmartPaletteDateRange","SmartPaletteRange","StepLine","OddShapeColumns","MathFunctionPlot","PieSumRing","PieHeights","ManualErrorBars","ComboWithPie","ComboWithPie2d","GDPBubblePie","OnOffChart","ElementBorders","ImageBarOneToOne","ImageBarOneToOne3","SunBubbles",
"GaugeMultiNeedles","GaugeNeedlesWithMarkers","GaugeStyling","GaugeClock","GaugeAngles","GaugeMarkers","DefaultGaugeClipping","GaugeCustomNeedle","GaugeCustomNeedle2","GaugeHRangeTick","GaugeIndicatorLightsByValue","GaugeLinearDynamicColorMarkers","GaugeLinearForcedMarkers","GaugeLinearScaleBreak","GaugeLinearStyled","GaugeLinearStyled2","GaugeLinearVAxisMarkers","GaugeLinearVSTyled","GaugeThermometerAxisMarkers","GaugeThermometerAxisRangeTickMarker",
"GaugeThermometerForcedMarkers","GaugeThermometerScaleBreak","GaugeThermometerStyled","GaugeThermometerStyled2","GaugeThermometerStyled3","GaugeBackground","MultiRowIndicators","AxisLineAndMarkers","CompoundGauge","CompoundGauge2","GaugeAbsolutePosition","GaugeRectangles","GaugeSizing","ImageBarGauge","ImageBarOneToOne4","ProgressBar2","LegendBox-Trick1","LegendBox","LegendBoxAbsolute","LegendBoxCustom",
"LegendBoxCustomHeader","LegendBoxDataSource","LegendBoxElement","LegendBoxElement2","LegendBoxEntryTemplate","LegendBoxHeader","LegendBoxMultiple","LegendBoxTitle","InvisibleLegendEntry","AlignLegendToChart","LegendColumnAlignment","LegendCustomIcons","TitleBox","TitleBoxTokens","TitleLegendBox","TitleBoxHeader","TitleBoxLegendHeader","TitleShading","TitleShading2","LineLabelAlignment",
"ElementTemplate","LabelOutlines","LabelTruncation","LabelExpressions","CustomType","SeriesLabeling","PieLabelTokens","CenteredBubbleLabels","DataSourceExpressions","PieLabelSmartWraping","RoundingLabels","AutoWrapLabels","TokensAndExpressions","TemplatesTooltipsLinks","LabelStyling","backgroundColor","backgroundGradient","backgroundImage","backgroundImageStretch","backgroundImageTile",
"BarFixedSize","Bevel","HatchEnhanced","HatchPalette","HatchStylePalette","GlassEffect","floatingPie","ImageBars","ImageBars2","ImageBars3","ImageBars4","ImageBar","GradientLine","GradientSpline","GradientSpline3D","HatchPie","BrushBackground","MarkerImageColorReplace","DynamicColorThreshold","DynamicColorThreshold2",
"TransparentBackground","FiveColorPalettes","BoxCornerStyling","BoxHeaders","BoxShadingEffects","BoxStyling","BoxCoordinates","PostRenderDrawing","PostRenderLabeling","ElementCoordinates","PdfImage","SvgImage","SvgzImage","XamlImage","SwfImage","ChartStream","StreamingUsage","Streaming","UseSession","DpiSetting",
"SaveLegendImage","TifImage","WmfImage","JpgImage","BmpImage","PngImage","Bubble","BubbleDateTime","cluster","items1","items2","items3","items4","items5","items6","manualSettings","orders1","orders2","orders3","orders4",
"orders6","sales1","sales2","sales3","sales4","sales5","sales6","sales7","sample","Scatter","day","days","quarter","quarters","minutes","month","months","year","years","hour",
"hours","week","Seconds","Minute","DataEngine","GanttDB","ScatterDateTimeDB","MultiSeriesFromColumns","MultiSeriesFromColumns2","datagrid","DatagridCustom","datagridTemplate","FinancialDB","splitby","StartDayOfWeek","StartMonthOfYear","customAttributes","ganttDateTimedb","limit","LimitPrimarySeries",
"CustomAttributeFormatting","PostDataProcessing","QuickDBChart","DataGridDataEngine","ArrayList","dataReader","dataSet","dataTable","dataView","excel","HashTable","ObjectCollection","storedProcedure","xml","xml2","xml3","xmlDocument","XmlCustomAttributes","xmlString","xmlXPath",
"SeriesCollectionSort","SeriesTrimming","sortElement","TiaSplit","TiaSplitRegroup","ElementNameGrouping","ElementNameGroupingOptions","AutoDataAccess","IterateElements","OperatorCalculations","TiaSplitName","SortElementStacks","SmartColorGradient","drilldown","drilldownManual","drilldownmultiseries","drilldownmultiseries2","DrillDownNoDateGrouping","drilldownOtherElement","drilldownOtherSeries",
"drilldownPie","drilldownSplitBy","DrillSlice","Hotspots","PagingElements","PagingSeries","JavascriptClick","JavaScriptToolTips","AxisValueFromClick","YAxisZoom","XAxisZoom","LabelHotspots","TabbedSeries","HideSeries","ImportShapefileData","IteratingShapes","LoadEcwFile","LoadShapefile","MapSmartPalette","Projections",
"ShapeGrouping","ShapeStyling","Zooming","SimpleMap","ViewAttributes","ProjectionsImageLayer","MarkerStyling","ShapeWithLegend","AddMapPoint","MapHotspots","MapLegend","MapLegend2","ClickMap","ClickMapProjected","ClickMapZoom","IdentifyShape","POIMap","ImageMapText","Codebehind","cleanupperiod",
"reloadPeriod","SaveState","LoadState","Postback","Outputcache","Fragmentcache","Usercontrol","multichart","multiChartpro","Debugging","SmartForecast","Gallery/a01","Gallery/a011","Gallery/a012","Gallery/a013","Gallery/A014","Gallery/a015","Gallery/a016","Gallery/a017","Gallery/a018",
"Gallery/a019","Gallery/a0191","Gallery/a02","Gallery/a021","Gallery/a022","Gallery/a023","gallery/a024","Gallery/A0241","gallery/a025","Gallery/a03","Gallery/a031","gallery/a0131","gallery/a0132","Gallery/a032","Gallery/a033","Gallery/a034","Gallery/a035","Gallery/a036","Gallery/a04","Gallery/a041",
"Gallery/a05","Gallery/a06","Gallery/a07","Gallery/a071","Gallery/a072","Gallery/a073","Gallery/a074","Gallery/a08","Gallery/a081","Gallery/a082","Gallery/a09","Gallery/a091","Gallery/a092","Gallery/a10","Gallery/a100","Gallery/a101","Gallery/a11","Gallery/a12","Gallery/a13","Gallery/a14",
"Gallery/a15","gallery/a151","Gallery/a16","Gallery/a17","Gallery/a18","Gallery/a19","Gallery/a20","Gallery/a21","Gallery/a22","Gallery/a23","Gallery/a24","Gallery/a25","Gallery/a26","Gallery/a27","Gallery/a28","Gallery/a281","Gallery/a282","Gallery/a29","Gallery/a30","Gallery/a31",
"Gallery/a311","Gallery/a312","Gallery/a32","Gallery/a33","Gallery/a34","Gallery/a35","Gallery/a36","Gallery/a37","Gallery/a371","Gallery/a372","Gallery/a373","Gallery/a374","Gallery/a38","Gallery/a39","Gallery/a3901","Gallery/a391","Gallery/a392","Gallery/a393","Gallery/a394","Gallery/a40",
"Gallery/a41","Gallery/a42","Gallery/a43","Gallery/a44","Gallery/a441","Gallery/a45","Gallery/a46","Gallery/a47","Gallery/a48","Gallery/a49","Gallery/a50","Gallery/a51","Gallery/a52","Gallery/a53","Gallery/a54","Gallery/a55","Gallery/a56","Gallery/a57","Gallery/a58","Gallery/a60",
"Gallery/a61","Gallery/a62","Gallery/a63","Gallery/a64","Gallery/a65","Gallery/c01","Gallery/c02","gallery/c021","gallery/c022","Gallery/c03","gallery/c031","gallery/c032","gallery/c033","Gallery/c04","Gallery/c05","Gallery/c06","Gallery/c07","Gallery/c08","Gallery/c09","Gallery/b01",
"Gallery/b011","Gallery/b012","Gallery/b013","Gallery/B014","Gallery/b02","Gallery/b021","Gallery/b022","Gallery/B024","Gallery/b03","Gallery/b04","Gallery/b05","Gallery/b051","Gallery/b06","Gallery/b061","Gallery/b07","gallery/b071","Gallery/b08","Gallery/b09","Gallery/b10","Gallery/b11",
"Gallery/b111","Gallery/b12","Gallery/b13","Gallery/b14","Gallery/b141","Gallery/b142","Gallery/b1421","Gallery/b1422","Gallery/b1423","Gallery/b1424","gallery/b0143","gallery/b0144","gallery/b0145","gallery/b0146","Gallery/b15","Gallery/b16","Gallery/b17","Gallery/b18","Gallery/b19","Gallery/b20",
"Gallery/b21","Gallery/b22","Gallery/b23","Gallery/b24","Gallery/b25","Gallery/b26","Gallery/b27","Gallery/b28","Gallery/b29","Gallery/b30","Gallery/b31","Gallery/b32","Gallery/b33","Gallery/b34","Gallery/b35","Gallery/b36","Gallery/b37","Gallery/b38","Gallery/b39","Gallery/b40",
"Gallery/b41","Gallery/b42","Gallery/b43","Gallery/b44","Gallery/b45","Gallery/b46","Gallery/b47","Gallery/b48","Gallery/b49","Gallery/b50","Gallery/b51","Gallery/b52","Gallery/b53","Gallery/b54","Gallery/b281","Gallery/b282","Gallery/b311","Gallery/b312","Gallery/b371","Gallery/b372",
"Gallery/b373","Gallery/b374","Gallery/b391","Gallery/b392","Gallery/b393","Gallery/b394","Gallery/d01","Gallery/d02","Gallery/d031","Gallery/d032","Gallery/d03","Gallery/d04","Gallery/d05","Gallery/d06","Gallery/d07","Gallery/d08","Gallery/d09","Gallery/d10","Gallery/d11","Gallery/d12",
"Gallery/d13","Gallery/d14","Gallery/d15","Gallery/d16","Gallery/d17","Gallery/e01","Gallery/e02","Gallery/e03","Gallery/e04","Gallery/e05","Gallery/e051","Gallery/e052","Gallery/e06","Gallery/e07","Gallery/e08","Gallery/e09","Gallery/e10","Gallery/e101","Gallery/e102","Gallery/e11",
"Gallery/e12","Gallery/e13","Gallery/e14","Gallery/e201","Gallery/e202","Gallery/e203","Gallery/e204","Gallery/e205","Gallery/e206","Gallery/e207","Gallery/e208","Gallery/e209","Gallery/e210","Gallery/e211","Gallery/e212","Gallery/e213","Gallery/e214","Gallery/e215","Gallery/e216","Gallery/e217",
"Gallery/f01","Gallery/f02","Gallery/f03","Gallery/f06","Gallery/f07","Gallery/f04","Gallery/f05","Gallery/f051","Gallery/f052","Gallery/f053","Gallery/f054","Gallery/L01","Gallery/L02","Gallery/L03","Gallery/L04","Gallery/L05","Gallery/L06","Gallery/k086","Gallery/k0801","Gallery/k081",
"Gallery/k0811","Gallery/k0815","Gallery/k0816","Gallery/k082","Gallery/k0821","Gallery/k0825","Gallery/k0826","Gallery/k083","Gallery/k0831","Gallery/k0835","Gallery/k0836","Gallery/k084","Gallery/k0846","Gallery/k091","Gallery/g01","Gallery/g02","Gallery/g03","Gallery/g04","Gallery/g05","Gallery/g06",
"Gallery/h01","Gallery/h02","Gallery/h03","Gallery/h003","Gallery/h04","Gallery/h05","Gallery/h06","Gallery/h065","Gallery/h08","Gallery/h09","Gallery/h010","Gallery/h011","Gallery/h012","Gallery/h013","Gallery/h0131","Gallery/h0132","Gallery/h0133","Gallery/h0134","Gallery/h0135","Gallery/h0136",
"Gallery/h046","Gallery/h014","Gallery/h015","Gallery/h016","Gallery/h017","Gallery/h018","Gallery/h019","Gallery/h020","Gallery/h026","Gallery/h027","Gallery/h031","Gallery/h032","Gallery/h033","Gallery/h038","Gallery/h039","Gallery/h040","Gallery/h044","Gallery/h021","Gallery/h022","Gallery/h023",
"Gallery/h024","Gallery/h025","Gallery/h0251","Gallery/h0252","Gallery/h0253","Gallery/h0254","Gallery/h0255","Gallery/h0256","Gallery/h0257","Gallery/h047","Gallery/h048","Gallery/h049","Gallery/h050","Gallery/h051","Gallery/h052","Gallery/h0521","Gallery/h0522","Gallery/h0523","Gallery/h0524","Gallery/h0525",
"Gallery/h0526","Gallery/h045","Gallery/h053","Gallery/h054","Gallery/h055","Gallery/h056","Gallery/h057","Gallery/h059","Gallery/h060","Gallery/h061","Gallery/h062","Gallery/h063","Gallery/h064","Gallery/h070","Gallery/h0701","Gallery/h0702","Gallery/h0703","Gallery/h0704","Gallery/i01","Gallery/i02",
"Gallery/i03","Gallery/i04","Gallery/i05","Gallery/i06","Gallery/i07","Gallery/i08","Gallery/i09","Gallery/i10","Gallery/i11","Gallery/i12","Gallery/i13","Gallery/i14","Gallery/i15","Gallery/i16","Gallery/i17","Gallery/i18","Gallery/i19","Gallery/i20","Gallery/i21","Gallery/i22",
"Gallery/i23","Gallery/i24","Gallery/i25","Gallery/i26","Gallery/i27","Gallery/i28","Gallery/i29","Gallery/i30","gallery/i31","gallery/i32","gallery/i33","gallery/i34","gallery/i35","gallery/i36","gallery/i37","gallery/i38","gallery/i39","gallery/i40","gallery/i41","gallery/i42",
"gallery/i43","gallery/i44","gallery/i45","gallery/i46","gallery/i47","gallery/i48","gallery/i49","gallery/i50","gallery/i51","Gallery/i52","Gallery/i53","Gallery/i54","Gallery/i55","Gallery/i56","Gallery/i57","Gallery/i58","Gallery/i59","Gallery/i60","Gallery/i61","Gallery/i62",
"Gallery/i63","Gallery/i64","Gallery/i65","Gallery/i66","Gallery/i67","Gallery/i68","Gallery/i69","Gallery/i70","Gallery/i71","Gallery/i72","Gallery/i73","Gallery/i74","Gallery/i75","Gallery/i76","Gallery/i77","Gallery/i78","Gallery/i79","Gallery/i80","Gallery/i81","Gallery/i82",
"Gallery/i83","Gallery/i84","Gallery/i85","Gallery/i86","Gallery/i87","Gallery/i88","Gallery/i89","Gallery/i90","Gallery/i91","Gallery/i92","Gallery/i93","Gallery/i94","Gallery/i95","Gallery/i96","Gallery/j01","Gallery/j02","Gallery/j03","Gallery/j04","Gallery/j05","Gallery/j06",
"Gallery/j07","gallery/k01","gallery/k02","gallery/k03","gallery/k04","gallery/k05","gallery/k06","Gallery/k011","Gallery/k021","Gallery/k0701","Gallery/k071","Gallery/k08","Gallery/k09","Gallery/k0702","Gallery/k0703","gallery/kf01","gallery/kf02","gallery/kf03","gallery/kf04","gallery/kf05",
"gallery/kf06","gallery/kfp01","gallery/kfp02","gallery/kfp03","gallery/kfp04","gallery/kfp05","gallery/kfp06","gallery/kc01","gallery/kc02","gallery/kc03","gallery/kp01","gallery/kp02","gallery/kp03"
};

        string[] versions = {
"-","-",
"-","-","-","-","-","42","-","-","-","-","-","-","-","-","-","-","-","-","-","-",
"-","-","-","43","-","44","-","-","-","-","-","-","-","-","-","-","-","-","-","-",
"-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","3.4",
"-","-","-","-","-","-","42","-","-","-","-","-","-","-","41","-","-","-","-","-",
"-","-","-","-","42","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-",
"-","-","-","-","-","-","-","-","-","3.4","3.4","3.4","3.4","3.4","3.4","3.4v","3.4","3.4","3.4","3.4",
"4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0","-","42","42","42","42","42","42","42",
"-","-","-","-","-","-","44","44","44","-","-","-","-","-","-","-","-","-","-","-",
"-","-","-","-","-","-","-","-","-","-","-","-","-","-","43","-","-","-","-","-",
"-","-","-","3.4","4.0","4.0","4.0","41","41","43","43","43","43","43","43","44","44","44","44","44",
"-","-","-","-","-","-","43","43","43","43","43","43","43","43","43","43","43","43","43","43",
"43","43","43","43","43","-","44","44","44","44","44","44","44","44","44","44","-","-","-","-",
"-","-","-","-","-","-","-","-","-","3.4","41","44","-","-","-","44","44","44","44","-",
"-","4.0","-","-","3.4","3.4","3.4","-","41","41","41","41","41","-","44","-","-","-","-","-",
"-","-","-","-","-","-","-","4.0","41","42","43","4.0","-","-","-","-","-","43","43","43",
"44","44","44","44","44","44","","-","-","42","4.0","4.0","41","41","4.0","4.0","3.0","3.0","42","3.4",
"-","43","43","43","43","43","-","-","-","-","-","-","-","-","-","-","-","-","-","-",
"-","-","-","-","-","-","-","-","-","-","-","","-","-","-","-","-","-","-","-",
"-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-",
"3.4","-","41","42","-","","-","-","-","-","-","-","-","-","-","43","-","41","41","41",
"-","-","-","41","41","41","42","41","-","","43","44","44","-","-","-","-","-","-","-",
"-","-","-","-","-","-","-","-","4.0","4.0","4.0","4.0","41","42","4.0","4.0","4.0","4.0","4.0","4.0",
"4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0","41","41","41","41","41","41","41","41","43","-","3.4","-",
"-","-","-","-","-","-","3.0","","","","41","-","-","-","4.0","42","44","44","44","44",
"44","44","-","-","-","4.0","41","42","41","-","-","41","41","-","4.0","42","44","44","-","-",
"-","-","-","-","-","44","44","-","42","42","-","42","42","-","42","42","-","-","-","-",
"-","41","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-",
"-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-",
"-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-",
"-","-","-","4.0","4.0","-","-","41","41","-","41","41","41","-","-","-","-","-","-","-",
"-","-","-","42","-","-","4.0","42","-","-","-","42","-","42","-","41","-","-","-","-",
"42","-","-","-","-","-","42","42","44","44","41","41","41","41","-","-","-","-","-","-",
"-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-",
"-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-","-",
"-","-","-","-","-","-","-","-","4.0","4.0","-","-","-","-","-","-","-","-","-","-",
"-","-","-","-","-","-","-","-","-","-","4.0","4.0","-","-","-","-","-","4.0","4.0","-",
"-","-","-","43","43","43","43","43","43","43","43","43","43","43","43","43","43","43","43","43",
"-","-","-","-","-","-","-","-","-","4.0","44","42","42","42","42","42","42","42","42","42",
"42","42","42","42","42","42","42","42","42","42","42","42","42","42","-","-","-","-","-","-",
"-","-","-","43","-","-","-","43","43","43","43","43","43","43","44","44","44","44","44","44",
"43","43","43","43","43","43","43","43","43","43","43","43","43","43","43","43","43","43","43","43",
"43","43","43","44","44","44","44","44","44","43","43","43","43","43","43","44","44","44","44","44",
"44","43","43","43","43","43","43","43","43","43","43","43","43","44","44","44","44","44","4.0","4.0",
"4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0",
"4.0","4.0","4.0","4.0","4.0","4.0","4.0","4.0","41","41","41","41","41","41","41","41","41","41","41","41",
"41","41","41","41","41","41","41","41","41","42","42","42","42","42","42","42","42","42","42","42",
"42","42","42","42","43","43","43","43","43","43","43","43","43","43","43","43","43","43","43","43",
"43","43","43","43","43","43","43","43","43","43","43","43","43","43","4.0","4.0","4.0","4.0","4.0","4.0",
"4.0","41","41","41","41","41","41","42","42","42","42","43","43","44","44","41","41","41","41","41",
"41","41","41","41","41","41","41","41","41","41","41","41","41"
};


        public string getSampleCode(string name)
        {
            StringBuilder SB = new StringBuilder(5000);
            string path = name + ".aspx";
            if (path.IndexOf(@":\") == -1)
                path = HttpContext.Current.Server.MapPath(path);
            try
            {
                using (StreamReader sr = File.OpenText(path))
                {
                    string s = "";
                    StringBuilder myS = new StringBuilder(2000);
                    while ((s = sr.ReadLine()) != null)
                    {
                        myS.Append(s + "\r");
                    }
                    SB.Append(myS.ToString());
                }
            }
            catch
            {
                return "Source file is inaccessible in " + path.ToString();
            }
            StringWriter writer = new StringWriter();
            Server.HtmlEncode(SB.ToString(), writer);
            string myString = writer.ToString();
            myString = myString.Replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            myString = myString.Replace("     ", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            myString = myString.Replace("\r", "<BR>");
            return myString;
        }
	
    </script>

    <script type="text/javascript" language="javascript">

	function showHelp()
	{
		f = document.getElementById('codetable');
		i = document.getElementById('helpTable');
		if(i.style.display == "none")
		{
			f.style.display = "none";
			i.style.display = "block";
		}
		else
		{
			i.style.display = "none";
		}
	}
	
	function showCode()
	{
		f = document.getElementById('codetable');
		i = document.getElementById('helpTable');
		if(f.style.display == "none")
		{
			i.style.display = "none";
			f.style.display = "block";
		}
		else
		{
			f.style.display = "none";
		}
	}

    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="C#">
    <meta name="vs_defaultClientScript" content="javascript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
</head>
<body bgcolor="#ffffff">
    <form id="Form1" method="post" runat="server">
        <div align="center">
            <table cellspacing="4" cellpadding="0" width="100%" border="0" id="table701">
                <tr valign="top">
                    <td width="100%">
                        <div style="border-right: #3171d6 1px solid; border-top: #3171d6 1px solid; border-left: #3171d6 1px solid;
                            width: 100%; border-bottom: #3171d6 1px solid">
                            <table cellspacing="0" cellpadding="0" width="100%" border="0" id="table702">
                                <tr valign="top">
                                    <td width="100%">
                                        <table cellspacing="0" cellpadding="0" width="100%" id="table703">
                                            <tr valign="middle">
                                                <td class="activewindow" align="left" background="images/headline_light.gif">
                                                    <img height="22" alt="" src="images/s.gif" width="5" border="0"></td>
                                                <td class="activewindow" align="left" background="images/headline_light.gif">
                                                    <img height="22" alt="" src="images/s.gif" width="5" border="0"></td>
                                                <td class="activewindow" nowrap align="left" background="images/headline_light.gif">
                                                    <span style="font-size: 8.5pt"><b><font face="Verdana" color="#ffffff">Current Sample</font></b></span></td>
                                                <td class="activewindow" align="left" background="images/headline_light.gif">
                                                    <img height="22" alt="" src="images/s.gif" width="5" border="0"><img height="22"
                                                        alt="" src="images/headline_divider.gif" border="0" align="top"></td>
                                                <td align="right" class="activewindow" width="100%" background="images/headline_dark.gif">
                                                    <asp:Label ID="TopPNLabel" runat="server">Label</asp:Label></td>
                                                <td align="right" background="images/headline_dark.gif">
                                                </td>
                                                <td align="right" background="images/headline_dark.gif">
                                                    &nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table border="0" style="border-collapse: collapse" width="100%" id="table704">
                                <tr>
                                    <td width="100%" valign="top" bgcolor="#f7fafd">
                                        <font face="Verdana" size="2">
                                            <asp:Label ID="SName" runat="server"></asp:Label>
                                            <br>
                                            <br>
                                            <br>
                                            <a href="" onclick="Javascript:showCode(); return false;"><font size="1">
                                                <img style="border-top-width: 0px; border-left-width: 0px; border-bottom-width: 0px;
                                                    border-right-width: 0px" alt="View source code" src="images\viewsource.gif" title="View Source Code"
                                                    align="absMiddle"></font></a><font size="1">&nbsp;<asp:Label ID="LangSelect" runat="server"></asp:Label></font><font
                                                        face="Arial" size="1"> <a href="" onclick="Javascript:showHelp(); return false;">
                                                            <img border="0" src="images/nvhelp.gif" align="middle" title="View Help"></a></font></font></td>
                                    <td>
                                        <table border="0" style="border-collapse: collapse" width="100%" id="table705">
                                            <tr>
                                                <td>
                                                    <p align="right">
                                                        <font face="Verdana" style="font-size: 8.5pt">
                                                            <asp:Label ID="NavLabel0" runat="server">x</asp:Label>
                                                        </font>
                                                    </p>
                                                </td>
                                                <td>
                                                    <center>
                                                        <font face="Verdana" style="font-size: 8.5pt">
                                                            <asp:Label ID="NavLabel1" runat="server">x</asp:Label>
                                                        </font>
                                                    </center>
                                                </td>
                                                <td>
                                                    <font face="Verdana" style="font-size: 8.5pt">
                                                        <asp:Label ID="NavLabel2" runat="server">x</asp:Label>
                                                    </font>
                                                </td>
                                                <td>
                                                    <center>
                                                        <font face="Verdana" style="font-size: 8.5pt">
                                                            <asp:Label ID="NavLabel3" runat="server">x</asp:Label>
                                                        </font>
                                                    </center>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <table id="codetable" style="border-right: 1px solid; border-top: 1px solid; display: none;
            border-left: 1px solid; border-bottom: 1px solid" cellspacing="0" cellpadding="4"
            width="100%" border="1" bgcolor="#f8f8f8" bordercolor="#3171d6">
            <tr valign="top">
                <td style="padding-right: 4px; padding-left: 4px; padding-bottom: 4px; padding-top: 4px"
                    align="left" bgcolor="#f2f7fd" bordercolor="#6c9be1">
                    <asp:Label ID="myCode" runat="server" Font-Size="Smaller" Font-Names="Verdana">Label</asp:Label></td>
            </tr>
        </table>
        <table id="helpTable" style="border-right: 1px solid; border-top: 1px solid; display: none;
            border-left: 1px solid; border-bottom: 1px solid" cellspacing="0" cellpadding="4"
            width="100%" border="0" bgcolor="#f8f8f8" bordercolor="#3171d6">
            <tr valign="top">
                <td style="padding-right: 4px; padding-left: 4px; padding-bottom: 4px; padding-top: 4px"
                    align="left">
                    <table border="0" style="border-collapse: collapse" width="100%" id="table8" cellpadding="3"
                        bordercolor="#6c9be1">
                        <tr>
                            <td bgcolor="#3171d6" style="border-top: 1px solid; border-left-width: 1px; border-bottom: 1px solid;
                                border-right-width: 1px">
                                <b><font size="2" face="Verdana" color="#ffffff">Problem</font></b></td>
                            <td bgcolor="#3171d6" style="border-top: 1px solid; border-left-width: 1px; border-bottom: 1px solid;
                                border-right-width: 1px">
                                <b><font size="2" face="Verdana" color="#ffffff">Cause / Solution</font></b></td>
                        </tr>
                        <tr>
                            <td style="border-top-width: 1px; border-left-width: 1px; border-bottom: 1px solid;
                                border-right-width: 1px" bgcolor="#f2f7fd">
                                <font face="Verdana" style="font-size: 8.5pt"><b>Error:
                                    <br>
                                </b><font color="#ff0000">File or assembly name dotnetCHARTING, or one of its dependencies,
                                    was not found.</font></font></td>
                            <td style="border-top-width: 1px; border-left-width: 1px; border-bottom: 1px solid;
                                border-right-width: 1px" bgcolor="#f2f7fd">
                                <font face="Verdana"><font style="font-size: 8.5pt">The most likely reason for this
                                    is that the dotnetcharting folder is not set up as an application.
                                    <br>
                                    <b>See</b>: </font><a href="http://www.dotnetcharting.com/documentation/v4_0/Illustrated%20QuickStart.html">
                                        <font style="font-size: 8.5pt">Illustrated Guide</font></a></font></td>
                        </tr>
                        <tr>
                            <td style="border-top-width: 1px; border-left-width: 1px; border-bottom: 1px solid;
                                border-right-width: 1px" bgcolor="#f2f7fd">
                                <font face="Verdana" style="font-size: 8.5pt"><b>Blank Page:<br>
                                </b>No Chart, No Error message</font></td>
                            <td style="border-top-width: 1px; border-left-width: 1px; border-bottom: 1px solid;
                                border-right-width: 1px" bgcolor="#f2f7fd">
                                <font face="Verdana" style="font-size: 8.5pt">If you experience this, the permissions
                                    for the chart's temp folder are not set correctly<br>
                                </font><font face="Verdana"><font style="font-size: 8.5pt"><b>See</b>: </font><a
                                    href="http://www.dotnetcharting.com/documentation/v4_0/Illustrated%20QuickStart.html">
                                    <font style="font-size: 8.5pt">Illustrated Guide</font></a></font></td>
                        </tr>
                        <tr>
                            <td style="border-top-width: 1px; border-left-width: 1px; border-bottom: 1px solid;
                                border-right-width: 1px" colspan="2" bgcolor="#f2f7fd">
                                <font face="Verdana"><font style="font-size: 8.5pt"><b>See Also:</b> <a target="_blank"
                                    href="http://www.dotnetcharting.com/support.aspx">Support Page</a> | <a target="_blank"
                                        href="http://www.dotnetcharting.com/kb">Knowledgebase</a> | </font><a href="http://www.dotnetcharting.com/documentation/v4_0/Illustrated%20QuickStart.html">
                                            <font style="font-size: 8.5pt">Illustrated Guide</font></a></font></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table border="0" style="border-collapse: collapse" width="100%" id="table4" height="75%">
            <tr>
                <td>
                    <asp:Literal ID="myIFrame" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
