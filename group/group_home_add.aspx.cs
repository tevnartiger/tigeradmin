using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
/// <summary>
/// Done by : Stanley Jocelyn
/// date    : oct 10, 2007
/// </summary>
public partial class group_group_home_add : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Page.IsPostBack == false)
        {
            tiger.Home l = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getHomeCount(Convert.ToInt32(Session["schema_id"]));

            // first we check if there's any property available
            if (home_count > 0)
            {

                tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                r_home_without_group_list.DataSource = h.getHomeWithoutGroupList(Convert.ToInt32(Session["schema_id"]));

                r_home_without_group_list.DataBind();

                tiger.Home p = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ddl_home_list.DataSource = p.getHomeList(Convert.ToInt32(Session["schema_id"]));
                ddl_home_list.DataBind();
                ddl_home_list.Items.Insert(0, "Select a property");
                ddl_home_list.SelectedIndex = 0;

                tiger.Group i = new tiger.Group(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                ddl_group_home_list.DataSource = i.getGroupHomeList(Convert.ToInt32(Session["schema_id"]));
                ddl_group_home_list.DataBind();
                ddl_group_home_list.Items.Insert(0, "Select a group");
                ddl_group_home_list.SelectedIndex = 0;
            }

            else
            {

                txt_message.InnerHtml = "<b><a href='~/home/home_add.aspx'>No property has been added yet ,Please create properties </a></b>";
            }
        }
        
    
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
       
       if (ddl_group_home_list.SelectedIndex != 0 || ddl_home_list.SelectedIndex != 0)
        { 
         string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

         SqlConnection conn = new SqlConnection(strconn);
         SqlCommand cmd = new SqlCommand("prGroupHomeAdd", conn);
         cmd.CommandType = CommandType.StoredProcedure;

         conn.Open();
         //Add the params
         cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
         cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
         cmd.Parameters.Add("@group_home_id", SqlDbType.VarChar, 50).Value = Convert.ToInt32(ddl_group_home_list.SelectedValue);
         cmd.Parameters.Add("@home_id", SqlDbType.VarChar, 50).Value = Convert.ToInt32(ddl_home_list.SelectedValue);
        
         cmd.ExecuteNonQuery();







         tiger.Home h = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
         r_home_without_group_list.DataSource = h.getHomeWithoutGroupList(Convert.ToInt32(Session["schema_id"]));

         r_home_without_group_list.DataBind();

         tiger.Home p = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
         ddl_home_list.DataSource = p.getHomeList(Convert.ToInt32(Session["schema_id"]));
         ddl_home_list.DataBind();
         ddl_home_list.Items.Insert(0, "Select a property");
         ddl_home_list.SelectedIndex = 0;

         tiger.Group i = new tiger.Group(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
         ddl_group_home_list.DataSource = i.getGroupHomeList(Convert.ToInt32(Session["schema_id"]));
         ddl_group_home_list.DataBind();
         ddl_group_home_list.Items.Insert(0, "Select a group");
         ddl_group_home_list.SelectedIndex = 0;
       }
        
 
        
    }
}
