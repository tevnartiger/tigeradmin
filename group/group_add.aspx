<%@ Page Language="C#" AutoEventWireup="true" CodeFile="group_add.aspx.cs" Inherits="group_group_add" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Create a home group</title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="txt_message" runat=server><br />
      CREATE A PROPERTY GROUP<br />
        <br />
        current home group(s)
        <br />
        <asp:Repeater runat=server ID="r_group_home_list">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
            <tr>
                <td>
                   name</td>
                <td >
                    : 
                   
                    <%#DataBinder.Eval(Container.DataItem, "group_home_name")%></td>
            </tr>
         
        </table>
        </ItemTemplate>
        </asp:Repeater>
       
        <br />Add a new group
        <br /><br />
        Group name :&nbsp;<asp:TextBox ID="group_home_name" runat="server"></asp:TextBox><br />
        <br />
        <br />
        <asp:Button ID="btn_create_group" runat="server" OnClick="btn_create_group_Click"
            Text="submit" />
            <br /><br />
        <asp:HyperLink ID="link_main" runat="server" NavigateUrl="~/home/home_main.aspx">go back to main</asp:HyperLink><br />
        </div>
    </form>
</body>
</html>
