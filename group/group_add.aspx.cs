using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class group_group_add : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Page.IsPostBack == false)
        {
            tiger.Home l = new tiger.Home(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            int home_count = l.getHomeCount(Convert.ToInt32(Session["schema_id"]));

            // first we check if there's any property available
            if (home_count > 0)
            {

                tiger.Group h = new tiger.Group(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                r_group_home_list.DataSource = h.getGroupHomeList(Convert.ToInt32(Session["schema_id"]));
                r_group_home_list.DataBind();
            }

            else
            {

                txt_message.InnerHtml = "<b><a href='~/home/home_add.aspx'>Please create properties before creating a group of properties</a></b>";
            }

        }

    }
    protected void btn_create_group_Click(object sender, EventArgs e)
    {


        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prGroupHomeNameAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        conn.Open();
        //Add the params
        cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);

        cmd.Parameters.Add("@group_home_name", SqlDbType.VarChar, 50).Value = group_home_name.Text;
        
        cmd.ExecuteNonQuery();



    }
}
