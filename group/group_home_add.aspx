<%@ Page Language="C#" AutoEventWireup="true" CodeFile="group_home_add.aspx.cs" Inherits="group_group_home_add" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Group home add</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    ADD-Update a Home/Property to a group<br /><br />
     <div id="txt_message" runat=server></div>
        Properties without a group
        <asp:Repeater runat=server ID="r_home_without_group_list">
        <ItemTemplate>
        <table bgcolor="#ffffcc" >
            <tr>
                <td >&nbsp;
                   <%#DataBinder.Eval(Container.DataItem, "home_name")%></td>
            </tr>
        </table>
        </ItemTemplate>
        </asp:Repeater><br />
    
        Group :
    
    <asp:DropDownList ID="ddl_group_home_list" runat=server DataTextField="group_home_name" DataValueField="group_home_id"></asp:DropDownList><br /><br />
        Property :
    <asp:DropDownList ID="ddl_home_list" runat=server DataTextField="home_name" DataValueField="home_id"></asp:DropDownList><br /><br />
    <br /><br />
   <asp:Button ID="btn_submit" runat="server" Text="submit" OnClick="btn_submit_Click" />
    </div>
    </form>
</body>
</html>
