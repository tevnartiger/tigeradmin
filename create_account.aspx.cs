using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class create_account : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void thirty_Click(object sender, EventArgs e)
    {
        Response.Redirect("create_account_30.aspx");
    }

    protected void full_Click(object sender, EventArgs e)
    {
        Response.Redirect("create_account_full.aspx");
    }
}
