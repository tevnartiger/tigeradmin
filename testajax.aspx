﻿<%@ Page Title="" Language="C#" MasterPageFile="~/manager/mp_manager.master" AutoEventWireup="true" CodeFile="testajax.aspx.cs" Inherits="testajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script type="text/javascript" language="javascript" src="js/json2.js"></script>
 <script type="text/javascript">

     function outputDT(dataTable) {
         var headers = [];
         var rows = [];

         headers.push("<tr>");
         for (var name in dataTable[0])
             headers.push("<td><b>" + name + "</b></td>");
         headers.push("</tr>");

         for (var row in dataTable) {
             rows.push("<tr>");
             for (var name in dataTable[row]) {
                 rows.push("<td>");
                 rows.push(dataTable[row][name]);
                 rows.push("</td>");
             }
             rows.push("</tr>");
         }

         var top = "<table border='1'>";
         var bottom = "</table>";

         return top + headers.join("") + rows.join("") + bottom;
     }

    $(function() {
        $("a#demo").click(function() {
            var ladate;
            $.jmsajax({
                type: "POST",
                url: "jMsAjax.aspx",
                method: "getTime",
                data: { date_in: new Date() },
                success: function(data) {
                    $("#div").fadeOut("slow", function() { $(this).html(String(data)) }).fadeIn("slow");

                    if (!(String(data) =="")) 
                   { window.location.replace("/logout.aspx") }
                }
            });
            return false;
        });

    });


    $(function() {
        $("a#demo2").click(function() {
            var ladate;
            $.jmsajax({
                type: "POST",
                url: "jMsAjax.aspx",
                method: "getAppliance",
                dataType: "msjson",
                data: {},
                success: function(data) {


                    if ((String(data) == "") || (String(data) == "{'':[]}"))
                    { window.location.replace("/logout.aspx") }

                    // var myJSONText = JSON.stringify(data);
                    
                    //  alert(data.mydt[0].appliance_name);
                    // $("#div").fadeOut("slow", function() { $(this).html(String(myJSONText)) }).fadeIn("slow");


                    $(outputDT(data.mydt)).appendTo("#div");


                }
            });
            return false;
        });

    });
    
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <a id="demo" href="#">Click here to return the date in the div below</a><br/>
<a id="demo2" href="#">Click here to return the date in the div below</a>

               <br /><br />
               <div class="code">
                    <div id="div">Result here</div>
               </div>


    <asp:Chart ID="Chart1" runat="server">
        <Series>
            <asp:Series Name="Series1">
            </asp:Series>
        </Series>
        <ChartAreas>
            <asp:ChartArea Name="ChartArea1">
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>
</asp:Content>

