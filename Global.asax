﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.IO.Compression" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="log4net" %>

<script runat="server">
    
   
    void Application_Start(object sender, EventArgs e) 
    {
        log4net.Config.XmlConfigurator.Configure();
        Application["Ip_restriction_status"] = "0";
        Application["TotalHits"] = "0";
          
    }

    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }

    /*
    void Application_BeginRequest(object source, EventArgs e)
    {
        //  Code that runs on application shutdown

        HttpContext context = ((HttpApplication)source).Context;
        string ipAddress = context.Request.UserHostAddress.ToString();


        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prIpStatus", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        conn.Open();
        //Add the params
        cmd.Parameters.Add("@return_status", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@ip_address", SqlDbType.VarChar, 15).Value = ipAddress;

        //execute the modification
        cmd.ExecuteNonQuery();


        if (Convert.ToInt32(cmd.Parameters["@return_status"].Value) == 1)
        {
            conn.Close();
            context.Response.Redirect("http://www.google.com");
        }
       

    }
    
    
    */
    
    
    
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

        Exception ex = Server.GetLastError();
        Exception baseex = Server.GetLastError().GetBaseException();
        
        ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        logger.Debug(ex.Message);
        
        // remplacer par SWITCH ... CASE

        if (ex is HttpRequestValidationException)
        {
             Server.ClearError();
             Session.Abandon();
             Response.Redirect("http://www.google.com", true);
             
        }

        // this exception ( entre autre ) will be raise if someone plays with the session id
        if (ex is  HttpException)
        {
        // Session.Abandon();
        // Response.Redirect("http://www.msnbc.com/", true);


        }
        
        if( ex is IndexOutOfRangeException)
        {
            
        }

        if (ex is  ArgumentException)
        {

            Session.Abandon();
            Response.Redirect("http://www.msnbc.com/", true);
        }

        if (baseex is System.FormatException)
        {
          //  Session.Abandon();
           // Response.Redirect("http://www.msnbc.com/", true);
       
        }
        

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

        Session["_lastCulture"] = "en-US";
     
    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

      //  Response.Redirect("https://sinfoca.info/login.aspx");


        Application.Lock();

        //Increment by 1 Application object every time when session starts

        Application["TotalHits"] = (Convert.ToInt32(Application["TotalHits"].ToString()) - 1).ToString();

        //Unlock the Application Object using Unlock() method if Application object

        Application.UnLock();

    }
       
</script>
