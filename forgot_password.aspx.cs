using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class forgot_password : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string temp_user_name = RegEx.getText();

        reg_user_name.ValidationExpression = temp_user_name;

    }
    protected void btnSend_Click(object sender, EventArgs e)
    {
      if(TextBox1.Text == "1906")
        
      {  //verify tampering
        bool bool_val = false;
        string str_val = string.Empty;
        string temp_user_name = txt_user_name.Text;


        if (!RegEx.IsText(temp_user_name))
        {
            bool_val = true;
            str_val += "<br/>" + txt_user_name.ID.ToString() + "<br/>Value: " + temp_user_name;
        }


        str_val += "<br/>" + bool_val;
        if (bool_val)
        {
            

            Security sa = new Security(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            System.Web.HttpBrowserCapabilities browser = Request.Browser;
            sa.sendUserInfo(str_val, Request.Browser, Request);

            lbl_success.Text = "Error occured ";
            btnSend.Enabled = false;
        }
        else
        {
            //check if user is valid
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prLoginUserExist", conn);

            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params
            //return value is the login_id
            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@login_user", SqlDbType.VarChar, 75).Value = temp_user_name;
            cmd.ExecuteNonQuery();
            int temp_login_id = Convert.ToInt32(cmd.Parameters["@Return"].Value);
            lbl_success.Text += "Login ID is "+ temp_login_id+"<br/>";

            if (temp_login_id > 0)
            {
                //generate salt and pwd
                string temp_pwd = sinfoca.tiger.security.Hash.createSalt(6);
                string temp_salt = sinfoca.tiger.security.Hash.createSalt(30);
                string temp_hash = sinfoca.tiger.security.Hash.createPasswordHash(temp_pwd, temp_salt);

                lbl_success.Text += "pwd : "+temp_pwd+"<br/>salt :"+temp_salt+"<br/>hash :"+temp_hash+"<br/>";

                //update
                //check if user is valid
                SqlConnection conn1 = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                SqlCommand cmd1 = new SqlCommand("prPasswordUpdate", conn1);

                cmd1.CommandType = CommandType.StoredProcedure;
                conn1.Open();

                //Add the params
                //return value is the login_id
                cmd1.Parameters.Add("@Return1", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd1.Parameters.Add("@login_id", SqlDbType.Int).Value = temp_login_id;
                cmd1.Parameters.Add("@login_salt", SqlDbType.VarChar, 50).Value = temp_salt;
                cmd1.Parameters.Add("@login_hash", SqlDbType.VarChar, 50).Value = temp_hash;  
                
                cmd1.ExecuteNonQuery();
                if (Convert.ToInt32(cmd1.Parameters["@Return1"].Value) == 0)
                {
                //get email address of user
                    SqlConnection conn2 = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                    SqlCommand cmd2 = new SqlCommand("prLoginUserEmail", conn1);

                    cmd2.CommandType = CommandType.StoredProcedure;
                   // conn2.Open();

                    //Add the params
                    //return value is the login_id
                    cmd2.Parameters.Add("@login_user", SqlDbType.VarChar, 50).Direction = ParameterDirection.Input;
                    cmd2.Parameters.Add("@name_email", SqlDbType.VarChar,50).Direction = ParameterDirection.Output;

                    string temp_name_email = Convert.ToString(cmd2.Parameters["@name_email"].Value);

                                       
                    //send email to 
                    Email.sendSESMailHtml("noreply@sappointment.com", temp_name_email, "Forgot password", "New password is " + temp_pwd);
                    lbl_success.Text += "An email has been sent to your email with a new password";
                }
                else
                {
                    lbl_success.Text += "your email has not been found in our system";
                }


            }
        }

    }
    }
}
