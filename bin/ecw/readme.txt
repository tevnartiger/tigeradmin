The DLLs in this directory are only required when using .netCHARTING's 
Mapping features in combination with ECW image layers.  If you are not using 
mapping with ECW layers these files may be removed.
