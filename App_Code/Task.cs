﻿using System;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Contact
/// </summary>
namespace tiger
{
    public class Task
    {
        private string str_conn;
        public Task(string str_conn)
        {
            this.str_conn = str_conn;
        }



        public DataTable getTaskList(int schema_id, int wo_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prTaskList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@wo_id", SqlDbType.Int).Value = wo_id;
                
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ds = new DataTable();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }


        public DataTable getTaskView(int schema_id, int task_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prTaskView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@task_id", SqlDbType.Int).Value = task_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ds = new DataTable();
                da.Fill(ds);
                return ds;
            }
            finally
            { 
                conn.Close();
            }
        }
    }
}
