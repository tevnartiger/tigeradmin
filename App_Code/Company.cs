using System;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Done by : Stanley Jocelyn
/// date    : sept 7 , 2007
/// 
/// </summary>
namespace tiger
{
    public class Company
    {
        private string str_conn;
        public Company(string str_conn)
        {
            this.str_conn = str_conn;
        }
        public DataTable getCompanyList(int schema_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prCompanyList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }



        public string getCompanyName(int schema_id , int tenant_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prCompanyName", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = tenant_id;
            string company_name = "";

            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                  company_name = dr["company_name"].ToString();
                }
            }

            finally 
            {
                conn.Close();

            }

            return company_name;
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public DataTable getCompanyLeaseList(int schema_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prCompanyLeaseList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }
        public DataTable getCompanyView(int schema_id,int company_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prCompanyView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@company_id", SqlDbType.Int).Value = company_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }

           
        }
        public string getCompanyLink(int Company_id)
        {
            string str = null;
            str += "<table><tr><td><a href='unit_view_info.aspx?tenant_id=" + Company_id + "'>Info</a></td>";
            str += "<td><a href='unit_view_info.aspx?unit_id=" + Company_id + "'>Service</a></td>";
            str += "<td><a href='unit_view_info.aspx?unit_id=" + Company_id + "'>Photo</a></td></table>";
            return str;
        }

     


        public int getCompanyFirstId(int schema_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prCompanyFirstId", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return Convert.ToInt32(cmd.Parameters["@tenant_id"].Value);
            }
            finally
            {
                conn.Close();
            }

        }



        public bool CompanyExist(int schema_id , string company_name)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prCompanyExist", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@company_name", SqlDbType.NVarChar , 50).Value = company_name;
                cmd.Parameters.Add("@exist", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);

                if (Convert.ToInt32(cmd.Parameters["@exist"].Value) == 1)
                    return true;
                else
                    return false;
            }
            finally
            {
                conn.Close();
            }

        }
    }
}
