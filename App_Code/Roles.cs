﻿using System;
using System.Data;
using System.Data.SqlClient;


/// <summary>
/// Summary description for Roles
/// </summary>
public class Roles
{
    private int return_id;
    private int out_schema_id;
    private string role_name;
    private string role_desc;
    private string role_status;

    public string Role_name
    {
        get { return role_name; }
        set { role_name = value; }
    }

    public string Role_desc
    {
        get { return role_desc; }
        set { role_desc = value; }
    }


    public string Role_status
    {
        get { return role_status; }
        set { role_status = value; }
    }


    public int Return_id
    {
        get { return return_id; }
        set { return_id = value; }
    }


    public int Role_out_schema_id
    {
        get { return out_schema_id; }
        set { out_schema_id = value; }
    }


	public Roles()
	{
		




	}



    public DataTable getRoleList(int schema_id,string lang, int num_record)
    {
         SqlConnection conn = new SqlConnection(Utils.CONN);
        SqlCommand cmd = new SqlCommand("prRoleList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
        cmd.Parameters.Add("@lang", SqlDbType.Char,2).Value = lang;
        cmd.Parameters.Add("@num_record", SqlDbType.Int).Value = num_record;

        try
        {
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();

            da.Fill(dt);

            return dt;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            conn.Close();
        }

    }

    /// <summary>
    /// Role liste excluding the Account owner
    /// </summary>
    /// <param name="schema_id"></param>
    /// <param name="lang"></param>
    /// <param name="num_record"></param>
    /// <returns></returns>
    public DataTable getRoleList2(int schema_id, string lang, int num_record)
    {
         SqlConnection conn = new SqlConnection(Utils.CONN);
        SqlCommand cmd = new SqlCommand("prRoleList2", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
        cmd.Parameters.Add("@lang", SqlDbType.Char, 2).Value = lang;
        cmd.Parameters.Add("@num_record", SqlDbType.Int).Value = num_record;

        try
        {
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();

            da.Fill(dt);

            return dt;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            conn.Close();
        }

    }



    public DataTable getRoleServices(int schema_id,int role_id)
    {
         SqlConnection conn = new SqlConnection(Utils.CONN);
        SqlCommand cmd = new SqlCommand("prRoleServices", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
        cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;
     
        try
        {
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();

            da.Fill(dt);

            return dt;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            conn.Close();
        }

    }



    public DataTable getTabCategServices(int schema_id, int role_id, string sc_code,string categ)
    {
        SqlConnection conn = new SqlConnection(Utils.CONN);
        SqlCommand cmd = new SqlCommand("prTabCategService", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
        cmd.Parameters.Add("@sc_code", SqlDbType.VarChar, 10).Value = sc_code;
        cmd.Parameters.Add("@categ", SqlDbType.VarChar, 10).Value = categ;

        try
        {
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();

            da.Fill(dt);

            return dt;
        }
        catch (Exception ex)
        {

            sinfoca.error.ErrorLog.addErrorLog(ex);
            return null;
        }
        finally
        {
            conn.Close();
        }

    }


    public DataTable getTabServices(int schema_id,int role_id, string sc_code)
    {
        SqlConnection conn = new SqlConnection(Utils.CONN);
        SqlCommand cmd = new SqlCommand("prTabService", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
        cmd.Parameters.Add("@sc_code", SqlDbType.VarChar,10).Value = sc_code;

        try
        {
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();

            da.Fill(dt);

            return dt;
        }
        catch (Exception ex)
        {

            sinfoca.error.ErrorLog.addErrorLog(ex);
            return null;
        }
        finally
        {
            conn.Close();
        }

    }


public DataTable getRolePersonServices(int schema_id,int role_id,string lang)
    {
         SqlConnection conn = new SqlConnection(Utils.CONN);
        SqlCommand cmd = new SqlCommand("prRolePersonServices", conn);
        cmd.CommandType = CommandType.StoredProcedure;

       cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;
       cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
       cmd.Parameters.Add("@lang", SqlDbType.VarChar,5).Value = lang;
       cmd.Parameters.Add("@zone_id", SqlDbType.Int).Value = 0;
       cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["name_id"].ToString()); ;
                     
     
        try
        {
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();

            da.Fill(dt);

            return dt;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            conn.Close();
        }

    }


public DataTable getRolePersonServices(int schema_id, int role_id,int zone_id)
{
    SqlConnection conn = new SqlConnection(Utils.CONN);
    SqlCommand cmd = new SqlCommand("prRolePersonServices", conn);
    cmd.CommandType = CommandType.StoredProcedure;

    cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;
    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
    cmd.Parameters.Add("@zone_id", SqlDbType.Int).Value = zone_id ;
    cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString()); ;


    try
    {
        conn.Open();

        SqlDataAdapter da = new SqlDataAdapter(cmd);

        DataTable dt = new DataTable();

        da.Fill(dt);

        return dt;
    }
    catch (Exception ex)
    {
        return null;
    }
    finally
    {
        conn.Close();
    }

}




    public void getRoleNameAndDesc(int schema_id, int role_id)
    {
         SqlConnection conn = new SqlConnection(Utils.CONN);
        SqlCommand cmd = new SqlCommand("prRoleNameAndDesc", conn);
        cmd.CommandType = CommandType.StoredProcedure;
      
        try
        {
            conn.Open();

            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;
            cmd.Parameters.Add("@role_name", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@role_desc", SqlDbType.VarChar, 200).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@role_status", SqlDbType.VarChar, 2).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@out_schema_id", SqlDbType.Int).Direction = ParameterDirection.Output;


            //execute the insert
            cmd.ExecuteReader();



         //   if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
            {
                //Return the values of the output params
                Role_name = Convert.ToString(cmd.Parameters["@role_name"].Value);
                Role_desc = Convert.ToString(cmd.Parameters["@role_desc"].Value);
                Role_status = Convert.ToString(cmd.Parameters["@role_status"].Value);
                Role_out_schema_id = Convert.ToInt32(cmd.Parameters["@out_schema_id"].Value);
            }
        }

        catch (Exception ex)
        {
          //  return null;
        }
        finally
        {
            conn.Close();
        }

    }

    public int getLeaguePublicRoleId(int schema_id)
    {
         SqlConnection conn = new SqlConnection(Utils.CONN);
        SqlCommand cmd = new SqlCommand("prLeaguePublicRoleId", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            //execute the insert
            cmd.ExecuteReader();
            //
            return Convert.ToInt32(cmd.Parameters["@role_id"].Value);
    
        }

        catch (Exception ex)
        {
            sinfoca.error.ErrorLog.addErrorLog(ex);
            return 0;
        }

        finally
        {
            conn.Close();
        }

    }


    /// <summary>
    /// mark multiple appointment as completed
    /// </summary>
    /// <param name="appointments"></param>
    public void setRoleServiceUpdate(int role_id,DataTable services)
    {
         SqlConnection conn = new SqlConnection(Utils.CONN);
        SqlCommand cmd = new SqlCommand("prRoleServiceUpdate", conn);
        cmd.CommandType = CommandType.StoredProcedure;

      //  try
        {
            conn.Open();
            //Add the params
            //return params
            cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            //Add the input params
            //Add the output params to the stor proc
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;
            cmd.Parameters.Add("@role_name", SqlDbType.VarChar, 50).Value = role_name;
            cmd.Parameters.Add("@role_desc", SqlDbType.VarChar, 200).Value = role_desc;
            cmd.Parameters.Add("@role_status", SqlDbType.VarChar, 2).Value = "AC";


            // the ID of the client
            cmd.Parameters.Add("@MyTable", SqlDbType.Structured).Value = services;
            cmd.Parameters.Add("@trace_user_id", SqlDbType.Int).Value = 12;// Convert.ToInt32(System.Web.HttpContext.Current.Session["user_id"].ToString());
            cmd.Parameters.Add("@trace_user_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();

            //execute the insert
            cmd.ExecuteReader();
            //

            Return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);

        }

       // catch (Exception ex)
        {
        //    sinfoca.error.ErrorLog.addErrorLog(ex);
        }
       // finally
        {
            conn.Close();
        }
    }




    /// <summary>
    /// mark multiple appointment as completed
    /// </summary>
    /// <param name="appointments"></param>
    public void setRoleServiceAdd( DataTable services, string role_name, string role_desc)
    {
         SqlConnection conn = new SqlConnection(Utils.CONN);
        SqlCommand cmd = new SqlCommand("prRoleServiceAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        //  try
        {
            conn.Open();
            //Add the params
            //return params
            cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            //Add the input params
            //Add the output params to the stor proc
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
            cmd.Parameters.Add("@role_name", SqlDbType.VarChar, 50).Value = role_name;
            cmd.Parameters.Add("@role_desc", SqlDbType.VarChar, 100).Value = role_desc;
            

            // the ID of the client
            cmd.Parameters.Add("@MyTable", SqlDbType.Structured).Value = services;
            cmd.Parameters.Add("@trace_user_id", SqlDbType.Int).Value = 999999;// Convert.ToInt32(System.Web.HttpContext.Current.Session["user_id"].ToString());
            cmd.Parameters.Add("@trace_user_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();

            //execute the insert
            cmd.ExecuteReader();
            //

            Return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);

        }

        // catch (Exception ex)
        {
         //       sinfoca.error.ErrorLog.addErrorLog(ex);
        }
       //  finally
        {
            conn.Close();
        }
    }

}