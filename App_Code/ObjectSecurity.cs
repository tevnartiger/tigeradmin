﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ObjectSecurity
/// </summary>
namespace sinfoca.tiger.security
{
    public static class ObjectSecurity
    {
       
        public static bool checkNameProfile(int schema_id, int name_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prObjectNameProfile", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
            conn.Close();

        }

    }
}