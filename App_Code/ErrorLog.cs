using System;
using System.Data;
using System.Web;
using System.Data.SqlClient;
using sinfoca.login;

/// <summary>
/// Summary description for ErrorLog
/// </summary>
namespace sinfoca.error
{
    public static class ErrorLog
    {
        public static void addErrorLog( Exception ex )
        {

            SqlConnection conn = new SqlConnection(Utils.ERRORLOG_CONN);
            SqlCommand cmd = new SqlCommand("prErrorLogAdd", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                cmd.Parameters.Add("@Application", SqlDbType.NVarChar, 50).Value = "Plongeur Express";
                cmd.Parameters.Add("@Host", SqlDbType.NVarChar, 50).Value = System.Web.HttpContext.Current.Request.Url.Authority.ToString();
                cmd.Parameters.Add("@Email_sent_to", SqlDbType.NVarChar, 500).Value = "jocestan@hotmail.com,stevemorisseau@hotmail.com";
                cmd.Parameters.Add("@name_ip", SqlDbType.NVarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();


                if (System.Web.HttpContext.Current.Session["user_id"] != null)
                    cmd.Parameters.Add("name_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["name_id"].ToString());
                else
                    cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = 0;
                
                cmd.Parameters.Add("@TimeUtc", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.Add("@Type", SqlDbType.NVarChar, 100).Value = ex.GetType().ToString();
                cmd.Parameters.Add("@UserAgent", SqlDbType.NVarChar, 100).Value = System.Web.HttpContext.Current.Request.UserAgent.ToString();
                cmd.Parameters.Add("@Source", SqlDbType.NVarChar, 60).Value = ex.Source.ToString();
                cmd.Parameters.Add("@Message", SqlDbType.NVarChar, 1000).Value = ex.Message.ToString();
                cmd.Parameters.Add("@TargerSite", SqlDbType.NVarChar, 500).Value = ex.TargetSite.ToString();
                cmd.Parameters.Add("@StackTrace", SqlDbType.NVarChar, 4000).Value = ex.StackTrace.ToString();


                if (System.Web.HttpContext.Current.Session["login_user"] != null)
                    cmd.Parameters.Add("@login_user", SqlDbType.NVarChar, 100).Value = System.Web.HttpContext.Current.Session["login_user"].ToString();
                else
                    cmd.Parameters.Add("@login_user", SqlDbType.NVarChar, 100).Value = "public";
                     
                cmd.Parameters.Add("@StatusCode", SqlDbType.Int).Value = Convert.ToInt32(HttpContext.Current.Response.StatusCode);
                //execute the insert
                cmd.ExecuteNonQuery();
               
                //Failure or Success (true = success (1), else fail) 
            }
            catch (Exception)
            {//do nothing 
            }
            finally 
            {
               conn.Close();

               
                 string content = "";
                content = content +"<span class='boldItalic'>Website Error Notification</span><br />";
                content = content +"<br />";
                content = content +"This is to notify you that an error has ocurred in a web application<br />";
                content = content +"<br />";
                content = content +"<span style='font-weight: bold;'>Application :ASMS+<br />";
                content = content + "Time :" + DateTime.Now.ToLongDateString() + " at " + DateTime.Now.ToLongTimeString()+ "<br />";
                content = content + "Host :&nbsp; " + System.Web.HttpContext.Current.Request.Url.Authority.ToString() + " <br />"; 
                content = content + "Type :" + ex.GetType().ToString() + "<br />";
                content = content + "StatusCode :" + HttpContext.Current.Response.StatusCode.ToString() +"<br />";
                content = content + "User Agent:" + System.Web.HttpContext.Current.Request.UserAgent.ToString()+ "<br />";
                content = content + "Source :" + ex.Source + "<br />";
                content = content + "<br />";
                if (HttpContext.Current.Session["login_user"] != null)
                {
                    content = content + "User Email :" + System.Web.HttpContext.Current.Session["login_user"].ToString() + "<br />";
                    content = content + "User IP :" + Convert.ToString(HttpContext.Current.Request.UserHostAddress) + "<br />";
                    content = content + "User ID :" + System.Web.HttpContext.Current.Session["name_id"].ToString() + "<br />";
                }
                content = content + "</span><br />";
                content = content +"&nbsp; <br />";
                content = content +"<span style='font-weight: bold;'>Message&nbsp;&nbsp; </span>:&nbsp;";;
                content = content + ex.Message +"&nbsp; <br /><br/>";
                content = content + "<span style='font-weight: bold;'>TargeTSite&nbsp;&nbsp; </span>:";
                content = content + ex.TargetSite +"&nbsp; <br /><br/>";
                content = content + "<span style='font-weight: bold;'>Stack Trace&nbsp;&nbsp; </span>:<br />"; ;
                content = content + ex.StackTrace;

              //  if(IsCritical(ex))
                 Email.sendSESMailHtml("error@sappointment.com", "jocestan@hotmail.com,stevemorisseau@hotmail.com", "Website Error Notification [ASMS+] ", content);
               
                System.Web.HttpContext.Current.Server.ClearError();
            }  
        }







        public static void addErrorLogGlobalAsax(Exception ex)
        {

            SqlConnection conn = new SqlConnection(Utils.ERRORLOG_CONN);
            SqlCommand cmd = new SqlCommand("prErrorLogAdd", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@Application", SqlDbType.NVarChar, 50).Value = "Appointment Global.asax Exception";
                cmd.Parameters.Add("@Host", SqlDbType.NVarChar, 50).Value = System.Web.HttpContext.Current.Request.Url.Authority.ToString();
                cmd.Parameters.Add("@Email_sent_to", SqlDbType.NVarChar, 500).Value = "jocestan@hotmail.com,stevemorisseau@hotmail.com";
                cmd.Parameters.Add("@user_ip", SqlDbType.NVarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();


                if (System.Web.HttpContext.Current.Session["user_id"] != null)
                    cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["user_id"].ToString());
                else
                    cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = 0;
                
                
                cmd.Parameters.Add("@UserAgent", SqlDbType.NVarChar, 100).Value = System.Web.HttpContext.Current.Request.UserAgent.ToString();
                cmd.Parameters.Add("@TimeUtc", SqlDbType.DateTime).Value = DateTime.Now;
                cmd.Parameters.Add("@Type", SqlDbType.NVarChar, 100).Value = ex.GetType().ToString();
                cmd.Parameters.Add("@Source", SqlDbType.NVarChar, 60).Value = ex.Source.ToString();
                cmd.Parameters.Add("@Message", SqlDbType.NVarChar, 1000).Value = ex.Message.ToString();
                cmd.Parameters.Add("@TargerSite", SqlDbType.NVarChar, 500).Value = ex.TargetSite.ToString();
                cmd.Parameters.Add("@StackTrace", SqlDbType.NVarChar, 4000).Value = ex.StackTrace.ToString();

                if (System.Web.HttpContext.Current.Session["login_user"] != null)
                    cmd.Parameters.Add("@login_user", SqlDbType.NVarChar, 100).Value = System.Web.HttpContext.Current.Session["login_user"].ToString();
                else
                    cmd.Parameters.Add("@login_user", SqlDbType.NVarChar, 100).Value = "public";
                
                cmd.Parameters.Add("@StatusCode", SqlDbType.Int).Value = Convert.ToInt32(HttpContext.Current.Response.StatusCode);
                //execute the insert
                cmd.ExecuteNonQuery();

                //Failure or Success (true = success (1), else fail) 
            }
            catch (Exception)
            {//do nothing 
            }
            finally
            {
                conn.Close();
                System.Web.HttpContext.Current.Server.ClearError();
            }
        }





        public static bool IsCritical(Exception ex)
        {
            if (ex is SqlException) return true;
            if (ex is IndexOutOfRangeException) return true;
            if (ex is NullReferenceException) return true;
            if (ex is ArgumentException) return true; 
            if (ex is OutOfMemoryException) return true;
            if (ex is AppDomainUnloadedException) return true;
            if (ex is BadImageFormatException) return true;
            if (ex is CannotUnloadAppDomainException) return true;
            if (ex is ExecutionEngineException) return true;
            if (ex is InvalidProgramException) return true;
            if (ex is System.Threading.ThreadAbortException)
                return true;
            return false;
        }


    }
}