using System;
using System.Data;
using System.Data.SqlClient;
using sinfoca.login;

/// <summary>
/// Summary description for Owner
/// </summary>
namespace tiger
{
    public class Owner
    {
        private string 	str_conn;
        private static int incidentCount;

        public Owner()
        {
        }

        public Owner(string str_conn)
        {
            this.str_conn = str_conn;
        }



        public DataTable getOwnerApplianceList(int schema_id, int home_id, int unit_id, int ac_id,int name_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prOwner_ApplianceList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;
                cmd.Parameters.Add("@ac_id", SqlDbType.Int).Value = ac_id;
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }



        public DataTable getOwnerRentDelequencyList(int schema_id, int home_id, int name_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prOwner_RentDelequencyList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }


        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable getOwnerIncidentList(int home_id, DateTime date_from, DateTime date_to,
                                              int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prOwner_IncidentList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@incidentCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["name_id"].ToString());
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@date_from", SqlDbType.DateTime).Value = date_from;
                cmd.Parameters.Add("@date_to", SqlDbType.DateTime).Value = date_to;

                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                incidentCount = Convert.ToInt32(cmd.Parameters["@incidentCount"].Value);


                return dt;
            }
            finally
            {
                conn.Close();
            }
        }


        public static int getTotalIncidentCount(int home_id, DateTime date_from, DateTime date_to)
        {
            return incidentCount;
        }



        public DataTable getOwnerApplianceSummaryList(int schema_id, int home_id, int ac_id, int name_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prOwner_ApplianceSummaryList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@ac_id", SqlDbType.Int).Value = ac_id;
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;




                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }



        public DataTable getOwnerApplianceStorageUnitList(int schema_id, int home_id, int ac_id,int name_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prApplianceStorageUnitList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@ac_id", SqlDbType.Int).Value = ac_id;
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }



        public DataTable getOwnerApplianceSearchList(int schema_id, int appliance_search_radio_value, string appliance_search, int name_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prOwner_ApplianceSearchList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@appliance_search_radio_value", SqlDbType.Int).Value = appliance_search_radio_value;
                cmd.Parameters.Add("@appliance_search", SqlDbType.VarChar, 50).Value = appliance_search;
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
                

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public DataTable getOwnerList(int schema_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prOwnerList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }

    public int addOwner(int schema_id, string owner_lname, string owner_fname,string owner_tel,
          string owner_cell, string owner_email, string owner_com ,int name_id, int name_insert_id , int name_id_ip)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prOwnerAdd", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
                cmd.Parameters.Add("@name_insert_id", SqlDbType.Int).Value = name_insert_id;
                cmd.Parameters.Add("@name_id_ip", SqlDbType.VarChar, 15).Value = name_id_ip;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@owner_lname", SqlDbType.VarChar, 50).Value = owner_lname;
                cmd.Parameters.Add("@owner_fname", SqlDbType.VarChar, 50).Value = owner_fname;
                cmd.Parameters.Add("@owner_tel", SqlDbType.VarChar, 50).Value = owner_tel;
                cmd.Parameters.Add("@owner_cell", SqlDbType.VarChar, 50).Value = owner_cell;
                cmd.Parameters.Add("@owner_email", SqlDbType.VarChar, 50).Value = owner_email;
                cmd.Parameters.Add("@owner_com", SqlDbType.VarChar, 50).Value = owner_com;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);

                return Convert.ToInt32(cmd.Parameters["@return"].Value);

            }
            finally
            {
                conn.Close();
            }
        }
        public string getOwnerLink(int owner_id, char user_lang)
        {
            string info, update, residence, photo, report; 

            if (user_lang == 'E')
            {
                info = "Info";
                update = "Update";
                residence = "Residence";
                photo = "Mortgage";
                report = "Report";
              

            }
            else
            {
                info = "Info";
                update = "Modifier";
                residence = "Résidence";
                photo = "Photo";
                report = "Rapport";
            }

            return "<a href='owner_view_info.aspx?owner_id=" + owner_id + "' class='sname'>" + info + "</a>&nbsp;&nbsp;<a href='home_update.aspx?owner_id=" + owner_id + "' class='sname'>" + update + "</a>&nbsp;&nbsp;<a href='home_residence.aspx?owner_id=" + owner_id + "' class='sname'>" + residence + "</a>&nbsp;&nbsp;&nbsp;<a href='home_photo.aspx?owner_id=" + owner_id + "' class='sname'>" + photo + "</a>&nbsp;&nbsp;&nbsp;<a href='home_report.aspx?owner_id=" + owner_id + "' class='sname'>" + report + "</a>";

        }


        public DataTable getOwnerHomeList(int schema_id, int name_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prOwner_HomeList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ds = new DataTable();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }



            public int getOwnerHomeCount(int schema_id, int name_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prOwner_HomeCount", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@tot", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return Convert.ToInt32(cmd.Parameters["@tot"].Value);
            }
            finally
            {
                conn.Close();
            }

        }

            public DataTable getOwnerWorkOrderList(int schema_id, int home_id, int wo_status, int wo_priority, DateTime date_from, DateTime date_to, int name_id)
            {
                SqlConnection conn = new SqlConnection(str_conn);
                SqlCommand cmd = new SqlCommand("prOwner_WorkOrderList", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    conn.Open();
                    //Add the params
                    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                    cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                    cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
                    cmd.Parameters.Add("@wo_status", SqlDbType.Int).Value = wo_status;
                    cmd.Parameters.Add("@wo_priority", SqlDbType.Int).Value = wo_priority;
                    cmd.Parameters.Add("@date_from", SqlDbType.DateTime).Value = date_from;
                    cmd.Parameters.Add("@date_to", SqlDbType.DateTime).Value = date_to;


                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    return dt;
                }
                finally
                {
                    conn.Close();
                }
            }
            public DataTable getOwnerRentPaymentArchive(int schema_id, int home_id, int unit_id, DateTime date_from, DateTime date_to,int name_id)
            {
                SqlConnection conn = new SqlConnection(str_conn);
                SqlCommand cmd = new SqlCommand("prOwner_RentPaymentArchive", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    conn.Open();
                    //Add the params
                    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                    cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                    cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;
                    cmd.Parameters.Add("@date_from", SqlDbType.DateTime).Value = date_from;
                    cmd.Parameters.Add("@date_to", SqlDbType.DateTime).Value = date_to;
                    cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
                    

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    return dt;
                }
                finally
                {
                    conn.Close();
                }
            }

            public int getOwnerHomeUnitAvailableCount(int schema_id, int name_id)
            {
                SqlConnection conn = new SqlConnection(str_conn);
                SqlCommand cmd = new SqlCommand("prOwner_HomeUnitAvailableCount", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    conn.Open();
                    //Add the params
                    cmd.Parameters.Add("@tot", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                    cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

                    //execute the insert
                    cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    return Convert.ToInt32(cmd.Parameters["@tot"].Value);
                }
                finally
                {
                    conn.Close();
                }

            }





            public int getOwnerHomeFirstId(int schema_id, int name_id)
            {
                SqlConnection conn = new SqlConnection(str_conn);
                SqlCommand cmd = new SqlCommand("prOwner_HomeFirstId", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    conn.Open();
                    //Add the params
                    cmd.Parameters.Add("@home_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                    cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

                    //execute the insert
                    cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    return Convert.ToInt32(cmd.Parameters["@home_id"].Value);
                }
                finally
                {
                    conn.Close();
                }




            }



            public int getOwnerHomeUnitAvailableFirstId(int schema_id, int name_id)
            {
                SqlConnection conn = new SqlConnection(str_conn);
                SqlCommand cmd = new SqlCommand("prOwner_HomeUnitAvailableFirstId", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    conn.Open();
                    //Add the params
                    cmd.Parameters.Add("@home_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                    cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

                    //execute the insert
                    cmd.ExecuteReader(CommandBehavior.CloseConnection);
                    return Convert.ToInt32(cmd.Parameters["@home_id"].Value);
                }
                finally
                {
                    conn.Close();
                }




            }



            public DataTable getOwnerHomeNumberOfUnitAvailable(int schema_id, int name_id)
            {
                SqlConnection conn = new SqlConnection(str_conn);
                SqlCommand cmd = new SqlCommand("prOwner_HomeNumberOfUnitAvailable", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    conn.Open();
                    //Add the params
                    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                    cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable ds = new DataTable();
                    da.Fill(ds);
                    return ds;
                }
                finally
                {
                    conn.Close();
                }
            }


    
    
    }
}