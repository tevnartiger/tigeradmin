using System;
using System.Data;
using System.Data.SqlClient;
using sinfoca.login;
/// <summary>
/// Summary description for janitor
/// </summary>
namespace tiger
{
    public class Janitor
    {
        private static int incidentCount;

        private string 	str_conn;

        public Janitor()
        {
        }


        public Janitor(string str_conn)
        {
            this.str_conn = str_conn;
        }

        public DataSet getJanitorList(int schema_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prJanitorList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }

        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable getJanitorIncidentList(int home_id, DateTime date_from,
                          DateTime date_to,  int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prJanitor_IncidentList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@incidentCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["name_id"].ToString());
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@date_from", SqlDbType.DateTime).Value = date_from;
                cmd.Parameters.Add("@date_to", SqlDbType.DateTime).Value = date_to;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                incidentCount = Convert.ToInt32(cmd.Parameters["@incidentCount"].Value);


                return dt;
            }
            finally
            {
                conn.Close();
            }
        }



        public static int getTotalIncidentCount(int home_id, DateTime date_from, DateTime date_to)
        {
            return incidentCount;
        }


        public DataTable getJanitorWorkOrderList(int schema_id, int home_id, int wo_status, int wo_priority, DateTime date_from, DateTime date_to, int name_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prJanitor_WorkOrderList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
                cmd.Parameters.Add("@wo_status", SqlDbType.Int).Value = wo_status;
                cmd.Parameters.Add("@wo_priority", SqlDbType.Int).Value = wo_priority;
                cmd.Parameters.Add("@date_from", SqlDbType.DateTime).Value = date_from;
                cmd.Parameters.Add("@date_to", SqlDbType.DateTime).Value = date_to;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }

        public DataTable getJanitorRentDelequencyList(int schema_id, int home_id, int name_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prJanitor_RentDelequencyList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }


    public int addJanitor(int schema_id, string janitor_lname, string janitor_fname,string janitor_tel,
          string janitor_cell, string janitor_email, string janitor_com)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prJanitorAdd", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@janitor_lname", SqlDbType.VarChar, 50).Value = janitor_lname;
                cmd.Parameters.Add("@janitor_fname", SqlDbType.VarChar, 50).Value = janitor_fname;
                cmd.Parameters.Add("@janitor_tel", SqlDbType.VarChar, 50).Value = janitor_tel;
                cmd.Parameters.Add("@janitor_cell", SqlDbType.VarChar, 50).Value = janitor_cell;
                cmd.Parameters.Add("@janitor_email", SqlDbType.VarChar, 50).Value = janitor_email;
                cmd.Parameters.Add("@janitor_com", SqlDbType.VarChar, 50).Value = janitor_com;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);

                return Convert.ToInt32(cmd.Parameters["@return"].Value);

            }
            finally
            {
                conn.Close();
            }




        }






    public DataTable getJanitorApplianceList(int schema_id, int home_id, int unit_id, int ac_id, int name_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prJanitor_ApplianceList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;
            cmd.Parameters.Add("@ac_id", SqlDbType.Int).Value = ac_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;


            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        finally
        {
            conn.Close();
        }
    }




    public DataTable getJanitorApplianceSummaryList(int schema_id, int home_id, int ac_id, int name_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prJanitor_ApplianceSummaryList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd.Parameters.Add("@ac_id", SqlDbType.Int).Value = ac_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;




            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        finally
        {
            conn.Close();
        }
    }



    public DataTable getJanitorApplianceStorageUnitList(int schema_id, int home_id, int ac_id, int name_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prApplianceStorageUnitList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd.Parameters.Add("@ac_id", SqlDbType.Int).Value = ac_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        finally
        {
            conn.Close();
        }
    }



    public DataTable getJanitorApplianceSearchList(int schema_id, int appliance_search_radio_value, string appliance_search, int name_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prJanitor_ApplianceSearchList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@appliance_search_radio_value", SqlDbType.Int).Value = appliance_search_radio_value;
            cmd.Parameters.Add("@appliance_search", SqlDbType.VarChar, 50).Value = appliance_search;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;


            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        finally
        {
            conn.Close();
        }
    }
   
    public DataTable getJanitorHomeList(int schema_id, int name_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prJanitor_HomeList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }
    }



    public int getJanitorHomeCount(int schema_id, int name_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prJanitor_HomeCount", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@tot", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Convert.ToInt32(cmd.Parameters["@tot"].Value);
        }
        finally
        {
            conn.Close();
        }

    }



    public int getJanitorHomeUnitAvailableCount(int schema_id, int name_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prJanitor_HomeUnitAvailableCount", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@tot", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Convert.ToInt32(cmd.Parameters["@tot"].Value);
        }
        finally
        {
            conn.Close();
        }

    }





    public int getJanitorHomeFirstId(int schema_id, int name_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prJanitor_HomeFirstId", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Convert.ToInt32(cmd.Parameters["@home_id"].Value);
        }
        finally
        {
            conn.Close();
        }




    }



    public int getJanitorHomeUnitAvailableFirstId(int schema_id, int name_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prJanitor_HomeUnitAvailableFirstId", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Convert.ToInt32(cmd.Parameters["@home_id"].Value);
        }
        finally
        {
            conn.Close();
        }




    }



    public DataTable getJanitorHomeNumberOfUnitAvailable(int schema_id, int name_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prJanitor_HomeNumberOfUnitAvailable", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }
    }

    }
}