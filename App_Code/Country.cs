using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Country
/// </summary>
namespace tiger
{
    public class Country
    {
        private string str_conn;
        public Country(string str_conn)
        {
            this.str_conn = str_conn;
        }

        public DataSet getCountryList()
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prCountryList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                //cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }
    }
}