using System;
using System.Data;
using System.Data.SqlClient;

namespace Briefcase
{
	public class Files
	{
		public static int Create(string FileName,byte[] FileData,int FileSize,int FolderId,DateTime DateCreated,
                int schema_id , int name_id, int trace_name_id, string trace_name_id_ip)
		{
			SqlParameter[] p=new SqlParameter[9];
			p[0]=new SqlParameter("@FileName",FileName);
			p[1]=new SqlParameter("@FileData",FileData);
			p[2]=new SqlParameter("@FileSize",FileSize);
			p[3]=new SqlParameter("@FolderId",FolderId);
			p[4]=new SqlParameter("@DateCreated",DateCreated);
            p[5]=new SqlParameter("@schema_id",schema_id);
            p[6] = new SqlParameter("@name_id", name_id);
            p[7] = new SqlParameter("@trace_name_id", trace_name_id);
            p[8] = new SqlParameter("@trace_name_id_ip", trace_name_id_ip);

			return SqlHelper.ExecuteNonQuery("prFiles_Create",p);
		}
        /// <summary>
        /// create files to be view by prperty owners , created by management
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="FileData"></param>
        /// <param name="FileSize"></param>
        /// <param name="DateCreated"></param>
        /// <param name="schema_id"></param>
        /// <param name="name_id"></param>
        /// <param name="name_id_ip"></param>
        /// <param name="home_id"></param>
        /// <param name="number_of_insert"></param>
        public void OwnerCreate(string FileName, byte[] FileData, int FileSize, DateTime DateCreated, int schema_id, int trace_name_id , string trace_name_id_ip ,string home_id , int number_of_insert)
        {
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

                SqlConnection conn = new SqlConnection(strconn);
                SqlCommand cmd = new SqlCommand("prOwnerFiles_Create", conn);
                cmd.CommandType = CommandType.StoredProcedure;

              //  try
                {
                    conn.Open();

                    cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 255).Value = FileName ;
                    cmd.Parameters.Add("@FileData", SqlDbType.Image).Value = FileData;
                    cmd.Parameters.Add("@FileSize", SqlDbType.Int).Value = FileSize;
                    cmd.Parameters.Add("@DateCreated", SqlDbType.DateTime).Value = DateCreated;
                    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                    cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = trace_name_id;
                    cmd.Parameters.Add("@home_id", SqlDbType.VarChar, 8000).Value = home_id;
                    cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;
                    cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = trace_name_id_ip;
  

                    //execute the insert
                    cmd.ExecuteReader();
                  
                }
              //  catch (Exception error)
                {
                    //   tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
                    //Response.Redirect("home_main.aspx");
                    //  error.ToString();
                    // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
                }

        }
        /// <summary>
        /// create files to be view by tenant , created by management
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="FileData"></param>
        /// <param name="FileSize"></param>
        /// <param name="DateCreated"></param>
        /// <param name="schema_id"></param>
        /// <param name="name_id"></param>
        /// <param name="home_id"></param>
        public void TenantCreate(string FileName, byte[] FileData, int FileSize, DateTime DateCreated, int schema_id, int trace_name_id,  string trace_name_id_ip, string home_id, int number_of_insert)
        {
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prTenantFiles_Create", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //  try
            {
                conn.Open();

                cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 255).Value = FileName;
                cmd.Parameters.Add("@FileData", SqlDbType.Image).Value = FileData;
                cmd.Parameters.Add("@FileSize", SqlDbType.Int).Value = FileSize;
                cmd.Parameters.Add("@DateCreated", SqlDbType.DateTime).Value = DateCreated;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = trace_name_id;
                cmd.Parameters.Add("@home_id", SqlDbType.VarChar , 8000).Value = home_id;
                cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = trace_name_id_ip;
  
                //execute the insert
                cmd.ExecuteReader();

            }
            //  catch (Exception error)
            {
                //   tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
                //Response.Redirect("home_main.aspx");
                //  error.ToString();
                // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
            }

        }


        /// <summary>
        /// create files to be view by tenant , created by management
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="FileData"></param>
        /// <param name="FileSize"></param>
        /// <param name="DateCreated"></param>
        /// <param name="schema_id"></param>
        /// <param name="name_id"></param>
        /// <param name="home_id"></param>
        public void PMCreate(string FileName, byte[] FileData, int FileSize, DateTime DateCreated, int schema_id, int trace_name_id, string trace_name_id_ip, string home_id, int number_of_insert)
        {
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prPMFiles_Create", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //  try
            {
                conn.Open();

                cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 255).Value = FileName;
                cmd.Parameters.Add("@FileData", SqlDbType.Image).Value = FileData;
                cmd.Parameters.Add("@FileSize", SqlDbType.Int).Value = FileSize;
                cmd.Parameters.Add("@DateCreated", SqlDbType.DateTime).Value = DateCreated;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = trace_name_id;
                cmd.Parameters.Add("@home_id", SqlDbType.VarChar, 8000).Value = home_id;
                cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = trace_name_id_ip;

                //execute the insert
                cmd.ExecuteReader();

            }
            //  catch (Exception error)
            {
                //   tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
                //Response.Redirect("home_main.aspx");
                //  error.ToString();
                // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
            }

        }



        /// <summary>
        /// create files to be view by tenant , created by management
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="FileData"></param>
        /// <param name="FileSize"></param>
        /// <param name="DateCreated"></param>
        /// <param name="schema_id"></param>
        /// <param name="name_id"></param>
        /// <param name="home_id"></param>
        public void JanitorCreate(string FileName, byte[] FileData, int FileSize, DateTime DateCreated, int schema_id, int trace_name_id, string trace_name_id_ip, string home_id, int number_of_insert)
        {
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prJanitorFiles_Create", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //  try
            {
                conn.Open();

                cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 255).Value = FileName;
                cmd.Parameters.Add("@FileData", SqlDbType.Image).Value = FileData;
                cmd.Parameters.Add("@FileSize", SqlDbType.Int).Value = FileSize;
                cmd.Parameters.Add("@DateCreated", SqlDbType.DateTime).Value = DateCreated;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = trace_name_id;
                cmd.Parameters.Add("@home_id", SqlDbType.VarChar, 8000).Value = home_id;
                cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = trace_name_id_ip;

                //execute the insert
                cmd.ExecuteReader();

            }
            //  catch (Exception error)
            {
                //   tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
                //Response.Redirect("home_main.aspx");
                //  error.ToString();
                // result.InnerHtml = "<font color='red'>An error as occured!</font>" + error.ToString();
            }

        }


 		public static int Delete(int Id, int schema_id , int name_id)
		{
			SqlParameter[] p=new SqlParameter[3];
			p[0]=new SqlParameter("@Id",Id);
            p[1]=new SqlParameter("@schema_id", schema_id);
            p[2] = new SqlParameter("@name_id", name_id);
       			return SqlHelper.ExecuteNonQuery("prFiles_Delete",p);
		}


        /// <summary>
        /// delete  property owners files, created by management
        /// </summary>
        /// <param name="ownerfiles_id"></param>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public static int OwnerDelete(int ownerfiles_id, int schema_id , int home_id)
        {
            SqlParameter[] p = new SqlParameter[3];
            p[0] = new SqlParameter("@ownerfiles_id", ownerfiles_id);
            p[1] = new SqlParameter("@schema_id", schema_id);
            p[2] = new SqlParameter("@home_id", home_id);
            return SqlHelper.ExecuteNonQuery("prFiles_OwnerDelete", p);
        }

        /// <summary>
        /// delete tenant files, created by management
        /// </summary>
        /// <param name="tenantfiles_id"></param>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public static int TenantDelete(int tenantfiles_id, int schema_id , int home_id)
        {
            SqlParameter[] p = new SqlParameter[3];
            p[0] = new SqlParameter("@tenantfiles_id", tenantfiles_id);
            p[1] = new SqlParameter("@schema_id", schema_id);
            p[2] = new SqlParameter("@home_id", home_id);
            return SqlHelper.ExecuteNonQuery("prFiles_TenantDelete", p);
        }


        /// <summary>
        /// delete property manager files, created by management
        /// </summary>
        /// <param name="pmfiles_id"></param>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public static int PMDelete(int tenantfiles_id, int schema_id, int home_id)
        {
            SqlParameter[] p = new SqlParameter[3];
            p[0] = new SqlParameter("@pmfiles_id", tenantfiles_id);
            p[1] = new SqlParameter("@schema_id", schema_id);
            p[2] = new SqlParameter("@home_id", home_id);
            return SqlHelper.ExecuteNonQuery("prFiles_PMDelete", p);
        }


        /// <summary>
        /// delete janitor files, created by management
        /// </summary>
        /// <param name="janitorfiles_id"></param>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public static int JanitorDelete(int tenantfiles_id, int schema_id, int home_id)
        {
            SqlParameter[] p = new SqlParameter[3];
            p[0] = new SqlParameter("@janitorfiles_id", tenantfiles_id);
            p[1] = new SqlParameter("@schema_id", schema_id);
            p[2] = new SqlParameter("@home_id", home_id);
            return SqlHelper.ExecuteNonQuery("prFiles_JanitorDelete", p);
        }



        public static int DeleteFromFolder(int folderid ,int schema_id , int name_id)
        {
            SqlParameter[] p = new SqlParameter[3];
            p[0] = new SqlParameter("@folderid", folderid);
            p[1]=new SqlParameter("@schema_id",schema_id);
            p[2] = new SqlParameter("@name_id", name_id);
            return SqlHelper.ExecuteNonQuery("prFiles_DeleteFromFolder", p);
        }

        public static DataTable GetFile(int Id , int schema_id, int name_id)
        {
            SqlParameter[] p = new SqlParameter[3];
            p[0] = new SqlParameter("@Id", Id);
            p[1]=new SqlParameter("@schema_id",schema_id);
            p[2] = new SqlParameter("@name_id", name_id);
            DataSet ds = SqlHelper.ExecuteDataSet("prFiles_GetFile", p);
            return ds.Tables[0];

        }

        /// <summary>
        /// get property a owners files, created by management
        /// </summary>
        /// <param name="ownerfiles_id"></param>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public static DataTable OwnerGetFile(int ownerfiles_id, int schema_id)
        {
            SqlParameter[] p = new SqlParameter[2];
            p[0] = new SqlParameter("@ownerfile_id", ownerfiles_id);
            p[1] = new SqlParameter("@schema_id", schema_id);
            DataSet ds = SqlHelper.ExecuteDataSet("prOwnerFiles_GetFile", p);
            return ds.Tables[0];

        }

        /// <summary>
        /// get a tenant files, created by management
        /// </summary>
        /// <param name="tenantfiles_id"></param>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public static DataTable TenantGetFile(int tenantfiles_id, int schema_id)
        {
            SqlParameter[] p = new SqlParameter[2];
            p[0] = new SqlParameter("@tenantfile_id", tenantfiles_id);
            p[1] = new SqlParameter("@schema_id", schema_id);
            DataSet ds = SqlHelper.ExecuteDataSet("prTenantFiles_GetFile", p);
            return ds.Tables[0];

        }


        /// <summary>
        /// get a property manager files, created by management
        /// </summary>
        /// <param name="tenantfiles_id"></param>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public static DataTable PMGetFile(int pmfiles_id, int schema_id)
        {
            SqlParameter[] p = new SqlParameter[2];
            p[0] = new SqlParameter("@pmfile_id", pmfiles_id);
            p[1] = new SqlParameter("@schema_id", schema_id);
            DataSet ds = SqlHelper.ExecuteDataSet("prPMFiles_GetFile", p);
            return ds.Tables[0];

        }

        /// <summary>
        /// get a janitor files, created by management
        /// </summary>
        /// <param name="tenantfiles_id"></param>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public static DataTable JanitorGetFile(int janitorfiles_id, int schema_id)
        {
            SqlParameter[] p = new SqlParameter[2];
            p[0] = new SqlParameter("@janitorfile_id", janitorfiles_id);
            p[1] = new SqlParameter("@schema_id", schema_id);
            DataSet ds = SqlHelper.ExecuteDataSet("prJanitorFiles_GetFile", p);
            return ds.Tables[0];

        }

        public static DataTable GetFilesFromFolder(int FolderId , int schema_id ,int name_id)
        {
            SqlParameter[] p = new SqlParameter[3];
            p[0] = new SqlParameter("@FolderId", FolderId);
            p[1]=new SqlParameter("@schema_id",schema_id);
            p[2] = new SqlParameter("@name_id", name_id);
            DataSet ds = SqlHelper.ExecuteDataSet("prFiles_GetFromFolder", p);
            return ds.Tables[0];

        }



        public static DataTable GetFilesForOwner(int home_id, int schema_id)
        {
            string str_conn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prFiles_GetForOwner", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ds = new DataTable();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public static DataTable GetFilesForTenant(int home_id, int schema_id)
        {
            string str_conn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prFiles_GetForTenant", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ds = new DataTable();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }


        }


        public static DataTable GetTenantFileList(int schema_id, int name_id)
        {
            string str_conn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prTenant_FileList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ds = new DataTable();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }


        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public static DataTable GetFilesForPM(int home_id, int schema_id)
        {
            string str_conn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prFiles_GetForPM", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ds = new DataTable();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }


        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public static DataTable GetFilesForJanitor(int home_id, int schema_id)
        {
            string str_conn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prFiles_GetForJanitor", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ds = new DataTable();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }


        }

        public static int Rename(int Id, string filename, int schema_id, int trace_name_id, string trace_name_id_ip)
        {
            SqlParameter[] p = new SqlParameter[5];
            p[0] = new SqlParameter("@id", Id);
            p[1] = new SqlParameter("@filename", filename);
            p[2]=new SqlParameter("@schema_id",schema_id);
            p[3] = new SqlParameter("@trace_name_id", trace_name_id);
            p[4] = new SqlParameter("@trace_name_id_ip", trace_name_id_ip);
            return SqlHelper.ExecuteNonQuery("prFiles_Rename", p);
        }



        public static int OwnerRename(int ownerfiles_id, string filename, int schema_id , int trace_name_id, string trace_name_id_ip)
        {
            SqlParameter[] p = new SqlParameter[5];
            p[0] = new SqlParameter("@ownerfiles_id", ownerfiles_id);
            p[1] = new SqlParameter("@filename", filename);
            p[2] = new SqlParameter("@schema_id", schema_id);
            p[3] = new SqlParameter("@trace_name_id", trace_name_id);
            p[4] = new SqlParameter("@trace_name_id_ip", trace_name_id_ip);
            return SqlHelper.ExecuteNonQuery("prFiles_OwnerRename", p);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tenantfiles_id"></param>
        /// <param name="filename"></param>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public static int TenantRename(int tenantfiles_id, string filename, int schema_id, int trace_name_id
                                        , string trace_name_id_ip)
        {
            SqlParameter[] p = new SqlParameter[5];
            p[0] = new SqlParameter("@tenantfiles_id", tenantfiles_id);
            p[1] = new SqlParameter("@filename", filename);
            p[2] = new SqlParameter("@schema_id", schema_id);
            p[3] = new SqlParameter("@trace_name_id", trace_name_id);
            p[4] = new SqlParameter("@trace_name_id_ip", trace_name_id_ip);
            return SqlHelper.ExecuteNonQuery("prFiles_TenantRename", p);
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="pmfiles_id"></param>
        /// <param name="filename"></param>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public static int PMRename(int tenantfiles_id, string filename, int schema_id, int trace_name_id
                                        , string trace_name_id_ip)
        {
            SqlParameter[] p = new SqlParameter[5];
            p[0] = new SqlParameter("@pmfiles_id", tenantfiles_id);
            p[1] = new SqlParameter("@filename", filename);
            p[2] = new SqlParameter("@schema_id", schema_id);
            p[3] = new SqlParameter("@trace_name_id", trace_name_id);
            p[4] = new SqlParameter("@trace_name_id_ip", trace_name_id_ip);
            return SqlHelper.ExecuteNonQuery("prFiles_PMRename", p);
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="pmfiles_id"></param>
        /// <param name="filename"></param>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public static int JanitorRename(int tenantfiles_id, string filename, int schema_id, int trace_name_id
                                        , string trace_name_id_ip)
        {
            SqlParameter[] p = new SqlParameter[5];
            p[0] = new SqlParameter("@janitorfiles_id", tenantfiles_id);
            p[1] = new SqlParameter("@filename", filename);
            p[2] = new SqlParameter("@schema_id", schema_id);
            p[3] = new SqlParameter("@trace_name_id", trace_name_id);
            p[4] = new SqlParameter("@trace_name_id_ip", trace_name_id_ip);
            return SqlHelper.ExecuteNonQuery("prFiles_JanitorRename", p);
        }
	}
}
