using System;
/// <summary>
/// Summary description for LabelStudList
/// </summary>
namespace tiger
{
    namespace label
    {
        public class LabelHome
        {
            private char lang;
            public LabelHome(char lang)
            {
                this.lang = lang;
            }
           
            public string getLabel(string categ)
            {
                string tempname = "";
                if (lang == 'F')
                {
                    switch (categ)
                    {
                        case "lbl_home_id": tempname = "#";
                            break;
                        case "lbl_txt_home_title_update": tempname = "Modifier r�sidence";
                            break;
                        
                        case "lbl_home_name": tempname = "R�sidence";
                            break;
                        case "lbl_home_date_purchase": tempname = "Date d'achat";
                            break;
                        case "lbl_home_buying_price": tempname = "Prix";
                            break;
                        case "lbl_home_desc": tempname = "Description";
                            break;
                        case "lbl_home_addr": tempname = "Adresse d'exp�dition";
                            break;
                        case "lbl_home_pc": tempname = "Code postal";
                            break;
                        case "lbl_home_city": tempname = "Ville";
                            break;
                        case "lbl_home_district": tempname = "District";
                            break;
                        case "lbl_home_prov": tempname = "Province";
                            break;
                        case "lbl_home_com": tempname = "Commentaire";
                            break;
                        case "lbl_unit_tenants": tempname = "Locataires";
                            break;




                    }
                }
                else
                {
                    switch (categ)
                    {
                        case "lbl_home_id": tempname = "#";
                            break;
                        case "lbl_home_title_update": tempname = "Update residence";
                            break;
                        case "lbl_home_name": tempname = "Residence";
                            break;
                        case "lbl_home_date_purchase": tempname = "Purchase date";
                            break;
                        case "lbl_home_buying_price": tempname = "Price";
                            break;
                        case "lbl_home_desc": tempname = "Desc.";
                            break;
                        case "lbl_home_addr": tempname = "Mailing";
                            break;
                        case "lbl_home_pc": tempname = "Postal code";
                            break;
                        case "lbl_home_city": tempname = "City";
                            break;
                        case "lbl_home_district": tempname = "District";
                            break;
                        case "lbl_home_prov": tempname = "Province";
                            break;
                        case "lbl_home_com": tempname = "Comment";
                            break;
                        case "lbl_unit_tenants": tempname = "Tenants";
                            break;
                    }
                }
                return tempname;

            }
        }
    }



}
