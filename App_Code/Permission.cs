﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;
using sinfoca.tiger.security;



/// <summary>
/// Summary description for Permission
/// </summary>
  public static class Permission
    {

    public static bool hasValidNumericQueryString(string requestquery, HttpRequest q)
    {
        bool response = true;
        NameValueCollection n = q.QueryString;

        List<string> queryStringList = new List<string>();
        queryStringList.Add("shift_id");
        queryStringList.Add("amount");
        queryStringList.Add("order_id");
        queryStringList.Add("wp_id");
        queryStringList.Add("person_id");
        queryStringList.Add("status_id");
        queryStringList.Add("srv_id");
        queryStringList.Add("sc_id");
        queryStringList.Add("role_id");
        queryStringList.Add("z_id");
        queryStringList.Add("rg_id");
        queryStringList.Add("cy_id");
        queryStringList.Add("company_id");
        queryStringList.Add("jcz_id");
        queryStringList.Add("tr_id");
        queryStringList.Add("fc_id");
        queryStringList.Add("jz_id");
        queryStringList.Add("jc_id");
        queryStringList.Add("inv_id");
        response = true;

        if (!string.IsNullOrEmpty(requestquery) && n.HasKeys())
        {
            for (int i = 0; i < n.Count; i++)
            {
                if (queryStringList.Contains(n.GetKey(i).ToString()))
                {
                    if (!RegEx.IsInteger(n.Get(i)))
                    {
                        response = false;
                    }
                    break;
                }
            }
        }
        return response;

    }


    public static bool hasValidAlphaNumericQueryString(string requestquery, HttpRequest q)
    {
        bool response = true;
        NameValueCollection n = q.QueryString;

        List<string> queryStringList = new List<string>();
        queryStringList.Add("e");
        queryStringList.Add("k");

        response = true;

        if (!string.IsNullOrEmpty(requestquery) && n.HasKeys())
        {
            for (int i = 0; i < n.Count; i++)
            {
                if (queryStringList.Contains(n.GetKey(i).ToString()))
                {
                    if (!RegEx.IsAlphaNumeric(n.Get(i)))
                    {
                        response = false;
                    }
                    break;
                }
            }
        }
        return response;

    }


    public static bool hasValidGuidQueryString(string requestquery, HttpRequest q)
    {
        bool response = true;
        NameValueCollection n = q.QueryString;

        List<string> queryStringList = new List<string>();
        queryStringList.Add("id");

        response = true;

        if (!string.IsNullOrEmpty(requestquery) && n.HasKeys())
        {
            for (int i = 0; i < n.Count; i++)
            {
                if (queryStringList.Contains(n.GetKey(i).ToString()))
                {
                    if (!RegEx.IsGuid(n.Get(i)))
                    {
                        response = false;
                    }
                    break;
                }
            }
        }
        return response;

    }


    public static bool hasValidAlphaQueryString(string requestquery, HttpRequest q)
    {

        bool response = true;
        NameValueCollection n = q.QueryString;
        List<string> queryStringList = new List<string>();
        queryStringList.Add("l");
        queryStringList.Add("p");
        queryStringList.Add("c");
        queryStringList.Add("mtk");

        List<string> queryValues = new List<string>();
        queryValues.Add("upd");
        queryValues.Add("updhe");
        queryValues.Add("catupd");
        queryValues.Add("list");
        queryValues.Add("view");
        queryValues.Add("cityupd");
        queryValues.Add("add");
        queryValues.Add("cityadd");
        queryValues.Add("dwview");
        queryValues.Add("schview");
        queryValues.Add("catadd");
        queryValues.Add("cityadd");
        queryValues.Add("pay");
        queryValues.Add("staffupdate");
        queryValues.Add("companylist");
        queryValues.Add("modify");
        queryValues.Add("view");
        queryValues.Add("addjc");
        queryValues.Add("listjc");
        queryValues.Add("updjc");
        queryValues.Add("talentpaymentmethod");



        if (n.HasKeys())
        {
            for (int i = 0; i < n.Count; i++)
            {
                if (queryStringList.Contains(n.GetKey(i).ToString()))
                {
                    if (!RegEx.IsAlphaCustomValue(n.Get(i)) /*|| !queryValues.Contains(n.GetKey(i).ToString())*/) // Allow "_" character, the underscore
                    {
                        response = false;
                    }
                    break;
                }
            }
        }
        return response;
    }

    public static bool hasPathAccess(string is_manager,string is_pm,
                                         string is_janitor,string is_tenant,
                                         string is_owner , string path)
        {
            bool response = false;
            switch (path)
            {
               

                case "/manager":
                   if (is_pm == "0" && is_janitor == "0" && is_tenant == "0" && is_owner == "0")
                   {
                       response = true;
                   }
                    break;


                case "/user":
                case "/user/default":
                    if (is_pm == "1" || is_janitor == "1" || is_tenant == "1" || is_owner == "1")
                    {
                        response = true;
                    }
                    break;
               
                case "/user/owner":
                    if (is_owner == "1")
                    {
                        response = true;
                    }
                    break;

                case "/user/pm":
                    if (is_pm == "1")
                    {
                        response = true;
                    }
                    break;

                case "/user/janitor":
                    if (is_janitor == "1")
                    {
                        response = true;
                    }
                    break;

                case "/user/tenant":
                    if (is_tenant == "1")
                    {
                        response = true;
                    }
                    break;
              

                default: return false;
                    
            }

            return response;
            
        }
       

                public static bool hasValidQueryStringPermission(string role_id,string filename,string requestquery,int schema_id,int user_id, HttpRequest q)
                {
                    bool response = true;
                    NameValueCollection n = q.QueryString;


                   List<string> pageList = new List<string>();

                   pageList.Add("appliance_image.aspx");
                   pageList.Add("appliance_invoice.aspx");
                   pageList.Add("appliance_update.aspx");
                   pageList.Add("appliance_view.aspx");
                    
         
                   pageList.Add("company_update.aspx");
                   pageList.Add("company_view.aspx");
                    
                   pageList.Add("contact_list.aspx");
                   
                   pageList.Add("event_update.aspx");
         
                   pageList.Add("financial_analysis_period_expense_graph.aspx");
                   pageList.Add("financial_analysis_period_income_graph.aspx"); 
         
                   pageList.Add("financial_analysis_period_view.aspx");       
                   pageList.Add("financial_analysis_period_update.aspx"); 
                   pageList.Add("financial_expenses.aspx.aspx");
                   pageList.Add("financial_expenses_graph.aspx");
                   pageList.Add("financial_expenses_update.aspx");
          
                   pageList.Add("financial_group_expenses_graph.aspx");       
                   pageList.Add("financial_group_analysis_period_income_graph.aspx"); 
                   pageList.Add("financial_group_analysis_period_expense_graph.aspx");
                   pageList.Add("financial_group_income_graph.aspx");
                   pageList.Add("financial_income.aspx");
                   pageList.Add("financial_income_graph.aspx");
                   pageList.Add("financial_income_update.aspx");
          
          
                   pageList.Add("financial_institution_update.aspx");
                   pageList.Add("financial_institution_view.aspx");
           
                   pageList.Add("group_profile.aspx");  //group_id
                   pageList.Add("group_modify.aspx"); 
          
          
          
                   pageList.Add("incident_wo_add.aspx");  //inc
                   pageList.Add("incident_view.aspx");  //inc_id
                   pageList.Add("incident_update.aspx");  //inc_id
          
          
                   pageList.Add("insurance_company_update.aspx");  //inc_id
                   pageList.Add("insurance_company_view.aspx");  //inc_id
           
          
                   pageList.Add("insurance_policy_archive_view.aspx");  
                   pageList.Add("insurance_policy_view.aspx");  
                   pageList.Add("insurance_policy_update.aspx");  
                   pageList.Add("insurance_policy_renew.aspx");  
                   pageList.Add("insurance_policy_pending_view.aspx");  
                   pageList.Add("insurance_policy_expiration.aspx");  
                   pageList.Add("insurance_policy_list.aspx");  
          
          
          
                   pageList.Add("lease_view.aspx");  
                   pageList.Add("lease_termsandconditions_archive_view.aspx");  
                   pageList.Add("lease_res_old_add_1_tmp.aspx");  
                   pageList.Add("lease_list.aspx");  
                   pageList.Add("lease_expiration.aspx");  
                   pageList.Add("lease_com_old_add_1_tmp.aspx");  
                   pageList.Add("lease_archive_view.aspx");  
                   pageList.Add("lease_accommodation_archive_view.aspx");  
          
          
                   pageList.Add("mortgage_archive_view.aspx");  
                   pageList.Add("mortgage_pending_view.aspx");  
                   pageList.Add("mortgage_renew.aspx");  
                   pageList.Add("mortgage_update.aspx");  
                   pageList.Add("mortgage_view.aspx");  
                   pageList.Add("mortgage_expiration.aspx");  
                   pageList.Add("mortgage_list.aspx");  
                  
           
                   pageList.Add("notice_delequency_send.aspx");  
                  
                   pageList.Add("portfolio_profile.aspx");  
          
          
                   pageList.Add("wiz_property_add.aspx");  
                   pageList.Add("property_view.aspx ");  
                   pageList.Add("property_update.aspx");  
                   pageList.Add("property_unit_list.aspx");  
                   pageList.Add("property_unit_add.aspx");  
                   pageList.Add("property_add_2.aspx");  
                   
          
                   pageList.Add("role_member_list.aspx");  
                   
          
                   pageList.Add("supplier_update.aspx");  //inc_id
                   pageList.Add("supplier_view.aspx");  //inc_id
            
          
          
                   pageList.Add("tenant_view.aspx");  
                   pageList.Add("tenant_update.aspx");  
                   pageList.Add("tenant_untreated_rent.aspx");  
                   pageList.Add("tenant_search.aspx");  
                   pageList.Add("tenant_rent_update.aspx");  
                   pageList.Add("tenant_prospect_view.aspx");  
                   pageList.Add("tenant_prospect_update.aspx");  
                   pageList.Add("tenant_inactive_view.aspx");  
                   pageList.Add("tenant_cancel_rent_payment.aspx");  
         
          
          
                   pageList.Add("warehouse_update.aspx");  
                   pageList.Add("warehouse_view.aspx");  
                    
          
                   pageList.Add("wo_update.aspx");  
                   pageList.Add("wo_view.aspx");  
         
                   
        
        
        /* 
                    if(pageList.Contains(filename))
                    {
                        response = false;
                        switch (filename)
                        {

                   
                            case "contact_list.aspx":
                            case "user_default.aspx":
                                if (requestquery.Contains("?categ=") && requestquery.Contains("&l="))
                                {
                                    if (n.HasKeys())
                                    {
                                        if (n.GetKey(0).ToString() == "categ"
                                                 && n.GetKey(1).ToString() == "l")
                                        {
                                            if (RegEx.IsInteger(n.Get(0))
                                                && RegEx.IsAlpha(n.Get(1)))
                                            {
                                                response = true;
                                            }
                                        }

                                    }

                                }

                                break;

                          case "contact_update.aspx":
                          case "contact_view.aspx":
                                if (requestquery.Contains("?contact_id="))
                                {
                                    if (n.HasKeys())
                                    {
                                        if (n.GetKey(0).ToString() == "contact_id")
                                        {
                                            if (RegEx.IsInteger(n.Get(0)))
                                            {
                                                response = sinfoca.tiger.security.AccountObjectAuthorization.Contact(schema_id, Convert.ToInt32(n.Get(0)));
                                            }
                                        }

                                    }

                                }

                              break;

                             case "user_update.aspx": 
                              if (requestquery.Contains("?user_id="))
                              {
                                  if (n.HasKeys())
                                  {
                                      if (n.GetKey(0).ToString() == "user_id")
                                      {
                                          if (RegEx.IsInteger(n.Get(0)))
                                          {
                                            if (role_id == "1")
                                              response = sinfoca.tiger.security.AccountObjectAuthorization.User(schema_id, Convert.ToInt32(n.Get(0)));
                                 
                                          }
                                      }

                                  }

                              }

                              break;


                            case "wo_view.aspx":
                            case "wo_update.aspx":
                              if (requestquery.Contains("?wo_id=") && requestquery.Contains("&inc="))
                              {
                                  if (n.HasKeys())
                                  {
                                      if (n.GetKey(0).ToString() == "wo_id"
                                               && n.GetKey(1).ToString() == "inc")
                                      {
                                          if (RegEx.IsInteger(n.Get(0))
                                              && RegEx.IsInteger(n.Get(1)))
                                          {
                                              if (role_id == "1" || role_id == "2")
                                              response = sinfoca.tiger.security.AccountObjectAuthorization.WorkOrder(schema_id, Convert.ToInt32(n.Get(0)));
                                   
                                              if (role_id == "3")
                                                response = sinfoca.tiger.security.UserAccountObjectAuthorization.WorkOrder(schema_id,user_id, Convert.ToInt32(n.Get(0)));
                                  
                                  
                                          }
                                      }

                                  }

                              }
                             break;

                                
                    
                             


                        }
                }
                    */
                    return response;

                }

        
    }
