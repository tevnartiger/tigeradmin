using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Cryptography;
///<summary>
///Summary description for Sec
///</summary>
namespace sinfoca.tiger.security
{

public static class Hash
{

    public static string createSaltUser(string login_user,int length)
    {
        char[] chars = "$@!*abcdefghijklmnopqrstuvwxyz1234567890;:ABCDEFGHIJKLMNOPQRSTUVWXYZ^".ToCharArray();
        string password = string.Empty;
        Random random = new Random();
        for (int i = 0; i < length; i++)
        {
            int x = random.Next(1, chars.Length);
            //Don't Allow Repetation of Characters
            if (!password.Contains(chars.GetValue(x).ToString()))
                password += chars.GetValue(x);
            else
                i--;
        }
        return password;    
 
    }

public static string createSalt(int length)
{
    char[] chars = "$@!*abcdefghijklmnopqrstuvwxyz1234567890;:ABCDEFGHIJKLMNOPQRSTUVWXYZ^".ToCharArray();
    string password = string.Empty;
    Random random = new Random();
    for (int i = 0; i < length; i++)
    {
        int x = random.Next(1, chars.Length);
        //Don't Allow Repetation of Characters
        if (!password.Contains(chars.GetValue(x).ToString()))
            password += chars.GetValue(x);
        else
            i--;
    }
    return password;    
}

public static string createPasswordHash(string pwd,string salt)
{

string saltAndPwd = String.Concat(pwd, salt);
string hashedPwd = FormsAuthentication.HashPasswordForStoringInConfigFile(saltAndPwd,"sha1");
return hashedPwd;
}

public static bool accessGranted(string pwd, string salt, string hash)
{ 
bool granted = false;
string temp_hash = createPasswordHash(pwd, salt);
if (temp_hash == hash)
{
    granted = true;
}
 
    return granted;
}

}

}