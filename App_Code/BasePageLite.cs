﻿using System;
using System.Data;
using System.Globalization;
using System.Configuration;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.IO;
using System.IO.Compression;


/// <summary>
/// Summary description for BasePageFreeMember
/// </summary>
public class BasePageLite : Page
{
    public const string DEFAULT_CULTURE = "en";
    public const string SECONDARY_CULTURE = "fr";
 public static string EMAIL_FROM = "admin@sinfoca.info";
    public static string EMAIL_FROM_DISPLAYNAME = "Administrator";

    public static string CURRENT_LANGUAGE;
    protected string current_filename;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (Page != null)
        {
            Page.RegisterRequiresViewStateEncryption();
        }
    }

    public event CultureChanged OnCultureChanged;

    
    protected override void OnError(EventArgs e)
    {
        // At this point we have information about the error
        HttpContext ctx = HttpContext.Current;

        Exception ex = ctx.Server.GetLastError();
        Exception baseex = ctx.Server.GetLastError().GetBaseException();

        sinfoca.error.ErrorLog.addErrorLog(baseex);
        ctx.Server.ClearError();
        Response.Redirect("~/default.aspx");


        base.OnError(e);
    }



    public string LastCultureName
    {
        get
        {
            string lastCultureName = (string)Session["_lastCulture"];

            if (lastCultureName == null)
            {
                Session["_lastCulture"] = Thread.CurrentThread.CurrentCulture.Name;
                lastCultureName = Thread.CurrentThread.CurrentCulture.Name;
            }

            return lastCultureName;
        }
        set
        {
            Session["_lastCulture"] = value;
        }
    }




    protected override void InitializeCulture()
    {
      
        if (Session["name_id"] == null)
        {
            Session.Abandon();
            Response.Redirect("~/sessionended.aspx");
        }

        if (Session["name_ip"] != null)
        {
            if (Session["name_ip"].ToString() != Request.UserHostAddress.ToString())
            {
                Session.Abandon();
                Response.Redirect("~/login.aspx");
            }
            else
            {
                // Response.Redirect("~/login.aspx", true);
            }
        }

        if (Session["user-agent-win32-win36"] != null)
        {
            if (Session["user-agent-win32-win36"].ToString() != Request.UserAgent + Request.Browser.Win16.ToString() + Request.Browser.Win32.ToString() + Request.AcceptTypes.ToString() + Request.IsSecureConnection.ToString() + Request.Browser.Browser.ToString() + Request.Browser.Version + Request.UserLanguages)
            {
                // ATTENTION ICI ON A AFFAIRE A SOIT UN VOL DE SESSION COOCKIE SOIT QUE
                // L'USAGER A ETE DECONNECTER ET RECONNECTER A INTERNET
                Session.Abandon();
                Response.Redirect("~/login.aspx");
            }
            else
            {
                // Response.Redirect("~/login.aspx", true);
            }
        }


       // try
      /*  {
            if (Permission.hasValidNumericQueryString(Request.Url.Query, Request) == false ||
                 Permission.hasValidAlphaNumericQueryString(Request.Url.Query, Request) == false ||
                 Permission.hasValidAlphaQueryString(Request.Url.Query, Request) == false
                )
            {
                Session.Abandon();
                Response.Redirect("~/login.aspx");
            }
        }
       

        Security s = new Security();
        string currentfile = System.IO.Path.GetFileName(Request.Path.ToString());
   
        if (Request.Url.PathAndQuery.Contains("schedule.aspx?status_id") || Request.Url.PathAndQuery.Contains("/schedule/schedule.aspx?p=close")
             || Request.Url.PathAndQuery.Contains("/c/validate_adm/validate.aspx?p=upd")
             || Request.Url.PathAndQuery.Contains("/persons/person.aspx")
             || Request.Url.PathAndQuery.Contains("/c/orders_wpm/order.aspx")
             || Request.Url.PathAndQuery.Contains("/persons/person_add.aspx"))
        {
            string theurl = Request.Url.PathAndQuery;
            if (Request.Url.PathAndQuery.Contains("/schedule/schedule.aspx?p=close"))
            {
                theurl = "/c/schedule/schedule.aspx?status_id=9";
            }

            if (Request.Url.PathAndQuery.Contains("/c/validate_adm/validate.aspx?p=upd"))
            {
                theurl = "/c/schedule/schedule.aspx?status_id=6";
            }

            if (Request.Url.PathAndQuery.Contains("/c/orders_wpm/order.aspx"))
            {
                theurl = "/c/orders_wpm/order.aspx?p=add_bv2";
            }

            if (Request.Url.PathAndQuery.Contains("/persons/person.aspx") 
                || Request.Url.PathAndQuery.Contains("/persons/person_add.aspx"))
            {
                theurl = "/persons/person_default.aspx";
            }

            if (!s.roleServiceValid(Convert.ToInt32(Session["role_id"]), theurl))
            {
                 Session.Abandon();
                 Response.Redirect("~/login.aspx");                       
            }
        }
        else 
        {
          if(!Request.Url.PathAndQuery.Contains("/persons/profile.aspx"))
            if (!s.roleServiceValid(Convert.ToInt32(Session["role_id"]), Request.Path))
            {
                 Session.Abandon();
                 Response.Redirect("~/login.aspx");                       
            }
        } */

        
        

        //TODO: make this prettier
        string lang = Request.QueryString["language"];
       /* switch(lang)
        {
            case "fr":
            case "fr-ca":           
        lang = "fr-CA";
        break;
            case "fr-fr":
        lang = "fr-FR";
        break;
            case  "en":
            case  "en-ca":
        lang = "en-CA";
        break;
            default:
        lang = "fr-CA";
        break;
        }*/

       
        if (lang == null || lang == String.Empty)
            lang = LastCultureName;

        if (!string.IsNullOrEmpty(Request.QueryString["language"]) && !config.queryLangList().Contains(Request.QueryString["language"].Substring(0, 2)))
            lang = Request.QueryString["language"];

       

        try
        {

            Thread.CurrentThread.CurrentCulture = new CultureInfo(lang);
            Thread.CurrentThread.CurrentCulture.DateTimeFormat = new CultureInfo(lang, false).DateTimeFormat;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(lang);
        }
        catch (Exception ex)
        {

            Thread.CurrentThread.CurrentCulture = new CultureInfo(config.fulldefault_lang);
            Thread.CurrentThread.CurrentCulture.DateTimeFormat = new CultureInfo(config.fulldefault_lang, false).DateTimeFormat;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(config.fulldefault_lang);
            sinfoca.error.ErrorLog.addErrorLog(ex);
        }
       // Session["_lastCulture"]= lang;
    }

    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);

        if (LastCultureName != Thread.CurrentThread.CurrentCulture.Name)
        {
            LastCultureName = Thread.CurrentThread.CurrentCulture.Name;

            if (this.OnCultureChanged != null)
                this.OnCultureChanged(this, LastCultureName);
        }
    }



    public static class CompressViewState
    {
        public static byte[] Compress(byte[] data)
        {
            MemoryStream output = new MemoryStream();
            GZipStream gzip = new GZipStream(output,
                              CompressionMode.Compress, true);
            gzip.Write(data, 0, data.Length);
            gzip.Close();
            return output.ToArray();
        }

        public static byte[] Decompress(byte[] data)
        {
            MemoryStream input = new MemoryStream();
            input.Write(data, 0, data.Length);
            input.Position = 0;
            GZipStream gzip = new GZipStream(input,
                              CompressionMode.Decompress, true);
            MemoryStream output = new MemoryStream();
            byte[] buff = new byte[64];
            int read = -1;
            read = gzip.Read(buff, 0, buff.Length);
            while (read > 0)
            {
                output.Write(buff, 0, read);
                read = gzip.Read(buff, 0, buff.Length);
            }
            gzip.Close();
            return output.ToArray();
        }
    }



    private ObjectStateFormatter _formatter =
        new ObjectStateFormatter();

    protected override void
        SavePageStateToPersistenceMedium(object viewState)
    {
        MemoryStream ms = new MemoryStream();
        _formatter.Serialize(ms, viewState);
        byte[] viewStateArray = ms.ToArray();
        ClientScript.RegisterHiddenField("__COMPRESSEDVIEWSTATE",
            Convert.ToBase64String(
            CompressViewState.Compress(viewStateArray)));
    }
    protected override object
        LoadPageStateFromPersistenceMedium()
    {
        string vsString = Request.Form["__COMPRESSEDVIEWSTATE"];
        byte[] bytes = Convert.FromBase64String(vsString);
        bytes = CompressViewState.Decompress(bytes);
        return _formatter.Deserialize(
            Convert.ToBase64String(bytes));
    }

}