using System;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for Access
/// </summary>

namespace tiger
{
    namespace security
    {
        public class Error
        {

            public static void errorAdd(string str_conn, int schema_id, DateTime error_date, string error_page, string error_desc )
            {

              
                SqlConnection conn = new SqlConnection(str_conn);
                SqlCommand cmd = new SqlCommand("prErrorAdd", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    conn.Open();
                    //Add the params
                    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                    cmd.Parameters.Add("@error_date", SqlDbType.DateTime).Value = error_date;
                    cmd.Parameters.Add("@error_page", SqlDbType.VarChar, 50).Value = error_page;
                    cmd.Parameters.Add("@error_desc", SqlDbType.VarChar, 50).Value = error_desc; 

                    //execute the insert
                    cmd.ExecuteReader(CommandBehavior.CloseConnection);
                }
                catch (Exception error)
                {
                    error.ToString();
                }



            }

                 }
    }
}

 