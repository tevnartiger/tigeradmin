using System;
using System.Data;
/// <summary>
/// Summary description for Access
/// </summary>
/// /// <summary>
/// Done by              : Stanley Jocelyn
/// date                 : sept 12 , 2007
///
namespace tiger
{
    namespace security
    {
        public class Access
        {

            public static bool hasAccess(string page_security, int group_index, int schema_id)
            {

                if ((schema_id > 0) && (Convert.ToInt32(page_security.Substring(group_index,1)) == 1)) 
                    return true;
                else
                    return false;
           }

            public static string toLoginPage()
            {
                return "/tiger/default.aspx";
            }
        }
    }
}

 