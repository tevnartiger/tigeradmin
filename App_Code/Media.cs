﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.ComponentModel;

/*
using System.Collections.Generic;
using System.Linq;
using System.Web;
*/

/// <summary>
/// Summary description for Object
/// </summary>
namespace tiger
{
    public class Media
    {
        private string str_conn;

        public Media(string str_conn)
        {
            this.str_conn = str_conn;
        }

        public int HomeImageUpdate(byte[] object_media, int schema_id,int home_id, int name_id  ,string name_id_ip)
        {
            // categ 1 : home image
            // categ 2 : appliance image
            // categ 3 : name image ( user )

            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prHomeImageUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

          try
           {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 20).Value = name_id_ip;
            cmd.Parameters.Add("@object_categ_id", SqlDbType.Int).Value = home_id;
            cmd.Parameters.Add("@object_categ", SqlDbType.VarChar, 20).Value = "home";
            cmd.Parameters.Add("@object_media", SqlDbType.Image).Value = object_media;
            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            return Convert.ToInt32(cmd.Parameters["@return"].Value);
            //view image

            }
            catch (Exception )
              {
                return 4;
            }
        }


        


        /// <summary>
        /// 
        /// </summary>
        /// <param name="object_media"></param>
        /// <param name="schema_id"></param>
        /// <param name="name_id"></param>
        /// <param name="name_id_ip"></param>
        /// <returns></returns>
        public int ApplianceImageUpdate(byte[] object_media, int schema_id,int appliance_id, int name_id, string name_id_ip)
        {
            // categ 1 : home image
            // categ 2 : appliance image
            // categ 3 : name image ( user )

            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prApplianceImageUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = name_id;
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = name_id_ip;
                cmd.Parameters.Add("@object_categ_id", SqlDbType.Int).Value = appliance_id ;
                cmd.Parameters.Add("@object_categ", SqlDbType.VarChar, 20).Value = "appliance";
                cmd.Parameters.Add("@object_media", SqlDbType.Image).Value = object_media;
                //execute the insert
                cmd.ExecuteReader();
                //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

                return Convert.ToInt32(cmd.Parameters["@return"].Value);
                //view image

            }
           catch (Exception)
            {
               return 4;
            }
        }

        public int ApplianceInvoiceImageUpdate(byte[] object_media, int schema_id, int appliance_id, int name_id, string name_id_ip)
        {
            // categ 1 : home image
            // categ 2 : appliance image
            // categ 3 : name image ( user )

            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prApplianceInvoiceImageUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = name_id;
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = name_id_ip;
                cmd.Parameters.Add("@object_categ_id", SqlDbType.Int).Value = appliance_id;
                cmd.Parameters.Add("@object_categ", SqlDbType.VarChar, 20).Value = "applianceinvoice";
                cmd.Parameters.Add("@object_media", SqlDbType.Image).Value = object_media;
                //execute the insert
                cmd.ExecuteReader();
                //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

                return Convert.ToInt32(cmd.Parameters["@return"].Value);
                //view image

            }
            catch (Exception)
            {
                return 0;
            }
        }


        public int NameImageUpdate(byte[] object_media, int schema_id,int name_insert_id , int name_id, string name_id_ip)
        {
            // categ 1 : home image
            // categ 2 : appliance image
            // categ 3 : name image ( user )

            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prNameImageUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = name_id;
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = name_id_ip;
                cmd.Parameters.Add("@object_categ_id", SqlDbType.Int).Value = name_insert_id ;
                cmd.Parameters.Add("@object_categ", SqlDbType.VarChar, 20).Value = "name";
                cmd.Parameters.Add("@object_media", SqlDbType.Image).Value = object_media;
                //execute the insert
                cmd.ExecuteReader();
                //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

                return Convert.ToInt32(cmd.Parameters["@return"].Value);
                //view image

            }
            catch (Exception)
            {
                return 0;
            }
        }
        // Convert byte array to Bitmap (byte[] to Bitmap)
        protected Bitmap ConvertToBitmap(byte[] bmp)
        {
            if (bmp != null)
            {
                TypeConverter tc = TypeDescriptor.GetConverter(typeof(Bitmap));
                Bitmap b = (Bitmap)tc.ConvertFrom(bmp);
                return b;
            }
            return null;
        }



     

        /*
        public byte[] ShowAlbumImage(int picid)
        {
            string conn = ConfigurationManager.ConnectionStrings["PictureAlbumConnectionString"].ConnectionString;
            SqlConnection connection = new SqlConnection(conn);
            string sql = "SELECT pic FROM Album WHERE Pic_ID = @ID";
            SqlCommand cmd = new SqlCommand(sql, connection);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@ID", picid);
            try
            {
                connection.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    seq = dr.GetBytes(0, 0, null, 0, int.MaxValue) - 1;
                    empPic = new byte[seq + 1];
                    dr.GetBytes(0, 0, empPic, 0, Convert.ToInt32(seq));
                    connection.Close();
                }

                return empPic;
            }

            catch
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
        }
 


        public byte[] getNameInfo(int schema_id, int object_categ_id, object_categ) 
        {
             SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prImageView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

          try
           {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@object_categ_id", SqlDbType.Int).Value = object_categ_id;
            cmd.Parameters.Add("@object_categ", SqlDbType.Char, 2).Value = object_categ;
           // cmd.Parameters.Add("@object_media", SqlDbType.Image). = ParameterDirection.Output;
            //execute the insert
            cmd.ExecuteReader();
            //  Response.Redirect("unit_add.aspx?home_id=" + Convert.ToString(cmd.Parameters["@return_home_id"].Value));

            ) cmd.Parameters["@object_media"].Value ;
            //view image

            }
            catch (Exception )
              {
                return null;
            }
            return null;
        }
 */
    }
}