﻿using System;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Done by : Stanley Jocelyn
/// date    : may 28 , 2008
/// 
/// </summary>
namespace tiger
{
    public class Warehouse
    {
        private string str_conn;
        public Warehouse(string str_conn)
        {
            this.str_conn = str_conn;
        }


        /// <summary>
        /// list of all the warehouses
        /// </summary>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public DataSet getWarehouseList(int schema_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prWarehouseList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="Warehouse_id"></param>
        /// <returns></returns>
        public DataTable getWarehouseView(int schema_id, int warehouse_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prWarehouseView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@warehouse_id", SqlDbType.Int).Value = warehouse_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public int getWarehouseFirstId(int schema_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prWarehouseFirstId", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return Convert.ToInt32(cmd.Parameters["@tenant_id"].Value);
            }
            finally
            {
                conn.Close();
            }




        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="Warehouse_id"></param>
        /// <returns></returns>
        public DataSet getWarehouseSearchList(int schema_id, int warehouse_search_radio_value, string warehouse_search)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prWarehouseSearchList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@warehouse_search_radio_value", SqlDbType.Int).Value = warehouse_search_radio_value;
                cmd.Parameters.Add("@warehouse_search", SqlDbType.VarChar, 50).Value = warehouse_search;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }
    }
}

