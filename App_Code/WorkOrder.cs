﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Rent
/// Done by : Stanley Jocelyn
/// date    : march 6 , 2009
/// </summary>
/// 
namespace tiger
{
    public class WorkOrder
    {
        private string str_conn;
        public WorkOrder(string str_conn)
        {
            this.str_conn = str_conn;
            SqlConnection conn = new SqlConnection(str_conn);
            //
            // TODO: Add constructor logic here
            //
        }


        public DataTable getWorkOrderList(int schema_id, int home_id,int wo_status,int wo_priority, DateTime date_from, DateTime date_to)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prWorkOrderList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@wo_status", SqlDbType.Int).Value = wo_status;
                cmd.Parameters.Add("@wo_priority", SqlDbType.Int).Value = wo_priority;
                cmd.Parameters.Add("@date_from", SqlDbType.DateTime).Value = date_from;
                cmd.Parameters.Add("@date_to", SqlDbType.DateTime).Value = date_to;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }

    }
}


