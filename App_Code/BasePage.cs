﻿using System;
using System.Data;
using System.Globalization;
using System.Configuration;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.IO;
using System.IO.Compression;
using sinfoca.tiger.security;

//public delegate void CultureChanged(object Sender, string CurrentCulture);
public class BasePage : Page
{

    public const string DEFAULT_CULTURE = "en";
    public const string SECONDARY_CULTURE = "fr";

    // public static string FULLSITE_BASEPATH = WebConfigurationManager.AppSettings["FullSitePath"];
    // public static string MEMBERS_BASEPATH = WebConfigurationManager.AppSettings["MemberSectionPath"];
    public static string EMAIL_FROM = "admin@sinfoca.info";
    public static string EMAIL_FROM_DISPLAYNAME = "Administrator";

    public static string CURRENT_LANGUAGE;
    protected string current_filename;

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        if (Page != null)
        {
            Page.RegisterRequiresViewStateEncryption();
        }
     }

    public event CultureChanged OnCultureChanged;


    public string LastCultureName
    {
        get
        {
            string lastCultureName = (string)Session["_lastCulture"];

            if (lastCultureName == null)
            {
                Session["_lastCulture"] = Thread.CurrentThread.CurrentCulture.Name;
                lastCultureName = Thread.CurrentThread.CurrentCulture.Name;
            }

            return lastCultureName;
        }
        set
        {
            Session["_lastCulture"] = value;
        }
    }


    

    protected override void InitializeCulture()
    {

        if (Session["name_id"] == null)
        {
            Session.Abandon();
            Response.Redirect("~/login.aspx", true);
        }
       
        if (Session["name_id_ip"] != null)
        {
            if (Session["name_id_ip"].ToString() != Request.UserHostAddress.ToString())
            {
                // ATTENTION ICI ON A AFFAIRE A SOIT UN VOL DE SESSION COOCKIE SOIT QUE
                // L'USAGER A ETE DECONNECTER ET RECONNECTER A INTERNET

                Session.Abandon();
                Response.Redirect("http://www.yahoo.com", true);


            }
            else
            {
                // Response.Redirect("https://sinfoca.info/login.aspx?l=f", true);

            }
        }


        if (Session["user-agent-win32-win36"] != null)
        {
            if (Session["user-agent-win32-win36"].ToString() != Request.UserAgent + Request.Browser.Win16.ToString() + Request.Browser.Win32.ToString() + Request.AcceptTypes.ToString() + Request.IsSecureConnection.ToString() + Request.Browser.Browser.ToString() + Request.Browser.Version + Request.UserLanguages)
            {
                // ATTENTION ICI ON A AFFAIRE A SOIT UN VOL DE SESSION COOCKIE SOIT QUE
                // L'USAGER A ETE DECONNECTER ET RECONNECTER A INTERNET

                Session.Abandon();
                Response.Redirect("http://www.google.com", true);


            }
            else
            {
                // Response.Redirect("https://sinfoca.info/login.aspx?l=f", true);

            }
        }


       string curDir = Page.TemplateSourceDirectory.ToString();
        //       test                            production
       if (curDir.StartsWith("/sinfo/user/owner") || curDir.StartsWith("/user/owner")) // OWNER MODULE ACCESS
       {
           curDir = "/user/owner";
       }

       if (curDir.StartsWith("/sinfo/user/default") || curDir.StartsWith("/user/default")) // everybody roles ACCESS
       {
           curDir = "/user/default";
       }

       if (curDir.StartsWith("/sinfo/manager") || curDir.StartsWith("/manager")) // OWNER MODULE ACCESS
       {
           curDir = "/manager";
       }

         if (curDir.StartsWith("/sinfo/user/pm") || curDir.StartsWith("/user/pm"))  // MANAGER MODULE ACCESS
         {
             curDir = "/user/pm";
         }

         if (curDir.StartsWith("/sinfo/user/janitor") || curDir.StartsWith("/user/janitor"))  // MANAGER MODULE ACCESS
         {
             curDir = "/user/janitor";
         }

         if (curDir.StartsWith("/sinfo/user/tenant") && curDir.StartsWith("/user/tenant"))  // MANAGER MODULE ACCESS
         {
             curDir = "/user/tenant";
         }


 
/*
    if (!Permission.hasPathAccess(Session["is_manager"].ToString(), Session["is_pm"].ToString(),
                                  Session["is_janitor"].ToString(), Session["is_tenant"].ToString(),
                                  Session["is_owner"].ToString(), curDir))
    {
        Session.Abandon();
        Response.Redirect("~/login.aspx", true);
    }

 
*/     //--------------       ACCESS CONTROL  END   -----------------------------------------------//

 




        //TODO: make this prettier
        string lang = Request["ctl00$Language"];

        if (lang == null || lang == String.Empty)
            lang = LastCultureName;

        Thread.CurrentThread.CurrentCulture = new CultureInfo(lang);
        //  Thread.CurrentThread.CurrentCulture.DateTimeFormat = new CultureInfo(lang, false).DateTimeFormat;
        Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(lang);
    }

    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);

        if (LastCultureName != Thread.CurrentThread.CurrentCulture.Name)
        {
            LastCultureName = Thread.CurrentThread.CurrentCulture.Name;

            if (this.OnCultureChanged != null)
                this.OnCultureChanged(this, LastCultureName);
        }
    }

    //--- TO COMPREESS AND DECOMPRESS VIEWSTATE
    /*
     protected override void
     SavePageStateToPersistenceMedium(object state)
     {
         LosFormatter formatter = new LosFormatter();
         StringWriter writer = new StringWriter();
         formatter.Serialize(writer, state);
         string viewState = writer.ToString();
         byte[] data = Convert.FromBase64String(viewState);
         byte[] compressedData = ViewStateHelper.Compress(data);
         string str = Convert.ToBase64String(compressedData);
         ClientScript.RegisterHiddenField("__MYVIEWSTATE", str);
     }

     protected override object
       LoadPageStateFromPersistenceMedium()
     {
         string viewstate = Request.Form["__MYVIEWSTATE"];
         byte[] data = Convert.FromBase64String(viewstate);
         byte[] uncompressedData =
         ViewStateHelper.Decompress(data);
         string str = Convert.ToBase64String
         (uncompressedData);
         LosFormatter formatter = new LosFormatter();
         return formatter.Deserialize(str);
     }
     */


    public static class CompressViewState
    {
        public static byte[] Compress(byte[] data)
        {
            MemoryStream output = new MemoryStream();
            GZipStream gzip = new GZipStream(output,
                              CompressionMode.Compress, true);
            gzip.Write(data, 0, data.Length);
            gzip.Close();
            return output.ToArray();
        }

        public static byte[] Decompress(byte[] data)
        {
            MemoryStream input = new MemoryStream();
            input.Write(data, 0, data.Length);
            input.Position = 0;
            GZipStream gzip = new GZipStream(input,
                              CompressionMode.Decompress, true);
            MemoryStream output = new MemoryStream();
            byte[] buff = new byte[64];
            int read = -1;
            read = gzip.Read(buff, 0, buff.Length);
            while (read > 0)
            {
                output.Write(buff, 0, read);
                read = gzip.Read(buff, 0, buff.Length);
            }
            gzip.Close();
            return output.ToArray();
        }
    }



    private ObjectStateFormatter _formatter =
        new ObjectStateFormatter();

    protected override void
        SavePageStateToPersistenceMedium(object viewState)
    {
        MemoryStream ms = new MemoryStream();
        _formatter.Serialize(ms, viewState);
        byte[] viewStateArray = ms.ToArray();
        ClientScript.RegisterHiddenField("__COMPRESSEDVIEWSTATE",
            Convert.ToBase64String(
            CompressViewState.Compress(viewStateArray)));
    }
    protected override object
        LoadPageStateFromPersistenceMedium()
    {
        string vsString = Request.Form["__COMPRESSEDVIEWSTATE"];
        byte[] bytes = Convert.FromBase64String(vsString);
        bytes = CompressViewState.Decompress(bytes);
        return _formatter.Deserialize(
            Convert.ToBase64String(bytes));
    }

}




