using System;
using System.Data;
using System.Data.SqlClient;

namespace Briefcase
{
	public class Folders
	{
		public static int Create(string FolderName,int ParentFolderId,DateTime DateCreated, int schema_id
                                  , int name_id, int name_id_insert, string name_id_ip)
		{
			SqlParameter[] p=new SqlParameter[7];
			p[0]=new SqlParameter("@FolderName",FolderName);
			p[1]=new SqlParameter("@ParentFolderId",ParentFolderId);
			p[2]=new SqlParameter("@DateCreated",DateCreated);
            p[3]=new SqlParameter("@schema_id",schema_id);
            p[4] = new SqlParameter("@name_id", name_id);
            p[5] = new SqlParameter("@trace_name_id", name_id_insert);
            p[6] = new SqlParameter("@trace_name_id_ip", name_id_ip);

			return SqlHelper.ExecuteNonQuery("prFolders_Create",p);
		}

 		public static int Delete(int Id , int schema_id,int name_id)
		{
			SqlParameter[] p=new SqlParameter[3];
			p[0]=new SqlParameter("@Id",Id);
            p[1]=new SqlParameter("@schema_id",schema_id);
            p[2] = new SqlParameter("@name_id", name_id);
			return SqlHelper.ExecuteNonQuery("prFolders_Delete",p);
		}

        public static int DeleteSubFolders(int ParentFolderId , int schema_id, int name_id)
        {
            SqlParameter[] p = new SqlParameter[2];
            p[0] = new SqlParameter("@Id", ParentFolderId);
            p[1]=new SqlParameter("@schema_id",schema_id);
            p[2] = new SqlParameter("@name_id", name_id);
            return SqlHelper.ExecuteNonQuery("pFolders_DeleteSubFolders", p);
        }

        public static int Rename(int Id, string foldername , int schema_id, int name_id ,int trace_name_id ,
                                  string trace_name_id_ip)
        {
            SqlParameter[] p = new SqlParameter[6];
            p[0] = new SqlParameter("@id", Id);
            p[1] = new SqlParameter("@foldername", foldername);
            p[2]=new SqlParameter("@schema_id",schema_id);
            p[3] = new SqlParameter("@name_id", name_id);
            p[4] = new SqlParameter("@trace_name_id", trace_name_id);
            p[5] = new SqlParameter("@trace_name_id_ip", trace_name_id_ip);
            return SqlHelper.ExecuteNonQuery("prFolders_Rename", p);
        }

       /* public static DataTable GetSubFolders(int Id , int schema_id)
        {
            SqlParameter[] p = new SqlParameter[3];
            p[0] = new SqlParameter("@ParentId", Id);
            p[1]=new SqlParameter("@schema_id",schema_id);
            p[2] = new SqlParameter("@name_id", 0);
            DataSet ds = SqlHelper.ExecuteDataSet("prFolders_GetSubFolders", p);
            return ds.Tables[0];
        }
        */

        public static DataTable GetSubFolders(int Id, int schema_id , int name_id)
        {
            SqlParameter[] p = new SqlParameter[3];
            p[0] = new SqlParameter("@ParentId", Id);
            p[1] = new SqlParameter("@schema_id", schema_id);
            p[2] = new SqlParameter("@name_id", name_id);
            DataSet ds = SqlHelper.ExecuteDataSet("prFolders_GetSubFolders", p);
            return ds.Tables[0];
        }
	}
}
