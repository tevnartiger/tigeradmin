﻿using System;
using System.Data;
using System.Data.SqlClient; 

/// <summary>
/// Summary description for Login
/// </summary>
namespace sinfoca.login
{
    public class Login
    { 
        //Properties 
        private string login_user = String.Empty;
        private int schema_id;
        private int role_id;
        private int user_id; 
        private string login_status = String.Empty;
        private string login_hash = String.Empty;
        private string login_salt = String.Empty;
        private bool   login_lost_password = false;
        private string login_activation_key = String.Empty;
        private string login_lang = String.Empty;
        //used for create account 

        private int wp_id;
        private string role_name = string.Empty;
        private string company_name = string.Empty;
        private int location_id;
        private int schema_country_id;
        private int country_id;
        private int region_id;
        private int city_id;

        private string role_name_abbr = String.Empty;
        private string country = String.Empty;
        private string region = String.Empty;
        private string city = String.Empty;

        private int person_id;

        private int is_tenant;
        private int is_owner;
        private int is_janitor;
        private int is_pm;
        private int is_manager;


        private int company_id;
        private int login_id;
        private string facebook_id;
        private int facebookidexist;
        private int assistant_person_id;
        private int accowner_person_id;
        private int schema_acc_selected;



        private decimal timezones_gmt;
        private string person_name = String.Empty;
        private string person_fname = String.Empty;
        private string schema_lang = String.Empty;
        private string login_type = String.Empty;
        private string login_mobilehash = String.Empty;
        private string schema_company_name = String.Empty;
        private string login_google_sessiontoken = String.Empty;
        private string schema_custom_css = String.Empty;


        private int getstarted;

        //Constructor
        public Login()
        { }
        public Login(string user)
        {
           //this.Load2();
          //  this.Load(user);

        }
      

        public string Login_status
        {
            get { return login_status; }
            set { login_status = value; }
        }
        public string Login_hash
        {
            get { return login_hash; }
            set { login_hash = value; }
        }
        public string Login_salt
        {
            get { return login_salt; }
            set { login_salt = value; }
        }
        public bool Login_lost_password 
        {
            get { return login_lost_password; }
            set { login_lost_password = value; }
        }
        public string Login_activation_key
        {
            get { return login_activation_key; }
            set { login_activation_key = value; }
        }
        public string Login_lang
        {
            get { return login_lang; }
            set { login_lang = value; }
        }
        public int Schema_id
        {
            get { return schema_id; }
            set {  schema_id = value; }
        }
        public int User_id
        {
            get { return user_id; }
            set { user_id = value; }
        }
        public int Role_id
        {
            get { return role_id; }
            set { role_id = value; }
        }

     
        public Login(string user, int temp_schema_id)
        {
            //this.Load2();
            this.Load(user, temp_schema_id);

        }

        public int Country_id
        {
            get { return country_id; }
            set { country_id = value; }
        }

        public int Schema_country_id
        {
            get { return schema_country_id; }
            set { schema_country_id = value; }
        }

        public int Region_id
        {
            get { return region_id; }
            set { region_id = value; }
        }

        public int Wp_id
        {
            get { return wp_id; }
            set { wp_id = value; }
        }

        public int City_id
        {
            get { return city_id; }
            set { city_id = value; }
        }

        public int Company_id
        {
            get { return company_id; }
            set { company_id = value; }
        }

        public string Country
        {
            get { return country; }
            set { country = value; }
        }

        public string Region
        {
            get { return region; }
            set { region = value; }
        }


        public string City
        {
            get { return city; }
            set { city = value; }
        }



        //Properties Setters and Getters 
        public string Login_user
        {
            get { return login_user; }
            set { login_user = Utils.sanitizeString(value, 65); }
        }


        public string Schema_company_name
        {
            get { return schema_company_name; }
            set { schema_company_name = Utils.sanitizeString(value, 100); }
        }


        public string Facebook_id
        {
            get { return facebook_id; }
            set { facebook_id = Utils.sanitizeString(value, 60); }
        }


        public int FacebookIDExist
        {
            get { return facebookidexist; }
            set { facebookidexist = value; }
        }


        public string Login_type
        {
            get { return login_type; }
            set { login_type = value; }
        }


        public string Person_name
        {
            get { return person_name; }
            set { person_name = value; }

        }

        public string Person_fname
        {
            get { return person_fname; }
            set { person_fname = value; }

        }

        public string Company_name
        {
            get { return company_name; }
            set { company_name = value; }

        }


        public string Login_mobilehash
        {
            get { return login_mobilehash; }
            set { login_mobilehash = value; }
        }


        public string Login_google_sessiontoken
        {
            get { return login_google_sessiontoken; }
            set { login_google_sessiontoken = value; }
        }

        public int Getstarted
        {
            get { return getstarted; }
            set { getstarted = value; }
        }


        public decimal Timezones_gmt
        {
            get { return timezones_gmt; }
            set { timezones_gmt = value; }
        }


      
        public string Schema_lang
        {
            get { return schema_lang; }
            set { schema_lang = value; }
        }

        public string Schema_custom_css
        {
            get { return schema_custom_css; }
            set { schema_custom_css = value; }
        }

        public string Role_name_abbr
        {
            get { return role_name_abbr; }
            set { role_name_abbr = value; }
        }

        public int Person_id
        {
            get { return person_id; }
            set { person_id = value; }
        }

        public int Is_manager
        {
            get { return is_manager; }
            set { is_manager = value; }
        }
    
        public int Is_pm
        {
            get { return is_pm; }
            set { is_pm = value; }
        }

        public int Is_janitor
        {
            get { return is_janitor; }
            set { is_janitor = value; }
        }

        public int Is_owner
        {
            get { return is_owner; }
            set { is_owner = value; }
        }

        public int Is_tenant
        {
            get { return is_tenant; }
            set { is_tenant = value; }
        }

        public int Assistant_person_id
        {
            get { return assistant_person_id; }
            set { assistant_person_id = value; }
        }


        public int Accowner_person_id
        {
            get { return accowner_person_id; }
            set { accowner_person_id = value; }
        }

        public int Location_id
        {
            get { return location_id; }
            set { location_id = value; }
        }


        public int Login_id
        {
            get { return login_id; }
            set { login_id = value; }
        }

        public string Role_name
        {
            get { return role_name; }
            set { role_name = value; }
        }





        private void Load(string user, int temp_schema_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prLoginInfoAddedCss", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();

            try
            {
                //return params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                //Add the input params
                cmd.Parameters.Add("@login_user", SqlDbType.VarChar, 65).Value = user;
                cmd.Parameters.Add("@temp_schema_id", SqlDbType.Int).Value = temp_schema_id;
                //Add the output params to the stor proc
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_name", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_fname", SqlDbType.NVarChar, 100).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_email", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@role_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@role_name", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@role_name_abbr", SqlDbType.NVarChar, 5).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@timezones_gmt", SqlDbType.Decimal).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_status", SqlDbType.Char, 2).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_hash", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_salt", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_google_sessiontoken", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_lang", SqlDbType.VarChar, 5).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_custom_css", SqlDbType.NVarChar, 2000).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_company_name", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@is_tenant", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@is_owner", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@is_janitor", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@is_pm", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@is_manager", SqlDbType.Int).Direction = ParameterDirection.Output;
                
                //execute the insert
                cmd.ExecuteReader();

                if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
                {
                    Login_user = Convert.ToString(cmd.Parameters["@login_user"].Value);
                    Schema_id = Convert.ToInt32(cmd.Parameters["@schema_id"].Value);
                    Person_id = Convert.ToInt32(cmd.Parameters["@person_id"].Value);
                    Person_name = Convert.ToString(cmd.Parameters["@person_name"].Value);
                    Person_fname = Convert.ToString(cmd.Parameters["@person_fname"].Value);
                    Role_id = Convert.ToInt32(cmd.Parameters["@role_id"].Value);
                    Role_name = Convert.ToString(cmd.Parameters["@role_name"].Value);
                    Role_name_abbr = Convert.ToString(cmd.Parameters["@role_name_abbr"].Value);
                    Timezones_gmt = Convert.ToDecimal(cmd.Parameters["@timezones_gmt"].Value);
                    Login_status = Convert.ToString(cmd.Parameters["@login_status"].Value);
                    Login_hash = Convert.ToString(cmd.Parameters["@login_hash"].Value);
                    Login_salt = Convert.ToString(cmd.Parameters["@login_salt"].Value);
                    Login_google_sessiontoken = Convert.ToString(cmd.Parameters["@login_google_sessiontoken"].Value);
                    Login_lang = Convert.ToString(cmd.Parameters["@login_lang"].Value);
                    Schema_custom_css = Convert.ToString(cmd.Parameters["@schema_custom_css"].Value);
                    Schema_company_name = Convert.ToString(cmd.Parameters["@schema_company_name"].Value);

                    Is_tenant = Convert.ToInt32(cmd.Parameters["@is_tenant"].Value);
                    Is_owner = Convert.ToInt32(cmd.Parameters["@is_owner"].Value);
                    Is_janitor = Convert.ToInt32(cmd.Parameters["@is_janitor"].Value);
                    Is_pm = Convert.ToInt32(cmd.Parameters["@is_pm"].Value);
                    Is_manager = Convert.ToInt32(cmd.Parameters["@is_manager"].Value);
                   

                }
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }

        }

        public void MobileLoginInfoAddedCss(string mtk)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prMobileLoginInfoAddedCss", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            try
            {
                //return params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                //Add the input params
                cmd.Parameters.Add("@mtk", SqlDbType.VarChar, 65).Value = mtk;
                //Add the output params to the stor proc
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_name", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_email", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@role_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@wp_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_country_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@company_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@role_name", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@company_name", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_company_name", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@role_name_abbr", SqlDbType.NVarChar, 5).Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@timezones_gmt", SqlDbType.Decimal).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_status", SqlDbType.Char, 2).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_hash", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_useragent", SqlDbType.VarChar, 400).Value = System.Web.HttpContext.Current.Request.UserAgent + System.Web.HttpContext.Current.Request.Browser.Win16.ToString() + System.Web.HttpContext.Current.Request.Browser.Win32.ToString() + System.Web.HttpContext.Current.Request.AcceptTypes.ToString() + System.Web.HttpContext.Current.Request.IsSecureConnection.ToString() + System.Web.HttpContext.Current.Request.Browser.Browser.ToString() + System.Web.HttpContext.Current.Request.Browser.Version + System.Web.HttpContext.Current.Request.UserLanguages;
                cmd.Parameters.Add("@login_mobilehash", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_salt", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_google_sessiontoken", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_lang", SqlDbType.VarChar, 5).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_custom_css", SqlDbType.NVarChar, 2000).Direction = ParameterDirection.Output;

                //execute the insert
                cmd.ExecuteReader();

                if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
                {
                    Company_id = Convert.ToInt32(cmd.Parameters["@company_id"].Value);
                    Wp_id = Convert.ToInt32(cmd.Parameters["@wp_id"].Value);
                    Schema_id = Convert.ToInt32(cmd.Parameters["@schema_id"].Value);
                    Person_id = Convert.ToInt32(cmd.Parameters["@person_id"].Value);
                    Person_name = Convert.ToString(cmd.Parameters["@person_name"].Value);
                    Role_id = Convert.ToInt32(cmd.Parameters["@role_id"].Value);
                    Role_name = Convert.ToString(cmd.Parameters["@role_name"].Value);
                    Role_name_abbr = Convert.ToString(cmd.Parameters["@role_name_abbr"].Value);
                    Company_name = Convert.ToString(cmd.Parameters["@company_name"].Value);
                    Schema_company_name = Convert.ToString(cmd.Parameters["@schema_company_name"].Value);
                    Schema_country_id = Convert.ToInt32(cmd.Parameters["@schema_country_id"].Value);
                    Timezones_gmt = Convert.ToDecimal(cmd.Parameters["@timezones_gmt"].Value);
                    Login_status = Convert.ToString(cmd.Parameters["@login_status"].Value);
                    Login_user = Convert.ToString(cmd.Parameters["@person_email"].Value);
                    Login_hash = Convert.ToString(cmd.Parameters["@login_hash"].Value);
                    Login_mobilehash = Convert.ToString(cmd.Parameters["@login_mobilehash"].Value);
                    Login_salt = Convert.ToString(cmd.Parameters["@login_salt"].Value);
                    Login_google_sessiontoken = Convert.ToString(cmd.Parameters["@login_google_sessiontoken"].Value);
                    Login_lang = Convert.ToString(cmd.Parameters["@login_lang"].Value);
                    Schema_custom_css = Convert.ToString(cmd.Parameters["@schema_custom_css"].Value);

                }
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }

        }






        public void getFacebookLoad(string facebook_id, int temp_schema_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prFacebookLoginInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();

            try
            {
                //return params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                //Add the input params
                cmd.Parameters.Add("@facebook_id", SqlDbType.VarChar, 60).Value = facebook_id;
                cmd.Parameters.Add("@temp_schema_id", SqlDbType.Int).Value = temp_schema_id;

                //Add the output params to the stor proc
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@timezones_gmt", SqlDbType.Decimal).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@assistant_person_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@accowner_person_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_name", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@role_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@location_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_status", SqlDbType.Char, 2).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_type", SqlDbType.Char, 2).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_hash", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_lang", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_salt", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_company_name", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@getstarted", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_google_sessiontoken", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                // cmd.Parameters.Add("@login_activation_key", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                //  cmd.Parameters.Add("@login_lang", SqlDbType.VarChar, 15).Direction = ParameterDirection.Output;

                //execute the insert
                cmd.ExecuteReader();

                if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
                {
                    //Return the values of the output params
                    Schema_id = Convert.ToInt32(cmd.Parameters["@schema_id"].Value);
                    Timezones_gmt = Convert.ToDecimal(cmd.Parameters["@timezones_gmt"].Value);
                    Person_id = Convert.ToInt32(cmd.Parameters["@person_id"].Value);
                    Assistant_person_id = Convert.ToInt32(cmd.Parameters["@assistant_person_id"].Value);
                    Accowner_person_id = Convert.ToInt32(cmd.Parameters["@accowner_person_id"].Value);
                    Location_id = Convert.ToInt32(cmd.Parameters["@location_id"].Value);
                    person_name = Convert.ToString(cmd.Parameters["@person_name"].Value);
                    Role_id = Convert.ToInt32(cmd.Parameters["@role_id"].Value);
                    Login_status = Convert.ToString(cmd.Parameters["@login_status"].Value);
                    Login_type = Convert.ToString(cmd.Parameters["@login_type"].Value);
                    Login_hash = Convert.ToString(cmd.Parameters["@login_hash"].Value);
                    Login_salt = Convert.ToString(cmd.Parameters["@login_salt"].Value);
                    Schema_company_name = Convert.ToString(cmd.Parameters["@schema_company_name"].Value);
                    Schema_lang = Convert.ToString(cmd.Parameters["@schema_lang"].Value);
                    Login_google_sessiontoken = Convert.ToString(cmd.Parameters["@login_google_sessiontoken"].Value);

                    Getstarted = Convert.ToInt32(cmd.Parameters["@getstarted"].Value);
                    //   Login_lost_password = Convert.ToBoolean(cmd.Parameters["@login_lost_password"].Value);
                    //   Login_activation_key = Utils.sanitizeString(Convert.ToString(cmd.Parameters["@login_activation_key"].Value),50);
                    //    Login_lang = Convert.ToString(cmd.Parameters["@login_lang"].Value);

                }
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }

        }










        public bool createAccount()
        //string member_email, string member_salt, string member_hash, string member_activation_key)
        {
            bool result = false;
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prLoginInsert", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Return value 
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                //Add the input params
                cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = 14;
                cmd.Parameters.Add("@login_user", SqlDbType.VarChar, 65).Value = Login_user;
                cmd.Parameters.Add("@login_hash", SqlDbType.VarChar, 50).Value = Login_hash;
                cmd.Parameters.Add("@login_salt", SqlDbType.VarChar, 50).Value = Login_salt;
                cmd.Parameters.Add("@login_activation_key", SqlDbType.VarChar, 50).Value = Login_activation_key;
                cmd.Parameters.Add("@login_lang", SqlDbType.VarChar, 50).Value = Login_lang;
                cmd.Parameters.Add("@trace_user_ip", SqlDbType.VarChar, 50).Value = System.Web.HttpContext.Current.Request.UserHostAddress; ;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            //execute the insert
            finally
            {
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            //Failure or Success (true = success (1), else fail) 
            if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
            {
                result = true;
            }
            return result;
        }



        public bool ModifyPwd()
        {
            bool result = false;
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prModifyPwd", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Return value 
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                //Add the input params

                cmd.Parameters.Add("@login_user", SqlDbType.VarChar, 65).Value = Login_user;
                cmd.Parameters.Add("@login_salt", SqlDbType.VarChar, 50).Value = Login_salt;
                cmd.Parameters.Add("@login_hash", SqlDbType.VarChar, 50).Value = Login_hash;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Schema_id;
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 50).Value = System.Web.HttpContext.Current.Request.UserHostAddress;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            //execute the insert
            finally
            {
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            //Failure or Success (true = success (1), else fail) 
            if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
            {
                result = true;

            }
            return result;

        }

        //----------------------------------------------------------------------------------------------------------------------------------

        public void LoginActivateInfo(string key)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prLoginActivateInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();

            try
            {
                //return params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                //Add the input params
                cmd.Parameters.Add("@key", SqlDbType.VarChar, 65).Value = key;
                //Add the output params to the stor proc
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_name", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@role_name", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@company_name", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@company_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@role_name_abbr", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@role_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_company_name", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_lang", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_status", SqlDbType.Char, 2).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_user", SqlDbType.VarChar, 75).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_hash", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_salt", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;

                //execute the insert
                cmd.ExecuteReader();

                if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
                {
                    //Return the values of the output params
                    Login_user = Convert.ToString(cmd.Parameters["@login_user"].Value);
                    Schema_id = Convert.ToInt32(cmd.Parameters["@schema_id"].Value);
                    Person_id = Convert.ToInt32(cmd.Parameters["@person_id"].Value);
                    Person_name = Convert.ToString(cmd.Parameters["@person_name"].Value);
                    Role_name = Convert.ToString(cmd.Parameters["@role_name"].Value);
                    Role_name_abbr = Convert.ToString(cmd.Parameters["@role_name_abbr"].Value);
                    Role_id = Convert.ToInt32(cmd.Parameters["@role_id"].Value);
                    Schema_company_name = Convert.ToString(cmd.Parameters["@schema_company_name"].Value);
                    Login_id = Convert.ToInt32(cmd.Parameters["@login_id"].Value);
                    Login_status = Convert.ToString(cmd.Parameters["@login_status"].Value);
                    Login_hash = Convert.ToString(cmd.Parameters["@login_hash"].Value);
                    Schema_lang = Convert.ToString(cmd.Parameters["@schema_lang"].Value);
                    Login_salt = Convert.ToString(cmd.Parameters["@login_salt"].Value);
                    Company_id = Convert.ToInt32(cmd.Parameters["@company_id"].Value);
                    Company_name = Convert.ToString(cmd.Parameters["@company_name"].Value);

                }
            }
            /*  catch (Exception ex)
              {
                  sinfoca.error.ErrorLog.addErrorLog(ex);
              }*/
            finally
            {
                conn.Close();
            }

        }

        public bool LoginActivatePerson()
        {
            bool result = false;
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prLoginActivatePerson", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            //Return value 
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            //Add the input params

            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Schema_id;
            cmd.Parameters.Add("@login_id", SqlDbType.Int).Value = Convert.ToInt32(Login_id);
            cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 20).Value = System.Web.HttpContext.Current.Request.UserHostAddress;


            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            //Failure or Success (true = success (1), else fail) 
            if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
            {
                result = true;

            }
            return result;

        }


        public void setLastLogin(int login_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prLoginUserExist", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Return value 
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                //Add the input params

                cmd.Parameters.Add("@login_id", SqlDbType.Int).Value = login_id;
                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (Exception)
            {

            }
        }



        //----------------------------------------------------------------------------------------------------------------------------------














        public int newLogin(int schema_id)
        //string member_email, string member_salt, string member_hash, string member_activation_key)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prNewLogin", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            //    try
            {
                conn.Open();
                //Return value 
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                //Add the input params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Person_id;

                cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = Role_id;
                cmd.Parameters.Add("@login_user", SqlDbType.VarChar, 65).Value = Login_user;

                cmd.Parameters.Add("@login_hash", SqlDbType.VarChar, 50).Value = Login_hash;
                cmd.Parameters.Add("@login_salt", SqlDbType.VarChar, 50).Value = Login_salt;
                cmd.Parameters.Add("@login_activation_key", SqlDbType.VarChar, 50).Value = Login_activation_key;
                cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = 0;// Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@schema_lang", SqlDbType.VarChar, 50).Value = Schema_lang;
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 50).Value = System.Web.HttpContext.Current.Request.UserHostAddress;

                //execute the insert
                cmd.ExecuteReader();
                //Failure or Success (true = success (1), else fail) 

            }
            //  catch (Exception ex)
            {
                //    sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            //   finally
            {
                conn.Close();
            }


            return Convert.ToInt32(cmd.Parameters["@return"].Value);

        }




        /// <summary>
        /// Set has lost password for a user
        /// </summary>
        /// <returns></returns>
        public bool setLostPassword(int schema_id)
        {
            bool result = false;

            //Create new activation key
            string temp_activation_key = Utils.createRandom2(30);
            Login_activation_key = temp_activation_key;
            //connect to DB
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prSetLostPassword", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();

            //Return value 
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

            //Add the input params
            cmd.Parameters.Add("@login_user", SqlDbType.VarChar, 65).Value = Login_user;
            cmd.Parameters.Add("@login_activation_key", SqlDbType.VarChar, 50).Value = Login_activation_key;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);

            //Failure or Success (true = success (1), else fail) 
            if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
            {
                result = true;

            }
            return result;

        }


        /// <summary>
        /// Set has lost password for a user
        /// </summary>
        /// <returns></returns>
        public bool setClientLostPassword(int schema_id, string login_user)
        {
            bool result = false;

            //Create new activation key
            string temp_activation_key = Utils.createRandom(30);
            Login_activation_key = temp_activation_key;
            //connect to DB
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prSetLostPassword", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();

            //Return value 
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

            //Add the input params
            cmd.Parameters.Add("@login_user", SqlDbType.VarChar, 65).Value = login_user;
            cmd.Parameters.Add("@login_activation_key", SqlDbType.VarChar, 50).Value = Login_activation_key;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);

            //Failure or Success (true = success (1), else fail) 
            if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
            {
                result = true;

            }
            return result;

        }

        public bool activateAccount()
        {
            bool result = false;
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prLoginActivate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Return value 
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                //Add the input params
                cmd.Parameters.Add("@login_user", SqlDbType.VarChar, 65).Value = Login_user;
                cmd.Parameters.Add("@login_activation_key", SqlDbType.VarChar, 50).Value = login_activation_key;

            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            //execute the insert
            finally
            {
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            //Failure or Success (true = success (1), else fail) 
            if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
            {
                result = true;

            }
            return result;

        }


        public int MpwdAccess()
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prMpwdAccess", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();


            //Return value 
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            //Add the input params
            cmd.Parameters.Add("@login_user", SqlDbType.VarChar, 65).Value = Login_user;
            cmd.Parameters.Add("@login_activation_key", SqlDbType.VarChar, 50).Value = Login_activation_key;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Schema_id;
            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            //Failure or Success (true = success (1), else fail) 

            return Convert.ToInt32(cmd.Parameters["@return"].Value);
        }



        public string MobileHashTokenUpdate(int schema_id, int person_id, string login_mobiletoken, string login_mobilehash)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prMobileHashTokenUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            //Return value 
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            //Add the input params
            cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;
            cmd.Parameters.Add("@login_mobiletoken", SqlDbType.VarChar, 50).Value = login_mobiletoken;
            cmd.Parameters.Add("@login_useragent", SqlDbType.VarChar, 400).Value = System.Web.HttpContext.Current.Request.UserAgent + System.Web.HttpContext.Current.Request.Browser.Win16.ToString() + System.Web.HttpContext.Current.Request.Browser.Win32.ToString() + System.Web.HttpContext.Current.Request.AcceptTypes.ToString() + System.Web.HttpContext.Current.Request.IsSecureConnection.ToString() + System.Web.HttpContext.Current.Request.Browser.Browser.ToString() + System.Web.HttpContext.Current.Request.Browser.Version + System.Web.HttpContext.Current.Request.UserLanguages;
            cmd.Parameters.Add("@login_mobilehash", SqlDbType.VarChar, 50).Value = login_mobilehash;
            cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
            cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 100).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            //Failure or Success (true = success (1), else fail) 

            return Convert.ToString(cmd.Parameters["@return"].Value);
        }


        public bool ActivateLostPassword()
        {
            bool result = false;

            //Create new activation key
            //    string temp_activation_key = sinfoca.login.Hash.createSalt(30);
            //  Login_activation_key = temp_activation_key;
            //connect to DB
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prLoginActivateLostPassword", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();

                //Return value 
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                //Add the input params

                cmd.Parameters.Add("@login_user", SqlDbType.VarChar, 65).Value = Login_user;
                cmd.Parameters.Add("@login_activation_key", SqlDbType.VarChar, 50).Value = Login_activation_key;
            }
            //execute the insert
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            //Failure or Success (true = success (1), else fail) 
            if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
            {
                result = true;

            }
            return result;

        }




        public bool UpdateMemberLastLogin(int temp_member_id)
        {
            bool result = false;

            //Create new activation key
            //  string temp_activation_key = sinfoca.login.Hash.createSalt(30);
            ////  Login_activation_key = temp_activation_key;
            //connect to DB
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prMemberLastLoginUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();

                //Return value 
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                //Add the input params

                cmd.Parameters.Add("@member_id", SqlDbType.VarChar, 65).Value = temp_member_id;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            //execute the insert
            finally
            {
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            //Failure or Success (true = success (1), else fail) 
            if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
            {
                result = true;

            }
            return result;



        }


        public string ModifyPwd(string schema_id, string login_user, string login_hash, string login_salt, string trace_person_ip)
        {
            string result = "0";
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prModifyPwd", conn);
            cmd.CommandType = CommandType.StoredProcedure;



            try
            {
                conn.Open();
                //Return value 
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                //Add the input params

                cmd.Parameters.Add("@login_user", SqlDbType.VarChar, 65).Value = login_user;
                cmd.Parameters.Add("@login_salt", SqlDbType.VarChar, 50).Value = login_salt;
                cmd.Parameters.Add("@login_hash", SqlDbType.VarChar, 50).Value = login_hash;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(schema_id);
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 50).Value = trace_person_ip;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            //execute the insert
            finally
            {
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            //Failure or Success (true = success (1), else fail) 
            if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
            {
                result = "1";

            }
            return result;

        }


        public bool removeLostPasswordStatus(int temp_login_id)
        {
            bool result = false;
            //Connection to the Store Proc "prMemberContestAdd"
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prRemoveLostPasswordStatus", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();

                //cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.Return;
                //Add the input params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                cmd.Parameters.Add("@login_id", SqlDbType.Int).Value = temp_login_id;
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
                {
                    result = true;

                }
                conn.Close();
                result = true;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }

            return result;
        }



        public bool GoogleSessionTokenUpdate()
        {
            bool result = false;

            //Create new activation key
            //connect to DB
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prGoogleSessionTokenUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();

                //Return value 
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@login_google_sessiontoken", SqlDbType.VarChar, 100).Value = login_google_sessiontoken;
                cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@trace_user_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();

            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            //execute the insert
            finally
            {
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }

            if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
            {
                result = true;

            }
            return result;



        }



        public void getFacebookIDExist(string facebook_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd2 = new SqlCommand("prFacebookIDExist", conn);

            cmd2.CommandType = CommandType.StoredProcedure;


            conn.Open();
            //Add the params
            cmd2.Parameters.Add("@facebookidexist", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd2.Parameters.Add("@facebook_id", SqlDbType.VarChar, 60).Value = facebook_id;

            try
            {

                SqlDataReader dr = null;
                dr = cmd2.ExecuteReader();

                facebookidexist = Convert.ToInt32(cmd2.Parameters["@facebookidexist"].Value);
            }

            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }

            finally
            {
                conn.Close();
            }

        }






        public void getFacebookIDUpdate(string facebook_id, int person_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd2 = new SqlCommand("prFacebookIDUpdate", conn);

            cmd2.CommandType = CommandType.StoredProcedure;


            conn.Open();
            //Add the params
            cmd2.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd2.Parameters.Add("@facebook_id", SqlDbType.VarChar, 60).Value = facebook_id;
            cmd2.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;
            cmd2.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
            cmd2.Parameters.Add("@trace_user_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();

            try
            {

                SqlDataReader dr = null;
                dr = cmd2.ExecuteReader();
            }

            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }

            finally
            {
                conn.Close();
            }



            if (Convert.ToInt32(cmd2.Parameters["@return"].Value) == 1)
            {


            }

        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="days"></param>
        /// <param name="acc_selected"></param>
        /// <returns></returns>
        public bool setUpdateAccountSelectedTrial(int days, int acc_selected)
        {
            bool result = false;
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prUpdateLoginTypeTrial", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Return value 
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                //Add the input params
                cmd.Parameters.Add("@days", SqlDbType.Int).Value = days;
                cmd.Parameters.Add("@acc_selected", SqlDbType.Int).Value = acc_selected;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@trace_user_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            //execute the insert
            finally
            {
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            //Failure or Success (true = success (1), else fail) 
            if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
            {
                result = true;
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="days"></param>
        /// <param name="acc_selected"></param>
        /// <returns></returns>
        public bool setUpdateAccountSelected(int acc_selected)
        {
            bool result = false;
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prUpdateLoginType", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Return value 
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                //Add the input params
                cmd.Parameters.Add("@acc_selected", SqlDbType.Int).Value = acc_selected;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@trace_user_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            //execute the insert
            finally
            {
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
            //Failure or Success (true = success (1), else fail) 
            if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
            {
                result = true;
            }
            return result;
        }
       
    }

}

