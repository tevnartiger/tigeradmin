﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;


/// <summary>
/// Summary description for Financial
/// Done by : Stanley Jocelyn
/// Date    : march 1 , 2008
/// </summary>
namespace tiger
{
    public class Financial
    {
        private string str_conn;
        public Financial(string str_conn)
        {
            this.str_conn = str_conn;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public DataTable getNumberUnitbyBedroomNumber(int home_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prNumberUnitbyBedroomNumber", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }  
            finally
            {
                conn.Close();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="group_id"></param>
        /// <returns></returns>
        public DataTable getGroupNumberUnitbyBedroomNumber(int group_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupNumberUnitbyBedroomNumber", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public DataTable getNumberComUnit(int schema_id ,int home_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prNumberComUnit", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }

/// <summary>
/// 
/// </summary>
/// <param name="schema_id"></param>
/// <returns></returns>
        public string getPaidLateRentFeeHomeSum(int home_id, int schema_id, DateTime date_month_year)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prPaidLateRentFeeHomeSum", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@sum", SqlDbType.Decimal).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@date_month_year", SqlDbType.DateTime).Value = date_month_year;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
               return String.Format("{0:0.00}", Convert.ToString(cmd.Parameters["@sum"].Value));
               // return Convert.ToString(cmd.Parameters["@sum"].Value);
            }
            finally
            {
                conn.Close();
            }

        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public string getGroupPaidLateRentFeeHomeSum(int group_id, int schema_id, DateTime date_month_year)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupPaidLateRentFeeHomeSum", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@sum", SqlDbType.Decimal).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
                cmd.Parameters.Add("@date_month_year", SqlDbType.DateTime).Value = date_month_year;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return String.Format("{0:0.00}", Convert.ToString(cmd.Parameters["@sum"].Value));
                // return Convert.ToString(cmd.Parameters["@sum"].Value);
            }
            finally
            {
                conn.Close();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public string getPaidLateRentFeeHomeSumPeriod(int home_id, int schema_id, DateTime from, DateTime to)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prPaidLateRentFeeHomeSumPeriod", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@sum", SqlDbType.Decimal).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@from", SqlDbType.DateTime).Value = from;
                cmd.Parameters.Add("@to", SqlDbType.DateTime).Value = to;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return String.Format("{0:0.00}", Convert.ToString(cmd.Parameters["@sum"].Value));
                // return Convert.ToString(cmd.Parameters["@sum"].Value);
            }
            finally
            {
                conn.Close();
            }

        }





        public string getGroupPaidLateRentFeeHomeSumPeriod(int group_id, int schema_id, DateTime from, DateTime to)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupPaidLateRentFeeHomeSumPeriod", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@sum", SqlDbType.Decimal).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
                cmd.Parameters.Add("@from", SqlDbType.DateTime).Value = from;
                cmd.Parameters.Add("@to", SqlDbType.DateTime).Value = to;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return String.Format("{0:0.00}", Convert.ToString(cmd.Parameters["@sum"].Value));
                // return Convert.ToString(cmd.Parameters["@sum"].Value);
            }
            finally
            {
                conn.Close();
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public DataTable getNumberUnitbyBedroomNumberTotalMonthlyRentIncome(int schema_id, int home_id, DateTime date_month_year)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prNumberUnitbyBedroomNumberTotalMonthlyRentIncome", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@date_month_year", SqlDbType.DateTime).Value = date_month_year;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public DataTable getGroupNumberUnitbyBedroomNumberTotalMonthlyRentIncome(int schema_id, int group_id, DateTime date_month_year)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupNumberUnitbyBedroomNumberTotalMonthlyRentIncome", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
                cmd.Parameters.Add("@date_month_year", SqlDbType.DateTime).Value = date_month_year;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public DataTable getNumberCommercialUnitTotalMonthlyRentIncome(int schema_id, int home_id, DateTime date_month_year)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prNumberCommercialUnitTotalMonthlyRentIncome", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@date_month_year", SqlDbType.DateTime).Value = date_month_year;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public DataTable getGroupNumberCommercialUnitTotalMonthlyRentIncome(int schema_id, int group_id, DateTime date_month_year)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupNumberCommercialUnitTotalMonthlyRentIncome", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
                cmd.Parameters.Add("@date_month_year", SqlDbType.DateTime).Value = date_month_year;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public DataSet getNumberUnitbyBedroomNumberTotalMonthlyRentIncomePeriod(int schema_id, int home_id, DateTime from, DateTime to)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prNumberUnitbyBedroomNumberTotalMonthlyRentIncomePeriod", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@from", SqlDbType.DateTime).Value = from;
                cmd.Parameters.Add("@to", SqlDbType.DateTime).Value = to;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }




        public DataSet getGroupNumberUnitbyBedroomNumberTotalMonthlyRentIncomePeriod(int schema_id, int group_id, DateTime from, DateTime to)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupNumberUnitbyBedroomNumberTotalMonthlyRentIncomePeriod", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
                cmd.Parameters.Add("@from", SqlDbType.DateTime).Value = from;
                cmd.Parameters.Add("@to", SqlDbType.DateTime).Value = to;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public DataTable getNumberCommercialUnitTotalMonthlyRentIncomePeriod(int schema_id, int home_id, DateTime from, DateTime to)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prNumberCommercialUnitTotalMonthlyRentIncomePeriod", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@from", SqlDbType.DateTime).Value = from;
                cmd.Parameters.Add("@to", SqlDbType.DateTime).Value = to;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }



        public DataTable getGroupNumberCommercialUnitTotalMonthlyRentIncomePeriod(int schema_id, int group_id, DateTime from, DateTime to)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupNumberCommercialUnitTotalMonthlyRentIncomePeriod", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
                cmd.Parameters.Add("@from", SqlDbType.DateTime).Value = from;
                cmd.Parameters.Add("@to", SqlDbType.DateTime).Value = to;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public DataTable getMfsNumberUnitbyBedroomNumber(int schema_id,int mfs_id )
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prMfsNumberUnitbyBedroomNumber", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@mfs_id", SqlDbType.Int).Value = mfs_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public DataTable getMfsNumberComUnit(int schema_id, int mfs_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prMfsNumberComUnit", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@mfs_id", SqlDbType.Int).Value = mfs_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public DataSet getFinancialMoneyFlowScenarioList(int schema_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prFinancialMoneyFlowScenarioList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }


        public int getFinancialMfsHome(int schema_id, int mfs_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prFinancialMfsHome", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@mfs_id", SqlDbType.Int).Value = mfs_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return Convert.ToInt32(cmd.Parameters["@home_id"].Value);
            }
            finally
            {
                conn.Close();
            }




        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public DataSet getExpenseMonthView(int home_id, int schema_id,DateTime expense_date_paid)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prExpenseMonthView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@expense_date_paid", SqlDbType.DateTime).Value = expense_date_paid;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public DataSet getExpenseMonthViewGroupByCateg(int home_id, int schema_id, DateTime date_month_year)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prExpenseMonthViewGroupByCateg", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@date_month_year", SqlDbType.DateTime).Value = date_month_year;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="group_id"></param>
        /// <returns></returns>
        public DataSet getGroupExpenseMonthViewGroupByCateg(int group_id, int schema_id, DateTime date_month_year)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupExpenseMonthViewGroupByCateg", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
                cmd.Parameters.Add("@date_month_year", SqlDbType.DateTime).Value = date_month_year;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <param name="schema_id"></param>
        /// <param name="date_month_year"></param>
        /// <returns></returns>
        public DataSet getExpenseMonthViewGroupByCategPeriod(int home_id, int schema_id, DateTime from, DateTime to)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prExpenseMonthViewGroupByCategPeriod", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@from", SqlDbType.DateTime).Value = from;
                cmd.Parameters.Add("@to", SqlDbType.DateTime).Value = to;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }



        public DataSet getGroupExpenseMonthViewGroupByCategPeriod(int group_id, int schema_id, DateTime from, DateTime to)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupExpenseMonthViewGroupByCategPeriod", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
                cmd.Parameters.Add("@from", SqlDbType.DateTime).Value = from;
                cmd.Parameters.Add("@to", SqlDbType.DateTime).Value = to;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }

        public DataSet getIncomeMonthView(int home_id, int schema_id, DateTime income_date_received)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prIncomeMonthView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@income_date_received", SqlDbType.DateTime).Value = income_date_received;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <param name="schema_id"></param>
        /// <param name="income_date_received"></param>
        /// <returns></returns>
        public DataSet getIncomeMonthViewGroupByCateg(int home_id, int schema_id, DateTime @date_month_year)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prIncomeMonthViewGroupByCateg", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@date_month_year", SqlDbType.DateTime).Value = @date_month_year;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="group_id"></param>
        /// <param name="schema_id"></param>
        /// <param name="income_date_received"></param>
        /// <returns></returns>
        public DataSet getGroupIncomeMonthViewGroupByCateg(int group_id, int schema_id, DateTime @date_month_year)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupIncomeMonthViewGroupByCateg", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
                cmd.Parameters.Add("@date_month_year", SqlDbType.DateTime).Value = @date_month_year;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <param name="schema_id"></param>
        /// <param name="income_date_received"></param>
        /// <returns></returns>
        public DataSet getIncomeMonthViewGroupByCategPeriod(int home_id, int schema_id, DateTime @from, DateTime @to)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prIncomeMonthViewGroupByCategPeriod", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@from", SqlDbType.DateTime).Value = @from;
                cmd.Parameters.Add("@to", SqlDbType.DateTime).Value = @to;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }



        public DataSet getGroupIncomeMonthViewGroupByCategPeriod(int group_id, int schema_id, DateTime @from, DateTime @to)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupIncomeMonthViewGroupByCategPeriod", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
                cmd.Parameters.Add("@from", SqlDbType.DateTime).Value = @from;
                cmd.Parameters.Add("@to", SqlDbType.DateTime).Value = @to;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <param name="schema_id"></param>
        /// <param name="expense_date_paid"></param>
        /// <returns></returns>
        public string getTotalMonthExpense(int home_id, int schema_id, DateTime expense_date_paid)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prTotalMonthExpense2", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@expense_date_paid", SqlDbType.DateTime).Value = expense_date_paid;
                cmd.Parameters.Add("@total_expense", SqlDbType.Money).Direction = ParameterDirection.Output;

                //execute the insert
                cmd.ExecuteReader();

                //return  Convert.ToString(cmd.Parameters["@total_expense"].Value);
                if (cmd.Parameters["@total_expense"].Value == DBNull.Value)
                    return String.Format("{0:0.00}", 0.00);
                else
                    return String.Format("{0:0.00}", Convert.ToDouble(cmd.Parameters["@total_expense"].Value));

            }
            finally
            {
                conn.Close();
            }
        }
/// <summary>
/// 
/// </summary>
/// <param name="home_id"></param>
/// <param name="schema_id"></param>
/// <param name="expense_date_paid"></param>
/// <returns></returns>
        public string getTotalMonthIncome(int home_id, int schema_id, DateTime income_date_received)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prTotalMonthIncome", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@income_date_received", SqlDbType.DateTime).Value = income_date_received;
                cmd.Parameters.Add("@total_income", SqlDbType.Money).Direction = ParameterDirection.Output;

                //execute the insert
                cmd.ExecuteReader();
                return Convert.ToString(cmd.Parameters["@total_income"].Value);
            }
            finally
            {
                conn.Close();
            }
        }



/// <summary>
/// 
/// </summary>
/// <param name="schema_id"></param>
/// <returns></returns>
        public DataTable getFinancialAnalysisPeriodList(int schema_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prFinancialAnalysisPeriodList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public DataTable getOwnerFinancialAnalysisPeriodList(int schema_id , int name_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prOwner_FinancialAnalysisPeriodList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }

    }







}
