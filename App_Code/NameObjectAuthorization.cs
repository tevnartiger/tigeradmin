﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using sinfoca.login;

/// <summary>
/// Summary description for NameObjectAuthorization
/// Done by : Stanley Jocelyn
/// date    : june 17 , 2009
/// Description : Verify if the object belong to the Name ( user )
/// </summary>
namespace sinfoca.tiger.security.NameObjectAuthorization
{
    public class NameObjectAuthorization
    {

        private string conn;
        public NameObjectAuthorization(string conn)
        {
            this.conn = conn;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="tu_id"></param>
        /// <param name="ta_id"></param>
        /// <returns></returns>
        public bool AccomodationArchive(int schema_id, int tu_id, int ta_id , int name_id, int role_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeAccomodationArchive", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;
            cmd.Parameters.Add("@ta_id", SqlDbType.Int).Value = ta_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="appliance_id"></param>
        /// <returns></returns>
        public bool Appliance(int schema_id, int appliance_id, int name_id , int role_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeAppliance", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@appliance_id", SqlDbType.Int).Value = appliance_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="appliances"></param>
        /// <param name="ua_ids"></param>
        /// <param name="number_of_insert"></param>
        /// <returns></returns>
        public bool ApplianceBatch(int schema_id, string appliances, string ua_ids, int number_of_insert, int name_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeApplianceBatch", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@appliance_id", SqlDbType.NVarChar, 4000).Value = appliances;
            cmd.Parameters.Add("@ua_id", SqlDbType.NVarChar, 4000).Value = ua_ids;
            cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public bool Home(int schema_id, int home_id, int name_id ,int role_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeHome", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="appliance_id"></param>
        /// <returns></returns>
        public bool Company(int schema_id, int company_id, int name_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeCompany", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@company_id", SqlDbType.Int).Value = company_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="event_id"></param>
        /// <returns></returns>
        public bool Event(int schema_id, int event_id, int name_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeEvent", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = event_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="mfs_id"></param>
        /// <returns></returns>
        public bool FinancialMoneyFlowScenario(int schema_id, int mfs_id, int name_id, int role_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeFinancialMoneyFlowScenario", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@mfs_id", SqlDbType.Int).Value = mfs_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="income_id"></param>
        /// <returns></returns>
        public bool Expense(int schema_id, int expense_id, int name_id,int role_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeExpense", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@expense_id", SqlDbType.Int).Value = expense_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }

        public bool HomeEvaluation(int schema_id, int he_id, int name_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeHomeEvaluation", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@he_id", SqlDbType.Int).Value = he_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="tu_id"></param>
        /// <param name="ta_id"></param>
        /// <returns></returns>
        public bool HomeWorkOrder(int schema_id, int home_id, int wo_id, int name_id, int role_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeHomeWorkOrder", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd.Parameters.Add("@wo_id", SqlDbType.Int).Value = wo_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="mfs_id"></param>
        /// <returns></returns>
        public bool Income(int schema_id, int income_id, int name_id , int role_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeIncome", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@income_id", SqlDbType.Int).Value = income_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="ic_id"></param>
        /// <returns></returns>
        public bool InsuranceCompany(int schema_id, int ic_id, int name_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeInsuranceCompany", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@ic_id", SqlDbType.Int).Value = ic_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="ip_id"></param>
        /// <returns></returns>
        public bool InsurancePolicy(int schema_id, int ip_id, int name_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeInsurancePolicy", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@ip_id", SqlDbType.Int).Value = ip_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="income_id"></param>
        /// <returns></returns>
        public bool FinancialAnalysisPeriod(int schema_id, int pa_id, int name_id, int role_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeFinancialAnalysisPeriod", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@period_analysis_id", SqlDbType.Int).Value = pa_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="pa_id"></param>
        /// <returns></returns>
        public bool FinancialInstitution(int schema_id, int fi_id, int name_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeFinancialInstitution", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@fi_id", SqlDbType.Int).Value = fi_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }



        public bool HomeUnit(int schema_id, int home_id, int unit_id, int name_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeHomeUnit", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="group_id"></param>
        /// <returns></returns>
        public bool Group(int schema_id, int group_id, int name_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeGroup", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
          //  cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }


        public bool Incident(int schema_id, int incident_id, int name_id, int role_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeIncident", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@incident_id", SqlDbType.Int).Value = incident_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }





        public bool Mortgage(int schema_id, int mortgage_id, int name_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeMortgage", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@mortgage_id", SqlDbType.Int).Value = mortgage_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }

        
        public bool Name(int schema_id, int name_id , int name_id2 , int role_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeName", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@name_id2", SqlDbType.Int).Value = name_id2;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;



            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }

        public bool RentPaid(int schema_id, int rp_id, int name_id, int role_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeRentPaid", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@rp_id", SqlDbType.Int).Value = rp_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="pt_id"></param>
        /// <returns></returns>
        public bool ProspectiveTenant(int schema_id, int pt_id, int name_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeProspectiveTenant", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@pt_id", SqlDbType.Int).Value = pt_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }

        public bool RentPaidBatch(int schema_id, string rp_id, int number_of_insert, int name_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeRentPaidBatch", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@rp_id", SqlDbType.NVarChar, 4000).Value = rp_id;
            cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="tu_id"></param>
        /// <param name="rl_id"></param>
        /// <param name="number_of_insert"></param>
        /// <returns></returns>
        public bool RentPaymentBatch(int schema_id, string tu_id, string rl_id, int number_of_insert, int name_id,int role_id)
        {
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prName_AuthorizeRentPaymentBatch", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tu_id", SqlDbType.NVarChar, 4000).Value = tu_id;
            cmd.Parameters.Add("@rl_id", SqlDbType.NVarChar, 4000).Value = rl_id;
            cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = number_of_insert;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="tu_id"></param>
        /// <returns></returns>
        public bool TenantUnit(int schema_id, int tu_id, int name_id, int role_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeTenantUnit", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="tu_id"></param>
        /// <returns></returns>
        public bool TenantUnitArchive(int schema_id, int tu_id, int name_id, int role_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeTenantUnitArchive", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="tu_id"></param>
        /// <returns></returns>
        public bool TenantUnitTempo(int schema_id, int tu_id, int name_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeTenantUnitTempo", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }



        public bool Tenant(int schema_id, int tenant_id, int name_id, int role_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeTenant", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = tenant_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }

        public bool TenantArchive(int schema_id, int tenant_id, int name_id, int role_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeTenantArchive", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = tenant_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="tenant_id"></param>
        /// <returns></returns>
        public bool TenantTempo(int schema_id, int tenant_id, int name_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeTenantTempo", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = tenant_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="tenant_id"></param>
        /// <returns></returns>
        public bool TermsAndConditions(int schema_id, int tt_id, int name_id, int role_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeTermsAndConditions", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tt_id", SqlDbType.Int).Value = tt_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="tenant_id"></param>
        /// <returns></returns>
        public bool TermsAndConditionsArchive(int schema_id, int tt_id, int name_id, int role_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeTermsAndConditionsArchive", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tt_id", SqlDbType.Int).Value = tt_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }

/*
        public bool TenantFolder(int schema_id, int tu_id, int unit_id, int home_id, int tenant_id, int name_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeTenantFolder", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;
            cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = tenant_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }
 */

        public bool Unit(int schema_id, int unit_id, int tu_id, int name_id, int role_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeUnit", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="warehouse_id"></param>
        /// <returns></returns>
        public bool Warehouse(int schema_id, int warehouse_id, int name_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeWarehouse", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@warehouse_id", SqlDbType.Int).Value = warehouse_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="tenant_id"></param>
        /// <returns></returns>
        public bool WorkOrder(int schema_id, int wo_id, int name_id, int role_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prName_AuthorizeWorkOrder", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@wo_id", SqlDbType.Int).Value = wo_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;


            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }



    }

}