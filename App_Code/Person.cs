﻿using System;
using System.Data;
using System.Data.SqlClient;
using sinfoca.login;

/// <summary>
/// Summary description for User
/// </summary>
namespace sinfoca.tiger
{
    public class Person
    {
        private static int userCount;
        private int userBiographyExistForLang;
        private int schemaDescriptionExistForlang;
        private int schema_id;
        private int timezone_id;
        private int person_id;
        private int new_person_id;
        private int role_id;
        private int number_of_insert;
        private int return_id;

        private int business_category_id;
        private int country_id;
        private int region_id;
        private int city_id;


        private string string_category_id = String.Empty;
        private string person_biography = String.Empty;
        private string string_location_id = String.Empty;
        private string string_service_id = String.Empty;
        private string schema_company_name = String.Empty;
        private string schema_lang = String.Empty;
        private string schema_description = String.Empty;
        private string location_name = String.Empty;
        private string schema_status = String.Empty;




        private int monday_working;
        private int tuesday_working;
        private int wednesday_working;
        private int thursday_working;
        private int friday_working;
        private int saturday_working;
        private int sunday_working;


        private int monday_location;
        private int tuesday_location; 
        private int wednesday_location;
        private int thursday_location;
        private int friday_location;
        private int saturday_location;
        private int sunday_location;

        // start time of working hours
        private int monday_start;
        private int tuesday_start;
        private int wednesday_start;
        private int thursday_start;
        private int friday_start;
        private int saturday_start;
        private int sunday_start;

        // end time of working hours
        private int monday_end;
        private int tuesday_end;
        private int wednesday_end;
        private int thursday_end;
        private int friday_end;
        private int saturday_end;
        private int sunday_end;

        private string salt = String.Empty;
        private string hash = String.Empty;

        private string apisalt = String.Empty;
        private string apihash = String.Empty;
        private string apiwebsite = String.Empty;
        private string apisubdomain = String.Empty;

        private int apischema_id;
        private int apiperson_id;

        private string login_user = String.Empty;
        private string schema_login_user = String.Empty;
        private string person_fname = String.Empty;
        private string person_lname = String.Empty;
        private string person_phone = String.Empty;
        private string trace_person_ip = String.Empty;

        private int person_client_completed_appointments;

        private char person_gender;
        private string person_attribute_1;
        private string person_attribute_2;
        private string person_attribute_3;
        private string person_attribute_4;
        private string person_attribute_5;
        private string person_attribute_6;
        private string person_attribute_7;
        private string person_attribute_8;
        private string person_attribute_9;
        private string person_attribute_10;


        public char Person_gender
        {
            get { return person_gender; }
            set { person_gender = value; }
        }

        public string Person_attribute_1
        {
            get { return person_attribute_1; }
            set { person_attribute_1 = value; }
        }

        public string Person_attribute_2
        {
            get { return person_attribute_2; }
            set { person_attribute_2 = value; }
        }

        public string Person_attribute_3
        {
            get { return person_attribute_3; }
            set { person_attribute_3 = value; }
        }

        public string Person_attribute_4
        {
            get { return person_attribute_4; }
            set { person_attribute_4 = value; }
        }

        public string Person_attribute_5
        {
            get { return person_attribute_5; }
            set { person_attribute_5 = value; }
        }

        public string Person_attribute_6
        {
            get { return person_attribute_6; }
            set { person_attribute_6 = value; }
        }

        public string Person_attribute_7
        {
            get { return person_attribute_7; }
            set { person_attribute_7 = value; }
        }

        public string Person_attribute_8
        {
            get { return person_attribute_8; }
            set { person_attribute_8 = value; }
        }

        public string Person_attribute_9
        {
            get { return person_attribute_9; }
            set { person_attribute_9 = value; }
        }

        public string Person_attribute_10
        {
            get { return person_attribute_10; }
            set { person_attribute_10 = value; }
        }

      
        public int Person_id
        {
            get { return person_id; }
            set { person_id = value; }
        }

        public int New_person_id
        {
            get { return new_person_id; }
            set { new_person_id = value; }
        }

        public int TimeZone_id
        {
            get { return timezone_id; }
            set { timezone_id = value; }
        }

        public int Person_client_completed_appointments
        {
            get { return person_client_completed_appointments; }
            set { person_client_completed_appointments = value; }
        }

        public int Return_id
        {
            get { return return_id; }
            set { return_id = value; }
        }

       public int UserBiographyExistForLang
        {
            get { return userBiographyExistForLang; }
            set { userBiographyExistForLang = value; }
        }


       public int SchemaDescriptionExistForlang
       {
           get { return schemaDescriptionExistForlang; }
           set { schemaDescriptionExistForlang = value; }
       }


        public int ApiSchema_id
        {
            get { return apischema_id; }
            set { apischema_id = value; }
        }

        public int Apiperson_id
        {
            get { return apiperson_id; }
            set { apiperson_id = value; }
        }


        public int Role_id
        {
            get { return role_id; }
            set { role_id = value; }
        }

        public int Schema_id
        {
            get { return schema_id; }
            set { schema_id = value; }
        }
        public int Number_of_insert
        {
            get { return number_of_insert; }
            set { number_of_insert = value; }
        }


        public int Business_category_id
        {
            get { return business_category_id; }
            set { business_category_id = value; }
        }

        public int Country_id
        {
            get { return country_id; }
            set { country_id = value; }
        }

        public int Region_id
        {
            get { return region_id; }
            set { region_id = value; }
        }

        public int City_id
        {
            get { return city_id; }
            set { city_id = value; }
        }


        public string Login_user
        {
            get { return login_user; }
            set { login_user = RegEx.getText(Utils.sanitizeString(value, 65)); }
        }



        public string Schema_login_user
        {
            get { return schema_login_user; }
            set { schema_login_user = RegEx.getText(Utils.sanitizeString(value, 65)); }
        }

        public string Person_biography
        {
            get { return person_biography; }
            set { person_biography = RegEx.getText(Utils.sanitizeString(value, 400)); }
        }

        public string String_category_id
        {
            get { return string_category_id; }
            set { string_category_id = RegEx.getText(Utils.sanitizeString(value, 2000)); }
        }


        public string String_location_id
        {
            get { return string_location_id; }
            set { string_location_id = RegEx.getText(Utils.sanitizeString(value, 2000)); }
        }

        public string String_service_id
        {
            get { return string_service_id; }
            set { string_service_id = RegEx.getText(Utils.sanitizeString(value, 2000)); }
        }


        public string Schema_status
        {
            get { return schema_status; }
            set { schema_status = value; }
        }

        public string Schema_lang
        {
            get { return schema_lang; }
            set { schema_lang = value; }
        }


        public string Location_name
        {
            get { return location_name; }
            set { location_name = RegEx.getText(Utils.sanitizeString(value, 100)); }
        }

        public string Schema_company_name
        {
            get { return schema_company_name; }
            set { schema_company_name = RegEx.getText(Utils.sanitizeString(value, 100)); }
        }


        public string Schema_description
        {
            get { return schema_description; }
            set { schema_description = RegEx.getText(Utils.sanitizeString(value, 400)); }
        }

        public string Person_phone
        {
            get { return person_phone; }
            set { person_phone = RegEx.getText(Utils.sanitizeString(value, 20)); }
        }


        public int Monday_working
        {
            get { return monday_working; }
            set { monday_working = value; }
        }

        public int Tuesday_working
        {
            get { return tuesday_working; }
            set { tuesday_working = value; }
        }


        public int Wednesday_working
        {
            get { return wednesday_working; }
            set { wednesday_working = value; }
        }

        public int Thursday_working
        {
            get { return thursday_working; }
            set { thursday_working = value; }
        }

        public int Friday_working
        {
            get { return friday_working; }
            set { friday_working = value; }
        }


        public int Saturday_working
        {
            get { return saturday_working; }
            set { saturday_working = value; }
        }

        public int Sunday_working
        {
            get { return sunday_working; }
            set { sunday_working = value; }
        }

        public int Monday_location
        {
            get { return monday_location; }
            set { monday_location = value; }
        }

        public int Tuesday_location
        {
            get { return tuesday_location; }
            set { tuesday_location = value; }
        }


        public int Wednesday_location
        {
            get { return wednesday_location; }
            set { wednesday_location = value; }
        }

        public int Thursday_location
        {
            get { return thursday_location; }
            set { thursday_location = value; }
        }

        public int Friday_location
        {
            get { return friday_location; }
            set { friday_location = value; }
        }


        public int Saturday_location
        {
            get { return saturday_location; }
            set { saturday_location= value; }
        }

        public int Sunday_location
        {
            get { return sunday_location; }
            set { sunday_location = value; }
        }


        public int Monday_start
        {
            get { return monday_start; }
            set { monday_start = value; }
        }

        public int Tuesday_start
        {
            get { return tuesday_start; }
            set { tuesday_start = value; }
        }


        public int Wednesday_start
        {
            get { return wednesday_start; }
            set { wednesday_start = value; }
        }

        public int Thursday_start
        {
            get { return thursday_start; }
            set { thursday_start = value; }
        }

        public int Friday_start
        {
            get { return friday_start; }
            set { friday_start = value; }
        }


        public int Saturday_start
        {
            get { return saturday_start; }
            set { saturday_start = value; }
        }

        public int Sunday_start
        {
            get { return sunday_start; }
            set { sunday_start = value; }
        }


        public int Monday_end
        {
            get { return monday_end; }
            set { monday_end = value; }
        }

        public int Tuesday_end
        {
            get { return tuesday_end; }
            set { tuesday_end = value; }
        }


        public int Wednesday_end
        {
            get { return wednesday_end; }
            set { wednesday_end = value; }
        }

        public int Thursday_end
        {
            get { return thursday_end; }
            set { thursday_end = value; }
        }

        public int Friday_end
        {
            get { return friday_end; }
            set { friday_end = value; }
        }


        public int Saturday_end
        {
            get { return saturday_end; }
            set { saturday_end = value; }
        }

        public int Sunday_end
        {
            get { return sunday_end; }
            set { sunday_end = value; }
        }


        public string Salt
        {
            get { return  salt; }
            set { salt = RegEx.getText(Utils.sanitizeString(value, 70)); }
        }


        public string Hash
        {
            get { return hash; }
            set { hash = RegEx.getText(Utils.sanitizeString(value, 70)); }
        }

        public string ApiSalt
        {
            get { return apisalt; }
            set { apisalt = RegEx.getText(Utils.sanitizeString(value, 70)); }
        }


        public string ApiHash
        {
            get { return apihash; }
            set {apihash = RegEx.getText(Utils.sanitizeString(value, 70)); }
        }

        public string ApiWebsite
        {
            get { return apiwebsite; }
            set { apiwebsite = RegEx.getText(Utils.sanitizeString(value, 200)); }
        }


        public string ApiSubdomain
        {
            get { return apisubdomain; }
            set { apisubdomain = RegEx.getText(Utils.sanitizeString(value, 200)); }
        }

        public string Person_fname
        {
            get { return person_fname; }
            set { person_fname = RegEx.getText(Utils.sanitizeString(value, 50)); }
        }



        public string Person_lname
        {
            get { return person_lname; }
            set { person_lname = RegEx.getText(Utils.sanitizeString(value, 50)); }

        }

        public string Trace_person_ip
        {
            get { return trace_person_ip; }
            set { trace_person_ip = RegEx.getText(Utils.sanitizeString(value, 15)); }
        }
       

        public Person()
        {
        }




        public DataTable getPersonSearchList(int category, string name)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prUserSearchList2", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable ds = new DataTable();
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }





        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable getFrameLiteAccOwnerAndAssistList(int schema_id, int category, string name, int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prLite_AccOwnerAndAssistList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

                userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);


                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }





        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable getFrameLiteAccOwnerAndStaffList(int schema_id, int category, string name, int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prLite_AccOwnerAndStaffList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

                userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);


                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }



        public DataTable getLiteAccOwnerAndStaffList2(int schema_id, int category, string name)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prLite_AccOwnerAndStaffList2", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;
              

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }








        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable getFrameLiteAccOwnerAndAssistbyLocationList(int schema_id, int category,int location_id, string name, int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prLite_AccOwnerAndAssistbyLocationList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@location_id", SqlDbType.Int).Value = location_id;
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

                userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);


                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable getFrameLiteAccOwnerAndStaffbyLocationList(int schema_id, int category, int location_id, string name, int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prLite_AccOwnerAndStaffbyLocationList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@location_id", SqlDbType.Int).Value = location_id;
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

                userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);


                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }






        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable getFrameUserSearchList(int schema_id ,int category, string name, int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prUserSearchList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                
                da.Fill(dt);

                userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);
                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }


        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable getPersonActivationSearchList(int category, string name, int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prPersonActivationSearchList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable("PersonActivationSearchList");

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@zone_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["zone_id"].ToString());
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

                userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);


                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }





        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable getPersonSearchList(int category, string name, int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prPersonSearchList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable("PersonSearchList");

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@zone_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["zone_id"].ToString());
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                
                da.Fill(dt);

                userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);


                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable getPersonDWSearchList(DateTime booking_date,int day,int begin,int end ,string name, int maximumRows, int startRowIndex,
                                               int country_id, int region_id, int city_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prAvailablePersonList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable("PersonSearchList");

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@booking_date", SqlDbType.DateTime).Value = booking_date;
                cmd.Parameters.Add("@day", SqlDbType.Int).Value = day;
                cmd.Parameters.Add("@begin", SqlDbType.Int).Value = begin;
                cmd.Parameters.Add("@end", SqlDbType.Int).Value =  end;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;
                cmd.Parameters.Add("@country_id", SqlDbType.Int).Value = country_id;
                cmd.Parameters.Add("@region_id", SqlDbType.Int).Value = region_id;
                cmd.Parameters.Add("@city_id", SqlDbType.Int).Value = city_id;
               

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

                userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);


                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }



        public int getPersonDWSearchCount(DateTime booking_date, int day, int begin, int end, string name, int maximumRows, int startRowIndex,
                                             int country_id, int region_id, int city_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prAvailablePersonCount", conn);
            cmd.CommandType = CommandType.StoredProcedure;

           // DataTable dt = new DataTable("PersonSearchCount");

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@booking_date", SqlDbType.DateTime).Value = booking_date;
                cmd.Parameters.Add("@day", SqlDbType.Int).Value = day;
                cmd.Parameters.Add("@begin", SqlDbType.Int).Value = begin;
                cmd.Parameters.Add("@end", SqlDbType.Int).Value = end;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;
                cmd.Parameters.Add("@country_id", SqlDbType.Int).Value = country_id;
                cmd.Parameters.Add("@region_id", SqlDbType.Int).Value = region_id;
                cmd.Parameters.Add("@city_id", SqlDbType.Int).Value = city_id;

                cmd.ExecuteReader();
     
                userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);


                return  userCount;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }





























        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable 
            getAvailablePersonList2(int appointment_id, int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prAvailablePersonList2", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable("PersonSearchList2");

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@appointment_id", SqlDbType.Int).Value = appointment_id;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;
              
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

                userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);


                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }









        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable
            getAvailablePersonList2_New(int appointment_id, int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prAvailablePersonList2_New", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable("PersonSearchList2");

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@appointment_id", SqlDbType.Int).Value = appointment_id;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

                userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);


                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }








        public DataTable 
            getPersonObjectList(string id, string categ)
        {
/************/
    SqlConnection cn = null;
         try
         {
        
        cn = new SqlConnection(Utils.CONN);
        SqlCommand cmd = new SqlCommand("prPersonObjectList", cn);
        cmd.CommandType = CommandType.StoredProcedure;
        // Add the input param to Stor Proc
        cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(id);
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
        cmd.Parameters.Add("@categ", SqlDbType.NVarChar,50).Value = categ; 
            cn.Open();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable("s");
        da.Fill(dt);
        return dt;
          }
         catch (Exception exe)
         {
             Email.sendErrorMail("ERROR",exe.ToString());
             return null;
         }
         finally
         {
             cn.Close();
         } 
         


/************/

        }


        public int getAvailablePersonCount2(int appointment_id, int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prAvailablePersonCount2", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            // DataTable dt = new DataTable("PersonSearchCount");

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@appointment_id", SqlDbType.Int).Value = appointment_id;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;
                cmd.ExecuteReader();

                userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);


                return userCount;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }









 public string personCvPath(string schema_id, string person_id, string person_cv_path)
        {
        SqlConnection conn = null;

        try
        {
            conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prCmsPersonCVUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            
            //Return value 
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            //Add the input params      
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(schema_id);
           cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(person_id);
            cmd.Parameters.Add("@person_cv_path", SqlDbType.NVarChar, 200).Value = person_cv_path; 

            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            //Failure or Success (true = success (1), else fail) 
            return Convert.ToString(cmd.Parameters["@return"].Value);
        }
        finally
        {
              conn.Close();
        }

    }

        public int getPersonActivationSearchCount(int category, string name, int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prPersonActivationSearchCount", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@zone_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["zone_id"].ToString());
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

                return userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);


            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return -1;
            }
            finally
            {
                conn.Close();
            }
        }







        public int getPersonSearchCount(int category, string name, int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prPersonSearchCount", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@zone_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["zone_id"].ToString());
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

               return  userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);


            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return  -1;
            }
            finally
            {
                conn.Close();
            }
        }









        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable getNewClients(int days,int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prNewClients", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@days", SqlDbType.Int).Value = days;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

                userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);


                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }

















        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable getLiteAccOwnerAndAssistList(int category, string name, int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prLite_AccOwnerAndAssistList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

                userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);


                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable getLiteAccOwnerAndStaffList(int category, string name, int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prLite_AccOwnerAndStaffList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

                userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);


                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable getClientUserSearchList(int category, string name, int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prClient_UserSearchList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@client_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                
                da.Fill(dt);

                userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);


                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }





        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable getAssistantUserSearchList(int category, string name, int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prAssistant_UserSearchList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                
                da.Fill(dt);
                userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);
                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }

        public static int getTotalPersonCount(int category, string name)
        {
            return userCount;
        }


        public static int getTotalPersonCount(int days)
        {
            return userCount;
        }

        public static int getTotalPersonCount(int schema_id,int category, string name)
        {
            return userCount;
        }

        public static int getTotalPersonCount(int schema_id, int category,int location_id, string name)
        {
            return userCount;
        }


        public void getUserView(int person_id)
        {


            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prUserView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;
                //execute the insert
                SqlDataReader dr = null;
                dr = cmd.ExecuteReader();

                while (dr.Read() == true)
                {
                    person_fname = dr["person_fname"].ToString();
                    person_lname = dr["person_lname"].ToString();
                    person_phone = dr["person_phone"].ToString();
                    Role_id = Convert.ToInt32(dr["role_id"]);
                    Login_user = dr["login_user"].ToString();
                    person_biography =  dr["person_biography"].ToString();
                    person_client_completed_appointments = Convert.ToInt32(dr["person_client_completed_appointments"]);
                    Schema_company_name = dr["schema_company_name"].ToString();
                    Schema_description = dr["schema_description"].ToString();
                    TimeZone_id = Convert.ToInt32(dr["timezones_id"]);
                }
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }
        }




        public void getUserView(int person_id, string lang)
        {


            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prUserView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;
                cmd.Parameters.Add("@lang", SqlDbType.VarChar,10).Value = lang;

                //execute the insert
                SqlDataReader dr = null;
                dr = cmd.ExecuteReader();

                while (dr.Read() == true)
                {
                    person_fname = dr["person_fname"].ToString();
                    person_lname = dr["person_lname"].ToString();
                    person_phone = dr["person_phone"].ToString();
                    Role_id = Convert.ToInt32(dr["role_id"]);
                    Login_user = dr["login_user"].ToString();
                    person_biography = dr["person_biography"].ToString();
                    person_client_completed_appointments = Convert.ToInt32(dr["person_client_completed_appointments"]);
                    Schema_company_name = dr["schema_company_name"].ToString();
                    Schema_description = dr["schema_description"].ToString();
                    TimeZone_id = Convert.ToInt32(dr["timezones_id"]);
                }
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }
        }

        public void setPersonImgPath(string person_id, string person_img_path)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prPubPersonSetImgPath", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(person_id);
                cmd.Parameters.Add("@person_img_path", SqlDbType.VarChar, 200).Value = person_img_path;
                cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = person_id;
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();

                //----------------------------------------------------------------------------------------
                cmd.ExecuteReader();

                Return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);



            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }

        }



        public DataTable getPersonView(int person_id)
        {


            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prPersonView", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable("personview");
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;
                //execute the insert

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;

            }
            finally
            {
                conn.Close();
            }
        }


        public DataTable getAppointmentPersonView(int appointment_id)
        {


            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prAppointmentPersonView", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable("appointmentpersonview");
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@appointment_id", SqlDbType.Int).Value = appointment_id;
                //execute the insert

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;

            }
            finally
            {
                conn.Close();
            }
        }

        public DataTable getAppointmentPersonTelView(string tel)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prAppointmentPersonTelView2", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                cmd.Parameters.Add("@tel", SqlDbType.VarChar, 20).Value = tel;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }


        public DataTable getAppointmentPersonTelView(string tel,int appointment_id)
        {


            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prAppointmentPersonTelView", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable("appointmentpersontelview");
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@tel", SqlDbType.VarChar,20).Value = tel;
                cmd.Parameters.Add("@appointment_id", SqlDbType.Int).Value = appointment_id;
                //execute the insert

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;

            }
            finally
            {
                conn.Close();
            }
        }

        public DataTable getPersonAddressView(int person_id,int address_id)
        {


            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prPersonAddressView", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable("personaddressview");
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;
                cmd.Parameters.Add("@address_id", SqlDbType.Int).Value = address_id;
              //execute the insert

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;

            }
            finally
            {
                conn.Close();
            }
        }


        public int setPersonAddressUpdate(int country_id, int region_id, int city_id, int address_id,string address,
                                                string pc, decimal latitude, decimal longitude)
        {


            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prPersonAddressUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable("personaddressupdate");
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@country_id", SqlDbType.Int).Value = country_id;
                cmd.Parameters.Add("@region_id", SqlDbType.Int).Value = region_id;
                cmd.Parameters.Add("@city_id", SqlDbType.Int).Value = city_id;
                cmd.Parameters.Add("@address_id", SqlDbType.Int).Value = address_id;
                cmd.Parameters.Add("@address", SqlDbType.NVarChar, 50).Value = address;
                cmd.Parameters.Add("@pc", SqlDbType.NVarChar, 20).Value = pc;
                cmd.Parameters.Add("@latitude", SqlDbType.Decimal).Value = latitude;
                cmd.Parameters.Add("@longitude", SqlDbType.Decimal).Value = longitude;
           
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 50).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
          
                //execute the insert

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                return 1;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return 0;

            }
            finally
            {
                conn.Close();
            }
        }




        public DataTable getPersonJobView(int person_id)
        {


            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prPersonJobView", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable("personview");
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;
                //execute the insert

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;

            }
            finally
            {
                conn.Close();
            }
        }


        public DataTable getPersonWorkingHoursView(int person_id)
        {


            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prPersonWorkingHoursView", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable("personworkinghoursview");
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;
                //execute the insert

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;

            }
            finally
            {
                conn.Close();
            }
        }




        public DataTable getPersonActivationView(int person_id)
        {


            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prPersonActivationView", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable("personactivationview");
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;
                //execute the insert

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;

            }
            finally
            {
                conn.Close();
            }
        }


        public bool setCompanyUpdate()
        {
            bool result = false;

            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prCompanyUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@schema_company_name", SqlDbType.NVarChar, 100).Value = schema_company_name;
                cmd.Parameters.Add("@schema_description", SqlDbType.VarChar, 400).Value = schema_description;
                cmd.Parameters.Add("@timezones_id", SqlDbType.Int).Value = timezone_id;             
                cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();

                //execute the insert

                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                //Failure or Success (true = success (0), else fail) 
                if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 0)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }

            return result;

        }


        public DataTable getEmployeZoneList(int person_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prEmployeZoneList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            DataTable dt = new DataTable("EmployeZoneList");

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id ;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }


        public DataTable getEmployeZoneList()
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prEmployeZoneList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            DataTable dt = new DataTable("EmployeZoneList");

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString()); ;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }




        public void getUserBiographyExistForLang(int person_id, string lang)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd2 = new SqlCommand("prUserBiographyExistForLang", conn);

            cmd2.CommandType = CommandType.StoredProcedure;


            conn.Open();
            //Add the params
            cmd2.Parameters.Add("@UserBiographyExistForLang", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
            cmd2.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;
            cmd2.Parameters.Add("@lang", SqlDbType.VarChar, 10).Value = lang;
            try
            {

                SqlDataReader dr = null;
                dr = cmd2.ExecuteReader();

                UserBiographyExistForLang = Convert.ToInt32(cmd2.Parameters["@UserBiographyExistForLang"].Value);
            }

            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }

            finally
            {
                conn.Close();
            }

        }


        public string setPersonWorkingHoursUpdate2(string person_id, DataTable jobcateglist,
                                              bool mo_1, bool mo_2, bool mo_3, bool mo_4, bool mo_5, bool mo_6,
                                              bool mo_7, bool mo_8, bool mo_9, bool mo_10, bool mo_11, bool mo_12,
                                              bool tu_1, bool tu_2, bool tu_3, bool tu_4, bool tu_5, bool tu_6,
                                              bool tu_7, bool tu_8, bool tu_9, bool tu_10, bool tu_11, bool tu_12,
                                              bool we_1, bool we_2, bool we_3, bool we_4, bool we_5, bool we_6,
                                              bool we_7, bool we_8, bool we_9, bool we_10, bool we_11, bool we_12,
                                              bool th_1, bool th_2, bool th_3, bool th_4, bool th_5, bool th_6,
                                              bool th_7, bool th_8, bool th_9, bool th_10, bool th_11, bool th_12,
                                              bool fr_1, bool fr_2, bool fr_3, bool fr_4, bool fr_5, bool fr_6,
                                              bool fr_7, bool fr_8, bool fr_9, bool fr_10, bool fr_11, bool fr_12,
                                              bool sa_1, bool sa_2, bool sa_3, bool sa_4, bool sa_5, bool sa_6,
                                              bool sa_7, bool sa_8, bool sa_9, bool sa_10, bool sa_11, bool sa_12,
                                              bool su_1, bool su_2, bool su_3, bool su_4, bool su_5, bool su_6,
                                              bool su_7, bool su_8, bool su_9, bool su_10, bool su_11, bool su_12)
        {
            SqlConnection cn = null;

            try
            {
                cn = new SqlConnection(Utils.CONN);
                SqlCommand cmd = new SqlCommand("prPersonWorkingHoursUpdate2", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();

                //Return value 
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                //Add the input params      
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(person_id);
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());


                //monday
                cmd.Parameters.Add("@mon_1", SqlDbType.Bit).Value = mo_1;//0am-2am
                cmd.Parameters.Add("@mon_2", SqlDbType.Bit).Value = mo_2;//2am-4am
                cmd.Parameters.Add("@mon_3", SqlDbType.Bit).Value = mo_3;//4am-6am
                cmd.Parameters.Add("@mon_4", SqlDbType.Bit).Value = mo_4;//6am-8am
                cmd.Parameters.Add("@mon_5", SqlDbType.Bit).Value = mo_5;//8am-10am
                cmd.Parameters.Add("@mon_6", SqlDbType.Bit).Value = mo_6;//10am-12pm
                cmd.Parameters.Add("@mon_7", SqlDbType.Bit).Value = mo_7;//12pm-2pm
                cmd.Parameters.Add("@mon_8", SqlDbType.Bit).Value = mo_8;//2pm-4pm
                cmd.Parameters.Add("@mon_9", SqlDbType.Bit).Value = mo_9;//4pm-6pm
                cmd.Parameters.Add("@mon_10", SqlDbType.Bit).Value = mo_10;//6pm-8pm
                cmd.Parameters.Add("@mon_11", SqlDbType.Bit).Value = mo_11;//8pm-10pm
                cmd.Parameters.Add("@mon_12", SqlDbType.Bit).Value = mo_12;//10pm-12am
                //tuesday
                cmd.Parameters.Add("@tue_1", SqlDbType.Bit).Value = tu_1;//0am-2am
                cmd.Parameters.Add("@tue_2", SqlDbType.Bit).Value = tu_2;//2am-4am
                cmd.Parameters.Add("@tue_3", SqlDbType.Bit).Value = tu_3;//4am-6am
                cmd.Parameters.Add("@tue_4", SqlDbType.Bit).Value = tu_4;//6am-8am
                cmd.Parameters.Add("@tue_5", SqlDbType.Bit).Value = tu_5;//8am-10am
                cmd.Parameters.Add("@tue_6", SqlDbType.Bit).Value = tu_6;//10am-12pm
                cmd.Parameters.Add("@tue_7", SqlDbType.Bit).Value = tu_7;//12pm-2pm
                cmd.Parameters.Add("@tue_8", SqlDbType.Bit).Value = tu_8;//2pm-4pm
                cmd.Parameters.Add("@tue_9", SqlDbType.Bit).Value = tu_9;//4pm-6pm
                cmd.Parameters.Add("@tue_10", SqlDbType.Bit).Value = tu_10;//6pm-8pm
                cmd.Parameters.Add("@tue_11", SqlDbType.Bit).Value = tu_11;//8pm-10pm
                cmd.Parameters.Add("@tue_12", SqlDbType.Bit).Value = tu_12;//10pm-12am
                //wednesday
                cmd.Parameters.Add("@wed_1", SqlDbType.Bit).Value = we_1;//0am-2am
                cmd.Parameters.Add("@wed_2", SqlDbType.Bit).Value = we_2;//2am-4am
                cmd.Parameters.Add("@wed_3", SqlDbType.Bit).Value = we_3;//4am-6am
                cmd.Parameters.Add("@wed_4", SqlDbType.Bit).Value = we_4;//6am-8am
                cmd.Parameters.Add("@wed_5", SqlDbType.Bit).Value = we_5;//8am-10am
                cmd.Parameters.Add("@wed_6", SqlDbType.Bit).Value = we_6;//10am-12pm
                cmd.Parameters.Add("@wed_7", SqlDbType.Bit).Value = we_7;//12pm-2pm
                cmd.Parameters.Add("@wed_8", SqlDbType.Bit).Value = we_8;//2pm-4pm
                cmd.Parameters.Add("@wed_9", SqlDbType.Bit).Value = we_9;//4pm-6pm
                cmd.Parameters.Add("@wed_10", SqlDbType.Bit).Value = we_10;//6pm-8pm
                cmd.Parameters.Add("@wed_11", SqlDbType.Bit).Value = we_11;//8pm-10pm
                cmd.Parameters.Add("@wed_12", SqlDbType.Bit).Value = we_12;//10pm-12am
                //thursday
                cmd.Parameters.Add("@thu_1", SqlDbType.Bit).Value = th_1;//0am-2am
                cmd.Parameters.Add("@thu_2", SqlDbType.Bit).Value = th_2;//2am-4am
                cmd.Parameters.Add("@thu_3", SqlDbType.Bit).Value = th_3;//4am-6am
                cmd.Parameters.Add("@thu_4", SqlDbType.Bit).Value = th_4;//6am-8am
                cmd.Parameters.Add("@thu_5", SqlDbType.Bit).Value = th_5;//8am-10am
                cmd.Parameters.Add("@thu_6", SqlDbType.Bit).Value = th_6;//10am-12pm
                cmd.Parameters.Add("@thu_7", SqlDbType.Bit).Value = th_7;//12pm-2pm
                cmd.Parameters.Add("@thu_8", SqlDbType.Bit).Value = th_8;//2pm-4pm
                cmd.Parameters.Add("@thu_9", SqlDbType.Bit).Value = th_9;//4pm-6pm
                cmd.Parameters.Add("@thu_10", SqlDbType.Bit).Value = th_10;//6pm-8pm
                cmd.Parameters.Add("@thu_11", SqlDbType.Bit).Value = th_11;//8pm-10pm
                cmd.Parameters.Add("@thu_12", SqlDbType.Bit).Value = th_12;//10pm-12am
                //friday
                cmd.Parameters.Add("@fri_1", SqlDbType.Bit).Value = fr_1;//0am-2am
                cmd.Parameters.Add("@fri_2", SqlDbType.Bit).Value = fr_2;//2am-4am
                cmd.Parameters.Add("@fri_3", SqlDbType.Bit).Value = fr_3;//4am-6am
                cmd.Parameters.Add("@fri_4", SqlDbType.Bit).Value = fr_4;//6am-8am
                cmd.Parameters.Add("@fri_5", SqlDbType.Bit).Value = fr_5;//8am-10am
                cmd.Parameters.Add("@fri_6", SqlDbType.Bit).Value = fr_6;//10am-12pm
                cmd.Parameters.Add("@fri_7", SqlDbType.Bit).Value = fr_7;//12pm-2pm
                cmd.Parameters.Add("@fri_8", SqlDbType.Bit).Value = fr_8;//2pm-4pm
                cmd.Parameters.Add("@fri_9", SqlDbType.Bit).Value = fr_9;//4pm-6pm
                cmd.Parameters.Add("@fri_10", SqlDbType.Bit).Value = fr_10;//6pm-8pm
                cmd.Parameters.Add("@fri_11", SqlDbType.Bit).Value = fr_11;//8pm-10pm
                cmd.Parameters.Add("@fri_12", SqlDbType.Bit).Value = fr_12;//10pm-12am
                //saturday
                cmd.Parameters.Add("@sat_1", SqlDbType.Bit).Value = sa_1;//0am-2am
                cmd.Parameters.Add("@sat_2", SqlDbType.Bit).Value = sa_2;//2am-4am
                cmd.Parameters.Add("@sat_3", SqlDbType.Bit).Value = sa_3;//4am-6am
                cmd.Parameters.Add("@sat_4", SqlDbType.Bit).Value = sa_4;//6am-8am
                cmd.Parameters.Add("@sat_5", SqlDbType.Bit).Value = sa_5;//8am-10am
                cmd.Parameters.Add("@sat_6", SqlDbType.Bit).Value = sa_6;//10am-12pm
                cmd.Parameters.Add("@sat_7", SqlDbType.Bit).Value = sa_7;//12pm-2pm
                cmd.Parameters.Add("@sat_8", SqlDbType.Bit).Value = sa_8;//2pm-4pm
                cmd.Parameters.Add("@sat_9", SqlDbType.Bit).Value = sa_9;//4pm-6pm
                cmd.Parameters.Add("@sat_10", SqlDbType.Bit).Value = sa_10;//6pm-8pm
                cmd.Parameters.Add("@sat_11", SqlDbType.Bit).Value = sa_11;//8pm-10pm
                cmd.Parameters.Add("@sat_12", SqlDbType.Bit).Value = sa_12;//10pm-12am
                //sunday
                cmd.Parameters.Add("@sun_1", SqlDbType.Bit).Value = su_1;//0am-2am
                cmd.Parameters.Add("@sun_2", SqlDbType.Bit).Value = su_2;//2am-4am
                cmd.Parameters.Add("@sun_3", SqlDbType.Bit).Value = su_3;//4am-6am
                cmd.Parameters.Add("@sun_4", SqlDbType.Bit).Value = su_4;//6am-8am
                cmd.Parameters.Add("@sun_5", SqlDbType.Bit).Value = su_5;//8am-10am
                cmd.Parameters.Add("@sun_6", SqlDbType.Bit).Value = su_6;//10am-12pm
                cmd.Parameters.Add("@sun_7", SqlDbType.Bit).Value = su_7;//12pm-2pm
                cmd.Parameters.Add("@sun_8", SqlDbType.Bit).Value = su_8;//2pm-4pm
                cmd.Parameters.Add("@sun_9", SqlDbType.Bit).Value = su_9;//4pm-6pm
                cmd.Parameters.Add("@sun_10", SqlDbType.Bit).Value = su_10;//6pm-8pm
                cmd.Parameters.Add("@sun_11", SqlDbType.Bit).Value = su_11;//8pm-10pm
                cmd.Parameters.Add("@sun_12", SqlDbType.Bit).Value = su_12;//10pm-12am

                cmd.Parameters.Add("@jobcateglist", SqlDbType.Structured).Value = jobcateglist;


                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                //Failure or Success (true = success (1), else fail) 
                return Convert.ToString(cmd.Parameters["@return"].Value);
            }
            finally
            {
                cn.Close();
            }

        }

   public string setPersonWorkingHoursUpdate( string person_id, DataTable jobcateglist,bool mo_m, bool mo_d, bool mo_n, bool tu_m, 
                                              bool tu_d,bool tu_n, bool we_m,bool we_d, bool we_n, bool th_m, bool th_d, bool th_n, 
                                              bool fr_m, bool fr_d, bool fr_n, bool sa_m, bool sa_d, bool sa_n, bool su_m, bool su_d, 
                                              bool su_n)
        {
            SqlConnection cn = null;

            try
            {
                cn = new SqlConnection(Utils.CONN);
                SqlCommand cmd = new SqlCommand("prPersonWorkingHoursUpdate", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cn.Open();

                //Return value 
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                //Add the input params      
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value =Convert.ToInt32( person_id);
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());


                //monday
                cmd.Parameters.Add("@mon_mo", SqlDbType.Bit).Value = mo_m;//6am-16pm
                cmd.Parameters.Add("@mon_an", SqlDbType.Bit).Value = mo_d;//16pm-00am
                cmd.Parameters.Add("@mon_nt", SqlDbType.Bit).Value = mo_n;//00am-6am
                //tuesday
                cmd.Parameters.Add("@tue_mo", SqlDbType.Bit).Value = tu_m;//6am-16pm
                cmd.Parameters.Add("@tue_an", SqlDbType.Bit).Value = tu_d;//16pm-00am
                cmd.Parameters.Add("@tue_nt", SqlDbType.Bit).Value = tu_n;//00am-6am
                //wednesday
                cmd.Parameters.Add("@wed_mo", SqlDbType.Bit).Value = we_m;//6am-16pm
                cmd.Parameters.Add("@wed_an", SqlDbType.Bit).Value = we_d;//16pm-00am
                cmd.Parameters.Add("@wed_nt", SqlDbType.Bit).Value = we_n;//00am-6am
                //thursday
                cmd.Parameters.Add("@thu_mo", SqlDbType.Bit).Value = th_m;//6am-16pm
                cmd.Parameters.Add("@thu_an", SqlDbType.Bit).Value = th_d;//16pm-00am
                cmd.Parameters.Add("@thu_nt", SqlDbType.Bit).Value = th_n;//00am-6am
                //friday
                cmd.Parameters.Add("@fri_mo", SqlDbType.Bit).Value = fr_m;//6am-16pm
                cmd.Parameters.Add("@fri_an", SqlDbType.Bit).Value = fr_d;//16pm-00am
                cmd.Parameters.Add("@fri_nt", SqlDbType.Bit).Value = fr_n;//00am-6am
                //saturday
                cmd.Parameters.Add("@sat_mo", SqlDbType.Bit).Value = sa_m;//6am-16pm
                cmd.Parameters.Add("@sat_an", SqlDbType.Bit).Value = sa_d;//16pm-00am
                cmd.Parameters.Add("@sat_nt", SqlDbType.Bit).Value = sa_n;//00am-6am
                //sunday
                cmd.Parameters.Add("@sun_mo", SqlDbType.Bit).Value = su_m;//6am-16pm
                cmd.Parameters.Add("@sun_an", SqlDbType.Bit).Value = su_d;//16pm-00am
                cmd.Parameters.Add("@sun_nt", SqlDbType.Bit).Value = su_n;//00am-6am

                cmd.Parameters.Add("@jobcateglist", SqlDbType.Structured).Value = jobcateglist;
             

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                //Failure or Success (true = success (1), else fail) 
                return Convert.ToString(cmd.Parameters["@return"].Value);
            }
            finally
            {
                 cn.Close();
            }

        }

        public void getUserBiographyUpdate(int person_id, string lang)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd2 = new SqlCommand("prUserBiographyUpdate", conn);

            cmd2.CommandType = CommandType.StoredProcedure;

            try
            {

            conn.Open();
            //Add the params
            cmd2.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd2.Parameters.Add("@person_biography", SqlDbType.VarChar, 400).Value = person_biography;
            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
            cmd2.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;
            cmd2.Parameters.Add("@lang", SqlDbType.VarChar, 10).Value = lang;
            cmd2.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
            cmd2.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();
  
             //execute the insert
            cmd2.ExecuteReader();

            if (Convert.ToInt32(cmd2.Parameters["@return_id"].Value) == 0)
                Return_id = 0;   
               
            }

            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }

            finally
            {
                conn.Close();
            }

        }


        public void setNewAccountOwner(string companyname,string mobile, int country_id,int region_id,int city_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prNewAccountOwner", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@new_person_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();

                //----------------------------------------------------------------------------------------
                cmd.Parameters.Add("@person_lname", SqlDbType.NVarChar, 50).Value = person_lname;
                cmd.Parameters.Add("@person_fname", SqlDbType.NVarChar, 50).Value = person_fname;
                cmd.Parameters.Add("@login_user", SqlDbType.NVarChar, 65).Value = login_user;
                cmd.Parameters.Add("@company_name", SqlDbType.NVarChar, 100).Value = companyname;
                cmd.Parameters.Add("@mobile", SqlDbType.NVarChar, 65).Value = mobile;
                cmd.Parameters.Add("@country_id", SqlDbType.Int).Value =  country_id;
                cmd.Parameters.Add("@region_id", SqlDbType.Int).Value = region_id;
                cmd.Parameters.Add("@city_id", SqlDbType.Int).Value = city_id;
              
                cmd.Parameters.Add("@schema_lang", SqlDbType.VarChar,50).Value = schema_lang;




                //execute the insert
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                Person_id = Convert.ToInt32(cmd.Parameters["@new_person_id"].Value);
                Schema_id = Convert.ToInt32(cmd.Parameters["@schema_id"].Value);

                Email.sendSESMailHtml("\"Staff Plus Support\" <noreply@asmsplus.com>", "jocestan@hotmail.com"/*u.Login_user*/, "test inside CLASS person_id= " + Person_id.ToString(), "");
                Email.sendSESMailHtml("\"Staff Plus Support\" <noreply@asmsplus.com>", "jocestan@hotmail.com"/*u.Login_user*/, "test inside CLASS SCHEMA_id= " + Schema_id.ToString(), "");
                 

            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }


        }




        public void setNewPerson2()
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prRoleNewWebAccount2", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = Role_id;
                cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();
                //   cmd.Parameters.Add("@person_trace_user_agent", SqlDbType.NVarChar, 200).Value = System.Web.HttpContext.Current.Request.UserAgent;
                //   cmd.Parameters.Add("@person_trace_browser", SqlDbType.NVarChar, 100).Value = System.Web.HttpContext.Current.Request.Browser.Browser;

                //----------------------------------------------------------------------------------------
                //  cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = Number_of_insert;
                //  cmd.Parameters.Add("@string_location_id", SqlDbType.NVarChar, 2000).Value = String_location_id;
                cmd.Parameters.Add("@person_lname", SqlDbType.NVarChar, 50).Value = person_lname;
                cmd.Parameters.Add("@person_fname", SqlDbType.NVarChar, 50).Value = person_fname;

                cmd.Parameters.Add("@person_attribute_1", SqlDbType.NVarChar, 100).Value = person_attribute_1;
                cmd.Parameters.Add("@person_attribute_2", SqlDbType.NVarChar, 100).Value = person_attribute_2;
                cmd.Parameters.Add("@person_attribute_3", SqlDbType.NVarChar, 100).Value = person_attribute_3;
                cmd.Parameters.Add("@person_attribute_4", SqlDbType.NVarChar, 100).Value = person_attribute_4;
                cmd.Parameters.Add("@person_attribute_5", SqlDbType.NVarChar, 100).Value = person_attribute_5;
                cmd.Parameters.Add("@person_attribute_6", SqlDbType.NVarChar, 100).Value = person_attribute_6;
                cmd.Parameters.Add("@person_attribute_7", SqlDbType.NVarChar, 100).Value = person_attribute_7;
                cmd.Parameters.Add("@person_attribute_8", SqlDbType.NVarChar, 100).Value = person_attribute_8;
                cmd.Parameters.Add("@person_attribute_9", SqlDbType.NVarChar, 100).Value = person_attribute_9;
                cmd.Parameters.Add("@person_attribute_10", SqlDbType.NVarChar, 100).Value = person_attribute_10;

                /* cmd.Parameters.Add("@person_gender", SqlDbType.Char, 1).Value = person_gender; */

                cmd.Parameters.Add("@login_user", SqlDbType.NVarChar, 65).Value = Login_user;
                cmd.Parameters.Add("@login_salt", SqlDbType.NVarChar, 70).Value = Salt;
                cmd.Parameters.Add("@login_hash", SqlDbType.NVarChar, 70).Value = Hash;
                //execute the insert
                cmd.ExecuteReader();

                Return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);

            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }


        }




        public void setActivateNewPerson(int person_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prRoleActivateNewWebAccount", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = Role_id;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;
                cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();
                //   cmd.Parameters.Add("@person_trace_user_agent", SqlDbType.NVarChar, 200).Value = System.Web.HttpContext.Current.Request.UserAgent;
                //   cmd.Parameters.Add("@person_trace_browser", SqlDbType.NVarChar, 100).Value = System.Web.HttpContext.Current.Request.Browser.Browser;

                //----------------------------------------------------------------------------------------
                //  cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = Number_of_insert;
                //  cmd.Parameters.Add("@string_location_id", SqlDbType.NVarChar, 2000).Value = String_location_id;
                cmd.Parameters.Add("@person_lname", SqlDbType.NVarChar, 50).Value = person_lname;
                cmd.Parameters.Add("@person_fname", SqlDbType.NVarChar, 50).Value = person_fname;
                cmd.Parameters.Add("@person_phone", SqlDbType.NVarChar, 50).Value = person_phone;

                cmd.Parameters.Add("@person_attribute_1", SqlDbType.NVarChar, 100).Value = person_attribute_1;
                cmd.Parameters.Add("@person_attribute_2", SqlDbType.NVarChar, 100).Value = person_attribute_2;
                cmd.Parameters.Add("@person_attribute_3", SqlDbType.NVarChar, 100).Value = person_attribute_3;
                cmd.Parameters.Add("@person_attribute_4", SqlDbType.NVarChar, 100).Value = person_attribute_4;
                cmd.Parameters.Add("@person_attribute_5", SqlDbType.NVarChar, 100).Value = person_attribute_5;
                cmd.Parameters.Add("@person_attribute_6", SqlDbType.NVarChar, 100).Value = person_attribute_6;
                cmd.Parameters.Add("@person_attribute_7", SqlDbType.NVarChar, 100).Value = person_attribute_7;
                cmd.Parameters.Add("@person_attribute_8", SqlDbType.NVarChar, 100).Value = person_attribute_8;
                cmd.Parameters.Add("@person_attribute_9", SqlDbType.NVarChar, 100).Value = person_attribute_9;
                cmd.Parameters.Add("@person_attribute_10", SqlDbType.NVarChar, 100).Value = person_attribute_10;

                /* cmd.Parameters.Add("@person_gender", SqlDbType.Char, 1).Value = person_gender; */

                cmd.Parameters.Add("@login_user", SqlDbType.NVarChar, 65).Value = Login_user;
                cmd.Parameters.Add("@login_salt", SqlDbType.NVarChar, 70).Value = Salt;
                cmd.Parameters.Add("@login_hash", SqlDbType.NVarChar, 70).Value = Hash;
                //execute the insert
                cmd.ExecuteReader();

                Return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);

            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }


        }




        public void setNewPerson(DataTable zonelist)
        {
                SqlConnection conn = new SqlConnection(Utils.CONN);
                SqlCommand cmd = new SqlCommand("prRoleNewWebAccount", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    conn.Open();
                    //Add the params
                    cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                    cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = Role_id;
                    cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                    cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();
                    cmd.Parameters.Add("@zonelist", SqlDbType.Structured).Value = zonelist;
                    //   cmd.Parameters.Add("@person_trace_user_agent", SqlDbType.NVarChar, 200).Value = System.Web.HttpContext.Current.Request.UserAgent;
                 //   cmd.Parameters.Add("@person_trace_browser", SqlDbType.NVarChar, 100).Value = System.Web.HttpContext.Current.Request.Browser.Browser;

                    //----------------------------------------------------------------------------------------
                    //  cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = Number_of_insert;
                    //  cmd.Parameters.Add("@string_location_id", SqlDbType.NVarChar, 2000).Value = String_location_id;
                    cmd.Parameters.Add("@person_lname", SqlDbType.NVarChar, 50).Value = person_lname;
                    cmd.Parameters.Add("@person_fname", SqlDbType.NVarChar, 50).Value = person_fname;
 
                    cmd.Parameters.Add("@person_attribute_1", SqlDbType.NVarChar, 100).Value = person_attribute_1;
                    cmd.Parameters.Add("@person_attribute_2", SqlDbType.NVarChar, 100).Value = person_attribute_2;
                    cmd.Parameters.Add("@person_attribute_3", SqlDbType.NVarChar, 100).Value = person_attribute_3;
                    cmd.Parameters.Add("@person_attribute_4", SqlDbType.NVarChar, 100).Value = person_attribute_4;
                    cmd.Parameters.Add("@person_attribute_5", SqlDbType.NVarChar, 100).Value = person_attribute_5;
                    cmd.Parameters.Add("@person_attribute_6", SqlDbType.NVarChar, 100).Value = person_attribute_6;
                    cmd.Parameters.Add("@person_attribute_7", SqlDbType.NVarChar, 100).Value = person_attribute_7;
                    cmd.Parameters.Add("@person_attribute_8", SqlDbType.NVarChar, 100).Value = person_attribute_8;
                    cmd.Parameters.Add("@person_attribute_9", SqlDbType.NVarChar, 100).Value = person_attribute_9;
                    cmd.Parameters.Add("@person_attribute_10", SqlDbType.NVarChar, 100).Value = person_attribute_10;
      
                 /* cmd.Parameters.Add("@person_gender", SqlDbType.Char, 1).Value = person_gender; */

                    cmd.Parameters.Add("@login_user", SqlDbType.NVarChar, 65).Value = Login_user;
                    cmd.Parameters.Add("@login_salt", SqlDbType.NVarChar, 70).Value = Salt;
                    cmd.Parameters.Add("@login_hash", SqlDbType.NVarChar, 70).Value = Hash;
                    //execute the insert
                    cmd.ExecuteReader();

                    Return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);
                    
                }
                catch (Exception ex)
                {
                    sinfoca.error.ErrorLog.addErrorLog(ex);
                }
                finally
                {
                 conn.Close();
                }

           
        }


        public void setPersonUpdate2(int person_id, string person_lang,string person_status)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prPersonUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;
                cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();
                //   cmd.Parameters.Add("@person_trace_user_agent", SqlDbType.NVarChar, 200).Value = System.Web.HttpContext.Current.Request.UserAgent;
                //   cmd.Parameters.Add("@person_trace_browser", SqlDbType.NVarChar, 100).Value = System.Web.HttpContext.Current.Request.Browser.Browser;

                //----------------------------------------------------------------------------------------
                //  cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = Number_of_insert;
                //  cmd.Parameters.Add("@string_location_id", SqlDbType.NVarChar, 2000).Value = String_location_id;
                cmd.Parameters.Add("@person_lname", SqlDbType.NVarChar, 50).Value = person_lname;
                cmd.Parameters.Add("@person_fname", SqlDbType.NVarChar, 50).Value = person_fname;
                cmd.Parameters.Add("@person_phone", SqlDbType.NVarChar, 50).Value = person_phone;
                cmd.Parameters.Add("@person_lang", SqlDbType.NVarChar, 50).Value = person_lang;
                cmd.Parameters.Add("@person_email", SqlDbType.NVarChar, 200).Value = login_user;
                cmd.Parameters.Add("@person_status", SqlDbType.NVarChar, 20).Value = person_status;

                cmd.Parameters.Add("@person_attribute_1", SqlDbType.NVarChar, 100).Value = person_attribute_1;
                cmd.Parameters.Add("@person_attribute_2", SqlDbType.NVarChar, 100).Value = person_attribute_2;
                cmd.Parameters.Add("@person_attribute_3", SqlDbType.NVarChar, 100).Value = person_attribute_3;
                cmd.Parameters.Add("@person_attribute_4", SqlDbType.NVarChar, 100).Value = person_attribute_4;
                cmd.Parameters.Add("@person_attribute_5", SqlDbType.NVarChar, 100).Value = person_attribute_5;
                cmd.Parameters.Add("@person_attribute_6", SqlDbType.NVarChar, 100).Value = person_attribute_6;
                cmd.Parameters.Add("@person_attribute_7", SqlDbType.NVarChar, 100).Value = person_attribute_7;
                cmd.Parameters.Add("@person_attribute_8", SqlDbType.NVarChar, 100).Value = person_attribute_8;
                cmd.Parameters.Add("@person_attribute_9", SqlDbType.NVarChar, 100).Value = person_attribute_9;
                cmd.Parameters.Add("@person_attribute_10", SqlDbType.NVarChar, 100).Value = person_attribute_10;

                /* cmd.Parameters.Add("@person_gender", SqlDbType.Char, 1).Value = person_gender; */

                //execute the insert
                cmd.ExecuteReader();

                Return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);

            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }


        }

        public void setPersonUpdate(DataTable zonelist, int person_id, string paypal_email,string status,string emp_type_code)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prPersonUpdateZone", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = Role_id;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value =person_id;
                cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@zonelist", SqlDbType.Structured).Value = zonelist;
                //   cmd.Parameters.Add("@person_trace_user_agent", SqlDbType.NVarChar, 200).Value = System.Web.HttpContext.Current.Request.UserAgent;
                //   cmd.Parameters.Add("@person_trace_browser", SqlDbType.NVarChar, 100).Value = System.Web.HttpContext.Current.Request.Browser.Browser;

                //----------------------------------------------------------------------------------------
                //  cmd.Parameters.Add("@number_of_insert", SqlDbType.Int).Value = Number_of_insert;
                //  cmd.Parameters.Add("@string_location_id", SqlDbType.NVarChar, 2000).Value = String_location_id;
                cmd.Parameters.Add("@status", SqlDbType.NVarChar, 5).Value = status;
                cmd.Parameters.Add("@person_lname", SqlDbType.NVarChar, 50).Value = person_lname;
                cmd.Parameters.Add("@emp_type_code", SqlDbType.NVarChar, 10).Value = emp_type_code;
                cmd.Parameters.Add("@person_fname", SqlDbType.NVarChar, 50).Value = person_fname;
                cmd.Parameters.Add("@person_phone", SqlDbType.NVarChar, 50).Value = person_phone;
                cmd.Parameters.Add("@paypal_email", SqlDbType.NVarChar, 65).Value = paypal_email;
                cmd.Parameters.Add("@person_attribute_1", SqlDbType.NVarChar, 100).Value = person_attribute_1;
                cmd.Parameters.Add("@person_attribute_2", SqlDbType.NVarChar, 100).Value = person_attribute_2;
                cmd.Parameters.Add("@person_attribute_3", SqlDbType.NVarChar, 100).Value = person_attribute_3;
                cmd.Parameters.Add("@person_attribute_4", SqlDbType.NVarChar, 100).Value = person_attribute_4;
                cmd.Parameters.Add("@person_attribute_5", SqlDbType.NVarChar, 100).Value = person_attribute_5;
                cmd.Parameters.Add("@person_attribute_6", SqlDbType.NVarChar, 100).Value = person_attribute_6;
                cmd.Parameters.Add("@person_attribute_7", SqlDbType.NVarChar, 100).Value = person_attribute_7;
                cmd.Parameters.Add("@person_attribute_8", SqlDbType.NVarChar, 100).Value = person_attribute_8;
                cmd.Parameters.Add("@person_attribute_9", SqlDbType.NVarChar, 100).Value = person_attribute_9;
                cmd.Parameters.Add("@person_attribute_10", SqlDbType.NVarChar, 100).Value = person_attribute_10;

                /* cmd.Parameters.Add("@person_gender", SqlDbType.Char, 1).Value = person_gender; */

                //execute the insert
                cmd.ExecuteReader();

                Return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);

            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }


        }

        /// </summary>
        public void getNewApiKey()
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prNewApiKey", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();

                //----------------------------------------------------------------------------------------

                cmd.Parameters.Add("@apikey_salt", SqlDbType.NVarChar, 70).Value = ApiSalt;
                cmd.Parameters.Add("@apikey_hash", SqlDbType.NVarChar, 70).Value = ApiHash;
                cmd.Parameters.Add("@apikey_website", SqlDbType.NVarChar, 200).Value = ApiWebsite;

                //execute the insert
                cmd.ExecuteReader();

                Return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);
                
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }

        }











        /// <summary>
        /// 
        /// </summary>
        public void setNewSubdomain()
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prNewSubdomain", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();

                //----------------------------------------------------------------------------------------
                cmd.Parameters.Add("@apikey_subdomain", SqlDbType.NVarChar, 200).Value = ApiSubdomain;

                //execute the insert
                cmd.ExecuteReader();

                Return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);



            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }

        }





        public DataTable getEmployeeAggregateList(string status)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prEmployeeAggregateList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@status", SqlDbType.VarChar,5).Value = status;


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

                userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);


                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }








        /// <summary>
        /// 
        /// </summary>
        public void getApiKey()
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prApiKey", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@apikey_hash", SqlDbType.NVarChar, 70).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@apikey_salt", SqlDbType.NVarChar, 70).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@apikey_website", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@apikey_subdomain", SqlDbType.NVarChar, 200).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());

                //----------------------------------------------------------------------------------------

                //execute the insert
                cmd.ExecuteReader();

                ApiHash = Convert.ToString(cmd.Parameters["@apikey_hash"].Value);
                ApiSalt = Convert.ToString(cmd.Parameters["@apikey_salt"].Value);
                ApiWebsite = Convert.ToString(cmd.Parameters["@apikey_website"].Value);
                ApiSubdomain = Convert.ToString(cmd.Parameters["@apikey_subdomain"].Value);
               
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
             conn.Close();
            }
        }



        public void getApiKeySchema(string apikey)
        {
           
            string current_lang = (System.Web.HttpContext.Current.Session["_lastCulture"].ToString().Substring(0, 2));
           

            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prApiKeySchema", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@location_name", SqlDbType.NVarChar, 100).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_company_name", SqlDbType.NVarChar, 100).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_description", SqlDbType.NVarChar, 400).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_lang", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_login_user", SqlDbType.NVarChar, 65).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_status", SqlDbType.NVarChar, 100).Direction = ParameterDirection.Output; 
                cmd.Parameters.Add("@apikey", SqlDbType.NVarChar, 75).Value = apikey;
                cmd.Parameters.Add("@lang", SqlDbType.VarChar, 10).Value = current_lang;
               
                //execute the insert
                cmd.ExecuteReader();

                ApiSchema_id = Convert.ToInt32(cmd.Parameters["@schema_id"].Value);
                Apiperson_id = Convert.ToInt32(cmd.Parameters["@person_id"].Value);
                Location_name = Convert.ToString(cmd.Parameters["@location_name"].Value);
                Schema_login_user = Convert.ToString(cmd.Parameters["@schema_login_user"].Value);
                Schema_company_name = Convert.ToString(cmd.Parameters["@schema_company_name"].Value);
                Schema_lang = Convert.ToString(cmd.Parameters["@schema_lang"].Value);
                Schema_status = Convert.ToString(cmd.Parameters["@schema_status"].Value);
                Schema_description = Convert.ToString(cmd.Parameters["@schema_description"].Value);
            }
            catch (Exception ex)
            {
               sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }


        }









        public void getSubdomainSchema(string subdomain)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prSubdomainSchema", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            string current_lang = (System.Web.HttpContext.Current.Session["_lastCulture"].ToString().Substring(0, 2));
            

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@location_name", SqlDbType.NVarChar, 100).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_company_name", SqlDbType.NVarChar, 100).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_lang", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_login_user", SqlDbType.NVarChar, 65).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_status", SqlDbType.NVarChar, 100).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@subdomain", SqlDbType.NVarChar, 200).Value = subdomain;
                cmd.Parameters.Add("@lang", SqlDbType.VarChar, 10).Value = current_lang;
                cmd.Parameters.Add("@schema_description", SqlDbType.NVarChar, 400).Direction = ParameterDirection.Output;
               
                //execute the insert
                cmd.ExecuteReader();

                ApiSchema_id = Convert.ToInt32(cmd.Parameters["@schema_id"].Value);
                Apiperson_id = Convert.ToInt32(cmd.Parameters["@person_id"].Value);
                Location_name = Convert.ToString(cmd.Parameters["@location_name"].Value);
                Schema_login_user = Convert.ToString(cmd.Parameters["@schema_login_user"].Value);
                Schema_company_name = Convert.ToString(cmd.Parameters["@schema_company_name"].Value);
                Schema_lang = Convert.ToString(cmd.Parameters["@schema_lang"].Value);
                Schema_description = Convert.ToString(cmd.Parameters["@schema_description"].Value);
                Schema_status = Convert.ToString(cmd.Parameters["@schema_status"].Value);

            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }


        }
     













        public void getApiKeyValidation(string apikey_salt, string apikey_hash, string apikey_website)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prApiKeyValidation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@apikey_salt", SqlDbType.NVarChar, 70).Value = apikey_salt;
                cmd.Parameters.Add("@apikey_hash", SqlDbType.NVarChar, 70).Value = apikey_hash;
                cmd.Parameters.Add("@apikey_website", SqlDbType.NVarChar, 50).Value = apikey_website;

                //execute the insert
                cmd.ExecuteReader();

                Return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);
                ApiSchema_id = Convert.ToInt32(cmd.Parameters["@schema_id"].Value);

            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
             conn.Close();
            }


        }
     


        public void getGettingStartedWorkSchedule(int location_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prGettingStartedWorkSchedule", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@location_id", SqlDbType.Int).Value = location_id;
               
                //----------------------------------------------------------------------------------------

                cmd.Parameters.Add("@monday_working", SqlDbType.Int).Value = Monday_working;
                cmd.Parameters.Add("@tuesday_working", SqlDbType.Int).Value = Tuesday_working;
                cmd.Parameters.Add("@wednesday_working", SqlDbType.Int).Value = Wednesday_working;
                cmd.Parameters.Add("@thursday_working", SqlDbType.Int).Value = Thursday_working;
                cmd.Parameters.Add("@friday_working", SqlDbType.Int).Value = Friday_working;
                cmd.Parameters.Add("@saturday_working", SqlDbType.Int).Value = Saturday_working;
                cmd.Parameters.Add("@sunday_working", SqlDbType.Int).Value = Sunday_working;

               
                cmd.Parameters.Add("@monday_start", SqlDbType.Int).Value = Monday_start;
                cmd.Parameters.Add("@tuesday_start", SqlDbType.Int).Value = Tuesday_start;
                cmd.Parameters.Add("@wednesday_start", SqlDbType.Int).Value = Wednesday_start;
                cmd.Parameters.Add("@thursday_start", SqlDbType.Int).Value = Thursday_start;
                cmd.Parameters.Add("@friday_start", SqlDbType.Int).Value = Friday_start;
                cmd.Parameters.Add("@saturday_start", SqlDbType.Int).Value = Saturday_start;
                cmd.Parameters.Add("@sunday_start", SqlDbType.Int).Value = Sunday_start;

                cmd.Parameters.Add("@monday_end", SqlDbType.Int).Value = Monday_end;
                cmd.Parameters.Add("@tuesday_end", SqlDbType.Int).Value = Tuesday_end;
                cmd.Parameters.Add("@wednesday_end", SqlDbType.Int).Value = Wednesday_end;
                cmd.Parameters.Add("@thursday_end", SqlDbType.Int).Value = Thursday_end;
                cmd.Parameters.Add("@friday_end", SqlDbType.Int).Value = Friday_end;
                cmd.Parameters.Add("@saturday_end", SqlDbType.Int).Value = Saturday_end;
                cmd.Parameters.Add("@sunday_end", SqlDbType.Int).Value = Sunday_end;
                //execute the insert
                cmd.ExecuteReader();

                Return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);


            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }

        }




        public void getUserSchedule(int person_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prUserWorkingHours", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;

                //----------------------------------------------------------------------------------------

                cmd.Parameters.Add("@monday_location", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@tuesday_location", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@wednesday_location", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@thursday_location", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@friday_location", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@saturday_location", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@sunday_location", SqlDbType.Int).Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@monday_start", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@tuesday_start", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@wednesday_start", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@thursday_start", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@friday_start", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@saturday_start", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@sunday_start", SqlDbType.Int).Direction = ParameterDirection.Output;

                cmd.Parameters.Add("@monday_end", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@tuesday_end", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@wednesday_end", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@thursday_end", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@friday_end", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@saturday_end", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@sunday_end", SqlDbType.Int).Direction = ParameterDirection.Output;

                //execute the insert
                cmd.ExecuteReader();

                Monday_location = Convert.ToInt32(cmd.Parameters["@monday_location"].Value);
                Tuesday_location = Convert.ToInt32(cmd.Parameters["@tuesday_location"].Value);
                Wednesday_location = Convert.ToInt32(cmd.Parameters["@wednesday_location"].Value);
                Thursday_location = Convert.ToInt32(cmd.Parameters["@thursday_location"].Value);
                Friday_location = Convert.ToInt32(cmd.Parameters["@friday_location"].Value);
                Saturday_location = Convert.ToInt32(cmd.Parameters["@saturday_location"].Value);
                Sunday_location = Convert.ToInt32(cmd.Parameters["@sunday_location"].Value);


                Monday_start = Convert.ToInt32(cmd.Parameters["@monday_start"].Value);
                Tuesday_start = Convert.ToInt32(cmd.Parameters["@tuesday_start"].Value);
                Wednesday_start = Convert.ToInt32(cmd.Parameters["@wednesday_start"].Value);
                Thursday_start = Convert.ToInt32(cmd.Parameters["@thursday_start"].Value);
                Friday_start = Convert.ToInt32(cmd.Parameters["@friday_start"].Value);
                Saturday_start = Convert.ToInt32(cmd.Parameters["@saturday_start"].Value);
                Sunday_start = Convert.ToInt32(cmd.Parameters["@sunday_start"].Value);

                Monday_end = Convert.ToInt32(cmd.Parameters["@monday_end"].Value);
                Tuesday_end = Convert.ToInt32(cmd.Parameters["@tuesday_end"].Value);
                Wednesday_end = Convert.ToInt32(cmd.Parameters["@wednesday_end"].Value);
                Thursday_end = Convert.ToInt32(cmd.Parameters["@thursday_end"].Value);
                Friday_end = Convert.ToInt32(cmd.Parameters["@friday_end"].Value);
                Saturday_end = Convert.ToInt32(cmd.Parameters["@saturday_end"].Value);
                Sunday_end = Convert.ToInt32(cmd.Parameters["@sunday_end"].Value);

            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
              conn.Close();
            }

        }




        public void getSchemaDescriptionExistForlang(string lang)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd2 = new SqlCommand("prSchemaDescriptionExistForlang", conn);

            cmd2.CommandType = CommandType.StoredProcedure;


            conn.Open();
            //Add the params
            cmd2.Parameters.Add("@SchemaDescriptionExistForlang", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
            cmd2.Parameters.Add("@lang", SqlDbType.VarChar, 10).Value = lang;
            try
            {

                SqlDataReader dr = null;
                dr = cmd2.ExecuteReader();

                SchemaDescriptionExistForlang = Convert.ToInt32(cmd2.Parameters["@SchemaDescriptionExistForlang"].Value);
            }

            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }

            finally
            {
                conn.Close();
            }

        }




      

        



        public void getUserNameUpdate(int person_id)
        {

            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prUserNameUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;
                cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = Role_id;
                cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@person_lname", SqlDbType.NVarChar, 50).Value = person_lname;
                cmd.Parameters.Add("@person_fname", SqlDbType.NVarChar, 50).Value = person_fname;
                cmd.Parameters.Add("@person_phone", SqlDbType.NVarChar, 50).Value = person_phone;
               

                //execute the insert
                cmd.ExecuteReader();

                Return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);

            }

            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }
        }



        public int setPersonAddressUpdate(string country_id, string region_id, string city_id,
                string person_address, string person_pc, string person_tel, decimal latitude, decimal longitude,int person_address_id)
        {

            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prPersonAddressUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            try
            {


                //return params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                //Add the input params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
             
                cmd.Parameters.Add("@address_id", SqlDbType.Int).Value = person_address_id;
                cmd.Parameters.Add("@country_id", SqlDbType.Int).Value = Convert.ToInt32(country_id);
                cmd.Parameters.Add("@region_id", SqlDbType.Int).Value = Convert.ToInt32(region_id);
                cmd.Parameters.Add("@city_id", SqlDbType.Int).Value = Convert.ToInt32(city_id);

                cmd.Parameters.Add("@person_address", SqlDbType.NVarChar, 50).Value = person_address;
                cmd.Parameters.Add("@person_pc", SqlDbType.NVarChar, 20).Value = person_pc;
                cmd.Parameters.Add("@latitude", SqlDbType.Decimal).Value = latitude;
                cmd.Parameters.Add("@longitude", SqlDbType.Decimal).Value = longitude;
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 50).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
             


                //execute the insert
                cmd.ExecuteReader();
                return Convert.ToInt32(cmd.Parameters["@return"].Value);
            }
            catch (Exception exc)
            {
                sinfoca.error.ErrorLog.addErrorLog(exc);
                return 0;
            }

            finally
            {
                conn.Close();
            }

        }




        public int setEmployeeStatusUpdate(string person_id,string status)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prEmployeeStatusUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@status", SqlDbType.NChar, 2).Value = status;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
            cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(person_id);

            try
            {
                conn.Open();
                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);

                return 1;
            }
            catch (Exception exc)
            {
                sinfoca.error.ErrorLog.addErrorLog(exc);
                return 0;
            }
            finally
            {
                
                conn.Close();
                
            }

        }


       public DataTable getEmployeeList(string status,string name)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prEmployeeList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@status", SqlDbType.NChar,2).Value =  status ;
            cmd.Parameters.Add("@name", SqlDbType.VarChar,50).Value = name;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
             
             
            try
            {
                conn.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable dt = new DataTable();

                da.Fill(dt);

                return dt;
            }
            finally
            {
                conn.Close();
            }

        }

        public DataTable getEmployeeView(string person_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prEmployeeView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@person_id", SqlDbType.Int).Value =  Convert.ToInt32(@person_id) ;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
             
            try
            {
                conn.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable dt = new DataTable();

                da.Fill(dt);

                return dt;
            }
            finally
            {
                conn.Close();
            }

        }

        public DataTable getPersonAdress(string person_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prPersonAddress", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(@person_id);
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());

            try
            {
                conn.Open();
                DataTable dt = new DataTable("personaddress");

                SqlDataAdapter da = new SqlDataAdapter(cmd);

              
                da.Fill(dt);

                return dt;
            }
            finally
            {
                conn.Close();
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable getEmployeeList(string status, string name, int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prEmployeeList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            DataTable dt = new DataTable();

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@userCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@status", SqlDbType.NChar, 2).Value = @status;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@status", SqlDbType.VarChar,2).Value = status;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(dt);

                userCount = Convert.ToInt32(cmd.Parameters["@userCount"].Value);


                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }

        public static int getTotalPersonCount(string status, string name)
        {
            return userCount;
        }


        public void setPersonStripePaymentMethod(int person_id,string person_stripe_pubkey, string person_stripe_privkey)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prPersonStripePaymentMethod", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;
                cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();

                cmd.Parameters.Add("@person_stripe_pubkey", SqlDbType.NVarChar, 50).Value = person_stripe_pubkey;
                cmd.Parameters.Add("@person_stripe_privkey", SqlDbType.NVarChar, 50).Value = person_stripe_privkey;

                /* cmd.Parameters.Add("@person_gender", SqlDbType.Char, 1).Value = person_gender; */

                //execute the insert
                cmd.ExecuteReader();

                Return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);

            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }


        }





        public void setPersonTaxNumberUpdate(int person_id, string person_notax1, string person_notax2, string person_notax3)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prPersonTaxNumberUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;
                cmd.Parameters.Add("@trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
                cmd.Parameters.Add("@trace_person_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();

                cmd.Parameters.Add("@person_notax1", SqlDbType.NVarChar, 200).Value = person_notax1;
                cmd.Parameters.Add("@person_notax2", SqlDbType.NVarChar, 200).Value = person_notax2;
                cmd.Parameters.Add("@person_notax3", SqlDbType.NVarChar, 200).Value = person_notax3;


                /* cmd.Parameters.Add("@person_gender", SqlDbType.Char, 1).Value = person_gender; */

                //execute the insert
                cmd.ExecuteReader();

                Return_id = Convert.ToInt32(cmd.Parameters["@return_id"].Value);

            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }


        }


        public DataTable getPersonServiceList()
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prCMSPersonService", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
            cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["person_id"].ToString());
           
            try
            {
                conn.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable dt = new DataTable();

                da.Fill(dt);

                return dt;
            }
            finally
            {
                conn.Close();
            }

        }


        public DataTable getEmployeeType()
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prEmployeeType", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable("employeetype");

            try
            {
                conn.Open();
                //Add the params
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }

            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }



        public void getUserDelete(int person_id)
        {

            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prUserDelete", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;

                //execute the insert
                cmd.ExecuteReader();
                
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }

            finally
            {
              conn.Close();
            }
        }

        public int getPersonAppointmentResponse(int person_id, int appointment_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prPersonAppointmentResponse", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@status_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;
                cmd.Parameters.Add("@appointment_id", SqlDbType.Int).Value = appointment_id;

                cmd.ExecuteReader();

                int status_id = Convert.ToInt32(cmd.Parameters["@status_id"].Value);


                return status_id;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }


    }
}