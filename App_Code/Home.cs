using System;
using System.Data;
using System.Data.SqlClient;
 
/// <summary>
/// Summary description for Home
/// </summary>


namespace tiger
{
    public class Home
    {
    
        private string strconn ;
         public Home(string strconn)
           {
         this.strconn = strconn;
       
          }


        

        public DataTable getHomeList(int schema_id)
        {
            
        /*    SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
           // try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                return dt;
            }
           // finally
            {
                
            }
*/





            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
            
        }

        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public DataTable getHomeUntreatedRentList(int schema_id)
        {
            
            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeUntreatedRentList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
            
        }
        /// <summary>
        /// List of property with commercial units
        /// </summary>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public DataTable getHomeComList(int schema_id)
        {

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeComList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }

        }

         /// <summary>
         /// 
         /// </summary>
         /// <param name="schema_id"></param>
         /// <returns></returns>
        public DataTable getHomeMortgageList(int schema_id)
        {

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeMortgageList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }

        }


        


        public DataSet getHomeNameLeftOver(int schema_id, int name_id, int group_id)
        {

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeNameLeftOverList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
         
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }

        }
       

        public DataSet getHomeUnitList(int home_id, int schema_id)
        {

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeUnitList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
           try
           {
            conn.Open();

            //Add the params
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id  ;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
     
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
           return ds;
            }
            finally
            {
                conn.Close();
            } 
            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="home_id"></param>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public DataTable getHomeUnitCommercialList(int home_id, int schema_id)
        {

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeUnitCommercialList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();

                //Add the params
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }

        }


        public DataSet getHomeOwnerList(int home_id, int schema_id)
        {

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeOwnerList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
            conn.Open();

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
           
            return ds;
            }
            finally
            {
                conn.Close();
            } 
            
        }
        public int getHomeFirstId(int schema_id)
        {
            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeFirstId", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            
            try
            {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Convert.ToInt32(cmd.Parameters["@home_id"].Value);
           }
            finally
            {
                conn.Close();
            }


            
     
        }
        /// <summary>
        /// Get the first property with a commercial unit
        /// </summary>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public int getHomeComFirstId(int schema_id)
        {
            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeComFirstId", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return Convert.ToInt32(cmd.Parameters["@home_id"].Value);
            }
            finally
            {
                conn.Close();
            }




        }
        public DataSet getHomeView(int schema_id, int home_id)
        {
            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }


        public DataSet getHomeTenantUnitView(int schema_id, int tu_id)
        {
            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeTenantUnitView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }
        /********************************************/
      /*  public string getHomeName(int schema_id, int home_id)
        {
            string result="";
            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeName", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
           
            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleResult);

                   while (dr.Read() == true)
                   {
                      result = Convert.ToString(dr["home_name"]);
                    }
                
                return result;
            }
            finally
            {
                conn.Close();
            }
        }
        */
        /*****************************************/
        public string getHomeView1(int schema_id, int home_id)
        {
             string result = "<table>";
            result += "<tr><td colspan='2'>List to Check</td></tr>";

            SqlConnection conn = new SqlConnection("server=mssql9.ixwebhosting.com;database=sinfoca_tiger;uid=sinfoca_sinfo;password=st19gu09;");
            SqlCommand cmd = new SqlCommand("prHomeList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
            conn.Open();

            SqlDataReader dr = null;
            dr = cmd.ExecuteReader();
            int duplicate = 0;
            int home = 0;
            while (dr.Read() == true)
            {

                if (home == 0)
                {
                    result += " <tr class='sname'><td> " + Convert.ToString(dr["home_name"]) + "</td></tr>";


                }
                if (duplicate != Convert.ToInt32(dr["home_id"]))
                {

                    result += " <tr class='sname'><td><a href='unit/unit_list.aspx?home_id=" + Convert.ToString(dr["home_id"]) + "'>" + Convert.ToString(dr["home_name"]) + "</a> </td><td>" + Convert.ToString(dr["home_desc"]) + "</td></tr>";
                    duplicate = Convert.ToInt32(dr["unit_id"]);
                }

            }
            result += "</table>";
            return result;
        }
        finally
        {
            conn.Close();
        }
           
        }
        private string getHomeViewInfoLink(int home_id,char user_lang)
        {
            string info, dimension,mortgage, report, photo,units;

            if (user_lang== 'E')
            {
                info = "info";
                dimension = "Dimension";
                units = "Appartments";
                mortgage = "Mortgage";
                report = "Report";
                photo = "Photo";

            }
            else
            {
                info = "info";
                dimension = "Dimension";
                units = "Appartements";
                mortgage = "Hypothèque";
                report = "Rapport";
                photo = "Photo";

            }

            return "<a href='home_view_info.aspx?home_id=" + home_id + "' class='sname'>" + info + "</a>&nbsp;&nbsp;<a href='home_view_dimension.aspx?home_id=" + home_id + "' class='sname'>" + dimension + "</a>&nbsp;&nbsp;<a href='home_view_mortgage.aspx?home_id=" + home_id + "' class='sname'>" + mortgage + "</a>&nbsp;&nbsp;&nbsp;<a href='home_view_photo.aspx?home_id=" + home_id + "' class='sname'>" + photo + "</a>&nbsp;&nbsp;&nbsp;<a href='home_view_report.aspx?home_id=" + home_id + "' class='sname'>" + report + "</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='unit_view_info.aspx?home_id=" + home_id + "' class='sname'>" + units + "</a>";

        }
        private string getHomeNameList(int schema_id,int home_id, int group_id, char user_lang)
        {
            /************************Janitor***************************/
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            //get owner
            SqlCommand cmd_name = new SqlCommand("prHomeNameList", conn);
            cmd_name.CommandType = CommandType.StoredProcedure;
            try
            {

                conn.Open();

                //Add the params
                cmd_name.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd_name.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd_name.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
              
                SqlDataReader dr_name = null;
                dr_name = cmd_name.ExecuteReader();
                // dg_home.DataSource = 
                string title = "";
                switch (group_id)
                { 
                    case 2:
                        title = "Janitor";
                        break;
                    case 4:
                        title = "Owner";
                        break;

                }
                string name = "<table><tr><td colspan=2 align=left class=nameb>"+title+"&nbsp;&nbsp;<a href=home_name_add.aspx?group_id="+group_id+"&home_id="+home_id+"><img src=http://familyship.com/i/add_photo.gif border=0 width=10></a></td></tr>";
                int i = 0;

                while (dr_name.Read())
                {
                    i++;
                    name += "<tr class='small_name'><td><a href='name_view.aspx?name_id="+dr_name["name_id"]+"' class='sname'>" + dr_name["name_fname"] + ", " + dr_name["name_lname"] + "</td></tr>";
                   // name += "<tr class='small_name'><td>Tel. :" + dr_name["name_tel"] + "<br>Cell. :" + dr_name["name_cell"] + "<br>email  :" + dr_name["name_email"] + "</tr>";
                   // name += "<tr class='small_name'><td><hr align=center width=50%></td></tr>";
                }
                name += "</table>";
                return name;
            }
            finally
            {
                conn.Close();
            }
         
        }
        /************************Janitor***************************/
      
     /*   private string getHomeJanitor(int schema_id,int home_id, char user_lang)
        {
                  SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            //get owner
            SqlCommand cmd_owner = new SqlCommand("prHomeJanitor", conn);
            cmd_owner.CommandType = CommandType.StoredProcedure;
            try
            {

                conn.Open();

                //Add the params
                cmd_owner.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd_owner.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

                SqlDataReader dr_janitor = null;
                dr_janitor = cmd_owner.ExecuteReader(CommandBehavior.SingleRow);
                // dg_home.DataSource = 

                string janitor = "<table><tr><td colspan=2 align=left class=nameb>Janitor&nbsp;&nbsp;<a href=janitor_update.aspx?home_id="+home_id+"><img src=http://familyship.com/i/add_photo.gif border=0 width=10></a></td></tr>";
                int i = 0;

                while (dr_janitor.Read() == true)
                {
                    i++;
                    janitor += "<tr><td>" + dr_janitor["janitor_fname"] + ", " + dr_janitor["janitor_lname"] + "</td></tr>";
                   
                    janitor += "<tr><td>"+dr_janitor["janitor_addr"]+"<br>"+dr_janitor["janitor_city"]+", &nbsp;"+dr_janitor["janitor_prov"]+"&nbsp;"+dr_janitor["janitor_pc"]+"<br>    Tel. :" + dr_janitor["janitor_tel"] + "<br>Cell. :" + dr_janitor["janitor_cell"] + "<br>" + dr_janitor["janitor_email"] + "</tr>";
                }
                janitor += "</table>";
                return janitor; 
            }
            finally
            {
                conn.Close();
            }
            


        }
*/
        public string getHomeViewInfo(int schema_id,int home_id,char user_lang)
        {
       
            /************************Home View*******************/
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prHomeView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
            conn.Open();

            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id ;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

            SqlDataReader dr = null;
            dr = cmd.ExecuteReader(CommandBehavior.SingleRow);
            // dg_home.DataSource = 

            //get language 
            tiger.label.LabelHome l = new tiger.label.LabelHome(user_lang);
            string lbl_home_id = l.getLabel("lbl_home_id");
            string lbl_home_name = l.getLabel("lbl_home_name");
            string lbl_home_addr = l.getLabel("lbl_home_addr");
            //string lbl_home_desc = l.getLabel("lbl_home_desc");

            string str="";

            while (dr.Read() == true)
            {
     
                string home_name = Convert.ToString(dr["home_name"]);
                string home_date_purchase = Convert.ToString(dr["home_date_purchase"]);
                string home_date_built = Convert.ToString(dr["home_date_built"]);
                //string home_living_space = Convert.ToString(dr["home_living_space"]);
                string home_floor = Convert.ToString(dr["home_floor"]);
                string home_unit = Convert.ToString(dr["home_unit"]);
                 string home_addr_no = Convert.ToString(dr["home_addr_no"]);
                //string home_addr_street = Convert.ToString(dr["home_addr_street"]);
                string home_city = Convert.ToString(dr["home_city"]);
                string home_pc = Convert.ToString(dr["home_pc"]);
                string home_pic = Convert.ToString(dr["home_pic"]);
               // string home_desc = Convert.ToString(dr["home_desc"]);

                str = "<table width='220' border=0><tr><td>" + getHomeViewInfoLink(home_id, user_lang) + "</td></tr><tr><td><table width='220'  cellspacing='0' cellpadding='0' border=1><tr><td><table width='220' border='0' cellspacing='0' cellpadding='0'>";
       str += "<tr><td>";
           //column 1 -- picture and address
                str += "<table width='220' border='0' cellspacing='0' cellpadding='0'>";
                //str += "<tr><td>"+getHomeViewInfoLink(home_id,user_lang)+"</td></tr>";
                str += "<tr><td>&nbsp;<img src=/tiger/i/adm/"+dr["home_pic"]+" width=250 border=1></td></tr>";
                str += "<tr><td>&nbsp;" + home_name + "&nbsp;&nbsp;<a href=home_manage.aspx?home_id=" + home_id + "><img src=http://familyship.com/i/add_photo.gif border=0 width=10></a></td></tr>";
               // str += "<tr><td>&nbsp;Purchase: " + home_date_purchase + "</td></tr>";
                //str += "<tr><td>&nbsp;" + home_addr_no+" "+home_addr_street + "<br>" + home_city + "<br>" + home_pc + "</td></tr>";
                //str += "<tr><td>" + home_desc + "</td></tr>";
                str += "</table>";

                str += "</td><td valign=top>";

           str += "<table width='220' border='0' cellspacing='0' cellpadding='0'>";
   /*         str += "<tr><td> Purchase: built: " + home_date_built + "</td></tr>";
           str += "<tr><td> Purchase: " + home_date_purchase + "</td></tr>";
           str += "<tr><td> Dim.   :   " + home_dimension + "</td></tr>";
           str += "<tr><td> Floor  :   " + home_floor + "</td></tr>";
           str += "<tr><td> Units  :   " + home_unit + "</td></tr>";
           str += "<tr><td><hr align=center width=100%></td></tr>";
        */ str += "<tr><td align=left>" +getHomeNameList(schema_id,home_id,4, user_lang)+"</td></tr>";
           str += "<tr><td align=left>" + getHomeNameList(schema_id, home_id,2, user_lang) + "</td></tr></table>";
         
          // str += "<tr><td>" + home_addr + "<br>" + home_city + "<br>" + home_pc + "</td></tr>";
          // str += "<tr><td>" + home_desc + "</td></tr>";
     
  str += "</td></tr></table>";
    /* <td><p>Year, dimension (land/house)#units,owner</p>    </td>
  </tr>
  <tr>
    <td><div id=txt runat=server /> </td>
  </tr>
</table>*/
           str += "</td></tr></table></td></tr></table></td></tr></table>";

            }
                 return str;
        }
        finally
        {
            conn.Close();
        }

           
        }
        public string getHomeViewDimension(int schema_id, int home_id, char user_lang)
        {

            /************************Home View*******************/
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prHomeView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();

                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);
                // dg_home.DataSource = 

                //get language 
                tiger.label.LabelHome l = new tiger.label.LabelHome(user_lang);
                string lbl_home_id = l.getLabel("lbl_home_id");
                string lbl_home_name = l.getLabel("lbl_home_name");
                string lbl_home_addr = l.getLabel("lbl_home_addr");
                //string lbl_home_desc = l.getLabel("lbl_home_desc");

                string str = "";

                while (dr.Read() == true)
                {

                    string home_name = Convert.ToString(dr["home_name"]);
                    string home_date_purchase = Convert.ToString(dr["home_date_purchase"]);
                    string home_date_built = Convert.ToString(dr["home_date_built"]);
                    string home_dimension = Convert.ToString(dr["home_dimension"]);
                    string home_floor = Convert.ToString(dr["home_floor"]);
                    string home_unit = Convert.ToString(dr["home_unit"]);
                    string home_addr = Convert.ToString(dr["home_addr"]);
                    string home_city = Convert.ToString(dr["home_city"]);
                    string home_pc = Convert.ToString(dr["home_pc"]);
                    string home_pic = Convert.ToString(dr["home_pic"]);
                    // string home_desc = Convert.ToString(dr["home_desc"]);

                    str = "<table width='220' border=0><tr><td>" + getHomeViewInfoLink(home_id, user_lang) + "</td></tr><tr><td><table width='220'  cellspacing='0' cellpadding='0' border=1><tr><td><table width='220' border='0' cellspacing='0' cellpadding='0'>";
                    str += "<tr><td>";
                    //column 1 -- picture and address
                    str += "<table width='220' border='0' cellspacing='0' cellpadding='0'>";
                    //str += "<tr><td>"+getHomeViewInfoLink(home_id,user_lang)+"</td></tr>";
                    str += "<tr><td>&nbsp;<img src=/swse/home/images/" + home_pic + " width=180 border=1></td></tr>";
                    str += "<tr><td>&nbsp;" + home_name + "</td></tr>";
                    // str += "<tr><td>&nbsp;Purchase: " + home_date_purchase + "</td></tr>";
                    str += "<tr><td>&nbsp;" + home_addr + "<br>" + home_city + "<br>" + home_pc + "</td></tr>";
                    //str += "<tr><td>" + home_desc + "</td></tr>";
                    str += "</table>";

                    str += "</td><td valign=top>";

                    str += "<table width='220' border='0' cellspacing='0' cellpadding='0'>";
                    str += "<tr><td> Purchase: built: " + home_date_built + "</td></tr>";
                    str += "<tr><td> Purchase: " + home_date_purchase + "</td></tr>";
                    str += "<tr><td> Dim.   :   " + home_dimension + "</td></tr>";
                    str += "<tr><td> Floor  :   " + home_floor + "</td></tr>";
                    str += "<tr><td> Units  :   " + home_unit + "</td></tr>";
                    str += "<tr><td><hr align=center width=100%></td></tr>";
                 //   str += "<tr><td align='center'><div class='nameb'>Owner  </div> <br>" + getHomeOwner(home_id, user_lang) + "</td></tr>";
                 //   str += "<tr><td align='center'>'<table><tr><td><div class='nameb'>Janitor</div></td><td>&nbsp;&nbsp;&nbsp;<a href='home_janitor_manage.aspx?home><img src=http://familyship.com/i/add_photo.gif border=0 width=10 alt=Change></a></td></tr><tr><td colspan=2>" + getHomeJanitor(home_id, user_lang) + "</td></tr></table></td></tr>";

                    // str += "<tr><td>" + home_addr + "<br>" + home_city + "<br>" + home_pc + "</td></tr>";
                    // str += "<tr><td>" + home_desc + "</td></tr>";
                    str += "</table>";
                    /* <td><p>Year, dimension (land/house)#units,owner</p>    </td>
                   </tr>
                   <tr>
                     <td><div id=txt runat=server /> </td>
                   </tr>
                 </table>*/
                    str += "</td></tr></table></td></tr></table></td></tr></table>";

                }
                return str;
            }
            finally
            {
                conn.Close();
            }


        }
        public DataSet getHomeTaxe(int home_id)
        {
            return null;
        }
        public DataSet getHomeDimension(int home_id)
        {
            return null;
        }
        public int getHomeCount(int schema_id)
        {
            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeCount", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@tot", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return Convert.ToInt32(cmd.Parameters["@tot"].Value);
            }
            finally
            {
                conn.Close();
            }

        }

/// <summary>
/// Get the number of home with commercial units
/// </summary>
/// <param name="schema_id"></param>
/// <returns></returns>
        public int getHomeComCount(int schema_id)
        {
            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeComCount", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@tot", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return Convert.ToInt32(cmd.Parameters["@tot"].Value);
            }
            finally
            {
                conn.Close();
            }

        }

/// <summary>
/// 
/// </summary>
/// <param name="schema_id"></param>
/// <returns></returns>
        public DataSet getHomeWithoutGroupList(int schema_id)
        {

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeWithoutGroupList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }



        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public DataSet getHomeUnitRentedList(int schema_id, int home_id, int rent_frequency , DateTime the_date)
        {

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeUnitRentedList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@rent_frequency", SqlDbType.Int).Value = rent_frequency;
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <param name="rent_frequency"></param>
        /// <returns></returns>
        public DataTable getHomeUnitRentedListArchives(int schema_id, int home_id, int rent_frequency)
        {

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeUnitRentedListArchives", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@rent_frequency", SqlDbType.Int).Value = rent_frequency;
                

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }

        }
        /// <summary>
        /// List of units that are currently rented wich the rent is unpaid as of the current month
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public DataSet getHomeUnitUnpaidRentedList(int schema_id, int home_id, int rent_frequency, DateTime the_date)
        {

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeUnitUnpaidRentedList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@rent_frequency", SqlDbType.Int).Value = rent_frequency;
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <param name="rent_frequency"></param>
        /// <param name="the_date"></param>
        /// <returns></returns>
        public DataSet getHomeUnitUnpaidRent(int schema_id, int home_id, int rent_frequency, DateTime the_date, int unit_id)
        {

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeUnitUnpaidRent", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@rent_frequency", SqlDbType.Int).Value = rent_frequency;
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;
                cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }

        }

        /// <summary>
        /// List of units that are currently rented wich the rent is paid as of the current month
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public DataSet getHomeUnitPaidRentedList(int schema_id, int home_id, int rent_frequency, DateTime the_date)
        {

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeUnitPaidRentedList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@rent_frequency", SqlDbType.Int).Value = rent_frequency;
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <param name="rent_frequency"></param>
        /// <param name="the_date"></param>
        /// <returns></returns>
        public DataSet getHomeTenantUnitPaidRent(int schema_id, int home_id, int rent_frequency, DateTime the_date, int unit_id, int tu_id)
        {

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeTenantUnitPaidRent", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@rent_frequency", SqlDbType.Int).Value = rent_frequency;
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;
                cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;
                cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public DataSet getHomeUnitRentedSumList(int schema_id, int home_id, DateTime the_date)
        {

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeUnitRentedSumList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <param name="the_date"></param>
        /// <returns></returns>
        public DataTable getHomeUnitRentedSumListArchives(int schema_id, int home_id)
        {

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeUnitRentedSumListArchives", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
               

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }

        }



        public DataTable getHomeNumberOfUnitAvailable(int schema_id)
        {
            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prHomeNumberOfUnitAvailable", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                 cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ds = new DataTable();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }


    }
}