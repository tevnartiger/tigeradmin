﻿using System;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Contact
/// </summary>
namespace tiger
{
    public class Contact
    { 
        private string str_conn;
        public Contact(string str_conn)
        {
            this.str_conn = str_conn;
        }



        public DataTable getContactSearchList(int schema_id, int category, string name)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prContactSearchList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ds = new DataTable();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }
    }
}