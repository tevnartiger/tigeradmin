﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.UI;

/// <summary>
/// Summary description for PageUtility
/// </summary>
/// 
namespace tiger
{
    public class PageUtility
    {
        public PageUtility()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static void SetFocus(Control control)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("\r\n<script language='JavaScript'>\r\n");
            sb.Append("<!--\r\n");
            sb.Append("function SetFocus()\r\n");
            sb.Append("{\r\n");
            sb.Append("\tdocument.");

            Control p = control.Parent;
            while (!(p is System.Web.UI.HtmlControls.HtmlForm)) p = p.Parent;

            sb.Append(p.ClientID);
            sb.Append("['");
            sb.Append(control.UniqueID);
            sb.Append("'].focus();\r\n");
            sb.Append("}\r\n");
            sb.Append("window.onload = SetFocus;\r\n");
            sb.Append("// -->\r\n");
            sb.Append("</script>");

            control.Page.RegisterClientScriptBlock("SetFocus", sb.ToString());
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        public static string getLanguage(string _lastCulture)
        {
            string language = "en";

            if (_lastCulture == "en-US")
            {
                language = "en";
            }

            if (_lastCulture == "fr-FR")
            {
                language = "fr";
            }
            return language;

        }





    }
}