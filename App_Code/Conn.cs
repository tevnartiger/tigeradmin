using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Conn
/// </summary>
namespace tiger
{
    namespace security
    {
        public class Conn
        {
            private string conn;
            public Conn(string conn)
            {
                this.conn = conn;
                
            }
            public bool isLoginValid(string login_user, string login_pwd)
            {
                bool temp_valid = false;

                SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                SqlCommand cmd = new SqlCommand("prLoginValid", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                conn.Open();

                    //Add the params
                //cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;          
                cmd.Parameters.Add("@login_user", SqlDbType.NVarChar, 25).Value = login_user;
                cmd.Parameters.Add("@login_id", SqlDbType.NVarChar, 25).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_salt", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@login_hash", SqlDbType.NVarChar, 50).Direction = ParameterDirection.Output;
                
                cmd.ExecuteReader();


                if (Convert.ToInt32(cmd.Parameters["@login_id"].Value) > 0)
                {
                    string temp_salt = Convert.ToString(cmd.Parameters["@login_salt"].Value);
                    string temp_hash = Convert.ToString(cmd.Parameters["@login_hash"].Value);

                    
                    //now let us verify the password with the hash
                    if (sinfoca.tiger.security.Hash.accessGranted(login_pwd, temp_salt, temp_hash))
                    {
                        temp_valid = true;
                    }
                }

                return temp_valid;
            }
            public int modifyPassword(string login_user, string login_pwd,int name_id, string name_id_ip)
            {
            string temp_salt = sinfoca.tiger.security.Hash.createSalt(30);
            string temp_hash = sinfoca.tiger.security.Hash.createPasswordHash(login_pwd,temp_salt);
            //edit tlogin
                    
                SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                SqlCommand cmd = new SqlCommand("prModifyPwd", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                
                    conn.Open();

                    //Add the params
                    cmd.Parameters.Add("@return",SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@login_user", SqlDbType.VarChar,75).Value = login_user;
                    cmd.Parameters.Add("@login_salt", SqlDbType.VarChar,75).Value = temp_salt;
                    cmd.Parameters.Add("@login_hash", SqlDbType.VarChar,75).Value = temp_hash;
                    cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = name_id;
                    cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar,20).Value = name_id_ip;

                    cmd.ExecuteReader(CommandBehavior.SingleRow);
                          
                return  Convert.ToInt32(cmd.Parameters["@return"].Value);
            }
            public string getLoginInfo(string login_user)
            {
                //index 0 = schema_id
                //index 1 = schema_manager_id
                //index 2 = login_user
                //index 3 = name_id
                //index 4 = schema_status
               
                string str= "0";
                //Now get home information
         
                SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
                SqlCommand cmd = new SqlCommand("prLoginInfo", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                try
                {
                    conn.Open();

                    //Add the params
                    cmd.Parameters.Add("@login_user", SqlDbType.VarChar,75).Value = login_user;
                 //   cmd.Parameters.Add("@login_pwd", SqlDbType.VarChar, 25).Value = login_pwd;

                    SqlDataReader dr = null;
                    dr = cmd.ExecuteReader(CommandBehavior.SingleRow);
                    while (dr.Read() == true)
                    {

                        str = Convert.ToString(dr["schema_id"]) + "_" + Convert.ToString(dr["schema_manager_id"]) + "_" + Convert.ToString(dr["login_user"]) + "_" + Convert.ToString(dr["name_id"]) + "_" + Convert.ToString(dr["isTenant"]) + "_" + Convert.ToString(dr["isOwner"]) + "_" + Convert.ToString(dr["isJanitor"]) + "_" + Convert.ToString(dr["isPM"]) + "_" + Convert.ToString(dr["schema_status"]+ "_" + Convert.ToString(dr["name_fname"]));
                    }
                    return str;
                }
                finally
                {
                    conn.Close();
                }
                {




                }

            }


          


        }
    }
}