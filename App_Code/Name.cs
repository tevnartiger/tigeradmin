using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Person
/// </summary>
/// 
namespace tiger
{

public class Name
{
	private string strconn ;
         public Name(string strconn)
           {
         this.strconn = strconn;
          }

  public DataSet getNameList(int schema_id, int name_id)
    {

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prNameList", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }

    }


  public DataTable getNameRoleList(int schema_id, int name_id)
  {

      SqlConnection conn = new SqlConnection(strconn);
      SqlCommand cmd = new SqlCommand("prNameRoleList", conn);
      cmd.CommandType = CommandType.StoredProcedure;
      try
      {
          conn.Open();
          //Add the params
          cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
          cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

          SqlDataAdapter da = new SqlDataAdapter(cmd);
          DataTable ds = new DataTable();
          da.Fill(ds);
          return ds;
      }
      finally
      {
          conn.Close();
      }

  }


  public DataTable getNameRoleAssignementList(int schema_id, int name_id)
  {

      SqlConnection conn = new SqlConnection(strconn);
      SqlCommand cmd = new SqlCommand("prNameRoleAssignementList", conn);
      cmd.CommandType = CommandType.StoredProcedure;
      try
      {
          conn.Open();
          //Add the params
          cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
          cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

          SqlDataAdapter da = new SqlDataAdapter(cmd);
          DataTable ds = new DataTable();
          da.Fill(ds);
          return ds;
      }
      finally
      {
          conn.Close();
      }

  }


 



  public DataTable getPropertyForNameRole(int schema_id, int name_id , int role_id)
  {

      SqlConnection conn = new SqlConnection(strconn);
      SqlCommand cmd = new SqlCommand("prPropertyForNameRole", conn);
      cmd.CommandType = CommandType.StoredProcedure;
      try
      {
          conn.Open();
          //Add the params
          cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
          cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
          cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

          SqlDataAdapter da = new SqlDataAdapter(cmd);
          DataTable ds = new DataTable();
          da.Fill(ds);
          return ds;
      }
      finally
      {
          conn.Close();
      }

  }


  public DataTable getPropertyNotForNameRole(int schema_id, int name_id, int role_id)
  {

      SqlConnection conn = new SqlConnection(strconn);
      SqlCommand cmd = new SqlCommand("prPropertyNotForNameRole", conn);
      cmd.CommandType = CommandType.StoredProcedure;
      try
      {
          conn.Open();
          //Add the params
          cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
          cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
          cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;

          SqlDataAdapter da = new SqlDataAdapter(cmd);
          DataTable ds = new DataTable();
          da.Fill(ds);
          return ds;
      }
      finally
      {
          conn.Close();
      }

  }

  public string  nameSearch( string search,int categ)
  {
      string temp_search ="";
      SqlConnection conn = new SqlConnection(strconn);
      SqlCommand cmd = new SqlCommand("prNameSearch", conn);
      cmd.CommandType = CommandType.StoredProcedure;
      try
      {
          conn.Open();
          //Add the params
         // cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
          cmd.Parameters.Add("@search", SqlDbType.VarChar, 15).Value =  search;
          cmd.Parameters.Add("@categ", SqlDbType.Int).Value = categ;

          SqlDataReader dr_name = null;
            dr_name = cmd.ExecuteReader();
            // dg_home.DataSource = 
             
             
            temp_search += "<table width='150'><tr><td align='center'>SEARCH</td></tr>";
            while (dr_name.Read())
            {
            temp_search += " <tr><td align='left'><a href='/manager/tenants/tenant_profile.aspx?name_id=" + Convert.ToString(dr_name["name_id"]) + "'>" + Convert.ToString(dr_name["name_fname"]) + " "+Convert.ToString(dr_name["name_lname"])+"</td></tr>";
            }
            temp_search += "</table>";
          return temp_search;
      }
      finally
      {
          conn.Close();
      }

  }

  


    public string getOwnerJanitorHome(int schema_id, int name_id, int group_id)
    {

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prOwnerJanitorHomeList", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        try
        {
            conn.Open();
            //Add the params
            //cmd.Parameters.Add("@tot", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
          
            SqlDataReader dr_name = null;
            dr_name = cmd.ExecuteReader();
            // dg_home.DataSource = 
            string title = "";
            string link = "";
            switch (group_id)
            {
                case 4 :
                    title = "Owner of :";
                    link = "<a href='home_name_add.aspx?name_id="+name_id+"&group_id="+group_id+"'><img src='/tiger/i/plus.gif' border='0' alt='Link person to a home'>";
                    break;
                case 2 :
                    title = "Janitor of :";
                    link = "<a href='home_name_add.aspx?name_id=" + name_id + "&group_id=" + group_id + "'><img src='/tiger/i/plus.gif' border='0' alt='Link '>";
                    break;
            }
                    
            string name  = "<table width='150'><tr><td align='right'>"+link+"</td></tr>";
              int i = 0;
             string status = "";
            while (dr_name.Read())
            {
                i++;
                if (i == 1)
                {
                    name += "<tr><td colspan=2 align=left class=nameb>" + title + "</td></tr>";
                }

                
                status = "is the account manager";
                 
             name += "<tr class='small_name'><td>"+dr_name["home_name"]+"</td></tr>";
             
            }
            name += "</table>";
           return name; 
        }
        finally
        {
            conn.Close();
        }
         
        
             
    }





    public string getManager(int schema_id, int name_id)
    {

        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prIsManager", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        try
        {
            conn.Open();
            //Add the params
            //cmd.Parameters.Add("@tot", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            
            SqlDataReader dr_name = null;
            dr_name = cmd.ExecuteReader(CommandBehavior.SingleRow);
            // dg_home.DataSource = 
            string title = "Manager";

            string name = "";
            name += "<table width='150'><tr><td align='right'><a href='manager_add.aspx'><img src='/tiger/i/plus.gif' border='0' alt='Add a manager'></td></tr>";
            while (dr_name.Read())
            {
                if (Convert.ToInt32(dr_name["tot"]) > 0)
                {
                    name += " <tr><td colspan=2 align=left class=nameb>" + title + "</td></tr>";
                    name += "<tr class='small_name'><td>is the account manager</td></tr>";
                    name += "</table>";
                }
            }   
           
           return name; 
        }
        finally
        {
            conn.Close();
        }
         
        
             
    }


    public string getNameView(int schema_id, int name_id, char user_lang)
    {
        /************************Janitor***************************/
        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        //get owner
        SqlCommand cmd_name = new SqlCommand("prNameView", conn);
        cmd_name.CommandType = CommandType.StoredProcedure;
        try
        {

            conn.Open();

            //Add the params
            cmd_name.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd_name.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
           
            SqlDataReader dr_name = null;
            dr_name = cmd_name.ExecuteReader(CommandBehavior.SingleRow);
            // dg_home.DataSource = 
            string title = "Info";

            string name = "<table><tr><td colspan=2 align=left class=nameb>" + title + "</td></tr>";
            
            int i = 0;

            while (dr_name.Read())
            {
                i++;
 
                name += "<tr class='small_name'><td><img src='/tiger/i/adm/"+dr_name["name_pic"]+"' width='100' border='1'></td></tr>";
                
                name += "<tr class='small_name'><td>" + dr_name["name_fname"] + ", " + dr_name["name_lname"] + "</td></tr>";
                  name += "<tr class='small_name'><td>Tel. :" + dr_name["name_tel"] + "<br>Cell. :" + dr_name["name_cell"] + "<br>" + dr_name["name_email"] + "</tr>";
                 name += "<tr class='small_name'><td><hr align=center width=50%></td></tr>";
            }
            name += "</table>";
            return name;
        }
        finally
        {
            conn.Close();
        }

    }


    public string getName(int schema_id, int name_id)
    {
        /************************Janitor***************************/
        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd_name = new SqlCommand("prNameView", conn);
        cmd_name.CommandType = CommandType.StoredProcedure;
        try
        {

            conn.Open();

            //Add the params
            cmd_name.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd_name.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            SqlDataReader dr_name = null;
            dr_name = cmd_name.ExecuteReader(CommandBehavior.SingleRow);
            // dg_home.DataSource = 
            string title = "Info";

            string name = "<table><tr><td colspan=2 align=left class=nameb>" + title + "</td></tr>";

            int i = 0;

            while (dr_name.Read())
            {
                i++;               
                name += "<tr class='small_name'><td>" + dr_name["name_fname"] + ", " + dr_name["name_lname"] + "</td></tr>";
               
            }
            name += "</table>";
            return name;
        }
        finally
        {
            conn.Close();
        }

    }

    public int ProspectAccept(int schema_id,int pt_id, int trace_name_id, string trace_name_id_ip)
    {
        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prProspectAccept", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@result", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@pt_id", SqlDbType.Int).Value = pt_id;
            cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = trace_name_id;
            cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar , 15).Value = trace_name_id_ip;

            
            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Convert.ToInt32(cmd.Parameters["@result"].Value);
        }
        finally
        {
            conn.Close();
        }
    }




    public int ProspectDelete(int schema_id, int pt_id)
    {
        SqlConnection conn = new SqlConnection(strconn);
        SqlCommand cmd = new SqlCommand("prProspectDelete", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@result", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@pt_id", SqlDbType.Int).Value = pt_id;

            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Convert.ToInt32(cmd.Parameters["@result"].Value);
        }
        finally
        {
            conn.Close();
        }
    }
}
}