using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.login;

/// <summary>
/// Done by : Stanley Jocelyn
/// date    : sept 25 , 2007
/// Summary description for Lease
/// </summary>
namespace tiger
{
public class Lease
{


    // Attributs
    private static int leaseCount;

    private string str_conn;
	public Lease(string conn)
	{
        this.str_conn = conn;
	}



    public Lease()
    {
       
    }
    /// <summary>
    /// Ths method returns the number of pending leases for an unit
    /// </summary>
    /// <param name="schema_id"></param>
    /// <param name="unit_id"></param>
    /// <returns></returns>
    public int getTenantUnitGreaterThenToday(int schema_id, int unit_id)
    {

        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prTenantUnitGreaterThenTodayReturn", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@tot", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;

            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            //if unit is currently occupied then open current tenant panel
            /* if (Convert.ToInt32(cmd.Parameters["@tot"].Value)>0)
             {
                 temp_bool = true;
             }
             */
            return Convert.ToInt32(cmd.Parameters["@tot"].Value);
        }
        finally
        {
            conn.Close();
        }

    }

    /// <summary>
    /// Get the pending lease for an unit
    /// </summary>
    /// <param name="schema_id"></param>
    /// <param name="unit_id"></param>
    /// <returns></returns>
    public DataSet getPendingLeasesList(int schema_id, int unit_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prPendingLeasesList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;


            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }
    }


    /// <summary>
    /// Pending lease for a property or all properties within a time frame
    /// </summary>
    /// <param name="schema_id"></param>
    /// <returns></returns>
    public DataTable getLeasePendingList(int schema_id, int home_id, int time_frame)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prLeasePendingList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd.Parameters.Add("@time_frame", SqlDbType.Int).Value = time_frame;


            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        finally
        {
            conn.Close();
        }
    }




    public string getTenantUnitName(int schema_id, int tenant_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prTenantUnitNameList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = tenant_id;
            //     SqlDataAdapter da = new SqlDataAdapter(cmd);
            // DataSet ds = new DataSet();
            // da.Fill(ds);
            // return ds;

            string str = "<table>";
            //  str += "<tr><td colspan='2' class='nameb'>Current Tenant  </td></tr>";
            int i = 0;
            SqlDataReader dr = null;
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            while (dr.Read() == true)
            {
                if (i == 0)
                {
                    str += "<tr><td ><b>" + Resources.Resource.txt_lease_date_begin + "</b></td><td>&nbsp;&nbsp;" + dr["tu_date_begin"] + "</td></tr>";

                    if (tiger.Date.isDate(Convert.ToString(dr["tu_date_end"])))
                        str += "<tr><td ><b>" + Resources.Resource.lbl_lease_date_end + "</b></td><td>&nbsp;&nbsp;" + dr["tu_date_end"] + "</br></br></td></tr>";
                    str += "<tr><td colspan='2' class='nameb'><b>" + Resources.Resource.lbl_tenant + "</b></td></tr>";
                }
                i++;
                str += "<tr><td colspan='2'>" + dr["name_fname"] + ", " + dr["name_lname"] + "</td></tr>";
            }

            str += "<tr ><td><td></tr></table>";
            return str;
        }
        finally
        {
            conn.Close();
        }
    }


    /// <summary>
    /// Get the pending accommodation for a tenant unit
    /// </summary>
    /// <param name="schema_id"></param>
    /// <param name="tu_id"></param>
    /// <returns></returns>
    public DataSet getPendingAccommodationList(int schema_id, int tu_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prPendingAccommodationList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;


            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }
    }


    /// <summary>
    /// Get the pending accommodation for a tenant unit
    /// </summary>
    /// <param name="schema_id"></param>
    /// <param name="tu_id"></param>
    /// <returns></returns>
    public DataSet getPendingTermsAndConditionsList(int schema_id, int tu_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prPendingTermsAndConditionsList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;


            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }
    }


    public DataSet getPendingRentList(int schema_id, int tu_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prPendingRentList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;


            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }
    }
    /// <summary>
    /// Get the pending rent for a tenant unit
    /// </summary>
    /// <param name="schema_id"></param>
    /// <param name="tu_id"></param>
    /// <returns></returns>
    public int getCountPendingRent(int schema_id, int tu_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prCountPendingRent", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@count", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;
            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Convert.ToInt32(cmd.Parameters["@count"].Value);
        }
        finally
        {
            conn.Close();
        }

    }

    /// <summary>
    /// Count the pending lease for an unit
    /// </summary>
    /// <param name="schema_id"></param>
    /// <param name="unit_id"></param>
    /// <returns></returns>
    public int getCountPendingLeases(int schema_id, int unit_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prCountPendingLeases", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@count", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;
            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Convert.ToInt32(cmd.Parameters["@count"].Value);
        }
        finally
        {
            conn.Close();
        }

    }


    /// <summary>
    /// Count the pending accommodation for a tanant unit
    /// </summary>
    /// <param name="schema_id"></param>
    /// <param name="tu_id"></param>
    /// <returns></returns>
    public int getCountPendingAccommodation(int schema_id, int tu_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prCountPendingAccommodation", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@count", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;
            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Convert.ToInt32(cmd.Parameters["@count"].Value);
        }
        finally
        {
            conn.Close();
        }

    }



    /// <summary>
    /// Count the pending terms and conditions for a tenant unit
    /// </summary>
    /// <param name="schema_id"></param>
    /// <param name="tu_id"></param>
    /// <returns></returns>
    public int getCountPendingTermsAndConditions(int schema_id, int tu_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prCountPendingTermsAndConditions", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@count", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;
            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Convert.ToInt32(cmd.Parameters["@count"].Value);
        }
        finally
        {
            conn.Close();
        }

    }

    /// <summary>
    /// List of units that are currently rented 
    /// </summary>
    /// <param name="schema_id"></param>
    /// <param name="home_id"></param>
    /// <returns></returns>
    public DataSet getLeaseList(int schema_id, int home_id,int unit_id ,  DateTime the_date)
    {

        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prLeaseList", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;
            cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }

    }

/// <summary>
/// 
/// </summary>
/// <param name="schema_id"></param>
/// <param name="home_id"></param>
/// <param name="unit_id"></param>
/// <param name="the_date"></param>
/// <returns></returns>
/// 




    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
    public DataTable getLeaseList_2(int home_id, int unit_id, DateTime the_date, int maximumRows, int startRowIndex)
    {

        SqlConnection conn = new SqlConnection(Utils.CONN);
        SqlCommand cmd = new SqlCommand("prLeaseList_2", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        int PageNo = 0;
        if (startRowIndex == 0)
            PageNo = 1;
        else
            PageNo = (startRowIndex / maximumRows) + 1;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@leaseCount", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;
            cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
            cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            da.Fill(ds);

            leaseCount = Convert.ToInt32(cmd.Parameters["@leaseCount"].Value);

            return ds;
        }
        finally
        {
            conn.Close();
        }

    }


    public static int getTotalLeaseCount(int home_id, int unit_id, DateTime the_date)
    {
        return leaseCount;
    }


    [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
    public DataSet getLeaseArchiveList(int home_id, int unit_id, DateTime the_date, int maximumRows, int startRowIndex)
    {

        SqlConnection conn = new SqlConnection(Utils.CONN);
        SqlCommand cmd = new SqlCommand("prLeaseArchiveList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        int PageNo = 0;
        if (startRowIndex == 0)
            PageNo = 1;
        else
            PageNo = (startRowIndex / maximumRows) + 1;
        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@leaseCount", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;
            cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;
            cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
            cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            leaseCount = Convert.ToInt32(cmd.Parameters["@leaseCount"].Value);

            return ds;
        }
        finally
        {
            conn.Close();
        }

    }



    /// <summary>
    /// List of expiring mortgage ( 0-30 days , 31-60 days , 61-90 days
    /// </summary>
    /// <param name="schema_id"></param>
    /// <returns></returns>
    public DataTable getLeaseExpirationList(int schema_id, int home_id, int time_frame)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prLeaseExpirationList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd.Parameters.Add("@time_frame", SqlDbType.Int).Value = time_frame;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        finally
        {
            conn.Close();
        }
    }
       

    




}
}