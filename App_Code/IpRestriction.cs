using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for IpRestriction
/// </summary>
namespace sinfoca.tiger.security
{

    public class IpRestriction
    {
        private string conn;

        public IpRestriction(string conn)
        {
            this.conn = conn;
        }

        public  bool addIpRestriction(string ip_address)
        {
            SqlConnection conn = new SqlConnection(this.conn);
            SqlCommand cmd = new SqlCommand("prIpInsert",conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@ip_address", SqlDbType.VarChar, 15).Value = ip_address;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

    }
}