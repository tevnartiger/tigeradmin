﻿using System;
using System.Web.Configuration;
using System.Web.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Net;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Data;



/// <summary>
/// Summary description for Login
/// </summary>

public static class Utils
    {
        //Connection String referencing Web.config
        public static string CONN = WebConfigurationManager.AppSettings["ConnectionString"].ToString();
        public static string ERRORLOG_CONN = "";
        public const string ACTIVE = "AC";
        public const string PENDING = "PE";
        public const string INACTIVE = "IN";
        //RegX
        public const string REGEX_NAME = @"^[ \u00c0-\u01ffa-zA-Z']+(([\'\,\.\- ][ \u00c0-\u01ffa-zA-Z' ])?[ \u00c0-\u01ffa-zA-Z']*)*$";
        public const string REGEX_ADDRESS = @"^[0-9\'\#\,\.\s\-\u00c0-\u01ffa-zA-Z.]*$";
        public const string REGEX_NUMERIC = @"^[0-9]+$";
        public const string REGEX_ALPHA = @"^[a-zA-Z]*$";
        public const string REGEX_EMAIL = @"^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,6})$";
        public const string REGEX_TEXT = @"^[0-9\'\!\$\%\&\(\)\/\?\:\[\]\#\,\.\s\-\u00c0-\u01ffa-zA-Z._ ]*$";
        public const string REGEX_PWD = @"^[a-zA-Z0-9\!\@\#\$\%\&\*\.\-\+\?\~\|\=\¦]{6,10}$";
        public const string REGEX_ALPHANUMERIC = @"^[a-zA-Z0-9]+$";
        public const string REGEX_MONEY = @"^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$";
           
 

        public static bool sendEmail(string to, string subject, string body) 
        {
            bool success= false;
         
                
                try
                {
                    MailMessage m = new MailMessage();
                    m.From = new System.Net.Mail.MailAddress(BasePage.EMAIL_FROM, BasePage.EMAIL_FROM_DISPLAYNAME).ToString();
                    m.To = to;
                    m.Subject = subject; 
                    m.BodyFormat = MailFormat.Html;
                    m.Body = body;

                    SmtpMail.SmtpServer = WebConfigurationManager.AppSettings["SMTP"];
                    SmtpMail.Send(m);

                    success = true;
                }
                catch (Exception e) {
                    return success;

                }
                return success;           
        
        }
     
        public static bool validateDate(string date_param)
        {
           
            try
            {
                Convert.ToDateTime(date_param);
                
                return true;
            }
            catch (Exception)             
            { 
                return false;
            }
        }
         
    public static bool isNumeric(string str)
    {
        try 
        {
            Int32.Parse(str);
            return true;
        }
        catch (Exception ) 
        {
            return false;
        }
   
    }

    public static string sanitizeString(string str, int length)
    {
        try
        {
            if (str.Length <= length)
            {
                return str;
            }
            else
            {
                return str.Substring(0, length); 
            }
        }
        catch(Exception)
        {
            return ""; 
        }
    }


        public static bool validateEmail(string inputEmail)
        {
            try
            {
                if (inputEmail == null || inputEmail.Length == 0)
                {
                    throw new ArgumentNullException("inputEmail");
                }

                const string expression = REGEX_EMAIL;
                System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(expression);

                return regex.IsMatch(inputEmail);
            }
            catch (Exception)
            {
                return false;
            }


        }


    public static string createRandom(int length)
    {

        char[] chars = "$@abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
        string password = string.Empty;
        Random random = new Random();
        for (int i = 0; i < length; i++)
        {
            int x = random.Next(1, chars.Length);
            //Don't Allow Repetation of Characters
            if (!password.Contains(chars.GetValue(x).ToString()))
                password += chars.GetValue(x);
            else
                i--;
        }
        return password;



    }

    public static string createRandom2(int length)
    {

        char[] chars = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
        string password = string.Empty;
        Random random = new Random();
        for (int i = 0; i < length; i++)
        {
            int x = random.Next(1, chars.Length);
            //Don't Allow Repetation of Characters
            if (!password.Contains(chars.GetValue(x).ToString()))
                password += chars.GetValue(x);
            else
                i--;
        }
        return password;



    }

    public static DateTime TodayNow(string timezones_standard_name, decimal time_diff_hours)
    {
        DateTime today = new DateTime();
        // Represent the datetime in the timezone of the user
        today = DateTime.UtcNow.AddMinutes(Convert.ToDouble(time_diff_hours * 60));
        //  today = today.Date;  

        // get Time zone - not sure about that
        TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById(timezones_standard_name);
        bool isDaylight = tst.IsDaylightSavingTime(DateTime.UtcNow);

        if (isDaylight)
            today = today.AddMinutes(60);

        return today;
    }

    public static bool IsValidFilename(string testName)
    {
        Regex containsABadCharacter = new Regex("["
              + Regex.Escape(new string(System.IO.Path.GetInvalidPathChars())) + "]");
        if (containsABadCharacter.IsMatch(testName)) { return false; };

        // other checks for UNC, drive-path format, etc

        return true;
    }

    public static string MappedApplicationPath
    {
        get
        {
            string APP_PATH = System.Web.HttpContext.Current.Request.ApplicationPath.ToLower();
            if (APP_PATH == "/")      //a site
                APP_PATH = "/";
            else if (!APP_PATH.EndsWith(@"/")) //a virtual
                APP_PATH += @"/";

            string it = System.Web.HttpContext.Current.Server.MapPath(APP_PATH);
            if (!it.EndsWith(@"\"))
                it += @"\";
            return it;
        }
    }

   
    public static string Money(string money)
    {
        string response = "";

        string _lastCulture = System.Web.HttpContext.Current.Session["_lastCulture"].ToString().Trim();
        if (_lastCulture == "en-CA")
            _lastCulture = "en-ca";
        else if (_lastCulture == "fr-FR")
            _lastCulture = "fr-fr";
        else if (_lastCulture == "fr-CA")
            _lastCulture = "fr-ca";
        else if (_lastCulture == "es-ES")
            _lastCulture = "es-es";
        else if (_lastCulture == "fr-CA")
            _lastCulture = "fr-ca";
        else if (_lastCulture == "en-US")
            _lastCulture = "en-us";
        else
            _lastCulture = "en-us";


        if (_lastCulture == "en-us")
        {
            response = money.Replace(",", ".");
        }

        if (_lastCulture == "en-ca")
        {
            response = money.Replace(",", ".");
        }

        if (_lastCulture == "fr-ca")
        {
            response = money.Replace(".", ",");
        }

        if (_lastCulture == "fr-fr")
        {
            response = money.Replace(",", ".");
        }


        if (_lastCulture == "es-es")
        {

            response = money;
        }

        return response;

    }


    public static string DateString(string date, string format)
    {
        char[] delimiters = new char[] { '-' };
        string[] parts = date.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

        string month = parts[0];
        string day = parts[1];
        string year = parts[2];

        if (day.StartsWith("0"))
            day = day.Replace("0", "");

        if (month.StartsWith("0"))
            month = month.Replace("0", "");

        DateTime d = new DateTime(Convert.ToInt16(parts[2]), Convert.ToInt16(parts[0]), Convert.ToInt16(parts[1]));

        return String.Format(format, d);
    }

    public static AsymmetricAlgorithm getRsaKey(string filepath, string password)
    {
        X509Certificate2 cert = new X509Certificate2(filepath, password);
        RSACryptoServiceProvider privateKey = cert.PrivateKey as RSACryptoServiceProvider;
        return cert.PrivateKey;
    }

    public static string Sign(string url, string keyString)
    {
        ASCIIEncoding encoding = new ASCIIEncoding();

        // converting key to bytes will throw an exception, need to replace '-' and '_' characters first.
        string usablePrivateKey = keyString.Replace("-", "+").Replace("_", "/");
        byte[] privateKeyBytes = Convert.FromBase64String(usablePrivateKey);

        Uri uri = new Uri(url);
        byte[] encodedPathAndQueryBytes = encoding.GetBytes(uri.LocalPath + uri.Query);

        // compute the hash
        HMACSHA1 algorithm = new HMACSHA1(privateKeyBytes);
        byte[] hash = algorithm.ComputeHash(encodedPathAndQueryBytes);

        // convert the bytes to string and make url-safe by replacing '+' and '/' characters
        string signature = Convert.ToBase64String(hash).Replace("+", "-").Replace("/", "_");

        // Add the signature to the existing URI.
        return uri.Scheme + "://" + uri.Host + uri.LocalPath + uri.Query + "&signature=" + signature;
    }

    /// Gets the response with post.
    ///

    /// <param name="StrURL">The URL.
    /// <param name="strPostData">The post data.
    /// HTML Result
    public static string GetResponseWithPost(string StrURL, string strPostData)
    {
        string strReturn = "";
        HttpWebRequest objRequest = null;
        ASCIIEncoding objEncoding = new ASCIIEncoding();
        Stream reqStream = null;
        HttpWebResponse objResponse = null;
        StreamReader objReader = null;
        try
        {
            objRequest = (HttpWebRequest)WebRequest.Create(StrURL);

            objRequest.Method = "POST";
            byte[] objBytes = objEncoding.GetBytes(strPostData);
            objRequest.ContentLength = objBytes.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";
            reqStream = objRequest.GetRequestStream();
            reqStream.Write(objBytes, 0, objBytes.Length);

            IAsyncResult ar = objRequest.BeginGetResponse(new AsyncCallback(GetScrapingResponse), objRequest);
            //// Wait for request to complete
            ar.AsyncWaitHandle.WaitOne(1000 * 60 * 3, true);
            if (objRequest.HaveResponse == false)
            {
                throw new Exception("No Response!!!");
            }
            objResponse = (HttpWebResponse)objRequest.EndGetResponse(ar);
            objReader = new StreamReader(objResponse.GetResponseStream());
            strReturn = objReader.ReadToEnd();

        }
        catch (Exception exp)
        {
            sinfoca.error.ErrorLog.addErrorLog(exp);

            //   throw exp;
        }
        finally
        {
            objRequest = null;
            objEncoding = null;
            reqStream = null;
            if (objResponse != null)
                objResponse.Close();
            objResponse = null;
            objReader = null;
        }
        return strReturn;
    }
    public static string GetResponse(string StrURL)
    {
        string strReturn = "";
        HttpWebRequest objRequest = null;
        IAsyncResult ar = null;
        HttpWebResponse objResponse = null;
        StreamReader objs = null;
        try
        {

            objRequest = (HttpWebRequest)WebRequest.Create(StrURL);
            ar = objRequest.BeginGetResponse(new AsyncCallback(GetScrapingResponse), objRequest);

            //// Wait for request to complete
            ar.AsyncWaitHandle.WaitOne(1000 * 60, true);
            if (objRequest.HaveResponse == false)
            {
                throw new Exception("No Response!!!");
            }

            objResponse = (HttpWebResponse)objRequest.EndGetResponse(ar);
            objs = new StreamReader(objResponse.GetResponseStream());
            strReturn = objs.ReadToEnd();
        }
        catch (Exception exp)
        {
            sinfoca.error.ErrorLog.addErrorLog(exp);
            return "error";
        }
        finally
        {
            if (objResponse != null)
                objResponse.Close();
            objRequest = null;
            ar = null;
            objResponse = null;
            objs = null;
        }
        return strReturn;
    }


    ///
    /// Gets the scraping response.
    ///
    /// <param name="result">The result.
    public static void GetScrapingResponse(IAsyncResult result)
    {

    }

    public static int CountStringOccurrences(string text, string pattern)
    {
        // Loop through all instances of the string 'text'.
        int count = 0;
        int i = 0;
        while ((i = text.IndexOf(pattern, i)) != -1)
        {
            i += pattern.Length;
            count++;
        }
        return count;
    }


    public static string GetJson(DataTable dt)
    {
        OperationContext context = OperationContext.Current;
        MessageProperties messageProperties = context.IncomingMessageProperties;
        RemoteEndpointMessageProperty endpointProperty = messageProperties[RemoteEndpointMessageProperty.Name]
            as RemoteEndpointMessageProperty;


        HttpRequestMessageProperty prop;
        prop = (HttpRequestMessageProperty)OperationContext.Current.IncomingMessageProperties[HttpRequestMessageProperty.Name];
        var referer = prop.Headers[HttpRequestHeader.Referer];



        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
        Dictionary<string, object> row = null;

        foreach (DataRow dr in dt.Rows)
        {
            row = new Dictionary<string, object>();
            foreach (DataColumn col in dt.Columns)
            {
                row.Add(col.ColumnName, dr[col]);
            }
            rows.Add(row);
        }
        return "{ \"response\" :" + serializer.Serialize(rows) + "}" + "<br/>" + endpointProperty.Address;

    }


    public static string GetJSONString(DataTable Dt)
    {

        string[] StrDc = new string[Dt.Columns.Count];

        string HeadStr = string.Empty;
        for (int i = 0; i < Dt.Columns.Count; i++)
        {

            StrDc[i] = Dt.Columns[i].Caption;
            HeadStr += "\"" + StrDc[i] + "\" : \"" + StrDc[i] + i.ToString() + "¾" + "\",";  ////// ( ORIGINAL )
                                                                                             // HeadStr +=  StrDc[i] + " : \"" + StrDc[i] + i.ToString() + "¾" + "\",";

        }

        HeadStr = HeadStr.Substring(0, HeadStr.Length - 1);
        StringBuilder Sb = new StringBuilder();

        Sb.Append("{\"" + Dt.TableName + "\" : [");    ////// ( ORIGINAL )
                                                       // Sb.Append("[");

        for (int i = 0; i < Dt.Rows.Count; i++)
        {

            string TempStr = HeadStr;

            Sb.Append("{");
            for (int j = 0; j < Dt.Columns.Count; j++)
            {

                TempStr = TempStr.Replace(Dt.Columns[j] + j.ToString() + "¾", Dt.Rows[i][j].ToString());

            }
            Sb.Append(TempStr + "},");

        }
        Sb = new StringBuilder(Sb.ToString().Substring(0, Sb.ToString().Length - 1));

        Sb.Append("]}");     ////// ( ORIGINAL )
                             // Sb.Append("]");
        return Sb.ToString();

    }

    public static string breadCrumbs(string home, string bc_url1, string bc_text1, string bc_url2, string bc_text2, string bc_url3, string bc_text3)
    {
        string str = string.Empty;

        str += "<nav id=\"bcrumb\">";

        str += "<ol>";//home
        str += "<li><a href=\"" + home + "\"><i class=\"icon-home\"></i></a></li>";

        //breadcrumb 1
        if (!string.IsNullOrEmpty(bc_url1))
            str += "<li><a href=\"" + bc_url1 + "\">" + bc_text1 + "</a></li>";
        else
            str += "<li><span>" + bc_text1 + "</span></li>";

        //breadcrumb 2
        if (!string.IsNullOrEmpty(bc_url2))
            str += "<li><a href=\"" + bc_url2 + "\">" + bc_text2 + "</a></li>";
        else
        {
            if (!string.IsNullOrEmpty(bc_url2) || (!string.IsNullOrEmpty(bc_text2)))
                str += "<li><span>" + bc_text2 + "</span></li>";
        }
        //breadcrumb 3
        if (!string.IsNullOrEmpty(bc_url3))
            str += "<li><a href=\"" + bc_url3 + "\">" + bc_text3 + "</a></li>";
        else
        {
            if (!string.IsNullOrEmpty(bc_url3) || (!string.IsNullOrEmpty(bc_text3)))
                str += "<li><span>" + bc_text3 + "</span></li>";
        }

        str += "</ol>";
        str += "</nav>";
        return str;
    }


    //The Code Project Open License (CPOL) 1.02
    public static DataTable GetDistinctRecords(DataTable dt, string[] Columns)
    {
        DataTable dtUniqRecords = new DataTable();
        dtUniqRecords = dt.DefaultView.ToTable(true, Columns);
        return dtUniqRecords;
    }



    public static bool IsMobileUser()
    {
        string u = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];
        Regex b = new Regex(@"android.+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino", RegexOptions.IgnoreCase);
        Regex v = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|e\-|e\/|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(di|rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|xda(\-|2|g)|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase);

        if (b.IsMatch(u) || v.IsMatch(u.Substring(0, 4)))
            return true;
        else
            return false;
    }

}
