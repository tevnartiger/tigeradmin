﻿using System;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Done by : Stanley Jocelyn
/// date    : march 5 , 2008
///
/// </summary>
namespace tiger
{
    public class Amortization
    {
        private string str_conn;
         public Amortization(string str_conn)
         {
             this.str_conn = str_conn;
        }

        /// <summary>
        /// Amortization table for interest Compounded once a year ( America & Euro )
        /// </summary>
        /// <param name="from"></param>
        /// <param name="a_balance"></param>
        /// <param name="a_duration"></param>
        /// <param name="a_frequency"></param>
        /// <param name="a_interest"></param>
        /// <param name="a_additinalAmount"></param>
        /// <param name="a_addAmountEachYear"></param>
        /// <param name="a_eachYear"></param>
        /// <returns></returns>
         public DataTable getAmortizationTable(DateTime from,double a_balance, double a_duration, double a_frequency,
                                              double a_interest, double a_additinalAmount, double a_addAmountEachYear, int a_eachYear)
        {
             //ATTENTION ON PEUT AUSSI RETOURNER UN DATASET
             // MAIS IL PARAIT QUE LE DATATABLE EST PLUS "EFFICIENT"
         //   DataSet AmortizationDS = new DataSet();

         //   DataTable amortizatioTable = AmortizationDS.Tables.Add();

            DataTable amortizatioTable = new DataTable();



            //– Add columns to the data table
            amortizatioTable.Columns.Add("ID", typeof(int));
            amortizatioTable.Columns.Add("theyear", typeof(int));
            amortizatioTable.Columns.Add("themonth", typeof(int));
            amortizatioTable.Columns.Add("theday", typeof(int));
            amortizatioTable.Columns.Add("OustandingBalance", typeof(double));
            amortizatioTable.Columns.Add("InterestPaid", typeof(double));
            amortizatioTable.Columns.Add("CummInterestPaid", typeof(double));
            amortizatioTable.Columns.Add("PrincipalPaid", typeof(double));
            amortizatioTable.Columns.Add("CummPrincipalPaid", typeof(double));
            amortizatioTable.Columns.Add("Balance", typeof(double));
            amortizatioTable.Columns.Add("ladate", typeof(string));


            double  AccPayment = 0;
            double InterestRate, PeriodRate, Balance;
            double Duration, Frequency, Payment; //-- Mortgage payment
            double number_of_insert;
            double c_InterestPaid, c_CummInterestPaid, c_PrincipalPaid;
            Date la_date = new Date();

            double AdditinalAmount;
            double AdditinalAmount2 = 0;
            double c_OustandingBalance, c_Balance;



            AdditinalAmount = a_additinalAmount ;
            InterestRate = a_interest / 100;
            Frequency =  a_frequency; //12 equal each month;

            if (Frequency == 241)
            {
                Frequency = 24;

            }

            if (Frequency == 521)
            {

              Frequency = 52;
            }

            PeriodRate = InterestRate / Frequency;
            Balance = a_balance ;
            Duration = a_duration ;// Ammortization Period;


            if (Frequency == 24)
            {
                Frequency = 26;
                PeriodRate = @InterestRate / 26;
            }


             Payment = ((Balance * Math.Pow(1.0 + PeriodRate, (Duration * Frequency))) * PeriodRate)
                    / (Math.Pow(1.0 + PeriodRate, Duration * Frequency) - 1);


            double cummInterestPaid, cummPrincipalPaid;
            int compteur;
            DateTime Date = new DateTime();

            Date =  from ;
            compteur = 1;
            number_of_insert = Duration * Frequency;

            cummInterestPaid = 0;
            cummPrincipalPaid = 0;



             if (Frequency == 52)
                 PeriodRate = @InterestRate / 52;

             int insertperyear = 0;

            //– Add rows to the data table

       // we begin to pay not in the first day of the mortgage but in the next period
           /////////////////////////////////////////////////////////////
             if (Frequency == 12)
             {
                 Date = Date.AddMonths(1);
             }

             if (Frequency == 26)
             {
                 Date = Date.AddDays(14);
             }

             if (Frequency == 52)
             {
                 Date = Date.AddDays(7);
             }
         /////////////////////////////////////////////////////////////


             //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             ///////////////accelerated payments///////////////////////////////////////////////////////////////////////////

             if (a_frequency == 241)
             {
                 AccPayment = ((Balance * Math.Pow(1.0 + (InterestRate / 12.00), (Duration * 12.00))) * (InterestRate / 12.00))
                     / (Math.Pow(1.0 + (InterestRate / 12.00), Duration * 12.00) - 1);


                 AccPayment = AccPayment / 2.00;
                 // PeriodRate = @InterestRate / 26;
                 Payment = AccPayment;
             }

             if (a_frequency == 521)
             {

                 AccPayment = ((Balance * Math.Pow(1.0 + (InterestRate / 12.00), (Duration * 12.00))) * (InterestRate / 12.00))
                       / (Math.Pow(1.0 + (InterestRate / 12.00), Duration * 12.00) - 1);

                 AccPayment = AccPayment / 4.00;
                 //  PeriodRate = @InterestRate / 52;
                 Payment = AccPayment;
             }
             /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             //////////////////////////////////////////////////////////////////////////////////////



            while (compteur < number_of_insert + 1 && Balance > 0)
            {


              //--------------------------------------------
              //--------------------------------------------
              //  In this section we make sure that the amounts that is supposed to be added once a year
              // gets only added once a year, for that we utilize a counter (insertperyear) .
              // Once the amount is addded for the particular month, the counter is set to one . And when
              // we leave the month the counter is reset to 0.
                AdditinalAmount2 = 0;

                if (Date.Month == a_eachYear)
                {
                    AdditinalAmount2 = a_addAmountEachYear;
                    insertperyear++;
                }

                if (Date.Month == a_eachYear && insertperyear > 1)
                {
                    AdditinalAmount2 = 0 ;

                }

                if (Date.Month != a_eachYear )
                {
                    insertperyear = 0;
                }


                c_OustandingBalance = Balance; //
                c_InterestPaid = Balance * PeriodRate;
                Balance = (Balance * (1 + PeriodRate)) - Payment - AdditinalAmount - AdditinalAmount2;
                cummInterestPaid = cummInterestPaid + c_InterestPaid;
                c_PrincipalPaid = Payment - c_InterestPaid + AdditinalAmount + AdditinalAmount2;

                cummPrincipalPaid = cummPrincipalPaid + c_PrincipalPaid;


              //  if( c_OustandingBalance < (Payment + AdditinalAmount + AdditinalAmount2))

                if (Balance < 0)
                  {

                      if (a_frequency == 241 || a_frequency == 521 || Frequency == 26 || Frequency == 52 || Frequency == 12)
                         amortizatioTable.Rows.Add(compteur, Date.Year, Date.Month,Date.Day, c_OustandingBalance, c_InterestPaid,
                                                       cummInterestPaid + c_InterestPaid, c_PrincipalPaid + Balance, cummPrincipalPaid + Balance, 0, Date.Month.ToString() + "-" + Date.Day.ToString() + "-" + Date.Year.ToString());
                      else     // si ca ne marche pas enleve le if ci-dessus
                          amortizatioTable.Rows.Add(compteur, Date.Year, Date.Month, Date.Day, c_OustandingBalance, c_InterestPaid,
                                                       cummInterestPaid + Balance, c_PrincipalPaid + Balance, cummPrincipalPaid + Balance, 0, Date.Month.ToString() + "-" + Date.Day.ToString() + "-" + Date.Year.ToString());
                  }
                 else
                    amortizatioTable.Rows.Add(compteur, Date.Year, Date.Month, Date.Day, c_OustandingBalance, c_InterestPaid,
                                                        cummInterestPaid, c_PrincipalPaid, cummPrincipalPaid, Balance, Date.Month.ToString() + "-" + Date.Day.ToString() + "-" + Date.Year.ToString());


              
               if (Frequency == 12)
                {
                    Date = Date.AddMonths(1);
                }

                if (Frequency == 26)
                {
                    Date = Date.AddDays(14);
                }

                if (Frequency == 52)
                {
                    Date = Date.AddDays(7);
                }

                compteur = compteur + 1;

            }


         //   return AmortizationDS;

            return amortizatioTable;


        }
































         /// <summary>
         /// Amortization table for interest Compounded once a year ( America & Euro )
         /// </summary>
         /// <param name="from"></param>
         /// <param name="a_balance"></param>
         /// <param name="a_duration"></param>
         /// <param name="a_frequency"></param>
         /// <param name="a_interest"></param>
         /// <param name="a_additinalAmount"></param>
         /// <param name="a_addAmountEachYear"></param>
         /// <param name="a_eachYear"></param>
         /// <returns></returns>
         public DataTable getAmortizationPartialTable(DateTime from, DateTime to, double a_balance, double a_duration, double a_frequency,
                                              double a_interest, double a_additinalAmount, double a_addAmountEachYear, int a_eachYear)
         {
             //ATTENTION ON PEUT AUSSI RETOURNER UN DATASET
             // MAIS IL PARAIT QUE LE DATATABLE EST PLUS "EFFICIENT"
             //   DataSet AmortizationDS = new DataSet();

             //   DataTable amortizatioTable = AmortizationDS.Tables.Add();

             DataTable amortizatioTable = new DataTable();



             //– Add columns to the data table
             amortizatioTable.Columns.Add("ID", typeof(int));
             amortizatioTable.Columns.Add("theyear", typeof(int));
             amortizatioTable.Columns.Add("themonth", typeof(int));
             amortizatioTable.Columns.Add("theday", typeof(int));
             amortizatioTable.Columns.Add("OustandingBalance", typeof(double));
             amortizatioTable.Columns.Add("InterestPaid", typeof(double));
             amortizatioTable.Columns.Add("CummInterestPaid", typeof(double));
             amortizatioTable.Columns.Add("PrincipalPaid", typeof(double));
             amortizatioTable.Columns.Add("CummPrincipalPaid", typeof(double));
             amortizatioTable.Columns.Add("Balance", typeof(double));
             amortizatioTable.Columns.Add("ladate", typeof(string));


             double AccPayment = 0;
             double InterestRate, PeriodRate, Balance;
             double Duration, Frequency, Payment; //-- Mortgage payment
             double number_of_insert;
             double c_InterestPaid, c_CummInterestPaid, c_PrincipalPaid;
             Date la_date = new Date();

             double AdditinalAmount;
             double AdditinalAmount2 = 0;
             double c_OustandingBalance, c_Balance;



             AdditinalAmount = a_additinalAmount;
             InterestRate = a_interest / 100;
             Frequency = a_frequency; //12 equal each month;

             if (Frequency == 241)
             {
                 Frequency = 24;

             }

             if (Frequency == 521)
             {

                 Frequency = 52;
             }

             PeriodRate = InterestRate / Frequency;
             Balance = a_balance;
             Duration = a_duration;// Ammortization Period;


             if (Frequency == 24)
             {
                 Frequency = 26;
                 PeriodRate = @InterestRate / 26;
             }


             Payment = ((Balance * Math.Pow(1.0 + PeriodRate, (Duration * Frequency))) * PeriodRate)
                    / (Math.Pow(1.0 + PeriodRate, Duration * Frequency) - 1);


             double cummInterestPaid, cummPrincipalPaid;
             int compteur;
             DateTime Date = new DateTime();
             DateTime toDate = new DateTime();

             Date = from;
             toDate = to;

             compteur = 1;
             number_of_insert = Duration * Frequency;

             cummInterestPaid = 0;
             cummPrincipalPaid = 0;



             if (Frequency == 52)
                 PeriodRate = @InterestRate / 52;

             int insertperyear = 0;

             //– Add rows to the data table

             // we begin to pay not in the first day of the mortgage but in the next period
             /////////////////////////////////////////////////////////////
             if (Frequency == 12)
             {
                 Date = Date.AddMonths(1);
             }

             if (Frequency == 26)
             {
                 Date = Date.AddDays(14);
             }

             if (Frequency == 52)
             {
                 Date = Date.AddDays(7);
             }
             /////////////////////////////////////////////////////////////


             //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             ///////////////accelerated payments///////////////////////////////////////////////////////////////////////////

             if (a_frequency == 241)
             {
                 AccPayment = ((Balance * Math.Pow(1.0 + (InterestRate / 12.00), (Duration * 12.00))) * (InterestRate / 12.00))
                     / (Math.Pow(1.0 + (InterestRate / 12.00), Duration * 12.00) - 1);


                 AccPayment = AccPayment / 2.00;
                 // PeriodRate = @InterestRate / 26;
                 Payment = AccPayment;
             }

             if (a_frequency == 521)
             {

                 AccPayment = ((Balance * Math.Pow(1.0 + (InterestRate / 12.00), (Duration * 12.00))) * (InterestRate / 12.00))
                       / (Math.Pow(1.0 + (InterestRate / 12.00), Duration * 12.00) - 1);

                 AccPayment = AccPayment / 4.00;
                 //  PeriodRate = @InterestRate / 52;
                 Payment = AccPayment;
             }
             /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             //////////////////////////////////////////////////////////////////////////////////////



             while ( (compteur < number_of_insert + 1 && Balance > 0) && Date <= toDate )
             {


                 //--------------------------------------------
                 //--------------------------------------------
                 //  In this section we make sure that the amounts that is supposed to be added once a year
                 // gets only added once a year, for that we utilize a counter (insertperyear) .
                 // Once the amount is addded for the particular month, the counter is set to one . And when
                 // we leave the month the counter is reset to 0.
                 AdditinalAmount2 = 0;

                 if (Date.Month == a_eachYear)
                 {
                     AdditinalAmount2 = a_addAmountEachYear;
                     insertperyear++;
                 }

                 if (Date.Month == a_eachYear && insertperyear > 1)
                 {
                     AdditinalAmount2 = 0;

                 }

                 if (Date.Month != a_eachYear)
                 {
                     insertperyear = 0;
                 }


                 c_OustandingBalance = Balance; //
                 c_InterestPaid = Balance * PeriodRate;
                 Balance = (Balance * (1 + PeriodRate)) - Payment - AdditinalAmount - AdditinalAmount2;
                 cummInterestPaid = cummInterestPaid + c_InterestPaid;
                 c_PrincipalPaid = Payment - c_InterestPaid + AdditinalAmount + AdditinalAmount2;

                 cummPrincipalPaid = cummPrincipalPaid + c_PrincipalPaid;


                 //  if( c_OustandingBalance < (Payment + AdditinalAmount + AdditinalAmount2))

                 if (Balance < 0)
                 {

                     if (a_frequency == 241 || a_frequency == 521 || Frequency == 26 || Frequency == 52 || Frequency == 12)
                         amortizatioTable.Rows.Add(compteur, Date.Year, Date.Month, Date.Day, c_OustandingBalance, c_InterestPaid,
                                                       cummInterestPaid + c_InterestPaid, c_PrincipalPaid + Balance, cummPrincipalPaid + Balance, 0, Date.Month.ToString() + "-" + Date.Day.ToString() + "-" + Date.Year.ToString());
                     else     // si ca ne marche pas enleve le if ci-dessus
                         amortizatioTable.Rows.Add(compteur, Date.Year, Date.Month, Date.Day, c_OustandingBalance, c_InterestPaid,
                                                      cummInterestPaid + Balance, c_PrincipalPaid + Balance, cummPrincipalPaid + Balance, 0, Date.Month.ToString() + "-" + Date.Day.ToString() + "-" + Date.Year.ToString());
                 }
                 else
                     amortizatioTable.Rows.Add(compteur, Date.Year, Date.Month, Date.Day, c_OustandingBalance, c_InterestPaid,
                                                         cummInterestPaid, c_PrincipalPaid, cummPrincipalPaid, Balance, Date.Month.ToString() + "-" + Date.Day.ToString() + "-" + Date.Year.ToString());



                 if (Frequency == 12)
                 {
                     Date = Date.AddMonths(1);
                 }

                 if (Frequency == 26)
                 {
                     Date = Date.AddDays(14);
                 }

                 if (Frequency == 52)
                 {
                     Date = Date.AddDays(7);
                 }

                 compteur = compteur + 1;

             }


             //   return AmortizationDS;

             return amortizatioTable;


         }



















        /// <summary>
         /// Amortization table for interest Compounded twice a year , every 6 months ( Canada )
        /// </summary>
        /// <param name="from"></param>
        /// <param name="a_balance"></param>
        /// <param name="a_duration"></param>
        /// <param name="a_frequency"></param>
        /// <param name="a_interest"></param>
        /// <param name="a_additinalAmount"></param>
        /// <param name="a_addAmountEachYear"></param>
        /// <param name="a_eachYear"></param>
        /// <returns></returns>
         public DataTable getAmortizationCanTable(DateTime from, double a_balance, double a_duration, double a_frequency,
                                               double a_interest, double a_additinalAmount, double a_addAmountEachYear, double a_eachYear)
         {

             //ATTENTION ON PEUT AUSSI RETOURNER UN DATASET
             // MAIS IL PARAIT QUE LE DATATABLE EST PLUS "EFFICIENT"
             //  
            // DataSet AmortizationDS = new DataSet();

           //  DataTable amortizatioTable = AmortizationDS.Tables.Add();

             DataTable amortizatioTable = new DataTable();


             //– Add columns to the data table
             amortizatioTable.Columns.Add("ID", typeof(int));
             amortizatioTable.Columns.Add("theyear", typeof(int));
             amortizatioTable.Columns.Add("themonth", typeof(int));
             amortizatioTable.Columns.Add("theday", typeof(int));
             amortizatioTable.Columns.Add("OustandingBalance", typeof(double));
             amortizatioTable.Columns.Add("InterestPaid", typeof(double));
             amortizatioTable.Columns.Add("CummInterestPaid", typeof(double));
             amortizatioTable.Columns.Add("PrincipalPaid", typeof(double));
             amortizatioTable.Columns.Add("CummPrincipalPaid", typeof(double));
             amortizatioTable.Columns.Add("Balance", typeof(double));
             amortizatioTable.Columns.Add("ladate", typeof(string));


             double AccPayment = 0;
             double InterestRate, PeriodRate, Balance;
             double Duration, Frequency, Payment; //-- Mortgage payment
             double number_of_insert;
             double c_InterestPaid, c_CummInterestPaid, c_PrincipalPaid;
             Date la_date = new Date();

             double AdditinalAmount;
             double AdditinalAmount2 = 0;

             double c_OustandingBalance, c_Balance;



             AdditinalAmount = a_additinalAmount;
             InterestRate = a_interest / 100;
             Frequency = a_frequency; //12 equal each month;

             if (Frequency == 241)
             {
                 Frequency = 24;

             }

             if (Frequency == 521)
             {

                 Frequency = 52;
             }

             PeriodRate = InterestRate / Frequency;
             Balance = a_balance;
             Duration = a_duration;// Ammortization Period;

             if( Frequency== 24 )
                   Frequency= 26;


             PeriodRate = Math.Pow((1 + InterestRate / 2), (2.00 / @Frequency)) - 1;

             Payment = ((Balance * Math.Pow(1.0 + PeriodRate, (Duration * Frequency))) * PeriodRate)
                    / (Math.Pow(1.0 + PeriodRate, Duration * Frequency) - 1);

             // Pour payment acc by-weekly et acc weekly on prend monthly et on divise par 2 et par 4
             //
             //
             //
             //

             double cummInterestPaid, cummPrincipalPaid;
             int compteur;
             DateTime Date = new DateTime();

             Date = from;
             compteur = 1;
             number_of_insert = Duration * Frequency;

             cummInterestPaid = 0;
             cummPrincipalPaid = 0;

             if (Frequency == 24)
                 PeriodRate = @InterestRate / 26;


             //– Add rows to the data table

             int insertperyear = 0;

         // we begin to pay not in the first day of the mortgage but in the next period
             /////////////////////////////////////////////////////////////
             if (Frequency == 12)
             {
                 Date = Date.AddMonths(1);
             }

             if (Frequency == 26)
             {
                 Date = Date.AddDays(14);
             }

             if (Frequency == 52)
             {
                 Date = Date.AddDays(7);
             }
             //////////////////////////////////////////////////////////////



             //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             ///////////////accelerated payments///////////////////////////////////////////////////////////////////////////

             if (a_frequency == 241)
             {
                 //    AccPayment = ((Balance * Math.Pow(1.0 + (InterestRate / 12.00), (Duration * 12.00))) * (InterestRate / 12.00))
                 //        / (Math.Pow(1.0 + (InterestRate / 12.00), Duration * 12.00) - 1);

                 AccPayment = ((Balance * Math.Pow(1.0 + (Math.Pow((1 + InterestRate / 2), (2.00 / 12)) - 1), (Duration * 12))) * (Math.Pow((1 + InterestRate / 2), (2.00 / 12)) - 1))
                     / (Math.Pow(1.0 + (Math.Pow((1 + InterestRate / 2), (2.00 / 12)) - 1), Duration * 12) - 1);

                 AccPayment = AccPayment / 2.00;
                 // PeriodRate = @InterestRate / 26;
                 Payment = AccPayment;
             }

             if (a_frequency == 521)
             {

                 // AccPayment = ((Balance * Math.Pow(1.0 + (InterestRate / 12.00), (Duration * 12.00))) * (InterestRate / 12.00))
                 //  / (Math.Pow(1.0 + (InterestRate / 12.00), Duration * 12.00) - 1);

                 AccPayment = ((Balance * Math.Pow(1.0 + (Math.Pow((1 + InterestRate / 2), (2.00 / 12)) - 1), (Duration * 12))) * (Math.Pow((1 + InterestRate / 2), (2.00 / 12)) - 1))
                    / (Math.Pow(1.0 + (Math.Pow((1 + InterestRate / 2), (2.00 / 12)) - 1), Duration * 12) - 1);

                 AccPayment = AccPayment / 4.00;
                 //  PeriodRate = @InterestRate / 52;
                 Payment = AccPayment;
             }
             /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             ///////////\///////////////////////////////////////////////////////////////////////////


             while (compteur < number_of_insert + 1 && Balance > 0)
             {

              //--------------------------------------------
              //--------------------------------------------
                 //  In this section we make sure that the amounts that is supposed to be added once a year
                 // gets only added once a year, for that we utilize a counter (insertperyear) .
                 // Once the amount is addded for the particular month, the counter is set to one . And when
                 // we leave the month the counter is reset to 0.
                 AdditinalAmount2 = 0;

                 if (Date.Month == a_eachYear)
                 {
                     AdditinalAmount2 = a_addAmountEachYear;
                     insertperyear++;
                 }

                 if (Date.Month == a_eachYear && insertperyear > 1)
                 {
                     AdditinalAmount2 = 0;

                 }

                 if (Date.Month != a_eachYear)
                 {
                     insertperyear = 0;
                 }
             //--------------------------------------------------

                 c_OustandingBalance = Balance; //
                 c_InterestPaid = Balance * PeriodRate;
                 Balance = (Balance * (1 + PeriodRate)) - Payment - AdditinalAmount - AdditinalAmount2;
                 cummInterestPaid = cummInterestPaid + c_InterestPaid;
                 c_PrincipalPaid = Payment - c_InterestPaid + AdditinalAmount + AdditinalAmount2;

                 cummPrincipalPaid = cummPrincipalPaid + c_PrincipalPaid;

                 if (Balance < 0)
                  {

                      if (a_frequency == 241 || a_frequency == 521 || Frequency == 26 || Frequency == 52 || Frequency == 12)
                          amortizatioTable.Rows.Add(compteur, Date.Year, Date.Month, Date.Day, c_OustandingBalance, c_InterestPaid,
                                                       cummInterestPaid + c_InterestPaid, c_PrincipalPaid + Balance, cummPrincipalPaid + Balance, 0, Date.Month.ToString() + "-" + Date.Day.ToString() + "-" + Date.Year.ToString());
                                                                                                                                  
                      else     // si ca ne marche pas enleve le if ci-dessus
                          amortizatioTable.Rows.Add(compteur, Date.Year, Date.Month, Date.Day, c_OustandingBalance, c_InterestPaid,
                                                       cummInterestPaid + Balance, c_PrincipalPaid + Balance, cummPrincipalPaid + Balance, 0, Date.Month.ToString() + "-" + Date.Day.ToString() + "-" + Date.Year.ToString());
                  }
                 else
                     amortizatioTable.Rows.Add(compteur, Date.Year, Date.Month, Date.Day, c_OustandingBalance, c_InterestPaid,
                                                        cummInterestPaid, c_PrincipalPaid, cummPrincipalPaid, Balance, Date.Month.ToString() + "-" + Date.Day.ToString() + "-" + Date.Year.ToString());


                 if (Frequency == 12)
                 {
                     Date = Date.AddMonths(1);
                 }

                 if (Frequency == 26)
                 {
                     Date = Date.AddDays(14);
                 }

                 if (Frequency == 52)
                 {
                     Date = Date.AddDays(7);
                 }

                 compteur = compteur + 1;

             }


            // return AmortizationDS;

             return amortizatioTable;


         }









         /// <summary>
         /// Amortization table for interest Compounded twice a year , every 6 months ( Canada )
         /// </summary>
         /// <param name="from"></param>
         /// <param name="a_balance"></param>
         /// <param name="a_duration"></param>
         /// <param name="a_frequency"></param>
         /// <param name="a_interest"></param>
         /// <param name="a_additinalAmount"></param>
         /// <param name="a_addAmountEachYear"></param>
         /// <param name="a_eachYear"></param>
         /// <returns></returns>
         public DataTable getAmortizationPartialCanTable(DateTime from,DateTime to ,double a_balance, double a_duration, double a_frequency,
                                               double a_interest, double a_additinalAmount, double a_addAmountEachYear, double a_eachYear)
         {

             //ATTENTION ON PEUT AUSSI RETOURNER UN DATASET
             // MAIS IL PARAIT QUE LE DATATABLE EST PLUS "EFFICIENT"
             //  
             // DataSet AmortizationDS = new DataSet();

             //  DataTable amortizatioTable = AmortizationDS.Tables.Add();

             DataTable amortizatioTable = new DataTable();


             //– Add columns to the data table
             amortizatioTable.Columns.Add("ID", typeof(int));
             amortizatioTable.Columns.Add("theyear", typeof(int));
             amortizatioTable.Columns.Add("themonth", typeof(int));
             amortizatioTable.Columns.Add("theday", typeof(int));
             amortizatioTable.Columns.Add("OustandingBalance", typeof(double));
             amortizatioTable.Columns.Add("InterestPaid", typeof(double));
             amortizatioTable.Columns.Add("CummInterestPaid", typeof(double));
             amortizatioTable.Columns.Add("PrincipalPaid", typeof(double));
             amortizatioTable.Columns.Add("CummPrincipalPaid", typeof(double));
             amortizatioTable.Columns.Add("Balance", typeof(double));
             amortizatioTable.Columns.Add("ladate", typeof(string));


             double AccPayment = 0;
             double InterestRate, PeriodRate, Balance;
             double Duration, Frequency, Payment; //-- Mortgage payment
             double number_of_insert;
             double c_InterestPaid, c_CummInterestPaid, c_PrincipalPaid;
             Date la_date = new Date();

             double AdditinalAmount;
             double AdditinalAmount2 = 0;

             double c_OustandingBalance, c_Balance;



             AdditinalAmount = a_additinalAmount;
             InterestRate = a_interest / 100;
             Frequency = a_frequency; //12 equal each month;

             if (Frequency == 241)
             {
                 Frequency = 24;

             }

             if (Frequency == 521)
             {

                 Frequency = 52;
             }

             PeriodRate = InterestRate / Frequency;
             Balance = a_balance;
             Duration = a_duration;// Ammortization Period;

             if (Frequency == 24)
                 Frequency = 26;


             PeriodRate = Math.Pow((1 + InterestRate / 2), (2.00 / @Frequency)) - 1;

             Payment = ((Balance * Math.Pow(1.0 + PeriodRate, (Duration * Frequency))) * PeriodRate)
                    / (Math.Pow(1.0 + PeriodRate, Duration * Frequency) - 1);

             // Pour payment acc by-weekly et acc weekly on prend monthly et on divise par 2 et par 4
             //
             //
             //
             //

             double cummInterestPaid, cummPrincipalPaid;
             int compteur;
             DateTime Date = new DateTime();
             DateTime toDate = new DateTime(); 

             Date   = from;
             toDate = to;

             compteur = 1;
             number_of_insert = Duration * Frequency;

             cummInterestPaid = 0;
             cummPrincipalPaid = 0;

             if (Frequency == 24)
                 PeriodRate = @InterestRate / 26;


             //– Add rows to the data table

             int insertperyear = 0;

             // we begin to pay not in the first day of the mortgage but in the next period
             /////////////////////////////////////////////////////////////
             if (Frequency == 12)
             {
                 Date = Date.AddMonths(1);
             }

             if (Frequency == 26)
             {
                 Date = Date.AddDays(14);
             }

             if (Frequency == 52)
             {
                 Date = Date.AddDays(7);
             }
             //////////////////////////////////////////////////////////////



             //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             ///////////////accelerated payments///////////////////////////////////////////////////////////////////////////

             if (a_frequency == 241)
             {
                 //    AccPayment = ((Balance * Math.Pow(1.0 + (InterestRate / 12.00), (Duration * 12.00))) * (InterestRate / 12.00))
                 //        / (Math.Pow(1.0 + (InterestRate / 12.00), Duration * 12.00) - 1);

                 AccPayment = ((Balance * Math.Pow(1.0 + (Math.Pow((1 + InterestRate / 2), (2.00 / 12)) - 1), (Duration * 12))) * (Math.Pow((1 + InterestRate / 2), (2.00 / 12)) - 1))
                     / (Math.Pow(1.0 + (Math.Pow((1 + InterestRate / 2), (2.00 / 12)) - 1), Duration * 12) - 1);

                 AccPayment = AccPayment / 2.00;
                 // PeriodRate = @InterestRate / 26;
                 Payment = AccPayment;
             }

             if (a_frequency == 521)
             {

                 // AccPayment = ((Balance * Math.Pow(1.0 + (InterestRate / 12.00), (Duration * 12.00))) * (InterestRate / 12.00))
                 //  / (Math.Pow(1.0 + (InterestRate / 12.00), Duration * 12.00) - 1);

                 AccPayment = ((Balance * Math.Pow(1.0 + (Math.Pow((1 + InterestRate / 2), (2.00 / 12)) - 1), (Duration * 12))) * (Math.Pow((1 + InterestRate / 2), (2.00 / 12)) - 1))
                    / (Math.Pow(1.0 + (Math.Pow((1 + InterestRate / 2), (2.00 / 12)) - 1), Duration * 12) - 1);

                 AccPayment = AccPayment / 4.00;
                 //  PeriodRate = @InterestRate / 52;
                 Payment = AccPayment;
             }
             /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             ///////////\///////////////////////////////////////////////////////////////////////////


             while ((compteur < number_of_insert + 1 && Balance > 0) && Date <= toDate)
             {

                 //--------------------------------------------
                 //--------------------------------------------
                 //  In this section we make sure that the amounts that is supposed to be added once a year
                 // gets only added once a year, for that we utilize a counter (insertperyear) .
                 // Once the amount is addded for the particular month, the counter is set to one . And when
                 // we leave the month the counter is reset to 0.
                 AdditinalAmount2 = 0;

                 if (Date.Month == a_eachYear)
                 {
                     AdditinalAmount2 = a_addAmountEachYear;
                     insertperyear++;
                 }

                 if (Date.Month == a_eachYear && insertperyear > 1)
                 {
                     AdditinalAmount2 = 0;

                 }

                 if (Date.Month != a_eachYear)
                 {
                     insertperyear = 0;
                 }
                 //--------------------------------------------------

                 c_OustandingBalance = Balance; //
                 c_InterestPaid = Balance * PeriodRate;
                 Balance = (Balance * (1 + PeriodRate)) - Payment - AdditinalAmount - AdditinalAmount2;
                 cummInterestPaid = cummInterestPaid + c_InterestPaid;
                 c_PrincipalPaid = Payment - c_InterestPaid + AdditinalAmount + AdditinalAmount2;

                 cummPrincipalPaid = cummPrincipalPaid + c_PrincipalPaid;

                 if (Balance < 0)
                 {

                     if (a_frequency == 241 || a_frequency == 521 || Frequency == 26 || Frequency == 52 || Frequency == 12)
                         amortizatioTable.Rows.Add(compteur, Date.Year, Date.Month, Date.Day, c_OustandingBalance, c_InterestPaid,
                                                      cummInterestPaid + c_InterestPaid, c_PrincipalPaid + Balance, cummPrincipalPaid + Balance, 0, Date.Month.ToString() + "-" + Date.Day.ToString() + "-" + Date.Year.ToString());

                     else     // si ca ne marche pas enleve le if ci-dessus
                         amortizatioTable.Rows.Add(compteur, Date.Year, Date.Month, Date.Day, c_OustandingBalance, c_InterestPaid,
                                                      cummInterestPaid + Balance, c_PrincipalPaid + Balance, cummPrincipalPaid + Balance, 0, Date.Month.ToString() + "-" + Date.Day.ToString() + "-" + Date.Year.ToString());
                 }
                 else
                     amortizatioTable.Rows.Add(compteur, Date.Year, Date.Month, Date.Day, c_OustandingBalance, c_InterestPaid,
                                                        cummInterestPaid, c_PrincipalPaid, cummPrincipalPaid, Balance, Date.Month.ToString() + "-" + Date.Day.ToString() + "-" + Date.Year.ToString());


                 if (Frequency == 12)
                 {
                     Date = Date.AddMonths(1);
                 }

                 if (Frequency == 26)
                 {
                     Date = Date.AddDays(14);
                 }

                 if (Frequency == 52)
                 {
                     Date = Date.AddDays(7);
                 }

                 compteur = compteur + 1;

             }


             // return AmortizationDS;

             return amortizatioTable;


         }

















         public DataSet getAmortizationCalculation(DateTime from, double a_balance, double a_duration, double a_frequency,
                                              double a_interest, double a_additinalAmount, double a_addAmountEachYear, int a_eachYear)
         {
             DataSet AmortizationDS = new DataSet();

             DataTable amortizatioTable = AmortizationDS.Tables.Add();



             //– Add columns to the data table
             amortizatioTable.Columns.Add("ID", typeof(int));
             amortizatioTable.Columns.Add("theyear", typeof(int));
             amortizatioTable.Columns.Add("themonth", typeof(int));
             amortizatioTable.Columns.Add("theday", typeof(int));
             amortizatioTable.Columns.Add("OustandingBalance", typeof(double));
             amortizatioTable.Columns.Add("InterestPaid", typeof(double));
             amortizatioTable.Columns.Add("CummInterestPaid", typeof(double));
             amortizatioTable.Columns.Add("PrincipalPaid", typeof(double));
             amortizatioTable.Columns.Add("CummPrincipalPaid", typeof(double));
             amortizatioTable.Columns.Add("Balance", typeof(double));
             amortizatioTable.Columns.Add("Payments", typeof(double));
             amortizatioTable.Columns.Add("TimeToPayLoan", typeof(double));
             amortizatioTable.Columns.Add("AvgPeriodInterest", typeof(double));
             amortizatioTable.Columns.Add("PeriodRate", typeof(double));
             amortizatioTable.Columns.Add("FiftyFiftyPoint_m", typeof(double));
             amortizatioTable.Columns.Add("FiftyFiftyPoint_y", typeof(double));
             amortizatioTable.Columns.Add("TotalPaid", typeof(double));
             amortizatioTable.Columns.Add("ExtraPayments", typeof(double));
             


             double AccPayment = 0;
             double InterestRate, PeriodRate, Balance;
             double Duration, Frequency, Payment; //-- Mortgage payment
             double number_of_insert;
             double c_InterestPaid, c_CummInterestPaid, c_PrincipalPaid;
             Date la_date = new Date();

             double AdditinalAmount;
             double AdditinalAmount2 = 0;
             double c_OustandingBalance, c_Balance;



             AdditinalAmount = a_additinalAmount;
             InterestRate = a_interest / 100;
             Frequency = a_frequency; //12 equal each month;

             if (Frequency == 241)
             {
                 Frequency = 24;

             }

             if (Frequency == 521)
             {

                 Frequency = 52;
             }

             PeriodRate = InterestRate / Frequency;
             Balance = a_balance;
             Duration = a_duration;// Ammortization Period;

             if (Frequency == 24)
             {
                 Frequency = 26;
                 PeriodRate = @InterestRate / 26;
             }


             Payment = ((Balance * Math.Pow(1.0 + PeriodRate, (Duration * Frequency))) * PeriodRate)
                    / (Math.Pow(1.0 + PeriodRate, Duration * Frequency) - 1);


             double cummInterestPaid, cummPrincipalPaid;
             int compteur;
             DateTime Date = new DateTime();

             Date = from;
             compteur = 1;
             number_of_insert = Duration * Frequency;

             cummInterestPaid = 0;
             cummPrincipalPaid = 0;



             if (Frequency == 52)
                 PeriodRate = @InterestRate / 52;

             int insertperyear = 0;

             //– Add rows to the data table

             // we begin to pay not in the first day of the mortgage but in the next period
             /////////////////////////////////////////////////////////////
             if (Frequency == 12)
             {
                 Date = Date.AddMonths(1);
             }

             if (Frequency == 26)
             {
                 Date = Date.AddDays(14);
             }

             if (Frequency == 52)
             {
                 Date = Date.AddDays(7);
             }
             /////////////////////////////////////////////////////////////

             // serve compteur2 to check the 50/50 point, when it is found the counter is increased to 1
             int compteur2 = 0;
             int FiftyFiftyPoint_m = 0;
             int FiftyFiftyPoint_y = 0;
             int compteur3 =0;


             //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             ///////////////accelerated payments///////////////////////////////////////////////////////////////////////////

             if (a_frequency == 241)
             {
                 AccPayment = ((Balance * Math.Pow(1.0 + (InterestRate / 12.00), (Duration * 12.00))) * (InterestRate / 12.00))
                     / (Math.Pow(1.0 + (InterestRate / 12.00), Duration * 12.00) - 1);


                 AccPayment = AccPayment / 2.00;
                 // PeriodRate = @InterestRate / 26;
                 Payment = AccPayment;
             }

             if (a_frequency == 521)
             {

                 AccPayment = ((Balance * Math.Pow(1.0 + (InterestRate / 12.00), (Duration * 12.00))) * (InterestRate / 12.00))
                       / (Math.Pow(1.0 + (InterestRate / 12.00), Duration * 12.00) - 1);

                 AccPayment = AccPayment / 4.00;
                 //  PeriodRate = @InterestRate / 52;
                 Payment = AccPayment;
             }
             /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             ///////////\///////////////////////////////////////////////////////////////////////////


             while (compteur < number_of_insert + 1 || Balance > 0)
             {


                 //--------------------------------------------
                 //--------------------------------------------
                 //  In this section we make sure that the amounts that is supposed to be added once a year
                 // gets only added once a year, for that we utilize a counter (insertperyear) .
                 // Once the amount is addded for the particular month, the counter is set to one . And when
                 // we leave the month the counter is reset to 0.
                 AdditinalAmount2 = 0;

                 if (Date.Month == a_eachYear)
                 {
                     AdditinalAmount2 = a_addAmountEachYear;
                     insertperyear++;
                 }

                 if (Date.Month == a_eachYear && insertperyear > 1)
                 {
                     AdditinalAmount2 = 0;

                 }

                 if (Date.Month != a_eachYear)
                 {
                     insertperyear = 0;
                 }


                 c_OustandingBalance = Balance; //
                 c_InterestPaid = Balance * PeriodRate;
                 Balance = (Balance * (1 + PeriodRate)) - Payment - AdditinalAmount - AdditinalAmount2;
                 cummInterestPaid = cummInterestPaid + c_InterestPaid;
                 c_PrincipalPaid = Payment - c_InterestPaid + AdditinalAmount + AdditinalAmount2;

                 cummPrincipalPaid = cummPrincipalPaid + c_PrincipalPaid;

                 // Here we are checking to find the 50/50 point for the mortgage
                 if ( c_PrincipalPaid >= c_InterestPaid && compteur2 < 1)
                 {

                     FiftyFiftyPoint_m = Date.Month;
                     FiftyFiftyPoint_y = Date.Year;
                     compteur2++;
                 }

                 // the last entry in the table
                 if (c_OustandingBalance <= Payment && compteur3 < 1)
                 {
                     double TimeToPayLoan = 0;
                     double AvgPeriodInterest = cummInterestPaid / compteur;
                     TimeSpan diff;
                     if (Frequency == 12)
                     {




                     TimeToPayLoan = compteur /12.00 ;
                         
                        // DateTime first=Convert.ToDateTime("4/7/2007");
                       //DateTime second=Convert.ToDateTime("4/7/2006");
                       // diff = Date.Subtract(from);
                       // double totalDays = (double)diff.TotalDays ;
                       // TimeToPayLoan = totalDays / 365;

                     }

                     if (Frequency == 26)
                     {
                       //  TimeToPayLoan = (compteur * 14) /365.00;

                         diff = Date.Subtract(from);
                         double totalDays = (double)diff.TotalDays;
                         TimeToPayLoan = totalDays / 365;
                     }

                     if (Frequency == 52)
                     {
                         diff = Date.Subtract(from);
                         double totalDays = (double)diff.TotalDays;
                         TimeToPayLoan = totalDays / 365;
                     }

                     //accelerated
                     if (a_frequency == 241 || a_frequency == 521 || Frequency == 26 || Frequency == 52 || Frequency == 12)
                     {
                         amortizatioTable.Rows.Add(compteur, Date.Year, Date.Month, Date.Day, c_OustandingBalance, c_InterestPaid,
                                                            cummInterestPaid + c_InterestPaid, c_PrincipalPaid + Balance, cummPrincipalPaid + Balance, 0,
                                                            Payment, TimeToPayLoan, AvgPeriodInterest, PeriodRate * 100,
                                                               FiftyFiftyPoint_m, FiftyFiftyPoint_y, cummInterestPaid + a_balance, a_additinalAmount + Payment);
                        
                     }
                     else
                         amortizatioTable.Rows.Add(compteur, Date.Year, Date.Month, Date.Day, c_OustandingBalance, c_InterestPaid,
                                                           cummInterestPaid + Balance, c_PrincipalPaid + Balance, cummPrincipalPaid + Balance, 0,
                                                           Payment, TimeToPayLoan, AvgPeriodInterest, PeriodRate * 100,
                                                              FiftyFiftyPoint_m, FiftyFiftyPoint_y, cummInterestPaid + a_balance, a_additinalAmount + Payment);
                      compteur3++;
                 }

                 if (Frequency == 12)
                 {
                     Date = Date.AddMonths(1);
                 }

                 if (Frequency == 26)
                 {
                     Date = Date.AddDays(14);
                 }

                 if (Frequency == 52)
                 {
                     Date = Date.AddDays(7);
                 }

                 compteur = compteur + 1;

             }


             return AmortizationDS;


         }
        /// <summary>
        ///
        /// </summary>
        /// <param name="from"></param>
        /// <param name="a_balance"></param>
        /// <param name="a_duration"></param>
        /// <param name="a_frequency"></param>
        /// <param name="a_interest"></param>
        /// <param name="a_additinalAmount"></param>
        /// <param name="a_addAmountEachYear"></param>
        /// <param name="a_eachYear"></param>
        /// <returns></returns>
         public DataSet getAmortizationCanCalculation(DateTime from, double a_balance, double a_duration, double a_frequency,
                                               double a_interest, double a_additinalAmount, double a_addAmountEachYear, double a_eachYear)
         {
             DataSet AmortizationDS = new DataSet();

             DataTable amortizatioTable = AmortizationDS.Tables.Add();



             //– Add columns to the data table
             amortizatioTable.Columns.Add("ID", typeof(int));
             amortizatioTable.Columns.Add("theyear", typeof(int));
             amortizatioTable.Columns.Add("themonth", typeof(int));
             amortizatioTable.Columns.Add("theday", typeof(int));
             amortizatioTable.Columns.Add("OustandingBalance", typeof(double));
             amortizatioTable.Columns.Add("InterestPaid", typeof(double));
             amortizatioTable.Columns.Add("CummInterestPaid", typeof(double));
             amortizatioTable.Columns.Add("PrincipalPaid", typeof(double));
             amortizatioTable.Columns.Add("CummPrincipalPaid", typeof(double));
             amortizatioTable.Columns.Add("Balance", typeof(double));
             amortizatioTable.Columns.Add("Payments", typeof(double));
             amortizatioTable.Columns.Add("TimeToPayLoan", typeof(double));
             amortizatioTable.Columns.Add("AvgPeriodInterest", typeof(double));
             amortizatioTable.Columns.Add("PeriodRate", typeof(double));
             amortizatioTable.Columns.Add("FiftyFiftyPoint_m", typeof(double));
             amortizatioTable.Columns.Add("FiftyFiftyPoint_y", typeof(double));
             amortizatioTable.Columns.Add("TotalPaid", typeof(double));
             amortizatioTable.Columns.Add("ExtraPayments", typeof(double));


             double AccPayment = 0;
             double InterestRate, PeriodRate, Balance;
             double Duration, Frequency, Payment; //-- Mortgage payment
             double number_of_insert;
             double c_InterestPaid, c_CummInterestPaid, c_PrincipalPaid;
             Date la_date = new Date();

             double AdditinalAmount;
             double AdditinalAmount2 = 0;

             double c_OustandingBalance, c_Balance;



             AdditinalAmount = a_additinalAmount;
             InterestRate = a_interest / 100;
             Frequency = a_frequency; //12 equal each month;

             if (Frequency == 241)
             {
                 Frequency = 24;

             }

             if (Frequency == 521)
             {

                 Frequency = 52;
             }

             PeriodRate = InterestRate / Frequency;
             Balance = a_balance;
             Duration = a_duration;// Ammortization Period;

             if (Frequency == 24)
                 Frequency = 26;


             PeriodRate = Math.Pow((1 + InterestRate / 2), (2.00 / @Frequency)) - 1;

             Payment = ((Balance * Math.Pow(1.0 + PeriodRate, (Duration * Frequency))) * PeriodRate)
                    / (Math.Pow(1.0 + PeriodRate, Duration * Frequency) - 1);

             // Pour payment acc by-weekly et acc weekly on prend monthly et on divise par 2 et par 4
             //
             //
             //
             //

             double cummInterestPaid, cummPrincipalPaid;
             int compteur;
             DateTime Date = new DateTime();

             Date = from;
             compteur = 1;
             number_of_insert = Duration * Frequency;

             cummInterestPaid = 0;
             cummPrincipalPaid = 0;

             if (Frequency == 24)
                 PeriodRate = @InterestRate / 26;


             //– Add rows to the data table

             int insertperyear = 0;

             // we begin to pay not in the first day of the mortgage but in the next period
             /////////////////////////////////////////////////////////////
             if (Frequency == 12)
             {
                 Date = Date.AddMonths(1);
             }

             if (Frequency == 26)
             {
                 Date = Date.AddDays(14);
             }

             if (Frequency == 52)
             {
                 Date = Date.AddDays(7);
             }
             //////////////////////////////////////////////////////////////


            // serve compteur2 to check the 50/50 point, when it is found the counter is increased to 1
             int compteur2 = 0;
             int compteur3 = 0;
             int FiftyFiftyPoint_m = 0;
             int FiftyFiftyPoint_y = 0;


             //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             ///////////////accelerated payments///////////////////////////////////////////////////////////////////////////

             if (a_frequency == 241)
             {
                 //    AccPayment = ((Balance * Math.Pow(1.0 + (InterestRate / 12.00), (Duration * 12.00))) * (InterestRate / 12.00))
                 //        / (Math.Pow(1.0 + (InterestRate / 12.00), Duration * 12.00) - 1);

                 AccPayment = ((Balance * Math.Pow(1.0 + (Math.Pow((1 + InterestRate / 2), (2.00 / 12)) - 1), (Duration * 12))) * (Math.Pow((1 + InterestRate / 2), (2.00 / 12)) - 1))
                     / (Math.Pow(1.0 + (Math.Pow((1 + InterestRate / 2), (2.00 / 12)) - 1), Duration * 12) - 1);

                 AccPayment = AccPayment / 2.00;
                 // PeriodRate = @InterestRate / 26;
                 Payment = AccPayment;
             }

             if (a_frequency == 521)
             {

                 // AccPayment = ((Balance * Math.Pow(1.0 + (InterestRate / 12.00), (Duration * 12.00))) * (InterestRate / 12.00))
                 //  / (Math.Pow(1.0 + (InterestRate / 12.00), Duration * 12.00) - 1);

                 AccPayment = ((Balance * Math.Pow(1.0 + (Math.Pow((1 + InterestRate / 2), (2.00 / 12)) - 1), (Duration * 12))) * (Math.Pow((1 + InterestRate / 2), (2.00 / 12)) - 1))
                    / (Math.Pow(1.0 + (Math.Pow((1 + InterestRate / 2), (2.00 / 12)) - 1), Duration * 12) - 1);

                 AccPayment = AccPayment / 4.00;
                 //  PeriodRate = @InterestRate / 52;
                 Payment = AccPayment;
             }
             /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
             ///////////\///////////////////////////////////////////////////////////////////////////


             while (compteur < number_of_insert + 1 || Balance > 0)
             {

                 //--------------------------------------------
                 //--------------------------------------------
                 //  In this section we make sure that the amounts that is supposed to be added once a year
                 // gets only added once a year, for that we utilize a counter (insertperyear) .
                 // Once the amount is addded for the particular month, the counter is set to one . And when
                 // we leave the month the counter is reset to 0.
                 AdditinalAmount2 = 0;

                 if (Date.Month == a_eachYear)
                 {
                     AdditinalAmount2 = a_addAmountEachYear;
                     insertperyear++;
                 }

                 if (Date.Month == a_eachYear && insertperyear > 1)
                 {
                     AdditinalAmount2 = 0;

                 }

                 if (Date.Month != a_eachYear)
                 {
                     insertperyear = 0;
                 }
                 //--------------------------------------------------

                 c_OustandingBalance = Balance; //
                 c_InterestPaid = Balance * PeriodRate;
                 Balance = (Balance * (1 + PeriodRate)) - Payment - AdditinalAmount - AdditinalAmount2;
                 cummInterestPaid = cummInterestPaid + c_InterestPaid;
                 c_PrincipalPaid = Payment - c_InterestPaid + AdditinalAmount + AdditinalAmount2;

                 cummPrincipalPaid = cummPrincipalPaid + c_PrincipalPaid;



                 // Here we are checking to find the 50/50 point for the mortgage
                 if (c_PrincipalPaid >= c_InterestPaid && compteur2 < 1)
                 {

                     FiftyFiftyPoint_m = Date.Month;
                     FiftyFiftyPoint_y = Date.Year;
                     compteur2++;
                 }

                 // Balance last entry


                if (c_OustandingBalance <= Payment && compteur3 < 1)
                 {
                     double TimeToPayLoan = 0;
                     double AvgPeriodInterest = cummInterestPaid / compteur;
                     TimeSpan diff;
                     if (Frequency == 12)
                     {
                        TimeToPayLoan = compteur / 12.00;
                        // diff = Date.Subtract(from);
                        // double totalDays = (double)diff.TotalDays;
                        // TimeToPayLoan = totalDays / 365;
                     }

                     if (Frequency == 26)
                     {
                        // TimeToPayLoan = (compteur * 14) / 365.00;
                         diff = Date.Subtract(from);
                         double totalDays = (double)diff.TotalDays;
                         TimeToPayLoan = totalDays / 365;
                     }

                     if (Frequency == 52)
                     {
                         //TimeToPayLoan = (compteur * 7) / 365.00;
                         diff = Date.Subtract(from);
                         double totalDays = (double)diff.TotalDays;
                         TimeToPayLoan = totalDays / 365;
                     }

                    // for accelerated payments
                     if (a_frequency == 241 || a_frequency == 521 || Frequency == 26 || Frequency == 52 || Frequency == 12)
                      {
                          amortizatioTable.Rows.Add(compteur, Date.Year, Date.Month, Date.Day, c_OustandingBalance, c_InterestPaid,
                                                          cummInterestPaid + c_InterestPaid, c_PrincipalPaid + Balance, cummPrincipalPaid + Balance, 0,
                                                          Payment, TimeToPayLoan, AvgPeriodInterest, PeriodRate * 100,
                                                             FiftyFiftyPoint_m, FiftyFiftyPoint_y, cummInterestPaid + a_balance, a_additinalAmount + Payment);
                      }

                       else
                         amortizatioTable.Rows.Add(compteur, Date.Year, Date.Month, Date.Day, c_OustandingBalance, c_InterestPaid,
                                                         cummInterestPaid + Balance, c_PrincipalPaid + Balance, cummPrincipalPaid + Balance, 0,
                                                         Payment, TimeToPayLoan, AvgPeriodInterest, PeriodRate * 100,
                                                            FiftyFiftyPoint_m, FiftyFiftyPoint_y, cummInterestPaid + a_balance, a_additinalAmount + Payment);
                      compteur3++;
                 }

                 if (Frequency == 12)
                 {
                     Date = Date.AddMonths(1);
                 }

                 if (Frequency == 26)
                 {
                     Date = Date.AddDays(14);
                 }

                 if (Frequency == 52)
                 {
                     Date = Date.AddDays(7);
                 }

                 compteur = compteur + 1;

             }


             return AmortizationDS;


         }

        /// <summary>
        ///
        /// </summary>
        /// <param name="balance"></param>
        /// <param name="duration"></param>
        /// <param name="frequency"></param>
        /// <param name="interestRate"></param>
        /// <returns></returns>
        public double getAmortizationPayment(double balance,double duration,double frequency,
                                              double interestRate)
        {

            double  periodRate, Payment;

            periodRate = interestRate / frequency;

            Payment = ((balance * Math.Pow(1.0 + periodRate, (duration * frequency))) * periodRate)
                    / (Math.Pow(1.0 + periodRate, duration * frequency) - 1);


            return Payment ;

         }







    }





}