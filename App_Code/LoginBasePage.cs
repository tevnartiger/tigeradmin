﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.IO;
using System.IO.Compression;

public delegate void CultureChanged(object Sender, string CurrentCulture);
public class LoginBasePage : Page
{
    public event CultureChanged OnCultureChanged;


    public string LastCultureName
    {
        get
        {
            string lastCultureName = (string)Session["_lastCulture"];

            if (lastCultureName == null)
            {
                Session["_lastCulture"] = Thread.CurrentThread.CurrentCulture.Name;
                lastCultureName = Thread.CurrentThread.CurrentCulture.Name;
            }

            return lastCultureName;
        }
        set
        {
            Session["_lastCulture"] = value;
        }
    }


    protected override void InitializeCulture()
    {

      /*  SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("fnIPv4_InRange", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        conn.Open();
        //Add the params
        cmd.Parameters.Add("@return_status", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@ip_address_received", SqlDbType.VarChar, 15).Value = Request.UserHostAddress.ToString();
        cmd.Parameters.Add("@hour_time_frame", SqlDbType.Int).Value = 24;

        //execute the modification
        cmd.ExecuteNonQuery();


        if (Convert.ToInt32(cmd.Parameters["@return_status"].Value) == 1)
        {
            conn.Close();
            Session["ip_allowed"] = "0";
            Response.Redirect("http://www.msnbc.com", true);
        }
        else
        {
            conn.Close();
            Session["ip_allowed"] = "1";
        }


        if (Session["user_ip"] != null)
        {
            if (Session["user_ip"].ToString() != Request.UserHostAddress.ToString())
            {
                // ATTENTION ICI ON A AFFAIRE A SOIT UN VOL DE SESSION COOCKIE SOIT QUE
                // L'USAGER A ETE DECONNECTER ET RECONNECTER A INTERNET

                Session.Clear();
                Session.Abandon();                
                Response.Redirect("http://www.google.com", true);
            }
            //   else
            {
                //     context.Response.Redirect("http://www.sinfocatiger.com/login.aspx", true);

            }
        }
    */
        //TODO: make this prettier
        string lang = Request["ctl00$Language"];

        if (lang == null || lang == String.Empty)
            lang = LastCultureName;

        Thread.CurrentThread.CurrentCulture = new CultureInfo(lang);
        //  Thread.CurrentThread.CurrentCulture.DateTimeFormat = new CultureInfo(lang, false).DateTimeFormat;
        Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(lang);
    }

    protected override void OnLoadComplete(EventArgs e)
    {
        base.OnLoadComplete(e);

        if (LastCultureName != Thread.CurrentThread.CurrentCulture.Name)
        {
            LastCultureName = Thread.CurrentThread.CurrentCulture.Name;

            if (this.OnCultureChanged != null)
                this.OnCultureChanged(this, LastCultureName);
        }
    }

    //--- TO COMPREESS AND DECOMPRESS VIEWSTATE
    /*
     protected override void
     SavePageStateToPersistenceMedium(object state)
     {
         LosFormatter formatter = new LosFormatter();
         StringWriter writer = new StringWriter();
         formatter.Serialize(writer, state);
         string viewState = writer.ToString();
         byte[] data = Convert.FromBase64String(viewState);
         byte[] compressedData = ViewStateHelper.Compress(data);
         string str = Convert.ToBase64String(compressedData);
         ClientScript.RegisterHiddenField("__MYVIEWSTATE", str);
     }

     protected override object
       LoadPageStateFromPersistenceMedium()
     {
         string viewstate = Request.Form["__MYVIEWSTATE"];
         byte[] data = Convert.FromBase64String(viewstate);
         byte[] uncompressedData =
         ViewStateHelper.Decompress(data);
         string str = Convert.ToBase64String
         (uncompressedData);
         LosFormatter formatter = new LosFormatter();
         return formatter.Deserialize(str);
     }
     */


    public static class CompressViewState
    {
        public static byte[] Compress(byte[] data)
        {
            MemoryStream output = new MemoryStream();
            GZipStream gzip = new GZipStream(output,
                              CompressionMode.Compress, true);
            gzip.Write(data, 0, data.Length);
            gzip.Close();
            return output.ToArray();
        }

        public static byte[] Decompress(byte[] data)
        {
            MemoryStream input = new MemoryStream();
            input.Write(data, 0, data.Length);
            input.Position = 0;
            GZipStream gzip = new GZipStream(input,
                              CompressionMode.Decompress, true);
            MemoryStream output = new MemoryStream();
            byte[] buff = new byte[64];
            int read = -1;
            read = gzip.Read(buff, 0, buff.Length);
            while (read > 0)
            {
                output.Write(buff, 0, read);
                read = gzip.Read(buff, 0, buff.Length);
            }
            gzip.Close();
            return output.ToArray();
        }
    }



    private ObjectStateFormatter _formatter =
        new ObjectStateFormatter();

    protected override void
        SavePageStateToPersistenceMedium(object viewState)
    {
        MemoryStream ms = new MemoryStream();
        _formatter.Serialize(ms, viewState);
        byte[] viewStateArray = ms.ToArray();
        ClientScript.RegisterHiddenField("__COMPRESSEDVIEWSTATE",
            Convert.ToBase64String(
            CompressViewState.Compress(viewStateArray)));
    }
    protected override object
        LoadPageStateFromPersistenceMedium()
    {
        string vsString = Request.Form["__COMPRESSEDVIEWSTATE"];
        byte[] bytes = Convert.FromBase64String(vsString);
        bytes = CompressViewState.Decompress(bytes);
        return _formatter.Deserialize(
            Convert.ToBase64String(bytes));
    }

}




