﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// Summary description for TenantObjectAuthorization
/// </summary>
namespace sinfoca.tiger.security.TenantObjectAuthorization
{

    public class TenantObjectAuthorization
    {
        private string conn;
        public TenantObjectAuthorization(string conn)
        {
            this.conn = conn;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="tu_id"></param>
        /// <param name="ta_id"></param>
        /// <returns></returns>
        public bool AccomodationArchive(int schema_id, int name_id, int tu_id, int ta_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prTenant_AuthorizeAccomodationArchive", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;
            cmd.Parameters.Add("@ta_id", SqlDbType.Int).Value = ta_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public bool Home(int schema_id, int name_id, int home_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prTenant_AuthorizeHome", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.VarChar, 15).Value = schema_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="name_id"></param>
        /// <param name="tu_id"></param>
        /// <param name="tenant_id"></param>
        /// <returns></returns>
        public bool Tenant(int schema_id, int name_id, int tenant_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prTenant_AuthorizeTenant", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = tenant_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="name_id"></param>
        /// <param name="tu_id"></param>
        /// <param name="tenant_id"></param>
        /// <returns></returns>
        public bool TenantUnit(int schema_id, int name_id, int tu_id, int tenant_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prTenant_AuthorizeTenantUnit", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;
            cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = tenant_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }

        public bool TenantUnit(int schema_id, int name_id, int tu_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prTenant_AuthorizeTenantUnit", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;
            cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = 0;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="tt_id"></param>
        /// <returns></returns>
        public bool TermsAndConditions(int schema_id, int name_id, int tt_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prTenant_AuthorizeTermsAndConditions", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@tt_id", SqlDbType.Int).Value = tt_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="name_id"></param>
        /// <param name="tt_id"></param>
        /// <param name="tu_id"></param>
        /// <returns></returns>
        public bool TermsAndConditions(int schema_id, int name_id, int tt_id ,int tu_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prTenant_AuthorizeTermsAndConditions", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@tt_id", SqlDbType.Int).Value = tt_id;
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }



        public bool RentPaid(int schema_id, int name_id, int rp_id, int tu_id)
        {
            //SqlConnection conn = new SqlConnection(this.conn);
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));

            SqlCommand cmd = new SqlCommand("prTenant_AuthorizeRentPaid", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();

            //Add the params

            cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
            cmd.Parameters.Add("@rp_id", SqlDbType.Int).Value = rp_id;
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;

            cmd.ExecuteNonQuery();
            if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            {
                conn.Close();
                return true;

            }
            else
            {
                conn.Close();
                return false;
            }
        }





    }

}