using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Account
/// </summary>

namespace tiger.registration
{
    public class Account
    {
        private string str_conn;
        public Account(string str_conn)
        {
            this.str_conn = str_conn;
        }

        public int getDirtyAccount()
        {
            
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prFindDirtyAccount", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                //execute the insert
                cmd.ExecuteReader();
              return Convert.ToInt32(cmd.Parameters["@return"].Value);
 
            }
            catch (Exception err)
            {
                Email.sendErrorMail("Account.getdirtydata", err.ToString());

                return -1;
            }
          
            }

        public int replaceAccount(string name_lname, string name_fname, string name_email,string login_user, string login_pwd,int group_id, int schema_id, string schema_activation_code)
        { 
             //found dirty account
            //update new information to replace old info
          
            //generate salt and pwd
            string temp_salt = sinfoca.tiger.security.Hash.createSalt(30);
            string temp_hash = sinfoca.tiger.security.Hash.createPasswordHash(login_pwd, temp_salt);

            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prReplaceAccount", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@name_lname", SqlDbType.VarChar, 50).Value = name_lname;
                cmd.Parameters.Add("@name_fname", SqlDbType.VarChar, 50).Value = name_fname;
                cmd.Parameters.Add("@name_email", SqlDbType.VarChar, 75).Value = name_email;
                cmd.Parameters.Add("@login_user", SqlDbType.VarChar, 50).Value = login_user;
                cmd.Parameters.Add("@login_salt", SqlDbType.VarChar, 50).Value = temp_salt;
                cmd.Parameters.Add("@login_hash", SqlDbType.VarChar, 50).Value = temp_hash;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@schema_activation_code", SqlDbType.VarChar, 25).Value = schema_activation_code;
                //execute the insert
                cmd.ExecuteReader();
                return Convert.ToInt32(cmd.Parameters["@return"].Value);

            }
            catch (Exception err)
            {
                Email.sendErrorMail("Account.replaceAccount", err.ToString());
                conn.Close();
                return -1;
            }
                        
        }

        public int createAccount(string name_lname, string name_fname, string name_email, string login_user, string login_pwd, int group_id, string schema_activation_code)
        {

            //generate hash password
            string temp_salt = sinfoca.tiger.security.Hash.createSalt(30);
            string temp_hash = sinfoca.tiger.security.Hash.createPasswordHash(login_pwd, temp_salt);

            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prCreateAccount", conn);
            cmd.CommandType = CommandType.StoredProcedure;

          //  try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@name_lname", SqlDbType.VarChar, 50).Value = name_lname;
                cmd.Parameters.Add("@name_fname", SqlDbType.VarChar, 50).Value = name_fname;
                cmd.Parameters.Add("@name_email", SqlDbType.VarChar, 75).Value = name_email;
                cmd.Parameters.Add("@login_user", SqlDbType.VarChar, 50).Value = login_user;
                cmd.Parameters.Add("@login_salt", SqlDbType.VarChar, 50).Value = temp_salt;
                cmd.Parameters.Add("@login_hash", SqlDbType.VarChar, 50).Value = temp_hash;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
                cmd.Parameters.Add("@schema_activation_code", SqlDbType.VarChar, 25).Value = schema_activation_code;
                //execute the insert
                cmd.ExecuteReader();

                return Convert.ToInt32(cmd.Parameters["@return"].Value);
            }
         //   catch (Exception err)
            {
            //    tiger.Email.sendMail("runtime@sinfoca.com", "stevemorisseau@hotmail.com", "Account.createAccount", err.ToString());
             //   conn.Close();
             //   return -1;

            }




        }


        public int createAccount1(string name_lname, string name_fname, string name_email, string login_user, string login_pwd, int group_id, string schema_activation_code)
        {

            //generate hash password
            string temp_salt = sinfoca.tiger.security.Hash.createSalt(30);
            string temp_hash = sinfoca.tiger.security.Hash.createPasswordHash(login_pwd, temp_salt);

             SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prCreateAccount", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@name_lname", SqlDbType.VarChar, 50).Value = name_lname;
                cmd.Parameters.Add("@name_fname", SqlDbType.VarChar, 50).Value = name_fname;
                cmd.Parameters.Add("@name_email", SqlDbType.VarChar, 75).Value = name_email;
                cmd.Parameters.Add("@login_user", SqlDbType.VarChar, 50).Value = login_user;
                cmd.Parameters.Add("@login_salt", SqlDbType.VarChar, 50).Value = temp_salt;
                cmd.Parameters.Add("@login_hash", SqlDbType.VarChar, 50).Value = temp_hash;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;
                cmd.Parameters.Add("@schema_activation_code", SqlDbType.VarChar, 25).Value = schema_activation_code;
                //execute the insert
                cmd.ExecuteReader();

                return Convert.ToInt32(cmd.Parameters["@return"].Value);
            }
            catch (Exception err)
            {
                Email.sendSESMailHtml("runtime@sinfoca.com","stevemorisseau@hotmail.com","Account.createAccount", err.ToString());
                conn.Close();
                return -1;

            }


        
        
        }

        public int activateAccount(string login_user, string schema_activation_code, string ip)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prActivateAccount", conn);
            cmd.CommandType = CommandType.StoredProcedure;

          //  try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@login_user", SqlDbType.VarChar, 75).Value = login_user;
                cmd.Parameters.Add("@schema_activation_code", SqlDbType.VarChar, 50).Value = schema_activation_code;
                cmd.Parameters.Add("@ip", SqlDbType.VarChar, 20).Value = ip;
               
                //execute the insert
                cmd.ExecuteReader();

                return Convert.ToInt32(cmd.Parameters["@return"].Value);
            }
           // catch (Exception err)
            {
             //   tiger.Email.sendErrorMail("Account.createAccount", err.ToString());
             //   conn.Close();
              //  return -1;

            }
           
        }

        public string getSchemaActivationCode(string login_user)
        {
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prSchemaActivationCode", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();

                //Add the params
                cmd.Parameters.Add("@login_user", SqlDbType.VarChar, 75).Value = login_user;
                cmd.Parameters.Add("@schema_activation_code", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;

                cmd.ExecuteReader();
                return Convert.ToString(cmd.Parameters["@schema_activation_code"].Value);
            }
            finally
            {
                conn.Close();
            }
            {
            }

        }
    }
}