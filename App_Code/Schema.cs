using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Mail;
/// <summary>
/// Summary description for Schema
/// </summary>
namespace tiger
{
    public class Schema
    {
        private string strconn;
        public Schema(string strconn)
        {
            this.strconn = strconn;
        }

        public bool sendActivationCode(string login_user)
        {
             bool temp_value = false;

            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prSchemaPending", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            
            try
            {
             
                conn.Open();
                //Add the params
                //                cmd.Parameters.Add("@return", SqlDbType.Int).Value = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@login_user", SqlDbType.VarChar,75).Value = "stevemorisseau@hotmail.com";// login_user;
                cmd.Parameters.Add("@schema_activation_code", SqlDbType.VarChar,75).Direction = ParameterDirection.Output;
                
                //execute the insert
                cmd.ExecuteReader();

                 //temp_value =   Convert.ToString(cmd.Parameters["@schema_activation_code"].Value);
                
                if (Convert.ToInt32(Convert.ToString(cmd.Parameters["@schema_activation_code"]).Length) > 0)
                {
                    string str_to = login_user;
                    string str_from = "info@sinfoca.com";
                    string str_subject = "Access Code--tiger real estate";
                    string str_body = " Copy and paste the activation code to this link http://76.163.235.120/enable_account.aspx : " + Convert.ToString(cmd.Parameters["@schema_activation_code"].Value);
                    
                    SmtpMail.Send(str_from, str_to, str_subject, str_body);

                    temp_value = true;
                }
                else
                {
                    temp_value = false;

                } 
            
            }
            catch (Exception err)
            {
                err.ToString();
            }
            
             return temp_value;
        }
    }
}