﻿using System;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Contact
/// </summary>
namespace tiger
{
    public class Role
    {
        private string str_conn;
        public Role(string str_conn)
        {
            this.str_conn = str_conn;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public DataTable getRoleSearchList(int category, string name)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prRoleSearchList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
               

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }


        public string getAjax_RoleSearchList(int category, string name)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prAjax_RoleSearchList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
               
            string result = "";
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader();
                while (dr.Read() == true)
                {
                    result += Convert.ToString(dr["name"]) + ",";
                }
                return result;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return result;
            }
            finally
            {
                conn.Close();
            }
        }



        public string getAssistant_Ajax_RoleSearchList(int category, string name)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prAssistant_Ajax_RoleSearchList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            string result = "";
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader();
                while (dr.Read() == true)
                {
                    result += Convert.ToString(dr["name"]) + ",";
                }
                return result;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return result;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="category"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public DataTable getRoleNameSearchList(int schema_id, int category,int user_id, string name)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prRoleUserSearchList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
               
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@category", SqlDbType.Int).Value = category;
                cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = user_id;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
