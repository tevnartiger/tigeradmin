﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for ws_GetPreviousMonthExpenses
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class ws_GetPreviousMonthExpenses : System.Web.Services.WebService {

    public ws_GetPreviousMonthExpenses () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string getPreviousMonthExpenses(int home_id, int schema_id, DateTime expense_date_paid)
    {
        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prExpenseMonthView", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd.Parameters.Add("@expense_date_paid", SqlDbType.DateTime).Value = expense_date_paid;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds.GetXml();
        }
        finally
        {
            conn.Close();
        }
    }
}

