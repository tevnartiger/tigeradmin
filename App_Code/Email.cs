using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
/// <summary>
/// Summary description for Email
/// </summary>
public static class Email
{


    public static bool isEmail(string inputEmail)
    {
        // inputEmail = NulltoString(inputEmail);
        string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
              @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
              @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
        Regex re = new Regex(strRegex);
        if (re.IsMatch(inputEmail))
            return (true);
        else
            return (false);

    }

    public static void sendErrorMail(string subject, string body)
    {
        ////
        /* MailMessage m = new MailMessage();
        m.From = "runtime@sinfoca.info";
        m.To = "stevemorisseau@gmail.com";
        //Bcc = "stevemorisseau@gmail.com";
        m.Subject = subject;
        m.BodyFormat = MailFormat.Html;
        m.Body = body;
        SmtpMail.SmtpServer = "relais.videotron.ca";
        SmtpMail.Send(m);  */
        /////////////

        /*  MailMessage m = new MailMessage();

          m.From = new System.Net.Mail.MailAddress("adm@tigerrealestate.com");
          m.To.Add("stevemorisseau@hotmail.com");
          //m.CC = "jocestan@hotmail.com";
          m.Subject = subject;
          //m.BodyFormat =   new System.Net.Mail.MailAddress(MailFormat.Html);
          m.Body = body;
          SmtpMail.SmtpServer = "localhost";
          SmtpMail.Send(m);
*/

        /*            SmtpClient smtpClient = new SmtpClient(); 

       MailMessage message = new MailMessage(); 
       try 
       {

       message.From = new MailAddress(from);
       message.To.Add(to); 
       message.Subject = subject; 
       message.CC.Add("stevemorisseau@hotmail.com"); 
       message.IsBodyHtml = false; 
       message.Body = body;
       smtpClient.Send(message);
       lblStatus.Text = "Email successfully sent."; 
       }
       catch (Exception ex) 
       {
       //lblStatus.Text = "Send Email Failed." + ex.Message; 
       }*/

    }
    //---------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------

    public static void sendSESMailHtml(string from, string to, string subject, string body)
    {

        string style = "";

        style = "<head>";
        style += "<style id='oboutEditorDefaultStyle'  type='text/css'>";
        style += ".boldItalic    { color: #c60; font-weight: bold; font-style:italic;}";
        style += ".alert         { color: #FF0000; font-weight: bold; }";
        style += ".style1        { font-family: verdana,arial,helvetica,sans-serif; font-size: x-small; }";
        style += ".style2        { font-family: verdana,arial,helvetica,sans-serif; color: #003399; }";
        style += ".style3        { font-family: verdana,arial,helvetica,sans-serif; color: #996633; }";
        style += ".style4        { font-family: verdana,arial,helvetica,sans-serif; color: #FF9933; }";
        style += ".sans          { font-family: verdana,arial,helvetica,sans-serif; font-size: small; }";
        style += ".small         { font-family: verdana,arial,helvetica,sans-serif; font-size: x-small; }";
        style += ".h1            { font-family: verdana,arial,helvetica,sans-serif; color: #CC6600; font-size: small; }";
        style += ".h3color       { font-family: verdana,arial,helvetica,sans-serif; color: #CC6600; font-size: x-small; }";
        style += ".tiny          { font-family: verdana,arial,helvetica,sans-serif; font-size: xx-small; }";
        style += ".listprice     { font-family: arial,verdana,helvetica,sans-serif; text-decoration: line-through; }";
        style += ".price         { font-family: arial,verdana,helvetica,sans-serif; color: #990000; }";
        style += ".tinyprice     { font-family: verdana,arial,helvetica,sans-serif; color: #990000; font-size: xx-small; }";
        style += ".highlight     { font-family: verdana,arial,helvetica,sans-serif; color: #990000; font-size: x-small; }";
        style += ".topnav-link   { text-decoration: none; color: #003399; }";
        style += ".topnav-hover  { text-decoration: none; color: #CC6600; }";
        style += ".topnav-active { font-family: verdana,arial,helvetica,sans-serif; font-size: 12px; color: #CC6600; text-decoration: none; }";
        style += ".tabon         { font-size: 10px; color: #FFCC66; font-family: verdana,arial,helvetica,sans-serif; text-decoration: none; text-transform: uppercase; font-weight: bold; line-height: 10px; }";
        style += ".taboff        { font-size: 10px; color: #000000; font-family: verdana,arial,helvetica,sans-serif; text-decoration: none; text-transform: uppercase; font-weight: bold; line-height: 10px; }";
        style += ".amabot        { color: #c60; font-size: .92em; }";



        style += "body       {";
        style += "            color:#404040;background-color: #fff;";
        style += "            border-width: 0px;margin-top: 0px; margin-bottom: 0px;";
        style += "            margin-left: 0px; margin-right: 0px;";
        style += "            padding-top: 0px; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;";
        style += "           }";
        style += "body,table td";
        style += "           {";
        style += "            font-family: verdana,sans-serif;font-size: 10pt;";
        style += "           }";
        style += "h1         {";
        style += "            font-size: 24pt;";
        style += "           }";
        style += "h2         {";
        style += "            font-size: 18pt;";
        style += "           }";
        style += "h3         {";
        style += "            font-size: 14pt;";
        style += "           }";
        style += "h4         {";
        style += "            font-size: 12pt;";
        style += "           }";
        style += "h5         {";
        style += "            font-size: 10pt;";
        style += "           }";
        style += "h6         {";
        style += "            font-size:  8pt;";
        style += "           }";
        style += "</style>";


        style += "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
        style += "</head>";

        char[] delimiterChars = { ' ', ',', ';' };
        string[] mails_to = to.Split(delimiterChars);

        int nb_mail_sent = 0;

        List<string> toSES = new List<string>();
        foreach (string mail in mails_to)
        {

            if (RegEx.IsEmail(mail))
            {
                toSES.Add(mail);
                nb_mail_sent++;
            }
        }



        if (nb_mail_sent > 0)
        {
            try
            {
                string Body = "<html>" + style + "<body>" + body.Replace("&lt;", "<").Replace("&gt;", ">") + "</body></html>";

                //Set up the client with your access credentials
                AmazonSimpleEmailServiceClient client = new AmazonSimpleEmailServiceClient("AKIAILULNEFW5DEJBRJQ", "CrOOnmF6FReCvD0ZxJt4sC5bCoD+vdP2pnkqYEZd");

                Amazon.SimpleEmail.Model.SendEmailRequest req = new Amazon.SimpleEmail.Model.SendEmailRequest()
                    .WithDestination(new Destination() { BccAddresses = toSES })
                    .WithSource(from)
                    .WithReturnPath(from)
                    .WithMessage(
                    new Amazon.SimpleEmail.Model.Message(new Amazon.SimpleEmail.Model.Content(subject),
                    new Body().WithHtml(new Amazon.SimpleEmail.Model.Content(Body))));

                var resp = client.SendEmail(req);
            }
            catch (Exception ex)
            {
                //alternate email or send SMS
            }

        }
    }





    //---------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------

}
