using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;
using System.Text;
using com.fraudlabs.ws;
/// <summary>
/// Summary description for UserCheck
/// </summary>
namespace tiger{
    namespace security{
public static class UserLookup
{


    public static string lookup(string txtIP, string txtLicense)
    {
        /*Ip2LocationWebService.Ip2LocationWebService x_Ip2Location = new Ip2LocationWebServiceClientCSharp.Ip2LocationWebService.Ip2LocationWebService();
        IP2LOCATION oIp2Location = new IP2LOCATION();*/
         Ip2LocationWebService  x_Ip2Location = new Ip2LocationWebService();
        //IP2LOCATION = new  IP2LOCATION();
         string temp = string.Empty;
        try
       
        {
            //create the request URL
            string x_builder;
            //add the host element of the URL
            x_builder = "http://ws.fraudlabs.com/Ip2LocationWebService.asmx/IP2Location?";
            x_builder += "IP=" + txtIP;
            x_builder += "&LICENSE=" + txtLicense;
            // create the web client and obtain the response data
            // as a byte array
            WebClient x_web_client = new WebClient();
            byte[] x_response = x_web_client.DownloadData(x_builder.ToString());
            // process the string result to obtain a validation result
           temp =  processResultString(Encoding.Default.GetString(x_response));
        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message);
            Email.sendErrorMail("FraudLabs--UserLookup", ex.ToString());

        }
        return temp;
    }
    private static string processResultString(String p_result)
    {
        int indexOpen;
        int indexClose;
        string strParam, txtResult;
        strParam = "<COUNTRYCODE>";
        indexOpen = p_result.IndexOf(strParam) + strParam.Length;
        indexClose = p_result.IndexOf("</COUNTRYCODE>");
        txtResult = "Country Code: ";
        if (indexClose != -1)
        {
            txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
        }
        strParam = "<COUNTRYNAME>";
        indexOpen = p_result.IndexOf(strParam) + strParam.Length;
        indexClose = p_result.IndexOf("</COUNTRYNAME>");
        txtResult += "\rCountry Name: ";
        if (indexClose != -1)
        {
            txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
        }
        strParam = "<REGION>";
        indexOpen = p_result.IndexOf(strParam) + strParam.Length;
        indexClose = p_result.IndexOf("</REGION>");
        txtResult += "\rRegion Name: ";
        if (indexClose != -1)
        {
            txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
        }
        strParam = "<CITY>";
        indexOpen = p_result.IndexOf(strParam) + strParam.Length;
        indexClose = p_result.IndexOf("</CITY>");
        txtResult += "\rCity: ";
        if (indexClose != -1)
        {
            txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
        }
        strParam = "<LATITUDE>";
        indexOpen = p_result.IndexOf(strParam) + strParam.Length;
        indexClose = p_result.IndexOf("</LATITUDE>");
        txtResult += "\rLatitude: ";
        if (indexClose != -1)
        {
            txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
        }
        strParam = "<LONGITUDE>";
        indexOpen = p_result.IndexOf(strParam) + strParam.Length;
        indexClose = p_result.IndexOf("</LONGITUDE>");
        txtResult += "\rLongitude: ";
        if (indexClose != -1)
        {
            txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
        }
        strParam = "<ZIPCODE>";
        indexOpen = p_result.IndexOf(strParam) + strParam.Length;
        indexClose = p_result.IndexOf("</ZIPCODE>");
        txtResult += "\rZip Code: ";
        if (indexClose != -1)
        {
            txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
        }
        strParam = "<ISPNAME>";
        indexOpen = p_result.IndexOf(strParam) + strParam.Length;
        indexClose = p_result.IndexOf("</ISPNAME>");
        txtResult += "\rISP Name: ";
        if (indexClose != -1)
        {
            txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
        }
        strParam = "<DOMAINNAME>";
        indexOpen = p_result.IndexOf(strParam) + strParam.Length;
        indexClose = p_result.IndexOf("</DOMAINNAME>");
        txtResult += "\rDomain Name: ";
        if (indexClose != -1)
        {
            txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
        }
        strParam = "<CREDITSAVAILABLE>";
        indexOpen = p_result.IndexOf(strParam) + strParam.Length;
        indexClose = p_result.IndexOf("</CREDITSAVAILABLE>");
        txtResult += "\rCredits Available:";
        if (indexClose != -1)
        {
            txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
        }
        strParam = "<MESSAGE>";
        indexOpen = p_result.IndexOf(strParam) + strParam.Length;
        indexClose = p_result.IndexOf("</MESSAGE>");
        txtResult += "\rMessage: ";
        if (indexClose != -1)
        {
            txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
        }
        return txtResult;
    }





}
    }}