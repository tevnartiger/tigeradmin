using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Rent
/// Done by : Stanley Jocelyn
/// date    : nov 6 , 2007
/// </summary>
/// 
namespace tiger
{
    public class Rent
    {
        private string str_conn;
        public Rent(string str_conn)
        {
            this.str_conn = str_conn;
            SqlConnection conn = new SqlConnection(str_conn);
            //
            // TODO: Add constructor logic here
            //
        }

        public DataTable getRentUntreatedList(int schema_id, int tu_id)
        {

            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prRentUntreatedList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ds = new DataTable();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }

        }

        public DataTable getRentList(int schema_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prRentList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ds = new DataTable();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }

        public DataTable getRentView(int schema_id, int fi_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prRentView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@fi_id", SqlDbType.Int).Value = fi_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ds = new DataTable();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }




        public string getRentCurrentDueDate(int re_id ,DateTime the_date)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prRentCurrentDueDate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@re_id", SqlDbType.Int).Value = re_id;
                cmd.Parameters.Add("@the_date", SqlDbType.DateTime).Value = the_date;
                cmd.Parameters.Add("@due_date", SqlDbType.SmallDateTime).Direction = ParameterDirection.Output;

                //execute the insert
                cmd.ExecuteReader();
                return Convert.ToString(cmd.Parameters["@due_date"].Value);
            }
            finally
            {
                conn.Close();
            }
        }



        public DataTable getRentDelequencyList(int schema_id, int home_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prRentDelequencyList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }


        public DataTable getRentNameDelequencyList(int schema_id, int name_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prRentNameDelequencyList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }


        public DataTable getRentUnitDelequency(int schema_id, int home_id,int unit_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prRentUnitDelequency", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ds = new DataTable();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }



        public decimal getRentDelequencyAmount(int rp_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prRentDelequencyAmount", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@rp_id", SqlDbType.Int).Value = rp_id;
                cmd.Parameters.Add("@delequency_amount", SqlDbType.Decimal).Direction = ParameterDirection.Output;

                //execute the insert
                cmd.ExecuteReader();
                return Convert.ToDecimal(cmd.Parameters["@delequency_amount"].Value);
            }
            finally
            {
                conn.Close();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <param name="unit_id"></param>
        /// <param name="date_from"></param>
        /// <param name="date_to"></param>
        /// <returns></returns>
        public DataTable getLateRentList(int schema_id, int home_id, int unit_id, DateTime date_from, DateTime date_to)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prLateRentList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;
                cmd.Parameters.Add("@date_from", SqlDbType.DateTime).Value = date_from;
                cmd.Parameters.Add("@date_to", SqlDbType.DateTime).Value = date_to;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ds = new DataTable();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <param name="unit_id"></param>
        /// <param name="date_from"></param>
        /// <param name="date_to"></param>
        /// <returns></returns>
        public DataTable getLateRentLowestDueDate(int schema_id, int home_id, int unit_id, DateTime date_from, DateTime date_to)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prLateRentLowestDueDate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;
                cmd.Parameters.Add("@date_from", SqlDbType.DateTime).Value = date_from;
                cmd.Parameters.Add("@date_to", SqlDbType.DateTime).Value = date_to;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }

        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <param name="unit_id"></param>
        /// <param name="date_from"></param>
        /// <param name="date_to"></param>
        /// <returns></returns>
        public DataTable getPaidLateRentFeeList(int schema_id, int home_id, int unit_id, DateTime date_from, DateTime date_to)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prPaidLateRentFeeList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;
                cmd.Parameters.Add("@date_from", SqlDbType.DateTime).Value = date_from;
                cmd.Parameters.Add("@date_to", SqlDbType.DateTime).Value = date_to;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ds = new DataTable();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }


        public DataTable getRentPaymentArchive(int schema_id, int home_id, int unit_id, DateTime date_from, DateTime date_to)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prRentPaymentArchive", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;
                cmd.Parameters.Add("@date_from", SqlDbType.DateTime).Value = date_from;
                cmd.Parameters.Add("@date_to", SqlDbType.DateTime).Value = date_to;
    

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="tu_id"></param>
        /// <returns></returns>
        public DataTable getRentlogTempoView(int schema_id, int tu_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prRentlogTempoView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ds = new DataTable();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }
    }
}

    
