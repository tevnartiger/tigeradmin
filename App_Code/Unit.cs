using System;
using System.Data;
using System.Data.SqlClient;
 
/// <summary>
/// Summary description for Unit
/// </summary>
namespace tiger
{
    public class Unit
    {
        private string str_conn;
        public Unit(string conn)
        {
            this.str_conn = conn;
            //
            // TODO: Add constructor logic here
            //
        }
        public DataSet getUnitList(int schema_id, int home_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prUnitList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public DataTable getUnitResList(int schema_id, int home_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prUnitResList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public DataTable getUnitComList(int schema_id, int home_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prUnitComList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }

        }

        public int getUnitFirstId(int schema_id, int home_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prUnitFirstId", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@unit_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                                
                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return Convert.ToInt32(cmd.Parameters["@unit_id"].Value);
            }
            finally
            {
                conn.Close();
            }

        }



        /// <summary>
        /// first commercial unit of a property
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public int getUnitComFirstId(int schema_id, int home_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prUnitComFirstId", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@unit_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return Convert.ToInt32(cmd.Parameters["@unit_id"].Value);
            }
            finally
            {
                conn.Close();
            }

        }
       /*    public DataSet getUnitView(int schema_id, int unit_id)
                {
                    SqlConnection conn = new SqlConnection(str_conn);
                    SqlCommand cmd = new SqlCommand("prUnitView", conn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    try
                    {
                        conn.Open();
                        //Add the params
                        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                        cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                    finally
                    {
                        conn.Close();
                    }




                }
             */
        public string getUnitView(int schema_id, int unit_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prUnitView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;

                   SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);
                // dg_home.DataSource = 

               string str = "";

               while (dr.Read() == true)
               {
                   str = "<table><tr><td>Bedroom(s)</td><td>" + dr["unit_bedroom_no"] + "</td></tr>";
                   str += "<tr><td>Floor(s)</td><td>" + dr["unit_level"] + "</td></tr>";
                   str += "<tr><td>Maximum person(s)</td><td>" + dr["unit_max_tenant"] + "</td></tr>";
                   // str +="<tr><td>Floor(s)</td><td>"+dr["unit_floor"]+"</td></tr>";
               }
               return str;
               /* SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
*/           
            }
            finally
            {
                conn.Close();
            }




        }
        public string getUnitLink(int home_id,int unit_id)
        {
            string str = null;
            str += "<table><tr><td><a href='unit_view_info.aspx?unit_id=" + unit_id + "&home_id=" + home_id + "'>Info</a></td>";
            str += "<td><a href='unit_view_info.aspx?unit_id="+unit_id+"&home_id="+home_id+"'>Service</a></td>";
            str += "<td><a href='unit_view_info.aspx?unit_id=" + unit_id + "&home_id=" + home_id + "'>Photo</a></td>";
            str += "<td><a href='unit_tenant_link.aspx?unit_id=" + unit_id + "&home_id=" + home_id + "'>Link to Tenant</a></td></table>";
            return str;
        }
        public string getCurrentTenant(int unit_id)
        {
            /*
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prTenantUnitView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                  cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;
                //     SqlDataAdapter da = new SqlDataAdapter(cmd);
               // DataSet ds = new DataSet();
               // da.Fill(ds);
               // return ds;
             
                  string str = "";
                int i= 0;
                  SqlDataReader dr = null;
                  dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
    
                  while (dr.Read() == true)
                  {
                      if (Convert.ToInt32(dr["tu_id"]) != i)
                      {
                          str += "<tr><td><a href=tenant_view_info.aspx?tenant_id=" + Convert.ToString(dr["tenant_id"]) + " class=nameb>" + Convert.ToString(dr["tenant_id"]) + "</a></td><td><a href=tenant_name_info.aspx?tn_id="+Convert.ToString(dr["tn_id"])+" class=sname >" + Convert.ToString(dr["tn_lname"]) + ", " + Convert.ToString(dr["tn_fname"]) + "</a></td></tr>";
                          i = Convert.ToInt32(dr["tu_id"]);
                      }
                      else
                      {

                          str += "<tr><td>&nbsp;</td><td><a href=tenant_name_info.aspx?tn_id=" + Convert.ToString(dr["tn_id"]) + " class=sname >" + Convert.ToString(dr["tn_lname"]) + ", " + Convert.ToString(dr["tn_fname"]) + "</a></td></tr>";
                
                      }
                  }
            
            string str = "allo";
            return str;
            }
            finally
            {
                conn.Close();
            }
*/
            return "allo";
        }
        public int getUnitCount(int schema_id, int home_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prUnitCount", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@tot", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return Convert.ToInt32(cmd.Parameters["@tot"].Value);
            }
            finally
            {
                conn.Close();
            }

        }

        /// <summary>
        /// Number of commercial unit in a property
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <returns></returns>
        public int getUnitComCount(int schema_id, int home_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prUnitComCount", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@tot", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return Convert.ToInt32(cmd.Parameters["@tot"].Value);
            }
            finally
            {
                conn.Close();
            }

        }
         
            public string getTenantUnitName(int schema_id, int tenant_id)
            {
                SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prTenantUnitNameList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = tenant_id;
                //     SqlDataAdapter da = new SqlDataAdapter(cmd);
               // DataSet ds = new DataSet();
               // da.Fill(ds);
               // return ds;
             
                 string str = "<table>";
               //  str += "<tr><td colspan='2' class='nameb'>Current Tenant  </td></tr>";
                int i= 0;
                  SqlDataReader dr = null;
                  dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);
    
                  while (dr.Read() == true)
                  {
                      if (i == 0)
                      {
                          str += "<tr><td ><b>"+ Resources.Resource.txt_lease_date_begin +"</b></td><td>&nbsp;&nbsp;" + dr["tu_date_begin"] + "</td></tr>";
                        
                          if (tiger.Date.isDate(Convert.ToString(dr["tu_date_end"])))
                              str += "<tr><td ><b>" + Resources.Resource.lbl_lease_date_end + "</b></td><td>&nbsp;&nbsp;" + dr["tu_date_end"] + "</br></br></td></tr>";
                          str += "<tr><td colspan='2' class='nameb'><b>" + Resources.Resource.lbl_tenant + "</b></td></tr>";
                      }
                      i++;
                      str += "<tr><td colspan='2'>" + dr["name_fname"] +", "+dr["name_lname"]+ "</td></tr>";
                   }
               
                   str += "<tr ><td><td></tr></table>";
            return str;
            }
            finally
            {
                conn.Close();
            }
            }



            public DateTime getLeaseDateBegin(int schema_id, int tenant_id)
            {
                SqlConnection conn = new SqlConnection(str_conn);
                SqlCommand cmd = new SqlCommand("prTenantUnitNameList", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                DateTime date_begin = new DateTime();

                try
                {
                    conn.Open();
                    //Add the params
                    cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                    cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = tenant_id;
                    SqlDataReader dr = null;
                    dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);


                    while (dr.Read() == true)
                      {
                          date_begin = Convert.ToDateTime(dr["tu_date_begin"]) ;
                      }

                    return date_begin ;
                }
                finally
                {
                    conn.Close();
                }
            }




        public int getCurrentTenantUnitId(int tenant_id, int unit_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prTenantUnitCurrent2", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            

            try
            {
                int tu_id = 0 ;
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;
                cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = tenant_id;
                //     SqlDataAdapter da = new SqlDataAdapter(cmd);
                // DataSet ds = new DataSet();
                // da.Fill(ds);
                //
                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                while (dr.Read() == true)
                {

                    tu_id = Convert.ToInt32(dr["tu_id"]);
                
                }

                
                 return tu_id ;
            }
            finally
            {
                conn.Close();
            }
         
        }

        public int getCurrentTenantId(int schema_id, int unit_id)
        {
             
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prTenantUnitOccupiedReturn", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
               cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                //if unit is currently occupied then open current tenant panel
               /* if (Convert.ToInt32(cmd.Parameters["@tot"].Value)>0)
                {
                    temp_bool = true;
                }
                */
                return Convert.ToInt32(cmd.Parameters["@tenant_id"].Value);
            }
            finally
            {
                conn.Close();
            }

        }






        public string getTenantUnitArchiveName(int schema_id, int tenant_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prTenantUnitArchiveNameList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = tenant_id;
                //     SqlDataAdapter da = new SqlDataAdapter(cmd);
                // DataSet ds = new DataSet();
                // da.Fill(ds);
                // return ds;

                string str = "<table>";
                //  str += "<tr><td colspan='2' class='nameb'>Current Tenant  </td></tr>";
                int i = 0;
                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

                while (dr.Read() == true)
                {
                    if (i == 0)
                    {
                        str += "<tr><td ><b>" + Resources.Resource.txt_lease_date_begin + "</b></td><td>&nbsp;&nbsp;" + dr["tu_date_begin"] + "</td></tr>";

                        if (tiger.Date.isDate(Convert.ToString(dr["tu_date_end"])))
                            str += "<tr><td ><b>" + Resources.Resource.lbl_lease_date_end + "</b></td><td>&nbsp;&nbsp;" + dr["tu_date_end"] + "</br></br></td></tr>";
                        str += "<tr><td colspan='2' class='nameb'><b>" + Resources.Resource.lbl_tenant + "</b></td></tr>";
                    }
                    i++;
                    str += "<tr><td colspan='2'>" + dr["name_fname"] + ", " + dr["name_lname"] + "</td></tr>";
                }

                str += "<tr ><td><td></tr></table>";
                return str;
            }
            finally
            {
                conn.Close();
            }
        }

        //delete 
        public bool removeTempUnit(int temp_name_id, int temp_schema_id, string temp_page_session)
        {
            bool success = false;
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prTempUnitRemove", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@temp_name_id", SqlDbType.Int).Value = temp_name_id;
                cmd.Parameters.Add("@temp_session_id", SqlDbType.Int).Value = temp_schema_id;
                cmd.Parameters.Add("@temp_page_session", SqlDbType.NVarChar, 50).Value = temp_page_session;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 0)
                    success = true;

                return success;    
            
            }
            finally
            {
                conn.Close();
            }

        }

        public void cleanTempUnit(int schema_id,int minutes)
        {

            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prTempUnitRemove", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@minutes", SqlDbType.Int).Value = minutes;
                
            cmd.ExecuteNonQuery();
        }
        public int addUnitProperty(string schema_id,string home_name, string home_addr_no, string home_addr_street, 
            string home_city, string home_desc)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prTempUnitPropertyAdd", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                
                cmd.Parameters.Add("@home_name", SqlDbType.VarChar,50).Value = home_name;
                cmd.Parameters.Add("@home_addr_no", SqlDbType.VarChar, 50).Value = home_addr_no;
                cmd.Parameters.Add("@home_addr_street", SqlDbType.VarChar, 50).Value = home_addr_street;
                cmd.Parameters.Add("@home_city", SqlDbType.VarChar, 50).Value = home_city;
                cmd.Parameters.Add("@home_desc", SqlDbType.VarChar, 100).Value = home_desc;
                 
                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return Convert.ToInt32(cmd.Parameters["@return"].Value);

            }
            finally
            {
                conn.Close();
            }

        
        }
        
        public bool addUnit(int schema_id, int home_id, string unit_door_no,char unit_type,
            int unit_bedroom_no,int unit_bathroom_no,int unit_level, int unit_max_tenant,int name_id, string ip)
        {
           // str_sql = "select * top 10 from ttempunit where name_id = " + name_id + " and schema_id=" + schema_id + " and page_session=" + page_session+""
                 SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prUnitAdd", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params

                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@unit_door_no", SqlDbType.VarChar,50).Value = unit_door_no;
                cmd.Parameters.Add("@unit_type", SqlDbType.Char,1).Value = unit_type;
                cmd.Parameters.Add("@unit_bedroom_no", SqlDbType.Int).Value = unit_bedroom_no;
                cmd.Parameters.Add("@unit_bathroom_no", SqlDbType.Int).Value = unit_bathroom_no;
                cmd.Parameters.Add("@unit_level", SqlDbType.Int).Value = unit_level;
                cmd.Parameters.Add("@unit_max_tenant", SqlDbType.Int).Value = unit_max_tenant;
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = name_id;
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar,15).Value = ip;

                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 0)
                    return true;
                else
                    return false;
       
            }
            finally
            {
                conn.Close();
            }
            








        }


        public DataTable getTempUnitList(int name_id, int schema_id, string page_session)
        {
             
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prTempUnitList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@page_session", SqlDbType.NVarChar, 50).Value = page_session;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
                 
            }
            finally
            {
                conn.Close();
            }

        }

        //get total of temporary unit
        public int getTempUnitCount(int name_id, int schema_id, string page_session)
        {
           
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prTempUnitCount", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@page_session", SqlDbType.NVarChar, 50).Value = page_session;
                 
                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return Convert.ToInt32(cmd.Parameters["@return"].Value);
                   
            }
            finally
            {
                conn.Close();
            }

        }

        
public int getUnitAllowed(int schema_id)
        {
           
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prSchemaUnitAllowed", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                 
                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return Convert.ToInt32(cmd.Parameters["@return"].Value);
                   
            }
            finally
            {
                conn.Close();
            }

        }


        public int getUnitMax(int schema_id)
        {
           
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prSchemaUnitAvailable", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                 
                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return Convert.ToInt32(cmd.Parameters["@return"].Value);
                   
            }
            finally
            {
                conn.Close();
            }

        }


   public void addTempUnit(int name_id, int schema_id, string page_session,
        string unit_door_no, char unit_type,  int unit_bedroom_no, int unit_bathroom_no,
       int unit_level,int unit_max_tenant)
        {
            bool success = false;

            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prTempUnitInsert", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                //cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id ;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id ;
                cmd.Parameters.Add("@page_session", SqlDbType.NVarChar,50).Value = page_session ;
                cmd.Parameters.Add("@unit_door_no", SqlDbType.NVarChar,50).Value = unit_door_no ;
                cmd.Parameters.Add("@unit_type", SqlDbType.NChar,1).Value = unit_type ;
                cmd.Parameters.Add("@unit_bedroom_no", SqlDbType.Int).Value = unit_bedroom_no ;
                cmd.Parameters.Add("@unit_bathroom_no", SqlDbType.Int).Value = unit_bathroom_no  ;
                cmd.Parameters.Add("@unit_level", SqlDbType.Int).Value =  unit_level;
                cmd.Parameters.Add("@unit_max_tenant", SqlDbType.Int).Value = unit_max_tenant ;
              
                       //execute the insert
                cmd.ExecuteNonQuery();
                /*if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 0)
                {
                    success = true;

                    return success;
                }
                else { return false; }*/
            }
            finally
            {
                 
                conn.Close();
            }
        }

        public DataTable getUnitAvailableList(int schema_id, int home_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prUnitAvailableList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable ds = new DataTable();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }

        }

    }
}
