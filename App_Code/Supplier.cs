using System;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Done by : Stanley Jocelyn
/// date    : sept 16 , 2007
/// </summary>
namespace tiger{
public class Supplier
{
    private string str_conn;
	public Supplier(string str_conn)
	{
		this.str_conn = str_conn;
	}
    public DataSet getSupplierList(int schema_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prSupplierList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

             
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }
    }
    public DataSet getSupplierView(int schema_id , int company_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prSupplierView", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
                 cmd.Parameters.Add("@company_id", SqlDbType.Int).Value = company_id;
                 cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }
    }
    public string getSupplierLink(int supplier_id)
    {
        string str = null;
        str += "<table><tr><td><a href='unit_view_info.aspx?tenant_id=" + supplier_id + "'>Info</a></td>";
        str += "<td><a href='unit_view_info.aspx?unit_id=" + supplier_id + "'>Service</a></td>";
        str += "<td><a href='unit_view_info.aspx?unit_id=" + supplier_id + "'>Photo</a></td></table>";
        return str;
    }
    public int getSupplierFirstId(int schema_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prSupplierFirstId", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
      
            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Convert.ToInt32(cmd.Parameters["@tenant_id"].Value);
        }
        finally
        {
            conn.Close();
        }




    }
}
}
