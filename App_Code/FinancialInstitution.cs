using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for FianancialInstitution
/// Done by : Stanley Jocelyn
/// date    : oct 21 , 2007
/// </summary>
/// 
namespace tiger
{
   public class FinancialInstitution
  {
    private string str_conn;
	public FinancialInstitution(string str_conn)
    {
            this.str_conn = str_conn;
            SqlConnection conn = new SqlConnection(str_conn);
		//
		// TODO: Add constructor logic here
		//
	}

    public DataSet getFinancialInstitutionList(int schema_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prFinancialInstitutionList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;


            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }
    }
    
    public DataSet getFinancialInstitutionView(int schema_id, int fi_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prFinancialInstitutionView", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@fi_id", SqlDbType.Int).Value = fi_id;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }
    }

 }
}

    