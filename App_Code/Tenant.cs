using System;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Summary description for Tenant
/// </summary>
namespace tiger{
public class Tenant
{
    private string str_conn;
	public Tenant(string str_conn)
	{
		this.str_conn = str_conn;
	}

    public DataSet getProspectiveTenantList(int schema_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prProspectiveTenantList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        //Add the params
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

        try
        {
            conn.Open();
            //No param

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }
    }
   


    public DataSet getTenantList(int schema_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prTenantList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        //Add the params
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

        try
        {
            conn.Open();
            //No param
             
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }
    }



    public DataTable getTenantIncidentArchives(int schema_id , int name_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prtTenantIncidentArchives", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        //Add the params
        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
        cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

        try
        {
            conn.Open();
            //No param

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        finally
        {
            conn.Close();
        }
    }
    /// <summary>
    /// ????????
    /// </summary>
    /// <param name="schema_id"></param>
    /// <param name="tenant_id"></param>
    /// <param name="categ"></param>
    /// <returns></returns>
    public DataSet getTenantNameList(int schema_id, int tenant_id, string categ)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prTenantNameList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = tenant_id;
            cmd.Parameters.Add("@categ", SqlDbType.VarChar, 50).Value = categ;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }
    }


    /// <summary>
    /// "The good one"
    /// </summary>
    /// <param name="schema_id"></param>
    /// <param name="tenant_id"></param>
    /// <returns></returns>
    public DataSet getTenantNameList(int schema_id, int tenant_id )
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prTenantNameList2", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = tenant_id;
           // cmd.Parameters.Add("@categ", SqlDbType.VarChar, 50).Value = categ;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }
    }
    
    public DataSet getTenantNameList()
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prTenantNameList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //No param

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }
    }
 
    public DataSet getTenantView(int tenant_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prTenantView", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
                 cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = tenant_id;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }
    }
    public string getTenantLink(int tenant_id)
    {
        string str = null;
        str += "<table><tr><td><a href='unit_view_info.aspx?tenant_id=" + tenant_id + "'>Info</a></td>";
        str += "<td><a href='unit_view_info.aspx?unit_id=" + tenant_id + "'>Service</a></td>";
        str += "<td><a href='unit_view_info.aspx?unit_id=" + tenant_id + "'>Photo</a></td></table>";
        return str;
    }
    public int addTenant(int schema_id,string tenant_name, string tenant_desc)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prTenantAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tenant_name", SqlDbType.VarChar,50).Value = tenant_name;
            cmd.Parameters.Add("@tenant_desc", SqlDbType.VarChar, 500).Value = tenant_desc;
            
            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Convert.ToInt32(cmd.Parameters["@tenant_id"].Value);
        }
        finally
        {
            conn.Close();
        }
    }
    public int addTenantName(int schema_id, int tenant_id,int tn_main_tenant,
        string tn_lname,string tn_fname,string tn_tel,string tn_cell,string tn_email,
         string tn_com)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prTenantNameAdd", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = tenant_id;
            cmd.Parameters.Add("@tn_main_tenant", SqlDbType.Int).Value = tn_main_tenant;
            cmd.Parameters.Add("@tn_lname", SqlDbType.VarChar, 50).Value = tn_lname;
            cmd.Parameters.Add("@tn_fname", SqlDbType.VarChar, 50).Value = tn_fname;
            cmd.Parameters.Add("@tn_tel", SqlDbType.VarChar, 50).Value = tn_tel;
            cmd.Parameters.Add("@tn_cell", SqlDbType.VarChar, 50).Value = tn_cell;
            cmd.Parameters.Add("@tn_email", SqlDbType.VarChar, 50).Value = tn_email;
            cmd.Parameters.Add("@tn_com", SqlDbType.VarChar, 50).Value = tn_com;

            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);

            return Convert.ToInt32(cmd.Parameters["@return"].Value);
                
        }
        finally
        {
            conn.Close();
        }
    }

    public int getTenantFirstId(int schema_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prTenantFirstId", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
      
            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return Convert.ToInt32(cmd.Parameters["@tenant_id"].Value);
        }
        finally
        {
            conn.Close();
        }




    }




    public DataSet getTenantUnitTempoDateView(int schema_id,int tu_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prTenantUnitDateView", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tu_id", SqlDbType.Int).Value = tu_id;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }
    }
    /// <summary>
    /// View the temporary list of tenants when creating the old rent log ( archives of previous rents )
    /// </summary>
    /// <param name="schema_id"></param>
    /// <param name="tu_id"></param>
    /// <returns></returns>
    public DataSet getTenantTempoList(int schema_id, int tenant_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prTenantTempoList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Value = tenant_id;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }
    }

    public DataTable getTenantCurrentSearchList(int schema_id, int home_id , string name)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prTenantCurrentSearchList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd.Parameters.Add("@name", SqlDbType.VarChar,50).Value = name;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        finally
        {
            conn.Close();
        }
    }


    public DataSet getTenantInactiveSearchList(int schema_id, int home_id, string name)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prTenantInactiveSearchList", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
            cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }
        finally
        {
            conn.Close();
        }
    }



    public DataTable getTenantPaymentArchive(int schema_id, int name_id)
    {
        SqlConnection conn = new SqlConnection(str_conn);
        SqlCommand cmd = new SqlCommand("prTenantPaymentArchive", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        try
        {
            conn.Open();
            //Add the params
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@name_id", SqlDbType.Int).Value = name_id;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }
        finally
        {
            conn.Close();
        }
    }

   
}
}
