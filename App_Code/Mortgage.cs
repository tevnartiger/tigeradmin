using System;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Done by : Stanley Jocelyn
/// date    : nov 1 , 2007
/// 
/// </summary>
namespace tiger
{
    public class Mortgage
    {
        private string str_conn;
        public Mortgage(string str_conn)
        {
            this.str_conn = str_conn;
        }
        public DataSet getMortgageList(int schema_id, int home_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prMortgageList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }

        public DataTable getMortgageArchiveList(int schema_id , int home_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prMortgageArchiveList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }
        /// <summary>
        /// Mortgage archive of a property
        /// </summary>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public DataTable getMortgageHomeArchiveList(int schema_id, int home_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prMortgageHomeArchiveList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public DataTable getMortgagePendingList(int schema_id , int home_id , int time_frame)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prMortgagePendingList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@time_frame", SqlDbType.Int).Value = time_frame;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }

        public DataSet getMortgageView(int schema_id, int mortgage_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prMortgageView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@mortgage_id", SqlDbType.Int).Value = mortgage_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }



        /// <summary>
        /// List of expiring mortgage ( 0-30 days , 31-60 days , 61-90 days
        /// </summary>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public DataTable getMortgageExpirationList(int schema_id, int home_id, int time_frame)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prMortgageExpirationList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@time_frame", SqlDbType.Int).Value = time_frame;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }
        
    }
}
