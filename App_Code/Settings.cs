﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using sinfoca.login;
using Twilio;

/// <summary>
/// Summary description for Settings
/// </summary>
/// 
namespace sinfoca.tiger
{
    public class Settings
    {

        int setting_how_confirm;
        int setting_notice_reschedule;
        int setting_notice_cancel;
        int setting_notice_confirm;
        int setting_days_reschedule;
        int setting_days_cancel;
        int setting_timezone_gmt;
        int setting_confirmed_client_appointment_count;
        private int timezone_id;
        private string schema_company_name = String.Empty;
        private string schema_description = String.Empty;
      
        public int Settings_how_confirm
        {
            get { return setting_how_confirm; }
            set { setting_how_confirm = value; }
        }

        public int Settings_notice_reschedule
        {
            get { return setting_notice_reschedule; }
            set { setting_notice_reschedule = value; }
        }

        public int Settings_notice_cancel
        {
            get { return setting_notice_cancel; }
            set { setting_notice_cancel = value; }
        }

        public int Settings_timezone_gmt
        {
            get { return setting_timezone_gmt; }
            set { setting_timezone_gmt = value; }
        }

        public int Settings_notice_confirm
        {
            get { return setting_notice_confirm; }
            set { setting_notice_confirm = value; }
        }

        public int Settings_days_reschedule
        {
            get { return setting_days_reschedule; }
            set { setting_days_reschedule = value; }
        }

        public int Settings_days_cancel
        {
            get { return setting_days_cancel; }
            set { setting_days_cancel = value; }
        }

       public int Setting_confirmed_client_appointment_count
        {
            get { return setting_confirmed_client_appointment_count; }
            set { setting_confirmed_client_appointment_count = value; }
        }


       public int TimeZone_id
       {
           get { return timezone_id; }
           set { timezone_id = value; }
       }


       public string Schema_company_name
       {
           get { return schema_company_name; }
           set { schema_company_name = RegEx.getText(Utils.sanitizeString(value, 100)); }
       }



       public string Schema_description
       {
           get { return schema_description; }
           set { schema_description = RegEx.getText(Utils.sanitizeString(value, 400)); }
       }

        public Settings()
        { }

       
        public bool SettingsUpdate()
        {

            bool result = false;
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prSettingsUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            //Return value 
            cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            //Add the input params

            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
            cmd.Parameters.Add("@setting_how_confirm", SqlDbType.Int).Value = setting_how_confirm;
            cmd.Parameters.Add("@setting_notice_reschedule", SqlDbType.Int).Value = setting_notice_reschedule;
            cmd.Parameters.Add("@setting_notice_cancel", SqlDbType.Int).Value = setting_notice_cancel;
            cmd.Parameters.Add("@setting_notice_confirm", SqlDbType.Int).Value = setting_notice_confirm;
            cmd.Parameters.Add("@setting_days_reschedule", SqlDbType.Int).Value = setting_days_reschedule;
            cmd.Parameters.Add("@setting_days_cancel", SqlDbType.Int).Value = setting_days_cancel;
           
            cmd.Parameters.Add("@trace_user_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["user_id"].ToString());
            cmd.Parameters.Add("@trace_user_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();


            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            //Failure or Success (true = success (1), else fail) 
            if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
            {
                result = true;

            }
            return result;

        }


        public DataTable getTimeZoneList()
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prTimeZoneList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();

            try
            {
                conn.Open();
                //Add the params
             
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }

            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }



        public string[] getSettingsLangArray()
        {
            string str = System.Web.HttpContext.Current.Session["schema_lang"].ToString() ;
            string[] str_result = new string[60];
            //describe which character is separating field
            char[] spliter = { '|' };
            str_result = str.Split(spliter);

            return str_result;
        }


        public string[] getSettingsDDLArray(string ddl_value)
        {
            string str = ddl_value;
            string[] str_result = new string[60];
            //describe which character is separating field
            char[] spliter = { ':' };
            str_result = str.Split(spliter);

            return str_result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool setCompanyNameUpdate()
        {
            bool result = false;

            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prCompanyNameUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@schema_company_name", SqlDbType.NVarChar, 100).Value = schema_company_name;
                cmd.Parameters.Add("@trace_user_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["user_id"].ToString());
                cmd.Parameters.Add("@trace_user_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();

                //execute the insert

                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                //Failure or Success (true = success (0), else fail) 
                if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 0)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }

            return result;

        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool setSchemaLangUpdate(string schema_lang)
        {
            bool result = false;

            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prSchemaLangUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@schema_lang", SqlDbType.VarChar, 50).Value = schema_lang;
                cmd.Parameters.Add("@trace_user_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["user_id"].ToString());
                cmd.Parameters.Add("@trace_user_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();

                //execute the insert

                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                //Failure or Success (true = success (0), else fail) 
                if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 0)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }

            return result;

        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool setSchemaDescriptionUpdate(string lang)
        {
            bool result = false;

            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prSchemaDescriptionUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@schema_description", SqlDbType.NVarChar, 400).Value = schema_description;
                cmd.Parameters.Add("@lang", SqlDbType.VarChar, 10).Value = lang;
                cmd.Parameters.Add("@trace_user_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["user_id"].ToString());
                cmd.Parameters.Add("@trace_user_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();

                //execute the insert

                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                //Failure or Success (true = success (0), else fail) 
                if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 0)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }

            return result;

        }




        public bool setTimezoneUpdate()
        {
            bool result = false;

            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prTimezoneUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@timezones_id", SqlDbType.Int).Value = timezone_id;
                cmd.Parameters.Add("@trace_user_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["user_id"].ToString());
                cmd.Parameters.Add("@trace_user_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();

                //execute the insert

                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                //Failure or Success (true = success (0), else fail) 
                if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 0)
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }

            return result;

        }

     
        public DataTable getInitList(string schema_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prCMSInitList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable("init");

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(schema_id);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);


                return dt;
            }

            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }


        public DataTable getInitView(string schema_id, string init_code, DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
            {
                DataTable d = new DataTable();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (init_code == dt.Rows[i]["init_code"].ToString())
                    {
                        d.Columns.Add("init_id", typeof(int));
                        d.Columns.Add("initcateg_id", typeof(int));
                        d.Columns.Add("schema_id", typeof(int));
                        d.Columns.Add("init_enabled", typeof(bool));
                        d.Columns.Add("init_code", typeof(string));
                        d.Columns.Add("init_name", typeof(string));
                        d.Columns.Add("init_value", typeof(string));
                        d.Rows.Add(Convert.ToInt32(dt.Rows[i]["init_"]),
                        Convert.ToInt32(dt.Rows[i]["init_"]),
                        Convert.ToInt32(dt.Rows[i]["initcateg_id"]),
                        Convert.ToInt32(dt.Rows[i]["schema_id"]),
                        Convert.ToBoolean(dt.Rows[i]["init_enabled"]),
                        Convert.ToString(dt.Rows[i]["init_code"]),
                        Convert.ToString(dt.Rows[i]["init_name"]),
                        Convert.ToString(dt.Rows[i]["init_value"]));

                    }
                }
                return d;
            }
            else
                return null;
        }



        public string getSettingsLangText(string lang)
        {
            string language ;

            switch (lang)
            {
                case "en": language = Resources.Resource.setup_lang_en;
                    break;
                case "es": language = Resources.Resource.setup_lang_es;
                    break;
                case "fr": language = Resources.Resource.setup_lang_fr;
                    break;

                default: language = Resources.Resource.setup_lang_en;
                    break;
            }

            return language;
        }



        public DataTable getSettingsOtherLang(string lang)
        {
            DataTable otherLang = new DataTable();

            //– Add columns to the data table
            otherLang.Columns.Add("lang", typeof(string));
            otherLang.Columns.Add("lang_name", typeof(string));

            if(lang!="en")
                otherLang.Rows.Add("en", Resources.Resource.setup_lang_en);

            if (lang != "es")
                otherLang.Rows.Add("es", Resources.Resource.setup_lang_es);

            if (lang != "fr")
                otherLang.Rows.Add("fr", Resources.Resource.setup_lang_fr);


            return otherLang;
        }



        public void getSettingsInfo()
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prSettingsInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();

            try
            {
                //return params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                //Add the input params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@setting_how_confirm", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@setting_notice_reschedule", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@setting_notice_cancel", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@setting_notice_confirm", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@setting_days_reschedule", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@setting_days_cancel", SqlDbType.Int).Direction = ParameterDirection.Output;
               // cmd.Parameters.Add("@setting_confirmed_client_appointment_count", SqlDbType.Int).Direction = ParameterDirection.Output;
                //execute the insert
                cmd.ExecuteReader();

                if (Convert.ToInt32(cmd.Parameters["@return"].Value) == 1)
                {
                    //Return the values of the output params
                    Settings_how_confirm = Convert.ToInt16(cmd.Parameters["@setting_how_confirm"].Value);
                    Settings_notice_reschedule = Convert.ToInt16(cmd.Parameters["@setting_notice_reschedule"].Value);
                    Settings_notice_cancel = Convert.ToInt16(cmd.Parameters["@setting_notice_cancel"].Value);
                    Settings_notice_confirm = Convert.ToInt16(cmd.Parameters["@setting_notice_confirm"].Value);
                    Settings_days_reschedule = Convert.ToInt16(cmd.Parameters["@setting_days_reschedule"].Value);
                    Settings_days_cancel = Convert.ToInt16(cmd.Parameters["@setting_days_cancel"].Value);
                  //  Setting_confirmed_client_appointment_count = Convert.ToInt16(cmd.Parameters["@setting_confirmed_client_appointment_count"].Value);
                    
                }
            }

             catch (Exception ex)
            {
             sinfoca.error.ErrorLog.addErrorLog(ex);
            }

            finally
            {
                conn.Close();
            }

        }

    }
}