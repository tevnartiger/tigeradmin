using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.IO;
 

/// <summary>
/// Summary description for Email
/// </summary>
namespace sinfoca
{
    public static class googlegraph 
    {
 
    [WebMethod]
   public static string pieChart(string id, string title, DataTable dt, string width, string height)
   {
       string str = string.Empty;
     
       str = "";
       str += " <script type=\"text/javascript\">\n";
       str += "google.load(\"visualization\", \"1\", { packages: [\"corechart\"] });\n";
       str += "google.setOnLoadCallback(drawChart);\n";
       str += "function drawChart() {\n";
       str += "var data = google.visualization.arrayToDataTable([['name', 'value']";
       
       for (int i = 0; i < dt.Rows.Count; i++)
       {
           str +=",['"+dt.Rows[i][0].ToString()+"',"+dt.Rows[i][1].ToString()+"]";
       }
       str += "]);\n";

       str += "var options = { width:\"" + width + "\", height: \"" + height + "\", title: '" + title + "' };\n";
       str += "var chart = new google.visualization.PieChart(document.getElementById('" + id + "'));\n";
       str += "chart.draw(data, options);\n";

       str += " }\n";
       str += "</script>\n";

       dt = null;
       return str;
   }

    [WebMethod]
    public static string geoChart(string id, string title, DataTable dt, string width, string height)
    {
        string str = string.Empty;

        str = "";
        str += " <script type=\"text/javascript\">\n";
        str += "google.load(\"visualization\", \"1\", { packages: [\"geochart\"] });\n";
        str += "google.setOnLoadCallback(drawRegionsMap);\n";
        str += "function drawRegionsMap() {\n";
        str += "var data = google.visualization.arrayToDataTable([['name', 'value']";

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            str += ",['" + dt.Rows[i][0].ToString() + "'," + dt.Rows[i][1].ToString() + "]";
        }
        str += "]);\n";

        str += "var options = { width:\"" + width + "\", height: \"" + height + "\", title: '" + title + "' };\n";
        str += "var chart = new google.visualization.GeoChart(document.getElementById('" + id + "'));\n";
        str += "chart.draw(data, options);\n";

        str += " }\n";
        str += "</script>\n";

        dt = null;
        return str;
    }

    [WebMethod]
    public static string lineChart(string id, string title, string x_title, string val1, string val2, DataTable dt, string width, string height)
    {
        string str = string.Empty;

        str = "";
        str += " <script type=\"text/javascript\">\n";
        str += "google.load(\"visualization\", \"1\", { packages: [\"corechart\"] });\n";
        str += "google.setOnLoadCallback(drawChart);\n";
        str += "function drawChart() {\n";
        str += "var data = google.visualization.arrayToDataTable([['name', '" + val1 + "','" + val2 + "']";

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            str += ",['" + dt.Rows[i][0].ToString() + "'," + dt.Rows[i][1].ToString() + "," + dt.Rows[i][2].ToString() + "]";
        }
        str += "]);\n";

        str += "var options = {";
        str += "width:\"" + width + "\", height: \"" + height + "\",title:'" + title + "',";
        str += "hAxis:{title:'" + x_title + "', titleTextStyle:{color:'red'}}";

        str += "};\n";
        str += "var chart = new google.visualization.LineChart(document.getElementById('" + id + "'));\n";
        str += "chart.draw(data, options);\n";

        str += " }\n";
        str += "</script>\n";

        dt = null;
        return str;
    }

  [WebMethod]
    public static string columnChart(string id, string title, string x_title, string val1, string val2, DataTable dt, string width, string height)
    {
        string str = string.Empty;

        str = "";
        str += " <script type=\"text/javascript\">\n";
        str += "google.load(\"visualization\", \"1\", { packages: [\"corechart\"] });\n";
        str += "google.setOnLoadCallback(drawChart);\n";
        str += "function drawChart() {\n";
        str += "var data = google.visualization.arrayToDataTable([['name', '" + val1 + "','" + val2 + "']";

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            str += ",['" + dt.Rows[i][1].ToString() + "'," + dt.Rows[i][2].ToString() + "," + dt.Rows[i][3].ToString() + "]";
        }
        str += "]);\n";

        str += "var options = {";
        str += "width:\"" + width + "\", height: \"" + height + "\",title:'" + title + "',";
        str += "hAxis:{title:'" + x_title + "', titleTextStyle:{color:'red'}}";

        str += "};\n";
        str += "var chart = new google.visualization.ColumnChart(document.getElementById('" + id + "'));\n";
        str += "chart.draw(data, options);\n";

        str += " }\n";
        str += "</script>\n";

        dt = null;
        return str;
    }

        [WebMethod]
        public static string columnChart1(string id, string title, string x_title, string val1, string val2, DataTable dt, string width, string height)
        {
            string str = string.Empty;

            str = "";
            str += " <script type=\"text/javascript\">\n";
            str += "google.load(\"visualization\", \"1\", { packages: [\"corechart\"] });\n";
            str += "google.setOnLoadCallback(drawChart);\n";
            str += "function drawChart() {\n";
            str += "var data = google.visualization.arrayToDataTable([['name', '" + val1 + "']";

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                str += ",['" + dt.Rows[i][1].ToString() + "'," + dt.Rows[i][3].ToString()  + "]";
            }
            str += "]);\n";

            str += "var options = {";
            str += "width:\"" + width + "\", height: \"" + height + "\",title:'" + title + "',";
            str += "hAxis:{title:'" + x_title + "', titleTextStyle:{color:'red'}}";

            str += "};\n";
            str += "var chart = new google.visualization.ColumnChart(document.getElementById('" + id + "'));\n";
            str += "chart.draw(data, options);\n";

            str += " }\n";
            str += "</script>\n";

            dt = null;
            return str;
        }

        [WebMethod]
    public static string areaChart(string id, string title,string x_title,string val1, string val2,  DataTable dt, string width, string height)
    {
        string str = string.Empty;

        str = "";
        str += " <script type=\"text/javascript\">\n";
        str += "google.load(\"visualization\", \"1\", { packages: [\"corechart\"] });\n";
        str += "google.setOnLoadCallback(drawChart);\n";
        str += "function drawChart() {\n";
        str += "var data = google.visualization.arrayToDataTable([['name', '"+val1+"','"+val2+"']";

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            str += ",['" + dt.Rows[i][0].ToString() + "'," + dt.Rows[i][1].ToString() + ","+ dt.Rows[i][2].ToString() +"]";
        }
        str += "]);\n";

        str += "var options = {";
        str += "width:\"" + width + "\", height: \"" + height + "\",title:'"+title+"',";
        str += "hAxis:{title:'"+x_title+"', titleTextStyle:{color:'red'}}";

        str += "};\n";
        str += "var chart = new google.visualization.AreaChart(document.getElementById('" + id + "'));\n";
        str += "chart.draw(data, options);\n";

        str += " }\n";
        str += "</script>\n";

        dt = null;
        return str;
    }

    [WebMethod]
    public static string barChart(string id, string title, string x_title, string val1, string val2, DataTable dt, string width, string height)
    {
        string str = string.Empty;

        str = "";
        str += " <script type=\"text/javascript\">\n";
        str += "google.load(\"visualization\", \"1\", { packages: [\"corechart\"] });\n";
        str += "google.setOnLoadCallback(drawChart);\n";
        str += "function drawChart() {\n";
        str += "var data = google.visualization.arrayToDataTable([['name', '" + val1 + "','" + val2 + "']";

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            str += ",['" + dt.Rows[i][0].ToString() + "'," + dt.Rows[i][1].ToString() + "," + dt.Rows[i][2].ToString() + "]";
        }
        str += "]);\n";

        str += "var options = {";
        str += "width:\"" + width + "\", height: \"" + height + "\",title:'" + title + "',";
        str += "vAxis:{title:'" + x_title + "', titleTextStyle:{color:'red'}}";

        str += "};\n";
        str += "var chart = new google.visualization.BarChart(document.getElementById('" + id + "'));\n";
        str += "chart.draw(data, options);\n";

        str += " }\n";
        str += "</script>\n";

        dt = null;
        return str;
    }


   
/*
    [WebMethod]
    public static string tableChart(string title, DataTable dt)
    {
        string str = string.Empty;
        int x1 = 0;
        int x2 = 0;
        int x3 = 1;

        str = "";

        str += "<script type='text/javascript'>\n";
        str += "google.load('visualization', '1', {packages:['table']});\n";
        str += "google.setOnLoadCallback(drawTable);\n";
        str += "function drawTable() {\n";
        str += "var data = new google.visualization.DataTable();\n";
        str += "data.addColumn('string', 'Name');\n";
        str += "data.addColumn('number', 'Salary');\n";
        str += "data.addRows(" + dt.Rows.Count + ");\n";

        //json.InnerHtml = GetJSONString(dt);
        for (int i = 0; i < dt.Rows.Count; i++)
        {

            str += "data.setCell(" + x1 + ",0 ,'" + dt.Rows[i][0].ToString() + "');\n";
            str += "data.setCell(" + x1 + "," + x3 + "," + dt.Rows[i][1].ToString() + ",'" + String.Format("${0:C}", dt.Rows[i][1].ToString()) + "');\n";
            ++x1;
            x2++;
        }

        str += "var table = new google.visualization.Table(document.getElementById('chart_div'));\n";
        str += "table.draw(data, {showRowNumber: true});\n";

        str += "}\n";
        str += "</script>";

        dt = null;

        return str;

    }
    [WebMethod]
    public static string RevenueChart(string title, DataTable dt)
    {
        string str = string.Empty;
        int x1 = 0;
        int x2 = 0;
        int x3 = 1;

        str = "";

        str += " <script type=\"text/javascript\">";
        str += "google.load(\"visualization\", \"1\");";
        str += "google.setOnLoadCallback(drawChart);";
        str += "function drawChart() {";
        str += "var data = new google.visualization.DataTable();";
        str += "data.addColumn('string', 'name');";
        str += "data.addColumn('number', 'value');";
        str += " data.addRows(" + dt.Rows.Count + ");";

        //json.InnerHtml = GetJSONString(dt);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            str += "data.setCell(" + x1 + ",0 ,'" + dt.Rows[i][0].ToString() + "');\n";
            ++x1;
            x2++;
        }

        x1 = 0;
        x2 = 0;

        for (int x = 0; x < dt.Rows.Count; x++)
        {

            str += "data.setValue(" + x1 + "," + x2 + "," + dt.Rows[x][1].ToString() + "," + String.Format("{0:C}", dt.Rows[x][1].ToString()) + ");\n\n";
            ++x1;
            x2++;
        }




        str += "var chartDiv = document.getElementById('chartdiv');";
        str += "var options = {title: '" + title + "'};";
        str += "chart.draw(data, options);";
        str += "//google.visualization.events.addListener(chart, 'select', handleSelect);";
        str += "}";
        str += "</script>";

        dt = null;

        return str;

    }
    [WebMethod]
    public static string scatterChart(string title, DataTable dt)
    {
        string str = string.Empty;
        int x1 = 0;
        int x2 = 0;
        int x3 = 1;

        str = "";

        str += " <script type=\"text/javascript\">\n";
        str += "google.load(\"visualization\", \"1\", { packages: [\"corechart\"] });\n";
        str += "google.setOnLoadCallback(drawChart);\n";
        str += "function drawChart() {\n";
        str += "var data = new google.visualization.DataTable();\n";
        str += "data.addColumn('number', 'value1');\n";
        str += "data.addColumn('number', 'value2');\n";
        str += " data.addRows(" + dt.Rows.Count + ");\n";

        //json.InnerHtml = GetJSONString(dt);
        for (int i = 0; i < dt.Rows.Count; i++)
        {

            str += "data.setValue(" + x1 + ",0 ," + dt.Rows[i][0].ToString() + ");\n";
            str += "data.setValue(" + x1 + "," + x3 + "," + dt.Rows[i][1].ToString() + ");\n\n";
            ++x1;
            x2++;
        }


        str += "var chart = new google.visualization.ScatterChart(document.getElementById('chart_div')); ";
        str += "chart.draw(data, {width: 400, height: 240,";
        str += "title: 'Age vs. Weight comparison', ";
        str += "hAxis: {title: 'Age', minValue: 0, maxValue: 15}, ";
        str += "vAxis: {title: 'Weight', minValue: 0, maxValue: 15}, ";
        str += "legend: 'none'";
        str += "}); ";



        str += "   }";
        str += "</script>";

        dt = null;

        return str;
    }




    [WebMethod]
    public static string gaugeChart(string title, DataTable dt)
    {
        string str = string.Empty;
        int x1 = 0;
        int x2 = 0;
        int x3 = 1;

        str = "";

        str += " <script type=\"text/javascript\">\n";
        str += "google.load(\"visualization\", \"1\", { packages: [\"gauge\"] });\n";
        str += "google.setOnLoadCallback(drawChart);\n";
        str += "function drawChart() {\n";
        str += "var data = new google.visualization.DataTable();\n";
        str += "data.addColumn('string', 'label');\n";
        str += "data.addColumn('number', 'Value');\n";
        str += " data.addRows(" + dt.Rows.Count + ");\n";

        //json.InnerHtml = GetJSONString(dt);
        for (int i = 0; i < dt.Rows.Count; i++)
        {

            str += "data.setValue(" + x1 + ",0 ,'" + dt.Rows[i][0].ToString() + "');\n";
            str += "data.setValue(" + x1 + "," + x3 + "," + dt.Rows[i][1].ToString() + ");\n\n";
            ++x1;
            x2++;
        }


        str += "var chart = new google.visualization.Gauge(document.getElementById('chart_div'));\n";
        str += "var options = {width: 800, height: 240, redFrom: 90, redTo: 100, yellowFrom:75, yellowTo: 90, minorTicks: 5};\n";
        str += "chart.draw(data, options);\n";

        str += "   }\n";
        str += "</script>\n";

        dt = null;

        return str;

    }

*/

       
    }
}