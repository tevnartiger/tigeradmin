using System;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Done by : Stanley Jocelyn
/// date    : oct 10 , 2007
/// </summary>
namespace tiger
{
    public class Group
    {
        private string str_conn;
        public Group(string str_conn)
        {
            this.str_conn = str_conn;
        }

        /// <summary>
        /// list of all groups
        /// </summary>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public DataSet getGroupHomeList(int schema_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupHomeList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }


        public int getGroupHomeCount(int schema_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupHomeCount", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@tot", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return Convert.ToInt32(cmd.Parameters["@tot"].Value);
            }
            finally
            {
                conn.Close();
            }

        }



        public int addGroup(string temp_group_name,int temp_schema_id, int temp_name_id, string temp_name_id_ip)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupAdd", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@group_name", SqlDbType.VarChar, 15).Value = temp_group_name;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = temp_schema_id;
                cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = temp_name_id;
                cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = temp_name_id_ip;

                cmd.ExecuteReader(CommandBehavior.CloseConnection);

                return Convert.ToInt32(cmd.Parameters["@return"].Value);
            }
            finally
            {
                conn.Close();
            }


        }

        public int addGroupHome(int temp_schema_id, int temp_group_id,int temp_home_id, int temp_name_id, string temp_name_id_ip)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupHomeAdd", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
               
                conn.Open();
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = temp_schema_id;
               cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = temp_group_id;
               cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = temp_home_id;

               cmd.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = temp_name_id;
               cmd.Parameters.Add("@trace_name_id_ip", SqlDbType.VarChar, 15).Value = temp_name_id_ip;

                cmd.ExecuteReader(CommandBehavior.CloseConnection);

                return Convert.ToInt32(cmd.Parameters["@return"].Value);
            }
            finally
            {
                conn.Close();
            }
        }


        public bool removeGroup(int temp_schema_id, int temp_group_id)
        {

            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupRemove", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {

                conn.Open();
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = temp_schema_id;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = temp_group_id;
               
                cmd.ExecuteReader(CommandBehavior.CloseConnection);

                if(Convert.ToInt32(cmd.Parameters["@return"].Value)==0)
                {return true;}
                else
                { return false;}
            }
            finally
            {
                conn.Close();
            }
        }
        public DataTable viewGroupHome(int temp_schema_id, int temp_group_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupHomeView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {

                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = temp_schema_id;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = temp_group_id;

                cmd.ExecuteReader(CommandBehavior.CloseConnection);

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;

            }
            finally
            {
                conn.Close();
            }


        }

        public bool groupExist(int temp_schema_id,string temp_group_name)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupExist", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = temp_schema_id;
                cmd.Parameters.Add("@group_name", SqlDbType.VarChar, 15).Value = temp_group_name;
            
                cmd.ExecuteReader(CommandBehavior.CloseConnection);

               if(Convert.ToInt32(cmd.Parameters["@return"].Value)== 1)
                return true;
                else
                return false;
            }
            finally
            {
                conn.Close();
            }


        }
        public int groupFindFirst(int temp_schema_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupFindFirst", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
               // cmd.Parameters.Add("@group_id", SqlDbTypeInt).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = temp_schema_id;
                
                cmd.ExecuteReader(CommandBehavior.CloseConnection);

                return Convert.ToInt32(cmd.Parameters["@return"].Value);
            }
            finally
            {
                conn.Close();
            }

        }
        public string retrieveGroupName(int temp_schema_id, int temp_group_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupRetrieveName", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@group_name", SqlDbType.VarChar, 15).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = temp_schema_id;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = temp_group_id;

                cmd.ExecuteReader(CommandBehavior.CloseConnection);

            return Convert.ToString(cmd.Parameters["@group_name"].Value)  ;
               
            }
            finally
            {
                conn.Close();
            }


        }

        public DataTable getGroupProfileSingleHomeList(int temp_schema_id, int temp_group_id )
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupProfileSingleHomeList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = temp_schema_id;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = temp_group_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }

        public DataTable getGroupProfileHomeList(int temp_schema_id, int temp_group_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupProfileHomeList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = temp_schema_id;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = temp_group_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }




        public string getGroupProfileHomeView(int temp_schema_id, int temp_group_id)
        {
            string str_view = "<table border='1'><tr><td>Property</td><td>Owner</td><td>Property Manager</td><td>Janitor</td></tr>";
            SqlConnection conn = new SqlConnection(str_conn);
            //SqlCommand cmd = new SqlCommand("prHomeGrouplist", conn);
            SqlCommand cmd = new SqlCommand("select h.home_id, h.home_name from  thome h inner join thomegroup hg on hg.home_id = h.home_id where h.schema_id ="+temp_schema_id+"  and hg.group_id ="+ temp_group_id,conn);
            
            
            cmd.CommandType = CommandType.Text;//.StoredProcedure;
/*
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = temp_schema_id;
            cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = temp_group_id;
        */    
            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader();
                 
                while (dr.Read() == true)
                {
                    str_view += "<tr><td> <a href='~/manager/home/home_profile.aspx?home_id="+Convert.ToString(dr["home_id"])+"'>"+Convert.ToString(dr["home_name"])+"</a></td>";
                //get owner
                    str_view += "<td>"+getOwnerProfile(temp_schema_id,Convert.ToInt32(dr["home_id"]))+"</td>";
                    //get Manager
                    str_view += "<td>" + getManagerProfile(temp_schema_id, Convert.ToInt32(dr["home_id"])) + "</td>";
                    //get Janitor
                    str_view += "<td>" + getJanitorProfile(temp_schema_id, Convert.ToInt32(dr["home_id"])) + "</td>";
                    str_view += "</tr>";
            
               }
              // str_view += "<tr><td colspan='4'></td></tr></table>";

                return  str_view;
            }
            finally
            {
                conn.Close();
            }


            
            ;

        }
        protected string getOwnerProfile(int schema_id, int home_id)
        {
            string str_view = "";
        //get owner
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prProfileOwnerInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                   
            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader();

                while (dr.Read() == true)
                {
                    str_view += "<br/>" + Convert.ToString(dr["owner"]) + "<br/>" + Convert.ToString(dr["owner_tel"]) + "<br/>" + Convert.ToString(dr["owner_email"]) + "<br/><br/>";
                    //get owner
                }
                
                return str_view;
            }
            finally
            {
                conn.Close();
            }

        }



        /// <summary>
        /// get the names of the properties within a group
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="group_id"></param>
        /// <returns></returns>
        public DataTable getGroupHomeNameView(int schema_id, int group_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prGroupHomeNameView", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = group_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }

        }


        protected string getManagerProfile(int schema_id, int home_id)
        {
            string str_view = "";
            //get owner
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prProfileManagerInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader();

                while (dr.Read() == true)
                {
                    str_view += Convert.ToString(dr["manager"]) + "<br/>" + Convert.ToString(dr["manager_tel"]) + "<br/>" + Convert.ToString(dr["manager_email"])+"<br/>";
                    //get owner
                }

                return str_view;
            }
            finally
            {
                conn.Close();
            }



        }

        protected string getJanitorProfile(int schema_id, int home_id)
        {
            //get Janitor
            string str_view = "";
            //get owner
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prProfileJanitorInfo", conn);
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
            cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;

            try
            {
                conn.Open();

                SqlDataReader dr = null;
                dr = cmd.ExecuteReader();

                while (dr.Read() == true)
                {
                    str_view += Convert.ToString(dr["janitor"]) + "<br/>" + Convert.ToString(dr["janitor_tel"]) + "<br/>" + Convert.ToString(dr["janitor_email"]);
                    //get owner
                }

                return str_view;
            }
            finally
            {
                conn.Close();
            }

        }

         
    }
}
