﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using sinfoca.login;


/// <summary>
/// Summary description for Rent
/// Done by : Stanley Jocelyn
/// date    : march 4 , 2009
/// </summary>
/// 
namespace tiger
{
    public class Incident
    {
        private string str_conn;
        // Attributs
        private static int incidentCount;


        public Incident()
        {
        }


        public Incident(string str_conn)
        {
            this.str_conn = str_conn;
            SqlConnection conn = new SqlConnection(str_conn);
            //
            // TODO: Add constructor logic here
            //
        }




        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable getIncidentList(int home_id, DateTime date_from, DateTime date_to, int maximumRows, int startRowIndex)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prIncidentList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@incidentCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@date_from", SqlDbType.DateTime).Value = date_from;
                cmd.Parameters.Add("@date_to", SqlDbType.DateTime).Value = date_to;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);

                incidentCount = Convert.ToInt32(cmd.Parameters["@incidentCount"].Value);


                return dt;
            }
            finally
            {
                conn.Close();
            }
        }


        public static int getTotalIncidentCount(int home_id, DateTime date_from, DateTime date_to)
        {
            return incidentCount;
        }



        public DataTable getIncidentWorkOrderList(int schema_id, int incident_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prIncidentWorkOrderList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@incident_id", SqlDbType.Int).Value = incident_id;
               

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }

    }
}


