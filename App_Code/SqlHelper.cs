using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace Briefcase
{
    public static class SqlHelper
    {
        private static string strConn;

        static SqlHelper()
        {
            strConn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        
        }

        public static int ExecuteNonQuery(string sql)
        {
            return ExecuteNonQuery(sql, null);
        }

        public static int ExecuteNonQuery(string sql, SqlParameter[] p)
        {
            SqlConnection cnn = new SqlConnection(strConn);
            cnn.Open();
            SqlCommand cmd = new SqlCommand(sql,cnn);
            cmd.CommandType = CommandType.StoredProcedure;
            if (p != null)
            {
                for (int i = 0; i < p.Length; i++)
                {
                    cmd.Parameters.Add(p[i]);
                }
            }
            int retval = cmd.ExecuteNonQuery();
            cnn.Close();
            return retval;
        }

        public static object ExecuteScalar(string sql)
        {
            return ExecuteScalar(sql, null);
        }

        public static object ExecuteScalar(string sql, SqlParameter[] p)
        {
            SqlConnection cnn = new SqlConnection(strConn);
            cnn.Open();
            SqlCommand cmd = new SqlCommand(sql, cnn);
            cmd.CommandType = CommandType.StoredProcedure;
            if (p != null)
            {
                for (int i = 0; i < p.Length; i++)
                {
                    cmd.Parameters.Add(p[i]);
                }
            }
            object retval = cmd.ExecuteScalar();
            cnn.Close();
            return retval;
        }

        public static SqlDataReader ExecuteReader(string sql)
        {
            return ExecuteReader(sql, null);
        }

        public static SqlDataReader ExecuteReader(string sql, SqlParameter[] p)
        {
            SqlConnection cnn = new SqlConnection(strConn);
            cnn.Open();
            SqlCommand cmd = new SqlCommand(sql, cnn);
            cmd.CommandType = CommandType.StoredProcedure;
            if (p != null)
            {
                for (int i = 0; i < p.Length; i++)
                {
                    cmd.Parameters.Add(p[i]);
                }
            }
            SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            return reader;
        }

        public static DataSet ExecuteDataSet(string sql)
        {
            return ExecuteDataSet(sql, null);
        }

        public static DataSet ExecuteDataSet(string sql, SqlParameter[] p)
        {
            SqlConnection cnn = new SqlConnection(strConn);
            cnn.Open();
            SqlCommand cmd = new SqlCommand(sql, cnn);
            cmd.CommandType = CommandType.StoredProcedure;
            if (p != null)
            {
                for (int i = 0; i < p.Length; i++)
                {
                    cmd.Parameters.Add(p[i]);
                }
            }
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataSet ds = new DataSet();
            da.Fill(ds);
            cnn.Close();
            return ds;
        }

    }
}
