using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Net;
using System.Text;
using com.fraudlabs.ws;
using sinfoca.tiger.security;
using System.Net.Mail;
using sinfoca.login;

/// <summary>
/// Summary description for Security
/// </summary>

public class Security
{
    private string conn;
    private string browser_type;
    private string browser_browser;
    private string browser_version;
    private string ip;
    private string ip_location;

    public Security()
    {

    }

    public Security(string conn)
	{
		conn = this.conn;
	}
    public Security(HttpRequest Request)
    { Load(Request); }

    public bool roleServiceValid(int role_id, string service_url)
    {
        Boolean val = false;

        SqlConnection conn = new SqlConnection(Utils.CONN);
        SqlCommand cmd = new SqlCommand("prCmsRoleServiceValid", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

        cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
        cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = role_id;
        cmd.Parameters.Add("@service_url", SqlDbType.VarChar, 50).Value = service_url.Replace("/c2/", "/c/");

        try
        {

            conn.Open();
            //execute the insert
            cmd.ExecuteReader(CommandBehavior.CloseConnection);
            //Failure or Success (true = success (1), else fail) 
            if (Convert.ToInt32(cmd.Parameters["@return"].Value) >= 1)
            {
                val = true;

            }
            if (service_url.ToLower().Contains("default.aspx")
                || service_url.ToLower().Contains("processdata.aspx")
                || service_url.ToLower().Contains("paypal_pdt.aspx")
                || service_url.ToLower().Contains("paypal_wpm_pdt.aspx")
                || service_url.ToLower().Contains("asmspayment.aspx")
                || service_url.ToLower().Contains("asmspayment2.aspx")
                || service_url.ToLower().Contains("processscheduledata.aspx")
                || service_url.ToLower().Contains("processdatadw.aspx")
                || service_url.ToLower().Contains("approved.aspx")
                || service_url.ToLower().Contains("declined.aspx"))
            {
                val = true;
            }

            // Email.sendSESMailHtml("\"Staff Plus Support\" <support@sinfoca.com>", "jocestan@hotmail.com", "Permission", service_url + "<br/>" + val.ToString());
        }
        catch (Exception e)
        {
            sinfoca.error.ErrorLog.addErrorLog(e);

        }
        finally
        {
            conn.Close();
        }

        return val;
    }

    public string Ip
    {
        get { return ip; }
        set { ip = value; }
    }

    public string Ip_location
    {
        get { return ip_location; }
        set { ip_location = value; }
    }
    
    public string Browser_type
    { get { return browser_type; }
      set { browser_type = value; }
    }
    public string Browser_browser
    {
        get { return browser_browser; }
        set { browser_browser = value; }
    }

    public string Browser_version
    {
        get { return browser_version; }
        set { browser_version = value; }
    }

    protected void Load(HttpRequest Request)
    {
        HttpBrowserCapabilities browser = new HttpBrowserCapabilities();
        ip = HttpContext.Current.Request.UserHostAddress;
       // browser_browser = browser.Browser;
      //  browser_type = browser.Type;
      //  browser_version = browser.Version;
       // ip_location = addIp();
    }
    public bool addIp()
    {
        //SqlConnection conn = new SqlConnection(this.conn);
        SqlConnection conn = new SqlConnection(Utils.CONN);

        SqlCommand cmd = new SqlCommand("prIpInsert", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        conn.Open();

        //Add the params

        cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@ip", SqlDbType.VarChar, 15).Value = ip;
        cmd.Parameters.Add("@browser_type", SqlDbType.VarChar, 50).Value = browser_type;
        cmd.Parameters.Add("@browser_version", SqlDbType.VarChar, 50).Value = browser_version;
        cmd.Parameters.Add("@browser_browser", SqlDbType.VarChar, 50).Value = browser_browser;
        cmd.Parameters.Add("@ip_location", SqlDbType.VarChar, 500).Value = ip_location;

        cmd.ExecuteNonQuery();
        if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
            return false;
        else
            return true;  
    }

    public bool addIp(string ip_address,string ip_location)
    {
        //SqlConnection conn = new SqlConnection(this.conn);
        SqlConnection conn = new SqlConnection(Utils.CONN);
            
        SqlCommand cmd = new SqlCommand("prIpInsert", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        conn.Open();

        //Add the params

        cmd.Parameters.Add("@Return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@ip_address", SqlDbType.VarChar, 15).Value = ip_address;
        cmd.Parameters.Add("@ip_location", SqlDbType.VarChar, 500).Value = ip_location;

        cmd.ExecuteNonQuery();
        if (Convert.ToInt16(cmd.Parameters["@Return"].Value) == 1)
        {
            return false;
        }
        else
        {
            return true;
        }

    }
 

    public void sendUserInfo(string str_val, HttpBrowserCapabilities browser, HttpRequest Request )
    {
        //System.Web.HttpBrowserCapabilities browser = a.Browser;
        string s = "Browser Capabilities\n"
            + "Type = " + browser.Type + "\n"
            + "Name = " + browser.Browser + "\n"
            + "Version = " + browser.Version + "\n"
            + "Major Version = " + browser.MajorVersion + "\n"
            + "Minor Version = " + browser.MinorVersion + "\n"
            + "Platform = " + browser.Platform + "\n"
            + "Is Beta = " + browser.Beta + "\n"
            + "Is Crawler = " + browser.Crawler + "\n"
            + "Is AOL = " + browser.AOL + "\n"
            + "Is Win16 = " + browser.Win16 + "\n"
            + "Is Win32 = " + browser.Win32 + "\n"
            + "Supports Frames = " + browser.Frames + "\n"
            + "Supports Tables = " + browser.Tables + "\n"
            + "Supports Cookies = " + browser.Cookies + "\n"
            + "Supports VBScript = " + browser.VBScript + "\n"
            + "Supports JavaScript = " +
                browser.EcmaScriptVersion.ToString() + "\n"
            + "Supports Java Applets = " + browser.JavaApplets + "\n"
            + "Supports ActiveX Controls = " + browser.ActiveXControls + "\n";

        string VisitorsIPAddr = HttpContext.Current.Request.UserHostAddress;
        //Users IP Address.                
       /* if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
        {
            //To get the IP address of the machine and not the proxy
            VisitorsIPAddr = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
        }
        else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
        {
            VisitorsIPAddr = HttpContext.Current.Request.UserHostAddress;
         }*/
        //string temp_lookup = lookup(VisitorsIPAddr, "02-H35E-S94N");
        string desc = "";
        desc += "TIME:" + DateTime.Now;
        //desc += "<br/>User:" + Request.ServerVariables["LOGON_USER"].ToString();
        //desc += "<br/>LOCATION:" + temp_lookup;
        //desc += "<br/><br/>PAGE:" + Path.GetFileName(Request.PhysicalPath).ToString();
        desc += "<br/>VISITOR IP:" + ip;
        desc += "<br/>VISITOR BROWSER: " + browser_type;
        desc += "<br/><br/>VISITOR PAGE BEFORE:" + Request.ServerVariables["HTTP_REFERER"].ToString();
        desc += "<br/>TAMPERED VARIABLE:<br/>" + str_val+"<br/><br/><a href='https://sinfoca.info/tiger/ip_restriction.aspx'>check ip</a>";
        //addIp(VisitorsIPAddr, temp_lookup);
        
        sendSecMailHtml("runtime@sinfoca.com", "jocestan@hotmail.com", "Registration", desc);
  
    }

     public string lookup(string txtIP, string txtLicense)
     {
         /*Ip2LocationWebService.Ip2LocationWebService x_Ip2Location = new Ip2LocationWebServiceClientCSharp.Ip2LocationWebService.Ip2LocationWebService();
         IP2LOCATION oIp2Location = new IP2LOCATION();*/
         Ip2LocationWebService x_Ip2Location = new Ip2LocationWebService();
         //IP2LOCATION = new  IP2LOCATION();
         string temp = string.Empty;
         try
         {
             //create the request URL
             string x_builder;
             //add the host element of the URL
             x_builder = "http://ws.fraudlabs.com/Ip2LocationWebService.asmx/IP2Location?";
             x_builder += "IP=" + txtIP;
             x_builder += "&LICENSE=" + txtLicense;
             // create the web client and obtain the response data
             // as a byte array
             WebClient x_web_client = new WebClient();
             byte[] x_response = x_web_client.DownloadData(x_builder.ToString());
             // process the string result to obtain a validation result
             temp = processResultString(Encoding.Default.GetString(x_response));
         }
         catch (Exception ex)
         {
             //Response.Write(ex.Message);
             //tiger.Email.sendErrorMail("FraudLabs--UserLookup", ex.ToString());

         }
         return temp;
     }
     private  string processResultString(String p_result)
     {
         int indexOpen;
         int indexClose;
         string strParam, txtResult;
         strParam = "<COUNTRYCODE>";
         indexOpen = p_result.IndexOf(strParam) + strParam.Length;
         indexClose = p_result.IndexOf("</COUNTRYCODE>");
         txtResult = "Country Code: ";
         if (indexClose != -1)
         {
             txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
         }
         strParam = "<COUNTRYNAME>";
         indexOpen = p_result.IndexOf(strParam) + strParam.Length;
         indexClose = p_result.IndexOf("</COUNTRYNAME>");
         txtResult += "\rCountry Name: ";
         if (indexClose != -1)
         {
             txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
         }
         strParam = "<REGION>";
         indexOpen = p_result.IndexOf(strParam) + strParam.Length;
         indexClose = p_result.IndexOf("</REGION>");
         txtResult += "\rRegion Name: ";
         if (indexClose != -1)
         {
             txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
         }
         strParam = "<CITY>";
         indexOpen = p_result.IndexOf(strParam) + strParam.Length;
         indexClose = p_result.IndexOf("</CITY>");
         txtResult += "\rCity: ";
         if (indexClose != -1)
         {
             txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
         }
         strParam = "<LATITUDE>";
         indexOpen = p_result.IndexOf(strParam) + strParam.Length;
         indexClose = p_result.IndexOf("</LATITUDE>");
         txtResult += "\rLatitude: ";
         if (indexClose != -1)
         {
             txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
         }
         strParam = "<LONGITUDE>";
         indexOpen = p_result.IndexOf(strParam) + strParam.Length;
         indexClose = p_result.IndexOf("</LONGITUDE>");
         txtResult += "\rLongitude: ";
         if (indexClose != -1)
         {
             txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
         }
         strParam = "<ZIPCODE>";
         indexOpen = p_result.IndexOf(strParam) + strParam.Length;
         indexClose = p_result.IndexOf("</ZIPCODE>");
         txtResult += "\rZip Code: ";
         if (indexClose != -1)
         {
             txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
         }
         strParam = "<ISPNAME>";
         indexOpen = p_result.IndexOf(strParam) + strParam.Length;
         indexClose = p_result.IndexOf("</ISPNAME>");
         txtResult += "\rISP Name: ";
         if (indexClose != -1)
         {
             txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
         }
         strParam = "<DOMAINNAME>";
         indexOpen = p_result.IndexOf(strParam) + strParam.Length;
         indexClose = p_result.IndexOf("</DOMAINNAME>");
         txtResult += "\rDomain Name: ";
         if (indexClose != -1)
         {
             txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
         }
         strParam = "<CREDITSAVAILABLE>";
         indexOpen = p_result.IndexOf(strParam) + strParam.Length;
         indexClose = p_result.IndexOf("</CREDITSAVAILABLE>");
         txtResult += "\rCredits Available:";
         if (indexClose != -1)
         {
             txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
         }
         strParam = "<MESSAGE>";
         indexOpen = p_result.IndexOf(strParam) + strParam.Length;
         indexClose = p_result.IndexOf("</MESSAGE>");
         txtResult += "\rMessage: ";
         if (indexClose != -1)
         {
             txtResult += p_result.Substring(indexOpen, indexClose - indexOpen);
         }
         return txtResult;
     }

    public  void sendSecMailHtml(string from, string to, string subject, string body)
    {
        string style = "";

        style = "<head>";
        style += "<style id='oboutEditorDefaultStyle'>";
        style += ".boldItalic    { color: #c60; font-weight: bold; font-style:italic;}";
        style += ".alert         { color: #FF0000; font-weight: bold; }";
        style += ".style1        { font-family: verdana,arial,helvetica,sans-serif; font-size: x-small; }";
        style += ".style2        { font-family: verdana,arial,helvetica,sans-serif; color: #003399; }";
        style += ".style3        { font-family: verdana,arial,helvetica,sans-serif; color: #996633; }";
        style += ".style4        { font-family: verdana,arial,helvetica,sans-serif; color: #FF9933; }";
        style += ".sans          { font-family: verdana,arial,helvetica,sans-serif; font-size: small; }";
        style += ".small         { font-family: verdana,arial,helvetica,sans-serif; font-size: x-small; }";
        style += ".h1            { font-family: verdana,arial,helvetica,sans-serif; color: #CC6600; font-size: small; }";
        style += ".h3color       { font-family: verdana,arial,helvetica,sans-serif; color: #CC6600; font-size: x-small; }";
        style += ".tiny          { font-family: verdana,arial,helvetica,sans-serif; font-size: xx-small; }";
        style += ".listprice     { font-family: arial,verdana,helvetica,sans-serif; text-decoration: line-through; }";
        style += ".price         { font-family: arial,verdana,helvetica,sans-serif; color: #990000; }";
        style += ".tinyprice     { font-family: verdana,arial,helvetica,sans-serif; color: #990000; font-size: xx-small; }";
        style += ".highlight     { font-family: verdana,arial,helvetica,sans-serif; color: #990000; font-size: x-small; }";
        style += ".topnav-link   { text-decoration: none; color: #003399; }";
        style += ".topnav-hover  { text-decoration: none; color: #CC6600; }";
        style += ".topnav-active { font-family: verdana,arial,helvetica,sans-serif; font-size: 12px; color: #CC6600; text-decoration: none; }";
        style += ".tabon         { font-size: 10px; color: #FFCC66; font-family: verdana,arial,helvetica,sans-serif; text-decoration: none; text-transform: uppercase; font-weight: bold; line-height: 10px; }";
        style += ".taboff        { font-size: 10px; color: #000000; font-family: verdana,arial,helvetica,sans-serif; text-decoration: none; text-transform: uppercase; font-weight: bold; line-height: 10px; }";
        style += ".amabot        { color: #c60; font-size: .92em; }";



        style += "body       {";
        style += "            color:#404040;background-color: #fff;";
        style += "            border-width: 0px;margin-top: 0px; margin-bottom: 0px;";
        style += "            margin-left: 0px; margin-right: 0px;";
        style += "            padding-top: 0px; padding-bottom: 0px; padding-left: 0px; padding-right: 0px;";
        style += "           }";
        style += "body,table td";
        style += "           {";
        style += "            font-family: verdana,sans-serif;font-size: 10pt;";
        style += "           }";
        style += "h1         {";
        style += "            font-size: 24pt;";
        style += "           }";
        style += "h2         {";
        style += "            font-size: 18pt;";
        style += "           }";
        style += "h3         {";
        style += "            font-size: 14pt;";
        style += "           }";
        style += "h4         {";
        style += "            font-size: 12pt;";
        style += "           }";
        style += "h5         {";
        style += "            font-size: 10pt;";
        style += "           }";
        style += "h6         {";
        style += "            font-size:  8pt;";
        style += "           }";
        style += "</style>";


        style += "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />";
        style += "</head>";

        MailMessage message = new MailMessage();
        message.From = new MailAddress(from);

        /// Attention "to" recu en parametre n'est pas utilise
        message.To.Add(new MailAddress("jocestan@hotmail.com"));
        message.Bcc.Add(new MailAddress("stevemorisseau@hotmail.com"));

        message.Subject = subject;
        message.IsBodyHtml = true;
        message.Body = "<html>" + style + "<body>" + body.Replace("&lt;", "<").Replace("&gt;", ">") + "</body></html>";
        SmtpClient client = new SmtpClient("localhost");
        client.Send(message);


       

    }

}
