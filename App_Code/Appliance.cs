using System;
using System.Data;
using System.Data.SqlClient;
/// <summary>
/// Done by : Stanley Jocelyn
/// date    : sept 7 , 2007
/// 
/// </summary>
namespace tiger
{
    public class Appliance
    {
        private string str_conn;
        public Appliance(string str_conn)
        {
            this.str_conn  = str_conn;
        }
        /// <summary>
        /// GETS THE APPLIANCE IN A/ALL  PROPERTY , AND IN A UNIT OF THE PROPERTY BY APPLIANCE CATEGORY
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <param name="unit_id"></param>
        /// <param name="ac_id"></param>
        /// <returns></returns>
        public DataTable getApplianceList(int schema_id, int home_id, int unit_id, int ac_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prApplianceList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@unit_id", SqlDbType.Int).Value = unit_id;
                cmd.Parameters.Add("@ac_id", SqlDbType.Int).Value = ac_id;



            
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dt.TableName = "mydt";
                return dt;

             /*
                   using (SqlDataReader myReader = cmd.ExecuteReader())
                    {
                        DataTable myTable = new DataTable();
                        myTable.TableName = "mydt";
                        myTable.Load(myReader);
                        return myTable;
                      
                    }
             */



            }
            finally
            {
                conn.Close();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <param name="unit_id"></param>
        /// <param name="ac_id"></param>
        /// <returns></returns>
        public DataTable getApplianceSummaryList(int schema_id, int home_id,  int ac_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prApplianceSummaryList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@ac_id", SqlDbType.Int).Value = ac_id;




                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }
        /// <summary>
        /// GETS THE LIST OF APPLIANCES IN THE STORAGE UNIT OF A PROPERTY BY APPLAINCE CATEGORY
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <param name="unit_id"></param>
        /// <param name="ac_id"></param>
        /// <returns></returns>
        public DataTable getApplianceStorageUnitList(int schema_id, int home_id,int ac_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prApplianceStorageUnitList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@home_id", SqlDbType.Int).Value = home_id;
                cmd.Parameters.Add("@ac_id", SqlDbType.Int).Value = ac_id;




                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="appliance_id"></param>
        /// <returns></returns>
        public DataSet getApplianceView(int schema_id ,int appliance_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prApplianceView2", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@appliance_id", SqlDbType.Int).Value = appliance_id;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            finally
            {
                conn.Close();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Appliance_id"></param>
        /// <returns></returns>
        public string getApplianceLink(int Appliance_id)
        {
            string str = null;
            str += "<table><tr><td><a href='unit_view_info.aspx?tenant_id=" + Appliance_id + "'>Info</a></td>";
            str += "<td><a href='unit_view_info.aspx?unit_id=" + Appliance_id + "'>Service</a></td>";
            str += "<td><a href='unit_view_info.aspx?unit_id=" + Appliance_id + "'>Photo</a></td></table>";
            return str;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <returns></returns>
        public int getApplianceFirstId(int schema_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prApplianceFirstId", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@tenant_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;

                //execute the insert
                cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return Convert.ToInt32(cmd.Parameters["@tenant_id"].Value);
            }
            finally
            {
                conn.Close();
            }




        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="appliance_id"></param>
        /// <returns></returns>
        public DataTable getApplianceSearchList(int schema_id,int appliance_search_radio_value ,string appliance_search)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prApplianceSearchList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@appliance_search_radio_value", SqlDbType.Int).Value = appliance_search_radio_value;
                cmd.Parameters.Add("@appliance_search", SqlDbType.VarChar, 50).Value = appliance_search;
                
        
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="home_id"></param>
        /// <param name="unit_id"></param>
        /// <param name="ac_id"></param>
        /// <returns></returns>
        public DataTable getWarehouseApplianceList(int schema_id, int warehouse_id,int ac_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prWarehouseApplianceList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@warehouse_id", SqlDbType.Int).Value = warehouse_id;
                cmd.Parameters.Add("@ac_id", SqlDbType.Int).Value = ac_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="schema_id"></param>
        /// <param name="warehouse_id"></param>
        /// <param name="ac_id"></param>
        /// <returns></returns>
        public DataTable getWarehouseApplianceSummaryList(int schema_id, int warehouse_id, int ac_id)
        {
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prWarehouseApplianceSummaryList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@warehouse_id", SqlDbType.Int).Value = warehouse_id;
                cmd.Parameters.Add("@ac_id", SqlDbType.Int).Value = ac_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }

        public DataTable getWarehouseApplianceSearchList(int schema_id, int appliance_search_radio_value, string appliance_search)
        {
           
            //-------------------------------
            SqlConnection conn = new SqlConnection(str_conn);
            SqlCommand cmd = new SqlCommand("prWarehouseApplianceSearchList", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@appliance_search_radio_value", SqlDbType.Int).Value = appliance_search_radio_value;
                cmd.Parameters.Add("@appliance_search", SqlDbType.VarChar, 50).Value = appliance_search;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                return dt;
            }
            finally
            {
                conn.Close();
            }


           //--------------------------------
        }
    }
}
