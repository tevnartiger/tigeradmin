using System;
using System.Text.RegularExpressions;
using System.Web;

/// <summary>
/// Summary description for RegEx
/// </summary>

    public static class RegEx
    {    
            public static string name = @"^[ \u00c0-\u01ffa-zA-Z']+(([\'\,\.\- ][ \u00c0-\u01ffa-zA-Z' ])?[ \u00c0-\u01ffa-zA-Z']*)*$";
            public static string address = @"^[0-9\'\#\,\.\s\-\u00c0-\u01ffa-zA-Z.]*$";
            public static string numeric = @"^[0-9]+$";
            public static string alpha = @"^[a-zA-Z]*$";
            public static string email = @"^.+@[^\.].*\.[a-zA-Z]{2,}$";
            public static string pc = @"^[a-zA-Z]{1}[0-9]{1}[a-zA-Z]{1}[\s]{1}[0-9]{1}[a-zA-Z]{1}[0-9]{1}$";
            public static string tel = @"^[2-9]\d{2}-\d{3}-\d{4}$";
            public static string text = @"^[0-9\-\*\'\!\$\%\&\(\)\/\?\:\[\]\#\,\.\s\-\u00c0-\u01ffa-zA-Z._ ]*$";
            public static string pwd = @"^[0-9\'\#\,\.\s\-\u00c0-\u01ffa-zA-Z.]*$";
            public static string alpha_numeric = @"^[a-zA-Z0-9]+$";
            public static string guid = @"^[A-Fa-f0-9]{32}$|({|\()?[A-Fa-f0-9]{8}-([A-Fa-f0-9]{4}-){3}[A-Fa-f0-9]{12}(}|\))?$|^({)?[0xA-Fa-f0-9]{3,10}(, {0,1}[0xA-Fa-f0-9]{3,6}){2}, {0,1}({)([0xA-Fa-f0-9]{3,4}, {0,1}){7}[0xA-Fa-f0-9]{3,4}(}})$";
            public static string alpha_custom_value = @"^[a-zA-Z0-9_]+$";



    // private static string money = @"^([1-9]{1}[\d]{0,2}(\,[\d]{3})*(\.[\d]{0,2})?|[1-9]{1}[\d]{0,}(\.[\d]{0,2})?|0(\.[\d]{0,2})?|(\.[\d]{1,2})?)$";

            private static string money = @"^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$";
           
            public static string getName() { return name; }
            public static string getAddress() { return address; }
            public static string getNumeric() { return numeric; }
            public static string getEmail() { return email; }
            public static string getPc() { return pc; }
            public static string getTel() { return tel; }
           
            public static string getText() { return text; }
            public static string getText(String strToCheck)
            {

                Regex objNotWholePattern = new Regex(text);
                if (objNotWholePattern.IsMatch(strToCheck))
                    return strToCheck;
                else
                   return BVSoftware.Web.HtmlSanitizer.MakeHtmlSafe(strToCheck);
            }
            
            public static string getAlpha() { return alpha; }
            public static string getPwd() { return pwd; }
            public static string getAlphaNumeric() { return alpha_numeric; }
            public static string getMoney() { return money; }


            ///////////////////////////////////////////////////////////////////////////////////////
            /// <summary>
            /// Function to Test for Integers both Positive & Negative 
            /// </summary>
            /// <param name="strNumber"></param>
            /// <returns></returns>
            public static bool IsInteger(String strNumber)
            {
                    Regex objNotWholePattern = new Regex(numeric);
                    return (objNotWholePattern.IsMatch(strNumber));
            }


            public static bool IsAlphaCustomValue(String strAlphaCustomValue)
            {
                Regex objNotWholePattern = new Regex(alpha_custom_value);
                return (objNotWholePattern.IsMatch(strAlphaCustomValue));
            }
    /// <summary>
    /// Function to test whether the string is valid number or not 
    /// </summary>
    /// <param name="strNumber"></param>
    /// <returns></returns>
    public static bool IsNumber(String strNumber)
            {
                
                    Regex objNotNumberPattern = new Regex("[^0-9.-]");
                    Regex objTwoDotPattern = new Regex("[0-9]*[.][0-9]*[.][0-9]*");
                    Regex objTwoMinusPattern = new Regex("[0-9]*[-][0-9]*[-][0-9]*");
                    String strValidRealPattern = "^([-]|[.]|[-.]|[0-9])[0-9]*[.]*[0-9]+$";
                    String strValidIntegerPattern = "^([-]|[0-9])[0-9]*$";
                    Regex objNumberPattern = new Regex("(" + strValidRealPattern + ")|(" + strValidIntegerPattern + ")");
                    return !objNotNumberPattern.IsMatch(strNumber) &&
                    !objTwoDotPattern.IsMatch(strNumber) &&
                    !objTwoMinusPattern.IsMatch(strNumber) &&
                    objNumberPattern.IsMatch(strNumber);
               
            }





            /// <summary>
            /// Function to Check for Email. 
            /// Description : Most email validation regexps are outdated and ignore the fact that domain names 
            ///  can contain any foreign character these days, as well as the fact that anything before @ is acceptable. 
            ///  The only roman alphabet restriction is in the TLD, which for a long time has been more than 2 or 3 chars
            ///  (.museum, .aero, .info). The only dot restriction is that . cannot be placed directly after @. 
            ///  This pattern captures any valid, reallife email adress.
            ///
            ///  Matches whatever@somewhere.museum | foreignchars@myforeigncharsdomain.nu | me+mysomething@mydomain.com     
            /// </summary>
            /// <param name="strEmail"></param>
            /// <returns></returns>
            public static bool IsEmail(String strToCheck)
            {
                // string expr = @"^.+@[^\.].*\.[a-z]{2,}$" ;
                Regex objNotWholePattern = new Regex(email);
                return objNotWholePattern.IsMatch(strToCheck);
            }


            /// <summary>
            /// Function to Check for Name ( alphabetic & special characters only ).
            /// Expression to match names and dis-allow any attempts to send evil characters.
            /// In particular, it tries to allow non-english names by allowing unicode characters.
            /// </summary>
            /// <param name="strName"></param>
            /// <returns></returns>
            public static bool IsName(String strToCheck)
            {
                // string expr = @"^[ \u00c0-\u01ffa-zA-Z']+(([\'\,\.\- ][ \u00c0-\u01ffa-zA-Z' ])?[ \u00c0-\u01ffa-zA-Z']*)*$";
                Regex objNotWholePattern = new Regex(name);
                return objNotWholePattern.IsMatch(strToCheck);
            }



            ///////////////////////////////////////////////////////////////////////////////////////
            /// <summary>
            /// Function to Test for Integers both Positive & Negative 
            /// </summary>
            /// <param name="strMoney"></param>
            /// <returns></returns>
            public static bool IsMoney(String strMoney)
            {

               // Description  	Expresion para tipo Money &#243; Cantidad
               // Matches 	132,123,123.23 | 123456.23 | 0.23
               //  Non-Matches 	123,12 | 123.123 | 1322,132.23
                Regex objNotWholePattern = new Regex(money);
                return objNotWholePattern.IsMatch(strMoney);
            }


            public static string getMoney(String strMoney)
            {

                string culture = HttpContext.Current.Session["_lastCulture"].ToString();

                Regex objNotWholePattern = new Regex(money);
                if (objNotWholePattern.IsMatch(strMoney))
                {
                    if (culture == "fr-FR")
                        strMoney = strMoney.Replace(".",",");
                   
                    if (culture == "en-US")
                        strMoney = strMoney.Replace(",", ".");

                    return strMoney;
                   
                }
                else
                    return "0";
            }

            public static string getInteger(String strInteger)
            
            {
                Regex objNotWholePattern = new Regex(numeric);
                if (objNotWholePattern.IsMatch(strInteger))
                    return strInteger;
                else
                    return "0";
            }


            /// <summary>
            /// Function to Check for pwd ( alphabetic & special characters only ).
            /// Expression to match names and dis-allow any attempts to send evil characters.
            /// In particular, it tries to allow non-english names by allowing unicode characters.
            /// </summary>
            /// <param name="strName"></param>
            /// <returns></returns>
            public static bool IsPwd(String strToCheck)
            {
                // string expr = @"^[ \u00c0-\u01ffa-zA-Z']+(([\'\,\.\- ][ \u00c0-\u01ffa-zA-Z' ])?[ \u00c0-\u01ffa-zA-Z']*)*$";
                Regex objNotWholePattern = new Regex(pwd);
                return objNotWholePattern.IsMatch(strToCheck);
            }

            /// <summary>
            /// Description  	
            /// U. S. or Canadian telephone number regular expression.
            /// # Checks phone numbers for validity 
            /// [01]? # optional '0', or '1'
            /// [- .]? # optional separator is either a dash, a space, or a period. 
            /// \(? # optional parentheses
            /// [2-9] # first # of the area code must not be a '0' or '1' 
            /// \d{2} # next 2 digits of area code can be 0-9 
            /// \)? # optional parentheses &lt;BR&gt; // [- .]? # optional separator is either a dash, a space, or a period.
            /// \d{3} # 3-digit prefix &lt;BR&gt; // [- .]? # optional separator is either a dash, a space, or a period
            /// \d{4} # 4-digit station number &lt;BR&gt;

            /// </summary>
            /// <param name="strName"></param>
            /// <returns></returns>
            public static bool IsPhoneNumber(String strPhoneNumber)
            {
                // string expr = @"^[01]?[- .]?\(?[2-9]\d{2}\)?[- .]?\d{3}[- .]?\d{4}$";
                Regex objNotWholePattern = new Regex(tel);
                return objNotWholePattern.IsMatch(strPhoneNumber);
            }


            /// <summary>
            /// Description 	
            /// Regular expression to match a canadian postal code where it matches a string with or without
            ///          the hyphen and in upercase or lowercase
            ///          
            /// Matches : a1a-1a1 | A1A1A1
            /// Non-Matches  : 1a1-a1a | aaa-aaa | 111-111
            /// </summary>
            /// <param name="strPhoneNumber"></param>
            /// <returns></returns>
            public static bool IsCanPostalCode(String strCanPostalCode)
            {
                Regex objNotWholePattern = new Regex(pc);
                return objNotWholePattern.IsMatch(strCanPostalCode);
            }


            public static bool IsText(String strToCheck)
            {

                Regex objNotWholePattern = new Regex(text);
                return objNotWholePattern.IsMatch(strToCheck);
            }

          

            public static bool IsAddress(String strToCheck)
            {

                Regex objNotWholePattern = new Regex(address);
                return objNotWholePattern.IsMatch(strToCheck);
            }

            public static bool IsNumeric(String strToCheck)
            {

                Regex objNotWholePattern = new Regex(numeric);
                return objNotWholePattern.IsMatch(strToCheck);
            }
            public static bool IsAlphaNumeric(String strToCheck)
            {

                Regex objNotWholePattern = new Regex(alpha_numeric);
                return objNotWholePattern.IsMatch(strToCheck);
            }

            public static bool IsAlpha(String strToCheck)
            {

                Regex objNotWholePattern = new Regex(alpha);
                return objNotWholePattern.IsMatch(strToCheck);
            }

            public static bool IsGuid(String strToCheck)
            {

                Regex objNotWholePattern = new Regex(guid);
                return objNotWholePattern.IsMatch(strToCheck);
            }

}

 
