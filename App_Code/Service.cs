﻿using System;
using System.Data;
using System.Data.SqlClient;
using sinfoca.login;

/// <summary>
/// Summary description for Service
/// </summary>
namespace sinfoca.tiger
{
    public class Service
    {

        // Attributs

        private int new_contact_id;
        private int servicerecordexistforlang;
        private int service_id;
        private string lang = String.Empty;
        private int service_hours;
        private int service_minutes;
        private int schema_id;
        private int sc_id;
        private string service_name = String.Empty;
        private string service_name_fr = String.Empty;
        private string sc_name = String.Empty;
        private string sc_name_fr = String.Empty;
        private string service_comment = String.Empty;
        private string service_url = String.Empty;
        private string trace_user_ip;
        private int return_id;
        private int service_Count;


        //Constructor
        public Service()
        { }

        public int New_service_id
        {
            get { return new_contact_id; }
            set { new_contact_id = value; }
        }

        public int Schema_id
        {
            get { return schema_id; }
            set { schema_id = value; }
        }

        public int Sc_id
        {
            get { return sc_id; }
            set { sc_id = value; }
        }


        public string Lang
        {
            get { return lang; }
            set { lang = value; }
        }


        public int Service_id
        {
            get { return service_id; }
            set { service_id = value; }
        }

        public int Service_count
        {
            get { return service_Count; }
            set { service_Count = value; }
        }

        public int Service_hours
        {
            get { return service_hours; }
            set { service_hours = value; }
        }

        public int ServiceRecordExistForlang
        {
            get { return servicerecordexistforlang; }
            set { servicerecordexistforlang = value; }
        }


        public int Service_minutes
        {
            get { return service_minutes; }
            set { service_minutes = value; }
        }

        public string Service_name
        {
            get { return service_name; }
            set { service_name = RegEx.getText(Utils.sanitizeString(value, 100)); }
        }

        public string Service_name_fr
        {
            get { return service_name_fr; }
            set { service_name_fr = RegEx.getText(Utils.sanitizeString(value, 100)); }
        }



        public string Sc_name
        {
            get { return sc_name; }
            set { sc_name = RegEx.getText(Utils.sanitizeString(value, 100)); }
        }

        public string Sc_name_fr
        {
            get { return sc_name_fr; }
            set { sc_name_fr = RegEx.getText(Utils.sanitizeString(value, 100)); }
        }


        public string Service_comment
        {
            get { return service_comment; }
            set { service_comment = RegEx.getText(Utils.sanitizeString(value, 100)); }
        }

        public string Service_url
        {
            get { return service_url; }
            set { service_url = RegEx.getText(Utils.sanitizeString(value, 100)); }
        }

        public string Trace_user_ip
        {
            get { return trace_user_ip; }
            set { trace_user_ip = RegEx.getText(Utils.sanitizeString(value, 15)); }
        }

        public int Return_id
        {
            get { return return_id; }
            set { return_id = value; }
        }

        public int setServiceCategAdd()
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prServiceCategAdd", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                //cmd.Parameters.Add("@new_service_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString()); ;
                cmd.Parameters.Add("@sc_name_en", SqlDbType.NVarChar, 100).Value = sc_name;
                cmd.Parameters.Add("@sc_name_fr", SqlDbType.NVarChar, 100).Value = sc_name_fr;
               
                //execute the insert
                cmd.ExecuteReader();
                //
                return Convert.ToInt32(cmd.Parameters["@return_id"].Value);

                // New_service_id = Convert.ToInt32(cmd.Parameters["@new_service_id"].Value);

            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }



        public int setServiceAdd()
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prServiceAdd", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                //cmd.Parameters.Add("@new_service_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString()); ;
                cmd.Parameters.Add("@service_trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["name_id"].ToString());
                cmd.Parameters.Add("@service_trace_person_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@service_name_en", SqlDbType.NVarChar, 100).Value = service_name;
                cmd.Parameters.Add("@service_name_fr", SqlDbType.NVarChar, 100).Value = service_name_fr;
                cmd.Parameters.Add("@service_desc", SqlDbType.NVarChar, 100).Value = service_comment;
                cmd.Parameters.Add("@service_url", SqlDbType.NVarChar, 100).Value = service_url;
                cmd.Parameters.Add("@sc_id", SqlDbType.Int).Value = sc_id;
            
                //execute the insert
                cmd.ExecuteReader();
                //
               return Convert.ToInt32(cmd.Parameters["@return_id"].Value);
               
               // New_service_id = Convert.ToInt32(cmd.Parameters["@new_service_id"].Value);
                
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return 0;
            }
            finally
            {
             conn.Close();
            }
        }



        public int setServiceCategUpdate(int sc_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prServiceCategUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                //cmd.Parameters.Add("@new_service_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString()); ;
                cmd.Parameters.Add("@sc_name_en", SqlDbType.NVarChar, 100).Value = sc_name;
                cmd.Parameters.Add("@sc_name_fr", SqlDbType.NVarChar, 100).Value = sc_name_fr;
                cmd.Parameters.Add("@sc_id", SqlDbType.Int).Value = sc_id;
               
                //execute the insert
                cmd.ExecuteReader();
                //
                return Convert.ToInt32(cmd.Parameters["@return_id"].Value);

                // New_service_id = Convert.ToInt32(cmd.Parameters["@new_service_id"].Value);

            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

   

        public int setServiceUpdate(int service_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prServiceUpdate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                //cmd.Parameters.Add("@new_service_id", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@return_id", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString()); ;
                cmd.Parameters.Add("@service_trace_person_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["name_id"].ToString());
                cmd.Parameters.Add("@service_trace_person_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@service_name_en", SqlDbType.NVarChar, 100).Value = service_name;
                cmd.Parameters.Add("@service_name_fr", SqlDbType.NVarChar, 100).Value = service_name_fr;
                cmd.Parameters.Add("@service_desc", SqlDbType.NVarChar, 100).Value = service_comment;
                cmd.Parameters.Add("@service_url", SqlDbType.NVarChar, 100).Value = service_url;
                cmd.Parameters.Add("@sc_id", SqlDbType.Int).Value = sc_id;
                cmd.Parameters.Add("@service_id", SqlDbType.Int).Value = service_id;

                //execute the insert
                cmd.ExecuteReader();
                //
                return Convert.ToInt32(cmd.Parameters["@return_id"].Value);

                // New_service_id = Convert.ToInt32(cmd.Parameters["@new_service_id"].Value);

            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }

   



        public void getServiceDelete(int service_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prServiceDelete", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@trace_user_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["user_id"].ToString());
                cmd.Parameters.Add("@trace_user_ip", SqlDbType.VarChar, 15).Value = System.Web.HttpContext.Current.Request.UserHostAddress.ToString();
                cmd.Parameters.Add("@service_id", SqlDbType.Int).Value = service_id;

                //execute the insert
                cmd.ExecuteReader();
                //

                
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
              conn.Close();
            }
        }



        public DataTable getServiceCategView(int sc_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd2 = new SqlCommand("prServiceCategView", conn);

            DataTable dt = new DataTable("ServiceCategView");
            cmd2.CommandType = CommandType.StoredProcedure;


            try
            {

                conn.Open();
                //Add the params
                cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd2.Parameters.Add("@sc_id", SqlDbType.Int).Value = sc_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd2);
                da.Fill(dt);

                return dt;
            }

            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }

        }



        public DataTable getServiceView(int service_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd2 = new SqlCommand("prServiceView", conn);

            DataTable dt = new DataTable("ServiceView");
            cmd2.CommandType = CommandType.StoredProcedure;


            try
            {

                conn.Open();
                //Add the params
                cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd2.Parameters.Add("@service_id", SqlDbType.Int).Value =  service_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd2);
                da.Fill(dt);

                return dt;
            }

            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }

        }


        public void getServiceView(int service_id, string lang )
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd2 = new SqlCommand("prServiceView", conn);

            cmd2.CommandType = CommandType.StoredProcedure;


            conn.Open();
            //Add the params
            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
            cmd2.Parameters.Add("@lang", SqlDbType.VarChar,10).Value = lang;
            cmd2.Parameters.Add("@service_id", SqlDbType.Int).Value = service_id;
            try
            {

                SqlDataReader dr = null;
                dr = cmd2.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                    service_name = dr["service_name"].ToString();
                    service_comment = dr["service_com"].ToString();
                    service_hours = Convert.ToInt32(dr["service_hours"]);
                    service_minutes = Convert.ToInt32(dr["service_minutes"]);
                }

            }


            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }

        }




        public void getServiceExistForLang(int service_id, string lang)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd2 = new SqlCommand("prServiceExistForLang", conn);

            cmd2.CommandType = CommandType.StoredProcedure;


            conn.Open();
            //Add the params
            cmd2.Parameters.Add("@ServiceExistForLang", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
            cmd2.Parameters.Add("@service_id", SqlDbType.Int).Value = service_id;
            cmd2.Parameters.Add("@lang", SqlDbType.VarChar, 10).Value = lang;
            try
            {

                SqlDataReader dr = null;
                dr = cmd2.ExecuteReader();

                ServiceRecordExistForlang = Convert.ToInt32(cmd2.Parameters["@ServiceExistForLang"].Value);
            }

            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }

            finally
            {
                conn.Close();
            }

        }


        public DataTable getServiceList(string name)
        {

            string[] str_result = new string[25];
            sinfoca.tiger.Settings settings = new sinfoca.tiger.Settings();
            str_result = settings.getSettingsLangArray();
          
            string default_lang = Convert.ToString(str_result[0]);
            string current_lang = (System.Web.HttpContext.Current.Session["_lastCulture"].ToString().Substring(0, 2));
            string schema_lang = (System.Web.HttpContext.Current.Session["schema_lang"].ToString());
            string lang = "";

            if (schema_lang.Contains(current_lang))
                lang = current_lang;
            else
                lang = default_lang;
            

            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prServiceList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
               
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@lang", SqlDbType.VarChar, 10).Value = lang;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }
            return dt;

        }


        public DataTable getServiceList(string name, string lang)
        {

            string[] str_result = new string[25];
            sinfoca.tiger.Settings settings = new sinfoca.tiger.Settings();
            str_result = settings.getSettingsLangArray();

            string default_lang = Convert.ToString(str_result[0]);
            string current_lang = (lang.Substring(0, 2));
            string schema_lang = (System.Web.HttpContext.Current.Session["schema_lang"].ToString());
            
            if (schema_lang.Contains(current_lang))
                lang = current_lang;
            else
                lang = default_lang;


            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prServiceList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@lang", SqlDbType.VarChar, 10).Value = lang;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }
            return dt;

        }

        public DataTable getServiceList(string name,int schema_id)
        {

            string[] str_result = new string[25];
            sinfoca.tiger.Settings settings = new sinfoca.tiger.Settings();
            str_result = settings.getSettingsLangArray();

            string default_lang = Convert.ToString(str_result[0]);
            string current_lang = (System.Web.HttpContext.Current.Session["_lastCulture"].ToString().Substring(0, 2));
            string schema_lang = (System.Web.HttpContext.Current.Session["schema_lang"].ToString());
            string lang = "";

            if (schema_lang.Contains(current_lang))
                lang = current_lang;
            else
                lang = default_lang;
            
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prServiceList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@lang", SqlDbType.VarChar, 10).Value = lang;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }
            return dt;

        }

        public DataTable getFrameServiceList(int schema_id ,string name)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prServiceList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                
                da.Fill(dt);
                
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }
             return dt;

        }



        public DataTable getServiceForUserRole()
        {


            string[] str_result = new string[25];
            sinfoca.tiger.Settings settings = new sinfoca.tiger.Settings();
            str_result = settings.getSettingsLangArray();

            string default_lang = Convert.ToString(str_result[0]);
            string current_lang = (System.Web.HttpContext.Current.Session["_lastCulture"].ToString().Substring(0, 2));
            string schema_lang = (System.Web.HttpContext.Current.Session["schema_lang"].ToString());
            string lang = "";

            if (schema_lang.Contains(current_lang))
                lang = current_lang;
            else
                lang = default_lang;

            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prServiceForUserRole", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["user_id"].ToString());
                cmd.Parameters.Add("@lang", SqlDbType.VarChar, 10).Value = lang;
               
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                
                da.Fill(dt);
                
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close(); 
            }
            return dt;
        }

        public DataTable getServiceForUserRole(int user_id)
        {

            string[] str_result = new string[25];
            sinfoca.tiger.Settings settings = new sinfoca.tiger.Settings();
            str_result = settings.getSettingsLangArray();

            string default_lang = Convert.ToString(str_result[0]);
            string current_lang = (System.Web.HttpContext.Current.Session["_lastCulture"].ToString().Substring(0, 2));
            string schema_lang = (System.Web.HttpContext.Current.Session["schema_lang"].ToString());
            string lang = "";

            if (schema_lang.Contains(current_lang))
                lang = current_lang;
            else
                lang = default_lang;


            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prServiceForUserRole", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = user_id;
                cmd.Parameters.Add("@lang", SqlDbType.VarChar, 10).Value = lang;
             
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                
                da.Fill(dt);
               
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }


        public DataTable getFrameServiceForUserRole(int schema_id,int user_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prServiceForUserRole", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = schema_id;
                cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = user_id;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                
                da.Fill(dt);
                
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }


        public DataTable getServiceSearchForUserRole(string name)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prServiceSearchForUserRole", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["user_id"].ToString());
                cmd.Parameters.Add("@name", SqlDbType.VarChar, 50).Value = name;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
              
                da.Fill(dt);
                
            }
            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
            }
            finally
            {
                conn.Close();
            }
            
            return dt;
        }

        [System.ComponentModel.DataObjectMethodAttribute(System.ComponentModel.DataObjectMethodType.Select, false)]
        public DataTable getServiceList(int maximumRows, int startRowIndex, int category_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prServiceList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable("Service");

            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@serviceCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@category_id", SqlDbType.Int).Value = category_id;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                service_Count = Convert.ToInt32(cmd.Parameters["@serviceCount"].Value);


                return dt;
            }

            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }


        public int getServiceCount(int maximumRows, int startRowIndex, int category_id)
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prServiceCount", conn);
            cmd.CommandType = CommandType.StoredProcedure;
         
            int PageNo = 0;
            if (startRowIndex == 0)
                PageNo = 1;
            else
                PageNo = (startRowIndex / maximumRows) + 1;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
                cmd.Parameters.Add("@serviceCount", SqlDbType.Int).Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@category_id", SqlDbType.Int).Value = category_id;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = maximumRows;
                cmd.Parameters.Add("@PageNo", SqlDbType.Int).Value = PageNo;


                cmd.ExecuteReader();

                return service_Count = Convert.ToInt32(cmd.Parameters["@serviceCount"].Value);


           
            }

            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return 0;
            }
            finally
            {
                conn.Close();
            }
        }


       public DataTable getServiceCategList()
        {
            SqlConnection conn = new SqlConnection(Utils.CONN);
            SqlCommand cmd = new SqlCommand("prServiceCategList", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable("ServiceCateg");

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(System.Web.HttpContext.Current.Session["schema_id"].ToString());
               
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

             
                return dt;
            }

            catch (Exception ex)
            {
                sinfoca.error.ErrorLog.addErrorLog(ex);
                return dt;
            }
            finally
            {
                conn.Close();
            }
        }

    }
}
