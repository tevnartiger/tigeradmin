﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// Summary description for SecurityHttpModule
/// 
/// done by : Stanley Jocelyn
/// date    : 30 nov , 2008
/// </summary>
///
public class SecurityHttpModule : IHttpModule
{
    public SecurityHttpModule() { }

    public void Init(HttpApplication context)
    {
        context.BeginRequest += new EventHandler(Application_BeginRequest);
     //   context.AcquireRequestState += new EventHandler(Application_AcquireRequestState);
    }



    /// <summary>
    /// For each request , the application verify if the user within the session
    /// is allow to enter the site
    /// </summary>
    /// <param name="source"></param>
    /// <param name="e"></param>
    private void Application_BeginRequest(object source, EventArgs e)
    {
        HttpContext context = ((HttpApplication)source).Context;
        string ipAddress = context.Request.UserHostAddress.ToString();

        if (context.Application["Ip_restriction_status"].ToString() == "1")
        {

           if (IsNotValidIpAddress(ipAddress))
          {
            context.Response.StatusCode = 403;  // (Forbidden)
            context.Response.Redirect("http://www.google.com");

           }
        }
    }

    private bool IsNotValidIpAddress(string ipAddress)
    {

        SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        SqlCommand cmd = new SqlCommand("prIpStatus", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        conn.Open();
        //Add the params
        cmd.Parameters.Add("@return_status", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add("@ip_address", SqlDbType.VarChar,15).Value = ipAddress;
        
        //execute the modification
        cmd.ExecuteNonQuery();
           


        if (Convert.ToInt32(cmd.Parameters["@return_status"].Value) == 1)
        {
            conn.Close();
            return true;
        }
        else
        {
            conn.Close();
            return false;
        }

       // return (ipAddress == "127.0.0.1");  
    }

    public void Dispose() { /* clean up */ }
}

