using System;
using System.Data;
using System.Globalization;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for DateTime
/// </summary>
/// 
namespace tiger
{
        public class Date
        {
            /// <summary>
            /// Checks whether or not a date is valid
            /// </summary>
            /// <param name="date"></param>
            /// <returns>true or false</returns>
            public static bool isDate(string date)
            {
                try
                {
                    System.DateTime.Parse(date);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

        public string DateCulture(string month, string day, string year)
        {
            string date = "";

            string _lastCulture = System.Web.HttpContext.Current.Session["_lastCulture"].ToString().Trim();

            if (_lastCulture == "en-CA")
                _lastCulture = "en-ca";
            else if (_lastCulture == "fr-FR")
                _lastCulture = "fr-fr";
            else if (_lastCulture == "fr-CA")
                _lastCulture = "fr-ca";
            else if (_lastCulture == "es-ES")
                _lastCulture = "es-es";
            else if (_lastCulture == "fr-CA")
                _lastCulture = "fr-ca";
            else if (_lastCulture == "en-US")
                _lastCulture = "en-us";
            else
                _lastCulture = "en-us";


            if (_lastCulture == "en-us")
            {
                date = month + "/" + day + "/" + year;
            }

            if (_lastCulture == "en-ca")
            {
                date = day + "/" + month + "/" + year;
            }

            if (_lastCulture == "fr-ca")
            {
                date = month + "/" + day + "/" + year;
            }

            if (_lastCulture == "fr-fr")
            {

                string myDateTimeFrenchValue = day + "/" + month + "/" + year;

                IFormatProvider culture = new CultureInfo("fr-FR", true);
                DateTime myDateTimeFrench =
                    DateTime.Parse(myDateTimeFrenchValue,
                                   culture,
                                   DateTimeStyles.NoCurrentDateDefault);
                date = myDateTimeFrench.ToString();
            }


            if (_lastCulture == "es-es")
            {

                string myDateTimeSpanishValue = day + "/" + month + "/" + year;

                IFormatProvider culture = new CultureInfo("es-ES", true);
                DateTime myDateTimeFrench =
                    DateTime.Parse(myDateTimeSpanishValue,
                                   culture,
                                   DateTimeStyles.NoCurrentDateDefault);
                date = myDateTimeFrench.ToString();
            }

            return date;

        }

        public string DateCulture(string month,string day, string year, string _lastCulture)
            {
                string date = "";

                if (_lastCulture == "en-US")
                {
                    date = month + "/" + day + "/" + year;
                }

                if (_lastCulture == "fr-FR")
                {

                    string myDateTimeFrenchValue = day + "/" + month + "/" + year;
                    
                    IFormatProvider culture = new CultureInfo("fr-FR", true);
                    DateTime myDateTimeFrench =
                        DateTime.Parse(myDateTimeFrenchValue,
                                       culture,
                                       DateTimeStyles.NoCurrentDateDefault);
                    date = myDateTimeFrench.ToString();
                }
                 
                return date;

            }

        }
    }
