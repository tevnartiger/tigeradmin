﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.Configuration;
/// <summary>
/// Summary description for config
/// </summary>
public static class config
{
    public static string cdn_host = "http://s3.amazonaws.com/asmsplus/";
    public static string host = "http://"+ System.Web.HttpContext.Current.Request.Url.Host.ToLower();
    // public static string host = "http://127.0.0.1:81";
    // public static string host = "http://asmsplus.azurewebsites.net";
    public static string company_tel = "+33 7 78 56 21 41";
    public static string site_id = "9";
    public static string site_code = "dshx";
    public static string lang_used = "fr,en";
    public static string default_lang = "en";
    public static string default_lang_en = "en-ca";
    public static string default_lang_fr = "fr-ca";
    public static string fulldefault_lang = "en-CA";

    public static string bucket_name = "sinfoca_cache";
    public static string path_img_upload = "E:\\www\\web\\prod\\cmsplongeur.sinfodev.info\\App_Data\\";
    public static string fileloc = System.Web.HttpContext.Current.Request.PhysicalApplicationPath;
    //register 
    public static string dbconn = System.Configuration.ConfigurationManager.AppSettings["ConnectionString"];
    public static string cn = "server=50.19.238.215;database=plongeurexpress;uid=sinfoca_dwe_pub;password=111111;   MultipleActiveResultSets=true";
    public static string schema_id = "124";
    public static string ordermaximumdays = "30";
    public static string shiftminimumhours = "4";
    public static string paypal_email = "laurentlake@hotmail.com";

    public static string stripemaxpreauthdays = "7";


    public static string website = getWebsite();
    public static string company_name2 = getCompany_name2();
    public static string company_name = getCompany_name();
    public static string s3_host = "https://s3.amazonaws.com/sinfoca_cache/";
    public static string send_mail_to = "jocestan@hotmail.com;stevemorisseau@hotmail.com";


    public static string schema_address = "803-4, Notre-Dame Est";
    public static string shema_city_cp = "Montreal (Quebec), H2Y 1B8<br/>Tel.: (888) 601-1615<br/>Fax: (888) 280-4654<br/>www.netl2.com";
    public static string no_tps  =  "";
    public static string no_tvq =  "";
    public static string no_hst = "";
  
  
    public static List<string> queryLangList()
    {
        List<string> queryStringList = new List<string>();
        queryStringList.Add("en");
        queryStringList.Add("fr");

        return queryStringList;
    }

    public static Dictionary<string,string> LangList()
    {
        Dictionary<string,string> queryStringList = new Dictionary<string,string>();
        queryStringList.Add("en","English");
        queryStringList.Add("fr","Francais");
        return queryStringList;
    }

    public static string getCompany_name2()
    {
        string response = "";
        string host2 = System.Web.HttpContext.Current.Request.Url.Host.ToLower();
        string[] words = host2.Split('.');

        if (words[0] != "admin" && words[0] != "register")
            response = "NETL2";
        else
            response = "ASMS+";

        return response;
    }

    public static string getWebsite()
    {
        string response = "";
        response = "netl2.asmsplus.com";

        return response;
    }

    public static string getCompany_name()
    {
        string response = "";
        string host2 = System.Web.HttpContext.Current.Request.Url.Host.ToLower();
        string[] words = host2.Split('.');

        if (words[0] != "admin" && words[0] != "register")
            response = "NETL2";
        else
            response = "ASMS+";

        return response;
    }

    public static int getTotalLang()
    {
        int i = 0;
        string[] words = lang_used.Split(',');

        foreach (string word in words)
        {
            i++;
        }
        return i;
    }

    public static string getDefaultLang()
    {
        string[] words = lang_used.Split(',');
        return words[0];
    }

    public static DataTable getLangUsed()
    {
        DataTable dt = new DataTable("aa");
        DataColumn dc_lang;

        dc_lang = new DataColumn();
        dc_lang.ColumnName = "lang";
        dt.Columns.Add(dc_lang);

        string[] words = lang_used.Split(',');

        DataRow dr;

        foreach (string word in words)
        {
            dr = dt.NewRow();
            dr["lang"] = word;
            dt.Rows.Add(dr);
        }

        return dt;
    }


    public static DataTable getLangInDb(DataTable dtc)
    {
        try
        {
            DataTable dt = new DataTable("aa");
            DataColumn dc_lang, dc_status;

            dc_lang = new DataColumn();
            dc_status = new DataColumn();

            dc_lang.ColumnName = "lang";
            dc_status.ColumnName = "lang_status";

            dt.Columns.Add(dc_lang);
            dt.Columns.Add(dc_status);

            string lang_used = "";

            string[] words = lang_used.Split(',');

            DataRow dr;
            string var = "";
            bool val = false;
            string s = "";

            foreach (string word in words)
            {
                val = false;
                if ((dtc != null) && (dtc.Rows.Count > 0))
                {

                    for (int i = 0; i < dtc.Rows.Count; i++)
                    {
                        // s += word.ToUpper() + " --- " + lang_used + ".IndexOf(" + dtc.Rows[i]["lang"].ToString() + ") " + "fr,en".IndexOf(dtc.Rows[i]["lang"].ToString());

                        if (word == dtc.Rows[i]["lang"].ToString())// lang_used.IndexOf(dtc.Rows[i]["lang"].ToString()) != -1)
                        {
                            val = true;
                        }
                    }

                }
                if (val)
                {

                    dr = dt.NewRow();
                    dr["lang"] = word;
                    dr["lang_status"] = "1";
                    dt.Rows.Add(dr);
                }

                else
                {
                    dr = dt.NewRow();
                    dr["lang"] = word;
                    dr["lang_status"] = "0";
                    dt.Rows.Add(dr);

                }



                s = string.Empty;
            }

            return dt;
        }
        catch (Exception exception)
        {
            Utils.sendEmail("stevemorisseau@hotmail.com", "getlang", exception.ToString());
            return null;
        }
    }

}