﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class login : LoginBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToInt32(Session["name_id"]) > 0)
        {
            lbl_success.Text = "You already in a session, close your session if you want to login to another account";
            txt_user_name.Visible = false;
            txt_user_pwd.Visible = false;
            btnSend.Visible = false;
        }
   
        if ((Request.QueryString["l"] != null) && (Convert.ToChar(Request.QueryString["l"]) == 'f'))
                {
                    lbl_success.Text = "<font color='red'>Your login has expired. Please login again</font>";
                }
             
     
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        string temp_login_user = txt_user_name.Text;
        string temp_login_pwd = txt_user_pwd.Text;

        tiger.security.Conn c = new tiger.security.Conn(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        if (c.isLoginValid(temp_login_user, temp_login_pwd))
        {
            //lbl_success.Text += " Login is valid <br/><br/>";
            // we are counting the number of logged in user
            //Lock the Application using Lock method of application
            Application.Lock();
            //Increment by 1 Application object every time when session starts
            Application["TotalHits"] = (Convert.ToInt32(Application["TotalHits"].ToString()) + 1).ToString();
            //Unlock the Application Object using Unlock() method if Application object
             Application.UnLock();

             //index 0 = schema_id
             //index 1 = schema_manager_id
             //index 2 = login_user
             //index 3 = name_id
             //index 4 = schema_status
             
            string str = c.getLoginInfo(temp_login_user);
             lbl_success.Text += "<br/><br/>"+str+"<br/><br/>";
            if (str.Length == 0)
            {
                lbl_success.Text = "No Status";
            }
            else
            {
                string[] str_result = new string[100];

                //describe which character is separating field
                char[] spliter = { '_' };

                // riding_coord = ridingyear_coord.SelectedItem.Value.Split(spliter);
                str_result = str.Split(spliter);
               // lbl_success.Text += Convert.ToString(str_result[0]) + "_" + Convert.ToString(str_result[1]) + "_" + Convert.ToString(str_result[2]) + "_" + Convert.ToString(str_result[3]) + "_" + Convert.ToString(str_result[4]);
               //check the status of the schema

                string temp_schema_id  = Convert.ToString(str_result[0]);
                string temp_is_manager = Convert.ToString(str_result[1]);
                temp_login_user  = Convert.ToString(str_result[2]);
                string temp_name_id = Convert.ToString(str_result[3]);

                string temp_is_tenant = Convert.ToString(str_result[4]);
                string temp_is_owner = Convert.ToString(str_result[5]);
                string temp_is_janitor = Convert.ToString(str_result[6]);
                string temp_is_pm = Convert.ToString(str_result[7]);
                string temp_fname = Convert.ToString(str_result[9]);

                Session["is_tenant"] = temp_is_tenant;
                Session["is_owner"] = temp_is_owner;
                Session["is_janitor"] = temp_is_janitor;
                Session["is_pm"] = temp_is_pm;
                Session["fname"] = temp_fname;


                string temp_name_id_ip = Request.UserHostAddress.ToString();

                Session["user-agent-win32-win36"] = Request.UserAgent + Request.Browser.Win16.ToString() + Request.Browser.Win32.ToString() + Request.AcceptTypes.ToString() + Request.IsSecureConnection.ToString() + Request.Browser.Browser.ToString() + Request.Browser.Version + Request.UserLanguages;
            
           
                lbl_success.Text += "schema: " + temp_schema_id;
                lbl_success.Text += "<br/>isManager: " + temp_is_manager;
                lbl_success.Text += "<br/>login_user: " + temp_login_user;
                lbl_success.Text += "<br/>temp_name_id: " + temp_name_id;
                lbl_success.Text += "<br/>name_id_ip: " + temp_name_id_ip+"<br/><br/>";
                
                
                switch (Convert.ToString(str_result[8]))
                {
                    case "ON":

                       // lbl_success.Text += " CASE ON <br/><br/>";
                        //let us see if user is account manager 
                        if (Convert.ToInt64(str_result[1]) == Convert.ToInt64(str_result[3]))
                        {
                            //lbl_success.Text += " Is account manager <br/><br/>";
                            //let us make sure that it is in fact the account manager
                            if (Convert.ToInt64(str_result[1]) == Convert.ToInt64(str_result[3]))
                            {
                                lbl_success.Text += Convert.ToInt64(str_result[1]) + " == " + Convert.ToInt64(str_result[3]);
                         
                                Session["schema_id"] = temp_schema_id;
                                Session["is_manager"] = temp_is_manager;
                                Session["login_user"] = temp_login_user;
                                Session["name_id"] = temp_name_id;
                                Session["name_id_ip"] = temp_name_id_ip; 


                                // Session["schema_status"] = Convert.ToString(str_result[4]);
                                lbl_success.Text += "<br/><br/>"+ Convert.ToInt64(Session["scema_id"]) + " == " + Convert.ToInt64(Session["name_id"]);


                                lbl_success.Text += "<br/><br/><br/>schema: " + Session["schema_id"];
                                lbl_success.Text += "<br/>isManager: " + Session["is_manager"];
                                lbl_success.Text += "<br/>login_user: " + Session["login_user"];
                                lbl_success.Text += "<br/>temp_name_id: " + Session["name_id"];
                                lbl_success.Text += "<br/>name_id_ip: " + Session["name_id_ip"] + "<br/><br/>";

                                   Response.Redirect("~/manager/default.aspx");
                            }

                           
                       
                            else
                            {
                                //other module
                              lbl_success.Text += "there seems to be a complication with your role. You have not been mandated to access the MANAGER module";
                            }

                        }
                        else
                        {
                            /// TEST IF NOT MANAGER
                            Session["schema_id"] = temp_schema_id;
                            Session["is_manager"] = "0";
                            Session["login_user"] = temp_login_user;
                            Session["name_id"] = temp_name_id;
                            Session["name_id_ip"] = temp_name_id_ip;


                            if (Session["is_tenant"].ToString() == "1" || Session["is_pm"].ToString() == "1"
                                 || Session["is_janitor"].ToString() == "1" || Session["is_owner"].ToString() == "1")
                                Response.Redirect("~/user/name_profile.aspx");
                            else
                            {
                                Session.Abandon();
                            }
                            //other module
                            //find the roles and services that user has access by fetching the database
                        }
                        break;
                    case "PD":
                        lbl_success.Text += "your status is pending, you must activate your account";
                        break;
                    case "OF":
                        lbl_success.Text += "your account has been close";
                        break;
                }
 
            }
        }
        else
        {
            lbl_success.Text += "Login failed";
        }

    }
    protected void Language_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void txt_user_pwd_TextChanged(object sender, EventArgs e)
    {

    }
}
