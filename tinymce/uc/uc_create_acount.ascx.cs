using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Mail;

public partial class uc_create_acount : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void bn_create_Click(object sender, EventArgs e)
    {
        //insert home
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
        // resulta.InnerHtml = "home name : " + home_name.Text;
      
        
            SqlConnection conn = new SqlConnection(strconn);
            SqlCommand cmd = new SqlCommand("prCreateAccount", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                conn.Open();
                //Add the params
                cmd.Parameters.Add("@return", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add("@name_lname", SqlDbType.VarChar, 50).Value = name_lname.Text.ToString();
                cmd.Parameters.Add("@name_fname", SqlDbType.VarChar, 50).Value = name_fname.Text.ToString();
                cmd.Parameters.Add("@name_email", SqlDbType.VarChar, 75).Value = name_email.Text.ToString();
                cmd.Parameters.Add("@login_pwd", SqlDbType.VarChar, 25).Value = login_pwd.Text.ToString();
                cmd.Parameters.Add("@group_id", SqlDbType.Int).Value = 1;
                cmd.Parameters.Add("@schema_activation_code", SqlDbType.VarChar, 25).Value = tiger.RandomPassword.Generate(15, 20);
                //execute the insert
                cmd.ExecuteReader();
                
                switch(Convert.ToInt32(cmd.Parameters["@return"].Value))
                {
                    case 0:
                
                string str_to = name_email.Text;
                string str_from = "info@sinfoca.com";
                string str_subject = "tiger real estate project validation";
                string str_body = "Thank you for choosivng tiger real estate click <a href='http://76.163.235.120/validate_account.aspx?email=?email="+name_email.Text+"'>here</a> to validate account";
                SmtpMail.Send(str_from, str_to, str_subject,str_body);

                txt.Text = "Your account has been created. An email has been sent to validate your account";
                break;
                    case 1:
                txt.Text = "Erreur";
                break;
                    case 2:
                        txt.Text = "<span class='letter_red'>User is already being used, click <a href='forget_pwd.aspx?email="+name_email.Text+"' class='letter_link'>here</a>if you have forgotten your email.activation code</span>";
                        break;
               }
            }
            catch (Exception error)
            {
                tiger.security.Error.errorAdd(strconn, Convert.ToInt32(Session["schema_id"]), DateTime.Now, "home_add.aspx", error.ToString());
                txt.Text = "Insert did not succeed! <br>" + error.ToString();
            }
    }
}
