﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="globaltest.aspx.cs" Inherits="globaltest" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
   
<asp:Label ID="lbl_project_name" runat="server" 
            Text="<%$ Resources:Resource, lbl_project_name %>"></asp:Label><br />
 
  <asp:DropDownList ID="Language1" runat="server" AutoPostBack="True">
            <asp:ListItem Value="auto">Auto</asp:ListItem>
            <asp:ListItem Value="en-US">English (US)</asp:ListItem>
            <asp:ListItem Value="en-GB">English (UK)</asp:ListItem>
            <asp:ListItem Value="de">German</asp:ListItem>
            <asp:ListItem Value="fr-FR">French</asp:ListItem>
            <asp:ListItem Value="fr-CA">French (Canada)</asp:ListItem>
            <asp:ListItem Value="hi">Hindi</asp:ListItem>
            <asp:ListItem Value="th">Thai</asp:ListItem>
        </asp:DropDownList>
  
    </form>
</body>
</html>
