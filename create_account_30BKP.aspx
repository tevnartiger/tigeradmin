<%@ Page Language="C#" MasterPageFile="~/manager/MasterPage3.master" AutoEventWireup="true" CodeFile="create_account_30BKP.aspx.cs" Inherits="create_account" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<h1 >Create account</h1>
 <div style="width:100%;margin-left:10px; background-color:#fff;color:#000;" class="rounded">

<div style="width:50%; text-align:right;">
    
First name&nbsp;
<asp:TextBox MaxLength="50"  Width="150" ID="name_fname" runat="server" /><br />
<asp:RequiredFieldValidator Display="Dynamic"  ControlToValidate="name_fname" runat="server" ErrorMessage="<%$Resources:resource, iv_required %>" />
<asp:RegularExpressionValidator Display="Dynamic" ID="reg_name_fname" ControlToValidate="name_fname" runat="server" ErrorMessage="<%$Resources:resource, iv_invalid_char %>" />
<br /> 

Last name&nbsp;<asp:TextBox MaxLength="50" Width="150"  ID="name_lname" runat="server" /><br />
<asp:RequiredFieldValidator Display="Dynamic" ControlToValidate="name_lname" runat="server" ErrorMessage="Field cannot be empty" />
<asp:RegularExpressionValidator Display="dynamic" ID="reg_name_lname" ControlToValidate="name_lname" runat="server" />
<br />

E-mail&nbsp;
 <asp:TextBox MaxLength="50" width="150"  ID="name_email" runat="server" /><br />
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic"  ControlToValidate="name_email" runat="server" ErrorMessage="<%$Resources:resource, iv_required %>" />
<asp:RegularExpressionValidator Display="Dynamic" ID="reg_name_email" ControlToValidate="name_email" runat="server" ErrorMessage="<%$Resources:resource, iv_invalid_email %>" />
<br />
<br />
<br />
User name&nbsp;<asp:TextBox MaxLength="50" Width="150"  ID="login_user" runat="server" /><br />
<asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" ControlToValidate="login_user" runat="server" ErrorMessage="Field cannot be empty" />
<asp:RegularExpressionValidator Display="dynamic" ID="reg_login_user" ControlToValidate="login_user" runat="server" />
<br />

 
Password&nbsp;
<asp:TextBox MaxLength="50" Width="150" TextMode="password" ID="login_pwd" runat="server" /><br />
<asp:RequiredFieldValidator Display="Dynamic"  ControlToValidate="login_pwd" runat="server" ErrorMessage="Field cannot be empty" />
<asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="login_pwd" ID="reg_login_pwd" runat="server" ErrorMessage="<%$Resources:resource, iv_invalid_char %>" />
<br /> 
Re-type Password&nbsp;<asp:TextBox MaxLength="50" Width="150" TextMode="password" ID="retype_login_pwd" runat="server" /><br />
 <asp:CompareValidator ControlToCompare="login_pwd" Display="Dynamic" ControlToValidate="retype_login_pwd" ID="CompareValidator1" runat="server" ErrorMessage="CompareValidator" />
<asp:RequiredFieldValidator  ControlToValidate="retype_login_pwd" runat="server" ErrorMessage="Field cannot be empty" /> 
<asp:RegularExpressionValidator ControlToValidate="retype_login_pwd" Display="Dynamic" ID="reg_retype_login_pwd" runat="server" ErrorMessage="<%$Resources:resource, iv_invalid_char %>" /></td></tr>
<br />
<asp:Button ID="bn_create" runat="server" Text="Create Account" OnClick="bn_create_Click" /> 
<br /><asp:Label ID="txt" runat="server" /> 

</div>
</div>
</asp:Content>

