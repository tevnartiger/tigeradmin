using System;

public partial class AlertMessageUserControl : System.Web.UI.UserControl
{
    protected string text;

    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    public void ShowError(string message)
    {
        ErrorAlert.Visible = true;
        InfoAlert.Visible = false;
        WarningAlert.Visible = false;
        SuccessAlert.Visible = false;
        NormalAlert.Visible = false;
        text = message;
    }

    public void ShowInfo(string message)
    {
        ErrorAlert.Visible = false;
        InfoAlert.Visible = true;
        WarningAlert.Visible = false;
        SuccessAlert.Visible = false;
        NormalAlert.Visible = false;
        text = message;
    }

    public void ShowWarning(string message)
    {
        ErrorAlert.Visible = false;
        InfoAlert.Visible = false;
        WarningAlert.Visible = true;
        SuccessAlert.Visible = false;
        NormalAlert.Visible = false;
        text = message;
    }

    public void ShowSuccess(string message)
    {
        ErrorAlert.Visible = false;
        InfoAlert.Visible = false;
        WarningAlert.Visible = false;
        SuccessAlert.Visible = true;
        NormalAlert.Visible = false;
        text = message;
    }

    public void ShowNormal(string message)
    {
        ErrorAlert.Visible = false;
        InfoAlert.Visible = false;
        WarningAlert.Visible = false;
        SuccessAlert.Visible = false;
        NormalAlert.Visible = true;
        text = message;
    }
    public void Hide()
    {
        ErrorAlert.Visible = false;
        InfoAlert.Visible = false;
        WarningAlert.Visible = false;
        SuccessAlert.Visible = false;
        NormalAlert.Visible = false;
        text = "";
    }
}
