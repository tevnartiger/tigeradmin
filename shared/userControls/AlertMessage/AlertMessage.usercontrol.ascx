<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AlertMessage.usercontrol.ascx.cs" Inherits="AlertMessageUserControl" %>

<div  runat="server" ID="ErrorAlert" visible="false" class="alertMessage error"><%= this.text %></div>
<div  runat="server" ID="InfoAlert" visible="false" class="alertMessage info"><%= this.text %></div>
<div  runat="server" ID="WarningAlert" visible="false" class="alertMessage warning"><%= this.text %></div>
<div  runat="server" ID="SuccessAlert" visible="false" class="alertMessage success"><%= this.text %></div>
<div  runat="server" ID="NormalAlert" visible="false" class="alertMessage normal"><%= this.text %></div>
