﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_mobileapp_menu.ascx.cs" Inherits="uc_uc_mobileapp_menu" %>

<asp:Repeater ID="r_categ" runat="server">
  <ItemTemplate>
     <li><a href="#"><%# DataBinder.Eval(Container.DataItem, "sc_name")%></a>
      <ul>
         <asp:Repeater ID="r_list" DataSource='<%#ServiceCategList(Convert.ToString(Eval("sc_id")))%>' runat="server">
         <ItemTemplate>   
		    <li><a href="<%# DataBinder.Eval(Container.DataItem, "service_url")%>"><%# DataBinder.Eval(Container.DataItem, "service_title")%></a></li>
	     </ItemTemplate>
         </asp:Repeater>
      </ul>
    </li>	
  </ItemTemplate>
</asp:Repeater>

