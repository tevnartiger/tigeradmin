using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Codeplex.Data;

public partial class uc_uc_dash_wpm : System.Web.UI.UserControl
{

    protected string str, str2, str3, str4, str5, str6, str7, str8, str9;

    protected void Page_Load(object sender, EventArgs e)
    {
        str = sinfoca.googlegraph.pieChart("chart_div", "Registration", GetTablePie(), "100%", "100%");
        str2 = sinfoca.googlegraph.areaChart("chart_div2", "Comparisons: current year vs Last year", "Comparisons", "2011", "2012", GetTableCompare(), "100%", "100%");
        str3 = sinfoca.googlegraph.barChart("chart_div3", "Yearly 5 Report", "Sales/Expenses", "Sales", "Expenses", GetTableArea(), "100%", "100%");
        str4 = sinfoca.googlegraph.geoChart("chart_div4", "most spectacular color", GetTableco(), "100%", "100%");
        str5 = sinfoca.googlegraph.lineChart("chart_div5", "2011-2012", "Sales/Expenses", "2011", "2011", GetTableArea(), "100%", "100%");
        str6 = sinfoca.googlegraph.columnChart("chart_div6", "Yearly 2 Report", "Sales/Expenses", "Sales", "Expenses", GetTableArea(), "100%", "100%");
        str7 = sinfoca.googlegraph.geoChart("chart_div7", "most spectacular color", GetTableco(), "100%", "100%");
        str8 = sinfoca.googlegraph.pieChart("chart_div8", "most spectacular color", GetTablePie(), "100%", "100%");
        str9 = sinfoca.googlegraph.lineChart("chart_div9", "2012", "Sales/Expenses", "Sales", "Expenses", GetTableMonth(), "100%", "100%");


        sinfoca.tiger.Person person = new sinfoca.tiger.Person();

        DataTable dt3 = person.getPersonView(Convert.ToInt32(Session["person_id"]));
        var statuses3 = DynamicJson.Parse(Utils.GetJSONString(dt3));

        lbl_wpm.Text = Session["person_name"].ToString();
        lbl_tel.Text = statuses3.personview[0].cell.ToString();
        lbl_email.Text = statuses3.personview[0].login_user.ToString();


        Company cp = new Company();
        DataTable dt = new DataTable();
        dt = cp.getWMWorkPlaceList();


        if (!IsPostBack)
        {
            ddlWorkplace.DataSource = cp.getWMWorkPlaceList();
            ddlWorkplace.DataValueField = "wp_id";
            ddlWorkplace.DataTextField = "wp_name";
            ddlWorkplace.DataBind();
            ddlWorkplace.Items.Insert(0, new ListItem("-- select a location ---", "0"));

        }
        


        if (dt.Rows.Count == 1)
        {
            lbl_company.Text = dt.Rows[0]["wp_name"].ToString();
        }
        else
        {
            foreach (DataRow row in dt.Rows)
            {
                Session["company_name"] = row["company_name"].ToString();
            }
            lbl_company.Text = Session["company_name"].ToString();

          
        }

        DataTable dtcomp = new DataTable();
        dtcomp = cp.getCompanyView(Convert.ToInt32(Session["schema_id"]),Convert.ToInt32(ddlWorkplace.Items[1].Value));
        foreach (DataRow row in dtcomp.Rows)
        {
            lbl_address.Text = row["address_street"].ToString() + " , " + row["address_pc"].ToString();

        }

      
       

    }


    static DataTable GetTablePie()
    {
        //
        // Here we create a DataTable with four columns.
        //
        DataTable table = new DataTable("a");
        table.Columns.Add("Color", typeof(string));
        table.Columns.Add("total", typeof(int));

        //
        // Here we add five DataRows.
        //
        table.Rows.Add("Active DishWasher", 320);
        table.Rows.Add("Dishwasher on probation", 38);
        table.Rows.Add("Dishwasher Pending activation", 2143);
        /*    table.Rows.Add("green", 1933);
            table.Rows.Add("purple", 4343);
            table.Rows.Add("pink", 343);
            table.Rows.Add("orange", 2143);
            table.Rows.Add("black", 1933);*/
        return table;
    }

    static DataTable GetTableco()
    {
        //
        // Here we create a DataTable with four columns.
        //
        DataTable table = new DataTable("a");
        table.Columns.Add("Color", typeof(string));
        table.Columns.Add("total", typeof(int));

        //
        // Here we add five DataRows.
        //
        table.Rows.Add("Germany", 4343);
        table.Rows.Add("United States", 343);
        table.Rows.Add("Brazil", 2143);
        table.Rows.Add("Canada", 1933);
        table.Rows.Add("France", 4343);
        table.Rows.Add("RU", 343);

        return table;
    }
    static DataTable GetTableArea()
    {
        //
        // Here we create a DataTable with four columns.
        //
        DataTable table = new DataTable("a");
        table.Columns.Add("Year", typeof(string));
        table.Columns.Add("Sales", typeof(int));
        table.Columns.Add("Expenses", typeof(int));

        //
        // Here we add five DataRows.
        //
        table.Rows.Add("2009", 125989, 65876);
        table.Rows.Add("2010", 258000, 165400);
        table.Rows.Add("2011", 365000, 230988);
        table.Rows.Add("2012", 16000, 76688);
        return table;
    }

    static DataTable GetTableMonth()
    {
        //
        // Here we create a DataTable with four columns.
        //
        DataTable table = new DataTable("a");
        table.Columns.Add("Months", typeof(string));
        table.Columns.Add("Sales", typeof(int));
        table.Columns.Add("Expenses", typeof(int));

        //
        // Here we add five DataRows.
        //
        table.Rows.Add("jan", 10433, 6876);
        table.Rows.Add("feb", 258000, 165400);
        table.Rows.Add("march", 365000, 230988);
        table.Rows.Add("april", 16000, 76688);
        table.Rows.Add("may", 125989, 65876);
        table.Rows.Add("june", 258000, 165400);
        table.Rows.Add("july", 365000, 230988);
        table.Rows.Add("august", 16000, 76688);
        table.Rows.Add("sept", 125989, 65876);
        table.Rows.Add("oct", 258000, 165400);

        return table;
    }

    static DataTable GetTableCompare()
    {
        //
        // Here we create a DataTable with four columns.
        //
        DataTable table = new DataTable("a");
        table.Columns.Add("Months", typeof(string));
        table.Columns.Add("2011", typeof(int));
        table.Columns.Add("2012", typeof(int));

        //
        // Here we add five DataRows.
        //
        table.Rows.Add("jan", 10433, 6876);
        table.Rows.Add("feb", 14043, 19089);
        table.Rows.Add("march", 65000, 40988);
        table.Rows.Add("april", 16000, 22688);
        table.Rows.Add("may", 23989, 35876);
        table.Rows.Add("june", 48000, 55400);
        table.Rows.Add("july", 65000, 70988);
        table.Rows.Add("august", 45000, 67688);
        table.Rows.Add("sept", 34989, 65876);
        table.Rows.Add("oct", 38000, 22400);
        table.Rows.Add("nov", 38000, 0);
        table.Rows.Add("dec", 68000, 0);

        return table;
    }

}

