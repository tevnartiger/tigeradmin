<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_login.ascx.cs" Inherits="uc_uc_login" %>
  <div style="width:100%;margin-left:10px; background-color:1a3b69;" class="rounded">
   
  <table cellpadding="0" align="center" cellspacing="0" style="padding:4px;border:0px solid #000;margin:0px;padding:0px;">
 <!--<tr style="background:#3300E1;height:40px;"><td><div style="float:left"><img src="../logo.gif" /></div></td></tr>
<tr><td style="height:4px;background:#3300E1"></td></tr>
 <tr>
 <td>  
  <asp:Label CssClass="white" ID="lbl_text" runat="server" Text="<%$ Resources:Resource,lbl_login_text %>" /> <br /><br />
  </td>
 </tr>
 <tr><td style="height:4px;background:#3300E1"></td></tr>
-->
  <tr><td>
 <table  cellpadding="2" cellspacing="2" style="width:100%;border:0;background:1a3b69">
 <tr><td align="left">
 <asp:Label ID="lbl_user" runat="server" CssClass="white" Text="<%$ Resources:Resource, lbl_user %>"/><br />
 <asp:TextBox  MaxLength="50" Width="200" text="steve@tiger.com" ID="txt_user_name" runat="server" />
<br /><asp:RequiredFieldValidator runat="server" Display="dynamic" ID="sa" ControlToValidate="txt_user_name" Text="<%$ Resources:resource, iv_required %>" />
 <br />
 <asp:Label ID="lbl_password" CssClass="white" runat="server" Text="<%$ Resources:Resource, lbl_password %>" />
 <br />
 <asp:TextBox TextMode="Password"  MaxLength="25" Width="200" ID="txt_user_pwd" runat="server" />
        <br />
        <asp:RequiredFieldValidator runat="server" Display="dynamic" ID="RequiredFieldValidator1" ControlToValidate="txt_user_pwd" Text="<%$ Resources:resource, iv_required %>" />
<br />
<asp:Label ID="Label1" runat="server" CssClass="white" Text="<%$ Resources:Resource, lbl_gl_language %>" />
<br />
 <asp:DropDownList ID="Language" Width="200" runat="server" 
         CssClass="lang" AutoPostBack="True" 
         onselectedindexchanged="Language_SelectedIndexChanged">
            <asp:ListItem Value="en-US">English (US)</asp:ListItem>
            <asp:ListItem Value="fr-FR">Francais</asp:ListItem> 
        </asp:DropDownList> </td>      </tr>     
 <tr><td colspan="2" align="right"><asp:button  ID="btnSend" Text="Send" runat="server" OnClick="btnSend_Click" />
      <br /> <asp:Label ID="lbl_success" CssClass="white" runat="server" />
      
      <br /><br />
      <asp:HyperLink ID="dsa" runat="server"  CssClass="white" NavigateUrl="/forgot_password.aspx" Text="<%$ Resources:Resource,lbl_forget_password %>"/>
 </td></tr>
<tr><td colspan="2" align="left"><asp:Label CssClass="white" ID="status" runat="server"  /> </td></tr>
</table>
</td></tr>
 </table> 
   </div>