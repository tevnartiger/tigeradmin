using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class uc_uc_login : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Convert.ToInt32(Session["name_id"]) > 0)
        {
            lbl_success.Text = "You already in a session, close your session if you want to login to another account";
            txt_user_name.Visible = false;
            txt_user_pwd.Visible = false;
            btnSend.Visible = false;
        }
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        string temp_login_user = txt_user_name.Text;
        string temp_login_pwd = txt_user_pwd.Text;

        tiger.security.Conn c = new tiger.security.Conn(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
        if (c.isLoginValid(temp_login_user, temp_login_pwd))
        {

            // we are counting the number of logged in user


            //Lock the Application using Lock method of application

            Application.Lock();

            //Increment by 1 Application object every time when session starts

            Application["TotalHits"] = (Convert.ToInt32(Application["TotalHits"].ToString()) + 1).ToString();

            //Unlock the Application Object using Unlock() method if Application object

            Application.UnLock();


            //index 1 = user_schema
            //index 2 = module_id
            //index 3 = login_name
            //index 4 = name_id
            //index 5 = schema_status
            
            string str = c.getLoginInfo(temp_login_user);
            //response.Text = str;
            if (str.Length == 0)
            {
                lbl_success.Text = "No Status";
            }
            else
            {
                string[] str_result = new string[100];

                //describe which character is separating field
                char[] spliter = { '_' };

                // riding_coord = ridingyear_coord.SelectedItem.Value.Split(spliter);
                str_result = str.Split(spliter);
                lbl_success.Text = Convert.ToString(str_result[0]) + "_" + Convert.ToString(str_result[1]) + "_" + Convert.ToString(str_result[2]) + "_" + Convert.ToString(str_result[3]) + "_" + Convert.ToString(str_result[4]) + "_" + Convert.ToString(str_result[5]); //"Login failed";

                //check if session status first
                string schema_status = "NA";
                if (Convert.ToInt32(str_result[0]) > 0)
                {
                    schema_status = Convert.ToString(str_result[5]);
                }
                switch (schema_status)
                {

                    case "ON":
                        if (Convert.ToInt32(str_result[0]) == 1)
                        {
                            lbl_success.Text = "inside on <br/>";
                            lbl_success.Text = "GOOD";
                            //index 0 = the connection result bool 1 success 0 failed

                            //index 1 = user_schema
                            //index 2 = module_id
                            //index 3 = login_name
                            //index 4 = name_id
                            //index 5 = schema_status
                            //"1" + "_" + Convert.ToString(dr["schema_id"]) + "_" + Convert.ToString(dr["login_categ"]) + "_" + Convert.ToString(dr["tname"]) + "_" + Convert.ToString(dr["name_id"]);


                            Session["schema_id"] = Convert.ToString(str_result[1]);
                            Session["module_id"] = Convert.ToString(str_result[2]);
                            Session["login_name"] = Convert.ToString(str_result[3]);
                            Session["name_id"] = Convert.ToString(str_result[4]);
                            Session["schema_status"] = Convert.ToString(str_result[5]);

                            //lbl_success.Text = " Schema_id "+Session["schema_id"]+"<br>module_id "+Session["module_id"];
                            //every thing is good "+ Session["schema_id"]+"  "+Session["module_id"]+"  "+Session["user_name"];
                            // Response.Redirect("home/home_main.aspx");
                            //Redirect with module_id
                            switch (Convert.ToString(str_result[2]))
                            {
                                case "1":
                                    Response.Redirect("manager/alerts/alerts.aspx");
                                    break;
                                case "2":
                                    Response.Redirect("owner/home/home_main.aspx");
                                    break;
                                case "3":
                                    Response.Redirect("tenant/tenant/tenant_main.aspx");
                                    break;
                                case "4":
                                    Response.Redirect("owner/owner_main.aspx");
                                    break;


                            }
                        }
                        else
                        {
                            // response.Text = "ACCESS DENIED";
                            lbl_success.Text = "Account Expired";
                        }
                        //   lbl_success.Text = Convert.ToInt32(str_result[0])+ "  Did not succeed "+Session["schema_id"] + ", " + Session["module_id"] + ", " + Session["user_name"];
                        break;
                    case "PD":
                        lbl_success.Text = "<span class='letter_small'>You must activate your account! Check your email or click <a href='activate_account.aspx?login_user=" + txt_user_name.Text + "' class='letter_link'>here</a> to recieve another email</span>-->" + tiger.RandomPassword.getActivationCode(22).ToString();
                        break;
                    default:
                        lbl_success.Text = "Login failed";
                        break;
                }
            }
        }
        else
        {
            lbl_success.Text = "Login failed";
        }
    
    }
    protected void Language_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}
