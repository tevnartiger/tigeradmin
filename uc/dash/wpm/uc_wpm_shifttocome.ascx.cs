﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class uc_dash_wpm_uc_wpm_shifttocome : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Orders tfc = new Orders();
        int list = tfc.getWMValidationShiftCount(100, 0, 0, 0, 0, 4);

        sp_validate.InnerText = list.ToString();


        Company cp = new Company();
      
        if (cp.getWMWorkPlaceList().Rows.Count == 1)
        {
            td_location.Visible = false;
            th_location.Visible = false;
        }
    }
}