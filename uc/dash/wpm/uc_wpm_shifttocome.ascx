﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_wpm_shifttocome.ascx.cs" Inherits="uc_dash_wpm_uc_wpm_shifttocome" %>

<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/normalize/3.0.1/normalize.css" />
<style type="text/css">
fieldset {
     border: 0;
}
</style>

<style>
    @media 
    only screen and (max-width: 760px),
    (min-device-width: 768px) and (max-device-width: 1024px)  {

/*
Label the data
*/
 .wpm_table td:nth-of-type(1):before { content: "<%= Resources.orders.orders_shift_id %>"; }
 .wpm_table td:nth-of-type(2):before { content: "<%= Resources.booking.booking_date%>"; }
 .wpm_table td:nth-of-type(3):before { content: "<%= Resources.orders.orders_start_time%>"; }
 .wpm_table td:nth-of-type(4):before { content: "<%= Resources.orders.orders_end_time%>"; }
 .wpm_table td:nth-of-type(5):before { content: "<%= Resources.orders.orders_location %>"; }
 .wpm_table td:nth-of-type(6):before { content: "<%= Resources.orders.orders_job %>"; }
 .wpm_table td:nth-of-type(7):before { content: "<%= Resources.Resource.lbl_tag_employee %>"; }
}


</style>
<script src="../../js/date-<%= Session["_lastCulture"].ToString()%>.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="/js/jsrender.js"></script>
<script language="javascript" type="text/javascript" src="/js/json2.js"></script>
<script type="text/javascript"  src="/js/jquery.pagination.js"></script>
<script type="text/javascript"  src="/js/bootstrap.js"></script>
<script language="javascript" type="text/javascript" src="/js/querystring-0.9.0-min.js"></script>
    <script src="../../js/formatDatejs.js" type="text/javascript"></script>
<link rel="stylesheet" href="/css/pagination.css" />

 <script type="text/javascript">
     // When document has loaded, initialize pagination and form
     $(function () {
         $('#tab2 a').click(function (e) {
             e.preventDefault();
             $('a[href="' + $(this).attr('href') + '"]').tab('show');
         })
     });
     $(document).ready(function () {
         //--------------------------------------------------------------------------------------------------------------
         /////////////////////////////////////////     ONChange Company      //////////////////////////////////////////////////////////////////////

         $("#ddlWorkplace").change(function () {

             $.get("/uc/dash/wpm/ProcessData.aspx", { ag: "10", bg: 0, WPID: $("#ddlWorkplace option:selected").val(), fct: "wmshiftcount" },
                     function (data) {
                         $(" #sp_current_count").text(data);
                         $("#paginator").pagination(data, { items_per_page: 10, callback: handlePaginationClick });
                     });

         });

     })
    </script>  




    <script type="text/javascript">


        /**
        * Callback function that displays the content.
        *
        * Gets called every time the user clicks on a pagination link.
        *
        * @param {int}page_index New Page index
        * @param {jQuery} jq the container with the pagination links as a jQuery object
        */
        function handlePaginationClick(page_index, jq) {
            var item_page = 10;
            //alert("handle Pagination");

            var language = $.QueryString("language");
            language = (!language) ? "<%=Session["_lastCulture"].ToString()%>" : language; // Passing the value null to string;  

            //  $("#h_wp_id").val(WPID)
            //  $("#h_Company_id").val(CompanyID)
            $("#details2").html("");

            $.get("/uc/dash/wpm/ProcessData.aspx", { ag: "10", bg: page_index * item_page, language: language, WPID: $("#ddlWorkplace option:selected").val(), fct: "wmshiftlist" },
                   function (data) {
                       var mydata = jQuery.parseJSON(data);
                       // Render to string
                       $.views.helpers({
                           status_date: function (val) {
                               var split = val.split('/');
                               var month1 = split[0];
                               var day1 = split[1];
                               var year1 = split[2];


                               var date2;
                               var format = Date.CultureInfo.formatPatterns.shortDate;

                               date2 = getFormat("<%= Session["_lastCulture"].ToString()%>", split[0], split[1], split[2]);
                               date = Date.parse(date2, format);
                               return date.toString("ddd, MMMM dd, yyyy");
                           }
                       });

                       var html2 = $("#appTmpl2").render(mydata);
                       // Insert as HTML
                       $("#details2").html(html2);
                       //disable checkbox if no cell number
                   });

                   return false;
               }


               // The form contains fields for many pagiantion optiosn so you can
               // quickly see the resuluts of the different options.
               // This function creates an option object for the pagination function.
               // This will be be unnecessary in your application where you just set
               // the options once.


               // When document has loaded, initialize pagination and form
               $(document).ready(function () {
                   // alert("document ready");
                   // Create pagination element with options from form
                   $.get("/uc/dash/wpm/ProcessData.aspx", { ag: "10", bg: "0", WPID: $("#h_wp_id").val(), fct: "wmshiftcount" },
                            function (data) {
                                //  alert($("#ddlWorkplace option:selected").val()); 
                                $(" #sp_current_count").text(data);
                                $("#paginator").pagination(data, { items_per_page: 10, callback: handlePaginationClick });
                            });


                   $('#myTab a').click(function (e) {
                       e.preventDefault();
                       $(this).tab('show');
                   })

                   $('a[data-toggle="tab"]').on('shown', function (e) {
                       e.target // activated tab
                       e.relatedTarget // previous tab
                   })


               });

        </script>

<asp:Panel ID="panel_default" runat="server">
     <div class="header-title">
<h2 class="secondary-color"><%= Resources.Resource.talent_tocome2   %></h2>
 
     
</div><span class="label lbl_reserve_talent label-success"><a href="/c/orders_wpm/order.aspx?p=add_bv2" > <%= Resources.orders.orders_reserve_talent %></a></span>
 

 </asp:Panel>
    

 <asp:Panel ID="p_shortcut" runat="server">
     <div id="div_shorcuts" runat="server" class="row-fluid shortcuts">
     <div class="span12">
                      
     <span class="label lbl_tocome label-inverse"><a href="default.aspx" > <%= Resources.Resource.talent_tocome2 %>&nbsp;(<span id="sp_current_count" >0</span>)</a></span>
     <span class="label lbl_workedhours"><a href="/c/validate_wp/validate.aspx"><%= Resources.Resource.validation_hours_worked %>&nbsp;(<span id="sp_validate" runat="server" >0</span>)</a></span>
     <span class="label lbl_bills"><a href="/c/billing_wpm/billing.aspx"><%= Resources.Resource.compt_bills %></a></span>
     <span class="label lbl_archive"><a href="/c/billing_wpm/billing.aspx?p=closed"><%= Resources.Resource.compt_archive%></a></span>
     </div>

     </div>
 </asp:Panel>
 <div class="wpm_table">
<div id="myTabContent" class="tab-content">
  <div class="tab-pane active" id="home" >
   
  
    
<!--====== Template ======-->
 
        <script id="appTmpl2" type="text/x-jquery-tmpl">
        <tr id="{{>appointment_id}}" >
         <td>{{>appointment_id}}</td>
         <td>{{>~status_date(appointment_date3)}}</td>
          <td>{{>min_to_start_time}}</td>
         <td>{{>min_to_end_time}}</td>
         <td id="td_location" runat="server">{{>wp_name}}</td>
         <td>{{>jc_name}}</td>
         <td>{{>person_name}} </td>
         
         </tr>
        </script>
    
     <!--====== Container ======-->
           <div class="wpm_table">      
        <table class="table table-bordered table-condensed table-hover">
        <thead>
        <tr>
         <th><strong><%= Resources.orders.orders_shift_id %></strong></th>
            <th><strong><%= Resources.booking.booking_date%></strong></th>
          <th><strong><%= Resources.orders.orders_start_time%></strong></th>
          <th><strong><%= Resources.orders.orders_end_time%></strong></th>
         <th id="th_location" runat="server" ><strong><%= Resources.orders.orders_location %></strong></th>
          <th><strong><%= Resources.orders.orders_job %></strong></th>
          <th><strong><%= Resources.Resource.lbl_tag_employee %></strong></th>
         
          </tr>
        </thead>
        <tbody id="details2"></tbody>
        </table>

        <div id="paginator"  style="padding-top:15px;" class="pagination2"></div>
    </div>




  </div>
 </div>

 <div>
</div>
        <br />
       
       
        <div id="h_ids"></div>
        <div id="h_wp_id"></div>
        <div id="h_Company_id"></div>
        <div id="h_category_id"></div>
    
     </div>  

