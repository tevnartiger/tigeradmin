﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using sinfoca.login;

public partial class ProcessData : BasePageLite
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Clear();
        Response.ClearContent();

        List<string> queryStringList = new List<string>();
        queryStringList.Add("wmshiftcount");
        //queryStringList.Add("ordershiftcount");
        //queryStringList.Add("ordershiftlist");
        queryStringList.Add("workplace");
        queryStringList.Add("completed");
        queryStringList.Add("wmshiftlist");
        queryStringList.Add("select");
        queryStringList.Add("city");
        queryStringList.Add("zone");
        queryStringList.Add("zone2");
        queryStringList.Add("category");

        queryStringList.Add("list");
        queryStringList.Add("count");
        queryStringList.Add("region");
       


        int company_id = 0;
        int order_id = 0;
        int category_id = 0;
        int wp_id = 0;
        int shift_id = 0;
        int person_id = 0;
        int zone_id = 0;
        int fc_id = 0;

        string search = "";

        int a = 0;
        int b = 0;

        string lang = Session["_lastCulture"].ToString().Substring(0, 2);
        if (!string.IsNullOrEmpty(Request.QueryString["language"]) && config.queryLangList().Contains(Request.QueryString["language"].Substring(0, 2)))
            lang = Request.QueryString["language"].Substring(0, 2);

        string SEARCH = Request.QueryString["t"];
        if (SEARCH != null && RegEx.IsText(SEARCH))
        {
            search = SEARCH;
        }


        string CompanyID = Request.QueryString["CompanyID"];
        if (CompanyID != null && RegEx.IsInteger(CompanyID))
        {
            company_id = Convert.ToInt32(CompanyID);
        }

        string PersonID = Request.QueryString["PersonID"];
        if (PersonID != null && RegEx.IsInteger(PersonID))
        {
            person_id = Convert.ToInt32(PersonID);
        }

        string OrderID = Request.QueryString["OrderID"];
        if (OrderID != null && RegEx.IsInteger(OrderID))
        {
            order_id = Convert.ToInt32(OrderID);
        }


        string ShiftID = Request.QueryString["ShiftID"];
        if (ShiftID != null && RegEx.IsInteger(ShiftID))
        {
            shift_id = Convert.ToInt32(ShiftID);
        }

        string WpID = Request.QueryString["WPID"];
        if (WpID != null && RegEx.IsInteger(WpID))
        {
            wp_id = Convert.ToInt32(WpID);
        }

        string FC = Request.QueryString["fc_id"];
        if (FC != null && RegEx.IsInteger(FC))
        {
            fc_id = Convert.ToInt32(FC);
        }

          string ZoneID = Request.QueryString["ZoneID"];
        if (ZoneID != null && RegEx.IsInteger(ZoneID))
        {
            zone_id = Convert.ToInt32(ZoneID);
        }

        string categoryID = Request.QueryString["categoryID"];
        if (categoryID != null && RegEx.IsInteger(categoryID))
        {
            category_id = Convert.ToInt32(categoryID);
        }


        if (Request.QueryString["ag"] != null && RegEx.IsInteger(Request.QueryString["ag"]))
        {
            a = Convert.ToInt32(Request.QueryString["ag"]);
        }

        if (Request.QueryString["bg"] != null && RegEx.IsInteger(Request.QueryString["bg"]))
        {
            b = Convert.ToInt32(Request.QueryString["bg"]);
        }

        if (queryStringList.Contains(Request.QueryString["fct"]))
        {
            switch (Request.QueryString["fct"])
            {

                case "list": Response.Write(PersonList(a, b,shift_id));
                    break;

                case "select": Response.Write(PersonSelect(person_id,shift_id));
                    break;

                case "completed": Response.Write(AppointmentCompleted(shift_id));
                    break;

                case "count": Response.Write(PersonCount(a, b, shift_id));
                    break; 

                case "wmshiftcount": Response.Write(WMShiftCount(a, b, company_id, wp_id,category_id));
                    break;

                case "workplace": Response.Write(WorkPlaceList(company_id));
                    break;


                case "wmshiftlist": Response.Write(WMShiftList(a, b, company_id, wp_id, category_id,lang));
                    break;

                   
                case "city": Response.Write(CityList(wp_id));
                    break;

                case "category": Response.Write(CategoryList());
                    break;

            }
            Response.End();
        }



        if (Request.Form["ag"] != null && Request.Form["bg"] != null && Request.Form["cg"] != null && Request.Form["fct"] != null)
        {

            string sms = Request.Form["ag"].ToString().Replace(" ","+");
            string email = Request.Form["bg"].ToString();
            string person_sms_emailList = Request.Form["cg"].ToString();

           // string resp = sendEmailSMS(person_sms_emailList, sms,email);
            
            Response.Write("Success ");
            Response.End();
       }

      
    }





    public string WMShiftCount(int maxRows, int startRowIndex, int company_id, int wp_id, int category_id)
    {


         Orders tfc = new Orders();
         int list = tfc.getWMShiftConfirmedCount(maxRows, startRowIndex, company_id, wp_id, category_id,4);
        
         return  list.ToString();
    }


 


    public string WorkPlaceList(int company_id)
    {
        Company cp = new Company();
        string list = Utils.GetJSONString(cp.getWorkPlaceList(Convert.ToInt32(company_id), "", "", 1000, 0));
        return list.Replace("}]}", "}]").Replace("{\"WorkPLaceList\" :", "");
    }


    public string CityList(int wp_id)
    {
        CountryRegionCity rfc = new CountryRegionCity();
        string list = Utils.GetJSONString(rfc.getCityForRegionList(wp_id));
        return list.Replace("}]}", "}]").Replace("{\"cfr\" :", "");
    }


    public string CategoryList()
    {
        DataTable categ = new DataTable("categ");
        categ.Columns.Add("category_id", typeof(string));
        categ.Columns.Add("category_name", typeof(string));

        categ.Rows.Add("1","No Contract");
        categ.Rows.Add("3", "3 Months contract");
        categ.Rows.Add("6", "6 contract");
        categ.Rows.Add("12", "12 contract");
                  

        string list = Utils.GetJSONString(categ);
        return list.Replace("}]}", "}]").Replace("{\"categ\" :", "");
    }


    public string WMShiftList(int maxRows, int startRowIndex, int company_id, int wp_id,int category_id,string lang)
    {
         Orders tfc = new  Orders();
         string list = Utils.GetJSONString(tfc.getWMShiftConfirmedList(maxRows, startRowIndex, company_id, wp_id, category_id,4,lang));
         return  list.Replace("}]}", "}]").Replace("{\"shiftconfirmedlist\" :", "");
    }



    public string PersonList(int maxRows, int startRowIndex,int shift_id)
    {
        Roles role = new Roles();
        DataTable dt = new DataTable();
        dt = role.getRoleList(Convert.ToInt16(Session["schema_id"]), "en", 100);

        int dw_role_id = 1000;

        foreach (DataRow row in dt.Rows)
        {
            if (row["role_name_abbr"].ToString() == "DW")
                dw_role_id = Convert.ToInt32(row["role_id"]);
        }

        sinfoca.tiger.Person person = new sinfoca.tiger.Person();
        //  string list = Utils.GetJSONString(person.getPersonSearchList( dw_role_id, "", maxRows, startRowIndex));
        string list = Utils.GetJSONString(person.getAvailablePersonList2(shift_id, maxRows, startRowIndex));
        return list;
    }

    public string sendEmailSMS(string person_list, string sms_message, string email_message)
    {

        string str = person_list;
        string[] str_result = new string[60];
        char[] spliter = { '|' };
        str_result = str.Split(spliter);

        string response = "Great!!!";

        using (var rateGate = new PennedObjects.RateLimiting.RateGate(1, TimeSpan.FromSeconds(1)))
        {
            for (int i = 0; i < str_result.Length; i++)
            {

                string[] str_result2 = new string[5];
                char[] spliter2 = { '&' };
                str_result2 = str_result[i].Split(spliter2);

                if (str_result[i].Contains("sms&"))
                {
                    rateGate.WaitToProceed();
                    response = Utils.GetResponse("http://rest.nexmo.com/sms/json?username=16c5d9e5&password=4c79bf57&from=19057690406&to=" + str_result2[2] + "&text=%0aPlongeur+Mtl%0a+" + sms_message);
                    //  System.Threading.Thread.Sleep(1500);

                }

                if (str_result[i].Contains("email&"))
                {
                    Email.sendSESMailHtml("\"Plongeur Express\" <support@sinfoca.com>", str_result2[2], "J'ai un shift pour toé", email_message);
                }


            }
        }


        return response;

    }


    public string PersonSelect(int person_id, int shift_id)
    {

        sinfoca.tiger.Appointment person = new sinfoca.tiger.Appointment();
        bool  response = person.setAppointmentPersonAssign(person_id, shift_id,6);

        if (response == true)
            return "1";
        else
            return "0";
    }


    public string AppointmentCompleted(int shift_id)
    {

       sinfoca.tiger.Appointment person = new sinfoca.tiger.Appointment();
        bool response = person.setAppointmentCompleted(shift_id);

        if (response == true)
            return "1";
        else
            return "0";
    }



    public string PersonCount(int maxRows, int startRowIndex,int shift_id)
    {


        sinfoca.tiger.Person person = new sinfoca.tiger.Person();
        int count = person.getAvailablePersonCount2(shift_id, maxRows, startRowIndex);

        return count.ToString();
    }



   /* public string OrderShiftList(int maxRows, int startRowIndex, int order_id, int wp_id, int status)
    {
        Orders tfc = new Orders();
        string list = Utils.GetJSONString(tfc.getOrdersShiftList(maxRows, startRowIndex, order_id, wp_id, status));
        return list.Replace("}]}", "}]").Replace("{\"ordershiftlist\" :", "");
    }


    public string OrderShiftCount(int maxRows, int startRowIndex, int order_id, int wp_id, int status)
    {
        Orders tfc = new Orders();

        return tfc.getOrdersShiftCount(maxRows, startRowIndex, order_id, wp_id, status).ToString();
    }*/
   

}