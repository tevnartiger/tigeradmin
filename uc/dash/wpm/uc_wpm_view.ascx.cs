﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Win32;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

public partial class uc_dash_wpm_uc_wpm_view : System.Web.UI.UserControl
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
           

            ddl_lang.DataSource = config.LangList();
            ddl_lang.DataTextField = "Value";
            ddl_lang.DataValueField = "Key";
            ddl_lang.DataBind();

            sinfoca.tiger.Person p = new sinfoca.tiger.Person();

            DataTable v = new DataTable("a");
            DataTable w = new DataTable("a");
            DataTable x = new DataTable("a");

            link_add_location.Text = Resources.Resource.lbl_add_location;
          
            link_add_location.NavigateUrl = "~/persons/profile.aspx?p=add&person_id=" + Session["person_id"].ToString() + "&wp_id=" + Request.QueryString["wp_id"];
            //persons/profile.aspx?p=add&person_id=30&wp_id=
    
            string image_prev = "";

            v = p.getPersonView(Convert.ToInt32(Session["person_id"].ToString()));
            if (v.Rows.Count > 0)
            {
                user_fname.Text = v.Rows[0]["person_fname"].ToString();
                user_lname.Text = v.Rows[0]["person_lname"].ToString();
                image_prev = Convert.ToString(v.Rows[0]["person_img_path"].ToString());
                user_phone.Text = v.Rows[0]["cell"].ToString();
                user_email.Text = v.Rows[0]["login_user"].ToString();
                ddl_lang.SelectedValue = v.Rows[0]["person_lang"].ToString();
                lbl_lang.Text = ddl_lang.SelectedItem.Text;
                lbl_societe.Text = Session["identity"].ToString();

                if (Convert.ToString(v.Rows[0]["person_img_path"]).Length < 2)
                    img_logo.ImageUrl = "https://s3.amazonaws.com/asmsplus/0/default-dw.png";
                else
                {
                    img_logo.ImageUrl = config.cdn_host + Session["schema_id"].ToString() + "/" + v.Rows[0]["person_img_path"].ToString();
                }
            }

            Roles r = new Roles();
            DataTable dt = r.getRoleList(Convert.ToInt32(Session["schema_id"]), Session["_lastCulture"].ToString().Substring(0, 2), 100);

        }
    }

    public string getStatus(string status)
    {
        string ret = string.Empty;
        switch (status)
        {
            case "pe":
                ret = "Pending";
                break;

            case "ac":
                ret = "Active";
                break;

            case "lk":
                ret = "Lock Out";
                break;

            case "cl":
                ret = "Closed";
                break;

            case "rj":
                ret = "Rejected";
                break;

            default:
                ret = "No Status";
                break;
        }
        return ret;
    }
    
}