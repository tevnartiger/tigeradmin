﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.DataVisualization;
using System.Web.UI.WebControls.WebParts;

public partial class uc_dash_uc_dash_pm : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            SqlConnection conn = new SqlConnection(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
            SqlCommand cmd = new SqlCommand("prStats_Home", conn);
            SqlCommand cmd2 = new SqlCommand("prStats_Lease", conn);
            SqlCommand cmd3 = new SqlCommand("prAlertView", conn);
            SqlCommand cmd4 = new SqlCommand("prTotalHomeExpenseIncomeMonthChart", conn);
            SqlCommand cmd5 = new SqlCommand("prStats_ExpenseIncome", conn);
            SqlCommand cmd6 = new SqlCommand("prStats_DailyTask", conn);
            SqlCommand cmd7 = new SqlCommand("prStats_MonthlyRents", conn);
            SqlCommand cmd8 = new SqlCommand("prStats_Users", conn);
            SqlCommand cmd9 = new SqlCommand("prStats_Inventory", conn);
            SqlCommand cmd10 = new SqlCommand("prStats_Contacts", conn);



            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);

            try
            {

                conn.Open();
                SqlDataReader dr = null;
                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                while (dr.Read() == true)
                {
                    lbl_home_count.Text = dr["home_count"].ToString();
                    lbl_unit_count.Text = dr["unit_count"].ToString();
                    lbl_occ_unit_count.Text = dr["occ_unit_count"].ToString();
                    lbl_vacant_unit_count.Text = dr["vacant_unit_count"].ToString();

                }
            }
            finally
            {
                //  conn.Close();
            }





            cmd2.CommandType = CommandType.StoredProcedure;
            cmd2.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);

            try
            {
                SqlDataReader dr2 = null;
                dr2 = cmd2.ExecuteReader(CommandBehavior.SingleRow);

                while (dr2.Read() == true)
                {
                    lbl_lease_count.Text = dr2["lease_count"].ToString();
                    lbl_pending_lease_count.Text = dr2["pending_lease_count"].ToString();
                    lbl_archive_lease_count.Text = dr2["archive_lease_count"].ToString();
                }
            }
            finally
            {
                // conn.Close();
            }


            cmd3.CommandType = CommandType.StoredProcedure;

            //Add the params
            cmd3.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd3.Parameters.Add("@home_id", SqlDbType.Int).Value = 0;
            cmd3.Parameters.Add("@trace_name_id", SqlDbType.Int).Value = Convert.ToInt32(Session["name_id"]);

            int open_rows = 0;

            try
            {
                SqlDataReader dr3 = null;
                dr3 = cmd3.ExecuteReader(CommandBehavior.SingleRow);

                while (dr3.Read() == true)
                {
                    // amount of days for insurance policy pending and expiration
                    lbl_as_ip_expiration.Text = dr3["as_ip_expiration"].ToString();
                    lbl_as_ip_pending.Text = dr3["as_ip_pending"].ToString();

                    lbl_ip_expiration.Text = dr3["ip_expiration"].ToString();
                    if (Convert.ToInt32(dr3["ip_expiration"]) < 1)
                    {
                        tr_ip_expiration.Visible = false;
                        open_rows++;
                    }
                    lbl_ip_pending.Text = dr3["ip_pending"].ToString();
                    if (Convert.ToInt32(dr3["ip_pending"]) < 1)
                    {
                        tr_ip_pending.Visible = false;
                        open_rows++;
                    }

                    // amount of days for lease pending and expiration
                    lbl_as_lease_expiration.Text = dr3["as_lease_expiration"].ToString();
                    lbl_as_lease_pending.Text = dr3["as_lease_pending"].ToString();

                    lbl_lease_expiration.Text = dr3["lease_expiration"].ToString();
                    if (Convert.ToInt32(dr3["lease_expiration"]) < 1)
                    {
                        tr_lease_expiration.Visible = false;
                        open_rows++;
                    }

                    lbl_lease_pending.Text = dr3["lease_pending"].ToString();
                    if (Convert.ToInt32(dr3["lease_pending"]) < 1)
                    {
                        tr_lease_pending.Visible = false;
                        open_rows++;
                    }


                    // amount of days for mortgage pending and expiration
                    lbl_as_mortgage_expiration.Text = dr3["as_mortgage_expiration"].ToString();
                    lbl_as_mortgage_pending.Text = dr3["as_mortgage_pending"].ToString();

                    lbl_mortgage_expiration.Text = dr3["mortgage_expiration"].ToString();
                    if (Convert.ToInt32(dr3["mortgage_expiration"]) < 1)
                    {
                        tr_mortgage_expiration.Visible = false;
                        open_rows++;
                    }

                    lbl_mortgage_pending.Text = dr3["mortgage_pending"].ToString();
                    if (Convert.ToInt32(dr3["mortgage_pending"]) < 1)
                    {
                        tr_mortgage_pending.Visible = false;
                        open_rows++;
                    }


                    lbl_rent_delequency.Text = dr3["rent_delequency"].ToString();
                    if (Convert.ToInt32(dr3["rent_delequency"]) < 1)
                    {
                        tr_rent_delequency.Visible = false;
                        open_rows++;
                    }

                    lbl_wo_overdue.Text = dr3["as_wo_overdue"].ToString();
                    if (Convert.ToInt32(dr3["as_wo_overdue"]) < 1)
                    {
                        tr_wo_overdue.Visible = false;
                        open_rows++;
                    }

                    lbl_wo_pending.Text = dr3["as_wo_pending"].ToString();
                    if (Convert.ToInt32(dr3["as_wo_pending"]) < 1)
                    {
                        tr_wo_pending.Visible = false;
                        open_rows++;
                    }

                    lbl_untreated_rent.Text = dr3["nb_untreated_rent"].ToString();
                    if (Convert.ToInt32(dr3["nb_untreated_rent"]) < 1)
                    {
                        tr_untreated_rent.Visible = false;
                        open_rows++;
                    }
                }

            }
            finally
            {
                // conn.Close();
            }


            if (open_rows == 10)
            {
                panel_alerts.Visible = false;
            }
            else
                panel_alerts.Visible = true;






            cmd5.CommandType = CommandType.StoredProcedure;
            cmd5.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);

            try
            {
                SqlDataReader dr5 = null;
                dr5 = cmd5.ExecuteReader(CommandBehavior.SingleRow);

                while (dr5.Read() == true)
                {
                    lbl_income.Text = dr5["total_income"].ToString();
                    lbl_expense.Text = dr5["total_expense"].ToString();
                    lbl_cash_flow.Text = dr5["cash_flow"].ToString();
                }
            }
            finally
            {
                //   conn.Close();
            }







            Chart1.Width = 300;
            Chart1.Height = 250;

            Chart1.Palette = ChartColorPalette.BrightPastel;
            //.BrightPastel;

            //Chart1.ChartAreas.Add("Series 2");

            // create a couple of series
            // Chart1.Series.Add("Series 2");

            // Create a database command on the connection using query    
            cmd4.CommandType = CommandType.StoredProcedure;


            cmd4.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);
            cmd4.Parameters.Add("@lastCulture", SqlDbType.VarChar, 20).Value = Session["_lastCulture"].ToString();

            //  Label19.Text = Session["_lastCulture"].ToString();

            // Create a database reader    
            SqlDataReader dr4 = null;

            dr4 = cmd4.ExecuteReader();


            // Since the reader implements IEnumerable, pass the reader directly into
            //   the DataBind method with the name of the Column selected in the query    
            Chart1.Series["Series 2"].Points.DataBindXY(dr4, "categ", dr4, "amount");



            Chart1.Series["Series 2"].Label = " #VALY{N2}";

            Chart1.Series["Series 2"].ChartType = SeriesChartType.Bar;
            // Set data points label style
            Chart1.Series["Series 2"]["BarLabelStyle"] = "Center";
            Chart1.ChartAreas["ChartArea1"].AxisY.LabelStyle.Angle = 45;
            // Chart1.BackColor = Color.Orchid;


            // Enable 3D
            Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
            Chart1.ChartAreas["ChartArea1"].BorderColor = Color.Black;
            Chart1.ChartAreas["ChartArea1"].BorderWidth = 5;


            foreach (DataPoint point in Chart1.Series[0].Points)
            {
                point.YValues[0] = point.YValues[0];
                Chart1.Series["Series 2"].Palette = ChartColorPalette.BrightPastel;//assigning colors to datapoint  
            }






            cmd6.CommandType = CommandType.StoredProcedure;
            cmd6.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);

            try
            {
                SqlDataReader dr6 = null;
                dr6 = cmd6.ExecuteReader(CommandBehavior.SingleRow);

                while (dr6.Read() == true)
                {
                    lbl_incident_last30days_count.Text = dr6["incident_last30days_count"].ToString();
                    lbl_incident_last48hrs_count.Text = dr6["incident_last48hrs_count"].ToString();
                    lbl_wo_count.Text = dr6["wo_count"].ToString();
                    lbl_event_within7days_count.Text = dr6["event_within7days_count"].ToString();
                }
            }
            finally
            {
                //   conn.Close();
            }



            cmd7.CommandType = CommandType.StoredProcedure;
            cmd7.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);




            try
            {
                SqlDataReader dr7 = null;
                dr7 = cmd7.ExecuteReader(CommandBehavior.SingleRow);

                decimal lease; // amount of lease , current month
                decimal paid_rent; //amount of paid rent , current month
                decimal unpaid_rent; // amount of unpaid rent amount , current month
                decimal partial_rent; // amount of partial rent 

                while (dr7.Read() == true)
                {
                    lbl_lease_count2.Text = dr7["lease_count"].ToString();
                    /* try
                     {*/
                    lease = Convert.ToDecimal(dr7["lease_count"]);


                    lbl_paid_rent_count.Text = dr7["paid_rent_count"].ToString();
                    paid_rent = Convert.ToDecimal(dr7["paid_rent_count"]);

                    lbl_unpaid_rent_count.Text = dr7["unpaid_rent_count"].ToString();
                    unpaid_rent = Convert.ToDecimal(dr7["unpaid_rent_count"]);

                    lbl_partial_rent_count.Text = dr7["partial_rent_count"].ToString();
                    partial_rent = Convert.ToDecimal(dr7["partial_rent_count"]);

                    if (lease == 0)
                        lbl_partial_rent_count_percent.Text = "0.00";
                    else
                        lbl_partial_rent_count_percent.Text = string.Format("{0:0.00}", (partial_rent / lease) * 100);

                    if (lease == 0)
                        lbl_unpaid_rent_count_percent.Text = "0.00";
                    else
                        lbl_unpaid_rent_count_percent.Text = string.Format("{0:0.00}", (unpaid_rent / lease) * 100);

                    if (lease == 0)
                        lbl_paid_rent_count_percent.Text = "0.00";
                    else
                        lbl_paid_rent_count_percent.Text = string.Format("{0:0.00}", (paid_rent / lease) * 100);
                }
            }
            finally
            {
                //   conn.Close();
            }





            cmd8.CommandType = CommandType.StoredProcedure;
            cmd8.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);

            try
            {
                SqlDataReader dr8 = null;
                dr8 = cmd8.ExecuteReader(CommandBehavior.SingleRow);

                while (dr8.Read() == true)
                {
                    lbl_pm_count.Text = dr8["pm_count"].ToString();
                    lbl_tenant_count.Text = dr8["tenant_count"].ToString();
                    lbl_owner_count.Text = dr8["owner_count"].ToString();
                    lbl_janitor_count.Text = dr8["janitor_count"].ToString();
                }
            }
            finally
            {
                //   conn.Close();
            }



            cmd9.CommandType = CommandType.StoredProcedure;
            cmd9.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);

            try
            {
                SqlDataReader dr9 = null;
                dr9 = cmd9.ExecuteReader(CommandBehavior.SingleRow);

                while (dr9.Read() == true)
                {
                    lbl_home_item_count.Text = dr9["home_item_count"].ToString();
                    lbl_warehouse_item_count.Text = dr9["warehouse_item_count"].ToString();
                }
            }
            finally
            {
                //   conn.Close();
            }

            cmd10.CommandType = CommandType.StoredProcedure;
            cmd10.Parameters.Add("@schema_id", SqlDbType.Int).Value = Convert.ToInt32(Session["schema_id"]);

            try
            {
                SqlDataReader dr10 = null;
                dr10 = cmd10.ExecuteReader(CommandBehavior.SingleRow);

                while (dr10.Read() == true)
                {
                    lbl_company_count.Text = dr10["company_count"].ToString();
                    lbl_supplier_vendor_count.Text = dr10["supplier_vendor_count"].ToString();
                    lbl_fi_count.Text = dr10["fi_count"].ToString();
                    lbl_ic_count.Text = dr10["ic_count"].ToString();
                    lbl_warehouse_count.Text = dr10["warehouse_count"].ToString();
                }
            }
            finally
            {
                conn.Close();
            }

        }
    }
}