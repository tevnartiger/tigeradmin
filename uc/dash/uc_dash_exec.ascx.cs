using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class uc_uc_dash_default : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
      {
            Roles role = new Roles();

            string lang = Session["_lastCulture"].ToString().Substring(0, 2);


            if (!string.IsNullOrEmpty(Request.QueryString["language"]) && config.queryLangList().Contains(Request.QueryString["language"].Substring(0, 2)))
                lang = Request.QueryString["language"].Substring(0, 2);

          System.Data.DataTable df ;

        /*  if (Session["justloggedin"].ToString() == "1")
          {
              df = (Session["rolepersonservices"] as DataTable);
              Email.sendSESMailHtml("\"ASMS+\" <noreply@asmsplus.com>", "jocestan@hotmail.com", "Logged in test", "");
                        
          }
          else*/


              df = role.getRolePersonServices(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["role_id"]), lang);


          Session["justloggedin"] = "0";


            int tot = df.Rows.Count;
            int sc_id = 0;

            main.Text += "<div style='margin-top:10px;padding:5px;' class='row-fluid'><div class='span12'>";
            if (tot > 0)
            {
                for (int i = 0; i < tot; i++)
                {
                    if (sc_id != Convert.ToInt32(df.Rows[i]["sc_id"]))
                    {
                        if (i == 0)
                        {
                            main.Text += "<div  class='row-fluid'>";

                        }
                        else
                            main.Text += "</div>";

                        main.Text += "<div style='min-height:150px; padding:4px' class='span3'>";


                        main.Text += "<div style='float:left;width:98%;background:#f1f1f1;padding:3px;border-radius:0px;max-height:140px;' class='panel'>";
                        //  main.Text += "<img src='" + df.Rows[i]["sc_icon"].ToString() + "' style='float:left;' /><span class='dash-title'>" + df.Rows[i]["sc_name"].ToString() + "</span>";
                        main.Text += "<span class='dash-title'>" + df.Rows[i]["sc_name"].ToString() + "</span>";
                        main.Text += "</div>";
                        sc_id = Convert.ToInt32(df.Rows[i]["sc_id"]);
                    }
                    if (df.Rows[i]["service_status"].ToString() == "BA")
                        main.Text += "<div  class='row-fluid beta'><div class='span12' style='float:left;'><span class='title'>" + df.Rows[i]["service_title"].ToString() + "</span><span class='comming'>Comming in " + String.Format("{0:ddd, MMM d, yyyy}", Convert.ToDateTime(df.Rows[i]["service_online_date"].ToString())) + "</span><span style='float:left;width:98%;'>" + df.Rows[i]["service_desc"].ToString() + "</span></div></div>";
                    else
                    {
                        // if (df.Rows[i]["sc_attribute_1"].ToString() != "employee")
                        main.Text += "<div class='row-fluid beta'><div class='span12' style='float:left;'><a href='" + config.host + df.Rows[i]["service_url"].ToString() + "'>" + df.Rows[i]["service_title"].ToString() + "</a> ";

                        main.Text += ServiceCount(df.Rows[i]["servicecount"].ToString(), df.Rows[i]["service_abbr"].ToString());

                        main.Text += "<!--<span style='float:left;width:98%;'>" + df.Rows[i]["service_desc"].ToString() + "</span>--></div>";

                        main.Text += "</div>";


                        // else
                        //  if (df.Rows[i]["service_url"].ToString().Contains("dw.aspx"))
                        {
                            //    main.Text += "<div class='row-fluid beta'><div class='span12' style='float:left;'><a href='" + config.host + df.Rows[i]["service_url"].ToString() + "'>" + df.Rows[i]["service_title_en"].ToString() + "</a><!--<span style='float:left;width:98%;'>" + df.Rows[i]["service_desc"].ToString() + "</span>--></div></div>";
                        }
                    }


                    //  main.Text = tot.ToString();
                    if (i == tot)
                        main.Text += "</div>";

                }
            }
            main.Text += "</div>";
            main.Text += "</div>";
        }

    }



    string ServiceCount(string count, string service_abbr)
    {
        string response = count;

        if (service_abbr == "NA")
            response = "";
        else
        {
            if (Convert.ToInt32(count) > 0)
                response = "<span class='notempty'>(" + count + ")</span>";
            else
                response = "<span class='empty'>(" + count + ")</span>";

        }


        return response;
    }
}

