using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Codeplex.Data;

public partial class uc_uc_dash_wpm : System.Web.UI.UserControl
{

    protected string str, str2, str3, str4, str5, str6, str7, str8, str9;

    protected void Page_Load(object sender, EventArgs e)
    {
   
    /*    UserControl uch = Page.Master.FindControl("uc_adm_lmenu2") as UserControl;
        //PlaceHolder phProxylist = ucHeader1.FindControl("phProxy") as PlaceHolder;
        DropDownList ddlproxylist = uch.FindControl("ddlEmpZone") as DropDownList;
        // ddlproxylist.Visible = false;
        int zone_id = 0;// Convert.ToInt32(ddlproxylist.SelectedValue);
        main.Text = ddlproxylist.SelectedValue;
      */
        // if (!Page.IsPostBack)
        {



            if (Session["mobile"] != null)
                uc_wpm_view.Visible = false;

            Roles role = new Roles();

            string lang = Session["_lastCulture"].ToString().Substring(0, 2);


            if (!string.IsNullOrEmpty(Request.QueryString["language"]) && config.queryLangList().Contains(Request.QueryString["language"].Substring(0, 2)))
                lang = Request.QueryString["language"].Substring(0, 2);



            System.Data.DataTable df = role.getRolePersonServices(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["role_id"]),lang);
            /*  gv_main.DataSource = df;
         gv_main.DataBind();
              */
            int tot = df.Rows.Count;
            int sc_id = 0;
            string temp_sc_name = string.Empty;
            string temp_service_title = string.Empty;

            main.Text += "<div style='margin-top:10px;padding:5px;' class='row-fluid'><div class='span12'>";
            if (tot > 0)
            {
                for (int i = 0; i < tot; i++)
                {
                       temp_sc_name = df.Rows[i]["sc_name"].ToString() ;
                        temp_service_title = df.Rows[i]["service_title"].ToString();
                   

                    if (sc_id != Convert.ToInt32(df.Rows[i]["sc_id"]))
                    {
                        if (i == 0)
                        {
                            main.Text += "<div  class='row-fluid'>";

                        }
                        else
                            main.Text += "</div>";

                        main.Text += "<div style='min-height:150px; padding:4px' class='span4'>";


                        main.Text += "<div class='panel' style='float:left;width:98%;background:#f1f1f1;padding:3px;border-radius:0px;max-height:140px;'>";
                        main.Text += "<img src='" + df.Rows[i]["sc_icon"].ToString() + "' style='float:left;' /><span class='dash-title'>" + temp_sc_name + "</span>";
                        main.Text += "</div>";
                        sc_id = Convert.ToInt32(df.Rows[i]["sc_id"]);
                    }
                    if (df.Rows[i]["service_status"].ToString() == "BA")
                        main.Text += "<div  class='row-fluid beta'><div class='span12' style='float:left;'><span class='title'>" + temp_service_title+ "</span><span class='comming'>Comming in " + String.Format("{0:ddd, MMM d, yyyy}", Convert.ToDateTime(df.Rows[i]["service_online_date"].ToString())) + "</span><span style='float:left;width:98%;'>" + df.Rows[i]["service_desc"].ToString() + "</span></div></div>";
                    else
                    {
                        // if (df.Rows[i]["sc_attribute_1"].ToString() != "employee")
                        main.Text += "<div class='row-fluid beta'><div class='span12' style='float:left;'><a href='" + df.Rows[i]["service_url"].ToString() + "'>" + temp_service_title + "</a> ";

                    //    main.Text += ServiceCount(df.Rows[i]["servicecount"].ToString(), df.Rows[i]["service_abbr"].ToString());

                        main.Text += "<!--<span style='float:left;width:98%;'>" + df.Rows[i]["service_desc"].ToString() + "</span>--></div>";

                        main.Text += "</div>";


                        // else
                        //  if (df.Rows[i]["service_url"].ToString().Contains("dw.aspx"))
                        {
                            //    main.Text += "<div class='row-fluid beta'><div class='span12' style='float:left;'><a href='" + config.host + df.Rows[i]["service_url"].ToString() + "'>" + df.Rows[i]["service_title_en"].ToString() + "</a><!--<span style='float:left;width:98%;'>" + df.Rows[i]["service_desc"].ToString() + "</span>--></div></div>";
                        }
                    }


                    //  main.Text = tot.ToString();
                    if (i == tot)
                        main.Text += "</div>";

                }
            }
            main.Text += "</div>";
            main.Text += "</div>";

            //message wpm
            if (Session["_lastCulture"].ToString().Substring(0,2) == "en")      
            message_wpm.Text = "<div class='message-wpm'><h2>Welcome to netl2</h2>";
            else
            message_wpm.Text = "<div class='message-wpm'><h2>Bienvenue � netl2</h2>";

            message_wpm.Text += "<span>Lorem ipsum dolor sit amet, ut duo latine urbanitas necessitatibus, esse vocent legendos eu sit, referrentur vituperatoribus vis eu. Ex vim augue saepe mediocritatem, possim aliquip vel in, vim ne dicta gubergren incorrupte. In nec wisi populo praesent, duo vide accusata no, putant nominati tincidunt cum et. Ut idque eruditi dissentiet sed, quem oblique sed te, mea laoreet deserunt theophrastus ne. Ne eum quem ridens facete.</span>";
            message_wpm.Text += "</div>";
        }
  

    }


    static DataTable GetTablePie()
    {
        //
        // Here we create a DataTable with four columns.
        //
        DataTable table = new DataTable("a");
        table.Columns.Add("Color", typeof(string));
        table.Columns.Add("total", typeof(int));

        //
        // Here we add five DataRows.
        //
        table.Rows.Add("Active DishWasher", 320);
        table.Rows.Add("Dishwasher on probation", 38);
        table.Rows.Add("Dishwasher Pending activation", 2143);
        /*    table.Rows.Add("green", 1933);
            table.Rows.Add("purple", 4343);
            table.Rows.Add("pink", 343);
            table.Rows.Add("orange", 2143);
            table.Rows.Add("black", 1933);*/
        return table;
    }

    static DataTable GetTableco()
    {
        //
        // Here we create a DataTable with four columns.
        //
        DataTable table = new DataTable("a");
        table.Columns.Add("Color", typeof(string));
        table.Columns.Add("total", typeof(int));

        //
        // Here we add five DataRows.
        //
        table.Rows.Add("Germany", 4343);
        table.Rows.Add("United States", 343);
        table.Rows.Add("Brazil", 2143);
        table.Rows.Add("Canada", 1933);
        table.Rows.Add("France", 4343);
        table.Rows.Add("RU", 343);

        return table;
    }
    static DataTable GetTableArea()
    {
        //
        // Here we create a DataTable with four columns.
        //
        DataTable table = new DataTable("a");
        table.Columns.Add("Year", typeof(string));
        table.Columns.Add("Sales", typeof(int));
        table.Columns.Add("Expenses", typeof(int));

        //
        // Here we add five DataRows.
        //
        table.Rows.Add("2009", 125989, 65876);
        table.Rows.Add("2010", 258000, 165400);
        table.Rows.Add("2011", 365000, 230988);
        table.Rows.Add("2012", 16000, 76688);
        return table;
    }

    static DataTable GetTableMonth()
    {
        //
        // Here we create a DataTable with four columns.
        //
        DataTable table = new DataTable("a");
        table.Columns.Add("Months", typeof(string));
        table.Columns.Add("Sales", typeof(int));
        table.Columns.Add("Expenses", typeof(int));

        //
        // Here we add five DataRows.
        //
        table.Rows.Add("jan", 10433, 6876);
        table.Rows.Add("feb", 258000, 165400);
        table.Rows.Add("march", 365000, 230988);
        table.Rows.Add("april", 16000, 76688);
        table.Rows.Add("may", 125989, 65876);
        table.Rows.Add("june", 258000, 165400);
        table.Rows.Add("july", 365000, 230988);
        table.Rows.Add("august", 16000, 76688);
        table.Rows.Add("sept", 125989, 65876);
        table.Rows.Add("oct", 258000, 165400);

        return table;
    }

    static DataTable GetTableCompare()
    {
        //
        // Here we create a DataTable with four columns.
        //
        DataTable table = new DataTable("a");
        table.Columns.Add("Months", typeof(string));
        table.Columns.Add("2011", typeof(int));
        table.Columns.Add("2012", typeof(int));

        //
        // Here we add five DataRows.
        //
        table.Rows.Add("jan", 10433, 6876);
        table.Rows.Add("feb", 14043, 19089);
        table.Rows.Add("march", 65000, 40988);
        table.Rows.Add("april", 16000, 22688);
        table.Rows.Add("may", 23989, 35876);
        table.Rows.Add("june", 48000, 55400);
        table.Rows.Add("july", 65000, 70988);
        table.Rows.Add("august", 45000, 67688);
        table.Rows.Add("sept", 34989, 65876);
        table.Rows.Add("oct", 38000, 22400);
        table.Rows.Add("nov", 38000, 0);
        table.Rows.Add("dec", 68000, 0);

        return table;
    }

}

