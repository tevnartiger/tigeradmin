<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_dash_dw.ascx.cs" Inherits="uc_uc_dash_dw" %>
  
<link rel="stylesheet" href="/../cdnjs.cloudflare.com/ajax/libs/normalize/3.0.1/normalize.css" />
<style type="text/css">
fieldset {
     border: 0;
}
</style>

<style>
    @media 
    only screen and (max-width: 760px),
    (min-device-width: 768px) and (max-device-width: 1024px)  {
/*
Label the data
*/
 .talent_table td:nth-of-type(1):before { content: "ID"; }
 .talent_table td:nth-of-type(2):before { content: "<%= Resources.orders.orders_week %>"; }
 .talent_table td:nth-of-type(3):before { content: "<%= Resources.orders.orders_location %>"; }
 .talent_table td:nth-of-type(4):before { content: "<%= Resources.orders.orders_job %>"; }
 .talent_table td:nth-of-type(5):before { content: "<%= Resources.orders.orders_status%>"; }
 .talent_table td:nth-of-type(6):before { content: "<%= Resources.orders.orders_date%>"; }
 .talent_table td:nth-of-type(7):before { content: "<%= Resources.orders.orders_start_time%>"; }
 .talent_table td:nth-of-type(8):before { content: "<%= Resources.orders.orders_end_time%>"; }
 .talent_table .tohide:before {  visibility:hidden; }
}
 
</style>
<script src="js/jquery.simplemodal.1.4.1.min.js" type="text/javascript"></script>
<script src="../../js/date-<%= Session["_lastCulture"].ToString()%>.js"></script>
<script language="javascript" type="text/javascript" src="js/jsrender.js"></script>
<script language="javascript" type="text/javascript" src="js/json2.js"></script>
<script type="text/javascript"  src="js/jquery.pagination.js"></script>
<script type="text/javascript"  src="/js/bootstrap.js"></script>
<script language="javascript" type="text/javascript" src="js/querystring-0.9.0-min.js"></script>
<script src="../../js/formatDatejs.js"></script>
<link rel="stylesheet" href="css/pagination.css" />

 <script type="text/javascript">

     function confirmShift(id) {

         $.ajaxSetup({ cache: false });
         // Create pagination element with options from form
         $.get("uc/dash/employee/ProcessData.aspx", { ShiftID: id, fct: "confirm" },
                function (data) {
                    if (data = "True") {
                        alert("The shift is confirmed");

                        $("#st_" + id + " span").text("confirmed")


                        $.ajaxSetup({ cache: false });
                        // alert("document ready");
                        // Create pagination element with options from form
                        $.get("ProcessData.aspx", { ag: "10", bg: "0", WPID: 0, StatusID: 4, fct: "personshiftcount" },
                                function (data) {
                                    //  alert($("#ddlWorkplace option:selected").val()); 
                                    // alert(data);
                                    $(" #sp_current_count").text(data);
                                    $("#paginator").pagination(data, { items_per_page: 10, callback: handlePaginationClick });
                                });
                    }
                });
     };



     /**
     * Callback function that displays the content.
     *
     * Gets called every time the user clicks on a pagination link.
     *
     * @param {int}page_index New Page index
     * @param {jQuery} jq the container with the pagination links as a jQuery object
     */
     function handlePaginationClick(page_index, jq) {
         var item_page = 10;
         //alert("handle Pagination");


         //  $("#h_wp_id").val(WPID)
         //  $("#h_Company_id").val(CompanyID)

         $.get("uc/dash/employee/ProcessData.aspx", { ag: "10", bg: page_index * item_page, WPID: 0, StatusID: 4, fct: "personshiftlist" },
                   function (data) {

                       var mydata = jQuery.parseJSON(data);
                       // Render to string
                       //  alert(data);
                       $.views.helpers({
                           status_date: function (val) {
                               var split = val.split('/');
                               var month1 = split[0];
                               var day1 = split[1];
                               var year1 = split[2];

                               var date2;
                               var format = Date.CultureInfo.formatPatterns.shortDate;

                               date2 = getFormat("<%= Session["_lastCulture"].ToString()%>", split[0], split[1], split[2]);
                               date = Date.parse(date2, format);
                               return date.toString("ddd, MMMM dd, yyyy");
                           }
                       });

                       $.views.helpers({
                           status_name: function (val, duration) {
                               switch (val) {
                                   case "2":
                                       return "<%= Resources.Resource.shift_waiting%>";
                                   case "3":
                                       return "<%= Resources.Resource.shift_refused%>";
                                   case "4":
                                       return "<%= Resources.Resource.shift_confirmed%>";
                                   case "5":
                                       return "<%= Resources.Resource.shift_cancelled%>";
                                   case "6":
                                       if (duration != "")
                                           return "<%= Resources.Resource.shift_validated%>";
                                       else
                                           return "validated by client only";
                                   case "8":
                                       return "<%= Resources.Resource.shift_did_not_attend %>";
                                   case "7":
                                       return "<%= Resources.Resource.shift_validated%>";
                                   case "9":
                                       return "<%= Resources.Resource.shift_closed%>";

                               }

                           }
                       })
                       $.views.helpers({
                           label_display: function (val, duration) {

                               switch (val) {
                                   case "4":
                                       return val;
                                   case "6":
                                       return val;
                                   case "7":
                                       return val;
                                   case "8":
                                       return val;
                                   case "9":
                                       return val;
                                   default:
                                       return "";

                               }
                           }
                       })

                       $.views.helpers({
                           label_name: function (val) {
                               switch (val) {
                                   case "2":
                                       return "";
                                   case "3":
                                       return "label-success";
                                   case "4":
                                       return "label-important";

                               }

                           }
                       })


                       var html2 = $("#appTmpl2").render(mydata);
                       // Insert as HTML

                       $("#details2").html(html2);
                       //disable checkbox if no cell number



                       $('input[type=submit]').each(function () {
                           if (this.id.indexOf("ty_4") >= 0 || this.id.indexOf("ty_6") >= 0 || this.id.indexOf("ty_7") >= 0
                               || this.id.indexOf("ty_8") >= 0 || this.id.indexOf("ty_9") >= 0)
                               $(this).hide();

                       });

                   });




         return false;
     }




     // When document has loaded, initialize pagination and form
     $(document).ready(function () {




         $.ajaxSetup({ cache: false });
         // alert("document ready");
         // Create pagination element with options from form
         $.get("uc/dash/employee/ProcessData.aspx", { ag: "10", bg: "0", WPID: 0, StatusID: 4, fct: "personshiftcount" },
                 function (data) {
                     //  alert($("#ddlWorkplace option:selected").val()); 
                     // alert(data);
                     $(" #sp_current_count").text(data);
                     $("#paginator").pagination(data, { items_per_page: 10, callback: handlePaginationClick });
                 });


         $('#myTab a').click(function (e) {
             e.preventDefault();
             $(this).tab('show');
         })

         $('a[data-toggle="tab"]').on('shown', function (e) {
             e.target // activated tab
             e.relatedTarget // previous tab
         })


     });


  </script>


<asp:Panel ID="panel_default" runat="server">
 
       <dl class="dl-horizontal">
      <dt> <%= Resources.Resource.app_registration_date%></dt>
      <dd><asp:Label ID="lbl_registration_date" runat="server" Text="Label"></asp:Label></dd>
     
      <dt style="display:none"> <%= Resources.Resource.lbl_role%></dt>
      <dd style="display:none"><asp:Label ID="lbl_role" runat="server" Font-Size="Large" Text="Label"></asp:Label></dd>

      <dt> <%= Resources.Resource.global_lb_fname%></dt>
      <dd><asp:Label ID="lb_fname" runat="server" Text="Label"></asp:Label></dd>
      
       <dt> <%= Resources.Resource.global_lb_lname%></dt>
      <dd><asp:Label ID="lb_lname" runat="server" Text="Label"></asp:Label></dd>
      
       <dt> <%= Resources.Resource.global_lb_email%></dt>
      <dd><asp:Label ID="lb_email" runat="server" Text="Label"></asp:Label></dd>

      <dt> Mobile</dt>
      <dd><asp:Label ID="lbl_mobile" runat="server" Text="Label"></asp:Label></dd>


      </dl>

<hr />
     <div style="font-size:16px;margin-bottom:10px;" > <%= Resources.orders.orders_upcomming_shift %></div>
   <div class="talent_table"> 
  <!--====== Template ======-->

        <script id="appTmpl2" type="text/x-jquery-tmpl">
     
        <tr>
         <td>{{>appointment_id}}</td>
          <td>{{>wk}}</td>
          <td>{{>wp_name}}<br/><span class='dw_myschedule_address'>{{>wp_address}}</span></td>
         <td>{{>jc_name}}</td>
         <td><span id="st_{{=appointment_id}}" class="label {{>~label_name(appointment_status)}}">{{>~status_name(appointment_status,appointment_emp_duration)}}</span></td>
         <td>{{>~status_date(appointment_date)}}</td>
         <td>{{>min_to_start_time}}</td>
         <td>{{>min_to_end_time}}</td>
        </tr>
        </script>
     <!--====== Container ======-->

         
        <table class="table table-bordered table-condensed table-hover">
        <thead>
        <tr>
         <th><strong>ID</strong></th>
          <th><strong><%= Resources.orders.orders_week %></strong></th>
         <th><strong><%= Resources.orders.orders_location %></strong></th>
          <th><strong><%= Resources.orders.orders_job %></strong></th>
           <th><strong><%= Resources.orders.orders_status%></strong></th>
           <th><strong><%= Resources.orders.orders_date%></strong></th>
           <th><strong><%= Resources.orders.orders_start_time%></strong></th>
          <th><strong><%= Resources.orders.orders_end_time%></strong></th>
         </tr>
        </thead>
        <tbody id="details2"></tbody>
        </table>

        <div id="paginator"  class="pagination2"></div>   
             </div>



        <div id="h_person_id"></div>








</asp:Panel>
 
