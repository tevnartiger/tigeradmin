﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using sinfoca.login;

public partial class ProcessData : BasePageLite
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Clear();
        Response.ClearContent();

        List<string> queryStringList = new List<string>();
        queryStringList.Add("list");
        queryStringList.Add("count");

        queryStringList.Add("confirm");
        queryStringList.Add("personshiftlist");
        queryStringList.Add("personshiftcount");



        int status_id = 0;
        int person_id = 0;
        int shift_id = 0;


        string PersonID = Request.QueryString["PersonID"];
        if (PersonID != null && RegEx.IsInteger(PersonID))
        {
            person_id = Convert.ToInt32(PersonID);
        }

        string ShiftID = Request.QueryString["ShiftID"];
        if (ShiftID != null && RegEx.IsInteger(ShiftID))
        {
            shift_id = Convert.ToInt32(ShiftID);
        }


        string StatusID = Request.QueryString["StatusID"];
        if (StatusID != null && RegEx.IsInteger(StatusID))
        {
            status_id = Convert.ToInt32(StatusID);
        }

        int a = 0;
        int b = 0;

        string AG = Request.QueryString["ag"];
        if (AG != null && RegEx.IsInteger(AG))
        {
            a = Convert.ToInt32(AG);
        }

        string BG = Request.QueryString["bg"];
        if (BG != null && RegEx.IsInteger(BG))
        {
            b = Convert.ToInt32(BG);
        }


        if (queryStringList.Contains(Request.QueryString["fct"]))
        {
                     switch (Request.QueryString["fct"])
            {
                case "list": Response.Write(PersonList(a, b));
                    break;

           
                case "count": Response.Write(PersonCount(a, b));
                    break;

                case "confirm": Response.Write(PersonConfirm(shift_id, 4));
                    break;

                case "personshiftcount": Response.Write(PersonShiftCount(a, b,Convert.ToInt32(Session["person_id"]), status_id));
                    break;

                case "personshiftlist": Response.Write(PersonShiftList(a, b,Convert.ToInt32(Session["person_id"]), status_id));
                    break;

            }
            Response.End();
        }

      
    }




    public string PersonList(int maxRows, int startRowIndex)
    {
        Roles role = new Roles();
        DataTable dt = new DataTable();
        dt = role.getRoleList(Convert.ToInt16(Session["schema_id"]), "en", 100);
        
        int dw_role_id = 1000;

        foreach (DataRow row in dt.Rows)
        {
            if (row["role_name_abbr"].ToString() == "DW")
                dw_role_id = Convert.ToInt32(row["role_id"]);
        }
        
        sinfoca.tiger.Person person = new sinfoca.tiger.Person();
        string list = Utils.GetJSONString(person.getPersonSearchList( dw_role_id, "", maxRows, startRowIndex));
        return list;
    }



    public string PersonCount(int maxRows, int startRowIndex)
    {


        Roles role = new Roles();
        DataTable dt = new DataTable();
        dt = role.getRoleList(Convert.ToInt16(Session["schema_id"]), "en", 100);

        int dw_role_id = 1000;

        foreach (DataRow row in dt.Rows)
        {
            if (row["role_name_abbr"].ToString() == "DW")
                dw_role_id = Convert.ToInt32(row["role_id"]);

        }

        sinfoca.tiger.Person person = new sinfoca.tiger.Person();
        int count = person.getPersonSearchCount(dw_role_id, "", maxRows, startRowIndex);
        
        return count.ToString();
    }

    public string PersonShiftList(int maxRows, int startRowIndex, int person_id, int status_id)
    {
        sinfoca.tiger.Appointment tfc = new sinfoca.tiger.Appointment();
        string list = Utils.GetJSONString(tfc.getPersonShiftConfirmedList(maxRows, startRowIndex ,person_id,0, status_id));
        return list.Replace("}]}", "}]").Replace("{\"personshiftconfirmedlist\" :", "");
    }


    public string PersonShiftCount(int maxRows, int startRowIndex, int person_id, int status_id)
    {
        sinfoca.tiger.Appointment tfc = new sinfoca.tiger.Appointment();

        return tfc.getPersonShiftConfirmedCount(maxRows, startRowIndex, person_id,0, status_id).ToString();
    }

    public string PersonConfirm(int shift_id, int status_id)
    {
        sinfoca.tiger.Appointment tfc = new sinfoca.tiger.Appointment();
        return tfc.setAppointmentStatusChange(shift_id, status_id).ToString();
    }
   
}