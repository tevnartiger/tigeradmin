﻿/**
* Callback function that displays the content.
*
* Gets called every time the user clicks on a pagination link.
*
* @param {int}page_index New Page index
* @param {jQuery} jq the container with the pagination links as a jQuery object
*/

var nb_items = 10;

function handlePaginationClick(page_index, jq) {
    var item_page = 10;
    //  alert("handle Pagination");
    $.ajaxSetup({ cache: false });
    $.get("ProcessData.aspx", { ag: "10", bg: page_index * item_page, fct: "list", t:"" },
                   function (data) {
                       // GET Store current Json in h field

                  //     alert(data);

                  //     $("#h_ids").val(data);

                       var mydata = jQuery.parseJSON(data);

                       // Render to string
                       var html2 = $("#appTmpl2").render(mydata.PersonSearchList[0]);

                       // Insert as HTML
                       $("#details2").html(html2);

                   });
    return false;
}



$(document).keypress(function (e) {
    var arrsize2 = 1;
    if (e.keyCode == 13) {
        $.get("ProcessData.aspx", { ag: "10", bg: "0", fct: "count", t:"" },
                 function (data) {
                     
                     $("#paginator").pagination(data, { items_per_page: 10, num_edge_entries: 2, callback: handlePaginationClick });
                 });
        $.get("ProcessData.aspx", { ag: "10", bg: "0", fct: "list", t: "" },
                   function (data) {
                       // GET Store current Json in h field
                       // alert(data);
                       $("#h_ids").val(data);
                       if (arrsize2 > 0) {
                           var mydata = jQuery.parseJSON(data);
                           // Render to string
                           var html2 = $("#appTmpl2").render(mydata.PersonSearchList[0]);
                           // Insert as HTML
                           $("#details2").html(html2);
                       }
                   });
        return false;
    }
});  





// The form contains fields for many pagiantion optiosn so you can
// quickly see the resuluts of the different options.
// This function creates an option object for the pagination function.
// This will be be unnecessary in your application where you just set
// the options once.


// When document has loaded, initialize pagination and form
$(document).ready(function () {
    // alert("document ready");
    // Create pagination element with options from form
    $.ajaxSetup({ cache: false });
    $.get("ProcessData.aspx", { ag: "10", bg: "0", fct: "count", t: "" },
                 function (data) {
                     $("#paginator").pagination(data, { items_per_page: 10, num_edge_entries: 2, callback: handlePaginationClick });
                 });

    ////////////////////////////////////////////////////////////////////////////////////////
    $.modal.defaults.closeClass = "modalClose";

});