using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Codeplex.Data;

public partial class uc_uc_dash_dw : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            DataTable status = new DataTable("status");
            status.Columns.Add("status_id", typeof(string));
            status.Columns.Add("status_name", typeof(string));

            status.Rows.Add("2", Resources.Resource.shift_waiting);
            status.Rows.Add("3",Resources.Resource.shift_refused);
            status.Rows.Add("4", Resources.Resource.shift_confirmed);
            status.Rows.Add("5", Resources.Resource.shift_cancelled);
            status.Rows.Add("8", Resources.Resource.shift_did_not_attend);
            status.Rows.Add("9", Resources.Resource.shift_closed);



            sinfoca.tiger.Person person = new sinfoca.tiger.Person();

            DataTable dt3 = person.getPersonView(Convert.ToInt32(Session["person_id"]));
            var statuses3 = DynamicJson.Parse(Utils.GetJSONString(dt3));

            lb_fname.Text = statuses3.personview[0].person_fname.ToString();
            lb_lname.Text = statuses3.personview[0].person_lname.ToString();
            lbl_role.Text = statuses3.personview[0].role_name.ToString();
            lbl_registration_date.Text = statuses3.personview[0].registration_date.ToString();
            lb_email.Text = statuses3.personview[0].login_user.ToString();
            lbl_mobile.Text = statuses3.personview[0].cell.ToString();



        }
    }

}

