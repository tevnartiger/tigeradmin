<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_create_acount.ascx.cs" Inherits="uc_create_acount" %>
<table class="table_menu_module">
<tr><td colspan="2" class="title_menu_module" >Create account</td></tr>
<tr><td class="letter_bold">First name</td><td><asp:TextBox MaxLength="25" CssClass="letter" ID="name_fname" runat="server" />
<asp:RequiredFieldValidator ID="da" ControlToValidate="name_fname" runat="server" Text="<%$ Resources:resource, iv_notempty %>" />
</td></tr>
<tr><td class="letter_bold">Last name</td><td><asp:TextBox MaxLength="25" CssClass="letter" ID="name_lname" runat="server" /></td></tr>
<tr><td class="letter_bold">E-mail/username</td><td><asp:TextBox MaxLength="50" size="50" CssClass="letter" ID="name_email" runat="server" /></td></tr>
<tr><td class="letter_bold">Password</td><td><asp:TextBox MaxLength="25" CssClass="letter" TextMode="password" ID="login_pwd" runat="server" /></td></tr>
<tr><td class="letter_bold">Re-type Password</td><td><asp:TextBox CssClass="letter" TextMode="password" sID="retype_login_pwd" runat="server" /></td></tr>

<tr><td colspan="2"><asp:Button ID="bn_create" runat="server" Text="Create Account" OnClick="bn_create_Click" /></td></tr>
<tr><td colspan="2"><asp:Label ID="txt" runat="server" /></td></tr>

</table>