using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class manager_uc_uc_content_menu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string menu = "";
        switch (System.IO.Path.GetFileName(Request.Path.ToString()))
        {

            case "person_default.aspx":
                menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='user_default.aspx'>"+" &nbsp;Users "+"</a> / List";
             break;

            case "person_add.aspx":
             menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='user_default.aspx'>" + " &nbsp;Users " + "</a> / New user";
             break;

            case "roles.aspx":
             menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/settings/settings_default.aspx'>" + " &nbsp;Settings " + "</a> / Role Management";
             break;

            case "settings_default.aspx":
             menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/settings/settings_default.aspx'>" + " &nbsp;Settings " + "</a>";
             break;

            case "viewer_appointment_default.aspx":
            case "employee_appointment_default.aspx":
            case "client_appointment_default.aspx":
            case "staff_appointment_default.aspx":
            case "appointment_default.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/appointment/appointment_default.aspx'>"+Resources.Resource.app_dashboard+"</a>";
            break;

            case "viewer_appointment_view.aspx":
            case "employee_appointment_view.aspx":
            case "client_appointment_view.aspx":
            case "staff_appointment_view.aspx":
            case "appointment_view.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/appointment/appointment_default.aspx'>"+Resources.Resource.app_appointment+"</a>";
            break;

            case "client_appointment_add.aspx":
            case "client_appointment_add2.aspx":
            case "staff_appointment_add.aspx":
            case "appointment_add.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/appointment/appointment_add.aspx'>"+Resources.Resource.app_new_appointment+"</a>";
            break;


            case "appointment_template.aspx":
            case "appointment_staff_template.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/f/setup/setup.aspx'>" + Resources.Resource.menu_settings + "   >></a> " + Resources.Resource.menubar_email_templates;
          break;

            case "staff_user_default.aspx":
            case "assistant_user_default.aspx":
             menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/user/user_default.aspx'>"+Resources.Resource.app_clients+"</a>";
            
            break;


            case "client_profile_add.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/user/client_profile_add.aspx'>"+Resources.Resource.app_new_client+"</a>";
            break;


            case "assistant_list.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/f/setup/setup.aspx'>" + Resources.Resource.menu_administrators + "</a> ";
            break;


            case "assistant_profile_add.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/f/setup/setup.aspx'>" + Resources.Resource.menubar_new_administrator +"</a> " ;
            break;


            case "staff_list.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/f/setup/setup.aspx'>" + Resources.Resource.menu_staff + "</a> ";
            break;

            case "staff_profile_add.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/f/setup/setup.aspx'>" + Resources.Resource.menubar_new_staff + "</a> ";
            break;

            case "user_profile_update.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/f/setup/setup.aspx'>" + Resources.Resource.menu_settings +"   >></a> " + Resources.Resource.app_edit_profile;;
           break;

            case "staff_profile_update.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/f/setup/setup.aspx'>" + Resources.Resource.app_edit_staff_profile +"</a> " ;
           break;

            case "setup_appointment.aspx":
           menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/f/setup/setup.aspx'>" + Resources.Resource.menu_settings + "   >></a> " + Resources.Resource.app_appointment_settings ;
           break;


            case "staff_user_view.aspx":
            case "user_view.aspx":
                if(Request.Url.Query.Contains("c=c"))
                   menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/user/user_default.aspx'>"+ Resources.Resource.app_client+"</a>";
              
                if (Request.Url.Query.Contains("c=a"))
                    menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/user/user_default.aspx'>" + Resources.Resource.app_administrator+ "</a> ";         
             
                if (Request.Url.Query.Contains("c=s"))
                    menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/user/user_default.aspx'>" + Resources.Resource.app_staff+ "</a> ";         
              break;

            case "assistant_user_update.aspx":
            case "user_update.aspx":
               menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/user/user_default.aspx'>"+Resources.Resource.app_edit+"</a>";
            
            break;

            case "staff_user_update.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/user/user_default.aspx'>" + Resources.Resource.app_edit_client + "</a>";

            break;

            case "client_user_update.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/user/user_default.aspx'>" + Resources.Resource.app_modify_profil + "</a>";

            break;

          
            case "services_add.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/appointment/appointment_add.aspx'>"+Resources.Resource.menubar_new_service+"</a>";
            break;

           

            case "location_add.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/appointment/appointment_add.aspx'>" + Resources.Resource.menubar_new_location + "</a>";
            break;

            case "location_view.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/appointment/appointment_add.aspx'>" + Resources.Resource.app_location + "</a>";
            break;

            case "location_update.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/appointment/appointment_add.aspx'>" + Resources.Resource.menubar_edit_location+ "</a>";
            break;

            case "employee_location_list.aspx":
            case "client_location_list.aspx":
            case "viewer_location_list.aspx":
            case "location_list.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/appointment/appointment_add.aspx'>"+Resources.Resource.app_locations+"</a>";
            break;


            case "services_list.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/appointment/appointment_add.aspx'>"+Resources.Resource.app_services+"</a>";
            break;

            case "services_view.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/appointment/appointment_add.aspx'>" + Resources.Resource.app_service + "</a>";
            break;

            case "services_update.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/appointment/appointment_add.aspx'>" + Resources.Resource.app_edit_service + "</a>";
            break;


            case "setup.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/appointment/appointment_add.aspx'>" + Resources.Resource.menu_settings + "</a>";
            break;



            case "setup_sync.aspx":
            case "setup_sync_grant.aspx":
            case "setup_sync_revoke.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/f/setup/setup.aspx'>" + Resources.Resource.menu_settings + "  >></a> " + Resources.Resource.setup_google_sync;
            break;



            case "customize.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/f/setup/setup.aspx'>" + Resources.Resource.menu_settings + "   >></a> " + Resources.Resource.setup_customize_website;
            break;

            case "company_update.aspx":
            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='/f/setup/setup.aspx'>" + Resources.Resource.menu_settings + "   >></a> " + Resources.Resource.app_company;
         
            
            break;
          
            case "default.aspx":

            menu += "<a style='color: #585880;font-size:11pt;font-weight:bold;text-decoration: none;' href='default.aspx'>" + Resources.Resource.app_dashboard + "</a>&nbsp;";
            
                break;
         }
        content_menu.InnerHtml = menu;

    }
}
