using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class manager_uc_uc_adm_lmenu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    
  /*  <ul id="accordion">
	<li>Sports</li>
	<ul>
		<li><a href="#">Golf</a></li>
		<li><a href="#">Cricket</a></li>
		<li><a href="#">Football</a></li>
	</ul>
    */

        if (!Page.IsPostBack)
        {


            sinfoca.tiger.Person person = new sinfoca.tiger.Person();
            ddlEmpZone.DataSource = person.getEmployeZoneList();
            ddlEmpZone.DataBind();
            ddlEmpZone.Items.Insert(0, new ListItem(Resources.zone.zone_all_zones, "0"));

            if (Session["zone_id"] != null)
            {
                ddlEmpZone.SelectedValue = Session["zone_id"].ToString();
            }
    
        }

    Roles role = new Roles();


    switch (Session["role_id"].ToString())
    {
        case "15": ddlEmpZone.Visible = false;
            break;

    }

        System.Data.DataTable df = role.getRolePersonServices(Convert.ToInt32(Session["schema_id"]),Convert.ToInt32(Session["role_id"]));
        int tot = df.Rows.Count;
        int i = 0;
        string categ = string.Empty;
        string str = string.Empty;
        if (tot > 0)
        {
 //str = "<h3>Services</h3>";
 str  = "<ul id=\"accordion\">";
 
            foreach (System.Data.DataRow r in df.Rows)
            {
                 
                if (categ != r["sc_name_en"].ToString())
                {
                    categ = r["sc_name_en"].ToString();
                 if(i == 0)
                 {
                    str +="<li style='background-color:aliceblue;' class='lm_title'>"+r["sc_name_en"] +"</li>";
                    str +="<ul>";
				  }
				  else
				  {
				  str += "</ul>";
				 // str += "<ul>";
                  str += "<li style='background-color:aliceblue;' class='lm_title'>" + r["sc_name_en"] + "</li>";
                    str +="<ul>";
				  }
                
                
                
                }
                 
                
                
                if (categ != r["sc_name_en"].ToString())
               { str +="<li><a href='"  + r["service_url"] + "'>" + r["service_title_en"] + "</a></li>";
                str +="</ul>";
				}
				else
				str +="<li><a href='"  + r["service_url"] + "'>" + r["service_title_en"] + "</a></li>";
				
			i++;
            }

            str += "</ul>";
            home.InnerHtml = str;
        }

    }

    protected void ddlEmpZone_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["zone_id"] = ddlEmpZone.SelectedValue;
        if (System.IO.Path.GetFileName(Request.Path.ToString()) == "order.aspx")
        {
            Response.Redirect(Request.RawUrl); ;
        }
    }
}

