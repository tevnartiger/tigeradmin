﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DatePicker.usercontrol.ascx.cs" Inherits="DatePickerUserControl" %>
<div class="datePicker">
    <input class="datePicker--input" id="DatePickerTextInput" runat="server" type="text" />
    <div class="datePicker--logoWrapper" id="DatePickerLogo" runat="server">
        <i class="logoWrapper--logo fa fa-calendar" aria-hidden="true"></i>
    </div>
    <input type="hidden" id="DateHidden" runat="server" />
</div>

<script>
    window.addEventListener('DOMContentLoaded', function () {
        var date = flatpickr.parseDate("<%= this.DefaultDate.Year %>-<%= this.DefaultDate.Month %>-<%= this.DefaultDate.Day %>", "<%= this.DateFormat %>");

        document.getElementById("<%= DateHidden.ClientID %>").value = flatpickr.formatDate(date, "<%= this.DateFormat %>");

        var pick_<%= this.UniqueKey %> = flatpickr('#<%= DatePickerTextInput.ClientID %>', {
            enableTime: false,
            altInput: true,
            altFormat: "D j F Y",
            dateFormat: '<%= this.DateFormat %>',
            defaultDate: date,
            locale: {
                firstDayOfWeek: 1 // start week on Monday
            },
            onChange: function (selectedDates, dateStr, instance) {
                document.getElementById("<%= DateHidden.ClientID %>").value = dateStr;
            }
        });

        document.getElementById("<%= DatePickerLogo.ClientID %>").onclick = function() {
            pick_<%= this.UniqueKey %>.open();
        };
    });
</script>

