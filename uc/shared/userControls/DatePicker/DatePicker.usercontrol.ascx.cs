﻿using System;

// https://stackoverflow.com/questions/21303655/using-multiple-instances-of-an-aspx-user-control-in-a-aspx-page
public partial class DatePickerUserControl : System.Web.UI.UserControl
{
    protected string UniqueKey;
    protected string DateFormat = "Y-m-d";
    private string DateFormatForCsharp = "yyyy-MM-dd";
    protected DateTime DefaultDate = DateTime.Now;

    protected void Page_Load(object sender, EventArgs e)
    {
        UniqueKey = Guid.NewGuid().ToString("N");
    }

    public DateTime SelectedDate
    {
        get
        {
            try
            {
                return DateTime.ParseExact(DateHidden.Value, this.DateFormatForCsharp, null);

            }
            catch (Exception e)
            {
                return DefaultDate;
            }
        }
        set
        {
            this.DefaultDate = value;
        }
    }
}
