﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class uc_uc_adm_lmenu4 : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {


       


      

        Roles role = new Roles();


        string lang = Session["_lastCulture"].ToString().Substring(0, 2);


        if (!string.IsNullOrEmpty(Request.QueryString["language"]) && config.queryLangList().Contains(Request.QueryString["language"].Substring(0, 2)))
        {
            lang = Request.QueryString["language"].Substring(0, 2);
            if( Session["justloggedin"] != "1")
             Session["rolepersonservices1"] = role.getRolePersonServices(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["role_id"]), lang);
        }

        Session["justloggedin"] = "0";

        System.Data.DataTable df = Session["rolepersonservices1"] as DataTable;
       
        int tot = df.Rows.Count;
        int i = 0;
        string categ = string.Empty;
        string str = string.Empty;

     
        DataTable service_categ = new DataTable();
        service_categ.Columns.Add("sc_name", typeof(string));
        service_categ.Columns.Add("sc_name_en", typeof(string));
        service_categ.Columns.Add("sc_name_fr", typeof(string));
        service_categ.Columns.Add("sc_icon", typeof(string));
        service_categ.Columns.Add("sc_id", typeof(string));
      
        
        string thecateg = "";


        if (tot > 0)
        {
            //str = "<h3>Services</h3>";
          
            foreach (System.Data.DataRow r in df.Rows)
            {
              //  categ = r["sc_name_en"].ToString();

                if (lang == "fr") 
                {
                    thecateg = r["sc_name_fr"].ToString();
                }
                if (lang == "en")
                {
                    thecateg = r["sc_name_en"].ToString();
                }


                if (categ != r["sc_name_en"].ToString())
                {
                    categ = r["sc_name_en"].ToString();
                    service_categ.Rows.Add(r["sc_name"].ToString(), r["sc_name_en"].ToString(), r["sc_name_fr"].ToString(), r["sc_icon"].ToString(), r["sc_id"].ToString());
                }
            }

            r_categ.DataSource = service_categ;
            r_categ.DataBind();

        }

    }



    protected DataTable ServiceCategList(string sc_id)
    {

        Roles role = new Roles();

        string lang = Session["_lastCulture"].ToString().Substring(0, 2);


        if (!string.IsNullOrEmpty(Request.QueryString["language"]) && config.queryLangList().Contains(Request.QueryString["language"].Substring(0, 2)))
        {
            lang = Request.QueryString["language"].Substring(0, 2);

            if (Session["load"] != null && Session["load"].ToString() == "1")
            { }
            else{
             Session["rolepersonservices1"] = role.getRolePersonServices(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["role_id"]), lang);
             Session["load"] = "1";
            }
        
        }

        System.Data.DataTable df = Session["rolepersonservices1"] as DataTable;
        int tot = df.Rows.Count;
        int i = 0;
        string categ = string.Empty;
        string str = string.Empty;

        DataTable service = new DataTable();
        service.Columns.Add("sc_id", typeof(string));
        service.Columns.Add("service_id", typeof(int));
        service.Columns.Add("service_status", typeof(string));
        service.Columns.Add("service_title_en", typeof(string));
        service.Columns.Add("service_title_fr", typeof(string));
        service.Columns.Add("service_title", typeof(string));
        service.Columns.Add("sc_name_en", typeof(string));
        service.Columns.Add("sc_name_fr", typeof(string));
        service.Columns.Add("sc_name", typeof(string));
        service.Columns.Add("service_url", typeof(string));

       // service = df;


        foreach (DataRow MyDataRow in df.Select("sc_id = '"+sc_id+"'"))
        {
            service.ImportRow(MyDataRow);
        }

        DataTable service_categ = new DataTable();
        service_categ.Columns.Add("sc_id", typeof(string));
        service_categ.Columns.Add("sc_name_en", typeof(string));
        service_categ.Columns.Add("sc_name_fr", typeof(string));

        return service;
    }



    protected override void OnPreRender(EventArgs e)
    {
        Session["load"] = "0";
        Session["justloggedin"] = "0";
    }



 



    protected string Collapse(string filename,string sc_name_en)
    {
        DataTable rs = Session["rolepersonservices"] as DataTable;

        List<string> url = new List<string>();
        
        foreach (System.Data.DataRow r in rs.Rows)
        {
          
            if (sc_name_en == r["sc_name_en"].ToString())
            {
                url.Add(r["service_url"].ToString());
            }
        }

        bool response = false;

        for (int i = 0; i < url.Count; i++) // Loop through List with for
        {
            if (url[i].Contains(filename))
            {
                response = true;
            }
        }


        if (filename.Contains("tocome.aspx"))
            response = false;

        if (response == true)
            return "";
        else
            return "collapse";
    }

}