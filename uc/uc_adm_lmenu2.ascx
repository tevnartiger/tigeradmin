﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_adm_lmenu2.ascx.cs" Inherits="uc_uc_adm_lmenu2" %>
     <div class="antiScroll">
					<div class="antiscroll-inner">
						<div class="antiscroll-content">
					
							<div class="sidebar_inner">
								<asp:Panel ID="panel_default" runat="server">
                                    
                                </asp:Panel>

                              <div id="side_accordion" class="accordion">
                                 
                                 
                                  <asp:Repeater ID="r_categ" runat="server">
                                  <ItemTemplate>
                                  <div class="accordion-group">
										<div class="accordion-heading" style="height:28px;">
											<a href="#collapseOne<%# DataBinder.Eval(Container.DataItem, "sc_id")%>" data-parent="#side_accordion" data-toggle="collapse" class="accordion-toggle">
												<i class="<%# DataBinder.Eval(Container.DataItem, "sc_icon")%>"></i>&nbsp;<%# DataBinder.Eval(Container.DataItem, "sc_name")%>
											</a>
										</div>
                                    <div class="accordion-body <%#Collapse(System.IO.Path.GetFileName(Request.Path.ToString()),Convert.ToString(Eval("sc_name_en")))%>" id="collapseOne<%# DataBinder.Eval(Container.DataItem, "sc_id")%>">
											<div class="accordion-inner">
												<ul class="nav nav-list">

                                      <asp:Repeater ID="r_list" DataSource='<%#ServiceCategList(Convert.ToString(Eval("sc_id")))%>' runat="server">
                                       <ItemTemplate>                                     
										<li style="height:20px; padding:-1px;"><a href="<%# DataBinder.Eval(Container.DataItem, "service_url")%>"><%# DataBinder.Eval(Container.DataItem, "service_title")%></a></li>
										</ItemTemplate>
                                    </asp:Repeater>	
											</ul>
											</div>
										</div>
									</div>
                                  </ItemTemplate>
                                  </asp:Repeater>

                              </div>


							
								
								<div class="push"></div>
							</div>
							   
						 
						
						</div>
					</div>
				</div>
