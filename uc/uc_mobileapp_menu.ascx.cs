﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class uc_uc_mobileapp_menu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!Page.IsPostBack)
        {
            sinfoca.tiger.Person person = new sinfoca.tiger.Person();

        }


        Roles role = new Roles();


        string lang = Session["_lastCulture"].ToString().Substring(0, 2);


        if (!string.IsNullOrEmpty(Request.QueryString["language"]) && config.queryLangList().Contains(Request.QueryString["language"].Substring(0, 2)))
        {
            lang = Request.QueryString["language"].Substring(0, 2);
            if ( Convert.ToString(Session["justloggedin"]) != "1")
                Session["rolepersonservices1"] = role.getRolePersonServices(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["role_id"]), lang);
        }

        Session["justloggedin"] = "0";

        System.Data.DataTable df = Session["rolepersonservices1"] as DataTable;

        int tot = df.Rows.Count;
        int i = 0;
        string categ = string.Empty;
        string str = string.Empty;


        DataTable service_categ = new DataTable();
        service_categ.Columns.Add("sc_name", typeof(string));
        service_categ.Columns.Add("sc_name_en", typeof(string));
        service_categ.Columns.Add("sc_name_fr", typeof(string));
        service_categ.Columns.Add("sc_icon", typeof(string));
        service_categ.Columns.Add("sc_id", typeof(string));


        string thecateg = "";


        if (tot > 0)
        {
            //str = "<h3>Services</h3>";

            foreach (System.Data.DataRow r in df.Rows)
            {
                //  categ = r["sc_name_en"].ToString();

                if (lang == "fr")
                {
                    thecateg = r["sc_name_fr"].ToString();
                }
                if (lang == "en")
                {
                    thecateg = r["sc_name_en"].ToString();
                }


                if (categ != r["sc_name_en"].ToString())
                {
                    categ = r["sc_name_en"].ToString();


                    // if a work place manager don't show the invoice, we have hard coded the invoice
                    // so the client can access the page directly ( just one click )
                    if ( (r["sc_abbr"].ToString() == "inv" && Session["role_name_abbr"].ToString() == "WPM") ||
                         ( r["sc_abbr"].ToString() == "myvalidation" && Session["role_name_abbr"].ToString() == "WPM")
                        )
                    { }
                    else
                      service_categ.Rows.Add(r["sc_name"].ToString(), r["sc_name_en"].ToString(), r["sc_name_fr"].ToString(), r["sc_icon"].ToString(), r["sc_id"].ToString());
                }
            }

            r_categ.DataSource = service_categ;
            r_categ.DataBind();

        }

    }


    protected DataTable ServiceCategList(string sc_id)
    {

        Roles role = new Roles();

        string lang = Session["_lastCulture"].ToString().Substring(0, 2);


        if (!string.IsNullOrEmpty(Request.QueryString["language"]) && config.queryLangList().Contains(Request.QueryString["language"].Substring(0, 2)))
        {
            lang = Request.QueryString["language"].Substring(0, 2);

            if (Session["load"] != null && Session["load"].ToString() == "1")
            { }
            else {
                Session["rolepersonservices1"] = role.getRolePersonServices(Convert.ToInt32(Session["schema_id"]), Convert.ToInt32(Session["role_id"]), lang);
                Session["load"] = "1";
            }

        }

        System.Data.DataTable df = Session["rolepersonservices1"] as DataTable;
        int tot = df.Rows.Count;
        int i = 0;
        string categ = string.Empty;
        string str = string.Empty;

        DataTable service = new DataTable();
        service.Columns.Add("sc_id", typeof(string));
        service.Columns.Add("service_id", typeof(int));
        service.Columns.Add("service_status", typeof(string));
        service.Columns.Add("service_title_en", typeof(string));
        service.Columns.Add("service_title_fr", typeof(string));
        service.Columns.Add("service_title", typeof(string));
        service.Columns.Add("sc_name_en", typeof(string));
        service.Columns.Add("sc_name_fr", typeof(string));
        service.Columns.Add("sc_name", typeof(string));
        service.Columns.Add("service_url", typeof(string));

        // service = df;


        foreach (DataRow MyDataRow in df.Select("sc_id = '" + sc_id + "'"))
        {
            service.ImportRow(MyDataRow);
        }

        DataTable service_categ = new DataTable();
        service_categ.Columns.Add("sc_id", typeof(string));
        service_categ.Columns.Add("sc_name_en", typeof(string));
        service_categ.Columns.Add("sc_name_fr", typeof(string));

        return service;
    }



    protected override void OnPreRender(EventArgs e)
    {
        Session["load"] = "0";
        Session["justloggedin"] = "0";
    }

}