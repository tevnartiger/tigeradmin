﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Security.Cryptography;

public partial class uc_xsrf : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

       //----------------------------------------------------------------

        
if(!Page.IsPostBack)
{

    var bytes = new byte[sizeof(Int64)];
    RNGCryptoServiceProvider Gen = new RNGCryptoServiceProvider();
    Gen.GetBytes(bytes);
    BitConverter.ToInt64(bytes, 0).ToString(); 

    Random random = new Random();

    string hashedPwd = FormsAuthentication.HashPasswordForStoringInConfigFile(BitConverter.ToInt64(bytes, 0).ToString(), "sha1");

    ViewState["token"] = hashedPwd ;
    this.pageid.Value = ViewState["token"].ToString();

}
else
{
    if (((Request.Form["ctl00$ContentPlaceHolder1$ctl00$pageid"] != null) &&
      (ViewState["token"] != null)) &&
       (Request.Form["ctl00$ContentPlaceHolder1$ctl00$pageid"].ToString().Equals(ViewState["token"].ToString())))
 {
    // Valid Page
   // Response.Write("Safe Acces");
 }
  else
   {
       Session.Clear();
       Response.Redirect("http://www.facebook.com", true);
   }

 }
//------------------------------------------------------------------
    }

}
