﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class tiger_ip_restriction : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            lbl_ip.Text = HttpContext.Current.Request.UserHostAddress.ToString();
            lbl_host.Text = HttpContext.Current.Request.UserHostName.ToString();

            if (Application["Ip_restriction_status"].ToString() == "0")
                lbl_ip_restriction.Text = "OFF";
            else
                lbl_ip_restriction.Text = "ON";
        }
    

    }
    protected void btn_iprestriction_on_Click(object sender, EventArgs e)
    {
        Application["Ip_restriction_status"] = "1";
        lbl_ip_restriction.Text = "ON";
    }
    protected void btn_iprestriction_off_Click(object sender, EventArgs e)
    {
        Application["Ip_restriction_status"] = "0";
        lbl_ip_restriction.Text = "OFF";
    }
}
