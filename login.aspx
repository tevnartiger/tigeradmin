﻿<%@ Page Language="C#" AutoEventWireup="true" Theme="SinfoTiger" CodeFile="login.aspx.cs" Inherits="login" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style type="text/css"  >
    div.row {float: left; margin:  0px 0px 8px 0px; padding: 0; width: 98%;}
    div.col500px {float: left; width: 500px; margin: 0 3px 0 0; padding: 0;}
    div.col35em {float: left; width:35em; margin: 0 3px 0 0; padding: 0;}
    body{
        font-family:"Lucida Grande", "Lucida Sans Unicode", Verdana, Arial, Helvetica, sans-serif;
        font-size:12px;
         background-color:#ECF4FB;
        }
       
    </style>
   
</head>
<body>
    
     <br /><br /><br />
     <br />
     <br />   
   <form id="form"  runat="server"> 
  
   <div id="stylized" class="myform">
    <h1>
        <asp:Image ID="Image1" runat="server" ImageUrl="~/login2_files/logo.png" />
    </h1>
    <h1>Login&nbsp; ( Dev. )&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               <asp:HyperLink ID="HyperLink3" runat="server" 
                NavigateUrl="http://forum.sinfoca.info" style="font-size: xx-small">forum1</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;
               <asp:HyperLink ID="HyperLink4" runat="server" 
                NavigateUrl="http://forum.sinfoca.info/default2.aspx" style="font-size: xx-small">forum2</asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <asp:DropDownList ID="Language" runat="server" AutoPostBack="True" >
            <asp:ListItem Value="en-US">English (US)</asp:ListItem>
            <asp:ListItem Value="fr-FR">Francais</asp:ListItem> 
        </asp:DropDownList>
        </h1>
    <p>&nbsp;</p>
        <div id="Div3" class="row">
        <div class="col35em">
        <label >
 <asp:Label ID="lbl_user" runat="server" CssClass="bold" Text="<%$ Resources:Resource, lbl_user %>"/>&nbsp;<span class="small"></span></label>&nbsp;
        <asp:TextBox  MaxLength="50" Width="200" text="steve" ID="txt_user_name" runat="server" />
        </div>  
        </div> 

<div id="Div6" class="row">
        <div class="col35em">
    <label>
 <asp:Label ID="lbl_password" CssClass="bold" runat="server" Text="<%$ Resources:Resource, lbl_password %>" />
 &nbsp;<span class="small">
     <asp:RequiredFieldValidator runat="server"  ID="sa" ControlToValidate="txt_user_pwd" 
                                 Text="<%$ Resources:resource, iv_required %>" />
    </span>
    </label>
 <asp:TextBox TextMode="Password"  MaxLength="25" Width="200" ID="txt_user_pwd" runat="server" />
  </div>  
  </div> 

  
   
   
       <div id="Div1" class="row">
        <div class="col35em">
        <label >&nbsp;&nbsp;
        <span class="small">
            </span></label>&nbsp;
         <asp:button  ID="btnSend" Text="Send" runat="server" 
                OnClick="btnSend_Click" Width="110px" />
      &nbsp;
            <asp:HyperLink ID="HyperLink1" runat="server" 
                NavigateUrl="~/create_account_30.aspx" style="font-size: medium">Sign Up</asp:HyperLink>
       </div>  
        </div> 
   
    
    
       <div id="Div5" class="row">
        <div class="col35em">
        <label >&nbsp;
        <span class="small">
        </span>
        </label>&nbsp;
               <asp:HyperLink ID="dsa" runat="server"   NavigateUrl="/forgot_password.aspx" Text="<%$ Resources:Resource,lbl_forget_password %>"/>
        </div>  
        </div> 
    
       <div id="Div2" class="row">
        <div class="col35em">
        <label >&nbsp;
        <span class="small">
        </span>
        </label>&nbsp;
            <asp:Label ID="lbl_success"  runat="server" />
      
        </div>  
        </div> 
        
         <div id="Div4" class="row">
        <div class="col35em">
        <label >&nbsp;
        <span class="small">
        </span>
        </label>&nbsp;
        <asp:Label ID="status" runat="server"  /> 
        </div>  
        </div> 
 <div class="spacer"></div> 
 
 </div>
 </form>
   
  
</body>
</html>