﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
/// <summary>
/// Done by : Stanley Jocelyn   
/// Date    : oct 1 , 2009
/// </summary>
public partial class create_account_30 : LoginBasePage2
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            string temp_name = RegEx.name;
            string temp_pwd = RegEx.pwd;
            string temp_user = RegEx.alpha_numeric;
            string temp_email = RegEx.email;

            reg_name_fname.ValidationExpression = temp_name;
            reg_name_lname.ValidationExpression = temp_name;
            reg_name_email.ValidationExpression = temp_email;
            reg_login_user.ValidationExpression = temp_name;
            reg_login_pwd.ValidationExpression = temp_pwd;
            reg_retype_login_pwd.ValidationExpression = temp_pwd;
        }
    }
    protected void bn_create_Click(object sender, EventArgs e)
    {

        string temp_login_user = "";
        string temp_login_pwd = "";
        Page.Validate();
        if (!Page.IsValid)
        {
            //input validation server side
            string str_val = "";
           
            System.Web.HttpBrowserCapabilities browser = Request.Browser;
            Security sa = new Security(Request);

            sa.sendUserInfo(str_val, Request.Browser, Request);
            txt.Text = "Error occured <a href='https://sinfoca.info/login.aspx'>Back to main page</a>";
            bn_create.Enabled = true;
        }
        else
        {

            temp_login_user = login_user.Text;
            temp_login_pwd = login_pwd.Text;

            //insert home
            string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

            //validation code
            string schema_activation_code = tiger.RandomPassword.Generate(15, 20);

            /*****************************************************************/
            //check to see if dirty account exist 
            //SP will return dirty account schema_id 
            tiger.registration.Account a = new tiger.registration.Account(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]));
           
            //no dirty account found
            /**************************************************************************************/
            int create_account_result = a.createAccount(name_lname.Text, name_fname.Text, name_email.Text, temp_login_user, temp_login_pwd, 1, schema_activation_code);
            if (create_account_result == -1)
            {
                txt.Text = "***runtime error, cannot continue --create_account_result";
            }
            else
            {
                switch (create_account_result)
                {
                    case -1:
                        txt.Text = "Runtime error occurred";
                        break;
                    case 0:
                        //get activation code
                        string temp_schema_activation_code = a.getSchemaActivationCode(temp_login_user);
                        string str_to2 = name_email.Text;
                        string str_from2 = "noreply@sappointment.com";
                        string str_subject2 = "tiger real estate project validation";
                        string str_body2 = "Thank you for choosing tiger real estate.<br/> Please click <a href='https://kaypictiger.azurewebsites.net/account_validate.aspx?usr=" + temp_login_user + "&id=" + temp_schema_activation_code + "'>here</a> to validate account";
                        Email.sendSESMailHtml(str_from2, str_to2, str_subject2, str_body2);

                        txt.Text = "A validation key has been sent to your mailbox";
                        break;
                    case 1:
                        txt.Text = "Stored procedure transaction failed";
                        break;
                    case 2:
                        txt.Text = "<span class='letter_red'>User is already being used, click <a href='forget_pwd.aspx?email=" + name_email.Text + "' class='letter_link'>here</a>if you have forgotten your email.activation code</span>";
                        break;
                    case 3:
                        txt.Text = "<span class='letter_red'>Account already exist. To send validation key click  <a href='send_validation.aspx?email=" + name_email.Text + "' class='letter_link'>here</a></span>";
                        break;
                }
            }


        }
    }
}
