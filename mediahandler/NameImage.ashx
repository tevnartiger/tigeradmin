﻿<%@ WebHandler Language="C#" Class="NameImage" %>

using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.SessionState; // add to access session values
/// <summary>
/// Done by : Stanley Jocelyn
/// Date    : fev 23 , 2009
/// </summary>

public class NameImage : IHttpHandler, IReadOnlySessionState {

    public void ProcessRequest(HttpContext context)
    {
        string name_id = context.Request.QueryString["n_id"];
        string schema_id = context.Session["schema_id"].ToString();
       
        
        string strconn = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);

        SqlConnection conn = new SqlConnection(strconn);
        conn.Open();
        string sql = "select object_media from TObject where schema_id = @schema_id and object_categ='name' and  object_categ_id= @object_categ_id";
        SqlCommand cmd = new SqlCommand(sql, conn);

        cmd.CommandType = CommandType.Text;
        cmd.Parameters.AddWithValue("@object_categ_id", Convert.ToInt32(name_id));
        cmd.Parameters.AddWithValue("@schema_id", Convert.ToInt32(schema_id));

        SqlDataReader dr = cmd.ExecuteReader();
        dr.Read();

        context.Response.BinaryWrite((Byte[])dr[0]);

        conn.Close();

        context.Response.End();
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }


}