<%@ Page Language="C#" Debug="true" Trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	DataEngine de;
	SeriesCollection sc=null;
	Series seriesObj=null;
	
	//set global properties
    Chart.Title="Item sales";  
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.DefaultSeries.DefaultElement.ToolTip="%yvalue";

    dotnetCHARTING.TimeInterval curTimeInterval = TimeInterval.Years;
 
   string dateGrouping = HttpContext.Current.Request.QueryString["dategrouping"];
   if(dateGrouping ==null || dateGrouping=="")
		dateGrouping="Years";
   else if(dateGrouping !=null && dateGrouping!="")
   	curTimeInterval = (dotnetCHARTING.TimeInterval)Enum.Parse(typeof(dotnetCHARTING.TimeInterval), dateGrouping,true);
   	string startDateString = HttpContext.Current.Request.QueryString["startDate"];
   	DateTime startDate = new DateTime(2002,1,1,0,0,0);
   	if(startDateString !=null && startDateString!="")
   		startDate = DateTime.Parse(startDateString);
   		
	
	DateTime endDate = new DateTime();
	 
 	switch(curTimeInterval)
 	{
 		case TimeInterval.Years:
 			Chart.XAxis.Label.Text="Years";
 			de = new DataEngine();
 			de.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
 			de.DateGrouping = curTimeInterval ;
		    de.StartDate=new System.DateTime(2002,1,1,0,0,0);
    		de.EndDate = new System.DateTime(2002,12,31,23,59,59);
    		de.SqlStatement= @"SELECT OrderDate,Sum(Quantity) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate";
    		sc = de.GetSeries();
    		if(sc != null && sc.Count > 0)
    		{
    			seriesObj = sc[0];  		
    			foreach(Element el in seriesObj.Elements)
    			{
    				el.URL = "?dategrouping=quarters&startDate=" + startDate.ToString();
    				startDate = startDate.AddYears(1);
    			}
    		}
    	break;
    	case TimeInterval.Quarters:
 			Chart.XAxis.Label.Text="Quarters";
 			de = new DataEngine();
 			de.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
 			de.DateGrouping = curTimeInterval ;
		    de.StartDate=new System.DateTime(2002,1,1,0,0,0);
    		de.EndDate = new System.DateTime(2002,12,31,23,59,59);
    		de.SqlStatement= @"SELECT OrderDate,Sum(Quantity) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate";
    		sc = de.GetSeries();
    		if(sc != null && sc.Count > 0)
    		{
    			seriesObj = sc[0];  		
    			foreach(Element el in seriesObj.Elements)
    			{
    				el.URL = "?dategrouping=months&startDate=" + startDate.ToString();
    				startDate = startDate.AddMonths(3);
    			}
    		}
    	break;
    	case TimeInterval.Months:
 			Chart.XAxis.Label.Text="Months";
 			de = new DataEngine();
 			de.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
 			de.DateGrouping = curTimeInterval ;
		    de.StartDate=startDate;
		    endDate = startDate.AddMonths(3); 
		    endDate = endDate.AddSeconds(-1);
    		de.EndDate = endDate;
    		de.SqlStatement= @"SELECT OrderDate,Sum(Quantity) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate";
    		sc = de.GetSeries();
    		if(sc != null && sc.Count > 0)
    		{
    			seriesObj = sc[0];  		
    			foreach(Element el in seriesObj.Elements)
    			{
    				el.URL = "?dategrouping=days&startDate=" + startDate.ToString();
    				startDate = startDate.AddMonths(1);
    			}
    		}
    	break;
    	case TimeInterval.Days:
 			Chart.XAxis.Label.Text="Days";
 			de = new DataEngine();
 			de.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
 			de.DateGrouping = curTimeInterval ;
		    de.StartDate=startDate;
		    endDate = startDate.AddMonths(1); 
		    endDate = endDate.AddSeconds(-1);
    		de.EndDate = endDate;
    		de.SqlStatement= @"SELECT OrderDate,Sum(Quantity) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate";
    		sc = de.GetSeries();
    		if(sc != null && sc.Count > 0)
    		{
    			seriesObj = sc[0];  		
    			foreach(Element el in seriesObj.Elements)
    			{
    				el.URL = "?dategrouping=hours&startDate=" + startDate.ToString();
    				startDate = startDate.AddDays(1);
    			}
    		}
    	break;
    	case TimeInterval.Hours:
 			Chart.XAxis.Label.Text="hours";
 			de = new DataEngine();
 			de.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
 			de.DateGrouping = curTimeInterval ;
		    de.StartDate=startDate;
		    endDate = startDate.AddDays(1); 
		    endDate = endDate.AddSeconds(-1);
    		de.EndDate = endDate;
    		de.SqlStatement= @"SELECT OrderDate,Sum(Quantity) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate";
    		sc = de.GetSeries();
    	break;
    }

    Chart.SeriesCollection.Add(sc);
   
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Drill Down Manual Sample</title>
</head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
 
</div>
</body>
</html>
