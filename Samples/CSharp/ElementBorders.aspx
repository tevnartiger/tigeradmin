<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    void Page_Load(Object sender, EventArgs e)
    {
        // This sample demonstrates styling element borders and outlines.

        Chart.TempDirectory = "temp";
        Chart.Debug = true;

        Chart.Type = ChartType.Combo;
        Chart.Size = "600x350";
        Chart.Title = ".netCHARTING Sample";
        Chart.Use3D = true;
        
        Chart.DefaultElement.Transparency = 20;
        
        // Set the default element outline style.
        Chart.DefaultElement.ShapeType = ShapeType.Chat;
        Chart.DefaultElement.Outline.Color = Color.Red;
        Chart.DefaultElement.Outline.Width = 2;
        Chart.DefaultElement.Outline.DashStyle = DashStyle.Dash;

        // *DYNAMIC DATA NOTE* 
        // This sample uses random data to populate the chart. To populate 
        // a chart with database data see the following resources:
        // - Help File > Getting Started > Data Tutorials
        // - DataEngine Class in the help file	
        // - Sample: features/DataEngine.aspx

        SeriesCollection mySC = getRandomData();

        // Add the random data and specify the series types.
        Chart.SeriesCollection.Add(mySC);
        mySC[0].Type = SeriesType.Column;
        mySC[1].Type = SeriesType.BubbleShape;
        mySC[2].Type = SeriesType.Bar;
        mySC[3].Type = SeriesType.BarSegmented;
        mySC[4].Type = SeriesType.Cone;
        mySC[5].Type = SeriesType.Cylinder;
        mySC[6].Type = SeriesType.Pyramid;

    }

    SeriesCollection getRandomData()
    {
        Random myR = new Random(1);
        SeriesCollection SC = new SeriesCollection();
        int a = 0;
        int b = 0;
        for (a = 1; a < 8; a++)
        {
            Series s = new Series("Series " + a.ToString());
            for (b = 1; b < 5; b++)
            {
                Element e = new Element("Element " + b.ToString());
                e.YValue = 10+myR.Next(50);
                e.BubbleSize = myR.Next(50);
                s.Elements.Add(e);
            }
            SC.Add(s);
        }
        return SC;
    }

    SeriesCollection getLiveData()
    {
        DataEngine de = new DataEngine("ConnectionString goes here");
        de.ChartObject = Chart; // Necessary to view any errors the dataEngine may throw.
        de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ....";
        return de.GetSeries();
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <dotnet:Chart ID="Chart" runat="server" />
    </div>
</body>
</html>
