<%@ Page Language="C#" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    Chart.Title="Item sales";
    Chart.XAxis.Label.Text="Hours";
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.DateGrouping = TimeInterval.Day;
    Chart.EmptyElementText="N/A";
   
    
    //Add a series
    Chart.Series.Name="Items";
    Chart.Series.StoredProcedure = "spSalesLimit";
    //Set store procedure parameters, for third parameter other types are: Text,Date,Number,Currency,Double
    Chart.Series.AddParameter("@STARTDATE","3/10/02 12:00:00 AM",FieldType.Date);
    Chart.Series.AddParameter("@ENDDATE","3/10/02 11:59:59 PM",FieldType.Date);
    Chart.SeriesCollection.Add();
    
    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Orders Report</title>
</head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
