<%@ Page Language="C#" Description="dotnetCHARTING Component" Debug="true" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	//set global properties
	Chart.Title="2002 Sales (mouseover for sales amount)";
	Chart.Size = "600X300";
	Chart.TempDirectory="temp";
	Chart.Debug=true;
	Chart.YAxis.FormatString = "c";
	Chart.NoDataLabel.Text = "Click a sales persons yearly total to view a breakdown by months";

	//force vertical chart area layout
	Chart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical;

	//create a DataEngine to obtain data for two different series
	DataEngine de = new DataEngine();
	de.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
	de.StartDate=new DateTime (2002,1,1,0,0,0);
	de.EndDate = new DateTime (2002,12,31,23,59,59);

	//get a series for the default chart area and set its url for drilldown
	de.SqlStatement= @"SELECT Name, sum(Total) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.Name ORDER BY Orders.Name";
	SeriesCollection sc = de.GetSeries();
	sc[0].DefaultElement.URL = "chartareadb.aspx?elementname=%ElementName";
	sc[0].DefaultElement.ToolTip = "%Value";
 	Chart.SeriesCollection.Add(sc);
 	
	// Create the second chart area and add a series to its series collection.
	ChartArea ca2 = new ChartArea();

	//Add a series based on the person selected
    string en = Request.QueryString["elementname"];
    de.SqlStatement= @"SELECT Orders.OrderDate, Sum(Total) FROM Orders WHERE Orders.Name='"+en+"' AND OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate";
    de.DateGrouping = TimeInterval.Year;
	SeriesCollection sc2 = de.GetSeries();
	Chart.DefaultSeries.DefaultElement.ToolTip = "%Value";
	ca2.XAxis.Label.Text = en + "'s 2002 monthly breakdown";
	ca2.SeriesCollection.Add(sc2);

        // Add the new area to the chart.
	Chart.ExtraChartAreas.Add(ca2);	
   
}

</script>

	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
