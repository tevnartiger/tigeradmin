<%@ Page Language="C#" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	Chart.Type = ChartType.Combo;
	Chart.Width = 700;
	Chart.Height = 300;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = "Sample Data";
	
	SeriesCollection mySC = getRandomData();
	Chart.SeriesCollection.Add(mySC);
	Chart.DefaultSeries.Type = SeriesType.Spline;
    Chart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Horizontal;
	
	ChartArea sChartArea = new ChartArea ("S Chart");
	sChartArea.XAxis = new Axis ();
    sChartArea.LegendBox = new LegendBox();
    sChartArea.LegendBox.Visible = false;
    sChartArea.DefaultSeries.LegendEntry.Visible = false;
	Chart.ExtraChartAreas.Add (sChartArea);

	
	Series sChart = StatisticalEngine.SChart(mySC);
	sChartArea.SeriesCollection.Add (sChart);
	
	ChartArea sXBARChartArea = new ChartArea ("S XBAR");
	sXBARChartArea.XAxis = new Axis ();

	Chart.ExtraChartAreas.Add (sXBARChartArea);
    sXBARChartArea.LegendBox = new LegendBox();
    sXBARChartArea.LegendBox.Visible = false;
	
	Series sXBARChart = StatisticalEngine.SXBARChart(mySC);
	sXBARChartArea.SeriesCollection.Add (sXBARChart);
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(5);
	for(int a = 0; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 5; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	//SC[0].DefaultElement.Color = Color.FromArgb(255,99,49);
	//SC[1].DefaultElement.Color = Color.FromArgb(0,156,255);
	//SC[2].DefaultElement.Color = Color.FromArgb(49,255,49);
	//SC[3].DefaultElement.Color = Color.FromArgb(255,255,0);
	
	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
