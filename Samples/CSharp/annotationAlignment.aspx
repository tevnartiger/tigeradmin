<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	

	
	
	// This sample will demonstrate how to align text inside annotations.
	
	// First we get our data, if you would like to get the data from a database you need to use
	// the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
	SeriesCollection mySC = getData();
	
	// Create an annotation.
	Annotation a1 = new Annotation("Top-Right Aligned Text");
	
	// Set the horizontal alignment
	a1.Label.Alignment = StringAlignment.Far;
	
	// Set the Vertical Alignment alignment
	
    a1.Label.LineAlignment = StringAlignment.Near;
	
	mySC[0].Elements[0].Annotation = a1;

	
	// And do the same for two others
	Annotation a2 = new Annotation("Bottom-Right Aligned Text");
	
    a2.Label.Alignment = StringAlignment.Far;
	
    a2.Label.LineAlignment = StringAlignment.Far;
	mySC[0].Elements[1].Annotation = a2;
	
	Annotation a3 = new Annotation("Center Aligned Text");
	
    a3.Label.Alignment = StringAlignment.Center;
	
    a3.Label.LineAlignment = StringAlignment.Center;
	mySC[0].Elements[2].Annotation = a3;

	
	Annotation a4 = new Annotation("Top-Left Aligned Text");
	
    a4.Label.Alignment = StringAlignment.Near;
	
    a4.Label.LineAlignment = StringAlignment.Near;
	mySC[0].Elements[3].Annotation = a4;
	
	Annotation a5 = new Annotation("Bottom-Left Aligned Text");
	
    a5.Label.Alignment = StringAlignment.Near;
	a5.Label.LineAlignment = StringAlignment.Far;
	mySC[0].Elements[4].Annotation = a5;

		
	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
    
    
}

SeriesCollection getData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 2; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		Element e = new Element();
		e.Name = "Element 1";
		e.YValue = 5;
		s.Elements.Add(e);
		
		e = new Element();
		e.Name = "Element 2";
		e.YValue = 7;
		s.Elements.Add(e);
		
		e = new Element();
		e.Name = "Element 3";
		e.YValue = 3;
		s.Elements.Add(e);
		
		e = new Element();
		e.Name = "Element 4";
		e.YValue = 6;
		s.Elements.Add(e);
		
		e = new Element();
		e.Name = "Element 5";
		e.YValue = 3;
		s.Elements.Add(e);
		
		SC.Add(s);
	}
	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
