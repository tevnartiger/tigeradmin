<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    void Page_Load(Object sender, EventArgs e)
    {
        // This sample demonstrates using absolute positioning of several gauges on a single chart.

        Chart.TempDirectory = "temp";
        Chart.Debug = true;
        Chart.Palette = new Color[] { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) };
        Chart.Type = ChartType.Gauges;
        Chart.Size = "600x350";
	Chart.OverlapFooter = true;

        // Title Box Customization
        Chart.Title = ".netCHARTING Sample - Absolute Positioning";
        Chart.TitleBox.Label.Color = Color.White;
        Chart.TitleBox.Label.Shadow.Color = Color.FromArgb(105, 0, 0, 0);
        Chart.TitleBox.Label.Shadow.Depth = 2;
        Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Two;
        Chart.TitleBox.Background.Color = Color.FromArgb(100, 80, 180);

        Chart.ShadingEffectMode = ShadingEffectMode.Two;
        Chart.DefaultSeries.Background.Color = Color.White;
        Chart.LegendBox.Background = new Background(Color.White, Color.FromArgb(229, 233, 236), 45);

        // *DYNAMIC DATA NOTE* 
        // This sample uses random data to populate the chart. To populate 
        // a chart with database data see the following resources:
        // - Help File > Getting Started > Data Tutorials
        // - DataEngine Class in the help file	
        // - Sample: features/DataEngine.aspx

        SeriesCollection mySC = getRandomData();

        // Setup Gauges
        mySC[0].YAxis = new Axis();
        mySC[0].GaugeType = GaugeType.Circular;
        mySC[0].YAxis.SweepAngle = 90;
        mySC[0].YAxis.OrientationAngle = 270;

        mySC[1].YAxis = new Axis();
        mySC[1].GaugeType = GaugeType.Circular;
        mySC[1].YAxis.SweepAngle = 90;
        mySC[1].YAxis.OrientationAngle = 180;

        // Thermometers
        mySC[2].YAxis = new Axis();
        mySC[2].GaugeType = GaugeType.Vertical;
        mySC[2].GaugeLinearStyle = GaugeLinearStyle.Thermometer;

        mySC[3].YAxis = new Axis();
        mySC[3].GaugeType = GaugeType.Vertical;
        mySC[3].GaugeLinearStyle = GaugeLinearStyle.Thermometer;

        // Progress bars
        mySC[4].YAxis = new Axis();
        mySC[4].GaugeType = GaugeType.Horizontal;
        mySC[4].Type = SeriesType.BarSegmented;

        mySC[5].YAxis = new Axis();
        mySC[5].GaugeType = GaugeType.Horizontal;
        mySC[5].Type = SeriesType.BarSegmented;

        // Bars
        mySC[6].YAxis = new Axis();
        mySC[6].GaugeType = GaugeType.Bars;
        mySC[6].Palette = Chart.Palette;
        mySC[6].DefaultElement.LegendEntry.SortOrder = 3;

        LegendEntry le = new LegendEntry("Series 7", "");
        le.SortOrder = 2;
        le.PaddingTop = 9;
        le.DividerLine.Color = Color.Black;
        le.LabelStyle.Font = new Font("Arial", 8, FontStyle.Bold);
        Chart.LegendBox.ExtraEntries.Add(le);

        // Specify Absolute Positions for each gauge
        mySC[0].GaugeBorderBox.Position = new Rectangle(330, 40, 130, 130);
        mySC[1].GaugeBorderBox.Position = new Rectangle(330, 190, 130, 130);
        mySC[2].GaugeBorderBox.Position = new Rectangle(180, 50, 60, 260);
        mySC[3].GaugeBorderBox.Position = new Rectangle(260, 50, 60, 260);
        mySC[4].GaugeBorderBox.Position = new Rectangle(20, 50, 150, 50);
        mySC[5].GaugeBorderBox.Position = new Rectangle(20, 120, 150, 50);
        mySC[6].GaugeBorderBox.Position = new Rectangle(20, 190, 150, 120);

        // Add the random data.
        Chart.SeriesCollection.Add(mySC);
    }

    SeriesCollection getRandomData()
    {
        Random myR = new Random(1);
        SeriesCollection SC = new SeriesCollection();
        int a = 0, b = 0, c = 2;
        for (a = 1; a < 8; a++)
        {
            c = 2;
            if (a == 7) c = 5;
            Series s = new Series("Series " + a.ToString());
            for (b = 1; b < c; b++)
            {
                Element e = new Element("Element " + b.ToString());
                e.YValue = myR.Next(50);
                s.Elements.Add(e);
            }
            SC.Add(s);
        }
        return SC;
    }

    SeriesCollection getLiveData()
    {
        DataEngine de = new DataEngine("ConnectionString goes here");
        de.ChartObject = Chart; // Necessary to view any errors the dataEngine may throw.
        de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ....";
        return de.GetSeries();
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <dotnet:Chart ID="Chart" runat="server" />
    </div>
</body>
</html>
