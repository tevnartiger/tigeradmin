<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	//  This sample demonstrates image bars, title box customization,
	//  image background and axis label outline usage.

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Size = "500x350";
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = "Construction Housing Starts";

	
	// TitleBox Customization
	Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	Chart.TitleBox.Background = new Background(Color.White,Color.LightGray,90);
	Chart.TitleBox.Label.OutlineColor = Color.LightGray;
	Chart.TitleBox.CornerTopLeft = BoxCorner.Round;
	Chart.TitleBox.CornerTopRight = BoxCorner.Round;

	
	// The following line sets the image template.
	Chart.DefaultSeries.ImageBarTemplate = "../../Images/ImageBarTemplates/screw";

	// Background image for illusion of screws sinking into a wood board
	Chart.Background = new Background("../../Images/woodboard.jpg",BackgroundMode.Image); 

	// Enhance label visibility with an outline  
	Chart.XAxis.TickLabel.OutlineColor=Color.Orange;
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
        
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 2; a++)
	{
		Series s = new Series();
		s.Name = "Total " + a.ToString();
		for(int b = 1; b < 7; b++)
		{
			Element e = new Element();
			e.Name = "Street " + b.ToString();
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	

	return SC;
}
</script>



<html xmlns="http://www.w3.org/1999/xhtml"><head><title>.netCHARTING Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
