<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>

<script language="C#" runat="server">

  public void Page_Load(Object Source, EventArgs E)
  {
    //set global properties
    Chart.Title="Weekday Report";
    Chart.ChartArea.XAxis.Label.Text ="Days";
    Chart.ChartArea.YAxis.Label.Text="Dollars (Thousands)";
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.DefaultSeries.DefaultElement.ShowValue=true;

   Chart.ChartArea.XAxis.ReverseSeries=true;

   //Adding series programatically
   Series sr=new Series();
  
   sr.Name="Vancouver";
   sr.Type = SeriesType.Cylinder;
    
   Element el = new Element("Mon",2);
   sr.Elements.Add(el);
    el = new Element("Tue",4);
   sr.Elements.Add(el);
   el = new Element("Wed",5);
   sr.Elements.Add(el);
    el = new Element("Thr",6);
   sr.Elements.Add(el);
   el = new Element("Fri",5);
   sr.Elements.Add(el);
  Chart.SeriesCollection.Add(sr);
   
   sr=new Series();
   sr.Name="Seattle";
   sr.Type =SeriesType.AreaLine;
   el = new Element("Mon",5);
   sr.Elements.Add(el);
    el = new Element("Tue",8);
   sr.Elements.Add(el);
   el = new Element("Wed",6);
   sr.Elements.Add(el);
    el = new Element("Thr",7);
   sr.Elements.Add(el);
   el = new Element("Fri",6);
   sr.Elements.Add(el);
   Chart.SeriesCollection.Add(sr);

  }
</script>

 <dotnet:Chart id="Chart"  runat="server"/>

