<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Size = "700x350";
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = "Smart palette range coloring.";
	Chart.ChartArea.Label.Text = "Automatically specify colors for elements\nwithin a range defined by the SmartColor object.";
	Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;

	
	// This sample demonstrates how to use smart palettes to color elements with values within a specified range.
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();
	
	// Create smart palette.
	SmartPalette sp = new SmartPalette();
	
	// Create smart color with the range and color to use.
	SmartColor sc = new SmartColor(Color.Purple, new ScaleRange(new DateTime(2006,1,4),new DateTime(2006,1,8)),ElementValue.YDateTime);

	sc.LegendEntry.Visible = false;

	// Add the color to the palette, and the palette to the chart.
	sp.Add("Series 1", sc);	
	Chart.SmartPalette = sp;	
	
	
	Chart.YAxis.Markers.Add(new AxisMarker("",Color.FromArgb(100,Color.Purple),new DateTime(2006,1,4),new DateTime(2006,1,8)));
	Chart.YAxis.Markers[0].LegendEntry.Visible = false;
		
	//Chart.LegendBox.ExtraEntries.Add(new LegendEntry("10 < Elements < 30","",Color.Purple));
	
	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
        
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
	
	for(int a = 1; a < 2; a++)
	{
		DateTime dt = new DateTime(2006,1,1);
		Series s = new Series();
		s.Name = "Series " + a.ToString();
		for(int b = 1; b < 15; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b.ToString();
			//e.YValue = myR.Next(50);
			e.YDateTime = dt.AddDays(myR.Next(10));
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	//SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	//SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	//SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>
