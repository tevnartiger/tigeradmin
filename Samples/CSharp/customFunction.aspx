<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    Chart.Title="Sales";
    Chart.XAxis.Label.Text="Months";
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    
    //Using a custom function to convert yAxis values
    Chart.YAxis=Chart.YAxis.Calculate("Sales (USD) Thousands", new ChangeValueDelegate(MyFunction));
    
    Chart.Series.StartDate=new DateTime(2002,1,1,0,0,0);
    Chart.Series.EndDate = new DateTime(2002,12,31,23,59,59);
    Chart.DateGrouping = TimeInterval.Year;

       //Add a series
    Chart.Series.Name="sales";
    Chart.Series.SqlStatement= @"SELECT OrderDate,Total FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate";
    Chart.SeriesCollection.Add();
    
   
}
public static string MyFunction(string value)
{
	double dValue = Convert.ToDouble(value);
	dValue = dValue/1000;
	value = "$" + Convert.ToString(dValue) + " K";
	return value;
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Custom Function Sample</title>
</head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
