<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    Chart.Title="Sales for March 11,2002";
    Chart.ChartArea.XAxis.Label.Text="Hours";
    Chart.LegendBox.Template="%icon %name";
    Chart.ShowDateInTitle=false;

    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.ChartArea.XAxis.FormatString = "h tt";
 	
	Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
	Chart.DefaultSeries.StartDate=new System.DateTime(2002,3,11,0,0,0);
    Chart.DefaultSeries.EndDate = new System.DateTime(2002,3,11,23,59,59);

  
    //Add a series
    Chart.Series.Name="Items";
    Chart.Series.SqlStatement= @"SELECT OrderDate, Sum(Total) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate";
    Chart.Series.Visible=false;
    Chart.SeriesCollection.Add();
    
     //Add Sum series
    Chart.Series.Name = "Total";
    Chart.Series.DefaultElement.ShowValue=true;
    Chart.Series.Type = SeriesType.Line;
    Chart.SeriesCollection.Add(Calculation.RunningSum);

    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Running Sum</title>
</head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
