<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    void Page_Load(Object sender, EventArgs e)
    {
        // This sample demonstrates a number of background styling options which used to fill boxes and other objects.

        Chart.TempDirectory = "temp";
        Chart.Debug = true;
        Chart.Type = ChartType.Combo;
        Chart.Size = "500x380";
        Chart.ChartArea.ClearColors();
        Chart.ChartArea.Line.Color = Color.Black;
        Chart.Palette = new Color[] { Color.Transparent };
        Chart.DefaultShadow.Color = Color.Empty;
        Chart.LegendBox.Visible = false;
        Chart.DefaultAxis.ClearValues = true;
        Chart.DefaultElement.Outline.Color = Color.Transparent;

        Chart.DefaultBox.CornerBottomRight = BoxCorner.Round;
        Chart.DefaultBox.CornerTopLeft = BoxCorner.Round;

        Annotation a1 = getAnnotation("Shading One");
        Annotation a2 = getAnnotation("Shading Two");
        Annotation a3 = getAnnotation("Shading Three");
        Annotation a4 = getAnnotation("Shading Four");
        Annotation a5 = getAnnotation("Shading Five");
        Annotation a6 = getAnnotation("Bevel");
        Annotation a7 = getAnnotation("Brush");
        Annotation a8 = getAnnotation("Gradient");
        Annotation a9 = getAnnotation("Hatch");
        Annotation a10 = getAnnotation("Glass Effect");
        Annotation a11 = getAnnotation("Solid Color");
        Annotation a12 = getAnnotation("Image Background");

        a1.Position = new Point(50, 40);
        a2.Position = new Point(50, 90);
        a3.Position = new Point(50, 140);
        a4.Position = new Point(50, 190);
        a5.Position = new Point(50, 240);
        a6.Position = new Point(250, 40);
        a7.Position = new Point(250, 90);
        a8.Position = new Point(250, 140);
        a9.Position = new Point(250, 190);
        a10.Position = new Point(250, 240);
        a11.Position = new Point(50, 290);
        a12.Position = new Point(250, 290);

        Chart.Annotations.Add(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12);

        a1.Background.ShadingEffectMode = ShadingEffectMode.One;
        a2.Background.ShadingEffectMode = ShadingEffectMode.Two;
        a3.Background.ShadingEffectMode = ShadingEffectMode.Three;
        a4.Background.ShadingEffectMode = ShadingEffectMode.Four;
        a5.Background.ShadingEffectMode = ShadingEffectMode.Five;
        a6.Background.Bevel = true;

        PathGradientBrush pgb = new PathGradientBrush(new Point[] { new Point(250, 90), new Point(130, 90), new Point(400, 130), new Point(250, 130) });
        pgb.SurroundColors = Chart.Palette;
        pgb.CenterColor = Color.FromArgb(myRand.Next(120), myRand.Next(220), myRand.Next(255));
        pgb.CenterPoint = new PointF(275, 120);
        a7.Background = new Background(pgb);

        a8.Background = new Background(Color.FromArgb(myRand.Next(120), myRand.Next(220), myRand.Next(255)), Color.White, 90);//SecondaryColor = Color.White;
        a9.Background.HatchColor = Color.White;
        a9.Background.HatchStyle = HatchStyle.BackwardDiagonal;
        a10.Background.GlassEffect = true;
        a12.Background = new Background("../../images/back038.jpg");

        // *DYNAMIC DATA NOTE* 
        // This sample uses random data to populate the chart. To populate 
        // a chart with database data see the following resources:
        // - Help File > Getting Started > Data Tutorials
        // - DataEngine Class in the help file	
        // - Sample: features/DataEngine.aspx

        SeriesCollection mySC = getRandomData();

        // Add the random data.
        Chart.SeriesCollection.Add(mySC);
    }

    Random myRand = new Random(1);

    Annotation getAnnotation(string text)
    {
        Annotation an = new Annotation();
        an.Label.Text = text;
        an.Label.Font = new Font("Arial", 12, FontStyle.Bold);
        an.Label.OutlineColor = Color.White;
        an.Background.Color = Color.FromArgb(myRand.Next(120), myRand.Next(220), myRand.Next(255));
        an.Size = new Size(190, 40);
        return an;
    }

    SeriesCollection getRandomData()
    {
        Random myR = new Random(1);
        SeriesCollection SC = new SeriesCollection();
        int a = 0;
        int b = 0;
        for (a = 1; a < 2; a++)
        {
            Series s = new Series("Series " + a.ToString());
            for (b = 1; b < 2; b++)
            {
                Element e = new Element("Element " + b.ToString());
                e.YValue = myR.Next(50);
                s.Elements.Add(e);
            }
            SC.Add(s);
        }
        return SC;
    }

    SeriesCollection getLiveData()
    {
        DataEngine de = new DataEngine("ConnectionString goes here");
        de.ChartObject = Chart; // Necessary to view any errors the dataEngine may throw.
        de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ....";
        return de.GetSeries();
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <dotnet:Chart ID="Chart" runat="server" />
    </div>
</body>
</html>
