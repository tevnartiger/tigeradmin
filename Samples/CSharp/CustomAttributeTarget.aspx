<%@ Page Language="C#" Description="dotnetCHARTING Component" %>

<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	CellEmployeeID.Text = Request.QueryString["id"];
	CellEmployeeName.Text = Request.QueryString["name"];
	CellEmployeeSalary.Text = Request.QueryString["salary"];
	CellEmployeeDepartment.Text = Request.QueryString["department"];
	CellEmployeeEmail.Text = Request.QueryString["email"];
	CellEmployeePhone.Text = Request.QueryString["phone"];
	
	
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Employee information</title>
</head>
<body>

<p>&nbsp;</p>
<div style="text-align:center"><center>
<asp:Table id="TableEmployee"  cellpadding="2" bordercolor="Gray" BorderWidth="1" style="border-color:Gray;border-width:1px;border-style:solid;font-family:Arial;width:50%;border-collapse: collapse" runat="server">
<asp:TableRow style="background-color:SteelBlue;" forecolor="White" id="RowEmployeeHeader" runat="server">
    <ASP:TableCell Font-Bold="true" ColumnSpan="2" runat="server">Employee information:</ASP:TableCell>
 </asp:TableRow>
 <asp:TableRow id="RowEmployeeID" runat="server">
    <ASP:TableCell style="background-color:LightSteelBlue" HorizontalAlign="right" runat="server">Employee ID:</ASP:TableCell>
    <ASP:TableCell style="background-color:Silver" id="CellEmployeeID" runat="server">
</ASP:TableCell>
 </asp:TableRow>
 <asp:TableRow id="RowEmployeeName" runat="server">
    <ASP:TableCell  style="background-color:LightSteelBlue" HorizontalAlign="right" runat="server">Name:</ASP:TableCell>
    <ASP:TableCell style="background-color:Silver" id="CellEmployeeName" runat="server">
</ASP:TableCell>
 </asp:TableRow>
<asp:TableRow id="RowEmployeeSalary" runat="server">
    <ASP:TableCell  style="background-color:LightSteelBlue" HorizontalAlign="right" runat="server">Salary:</ASP:TableCell>
    <ASP:TableCell style="background-color:Silver" id="CellEmployeeSalary" runat="server">
</ASP:TableCell>
 </asp:TableRow>
<asp:TableRow id="RowEmployeeDepartment" runat="server">
    <ASP:TableCell  style="background-color:LightSteelBlue" HorizontalAlign="right" runat="server">Department:</ASP:TableCell>
    <ASP:TableCell style="background-color:Silver" id="CellEmployeeDepartment" runat="server">
</ASP:TableCell>
 </asp:TableRow>
<asp:TableRow id="RowEmployeeEmail" runat="server">
    <ASP:TableCell  style="background-color:LightSteelBlue" HorizontalAlign="right" runat="server">Email:</ASP:TableCell>
    <ASP:TableCell style="background-color:Silver" id="CellEmployeeEmail" runat="server">
</ASP:TableCell>
 </asp:TableRow>
<asp:TableRow id="RowEmployeePhone" runat="server">
    <ASP:TableCell  style="background-color:LightSteelBlue" HorizontalAlign="right" runat="server">Phone number:</ASP:TableCell>
    <ASP:TableCell style="background-color:Silver" id="CellEmployeePhone" runat="server">
</ASP:TableCell>
 </asp:TableRow>

 </asp:Table>
 </center></div>
</body>
</html>