<%@ Page Language="C#" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>

<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    Chart.Title="Item sales report";
    
    // Set the x axis label
	Chart.ChartArea.XAxis.Label.Text="X Axis Label";

	// Set the y axis label
	Chart.ChartArea.YAxis.Label.Text="Y Axis Label";
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    
    
   //Adding series programatically
   	Chart.Series.Name = "Item sales";
   	Chart.Series.DataFields="xAxis=Name,yAxis=Total";
   	Chart.Series.Data = CreateObjectCollection();
    Chart.SeriesCollection.Add();
   
   
}
Products CreateObjectCollection()
{
	Products myProducts = new Products();  
	myProducts.Add(new Product("P1",23));
	myProducts.Add(new Product("P2",12));
	myProducts.Add(new Product("P3",20));
	myProducts.Add(new Product("P4",65));
	myProducts.Add(new Product("P5",50));
	myProducts.Add(new Product("P6",40));
	
    return myProducts;
}
public class Products : System.Collections.CollectionBase
{
	public int Add(Product newProduct)
	{
		return List.Add(newProduct);
		
	}

}
public class Product
{
	string name;
	double total;
	public Product()
	{
	}
	public Product(string proName,double proTotal)
	{
		name = proName;
		total = proTotal;
	}

	
	public string Name
	{
		get
		{
			return name;
		}
		set
		{
			name = value;
		}
	}
	public double Total
	{
		get
		{
			return total;
		}
		set
		{
			total = value;
		}
	}


}





</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Custom collection Sample</title>
</head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
