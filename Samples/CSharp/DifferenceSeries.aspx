<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	
	// This sample will demonstrate how a calculated series can be created and placed in a chart area.
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();
	
	ChartArea ca1 = new ChartArea();
	ChartArea ca2 = new ChartArea();
	ca2.Label.Text = "Difference";	
	
	// Create the difference series.
	Series diffSeries = mySC[1] - mySC[0];
	diffSeries.DefaultElement.Color = Color.Red;
	diffSeries.Name = "Difference";
	
	// Iterate the elements and change their colors based on value.	
	foreach(Element el in diffSeries.Elements)
		if(el.YValue > 0)
			el.Color = Color.Green;
		else
			el.Color = Color.Red;
	
	// Add the series and chart areas to the chart.
	Chart.SeriesCollection.Add(mySC[0]);	
	ca1.SeriesCollection.Add(mySC[1]);
	ca2.SeriesCollection.Add(diffSeries);
	
	Chart.ExtraChartAreas.Add(ca1, ca2);

}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
	for(int a = 0; a < 2; a++)
	{
		Series s = new Series();
		s.Name = "Series " + (a+1);
		for(int b = 0; b < 15; b++)
		{
			Element e = new Element();
			e.Name = "Element " + (b+1);
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);


	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
