<%@ Page Language="C#" Debug="true" Trace="false" Description="dotnetChart Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Web.UI" %>

<script runat="server">

private TimeInterval myInterval = TimeInterval.Week;

void Page_Load(Object sender,EventArgs e)
{
	Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	Chart.Use3D = true;
	Chart.Size = "600x350";
	Chart.TempDirectory = "temp";
	Chart.Debug = true;	
		
	// This sample demonstrates the smart forecast feature.
	// Fullscreen
	
	if(!IsPostBack)
	{
		// Setup a dropdown
		DropDownForecast.Items.Add("Week");
		DropDownForecast.Items.Add("Month");
		DropDownForecast.Items.Add("Quarter");
	}
    
    
    // Load form values.    
    myInterval = (dotnetCHARTING.TimeInterval)Enum.Parse(typeof(dotnetCHARTING.TimeInterval),DropDownForecast.SelectedItem.Value,true);
    
    // The smart forecast setting takes a time interval advanced object representing the time interval of the forcast.
	Chart.SmartForecast = new TimeIntervalAdvanced(myInterval,1);
	

	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	
	SeriesCollection mySC = getRandomData();
	Series s = mySC[0];
	
	// Empty Element settings control the apperance of the forecast series.
	s.EmptyElement.Color = Color.FromArgb(150,Chart.Palette[0]);
	s.EmptyElement.Line.Color = Color.FromArgb(93,92,153);
	s.EmptyElement.Hotspot.ToolTip = "%Value";
	
	
	s.LegendEntry.Value = "%Sum";

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
	Chart1.SeriesCollection.Add(getRandomData());
	
	Chart1.Title = "Long-term data used with the above chart without SmartForecasting.";
	Chart1.ChartArea.Label.Text = "This data is used with the above \nchart to improve forecasting accuracy.";
    Chart1.Size = "600x200";
    Chart1.TempDirectory = "temp";
    Chart1.Debug = true;	
    Chart1.LegendBox.Visible = false;
    
}

void ChangeForecast(Object sender, EventArgs e)
{
	myInterval = (dotnetCHARTING.TimeInterval)Enum.Parse(typeof(dotnetCHARTING.TimeInterval),DropDownForecast.SelectedItem.Value,true);
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(2);
	DateTime dt = (DateTime)(DateTime.Today - TimeSpan.FromDays(199));
	for(int a = 1; a < 2; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a.ToString();
		for(int b = 1; b < 200; b++)
		{
			Element e = new Element();
			//e.Name = "Element " + b.ToString();
			e.YValue = myR.Next(50)+b;
			e.XDateTime = dt = dt.AddDays(1);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}


	return SC;
}

</script>
		<html>
	<head>
		<title>.netCHARTING Sample</title>
<script language="JavaScript">
function submitform()
{
	document.Form1.submit();
}
</script> 

	</head>
	<body>
		<div align="center">
		
		<form id="Form1"  runat=server>
			Forecast Setting: <ASP:DropDownList id="DropDownForecast" OnSelectedIndexChanged="ChangeForecast" onChange="Javascript: submitform()" runat=server></ASP:DropDownList>
			</form>
		
			<dotnet:Chart id="Chart" runat="server" />
			<dotnet:Chart id="Chart1" runat="server" />
			
		</div>
	</body>
</html>
