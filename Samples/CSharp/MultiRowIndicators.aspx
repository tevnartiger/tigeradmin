<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    void Page_Load(Object sender, EventArgs e)
    {
        // This sample demonstrates using multiple rows of indicator lights on a chart.

        Chart.TempDirectory = "temp";
        Chart.Debug = true;
        Chart.Palette = new Color[] { Color.FromArgb(49, 255, 49), Color.FromArgb(0, 156, 255), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49) };

        Chart.Type = ChartType.Gauges;
        Chart.DefaultSeries.GaugeType = GaugeType.IndicatorLight;
        Chart.Size = "230x400";
        Chart.Title = ".netCHARTING Sample";
        Chart.LegendBox.Visible = false;

        Chart.DefaultElement.Marker.Size = 35;
        Chart.DefaultAxis.DefaultTick.Label.Font = new Font("Arial", 12, FontStyle.Italic);
        Chart.DefaultSeries.GaugeBorderBox.ClearColors();
        Chart.DefaultElement.SmartLabel.Type = LabelType.Digital;
        Chart.DefaultElement.SmartLabel.Color = Color.White;
        Chart.DefaultElement.SmartLabel.Font = new Font("Arial", 15, FontStyle.Bold);
        Chart.DefaultElement.ShowValue = true;
        Chart.DefaultElement.SmartLabel.Alignment = LabelAlignment.Center;

        SmartColor sc = new SmartColor(Color.Red, new ScaleRange(0, 0));
        sc.LegendEntry.Visible = false;
        Chart.SmartPalette.Add("Series 1", sc);
        Chart.SmartPalette.Add("*", new SmartColor(Color.Orange, new ScaleRange(0, 50)));

        // *DYNAMIC DATA NOTE* 
        // This sample uses random data to populate the chart. To populate 
        // a chart with database data see the following resources:
        // - Use the getLiveData() method using the dataEngine to query a database.
        // - Help File > Getting Started > Data Tutorials
        // - DataEngine Class in the help file	
        // - Sample: features/DataEngine.aspx

        SeriesCollection mySC = getRandomData();

        mySC[1].Name = "Change";
        mySC[1].DefaultElement.ShowValue = false;
        mySC[1].DefaultElement.ForceMarker = true;

        mySC[0].XAxis = new Axis();
        mySC[0].DefaultElement.SmartLabel.Alignment = LabelAlignment.Center;
        mySC[0].DefaultElement.ShowValue = true;

        foreach (Element el in mySC[1].Elements)
        {
            if (el.YValue > 40 && el.YValue < 60)
            {
                el.Marker = new ElementMarker("../../images/minusSign.png");
            }
            else if (el.YValue >= 60)
            {
                el.Color = Chart.Palette[0];
                el.Marker = new ElementMarker("../../images/upArrow.png");
            }
            else if (el.YValue <= 40)
            {
                el.Color = Chart.Palette[3];
                el.Marker = new ElementMarker("../../images/downArrow.png");
            }
        }

        // Add the random data.
        Chart.SeriesCollection.Add(mySC);
    }

    SeriesCollection getRandomData()
    {
        Random myR = new Random(3);
        SeriesCollection SC = new SeriesCollection();
        int a = 0;
        int b = 0;
        for (a = 1; a < 3; a++)
        {
            Series s = new Series("Series " + a.ToString());
            s.XAxis = new Axis();
            s.XAxis.ClearValues = true;
            s.DefaultElement.ShowValue = true;

            for (b = 1; b < 8; b++)
            {
                Element e = new Element("Element " + b.ToString());
                e.YValue = myR.Next(100);
                if (myR.Next(3) == 2 && a == 1)
                    e.YValue = 0;
                s.Elements.Add(e);
            }
            SC.Add(s);
        }
        return SC;
    }

    SeriesCollection getLiveData()
    {
        DataEngine de = new DataEngine("ConnectionString goes here");
        de.ChartObject = Chart; // Necessary to view any errors the dataEngine may throw.
        de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ....";
        return de.GetSeries();
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <dotnet:Chart ID="Chart" runat="server" />
    </div>
</body>
</html>
