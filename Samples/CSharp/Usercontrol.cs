using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using dotnetCHARTING;

public class ChartClass : UserControl
{
	public dotnetCHARTING.Chart ChartObj;
	public void Page_Load(Object Source, EventArgs E)
	{
		//set global properties
		ChartObj.Title="Weekday Report";
		ChartObj.ChartArea.XAxis.Label.Text ="Days";
		ChartObj.ChartArea.YAxis.Label.Text="Dollars (Thousands)";
		ChartObj.TempDirectory="temp";
		ChartObj.Debug=true;
		ChartObj.DefaultSeries.DefaultElement.ShowValue=true;

		ChartObj.ChartArea.XAxis.ReverseSeries=true;

		//Adding series programatically
		Series sr=new Series();
  
		sr.Name="Vancouver";
		sr.Type = SeriesType.Cylinder;
    
		Element el = new Element("Mon",2);
		sr.Elements.Add(el);
		el = new Element("Tue",4);
		sr.Elements.Add(el);
		el = new Element("Wed",5);
		sr.Elements.Add(el);
		el = new Element("Thr",6);
		sr.Elements.Add(el);
		el = new Element("Fri",5);
		sr.Elements.Add(el);
		ChartObj.SeriesCollection.Add(sr);
   
		sr=new Series();
		sr.Name="Seattle";
		sr.Type =SeriesType.AreaLine;
		el = new Element("Mon",5);
		sr.Elements.Add(el);
		el = new Element("Tue",8);
		sr.Elements.Add(el);
		el = new Element("Wed",6);
		sr.Elements.Add(el);
		el = new Element("Thr",7);
		sr.Elements.Add(el);
		el = new Element("Fri",6);
		sr.Elements.Add(el);
		ChartObj.SeriesCollection.Add(sr);

   }

  }
