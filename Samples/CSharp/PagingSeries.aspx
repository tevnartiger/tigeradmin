<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="C#" Debug="true" Trace="false" EnableSessionState="true" Description="dotnetCHARTING Component" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Paging series</title>
		<script runat="server">
int currentPage = 1 ;
int pageSize=5;
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    Chart.Width = 600;
    Chart.Height = 300;
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.Title="Item sales";   
    Chart.TitleBox.Position = TitleBoxPosition.Right;
    Chart.TitleBox.ClearColors();
    Chart.XAxis.Label.Text="Year";
    
    

	// Get the page from the query string.
    if(Request.QueryString["Page"]!=null)
    	currentPage = Convert.ToInt32(Request.QueryString["Page"]);
    
	Chart.PaletteName=(Palette)currentPage;	
	//set global database properties
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    Chart.DateGrouping = TimeInterval.Years;
    
     //Add a series
    Chart.Series.Name="Items 1";
    Chart.Series.StartDate=new System.DateTime(2002,1,1,0,0,0);
    Chart.Series.EndDate = new System.DateTime(2002,12,31,23,59,59);
    Chart.Series.SqlStatement= @"SELECT OrderDate,Sum(Quantity),Name FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate,Name ORDER BY Orders.OrderDate";  							
    Chart.SeriesCollection.Add();
   
    Chart.DefaultSeries.SplitByLimit= currentPage.ToString() + "x" + pageSize.ToString();       
	Chart.DefaultSeries.LimitMode = dotnetCHARTING.LimitMode.Top;
	Chart.DefaultSeries.ShowOther=false;
	
	
	// Create an annotation button          
	Annotation an2 = new Annotation(" < Previous");
	setAnnotation(an2);
	
	an2.Position = new Point(49,20);
	an2.CornerTopLeft = BoxCorner.Cut;
	an2.CornerTopRight = BoxCorner.Square;
	an2.Size = new Size(85,16);
	Chart.Annotations.Add(an2);
	
	// Activate it
	if(currentPage > 1)
	{
		an2.URL = "Pagingseries.aspx?page=" + Convert.ToString(currentPage-1);
	}
	else
		an2.Label.Color = Color.Gray;
		
 
 


   
    
Chart.PostDataProcessing +=new PostDataProcessingEventHandler(OnPostDataProcessing);

}

void setAnnotation(Annotation an)
{
	// Set default annotation properties
	an.DefaultCorner = BoxCorner.Square;
	an.Size = new Size(66,16);
	an.Background = new Background(Color.FromArgb(241,250,180), Color.White,90);
	an.Label.Font = new Font("Verdana",7.7f,FontStyle.Bold);
	an.CornerTopRight = BoxCorner.Cut;
	an.Padding = 2;
	
}

void OnPostDataProcessing(Object sender)
{
	int totalPage = Chart.TotalSeries%pageSize==0?Chart.TotalSeries/pageSize:(Chart.TotalSeries/pageSize)+1;
	
	//Create the next button annotation
	Annotation an = new Annotation("Next >");
	setAnnotation(an);
	an.Position = new Point(224,20);
	
	// Activate it
	if(totalPage>currentPage)
	{
		an.URL = "Pagingseries.aspx?page=" + Convert.ToString(currentPage+1);
	}
	else
		an.Label.Color = Color.Gray;
	
	// 	
	Annotation an2 = new Annotation("(Page "+currentPage+" of "+totalPage+")");
	
	
	setAnnotation(an2);
	an2.Size = new Size(90,16);
	an2.Position = new Point(134,20);
	an2.CornerTopRight = BoxCorner.Square;
	an2.Label.Font = new Font("Arial",7.7f);
	an2.Label.Color = Color.DarkBlue;
		
	Chart.Annotations.Add(an,an2);
	
}

		</script>
    <style type="text/css">
      body.myBody  
      {
       border-top-width:0px;
       border-left-width:0px;
     
     }
     </style>
	</head>
	<body class="myBody">
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" />
		</div>
	</body>
</html>
