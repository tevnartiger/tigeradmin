<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    void Page_Load(Object sender, EventArgs e)
    {
        // This sample demonstrates how to set up a timeline during which a machine is shown to be on or off.

        Chart.TempDirectory = "temp";
        Chart.Debug = true;
        Chart.Palette = new Color[] { Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) };

        Chart.LegendBox.Visible = false;
        Chart.Type = ChartType.Combo;
        Chart.Size = "700x200";
        Chart.Title = "Pump Activity";
        Chart.Background.Color = Color.Transparent;

        Chart.YAxis.ScaleRange = new ScaleRange(0, 1);
        Chart.YAxis.DefaultTick.Label.Font = new Font("Arial", 15, FontStyle.Bold);
        Chart.YAxis.ClearValues = true;
        Chart.YAxis.ExtraTicks.Add(new AxisTick(0, "Off"), new AxisTick(1, "On"));

        Chart.XAxis.TimeScaleLabels.Mode = TimeScaleLabelMode.Smart;
        Chart.XAxis.TimeScaleLabels.RangeMode = TimeScaleLabelRangeMode.Dynamic;
        Chart.XAxis.TimeScaleLabels.DayFormatString = "ddd d";
        Chart.XAxis.SpacingPercentage = 0;

        Chart.DefaultElement.ForceMarker = true;
        Chart.DefaultElement.Marker.Size = 5;
        Chart.DefaultElement.Outline.Color = Color.Green;

        Chart.SmartPalette.Add("*", new SmartColor(Color.Red, new ScaleRange(0, 0)));
        Chart.SmartPalette.Add("*", new SmartColor(Color.Green, new ScaleRange(1, 1)));

        // *DYNAMIC DATA NOTE* 
        // This sample uses random data to populate the chart. To populate 
        // a chart with database data see the following resources:
        // - Use the getLiveData() method using the dataEngine to query a database.
        // - Help File > Getting Started > Data Tutorials
        // - DataEngine Class in the help file	
        // - Sample: features/DataEngine.aspx

        SeriesCollection mySC = getRandomData();

        // Add the random data.
        Chart.SeriesCollection.Add(mySC);
    }

    SeriesCollection getRandomData()
    {
        Random myR = new Random(1);
        SeriesCollection SC = new SeriesCollection();
        DateTime dt = new DateTime(2009, 1, 1);
        int a = 0;
        int b = 0;
        for (a = 1; a < 2; a++)
        {
            Series s = new Series("Series " + a.ToString());
            for (b = 1; b < 100; b++)
            {
                Element e = new Element();
                e.XDateTime = dt.AddHours(b);
                if (e.XDateTime.Hour < 12)
                    e.YValue = 0;
                else
                    e.YValue = myR.Next(2);
                s.Elements.Add(e);
            }
            SC.Add(s);
        }
        return SC;
    }

    SeriesCollection getLiveData()
    {
        DataEngine de = new DataEngine("ConnectionString goes here");
        de.ChartObject = Chart; // Necessary to view any errors the dataEngine may throw.
        de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ....";
        return de.GetSeries();
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <dotnet:Chart ID="Chart" runat="server" />
    </div>
</body>
</html>
