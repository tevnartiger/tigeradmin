<%@ Page Language="C#" Debug="True" Trace="false" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Web.UI"%>


<script runat="server">
//This sample demonstrates how to use the reloaded data during the post back request.
void Page_Load(Object sender,EventArgs e)
{
   if(Page.IsPostBack==false)
   {
   	  UserID.Text = Guid.NewGuid().ToString();
      CreateChart();
   }
   else
   {
    		//use the old image
   		System.Web.UI.WebControls.Image chartImage = new System.Web.UI.WebControls.Image();
   		chartImage.ID = "PostBackChart";
		string basePath = Request.Path;
		basePath = basePath.Substring(0,basePath.LastIndexOf("/"));
   		chartImage.ImageUrl = basePath + "/temp/chart" + UserID.Text + ".png";
   		chartHolder.Controls.Add(chartImage);
   }
  
}
SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			e.YValue = 25 + myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
void CreateChart()
{
	dotnetCHARTING.Chart myChart = new  dotnetCHARTING.Chart();
	myChart.Size="600x350";
	myChart.TempDirectory = "temp";
	myChart.FileName="chart" + UserID.Text;
	myChart.DisableBrowserCache=false;

	
 	// Set the title.
   myChart.Title = "My Chart";
      
   // Set the x axis label
   myChart.ChartArea.XAxis.Label.Text = "X Axis Label";
   
   // Set the y axis label
   myChart.ChartArea.YAxis.Label.Text = "Y Axis Label";
   
   // Set the bar shading effect
   myChart.ShadingEffect = true;
      
   // Add the random data.
   myChart.SeriesCollection.Add(getRandomData());
   
   chartHolder.Controls.Add(myChart);
}   

</script>
<html>
<head>
<title>Postback Sample</title>
</HEAD>
<BODY>
<center>
<form id="postback" action="postback.aspx" runat="server">
<asp:TextBox Visible="False" id="UserID" runat="server"></asp:TextBox>
<table>
<tr><td>
<asp:PlaceHolder ID="chartHolder" Runat="server" ></asp:PlaceHolder>
 </tr></td>
 <tr><td align="center">
 <asp:Button Text="Get Chart" Runat=server/>
  </tr></td>
 </form>
 </table>
</center>
</BODY>
</HTML>