<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    void Page_Load(Object sender, EventArgs e)
    {
        // This sample demonstrates specifying a rectangle where the gauge will render.

        Chart.TempDirectory = "temp";
        Chart.Debug = true;
        Chart.Palette = new Color[] { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) };
        Chart.Type = ChartType.Gauges;
        Chart.Size = "600x450";
        Chart.Title = ".netCHARTING Sample";
        Chart.LegendBox.Visible = false;
        
        Chart.DefaultAxis.SweepAngle = 90;
        Chart.DefaultSeries.GaugeBorderBox.Background.Color = Color.White;

        // *DYNAMIC DATA NOTE* 
        // This sample uses random data to populate the chart. To populate 
        // a chart with database data see the following resources:
        // - Use the getLiveData() method using the dataEngine to query a database.
        // - Help File > Getting Started > Data Tutorials
        // - DataEngine Class in the help file	
        // - Sample: features/DataEngine.aspx

        SeriesCollection mySC = getRandomData();
        mySC[0].YAxis = new Axis();
        mySC[1].YAxis = new Axis();
        mySC[2].YAxis = new Axis();
        mySC[0].YAxis.OrientationAngle = 90;
        mySC[2].YAxis.OrientationAngle = 180;

        mySC[0].GaugeBorderBox.Position = new Rectangle(new Point(20, 50), new Size(150, 150));
        mySC[1].GaugeBorderBox.Position = new Rectangle(new Point(20, 260), new Size(150, 150));
        mySC[2].GaugeBorderBox.Position = new Rectangle(new Point(420, 50), new Size(150, 150));

        // Add the random data.
        Chart.SeriesCollection.Add(mySC);
    }

    SeriesCollection getRandomData()
    {
        Random myR = new Random(1);
        SeriesCollection SC = new SeriesCollection();
        int a = 0;
        int b = 0;
        for (a = 1; a < 4; a++)
        {
            Series s = new Series("Series " + a.ToString());
            for (b = 1; b < 2; b++)
            {
                Element e = new Element("Element " + b.ToString());
                e.YValue = myR.Next(50);
                s.Elements.Add(e);
            }
            SC.Add(s);
        }
        return SC;
    }

    SeriesCollection getLiveData()
    {
        DataEngine de = new DataEngine("ConnectionString goes here");
        de.ChartObject = Chart; // Necessary to view any errors the dataEngine may throw.
        de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ....";
        return de.GetSeries();
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <dotnet:Chart ID="Chart" runat="server" />
    </div>
</body>
</html>
