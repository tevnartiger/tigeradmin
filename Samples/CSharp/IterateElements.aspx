<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	
	return SC;
}


void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	
	
	// This sample demonstrates how you can iterate through elements and change their color
	// based on the data.
	
	// We'll set the default type to line so it can be viewed better.
	Chart.DefaultSeries.Type = SeriesType.Line;
	
	// We'll also make the markers bigger so they can be seen better.
	Chart.DefaultSeries.DefaultElement.Marker.Size = 13;
	
	// First we get our data, if you would like to get the data from a database you need to use
	// the data engine. See sample: features/dataEngine.aspx.
	
	SeriesCollection sc = getRandomData();
	
	// The elements values are between 0 and 50 so we'll make any elements outside on 10 to 40 red and within green.
	foreach(Series s in sc)
		foreach(Element el in s.Elements)
			if(el.YValue < 10 || el.YValue > 40)
				el.Color = Color.Red;
			else
				el.Color = Color.Green;
	
	

	// Add the random data.
	Chart.SeriesCollection.Add(sc);
    
    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Interate Elements</title>
</head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
