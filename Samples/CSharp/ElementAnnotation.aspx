<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Element Annotation</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	
	
	// This sample demonstrates how to attach an annotation to an element on the chart. 
	// This can be any element in any chart type.
	
	
	// First we get our data, if you would like to get the data from a database you need to use
	// the data engine. See sample: features/dataEngine.aspx.
	SeriesCollection sc = getRandomData();
	
	// Instantiate the annotation with the initial text. Because this annotation will be associated with an element
	// we can use all the element tokens in the text, toolTip and even url of this annotation.
	Annotation a = new Annotation("Element: %Name \n Series: %SeriesName \n Value: %YValue");
	
	// Set the background color.
	a.Background.Color = Color.FromArgb(200,180,180,220);
	
	// Bevel the background.
	a.Background.Bevel = true;

	// Orient the annotation be on the top left side of our element.
	a.Orientation = dotnetCHARTING.Orientation.TopLeft;
	
	// Make all the corners round.
	a.DefaultCorner = BoxCorner.Round;
	
	// Dont dynamically size the annotation.
	a.DynamicSize = false;
	
	
	// Assign the annotation to an element in the sc collection.
	sc[0].Elements[2].Annotation = a;
	
	// Add the random data.
	Chart.SeriesCollection.Add(sc);
    
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px" Depth="13px" Use3D="True">
				
			</dotnet:Chart>
		</div>
	</body>
</html>
