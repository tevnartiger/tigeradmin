<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    Chart.Title="Acme Company 2003 Sales Summary";
    Chart.YAxis.Label.Text="Sales (USD)";
    Chart.ShadingEffect = true;
    Chart.Use3D = true;
    Chart.XAxis.SpacingPercentage = 30;
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.YAxis.FormatString = "Currency";
        
    //Add a series
    Chart.Series.Name="Items";
    Chart.Series.Element.Transparency = 20;
    Chart.Series.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;data source=" + Server.MapPath("../../database/sales.xls")+ ";Extended Properties=\"Excel 8.0;\"";
    Chart.Series.SqlStatement= @"Select Periods,Sales from [Yearly Summary$]";    							
    Chart.SeriesCollection.Add();
    
    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Excel Sample</title>
</head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
