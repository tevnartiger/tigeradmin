<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    void Page_Load(Object sender, EventArgs e)
    {
        // This sample demonstrates a number box corner stylin options.

        Chart.TempDirectory = "temp";
        Chart.Debug = true;
        Chart.Type = ChartType.Combo;
        Chart.Size = "290x380";
        Chart.ChartArea.ClearColors();
        Chart.ChartArea.Line.Color = Color.Black;
        Chart.Palette = new Color[] { Color.Transparent };
        Chart.DefaultShadow.Color = Color.Empty;
        Chart.LegendBox.Visible = false;
        Chart.DefaultAxis.ClearValues = true;
        Chart.DefaultElement.Outline.Color = Color.Transparent;

        Annotation a1 = getAnnotation("Cut");
        Annotation a2 = getAnnotation("Rounded");
        Annotation a3 = getAnnotation("Square");
        Annotation a4 = getAnnotation("Cut 5px");
        Annotation a5 = getAnnotation("Rounded 5px");
        Annotation a6 = getAnnotation("Mixed");

        a1.Position = new Point(50, 40);
        a2.Position = new Point(50, 90);
        a3.Position = new Point(50, 140);
        a4.Position = new Point(50, 190);
        a5.Position = new Point(50, 240);
        a6.Position = new Point(50, 290);

        Chart.Annotations.Add(a1, a2, a3, a4, a5, a6 );
        a1.DefaultCorner = BoxCorner.Cut;
        a2.DefaultCorner = BoxCorner.Round;
        a3.DefaultCorner = BoxCorner.Square;
        a4.DefaultCorner = BoxCorner.Cut;
        a5.DefaultCorner = BoxCorner.Round;
        a6.DefaultCorner = BoxCorner.Cut;
        a6.CornerTopRight = BoxCorner.Round;
        a6.CornerBottomLeft = BoxCorner.Square;

        a4.CornerSize = 5;
        a5.CornerSize = 5;

        // *DYNAMIC DATA NOTE* 
        // This sample uses random data to populate the chart. To populate 
        // a chart with database data see the following resources:
        // - Help File > Getting Started > Data Tutorials
        // - DataEngine Class in the help file	
        // - Sample: features/DataEngine.aspx

        SeriesCollection mySC = getRandomData();

        // Add the random data.
        Chart.SeriesCollection.Add(mySC);
    }

    Random myRand = new Random(1);

    Annotation getAnnotation(string text)
    {
        Annotation an = new Annotation();
        an.Label.Text = text;
        an.Label.Font = new Font("Arial", 12, FontStyle.Bold);
        an.Label.OutlineColor = Color.White;
        an.Background.Color = Color.FromArgb(100, 230, 125);
        //an.Background.Color = Color.FromArgb(myRand.Next(120), myRand.Next(220), myRand.Next(255));
        an.Size = new Size(190, 40);
        return an;
    }

    SeriesCollection getRandomData()
    {
        Random myR = new Random(1);
        SeriesCollection SC = new SeriesCollection();
        int a = 0;
        int b = 0;
        for (a = 1; a < 2; a++)
        {
            Series s = new Series("Series " + a.ToString());
            for (b = 1; b < 2; b++)
            {
                Element e = new Element("Element " + b.ToString());
                e.YValue = myR.Next(50);
                s.Elements.Add(e);
            }
            SC.Add(s);
        }
        return SC;
    }

    SeriesCollection getLiveData()
    {
        DataEngine de = new DataEngine("ConnectionString goes here");
        de.ChartObject = Chart; // Necessary to view any errors the dataEngine may throw.
        de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ....";
        return de.GetSeries();
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <dotnet:Chart ID="Chart" runat="server" />
    </div>
</body>
</html>
