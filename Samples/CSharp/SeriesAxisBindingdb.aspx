<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    Chart.Title="Orders and Sales by Customers";
    Chart.XAxis.Label.Text="Customers";
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.ChartArea.Shadow.Depth=10;
    Chart.Width=400;
    
	    
    //Add a series
    Chart.Series.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    Chart.Series.Name="Orders";
    Chart.Series.Limit ="5";
    Chart.Series.SqlStatement= @"SELECT Name,Sum(1) FROM Orders GROUP BY Name ORDER BY Name DESC";
    Chart.Series.Type = SeriesType.Cylinder;
    Chart.SeriesCollection.Add();
    
    //Add a series
    Chart.Series.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    Chart.Series.Name="Sales";
    Chart.Series.Limit ="5";
    Chart.Series.SqlStatement= @"SELECT Name,Sum(Total) FROM Orders GROUP BY Name ORDER BY Name DESC";
    Chart.Series.Type = SeriesType.Cylinder;
    Chart.SeriesCollection.Add();

    
	//set global properties
    Chart2.Title="Orders and Sales by Customers";
    Chart2.XAxis.Label.Text="Customers";
    Chart2.YAxis.Label.Text="Orders";
    Chart2.TempDirectory="temp";
    Chart2.Debug=true;
    Chart2.ChartArea.Shadow.Depth=10;
    Chart2.Width=540;


    
    //Add a series
    Chart2.Series.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    Chart2.Series.Name="Orders";
    Chart2.Series.Limit ="5";
    Chart2.Series.SqlStatement= @"SELECT Name,Sum(1) FROM Orders GROUP BY Name ORDER BY Name DESC";
    Chart2.Series.Type = SeriesType.Cylinder;
    Chart2.SeriesCollection.Add();
    
     //Add a series
    Chart2.Series.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    Chart2.Series.Name="Sales";
    Chart2.Series.Limit ="5";
    Chart2.Series.SqlStatement= @"SELECT Name,Sum(Total) FROM Orders GROUP BY Name ORDER BY Name DESC";
    Chart2.Series.Type = SeriesType.Cylinder;
     Axis ATotal = new Axis();
    ATotal.Orientation = dotnetCHARTING.Orientation.Right;
    ATotal.Label.Text = "Sales";
    Chart2.Series.YAxis = ATotal; 
    Chart2.SeriesCollection.Add();

  
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Series Axis Binding DB Sample</title></head>
<body>
<div style="text-align:center">
 
  

</div>
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" id="AutoNumber1">
  <tr>
    <td width="50%">&nbsp;<dotnet:Chart id="Chart"  runat="server"/></td>
     <td width="50%">&nbsp;<dotnet:Chart id="Chart2"  runat="server"/></td>
   
  </tr>
</table>
</body>
</html>
