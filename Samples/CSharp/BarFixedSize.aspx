<%@ Page Language="C#" debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Bar Fixed Size</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	
	
	// This sample will demonstrate how to manipulate the bar size of a chart. This is also useful when drilling
	// down data to keep the columns looking the size same.
	
	// First we get our data, if you would like to get the data from a database you need to use
	// the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
	SeriesCollection mySC = getRandomData();
	
	// If you would like to keep the columns a specific width for any reason, you can change the StaticColumnWidth 
	// property of the axis from which it stems.
	Chart.XAxis.StaticColumnWidth = 50;
	
	// For horizontal charts you would use the yAxis.

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
    
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	int ElementCount = myR.Next(4,8);

	Series s = new Series();
	s.Name = "Series 1";
	for(int b = 1; b < ElementCount ; b++)
	{
		Element e = new Element();
		e.Name = "Element " + b;
		e.YValue = myR.Next(50);
		s.Elements.Add(e);
	}
	SC.Add(s);
	

	return SC;
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<font face="Arial" size="2">Refresh this sample to see a different number of elements.
            </font>
		</div>
	</body>
</html>
