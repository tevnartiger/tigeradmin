<%@ Page Language="C#" debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Outputcache Duration = "10" Varybyparam = "none"%>


<script runat="server">

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 1; b < 6; b++)
		{
			Element e = new Element();
			e.Name = "E " + b;
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	return SC;
}


void Page_Load(Object sender,EventArgs e)
{

	
	
	//Set file name 
	Chart.FileName ="outputcachesample";
	
	// Set the title to the current time
        DateTime Dt = DateTime.Now;
	Chart.Title="This chart was created at "+Dt.ToString();
	Response.Write("This page was created at "+Dt.ToString());

	// Set the x axis label
	Chart.ChartArea.XAxis.Label.Text="X Axis Label";

	// Set the y axis label
	Chart.ChartArea.YAxis.Label.Text="Y Axis Label";

	// Set the directory where the images will be stored.
	Chart.TempDirectory="temp";

	// Set the bar shading effect
	Chart.ShadingEffect = true;

	// Set he chart size.
	Chart.Width = 600;
	Chart.Height = 350;
	
	Chart.Debug=true;

	// Add the random data.
	Chart.SeriesCollection.Add(getRandomData());
    
    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Output Cache Sample</title>
</head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
 <font size="2" face="Arial">Refresh this chart, it will be cached for 10 seconds then a new chart will be created. You will also see that page render and chart render are both cached and syncronized.  See <a href="fragmentcache.aspx">fragmentcache</a> for a sample where the chart image is cached but the page contents are not.
 </font> 
</div>
</body>
</html>
