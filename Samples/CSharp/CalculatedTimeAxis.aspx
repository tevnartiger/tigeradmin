<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	
	
	// This sample will demonstrate how to use calculated time axes.
	
	Chart.DefaultLegendBox.Orientation = dotnetCHARTING.Orientation.Bottom;
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();
	
	// Set some properties for the main x axis.
	Chart.XAxis.TimeInterval = TimeInterval.Days;
	Chart.XAxis.Label.Text = "Days";
	
	// Calculate an axis from the x axis and add it with 'weeks' intervals.
	Chart.AxisCollection.Add(Chart.XAxis.Calculate("Weeks",TimeInterval.Weeks));
	
	// Calculate an axis from the x axis and add it with an advanced time interval (3 Days) intervals.
	TimeIntervalAdvanced tia = TimeIntervalAdvanced.Days;
	tia.Multiplier = 3;
	Axis calculatedXAxis = Chart.XAxis.Calculate("TimeIntervalAdvanced: Every 3 Days",tia);
	calculatedXAxis.Orientation = dotnetCHARTING.Orientation.Top;
	Chart.AxisCollection.Add(calculatedXAxis);
	

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
    
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	DateTime dt = new DateTime(2005,1,1);
	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 23; b++)
		{
		dt = dt.AddDays(1);
			Element e = new Element();
			//e.Name = "Element " + b;
			e.XDateTime = dt;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);


	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
