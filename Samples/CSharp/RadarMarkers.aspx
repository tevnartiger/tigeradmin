<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	// This sample demonstrates using axis markers with polar and spider radar charts.
	
	// Specify RadarMode
	Chart.XAxis.RadarMode = RadarMode.Spider;
	Chart1.XAxis.RadarMode = RadarMode.Polar;
	
	// Add axis markers.
	Chart.YAxis.Markers.Add(new AxisMarker("Marker",Color.Red,55));
	Chart1.YAxis.Markers.Add(new AxisMarker("Marker",Color.Red,55));
	Chart.XAxis.Markers.Add(new AxisMarker("Marker",Color.Orange,55,100));
	Chart1.XAxis.Markers.Add(new AxisMarker("Marker",Color.Orange,55,100));
	
	// Specify the scale range.
	Chart.XAxis.ScaleRange = Chart1.XAxis.ScaleRange = new ScaleRange(0,360);

	// Setup the two charts.
	Chart.Type = ChartType.Radar;//Horizontal;
	Chart.Size = "600x350";
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = ".netCHARTING Sample";
	
	Chart1.Type = ChartType.Radar;//Horizontal;
	Chart1.Size = "600x350";
	Chart1.TempDirectory = "temp";
	Chart1.Debug = true;
	Chart1.Title = ".netCHARTING Sample";
		

		
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
	Chart1.SeriesCollection.Add(getRandomData());
        
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(3);
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a.ToString();
		for(int b = 1; b < 10; b++)
		{
			Element e = new Element();
			//e.Name = "Element " + b.ToString();
			e.YValue = myR.Next(50);
			e.XValue = (360/10)*b ;
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<dotnet:Chart id="Chart1" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
