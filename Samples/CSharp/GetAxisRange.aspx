<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	// This sample demonstrates getting the dynamically determined axis ranges.

	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Palette = new Color[]{Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)};
	
	Chart.Type = ChartType.Combo;
	Chart.Size = "600x350";
	Chart.Title = ".netCHARTING Sample";
    Chart.DefaultSeries.Type = SeriesType.Marker;
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Help File > Getting Started > Data Tutorials
	// - DataEngine Class in the help file	
	// - Sample: features/DataEngine.aspx
	
	SeriesCollection mySC = getRandomData();

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);

    //Get a bitmap of the chart so the axes can be processed..
    Bitmap bmp = Chart.GetChartBitmap();

    // Get chart area rectangle
    Point[] caPoints = Chart.ChartArea.GetCoordinates();
    GraphicsPath gp = new GraphicsPath();
    gp.AddPolygon(caPoints);
    RectangleF caRect = gp.GetBounds();
    gp.Dispose();
    
    // Get axis ranges
    double xLow = (double)Chart.XAxis.GetValueAtX(((int)caRect.Left+1).ToString() + ",100");
    double xHigh = (double)Chart.XAxis.GetValueAtX(((int)caRect.Right ).ToString() + ",100");

    // Get axis ranges
    double yHigh = (double)Chart.YAxis.GetValueAtY( "100,"+((int)caRect.Top+1).ToString());
    double yLow = (double)Chart.YAxis.GetValueAtY("100," + ((int)caRect.Bottom ).ToString());
    label1.Text = "X Low: " + xLow.ToString() + " <br> X High: " + xHigh.ToString();
    label1.Text += "<BR>Y Low: " + yLow.ToString() + "<br> Y High: " + yHigh.ToString();
    label1.Text += "<BR> Rectangle: " + caRect.ToString();

    //Save the image using the FileManager.
    Chart.FileManager.SaveImage(bmp);
  
}

SeriesCollection getRandomData()
{
	Random myR = new Random(1);
	SeriesCollection SC = new SeriesCollection();
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series("Series " + a.ToString());
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element("");
            e.XValue = myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
		<asp:Label ID="label1" runat=server/>
	</body>
</html>
