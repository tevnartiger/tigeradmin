<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = "Logarithmic scale with ranges.";
	
	// This sample will demonstrate setting an axis range with the logarithmic scale.
	
	// Set the axis scale.
	Chart.YAxis.Scale = Scale.Logarithmic;
	
	// The range can be set using this code:
	Chart.YAxis.ScaleRange.ValueHigh = 725;
	Chart.YAxis.ScaleRange.ValueLow = 50;
	
	// Or using this code:
	Chart.YAxis.ScaleRange = new ScaleRange(5, 725);
	
	// Add arbitrary ticks at scale bounds.
	Chart.YAxis.ExtraTicks.Add(new AxisTick(725));
	Chart.YAxis.ExtraTicks.Add(new AxisTick(5));


	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	Chart.SeriesCollection.Add(getRandomData());
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 4; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(500);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="341px"></dotnet:Chart>
		</div>
	</body>
</html>
