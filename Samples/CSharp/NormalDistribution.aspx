<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
     
	// This sample demonstrates the use of NormalDistribution method from the StatisticalEngine. This method return the normal distribution for a series when the standard deviation is known. The mean is calculated using the arithmetic mean of the XValues of the given series
	
	// The Probability Chart
	ProbabilityChart.Title="Standard Normal distribution";
	ProbabilityChart.TempDirectory="temp";
	ProbabilityChart.Debug=true;
	ProbabilityChart.Size = "600x400";
	ProbabilityChart.LegendBox.Template ="%icon %name";
	ProbabilityChart.XAxis.Scale = Scale.Normal;
	ProbabilityChart.XAxis.Maximum = 5;
	ProbabilityChart.YAxis.ScaleRange.ValueHigh = 0.5;
	ProbabilityChart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	ProbabilityChart.DefaultSeries.DefaultElement.Marker.Visible = true;
	ProbabilityChart.DefaultSeries.Type = SeriesType.Spline;
	ProbabilityChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical;

	
	SeriesCollection sprobability = new SeriesCollection();		
	
	// Generate the sample data.
	Series sampledata1 = new Series ("Sample Data");
	sampledata1.Elements.Add (new Element("",0.0,0.03125));
	sampledata1.Elements.Add (new Element("",1.0,0.15625));
	sampledata1.Elements.Add (new Element("",2.0,0.31250));
	sampledata1.Elements.Add (new Element("",3.0,0.31250));
	sampledata1.Elements.Add (new Element("",4.0,0.15625));
	sampledata1.Elements.Add (new Element("",5.0,0.003125));
	sprobability.Add (sampledata1);
	
	// Add the series collection to the chart
	ProbabilityChart.SeriesCollection.Add(sprobability);
	
	// The second parameter of this method is the standard deviation of the normal probability distribution
	Series normalDistribution = StatisticalEngine.NormalDistribution(sampledata1, 1);
	normalDistribution.Type = SeriesType.Spline;
	ProbabilityChart.SeriesCollection.Add (normalDistribution);
													
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="ProbabilityChart" runat="server"/>		
		</div>
	</body>
</html>
