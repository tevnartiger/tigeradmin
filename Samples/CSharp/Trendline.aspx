<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Trend Line</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 750;
	Chart.Height = 300;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	
	
	// This sample will demonstrate how to derive trend lines from your data.
	
	// First we get our data, if you would like to get the data from a database you need to use
	// the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
	SeriesCollection mySC = getRandomData();
	
	// Setup 
	Chart.DefaultSeries.Line.Width = 2;
	Chart.DefaultSeries.DefaultElement.Transparency = 45;
	Chart.LegendBox.Position = LegendBoxPosition.BottomMiddle;
	Chart.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.None;
	
	
	
	// Get a trend line from series 1. Because only the SeriesCollection Calculate method returns a series a
	// SeriesCollection is instantiated and the method is used.
	Series trend1 = (new SeriesCollection(mySC[0])).Calculate("Series 1 Trend",Calculation.TrendLineLinear);
	
	// Set the type to line
	trend1.Type = SeriesType.Line;
	
	//We'll give this line a green color so it looks like the related green columns of series 2.
	trend1.DefaultElement.Color = Color.FromArgb(25,225,25);


	
	// Trend line from Series 1 & 2
	// This time we want to predict what will happen 10 steps from now so we pass an extra parameter to the
	// calculate method.
	Series trend2 = mySC.Calculate("Series 1 & 2 Trend + (10 elements)",Calculation.TrendLineLinear,10);
	
	// Set the series type to line
	trend2.Type = SeriesType.Line;
	
	// Give this trend line a blue color.
	trend2.DefaultElement.Color = Color.FromArgb(0,156,255);
	
	
	
	// Add the trend lines to the collection.
	mySC.Add(trend1);	
	mySC.Add(trend2);	


	// Add the collection.
	Chart.SeriesCollection.Add(mySC);
    
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 3; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 1; b < 36; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			if(a == 0)
				e.YValue = myR.Next(30);
			else
				e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);


	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
