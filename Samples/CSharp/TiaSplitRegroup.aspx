<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	// This sample demonstrates how Series.SplitRegroupCalculate works in stages.
	// FullScreen

	SetChart(Chart);
	SetChart(Chart1);
	SetChart(Chart2);

	Chart.XAxis.Scale = Scale.Time;
	Chart2.DefaultElement.DefaultSubValue.Type = SubValueType.Marker;	
	
	Chart.Title = "Original Data";
	Chart1.Title = "Data after the first split is applied.";
	Chart2.Title = "Resulting series after Series.SplitRegroupCalculate is applied to the data from the first chart.";
	Chart1.ChartArea.Label.Text = "Splitting a series by a TimeIntervalAdvanced object. The elements \nwere originally named after the day of the week they fall on, hence, \nthis x axis is a category x axis which demonstrates how these will be grouped.";
	Chart2.ChartArea.Label.Text = "At this stage the above elements are consolidated into a single \nelement with subvalues representing the original element's values. \nThese elements are average values.";
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	
	SeriesCollection mySC = getRandomData();
	
	Series splits = mySC[0].SplitRegroupCalculate(TimeIntervalAdvanced.Week,TimeIntervalAdvanced.Day,Calculation.Average,"ddd",true);
	
	// Add the random data.
	Chart.SeriesCollection.Add(mySC); // original data
	Chart1.SeriesCollection.Add(mySC[0].Split(TimeIntervalAdvanced.Week)); // data split into weeks
	Chart2.SeriesCollection.Add(splits);// data after SplitRegroupCalculate processing
	
}

void SetChart(Chart c)
{
	c.Type = ChartType.Combo;
	c.Size = "600x350";
	c.TempDirectory = "temp";
	c.Debug = true;
	c.Title = ".netCHARTING Sample";
	c.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom;
	}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
	DateTime dt = new DateTime(2006,1,1);
	for(int a = 1; a < 2; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a.ToString();
		for(int b = 1; b < 200; b++)
		{dt = dt.AddDays(1);
			Element e = new Element();
			e.Name = dt.DayOfWeek.ToString();
			e.YValue = myR.Next(50);
			e.XDateTime = dt;
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	return SC;
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	</head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
			<dotnet:Chart id="Chart1" runat="server"/>
			<dotnet:Chart id="Chart2" runat="server"/>
		</div>
	</body>
</html>
