<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	// This sample demonstrates adding points of interest icons onto a map based on GPS coordinates.

	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Palette = new Color[]{Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)};
	
	Chart.Type = ChartType.Map;
	Chart.Size = "600x350";
	Chart.Title = ".netCHARTING Sample";
	


    Chart.Mapping.ZoomCenterPoint = new PointF(41.75f, -88);
    Chart.Mapping.ZoomPercentage = 7500;


    MapLayer layer = MapDataEngine.LoadLayer("../../images/mapFiles/primusa.shp");
    layer.DefaultShape.Background.Color = Color.White;
    Chart.Mapping.MapLayerCollection.Add(layer);
    Chart.Mapping.MapLayerCollection.Add("../../images/mapFiles/co17_d00.shp");
    
    MapLayer pizza = new MapLayer();
    ElementMarker pizzaMarker = new ElementMarker("../../images/pin_red.png");
    pizza.AddLatLongPoint(new PointF(41.75f, -87.99f), pizzaMarker);
    pizza.AddLatLongPoint(new PointF(41.79f,-88.07f ), pizzaMarker);
    pizza.AddLatLongPoint(new PointF(41.75f, -88.015f), pizzaMarker);
    pizza.AddLatLongPoint(new PointF(41.77f, -87.994f), pizzaMarker);
    Chart.Mapping.MapLayerCollection.Add(pizza);

    MapLayer deli = new MapLayer();
    ElementMarker deliMarker = new ElementMarker("../../images/pin_blue.png");
    deli.AddLatLongPoint(new PointF(41.79f, -88.01f), deliMarker);
    deli.AddLatLongPoint(new PointF(41.83f, -88.03f), deliMarker);
    deli.AddLatLongPoint(new PointF(41.83f, -88.008f), deliMarker);
    deli.AddLatLongPoint(new PointF(41.829f, -88.022f), deliMarker);
    Chart.Mapping.MapLayerCollection.Add(deli);

    LegendEntry le = new LegendEntry("Pizza Restaurants","");
    le.SeriesType = SeriesType.Marker;
    le.Marker = pizzaMarker;
    LegendEntry le2 = new LegendEntry("Delis","");
    le2.SeriesType = SeriesType.Marker;
    le2.Marker = deliMarker;
    Chart.LegendBox.ExtraEntries.Add(le, le2);
    Chart.LegendBox.Visible = true; 
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
