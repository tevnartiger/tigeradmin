<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	// This sample demonstrates making series invisible based on a legend entry click.

	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Palette = new Color[]{Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)};
	
	Chart.Type = ChartType.Combo;
	Chart.Size = "600x350";
	Chart.Title = ".netCHARTING Sample";
	
	Annotation an = new Annotation("Click the legend entries to toggle series visibility.");
	an.Position = new Point(500,115);
	Chart.Annotations.Add(an);
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Help File > Getting Started > Data Tutorials
	// - DataEngine Class in the help file	
	// - Sample: features/DataEngine.aspx
	
	SeriesCollection mySC = getRandomData();

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);

	// This will ensure the series always have the same colors based on their name.
	Chart.SmartPalette = mySC.GetSmartPalette(Chart.Palette);
	
	Chart.DefaultSeries.LegendEntry.URL = "?name=%Name";
	
	// By enumerating the legned entry positions in the box, the custom entry added below can replace the position of the original.
	int i = 0;
	foreach(Series s in mySC)
	{
		s.LegendEntry.SortOrder = i++;
	}
	
	string query = "";
	if(Request.QueryString["name"] != null)
	{
		query = Request.QueryString["name"];
		foreach(Series s in mySC)
		{
			if(s.Name == query && Request.QueryString["show"] == null)
			{
				s.Visible = false;
				LegendEntry le = new LegendEntry();
				le.Name = query;
				le.URL = "?name="+query+"&show=true";
				le.SortOrder = s.LegendEntry.SortOrder;
				le.Background.Color = Color.FromArgb(1,Color.Transparent);
				Chart.LegendBox.ExtraEntries.Add(le);
				break;
			}
		}
	}   
}

SeriesCollection getRandomData()
{
	Random myR = new Random(1);
	SeriesCollection SC = new SeriesCollection();
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series("Series " + a.ToString());
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element("Element " + b.ToString());
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
