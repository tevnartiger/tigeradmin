<%@ Page Language="C#" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Forecasting Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
     
	// This sample demonstrates the use of GeneralLinear Forecasting engine in order to
	// find the function of best fit from three functions spaces. The data used for which
	// the functions are fit is a set of data which represents a FX exchange rate over a given
	// period of time. We index the period by the number of days after the inital date and the
	// three function spaces are the spaces spanned by the following basis elements:
	//
	// 1) {(x - 2*x^2)
	// 2) {(x - 2*x^2) (x^3 + 1)}
	// 3) {(x - 2*x^2) (x^3 + 1), (10*cos(0.003*x))}
	//
	
	// The Forecast Chart
	ForecastChart.Title="Exchange";
	ForecastChart.TempDirectory="temp";
	ForecastChart.Debug=true;
	ForecastChart.Size = "800x800";
	ForecastChart.LegendBox.Template ="%icon %name";
	ForecastChart.YAxis.ScaleRange.ValueLow = 150;
	
	// The Forecast data
	DataEngine de = new DataEngine();
	de.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];
	de.DateGrouping = TimeInterval.Days;
	de.StartDate = new DateTime(1973,3,10,0,0,0);
	de.EndDate = new DateTime(1984,12,10,23,59,59);
	de.SqlStatement= @"SELECT ID, Value AS q FROM Exchange WHERE Data >= #STARTDATE# AND Data <= #ENDDATE# ORDER BY Data ";

	//Add a series
	SeriesCollection scForecast = de.GetSeries ();
	ForecastChart.SeriesCollection.Add (scForecast);

	scForecast[0].Name = "Exchange";
	scForecast[0].Type = SeriesType.Spline;


	/*
	 * Takes off the marker off the line and spline series.
	 */
	ForecastChart.DefaultSeries.DefaultElement.Marker = new ElementMarker (ElementMarkerType.None);
	ForecastChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical;
	
	// Generate a series of standard deviation for the given points
	Series deviation = new Series();
	for (int i = 0; i < scForecast[0].Elements.Count; i++ )
	{
		Element el = new Element();
		el.XValue = scForecast[0].Elements[i].XValue;
		el.YValue = 0.0000000001;
		deviation.Elements.Add(el);
	}

	// Note that this line is necessary in order to clear the function basis set by previous
	// example.
	//
	ForecastEngine.Options.Reset();
	
	// Set the first model function
	//
	// The first basis element: (x - 2*x^2)
	ForecastEngine.Options.AddSumOfPowerTerms(new double[]{1, -2}, new double[]{1, 2});
	// The second basis element: (1)
	ForecastEngine.Options.AddSumOfPowerTerms(new double[]{1}, new double[]{0});
	// Generate a new series which will draw the best fit line according with the model function which we just set
	Series generalLinearModel1 =  new Series();
	generalLinearModel1 = ForecastEngine.Advanced.GeneralLinear(scForecast[0], deviation);
	generalLinearModel1.Name = "Model1";
	generalLinearModel1.Type = SeriesType.Spline;
	//The next three lines display no to the chart the given function used
	// to fit the curve
	generalLinearModel1.Elements[1500].SmartLabel.Text = "Function 1: %Function";
	generalLinearModel1.Elements[1500].ShowValue =  true;
	ForecastChart.SeriesCollection.Add(generalLinearModel1);

	// Set the second model function ; we add x^3 function to the basis functions
	ForecastEngine.Options.AddSumOfPowerTerms(new double[]{1, 2}, new double[]{3, 0});
	// Generate a new series which will draw the best fit line according with the model function which we just set
	Series generalLinearModel2 =  new Series();
	generalLinearModel2 = ForecastEngine.Advanced.GeneralLinear(scForecast[0], deviation);
	generalLinearModel2.Name = "Model2";
	generalLinearModel2.DefaultElement.Color = Color.FromArgb(255, 99, 49);
	generalLinearModel2.Type = SeriesType.Spline;
	//The next three lines display no to the chart the given function used
	// to fit the curve
	generalLinearModel2.Elements[600].SmartLabel.Text = "Function 2: %Function";
	generalLinearModel2.Elements[600].SmartLabel.Color = Color.FromArgb(255, 99, 49);
	generalLinearModel2.Elements[600].ShowValue =  true;
	ForecastChart.SeriesCollection.Add(generalLinearModel2);
	
	// Set the third model function ; we add 10*cos(0.003*x)function to the basis functions
	ForecastEngine.Options.AddCosineSum(new double[]{10}, new double[]{0.003}, new double[]{0}, new double[]{1});
	// Generate a new series which will draw the best fit line according with the model function which we just set
	Series generalLinearModel3 =  new Series();
	generalLinearModel3 = ForecastEngine.Advanced.GeneralLinear(scForecast[0], deviation);
	generalLinearModel3.Name = "Model3";
	generalLinearModel3.DefaultElement.Color = Color.FromArgb(0, 0, 0);
	generalLinearModel3.Type = SeriesType.Spline;
	//The next three lines display no to the chart the given function used
	// to fit the curve
	generalLinearModel3.Elements[1600].SmartLabel.Text = "Function 3: %Function";
	// The label will have the same color as the element which he represents
	generalLinearModel3.Elements[1600].SmartLabel.Color = Color.FromArgb(0, 0, 0);
	generalLinearModel3.Elements[1600].ShowValue =  true;
	ForecastChart.SeriesCollection.Add(generalLinearModel3);
	
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="ForecastChart" runat="server"/>			
		</div>
	</body>
</html>
