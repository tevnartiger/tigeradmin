<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    Chart.Title="Item sales";
    Chart.XAxis.Label.Text ="months";
    Chart.YAxis.FormatString="currency";
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.Size="800X500";
    Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
    
    //Make series legend entries invisible and add 3 custom legend entries
    Chart.DefaultSeries.LegendEntry.Visible=false;
    LegendEntry singleLegendEntry = new LegendEntry();
    singleLegendEntry.Name="Sales < $3500";
    singleLegendEntry.Background.Color=Color.Red;
    Chart.LegendBox.ExtraEntries.Add(singleLegendEntry);
    singleLegendEntry = new LegendEntry();
    singleLegendEntry.Name="$3500 < Sales < $5000";
    singleLegendEntry.Background.Color=Color.Yellow;
    Chart.LegendBox.ExtraEntries.Add(singleLegendEntry);
    singleLegendEntry = new LegendEntry();
    singleLegendEntry.Name="$5000 < Sales ";
    singleLegendEntry.Background.Color=Color.Green;
    Chart.LegendBox.ExtraEntries.Add(singleLegendEntry);


     
    //Add a series
     DataEngine de = new DataEngine();
    de.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    de.StartDate=new DateTime (2002,1,1,0,0,0);
    de.EndDate = new DateTime (2002,12,31,23,59,59);
    de.DateGrouping = TimeInterval.Year;
	de.SqlStatement= @"SELECT OrderDate,Total, Name FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate"; 
    SeriesCollection sc = de.GetSeries();
    // customize element settings
    //set the color for different customers in each month based on their total sale.
	foreach(dotnetCHARTING.Series sr in sc)
	{
		foreach(dotnetCHARTING.Element el in sr.Elements)
		{
      		if (el.YValue < 3500)
           		el.Color = Color.Red;
           	else if (el.YValue < 5000)
           		el.Color = Color.Yellow;
      		else
      			el.Color = Color.Green;
      	}
 	}
    Chart.SeriesCollection.Add(sc);
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Invisible Legend Entry</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>