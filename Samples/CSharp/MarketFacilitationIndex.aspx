<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>
<%@ Import Namespace="dotnetCHARTING"%>


<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Financial Market Indicators Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
     
	// This sample demonstrates the use of the Market Facilitation Index indicator
		
	// First we declare a chart of type FinancialChart	
	// The Financial Chart
	FinancialChart.Title="Company X Stock Price";
	FinancialChart.TempDirectory="temp";
	FinancialChart.Debug=true;
	FinancialChart.ShadingEffect = true;
	FinancialChart.LegendBox.Template ="%icon %name";
	FinancialChart.Size="800X500";
FinancialChart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
		
	FinancialChart.XAxis.Scale = Scale.Time;
	FinancialChart.XAxis.FormatString = "MMM d";
	FinancialChart.XAxis.TimeInterval = TimeInterval.Day;
	// For financial indicators the time scale is inverted (i.e. the first element of the series is the newest)
	FinancialChart.XAxis.InvertScale = true; 
	
	
	FinancialChart.YAxis.Label.Text = "Price (USD)";
	FinancialChart.YAxis.FormatString = "currency";
	FinancialChart.YAxis.ScaleRange.ValueLow = 15;
	
	// Here we load data samples from the FinancialCompany table from within chartsample.mdb
	DataEngine priceDataEngine = new DataEngine ();
	priceDataEngine.ChartType = ChartType.Financial;
	priceDataEngine.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];
	priceDataEngine.DateGrouping = TimeInterval.Day;
	priceDataEngine.StartDate = new DateTime (2001,2,1);
	priceDataEngine.EndDate = new DateTime (2001,5,30);
	priceDataEngine.SqlStatement = @"SELECT TransDate, HighPrice, LowPrice, OpenPrice, ClosePrice,Volume FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate  DESC ";
	priceDataEngine.DataFields = "xAxis=TransDate,High=HighPrice,Low=LowPrice,Open=OpenPrice,Close=ClosePrice,Volume=Volume";

	SeriesCollection sc = priceDataEngine.GetSeries ();
	Series prices = null;
	if(sc.Count>0)
		prices = sc[0];
	else
		return;
		
	
	prices.DefaultElement.ToolTip="L:%Low-H:%High";
	prices.DefaultElement.SmartLabel.Font = new Font("Arial", 6);
	prices.DefaultElement.SmartLabel.Text="O:%Open-C:%Close";
	prices.Type = SeriesTypeFinancial.CandleStick;

	CalendarPattern cp = new CalendarPattern (TimeInterval.Day, TimeInterval.Week, "0000001");
	cp.AdjustmentUnit = TimeInterval.Day;
	prices.Trim (cp, ElementValue.XDateTime);
	prices.Name = "Prices";
	FinancialChart.SeriesCollection.Add (prices);

	// Create the second chart area 
	ChartArea volumeChartArea = new ChartArea();
	volumeChartArea.Label.Text = "Stock Volume";
	volumeChartArea.YAxis.Label.Text = "Volumes";
	volumeChartArea.Series.Name="Stock Volume";
	volumeChartArea.HeightPercentage = 17;
	volumeChartArea.Series.DefaultElement.ToolTip="%YValue";
	FinancialChart.ExtraChartAreas.Add(volumeChartArea);

	// Add a volume series to the chart area
	DataEngine volumeDataEngine = new DataEngine ();
	volumeDataEngine.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];
	volumeDataEngine.DateGrouping = TimeInterval.Days;
	volumeDataEngine.StartDate=new DateTime (2001,2,1);
	volumeDataEngine.EndDate = new DateTime (2001,5,30);
	volumeDataEngine.SqlStatement= @"SELECT TransDate,volume FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate DESC"; 
	volumeDataEngine.DataFields="xAxis=TransDate,yAxis=Volume";
		
	Series volumes = volumeDataEngine.GetSeries () [0];
	volumes.Trim (cp, ElementValue.XDateTime);
	volumes.Name = "Volumes";
	volumes.Type = SeriesType.Bar;
	volumeChartArea.SeriesCollection.Add (volumes);
	
	// Market Facilitation Index
	
	FinancialChart.DefaultSeries.DefaultElement.Marker = new ElementMarker (ElementMarkerType.None);
	FinancialChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical;
	FinancialChart.DefaultSeries.Type = SeriesType.Spline;
	
	// Create a new char area for the indicator Market Facilitation Index
	ChartArea mfiChartArea = new ChartArea();
	mfiChartArea.Label.Text = "Market Facilitation Index";
	mfiChartArea.YAxis = new Axis();
	mfiChartArea.HeightPercentage = 20;
	FinancialChart.ExtraChartAreas.Add(mfiChartArea);

	// Market Facilitation Index
	Series mfiSeries = FinancialEngine.MarketFacilitationIndex(prices);
	mfiSeries.Type = SeriesType.Spline;
	mfiSeries.DefaultElement.Color = Color.FromArgb(45,125,255);
	mfiChartArea.SeriesCollection.Add(mfiSeries);

}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="FinancialChart" runat="server"/>			
		</div>
	</body>
</html>
