<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	Chart.Type = ChartType.Map;//Horizontal;
	Chart.Size = "680x550";
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = ".netCHARTING Sample";

	// This sample demonstrates a variation of using a legend with maps.
	
	MapLayer layer = MapDataEngine.LoadLayer( @"../../images/MapFiles/states.shp");
	Chart.Mapping.MapLayerCollection.Add(layer);	
	
	// This line will cause the legend to appear, however, it may not contain the correct column template to display information pertaining to the chart.
	layer.PaletteName = Palette.Bright;	
	

	// Set legendbox properties.
	Chart.LegendBox.Template = "%ICON%State_Name";

	Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom;
	Chart.LegendBox.Padding = 8;
	Chart.LegendBox.ColumnAlignments = new StringAlignment[]{StringAlignment.Far,StringAlignment.Near};
	Chart.LegendBox.ClearColors();
	Chart.ChartArea.ClearColors();
	Chart.TitleBox.ClearColors();

}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	</head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
