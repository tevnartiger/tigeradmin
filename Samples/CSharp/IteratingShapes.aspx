<%@ Page Language="C#" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

// This sample demonstrates iterating through and customizing individual shapes.
void Page_Load(Object sender,EventArgs e)
{
	Chart.Type = ChartType.Map;
	Chart.Size = "600x600";
	Chart.Title = " Customize individual countries in Europe through shape iteration.";
	Chart.TempDirectory = "temp";
	Chart.Debug=true;
	Chart.TitleBox.Position = TitleBoxPosition.Full;
	
	Chart.ChartArea.Background.Color = Color.LightBlue;
	Chart.Mapping.ZoomPercentage = 134;
				
	Chart.Mapping.ZoomCenterPoint = new PointF(52,12);
	MapLayer layer = MapDataEngine.LoadLayer("../../images/MapFiles/europe.shp");
	
	Chart.Mapping.DefaultShape.Label.Text = "%Code";
	Chart.Mapping.DefaultShape.Label.OutlineColor = Color.White;
	Chart.Mapping.DefaultShape.Label.Hotspot.ToolTip = "%Cntry_Name";

	
	Chart.Mapping.MapLayerCollection.Add(layer);
	
	foreach(Shape shape in layer.Shapes)
	{
		shape.Background.Mode = BackgroundMode.ImageStretch;
		string code = (string)shape["code"];
		if(code==null)
			break;
		switch(code.Trim())
		{
			case "FR":
				shape.Background.ImagePath = "../../images/MapFiles/fr.Png";
				shape.Label.Font = new Font("Verdana",20);
				shape.Label.OutlineColor = Color.Gold;

			break;
			case "FI":
				shape.Background.ImagePath = "../../images/MapFiles/fi.Png";
				shape.Label.Font = new Font("Verdana",20);
			break;
			case "NO":
				shape.Background.ImagePath = "../../images/MapFiles/no.Png";
				shape.Label.Font = new Font("Verdana",20);
			break;
			case "SE":
				shape.Background.ImagePath = "../../images/MapFiles/se.Png";
				shape.Label.Font = new Font("Verdana",20);
			break;
			case "EE":
				shape.Background.ImagePath = "../../images/MapFiles/ee.Png";
			break;
			case "LV":
				shape.Background.ImagePath = "../../images/MapFiles/lv.Png";
			break;
			case "LT":
				shape.Background.ImagePath = "../../images/MapFiles/lt.Png";
			break;
			case "PL":
				shape.Background.ImagePath = "../../images/MapFiles/pl.Png";
				shape.Label.Font = new Font("Verdana",20);
			break;
			case "SK":
				shape.Background.ImagePath = "../../images/MapFiles/sk.Png";
			break;
			case "RO":
				shape.Background.ImagePath = "../../images/MapFiles/ro.Png";
			break;
			case "HU":
				shape.Background.ImagePath = "../../images/MapFiles/hu.Png";
				shape.Label.OutlineColor = Color.Gold;

			break;
			case "BG":
				shape.Background.ImagePath = "../../images/MapFiles/bg.Png";
			break;
			case "YU":
				shape.Background.ImagePath = "../../images/MapFiles/yu.Png";
				shape.Label.OutlineColor = Color.Gold;

			break;
			case "AL":
				shape.Background.ImagePath = "../../images/MapFiles/al.Png";
			break;
			case "BA":
				shape.Background.ImagePath = "../../images/MapFiles/ba.Png";
			break;
			case "SI":
				shape.Background.ImagePath = "../../images/MapFiles/si.Png";
			break;
			case "AT":
				shape.Background.ImagePath = "../../images/MapFiles/at.Png";
				shape.Label.OutlineColor = Color.Gold;
			break;
			case "CZ":
				shape.Background.ImagePath = "../../images/MapFiles/cz.Png";
			break;
			case "DE":
				shape.Background.ImagePath = "../../images/MapFiles/de.Png";
				shape.Label.Font = new Font("Verdana",20);
			break;
			case "DK":
				shape.Background.ImagePath = "../../images/MapFiles/dk.Png";
			break;
			case "NL":
				shape.Background.ImagePath = "../../images/MapFiles/nl.Png";
			break;
			case "BE":
				shape.Background.ImagePath = "../../images/MapFiles/be.Png";
			break;
			case "CH":
				shape.Background.ImagePath = "../../images/MapFiles/ch.Png";
			break;
			case "ES":
				shape.Background.ImagePath = "../../images/MapFiles/es.Png";
				shape.Label.Font = new Font("Verdana",20);
			break;
			case "PT":
				shape.Background.ImagePath = "../../images/MapFiles/pt.Png";
			break;
			case "UK":
				shape.Background.ImagePath = "../../images/MapFiles/uk.Png";
				shape.Label.Font = new Font("Verdana",20);
			break;
			case "IE":
				shape.Background.ImagePath = "../../images/MapFiles/ie.Png";
				shape.Label.Font = new Font("Verdana",12);
				shape.Label.OutlineColor = Color.Gold;
			break;
			case "IT":
				shape.Background.ImagePath = "../../images/MapFiles/it.Png";
				shape.Label.Font = new Font("Verdana",12);
				shape.Label.OutlineColor = Color.Gold;

			break;
			case "VA":
				shape.Background.ImagePath = "../../images/MapFiles/va.Png";
			break;
			case "HR":
				shape.Background.ImagePath = "../../images/MapFiles/hr.Png";
			break;
			case "GR":
				shape.Background.ImagePath = "../../images/MapFiles/gr.Png";
			break;
			case "MD":
				shape.Background.ImagePath = "../../images/MapFiles/md.Png";
			break;
			case "LU":
				shape.Background.ImagePath = "../../images/MapFiles/lu.Png";
			break;
			case "MK":
				shape.Background.ImagePath = "../../images/MapFiles/mk.Png";
			break;						
			default:
				shape.Background.Mode = BackgroundMode.Brush;
			break;
			
		}
		
	
	}
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>
