<%@ Page Language="C#" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    void Page_Load(Object sender, EventArgs e)
    {
        // This sample demonstrates a vertical linear gauge using shading effect one with gradient axis markers and image axis ticks.
        Chart.Size = "110x220";

        Chart.TempDirectory = "temp";
        Chart.Debug = true;
        Chart.Palette = new Color[] { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) };
        Chart.LegendBox.Visible = false;

        Chart.Type = ChartType.Gauges;
        Chart.DefaultSeries.GaugeType = GaugeType.Vertical;
        Chart.ShadingEffectMode = ShadingEffectMode.One;

        Chart.ChartArea.Padding = 0;
        Chart.Margin = "-2";
        Chart.ChartArea.ClearColors();
        Chart.DefaultElement.ShowValue = true;
        
        Chart.DefaultSeries.GaugeBorderBox.DefaultCorner = BoxCorner.Round;
        Chart.DefaultSeries.GaugeBorderShape = GaugeBorderShape.UseBox;
        Chart.YAxis.Orientation = dotnetCHARTING.Orientation.Right;

        Chart.YAxis.ExtraTicks.Add(new AxisTick(50, new ElementMarker("../../images/error2.png")));
        Chart.YAxis.ExtraTicks.Add(new AxisTick(30, new ElementMarker("../../images/error.png")));
        Chart.YAxis.Markers.Add(new AxisMarker("", new Background(Color.Orange, Color.Red, -90), 30, 50));
        Chart.YAxis.Markers.Add(new AxisMarker("", new Background(Color.Yellow, Color.Orange, -90), 15, 30));
        Chart.YAxis.Markers.Add(new AxisMarker("", new Background(Color.Green, Color.Yellow, -90), 0, 15));
        // *DYNAMIC DATA NOTE* 
        // This sample uses random data to populate the chart. To populate 
        // a chart with database data see the following resources:
        // - Help File > Getting Started > Data Tutorials
        // - DataEngine Class in the help file	
        // - Sample: features/DataEngine.aspx

        SeriesCollection mySC = getRandomData();

        // Add the random data.
        Chart.SeriesCollection.Add(mySC);

    }

    SeriesCollection getRandomData()
    {
        Random myR = new Random();
        SeriesCollection SC = new SeriesCollection();
        for (int a = 1; a < 2; a++)
        {
            Series s = new Series("");
            for (int b = 1; b < 2; b++)
            {
                Element e = new Element("Element " + b.ToString());
                e.YValue = 45;
                s.Elements.Add(e);
            }
            SC.Add(s);
        }
        return SC;
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <dotnet:Chart ID="Chart" runat="server" />
    </div>
</body>
</html>
