<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">




void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Gauges;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 400;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.ChartArea.ClearColors();

	
	Chart.LegendBox.Position = LegendBoxPosition.None;
	
	// This sample will demonstrate how Gauge backgrounds can be used.
	
	// First create a series, if you would like to get the data from a database you need to use
	// the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
	Series s = new Series();
	
	// Turn off gauge clipping so the gauge is a complete circle to match our image.
	Chart.ClipGauges = false;
	
	// Set the background for the series.
	s.Background = new Background("../../Images/gauge.gif");
	
	// Add an element
	s.Elements.Add(new Element("",55));
	
	// Update the main y axis settings to match the ticks in our image.
	Axis a = Chart.YAxis;
	
	a.RangeAngle = 242;
	a.Maximum = 150;
	a.Minimum = 10;
	a.Interval = 10;
	
	// Make the axis invisible.
	a.Clear();
	
	// Add the random data.
	Chart.SeriesCollection.Add(s);
    
}


		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
