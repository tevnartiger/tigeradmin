<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Size = "600x350";
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = ".netCHARTING Sample";
	
	
	// This sample demonstrates how to label the total values of column stacks.

    Chart.DefaultElement.ShowValue = true;
    Chart.YAxis.Scale = Scale.Stacked;
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();

    // Clear the secondary alignment options of element labels.
    foreach(Series ser in mySC)
    {
        ser.DefaultElement.SmartLabel.AlignmentSecondary.Clear();
        foreach(Element el in ser.Elements)
        {
            el.SmartLabel.AlignmentSecondary.Clear();
        }
    }


    // Create a total series that is invisible. Just used to place the totals labels.
    Series total = mySC.Calculate("Total",Calculation.Sum);
    total.Type = SeriesType.Marker;

    total.DefaultElement.Marker.Type = ElementMarkerType.None;
    total.DefaultElement.SmartLabel.Alignment = LabelAlignment.Top;
    total.DefaultElement.SmartLabel.Font = new Font("Arial Black",8,FontStyle.Bold);
	
    // Add the random data.
	Chart.SeriesCollection.Add(mySC);
    Chart.SeriesCollection.Add(total);

}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(7);
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a.ToString();
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b.ToString();
			e.YValue = myR.Next(20,50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
