<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Logarithm Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
     
	// This sample demonstrates the use of TrendLineLogarithmic from within Forecasting 	
	//which find the best fitting logarithmical curve to sets of data
	// The Forecast Chart
	ForecastChart.Title="Forecast Logarithm";
	ForecastChart.TempDirectory="temp";
	ForecastChart.Debug=true;
	ForecastChart.Size = "1000x400";
	ForecastChart.LegendBox.Template ="%icon %name";
	ForecastChart.XAxis.Scale = Scale.Normal;
		
	SeriesCollection slogarithm = new SeriesCollection();
	Series sampledata1 = new Series ("Sample 1");
	for ( int i = 1; i < 10; i++) {
		Element el = new Element();
		el.YValue = Math.Log(i)* i + 2;
		el.XValue = i ;
		sampledata1.Elements.Add (el);
	}
	slogarithm.Add (sampledata1);
	slogarithm[0].Type = SeriesType.Marker;

	ForecastChart.SeriesCollection.Add (slogarithm);

	
	// Here we create a series which will hold the Y values calculated with the LogarithmFit indicator
	Series trendLineLogarithmic = new Series();
	trendLineLogarithmic = ForecastEngine.TrendLineLogarithmic(sampledata1);
	
	//The next two lines display on to the chart the logarithmic function used
	// to fit the curve
	trendLineLogarithmic.Elements[0].SmartLabel.Text = "Function: %Function";
	trendLineLogarithmic.Elements[0].ShowValue =  true;
	
	trendLineLogarithmic.DefaultElement.Color = Color.FromArgb(255,99,49);
	trendLineLogarithmic.Type = SeriesType.Spline;
	ForecastChart.SeriesCollection.Add(trendLineLogarithmic);
	
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="ForecastChart" runat="server"/>
			
			
		</div>
	</body>
</html>
