<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart2.TempDirectory = "temp";
	Chart2.Debug = true;
	Chart3.TempDirectory = "temp";
	Chart3.Debug = true;
	Chart.XAxis.SpacingPercentage = 16;
	Chart2.XAxis.SpacingPercentage = 16;	
	
	
	// This sample will demonstrate how using different tick alignments.
	

	// This chart doesnt center tick marks above the label. With time based x axes, this is the accepted way to draw ticks.
	Chart.XAxis.CenterTickMarks = false;
	
	// To move (un-centered) labels, tick length wont work, instead padding has to be used.
	Chart.XAxis.TickLabelPadding = 5;
	
	// Only the DefaultTick settings will have an affect when ticks are not centered.
	Chart.XAxis.DefaultTick.Line.Length = 30;
	

	// The second chart doesnt center tick marks above the label.
	// The second chart uses a category x axis. This also is the accepted way to draw ticks.
	Chart2.XAxis.CenterTickMarks = false;

	

	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	Chart.SeriesCollection.Add(getRandomData());
	Chart2.SeriesCollection.Add(getRandomData2());
	Chart3.SeriesCollection.Add(getRandomData3());
    
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	DateTime dt = new DateTime(2005,1,1);
	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.YValue = myR.Next(50);
			e.XDateTime = dt;
			dt = dt.AddMonths(1);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	return SC;
}

SeriesCollection getRandomData2()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();

	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}

SeriesCollection getRandomData3()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();

	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.YValue = myR.Next(50);
			e.XValue = b*2;
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<dotnet:Chart id="Chart2" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<dotnet:Chart id="Chart3" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
