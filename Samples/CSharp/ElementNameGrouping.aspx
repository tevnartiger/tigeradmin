<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<html>
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	// This sample demonstrates the element grouping technology introduced in version 4.1 which uses intelligent pattern recognition to determine the order of elements when it is not clearly provided.
	// FullScreen
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Palette = new Color[]{Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)};
	
	Chart.Type = ChartType.Combo;
	Chart.Size = "600x350";
	Chart.Title = ".netCHARTING Sample";
	
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Help File > Getting Started > Data Tutorials
	// - DataEngine Class in the help file	
	// - Sample: features/DataEngine.aspx
	
	SeriesCollection mySC = getRandomData();
	mySC[0][0] = new Element("C",10);
	mySC[0][1] = new Element("D",10);
	mySC[0][2] = new Element("E",10);
	mySC[0][3] = new Element("F",10);
	
	mySC[1][0] = new Element("E",10);
	mySC[1][1] = new Element("F",10);
	mySC[1][2] = new Element("G",10);
	mySC[1][3] = new Element("H",10);

	mySC[2][0] = new Element("A",10);
	mySC[2][1] = new Element("B",10);
	mySC[2][2] = new Element("C",10);
	mySC[2][3] = new Element("D",10);



	// Add the random data.
	Chart.SeriesCollection.Add(mySC);

        
}

SeriesCollection getRandomData()
{
	Random myR = new Random(1);
	SeriesCollection SC = new SeriesCollection();
	for(int a = 1; a < 4; a++)
	{
		Series s = new Series("Series " + a.ToString());
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element();
			e.YValue = double.NaN;
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}

		</script>
	</head>
	<body>
	<div align="center">

		<table border="1" style="border-collapse: collapse" width="57%" id="table7">
	<tr>
		<td><font face="Arial" size="2">The elements will be provided for the 
		chart in a segmented order.</font><p><font face="Arial" size="2">3 
		Series with element names scattered throughout.</font></p>
		<table border="1" style="border-collapse: collapse" width="43%" id="table8">
			<tr>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">C</font></td>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">D</font></td>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">E</font></td>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">F</font></td>
			</tr>
			<tr>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">E</font></td>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">F</font></td>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">G</font></td>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">H</font></td>
			</tr>
			<tr>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">A</font></td>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">B</font></td>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">C</font></td>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">D</font></td>
			</tr>
		</table>
		<p><font face="Arial" size="2">Intended order:</font></p>
		<table border="1" style="border-collapse: collapse" width="44%" id="table9">
			<tr>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">A</font></td>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">B</font></td>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">C</font></td>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">D</font></td>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">E</font></td>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">F</font></td>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">G</font></td>
				<td bgcolor="#F7F7F7"><font face="Arial" size="2">H</font></td>
			</tr>
		</table>
		<p><font face="Arial" size="2">.netCHARTING will analyze this list and 
		determine what the true intended order should be. This does not employ 
		sorting, it looks for patterns in element orders of different series to 
		determine the original order.</font></p>
		<p>&nbsp;</td>
	</tr>
</table></div>
		<div align="center">
			<div align="center">&nbsp;</div>
			<div align="center">
				<dotnet:Chart id="Chart" runat="server" /></div>
			<div></div>
		</div>
	</body>
</html>
