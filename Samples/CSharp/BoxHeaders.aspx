<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    void Page_Load(Object sender, EventArgs e)
    {
        // This sample demonstrates background shading for titlebox, legendbox and annotations along with label shadows.

        // Chart Settings
        Chart.TempDirectory = "temp";
        Chart.Debug = true;
        Chart.Type = ChartType.Combo;
        Chart.Size = "600x350";        
        Chart.Palette = new Color[] { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) };
        Chart.ChartArea.Background.Color = Color.FromArgb(255, 250, 250, 250);
        Chart.YAxis.AlternateGridBackground.Color = Color.FromArgb(100, 235, 235, 235);

        Chart.ShadingEffectMode = ShadingEffectMode.Two;

        // Corner Customization
        Chart.DefaultBox.CornerSize = 4;
        Chart.TitleBox.CornerSize = 20;
        Chart.ChartArea.CornerTopRight = BoxCorner.Round;
        Chart.TitleBox.CornerTopRight = BoxCorner.Round;
        Chart.TitleBox.CornerTopLeft = BoxCorner.Round;
        Chart.LegendBox.DefaultCorner = BoxCorner.Round;

        // Title Box Customization
        Chart.TitleBox.Label.Text = ".netCHARTING Titlebox";
        Chart.TitleBox.Label.Font = new Font("Arial", 11, FontStyle.Bold);
        Chart.TitleBox.Label.Color = Color.White;
        Chart.TitleBox.Label.Shadow.Color = Color.FromArgb(105, 0, 0, 0);
        Chart.TitleBox.Label.Shadow.Depth = 2;
        Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Two;
        Chart.TitleBox.Background.Color = Color.FromArgb(100, 225, 165, 50);

        // Legend Box Customization
        Chart.LegendBox.HeaderLabel = new dotnetCHARTING.Label("Legend Box", new Font("Arial", 9, FontStyle.Bold), Color.White);
        Chart.LegendBox.HeaderLabel.Alignment = StringAlignment.Center;
        Chart.LegendBox.HeaderBackground.ShadingEffectMode = ShadingEffectMode.Two;
        Chart.LegendBox.HeaderBackground.Color = Color.FromArgb(0, 156, 255);
        Chart.LegendBox.HeaderLabel.Shadow.Color = Color.Gray;
        Chart.LegendBox.HeaderLabel.Shadow.Depth = 1;
        Chart.LegendBox.Background.Color = Color.White;

        // *DYNAMIC DATA NOTE* 
        // This sample uses random data to populate the chart. To populate 
        // a chart with database data see the following resources:
        // - Use the getLiveData() method using the dataEngine to query a database.
        // - Help File > Getting Started > Data Tutorials
        // - DataEngine Class in the help file	
        // - Sample: features/DataEngine.aspx

        SeriesCollection mySC = getRandomData();

        // Add the random data.
        Chart.SeriesCollection.Add(mySC);

        // Add and customize the annotation
        mySC[1][0].Annotation = new Annotation("annotation");
        mySC[1][0].Annotation.HeaderLabel = new dotnetCHARTING.Label("Annotation", new Font("Arial", 8, FontStyle.Bold), Color.White);
        mySC[1][0].Annotation.HeaderLabel.Shadow.Color = Color.Gray;
        mySC[1][0].Annotation.HeaderLabel.Shadow.Depth = 1;
        mySC[1][0].Annotation.HeaderLabel.Color = Color.White;
        mySC[1][0].Annotation.HeaderBackground.ShadingEffectMode = ShadingEffectMode.Two;
        mySC[1][0].Annotation.HeaderBackground.Color = Color.Green;
        mySC[1][0].Annotation.Label.Text = "%Name peak value indicated.";
        mySC[1][0].Annotation.Orientation = dotnetCHARTING.Orientation.TopRight;
        mySC[1][0].Annotation.Orientation = dotnetCHARTING.Orientation.TopRight;
        mySC[1][0].Annotation.DefaultCorner = BoxCorner.Round;
        mySC[1][0].Annotation.Background.Color = Color.White;
    }

    SeriesCollection getRandomData()
    {
        Random myR = new Random(1);
        SeriesCollection SC = new SeriesCollection();
        int a = 0;
        int b = 0;
        for (a = 1; a < 5; a++)
        {
            Series s = new Series("Series " + a.ToString());
            for (b = 1; b < 5; b++)
            {
                Element e = new Element("Element " + b.ToString());
                e.YValue = myR.Next(50);
                s.Elements.Add(e);
            }
            SC.Add(s);
        }
        return SC;
    }

    SeriesCollection getLiveData()
    {
        DataEngine de = new DataEngine("ConnectionString goes here");
        de.ChartObject = Chart; // Necessary to view any errors the dataEngine may throw.
        de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ....";
        return de.GetSeries();
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <dotnet:Chart ID="Chart" runat="server" />
    </div>
</body>
</html>
