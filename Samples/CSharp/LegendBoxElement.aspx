<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">




void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	
	
	// This sample will demonstrate how to use a legend box to refer to particular elements.
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();
	
	// First we make the marker appear on the column and set larger size.
	mySC[3].Elements[1].ForceMarker = true;
	mySC[3].Elements[1].Marker.Size = 10;
	
	// Initialize the new legend box and make it only show the marker and name.
	LegendBox newLegend = new LegendBox();
	newLegend.Template = "%Icon%Name";
	
	// Initialize the new legend entry.
	LegendEntry newEntry = new LegendEntry();
	newEntry.Name = "Important \nElement";	
	
	// Setup the marker for the legend entry
	newEntry.Marker.Type = ElementMarkerType.FivePointStar;
	newEntry.Marker.Size = 10;
	newEntry.SeriesType = SeriesType.Marker;
	// Ensure the same color is used for the marker as the element.
	newEntry.Marker.Color = Chart.Palette[3]; 
	
	// Add the entry to the legend and the legend to the chart.	
	newLegend.ExtraEntries.Add(newEntry);
	Chart.ExtraLegendBoxes.Add(newLegend);
	
	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 4; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}


	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
