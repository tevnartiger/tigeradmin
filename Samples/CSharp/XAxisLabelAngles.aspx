<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 450;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	
	// This sample demonstrates how to angle x axis labels.
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();
	
	mySC[1].XAxis = new Axis();
	mySC[1].XAxis.TickLabelMode = TickLabelMode.Angled;
	mySC[1].XAxis.Orientation = dotnetCHARTING.Orientation.Top;
	
	Chart.XAxis.TickLabelMode = TickLabelMode.Angled;
	
	mySC[1].XAxis.TickLabelAngle = 20;
	Chart.XAxis.TickLabelAngle = 160;
	
	Chart.XAxis.Label.Text = "160 Degree Angle";
	mySC[1].XAxis.Label.Text = "20 Degree Angle";
	
	mySC[2].XAxis = new Axis();
	mySC[2].XAxis.TickLabelMode = TickLabelMode.Angled;
	mySC[2].XAxis.Label.Text = "20 Degree Angle";
	mySC[2].XAxis.TickLabelAngle = 20;
	
	mySC[3].XAxis = new Axis();
	mySC[3].XAxis.TickLabelMode = TickLabelMode.Angled;
	mySC[3].XAxis.Label.Text = "160 Degree Angle";
	mySC[3].XAxis.TickLabelAngle = 160;
	mySC[3].XAxis.Orientation = dotnetCHARTING.Orientation.Top;
	

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
    
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 4; a++)
	{
		Series s = new Series();
		s.Name = "Series " + (a+1);
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.Name = "Element " + (b+1);
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
