<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="C#" Description="dotnetCHARTING Component" %>

		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Size = "800x450";
	Chart.TempDirectory = "temp";

	// This sample demonstrates an interactive way to zoom a section of an axis.

    Chart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Horizontal; 
	Chart.DefaultSeries.Type = SeriesType.AreaLine;
	Chart.DefaultElement.Transparency = 50;
	Chart.DefaultElement.Marker.Visible = false;
	
	
	Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom;
	Chart.ChartAreaSpacing = 15;
	Chart.ChartArea.Label.Text = "Click any two points on this chart.";
	// Add the random data.	
	SeriesCollection sc =  getRandomData();
	Chart.SeriesCollection.Add(sc);
	
	imageLabel.Text = "";
	
	Bitmap bp = null;

	if(Page.Request.Params["x2"] != null || Page.Request.Params["x"] != null)
	{// Only generate the chart the first time if necessary.
		bp = Chart.GetChartBitmap();
	}
	
	// Check if the start and end range is specified
	if(Page.Request.Params["x2"] != null && Page.Request.Params["x"] != null)
	{
	
		// Get x and y points
		int x = Convert.ToInt32( Page.Request.Params["x"]);
		int y = Convert.ToInt32(Page.Request.Params["y"]);
		int x2 = Convert.ToInt32( Page.Request.Params["x2"]);
		int y2 = Convert.ToInt32(Page.Request.Params["y2"]);
		
		// Get the axis values at xy positions.
		object va = Chart.YAxis.GetValueAtY(x+","+y);
		object va2 = Chart.YAxis.GetValueAtY(x2+","+y2);
		
		
		if(va != null && va2 != null)
		{// If both click positions were valid:
		
			// add a zoom area.
			double val = Convert.ToDouble(va);
			double val2 = Convert.ToDouble(va2);
			Chart.YAxis.Markers.Clear();
			ChartArea ca = Chart.ChartArea.GetYZoomChartArea(Chart.YAxis, new ScaleRange(val, val2),new Line(Color.LightGreen,DashStyle.Dash));
			Chart.ExtraChartAreas.Add(ca);

			
		}
	}
	else if(Page.Request.Params["x"] != null)
	{// If only one click position is available:
	
		// Get the value and draw a line marker.
		int x = Convert.ToInt32( Page.Request.Params["x"]);
		int y = Convert.ToInt32(Page.Request.Params["y"]);
		object va = Chart.YAxis.GetValueAtY(x+","+y);
		
		if(va != null)
		{
			double val = Convert.ToDouble(va);
			AxisMarker am = new AxisMarker("",Color.Red,val);
			Chart.YAxis.Markers.Add(am);
			
			// Pass the current click position with the next click.
			imageLabel.Text += "<input type=hidden name=x2 value="+x+">";
			imageLabel.Text += "<input type=hidden name=y2 value="+y+">";
		}
	}	
	
   
	// Generate the chart again.
  	bp = Chart.GetChartBitmap();
	string fileName = Chart.FileManager.SaveImage(bp);
	imageLabel.Text += "<input type=image value=\"submit\" border=0 src=\"" +fileName+ "\" ISMAP>";  
	
	bp.Dispose();	
}



SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(6);
	for(int a = 0; a < 4; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 40; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next((4-a)*20) + (4-a)*60;
			s.Elements.Add(e);
		
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,150,49);
	SC[3].DefaultElement.Color = Color.FromArgb(140,140,255);

	return SC;
}

		</script>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" visible ="false" />
		</div>


<form action="YAxisZoom.aspx" method="get" >
		<asp:Label id="imageLabel" runat="server"/>
		</form>
	</body>
</html>






