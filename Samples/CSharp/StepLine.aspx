<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Size = "600x350";
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = ".netCHARTING Sample";
	Chart.DefaultSeries.Type = SeriesType.Line;
	
	// This sample demonstrates how a step line can be created from a normal line.
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
	Chart.SeriesCollection.Add(makeDateTimeStepSeries(mySC[0]));
	
	Chart.SeriesCollection[0].Name = "Original Series";
	Chart.SeriesCollection[1].Name = "Stepped Series";
        
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
	DateTime dt = new DateTime(2006,1,1);
	for(int a = 1; a < 2; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a.ToString();
		for(int b = 1; b < 10; b++)
		{
			Element e = new Element();
			//e.Name = "Element " + b.ToString();
			e.XDateTime = dt = dt.AddDays(1);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}


	return SC;
}

Series makeDateTimeStepSeries(Series s)
{
	// This method creates a series with an additional element for each original element which creates steps.
	Series ns = new Series();
	for(int i = 0; i < s.Elements.Count;i++)
	{
		if(i > 0)
			ns.Elements.Add(new Element(s.Elements[i-1].Name,s.Elements[i-1].XDateTime.AddSeconds(1),s.Elements[i].YValue));
		ns.Elements.Add(new Element(s.Elements[i].Name,s.Elements[i].XDateTime,s.Elements[i].YValue));
	}
	return ns;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>
