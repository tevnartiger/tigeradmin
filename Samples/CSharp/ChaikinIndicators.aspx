<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
     
	// This sample demonstrates the use of Chaikin indicators
	// These indicators measure to what degree on net an asset is being accumulated 
    // (i.e. brought) or distributed (i.e. sold) by the market as a whole. 
	
	// The Financial Chart
	FinancialChart.Title="Financial Chart";
	FinancialChart.TempDirectory="temp";
	FinancialChart.Debug=true;
	FinancialChart.ShadingEffect = true;
	FinancialChart.LegendBox.Template ="%icon %name";
	FinancialChart.Size="1000X800";
	FinancialChart.DefaultElement.Marker.Visible = false;
	
	FinancialChart.XAxis.Scale = Scale.Time;

	// For Chaikin indicators the time scale is inverted (i.e. the first element of the series is the newest)
	FinancialChart.XAxis.InvertScale = true; 

	FinancialChart.YAxis.Label.Text = "Price (USD)";
	FinancialChart.YAxis.FormatString = "currency";
	FinancialChart.YAxis.Scale = Scale.Range;
	FinancialChart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	
	// Modify the x axis labels.
	FinancialChart.XAxis.TimeInterval = TimeInterval.Day;	
	FinancialChart.XAxis.TimeScaleLabels.DayFormatString = "o";
	FinancialChart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Month);
	FinancialChart.XAxis.TimeScaleLabels.MonthFormatString = "MMM";
	
	DataEngine priceDataEngine = new DataEngine ();
	priceDataEngine.ChartObject = FinancialChart;
	priceDataEngine.ChartType = ChartType.Financial;
	priceDataEngine.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];
	priceDataEngine.DateGrouping = TimeInterval.Day;
	priceDataEngine.StartDate = new DateTime (2002,1,1);
	priceDataEngine.EndDate = new DateTime (2002,4,1);
	priceDataEngine.SqlStatement = @"SELECT TransDate, HighPrice, LowPrice, OpenPrice, ClosePrice, Volume FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate  DESC";
	priceDataEngine.DataFields = "xAxis=TransDate,High=HighPrice,Low=LowPrice,Open=OpenPrice,Close=ClosePrice,Volume=Volume";

	SeriesCollection sc = priceDataEngine.GetSeries ();
	Series prices = null;
	if(sc.Count>0)
		prices = sc[0];
	else
		return;
		
	prices.DefaultElement.ShowValue=true;
	prices.DefaultElement.ToolTip="L:%Low-H:%High";
	prices.DefaultElement.SmartLabel.Font = new Font("Arial", 6);
	prices.DefaultElement.SmartLabel.Text="O:%Open-C:%Close";
	prices.Type = SeriesTypeFinancial.CandleStick;

	CalendarPattern cp = new CalendarPattern (TimeInterval.Day, TimeInterval.Week, "0000001");
	prices.Trim (cp, ElementValue.XDateTime);
	prices.Name = "Prices";
	FinancialChart.SeriesCollection.Add (prices);

	
	FinancialChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical;
	
	// Create the second chart area
	ChartArea volumeChartArea = new ChartArea();
	volumeChartArea.Label.Text = "Stock Volume";
	volumeChartArea.YAxis.Label.Text = "Volumes";
	volumeChartArea.HeightPercentage = 20;
	volumeChartArea.DefaultElement.ToolTip="%YValue";
	FinancialChart.ExtraChartAreas.Add(volumeChartArea);

	// Add a volume series to the chart area
	DataEngine volumeDataEngine = new DataEngine ();
	volumeDataEngine.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];
	volumeDataEngine.DateGrouping = TimeInterval.Days;
	volumeDataEngine.StartDate=new DateTime (2002,1,1);
	volumeDataEngine.EndDate = new DateTime (2002,4,1);
	volumeDataEngine.SqlStatement= @"SELECT TransDate,Volume FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate Desc"; 
	volumeDataEngine.DataFields="xAxis=TransDate,yAxis=Volume";
		
	Series volumes = volumeDataEngine.GetSeries () [0];
	volumes.Trim (cp, ElementValue.XDateTime);
	volumes.Name = "Volumes";
	volumes.Type = SeriesType.Bar;
	volumeChartArea.SeriesCollection.Add (volumes);

	// Accumulate distribute chart area
	ChartArea accDistributeChartArea = new ChartArea ();
	accDistributeChartArea.HeightPercentage = 10;
	accDistributeChartArea.YAxis = new Axis();
	accDistributeChartArea.YAxis.Label.Text = "AccDist";
	FinancialChart.ExtraChartAreas.Add (accDistributeChartArea);

	// The accumulation/distribution indicator illustrates the degree to which an 
    // asset is being accumulated or reduced by the market on a given day. 
	Series acDistribute = FinancialEngine.AcumulateDistributeOverPeriod(prices,5);
	acDistribute.Name = "AcumulateDistributeOver5 per";
	acDistribute.Type = SeriesType.Spline;
	acDistribute.DefaultElement.Color = Color.FromArgb(150,Color.Red);
	accDistributeChartArea.SeriesCollection.Add (acDistribute);

	// Chaikin Chart Area
	ChartArea chaikinChartArea = new ChartArea ();
	chaikinChartArea.HeightPercentage = 20;
	chaikinChartArea.YAxis = new Axis ();
	chaikinChartArea.YAxis.Label.Text = "ChaikinMoneyFlow";
	FinancialChart.ExtraChartAreas.Add (chaikinChartArea);

	// Chaikin Money Flow - Chaikin Money Flow (CMF) is a volume weighted average of 
	//Accumulation/Distribution over a specified period, which is usually taken to be 
	// 21 days.
	Series chMoneyFlowPeriod21 = FinancialEngine.ChaikinMoneyFlowOverPeriod(prices,21);
	chMoneyFlowPeriod21.Name = "ChaikinMoneyFlow21";
	chMoneyFlowPeriod21.Type = SeriesType.Spline;
	chMoneyFlowPeriod21.DefaultElement.Color = Color.FromArgb(150,Color.Green);
	chaikinChartArea.SeriesCollection.Add(chMoneyFlowPeriod21);
		
	// Oscillator Chart Area
	ChartArea oscillatorChartArea = new ChartArea ();
	oscillatorChartArea.HeightPercentage = 10;
	oscillatorChartArea.YAxis = new Axis ();
	oscillatorChartArea.YAxis.Label.Text = "ChaikinOsc";
	FinancialChart.ExtraChartAreas.Add (oscillatorChartArea);

	// The Chaiken Oscillator financial indicator presents the information contained within the A/D 
    // indicator in the convenient form of an oscillator.
	Series chOscillator = FinancialEngine.ChaikinOscillator(prices,0.1);
	chOscillator.Name = "ChaikinOscillator";
	chOscillator.Type = SeriesType.Spline;
	chOscillator.DefaultElement.Color = Color.FromArgb(250,Color.Orange);
	oscillatorChartArea.SeriesCollection.Add (chOscillator);
	        
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="FinancialChart" runat="server"/>			
		</div>
	</body>
</html>
