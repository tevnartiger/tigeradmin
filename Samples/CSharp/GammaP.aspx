<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>


<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	// This sample demonstrates the Gamma P function.

	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Palette = new Color[]{Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)};
	
	Chart.Type = ChartType.Combo;
	Chart.Size = "600x350";
	Chart.Title = "Special Functions";
	
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Help File > Getting Started > Data Tutorials
	// - DataEngine Class in the help file	
	// - Sample: features/DataEngine.aspx
	
	SeriesCollection mySC = getRandomData();
	SeriesCollection gammaPSC = new SeriesCollection();
	// Calculate the gammaP function 
	for(int i = 0; i < 4; i++ ) {
		Series gammaP = ForecastEngine.Advanced.GammaP(mySC[i],10);
		gammaP.Type = SeriesType.Line;
		gammaPSC.Add(gammaP);
	}
	
	// Bessel chart area
	ChartArea gammaChartArea = new ChartArea ();
	gammaChartArea.HeightPercentage = 40;
	gammaChartArea.YAxis.Label.Text = "GammaP";
	Chart.ExtraChartAreas.Add (gammaChartArea);
	
	
	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
	gammaChartArea.SeriesCollection.Add(gammaPSC);
        
}

SeriesCollection getRandomData()
{
	Random myR = new Random(1);
	SeriesCollection SC = new SeriesCollection();
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series("Series " + a.ToString());
		
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element();
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
