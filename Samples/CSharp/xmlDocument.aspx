<%@ Page Language="C#" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml" %>

<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set Title 
    Chart.Title="sales report";
    
    // Set the x axis label
	Chart.ChartArea.XAxis.Label.Text="X Axis Label";
	
	Chart.XAxis.FormatString = "MMM d";

	// Set the y axis label
	Chart.ChartArea.YAxis.Label.Text="Y Axis Label";

    Chart.TempDirectory="temp";
    Chart.Debug=true;
    
    
   //Adding series programatically
   	Chart.Series.Name = "Sales";
   	Chart.Series.Data = GetXmlDocument();
   	Chart.Series.DataFields="xaxis=Name,yaxis=Total";
    Chart.SeriesCollection.Add();
                       
   
}
XmlDocument GetXmlDocument()
{
	XmlDocument doc = new XmlDocument();
	doc.Load(Server.MapPath("../../database/Orders.xml"));
	return doc;

}
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>XmlDocument Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
