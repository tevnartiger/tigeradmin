<%@ Page Language="C#" %>

<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	
	DateTime Dt = DateTime.Now;
        TimeLabel.Text = Dt.ToString();
    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Streaming Usage Sample</title></head>
<body>
<div style="text-align:center">
 <asp:Label id="TimeLabel"  runat="server"/><br>
 <img border="0" src="streaming.aspx"><br>
 View the source of this page in your browser to see that streaming.aspx is included<br>in the page with a regular HTML img reference.  There is no reference to an actual image file.
</div>
</body>
</html>
