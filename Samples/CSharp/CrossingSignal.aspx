<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>


<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Financial Crossing Signal Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
     
	// This sample demonstrates the use of the Crossing Signal indicators within Stochastic
	
	string connection = @"Provider=Microsoft.Jet.OLEDB.4.0;data source=" + 	Server.MapPath("../../database/chartsample.mdb");
	
	// First we declare a chart of type FinancialChart	
	// The Financial Chart
	FinancialChart.Title="Financial Chart";
	FinancialChart.TempDirectory="temp";
	FinancialChart.Debug=true;
	FinancialChart.ShadingEffect = true;
	FinancialChart.LegendBox.Template ="%icon %name";
	FinancialChart.Size="800X600";
	FinancialChart.XAxis.Scale = Scale.Time;
	FinancialChart.XAxis.FormatString = "MMM d";
	FinancialChart.XAxis.TimeInterval = TimeInterval.Day;
	FinancialChart.YAxis.Label.Text = "Price (USD)";
	FinancialChart.YAxis.FormatString = "currency";
	FinancialChart.YAxis.ScaleRange.ValueLow = 11;
		
	// Here we load data samples from the FinancialCompany table from within chartsample.mdb
	DataEngine priceDataEngine = new DataEngine ();
	priceDataEngine.ChartType = ChartType.Financial;
	priceDataEngine.ConnectionString = connection;
	priceDataEngine.DateGrouping = TimeInterval.Day;
	priceDataEngine.StartDate = new DateTime (2001,7,1);
	priceDataEngine.EndDate = new DateTime (2001,12,30);
	priceDataEngine.SqlStatement = @"SELECT TransDate, HighPrice, LowPrice, OpenPrice, ClosePrice FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate ";
	priceDataEngine.DataFields = "xAxis=TransDate,High=HighPrice,Low=LowPrice,Open=OpenPrice,Close=ClosePrice";

	SeriesCollection sc = priceDataEngine.GetSeries ();
	Series prices = null;
	if(sc.Count>0)
		prices = sc[0];
	else
		return;
			
	prices.DefaultElement.ToolTip="L:%Low-H:%High";
	prices.DefaultElement.SmartLabel.Font = new Font("Arial", 6);
	prices.DefaultElement.SmartLabel.Text="O:%Open-C:%Close";
	prices.Type = SeriesTypeFinancial.CandleStick;

	CalendarPattern cp = new CalendarPattern (TimeInterval.Day, TimeInterval.Week, "0000001");
	cp.AdjustmentUnit = TimeInterval.Day;
	prices.Trim (cp, ElementValue.XDateTime);
	prices.Name = "Prices";
	FinancialChart.SeriesCollection.Add (prices);

		
	// Financial Series Crossing Signal and Stochastic Indicators
	
	FinancialChart.DefaultSeries.DefaultElement.Marker = new ElementMarker (ElementMarkerType.None);
	FinancialChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical;
	FinancialChart.DefaultSeries.Type = SeriesType.Spline;

	// Here we display the financial simple moving average for the prices series over five days
	FinancialChart.SeriesCollection.Add (FinancialEngine.SimpleMovingAverage (prices, ElementValue.High, 10));

    // Create a new chart area for KFastStochastic and DStochastic.
	ChartArea stochasticChartArea = new ChartArea ("Stochastic");
	stochasticChartArea.HeightPercentage = 20;
	stochasticChartArea.YAxis = new Axis();
	FinancialChart.ExtraChartAreas.Add (stochasticChartArea);
	
	// Here we display the kFastStochastic over a period of ten days for the Prices Series
	Series kFast = FinancialEngine.KFastStochastic (prices, 10);
	kFast.Type = SeriesType.Spline;
	kFast.DefaultElement.Color = Color.FromArgb(255, 99, 49);
	if (kFast.Elements.Count > 0)
		stochasticChartArea.SeriesCollection.Add (kFast);
	else Console.WriteLine ("The series kfast is empty");
	
	// Here we display the DStochastic over a period of ten days.
	// For the calculation of DStochastic we use SimpleMovingAverage over 10 days.
	Series dStochastic = FinancialEngine.DStochastic (prices, 10, 1, 10);
	dStochastic.Type = SeriesType.Spline;
	dStochastic.DefaultElement.Color = Color.FromArgb(0, 255, 0);
	if (dStochastic.Elements.Count > 0)
		stochasticChartArea.SeriesCollection.Add (dStochastic);
	else Console.WriteLine ("The series dStochastic is empty");


	// Here we display the trading signals based on the crossing of two Stochastics.
	Series crossingSignal = FinancialEngine.CrossingSignal(prices, 10, 1, 10);
	crossingSignal.Type = SeriesType.Marker;
	crossingSignal.LegendEntry.Visible = false;
	FinancialChart.SeriesCollection.Add(crossingSignal);	    	    

	
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="FinancialChart" runat="server"/>
		</div>
	</body>
</html>
