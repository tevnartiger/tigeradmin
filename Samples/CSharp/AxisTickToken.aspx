<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = "Axis Ticks Tokens";
	Chart.DefaultSeries.Type=SeriesType.AreaLine;
	
	Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	Chart.XAxis.Label.Text = "\\<\\%Value,MMM\\>\n\\<\\%Value,o\\>";

	
	// This sample will demonstrate how to use the %Value token with value axis ticks and manipulate their labels.
	
	// X Axis label.
	Chart.XAxis.DefaultTick.Label.Text = "<%Value,MMM> \n <%Value,o>";

	// Setup new axis ticks to.
	AxisTick at = new AxisTick(20, 60, "%Low to %High\n(\\%Low to \\%High)");
	AxisTick at2 = new AxisTick(70, "<%Value,Currency>\n\\<\\%Value,Currency\\>");
	
	// Add the new ticks.
	Chart.YAxis.ExtraTicks.Add(at, at2);
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	
	// Add the random data.
	Chart.SeriesCollection.Add(getRandomData());
    
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
	DateTime dt = new DateTime(2005,1,1);
	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + (a+1);
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.YValue = myR.Next(100);
			e.XDateTime = dt = dt.AddDays(4);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
