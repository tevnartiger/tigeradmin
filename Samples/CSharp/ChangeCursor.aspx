<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
		<script runat="server">



void Page_Load(Object sender,EventArgs e)
{
	//This sample demonstrates how the cursor can be changed based on hovering over hotspots.
	// FullScreen
	
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Title = "Cursor Changes on bars (IE Only)";
	Chart.Palette = new Color[]{Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)};
		
	
     Chart.DefaultElement.Hotspot.Attributes.Custom.Add("OnMouseOver", "scss('pointer');");
     Chart.DefaultElement.Hotspot.Attributes.Custom.Add("OnMouseOut", "scss('default');");  

	// Add the random data.
	Chart.SeriesCollection.Add(getRandomData());
    
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>.netCHARTING Sample</title>

<script language=javascript>
function scss( tid)
{
     myImg = document.getElementById("ChartArea");
     myImg.style.cursor = tid;
}
</script>
</head>
	<body>
		<div align="center" ID="ChartArea">
			<dotnet:Chart id="Chart" runat="server" />
		</div>
	</body>
</html>
