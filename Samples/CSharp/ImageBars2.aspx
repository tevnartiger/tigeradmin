<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	Chart.Type = ChartType.Combo;
	Chart.Size = "1200x500";
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = ".netCHARTING Sample: Image Bars";
	Chart.LegendBox.Visible = false;
	Chart.DefaultAxis.SpacingPercentage = 3;
	
	// This sample demonstrates how to load and set image bar template from the cache.
	
	// Add the random data.
	SeriesCollection mySC = getRandomData();
	
		
	mySC[0].ImageBarTemplate = "../../images/ImageBarTemplates/soccerball";
	mySC[1].ImageBarTemplate = "../../images/ImageBarTemplates/golfball";
	mySC[2].ImageBarTemplate = "../../images/ImageBarTemplates/football";
	mySC[3].ImageBarTemplate = "../../images/ImageBarTemplates/rugbyball";
	mySC[4].ImageBarTemplate = "../../images/ImageBarTemplates/volleyball";
	mySC[5].ImageBarTemplate = "../../images/ImageBarTemplates/bowlingball";
	mySC[6].ImageBarTemplate = "../../images/ImageBarTemplates/basketball";
	mySC[7].ImageBarTemplate = "../../images/ImageBarTemplates/baseball";
	mySC[8].ImageBarTemplate = "../../images/ImageBarTemplates/tennisball";
	mySC[9].ImageBarTemplate = "../../images/ImageBarTemplates/hockeypuck";
	mySC[10].ImageBarTemplate = "../../images/ImageBarTemplates/dollarsign";
	mySC[11].ImageBarTemplate = "../../images/ImageBarTemplates/pills";
	mySC[12].ImageBarTemplate = "../../images/ImageBarTemplates/tablets";
	mySC[13].ImageBarTemplate = "../../images/ImageBarTemplates/coaxialcable";
	mySC[14].ImageBarTemplate = "../../images/ImageBarTemplates/phonecable";
	mySC[15].ImageBarTemplate = "../../images/ImageBarTemplates/ethernetcable";
	mySC[16].ImageBarTemplate = "../../images/ImageBarTemplates/wheat";
	mySC[17].ImageBarTemplate = "../../images/ImageBarTemplates/pipe";
	mySC[18].ImageBarTemplate = "../../images/ImageBarTemplates/figureblue";
	mySC[19].ImageBarTemplate = "../../images/ImageBarTemplates/figurepink";
	mySC[20].ImageBarTemplate = "../../images/ImageBarTemplates/dna";


	
	Chart.SeriesCollection.Add(mySC);
        
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 22; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a.ToString();
		for(int b = 1; b < 2; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b.ToString();
			e.YValue = myR.Next(50)+50;
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}
</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server">
			</dotnet:Chart>			

		</div>
	</body>
</html>
