<%@ Page Language="C#" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>



<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    Chart.Title="Sales for 2002";
    Chart.XAxis.Label.Text="Months";
    Chart.YAxis.Label.Text="Sales (USD)";
    Chart.TempDirectory="temp";
    Chart.Debug=true;
   Chart.Mentor=false;
    Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
    
    Chart.DateGrouping = TimeInterval.Year;
    Chart.Use3D=true;
    
     Axis b=Chart.YAxis.Calculate("Sales (CAD)", new ChangeValueDelegate(MyFunction));
	b.Orientation = dotnetCHARTING.Orientation.Right;
	Chart.AxisCollection.Add(b);
	
	 //Add a series
     DataEngine de = new DataEngine();
    de.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    de.StartDate=new DateTime (2002,1,1,0,0,0);
    de.EndDate = new DateTime (2002,12,31,23,59,59);
    de.DateGrouping = TimeInterval.Year;
	de.SqlStatement= @"SELECT OrderDate, Total FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate";
    SeriesCollection sc = de.GetSeries();

	SetDataToDataGrid(sc);
    
    Chart.SeriesCollection.Add(sc);
}
public void SetDataToDataGrid(SeriesCollection sc)
{
	DataTable dt = new DataTable();
	DataColumn dataCol = new DataColumn();
	dataCol.ColumnName ="Months";
	dt.Columns.Add(dataCol);
	for (int j = 0; j < sc.Count; j++) 
	{
		dotnetCHARTING.Series sr = sc[j];
		sr.Name = "Sales";
		dataCol = new DataColumn();
		dataCol.ColumnName ="Sales (USD)";
		dt.Columns.Add(dataCol);
		
		dataCol = new DataColumn();
		dataCol.ColumnName ="Sales (CAD)";
		dt.Columns.Add(dataCol);

		DataRow dr=null;				
		for (int k = 0; k < sr.Elements.Count; k++) 
		{
			if(j==0)
			{
				dr = dt.NewRow();
				dr[j] = sr.Elements[k].Name;
				dt.Rows.Add(dr);
			}

			if(sr.Elements[k].YValue.Equals(double.NaN))
			{
				dt.Rows[k][j+1]=Chart.EmptyElementText;
				dt.Rows[k][j+2]=Chart.EmptyElementText;
			}
			else
			{
				dt.Rows[k][j+1] = string.Format("{0:C}",sr.Elements[k].YValue);
				dt.Rows[k][j+2] = (sr.Elements[k].YValue * 1.36).ToString("c");

			}
		}
	}
				
	DataView dv = new DataView(dt);
	chartdatagrid.DataSource = dv;
	chartdatagrid.DataBind();

}
public static string MyFunction(string value)
{
	double dValue = Convert.ToDouble(value);
	dValue = dValue*1.36;
	return Convert.ToString(dValue);
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>.netCHARTING Sample</title>
</head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
  <asp:DataGrid id="chartdatagrid" Width="50%" Font-Names="Arial" HeaderStyle-BackColor="skyblue"
BackColor="lightblue" runat="server"/>
</div>
</body>
</html>
