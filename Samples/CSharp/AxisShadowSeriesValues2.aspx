<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	
	// This sample will demonstrate how axis ticks can show data totals.
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();
	
	// Setup the y axis and chart colors.
	Chart.YAxis.Scale = Scale.FullStacked;
	Chart.PaletteName = Palette.Three;
	
	// Create the shadow axis and add it.
	Axis shadow = Chart.YAxis.Calculate("Totals");
	shadow.Orientation = dotnetCHARTING.Orientation.Right;
	shadow.ClearValues = true;
	Chart.AxisCollection.Add(shadow);
	
	// Do some calculations
	double sum = mySC.Calculate("",Calculation.Sum).Calculate("",Calculation.Sum).YValue;
	double s1Sum = mySC[0].Calculate("",Calculation.Sum).YValue;
	double s2Sum = mySC[1].Calculate("",Calculation.Sum).YValue;
	double s3Sum = mySC[2].Calculate("",Calculation.Sum).YValue;
	double runningSum = 0;
	double currentValue = 0;	
	
	currentValue = s1Sum*100/sum;
	
	// Create the axis ticks and set their colors based on the chart palette.
	AxisTick tickA = new AxisTick(runningSum,currentValue+runningSum);
	tickA.Label.Text =  currentValue.ToString("0") + "%";
	tickA.Label.Color = tickA.Line.Color = Chart.Palette[0];

	runningSum += currentValue;
	currentValue = s2Sum*100/sum;
	
	AxisTick tickB = new AxisTick(runningSum,currentValue+runningSum);
	tickB.Label.Text =   currentValue.ToString("0") + "%";
	tickB.Label.Color = tickB.Line.Color = Chart.Palette[1];

	runningSum += currentValue;
	currentValue = s3Sum*100/sum;
	
	AxisTick tickC = new AxisTick(runningSum,currentValue+runningSum);
	tickC.Label.Text =   currentValue.ToString("0") + "%";
	tickC.Label.Color = tickC.Line.Color = Chart.Palette[2];
	
	// Add the extra ticks.
	shadow.ExtraTicks.Add(tickA, tickB, tickC);

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(2);
	for(int a = 0; a < 3; a++)
	{
		Series s = new Series();
		s.Name = "Series " + (a+1);
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.Name = "Element " + (b+1);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series


	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
