<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.DefaultSeries.Type = SeriesType.Line;
	
	// This sample will demonstrate using axis ticks to identify series and show their values.
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();
	
	// Get the x axis closer to the second axis.
	Chart.XAxis.Maximum = 3;
	
	// Setup the shadow axis and add it.
	Axis shadow = Chart.YAxis.Calculate("");
	shadow.Orientation = dotnetCHARTING.Orientation.Right;
	shadow.ClearValues = true;
	Chart.AxisCollection.Add(shadow);	
	
	// Add a tick for each series.
	foreach(Series s in mySC)
	{
		AxisMarker am = new AxisMarker("",new Background(Color.FromArgb(70,s.DefaultElement.Color)),s.Calculate("",Calculation.Minimum).YValue, s.Calculate("",Calculation.Maximum).YValue);
		am.LegendEntry.Visible = false;
		AxisTick tick = new AxisTick(s.Calculate("",Calculation.Minimum).YValue, s.Calculate("",Calculation.Maximum).YValue);
		tick.Label.Text = s.Name;
		shadow.ExtraTicks.Add(tick);
		Chart.YAxis.Markers.Add(am);
	}
	
	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(8);
	for(int a = 0; a < 2; a++)
	{
		Series s = new Series();
		s.Name = "Series " + (a+1);
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			//e.Name = "Element " + (b+1);
			e.XValue = b;
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	//SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	//SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
