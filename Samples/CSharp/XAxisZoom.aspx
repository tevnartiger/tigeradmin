<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Size = "800x450";

	Chart.TempDirectory = "temp";
	
	// This sample demonstrates an interactive way to zoom a section of an axis.

    Chart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Horizontal; 
	Chart.DefaultSeries.Type = SeriesType.Line;
	Chart.DefaultElement.Marker.Visible = false;
	
	
	Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom;
	Chart.ChartAreaSpacing = 15;
	Chart.ChartArea.Label.Text = "Click any two points on this chart.";
	// Add the random data.	
	SeriesCollection sc =  getRandomData();
	Chart.SeriesCollection.Add(sc);

	// Setup x axis ticks.
	Chart.XAxis.TimeScaleLabels.RangeMode = TimeScaleLabelRangeMode.Dynamic;
	Chart.XAxis.TimeScaleLabels.Mode = TimeScaleLabelMode.Hidden;
	Chart.XAxis.TimeScaleLabels.DayTick.Label.Text = "";
    Chart.XAxis.DefaultTick.Label.Font = new Font("Arial",8,FontStyle.Bold);
    Chart.XAxis.Orientation = dotnetCHARTING.Orientation.Top;
    // Generate the chart.
	Bitmap bp = null;

	if(Page.Request.Params["x2"] != null || Page.Request.Params["x"] != null)
	{// Only generate the chart the first time if necessary.
		bp = Chart.GetChartBitmap();
	}
        
        	// Check if the start and end range is specified
	if(Page.Request.Params["x2"] != null && Page.Request.Params["x"] != null)
	{
	
		// Get x and y points
		int x = Convert.ToInt32( Page.Request.Params["x"]);
		int y = Convert.ToInt32(Page.Request.Params["y"]);
		int x2 = Convert.ToInt32( Page.Request.Params["x2"]);
		int y2 = Convert.ToInt32(Page.Request.Params["y2"]);
		
		// Get the axis values at xy positions.
		object va = Chart.XAxis.GetValueAtX(x+","+y);
		object va2 = Chart.XAxis.GetValueAtX(x2+","+y2);
		
		
		if(va != null && va2 != null)
		{// If both click positions were valid:
		
			// add a zoom area.
			DateTime val = Convert.ToDateTime(va);
			DateTime val2 = Convert.ToDateTime(va2);
			Chart.XAxis.Markers.Clear();
			ChartArea ca = Chart.ChartArea.GetXZoomChartArea(Chart.XAxis, new ScaleRange(val, val2),new Line(Color.LightGreen,DashStyle.Dash));
			Chart.ExtraChartAreas.Add(ca);

			
		}
	}
	else if(Page.Request.Params["y"] != null)
	{// If only one click position is available:
	
		// Get the value and draw a line marker.
		int x = Convert.ToInt32( Page.Request.Params["x"]);
		int y = Convert.ToInt32(Page.Request.Params["y"]);
		object va = Chart.XAxis.GetValueAtX(x+","+y);
		
		if(va != null)
		{
			DateTime val = Convert.ToDateTime(va);
			AxisMarker am = new AxisMarker("",Color.Red,val);
			Chart.XAxis.Markers.Add(am);
			
			// Pass the current click position with the next click.
			imageLabel.Text += "<input type=hidden name=x2 value="+x+">";
			imageLabel.Text += "<input type=hidden name=y2 value="+y+">";
		}
	}
	
	
	// Generate the chart again.
  	bp = Chart.GetChartBitmap();
	string fileName = Chart.FileManager.SaveImage(bp);
	imageLabel.Text += "<input type=image value=\"submit\" border=0 src=\"" +fileName+ "\" ISMAP>";  
	
	bp.Dispose();	
}



SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
	DateTime dt = new DateTime(2006,1,1);
	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 2050; b++)
		{
			Element e = new Element();
			//e.Name = "Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(20)+20;
			s.Elements.Add(e);
			e.XDateTime = dt = dt.AddDays(1);
		
		}
		SC.Add(s);
	}


	return SC;
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	
	</head>
	<body>
<form action="XAxisZoom.aspx" method="get" >
		<asp:Label ID="imageLabel2" runat="server"/>
		<asp:Label ID="imageLabel" runat="server"/>
		</form>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px" visible="false">
			</dotnet:Chart>
		</div>
	</body>
</html>
