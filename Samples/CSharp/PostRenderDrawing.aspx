<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="C#" Debug="true" Description="dotnetCHARTING Component" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Post Render Drawing Image</title>
		<script runat="server">




void Page_Load(Object sender,EventArgs e)
{
	//dotnetCHARTING.Chart Chart = new dotnetCHARTING.Chart();

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	
	// First we get our data, if you would like to get the data from a database you need to use
	// the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
	Series s = getRandomData();
	
	
	// Add the random data.
	Chart.SeriesCollection.Add(s);	
	
	//Get a bitmap of the chart.
	Bitmap bmp = Chart.GetChartBitmap();
	
	// Create a graphics object from the bitmap
	Graphics g = Graphics.FromImage(bmp);
	
	
	// After the chart is generated, the element's positions are available.
	// Lets iterate the elements we have added and make an array of these positions. 
	PointF[] points = new PointF[s.Elements.Count];
	for(int i = 0; i < s.Elements.Count; i++)
		points[i] = s.Elements[i].Point;
	
	// Draw a curve connecting the points.
	g.DrawCurve(Pens.Red,points);

	// Draw a string on the first element
	g.DrawString("First Element",new Font("Arial",10),Brushes.Black,points[0]);
	
	g.Dispose();
	
	
	//Save the image using the FileManager.
	Chart.FileManager.SaveImage(bmp);

}

Series getRandomData()
{
		Random myR = new Random();
		Series s = new Series("My Series");
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element("Element " + b, myR.Next(50));
			s.Elements.Add(e);
		}
		return s;
}


        </script>
	</head>
	<body>
		<div style="text-align:center">

           <dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px" Depth="13px" Use3D="True">
			</dotnet:Chart>
			
		</div>
	</body>
</html>
