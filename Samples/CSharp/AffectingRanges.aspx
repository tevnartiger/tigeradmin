<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">




void Page_Load(Object sender,EventArgs e)
{

	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = "Tick affects the axis range.";
	
	Chart2.TempDirectory = "temp";
	Chart2.Debug = true;
	Chart2.Title = "Axis marker affects the axis range.";	
	
	// * INTRO *
	// This sample will demonstrate how axis ticks and axis markers can force an axis scale to expand so they can be seen.

	// 1. CREATE A AXIS TICK ( For Chart1 )
	// Create a marker with the value of 550, well beyond the data we will chart.
	AxisTick at = new AxisTick();
	at.Value = 550;
	at.Label.Color = Color.Red;
	at.Label.Text = "Show On Scale at 550";	
	
	// This property will indicate we want the scale where the tick is shown to expand.
	at.IncludeInAxisScale = true;	
	// Add the tick to the y axis.
	Chart.YAxis.ExtraTicks.Add(at);
		
	// 2. CREATE A AXIS MARKER ( For Chart2 )
	// Create an axis marker with the value of 550, well beyond the data we will chart.
	AxisMarker am = new AxisMarker();
	am.Label.Text = "Quota";
	am.Value = 550;
	am.Line.Color = Color.Red;
	// This property will indicate we want the scale where the marker is shown to expand.
	am.IncludeInAxisScale = true;
	// Add the marker to the y axis.	
	Chart2.YAxis.Markers.Add(am);

	
	// 3. CREATE A AXIS MARKER ( For Chart2 )
	// Create an axis marker with the value of November 2005, well beyond the data we will chart.
	AxisMarker am2 = new AxisMarker();
	am2.Label.Text = "Deadline";
	// Notice the alignment is changed because the default would draw on top of another marker.
	am2.Label.Alignment = StringAlignment.Far;
	am2.Value = new DateTime(2005,11,1);
	am2.Line.Color = Color.Green;
	
	// This property will indicate we want the scale where the marker is shown to expand.
	am2.IncludeInAxisScale = true;
	// Add the marker to the x axis .
	Chart2.XAxis.Markers.Add(am2);


	// 4. ADD DATA
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx

	// Add the random data.
	Chart.SeriesCollection.Add(getRandomData3());
	Chart2.SeriesCollection.Add(getRandomData());

    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
	DateTime dt = new DateTime(2005,1,1);
	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.YValue = myR.Next(50);
			e.XDateTime = dt = dt.AddMonths(1);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}


SeriesCollection getRandomData3()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<dotnet:Chart id="Chart2" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>

		</div>
	</body>
</html>
