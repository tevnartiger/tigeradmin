<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	Table tb = new Table();
	tb.GridLines=GridLines.Both;
	tb.Style.Add("border-collapse","collapse");
	Controls.Add(tb);
	TableRow tr = new TableRow();
	TableCell tc = new TableCell();
	tb.Controls.Add(tr);
	tr.Controls.Add(tc);
	
	dotnetCHARTING.Chart Chart1 = new dotnetCHARTING.Chart();
	tc.Controls.Add(Chart1);
	
	//Set Chart 1 properties
	Chart1.Title="My Chart 1";
	Chart1.Type = ChartType.Combo;		
	Chart1.DefaultSeries.DefaultElement.Transparency = 35;
	Chart1.TempDirectory="temp";
	Chart1.Use3D=true;
	Chart1.Debug=true;
	Chart1.Width = 420;
	Chart1.Height = 300;
	// Add the random data.
	Chart1.SeriesCollection.Add(getRandomData());
	
	tc = new TableCell();
	tr.Controls.Add(tc);
	dotnetCHARTING.Chart Chart2 = new dotnetCHARTING.Chart();
	tc.Controls.Add(Chart2);

	
	//Set Chart 2 properties
	Chart2.Title="My Chart 2";
	Chart2.Type = ChartType.ComboHorizontal;		
	Chart2.DefaultSeries.DefaultElement.Transparency = 35;
	Chart2.TempDirectory="temp";	
	Chart2.Use3D=true;
	Chart2.Debug=true;
	Chart2.Width = 420;
	Chart2.Height = 300;
	// Add the random data.
	Chart2.SeriesCollection.Add(getRandomData());
	
	
	tr = new TableRow();
	tc = new TableCell();
	tb.Controls.Add(tr);
	tr.Controls.Add(tc);
	dotnetCHARTING.Chart Chart3 = new dotnetCHARTING.Chart();
	tc.Controls.Add(Chart3);


	//Set Chart 3 properties
	Chart3.Title="My Chart 3";
	Chart3.Type = ChartType.ComboSideBySide;		
	Chart3.DefaultSeries.DefaultElement.Transparency = 35;
	Chart3.TempDirectory="temp";
	Chart3.Use3D=true;
	Chart3.Debug=true;
	Chart3.Width = 420;
	Chart3.Height = 300;
	// Add the random data.
	Chart3.SeriesCollection.Add(getRandomData());
	
	tc = new TableCell();
	tr.Controls.Add(tc);
	dotnetCHARTING.Chart Chart4 = new dotnetCHARTING.Chart();
	tc.Controls.Add(Chart4);

	
	//Set Chart 4 properties
	Chart4.Title="My Chart 4";
	Chart4.Type = ChartType.Radar;		
	Chart4.DefaultSeries.DefaultElement.Transparency = 35;
	Chart4.RadarLabelMode = RadarLabelMode.Outside;
	Chart4.TempDirectory="temp";
	Chart4.Use3D=true;
	Chart4.Debug=true;
	Chart4.Width = 420;
	Chart4.Height = 300;
	// Add the random data.
	Chart4.SeriesCollection.Add(getRandomData());
	
	
	tr = new TableRow();
	tc = new TableCell();
	tb.Controls.Add(tr);
	tr.Controls.Add(tc);
	dotnetCHARTING.Chart Chart5 = new dotnetCHARTING.Chart();
	tc.Controls.Add(Chart5);

	
	//Set Chart 5 properties
	Chart5.Title="My Chart 5";
	Chart5.Type = ChartType.Pies;		
	Chart5.DefaultSeries.DefaultElement.Transparency = 35;
	Chart5.TempDirectory="temp";
	Chart5.Use3D=true;
	Chart5.Debug=true;
	Chart5.Width = 420;
	Chart5.Height = 300;
	// Add the random data.
	Chart5.SeriesCollection.Add(getRandomData());
	
	tc = new TableCell();
	tr.Controls.Add(tc);
	dotnetCHARTING.Chart Chart6 = new dotnetCHARTING.Chart();
	tc.Controls.Add(Chart6);


	//Set Chart 6 properties
	Chart6.Title="My Chart 6";
	Chart6.Type = ChartType.Donut;		
	Chart6.DefaultSeries.DefaultElement.Transparency = 35;
	Chart6.TempDirectory="temp";
	Chart6.Use3D=true;
	Chart6.Debug=true;
	Chart6.Width = 420;
	Chart6.Height = 300;
	// Add the random data.
	Chart6.SeriesCollection.Add(getRandomData()); 
}
SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 1; b < 8; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Multi Chart Programmatically</title></head>
<body>
</body>

</html>
