<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	
	// This sample will demonstrates using a custom legend entry with additional custom columns.
	
	// First we create the new box.
	LegendBox newLegend = new LegendBox();
	
	// Now we'll set the columns for the box to use. The first 3 columns refer to legend entry properties
	// LegendEntry.Name, LegendEntry.Marker, and LegendEntry.Value. The 4th column is a custom attribute column.
	newLegend.Template = "%Name %Icon %Value %customToken";
	
	// instantiate the legend entries with a name, value, and background color (LegendEntry.Marker will use this background).
	LegendEntry entry1 = new LegendEntry("Entry 1", "No Value",new Background(Color.Orange));
	LegendEntry entry2 = new LegendEntry("Entry 2", "No Value",new Background(Color.Purple));
	LegendEntry entry3 = new LegendEntry("Entry 3", "No Value",new Background(Color.LightBlue));
	
	// Now we'll add custom attributes to populate the customToken column of the legend box by adding custom attributes.
	entry1.CustomAttributes.Add("customToken","custom string 1");
	entry2.CustomAttributes.Add("customToken","custom string 2");
	
	// This shows another method of adding the attributes.
	entry3.CustomAttributes = "customToken=custom string 3";// Equivalent to: .Add("customToken","custom string 3");
	// Multiple attributes can be added using a string like: "key1=value1,key2=value2,key3=value3";
	
	// Add the entries to our custom legend.
	newLegend.ExtraEntries.Add(entry1);
	newLegend.ExtraEntries.Add(entry2);
	newLegend.ExtraEntries.Add(entry3);
	
	// Set an orientation for the custom legend.
	newLegend.Orientation = dotnetCHARTING.Orientation.Top;
	// The the new legend to the chart.
	Chart.ExtraLegendBoxes.Add(newLegend);

	// Add some data.
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	Chart.SeriesCollection.Add(getRandomData());
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 4; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="519px" Height="400px"></dotnet:Chart>
		</div>
	</body>
</html>
