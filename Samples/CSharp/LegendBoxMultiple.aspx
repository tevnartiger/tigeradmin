<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	
	// This sample will demonstrate how multiple chart areas can have their data placed in separate legend boxes.
	// By default all the data is placed in the main legend box, however, if you instantiate a legend box for a 
	// particular chart area, it's data will automatically be placed in it.
	
		// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();
	
	// Set up chart areas. For more info see sample: ChartAreaRelationships.aspx --------------
	Axis y1 = new Axis();
	Axis y2 = new Axis();
	mySC[0].YAxis = y1;
	mySC[1].YAxis = y1;
	mySC[2].YAxis = y2;
	mySC[3].YAxis = y2;

	ChartArea ca2 = new ChartArea();
	ChartArea ca3 = new ChartArea();
	ChartArea ca4 = new ChartArea();
	
	Chart.ChartArea.SeriesCollection.Add(mySC[0]);
	ca2.SeriesCollection.Add(mySC[1]);
	ca3.SeriesCollection.Add(mySC[2]);
	ca4.SeriesCollection.Add(mySC[3]);
	
	Chart.ExtraChartAreas.Add(ca2);
	Chart.ExtraChartAreas.Add(ca3);
	Chart.ExtraChartAreas.Add(ca4);
	Chart.ChartAreaLayout.Mode = ChartAreaLayoutMode.HorizontalPriority;	
	
	// --------------
	
	// Now we'll instantiate legend boxes for 2 of our areas. The main chart area already has a legend box.
	ca2.LegendBox = new LegendBox();
	ca3.LegendBox = new LegendBox();
	
	// Orient the legend boxes in different positions.
	ca3.LegendBox.Orientation = dotnetCHARTING.Orientation.Left;
	ca2.LegendBox.Orientation = dotnetCHARTING.Orientation.Right;	
	Chart.ChartArea.LegendBox.Orientation = dotnetCHARTING.Orientation.Top;	
	
	// Another option besides instantiating a legend box for a chart area is to instruct the tite box to act as the legend box.
	ca4.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	
	// This is all that is required, however, to control properties of this legend a box we'll need to instantiate a legend box
	// and set them there. We will do so now.
	ca4.LegendBox = new LegendBox();
	ca4.LegendBox.LabelStyle.Font = new Font("Verdana",8);
	// The same can be achieved by setting a font for the default entry
	ca4.LegendBox.DefaultEntry.LabelStyle.Font = new Font("Verdana",8);
	
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 4; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
