<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<script runat="server">

		void Page_Load(Object sender,EventArgs e)
		{

			Chart.Type = ChartType.Combo;//Horizontal;
			Chart.Width = 600;
			Chart.Height = 350;
			Chart.TempDirectory = "temp";
			Chart.Debug=true;
			
			// First we get our data, if you would like to get the data from a database you need to use
			// the data engine. See sample: features/dataEngine.aspx.
			SeriesCollection sc = getRandomData();
			
			//The data has one series, we want to sort it in ascending order by yValues.
			sc[0].Sort(ElementValue.YValue,"ASC");

			// Add the random data.
			Chart.SeriesCollection.Add(sc);
		    
		    
		}

		SeriesCollection getRandomData() // Generate some random data.
		{
			Random myR = new Random();
			SeriesCollection SC = new SeriesCollection();

			Series s = new Series();
			s.Name = "My Series";
			for(int b = 1; b < 5; b++)
			{
				Element e = new Element();
				e.Name = "Element " + b;
				e.YValue = myR.Next(50);
				s.Elements.Add(e);
			}
			
			SC.Add(s);
			return SC;
		}


</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Sort Elements</title>

	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
            </dotnet:Chart>
		</div>
	</body>
</html>
