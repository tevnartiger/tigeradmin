<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = "Smart Scale Breaks With Markers";
	
	// This sample will demonstrate how smart breaks can be used with limit markers.	
	
	// Setup the y axis.
	Chart.YAxis.Label.Text = "Bandwidth Usage";
	Chart.YAxis.DefaultTick.Label.Text = "%Value MB";
	Chart.YAxis.SmartScaleBreak = true;	
	Chart.YAxis.Maximum=550;
	
	// Setup the legend box.
	Chart.LegendBox.HeaderEntry.Visible = true;
	Chart.LegendBox.HeaderEntry.Value = "(MB)";
	Chart.LegendBox.HeaderEntry.Name = "Site";
	
	// Create an axis marker and add it..
	AxisMarker am = new AxisMarker("Limit (530 MB)", new Line(Color.Red),530);
	
	// This makes sure the axis range encompases the axis marker.
	am.IncludeInAxisScale = true;
	am.Label.Alignment = StringAlignment.Near;
	
	Chart.YAxis.Markers.Add(am);
	
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();
	
	// Add a divider line to the last series entry.
	mySC[3].LegendEntry.DividerLine.Color = Color.Black;
	
	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 4; a++)
	{
		Series s = new Series();
		s.Name = "Website " + (a+1);
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.Name = "Month " + (b+1);
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
