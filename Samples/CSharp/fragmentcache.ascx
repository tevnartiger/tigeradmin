<%@ Control Language="C#" debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Outputcache Duration = "10" Varybyparam = "none"%>


<script runat="server">

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 1; b < 6; b++)
		{
			Element e = new Element();
			e.Name = "E " + b;
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	return SC;
}


void Page_Load(Object sender,EventArgs e)
{

	
	
	//Set file name 
	Chart.FileName ="fragmentcachesample";
	
	// Set the title to the current time
        DateTime Dt = DateTime.Now;
	Chart.Title="This chart was created at "+Dt.ToString();

	// Set the x axis label
	Chart.ChartArea.XAxis.Label.Text="X Axis Label";

	// Set the y axis label
	Chart.ChartArea.YAxis.Label.Text="Y Axis Label";

	// Set the directory where the images will be stored.
	Chart.TempDirectory="temp";

	// Set the bar shading effect
	Chart.ShadingEffect = true;

	// Set he chart size.
	Chart.Width = 600;
	Chart.Height = 350;
	
	Chart.Debug=true;

	// Add the random data.
	Chart.SeriesCollection.Add(getRandomData());
    
    
}

</script>

 <dotnet:Chart id="Chart"  runat="server"/>
