<%@ Page Language="C#" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="dotnetCHARTING" %>

<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	if(!IsPostBack)
	{
    	ListItem myListItem = new ListItem("Jan","1");
    	MonthOfYearDropDown.Items.Add(myListItem);
    	myListItem = new ListItem("Feb","2");
    	MonthOfYearDropDown.Items.Add(myListItem);
		myListItem = new ListItem("Mar","3");
    	MonthOfYearDropDown.Items.Add(myListItem);
    	myListItem = new ListItem("Apr","4");
    	MonthOfYearDropDown.Items.Add(myListItem);
    	myListItem = new ListItem("May","5");
    	MonthOfYearDropDown.Items.Add(myListItem);
		myListItem = new ListItem("Jun","6");
    	MonthOfYearDropDown.Items.Add(myListItem);
    	myListItem = new ListItem("Jul","7");
    	MonthOfYearDropDown.Items.Add(myListItem);
    	myListItem = new ListItem("Aug","8");
    	MonthOfYearDropDown.Items.Add(myListItem);
		myListItem = new ListItem("Sep","9");
    	MonthOfYearDropDown.Items.Add(myListItem);
    	myListItem = new ListItem("Oct","10");
    	MonthOfYearDropDown.Items.Add(myListItem);
		myListItem = new ListItem("Nov","11");
    	MonthOfYearDropDown.Items.Add(myListItem);
		myListItem = new ListItem("Dec","12");
    	MonthOfYearDropDown.Items.Add(myListItem);
	}
	
	//set global properties
	Chart.Title="sales";
	Chart.ChartArea.XAxis.Label.Text="Years";
	Chart.TempDirectory="temp";
	Chart.Debug=true; 
	Chart.Use3D=true;
	Chart.DateGrouping = TimeInterval.Years;
	//Chart.XAxis.FormatString="MM/dd";
	Chart.StartDateOfYear = new DateTime(2004,MonthOfYearDropDown.SelectedIndex+1,1);
	Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
	//Chart.DefaultSeries.Type = SeriesType.AreaLine;
	Chart.DefaultSeries.DefaultElement.Transparency =20;

	Chart.OverlapFooter=false;

	    

	//Add a series
	Chart.Series.Name="Item Sales ";
	Chart.Series.StartDate=new System.DateTime(2002,1,1,0,0,0);
	Chart.Series.EndDate = new System.DateTime(2002,12,31,23,59,59);
	Chart.Series.SqlStatement= @"SELECT OrderDate,Sum(Quantity) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate";
	Chart.SeriesCollection.Add();

		//Add a series
	Chart.Series.Name="Orders";
	Chart.Series.StartDate=new System.DateTime(2002,1,1,0,0,0);
	Chart.Series.EndDate = new System.DateTime(2002,12,31,23,59,59);
	Chart.Series.SqlStatement= @"SELECT OrderDate,Sum(1) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate";
	Chart.SeriesCollection.Add();
    
 
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Start Date of Year Options</title>
</head>
<body topmargin="0" leftmargin="0">
    <form runat="server">
        <div style="text-align:center">
            <table border="1" cellpadding="3" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" id="AutoNumber1">
                <tbody>
                <tr>
    <td width="1%">
<img border="0" src="../../images/dotnetCharting.gif" width="230" height="94"></td>
    <td width="99%" bgcolor="#BFC0DB">Start Month of Year: 
 <ASP:DropDownList id="MonthOfYearDropDown" runat="server">
    </ASP:DropDownList>
     <asp:Button id="ButtonSet" runat="server" Text="Set">
    </asp:Button>
 </td>
          </tbody>
            </table>
            <DOTNET:Chart id="Chart" runat="server" Visible="true" />
        </div>
    </form></body>
</html>