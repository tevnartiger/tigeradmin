<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = "Element SubValue Styling";
	
	// This sample will demonstrate how to style element sub values such as error bars.
	
	// Set a default error offset value.
	Chart.DefaultElement.ErrorOffset = 5;
	
	// Default Line Styling
	Chart.DefaultElement.DefaultSubValue.Line.DashStyle = DashStyle.Dash;
	Chart.DefaultElement.DefaultSubValue.Line.Length = 55;
	Chart.DefaultElement.DefaultSubValue.Line.Width = 2;
	Chart.DefaultElement.DefaultSubValue.Line.Color = Color.Black;
	
	// Default Marker Styling
	Chart.DefaultElement.DefaultSubValue.Marker.Type = ElementMarkerType.FivePointStar;
	Chart.DefaultElement.DefaultSubValue.Marker.Size = 12;
	Chart.DefaultElement.DefaultSubValue.Marker.Color = Color.Orange;	
		
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();
	// Add the random data.
	Chart.SeriesCollection.Add(mySC);	
	
	// Specify SubValue Types
	mySC[0].Elements[0].DefaultSubValue.Type = SubValueType.ErrorBar;
	mySC[0].Elements[1].DefaultSubValue.Type = SubValueType.Line;
	mySC[0].Elements[2].DefaultSubValue.Type = SubValueType.Marker;
	
	// Name the elements
	mySC[0].Elements[0].Name = "ErrorBar";
	mySC[0].Elements[1].Name = "Line";
	mySC[0].Elements[2].Name = "Marker";
	
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(2);
	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + (a+1);
		for(int b = 0; b < 3; b++)
		{
			Element e = new Element();
			e.Name = "Element " + (b+1);
			//e.YValue = -25 + myR.Next(50);
			e.YValue = 20+ myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	
	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
