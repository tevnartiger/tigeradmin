<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	Chart.Type = ChartType.Map;
	Chart.Size = "1000x600";
	Chart.Title = " US states with custom sales information";
	Chart.TempDirectory = "temp";
	Chart.Mapping.DefaultShape.Label.OutlineColor = Color.White;
	Chart.ChartArea.Background.Color = Color.FromArgb(220,255,255);
	Chart.TitleBox.Label.Color = Color.Black;
	Chart.TitleBox.Background.Color = Color.LightYellow;
	Chart.TitleBox.Background.GlassEffect = true;
	
	// This sample demonstrates how to integrate custom data from a database with a map layer.
	MapLayer layer = MapDataEngine.LoadLayer("../../images/MapFiles/primusa.shp");
	layer.ImportData(CreateDataTable(),"STATE_ABBR","Code");
	
	SmartPalette sp = Chart.SmartPalette = new SmartPalette();
	
	sp.Add("SALES",new SmartColor(Color.FromArgb(0,250,0), new ScaleRange(1,1000)));
	sp.Add("SALES",new SmartColor(Color.FromArgb(0,200,0), new ScaleRange(1000,10000)));
	sp.Add("SALES",new SmartColor(Color.FromArgb(0,150,0), new ScaleRange(10000,100000)));
	sp.Add("SALES",new SmartColor(Color.FromArgb(0,100,0), new ScaleRange(100000,1000000)));
	
	Chart.Mapping.ZoomPercentage = 120;
				
	Chart.Mapping.ZoomCenterPoint = new PointF(35,-98);
	Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	Chart.Mapping.MapLayerCollection.Add(layer);
	Chart.Mapping.DefaultShape.Label.Text = "$%Sales";
	Chart.Mapping.DefaultShape.Label.Hotspot.ToolTip = "%State_Name";

}
private DataTable CreateDataTable()
{
	DataTable dt = new DataTable();
	string connString = "Provider=Microsoft.Jet.OLEDB.4.0;user id=admin;password=;data source=" + 	Server.MapPath("../../database/chartsample.mdb");
	OleDbConnection conn = new OleDbConnection(connString);
	OleDbDataAdapter adapter = new OleDbDataAdapter();
	string selectQuery = @"SELECT [Code],[Sales] FROM Locations"; 
	adapter.SelectCommand = new OleDbCommand(selectQuery, conn);
	adapter.Fill(dt);
	return dt;
}
</script>
	</head>
	<body>
		<div style="text-align:center">
				<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>