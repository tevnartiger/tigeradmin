<%@ Page Language="C#" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>

<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    Chart.Title="Item sales report";
    
    // Set the x axis label
	Chart.ChartArea.XAxis.Label.Text="X Axis Label";

	// Set the y axis label
	Chart.ChartArea.YAxis.Label.Text="Y Axis Label";
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    
    
   //Adding series programatically
   	Chart.Series.Name = "Item sales";
    SetData();
    Chart.SeriesCollection.Add();
                       
   
}
void SetData()
{
	string connString = @"Provider=Microsoft.Jet.OLEDB.4.0;user id=admin;password=;data source=" + Server.MapPath("../../database/chartsample.mdb");  
    OleDbConnection conn = new OleDbConnection(connString);
   
    string selectQuery = @"SELECT Name,Sum(Quantity) FROM Orders GROUP BY Orders.Name ORDER BY Orders.Name"; 
    OleDbCommand myCommand = new OleDbCommand(selectQuery, conn);
   	conn.Open();
   	OleDbDataReader myReader= myCommand.ExecuteReader();
   	
   	//set dataReader to data property
   	Chart.Series.Data = myReader;
   	
   	//Cleanup
 	myReader.Close();
   	conn.Close();
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Data Reader Sample</title>
</head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
