<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="C#" debug="true" Description="dotnetCHARTING Component" %>

<script runat="server">

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}


	return SC;
}


void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.TempDirectory = "temp";
	
	// Set this property in order to get error messages when the chart has problems:
	Chart.Debug = true;
	
	
	// If there is a problem with the data engine, we can also debug it with the following:
	
	// We setup a dataengine object and cause an error.
	DataEngine de = new DataEngine();
	de.ConnectionString = "error causing connection string";
	de.SqlStatement = "error causing statement";
	SeriesCollection sc = de.GetSeries();
	
	// Now assign the error message to a label on the page.
	Label.Text = "DataEngine ErrorMessage: " + de.ErrorMessage;
	
	
	Chart.YAxis.Scale = Scale.Time;
	

	// Add the random data.
	Chart.SeriesCollection.Add(getRandomData());
    
    
}
		</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Debugging Sample</title>

	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" /><br>
			<asp:Label id="Label" runat="server" />
		</div>
	</body>
</html>
