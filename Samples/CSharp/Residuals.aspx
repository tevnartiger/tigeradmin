<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Residuals Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
     
	// This sample demonstrates the use of EstimateX and EstimateY from
	// within Correlation class
	
	// The Correlation Chart
	CorrelationChart.Title="Correlation";
	CorrelationChart.TempDirectory="temp";
	CorrelationChart.Debug=true;
	CorrelationChart.Size = "1000x400";
	CorrelationChart.LegendBox.Template ="%icon %name";
	CorrelationChart.XAxis.Scale = Scale.Normal;
		
	SeriesCollection scorrelation = new SeriesCollection();
	Series sampledata1 = new Series ("Sample 1");
	for ( int b=1; b<10; b++) {
		Element el = new Element();
		el.YValue = -Math.Sin(b)* b;
		el.XValue = b ;
		sampledata1.Elements.Add (el);
	}
	scorrelation.Add (sampledata1);
	scorrelation[0].Type = SeriesType.Spline;

	CorrelationChart.SeriesCollection.Add (scorrelation);
	/*
	 * Takes off the marker off the line and spline series.
	 */
	CorrelationChart.DefaultSeries.DefaultElement.Marker = new ElementMarker (ElementMarkerType.None);
	

	
	// Here we create a series which will hold 
	// the estimated value of the Y variable when the X variable is known
	Series estYPoints = new Series();
	for (int i = 1; i < 10; i++){
		Element estY = StatisticalEngine.EstimateY(sampledata1,i);
		estYPoints.Elements.Add(estY);
	}
	estYPoints.Name = "EstYPoints";
	estYPoints.DefaultElement.Color = Color.FromArgb(255,99,49);
	estYPoints.DefaultElement.Marker = new ElementMarker (ElementMarkerType.Circle);
	estYPoints.Type = SeriesType.Marker;
	CorrelationChart.SeriesCollection.Add(estYPoints);
	
	
	// Here we create a series which will hold 
	// the estimated value of the X variable when the Y variable is known
	Series estXPoints = new Series();
	for (int i = 1; i < 10; i++){
		Element estX = StatisticalEngine.EstimateX(sampledata1,-i);
		estXPoints.Elements.Add(estX);
	}
	estXPoints.Name = "EstXPoints";
	estXPoints.DefaultElement.Marker = new ElementMarker (ElementMarkerType.Triangle);
	estXPoints.Type = SeriesType.Marker;
	CorrelationChart.SeriesCollection.Add(estXPoints);
	
	ChartArea residualsChartArea = new ChartArea ("Residuals");
	residualsChartArea.HeightPercentage = 20;
	CorrelationChart.ExtraChartAreas.Add (residualsChartArea);

	// Here we create a series which display the residuals
	// in accordance with the regression line constructed using LeastSquaresRegressionLineX 
	Series residuals = StatisticalEngine.Residuals(sampledata1);
	residuals.Type = SeriesType.Spline;
	residuals.Name = "Residuals";
	residuals.DefaultElement.Color = Color.FromArgb(125,255,49);
	residualsChartArea.SeriesCollection.Add (residuals);
	
	// Here we calculate the residual Average Of the data sample
	Element residualAvg = StatisticalEngine.ResidualsAverage(sampledata1);
	residualAvg.XValue = 1;
	Series resAvg = new Series();
	resAvg.Type = SeriesType.Marker;
	resAvg.DefaultElement.Marker = new ElementMarker (ElementMarkerType.Circle, 5, Color.FromArgb(255, 99, 49));
	resAvg.Name = "ResidualAverage";
	if (residualAvg.YValue == 0) 
		Console.WriteLine ("The residuals average is 0.");
	else
	{
		resAvg.Elements.Add(residualAvg);
		resAvg.DefaultElement.Color = Color.FromArgb(99,255,49);
		residualsChartArea.SeriesCollection.Add(resAvg);
	}
	
	
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="CorrelationChart" runat="server"/>
		
			
		</div>
	</body>
</html>
