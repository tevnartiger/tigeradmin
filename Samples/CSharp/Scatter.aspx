<%@ Page Language="C#" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>

<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    Chart.Title="Scatter sample";
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.Type=ChartType.Scatter;
    Chart.DefaultSeries.DefaultElement.ShowValue=true;
    Chart.DefaultSeries.DefaultElement.LabelTemplate="(%Xvalue,%Yvalue)";
    
   //Adding series programatically
   	Chart.Series.Name = "Series 1";
   	Chart.Series.SqlStatement = @"SELECT * FROM SampleData";
   	Chart.Series.DataFields="xaxis=X,yaxis=Y,tooltip=ToolTip";
   	Chart.Series.Type = SeriesType.Spline;
    Chart.SeriesCollection.Add();
                       
   
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Scatter sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
