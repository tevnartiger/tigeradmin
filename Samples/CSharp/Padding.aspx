<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Pies;//Horizontal;
	Chart.Width = 700;
	Chart.Height = 550;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = "Padding chart elements";
	
	// This sample will demonstrate how to pad different parts of the chart.
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();
	
	// 1. PADDING THE CHART IMAGE
	Chart.Margin = "50";
	
	// 2. PADDING THE TITLE BOX
	Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	Chart.TitleBox.Padding = 20;
	
	// 3. PADDING LEGEND BOXES IN TITLES
	// This shows how padding of legendboxes works independently of the title boxes they are in.
	// If a chart area is new with the legend in title and doesnt have a legend box instantiated 
	// you must do so before you can  modify it's properties.
	Chart.LegendBox.Padding = 8;
	
	// 4. PADDING LEGEND BOXES
	// We'll create another legend box to show the padding functionality
	LegendBox lb = new LegendBox();
	lb.HeaderEntry.Visible = true;
	lb.HeaderEntry.Name = "New Box";
	lb.HeaderEntry.Value = "";
	lb.HeaderEntry.CustomAttributes = "Icon=";
	// Legend entries can also add padding at the top which is useful with when used as headers.
	lb.HeaderEntry.PaddingTop = 25;
	lb.Padding = 10;
	lb.DataSource =  mySC;
	// Add the new legend box.
	Chart.ExtraLegendBoxes.Add(lb);
	
	// 5. PADDING BETWEEN PIES AND LABELS
	// This works also with radar and gauge charts.	
	Chart.XAxis.TickLabelPadding = 15;
	
	// 6. PADDING OF CHART BOXES
	// This includes chart areas, legend boxes etc.
	Chart.ChartAreaSpacing = 20;
	
	// 7. PADDING BETWEEN PIES
	// This works also with radar and gauge charts.	
	Chart.ChartArea.Padding = 50;
	
	// 8. PADDING ANNOTATIONS
	Annotation an = new Annotation("Padded \nannotation");
	an.Position = new Point(15,250);
	// Specify the annotaiton padding.
	an.Padding = 25;	
	// Add the annotation.
	Chart.Annotations.Add(an);
	
	// NOTE: If you would like to control column width please see sample features/BarFixedSize.aspx
	
	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
        
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 4; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
