<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
     
	// This sample demonstrates the use of one-value financial indicators arithmeticMean ,
	// geometric mean, standard deviation, mean deviation, range, median.
	
	// The Financial Chart
	FinancialChart.Title="Financial Chart";
	FinancialChart.TempDirectory="temp";
	FinancialChart.Debug=true;
	FinancialChart.ShadingEffect = true;
	FinancialChart.LegendBox.Template ="%icon %name";
	FinancialChart.Size="800X600";
	FinancialChart.YAxis.Label.Text = "Price (USD)";
	FinancialChart.YAxis.FormatString = "currency";
	FinancialChart.YAxis.Scale = Scale.Range;
	FinancialChart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	FinancialChart.DefaultElement.ShowValue = true;
	FinancialChart.DefaultElement.Marker.Visible = false;
	
			// Modify the x axis labels.
	FinancialChart.XAxis.Scale = Scale.Time;
	FinancialChart.XAxis.TimeInterval = TimeInterval.Day;		
	FinancialChart.XAxis.TimeScaleLabels.DayFormatString = "o";
	FinancialChart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Month);
	FinancialChart.XAxis.TimeScaleLabels.MonthFormatString = "MMM";
	
	DataEngine priceDataEngine = new DataEngine ();
	priceDataEngine.ChartObject = FinancialChart;
	priceDataEngine.ChartType = ChartType.Financial;
	priceDataEngine.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];
	priceDataEngine.DateGrouping = TimeInterval.Day;
	priceDataEngine.StartDate = new DateTime (2001,2,1);
	priceDataEngine.EndDate = new DateTime (2001,2,28);
	// Here we import financial data sample from the FinancialCompany table from within chartsample.mdb
	priceDataEngine.SqlStatement = @"SELECT TransDate, HighPrice, LowPrice, OpenPrice, ClosePrice FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate";
	priceDataEngine.DataFields = "xAxis=TransDate,High=HighPrice,Low=LowPrice,Open=OpenPrice,Close=ClosePrice";

	SeriesCollection sc = priceDataEngine.GetSeries ();
	Series prices = null;
	if(sc.Count>0)
		prices = sc[0];
	else
		return;
		
	prices.DefaultElement.ToolTip="L:%Low | H:%High";
	prices.DefaultElement.SmartLabel.Text="O:%Open | C:%Close";
	prices.Type = SeriesTypeFinancial.CandleStick;

	CalendarPattern cp = new CalendarPattern (TimeInterval.Day, TimeInterval.Week, "0000001");
	prices.Trim (cp, ElementValue.XDateTime);
	prices.Name = "Prices";
	FinancialChart.SeriesCollection.Add (prices);
	
	FinancialChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical;
		
	/*
	 * One-Value Indicators
	 */
	ChartArea indicatorsChartArea = new ChartArea ("Calculations");
	indicatorsChartArea.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	indicatorsChartArea.XAxis = new Axis ();
	indicatorsChartArea.YAxis = new Axis ();
	indicatorsChartArea.HeightPercentage = 40;
	indicatorsChartArea.DefaultElement.DefaultSubValue.Type = SubValueType.Line;
    indicatorsChartArea.DefaultElement.DefaultSubValue.Line.Length = 10;
	FinancialChart.ExtraChartAreas.Add (indicatorsChartArea);
		
	// Mean - calculates the arithmetic mean of the currently registered data set. 
	Series mean = new Series ("Mean");
	// If this option is setted true it will allow the user to place all the source values
	// into the elementís subValues collection
	FinancialEngine.Options.PopulateSubValues = true; 
	mean.Elements.Add (FinancialEngine.Mean (prices, ElementValue.High));
	mean.Elements.Add (FinancialEngine.Mean (prices, ElementValue.Low));
	mean.Elements.Add (FinancialEngine.Mean (prices, ElementValue.Open));
	mean.Elements.Add (FinancialEngine.Mean (prices, ElementValue.Close));
	mean.Type = SeriesType.Bar;
	indicatorsChartArea.SeriesCollection.Add (mean);

	// GeometricMean - calculates the geometric mean of the currently registered data set. 
	Series GeometricMean = new Series ("GeometricMean");
	GeometricMean.Elements.Add (FinancialEngine.GeometricMean (prices, ElementValue.High));
	GeometricMean.Elements.Add (FinancialEngine.GeometricMean (prices, ElementValue.Low));
	GeometricMean.Elements.Add (FinancialEngine.GeometricMean (prices, ElementValue.Open));
	GeometricMean.Elements.Add (FinancialEngine.GeometricMean (prices, ElementValue.Close));
	GeometricMean.Type = SeriesType.Bar;
	indicatorsChartArea.SeriesCollection.Add (GeometricMean);

	// StdDev - calculates the sample standard variance of the currently registered data set.
	Series stdDev = new Series ("StdDev");
	// Here we set the value of the option  WithSubValues to false.
	FinancialEngine.Options.Reset();
	stdDev.Elements.Add (FinancialEngine.StandardDeviation (prices, ElementValue.High));
	stdDev.Elements.Add (FinancialEngine.StandardDeviation (prices, ElementValue.Low));
	stdDev.Elements.Add (FinancialEngine.StandardDeviation (prices, ElementValue.Open));
	stdDev.Elements.Add (FinancialEngine.StandardDeviation (prices, ElementValue.Close));
	stdDev.Type = SeriesType.Bar;
	indicatorsChartArea.SeriesCollection.Add (stdDev);

	// MeanDev - calculates the mean deviation of the currently registered data set.
	Series meanDev = new Series ("MeanDev");
	meanDev.Elements.Add (FinancialEngine.MeanDeviation (prices, ElementValue.High));
	meanDev.Elements.Add (FinancialEngine.MeanDeviation (prices, ElementValue.Low));
	meanDev.Elements.Add (FinancialEngine.MeanDeviation (prices, ElementValue.Open));
	meanDev.Elements.Add (FinancialEngine.MeanDeviation (prices, ElementValue.Close));
	meanDev.Type = SeriesType.Bar;
	indicatorsChartArea.SeriesCollection.Add (meanDev);

	// Range - calculates the range of the currently registered data set.
	Series range = new Series("Range");
	range.Elements.Add (FinancialEngine.Range (prices, ElementValue.High));
	range.Elements.Add (FinancialEngine.Range (prices, ElementValue.Low));
	range.Elements.Add (FinancialEngine.Range (prices, ElementValue.Open));
	range.Elements.Add (FinancialEngine.Range (prices, ElementValue.Close));
	range.Type = SeriesType.Bar;
	indicatorsChartArea.SeriesCollection.Add (range);

	// Median - calculates the median of the currently registered data set. 
	Series median = new Series ("Median");
	median.Elements.Add (FinancialEngine.Median (prices, ElementValue.High));
	median.Elements.Add (FinancialEngine.Median (prices, ElementValue.Low));
	median.Elements.Add (FinancialEngine.Median (prices, ElementValue.Open));
	median.Elements.Add (FinancialEngine.Median (prices, ElementValue.Close));
	median.Type = SeriesType.Bar;
	indicatorsChartArea.SeriesCollection.Add (median);
			        
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="FinancialChart" runat="server"/>
			
			
		</div>
	</body>
</html>
