<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	// This sample demonstrates using ImageBar templates.

	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Palette = new Color[]{Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)};
	
	Chart.Type = ChartType.Combo;
	Chart.Size = "600x350";
	Chart.Title = ".netCHARTING Sample";
	Chart.LegendBox.Visible = false;
	Chart.DefaultAxis.SpacingPercentage = 3;
	
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Help File > Getting Started > Data Tutorials
	// - DataEngine Class in the help file	
	// - Sample: features/DataEngine.aspx
	
	SeriesCollection mySC = getRandomData();

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
	mySC[0].ImageBarTemplate = "../../images/ImageBarTemplates/battery";
	mySC[1].ImageBarTemplate = "../../images/ImageBarTemplates/batteryvertical";
	mySC[2].ImageBarTemplate = "../../images/ImageBarTemplates/binder";
	mySC[3].ImageBarTemplate = "../../images/ImageBarTemplates/cake";
	mySC[4].ImageBarTemplate = "../../images/ImageBarTemplates/chain";
	mySC[5].ImageBarTemplate = "../../images/ImageBarTemplates/gold";
	mySC[6].ImageBarTemplate = "../../images/ImageBarTemplates/goldbar";
	mySC[7].ImageBarTemplate = "../../images/ImageBarTemplates/money";
	mySC[8].ImageBarTemplate = "../../images/ImageBarTemplates/paperchart";
	mySC[9].ImageBarTemplate = "../../images/ImageBarTemplates/paperplain";
	mySC[10].ImageBarTemplate = "../../images/ImageBarTemplates/papertext";
	mySC[11].ImageBarTemplate = "../../images/ImageBarTemplates/phonecabletwist";
	mySC[12].ImageBarTemplate = "../../images/ImageBarTemplates/platinum";
	mySC[13].ImageBarTemplate = "../../images/ImageBarTemplates/rope";
	mySC[14].ImageBarTemplate = "../../images/ImageBarTemplates/silver";

        
}

SeriesCollection getRandomData()
{
	Random myR = new Random(2);
	SeriesCollection SC = new SeriesCollection();
	for(int a = 1; a < 16; a++)
	{
		Series s = new Series("Series " + a.ToString());
		for(int b = 1; b < 2; b++)
		{
			Element e = new Element("Element " + b.ToString());
			e.YValue = 50+myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
