<%@ Page Language="C#" Debug="true" Trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
   	Chart.Title="Item sales";
    Chart.XAxis.Label.Text="Customers";
    Chart.YAxis.Label.Text="Sales (USD)";

    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.DefaultSeries.DefaultElement.ToolTip="%yvalue";
     
          
 
    //Add a series
    //Chart.Series.Name="Item Sale";
    Chart.Series.StartDate=new System.DateTime(2002,1,1,0,0,0);
    Chart.Series.EndDate = new System.DateTime(2002,12,31,23,59,59);
    Chart.Series.SqlStatement= @"SELECT Orders.Name, Sum(Total) As TotalSale, Sum(1) As TotalOrder  FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.Name ORDER BY Orders.Name";  							
    Chart.Series.DataFields ="xAxis=Name,yAxis=TotalSale=Total Sales,yAxis=TotalOrder=Total Orders";
    Chart.SeriesCollection.Add();
    
Chart.PostDataProcessing +=new PostDataProcessingEventHandler(OnPostDataProcessing);

}
void OnPostDataProcessing(Object sender)
{	
	
	Axis ATotal = new Axis();
   ATotal.Orientation = dotnetCHARTING.Orientation.Right;
   ATotal.Label.Text = "Number of Orders";

   Chart.SeriesCollection[1].YAxis = ATotal;
	
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Multi Series From Columns Sample</title>
</head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
 
</div>
</body>
</html>
