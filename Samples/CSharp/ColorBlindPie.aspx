<%@ Page Language="C#" debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Color Blind Pie</title>
		<script runat="server">




void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Pies;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	
	// This sample demonstrates how to apply markers to the data when differences in color
	// are difficult to distinguish.
	
	// First we get our data, if you would like to get the data from a database you need to use
	// the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
	SeriesCollection sc = getRandomData();
	
	// The function below takes care of modifying the series.
	ModifySeries(sc);
	

	// Add the random data.
	Chart.SeriesCollection.Add(sc);
    
    
}

	// Markers array
	private static ElementMarkerType[] defaultDotTypes = {
																 ElementMarkerType.Circle,ElementMarkerType.FourPointStar,ElementMarkerType.Diamond,ElementMarkerType.FivePointStar,
																 ElementMarkerType.Square,ElementMarkerType.SevenPointStar,ElementMarkerType.Triangle,ElementMarkerType.SixPointStar
															 };

void ModifySeries(SeriesCollection sc)
{

	foreach(Series s in sc)
	{
	
		// We want the markers to always appear no matter what the chart or series type. This setting will ensure that.
		s.DefaultElement.ForceMarker = true;
		// We also want the marker to appear in the legend so we need to set the series type of the series legend entry.
		s.DefaultElement.LegendEntry.SeriesType = SeriesType.Marker;
		
		// Each element must also use this marker type and if shown in the legend it must be shown as a marker.
		for(int i = 0; i < s.Elements.Count; i++)
		{
			s.Elements[i].Marker.Type = defaultDotTypes[i%defaultDotTypes.Length];
			s.Elements[i].LegendEntry.Marker.Type = defaultDotTypes[i%defaultDotTypes.Length];
		}
	}


}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 2; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	
	return SC;
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
