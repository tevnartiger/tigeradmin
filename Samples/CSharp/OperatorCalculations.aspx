<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component" %>
		<script runat="server">

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "GNP";
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element();
			e.Name = "Country " + b;
			// Load a very large value.
			e.YValue = myR.Next(50000000)+myR.Next(50000000)+myR.Next(50000000)+myR.Next(50000000);
			e.YValue += myR.Next(50000000)+myR.Next(50000000)+myR.Next(50000000)+myR.Next(50000000);
			e.YValue += myR.Next(500000000)+myR.Next(50000000)+myR.Next(50000000)+myR.Next(500000000);
			e.YValue += myR.Next(500000000)+myR.Next(50000000)+myR.Next(50000000)+myR.Next(500000000);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}
void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.TempDirectory = "temp";
	Chart.YAxis.NumberPrecision =2;
	Chart.Debug=true;
	
	// This sample demonstrates how operators can be used on series to manipulate chart data.
	
	// Scenario:
	// We are charting dollar amounts some nation's GNP. These values are very large so we will scale 
	// the data down and show it in billions.
	
	// First we get our data, if you would like to get the data from a database you need to use
	// the data engine. See sample: features/dataEngine.aspx.
	SeriesCollection sc = getRandomData();
	
	// We can scale the data by multiplying the series.
	for(int i = 0; i < sc.Count; i ++)
		sc[i] = sc[i]*.000000001;
		
	// Now we label the axis apropriately.
	Chart.YAxis.Label.Text = "(GNP in Billions)";
	
	
	// Add the random data.
	Chart.SeriesCollection.Add(sc);
}
</script>
		<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Operator Calculations</title>

	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px" />
		</div>
	</body>
</html>
