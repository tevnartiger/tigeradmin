<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Dynamic Annotations</title>
		<script runat="server">




void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	
	
	// This sample demonstrates using annotations with data sources.
	
	// First we get our data, if you would like to get the data from a database you need to use
	// the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
	SeriesCollection sc = getRandomData();
	
	// We'll create an axis marker to use as the source for the annotation.
	AxisMarker am = new AxisMarker("my Marker", new Line(Color.Red),30);
	Chart.YAxis.Markers.Add(am);	
	
	// Instantiate our annotation.
	Annotation a = new Annotation();
	
	// Any of the following can be used as source but we'll use the axis marker.
	//a.DataSource = sc[0].Elements[0];
	//a.DataSource = sc[0];
	a.DataSource = am;
	
	// Set the text
	a.Label.Text = "Source Name: %name \n Value: %High";
	
	
	// Set a point where the annotation appears
	a.Position = new Point(50,30);
	
	// By default the annotation will create a rectangle for the text. In order to place the text on one line we'll 
	// set the dynamic size property to false.
	a.DynamicSize = false;
	
	// Add the annotation.
	Chart.Annotations.Add(a);

	// Add the random data.
	Chart.SeriesCollection.Add(sc);
    
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
