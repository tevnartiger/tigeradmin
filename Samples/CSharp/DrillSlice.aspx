<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Drill Slice</title>
		<script runat="server">


void Page_Load(Object sender,EventArgs e)
{

	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Use3D = true;
	
	// Set the chart type.
	Chart.Type = ChartType.Donuts;	
	
	// Get the static chart data.
	SeriesCollection sc = getStaticData();
	
	
	// When a pie slice is clicked we'll pass the series and element name in the query string.
	// In order to pass info to the next time this page loads we will specify a url to use on the pie slice link.
	// By setting the default element all elements will inherit this setting.
	Chart.DefaultSeries.DefaultElement.URL = "?eName=%Name&sName=%SeriesName";
	
	
	// Here we will look for info in the query string that will tell us which slice was clicked.
	// Get the series and element names from the query string.
	string seriesName = Request.QueryString["sName"];
	string elementName = Request.QueryString["eName"];
	
	// Use the GetElement method to find our element by passing the series and element name.
	Element el = sc.GetElement(seriesName,elementName);
	
	// If we get an element with the specified name, set the ExplodeSlice property to true.
	if(el != null)
		el.ExplodeSlice = true;
	
	// Add the static data.
	Chart.SeriesCollection.Add(sc);
    
    
}

SeriesCollection getStaticData()
{
	// Create a series collection object.
	SeriesCollection SC = new SeriesCollection();
	
	// Create a series object
	Series s = new Series("mySeries");
	
	// Add some elements
	s.Elements.Add(new Element("1",15));
	s.Elements.Add(new Element("2",13));
	s.Elements.Add(new Element("3",17));
	s.Elements.Add(new Element("4",16));
	
	// Add the series to the series collection.
	SC.Add(s);
	
	// Repeat with another series.
	Series s2 = new Series("mySeries 2");
	s2.Elements.Add(new Element("1",15));
	s2.Elements.Add(new Element("2",13));
	s2.Elements.Add(new Element("3",17));
	s2.Elements.Add(new Element("4",16));
	
	SC.Add(s2);
	
	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
