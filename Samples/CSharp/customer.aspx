<%@ Page Language="C#" debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	string customerName= Request.QueryString["name"];
	//set global properties
    Chart.Title="Orders by " + customerName;
    Chart.ChartArea.XAxis.Label.Text="Months";

    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.ChartArea.XAxis.FormatString = "MMM";
    Chart.DateGrouping = TimeInterval.Year;

    	
	Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
	Chart.DefaultSeries.StartDate=new System.DateTime(2002,1,1,0,0,0);
    Chart.DefaultSeries.EndDate = new System.DateTime(2002,12,31,23,59,59);

  
    //Add a series
    Chart.Series.Name="Orders";
    Chart.Series.SqlStatement= @"SELECT OrderDate, Sum(1) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# AND Name='" + customerName + "' GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate";
    Chart.SeriesCollection.Add();
    
       
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Orders Report</title>
</head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
