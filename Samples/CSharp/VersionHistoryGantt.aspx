<%@ Page Language="C#" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>

<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
    //set global properties
    Chart.Size="1270X500";
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.Title=".netCHARTING Version History";
    Chart.Type=ChartType.Gantt;
    Chart.ShadingEffect=true;
    Chart.Use3D=true;
    Chart.Depth=5;
    Chart.YAxis.Label.Text="Versions";
    Chart.XAxis.Scale = Scale.Time;
    Chart.ChartArea.Label.Text="We continue to innovate and solve\nreal-world client issues. Discover the\ndifference .netCHARTING can make!";
    Chart.XAxis.TimePadding = new TimeSpan(1,0,0,0);
    Chart.XAxis.TimeInterval = TimeInterval.Months;
    Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
       
    //Add a series
    Chart.Series.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"]; 
    Chart.Series.DataFields="ganttstartdate=startDate,ganttenddate=ReleaseDate,ganttname=Version";
    Chart.Series.DefaultElement.ShowValue=true;
    Chart.Series.PaletteName=Palette.Two;
    Chart.Series.SqlStatement= @"SELECT StartDate,ReleaseDate,Version FROM Versions WHERE Version <> '1.0'";
    Chart.SeriesCollection.Add(); 
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>.netCHARTING Version History</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart" runat="server"/>
</div>
</body>
</html>
