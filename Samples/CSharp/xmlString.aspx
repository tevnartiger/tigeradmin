<%@ Page Language="C#" debug="true" trace="false" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Xml" %>

<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//This sample demonstrates how to use an xml string to generate a chart.
	
	//set Title 
    Chart.Title="sales report";
    
    // Set the x axis label
	Chart.ChartArea.XAxis.Label.Text="X Axis Label";
	
	Chart.XAxis.FormatString = "MMM d";

	// Set the y axis label
	Chart.ChartArea.YAxis.Label.Text="Y Axis Label";

    Chart.TempDirectory="temp";
    Chart.Debug=true;
    
    
   //Adding series programatically
   	Chart.Series.Name = "Sales";
   	Chart.Series.Data = GetXmlString();
   	Chart.Series.DataFields="xaxis=Name,yaxis=Total";
    Chart.SeriesCollection.Add();
                       
   
}
string GetXmlString()
{
	StringBuilder xmlString = new StringBuilder("<?xml version=\"1.0\" standalone=\"yes\"?>");
	xmlString.Append(@"<NewDataSet>
  	<OrderTable>
    <Name>Aida</Name>
    <Total>3759</Total>
</OrderTable>
<OrderTable>
     <Name>Ain</Name>
    <Total>3510</Total>
</OrderTable>
<OrderTable>
<Name>Ali</Name>
    <Total>5478</Total>
</OrderTable>
<OrderTable>
     <Name>Barbi</Name>
    <Total>3029</Total>
</OrderTable>
<OrderTable>
<Name>Bard</Name>
    <Total>2743</Total>
</OrderTable>
<OrderTable>
     <Name>Bo</Name>
    <Total>2331</Total>
</OrderTable>
<OrderTable>
<Name>David</Name>
    <Total>1954</Total>
</OrderTable>
<OrderTable>
     <Name>Joe</Name>
    <Total>7661</Total>
</OrderTable>
<OrderTable>
<Name>John</Name>
    <Total>7026</Total>
</OrderTable>
<OrderTable>
     <Name>Kei</Name>
    <Total>6408</Total>
  </OrderTable>
  <OrderTable>
     <Name>Moe</Name>
    <Total>6287</Total>
  </OrderTable>
<OrderTable>
     <Name>Pearl</Name>
    <Total>4105</Total>
  </OrderTable>
</NewDataSet>");
return xmlString.ToString();
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>XmlString Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
