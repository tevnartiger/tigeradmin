<%@ Page Language="C#" debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Tick Label Mode Sample</title>
		<script runat="server">
void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	
	Chart1.Type = ChartType.ComboHorizontal;
	Chart1.Width = 600;
	Chart1.Height = 350;
	Chart1.TempDirectory = "temp";
	Chart1.Debug = true;
	
	Chart2.Type = ChartType.Combo;
	Chart2.Width = 600;
	Chart2.Height = 350;
	Chart2.TempDirectory = "temp";
	Chart2.Debug = true;
	
	// This sample will demonstrate how ....
	
	// First we get our data, if you would like to get the data from a database you need to use
	// the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
	SeriesCollection mySC = getRandomData();
	
	
	Chart.YAxis.Label.Text = "TickLabelMode.Normal";
	Chart.YAxis.TickLabelMode = TickLabelMode.Normal;
	Chart.XAxis.Label.Text = "TickLabelMode.Normal";
	Chart.XAxis.TickLabelMode = TickLabelMode.Normal;
	
	
	Chart1.YAxis.Label.Text = "TickLabelMode.Wrapped";
	Chart1.YAxis.TickLabelMode = TickLabelMode.Wrapped;
	Chart1.XAxis.Label.Text = "TickLabelMode.Angled";
	Chart1.XAxis.TickLabelMode = TickLabelMode.Angled;
	
	Chart2.YAxis.Label.Text = "TickLabelMode.Normal";
	Chart2.YAxis.TickLabelMode = TickLabelMode.Normal;
	Chart2.XAxis.Label.Text = "TickLabelMode.Wrapped";
	Chart2.XAxis.TickLabelMode = TickLabelMode.Wrapped;
	

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
	Chart1.SeriesCollection.Add(getRandomData2());
	Chart2.SeriesCollection.Add(getRandomData2());
    
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 4; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 3; b++)
		{
			Element e = new Element();
			e.Name = "Long Name Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}

SeriesCollection getRandomData2()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 5; b++)
		{
			Element e = new Element();
			e.Name = "Long Name Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<dotnet:Chart id="Chart1" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<dotnet:Chart id="Chart2" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
