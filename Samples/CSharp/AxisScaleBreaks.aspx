<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	
	
	// This sample demonstrates using automatic scale breaks with numeric and time axes.
	

	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	
	
	// Page link related code.
	string page = HttpContext.Current.Request.QueryString["page"];
	if(page == null || page == "") page = "1";

	
	if(page == "1") // Numeric, no break
	{
		// Add the random data.
		Chart.SeriesCollection.Add(getRandomNumericData());
		Chart.Title = "No Smart Scale Break";
	}
	else if(page == "2") // Numeric, with break
	{
		
		// Activate the automatic scale break
		Chart.YAxis.SmartScaleBreak = true;
	
		// Add the random data.
		Chart.SeriesCollection.Add(getRandomNumericData());
		Chart.Title = "Smart Scale Break";
	}
	else if(page == "3") // Time, no break
	{

		// Add the random data.
		Chart.SeriesCollection.Add(getRandomTimeData());
		Chart.Title = "Time Scale without Smart Scale Break";	
	}
	else if(page == "4") // Time, with break
	{
		// Activate the automatic scale break
		Chart.YAxis.SmartScaleBreak = true;
	
		// Add the random data.
		Chart.SeriesCollection.Add(getRandomTimeData());
		Chart.Title = "Time Scale with Smart Scale Break";
	}
	
}

SeriesCollection getRandomNumericData()
{

	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
	for(int a = 0; a < 4; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	
	// Make one element very large
	SC[0].Elements[2].YValue = 1000;

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}

SeriesCollection getRandomTimeData()
{

	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	
	DateTime dt = new DateTime(2002,2,2);
	for(int a = 0; a < 4; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			dt = dt.AddDays(1);
			e.YDateTime = dt;

			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	
	
	// Make one element very large
	SC[0].Elements[2].YDateTime = new DateTime(2005,2,2);

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
		<a href="?page=1">Numeric Without Smart Break</a> | <a href="?page=2">Numeric With Smart Break</a> | <a href="?page=3">Time Without Smart Break</a> | <a href="?page=4">Time With Smart Break</a><br>
		</div>
		<div style="text-align:center">&nbsp;</div>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px"></dotnet:Chart></div>
	</body>
</html>
