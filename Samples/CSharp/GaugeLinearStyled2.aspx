<%@ Page Language="C#" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    void Page_Load(Object sender, EventArgs e)
    {
        // This sample demonstrates a horizontal linear gauge using shading effect Three and customized axis line.
        Chart.Size = "220x110";

        Chart.TempDirectory = "temp";
        Chart.Debug = true;
        Chart.Palette = new Color[] {  Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) };
        Chart.LegendBox.Visible = false;

        Chart.Type = ChartType.Gauges;
        Chart.DefaultSeries.GaugeType = GaugeType.Horizontal;
        Chart.ShadingEffectMode = ShadingEffectMode.Three;

        Chart.Margin = "-6";
        Chart.ChartArea.ClearColors();


        Chart.DefaultElement.Transparency = 40;

        Chart.YAxis.Line.Width = 3;
        Chart.YAxis.Line.EndCap = LineCap.ArrowAnchor;
        Chart.YAxis.Line.AnchorCapScale = 2;
        Chart.DefaultSeries.GaugeBorderBox.DefaultCorner = BoxCorner.Round;
        Chart.DefaultSeries.GaugeBorderShape = GaugeBorderShape.UseBox;
        Chart.DefaultSeries.GaugeBorderBox.Padding = 2;
        Chart.YAxis.Label.Text = "Hits";
        Chart.YAxis.TickLabelPadding = 2;
        Chart.YAxis.Orientation = dotnetCHARTING.Orientation.Top;

        // *DYNAMIC DATA NOTE* 
        // This sample uses random data to populate the chart. To populate 
        // a chart with database data see the following resources:
        // - Help File > Getting Started > Data Tutorials
        // - DataEngine Class in the help file	
        // - Sample: features/DataEngine.aspx

        SeriesCollection mySC = getRandomData();

        // Add the random data.
        Chart.SeriesCollection.Add(mySC);

    }

    SeriesCollection getRandomData()
    {
        return new SeriesCollection(new Series("",new Element("Element 1", 8)));
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <dotnet:Chart ID="Chart" runat="server" />
    </div>
</body>
</html>
