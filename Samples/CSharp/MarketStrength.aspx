<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
     
	// This sample demonstrates the use of BalanceOfPower financial indicator
	
	// The Financial Chart
	FinancialChart.Title="Financial Chart";
	FinancialChart.TempDirectory="temp";
	FinancialChart.Debug=true;
	FinancialChart.ShadingEffect = true;
	FinancialChart.LegendBox.Template ="%icon %name";
	FinancialChart.Size="800X600";
	FinancialChart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	
	// For financial indicators the time scale is inverted (i.e. the first element of the series is the newest)
	FinancialChart.XAxis.InvertScale = true; 
	FinancialChart.YAxis.Label.Text = "Price (USD)";
	FinancialChart.YAxis.FormatString = "currency";
	FinancialChart.YAxis.Scale = Scale.Range;

	
				// Modify the x axis labels.
	FinancialChart.XAxis.Scale = Scale.Time;
	FinancialChart.XAxis.TimeInterval = TimeInterval.Day;		
	FinancialChart.XAxis.TimeScaleLabels.DayFormatString = "o";
	FinancialChart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Month);
	FinancialChart.XAxis.TimeScaleLabels.MonthFormatString = "MMM";
	
	DataEngine priceDataEngine = new DataEngine ();
	priceDataEngine.ChartObject = FinancialChart;
	priceDataEngine.ChartType = ChartType.Financial;
	priceDataEngine.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];
	priceDataEngine.DateGrouping = TimeInterval.Day;
	priceDataEngine.StartDate = new DateTime (2001,6,1);
	priceDataEngine.EndDate = new DateTime (2001,6,30);
	priceDataEngine.SqlStatement = @"SELECT TransDate, HighPrice, LowPrice, OpenPrice, ClosePrice FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate Desc";
	priceDataEngine.DataFields = "xAxis=TransDate,High=HighPrice,Low=LowPrice,Open=OpenPrice,Close=ClosePrice";

	SeriesCollection sc = priceDataEngine.GetSeries ();
	Series prices = null;
	if(sc.Count>0)
		prices = sc[0];
	else
		return;
	prices.Name = "Prices";	
	prices.DefaultElement.ShowValue=true;
	prices.DefaultElement.ToolTip="L:%Low | H:%High";
	prices.DefaultElement.SmartLabel.Text="O:%Open | C:%Close";
	prices.Type = SeriesTypeFinancial.CandleStick;

	CalendarPattern cp = new CalendarPattern (TimeInterval.Day, TimeInterval.Week, "0000001");
	prices.Trim (cp, ElementValue.XDateTime);

	FinancialChart.SeriesCollection.Add (prices);

	// BalanceOfPower chart area
	ChartArea balanceChartArea = new ChartArea ();
	balanceChartArea.HeightPercentage = 30;
	balanceChartArea.YAxis.Label.Text = "MarketStrength";
	FinancialChart.ExtraChartAreas.Add (balanceChartArea);

	// BalanceOfPower - Implements the Balance of Power (BOP) indicator, created by Igor Livshin; 
	// which captures the struggle between the Bulls and Bears throughout a trading 
	// day. The BOP indicator is evaluated by the following formulae: BOP = (Close - Open)/(High - Low)
	Series balanceOverPeriod = FinancialEngine.BalanceOfPowerOverPeriod("Balance Of Power", prices);
	balanceOverPeriod.Type = SeriesType.Spline;
	balanceChartArea.SeriesCollection.Add (balanceOverPeriod);

	        
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="FinancialChart" runat="server"/>
			
			
		</div>
	</body>
</html>

