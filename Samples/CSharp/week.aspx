<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    Chart.Title="Orders";
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    Chart.DefaultSeries.StartDate=new System.DateTime(2002,3,10,0,0,0);
    Chart.DefaultSeries.EndDate = new System.DateTime(2002,3,16,23,59,59);
    Chart.DateGrouping = TimeInterval.Week;
    
    //Add a series
    Chart.Series.Name="Order";
    Chart.Series.SqlStatement= @"SELECT OrderDate,1 AS q FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate";
    Chart.SeriesCollection.Add();
    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Date Grouping By Week Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
