<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    void Page_Load(Object sender, EventArgs e)
    {
        // This sample demonstrates using the ImageBarSyncToValue feature where the image bars repeat the same number of times as represented by the element's value.

        Chart.TempDirectory = "temp";
        Chart2.TempDirectory = "temp";
        Chart.Debug = false;
        Chart.Size = "600x350";
        Chart.Title = "Football Touchdowns - ImageBarSyncToValue = true";
        Chart.YAxis.Maximum = 8;
        Chart.YAxis.Interval = 1;
        Chart.LegendBox.Visible = false;    
            
        Chart2.Size = "600x350";
        Chart2.Title = "Football Touchdowns - ImageBarSyncToValue = false (default)";
        Chart2.YAxis.Maximum = 8;
        Chart2.YAxis.Interval = 1; 
        Chart2.LegendBox.Visible = false;  
               
        // *DYNAMIC DATA NOTE* 
        // This sample uses random data to populate the chart. To populate 
        // a chart with database data see the following resources:
        // - Help File > Getting Started > Data Tutorials
        // - DataEngine Class in the help file	
        // - Sample: features/DataEngine.aspx

        SeriesCollection mySC = getRandomData();
        mySC[0].ImageBarTemplate = "../../images/ImageBarTemplates/football";

        Chart.DefaultSeries.ImageBarSyncToValue = true;
        Chart2.DefaultSeries.ImageBarSyncToValue = false;

        // Add the random data.
        Chart.SeriesCollection.Add(mySC);
        Chart2.SeriesCollection.Add(mySC);
    }

    SeriesCollection getRandomData()
    {
        Random myR = new Random();
        SeriesCollection SC = new SeriesCollection();
        int a = 0;
        int b = 0;
        for (a = 1; a < 2; a++)
        {
            Series s = new Series("Series " + a.ToString());
            for (b = 1; b < 7; b++)
            {
                Element e = new Element("Team " + b.ToString());
                e.YValue = 1 + myR.Next(7);
                s.Elements.Add(e);
            }
            SC.Add(s);
        }
        return SC;
    }

    SeriesCollection getLiveData()
    {
        DataEngine de = new DataEngine("ConnectionString goes here");
        de.ChartObject = Chart; // Necessary to view any errors the dataEngine may throw.
        de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ....";
        return de.GetSeries();
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <dotnet:Chart ID="Chart" runat="server" />
    </div>
    <div align="center">
        <dotnet:Chart ID="Chart2" runat="server" />
    </div>
</body>
</html>
