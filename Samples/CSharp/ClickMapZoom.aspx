<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	Chart.Type = ChartType.Map;//Horizontal;
	Chart.Size = "600x350";
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = ".netCHARTING Sample";
	Chart.Visible = false;
	Chart.ChartArea.Label.Text = "Please click to zoom and center the map\n view on the position clicked.";
	Chart.ChartArea.Background = new Background(Color.FromArgb(142,195,236),Color.FromArgb(63,137,200),90);
	Chart.Mapping.DefaultShape.Background.Color = Color.LightGray;

	
	// This sample demonstrates how to zoom into a map, based on a click position using lat/long coordinates..
	// FullScreen
	
	Bitmap bp = null;
	
	// Set the chart zooming settings if necessary (it was more than once).	
	// This will ensure the click position will return the correct results because the map was already zoomed when clicked.
	if(Page.Request.Params["x2"] != null && Page.Request.Params["y2"] != null)
	{
		float x = (float)Convert.ToDouble( Page.Request.Params["x2"]);
		float y = (float)Convert.ToDouble(Page.Request.Params["y2"]);
		Chart.Mapping.ZoomCenterPoint = new PointF(x,y);
		Chart.Mapping.ZoomPercentage = 250;
	}
	
	Chart.Mapping.MapLayerCollection.Add(@"../../images/MapFiles/primusa.shp");
	if(Page.Request.Params["y"] != null && Page.Request.Params["x"] != null)
	{
	
		bp = Chart.GetChartBitmap();
		
		// Get x and y points
		int x = Convert.ToInt32( Page.Request.Params["x"]);
		int y = Convert.ToInt32(Page.Request.Params["y"]);
		
		PointF p = Chart.Mapping.GetLatLongCoordinates(new Point(x,y));

		Chart.Mapping.ZoomCenterPoint = p;
		Chart.Mapping.ZoomPercentage = 250;
		
		// Pass the current click position with the next click.
		imageLabel.Text += "<input type=hidden name=x2 value="+p.X+">";
		imageLabel.Text += "<input type=hidden name=y2 value="+p.Y+">";
	}
	
    // Generate the chart again.
  	bp = Chart.GetChartBitmap();
	string fileName = Chart.FileManager.SaveImage(bp);
	imageLabel.Text += "<input type=image value=\"submit\" border=0 src=\"" +fileName+ "\" ISMAP>";  
	
	bp.Dispose();	
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	</head>
	<body>
		<div align="center">
		<form method =get >
		<asp:Label ID=imageLabel Runat=server/>
			</form>
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
