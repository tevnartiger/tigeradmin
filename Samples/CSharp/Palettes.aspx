<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>

<script runat="server">
private void Page_Load(object sender, System.EventArgs e)
{	
	//This sample demonstrates different palette settings.
	foreach(string name in Enum.GetNames(typeof(dotnetCHARTING.Palette)))
	{
		if(name=="FiveColor1")
			break;			
		dotnetCHARTING.Chart chartObject = new dotnetCHARTING.Chart();
		chartObject.Type = ChartType.Pies;
		chartObject.Title= "Palette Name: " + name;
		chartObject.TempDirectory="temp";
		chartObject.Debug=true;
		chartObject.ShadingEffect = true;
		chartObject.LegendBox.Template ="%icon %name";
		chartObject.Size="800X600";
		chartObject.PaletteName = (dotnetCHARTING.Palette)Enum.Parse(typeof(dotnetCHARTING.Palette),name);
		chartObject.SeriesCollection.Add(getRandomData());
		Controls.Add(chartObject);
		
	}
}
SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Series s = new Series();
		for(int b = 1; b <51; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b.ToString();
			e.YValue = 3;
			s.Elements.Add(e);
		}
	SC.Add(s);
	return SC;
}
</script>
	</head>
	<body>
		<center>
		</center>
	</body>
</html>
