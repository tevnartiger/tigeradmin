<%@ Page Language="C#" debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>


<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{ 
	// This sample demonstrates the use of NormalDistribution
	// within StatisticalEngine.
	
	// The Distribution Chart
	DistributionChart.Title="Distribution";
	DistributionChart.TempDirectory = "temp";
	DistributionChart.Debug=true;
	DistributionChart.Size = "600x300";
	DistributionChart.LegendBox.Template ="%icon %name";
	DistributionChart.XAxis.ScaleRange.ValueLow =30;
	DistributionChart.XAxis.Interval = 5;
	DistributionChart.YAxis.Percent = true;
	DistributionChart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	DistributionChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical;
	DistributionChart.DefaultSeries.DefaultElement.Marker.Visible = false;
	
	double[] normalDistVector = new double[] {40, 55, 70, 85, 115, 130, 145, 160};


		
	Series sampledata = new Series ("Sample1");
	for ( int i=0; i < normalDistVector.Length; i++) {
		Element el = new Element();
		el.XValue = normalDistVector[i];
		sampledata.Elements.Add (el);
	}
	
	
	// The second parameter of this method is the standard deviation of the normal probability distribution
	Series normalDistribution = StatisticalEngine.NormalDistribution(sampledata, 19	);
	normalDistribution.Type = SeriesType.AreaSpline;
	normalDistribution.DefaultElement.ShowValue = false;
	normalDistribution.DefaultElement.SmartLabel.Text = "%PercentOfTotal";
	//normalDistribution.DefaultElement.SmartLabel.Color = Color.Black;
	normalDistribution.DefaultElement.ShowValue = true;
	
	DistributionChart.DefaultElement.SmartLabel.Alignment = LabelAlignment.Center;
	
	//normalDistribution.
	
	
	//Series tmpS = new Series("",normalDistribution.Trim(new ScaleRange(50,70),ElementValue.XValue).Elements);
	Series tmpS = new Series("",normalDistribution.Elements);
	tmpS.Trim(new ScaleRange(0,70),ElementValue.XValue);
	ColorElements(tmpS,Color.Orange);
	
	tmpS = new Series("",normalDistribution.Elements);
	tmpS.Trim(new ScaleRange(130,190),ElementValue.XValue);
	ColorElements(tmpS,Color.Orange);
		
	tmpS = new Series("",normalDistribution.Elements);
	tmpS.Trim(new ScaleRange(71, 85),ElementValue.XValue);
	ColorElements(tmpS,Color.Blue);

	tmpS = new Series("",normalDistribution.Elements);
	tmpS.Trim(new ScaleRange(115, 129),ElementValue.XValue);
	ColorElements(tmpS,Color.Blue);	
	DistributionChart.SeriesCollection.Add (normalDistribution);	

}

void ColorElements(Series s, Color color)
{
int count = s.Elements.Count;
int i = 0;
	foreach(Element el in s.Elements)
	{
		//if(count/2 == i)
		el.ShowValue = true;
		el.Color = color;
		i++;
	}
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="DistributionChart" runat="server"/>
		</div>
	</body>
</html>
