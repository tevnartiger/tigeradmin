<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	// This sample demonstrates different projections available for map rendering along with and ecw image layer.
	Chart.Title = "No Projection";	
	Chart.Mapping.ZoomPercentage = 100;
	LoadDefaults(Chart,3);
	
	Chart1.Title = "Mercator Projection";	
	Chart1.Mapping.Projection.Type = ProjectionType.Mercator;
	Chart1.Mapping.Projection.Parameters = "-60";
	LoadDefaults(Chart1,2);
	
	Chart2.Title = "Lambert Conic Projection";	

	Chart2.Mapping.Projection.Type = ProjectionType.LambertConic;
	Chart2.Mapping.Projection.Parameters = "-10,-20,-32,49";



	LoadDefaults(Chart2,1);

}

void LoadDefaults(Chart c, int unique)
{
	c.Type = ChartType.Map;
	c.Size = "800x400";

	c.TempDirectory = "temp";
	MapLayer layer = MapDataEngine.LoadLayer( "../../images/MapFiles/worldmap.ecw","../../images/MapFiles/worldmapProj"+unique+".jpg");
	c.Debug = true;
	c.Mapping.MapLayerCollection.Add(layer);
	c.Mapping.MapLayerCollection.Add( "../../images/MapFiles/states.shp");
	c.Mapping.MapLayerCollection.Add( "../../images/MapFiles/europe.shp");	
	c.Mapping.DefaultShape.Line.Color = Color.White;
}

</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
			<dotnet:Chart id="Chart1" runat="server" >
			</dotnet:Chart>
			<dotnet:Chart id="Chart2" runat="server" >
			</dotnet:Chart>

		</div>
	</body>
</html>
