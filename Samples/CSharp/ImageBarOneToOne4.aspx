<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    void Page_Load(Object sender, EventArgs e)
    {
        // This sample demonstrates using the ImageBarSyncToValue feature where the image bars repeat the same number of times as represented by the element's value.

        Chart.TempDirectory = "temp";
        Chart.Debug = true;
        Chart.DefaultSeries.Palette = new Color[] { Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) };
        Chart.LegendBox.Visible = false;
        Chart.Size = "200x300";
        Chart.Type = ChartType.Gauges;
        Chart.DefaultSeries.GaugeType = GaugeType.Vertical;
        Chart.ShadingEffectMode = ShadingEffectMode.Three;
        Chart.ChartArea.ClearColors();

        Chart.DefaultSeries.ImageBarSyncToValue = true;
        Chart.DefaultSeries.GaugeBorderBox.DefaultCorner = BoxCorner.Round;
        Chart.DefaultSeries.GaugeBorderShape = GaugeBorderShape.UseBox;
        Chart.DefaultSeries.GaugeBorderBox.Padding = 6;

        Chart.YAxis.Label.Text = "Home Run Competition";
        Chart.YAxis.TickLabelPadding = 2;
        Chart.YAxis.Orientation = dotnetCHARTING.Orientation.Right;
        Chart.YAxis.Maximum = 8;
        Chart.YAxis.Interval = 1;

        // *DYNAMIC DATA NOTE* 
        // This sample uses random data to populate the chart. To populate 
        // a chart with database data see the following resources:
        // - Help File > Getting Started > Data Tutorials
        // - DataEngine Class in the help file	
        // - Sample: features/DataEngine.aspx

        SeriesCollection mySC = getRandomData();
        mySC[0].ImageBarTemplate = "../../images/ImageBarTemplates/baseball";

        // Add the random data.
        Chart.SeriesCollection.Add(mySC);
    }

    SeriesCollection getRandomData()
    {
        Random myR = new Random();
        SeriesCollection SC = new SeriesCollection();
        int a = 0;
        int b = 0;
        for (a = 1; a < 2; a++)
        {
            Series s = new Series("Jay Vs Bill");
            for (b = 1; b < 3; b++)
            {
                Element e = new Element("Team " + b.ToString());
                e.YValue = 1 + myR.Next(7);
                s.Elements.Add(e);
            }
            SC.Add(s);
        }
        return SC;
    }

    SeriesCollection getLiveData()
    {
        DataEngine de = new DataEngine("ConnectionString goes here");
        de.ChartObject = Chart; // Necessary to view any errors the dataEngine may throw.
        de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ....";
        return de.GetSeries();
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <dotnet:Chart ID="Chart" runat="server" />
    </div>
</body>
</html>
