<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	Chart.Type = ChartType.Map;//Horizontal;
	Chart.Size = "600x350";
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = ".netCHARTING Sample";
	Chart.Visible = false;
	Chart.ChartArea.Label.Text = "Please click on a point on this map";
	Chart.ChartArea.Background = new Background(Color.FromArgb(142,195,236),Color.FromArgb(63,137,200),90);
	Chart.Mapping.DefaultShape.Background.Color = Color.LightGray;

	
	// This sample demonstrates adding a point to the chart based on lat/long coordinates of the click.
	// FullScreen
	
	Chart.Mapping.MapLayerCollection.Add(@"../../images/MapFiles/primusa.shp");
	
	Bitmap bp = null;
	
	if(Page.Request.Params["y"] != null && Page.Request.Params["x"] != null)
	{
		// Generate the chart in order to get the correct coordinates from the click position.
		bp = Chart.GetChartBitmap();
				
		// Get x and y points
		int x = Convert.ToInt32( Page.Request.Params["x"]);
		int y = Convert.ToInt32(Page.Request.Params["y"]);
		PointF p = Chart.Mapping.GetLatLongCoordinates(x+","+y);
		
		// Create a layer with a point at the clicked position.
		MapLayer layer2 = new MapLayer();
		layer2.AddLatLongPoint(p,new ElementMarker(ElementMarkerType.Circle,8,Color.Red));
		Chart.Mapping.MapLayerCollection.Add(layer2);
		iLabel.Text+="<BR>Latitude: " + p.X + " Longitude: " + p.Y;
	
	}
	
    // Generate the chart again to show the added point.
  	bp = Chart.GetChartBitmap();
	string fileName = Chart.FileManager.SaveImage(bp);
	
	imageLabel.Text += "<input type=image value=\"submit\" border=0 src=\"" +fileName+ "\" ISMAP>";  
	bp.Dispose();	
}


</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	</head>
	<body>
		<div align="center">
		<form method =get >
		<asp:Label ID=imageLabel Runat=server/>
			</form>
			<dotnet:Chart id="Chart" runat="server"/>
			<asp:Label ID="iLabel" Runat=server></asp:Label>
		</div>
	</body>
</html>
