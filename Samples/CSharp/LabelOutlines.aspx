<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Size = "600x350";
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = ".netCHARTING Sample";
	
	
	// This sample demonstrates how to outline labels that may get obscured by their background.
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();

	Chart.DefaultElement.ShowValue = true;
	Chart.DefaultElement.SmartLabel.OutlineColor = Color.Orange;
	Chart.LegendBox.DefaultEntry.LabelStyle.OutlineColor = Color.Green;
	Chart.DefaultAxis.DefaultTick.Label.OutlineColor = Color.Blue;
	Chart.TitleBox.Label.OutlineColor = Color.Red;
	
	Chart.ChartArea.Label.Text = "Text on this chart is outlined";
	Chart.ChartArea.Label.OutlineColor = Color.Yellow;

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
        
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(5);
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a.ToString();
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b.ToString();
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(184,255,113);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,149,114);
	SC[3].DefaultElement.Color = Color.FromArgb(157,255,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server">
			</dotnet:Chart>

		</div>
	</body>
</html>
