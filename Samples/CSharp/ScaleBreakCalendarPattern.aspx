<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;
	Chart.Size = "800x350";
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = "Calendar Pattern Scale Breaks (Weekends)";
	
	Chart.XAxis.Line.Color= Color.Orange;
	
	Chart.XAxis.TimeInterval = TimeInterval.Days;
	CalendarPattern cp = CalendarPattern.Weekends;
	cp.AdjustmentUnit= TimeInterval.Day;
	
	Chart.XAxis.ScaleBreakStyle = ScaleBreakStyle.Gap;
	Chart.XAxis.DefaultTick.Label.Text = "<%Value,ddd d yyyy>";
    Chart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Months);
	Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Top;
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();
	
	Chart.XAxis.ScaleBreakCalendarPattern = cp;
	Chart.XAxis.ScaleRange = new ScaleRange(new DateTime(2005,1,1),new DateTime(2005,1,30));
	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
	
	Chart.XAxis.Markers.Add(new AxisMarker("",Color.Red,new DateTime(2005,1,13)));
        
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
	DateTime dt = new DateTime(2005,1,myR.Next(1,30));
	for(int a = 1; a < 3; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a.ToString();
		for(int b = 1; b < 10; b++)
		{
			Element e = new Element();
			//e.Name = "Element " + b.ToString();
			e.YValue = myR.Next(50);
			e.XDateTime = dt = dt.AddDays(1);
			if(dt.DayOfWeek != DayOfWeek.Saturday && dt.DayOfWeek != DayOfWeek.Sunday )
				s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
