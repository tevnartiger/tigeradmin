<%@ Page Language="C#" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="dotnetCHARTING" %>

<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
  if(!IsPostBack)
  {
	
	foreach(string s in Enum.GetNames(typeof(dotnetCHARTING.Length)))
		LengthOptions.Items.Add(s);
	for(int i=0;i<7;i++)
		NumberPercision.Items.Add(i.ToString());
  }
	

	//set global properties
    Chart.Title="Tallest buildings in the World";
    Chart.ChartArea.XAxis.Label.Text ="Building Names";
    Chart.ChartArea.YAxis.Label.Text="Hieght";
    Chart.LegendBox.Position=LegendBoxPosition.None;
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.Use3D=true;
    Chart.ShadingEffect=true;
    Chart.DefaultSeries.DefaultElement.ToolTip ="%yValue Feet";
    Chart.DefaultSeries.DefaultElement.ShowValue=true;
    Chart.DefaultSeries.DefaultElement.ForceMarker=true;
    Chart.DefaultSeries.DefaultElement.Marker.Type=ElementMarkerType.FivePointStar;
    Chart.DefaultSeries.DefaultElement.SmartLabel.ForceVertical=true;
    Chart.YAxis.Label.Text="Foot"; 
    Chart.Size="600X500";
    
   
  
   //Adding series programatically
   Series sr=new Series();
   sr.Name="Building Height";
 
   Element el = new Element("CN Tower",1861);
   el.LabelTemplate = " Toronto";
   sr.Elements.Add(el);
   el = new Element("Sears Tower",1707);
   el.LabelTemplate ="Chicago";
   sr.Elements.Add(el);
   el = new Element("Ostankino Tower",1771);
   el.LabelTemplate = "Moscow";
   sr.Elements.Add(el);
   el = new Element("John Hancock Center",1476);
   el.LabelTemplate = "Chicago";
   sr.Elements.Add(el);
	el = new Element("Petronas Towers",1482);
   el.LabelTemplate = "Kuala Lumpur";
   sr.Elements.Add(el);
	el = new Element("OPB Tower",1535);
   el.LabelTemplate = "Shanghai";
   sr.Elements.Add(el);
	el = new Element("Jim Mao Building",1378);
   el.LabelTemplate = "Shanghai";
   sr.Elements.Add(el);
   el = new Element("Menara Telecom Tower",1403);
   el.LabelTemplate = "Kuala Lumpur";
   sr.Elements.Add(el);
   el = new Element("Empire State Building",1454);
   el.LabelTemplate = "New York";
   sr.Elements.Add(el);
   Chart.SeriesCollection.Add(sr);
 
}
void ButtonConvert_Click(Object sender, EventArgs e)
{
	Axis F=Chart.YAxis.Calculate(LengthOptions.SelectedItem.Value, Length.Foot,Enum.Parse(typeof(dotnetCHARTING.Length),LengthOptions.SelectedItem.Value,true),RefreshScale.Checked);
	F.Orientation = dotnetCHARTING.Orientation.Right;
	F.NumberPercision = Convert.ToInt32(NumberPercision.SelectedItem.Value);
	F.DefaultTick.GridLine = new Line(Color.Red, 2, System.Drawing.Drawing2D.DashStyle.DashDot);
	F.ShowGrid=true;
	Chart.AxisCollection.Add(F);
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Length Conversion Options</title>
    <style type="text/css">
     body.myBody  
     {
     border-top-width:0px;
      border-left-width:0px;
     
     }
     img.myImage  
     {
     border:0px;
     
     }
     td.myTD1  
     {
      width:"1%";
        border:1px; 
        margin:0;
     }
     td.myTD2  
     {     
      width:"99%";
      background:"#BFC0DB";
      border-collapse:collapse;
       
      
     }
     table.myTable
     {
      border:1px; 
      border-style:solid;
      border-spacing:0;
      border-color:"#111111";
      border-collapse:collapse;
 
     }
     
    </style>
</head>
<body class="myBody">
    <form runat="server"  action="interactiveunitconversion.aspx">
        <div style="text-align:center">
            <table class="myTable" id="AutoNumber1">
                
                <tr>
    <td class="myTD1">
<img class="myImage" alt="dotnetChartingImage" src="../../images/dotnetCharting.gif" width="230" height="94"/></td>
    <td class="myTD2">Length Options: 
 <ASP:DropDownList id="LengthOptions" runat="server">
    </ASP:DropDownList>
    Decimal Places: 
    <ASP:DropDownList id="NumberPercision" runat="server">
    </ASP:DropDownList>
     <ASP:CheckBox id="RefreshScale" Text="Independent Scale" Checked="true" runat="server">
    </ASP:CheckBox>
     <asp:Button id="ButtonConvert" onclick="ButtonConvert_Click" runat="server" Text="Convert">
    </asp:Button>
 </td>
          </tr>
            </table>
            <DOTNET:Chart id="Chart" runat="server" Visible="true" />
        </div>
    </form></body>
</html>