<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Label Truncation</title>
		<script runat="server">




void Page_Load(Object sender,EventArgs e)
{
	
	Chart.Width = 500;
	Chart.Height = 400;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.LegendBox.Position = LegendBoxPosition.None;
	
	
	// This sample will demonstrate how strings can be truncated.
	
	// First we get our data, if you would like to get the data from a database you need to use
	// the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
	SeriesCollection mySC = getRandomData();
	
	// Setup the chart
	Chart.DefaultSeries.Type = SeriesType.Marker;
	Chart.YAxis.Maximum = 40;
	Chart.DefaultSeries.DefaultElement.ShowValue = true;
	
	// Specify a default string to trim.
	Chart.DefaultSeries.DefaultElement.SmartLabel.Text = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	// Set the default trimming length to 10 characters.
	Chart.DefaultSeries.DefaultElement.SmartLabel.Truncation.Length = 10;
	
	// Specify a trimming mode and name for each element.
	mySC[0].Elements[0].Name = "TruncationMode.End";
	mySC[0].Elements[0].SmartLabel.Truncation.Mode = TruncationMode.End;
	
	mySC[0].Elements[1].Name = "TruncationMode.Middle";
	mySC[0].Elements[1].SmartLabel.Truncation.Mode = TruncationMode.Middle;
	
	mySC[0].Elements[2].Name = "TruncationMode.StartEnd";
	mySC[0].Elements[2].SmartLabel.Truncation.Mode = TruncationMode.StartEnd;
	
	mySC[0].Elements[3].Name = "TruncationMode.Start";
	mySC[0].Elements[3].SmartLabel.Truncation.Mode = TruncationMode.Start;
	
	

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
    
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 2; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			e.YValue = 20;
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);


	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
