<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Size = "600x350";
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = ".netCHARTING Sample";
		
	// This sample demonstrates using a multi-color 2D line.
	
	// Set some chart properties.
	Chart.ChartArea.Label.Text = "Notice the line color change between points of different colors.";
	Chart.DefaultSeries.Type = SeriesType.Line;
	Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();
	
	// Iterate the elements and set the color to red if an elements YValue is outside the 20-40 range.
	foreach(Element myE in mySC[0].Elements)
		if(myE.YValue > 40 || myE.YValue < 20) myE.Color = Color.Red;
		
	// Add a couple axis markers.
	Chart.YAxis.Markers.Add(new AxisMarker("",Color.FromArgb(50,Color.Red),40,100));
	Chart.YAxis.Markers.Add(new AxisMarker("",Color.FromArgb(50,Color.Red),0,20));
	Chart.YAxis.Markers[0].LegendEntry.Visible = false;
	Chart.YAxis.Markers[1].LegendEntry.Visible = false;
	
	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
        
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
	for(int a = 1; a < 2; a++)
	{
		Series s = new Series();
		s.Name = "Sample Data " + a.ToString();
		for(int b = 1; b < 15; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b.ToString();
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	//SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	//SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	//SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	//SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
