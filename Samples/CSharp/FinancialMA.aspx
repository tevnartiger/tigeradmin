<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>


<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Financial Moving Average Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	string connection = @"Provider=Microsoft.Jet.OLEDB.4.0;data source=" + Server.MapPath("../../database/chartsample.mdb");
     
     
	// This sample demonstrates the use of the SimpleMovingAverages and GeometricalMovingAverage
	
	// First we declare a chart of type FinancialChart	
	// The Financial Chart
	FinancialChart.Title="Financial Chart";
	FinancialChart.TempDirectory="temp";
	FinancialChart.Debug=true;
	FinancialChart.ShadingEffect = true;
	FinancialChart.LegendBox.Template ="%icon %name";
	FinancialChart.Size="800X600";
	FinancialChart.PaletteName = Palette.Three;
	FinancialChart.YAxis.Label.Text = "Price (USD)";
	FinancialChart.YAxis.FormatString = "currency";
	FinancialChart.YAxis.Scale = Scale.Range;
	FinancialChart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	FinancialChart.DefaultSeries.DefaultElement.Marker.Visible = false;
	FinancialChart.DefaultSeries.Type = SeriesType.Spline;
	
			// Modify the x axis labels.
	FinancialChart.XAxis.Scale = Scale.Time;
	FinancialChart.XAxis.TimeInterval = TimeInterval.Day;		
	FinancialChart.XAxis.TimeScaleLabels.DayFormatString = "o";
	FinancialChart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Month);
	FinancialChart.XAxis.TimeScaleLabels.MonthFormatString = "MMM";
	
	// Here we load data samples from the FinancialCompany table from within chartsample.mdb
	DataEngine priceDataEngine = new DataEngine (connection);
	priceDataEngine.ChartObject = FinancialChart;
	priceDataEngine.ChartType = ChartType.Financial;
	priceDataEngine.DateGrouping = TimeInterval.Day;
	priceDataEngine.StartDate = new DateTime(2001,4,1);
	priceDataEngine.EndDate = new DateTime(2001,6,30);
	priceDataEngine.SqlStatement = @"SELECT TransDate, HighPrice, LowPrice, OpenPrice, ClosePrice FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate";
	priceDataEngine.DataFields = "xAxis=TransDate,High=HighPrice,Low=LowPrice,Open=OpenPrice,Close=ClosePrice";

	SeriesCollection sc = priceDataEngine.GetSeries ();
	Series prices = null;
	if(sc.Count>0)
		prices = sc[0];
	else
		return;
		
	prices.DefaultElement.ToolTip="L:%Low-H:%High";
	prices.DefaultElement.SmartLabel.Font = new Font("Arial", 6);
	prices.DefaultElement.SmartLabel.Text="O:%Open-C:%Close";
	prices.Type = SeriesTypeFinancial.CandleStick;

	CalendarPattern cp = new CalendarPattern (TimeInterval.Day, TimeInterval.Week, "0000001");
	prices.Trim (cp, ElementValue.XDateTime);
	prices.Name = "Prices";
	FinancialChart.SeriesCollection.Add (prices);

	// Create the second chart area 
	ChartArea volumeChartArea = new ChartArea();
	volumeChartArea.Label.Text = "Stock Volume";
	volumeChartArea.YAxis.Label.Text = "Volumes";
	volumeChartArea.Series.Name="Stock Volume";
	volumeChartArea.HeightPercentage = 30;
	volumeChartArea.Series.DefaultElement.ToolTip="%YValue";
	FinancialChart.ExtraChartAreas.Add(volumeChartArea);

	// Add a volume series to the chart area
	DataEngine volumeDataEngine = new DataEngine(connection);
	volumeDataEngine.DateGrouping = TimeInterval.Days;
	volumeDataEngine.StartDate=new DateTime (2001,4,1);
	volumeDataEngine.EndDate = new DateTime (2001,6,30);
	volumeDataEngine.SqlStatement= @"SELECT TransDate,volume FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate"; 
	volumeDataEngine.DataFields="xAxis=TransDate,yAxis=Volume";
		
	Series volumes = volumeDataEngine.GetSeries()[0];
	volumes.Trim (cp, ElementValue.XDateTime);
	volumes.Name = "Volumes";
	volumes.Type = SeriesType.Bar;
	volumeChartArea.SeriesCollection.Add (volumes);
		
	/*
	 * Financial Series Moving Average Indicators
	 */

	// Here we display the financial simple moving average over a period of three days for the Prices Series
	FinancialChart.SeriesCollection.Add (FinancialEngine.SimpleMovingAverage (prices, ElementValue.High, 3));

	// Here we display the statistical simple moving average for the Volumes Series
	volumeChartArea.SeriesCollection.Add (StatisticalEngine.SimpleMovingAverage (volumes, 3));

	// Here we display the financial geometrical moving avereage over a period of three days for the Prices Series
	FinancialChart.SeriesCollection.Add (FinancialEngine.GeometricMovingAverage (prices, ElementValue.Low, 3));
		
	// NOTE: The other moving average indicators can be displayed in a similar way
	
	// Kairi indicator - measures as a percentage of the price the divergence between the a moving 
    // average (generally the simple moving average) of the price and the price itself. 
	ChartArea kairiChartArea = new ChartArea();
	kairiChartArea.YAxis.Label.Text = "Kairi";
	kairiChartArea.HeightPercentage = 10;
	FinancialChart.ExtraChartAreas.Add (kairiChartArea);

	Series kairi = FinancialEngine.Kairi (prices, ElementValue.High,
	                                FinancialEngine.SimpleMovingAverage (prices, ElementValue.High, 3));
	kairi.Type = SeriesType.Cylinder;
	kairiChartArea.SeriesCollection.Add (kairi);
        
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="FinancialChart" runat="server"/>
		</div>
	</body>
</html>
