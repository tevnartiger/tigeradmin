<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>


<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	// This sample demonstrates bessel K function.

	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Palette = new Color[]{Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)};
	
	Chart.Type = ChartType.Combo;
	Chart.Size = "600x350";
	Chart.Title = "Special Functions";
	
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Help File > Getting Started > Data Tutorials
	// - DataEngine Class in the help file	
	// - Sample: features/DataEngine.aspx
	
	SeriesCollection mySC = getRandomData();
	SeriesCollection besselKSC = new SeriesCollection();
	// Calculate the BesselK function of the order 0, 1, 2, 3
	for(int i = 0; i < 4; i++ ) {
		Series besselK = ForecastEngine.Advanced.BesselK(mySC[i],i);
		besselK.Type = SeriesType.Line;
		besselKSC.Add(besselK);
	}
	
	// Bessel chart area
	ChartArea besselChartArea = new ChartArea ();
	besselChartArea.HeightPercentage = 40;
	besselChartArea.YAxis.Label.Text = "BesselK";
	Chart.ExtraChartAreas.Add (besselChartArea);
	
	
	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
	besselChartArea.SeriesCollection.Add(besselKSC);
        
}

SeriesCollection getRandomData()
{
	Random myR = new Random(1);
	SeriesCollection SC = new SeriesCollection();
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series("Series " + a.ToString());
		
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element();
			e.YValue = myR.Next(5);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
