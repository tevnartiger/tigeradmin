<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	
	Chart.DefaultSeries.Type = SeriesType.Spline;
	
	Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom;
	Chart.LegendBox.Template="%name %icon";
	
	// This sample will demonstrates how to use and attach axis markers to axes and elements.
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();
	
	// 1. SINGLE VALUES
	AxisMarker am1 = new AxisMarker("Marker at 45",new Line(Color.Orange),45);
	Chart.YAxis.Markers.Add(am1);
	
	AxisMarker am2 = new AxisMarker("Text marker at 'Element 0'",new Line(Color.Green),"Element 0");
	Chart.XAxis.Markers.Add(am2);
	
	// 2. RANGE MARKERS
	
	AxisMarker am3 = new AxisMarker("20-30",new Background(Color.FromArgb(100,Color.LightGreen)),20,30);
	Chart.YAxis.Markers.Add(am3);
	
	AxisMarker am4 = new AxisMarker("'Element 1'-'Element 2'",new Background(Color.FromArgb(100,Color.LightBlue)),"Element 1","Element 2");
	am4.Label.LineAlignment = StringAlignment.Near;
	Chart.XAxis.Markers.Add(am4);
	
	// 3.ELEMENT MARKERS
	mySC[0].Elements[3].AxisMarker = new AxisMarker("Attached to element 3",new Line(Color.Blue),0);
	
	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 4; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
