<%@ Page Language="C#" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    Chart.Title="Orders By Customers";
    Chart.XAxis.Label.Text="Customers";
    Chart.TempDirectory="temp";
   Chart.Debug=true;
   Chart.Use3D=false;
    Chart.DefaultSeries.DefaultElement.ShowValue=true;
    Chart.DefaultSeries.DefaultElement.LabelTemplate="%Yvalue";
    
 
    
    //Add a series
    Chart.Series.Name="Orders";
    Chart.Series.SqlStatement= @"SELECT Name,Sum(1) FROM Orders GROUP BY Name ORDER BY Name";
    Chart.Series.Type = SeriesType.Line;
    Chart.Series.Line = new Line(Color.Blue,8);
    Chart.SeriesCollection.Add();
    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Orders Report</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
