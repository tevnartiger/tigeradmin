<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.DefaultSeries.Type = SeriesType.Line;
	Chart.XAxis.Maximum = 3;	
	
	// This sample will demonstrate using a shadow axis to mark element values.
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();
	
	// Setup the shadow axis.	
	Axis shadow = Chart.YAxis.Calculate("");
	shadow.Orientation = dotnetCHARTING.Orientation.Right;
	shadow.ClearValues = true;
	shadow.DefaultTick.GridLine.DashStyle = DashStyle.Dash;
	shadow.DefaultTick.GridLine.Color = Color.LawnGreen;
	
	
	// Add a tick to the shadow axis for each element.
	foreach(Element el in mySC[0].Elements)
	{
		shadow.ExtraTicks.Add(new AxisTick(el.YValue));
	}
	
	// Add the shadow axis.
	Chart.AxisCollection.Add(shadow);

	// Add the data.
	Chart.SeriesCollection.Add(mySC);
    
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(2);
	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + (a+1);
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			//e.Name = "Element " + (b+1);
			e.XValue = b;
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	//SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	//SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	//SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
