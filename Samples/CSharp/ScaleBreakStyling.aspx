<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;
	Chart.Size = "600x350";
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = "Scale Break Styling ( Gap, ZigZag, Line )";
	
	
	// This sample demonstrates ....
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();
	
	// Page link related code.
	string scaleBreakStyleOption = HttpContext.Current.Request.QueryString["ScaleBreakStyle"];
	if(scaleBreakStyleOption == null || scaleBreakStyleOption  == "") scaleBreakStyleOption = "Gap";

	Chart.YAxis.ScaleBreakStyle = (dotnetCHARTING.ScaleBreakStyle)Enum.Parse(typeof(dotnetCHARTING.ScaleBreakStyle),scaleBreakStyleOption,true);
	Chart.YAxis.SmartScaleBreak = true; //For disabling scale break, set to false

	Chart.DefaultAxis.AlternateGridBackground.Color = Color.Empty;
	Chart.DefaultAxis.DefaultTick.GridLine.Color = Color.Empty;
	
	

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
        
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a.ToString();
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b.ToString();
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	SC[0].Elements[1].YValue=1000;

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
		<a href="?ScaleBreakStyle=Gap">Gap style scale break</a> | <a href="?ScaleBreakStyle=ZigZag">ZigZag style scale break</a> | <a href="?ScaleBreakStyle=Line">Line style scale break</a><br>
		</div>

		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
