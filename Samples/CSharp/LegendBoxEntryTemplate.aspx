<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
	Chart.Title ="Orders By Customers";
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    Chart.XAxis.Label.Text="Customers";
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.Use3D=true;
    Chart.Transpose =true;
    
    Chart.LegendBox.Template ="%icon %name";
    Chart.DefaultSeries.LegendEntry.URL="%Xvalue,%Yvalue";
    Chart.DefaultSeries.LegendEntry.ToolTip = "Total: %YSum";
    Chart.DefaultSeries.LegendEntry.URL = "customer.aspx?name=%Name";
   	   	
    //Add a series
    Chart.Series.Name="Orders";
    Chart.Series.SqlStatement= @"SELECT Name,Sum(1) FROM Orders GROUP BY Name ORDER BY Sum(1) DESC";
    Chart.SeriesCollection.Add();
    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>LegendBox Entry Template</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>

</html>
