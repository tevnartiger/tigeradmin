<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Empty Element 1</title>
		<script runat="server">




void Page_Load(Object sender,EventArgs e)
{


	Chart.Type = ChartType.ComboSideBySide;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 200;
	Chart.TempDirectory = "temp";
	Chart.Use3D = true;
	Chart.Debug = true;
	Chart.LegendBox.Position = LegendBoxPosition.None;
	
	
	
	// This sample will demonstrate advanced empty element handling. (Modes)
	
	// Changing the default series type will show how different types behave with empty elements.
	Chart.DefaultSeries.Type = SeriesType.AreaLine;
	
	
	// First we get our data, if you would like to get the data from a database you need to use
	// the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
	SeriesCollection mySC = getData();
	
	
	// EmptyElementMode.None
	mySC[3].EmptyElement.Mode = EmptyElementMode.None;
	mySC[3].Name = "Mode: None";
	
	// EmptyElementMode.Ignore;
	mySC[0].EmptyElement.Mode = EmptyElementMode.Ignore;
	mySC[0].Name = "Mode: Ignore";
	
	// EmptyElementMode.Fill;
	mySC[1].EmptyElement.Mode = EmptyElementMode.Fill;
	mySC[1].Name = "Mode: Fill";
	
	// EmptyElementMode.TreatAsZero;
	mySC[2].EmptyElement.Mode = EmptyElementMode.TreatAsZero;
	mySC[2].Name = "Mode: TreatAsZero";
	
	
	

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
    
    
}


SeriesCollection getData()
{
	SeriesCollection sc = new SeriesCollection();
	
	Series s1 = new Series();
	s1.Elements.Add(new Element("1",8));
	s1.Elements.Add(new Element("2",8));
	s1.Elements.Add(new Element("3",double.NaN));
	s1.Elements.Add(new Element("4",double.NaN));
	s1.Elements.Add(new Element("5",5));
	s1.Elements.Add(new Element("6",5));
	
	Series s2 = new Series();
	s2.Elements.Add(new Element("1",8));
	s2.Elements.Add(new Element("2",8));
	s2.Elements.Add(new Element("3",double.NaN));
	s2.Elements.Add(new Element("4",double.NaN));
	s2.Elements.Add(new Element("5",5));
	s2.Elements.Add(new Element("6",5));
	
	Series s3 = new Series();
	s3.Elements.Add(new Element("1",5));
	s3.Elements.Add(new Element("2",5));
	s3.Elements.Add(new Element("3",double.NaN));
	s3.Elements.Add(new Element("4",double.NaN));
	s3.Elements.Add(new Element("5",8));
	s3.Elements.Add(new Element("6",8));
	
	Series s4 = new Series();
	s4.Elements.Add(new Element("1",5));
	s4.Elements.Add(new Element("2",5));
	s4.Elements.Add(new Element("3",double.NaN));
	s4.Elements.Add(new Element("4",double.NaN));
	s4.Elements.Add(new Element("5",8));
	s4.Elements.Add(new Element("6",8));
	

	sc.Add(s2);	
	sc.Add(s1);
	sc.Add(s4);	
	sc.Add(s3);	


	
	return sc;
	

}
		</script>
	</head>
	<body>
	<br>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
