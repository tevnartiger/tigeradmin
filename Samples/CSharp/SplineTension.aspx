<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;
	Chart.Size = "600x350";
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = "Spline Tension";
	
	Chart.DefaultSeries.Type = SeriesType.Spline;
	Chart.DefaultElement.Marker.Visible = false;
	Chart.LegendBox.Position = new Point(50,50);	
	
	// This sample demonstrates specifying spline tension.
		
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx

	// Get the same data for both series.
	Series s1 = getRandomData()[0];
	Series s2 = getRandomData()[0];
	
	// Specify the tension.
	s1.SplineTensionPercent = 60;
	s2.SplineTensionPercent = 20;
	
	// Some aesthetic settings.
	s1.Name = "Tension = 60%";
	s2.Name = "Tension = 20%";
	s2.DefaultElement.Marker.Visible = true;
	
	LegendEntry le = new LegendEntry();
	le.Name = "Default = 50%";
	Chart.LegendBox.ExtraEntries.Add(le);
	
	// Add the random data.
	Chart.SeriesCollection.Add(s1,s2);
        
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(2);
	for(int a = 1; a < 2; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a.ToString();
		for(int b = 1; b < 10; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b.ToString();
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	
	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
