<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	//chartObj.Type = ChartType.ComboHorizontal;
	chartObj.Size = "700x350";
	chartObj.TempDirectory = "temp";
	chartObj.FileName ="XamlImage";
	chartObj.Debug = false;
	chartObj.Title = ".netCHARTING Sample";
	chartObj.ImageFormat = ImageFormat.Xaml;
		
	chartObj.SeriesCollection.Add(getRandomData());
	        
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a.ToString();
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b.ToString();
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
		<dotnet:Chart id="chartObj" runat="server" Width="568px" Height="344px"/>
			<div style="text-align:center">
		<font face="Arial" size="2">To view the generated XAML file you must download the .NET Framework 3.0 
			distribution at http://www.netfx3.com/ and view this sample using Internet Explorer.

			The XAML file has been generated when you ran this sample and is now 
			available in the temp directory.  You can load it directly by visiting 
			<a href="temp/XamlImage.xaml">this link </a> or open the file directly in any program that support XAML.
		</font>
</div>
</div>
	</body>
</html>
