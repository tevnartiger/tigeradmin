<%@ Page Language="C#" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>

    <script runat="server">

        void Page_Load(Object sender, EventArgs e)
        {
            Chart.Type = ChartType.Combo;
            Chart.Width = 600;
            Chart.Height = 450;
            Chart.TempDirectory = "temp";
            Chart.Debug = true;
            Chart.Title = "Sample Data";

            Chart.ChartAreaSpacing = 0;
            Chart.ChartArea.HeightPercentage = 20;
            SeriesCollection mySC = getRandomData();
            Chart.SeriesCollection.Add(mySC);
            Chart.DefaultSeries.Type = SeriesType.Spline;
            Chart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical;

            ChartArea rChartArea = new ChartArea("R Chart Example");
            rChartArea.XAxis = new Axis();
            //rChartArea.HeightPercentage = 20;
            rChartArea.TitleBox.Position = TitleBoxPosition.FullWithLegend;
            Chart.ExtraChartAreas.Add(rChartArea);

            Series rChart = StatisticalEngine.RChart(mySC);
            rChartArea.SeriesCollection.Add(rChart);

            ChartArea rXBARChartArea = new ChartArea("R XBAR Chart Example");
            rXBARChartArea.XAxis = new Axis();
            //rXBARChartArea.HeightPercentage = 20;
            Chart.ExtraChartAreas.Add(rXBARChartArea);
            rXBARChartArea.TitleBox.Position = TitleBoxPosition.FullWithLegend;

            Series rXBARChart = StatisticalEngine.RXBARChart(mySC);
            rXBARChartArea.SeriesCollection.Add(rXBARChart);
        }

        SeriesCollection getRandomData()
        {
            SeriesCollection SC = new SeriesCollection();
            Random myR = new Random();

            for (int a = 0; a < 10; a++)
            {
                Series s = new Series();
                s.Name = "Series " + a;
                s.LegendEntry.Visible = false;
                for (int b = 0; b < 5; b++)
                {
                    Element e = new Element();
                    e.Name = "Element " + b;
                    e.YValue = myR.Next(50);
                    s.Elements.Add(e);
                }
                SC.Add(s);
            }

            // Set Different Colors for our Series
            //SC[0].DefaultElement.Color = Color.FromArgb(255,99,49);
            //SC[1].DefaultElement.Color = Color.FromArgb(0,156,255);
            //SC[2].DefaultElement.Color = Color.FromArgb(49,255,49);
            //SC[3].DefaultElement.Color = Color.FromArgb(255,255,0);

            return SC;
        }
    </script>

</head>
<body>
    <div style="text-align: center">
        <dotnet:Chart ID="Chart" runat="server" Width="568px" Height="344px">
        </dotnet:Chart>
    </div>
</body>
</html>
