<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
     
	// This sample demonstrates the use of LeastSquaresRegressionLineX and LeastSquaresRegressionLineY from
	// within Correlation class
	
	// The Correlation Chart
	CorrelationChart.Title="Correlation";
	CorrelationChart.TempDirectory="temp";
	CorrelationChart.Debug=true;
	CorrelationChart.Size = "600x400";
	CorrelationChart.LegendBox.Template ="%icon %name";
	CorrelationChart.XAxis.Scale = Scale.Normal;
	CorrelationChart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	CorrelationChart.DefaultSeries.DefaultElement.Marker.Visible = false;
	CorrelationChart.DefaultSeries.Type = SeriesType.Spline;
		
	// Generate the sample data.
	SeriesCollection scorrelation = new SeriesCollection();
	Series sampledata1 = new Series ("Sample 1");
	for ( int b=1; b<10; b++) {
		Element el = new Element();
		el.YValue = -Math.Sin(b)-b;
		el.XValue = b ;
		sampledata1.Elements.Add (el);
	}
	scorrelation.Add (sampledata1);
	CorrelationChart.SeriesCollection.Add (scorrelation);
	
    // Here we create a series which describe the regression line of Y on X using the 
    // method of least squares
	CorrelationChart.SeriesCollection.Add ( StatisticalEngine.LeastSquaresEstimateY("EstimateY",sampledata1) );

	// Here we create a series which describe the regression line of X on Y using the 
    // method of least squaresLeastSquaresEstimatex
	Series estimateX = StatisticalEngine.LeastSquaresEstimateX("EstimateX", sampledata1);
	estimateX.DefaultElement.Color = Color.FromArgb(255,99,49);
	CorrelationChart.SeriesCollection.Add (estimateX);
		
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="CorrelationChart" runat="server"/>			
		</div>
	</body>
</html>
