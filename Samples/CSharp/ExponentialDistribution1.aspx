<%@ Page Language="C#" debug="True" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>


<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{ 
	// This sample demonstrates the use of Exponential Distribution within StatisticalEngine.
	
	// The Distribution Chart
	DistributionChart.Title=" Exponential Distribution";
	DistributionChart.TempDirectory = "temp";
	DistributionChart.Debug=true;
	DistributionChart.Size = "600x300";
	DistributionChart.LegendBox.Template ="%icon %name";
	DistributionChart.YAxis.Percent = true;
	DistributionChart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	DistributionChart.DefaultSeries.DefaultElement.Marker.Visible = false;
	DistributionChart.YAxis.FormatString = "0.000";
	DistributionChart.YAxis.Scale = Scale.Range;

	
	//double[] expDistVector = new double[] {0, 1, 2, 3, 4, 5};
	double[] expDistVector = new double[] {0,1, 2, 3, 4, 10};

		
	Series sampledata = new Series ("Sample1");
	for ( int i=0; i < expDistVector.Length; i++) {
		Element el = new Element();
		el.XValue = expDistVector[i];
		sampledata.Elements.Add (el);
	}
	
	// The second parameter of this method is the scale parameter of the exponential probability distribution
	Series expDistribution = StatisticalEngine.ExponentialDistribution(sampledata,3.67);
	expDistribution.Type = SeriesType.Spline;
	expDistribution.DefaultElement.ShowValue = true;
	expDistribution.DefaultElement.SmartLabel.Text = "%YValue%";
	expDistribution.DefaultElement.SmartLabel.Color = Color.Black;
	DistributionChart.SeriesCollection.Add (expDistribution);	
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="DistributionChart" runat="server"/>
		</div>
	</body>
</html>
