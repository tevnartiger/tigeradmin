<%@ Page Language="C#" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    void Page_Load(Object sender, EventArgs e)
    {
        // This sample demonstrates a vertical linear gauge using shading effect Three and Forced element image markers.
        Chart.Size = "120x230";

        Chart.TempDirectory = "temp";
        Chart.Debug = true;
        Chart.DefaultSeries.Palette = new Color[] {  Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) };
        Chart.LegendBox.Visible = false;

        Chart.Type = ChartType.Gauges;
        Chart.DefaultSeries.GaugeType = GaugeType.Vertical;
        Chart.ShadingEffectMode = ShadingEffectMode.Three;

        Chart.ChartArea.Padding = 0;


        Chart.ChartArea.ClearColors();


        Chart.DefaultSeries.GaugeBorderBox.DefaultCorner = BoxCorner.Round;
        Chart.DefaultSeries.GaugeBorderShape = GaugeBorderShape.UseBox;
        Chart.DefaultSeries.GaugeBorderBox.Padding = 6;
        Chart.YAxis.Label.Text = "Hits";
        Chart.YAxis.TickLabelPadding = 2;
        Chart.YAxis.Orientation = dotnetCHARTING.Orientation.Right;

        // *DYNAMIC DATA NOTE* 
        // This sample uses random data to populate the chart. To populate 
        // a chart with database data see the following resources:
        // - Help File > Getting Started > Data Tutorials
        // - DataEngine Class in the help file	
        // - Sample: features/DataEngine.aspx

        SeriesCollection mySC = getRandomData();
        mySC[0][0].Marker = new ElementMarker("../../images/browserIE.png");
        mySC[0][1].Marker = new ElementMarker("../../images/browserN.png");
        Chart.DefaultElement.ForceMarker = true;

        // Add the random data.
        Chart.SeriesCollection.Add(mySC);

    }

    SeriesCollection getRandomData()
    {
        Random myR = new Random(1);
        SeriesCollection SC = new SeriesCollection();
        for (int a = 1; a < 2; a++)
        {
            Series s = new Series("");
            for (int b = 1; b < 3; b++)
            {
                Element e = new Element("Element " + b.ToString());
                e.YValue = myR.Next(50);
                s.Elements.Add(e);
            }
            SC.Add(s);
        }
        return SC;
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <dotnet:Chart ID="Chart" runat="server" />
    </div>
</body>
</html>
