<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">




void Page_Load(Object sender,EventArgs e)
{

	Chart.Title = "Annotation Orientation Options";
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
		
	
	// This sample will demonstrate how to align text inside annotations.
	
	// First we get our data, if you would like to get the data from a database you need to use
	// the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
	SeriesCollection mySC = getData();
	
	// Create an annotation.
	Annotation a1 = new Annotation("Bottom Orientation");
	// Set the dotnetCHARTING.Orientation.
	a1.Orientation = dotnetCHARTING.Orientation.Bottom;
	mySC[0].Elements[0].Annotation = a1;
	
	// Create an annotation.
	Annotation a2 = new Annotation("Bottom-Left Orientation");
	// Set the dotnetCHARTING.Orientation.
	a2.Orientation = dotnetCHARTING.Orientation.BottomLeft;
	mySC[0].Elements[1].Annotation = a2;
	
	// Create an annotation.
	Annotation a3 = new Annotation("Bottom-Right Orientation");
	// Set the dotnetCHARTING.Orientation.
	a3.Orientation = dotnetCHARTING.Orientation.BottomRight;
	mySC[0].Elements[2].Annotation = a3;
	
	// Create an annotation.
	Annotation a4 = new Annotation("Left  Orientation");
	// Set the dotnetCHARTING.Orientation.
	a4.Orientation = dotnetCHARTING.Orientation.Left;
	mySC[0].Elements[3].Annotation = a4;
	
	
	// Create an annotation.
	Annotation a5 = new Annotation("None Orientation");
	// Set the dotnetCHARTING.Orientation.
	a5.Orientation = dotnetCHARTING.Orientation.None;
	mySC[0].Elements[4].Annotation = a5;
	
	// Create an annotation.
	Annotation a6 = new Annotation("Right Orientation");
	// Set the dotnetCHARTING.Orientation.
	a6.Orientation = dotnetCHARTING.Orientation.Right;
	mySC[0].Elements[5].Annotation = a6;
	
	// Create an annotation.
	Annotation a7= new Annotation("Top-Right  Orientation");
	// Set the dotnetCHARTING.Orientation.
	a7.Orientation = dotnetCHARTING.Orientation.TopRight;
	mySC[0].Elements[6].Annotation = a7;
	
	// Create an annotation.
	Annotation a8 = new Annotation("Top-Left Orientation");
	// Set the dotnetCHARTING.Orientation.
	a8.Orientation = dotnetCHARTING.Orientation.TopLeft;
	mySC[0].Elements[7].Annotation = a8;
	
	// Create an annotation.
	Annotation a9 = new Annotation("Top  Orientation");
	// Set the dotnetCHARTING.Orientation.
	a9.Orientation = dotnetCHARTING.Orientation.Top;
	mySC[0].Elements[8].Annotation = a9;
	
	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
}

SeriesCollection getData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 2; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		//for(int b = 1; b < 8; b++)
		//{
		Element e = new Element();
		e.Name = "Element 1";
		e.YValue = 3;
		s.Elements.Add(e);
		
		e = new Element();
		e.Name = "Element 2";
		e.YValue = 7;
		s.Elements.Add(e);
		
		e = new Element();
		e.Name = "Element 3";
		e.YValue = 12;
		s.Elements.Add(e);

		e = new Element();
		e.Name = "Element 4";
		e.YValue = 7;
		s.Elements.Add(e);
		
		e = new Element();
		e.Name = "Element 5";
		e.YValue = 9;
		s.Elements.Add(e);

		e = new Element();
		e.Name = "Element 6";
		e.YValue = 3;
		s.Elements.Add(e);
		
		e = new Element();
		e.Name = "Element 7";
		e.YValue = 5;
		s.Elements.Add(e);
		
		e = new Element();
		e.Name = "Element 8";
		e.YValue = 9;
		s.Elements.Add(e);
				
		e = new Element();
		e.Name = "Element 9";
		e.YValue = 6;
		s.Elements.Add(e);
		SC.Add(s);

	}
	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
