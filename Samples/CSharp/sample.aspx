<%@ Page Language="C#" Debug="true" Trace="false" Description="dotnetChart Component"%>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<script runat="server">

    void Page_Load(Object sender,EventArgs e)
    {
       
            if(!IsPostBack)
            {
            	DateTime dt = DateTime.Today;
                ListItem myListItem;
    
                DropDownShow.Items.Add("Sales");
                DropDownShow.Items.Add("Orders");
                DropDownShow.Items.Add("Items");
    
    			DropDownBy.Items.Add("Hours");
                DropDownBy.Items.Add(new ListItem("Day/Hour","Day"));
                DropDownBy.Items.Add("Days");   		
                DropDownBy.Items.Add(new ListItem("Week/Day","Week"));
                DropDownBy.Items.Add("Weeks");
                DropDownBy.Items.Add(new ListItem("Month/Day","Month"));                
                DropDownBy.Items.Add("Months");
                DropDownBy.Items.Add("Quarters");
                DropDownBy.Items.Add(new ListItem("Year/Month","Year"));
                DropDownBy.Items.Add(new ListItem("Year/Quarter","Quarter"));                
                DropDownBy.Items.Add("Years");
                DropDownBy.Items.Add("Customer");
                DropDownBy.SelectedIndex=8;
                
    			
    			 myListItem = new ListItem("None","");
    			 Month.Items.Add(myListItem);
    			 myListItem = new ListItem("Jan","1");
    			 Month.Items.Add(myListItem);
    			 myListItem = new ListItem("Feb","2");
    			 Month.Items.Add(myListItem);
				 myListItem = new ListItem("Mar","3");
    			 Month.Items.Add(myListItem);
    			 myListItem = new ListItem("Apr","4");
    			 Month.Items.Add(myListItem);
    			 myListItem = new ListItem("May","5");
    			 Month.Items.Add(myListItem);
				 myListItem = new ListItem("Jun","6");
    			 Month.Items.Add(myListItem);
    			 myListItem = new ListItem("Jul","7");
    			 Month.Items.Add(myListItem);
    			 myListItem = new ListItem("Aug","8");
    			 Month.Items.Add(myListItem);
				 myListItem = new ListItem("Sep","9");
    			 Month.Items.Add(myListItem);
    			 myListItem = new ListItem("Oct","10");
    			 Month.Items.Add(myListItem);
				 myListItem = new ListItem("Nov","11");
    			 Month.Items.Add(myListItem);
				 myListItem = new ListItem("Dec","12");
    			 Month.Items.Add(myListItem);
    			 Month.SelectedIndex = dt.Month;
				 
				 myListItem = new ListItem("None","");
				 Day.Items.Add(myListItem);
				 for(int d=1;d<32;d++)
    			 Day.Items.Add(d.ToString());
    			 Day.SelectedIndex = dt.Day;
    			 
    			 myListItem = new ListItem("None","");
    			 Year.Items.Add(myListItem);
    			 Year.Items.Add("2001");
    			 Year.Items.Add("2002");
    			 Year.Items.Add("2003");
    			 Year.Items.Add("2004");
    			 Year.SelectedIndex = 2;
    			 
    			 //Set FromDate
    			 myListItem = new ListItem("","");
    			 MonthFrom.Items.Add(myListItem);
    			 myListItem = new ListItem("Jan","1");
    			 MonthFrom.Items.Add(myListItem);
    			 myListItem = new ListItem("Feb","2");
    			 MonthFrom.Items.Add(myListItem);
				 myListItem = new ListItem("Mar","3");
    			 MonthFrom.Items.Add(myListItem);
    			 myListItem = new ListItem("Apr","4");
    			 MonthFrom.Items.Add(myListItem);
    			 myListItem = new ListItem("May","5");
    			 MonthFrom.Items.Add(myListItem);
				 myListItem = new ListItem("Jun","6");
    			 MonthFrom.Items.Add(myListItem);
    			 myListItem = new ListItem("Jul","7");
    			 MonthFrom.Items.Add(myListItem);
    			 myListItem = new ListItem("Aug","8");
    			 MonthFrom.Items.Add(myListItem);
				 myListItem = new ListItem("Sep","9");
    			 MonthFrom.Items.Add(myListItem);
    			 myListItem = new ListItem("Oct","10");
    			 MonthFrom.Items.Add(myListItem);
				 myListItem = new ListItem("Nov","11");
    			 MonthFrom.Items.Add(myListItem);
				 myListItem = new ListItem("Dec","12");
    			 MonthFrom.Items.Add(myListItem);
    			 
				 
				 myListItem = new ListItem("","");
				 DayFrom.Items.Add(myListItem);
				 for(int d=1;d<32;d++)
    			 DayFrom.Items.Add(d.ToString());
    			 
    			 
    			 myListItem = new ListItem("","");
    			 YearFrom.Items.Add(myListItem);
    			 YearFrom.Items.Add("2001");
    			 YearFrom.Items.Add("2002");
    			 YearFrom.Items.Add("2003");
    			 YearFrom.Items.Add("2004");
    			 

				//Set ToDate
				 myListItem = new ListItem("","");
    			 MonthTo.Items.Add(myListItem);
    			 myListItem = new ListItem("Jan","1");
    			 MonthTo.Items.Add(myListItem);
    			 myListItem = new ListItem("Feb","2");
    			 MonthTo.Items.Add(myListItem);
				 myListItem = new ListItem("Mar","3");
    			 MonthTo.Items.Add(myListItem);
    			 myListItem = new ListItem("Apr","4");
    			 MonthTo.Items.Add(myListItem);
    			 myListItem = new ListItem("May","5");
    			 MonthTo.Items.Add(myListItem);
				 myListItem = new ListItem("Jun","6");
    			 MonthTo.Items.Add(myListItem);
    			 myListItem = new ListItem("Jul","7");
    			 MonthTo.Items.Add(myListItem);
    			 myListItem = new ListItem("Aug","8");
    			 MonthTo.Items.Add(myListItem);
				 myListItem = new ListItem("Sep","9");
    			 MonthTo.Items.Add(myListItem);
    			 myListItem = new ListItem("Oct","10");
    			 MonthTo.Items.Add(myListItem);
				 myListItem = new ListItem("Nov","11");
    			 MonthTo.Items.Add(myListItem);
				 myListItem = new ListItem("Dec","12");
    			 MonthTo.Items.Add(myListItem);
    			 
				 
				 myListItem = new ListItem("","");
				 DayTo.Items.Add(myListItem);
				 for(int d=1;d<32;d++)
    			 DayTo.Items.Add(d.ToString());
    			
    			 
    			 myListItem = new ListItem("","");
    			 YearTo.Items.Add(myListItem);
    			 YearTo.Items.Add("2001");
    			 YearTo.Items.Add("2002");
    			 YearTo.Items.Add("2003");
    			 YearTo.Items.Add("2004");
    			
    			
                 myListItem = new ListItem("None","");
                SplitBy.Items.Add(myListItem);
                SplitBy.Items.Add("Customer");
    			
                DropDownChartType.Items.Add("Combo");
                DropDownChartType.Items.Add("ComboSideBySide");
                DropDownChartType.Items.Add("ComboHorizontal");
                myListItem = new ListItem("Single Pie","Pie");
                DropDownChartType.Items.Add(myListItem);
                myListItem = new ListItem("Multi Pie","Pies");
                DropDownChartType.Items.Add(myListItem);
                myListItem = new ListItem("Single Donut","Donut");
                DropDownChartType.Items.Add(myListItem);
                myListItem = new ListItem("Multi Donut","Donuts");
                DropDownChartType.Items.Add(myListItem);
                 myListItem = new ListItem("Single Radar","radar");
                DropDownChartType.Items.Add(myListItem);
                myListItem = new ListItem("Multi Radar","radars");
                DropDownChartType.Items.Add(myListItem);
                DropDownChartType.Items.Add("Scatter");
                DropDownChartType.Items.Add("Bubble");
                
                DropDownScale.Items.Add("Normal");
                DropDownScale.Items.Add("Range");
				DropDownScale.Items.Add("Logarithmic");
				DropDownScale.Items.Add("Stacked");
				DropDownScale.Items.Add("FullStacked");
				DropDownScale.Items.Add("LogarithmicStacked");
    
                 myListItem = new ListItem("3D","true");
                Chart3D.Items.Add(myListItem);
                 myListItem = new ListItem("2D","false");
                Chart3D.Items.Add(myListItem);
    
                DropDownSeriesType.Items.Add("Column");
                DropDownSeriesType.Items.Add("Cylinder");
                DropDownSeriesType.Items.Add("Marker");
                DropDownSeriesType.Items.Add("Line");
                DropDownSeriesType.Items.Add("AreaLine");
                DropDownSeriesType.Items.Add("Spline");
    
    
                myListItem = new ListItem("None","");
                DropDownSeriesAggregation.Items.Add(myListItem);
                DropDownSeriesAggregation.Items.Add("Average");
                DropDownSeriesAggregation.Items.Add("Sum");
                DropDownSeriesAggregation.Items.Add("Mode");
				DropDownSeriesAggregation.Items.Add("Median");
				DropDownSeriesAggregation.Items.Add("Maximum");
				DropDownSeriesAggregation.Items.Add("Minimum");
				DropDownSeriesAggregation.Items.Add("RunningAverage");
                DropDownSeriesAggregation.Items.Add("RunningSum");
                DropDownSeriesAggregation.Items.Add("RunningMode");
				DropDownSeriesAggregation.Items.Add("RunningMedian");
				DropDownSeriesAggregation.Items.Add("RunningMaximum");
				DropDownSeriesAggregation.Items.Add("RunningMinimum");


    
                SummerySeriesType.Items.Add("Column");
                SummerySeriesType.Items.Add("Cylinder");
                SummerySeriesType.Items.Add("Marker");
                SummerySeriesType.Items.Add("Line");
                SummerySeriesType.Items.Add("AreaLine");
                SummerySeriesType.Items.Add("Spline");
                SummerySeriesType.SelectedIndex=3;
                
                DropDownLimitMode.Items.Add("Top");
                DropDownLimitMode.Items.Add("Bottom");
				DropDownLimitMode.Items.Add("ExcludeTop");
				DropDownLimitMode.Items.Add("ExcludeBottom");

   
            }
    
    }
    
void ButtonDisplay_Click(Object sender, EventArgs e)
{
       dotnetCHARTING.ChartType myChartType = (dotnetCHARTING.ChartType)Enum.Parse(typeof(dotnetCHARTING.ChartType),DropDownChartType.SelectedItem.Value,true);
       dotnetCHARTING.Scale myAxisScale = (dotnetCHARTING.Scale)Enum.Parse(typeof(dotnetCHARTING.Scale),DropDownScale.SelectedItem.Value,true);
       dotnetCHARTING.SeriesType mySeriesType= (dotnetCHARTING.SeriesType)Enum.Parse(typeof(dotnetCHARTING.SeriesType),DropDownSeriesType.SelectedItem.Value,true);
       string show = DropDownShow.SelectedItem.Value;
       string yearFromDate = YearFrom.SelectedItem.Value;
       string monthFromDate = MonthFrom.SelectedItem.Value;
	   string dayFromDate = DayFrom.SelectedItem.Value;

       string yearToDate = YearTo.SelectedItem.Value;
       string monthToDate = MonthTo.SelectedItem.Value;
	   string dayToDate = DayTo.SelectedItem.Value;

       dotnetCHARTING.TimeInterval myDategrouping = TimeInterval.Year;
       string ByCustomer="";
       if(DropDownBy.SelectedItem.Value=="Customer")
       		ByCustomer="Customer";
       	else
	       myDategrouping = (dotnetCHARTING.TimeInterval)Enum.Parse(typeof(dotnetCHARTING.TimeInterval),DropDownBy.SelectedItem.Value,true);

       string splitBy = SplitBy.SelectedItem.Value;
       string splitByLimit = SplitByLimit.Text;
       string seriesAggregation = DropDownSeriesAggregation.SelectedItem.Value;
       string limit = TextBoxLimit.Text;
       string show3D = Chart3D.SelectedItem.Value;
       string reportType = show + "by";
       if(ByCustomer!="")
       	reportType += ByCustomer;
       else
       	reportType += myDategrouping;

	   StringBuilder SqlSelect = null;
       StringBuilder where=null;
       if((yearFromDate!="" && monthFromDate!=""  && dayFromDate!="" ) || (yearToDate !="" && monthToDate!=""  && dayToDate!=""))
       {
           where = new StringBuilder(" WHERE ");
           if(yearFromDate!="" && monthFromDate!=""  && dayFromDate!="" )
           {
           	   Chart.DefaultSeries.StartDate= new DateTime(Convert.ToInt32(yearFromDate), Convert.ToInt32(monthFromDate),Convert.ToInt32(dayFromDate),0,0,0);
               where.Append("OrderDate >= #STARTDATE#");
               
                if(yearToDate !="" && monthToDate!=""  && dayToDate!="")
                {
                	Chart.DefaultSeries.EndDate=new DateTime(Convert.ToInt32(yearToDate), Convert.ToInt32(monthToDate),Convert.ToInt32(dayToDate),23,59,59);
          			where.Append("AND OrderDate <= #ENDDATE#");
          			    
                }
           }
           else
           {
              	Chart.DefaultSeries.EndDate=new DateTime(Convert.ToInt32(yearToDate), Convert.ToInt32(monthToDate),Convert.ToInt32(dayToDate),23,59,59);
          		where.Append("AND OrderDate <= #ENDDATE#");
           }
    
       }
       else
       {
       	string daySelection = Day.SelectedItem.Value;
       	string monthSelection = Month.SelectedItem.Value;
       	string yearSelection = Year.SelectedItem.Value;
       		if(yearSelection!="" && monthSelection!="" && daySelection!="")
       		{
       	
       			int d = DateTime.DaysInMonth(int.Parse(yearSelection),int.Parse(monthSelection));
       			if(int.Parse(daySelection) > d)
       			{
       				daySelection = d.ToString();
       				Day.SelectedIndex = d;
       			}
       		do{
       			if(myDategrouping == TimeInterval.Day || myDategrouping == TimeInterval.Hours)
       			{
       				Chart.DefaultSeries.StartDate= new DateTime(Convert.ToInt32(yearSelection), Convert.ToInt32(monthSelection), Convert.ToInt32(daySelection),0,0,0);
       				Chart.DefaultSeries.EndDate=new DateTime(Convert.ToInt32(yearSelection), Convert.ToInt32(monthSelection), Convert.ToInt32(daySelection),23,59,59);

       				where = new StringBuilder(" WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ");
           			
          		           			
       				break;
       			}
       			else if(myDategrouping == TimeInterval.Month || myDategrouping== TimeInterval.Days || myDategrouping== TimeInterval.Weeks)
       			{
       				Chart.DefaultSeries.StartDate= new DateTime(Convert.ToInt32(yearSelection), Convert.ToInt32(monthSelection), 1,0,0,0);
       				Chart.DefaultSeries.EndDate=new DateTime(Convert.ToInt32(yearSelection), Convert.ToInt32(monthSelection), d,23,59,59);


       				where = new StringBuilder(" WHERE OrderDate >= #STARTDATE# ");
           			where.Append("AND OrderDate <= #ENDDATE#");
       				break;
       			}
       			else if(myDategrouping == TimeInterval.Year || myDategrouping == TimeInterval.Quarter  || myDategrouping == TimeInterval.Weeks)
       			{
       				Chart.DefaultSeries.StartDate= new DateTime(Convert.ToInt32(yearSelection), 1, 1,0,0,0);
       				Chart.DefaultSeries.EndDate=new DateTime(Convert.ToInt32(yearSelection), 12, 31,23,59,59);

       				where = new StringBuilder(" WHERE OrderDate >= #STARTDATE# ");
           			where.Append("AND OrderDate <= #ENDDATE#");       				
       				break;
       			}
       			

       		}while(false);

		}
  	}
       
       			//General settings
       			Chart.Visible=true;    
       			Chart.Type = myChartType;
    			Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];
    			Chart.Height=480;
                Chart.Width=640;
                Chart.UseFile=true;
           		Chart.TempDirectory="temp";
				//Chart.TitleBox.IconPath="../../Images/icon.gif";
				Chart.DefaultSeries.Limit=limit;
				Chart.DefaultSeries.LimitMode = (dotnetCHARTING.LimitMode)Enum.Parse(typeof(dotnetCHARTING.LimitMode),DropDownLimitMode.SelectedItem.Value,true);
				Chart.Debug=true;
				Chart.DonutHoleSize = 50;
				Chart.LegendBox.Template="%icon%name";
				 if(ShowValues.Checked)
          			Chart.DefaultSeries.DefaultElement.ShowValue= true;

				 if(show3D=="true")
                    Chart.Use3D=true;
                else
                    Chart.Use3D=false;
                
                if(Transpose.Checked)
                	Chart.Transpose=true;
                	
                if(ShowOther.Checked)
       				Chart.DefaultSeries.ShowOther=true;
       			else
       				Chart.DefaultSeries.ShowOther=false;
	       		Chart.DefaultSeries.Type = mySeriesType;
                
				Chart.DefaultAxis.Scale = myAxisScale;
								
				if(ByCustomer=="" && myChartType!=ChartType.Scatter && myChartType!=ChartType.Bubble)
				{
					Chart.DateGrouping = myDategrouping;
					if(DropDownShow.SelectedItem.Value == "Sales")
						Chart.YAxis.FormatString ="currency";
					else
						Chart.DefaultAxis.NumberPercision = 1;
						
						
					
				}
                reportType = reportType.ToLower();
				if(myChartType.Equals(ChartType.Bubble))
				{
					//set global properties
    					Chart.Title="Sales comparison for 3 days";
				    Chart.YAxis.Label.Text ="Sales";
				    Chart.XAxis.Label.Text ="No of Items";
    					Chart.Palette = new Color [] {Color.Blue,Color.Red,Color.Yellow};
    					Chart.DefaultSeries.DefaultElement.Transparency = 50;
    					Chart.XAxis.NumberPercision=2;
    					Chart.LegendBox.Template="%icon %name";
    
    
    				//Add a series
    				Chart.Series.Name="Dec 6,2002";
    				Chart.Series.StartDate = new DateTime(2002,12,6,0,0,0);
    				Chart.Series.EndDate = new DateTime(2002,12,6,23,59,59);
					Chart.Series.SqlStatement = @"SELECT Orders.ItemNo,Sum(Orders.Total), Sum(Orders.Quantity) FROM Orders WHERE (Orders.OrderDate >= #STARTDATE#) And (Orders.OrderDate <= #ENDDATE# ) GROUP BY Orders.ItemNo";
    				Chart.SeriesCollection.Add();
   
  					//Add a series
    				Chart.Series.Name="Dec 7,2002";
    				Chart.Series.StartDate =new DateTime(2002,12,7,0,0,0);
					Chart.Series.EndDate=new DateTime(2002,12,7,23,59,59);
    				Chart.Series.SqlStatement = @"SELECT  Orders.ItemNo,Sum(Orders.Total), Sum(Orders.Quantity) FROM Orders WHERE (Orders.OrderDate >= #STARTDATE#) And (Orders.OrderDate <= #ENDDATE# ) GROUP BY Orders.ItemNo";
    				Chart.SeriesCollection.Add();
    				
    				//Add a series
    				Chart.Series.Name="Dec 8,2002";
    				Chart.Series.StartDate =new DateTime(2002,12,8,0,0,0);
    				Chart.Series.EndDate=new DateTime(2002,12,8,23,59,59);

    				Chart.Series.SqlStatement = @"SELECT  Orders.ItemNo,Sum(Orders.Total), Sum(Orders.Quantity) FROM Orders WHERE (Orders.OrderDate >= #STARTDATE#) And (Orders.OrderDate <= #ENDDATE# ) GROUP BY Orders.ItemNo";
    				Chart.SeriesCollection.Add();
    

				}
				else if(myChartType.Equals(ChartType.Scatter))
				{
					Chart.Title=show + " By item no";
					Chart.YAxis.Label.Text =show;
					Chart.XAxis.Label.Text ="Item No";
					Chart.DefaultSeries.Name="Dec 6-8,2002";
    				Chart.DefaultSeries.StartDate =new DateTime(2002,12,6,0,0,0);
    				Chart.DefaultSeries.EndDate=new DateTime(2002,12,8,23,59,59);
    				Chart.DefaultSeries.DefaultElement.LabelTemplate="(%xvalue, %yvalue)";
    				Chart.XAxis.NumberPercision=2;


					switch(show.ToLower())
					{
    					case "sales":
    						//Add a series
							Chart.Series.SqlStatement= @"SELECT Orders.ItemNo,Sum(Orders.Total) FROM Orders WHERE (Orders.OrderDate >= #STARTDATE#) And (Orders.OrderDate <= #ENDDATE# ) GROUP BY Orders.ItemNo";
    						Chart.SeriesCollection.Add();
    					break;
    					case "orders":
    						//Add a series
							Chart.Series.SqlStatement= @"SELECT Orders.ItemNo,Count(1) FROM Orders WHERE (Orders.OrderDate >= #STARTDATE#) And (Orders.OrderDate <= #ENDDATE# ) GROUP BY Orders.ItemNo";
    						Chart.SeriesCollection.Add();
    					break;
    					case "items":
    						//Add a series
							Chart.Series.SqlStatement= @"SELECT  Orders.ItemNo,Sum(Orders.Quantity) FROM Orders WHERE (Orders.OrderDate >= #STARTDATE#) And (Orders.OrderDate <= #ENDDATE# ) GROUP BY Orders.ItemNo";
    						Chart.SeriesCollection.Add();
    					break;
					}
				}
				else
				{
					if(Sum.Checked)
						Chart.Series.Elements.Add(Calculation.Sum,"Total");
					if(Average.Checked)
                			Chart.Series.Elements.Add(Calculation.Average,"Average");
                		if(Mode.Checked)
                 		Chart.Series.Elements.Add(Calculation.Mode,"Mode");
                		if(Median.Checked)
	                 	Chart.Series.Elements.Add(Calculation.Median,"Median");
	            		if(Min.Checked)
					 	Chart.Series.Elements.Add(Calculation.Minimum,"Min");
			    		if(Max.Checked)
                 		Chart.Series.Elements.Add(Calculation.Maximum,"Max");
                 
				
                		if(ByCustomer!="")
                		{
                			Chart.XAxis.Label.Text="Customers";
                		}
						else
						{
							if(myChartType == dotnetCHARTING.ChartType.Combo)
								Chart.XAxis.Label.Text=myDategrouping.ToString();
						}
					Chart.Series.Name = DropDownShow.SelectedItem.Value;
					Chart.DefaultSeries.SplitByLimit=splitByLimit;

                switch(reportType.ToLower())
                {
                    case "salesbyday":
                    case "salesbyday/hour":
                    case "salesbyhours":
                    case "salesbyweek":
                    case "salesbyweeks":
                    case "salesbyweek/day":
                    case "salesbymonth/day":
                    case "salesbymonth":
                    case "salesbydays":
                    case "salesbyyear":
                    case "salesbyquarter":
                    case "salesbyyear/month":
                    case "salesbymonths":
                    case "salesbyquarters":
                    case "salesbyyears":
                    {
                        switch(splitBy.ToLower())
                        {
                            case "customer":
                            	Chart.Title="Sales By customer";
                                SqlSelect = new StringBuilder("SELECT  OrderDate,Total, Name FROM Orders ",128);
                                if(where !=null)
                                    SqlSelect.Append(where);
                                SqlSelect.Append(" ORDER BY Orders.OrderDate");
                                Chart.Series.SqlStatement= SqlSelect.ToString();
                                Chart.SeriesCollection.Add();
                                break;
                            default:
                            	Chart.Title="Sales";
                                SqlSelect = new StringBuilder("SELECT OrderDate,Total FROM Orders ");
                                if(where !=null)
                                    SqlSelect.Append(where);
                                SqlSelect.Append(" ORDER BY OrderDate");
                                Chart.Series.SqlStatement= SqlSelect.ToString();
                                Chart.SeriesCollection.Add();
                                break;
                            }
                        }
                    break;
                 case "ordersbyday/hour":
                case "ordersbyday":
                case "ordersbyhours":
                case "ordersbyweek":
                case "ordersbyweek/day":
                case "ordersbymonth/day":
                case "ordersbymonth":
                case "ordersbydays":
                case "ordersbyyear":
                case "ordersbyquarter":
                case "ordersbyyear/month":
                case "ordersbymonths":
    			   case "ordersbyweeks":
                case "ordersbyyears":
                case "ordersbyquarters":
                    {
                        switch(splitBy.ToLower())
                        {

                            case "customer":
                            	Chart.Title="Orders by customer";
                                SqlSelect = new StringBuilder("SELECT  OrderDate,1 AS q, Name FROM Orders ",128);
                                if(where !=null)
                                    SqlSelect.Append(where);
                                SqlSelect.Append(" ORDER BY Orders.OrderDate");
                                Chart.Series.SqlStatement= SqlSelect.ToString();
                                Chart.SeriesCollection.Add();
                                break;
                            default:
                            	Chart.Title="Orders";
                                SqlSelect = new StringBuilder("SELECT OrderDate,1 AS q FROM Orders ");
                                if(where !=null)
                                    SqlSelect.Append(where);
                                SqlSelect.Append(" ORDER BY OrderDate");
                                Chart.Series.SqlStatement= SqlSelect.ToString();
                                Chart.SeriesCollection.Add();
                                break;
                            }
                      }
                break;
     
                case "itemsbyday/hour":
                case "itemsbyday":
                case "itemsbyhours":
                case "itemsbyweek/day":
                case "itemsbyweek":
                case "itemsbymonth/day":
                case "itemsbymonth":
                case "itemsbyweeks":
                case "itemsbydays":
                case "itemsbyyear":
                case "itemsbyquarter":
                case "itemsbyyear/month":
                case "itemsbymonths":
                case "itemsbyyears":
                case "itemsbyquarters":
                {
                    switch(splitBy.ToLower())
                    {

                        case "customer":
                        	Chart.Title="Items by customer";
                            SqlSelect = new StringBuilder(@"SELECT  OrderDate,Sum(Quantity) AS CountOfQuantity, Name FROM Orders",128);
                            if(where !=null)
                                SqlSelect.Append(where);
                            SqlSelect.Append(" GROUP BY Orders.OrderDate, Orders.Name ORDER BY Orders.OrderDate");
                            Chart.Series.SqlStatement= SqlSelect.ToString();
                            Chart.SeriesCollection.Add();
                            break;
                        default:
                        	Chart.Title="Items";
                            SqlSelect = new StringBuilder("SELECT  OrderDate,Sum(Quantity) FROM Orders ",128);
                            if(where !=null)
                               SqlSelect.Append(where);
                            SqlSelect.Append(" GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate");
                            Chart.Series.SqlStatement= SqlSelect.ToString();
                            Chart.SeriesCollection.Add();
                            break;
                        }
                    }
                break;
    
                case "salesbycustomer":
                	Chart.Title="Sales by customer";
                    SqlSelect = new StringBuilder("SELECT  Orders.Name,Sum(Orders.Total) AS TotalOrders FROM Orders ");
                            if(where !=null)
                               SqlSelect.Append(where);
                            SqlSelect.Append(" GROUP BY Orders.Name ORDER BY Count(Orders.Total) DESC");
                    Chart.Series.SqlStatement= SqlSelect.ToString();
                    Chart.SeriesCollection.Add();
                break;
                case "itemsbycustomer":
                	Chart.Title="Items by customer";
                    SqlSelect = new StringBuilder("SELECT Name,Sum(Quantity) FROM Orders ");
                            if(where !=null)
                               SqlSelect.Append(where);
                            SqlSelect.Append(" GROUP BY Name ORDER BY Sum(Quantity) DESC");
                    Chart.Series.SqlStatement= SqlSelect.ToString();    
                    Chart.SeriesCollection.Add();
                break;
    
                case "ordersbycustomer":
                	Chart.Title="Orders by customer";
              				SqlSelect = new StringBuilder("SELECT Name,Count(1) FROM Orders ",128);
                            if(where !=null)
                               SqlSelect.Append(where);
                            SqlSelect.Append(" GROUP BY Name ORDER BY Count(1) DESC");
                            Chart.Series.SqlStatement= SqlSelect.ToString();
                    Chart.SeriesCollection.Add();
                break;

                //**************************************************************************    
                }
                }
                if(seriesAggregation!="")
                {                  
                   
                        Chart.Series.Name = seriesAggregation;
                        Chart.Series.Type = (dotnetCHARTING.SeriesType)Enum.Parse(typeof(dotnetCHARTING.SeriesType),SummerySeriesType.SelectedItem.Value,true);
                        Chart.SeriesCollection.Add((dotnetCHARTING.Calculation)Enum.Parse(typeof(dotnetCHARTING.Calculation), seriesAggregation,true));
                        
                }
    }
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Report Options</title>
    <style type="text/css">
     body.myBody  
     {
     border-top-width:0px;
     
     }
     img.myImage  
     {
     border:0px;
     
     }
     td.myTD1  
     {
      width:"1%";
     }
     td.myTD2  
     {
      width:"99%";
      background:"#BFC0DB";
     }
     
    </style>
</head>
<body class="myBody">
    <form runat="server" action="sample.aspx">
        <div style="text-align:center">
            <table border="1" cellpadding="3" cellspacing="0" style="border-collapse: collapse; border-color: #111111" id="AutoNumber1">
                <tr>
    <td class="myTD1">
<img class="myImage" src="../../images/dotnetCharting.gif" width="230"  alt="dotnetChartingImage" height="94"/></td>
    <td class="myTD2" >Report: 
 <ASP:DropDownList id="DropDownShow" runat="server">
    </ASP:DropDownList>&nbsp;By:&nbsp; 
<ASP:DropDownList id="DropDownBy" runat="server">
    </ASP:DropDownList>
<ASP:DropDownList id="Month" runat="server">
    </ASP:DropDownList>/
<ASP:DropDownList id="Day" runat="server">
    </ASP:DropDownList>/
<ASP:DropDownList id="Year" runat="server">
    </ASP:DropDownList>
Split By: 
    <ASP:DropDownList id="SplitBy" runat="server"/>
 Limit Series:
 <asp:TextBox id="SplitByLimit" width="25" runat="server" /> 
        <br/>From Date: 
    <ASP:DropDownList id="MonthFrom" runat="server">
    </ASP:DropDownList>/
<ASP:DropDownList id="DayFrom" runat="server">
    </ASP:DropDownList>/
<ASP:DropDownList id="YearFrom" runat="server">
    </ASP:DropDownList>
&nbsp;To Date: 
      <ASP:DropDownList id="MonthTo" runat="server">
    </ASP:DropDownList>/
<ASP:DropDownList id="DayTo" runat="server">
    </ASP:DropDownList>/
<ASP:DropDownList id="YearTo" runat="server">
    </ASP:DropDownList>
&nbsp;Limit Elements: <asp:TextBox id="TextBoxLimit" width="25" runat="server" ></asp:TextBox>
Limit Mode:<ASP:DropDownList id="DropDownLimitMode" runat="server"> 
    </ASP:DropDownList>
	<asp:CheckBox id="ShowOther" Text="Show other" runat="server"/>
      <asp:CheckBox id="ShowValues" Text="Show Values" runat="server"/>
        <asp:CheckBox id="Average" Text="Average" runat="server"/>
      	<asp:CheckBox id="Sum" Text="Sum" runat="server"/>
      	<asp:CheckBox id="Mode" Text="Mode" runat="server"/>
		<asp:CheckBox id="Median" Text="Median" runat="server"/>
		<asp:CheckBox id="Min" Text="Minimum" runat="server"/>
		<asp:CheckBox id="Max" Text="Maximum" runat="server"/>
	
of series. Chart Type: 
    <ASP:DropDownList id="DropDownChartType" runat="server">
    </ASP:DropDownList>&nbsp; 
     Axis Scale:<ASP:DropDownList id="DropDownScale" runat="server">
    </ASP:DropDownList>&nbsp;
    Transpose:
		<asp:CheckBox id="Transpose" runat="server"/>

<ASP:DropDownList id="Chart3D" runat="server">
    </ASP:DropDownList>&nbsp; 
<ASP:DropDownList id="DropDownSeriesType" runat="server">
    </ASP:DropDownList>
    &nbsp;Summary series: 
    <ASP:DropDownList id="DropDownSeriesAggregation" runat="server">
    </ASP:DropDownList>
    <ASP:DropDownList id="SummerySeriesType" runat="server">
    </ASP:DropDownList>
    <asp:Button id="ButtonDisplay" onclick="ButtonDisplay_Click" runat="server" Text="Display">
    </asp:Button>
                  </td>
  
                                        
                </tr>
            </table>
            <dotnet:Chart id="Chart" runat="server" Visible="false" />
        </div>
    </form></body>
</html>