<%@ Page Language="C#" debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Annotation Link</title>
		<script runat="server">



void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.ShadingEffect=true;
	
	
	// This sample demonstrates how you can place links anywhere on the chart using annotations.
	
	// First we'll instantiate our annotation.
	Annotation a = new Annotation();
	
	// Because we just want a text link we'll get rid of the box. You can comment this line out to see what the box looks like.
	a.ClearColors();
	
	// Set the link text
	a.Label.Text = ".netCHARTING Website";
	
	// In order to make it look like a link we'll make it blue and underlined.
	a.Label.Color = Color.Blue;
	a.Label.Font = new Font("Arial",8,FontStyle.Underline);
	
	// Set the url for our link.
	a.URL = "http://www.dotnetcharting.com";
	
	// We set a point where the annotation appears
	a.Position = new Point(50,30);
	
	// By default, the annotation will create a rectangle for the text. In order to place the text on one line we'll 
	// set the dynamic size property to false.
	a.DynamicSize = false;
	
	// Add the annotation to our chart.
	Chart.Annotations.Add(a);
	

	// Add the random data.
	Chart.SeriesCollection.Add(getRandomData());
    
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
