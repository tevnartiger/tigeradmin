<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 6; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 1; b < 7; b++)
		{
			Element e = new Element();
			e.Name = "E " + b;
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}


void Page_Load(Object sender,EventArgs e)
{
	// Set the title.
	Chart.Title="My Chart";
	Chart.Type = ChartType.Combo;
	Chart.TempDirectory="temp";

	// Set the chart size.
	Chart.Width = 600;
	Chart.Height = 350;
	
	// Get the data.
	SeriesCollection sc = getRandomData();
	
	//Set one of the series on a different axis
	Axis newAxis = new Axis();
	sc[3].XAxis = newAxis;
	
	//Clear method makes the original axis invisible.
	Chart.XAxis.Clear();


	// Add the random data.
	Chart.SeriesCollection.Add(sc);
    
    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Invisible Axis Sample</title>
</head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
