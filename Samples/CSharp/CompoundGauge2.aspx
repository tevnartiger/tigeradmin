<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    void Page_Load(Object sender, EventArgs e)
    {
        // This sample demonstrates using compounded gauges.

        Chart.TempDirectory = "temp";
        Chart.Debug = true;
        Chart.Palette = new Color[] {  Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) };
        Chart.Mentor = false;
        Chart.Type = ChartType.Gauges;
        Chart.Size = "500x350";
	Chart.OverlapFooter = true;
        Chart.Title = ".netCHARTING Sample";
        Chart.Background.Color = Color.Transparent;

        // *DYNAMIC DATA NOTE* 
        // This sample uses random data to populate the chart. To populate 
        // a chart with database data see the following resources:
        // - Help File > Getting Started > Data Tutorials
        // - DataEngine Class in the help file	
        // - Sample: features/DataEngine.aspx

        SeriesCollection mySC = getRandomData();
        Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;

        // Add the random data.
        Chart.SeriesCollection.Add(mySC);
        Chart.ChartArea.ClearColors();

        Series digital = Series.FromYValues(mySC[0][0].YValue);
        digital.XAxis = new Axis();
        digital.XAxis.DefaultTick.Label.Text = "";
        digital.GaugeType = GaugeType.Bars;
        digital.GaugeBorderBox.Padding = 2;
        mySC[0].Background.Color = Color.White;
        digital.GaugeBorderBox.Position = new Rectangle(180,270,130,20);
        digital.GaugeBorderBox.Shadow.Color = Color.Empty;
        digital.LegendEntry.Visible = false;
        Chart.SeriesCollection.Add(digital);
    }

    SeriesCollection getRandomData()
    {
        Random myR = new Random(1);
        SeriesCollection SC = new SeriesCollection();
        int a = 0;
        int b = 0;
        for (a = 1; a < 2; a++)
        {
            Series s = new Series("Series " + a.ToString());
            for (b = 1; b < 2; b++)
            {
                Element e = new Element("Element " + b.ToString());
                e.YValue = myR.Next(14150);
                s.Elements.Add(e);
            }
            SC.Add(s);
        }
        return SC;
    }

    SeriesCollection getLiveData()
    {
        DataEngine de = new DataEngine("ConnectionString goes here");
        de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ....";
        return de.GetSeries();
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body bgcolor="#00cc33" background="../../images/back038.jpg">
    <div align="center">
        <dotnet:Chart ID="Chart" runat="server" />
    </div>
</body>
</html>
