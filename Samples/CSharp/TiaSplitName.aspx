<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	// This sample demonstrates splitting a single series into multiple series based on element's name.

	SetChart(Chart);
	SetChart(Chart1);
	SetChart(Chart2);

	Chart.Title = "Original Data";
	Chart.XAxis.Scale = Scale.Time;
	Chart.LegendBox.DefaultEntry.Value = "%Sum";	
	SeriesCollection mySC = getRandomData();
	Chart.SeriesCollection.Add(mySC); //Original Data
	
	//Group the data with year by months
	Chart1.DefaultElement.DefaultSubValue.Marker.Visible = false;
	Chart1.DefaultElement.DefaultSubValue.Type = SubValueType.Marker;
	Chart1.DefaultElement.DefaultSubValue.Marker.Type = ElementMarkerType.None;
	Chart1.ChartArea.Label.Text = "Grouping the original series by using TimeIntervalAdvanced\nobject. The elements are grouped with\nyear by months.";
	Series splits = mySC[0].SplitRegroupCalculate(TimeIntervalAdvanced.Year,TimeIntervalAdvanced.Month,Calculation.Sum,"MMM",true);
	Chart1.SeriesCollection.Add(splits); 
	
	//Split the previous series based on original element's name.
	Chart2.ChartArea.Label.Text = "Splitting the previous series by the original element's name.\n(Element subvalue's name)";
	
	SeriesCollection mySC2 = splits.SplitByName(); 
	//SplitByName with parameters.
	//SeriesCollection mySC2 = splits.SplitByName("3",LimitMode.Bottom,true,"All others");
	Chart2.DefaultElement.DefaultSubValue.Marker.Visible = false;
	Chart2.DefaultElement.DefaultSubValue.Type = SubValueType.Marker;
	Chart2.DefaultElement.DefaultSubValue.Marker.Type = ElementMarkerType.None;
	Chart2.SeriesCollection.Add(mySC2); 
	
       
}
void SetChart(Chart c)
{
	c.Type = ChartType.Combo;
	c.Size = "600x350";
	c.TempDirectory = "temp";
	c.Debug = true;
	c.Title = ".netCHARTING Sample";
	c.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom;
	}


SeriesCollection getRandomData()
{
	string [] names = {"John","David", "Mary", "Carlson", "Sam", "Mark"};
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
	DateTime dt = new DateTime(2006,1,1);
	for(int a = 1; a < 2; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a.ToString();
		for(int b = 1; b < 365; b++)
		{
			Element e = new Element();
			e.Name = names[myR.Next(0,5)];
			e.YValue = myR.Next(50);
			e.XDateTime = dt = dt.AddDays(1);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	return SC;
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	</head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
			<dotnet:Chart id="Chart1" runat="server"/>
			<dotnet:Chart id="Chart2" runat="server"/>
		</div>
	</body>
</html>
