<%@ Page Language="C#" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	Chart.Type = ChartType.Combo;
	Chart.Width = 800;
	Chart.Height = 500;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = "DataDistribution";
	Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	SeriesCollection mySC = getRandomData();
	Chart.SeriesCollection.Add(mySC[0]);
	Chart.DefaultSeries.Type = SeriesType.Spline;
	
	StatisticalEngine.Options.MatchColors = true;
	Series dataDistribution = StatisticalEngine.DataDistribution(mySC[0]);
	Chart.SeriesCollection.Add (dataDistribution);
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
    DateTime dt = new DateTime(2005,1,1);
	for(int a = 1; a < 3; a++)
	{
		Series s = new Series();
		s.Name = "Downloads in January";
		for(int b = 0; b < 31; b++)
		{
			Element e = new Element();
			//e.Name = "Element " + b;
			e.YValue = myR.Next(50);
            e.XDateTime = dt.AddDays(b);
            e.Name = e.XDateTime.ToString("d");
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(255,99,49);
	
	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
