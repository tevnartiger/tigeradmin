<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart Chart = new Chart();

	Chart.Type = ChartType.Combo;
	Chart.Width = 800;
	Chart.Height = 450;
	Chart.TempDirectory = "temp";
	Chart.ChartArea.Label.Text = "Click a point on this chart.";
	Chart.YAxis.Label.Text = "Numeric Axis";
	Chart.XAxis.Label.Text = "Time Axis";
	
	// This sample demonstrates getting the value of an axis when the chart is clicked.
	// This method does not use the chart as a control, it's used as a library to generate the image and displaying it on the page is handled in manually.

	// IMPORTANT NOTES:
	// 1. Chart must be generated in order to use GetValueAt_ methods.
	// 2. The chart must be in an image tag with ISMAP in the tag.
	// 3. If the chart area size changes between the chart that was clicked and the one that is generated in order to get the axis values, the results will be inaccurate.

	Chart.DefaultSeries.Type = SeriesType.Line;
	Chart.DefaultElement.Marker.Visible = false;
	Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom;

	
	// Add the random data.	
	SeriesCollection sc =  getRandomData();
	Chart.SeriesCollection.Add(sc);


	// IMPORTANT: In order for the Axis.GetValueAt_ Methods to work, the chart must be generated first.
    // Generate the chart.
	Bitmap bp = Chart.GetChartBitmap();    
        
    // Process chart clicks and create zoom chart area.
    if(Page.Request.QueryString.Count > 0)
    {
		string query = Page.Request.QueryString[0];
		
		// From the query string, get the values of the axes where clicked.
		Object xval = Chart.XAxis.GetValueAtX(query);
		Object yval = Chart.YAxis.GetValueAtY(query);
		
		clickLabel.Text = "<BR>X Axis Value = " + xval + "<BR>Y Axis Value = " + yval; 

		// Add axis markers at click positions.
		if(xval != null)
			Chart.XAxis.Markers.Add(new AxisMarker("",Color.Orange,(DateTime)xval));
		if(yval != null)
			Chart.YAxis.Markers.Add(new AxisMarker("",Color.Red,(double)yval));

				
		// Generate the chart once more to show the axis markers that were added above.
  		bp = Chart.GetChartBitmap();	
    }

	string fileName = Chart.FileManager.SaveImage(bp);
    imageLabel.Text = "<img border = 0 src=\"" +fileName+ "\" ISMAP>";  
    
}



SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
	DateTime dt = new DateTime(2006,1,1);
	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 200; b++)
		{
			Element e = new Element();
			//e.Name = "Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(20)+20;
			s.Elements.Add(e);
			e.XDateTime = dt = dt.AddDays(1);
		
		}
		SC.Add(s);
	}


	return SC;
}
		</script>
	</head>
	<body bgcolor=#ffffff>
	<a href="">
			<asp:Label ID="imageLabel" Runat="server" /></a>
			<asp:Label ID="clickLabel" Runat="server" />
	</body>
</html>
