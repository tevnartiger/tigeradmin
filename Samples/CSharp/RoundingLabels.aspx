<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;
	Chart.Size = "600x350";
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Palette = new Color[]{Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)};
		
	Chart.Title = ".netCHARTING Sample";

	
	// This sample demonstrates rounding values in a string using expressions.
	
	Chart.DefaultElement.SmartLabel.Text = "Actual: <%Value,###.###>\nRounded: <<round(%Value)>,###.###>";
	
	Chart.DefaultElement.ShowValue = true;	
	Chart.DefaultSeries.Type = SeriesType.Line;
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
        
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a.ToString();
		for(int b = 1; b < 7; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b.ToString();
			e.YValue = (double)(myR.Next(50)+(myR.Next(100)/100f));
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	return SC;
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	</head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
