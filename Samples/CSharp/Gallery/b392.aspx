<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	// Set the size
	Chart.Width = 600;
	Chart.Height = 350;
	// Set the temp directory
	Chart.TempDirectory = "temp";
	// Debug mode. ( Will show generated errors if any )
	Chart.Debug = true;
	Chart.Title = "My Chart";
	Chart.Type = ChartType.ComboHorizontal;
	Chart.DefaultSeries.Type = SeriesType.Line;
	
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
        
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + (a+1).ToString();
		for(int b = 0; b < 11; b++)
		{
			Element e = new Element();
			e.Name = "Element " + (b+1).ToString();
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	
	

	// Set Different Colors for our Series
	SC[0].PaletteName = Palette.Two;//.Color = Color.FromArgb(49,255,49);
	
	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
		</div>
	</body>
</html>
