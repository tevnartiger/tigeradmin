<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	// Set the chart type
	Chart.Type = ChartType.Combo;
	// Set the size
	Chart.Width = 600;
	Chart.Height = 350;
	// Set the temp directory
	Chart.TempDirectory = "temp";
	// Debug mode. ( Will show generated errors if any )
	Chart.Debug = true;
	Chart.Title = "My Chart\n(Time Label Automation)";
	// Axis Labels
	Chart.XAxis.Label.Text = "X Axis Label";
	Chart.YAxis.Label.Text = "Y Axis Label";
	
	Chart.DefaultSeries.Type = SeriesType.Line;
	Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	
	Chart.XAxis.Scale = Scale.Time;
	Chart.XAxis.TimeScaleLabels.RangeMode = TimeScaleLabelRangeMode.Dynamic;
	Chart.XAxis.TimeScaleLabels.Mode = TimeScaleLabelMode.Hidden;
	
	Chart.XAxis.TimeScaleLabels.YearTick.SetAllColors(Color.DarkRed);
	

	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
        
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	DateTime dt = new DateTime(2005,11,1);
	for(int a = 0; a < 4; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 3; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			dt = dt.AddDays(11);
			e.XDateTime = dt;
			e.YValue = 20+myR.Next(20);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
		</div>
	</body>
</html>
