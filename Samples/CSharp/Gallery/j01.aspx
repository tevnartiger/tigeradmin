<%@ Page Language="C#" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	Chart.Size = "800x600";
	Chart.Title = " Earth false color composite image layer";
	Chart.TempDirectory = "temp";
	Chart.TitleBox.Background.Color = Color.LightSkyBlue;
	
	// This sample demonstrates loading the earth.ecw file.
	Chart.Type = ChartType.Map;
	Chart.Mapping.MapLayerCollection.Add("../../../Images/MapFiles/earth.ecw");




}
</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>
