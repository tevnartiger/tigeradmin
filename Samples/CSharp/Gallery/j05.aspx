<%@ Page Language="C#" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	// This sample uses a pre-generated image. To run the sample live, 
	// please uncomment the code below. 
	// You will need to download the asia.shp and asia.dbf files from
 	// www.dotnetcharting.com/mapfiles.zip and put them in the /images/mapfiles directory.
	
	/*dotnetCHARTING.Chart asiaChart = new dotnetCHARTING.Chart();
	asiaChart.Type = ChartType.Map;
	asiaChart.Size = "1100x950";
	asiaChart.Title = "Asia colored by Country";
	asiaChart.TempDirectory = "temp";
	asiaChart.Debug=true;
	asiaChart.TitleBox.Position = TitleBoxPosition.Full;
	asiaChart.LegendBox.Template="%name";
	asiaChart.ChartArea.Background=new Background(Color.FromArgb(142,195,236),Color.FromArgb(63,137,200),90);
	MapLayer layer = MapDataEngine.LoadLayer("../../../Images/MapFiles/asia.shp");
	asiaChart.Mapping.DefaultShape.Label.Text = "%Country";
	asiaChart.Mapping.DefaultShape.Label.OutlineColor = Color.White;
	asiaChart.Mapping.DefaultShape.Label.Hotspot.ToolTip = "%Country";
	asiaChart.Mapping.MapLayerCollection.Add(layer);
	asiaChart.PaletteName = Palette.Five;
	int i=0;
	foreach(Shape shape in layer.Shapes)
	{
		if(i >= asiaChart.Palette.Length )
		{
			i=0;
			asiaChart.PaletteName = Palette.Four;
		}
		shape.Background.Color = asiaChart.Palette[i];
		i++;	
	}
	Controls.Add(asiaChart);*/	
}

</script>
	</head>
	<body>
		<div style="text-align:center">
			<img alt="Asia Map" src="temp/asia.png"/>
		</div>
	</body>
</html>
