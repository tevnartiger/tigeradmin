<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">


void Page_Load(Object sender,EventArgs e)
{
	// Set the title.
	Chart.Title="My Chart";

	// Set the directory where images are temporarily be stored.
	Chart.TempDirectory="temp";	
	
	// Disable the legend box
	Chart.LegendBox.Position = LegendBoxPosition.None;
	
	// Set a default gauge face background color.
	Chart.DefaultSeries.Background.Color = Color.White;
		
	// Set he chart size.
	Chart.Width = 600;
	Chart.Height = 350;	
	
	// Specify the gauges chart type
	Chart.Type = ChartType.Gauges;
	
	// Create a series with a single element
	Series s1 = new Series("series 1",new Element("",50));
	// Instantiate an axis for the created series.
	s1.YAxis = new Axis();
	// Specify the axis orientation
	s1.YAxis.OrientationAngle = -45;
	s1.YAxis.RangeAngle = 90;


	// Create a series with a single element
	Series s2 = new Series("series 2",new Element("",50));
	// Instantiate an axis for the created series.
	s2.YAxis = new Axis();
	// Specify the axis orientation
	s2.YAxis.OrientationAngle = 45;
	s2.YAxis.RangeAngle = 90;


	// Add the two series.
	Chart.SeriesCollection.Add(s1);
	Chart.SeriesCollection.Add(s2);
    
    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Gallery Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
