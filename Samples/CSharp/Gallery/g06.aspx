<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	// Set the chart type
	Chart.Type = ChartType.Combo;
	// Set the size
	Chart.Width = 600;
	Chart.Height = 350;
	// Set the temp directory
	Chart.TempDirectory = "temp";
	// Debug mode. ( Will show generated errors if any )
	Chart.Debug = true;
	Chart.Title = "My Chart";
	
	Chart.DefaultSeries.Type = SeriesType.Column;
	Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom;// = false;
	Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	Chart.DefaultElement.Marker.Visible = false;
	Chart.PaletteName = Palette.Five;
	Chart.ChartAreaSpacing = 8;
	Chart.LegendBox.Template = "%Icon%Name";
	
	// Modify the x axis labels.
	Chart.XAxis.TimeScaleLabels.Mode = TimeScaleLabelMode.Smart;
	Chart.XAxis.TimeScaleLabels.DayFormatString = "o";
	Chart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Month);
	Chart.XAxis.TimeScaleLabels.MonthFormatString = "MMM";
	
	
	// Setup the axes.
	Chart.YAxis.Label.Text = "Price";
	Chart.YAxis.FormatString = "Currency";
	Chart.YAxis.Scale = Scale.Range;

	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getPriceData();
	mySC[0].Type = SeriesTypeFinancial.CandleStick;
	SeriesCollection mySCV = getVolumeData();
	
	// Add volume chart area.
	ChartArea volumeArea = new ChartArea();
	volumeArea.YAxis.Label.Text = "Volume";
	volumeArea.SeriesCollection.Add(mySCV);
	volumeArea.HeightPercentage = 25;
	Chart.ExtraChartAreas.Add(volumeArea);
	
	
	// Add the price data.
	Chart.SeriesCollection.Add(mySC);
	
	
	// Create a new Aroon chart area
	ChartArea aroonChartArea = new ChartArea ();
	aroonChartArea.HeightPercentage = 32;
	aroonChartArea.YAxis.Label.Text = "Aroon\nIndicators";
	Chart.ExtraChartAreas.Add (aroonChartArea);

	// AroonUpOverPeriod Calculation
	Series aroonUpOverPeriod = FinancialEngine.AroonUpOverPeriod(mySC[0],5);
	aroonUpOverPeriod.Name = " Aroon Up";
	aroonUpOverPeriod.Type = SeriesType.Spline;
	aroonChartArea.SeriesCollection.Add (aroonUpOverPeriod);

	// AroonDownOverPeriod Calculation
	Series aroonDownOverPeriod = FinancialEngine.AroonDownOverPeriod(mySC[0],5);
	aroonDownOverPeriod.Name = " Aroon Down";
	aroonDownOverPeriod.Type = SeriesType.Spline;
	aroonChartArea.SeriesCollection.Add (aroonDownOverPeriod);
        
        
        
}

	private SeriesCollection getPriceData()
		{
			SeriesCollection SC = new SeriesCollection();
			Random myR = new Random();
			for(int i = 0; i < 1; i++)
			{
				Series s = new Series();
				s.Name = "Price";			
				double startPrice = 50;

				DateTime startDT = new DateTime(2000,1,1);
				for(int b = 0; b < 75; b++)
				{
					Element e = new Element();
					e.XDateTime = startDT;
					startDT = startDT.AddDays(1);
					
					if(myR.Next(10) > 5)
						startPrice += myR.Next(5);
					else
						startPrice -= myR.Next(3);
					
					e.Close = startPrice;
					
					if(myR.Next(10) > 5)
						e.Open = startPrice + myR.Next(6);
					else
						e.Open = startPrice - myR.Next(6);
					
					if(e.Open > e.Close)
					{
						e.High = e.Open + myR.Next(6);
						e.Low = e.Close - myR.Next(6);
					}
					else
					{
						e.High = e.Close + myR.Next(6);
						e.Low = e.Open - myR.Next(6);
					}
					s.Elements.Add(e);
				}
				SC.Add(s);
			}
			return(SC);
		}
		
		private SeriesCollection getVolumeData()
		{
			SeriesCollection SC = new SeriesCollection();
			Random myR = new Random();
			for(int i = 0; i < 1; i++)
			{
				Series s = new Series();
				s.Name = "Volume";			
				//double startPrice = 50;

				DateTime startDT = new DateTime(2000,1,1);
				for(int b = 0; b < 75; b++)
				{
					Element e = new Element();
					e.XDateTime = startDT;
					startDT = startDT.AddDays(1);
					e.YValue = myR.Next(100);
					s.Elements.Add(e);
				}
				SC.Add(s);
			}
			return(SC);
		}
		</script>
	</head>
	<body>
		<div style="text-align:center">
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
		</div>
	</body>
</html>
