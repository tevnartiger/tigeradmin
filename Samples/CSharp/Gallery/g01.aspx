<%@ Page Language="C#" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">

		private SeriesCollection getData()
		{
			SeriesCollection SC = new SeriesCollection();
				Random myR = new Random();
			for(int i = 0; i < 1; i++)
			{


				Series s = new Series();
				s.Name = "X Company";			
				double startPrice = 50;

				DateTime startDT = new DateTime(2000,1,1);
				for(int b = 0; b < 75; b++)
				{
					Element e = new Element();
					e.XDateTime = startDT;
					startDT = startDT.AddDays(1);
					
					if(myR.Next(10) > 5)
						startPrice += myR.Next(5);
					else
						startPrice -= myR.Next(3);
					
					e.Close = startPrice;

					e.Volume = myR.Next(100);
					
					if(myR.Next(10) > 5)
						e.Open = startPrice + myR.Next(6);
					else
						e.Open = startPrice - myR.Next(6);
					
					if(e.Open > e.Close)
					{
						e.High = e.Open + myR.Next(6);
						e.Low = e.Close - myR.Next(6);
					}
					else
					{
						e.High = e.Close + myR.Next(6);
						e.Low = e.Open - myR.Next(6);
					}



					s.Elements.Add(e);

				}

				SC.Add(s);

			}
			return(SC);


		}


void Page_Load(Object sender,EventArgs e)
{
	// Set the title.
	Chart.Title="My Chart";

	// Set 3D
	Chart.Use3D = true;

	// Set the chart Type
	Chart.Type = ChartType.Financial;

	// Label the chart areas
	Chart.ChartArea.Label.Text = "Stock Price for X Company";
	Chart.ChartArea.Label.Text = "Volume";

	// Set the financial chart type
	Chart.DefaultSeries.Type = SeriesTypeFinancial.CandleStick;

	// Set the legend template
	Chart.LegendBox.Template = "IconName";		


	// Set the directory where the images will be stored.
	Chart.TempDirectory="temp";

	// Set the format
	Chart.XAxis.FormatString = "d";

	// Set the time padding for the x axis.
	Chart.XAxis.TimePadding = new TimeSpan(5,0,0,0);

	// Set he chart size.
	Chart.Width = 600;
	Chart.Height = 350;

	// Add the random data.
	Chart.SeriesCollection.Add(getData());
    
    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Gallery Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
