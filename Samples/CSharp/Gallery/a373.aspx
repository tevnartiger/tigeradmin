<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Size = "600x350";
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = ".netCHARTING Sample";
	
	Chart.DefaultSeries.Type = SeriesType.AreaSpline;
	
	
	// This sample demonstrates ....
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
        
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 2; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a.ToString();
		for(int b = 1; b < 10; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b.ToString();
			e.YValue = 50 + myR.Next(50);
			e.YValueStart = e.YValue - 15 - myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	
	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
		</div>
	</body>
</html>
