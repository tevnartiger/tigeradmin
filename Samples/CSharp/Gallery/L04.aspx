<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	// This sample demonstrates shaded BubbleShape series in 3D.

	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Palette = new Color[]{Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)};
	
	Chart.Type = ChartType.Combo;
	Chart.Size = "600x350";
	Chart.Title = ".netCHARTING Sample";
	
	Chart.DefaultSeries.Type = SeriesType.BubbleShape;
	Chart.ShadingEffectMode = ShadingEffectMode.One;
	Chart.DefaultElement.ShapeType = ShapeType.None;
	Chart.Use3D = true;
	Chart.Depth = 5;
	Chart.YAxis.ClearValues = true;
	Chart.XAxis.ClearValues = true;
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Help File > Getting Started > Data Tutorials
	// - DataEngine Class in the help file	
	// - Sample: features/DataEngine.aspx
	
	SeriesCollection mySC = getRandomData();
	Chart.MaximumBubbleSize = 30;
	Chart.LegendBox.Visible = false;

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
	mySC[0].PaletteName = Palette.Three;
	
	string[] shapeNames = new string[] {"Triangle", 
		"Cube", 
		"Pentagon", 
		"Hexagon", 
		"Heptagon",
		"Octagon",
		"Nonagon", 
		"Decagon",
		"FivePointStar",
		"SixPointStar",
		"SevenPointStar",
		"EightPointStar", 
		"ArrowUp",
		"ArrowDown",
		"ArrowLeft",
		"ArrowRight",
		"Refresh",
		"Heart",
		"Ribbon",
		"Zap",
		"Trophy",
		"Chat",
		"Peace",
		"ThumbsUp",
		"ThumbsDown",
		"PointLeft",
		"PointRight",
		"PointUp",
		"PointDown",
		"Hand",
		"Flag",
		"Plane",
		"Drop",
		"Flake",
		"Hourglass",
		"Mouse",
		"Folder",
		"Candle",
		"Quote",
		"Bell",
		"Clover",
		"X",
		"Check" };
		
		mySC[0][0].Name = "";
	for(int i = 1; i < mySC[0].Elements.Count-1; i++)
	{
		mySC[0][i].ShapeType = (ShapeType)ShapeType.Parse(typeof(ShapeType),shapeNames[i-1]);
		mySC[0][i].Name = shapeNames[i-1];
	}

        
}

SeriesCollection getRandomData()
{
	Random myR = new Random(1);
	SeriesCollection SC = new SeriesCollection();
	for(int a = 1; a < 2; a++)
	{
		Series s = new Series("Series " + a.ToString());
		for(int b = 1; b < 46; b++)
		{
			Element e = new Element("Element " + b.ToString());
			e.YValue = b%5;
			s.Elements.Add(e);
			e.BubbleSize = 5;
		}
		SC.Add(s);
	}
	return SC;
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
