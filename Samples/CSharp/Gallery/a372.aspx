<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 10; b++)
		{
			Element e = new Element();
			e.Name = "E " + b;
			e.YValueStart = 20 + myR.Next(10);
			e.YValue = e.YValueStart + 10 + b + myR.Next(10);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	//SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	//SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	//SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);
	return SC;
}


void Page_Load(Object sender,EventArgs e)
{
	// Set the title.
	Chart.Title="My Chart";

	// Specify a hatch style palette for all series.
	//Chart.HatchStylePaletteName = Palette.One;
	
	// Set a transparency
	Chart.DefaultSeries.DefaultElement.Transparency = 20;
	
	// Set 3d mode
	Chart.Use3D = true;
	
	// Specify a series type.
	Chart.DefaultSeries.Type = SeriesType.AreaLine;
	
	// Set a defaut hatch color.
	//Chart.DefaultSeries.DefaultElement.HatchColor = Color.FromArgb(120,Color.Gray);

	// Set the x axis label
	Chart.XAxis.Label.Text="X Axis Label";

	// Set the y axis label
	Chart.YAxis.Label.Text="Y Axis Label";

	// Set the directory where the images will be stored.
	Chart.TempDirectory="temp";

	// Set the bar shading effect
	Chart.ShadingEffectMode = ShadingEffectMode.Three;
	
	// Set he chart size.
	Chart.Width = 600;
	Chart.Height = 350;

	// Add the random data.
	Chart.SeriesCollection.Add(getRandomData());
    
    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Gallery Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
