<%@ Page Language="C#" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">
// This sample demonstrates how to iterate a layer of shape files.
void Page_Load(Object sender,EventArgs e)
{
	Chart.Type = ChartType.Map;
	Chart.Size = "900x550";
	Chart.Title = " USA states with inset Alaska and Hawaii";
	Chart.TempDirectory = "temp";
	Chart.Debug=true;
	Chart.TitleBox.Position = TitleBoxPosition.Full;
        Chart.ChartArea.Background = new Background(Color.FromArgb(142,195,236),Color.FromArgb(63,137,200),90);
	MapLayer layer = MapDataEngine.LoadLayer("../../../Images/MapFiles/primusa.shp");
	
	Chart.Mapping.DefaultShape.Label.Text = "%State_Abbr";
	Chart.Mapping.DefaultShape.Label.OutlineColor = Color.White;
	Chart.Mapping.DefaultShape.Label.Hotspot.ToolTip = "%State_NAME";
	
	Chart.Mapping.MapLayerCollection.Add(layer);
	
	//Set different colors to each state
	Chart.PaletteName = Palette.Three;
		int i=0;
	foreach(Shape shape in layer.Shapes)
	{
		if(i >= Chart.Palette.Length )
			i=0;
		shape.Background.Color = Chart.Palette[i];
		i++;	
	}
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>
