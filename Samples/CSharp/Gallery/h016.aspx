<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
    // This sample demonstrates gauge indicator lights with shading effect mode two.

	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Palette = new Color[]{Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)};
	
	Chart.Type = ChartType.Gauges;
    Chart.DefaultSeries.GaugeType = GaugeType.IndicatorLight;
	Chart.Size = "160x280";

   Chart.ShadingEffectMode = ShadingEffectMode.Two;
   Chart.Margin = "0";
    Chart.DefaultElement.Marker.Size = 35;
    Chart.LegendBox.Visible = false;
    Chart.ChartArea.ClearColors();
    Chart.DefaultSeries.GaugeBorderShape = GaugeBorderShape.UseBox;
    Chart.DefaultSeries.GaugeBorderBox.DefaultCorner = BoxCorner.Round;
    Chart.DefaultSeries.GaugeBorderBox.Background.Color = Color.FromArgb(20,Color.Blue);
    //Chart.DefaultSeries.Background.Color = Color.Blue;
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Help File > Getting Started > Data Tutorials
	// - DataEngine Class in the help file	
	// - Sample: features/DataEngine.aspx

    // Create smart palette.
    SmartPalette sp = new SmartPalette();

    // Create smart color with the range and color to use.
    SmartColor sc = new SmartColor(Color.Green, new ScaleRange(0, 30));
    SmartColor sc2 = new SmartColor(Color.Yellow, new ScaleRange(31, 60));
    SmartColor sc3 = new SmartColor(Color.Red, new ScaleRange(61, 100));

   // sc.LegendEntry.Visible = false;

    // Add the color to the palette, and the palette to the chart.
    sp.Add("*", sc);
    sp.Add("*", sc2);
    sp.Add("*", sc3);
    Chart.SmartPalette = sp;


    Chart.DefaultElement.SmartLabel.Color = Color.Black;

    //Chart.DefaultElement.SmartLabel.Alignment = LabelAlignment.Center;
    Chart.DefaultElement.SmartLabel.Font = new Font("Arial", 10, FontStyle.Bold);
    Chart.XAxis.Orientation = dotnetCHARTING.Orientation.Top;
	
	SeriesCollection mySC = getRandomData();

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);

        
}

SeriesCollection getRandomData()
{
	Random myR = new Random(1);
	SeriesCollection SC = new SeriesCollection();
	for(int a = 1; a < 2; a++)
	{
		Series s = new Series("");
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element("Element " + b.ToString());
			e.YValue = myR.Next(100);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
