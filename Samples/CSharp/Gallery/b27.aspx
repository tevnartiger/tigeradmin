<%@ Page Language="C#" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 4; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			e.YValue = -25 + myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}


void Page_Load(Object sender,EventArgs e)
{
	// Set the title.
	Chart.Title="My Chart";

	// Set the chart Type
	Chart.Type = ChartType.ComboHorizontal;

	// Set the default series type
	Chart.DefaultSeries.Type = SeriesType.Cylinder;


	// Turn 3D off.
	Chart.Use3D = false;

	// Set a default transparency
	Chart.DefaultSeries.DefaultElement.Transparency = 20;

	// Set the x axis scale
	Chart.XAxis.Scale = Scale.Stacked;

	// Set the x axis label
	Chart.XAxis.Label.Text="X Axis Label";

	// Set the y axis label
	Chart.YAxis.Label.Text="Y Axis Label";


	// Set the directory where the images will be stored.
	Chart.TempDirectory="temp";

	// Set the chart size.
	Chart.Width = 600;
	Chart.Height = 350;

	// Add the random data.
	Chart.SeriesCollection.Add(getRandomData());
    
    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Gallery Sample (Time Gantt Chart in 2D)</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
