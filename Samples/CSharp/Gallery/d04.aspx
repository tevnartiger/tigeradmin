<%@ Page Language="C#" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 2; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 7; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	//SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	//SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);
	return SC;
}


void Page_Load(Object sender,EventArgs e)
{
	// Set the title.
	Chart.Title="My Chart";

	// Set the chart Type
	Chart.Type = ChartType.Radars;		
	
	// Set 3D
	Chart.Use3D = true;

	// Set default transparency
	Chart.DefaultSeries.DefaultElement.Transparency = 35;

	// Set the radar labeling mode
	Chart.RadarLabelMode = RadarLabelMode.Angled;

	// Set the directory where the images will be stored.
	Chart.TempDirectory="temp";

	// Set he chart size.
	Chart.Width = 600;
	Chart.Height = 350;

	// Add the random data.
	Chart.SeriesCollection.Add(getRandomData());
    
    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Gallery Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
