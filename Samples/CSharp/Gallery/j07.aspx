<%@ Page Language="C#" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">
// This sample demonstrates how to iterate a layer of shape files.
void Page_Load(Object sender,EventArgs e)
{
	Chart.Type = ChartType.Map;
	Chart.Size = "1200x700";
	Chart.Title = "Election 2004 results by state";
	Chart.TempDirectory = "temp";
	Chart.Debug=true;
	Chart.TitleBox.Position = TitleBoxPosition.Full;
        Chart.ChartArea.Background =new Background(Color.FromArgb(142,195,236),Color.FromArgb(63,137,200),90);
	MapLayer layer = MapDataEngine.LoadLayer("../../../Images/MapFiles/primusa.shp");
	layer.ImportData(CreateDataTable(),"STATE_ABBR", "Code");
	Chart.Mapping.DefaultShape.Background.Color  = Color.Red;
	Chart.Mapping.DefaultShape.Label.Text = "%STATE_ABBR\n%Seats2004";
	Chart.Mapping.DefaultShape.Label.OutlineColor = Color.White;
	Chart.Mapping.DefaultShape.Label.Font = new Font("Arial", 9);
	Chart.Mapping.DefaultShape.Label.Hotspot.ToolTip = "%STATE_NAME";
	
	Chart.Mapping.MapLayerCollection.Add(layer);
	
	//Set different colors to each state
	foreach(Shape shape in layer.Shapes)
	{
		string code =(string) shape["STATE_ABBR"];	
		if(code=="WA"||code=="OR"||code=="CA"||code=="MN"||code=="WI"||code=="MI"||code=="PA"||
			code=="IL"||code=="NY"||code=="MA"||code=="NH"||code=="MA"||code=="RI"||code=="NJ"||
			code=="ME"||code=="DL"||code=="MD"||code=="DE"||code=="HI"||code=="VT"||code=="ME"||code=="CT")
		
		shape.Background.Color = Color.Blue;
		
	}
}
private DataTable CreateDataTable()
{
	DataTable dt = new DataTable();
	string connString = "Provider=Microsoft.Jet.OLEDB.4.0;user id=admin;password=;data source=" + 	Server.MapPath("../../../database/chartsample.mdb");
	OleDbConnection conn = new OleDbConnection(connString);
	OleDbDataAdapter adapter = new OleDbDataAdapter();
	string selectQuery = @"SELECT [Code],[Seats2004] FROM Locations"; 
	adapter.SelectCommand = new OleDbCommand(selectQuery, conn);
	adapter.Fill(dt);
	return dt;
}


		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>
