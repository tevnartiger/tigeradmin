<%@ Page Language="C#" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>


<script runat="server">

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 4; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 5; b++)
		{
			Element e = new Element();
			e.Name = "E " + b;
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	// Set 2 series to bars and 2 to lines.
	SC[0].Type = SeriesType.Column;
	SC[1].Type = SeriesType.Column;
	SC[2].Type = SeriesType.Line;
	SC[3].Type = SeriesType.Line;
	
	return SC;
}


void Page_Load(Object sender,EventArgs e)
{
	// Set the title.
	Chart.Title="My Chart"; Chart.Type = ChartType.ComboHorizontal;

	// Set the Depth
	Chart.Depth = 15;

	// Set 3D
	Chart.Use3D = false;

	// set the x axis clustering
	Chart.XAxis.ClusterColumns = false;

	// Set a default transparency
	Chart.DefaultSeries.DefaultElement.Transparency = 20;

	// Set a default line Width
	Chart.DefaultSeries.Line.Width = 3;


	// Set the y Axis Scale
	Chart.YAxis.Scale = Scale.Normal;

	// Set the x axis label
	Chart.XAxis.Label.Text="X Axis Label";

	// Set the y axis label
	Chart.YAxis.Label.Text="Y Axis Label";

	// Set the directory where the images will be stored.
	Chart.TempDirectory="temp";


	// Set he chart size.
	Chart.Width = 600;
	Chart.Height = 350;

	// Add the random data.
	Chart.SeriesCollection.Add(getRandomData());
    
    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Gallery Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
