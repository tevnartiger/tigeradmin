<%@ Page Language="C#" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    void Page_Load(Object sender, EventArgs e)
    {
        // This sample demonstrates digital readout gauges.

        Chart.TempDirectory = "temp";
        Chart.Debug = true;
        Chart.PaletteName = Palette.DarkRainbow;

        Chart.Type = ChartType.Gauges;
        Chart.DefaultSeries.GaugeType = GaugeType.DigitalReadout;
        Chart.DefaultSeries.GaugeBorderBox.Background.Color = Color.White;
        Chart.Size = "600x350";
        Chart.Title = ".netCHARTING Sample";


        // *DYNAMIC DATA NOTE* 
        // This sample uses random data to populate the chart. To populate 
        // a chart with database data see the following resources:
        // - Help File > Getting Started > Data Tutorials
        // - DataEngine Class in the help file	
        // - Sample: features/DataEngine.aspx

        SeriesCollection mySC = getRandomData();

        // Add the random data.
        Chart.SeriesCollection.Add(mySC);
        mySC[0].DefaultElement.SmartLabel.Font = new Font("Arial", 18);
        mySC[1].DefaultElement.SmartLabel.Font = new Font("Arial", 18,FontStyle.Italic);
        mySC[2].DefaultElement.SmartLabel.Font = new Font("Arial", 18,FontStyle.Bold);
        mySC[3].DefaultElement.SmartLabel.Font = new Font("Arial", 18, FontStyle.Bold | FontStyle.Italic);
        mySC[0].Name = "Normal";
        mySC[1].Name = "Italic";
        mySC[2].Name = "Bold";
        mySC[3].Name = "Bold Italic";


    }

    SeriesCollection getRandomData()
    {
        Random myR = new Random(1);
        SeriesCollection SC = new SeriesCollection();
        int a = 0;
        int b = 0;
        for (a = 1; a < 5; a++)
        {
            Series s = new Series("Series " + a.ToString());
            for (b = 1; b < 5; b++)
            {
                Element e = new Element("Element " + b.ToString());
                e.YValue = myR.Next(1150);
                s.Elements.Add(e);
            }
            SC.Add(s);
        }
        return SC;
    }

    SeriesCollection getLiveData()
    {
        DataEngine de = new DataEngine("ConnectionString goes here");
        de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ....";
        return de.GetSeries();
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <dotnet:Chart ID="Chart" runat="server" />
    </div>
</body>
</html>
