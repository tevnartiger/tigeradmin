<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	// This sample demonstrates vertical thermometer gauges using ShadingEffect.

	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Palette = new Color[]{Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)};

	// TitleBox Customization
	Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	Chart.TitleBox.Background = new Background(Color.White,Color.LightGray,90);
	Chart.TitleBox.Label.OutlineColor = Color.LightGray;
	Chart.TitleBox.CornerTopLeft = BoxCorner.Round;
	Chart.TitleBox.CornerTopRight = BoxCorner.Round;
	Chart.ChartArea.Background = new Background(Color.AliceBlue,Color.GhostWhite,90);


	Chart.Type = ChartType.Gauges;
	Chart.DefaultSeries.GaugeType = GaugeType.Vertical;
	Chart.DefaultSeries.GaugeLinearStyle = GaugeLinearStyle.Thermometer;
	Chart.Size = "600x350";
	Chart.Title = ".netCHARTING Sample";
	Chart.ShadingEffectMode = ShadingEffectMode.Four;

	Chart.DefaultSeries.Background.Color = Color.White;
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Help File > Getting Started > Data Tutorials
	// - DataEngine Class in the help file	
	// - Sample: features/DataEngine.aspx


	SeriesCollection mySC = getRandomData();

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);

        
}


SeriesCollection getRandomData()
{
	Random myR = new Random();
	SeriesCollection SC = new SeriesCollection();
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series("Series " + a.ToString());
		
			Element e = new Element("Element");
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
	
		SC.Add(s);
	}
	return SC;
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
