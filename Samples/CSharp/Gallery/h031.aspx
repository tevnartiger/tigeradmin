<%@ Page Language="C#" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    void Page_Load(Object sender, EventArgs e)
    {
        // This sample demonstrates a vertical linear gauge using shading effect Four.
        Chart.Size = "110x220";

        Chart.TempDirectory = "temp";
        Chart.Debug = true;
        Chart.Palette = new Color[] {   Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) };
        Chart.LegendBox.Visible = false;

        Chart.Type = ChartType.Gauges;
        Chart.DefaultSeries.GaugeType = GaugeType.Vertical;
        Chart.ShadingEffectMode = ShadingEffectMode.Four;

        Chart.ChartArea.Padding = 0;
        Chart.Margin = "0";
        Chart.ChartArea.ClearColors();
        Chart.DefaultElement.ShowValue = true;

        Chart.DefaultSeries.GaugeBorderBox.DefaultCorner = BoxCorner.Round;
        Chart.DefaultSeries.GaugeBorderShape = GaugeBorderShape.UseBox;

        // *DYNAMIC DATA NOTE* 
        // This sample uses random data to populate the chart. To populate 
        // a chart with database data see the following resources:
        // - Help File > Getting Started > Data Tutorials
        // - DataEngine Class in the help file	
        // - Sample: features/DataEngine.aspx

        SeriesCollection mySC = getRandomData();

        // Add the random data.
        Chart.SeriesCollection.Add(mySC);

    }

    SeriesCollection getRandomData()
    {
        Random myR = new Random();
        SeriesCollection SC = new SeriesCollection();
        for (int a = 1; a < 2; a++)
        {
            Series s = new Series("Traffic");
            for (int b = 1; b < 2; b++)
            {
                Element e = new Element("Element " + b.ToString());
                e.YValue = myR.Next(50);
                s.Elements.Add(e);
            }
            SC.Add(s);
        }
        return SC;
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <dotnet:Chart ID="Chart" runat="server" />
    </div>
</body>
</html>
