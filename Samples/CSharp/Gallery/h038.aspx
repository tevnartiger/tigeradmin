<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	// This sample demonstrates multiple horizontal thermometers.

	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Palette = new Color[]{Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)};
	
	Chart.Type = ChartType.Gauges;
	Chart.Size = "350x250";
	//Chart.Title = ".netCHARTING Sample";
    Chart.ShadingEffectMode = ShadingEffectMode.Three;
    Chart.DefaultSeries.GaugeLinearStyle = GaugeLinearStyle.Thermometer;
    Chart.ChartArea.Padding = 3;

    Chart.ChartArea.Background.Color = Color.Empty;
    Chart.ChartArea.Line.Color = Color.Empty;
    Chart.ChartArea.Shadow.Color = Color.Empty;
    


    Chart.DefaultSeries.GaugeBorderBox.DefaultCorner = BoxCorner.Round;
    Chart.DefaultSeries.GaugeBorderShape = GaugeBorderShape.UseBox;
    Chart.DefaultSeries.Type = SeriesType.Marker;
    Chart.DefaultElement.Marker = new ElementMarker("pointerMarker.gif");
    Chart.DefaultElement.Marker.DynamicImageColor =  Color.FromArgb(6,0,127);

    Chart.YAxis.Orientation = dotnetCHARTING.Orientation.Top;
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Help File > Getting Started > Data Tutorials
	// - DataEngine Class in the help file	
	// - Sample: features/DataEngine.aspx
	
	SeriesCollection mySC = getRandomData();
   // Chart.DefaultElement.ShowValue = true;
   // Chart.DefaultElement.SmartLabel.Alignment = LabelAlignment.Bottom;
   // Chart.DefaultElement.SmartLabel.Text = "%Name - (%YValue)";
    Chart.DefaultSeries.GaugeType = GaugeType.Horizontal;

    Chart.LegendBox.Visible = false;
	// Add the random data.
	Chart.SeriesCollection.Add(mySC);

        
}

SeriesCollection getRandomData()
{
	Random myR = new Random(1);
	SeriesCollection SC = new SeriesCollection();
	for(int a = 1; a < 3; a++)
	{
		Series s = new Series("");
		for(int b = 1; b < 2; b++)
		{
			Element e = new Element("Element " + b.ToString());
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
