<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component" %>
		
<script runat="server">

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();

		Series s = new Series();
		Element e = new Element();
		
		e.YValue = 100;
		e.Complete = myR.Next(100);
		
		s.Elements.Add(e);

		SC.Add(s);


	return SC;
}


void Page_Load(Object sender,EventArgs e)
{

	Chart.Width = 400;
	Chart.Height = 50;
	Chart.TempDirectory = "temp";
	Chart.Debug= true;


	// Set the shading effect for a nicer visual effect.
	Chart.ShadingEffectMode = ShadingEffectMode.Two;
	

	
	// Set the horizontal combo chart type.	
	Chart.Type = ChartType.ComboHorizontal;
	
	// Hide the legend box.
	Chart.LegendBox.Position = LegendBoxPosition.None;
	
	// Set the x axis scale as full stacked so that our single element can be stretched across the chart.
	Chart.XAxis.Scale = Scale.FullStacked;
	
	// Hide the x axis.
	Chart.XAxis.Clear();
	
	// Hide the y axis.
	Chart.YAxis.Clear();
	
	// Set the y axis spacing percentage so our bar can be stretch across the whole chart area.
	Chart.YAxis.SpacingPercentage = 1;

	// Hide the chart area beneath the bar.
	Chart.ChartArea.ClearColors();
	
	
	// Set a color for our element.
	Chart.DefaultSeries.DefaultElement.Color = Color.FromArgb(255,99,49);
	
	// Set the template for the element label.
	Chart.DefaultSeries.DefaultElement.SmartLabel.Text = "%Complete% Completed";
	
	// Allow the element labels to be visible.
	Chart.DefaultSeries.DefaultElement.ShowValue = true;

	// Set the color and font for the element label.
	Chart.DefaultSeries.DefaultElement.SmartLabel.Color = Color.White;
	Chart.DefaultSeries.DefaultElement.SmartLabel.Font = new Font("arial black",10);

	// Set a hatch style for the complete part of our stretched element.
	Chart.GanttCompleteHatchStyle = HatchStyle.LargeConfetti;
	
	// Get random data.
	SeriesCollection sc = getRandomData();
	

	// TIP: Here we'll demonstrate how we can set the alignment of our label dynamically.
	// If the complete hatch fill is about to touch the label we will set the alignment
	// so it is displayed on the right side of the bar.
	
	if(sc[0].Elements[0].Complete > 33)// If hatch is about to touch the label.
		Chart.DefaultSeries.DefaultElement.SmartLabel.Alignment = LabelAlignment.Top;// Top of the bar is the same as right.
	else // Otherwise we keep it centered.
		Chart.DefaultSeries.DefaultElement.SmartLabel.Alignment = LabelAlignment.Center;	


	// Add the random data.
	Chart.SeriesCollection.Add(sc);
    
    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Progress Bar</title>

	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" />
		</div>
	</body>
</html>
