<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Gallery Sample (Time Gantt Chart in 2D)</title>

    <script runat="server">

        void Page_Load(Object sender, EventArgs e)
        {

            // This sample demonstrates how 2D bars can overlap.

            Chart.Type = ChartType.ComboHorizontal;
            Chart.Width = 600;
            Chart.Height = 350;
            Chart.Debug = true;
            Chart.TempDirectory = "temp";
            Chart.Title = "Advanced Gantt Chart";

            // Applying the following settings is a trick used to uncluster columns, because it is only supported with 3D, so setting the depth to 0 brings it back to a 2D view.
            Chart.Use3D = true;
            Chart.Depth = 0;
            Chart.YAxis.ClusterColumns = false;



            // *DYNAMIC DATA NOTE* 
            // This sample uses static data to populate the chart. To populate 
            // a chart with database data see the following resources:
            // - Classic samples folder
            // - Help File > Data Tutorials
            // - Sample: features/DataEngine.aspx
            SeriesCollection sc = getData();


            // Add the random data.
            Chart.SeriesCollection.Add(sc);


        }

        SeriesCollection getData()
        {

            // Create two series one for Jack and Jenny. Notice Jack only has two tasks while Jenny has three.

            SeriesCollection SC = new SeriesCollection();

            Series s1 = new Series("Jack");
            Series s2 = new Series("Jenny");

            Element e1 = new Element();
            e1.Name = "Task 1";
            e1.YDateTimeStart = new DateTime(2000, 1, 1);
            e1.YDateTime = new DateTime(2000, 1, 5);
            e1.Complete = 100;

            Element e2 = new Element();
            e2.Name = "Task 2";
            e2.YDateTimeStart = new DateTime(2000, 1, 5);
            e2.YDateTime = new DateTime(2000, 1, 10);
            e2.Complete = 20;

            s1.Elements.Add(e1);
            s1.Elements.Add(e2);

            Element e3 = new Element();
            e3.Name = "Task 1";
            e3.YDateTimeStart = new DateTime(2000, 1, 5);
            e3.YDateTime = new DateTime(2000, 1, 10);
            e3.Complete = 25;

            Element e4 = new Element();
            e4.Name = "Task 2";
            e4.YDateTimeStart = new DateTime(2000, 1, 10);
            e4.YDateTime = new DateTime(2000, 1, 15);

            Element e5 = new Element();
            e5.Name = "Task 2";
            e5.YDateTimeStart = new DateTime(2000, 1, 15);
            e5.YDateTime = new DateTime(2000, 1, 20);


            s2.Elements.Add(e3);
            s2.Elements.Add(e4);
            s2.Elements.Add(e5);


            SC.Add(s1);
            SC.Add(s2);


            return SC;
        }
    </script>

</head>
<body>
    <div style="text-align: center">
        <dotnet:Chart ID="Chart" runat="server">
        </dotnet:Chart>
    </div>
</body>
</html>
