<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<HTML>
	<HEAD>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	// This sample demonstrates the element grouping technology introduced in version 4.1 which uses intelligent pattern recognition to determine the order of elements when it is not clearly provided.
	// FullScreen

	SetChart(Chart);
	SetChart(Chart1);
	Chart1.SeriesCollection.SmartGrouping = false;
        
}

void SetChart(Chart c)
{
	c.TempDirectory = "temp";
	c.Debug = true;
	c.Palette = new Color[]{Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)};
	
	c.Type = ChartType.Combo;
	c.Size = "600x350";
	c.Title = ".netCHARTING Sample";
	
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Help File > Getting Started > Data Tutorials
	// - DataEngine Class in the help file	
	// - Sample: features/DataEngine.aspx
	
	SeriesCollection mySC = getRandomData();
	mySC[0][0] = new Element("C",10);
	mySC[0][1] = new Element("D",10);
	mySC[0][2] = new Element("E",10);
	mySC[0][3] = new Element("F",10);
	
	mySC[1][0] = new Element("E",10);
	mySC[1][1] = new Element("F",10);
	mySC[1][2] = new Element("G",10);
	mySC[1][3] = new Element("H",10);

	mySC[2][0] = new Element("A",10);
	mySC[2][1] = new Element("B",10);
	mySC[2][2] = new Element("C",10);
	mySC[2][3] = new Element("D",10);



	// Add the random data.
	c.SeriesCollection.Add(mySC);
}

SeriesCollection getRandomData()
{
	Random myR = new Random(1);
	SeriesCollection SC = new SeriesCollection();
	for(int a = 1; a < 4; a++)
	{
		Series s = new Series("Series " + a.ToString());
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element();
			e.YValue = double.NaN;
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}

		</script>
	</HEAD>
	<body>
		<div align="center">
			<table border="1" style="BORDER-COLLAPSE: collapse" width="70%" id="table7">
				<tr>
					<td><font face="Arial" size="2">The elements will be provided for the chart in a 
							segmented order.</font><p><font face="Arial" size="2">3 Series with element names 
								scattered throughout.</font></p>
						<table border="1" style="BORDER-COLLAPSE: collapse" width="43%" id="table8">
							<tr>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">C</font></td>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">D</font></td>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">E</font></td>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">F</font></td>
							</tr>
							<tr>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">E</font></td>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">F</font></td>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">G</font></td>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">H</font></td>
							</tr>
							<tr>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">A</font></td>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">B</font></td>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">C</font></td>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">D</font></td>
							</tr>
						</table>
						<p><font face="Arial" size="2"><FONT face="Arial" size="2"><STRONG>SeriesCollection.SmartGrouping 
										= true</STRONG> (Default)</FONT></font></p>
						<P><FONT face="Arial" size="2">Intended order:</FONT></P>
						<table border="1" style="BORDER-COLLAPSE: collapse" width="44%" id="table9">
							<tr>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">A</font></td>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">B</font></td>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">C</font></td>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">D</font></td>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">E</font></td>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">F</font></td>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">G</font></td>
								<td bgcolor="#f7f7f7"><font face="Arial" size="2">H</font></td>
							</tr>
						</table>
						<p><font face="Arial" size="2">.netCHARTING Will analyze this list and determine what 
								the true intended order should be. This does not employ sorting, it looks for 
								patterns in element orders of different series to determine the original order.</font></p>
						<P><FONT face="Arial" size="2"><STRONG>SeriesCollection.SmartGrouping = false</STRONG></FONT></P>
						<p>
						When false, the element order will be based on the orders in which series 
						elements are provided.
						<P>&nbsp;</P>
					</td>
				</tr>
			</table>
		</div>
		<div align="center">
			<div align="center">&nbsp;</div>
			<div align="center">
				<dotnet:Chart id="Chart" runat="server">
				</dotnet:Chart></div>
				<dotnet:Chart id="Chart1" runat="server">
				</dotnet:Chart>
			<div></div>
		</div>
	</body>
</HTML>
