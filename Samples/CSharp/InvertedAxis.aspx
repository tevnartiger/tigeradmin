<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart2.TempDirectory = "temp";
	Chart2.Debug = true;
	Chart3.TempDirectory = "temp";
	Chart3.Debug = true;
	
	// This sample will demonstratates how to invert the axis scales. The different chart will show 
	// how inverted axes work with different axis types.
	
	// All that is required is to set the InvertScale property of ticks to true.
	// We'll use the default axis to quickly specify the setting for each axis on each chart.
	Chart.DefaultAxis.InvertScale = true;
	Chart2.DefaultAxis.InvertScale = true;
	Chart3.DefaultAxis.InvertScale = true;
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	Chart.SeriesCollection.Add(getRandomData());
	Chart2.SeriesCollection.Add(getRandomData2());
	Chart3.SeriesCollection.Add(getRandomData3());
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	DateTime dt = new DateTime(2005,1,1);
	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.YValue = myR.Next(50);
			e.XDateTime = dt;
			dt = dt.AddMonths(1);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	return SC;
}

SeriesCollection getRandomData2()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}

SeriesCollection getRandomData3()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();

	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.YValue = myR.Next(50);
			e.XValue = b*2;

			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px"></dotnet:Chart>
			<dotnet:Chart id="Chart2" runat="server" Width="568px" Height="344px"></dotnet:Chart>
			<dotnet:Chart id="Chart3" runat="server" Width="568px" Height="344px"></dotnet:Chart>
		</div>
	</body>
</html>
