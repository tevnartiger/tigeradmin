<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">




void Page_Load(Object sender,EventArgs e)
{

	Chart.Width = 600;
	Chart.Height = 250;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	
	Chart1.Width = 600;
	Chart1.Height = 250;
	Chart1.TempDirectory = "temp";
	Chart1.Debug = true;
	
	Chart2.Width = 600;
	Chart2.Height = 250;
	Chart2.TempDirectory = "temp";
	Chart2.Debug = true;
	
	Chart3.Width = 600;
	Chart3.Height = 250;
	Chart3.TempDirectory = "temp";
	Chart3.Debug = true;

	
	
	// This sample will demonstrate how to work with tick label separators. 
	
	// First we get our data, if you would like to get the data from a database you need to use
	// the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
	SeriesCollection mySC = getRandomData();
	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
	


	// The first chart doesnt automaticall show the separators so we set the color.
	Chart.Title = "Tick Label Separators on the x axis.";
	
	// The separator will appear because a color is set.
	Chart.XAxis.TickLabelSeparatorLine.Color = Color.Black;
	
	
	
	// The second chart automatically shows separators because the tick labels wrap.
	Chart1.Title = "Automatic tick label separators on the x axis.";
	Chart1.SeriesCollection.Add(getRandomData2());
	
	
	// The third chart automatically shows separators but we turn them off.
	Chart2.Title = "Tick separators turned off.";
	// The separator will not appear because a transparent color is set.
	Chart2.XAxis.TickLabelSeparatorLine.Color = Color.Transparent;
	
	Chart2.SeriesCollection.Add(getRandomData2());
	
	
	// // The fourth chart customize tickLabelSeparator
	Chart3.SeriesCollection.Add(mySC);
	Chart3.XAxis.TickLabelSeparatorLine.Color = Color.Red;
	Chart3.XAxis.TickLabelSeparatorLine.DashStyle = DashStyle.Dash;
	Chart3.XAxis.TickLabelSeparatorLine.Width=2;
	    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 4; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 20; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}

SeriesCollection getRandomData2()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 4; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 5; b++)
		{
			Element e = new Element();
			e.Name = "Long Name, Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			
			<dotnet:Chart id="Chart1" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			
			<dotnet:Chart id="Chart2" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			
			<dotnet:Chart id="Chart3" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>

		</div>
	</body>
</html>
