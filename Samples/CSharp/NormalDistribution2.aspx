<%@ Page Language="C#" debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>


<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{ 
	// This sample demonstrates the use of NormalDistribution
	// within StatisticalEngine.
	
	// The Distribution Chart
	DistributionChart.Title="Distribution";
	DistributionChart.TempDirectory = "temp";
	DistributionChart.Debug=true;
	DistributionChart.Size = "600x300";
	DistributionChart.LegendBox.Template ="%icon %name";
	//DistributionChart.XAxis.ScaleRange.ValueLow =50;
	DistributionChart.YAxis.Percent = true;
	DistributionChart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	DistributionChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical;
	DistributionChart.DefaultSeries.DefaultElement.Marker.Visible = false;
	DistributionChart.XAxis.Interval=1;
	DistributionChart.YAxis.FormatString = "0.000000";


	
	double[] normalDistVector = new double[] {1,2,3,4,5, 6, 7, 8, 9, 10,10};
		
	Series sampledata = new Series ("Sample1");
	for ( int i=0; i < normalDistVector.Length; i++) {
		Element el = new Element();
		el.XValue = normalDistVector[i];
		sampledata.Elements.Add (el);
	}
	// The second parameter of this method is the standard deviation of the normal probability distribution
	Series normalDistribution = StatisticalEngine.NormalDistribution(sampledata, 1);
	normalDistribution.Type = SeriesType.AreaSpline;
	normalDistribution.DefaultElement.ShowValue = true;
	normalDistribution.DefaultElement.SmartLabel.Text = "%YValue";
	normalDistribution.DefaultElement.SmartLabel.Color = Color.Black;
	DistributionChart.SeriesCollection.Add (normalDistribution);	
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="DistributionChart" runat="server"/>			
		</div>
	</body>
</html>
