<%@ Page Language="C#" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">
// This sample demonstrates how to add a legend box for the shapes.
void Page_Load(Object sender,EventArgs e)
{
	Chart.Type = ChartType.Map;
	Chart.Size = "1000x483";
	Chart.Title = " United States Map with Legend Entries";
	Chart.TempDirectory = "temp";
	Chart.Debug=true;
	Chart.TitleBox.Position = TitleBoxPosition.Full;
	
	Chart.ChartArea.Background = new Background(Color.FromArgb(142,195,236),Color.FromArgb(63,137,200),90);
	MapLayer layer = MapDataEngine.LoadLayer("../../images/MapFiles/primusa.shp");
	
	Chart.Mapping.DefaultShape.Label.Text = "%State_Abbr";
	Chart.Mapping.DefaultShape.Label.Color = Color.White;
	Chart.Mapping.DefaultShape.Label.OutlineColor = Color.Black;
	Chart.Mapping.DefaultShape.Label.Hotspot.ToolTip = "%State_NAME";
        Chart.Mapping.DefaultShape.Label.Font = new Font("Arial",8,System.Drawing.FontStyle.Bold);
	
	Chart.Mapping.MapLayerCollection.Add(layer);
	
	//Set different colors to each state
	Chart.PaletteName = Palette.Three;
	int i=0;
	foreach(Shape shape in layer.Shapes)
	{
		if(i >= Chart.Palette.Length )
		{
			i=0;
			Chart.PaletteName = Palette.Four;
		}
		shape.Background.Color = Chart.Palette[i];
		Chart.ExtraLegendEntries.Add(new LegendEntry((string)shape["STATE_NAME"],"",Chart.Palette[i]));
		i++;	
	}

}

</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>
