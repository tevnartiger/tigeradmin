<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    void Page_Load(Object sender, EventArgs e)
    {
        // This sample demonstrates using compounded gauges.

        Chart.TempDirectory = "temp";
        Chart.Debug = true;
        Chart.Palette = new Color[] { Color.FromArgb(255, 99, 49), Color.DarkBlue };
        Chart.Type = ChartType.Gauges;
        Chart.Size = "500x350";
        Chart.Title = ".netCHARTING Sample";
	Chart.OverlapFooter = true;
        Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
        Chart.TitleBox.ClearColors();
        Chart.ChartArea.ClearColors();

        // *DYNAMIC DATA NOTE* 
        // This sample uses random data to populate the chart. To populate 
        // a chart with database data see the following resources:
        // - Help File > Getting Started > Data Tutorials
        // - DataEngine Class in the help file	
        // - Sample: features/DataEngine.aspx

        SeriesCollection mySC = getRandomData();

        // Add the random data.
        Chart.SeriesCollection.Add(mySC);

        mySC[0].Background.Color = Color.WhiteSmoke;

        // Create a second series to embed into the circular gauge
        Series digital = Series.FromYValues(mySC[0][0].YValue);
        digital.GaugeType = GaugeType.DigitalReadout;
        digital.GaugeBorderBox.Padding = 2;
        digital.GaugeBorderBox.Background.Color = Color.FromArgb(100, Color.Black);
        digital.GaugeBorderBox.Shadow.Color = Color.Empty;
        digital.GaugeBorderBox.Position = new Rectangle(195, 240, 100, 40);
        digital.LegendEntry.Visible = false;

        // Specify the digital readout font size
        digital.DefaultElement.SmartLabel.Font = new Font("Arial", 30);

        // Get rid of digital gauge labels
        digital.XAxis = new Axis();
        digital.XAxis.DefaultTick.Label.Text = "";

        // Add the digital readout series to the chart.
        Chart.SeriesCollection.Add(digital);
    }

    SeriesCollection getRandomData()
    {
        Random myR = new Random(1);
        SeriesCollection SC = new SeriesCollection();
        int a = 0;
        int b = 0;
        for (a = 1; a < 2; a++)
        {
            Series s = new Series("Series " + a.ToString());
            for (b = 1; b < 2; b++)
            {
                Element e = new Element("Element " + b.ToString());
                e.YValue = myR.Next(14150);
                s.Elements.Add(e);
            }
            SC.Add(s);
        }
        return SC;
    }

    SeriesCollection getLiveData()
    {
        DataEngine de = new DataEngine("ConnectionString goes here");
        de.ChartObject = Chart; // Necessary to view any errors the dataEngine may throw.
        de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ....";
        return de.GetSeries();
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <dotnet:Chart ID="Chart" runat="server" />
    </div>
</body>
</html>
