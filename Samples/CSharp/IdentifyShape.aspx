<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	Chart.Type = ChartType.Map;
	Chart.Size = "600x350";
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = ".netCHARTING Sample";
	Chart.Visible = false;
	Chart.ChartArea.Label.Text = "Please click on a state to fill it with red.";
	Chart.ChartArea.Background = new Background(Color.FromArgb(142,195,236),Color.FromArgb(63,137,200),90);
	Chart.Mapping.DefaultShape.Background.Color = Color.LightGray;
	
	// This sample demonstrates getting and setting the properties of a shape on a map based on a click position.
	// FullScreen
	
	Bitmap bp = null;
	Chart.Mapping.MapLayerCollection.Add(@"../../images/MapFiles/primusa.shp");
	
	if(Page.Request.Params["y"] != null && Page.Request.Params["x"] != null)
	{
		bp = Chart.GetChartBitmap();
		
		// Get x and y points
		int x = Convert.ToInt32( Page.Request.Params["x"]);
		int y = Convert.ToInt32(Page.Request.Params["y"]);
		
		ArrayList al = Chart.Mapping.GetShapesAtPoint(new Point(x,y));
		foreach(object o in al)
		{
			((Shape)o).Background.Color = Color.Red;
			iLabel.Text+="<BR>State: " + ((Shape)o)["STATE_NAME"];
			iLabel.Text+="<BR>Population: " + double.Parse(((Shape)o)["Population"].ToString()).ToString("###,###");
		}
	}
	
    // Update the bitmap image.
  	bp = Chart.GetChartBitmap();
	string fileName = Chart.FileManager.SaveImage(bp);
	imageLabel.Text += "<input type=image value=\"submit\" border=0 src=\"" +fileName+ "\" ISMAP>";  
	
	bp.Dispose();	
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	</head>
	<body>
		<div align="center">
		<form method =get >
		<asp:Label ID=imageLabel Runat=server/>
			</form>
			<dotnet:Chart id="Chart" runat="server" />
		<asp:Label ID="iLabel" Runat=server></asp:Label>
		</div>
		
	</body>
</html>
