<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>



		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
     
	// This sample demonstrates the use of the Volume indicators
	
	// First we declare a chart of type FinancialChart	
	// The Financial Chart
	FinancialChart.Title="Financial Chart";
	FinancialChart.TempDirectory="temp";
	FinancialChart.Debug=true;
	FinancialChart.ShadingEffect = true;
	FinancialChart.LegendBox.Template ="%icon %name";
	FinancialChart.Size="900X800";
		
	FinancialChart.XAxis.Scale = Scale.Time;
	FinancialChart.XAxis.FormatString = "MMM d";
	FinancialChart.XAxis.TimeInterval = TimeInterval.Day;
	// For financial indicators the time scale is inverted (i.e. the first element of the series is the newest)
	FinancialChart.XAxis.InvertScale = true; 
	
	
	FinancialChart.YAxis.Label.Text = "Price (USD)";
	FinancialChart.YAxis.FormatString = "currency";
	FinancialChart.YAxis.ScaleRange.ValueLow = 15;
	
	// Here we load data samples from the FinancialCompany table from within chartsample.mdb
	DataEngine priceDataEngine = new DataEngine ();
	priceDataEngine.ChartType = ChartType.Financial;
	priceDataEngine.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];
	priceDataEngine.DateGrouping = TimeInterval.Day;
	priceDataEngine.StartDate = new DateTime (2001,2,1);
	priceDataEngine.EndDate = new DateTime (2001,9,30);
	priceDataEngine.SqlStatement = @"SELECT TransDate, HighPrice, LowPrice, OpenPrice, ClosePrice,Volume FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate  DESC ";
	priceDataEngine.DataFields = "xAxis=TransDate,High=HighPrice,Low=LowPrice,Open=OpenPrice,Close=ClosePrice,Volume=Volume";

	SeriesCollection sc = priceDataEngine.GetSeries ();
	Series prices = null;
	if(sc.Count>0)
		prices = sc[0];
	else
		return;
		
	
	prices.DefaultElement.ToolTip="L:%Low-H:%High";
	prices.DefaultElement.SmartLabel.Font = new Font("Arial", 6);
	prices.DefaultElement.SmartLabel.Text="O:%Open-C:%Close";
	prices.Type = SeriesTypeFinancial.CandleStick;

	CalendarPattern cp = new CalendarPattern (TimeInterval.Day, TimeInterval.Week, "0000001");
	cp.AdjustmentUnit = TimeInterval.Day;
	prices.Trim (cp, ElementValue.XDateTime);
	prices.Name = "Prices";
	FinancialChart.SeriesCollection.Add (prices);

	// Create the second chart area 
	ChartArea volumeChartArea = new ChartArea();
	volumeChartArea.Label.Text = "Stock Volume";
	volumeChartArea.YAxis.Label.Text = "Volumes";
	volumeChartArea.Series.Name="Stock Volume";
	volumeChartArea.HeightPercentage = 17;
	volumeChartArea.Series.DefaultElement.ToolTip="%YValue";
	FinancialChart.ExtraChartAreas.Add(volumeChartArea);

	// Add a volume series to the chart area
	DataEngine volumeDataEngine = new DataEngine ();
	volumeDataEngine.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];
	volumeDataEngine.DateGrouping = TimeInterval.Days;
	volumeDataEngine.StartDate=new DateTime (2001,2,1);
	volumeDataEngine.EndDate = new DateTime (2001,9,30);
	volumeDataEngine.SqlStatement= @"SELECT TransDate,volume FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate DESC"; 
	volumeDataEngine.DataFields="xAxis=TransDate,yAxis=Volume";
		
	Series volumes = volumeDataEngine.GetSeries () [0];
	volumes.Trim (cp, ElementValue.XDateTime);
	volumes.Name = "Volumes";
	volumes.Type = SeriesType.Bar;
	volumeChartArea.SeriesCollection.Add (volumes);
	
		
	
	 // Financial Volume Indicators
	
	FinancialChart.DefaultSeries.DefaultElement.Marker = new ElementMarker (ElementMarkerType.None);
	FinancialChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical;
	FinancialChart.DefaultSeries.Type = SeriesType.Spline;
	
	// Negative Volume Index
	Series negativeVolume = FinancialEngine.NegativeVolumeIndex(prices);
	negativeVolume.Type = SeriesType.Spline;
	negativeVolume.DefaultElement.Color = Color.FromArgb(122,255,45);
	volumeChartArea.SeriesCollection.Add(negativeVolume);
	
	// Positive Volume Index
	Series positiveVolume = FinancialEngine.PositiveVolumeIndex(prices);
	positiveVolume.Type = SeriesType.Spline;
	positiveVolume.DefaultElement.Color = Color.FromArgb(255,125,45);
	volumeChartArea.SeriesCollection.Add(positiveVolume);

	// Create a new char area for the indicator OnBalanceVolume
	ChartArea balanceChartArea = new ChartArea();
	balanceChartArea.Label.Text = "On Balance Volume";
	balanceChartArea.YAxis.Label.Text = "Volumes";
	balanceChartArea.HeightPercentage = 17;
	FinancialChart.ExtraChartAreas.Add(balanceChartArea);

	// On Balance Volume indicator
	Series onBalanceVolume = FinancialEngine.OnBalanceVolume(prices);
	onBalanceVolume.Type = SeriesType.Spline;
	onBalanceVolume.DefaultElement.Color = Color.FromArgb(45,125,255);
	balanceChartArea.SeriesCollection.Add(onBalanceVolume);

}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Financial Volume Indicators Sample</title>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="FinancialChart" runat="server"/>
		</div>
	</body>
</html>
