<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart2.TempDirectory = "temp";
	Chart2.Debug = true;
	Chart3.TempDirectory = "temp";
	Chart3.Debug = true;
	Chart.Title = "Single Value Axis Tick";	
	Chart2.Title = "Range Axis Tick.";
	Chart3.Title = "Image Axis Tick.";	
	
	// This sample will demonstrate how different tick types can be used.
	// It shows
	// - A normal tick
	// - A range tick
	// - an image tick.

	// 1. NORMAL TICK
	
	// Setup the axes
	Chart.YAxis.Interval = 500;
	Chart.YAxis.ScaleRange.ValueHigh = 500;
	Chart.XAxis.Interval = 500;
	Chart.XAxis.ScaleRange.ValueHigh = 500;

	// Setup a single value axis tick and add it.
	AxisTick at = new AxisTick(200);
	at.Line.Length = 20;
	at.Line.Width = 3;
	at.Line.Color = Color.Gray;
	at.Line.DashStyle = DashStyle.Dash;
	at.GridLine.Width = 5;
	at.Label.Font = new Font("Arial",10,FontStyle.Italic | FontStyle.Bold | FontStyle.Underline);
	at.Label.Color = Color.Blue;
	at.Label.Text = "Two Hundred";
	
	Chart.YAxis.ExtraTicks.Add(at);
	
	// 2. RANGE TICK
	// Setup a single value axis tick and add it.
	// Notice a range is used to instantiate the tick.
	AxisTick at2 = new AxisTick(50,450);
	at2.Line.Length = 5;
	at2.Line.Width = 3;
	at2.Line.Color = Color.Gray;
	at2.Line.DashStyle = DashStyle.Dash;
	
	at2.Label.Font = new Font("Arial",10,FontStyle.Italic | FontStyle.Bold | FontStyle.Underline);
	at2.Label.Color = Color.Blue;
	at2.Label.Text = "50 to 450";
	
	Chart2.YAxis.ExtraTicks.Add(at2);

	// Setup the axes
	Chart2.YAxis.Interval = 500;
	Chart2.YAxis.ScaleRange.ValueHigh = 500;
	Chart2.XAxis.Interval = 500;
	Chart2.XAxis.ScaleRange.ValueHigh = 500;

	// 3. IMAGE TICK
	
	// Setup a single value axis tick and add it.
	AxisTick at3 = new AxisTick(200);
	// The tick's Marker property is set with an image marker.
	at3.Marker = new ElementMarker("../../images/us.png");
	
	Chart3.YAxis.ExtraTicks.Add(at3);

	// Setup the axis
	Chart3.YAxis.Interval = 500;
	Chart3.YAxis.ScaleRange.ValueHigh = 500;

	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	Chart.SeriesCollection.Add(getRandomData());
	Chart2.SeriesCollection.Add(getRandomData());
	Chart3.SeriesCollection.Add(getRandomData());

}


SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(6);
	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.YValue = myR.Next(400);
			e.XValue = myR.Next(400);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<dotnet:Chart id="Chart2" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<dotnet:Chart id="Chart3" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
