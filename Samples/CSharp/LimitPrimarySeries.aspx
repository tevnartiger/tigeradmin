<%@ Page Language="C#" Debug="true" Trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    Chart.Title="Top Four Users In Each Category";
    Chart.XAxis.Label.Text="Users";
    Chart.YAxis.Label.Text="Protein, Carbs and Fat";
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.DefaultSeries.DefaultElement.ToolTip="%yvalue";
    Chart.PaletteName = Palette.Two;
    Chart.DefaultSeries.Limit="4";
    Chart.DefaultSeries.DefaultElement.ShowValue=true;
    
         
    Chart.Series.SqlStatement= @"SELECT Name,Calories,Protein,Carbs,Fat FROM Eaten ORDER BY Name";  							
    Chart.Series.DataFields ="xAxis=Name,yAxis=Protein,yAxis=Carbs,yAxis=Fat";
    Chart.SeriesCollection.Add();
    
    //set global properties
    Chart2.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    Chart2.Title="Top Four Carbs' Users";
    Chart2.XAxis.Label.Text="Users";
    Chart2.YAxis.Label.Text="Protein, Carbs and Fat";
    Chart2.TempDirectory="temp";
    Chart2.Debug=true;
    Chart2.DefaultSeries.DefaultElement.ToolTip="%yvalue";
    Chart2.PaletteName = Palette.Two;
    Chart2.DefaultSeries.Limit="4";
    Chart2.LimitPrimarySeries="Carbs";
    Chart2.DefaultSeries.DefaultElement.ShowValue=true;
    
         
    Chart2.Series.SqlStatement= @"SELECT Name,Calories,Protein,Carbs,Fat FROM Eaten ORDER BY Name";  							
    Chart2.Series.DataFields ="xAxis=Name,yAxis=Protein,yAxis=Carbs,yAxis=Fat";
    Chart2.SeriesCollection.Add();

    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Limit Primary Series Sample</title>
</head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
 <dotnet:Chart id="Chart2"  runat="server"/>
</div>
</body>
</html>
