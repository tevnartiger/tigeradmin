<%@ Page Language="C#"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Dynamic Ticks</title>
		<script runat="server">


void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	
	
	// This sample will demonstrate how to create dynamic tick labels based on chart data.
	
	// First we get our data, if you would like to get the data from a database you need to use
	// the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
	SeriesCollection sc = getRandomData();
	
	// Pass a simple string manipulation funtion to the x axis.
	Chart.XAxis = Chart.XAxis.Calculate("XAxis", new ChangeValueDelegate(MyFunction));
	
	// Have the tick mark labels be the total value of the each ticks group of elements.
	Chart.XAxis.DefaultTick.Label.Text = "%Value";
	
	// Each tick label will first become a value, then our function below will change the tick label based
	// the ticks value. 
	
	// To expand on this we'll use an annotation to show how many tick groups are over and under 100.
	// We have initialized an annotation and 2 counters below. 
	
	// Zero out the counters.
	underCount = 0;
	overCount = 0;
	
	// Add the annotation to our chart.	
	Chart.Annotations.Add(ann);
	

	// Add the random data.
	Chart.SeriesCollection.Add(sc);
    
    
}

public static Annotation ann = new Annotation();
public static int underCount = 0;
public static int overCount = 0;

public static string MyFunction(string value)
{
	int num = int.Parse(value);
	string newLabel = "";
	// Modify the string and increment counters.
	if(num > 100)
	{
		newLabel = " Over 100 (" + value + ")";
		overCount++;
	}
	else
	{
		newLabel = " Under 100 (" + value + ")";
		underCount++;		
	}
	
	// Update the annotation with the new count.
	ann.Label.Text = overCount + " group(s) are over, and " + underCount + " are under 100.";
	ann.Position = new Point(100,0);

	
	return newLabel;

}


SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
