using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using dotnetCHARTING;

public class WebFormChart : System.Web.UI.Page

{
	protected dotnetCHARTING.Chart ChartObj;

	override protected void OnInit(EventArgs e)

	{		
		ChartObj.DefaultSeries.ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;data source=" + Server.MapPath("../../database/chartsample.mdb");  
		ChartObj.Title="Orders";
		ChartObj.TempDirectory="temp";
		ChartObj.Debug=true;
		ChartObj.Type = ChartType.ComboHorizontal;
		ChartObj.Transpose=true;
		ChartObj.Series.StartDate=new DateTime(2002,3,10,0,0,0);
		ChartObj.Series.EndDate = new DateTime(2002,3,16,23,59,59);
		ChartObj.DateGrouping = TimeInterval.Week;

		//Add a series
		ChartObj.Series.Name="Order";
		ChartObj.Series.SqlStatement= @"SELECT OrderDate,1 AS q FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate";
		ChartObj.Series.DefaultElement.ShowValue=true;
		ChartObj.SeriesCollection.Add();

	}
}