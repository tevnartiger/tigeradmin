<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
     
	// This sample demonstrates the use of ZScore indicator
	
	// First we declare a chart of type FinancialChart	
	// The Financial Chart
	FinancialChart.Title="Financial Chart";
	FinancialChart.TempDirectory="temp";
	FinancialChart.Debug=true;
	FinancialChart.ShadingEffect = true;
	FinancialChart.PaletteName = Palette.WarmEarth;
	FinancialChart.LegendBox.Template ="%icon %name";
	FinancialChart.Size="800X600";
	FinancialChart.DefaultSeries.DefaultElement.Marker.Visible = false;
	FinancialChart.TitleBox.Position = TitleBoxPosition.FullWithLegend;	

	FinancialChart.YAxis.Label.Text = "Price (USD)";
	FinancialChart.YAxis.FormatString = "currency";
	FinancialChart.YAxis.Scale = Scale.Range;

				// Modify the x axis labels.
	FinancialChart.XAxis.Scale = Scale.Time;
	FinancialChart.XAxis.TimeInterval = TimeInterval.Day;		
	FinancialChart.XAxis.TimeScaleLabels.DayFormatString = "o";
	FinancialChart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Month);
	FinancialChart.XAxis.TimeScaleLabels.MonthFormatString = "MMM";
	
	
	DataEngine priceDataEngine = new DataEngine ();
	priceDataEngine.ChartObject = FinancialChart;
	priceDataEngine.ChartType = ChartType.Financial;
	priceDataEngine.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];
	priceDataEngine.DateGrouping = TimeInterval.Day;
	priceDataEngine.StartDate = new DateTime (2001,4,1);
	priceDataEngine.EndDate = new DateTime (2001,5,30);
	// Here we load data examples from the FinancialCompany table from within chartsample.mdb database 
	priceDataEngine.SqlStatement = @"SELECT TransDate, HighPrice, LowPrice, OpenPrice, ClosePrice FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate";
	priceDataEngine.DataFields = "xAxis=TransDate,High=HighPrice,Low=LowPrice,Open=OpenPrice,Close=ClosePrice";

	SeriesCollection sc = priceDataEngine.GetSeries ();
	Series prices = null;
	if(sc.Count>0)
		prices = sc[0];
	else
		return;
		
	prices.Name = "Prices";
	prices.DefaultElement.ToolTip="L:%Low | H:%High";
	prices.Type = SeriesTypeFinancial.CandleStick;
	
	CalendarPattern cp = new CalendarPattern (TimeInterval.Day, TimeInterval.Week, "0000001");
	prices.Trim (cp, ElementValue.XDateTime);

	FinancialChart.SeriesCollection.Add (prices);
	

	FinancialChart.DefaultSeries.Type = SeriesType.Spline;
			
	// Create a new chart area
	ChartArea zScoreChartArea = new ChartArea ();
	zScoreChartArea.HeightPercentage = 20;
	zScoreChartArea.YAxis.Label.Text = "Z Score";
	FinancialChart.ExtraChartAreas.Add (zScoreChartArea);

	// Here we add to the chart the Z-Score financial series 
	zScoreChartArea.SeriesCollection.Add (FinancialEngine.ZScore (prices, ElementValue.High));	
	zScoreChartArea.SeriesCollection.Add (FinancialEngine.ZScore (prices, ElementValue.Low));	
	
		
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING ZScore Sample</title>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="FinancialChart" runat="server"/>
		</div>
	</body>
</html>
