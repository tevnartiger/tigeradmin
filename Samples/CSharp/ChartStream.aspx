<%@ Page Language="C#" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.IO" %>

<script runat="server">
//This sample demonstrates how to get the chart memory stream
dotnetCHARTING.Chart myChart = new  dotnetCHARTING.Chart();
string filePath="";
void Page_Load(Object sender,EventArgs e)
{

	//Add data to the chart
	CreateChart();
	//Get the chart memoryStream
	System.IO.MemoryStream st = myChart.GetChartStream();
	//Save the memoryStream to the file and return the file's relative path 
	//and set it to src attribute of embed element.
	filePath  = myChart.FileManager.SaveImage(st);
	
	//Check the temp directory for writeEmbed.js and creates it if doesn't exist.
	CreateFileJS();
	   
}
SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 1; b < 5; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			e.YValue = 25 + myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
void CreateChart()
{
	
	myChart.Size="600x480";
	myChart.TempDirectory = "temp";
	myChart.FileName="chartStream";
	myChart.DisableBrowserCache=false;
	myChart.ImageFormat = ImageFormat.Swf;

	
 	// Set the title.
   myChart.Title = "My Chart";
      
   // Set the x axis label
   myChart.ChartArea.XAxis.Label.Text = "X Axis Label";
   
   // Set the y axis label
   myChart.ChartArea.YAxis.Label.Text = "Y Axis Label";
   
   // Set the bar shading effect
   myChart.ShadingEffect = true;
      
   // Add the random data.
   myChart.SeriesCollection.Add(getRandomData());
   
 }
void CreateFileJS()
{
	string tempPath = MapPath("temp");
	DirectoryInfo di = new DirectoryInfo(tempPath);
	FileInfo[] fiArr = di.GetFiles("writeEmbed.js");				
	if(fiArr.Length <1)
	{
		FileStream fout = new FileStream(tempPath + "\\writeEmbed.js", FileMode.Create, FileAccess.Write);
		string data = "function WriteEmbed(stringVlaue){document.write(stringVlaue);}";
		byte[] adData = new byte[data.Length];
		adData = ASCIIEncoding.ASCII.GetBytes(data);
		fout.Write(adData,0,adData.Length);
		fout.Close();
	}
}   

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Chart Stream Sample</title>
<script src="temp/writeEmbed.js" type="text/javascript"></script>
</head>
<body>
<center>
<%
Response.Write("<script type=\"text/javascript\">WriteEmbed('<embed height=\"480\" width=\"600\" src=\"");
Response.Write(filePath.Replace("\\","/"));
Response.Write("\" type=\"application/swf\"/>');");
Response.Write("</script>");
%></center>
</body>
</html>