<%@ Page Language="C#" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    Chart.Title="Sales";
    Chart.XAxis.Label.Text="%name";
    Chart.Type = ChartType.Pies;
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    
    Chart.DateGrouping = TimeInterval.Months;

    //Add a series
    Chart.Series.StartDate=new System.DateTime(2002,2,1,0,0,0);
    Chart.Series.EndDate = new System.DateTime(2002,6,30,23,59,59);
    Chart.Series.SqlStatement= @"SELECT OrderDate,Total, Name FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate";
    Chart.SeriesCollection.Add();

}
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Sales Report</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
