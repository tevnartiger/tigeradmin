<%@ Page Language="C#" Description="dotnetCHARTING Component" debug = "true" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	//View available attributes for a given shape file
	Chart.Type = ChartType.Map;
	Chart.TempDirectory = "temp";
        MapLayer layer = MapDataEngine.LoadLayer("../../images/MapFiles/canada.shp");
	foreach(string attributename in layer.AttributeNames)
	{
		Response.Write(attributename+"<br>");
	}

	Chart.Mapping.MapLayerCollection.Add(layer);
	Chart.Mapping.ZoomPercentage = 90;
}
</script>
	</head>
	<body>
		<div style="text-align:center">
				<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>