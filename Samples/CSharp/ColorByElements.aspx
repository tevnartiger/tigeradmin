<%@ Page Language="C#" debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">

Series getRandomData() 
{
	Random myR = new Random();
	
	Series s = new Series();
	s.Name = "Clicks";
	for(int b = 1; b < 8; b++)
	{
		Element e = new Element();
		e.Name = "Element " + b;;
		e.YValue = myR.Next(50);
		s.Elements.Add(e);
	}

	return s;
}


void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	
	// This sample wil demonstrate how you can color an element in each series so you can do without the x axis.
	
	Series mySeries = getRandomData();
	
	// By setting a palette to this series it will instruct the chart engine to color each of it's elements
	// and place each element into the legend and remove the series.
	mySeries.PaletteName = Palette.Two;
	
	// Now we can remove the x axis by hiding it.
	Chart.XAxis.ClearValues = true;
	Chart.XAxis.DefaultTick.Line.Length = 0;
	
	// For fun lets also add some shading to the bars.
	Chart.ShadingEffectMode = ShadingEffectMode.Two;
	

	// Add the random data.
	Chart.SeriesCollection.Add(mySeries);
    
    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Color By Element</title>
</head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
