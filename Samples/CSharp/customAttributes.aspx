<%@ Page Language="C#" Debug="true" Trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    
    Chart.Title=" Employees' Report";
    Chart.ChartArea.Label.Text = "MouseOver John's column to see a tooltip.";
    Chart.XAxis.Label.Text="Employee's Name";
    Chart.YAxis.Label.Text="Annual Salary";
    Chart.YAxis.FormatString="currency";
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.Use3D=true;
    Chart.DefaultSeries.DefaultElement.URL = "customAttributeTarget.aspx?name=%name&salary=%yvalue&id=%EmployeeID&department=%Department&email=%email&phone=%PhoneNumber";
    
    //Create a series  
 	DataEngine de = new DataEngine();
 	de.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    de.SqlStatement= @"SELECT * FROM Employees"; 
    //Set predefined data fields and some custom fields as attributes for x-Axis. (Employee�s name)
    de.DataFields="xAxis=name,yAxis=salary,id=EmployeeID,department=Department,email,phone=PhoneNumber";
    SeriesCollection sc = de.GetSeries();
    if(sc.Count>0)
    {
    	Series ser1 = sc[0];
    	ser1.Name = "Employees";
    	for(int i=0;i<3;i++)
    	{
    		switch(i)
    		{
    			case 0:
    			 	//set employees' information to annotation 
    			 	// we can use all the element tokens in the text, toolTip and even url of this annotation.
				 	Annotation a = new Annotation("Employee No: %EmployeeID\nDepartment: %Department\nEmail: %email\nPhone: %PhoneNumber");
				 	a.Background.Bevel = true;
				 	a.Background.Color = Color.FromArgb(150,180,180,220);
				 	// Assign the annotation to an element in the sc collection.
	    		 	ser1.Elements[0].Annotation = a;
    			 break;
    			 case 2:
    			 	//set employees' information to Tooltip 
    			 	ser1.Elements[2].ToolTip = "Employee No: %EmployeeID\nDepartment: %Department\nEmail: %email\nPhone: %PhoneNumber";
    			 break;   			 
    			default:
    			 break;
    		}
    	}
    }
   Chart.SeriesCollection.Add(sc);    
 
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Custom Attributes</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart" TitleBox-Position = "FullWithLegend" runat="server"/>

</div>
</body>

</html>
