<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	// This sample will show how chart data and settings can be loaded from a saved xml file.
	Chart.TempDirectory = "temp";	
	// Load the chart data and settings from the myChart.xml in the temp directory and use it for this chart.
	Chart.LoadState("temp/myChart.xml");
	
	//Change some labels
	Chart.ChartArea.Label.Text = "This chart is loaded from file 'myChart.xml'.";
	if(Chart.Annotations.Count==1)
	{
		Chart.Annotations[0].Label.Text = "Go to the sample that saves the myChart.xml file";
		Chart.Annotations[0].URL = "SaveState.aspx";
	}
	
        
}

</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" NoDataLabel-Text="Please run the SaveState.aspx sample to generate the xml file." runat="server">
			</dotnet:Chart>
		</div>
	</body>
</html>
