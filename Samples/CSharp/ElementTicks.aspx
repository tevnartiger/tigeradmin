<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart2.TempDirectory = "temp";
	Chart2.Debug = true;
	Chart3.TempDirectory = "temp";
	Chart3.Debug = true;
	Chart.Title = "Automatic ticks for each element (Method 1)";
	Chart2.Title = "Automatic ticks for each element (Method 2)";
	Chart3.Title = "Controling individual element ticks";	
	
	// This sample will demonstrate how axes can automatically generate ticks at each element's position and
	// how to control each element's tick independently.
	// The first and second chart show 2 different ways of doing this while the third shows how to control
	// an individual tick and use tokens.
	
	// Notice that some ticks don't appear. This is because there is no space for the label, however, the
	// grid line is there to indicate that a tick should be there.
	
	// 1. AUTOMATICALLY GENERATE TICKS (METHOD 1): 
	
	// First we setup the scale.
	Chart.YAxis.Interval = 500;
	Chart.YAxis.ScaleRange.ValueHigh = 500;
	Chart.XAxis.Interval = 500;
	Chart.XAxis.ScaleRange.ValueHigh = 500;
	
	// Now we say we want ticks generated for each element.
	Chart.YAxis.GenerateElementTicks = true;
	Chart.XAxis.GenerateElementTicks = true;

	// 2. AUTOMATICALLY GENERATE TICKS (METHOD 2): 
	// The second method involves instantiating a tick for each element by using the default element.
	Chart2.DefaultSeries.DefaultElement.XAxisTick = new AxisTick();
	Chart2.DefaultSeries.DefaultElement.YAxisTick = new AxisTick();
	
	// Setup the scale.
	Chart2.YAxis.Interval = 500;
	Chart2.YAxis.ScaleRange.ValueHigh = 500;
	Chart2.XAxis.Interval = 500;
	Chart2.XAxis.ScaleRange.ValueHigh = 500;

	// 3. CONTROLING INDIVIDUAL TICKS: 
	// Setup the scale.
	Chart3.YAxis.Interval = 500;
	Chart3.YAxis.ScaleRange.ValueHigh = 500;

	// First we get our data so we can work the the elements.
	SeriesCollection sc = getRandomData();
	
	// We we'll select an element, instantiate a tick for it and set it's color.
	sc[0].Elements[2].YAxisTick = new AxisTick();
	sc[0].Elements[2].YAxisTick.Label.Color = Color.Red;
	sc[0].Elements[2].YAxisTick.Label.Text = "%YValue (%PercentOfTotal)";
	
	
		// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	Chart.SeriesCollection.Add(getRandomData());
	Chart2.SeriesCollection.Add(getRandomData());
	Chart3.SeriesCollection.Add(sc);

}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(6);
	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.YValue = myR.Next(400);
			e.XValue = myR.Next(400);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<dotnet:Chart id="Chart2" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<dotnet:Chart id="Chart3" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
