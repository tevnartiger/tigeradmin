<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.DefaultSeries.Type = SeriesType.Line;
		
	// This sample will demonstrate how to use data sources to process string tokens in an annotation.
	// It will take the series collection we chart and show some additional information about it in an annotation.
	
	// 1. GET DATA
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. For information on acquiring 
	// database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();
		
	// 2. CREATE AN ANNOTATION AND SET THE DATA SOURCE.
	Annotation an = new Annotation();
	
	// Create the data source.
	an.DataSource = DataSource.FromSeriesCollection(mySC);
	// A simpler way to set the data source that also works is to use 
	// the implicit casting functionality of the DataSource object.
	an.DataSource = mySC;
	
	// Specify the text with tokens to replace.
	an.Label.Text = "y sum: <%YSum,Currency> \n x sum : %XSum";
	
	// Add the annotation to the chart.
	Chart.Annotations.Add(an);
		
	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 20; b++)
		{
			Element e = new Element();
			//e.Name = "Element " + b;
			e.YValue = myR.Next(50);
			e.XValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
