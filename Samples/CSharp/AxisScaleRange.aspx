<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart2.TempDirectory = "temp";
	Chart2.Debug = true;
	Chart3.TempDirectory = "temp";
	Chart3.Debug = true;
	Chart.Title = "Y axis range set to 5 - 725 with a logarithmic scale";	
	Chart2.Title = "Y axis high range set to 53 with a normal numeric scale.";	
	Chart3.Title = "X axis range set 12/1/04 - 9/1/05";	
	
	
	// This sample will demonstrate how how scale ranges can be set.
	
	
	// Set the axis scale.
	Chart.YAxis.Scale = Scale.Logarithmic;
	
	// The range can be set using this code:
	Chart.YAxis.ScaleRange.ValueHigh = 725;
	Chart.YAxis.ScaleRange.ValueLow = 50;
	
	// Or using this code:
	Chart.YAxis.ScaleRange = new ScaleRange(5, 725);
	
	// Add arbitrary ticks at scale bounds.
	Chart.YAxis.ExtraTicks.Add(new AxisTick(725));
	Chart.YAxis.ExtraTicks.Add(new AxisTick(5));

	// CHART 2

	// Specify an interval and a high range value.
	Chart2.YAxis.Interval = 20;
	Chart2.YAxis.ScaleRange.ValueHigh = 53;
	
	// CHART 3
	// Specify the range using a new scale range object.
	Chart3.XAxis.ScaleRange = new ScaleRange(new DateTime(2004,12,1), new DateTime(2005,9,1));

	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	Chart.SeriesCollection.Add(getRandomData2());
	Chart2.SeriesCollection.Add(getRandomData2());
	Chart3.SeriesCollection.Add(getRandomData());
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	DateTime dt = new DateTime(2005,1,1);
	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.YValue = myR.Next(50);
			e.XDateTime = dt;
			dt = dt.AddMonths(1);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}



SeriesCollection getRandomData2()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<dotnet:Chart id="Chart2" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<dotnet:Chart id="Chart3" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
