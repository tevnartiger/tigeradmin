<%@ Page Language="C#" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    void Page_Load(Object sender, EventArgs e)
    {
        // This sample demonstrates using different dynamic image color tolerance settings and their effects.

        Chart.TempDirectory = "temp";
        Chart.Debug = true;
        Chart.Palette = new Color[] {  Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) };

        Chart.Type = ChartType.Combo;
        Chart.Size = "600x350";
        Chart.Title = ".netCHARTING Dynamic Color Tolerance";


        // *DYNAMIC DATA NOTE* 
        // This sample uses random data to populate the chart. To populate 
        // a chart with database data see the following resources:
        // - Help File > Getting Started > Data Tutorials
        // - DataEngine Class in the help file	
        // - Sample: features/DataEngine.aspx

        SeriesCollection mySC = getRandomData();
        Chart.DefaultSeries.Type = SeriesType.Marker;

        // Add the random data.
        Chart.SeriesCollection.Add(mySC);
        Chart.DefaultElement.Marker = new ElementMarker("../../images/shieldBlurred.png");
        Chart.DefaultElement.Marker.DynamicImageColor = Color.FromArgb(6, 0, 127);

        mySC[0][0].Marker.DynamicImageColorTolerance = 0;
        mySC[0][1].Marker.DynamicImageColorTolerance = 10;
        mySC[0][2].Marker.DynamicImageColorTolerance = 20;
        mySC[0][3].Marker.DynamicImageColorTolerance = 30;
        mySC[0][4].Marker.DynamicImageColorTolerance = 40;
        mySC[0][5].Marker.DynamicImageColor = Color.Empty;
        mySC[0][5].Name = "Original";

    }

    SeriesCollection getRandomData()
    {
        Random myR = new Random(1);
        SeriesCollection SC = new SeriesCollection();
        for (int a = 1; a < 2; a++)
        {
            Series s = new Series("Series " + a.ToString());
            for (int b = 1; b < 7; b++)
            {
                Element e = new Element("Tolerance " + ((b-1)*10).ToString());
                e.YValue = myR.Next(50);
                s.Elements.Add(e);
            }
            SC.Add(s);
        }
        return SC;
    }

    SeriesCollection getLiveData()
    {
        DataEngine de = new DataEngine("ConnectionString goes here");
        de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ....";
        return de.GetSeries();
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <dotnet:Chart ID="Chart" runat="server" />
    </div>
</body>
</html>
