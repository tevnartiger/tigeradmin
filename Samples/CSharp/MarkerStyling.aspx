<%@ Page Language="C#" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>


<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	Chart.Width = 1000;
	Chart.Height = 500;
	Chart.Title = "marker settings based on population and state capitals (Red marker).";
	Chart.TempDirectory = "temp";
	Chart.TitleBox.Position = TitleBoxPosition.Full;
	Chart.ChartArea.Background = new Background(Color.FromArgb(142,195,236),Color.FromArgb(63,137,200),90);
	Chart.Mapping.ZoomCenterPoint = new PointF(38f,-96.5f);
	Chart.Mapping.ZoomPercentage = 180;
	Chart.Type = ChartType.Map;	
	Chart.Mapping.MapLayerCollection.Add("../../images/MapFiles/states.shp");
	Chart.Mapping.MapLayerCollection[0].DefaultShape.Background.Color = Color.PapayaWhip;
	
	MapLayer layer = MapDataEngine.LoadLayer("../../images/MapFiles/cities.SHP");
	
	// set different marker settings based on population and state capitals.
	foreach(Shape shape in layer.Shapes)
	{
		
		
		int population = Convert.ToInt32(shape["POP"]);
		string capital = (string)shape["CAPITAL"];
		if(capital=="N")
		{
			shape.Marker.Type = ElementMarkerType.Circle;
			shape.Marker.Color = Color.Blue;
		}
		else
		{
			shape.Marker.Type = ElementMarkerType.Triangle;
			shape.Marker.Color = Color.Red;
		}
			
		if(population<10000)
			shape.Marker.Size = 1;
		else if(population<100000)
			shape.Marker.Size = 4;
		else if(population<500000)
			shape.Marker.Size = 7;
		else if(population<2000000)
			shape.Marker.Size = 10;
		else if(population<3000000)
			shape.Marker.Size = 15;
		else if(population<5000000)
			shape.Marker.Size = 20;
		else
			shape.Marker.Size = 25;
	}
	
	Chart.Mapping.MapLayerCollection.Add(layer);
}

</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>
