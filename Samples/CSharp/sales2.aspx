<%@ Page Language="C#" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    Chart.Title="Sales For January";
    Chart.XAxis.Label.Text="Weeks";
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.Use3D=true;
    Chart.TitleBox.Position= TitleBoxPosition.Full;
    Chart.LegendBox.LabelStyle = new dotnetCHARTING.Label("", new Font("Bold Garamond", 9), Color.Red);
    Chart.DateGrouping = TimeInterval.Weeks;

    //Add a series
    Chart.Series.Name = "sales";
    Chart.Series.StartDate=new System.DateTime(2002,1,1,0,0,0);
    Chart.Series.EndDate = new System.DateTime(2002,1,31,23,59,59);
    Chart.Series.SqlStatement= @"SELECT OrderDate,Total, Name FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate";
    Chart.SeriesCollection.Add();

}
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Sales Report</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
