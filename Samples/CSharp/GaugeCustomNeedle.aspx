<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	// This sample demonstrates using a custom image with a dynamic color for gauge needles.

	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Palette = new Color[]{Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)};
	
	Chart.Type = ChartType.Combo;
	Chart.Size = "600x350";
	Chart.Title = ".netCHARTING Sample";
    Chart.Type = ChartType.Gauges;
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Help File > Getting Started > Data Tutorials
	// - DataEngine Class in the help file	
	// - Sample: features/DataEngine.aspx
	
	SeriesCollection mySC = getRandomData();
    Chart.DefaultSeries.Background.Color = Color.White;

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
   // mySC[0].YAxis = new Axis();
    Chart.YAxis.MinorTicksPerInterval = 3;
    Chart.YAxis.DefaultMinorTick.Line.Width = 1;
    Chart.YAxis.DefaultMinorTick.Line.Length = 5;
    Chart.YAxis.GaugeNeedleType = GaugeNeedleType.UseMarker;
    mySC[0].DefaultElement.Marker = new ElementMarker("../../images/needle2.png");
    mySC[0].DefaultElement.Marker.DynamicImageColor = Color.FromArgb(255, 255, 255);

    mySC[1].DefaultElement.Marker = new ElementMarker("../../images/g2.gif");
    mySC[1].DefaultElement.Marker.DynamicImageColor = Color.FromArgb(119,119,119);

    Chart.ChartArea.ClearColors();
    //mySC[0].LegendEntry.SeriesType = SeriesType.Marker;
    //mySC[0].LegendEntry.Marker = new ElementMarker("CustomNeedle.gif");

        
}

SeriesCollection getRandomData()
{
	Random myR = new Random();
	SeriesCollection SC = new SeriesCollection();
	for(int a = 1; a < 3; a++)
	{
		Series s = new Series("Series " + a.ToString());
		for(int b = 1; b < 2; b++)
		{
			Element e = new Element("Element " + b.ToString());
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}
	return SC;
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
