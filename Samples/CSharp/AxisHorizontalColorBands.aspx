<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.YAxis.Maximum = 100;
	Chart.YAxis.AlternateGridBackground.Color = Color.FromArgb(40,Color.White);
	
	// This sample will demonstrate how axis markers can create gradiant zones on the chart.

	// The axis markers have solid colors and transition colors to blend between solid markers.
	// am1, am3, and am4 are solid color markers while the others will be gradient color transition markers.

	// Solid Markers
	AxisMarker am1 = new AxisMarker("Danger", new Background(Color.Red,Color.Red,90),85,100);
	AxisMarker am3 = new AxisMarker("Warning", new Background(Color.Orange,Color.Orange,90),50,70);
	AxisMarker am5 = new AxisMarker("Safe", new Background(Color.Yellow,Color.Yellow,90),0,30);	
	
	// Color transition markers.
	AxisMarker am2 = new AxisMarker("", new Background(Color.Red,Color.Orange,90),70,85);
	AxisMarker am4 = new AxisMarker("", new Background(Color.Orange,Color.Yellow,90),30,50);
	
	// Hide transition marker legend entries.
	am2.LegendEntry.Visible = false;
	am4.LegendEntry.Visible = false;
	
	// Add all the markers.
	Chart.YAxis.Markers.Add(am1, am2, am3, am4, am5);

	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(2);
	for(int a = 0; a < 4; a++)
	{
		Series s = new Series();
		s.Name = "Series " + (a+1);
		for(int b = 0; b < 4; b++)
		{
			Element e = new Element();
			e.Name = "Element " + (b+1);
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
