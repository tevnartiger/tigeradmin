<%@ Page Language="C#" debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="fragment" Tagname= "chart" Src="fragmentcache.ascx" %>


<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	DateTime Dt = DateTime.Now;
	Response.Write("This page was created at "+Dt.ToString());
    
    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Fragmnet Cache Sample</title>
</head>
<body>
<div style="text-align:center">
 <fragment:chart id="myChart" runat="server"/>
 <font size="2" face="Arial">Refresh this chart, it will be cached for 10 seconds then a new chart will be created. You will also see that page render itself is not cached.  You will see the page time increase while the chart time does not until 10 seconds pass. See <a href="outputcache.aspx">outputcache</a> for a sample where both the chart image and page are cached together.
 </font> 
</div>
</body>
</html>
