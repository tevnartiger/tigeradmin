<%@ Page Language="C#" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	// This sample demonstrates the use of financial indicator Finite Impulse Response 
	// The Financial Chart
	FinancialChart.Title="Company X Financial Analysis";
	FinancialChart.TempDirectory="temp";
	FinancialChart.Debug=true;
	FinancialChart.LegendBox.Template ="%icon %name";
	FinancialChart.Size="850X400";

	FinancialChart.YAxis.Label.Text = "Price (USD)";
	FinancialChart.YAxis.FormatString = "currency";
	FinancialChart.YAxis.Scale = Scale.Range;
	FinancialChart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	FinancialChart.DefaultSeries.DefaultElement.Marker.Visible = false;
	FinancialChart.DefaultSeries.Type = SeriesType.Spline;
	FinancialChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical;
	
	
	// Modify the x axis labels.
	FinancialChart.XAxis.Scale = Scale.Time;
	FinancialChart.XAxis.TimeInterval = TimeInterval.Day;
FinancialChart.XAxis.TimeIntervalAdvanced.Multiplier = 5;
	
	FinancialChart.XAxis.TimeScaleLabels.DayFormatString = "o";
	FinancialChart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Month);
	FinancialChart.XAxis.TimeScaleLabels.MonthFormatString = "MMM";
	// For Finite Impulse Response indicator the time scale is inverted (i.e. the first element of the series is the newest)
	FinancialChart.XAxis.InvertScale = true; 
	
	DataEngine priceDataEngine = new DataEngine ();
	priceDataEngine.ChartObject = FinancialChart;
	priceDataEngine.ChartType = ChartType.Financial;
	priceDataEngine.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];
	priceDataEngine.DateGrouping = TimeInterval.Day;
	priceDataEngine.StartDate = new DateTime (2002,1,1,0,0,0);
	priceDataEngine.EndDate = new DateTime (2002,2,28,23,59,59);
	// Here we import data from the FinancialCompany table from within chartsample.mdb
	priceDataEngine.SqlStatement = @"SELECT TransDate, HighPrice, LowPrice, OpenPrice, ClosePrice FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate DESC";
	priceDataEngine.DataFields = "xAxis=TransDate,High=HighPrice,Low=LowPrice,Open=OpenPrice,Close=ClosePrice";

	SeriesCollection sc = priceDataEngine.GetSeries ();
	Series prices = null;
	if(sc.Count>0)
		prices = sc[0];
	else
		return;
	prices.Name = "Prices";
	//prices.DefaultElement.ShowValue=true;
	prices.DefaultElement.ToolTip="L:%Low | H:%High";
	prices.DefaultElement.SmartLabel.Text="O:%Open | C:%Close";
	prices.Type = SeriesTypeFinancial.Bar;

	CalendarPattern cp = new CalendarPattern (TimeInterval.Day, TimeInterval.Week, "0000001");
	prices.Trim (cp, ElementValue.XDateTime);
	
	FinancialChart.SeriesCollection.Add (prices);
	
	// AveragePrice financial indicator which is arithmetic average of the High, Low, Close and Open price 
	// for a trading day. 
	Series averagePrice = FinancialEngine.AveragePrice(prices);
	averagePrice.DefaultElement.Color = Color.FromArgb(49,99,49);
	FinancialChart.SeriesCollection.Add (averagePrice);
	
	// Finite Impulse Response financial indicator.
	double[] weights = new double[] {0.1, 0.2, 0.3, 0.4};
	Series firIndicator = FinancialEngine.FiniteImpulseResponse(prices, ElementValue.High, weights);
	firIndicator.DefaultElement.Color = Color.FromArgb(126,125,255);
	FinancialChart.SeriesCollection.Add (firIndicator);
		        
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="FinancialChart" runat="server"/>			
		</div>
	</body>
</html>
