<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Palette = new Color[]{Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)};
	
	Chart.DefaultSeries.Type = SeriesType.Line;
	Chart.Size = "600x350";
	Chart.Title = ".netCHARTING Sample";
	Chart.LegendBox.Visible = false;
	Chart.XAxis.Scale = Scale.Time;
	Chart.XAxis.FormatString="MM/dd/yy";

	
	// This sample demonstrates accessing elements within a database populated without the use of the dataEngine.	
    Series s = new Series();
    s.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];
    s.StartDate=new System.DateTime(2002,1,1,0,0,0);
    s.EndDate = new System.DateTime(2002,1,31,23,59,59);
    s.SqlStatement= "SELECT OrderDate, Total FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY Orders.OrderDate";


   for(int i = 0; i < 70; i++)
    {
		 s.Elements[i].Color = Color.FromArgb(210,((i*2)%255),((i+50)%255));

    }
    
    Chart.SeriesCollection.Add(s);
     
}

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	</head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
