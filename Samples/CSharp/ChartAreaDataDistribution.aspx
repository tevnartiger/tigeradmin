<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

void Page_Load(Object sender,EventArgs e)
{
	Chart.Type = ChartType.Combo;
	Chart.Width = 600;
	Chart.Height = 400;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = "Sample Data";
	
	SeriesCollection mySC = getRandomData();
	Chart.SeriesCollection.Add(mySC);
	Chart.DefaultSeries.Type = SeriesType.Spline;
Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend;
	
	//StatisticalEngine.Options.MatchColors = true;
	ChartAreaCollection dataDistribution = StatisticalEngine.DataDistributionChart(mySC);
    foreach(ChartArea ca in dataDistribution)
    {
        ca.DefaultSeries.LegendEntry.Visible = false;
Series tmp = new Series();
tmp.YAxis = new Axis();
tmp.YAxis.AlternateGridBackground.Color = Color.Empty;
tmp.YAxis.SynchronizeScale.Add(ca.YAxis);
ca.SeriesCollection.Add(tmp);
        ca.YAxis.ClearValues = true;
        ca.WidthPercentage = 12;
        ca.ClearColors();
    }
	Chart.ExtraChartAreas.Add (dataDistribution);
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(2);
	for(int a = 0; a < 2; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 0; b < 30; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[1].DefaultElement.Color = Color.FromArgb(0,156,255);
	//SC[2].DefaultElement.Color = Color.FromArgb(49,255,49);
	//SC[3].DefaultElement.Color = Color.FromArgb(255,255,0);

	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
