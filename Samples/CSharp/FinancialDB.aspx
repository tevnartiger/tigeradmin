<%@ Page Language="C#" Debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    Chart.Title="Financial chart";
    Chart.Type=ChartType.Financial;
    Chart.XAxis.Scale = Scale.Time;
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.ShadingEffect = true;
    Chart.LegendBox.Position = LegendBoxPosition.None;
    Chart.Size="600X500";
    
    
    // Set the financial chart type
	Chart.DefaultSeries.Type = SeriesTypeFinancial.CandleStick;
	Chart.DefaultSeries.DefaultElement.ShowValue=true;
	Chart.DateGrouping = TimeInterval.Days;
	
	
	//Customize volume area
	Chart.ChartArea.Background.Color = Color.LightBlue;
    Chart.ChartArea.Line.Color=Color.Yellow;
    Chart.ChartArea.InteriorLine.Color=Color.Blue;
    Chart.ChartArea.Label.Text="Sales volume";
  	Chart.ChartArea.XAxis.FormatString = "d";
	Chart.ChartArea.XAxis.Label.Text ="Days";
	

	// Set the time padding for the x axis.
	Chart.ChartArea.XAxis.TimePadding = new TimeSpan(2,0,0,0);
   
    
    Chart.Series.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
    Chart.Series.StartDate=new DateTime (2003,6,1,0,0,0);
    Chart.Series.EndDate = new DateTime (2003,6,30,23,59,59);
   
	Chart.Series.SqlStatement= @"SELECT TransDate,volume,price FROM Financial WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate"; 
	Chart.Series.DataFields="xAxis=TransDate,price=price,yAxis=volume";
    Chart.SeriesCollection.Add();
   
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>.netCHARTING Sample</title>
</head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
