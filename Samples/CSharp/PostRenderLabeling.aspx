<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="C#" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Text" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Post Render Drawing Image</title>
		<script runat="server">


void Page_Load(Object sender,EventArgs e)
{
	//dotnetCHARTING.Chart Chart = new dotnetCHARTING.Chart();

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.DefaultSeries.Type = SeriesType.Bubble;
	Chart.Title = "Label elements after the chart is generated.";
	
	// This sample shows how element can be labeled after the chart image was created.
	
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection s = getRandomData();
		
	// Add the random data.
	Chart.SeriesCollection.Add(s);	
	
	
	//Get a bitmap of the chart.
	Bitmap bmp = Chart.GetChartBitmap();
	
	// Create a graphics object from the bitmap
	Graphics g = Graphics.FromImage(bmp);
	g.TextRenderingHint = TextRenderingHint.AntiAlias;
	
	
	// After the chart is generated, the element's positions are available.
	// Lets iterate the elements we have added and draw a centered label at their positions.
	
	// String format to center the labels.
	StringFormat sf = new StringFormat();
	sf.Alignment = StringAlignment.Center;
	sf.LineAlignment = StringAlignment.Center;
	
	for(int i = 0; i < s.Count; i++)
	{
		for(int ib = 0; ib < s[i].Elements.Count;ib++)
		{
			Element el = s[i].Elements[ib];
			g.DrawString(el.Name,new Font("Arial",8),Brushes.Black,el.Point,sf);
			
		}
	}

	// Clean up
	g.Dispose();
	
	//Save the image using the FileManager.
	Chart.FileManager.SaveImage(bmp);

}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();

	for(int a = 0; a < 1; a++)
	{
		Series s = new Series();
		s.Name = "Series " + (a+1);
		for(int b = 0; b < 4; b++)
		{
			{
				Element e = new Element("Element " + b, myR.Next(50));
				e.BubbleSize = myR.Next(50);
				s.Elements.Add(e);
			}
		}
		SC.Add(s);
	}
	return SC;
}


        </script>
	</head>
	<body>
		<div style="text-align:center">

           <dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px" Depth="13px" Use3D="True">
			</dotnet:Chart>
			
		</div>
	</body>
</html>
