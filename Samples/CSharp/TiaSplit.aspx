<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<script runat="server">

void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.Combo;//Horizontal;
	Chart.Size = "600x350";
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = ".netCHARTING Sample";
	Chart.ChartArea.Label.Text = "Splitting a series by a TimeIntervalAdvanced object.";
	
	// This sample demonstrates splitting a single series into multiple series based on a TimeIntervalAdvanced object.
	
	Chart.DefaultSeries.Type = SeriesType.AreaLine;
	Chart.DefaultElement.Marker.Visible = false;
	Chart.LegendBox.DefaultEntry.Value = "%Sum";
		
	// *DYNAMIC DATA NOTE* 
	// This sample uses random data to populate the chart. To populate 
	// a chart with database data see the following resources:
	// - Classic samples folder
	// - Help File > Data Tutorials
	// - Sample: features/DataEngine.aspx
	SeriesCollection mySC = getRandomData();
	
	// Add the random data.
	Chart.SeriesCollection.Add(mySC[0].Split(TimeIntervalAdvanced.Month));
        
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
	DateTime dt = new DateTime(2006,1,1);
	for(int a = 1; a < 2; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a.ToString();
		for(int b = 1; b < 200; b++)
		{
			Element e = new Element();
			//e.Name = "Element " + b.ToString();
			e.YValue = myR.Next(50);
			e.XDateTime = dt = dt.AddDays(1);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	return SC;
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	</head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
