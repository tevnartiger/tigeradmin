<%@ Page Language="C#" Debug="true" Trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">


void Page_Load(Object sender,EventArgs e)
{
	//Demonstrates streaming a chart image directly to the browser with all HTML code.
	//Since streaming turns the page you include the control on into the actual
	// image file return, the browser will not also allow HTML within the same http
	//request. As a result url, tooltip and drilldown options will not be
	//enabled. Setting UseSession to true works around this issue by returning an
	//image reference to the same aspx page for image streaming. 

	Chart.Title="My Chart";
	Chart.TempDirectory="temp";
	Chart.DefaultSeries.DefaultElement.Hotspot.ToolTip="%Name: %Value";

	//Enable streaming
	Chart.UseFile =false;
	Chart.UseSession=true;
	
	// Change the shading mode
	Chart.ShadingEffectMode = ShadingEffectMode.Two;

	// Set the x axis label
	Chart.ChartArea.XAxis.Label.Text="X Axis Label";

	// Set the y axis label
	Chart.ChartArea.YAxis.Label.Text="Y Axis Label";

	// Set he chart size.
	Chart.Width = 600;
	Chart.Height = 350;

	// Add the random data.
	Chart.SeriesCollection.Add(getRandomData());
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random(1);
	for(int a = 1; a < 5; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a.ToString();
		for(int b = 1; b < 6; b++)
		{
			Element e = new Element();
			e.Name = "E " + b.ToString();
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	SC[2].DefaultElement.Color = Color.FromArgb(255,99,49);
	SC[3].DefaultElement.Color = Color.FromArgb(0,156,255);
	return SC;
	
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Use session Sample</title></head>
<body>
<div style="text-align:center">
<dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
