<%@ Page Language="C#" debug="true" trace="false" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>

<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//This sample demonstrates how to use XPath to get data fields from an xml file for generating a chart.
	
	//set Title 
    Chart.Title="sales report";
    
    // Set the x axis label
	Chart.ChartArea.XAxis.Label.Text="X Axis Label";
	
	Chart.XAxis.FormatString = "MMM d";

	// Set the y axis label
	Chart.ChartArea.YAxis.Label.Text="Y Axis Label";

    Chart.TempDirectory="temp";
    Chart.Debug=true;
    
    
   //Adding series programatically
   	Chart.Series.Name = "Sales";
    Chart.Series.DataFields="xaxis=//Customers/Customer/Name,yaxis=//Customers/Customer/Total";
   	Chart.Series.Data = "../../database/Orders.xml";
    Chart.SeriesCollection.Add();
                       
   
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>XML XPath Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
