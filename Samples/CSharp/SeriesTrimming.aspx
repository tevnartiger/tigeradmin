<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">




void Page_Load(Object sender,EventArgs e)
{

	Chart.Type = ChartType.ComboSideBySide;//Horizontal;
	Chart.Width = 600;
	Chart.Height = 350;
	Chart.TempDirectory = "temp";
	Chart.Debug = true;
	Chart.Title = "Series.Trim() Method";
	Annotation a = new Annotation();
	a.Position = new Point(300,70);
	a.ClearColors();
	a.Label.Text = "Series 2 is trimmed to only leave elements with y values between 15 and 30"; 
	Chart.Annotations.Add(a);
	
	
	// This sample will demonstrate how to trim elements from a series.
	
	// First we get our data, if you would like to get the data from a database you need to use
	// the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
	SeriesCollection mySC = getRandomData();
	
	// mySC is a series collection that contains two series
	
	// The first series will be shown in it's entirety.
	// The second is trimmed to only leave elements with y values between 20 and 30
	mySC[1].Trim(15,30,ElementValue.YValue);
	
	// Add a marker to the y axis to show the range of vales not trimmed from the second series.
   AxisMarker M=new AxisMarker();
   M.LegendEntry.Name = "Active zone";
   M.Background = new Background(Color.FromArgb(100,Color.Red));
   M.ValueHigh = 30;
   M.ValueLow = 15;
   Chart.YAxis.Markers.Add(M);

	// Add the random data.
	Chart.SeriesCollection.Add(mySC);
    
}

SeriesCollection getRandomData()
{
	SeriesCollection SC = new SeriesCollection();
	Random myR = new Random();
	for(int a = 1; a < 3; a++)
	{
		Series s = new Series();
		s.Name = "Series " + a;
		for(int b = 1; b < 51; b++)
		{
			Element e = new Element();
			e.Name = "Element " + b;
			//e.YValue = -25 + myR.Next(50);
			e.YValue = myR.Next(50);
			s.Elements.Add(e);
		}
		SC.Add(s);
	}

	// Set Different Colors for our Series
	SC[0].DefaultElement.Color = Color.FromArgb(49,255,49);
	SC[1].DefaultElement.Color = Color.FromArgb(255,255,0);
	return SC;
}
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
