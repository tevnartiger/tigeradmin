<%@ Page Language="C#" Debug="true" Trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
void Page_Load(Object sender,EventArgs e)
{
	//set global properties
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings["DNCConnectionString"];  
   	Chart.Title="Dietary Breakdown";
    Chart.XAxis.Label.Text="Users";
    Chart.YAxis.Label.Text="Protein, Carbs and Fat";
    Chart.TempDirectory="temp";
    Chart.Debug=true;
    Chart.DefaultSeries.DefaultElement.ToolTip="%yvalue";
    Chart.PaletteName = Palette.Two;
     
          
 
    //Add a series   
    Chart.Series.SqlStatement= @"SELECT Name,Protein,Carbs,Fat FROM Eaten ORDER BY Name";  							
    Chart.Series.DataFields ="xAxis=Name,yAxis=Protein,yAxis=Carbs,yAxis=Fat";
    Chart.SeriesCollection.Add();
    
}
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Multi Series From Columns Sample</title>
</head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
 
</div>
</body>
</html>
