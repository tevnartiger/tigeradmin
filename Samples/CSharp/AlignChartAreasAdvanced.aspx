<%@ Page Language="C#" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

    void Page_Load(Object sender, EventArgs e)
    {
        // This sample demonstrates how to align two different charts.

        // Unaligned Charts
        SetChart(Chart);
        SetChart(Chart1);
        Chart.DefaultSeries.Type = SeriesType.AreaLine;
        Chart1.YAxis.SmartScaleBreak = true;
        Chart1.SeriesCollection[0][0].YValue = 5000;
        Chart1.SeriesCollection[0].DefaultElement.Color = Color.FromArgb(255, 255, 0);
        
        // The original charts are now ready to be drawn. This will be invoked now in order to get access to the chart area coordinates.
        Chart.FileManager.SaveImage();
        Chart1.FileManager.SaveImage();
        Rectangle ca1 = Chart.ChartArea.GetRectangle();
        Rectangle ca2 = Chart1.ChartArea.GetRectangle();
        
        // Based on the rectangles these chart areas produce, the margins will be calculated to offset the differences on the next two charts.
        // If using only two charts, these corrections can be applied to the original charts and calling Chart.FileManager.SaveImage() again will update them.

        if (ca1.Left > ca2.Left)
            Chart3.MarginLeft += ca1.Left - ca2.Left;
        else
            Chart2.MarginLeft += ca2.Left - ca1.Left;

        if (ca1.Right < ca2.Right)
            Chart3.MarginRight += ca2.Right - ca1.Right;
        else
            Chart2.MarginRight += ca1.Right - ca2.Right;

        SetChart(Chart2);
        SetChart(Chart3);
        Chart2.DefaultSeries.Type = SeriesType.AreaLine;
        Chart3.YAxis.SmartScaleBreak = true;
        Chart3.SeriesCollection[0][0].YValue = 5000;
        Chart3.SeriesCollection[0].DefaultElement.Color = Color.FromArgb(255, 255, 0);

        Chart2.Title = ".netCHARTING (Aligned)";
        Chart3.Title = ".netCHARTING (Aligned)";
    }

    void SetChart(Chart c) // Shared chart settings.
    {
        c.Type = ChartType.Combo;
        c.Size = "500x250";
        c.Title = ".netCHARTING Not Aligned";
        c.TempDirectory = "temp";
        c.Mentor = false;
        c.Debug = true;
        c.Palette = new Color[] { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) };

        // *DYNAMIC DATA NOTE* 
        // This sample uses random data to populate the chart. To populate 
        // a chart with database data see the following resources:
        // - Use the getLiveData() method using the dataEngine to query a database.
        // - Help File > Getting Started > Data Tutorials
        // - DataEngine Class in the help file	
        // - Sample: features/DataEngine.aspx

        SeriesCollection mySC = getRandomData();

        // Add the random data.
        c.SeriesCollection.Add(mySC);
    }

    SeriesCollection getRandomData()
    {
        Random myR = new Random(1);
        SeriesCollection SC = new SeriesCollection();
        for (int a = 1; a < 2; a++)
        {
            Series s = new Series("Series " + a.ToString());
            for (int b = 1; b < 5; b++)
            {
                Element e = new Element("Element " + b.ToString());
                e.YValue = myR.Next(50);
                s.Elements.Add(e);
            }
            SC.Add(s);
        }
        return SC;
    }

    SeriesCollection getLiveData()
    {
        DataEngine de = new DataEngine("ConnectionString goes here");
        de.ChartObject = Chart; // Necessary to view any errors the dataEngine may throw.
        de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ....";
        return de.GetSeries();
    }

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>.netCHARTING Sample</title>
</head>
<body>
    <div align="center">
        <table style="width: 100%">
            <tr>
                <td style="width: 50%">
                    <dotnet:Chart ID="Chart" runat="server" />
                    <dotnet:Chart ID="Chart1" runat="server" />
                </td>
                <td style="width: 50%">
                    <dotnet:Chart ID="Chart2" runat="server" />
                    <dotnet:Chart ID="Chart3" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
