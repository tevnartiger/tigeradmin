<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


		    Sub Page_Load(ByVal sender As [Object], ByVal e As EventArgs)
		        Chart.NoDataLabel.Text = "Please run the SaveState.aspx sample to generate the xml file."
		        ' This sample will show how chart data and settings can be loaded from a saved xml file.
		        ' Load the chart data and settings from the myChart.xml in the temp directory and use it for this chart.
		        Chart.TempDirectory = "temp"
			Chart.LoadState("temp/myChart.xml")
   
		        'Change some labels
		        Chart.ChartArea.Label.Text = "This chart is loaded from file 'myChart.xml'."
		        If Chart.Annotations.Count = 1 Then
		            Chart.Annotations(0).Label.Text = "Go to the sample that saves the myChart.xml file"
		            Chart.Annotations(0).URL = "SaveState.aspx"
		        End If
		    End Sub 'Page_Load 

</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server">
			</dotnet:Chart>
		</div>
	</body>
</html>
