<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.Type = ChartType.Combo
   Chart.Size = "600x400"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "Sample Data"
   
   Dim mySC As SeriesCollection = getRandomData()
   Chart.SeriesCollection.Add(mySC)
   Chart.DefaultSeries.Type = SeriesType.Spline
   Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   
   'StatisticalEngine.Options.MatchColors = true;
   Dim dataDistribution As ChartAreaCollection = StatisticalEngine.DataDistributionChart(mySC)
   Dim ca As ChartArea
   For Each ca In  dataDistribution
      ca.DefaultSeries.LegendEntry.Visible = False
      Dim tmp As New Series()
      tmp.YAxis = New Axis()
      tmp.YAxis.AlternateGridBackground.Color = Color.Empty
      tmp.YAxis.SynchronizeScale.Add(ca.YAxis)
      ca.SeriesCollection.Add(tmp)
      ca.YAxis.ClearValues = True
      ca.WidthPercentage = 12
      ca.ClearColors()
   Next ca
   Chart.ExtraChartAreas.Add(dataDistribution)
End Sub 'Page_Load


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(2)
   Dim a As Integer
   For a = 0 To 1
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 0 To 29
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   'SC[2].DefaultElement.Color = Color.FromArgb(49,255,49);
   'SC[3].DefaultElement.Color = Color.FromArgb(255,255,0);
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
