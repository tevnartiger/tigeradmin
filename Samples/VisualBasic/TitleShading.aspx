<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates shading the title box using ShadingEffectMode.Five.
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.Type = ChartType.Gauges
		Chart.Size = "650x350"
		Chart.Use3D = True
		Chart.DefaultSeries.GaugeType = GaugeType.Bars
		Chart.ShadingEffectMode = ShadingEffectMode.Two

		Chart.ChartArea.Background.Color = Color.White
		Chart.Background.Color = Color.Transparent

		Chart.ChartArea.CornerBottomRight = BoxCorner.Round
		Chart.ChartArea.CornerBottomLeft = BoxCorner.Round

		Chart.TitleBox.Label = New dotnetCHARTING.Label("", New Font("Arial", 13, FontStyle.Bold), Color.White)
		Chart.Title = ".netCHARTING Titlebox"

		' TitleBox Customization
		Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
		Chart.TitleBox.CornerTopLeft = BoxCorner.Round
		Chart.TitleBox.CornerTopRight = BoxCorner.Round
		Chart.TitleBox.Label.Shadow.Color = Color.FromArgb(100, 0, 0, 0)
		Chart.TitleBox.Label.Shadow.Depth = 1
		Chart.TitleBox.Background.ShadingEffectMode = ShadingEffectMode.Five
		Chart.TitleBox.Label.Color = Color.White
		Chart.TitleBox.Background.Color = Color.FromArgb(100, 225, 165, 50)

		Chart.LegendBox.DefaultCorner = BoxCorner.Round
		Chart.LegendBox.HeaderLabel.Alignment = StringAlignment.Center
		Chart.LegendBox.HeaderBackground.ShadingEffectMode = ShadingEffectMode.Two
		Chart.LegendBox.HeaderBackground.Color = Color.FromArgb(0, 180, 255)
		Chart.LegendBox.HeaderLabel.Shadow.Color = Color.Gray
		Chart.LegendBox.HeaderLabel.Shadow.Depth = 1
		Chart.LegendBox.Background.Color = Color.White

		Chart.DefaultSeries.Background.Color = Color.White
		Chart.DefaultElement.SmartLabel.Alignment = LabelAlignment.Center
		Chart.DefaultSeries.GaugeBorderBox.Padding = 5

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random()
		Dim SC As SeriesCollection = New SeriesCollection()
		For a As Integer = 1 To 4
			Dim s As Series = New Series("Series " & a.ToString())
			s.Background.Color = Color.FromArgb(245, 245, 245)
			For b As Integer = 1 To 4
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
