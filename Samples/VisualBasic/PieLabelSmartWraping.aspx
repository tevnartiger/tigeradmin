<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<script runat="server">
Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.Palette = New Color(){Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)}

	' This sample demonstrates smart pie label wraping.	

	' Activate wrapping.
	Chart.DefaultElement.SmartLabel.AutoWrap = True

	Chart.Type = ChartType.Pies
	Chart.Size = "600x340"
	Chart.Title = ".netCHARTING Sample"
	Chart.DefaultElement.SmartLabel.Text = "This is a very long label"
	Chart.Title = "Notice how with smart pie label wraping, the labels on the left side don't wrap because if they did there may not be enough vertical room to display them."

	Chart.DefaultElement.ShowValue = True
	Chart.PieLabelMode = PieLabelMode.Outside
	Chart.ShadingEffectMode = ShadingEffectMode.Two

	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Help File > Getting Started > Data Tutorials
	' - DataEngine Class in the help file	
	' - Sample: features/DataEngine.aspx

	Dim mySC As SeriesCollection = getRandomData()

	' Add the random data.
	Chart.SeriesCollection.Add(mySC)
	mySC(0)(2).YValue = 100

End Sub

Function getRandomData() As SeriesCollection
	Dim myR As Random = New Random(2)
	Dim SC As SeriesCollection = New SeriesCollection()
	For a As Integer = 1 To 1
		Dim s As Series = New Series("Series " & a.ToString())
		For b As Integer = 1 To 11
			Dim e As Element = New Element("Element " & b.ToString())
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
	Return SC
End Function
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>    </head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
