<%@ Page Language="VB" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>

<script runat="server">
Sub Page_Load(sender As [Object], e As EventArgs)
   'set global properties
   Chart.Title = "Item sales report"
   
   ' Set the x axis label
   Chart.ChartArea.XAxis.Label.Text = "X Axis Label"
   
   ' Set the y axis label
   Chart.ChartArea.YAxis.Label.Text = "Y Axis Label"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   
   'Adding series programatically
   Chart.Series.Name = "Item sales"
   Chart.Series.DataFields = "xAxis=Name,yAxis=Total"
   'If the hash table value is object, the fields can be any property in the object.
   Chart.Series.Data = CreateHashTable()
   
   'For CreateHashTable2, because the key is string , the order of keys in the hash table is unspecified. 
   'Chart.Series.Data = CreateHashTable2();
   'Chart.Series.DataFields="xAxis=key,yAxis=value"; 
   
   'If the value is not object type, the fields can set to key or value.
   'Chart.Series.DataFields="xAxis=value,yAxis=key";
   'Chart.Series.Data = CreateHashTable3()
   
   Chart.SeriesCollection.Add()
End Sub 'Page_Load
 


Function CreateHashTable() As Hashtable
   Dim h As New Hashtable()
   h.Add(1, New Product("Sun", 30))
   h.Add(2, New Product("Mon", 23))
   h.Add(3, New Product("Tue", 33))
   h.Add(4, New Product("Wed", 30))
   h.Add(5, New Product("Thu", 23))
   h.Add(6, New Product("Fri", 40))
   h.Add(7, New Product("Sat", 20))
   Return h
End Function 'CreateHashTable


Function CreateHashTable2() As Hashtable
   Dim h As New Hashtable()
   h.Add("Sun", 30)
   h.Add("Mon", 23)
   h.Add("Tue", 33)
   h.Add("Wed", 30)
   h.Add("Thu", 23)
   h.Add("Fri", 40)
   h.Add("Sat", 20)
   Return h
End Function 'CreateHashTable2

Function CreateHashTable3() As Hashtable
   Dim h As New Hashtable()
   h.Add(1, "Sun")
   h.Add(2, "Mon")
   h.Add(3, "Tue")
   h.Add(4, "Wed")
   h.Add(5, "Thu")
   h.Add(6, "Fri")
   h.Add(7, "Sat")
   Return h
End Function 'CreateHashTable3
 _


Public Class Product
   Private nameField As String
   Private totalField As Double
   
   Public Sub New()
   End Sub 'New
   
   Public Sub New(proName As String, proTotal As Double)
      nameField = proName
      totalField = proTotal
   End Sub 'New
   
   
   
   Public Property Name() As String
      Get
         Return nameField
      End Get
      Set
         nameField = value
      End Set
   End Property
   
   Public Property Total() As Double
      Get
         Return totalField
      End Get
      Set
         totalField = value
      End Set
   End Property
End Class 'Product 

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>hash Table Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>

</div>
</body>
</html>
