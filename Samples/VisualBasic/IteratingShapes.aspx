<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">
 'This sample demonstrates iterating through and customizing individual shapes.
Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.Type = ChartType.Map
   Chart.Size = "600x600"
   Chart.Title = " Customize individual countries in Europe through shape iteration."
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.TitleBox.Position = TitleBoxPosition.Full
   
   Chart.ChartArea.Background.Color = Color.LightBlue
   Chart.Mapping.ZoomPercentage = 134
   
   Chart.Mapping.ZoomCenterPoint = New PointF( 52,12)
   Dim layer As MapLayer = MapDataEngine.LoadLayer("../../Images/MapFiles/europe.shp")
   
   Chart.Mapping.DefaultShape.Label.Text = "%Code"
   Chart.Mapping.DefaultShape.Label.OutlineColor = Color.White
   Chart.Mapping.DefaultShape.Label.Hotspot.ToolTip = "%Cntry_Name"
   
   
   Chart.Mapping.MapLayerCollection.Add(layer)
   
   Dim shape As Shape
   For Each shape In  layer.Shapes
      shape.Background.Mode = BackgroundMode.ImageStretch
      Dim code As String = CStr(shape("code"))
      If code Is Nothing Then
         Exit For
      End If
      Select Case code.Trim()
         Case "FR"
            shape.Background.ImagePath = "../../Images/MapFiles/fr.Png"
            shape.Label.Font = New Font("Verdana", 20)
            shape.Label.OutlineColor = Color.Gold
         
         Case "FI"
            shape.Background.ImagePath = "../../Images/MapFiles/fi.Png"
            shape.Label.Font = New Font("Verdana", 20)
         Case "NO"
            shape.Background.ImagePath = "../../Images/MapFiles/no.Png"
            shape.Label.Font = New Font("Verdana", 20)
         Case "SE"
            shape.Background.ImagePath = "../../Images/MapFiles/se.Png"
            shape.Label.Font = New Font("Verdana", 20)
         Case "EE"
            shape.Background.ImagePath = "../../Images/MapFiles/ee.Png"
         Case "LV"
            shape.Background.ImagePath = "../../Images/MapFiles/lv.Png"
         Case "LT"
            shape.Background.ImagePath = "../../Images/MapFiles/lt.Png"
         Case "PL"
            shape.Background.ImagePath = "../../Images/MapFiles/pl.Png"
            shape.Label.Font = New Font("Verdana", 20)
         Case "SK"
            shape.Background.ImagePath = "../../Images/MapFiles/sk.Png"
         Case "RO"
            shape.Background.ImagePath = "../../Images/MapFiles/ro.Png"
         Case "HU"
            shape.Background.ImagePath = "../../Images/MapFiles/hu.Png"
            shape.Label.OutlineColor = Color.Gold
         
         Case "BG"
            shape.Background.ImagePath = "../../Images/MapFiles/bg.Png"
         Case "YU"
            shape.Background.ImagePath = "../../Images/MapFiles/yu.Png"
            shape.Label.OutlineColor = Color.Gold
         
         Case "AL"
            shape.Background.ImagePath = "../../Images/MapFiles/al.Png"
         Case "BA"
            shape.Background.ImagePath = "../../Images/MapFiles/ba.Png"
         Case "SI"
            shape.Background.ImagePath = "../../Images/MapFiles/si.Png"
         Case "AT"
            shape.Background.ImagePath = "../../Images/MapFiles/at.Png"
            shape.Label.OutlineColor = Color.Gold
         Case "CZ"
            shape.Background.ImagePath = "../../Images/MapFiles/cz.Png"
         Case "DE"
            shape.Background.ImagePath = "../../Images/MapFiles/de.Png"
            shape.Label.Font = New Font("Verdana", 20)
         Case "DK"
            shape.Background.ImagePath = "../../Images/MapFiles/dk.Png"
         Case "NL"
            shape.Background.ImagePath = "../../Images/MapFiles/nl.Png"
         Case "BE"
            shape.Background.ImagePath = "../../Images/MapFiles/be.Png"
         Case "CH"
            shape.Background.ImagePath = "../../Images/MapFiles/ch.Png"
         Case "ES"
            shape.Background.ImagePath = "../../Images/MapFiles/es.Png"
            shape.Label.Font = New Font("Verdana", 20)
         Case "PT"
            shape.Background.ImagePath = "../../Images/MapFiles/pt.Png"
         Case "UK"
            shape.Background.ImagePath = "../../Images/MapFiles/uk.Png"
            shape.Label.Font = New Font("Verdana", 20)
         Case "IE"
            shape.Background.ImagePath = "../../Images/MapFiles/ie.Png"
            shape.Label.Font = New Font("Verdana", 12)
            shape.Label.OutlineColor = Color.Gold
         Case "IT"
            shape.Background.ImagePath = "../../Images/MapFiles/it.Png"
            shape.Label.Font = New Font("Verdana", 12)
            shape.Label.OutlineColor = Color.Gold
         
         Case "VA"
            shape.Background.ImagePath = "../../Images/MapFiles/va.Png"
         Case "HR"
            shape.Background.ImagePath = "../../Images/MapFiles/hr.Png"
         Case "GR"
            shape.Background.ImagePath = "../../Images/MapFiles/gr.Png"
         Case "MD"
            shape.Background.ImagePath = "../../Images/MapFiles/md.Png"
         Case "LU"
            shape.Background.ImagePath = "../../Images/MapFiles/lu.Png"
         Case "MK"
            shape.Background.ImagePath = "../../Images/MapFiles/mk.Png"
         Case Else
            shape.Background.Mode = BackgroundMode.Brush
      End Select
   Next shape 
End Sub 'Page_Load


		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>
