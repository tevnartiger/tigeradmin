<%@ Page Language="vb" Debug="true" Trace="false" Description="dotnetChart Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Web.UI" %>

<script runat="server">

Private myInterval As TimeInterval = TimeInterval.Week


Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   Chart.Use3D = True
   Chart.Size = "600x350"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   ' This sample demonstrates the smart forecast feature.
   ' Fullscreen
   If Not IsPostBack Then
      ' Setup a dropdown
      DropDownForecast.Items.Add("Week")
      DropDownForecast.Items.Add("Month")
      DropDownForecast.Items.Add("Quarter")
   End If
   
   
   ' Load form values.    
   myInterval = CType([Enum].Parse(GetType(dotnetCHARTING.TimeInterval), DropDownForecast.SelectedItem.Value, True), dotnetCHARTING.TimeInterval)
   
   ' The smart forecast setting takes a time interval advanced object representing the time interval of the forcast.
   Chart.SmartForecast = New TimeIntervalAdvanced(myInterval, 1)
   
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   Dim s As Series = mySC(0)
   
   ' Empty Element settings control the apperance of the forecast series.
   s.EmptyElement.Color = Color.FromArgb(150, Chart.Palette(0))
   s.EmptyElement.Line.Color = Color.FromArgb(93, 92, 153)
   s.EmptyElement.Hotspot.ToolTip = "%Value"
   
   
   s.LegendEntry.Value = "%Sum"
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
   Chart1.SeriesCollection.Add(getRandomData())
   
   Chart1.Title = "Long-term data used with the above chart without SmartForecasting."
   Chart1.ChartArea.Label.Text = "This data is used with the above " + ControlChars.Lf + "chart to improve forecasting accuracy."
   Chart1.Size = "600x200"
   Chart1.TempDirectory = "temp"
   Chart1.Debug = True
   Chart1.LegendBox.Visible = False
End Sub 'Page_Load
 

Sub ChangeForecast(sender As [Object], e As EventArgs)
   myInterval = CType([Enum].Parse(GetType(dotnetCHARTING.TimeInterval), DropDownForecast.SelectedItem.Value, True), dotnetCHARTING.TimeInterval)
End Sub 'ChangeForecast


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(2)
   Dim dt As DateTime = CType(DateTime.Today.Subtract(TimeSpan.FromDays(200)), DateTime)
   
   Dim a As Integer
   For a = 1 To 1
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 199
         Dim e As New Element()
         'e.Name = "Element " + b.ToString();
         e.YValue = myR.Next(50) + b
         dt = dt.AddDays(1)
         e.XDateTime = dt
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   
   Return SC
End Function 'getRandomData

</script>
		<html>
	<head>
		<title>.netCHARTING Sample</title>
<script language="JavaScript">
function submitform()
{
	document.Form1.submit();
}
</script> 

	</head>
	<body>
		<div align="center">
		
		<form id="Form1"  runat=server>
			Forecast Setting: <ASP:DropDownList id="DropDownForecast" OnSelectedIndexChanged="ChangeForecast" onChange="Javascript: submitform()" runat=server></ASP:DropDownList>
			</form>
		
			<dotnet:Chart id="Chart" runat="server" />
			<dotnet:Chart id="Chart1" runat="server" />
			
		</div>
	</body>
</html>
