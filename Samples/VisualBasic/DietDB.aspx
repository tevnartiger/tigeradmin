<%@ Page Language="VB" debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   'set global properties
   Chart.Title = "Dietary Breakdown"
   Chart.TempDirectory = "temp"
   Chart.Size = "700X400"
   Chart.DateGrouping = TimeInterval.Days
   Chart.ChartArea.YAxis.Scale = Scale.FullStacked
   Chart.XAxis.FormatString = "MMM d"
   Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   Chart.YAxis.Label.Text = "Protein, Carbs and Fat"
   
   
   'Add visual effect, set colors and tooltip on mouseover
   Chart.ShadingEffectMode = ShadingEffectMode.Three
   Chart.PaletteName = Palette.Two
   Chart.LegendBox.Template = "%name %icon"
   
   Chart.DefaultSeries.DefaultElement.ShowValue = True
   
   'Add series
   Chart.Series.Name = "Protein"
   Chart.Series.SqlStatement = "SELECT EatenDate,Sum(Protein) FROM Eaten GROUP BY EatenDate"
   Chart.SeriesCollection.Add()
   
   Chart.Series.Name = "Carbs"
   Chart.Series.SqlStatement = "SELECT EatenDate,Sum(Carbs) FROM Eaten GROUP BY EatenDate"
   Chart.SeriesCollection.Add()
   
   Chart.Series.Name = "Fat"
   Chart.Series.SqlStatement = "SELECT EatenDate,Sum(Fat) FROM Eaten GROUP BY EatenDate"
   Chart.SeriesCollection.Add()
   
   Chart.Series.Name = "Calories"
   Chart.Series.Type = SeriesType.Line
   Chart.Series.Line.Width = 2
   Chart.Series.SqlStatement = "SELECT EatenDate,Sum(Calories) FROM Eaten GROUP BY EatenDate"
   'Bind Calories series to second Y axis
   Dim CalTotal As New Axis()
   CalTotal.Orientation = dotnetCHARTING.Orientation.Right
   CalTotal.Label.Text = "Calories"
   Chart.Series.YAxis = CalTotal
   Chart.SeriesCollection.Add()
End Sub 'Page_Load 

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Date Grouping By Days Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
