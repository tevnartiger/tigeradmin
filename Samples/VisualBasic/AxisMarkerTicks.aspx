<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender as Object ,e as EventArgs )


	Chart.Type = ChartType.Combo
	Chart.Width = new unit(600)
	Chart.Height = new unit(350)
	Chart.TempDirectory = "temp"
	Chart.Debug = true
	Chart.Title = "AxisMaker ticks"
	
	' This sample will demonstrate how to use AxisTicks associated with AxisMarkers.
	
	' 1. CREATE AND ADD AXIS MARKERS	
	' A range marker
	Dim am as AxisMarker  = new AxisMarker("My range",new Background(Color.Red),5,30)

	Chart.YAxis.Markers.Add(am)	
	
	' A single value marker.
	Dim am2 as AxisMarker = new AxisMarker("Maximum",new Line(Color.Blue,2),40)

	Chart.YAxis.Markers.Add(am2)

	' 2. INSTANTIATE AXIS TICKS FOR MARKERS
	' This code will generate axis ticks for the markers at their positions.
	am.Tick = new AxisTick()
	am.Tick.Label.Text = "My Range"
	am.Tick.Line.Color = Color.Red	
	am2.Tick = new AxisTick()	
	am2.Tick.Label.Text = "Maximum"	
	
	' 3. ADD DATA
	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Classic samples folder
	' - Help File > Data Tutorials
	' - Sample: features/DataEngine.aspx
	Chart.SeriesCollection.Add(getRandomData())
    
End Sub



Function  getRandomData() As SeriesCollection

	Dim SC As SeriesCollection  = new SeriesCollection()
	Dim myR AS Random  = new Random()
	Dim a,b as Integer
	Dim s as Series 
	Dim e As Element
		
	For a = 0 To 3
	
		s = new Series()
		s.Name = "Series " & a
		For b = 0 To 3
		
			e = new Element()
			e.Name = "Element " & b
			'e.YValue = -25 & myR.Next(50)
			e.YValue = myR.Next(50)'
			s.Elements.Add(e)'
		Next b
		SC.Add(s)
	Next a

	' Set Different Colors for our Series
	SC(0).DefaultElement.Color = Color.FromArgb(49,255,49)
	SC(1).DefaultElement.Color = Color.FromArgb(255,255,0)
	SC(2).DefaultElement.Color = Color.FromArgb(255,99,49)
	SC(3).DefaultElement.Color = Color.FromArgb(0,156,255)

	return SC
End Function
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
