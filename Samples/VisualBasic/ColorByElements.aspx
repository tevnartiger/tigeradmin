<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">


Function getRandomData() As Series
   Dim myR As New Random()
   
   Dim s As New Series()
   s.Name = "Clicks"
   Dim b As Integer
   For b = 1 To 7
      Dim e As New Element()
      e.Name = "Element " + CStr(b)
      e.YValue = myR.Next(50)
      s.Elements.Add(e)
   Next b
   
   Return s
End Function 'getRandomData



Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.TempDirectory = "temp"
   Chart.Debug=true
   
   ' This sample wil demonstrate how you can color an element in each series so you can do without the x axis.
   Dim mySeries As Series = getRandomData()
   
   ' By setting a palette to this series it will instruct the chart engine to color each of it's elements
   ' and place each element into the legend and remove the series.
   mySeries.PaletteName = Palette.Two
   
   ' Now we can remove the x axis by hiding it.
   Chart.XAxis.ClearValues = True
        Chart.XAxis.DefaultTick.Line.Length = 0
   
   ' For fun lets also add some shading to the bars.
   Chart.ShadingEffectMode = ShadingEffectMode.Two
   
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySeries)
End Sub 'Page_Load 
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Color By Element</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
