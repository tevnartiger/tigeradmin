<%@ Page Language="VB" debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 0 To 0
      Dim s As New Series()
      Dim b As Integer
      For b = 0 To 3
         Dim e As New Element()
         e.Name = "Element " & b
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   Return SC
End Function 'getRandomData



Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(75)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.MarginBottom = - 5
   Chart.LegendBox.Padding = 1
   
   
   ' Visual effects.
   Chart.Use3D = True
   Chart.ShadingEffectMode = ShadingEffectMode.One
   
   ' Select horizontal combo graph.
   Chart.Type = ChartType.ComboHorizontal
   
   ' Set full stacked scale for the x axis.
   Chart.XAxis.Scale = Scale.FullStacked
   
   ' Make the bar fill the whole chart area vertically. Default is 16.
   Chart.YAxis.SpacingPercentage = 1
   
   ' Place the legend underneath the chart.
   Chart.LegendBox.Position = LegendBoxPosition.BottomMiddle
   
   ' The following will set axis colors to empty which will make the axis disappear.
   Chart.XAxis.Clear()
   Chart.YAxis.Clear()
   
   ' Set the legend box and chart area colors to empty.
   Chart.LegendBox.ClearColors()
   Chart.ChartArea.ClearColors()
   
   ' Give the bars some transparency 
   Chart.DefaultSeries.DefaultElement.Transparency = 20
   
   ' Get our data, if you would like to get data from a database you need to use
   ' the data engine. See sample: features/dataEngine.aspx.
   Dim sc As SeriesCollection = getRandomData()
   
   ' Transpose the data because currently we have a single series with 4 elements. After the collection
   ' is transposed we end up with 4 series with a single element each. This is necessary because only
   ' series can be stacked on top of each other.
   sc.Transpose()
   
   ' The bottom legend will only display the name property of legend entries so we set the name template to 
   ' percent of total for each series on the chart.
   Chart.DefaultSeries.LegendEntry.Name = "%PercentOfTotal"
   
   
   ' Add the random data.
   Chart.SeriesCollection.Add(sc)
End Sub 'Page_Load 

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Mini Stacked Bar</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
