﻿<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	' This sample demonstrates adding points of interest icons onto a map based on GPS coordinates.

	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.Palette = New Color(){Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)}

	Chart.Type = ChartType.Map
	Chart.Size = "600x350"
	Chart.Title = ".netCHARTING Sample"



	Chart.Mapping.ZoomCenterPoint = New PointF(41.75f, -88)
	Chart.Mapping.ZoomPercentage = 7500


	Dim layer As MapLayer = MapDataEngine.LoadLayer("../../images/mapFiles/primusa.shp")
	layer.DefaultShape.Background.Color = Color.White
	Chart.Mapping.MapLayerCollection.Add(layer)
	Chart.Mapping.MapLayerCollection.Add("../../images/mapFiles/co17_d00.shp")

	Dim pizza As MapLayer = New MapLayer()
	Dim pizzaMarker As ElementMarker = New ElementMarker("../../images/pin_red.png")
	pizza.AddLatLongPoint(New PointF(41.75f, -87.99f), pizzaMarker)
	pizza.AddLatLongPoint(New PointF(41.79f,-88.07f), pizzaMarker)
	pizza.AddLatLongPoint(New PointF(41.75f, -88.015f), pizzaMarker)
	pizza.AddLatLongPoint(New PointF(41.77f, -87.994f), pizzaMarker)
	Chart.Mapping.MapLayerCollection.Add(pizza)

	Dim deli As MapLayer = New MapLayer()
	Dim deliMarker As ElementMarker = New ElementMarker("../../images/pin_blue.png")
	deli.AddLatLongPoint(New PointF(41.79f, -88.01f), deliMarker)
	deli.AddLatLongPoint(New PointF(41.83f, -88.03f), deliMarker)
	deli.AddLatLongPoint(New PointF(41.83f, -88.008f), deliMarker)
	deli.AddLatLongPoint(New PointF(41.829f, -88.022f), deliMarker)
	Chart.Mapping.MapLayerCollection.Add(deli)

	Dim le As LegendEntry = New LegendEntry("Pizza Restaurants","")
	le.SeriesType = SeriesType.Marker
	le.Marker = pizzaMarker
	Dim le2 As LegendEntry = New LegendEntry("Delis","")
	le2.SeriesType = SeriesType.Marker
	le2.Marker = deliMarker
	Chart.LegendBox.ExtraEntries.Add(le, le2)
	Chart.LegendBox.Visible = True
End Sub

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
