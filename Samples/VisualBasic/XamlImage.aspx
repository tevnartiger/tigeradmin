<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   chartObj.Type = ChartType.Combo
   chartObj.Size = "700x350"
   chartObj.TempDirectory = "temp"
   chartObj.FileName = "xamlImage"
   chartObj.Debug = False
   chartObj.Title = ".netCHARTING Sample"
   chartObj.ImageFormat = ImageFormat.Xaml
   
   chartObj.SeriesCollection.Add(getRandomData())
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
		<dotnet:Chart id="chartObj" runat="server" Width="568px" Height="344px"/>
			<font face="Arial" size="2">To view the generated XAML file you must download the .NET Framework 3.0 
			distribution at http://www.netfx3.com/ and view this sample using Internet Explorer.

			The XAML file has been generated when you ran this sample and is now 
			available in the temp directory.  You can load it directly by visiting 
			<a href="temp/xamlImage.xaml">this link </a> or open the file directly in any program that support XAML.
		</font>
</div>
	</body>
</html>
