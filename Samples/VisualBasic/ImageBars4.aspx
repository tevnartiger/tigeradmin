<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	' This sample demonstrates using ImageBar templates.

	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.Palette = New Color(){Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)}

	Chart.Type = ChartType.Combo
	Chart.Size = "900x350"
	Chart.Title = ".netCHARTING Sample"
	Chart.LegendBox.Visible = False
	Chart.DefaultAxis.SpacingPercentage = 3


	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Help File > Getting Started > Data Tutorials
	' - DataEngine Class in the help file	
	' - Sample: features/DataEngine.aspx

	Dim mySC As SeriesCollection = getRandomData()

	' Add the random data.
	Chart.SeriesCollection.Add(mySC)
	mySC(0).ImageBarTemplate = "../../images/ImageBarTemplates/bamboo"
	mySC(1).ImageBarTemplate = "../../images/ImageBarTemplates/film"
	mySC(2).ImageBarTemplate = "../../images/ImageBarTemplates/tomato"
	mySC(3).ImageBarTemplate = "../../images/ImageBarTemplates/barrel"
	mySC(4).ImageBarTemplate = "../../images/ImageBarTemplates/oildrum"
	mySC(5).ImageBarTemplate = "../../images/ImageBarTemplates/apple"
	mySC(6).ImageBarTemplate = "../../images/ImageBarTemplates/present"
	mySC(7).ImageBarTemplate = "../../images/ImageBarTemplates/paintbucketstacked"
	mySC(8).ImageBarTemplate = "../../images/ImageBarTemplates/pan"
	mySC(9).ImageBarTemplate = "../../images/ImageBarTemplates/paintbucket"
	mySC(10).ImageBarTemplate = "../../images/ImageBarTemplates/paint"
	mySC(11).ImageBarTemplate = "../../images/ImageBarTemplates/boxclosed"
	mySC(12).ImageBarTemplate = "../../images/ImageBarTemplates/glass"
	mySC(13).ImageBarTemplate = "../../images/ImageBarTemplates/banana"
	mySC(14).ImageBarTemplate = "../../images/ImageBarTemplates/gaspump"
	mySC(15).ImageBarTemplate = "../../images/ImageBarTemplates/fireworks"
	mySC(16).ImageBarTemplate = "../../images/ImageBarTemplates/garbagecan"
	mySC(17).ImageBarTemplate = "../../images/ImageBarTemplates/carfront"
	mySC(18).ImageBarTemplate = "../../images/ImageBarTemplates/candycane"
	mySC(19).ImageBarTemplate = "../../images/ImageBarTemplates/box"
	mySC(20).ImageBarTemplate = "../../images/ImageBarTemplates/uparrow"
	mySC(21).ImageBarTemplate = "../../images/ImageBarTemplates/downarrow"
	mySC(22).ImageBarTemplate = "../../images/ImageBarTemplates/toothpaste"
	mySC(23).ImageBarTemplate = "../../images/ImageBarTemplates/cup"
	mySC(24).ImageBarTemplate = "../../images/ImageBarTemplates/garbagecanfull"
	mySC(25).ImageBarTemplate = "../../images/ImageBarTemplates/tapemeasure"
	mySC(26).ImageBarTemplate = "../../images/ImageBarTemplates/ropebrown"
	mySC(27).ImageBarTemplate = "../../images/ImageBarTemplates/carside"
	mySC(28).ImageBarTemplate = "../../images/ImageBarTemplates/bricks"



End Sub

Function getRandomData() As SeriesCollection
	Dim myR As Random = New Random(2)
        Dim SC As SeriesCollection = New SeriesCollection()
        Dim a As Integer
        Dim b As Integer
	For a = 1 To 29
		Dim s As Series = New Series("Series " & a.ToString())
		For b = 1 To 1
			Dim e As Element = New Element("Element " & b.ToString())
			e.YValue = 50+myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
	Return SC
End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
