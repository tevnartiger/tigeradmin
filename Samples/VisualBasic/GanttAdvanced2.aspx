<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Gallery Sample (Time Gantt Chart in 2D)</title>

	<script runat="server">

		Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)

			' This sample demonstrates how 2D bars can overlap.

			Chart.Type = ChartType.ComboHorizontal
			Chart.Size = "600x350"
			Chart.Debug = True
			Chart.TempDirectory = "temp"
			Chart.Title = "Advanced Gantt Chart"

			' Applying the following settings is a trick used to uncluster columns, because it is only supported with 3D, so setting the depth to 0 brings it back to a 2D view.
			Chart.Use3D = True
			Chart.Depth = Unit.Parse(0)
			Chart.YAxis.ClusterColumns = False



			' *DYNAMIC DATA NOTE* 
			' This sample uses static data to populate the chart. To populate 
			' a chart with database data see the following resources:
			' - Classic samples folder
			' - Help File > Data Tutorials
			' - Sample: features/DataEngine.aspx
			Dim sc As SeriesCollection = getData()


			' Add the random data.
			Chart.SeriesCollection.Add(sc)


		End Sub

		Function getData() As SeriesCollection

			' Create two series one for Jack and Jenny. Notice Jack only has two tasks while Jenny has three.

			Dim SC As SeriesCollection = New SeriesCollection()

			Dim s1 As Series = New Series("Jack")
			Dim s2 As Series = New Series("Jenny")

			Dim e1 As Element = New Element()
			e1.Name = "Task 1"
			e1.YDateTimeStart = New DateTime(2000, 1, 1)
			e1.YDateTime = New DateTime(2000, 1, 5)
			e1.Complete = 100

			Dim e2 As Element = New Element()
			e2.Name = "Task 2"
			e2.YDateTimeStart = New DateTime(2000, 1, 5)
			e2.YDateTime = New DateTime(2000, 1, 10)
			e2.Complete = 20

			s1.Elements.Add(e1)
			s1.Elements.Add(e2)

			Dim e3 As Element = New Element()
			e3.Name = "Task 1"
			e3.YDateTimeStart = New DateTime(2000, 1, 5)
			e3.YDateTime = New DateTime(2000, 1, 10)
			e3.Complete = 25

			Dim e4 As Element = New Element()
			e4.Name = "Task 2"
			e4.YDateTimeStart = New DateTime(2000, 1, 10)
			e4.YDateTime = New DateTime(2000, 1, 15)

			Dim e5 As Element = New Element()
			e5.Name = "Task 2"
			e5.YDateTimeStart = New DateTime(2000, 1, 15)
			e5.YDateTime = New DateTime(2000, 1, 20)


			s2.Elements.Add(e3)
			s2.Elements.Add(e4)
			s2.Elements.Add(e5)


			SC.Add(s1)
			SC.Add(s2)


			Return SC
		End Function
	</script>

</head>
<body>
	<div style="text-align: center">
		<dotnet:Chart ID="Chart" runat="server">
		</dotnet:Chart>
	</div>
</body>
</html>
