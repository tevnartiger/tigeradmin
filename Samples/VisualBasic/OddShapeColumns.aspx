<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   SetChart(Chart)
   SetChart(Chart1)
   Chart1.Type = ChartType.Radar
End Sub 'Page_Load
 
' This sample demonstrates using AreaLine series to draw arbitrary rectangles anywhere on a chart. These can be used as 2D axis markers or elements to display data.

Sub SetChart(c As Chart)
   c.TempDirectory = "temp"
   c.Debug = True
   c.Palette = New Color() {Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255)}
   
   c.Type = ChartType.Combo
   c.Size = "600x350"
   c.Title = ".netCHARTING Sample"
   c.DefaultSeries.Type = SeriesType.AreaLine
   
   ' This will ensure the x axis is numeric despite elements having names.
   c.XAxis.Scale = Scale.Normal
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Help File > Getting Started > Data Tutorials
   ' - DataEngine Class in the help file	
   ' - Sample: features/DataEngine.aspx
   
   Dim s As Series = Series.FromYValues(0, 20, 20, 0)
   s(0).XValue = 0.99
   s(1).XValue = 1
   s(2).XValue = 5
   s(3).XValue = 5.01
   c.SeriesCollection.Add(s)
   
   
   Dim s2 As Series = Series.FromYValues(5, 20, 20, 5)
   s2(0).XValue = 8.99
   s2(1).XValue = 9
   s2(2).XValue = 25
   s2(3).XValue = 25.01
   s2(0).YValueStart = 5
   s2(1).YValueStart = 5
   s2(2).YValueStart = 5
   s2(3).YValueStart = 5
   
   c.SeriesCollection.Add(s2)
End Sub 'SetChart


</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	</head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
			<dotnet:Chart id="Chart1" runat="server"/>
		</div>
	</body>
</html>
