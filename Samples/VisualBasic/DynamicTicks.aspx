<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Dynamic Ticks</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   
   ' This sample will demonstrate how to create dynamic tick labels based on chart data.
   ' First we get our data, if you would like to get the data from a database you need to use
   ' the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
   Dim sc As SeriesCollection = getRandomData()
   
   ' Pass a simple string manipulation funtion to the x axis.
   Chart.XAxis = Chart.XAxis.Calculate("XAxis", new ChangeValueDelegate(AddressOf MyFunction))
   
   ' Have the tick mark labels be the total value of the each ticks group of elements.
		        Chart.XAxis.DefaultTick.Label.Text = "%Value"
   
   ' Each tick label will first become a value, then our function below will change the tick label based
   ' the ticks value. 
   ' To expand on this we'll use an annotation to show how many tick groups are over and under 100.
   ' We have initialized an annotation and 2 counters below. 
   ' Zero out the counters.
   underCount = 0
   overCount = 0
   
   ' Add the annotation to our chart.	
   Chart.Annotations.Add(ann)
   
   
   ' Add the random data.
   Chart.SeriesCollection.Add(sc)
End Sub 'Page_Load 


Public Shared ann As New Annotation()
Public Shared underCount As Integer = 0
Public Shared overCount As Integer = 0


Public Shared Function MyFunction(value As String) As String
   Dim num As Integer = Integer.Parse(value)
   Dim newLabel As String = ""
   ' Modify the string and increment counters.
   If num > 100 Then
      newLabel = " Over 100 (" + value + ")"
      overCount += 1
   Else
      newLabel = " Under 100 (" + value + ")"
      underCount += 1
   End If
   
   ' Update the annotation with the new count.
   ann.Label.Text = overCount.ToString() + " group(s) are over, and " + underCount.ToString() + " are under 100."
   ann.Position = New Point(100, 0)
   
   
   Return newLabel
End Function 'MyFunction
 


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
