<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.DefaultSeries.Type = SeriesType.Line
   
   ' This sample will demonstrate how to use data sources to process string tokens in an annotation.
   ' It will take the series collection we chart and show some additional information about it in an annotation.
   ' 1. GET DATA
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. For information on acquiring 
   ' database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' 2. CREATE AN ANNOTATION AND SET THE DATA SOURCE.
   Dim an As New Annotation()
   
   ' Create the data source.
   an.DataSource = DataSource.FromSeriesCollection(mySC)
   ' A simpler way to set the data source that also works is to use 
   ' the implicit casting functionality of the DataSource object.
   an.DataSource = mySC
   
   ' Specify the text with tokens to replace.
   an.Label.Text = "y sum: <%YSum,Currency> " + ControlChars.Lf + " x sum : %XSum"
   
   ' Add the annotation to the chart.
   Chart.Annotations.Add(an)
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 0 To 0
      Dim s As New Series()
      s.Name = "Series " & a
      Dim b As Integer
      For b = 0 To 19
         Dim e As New Element()
         'e.Name = "Element " & b;
         e.YValue = myR.Next(50)
         e.XValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
