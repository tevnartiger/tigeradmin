<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender as Object ,e as EventArgs )


	Chart.TempDirectory = "temp"
	Chart.Debug = true
	Chart2.TempDirectory = "temp"
	Chart2.Debug = true
	Chart3.TempDirectory = "temp"
	Chart3.Debug = true
	Chart.Title = "Logarithmic Base: 2"	
	Chart2.Title = "Logarithmic Base: 5"
	Chart3.Title = "Logarithmic Base: 10"
		
	' This sample will demonstrate using log scales with different logarithmic bases.

	' 1. SET SCALE TO LOGARITHMIC
	Chart.YAxis.Scale = Scale.Logarithmic
	Chart2.YAxis.Scale = Scale.Logarithmic	
	Chart3.YAxis.Scale = Scale.Logarithmic	
	
	' 2. SET LOG BASES
	Chart.YAxis.LogarithmicBase = 2
	Chart2.YAxis.LogarithmicBase = 5
	Chart3.YAxis.LogarithmicBase = 10

	' 3. ADD DATA
	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Classic samples folder
	' - Help File > Data Tutorials
	' - Sample: features/DataEngine.aspx
	Chart.SeriesCollection.Add(getRandomData())
	Chart2.SeriesCollection.Add(getRandomData())
	Chart3.SeriesCollection.Add(getRandomData())
    
End Sub



Function  getRandomData() As SeriesCollection
	
		
	Dim SC As SeriesCollection  = new SeriesCollection()
	Dim myR as Random  = new Random()
	Dim a,b As Integer
	Dim s As Series 
	Dim e As Element	
	
	For a = 0 To 0
	
		s = new Series()
		s.Name = "Series " & a
		For  b = 0 To 3
		
			e = new Element()
			e.Name = "Element " & b
			e.YValue = myR.Next(50)
			'e.XValue = b*2

			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a

	return SC
End Function
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<dotnet:Chart id="Chart2" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<dotnet:Chart id="Chart3" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
