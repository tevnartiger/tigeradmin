<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">



Sub Page_Load(sender As [Object], e As EventArgs)
   ' This sample demonstrates the use of financial indicators AveragePrice and PriceActionIndicator 
   ' The Financial Chart
   FinancialChart.Title = "Company X Stock Price"
   FinancialChart.TempDirectory = "temp"
   FinancialChart.Debug = True
   FinancialChart.LegendBox.Template = "%icon %name"
   FinancialChart.Size = "800X400"
   
   FinancialChart.YAxis.Label.Text = "Price (USD)"
   FinancialChart.YAxis.FormatString = "currency"
   FinancialChart.YAxis.Scale = Scale.Range
   FinancialChart.DefaultSeries.DefaultElement.Marker.Visible = False
   FinancialChart.DefaultSeries.Type = SeriesType.Spline
   FinancialChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical
   
   
   ' Modify the x axis labels.
   FinancialChart.XAxis.Scale = Scale.Time
   FinancialChart.XAxis.TimeInterval = TimeInterval.Day
   'FinancialChart.XAxis.TimeScaleLabels.DayFormatString = "o";
   FinancialChart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Month)
   FinancialChart.XAxis.TimeScaleLabels.MonthFormatString = "MMM"
   
   Dim priceDataEngine As New DataEngine()
   priceDataEngine.ChartObject = FinancialChart
   priceDataEngine.ChartType = ChartType.Financial
   priceDataEngine.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   priceDataEngine.DateGrouping = TimeInterval.Day
   priceDataEngine.StartDate = New DateTime(2001, 12, 31, 0, 0, 0)
   priceDataEngine.EndDate = New DateTime(2002, 2, 28, 23, 59, 59)
   ' Here we import data from the FinancialCompany table from within chartsample.mdb
   priceDataEngine.SqlStatement = "SELECT TransDate, HighPrice, LowPrice, OpenPrice, ClosePrice FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate "
   priceDataEngine.DataFields = "xAxis=TransDate,High=HighPrice,Low=LowPrice,Open=OpenPrice,Close=ClosePrice"
   
   Dim sc As SeriesCollection = priceDataEngine.GetSeries()
   Dim prices As Series = Nothing
   If sc.Count > 0 Then
      prices = sc(0)
   Else
      Return
   End If
   prices.Name = "Prices"
   prices.DefaultElement.ShowValue = True
   prices.DefaultElement.ToolTip = "L:%Low | H:%High"
   prices.DefaultElement.SmartLabel.Text = "O:%Open | C:%Close"
   prices.Type = SeriesTypeFinancial.Bar
   
   Dim cp As New CalendarPattern(TimeInterval.Day, TimeInterval.Week, "0000001")
   prices.Trim(cp, ElementValue.XDateTime)
   
   FinancialChart.SeriesCollection.Add(prices)
   
   ' AveragePrice financial indicator which is arithmetic average of the High, Low, Close and Open price 
   ' for a trading day. 
   Dim averagePrice As Series = FinancialEngine.AveragePrice(prices)
   averagePrice.DefaultElement.Color = Color.FromArgb(49, 99, 49)
   FinancialChart.SeriesCollection.Add(averagePrice)
   
   ' Create a new chart area for the Price Action Indicator(PAIN).
   Dim painChartArea As New ChartArea("Price Action Indicator")
   painChartArea.HeightPercentage = 20
   painChartArea.YAxis = New Axis()
   FinancialChart.ExtraChartAreas.Add(painChartArea)
   
   ' PriceActionIndicator financial indicator which has the following formula:
   ' PAIN = ((Close-Open)+(Close-High)+(Close-Low))/2.
   Dim priceActionIndicator As Series = FinancialEngine.PriceActionIndicator(prices)
   priceActionIndicator.DefaultElement.Color = Color.FromArgb(126, 125, 255)
   painChartArea.SeriesCollection.Add(priceActionIndicator)
End Sub 'Page_Load 
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="FinancialChart" runat="server"/>			
		</div>
	</body>
</html>
