<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo
   Chart.Size = "600x350"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   ' This sample will demonstrate how a calculated series can be created and placed in a chart area.
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   Dim ca1 As New ChartArea()
   Dim ca2 As New ChartArea()
   ca2.Label.Text = "Difference"
   
   ' Create the difference series.
   Dim diffSeries As Series = Series.Subtract(mySC(1),mySC(0))
   diffSeries.DefaultElement.Color = Color.Red
   diffSeries.Name = "Difference"
   
   ' Iterate the elements and change their colors based on value.	
   Dim el As Element
   For Each el In  diffSeries.Elements
      If el.YValue > 0 Then
         el.Color = Color.Green
      Else
         el.Color = Color.Red
      End If 
   Next el ' Add the series and chart areas to the chart.
   Chart.SeriesCollection.Add(mySC(0))
   ca1.SeriesCollection.Add(mySC(1))
   ca2.SeriesCollection.Add(diffSeries)
   
   Chart.ExtraChartAreas.Add(ca1, ca2)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim a As Integer
   For a = 0 To 1
      Dim s As New Series()
      s.Name = "Series " +Convert.ToString(a + 1)
      Dim b As Integer
      For b = 0 To 14
         Dim e As New Element()
         e.Name = "Element " +Convert.ToString(b + 1)
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   
   
   Return SC
End Function 'getRandomData

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
