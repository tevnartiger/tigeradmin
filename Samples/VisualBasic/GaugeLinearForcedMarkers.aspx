<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates a vertical linear gauge using shading effect Three and Forced element image markers.
		Chart.Size = "120x230"

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.DefaultSeries.Palette = New Color() { Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.LegendBox.Visible = False

		Chart.Type = ChartType.Gauges
		Chart.DefaultSeries.GaugeType = GaugeType.Vertical
		Chart.ShadingEffectMode = ShadingEffectMode.Three

		Chart.ChartArea.Padding = 0


		Chart.ChartArea.ClearColors()


		Chart.DefaultSeries.GaugeBorderBox.DefaultCorner = BoxCorner.Round
		Chart.DefaultSeries.GaugeBorderShape = GaugeBorderShape.UseBox
		Chart.DefaultSeries.GaugeBorderBox.Padding = 6
		Chart.YAxis.Label.Text = "Hits"
		Chart.YAxis.TickLabelPadding = 2
		Chart.YAxis.Orientation = dotnetCHARTING.Orientation.Right

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()
		mySC(0)(0).Marker = New ElementMarker("../../images/browserIE.png")
		mySC(0)(1).Marker = New ElementMarker("../../images/browserN.png")
		Chart.DefaultElement.ForceMarker = True

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)

	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		For a As Integer = 1 To 1
			Dim s As Series = New Series("")
			For b As Integer = 1 To 2
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
