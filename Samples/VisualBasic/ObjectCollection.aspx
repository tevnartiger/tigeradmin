<%@ Page Language="VB" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>

<script runat="server">
Sub Page_Load(sender As [Object], e As EventArgs)
   'set global properties
   Chart.Title = "Item sales report"
   
   ' Set the x axis label
   Chart.ChartArea.XAxis.Label.Text = "X Axis Label"
   
   ' Set the y axis label
   Chart.ChartArea.YAxis.Label.Text = "Y Axis Label"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   
   'Adding series programatically
   Chart.Series.Name = "Item sales"
   Chart.Series.DataFields = "xAxis=Name,yAxis=Total"
   Chart.Series.Data = CreateObjectCollection()
   Chart.SeriesCollection.Add()
End Sub 'Page_Load
 

Function CreateObjectCollection() As Products
   Dim myProducts As New Products()
   myProducts.Add(New Product("P1", 23))
   myProducts.Add(New Product("P2", 12))
   myProducts.Add(New Product("P3", 20))
   myProducts.Add(New Product("P4", 65))
   myProducts.Add(New Product("P5", 50))
   myProducts.Add(New Product("P6", 40))
   
   Return myProducts
End Function 'CreateObjectCollection
 _
Public Class Products
   Inherits System.Collections.CollectionBase
   
   Public Function Add(newProduct As Product) As Integer
      Return List.Add(newProduct)
   End Function 'Add 
End Class 'Products
 _

Public Class Product
   Private nameField As String
   Private totalField As Double
   
   Public Sub New()
   End Sub 'New
   
   Public Sub New(proName As String, proTotal As Double)
      nameField = proName
      totalField = proTotal
   End Sub 'New
   
   
   
   Public Property Name() As String
      Get
         Return nameField
      End Get
      Set
         nameField = value
      End Set
   End Property
   
   Public Property Total() As Double
      Get
         Return totalField
      End Get
      Set
         totalField = value
      End Set
   End Property
End Class 'Product 

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Custom collection Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
