<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Size = "600x350"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = ".netCHARTING Sample"
   
   ' This sample demonstrates how the coordinates of boxes on the chart can be acquired after the chart is generated.
   ' First we setup the data.
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
   
   '--------------
   ' Add an annotation to an element to get its position later.
   mySC(1)(2).Annotation = New Annotation("element annotation")
   
   
   ' Add an arbirtaty annotation.	
   Dim an As New Annotation("This is an annotation")
   an.Position = New Point(50, 50)
   Chart.Annotations.Add(an)
   
   ' These two lines will force the chart to generate so the coordinates can be determined. 
   ' We will also get a graphics object we can draw on before we save the final image.
   Dim bmp As Bitmap = Chart.GetChartBitmap()
   Dim g As Graphics = Graphics.FromImage(bmp)
   
   ' Draw an orange outline around the boxes we get coordinates for.
   Dim myPen As New Pen(Color.Orange, 2)
   g.DrawRectangle(myPen, Chart.TitleBox.GetRectangle())
   g.DrawRectangle(myPen, Chart.LegendBox.GetRectangle())
   g.DrawRectangle(myPen, Chart.ChartArea.GetRectangle())
   g.DrawRectangle(myPen, an.GetRectangle())
   g.DrawRectangle(myPen, mySC(1)(2).Annotation.GetRectangle())
   g.Dispose()
   myPen.Dispose()
   
   ' Save the final image.
   Chart.FileManager.SaveImage(bmp)
   
   ' Write the coordinates on the page.
   myLabel.Text = "TitleBox Rectangle is: " + Chart.TitleBox.GetRectangle().ToString()
   myLabel.Text += "<br>Legend Rectangle is: " + Chart.LegendBox.GetRectangle().ToString()
   myLabel.Text += "<br>ChartArea Rectangle is: " + Chart.ChartArea.GetRectangle().ToString()
   myLabel.Text += "<br>Annotation Rectangle is: " + an.GetRectangle().ToString()
   myLabel.Text += "<br>Element Annotation Rectangle is: " + mySC(1)(2).Annotation.GetRectangle().ToString()
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<br><asp:Label ID="myLabel" Runat=server></asp:Label>
		</div>
	</body>
</html>
