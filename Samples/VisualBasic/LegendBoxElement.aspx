<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">





Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   
   ' This sample will demonstrate how to use a legend box to refer to particular elements.
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' First we make the marker appear on the column and set larger size.
   mySC(3).Elements(1).ForceMarker = True
   mySC(3).Elements(1).Marker.Size = 10
   
   ' Initialize the new legend box and make it only show the marker and name.
   Dim newLegend As New LegendBox()
   newLegend.Template = "%Icon%Name"
   
   ' Initialize the new legend entry.
   Dim newEntry As New LegendEntry()
   newEntry.Name = "Important " + ControlChars.Lf + "Element"
   
   ' Setup the marker for the legend entry
   newEntry.Marker.Type = ElementMarkerType.FivePointStar
   newEntry.Marker.Size = 10
   newEntry.SeriesType = SeriesType.Marker
   ' Ensure the same color is used for the marker as the element.
   newEntry.Marker.Color = Chart.Palette(3)
   
   ' Add the entry to the legend and the legend to the chart.	
   newLegend.ExtraEntries.Add(newEntry)
   Chart.ExtraLegendBoxes.Add(newLegend)
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 0 To 3
      Dim s As New Series()
      s.Name = "Series " & a
      Dim b As Integer
      For b = 0 To 3
         Dim e As New Element()
         e.Name = "Element " & b
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
