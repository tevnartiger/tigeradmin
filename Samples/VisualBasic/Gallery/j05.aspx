<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">
 ' This sample demonstrates how to iterate a layer of shape files.
Sub Page_Load(sender As [Object], e As EventArgs)


   'This sample uses a pre-generated image. To run the sample live, 
   'please uncomment the code below. 
   'You will need to download the asia.shp and asia.dbf files from
   'www.dotnetcharting.com/mapfiles.zip and put them in the /images/mapfiles directory.
   'Dim asiaChart As New dotnetCHARTING.Chart()
   'Chart.Type = ChartType.Map
   'Chart.Size = "1100x950"
   'Chart.Title = "Asia colored by Country"
   'Chart.TempDirectory = "temp"
   'Chart.Debug = True
   'Chart.TitleBox.Position = TitleBoxPosition.Full
   'Chart.LegendBox.Template = "%name"
   
   'Chart.ChartArea.Background = New Background(Color.FromArgb(142, 195, 236), Color.FromArgb(63, 137, 200), 90)
   'Dim layer As MapLayer = MapDataEngine.LoadLayer("../../../images/MapFiles/asia.shp")
   
   'Chart.Mapping.DefaultShape.Label.Text = "%Country"
   'Chart.Mapping.DefaultShape.Label.OutlineColor = Color.White
   'Chart.Mapping.DefaultShape.Label.Hotspot.ToolTip = "%Country"
   'Chart.Mapping.MapLayerCollection.Add(layer)
   
   'Set different colors to each state
   'Chart.PaletteName = Palette.Five
   'Dim i As Integer = 0
   'Dim shape As Shape
   'For Each shape In  layer.Shapes
    '  If i >= Chart.Palette.Length Then
     '    i = 0
     '    Chart.PaletteName = Palette.Four
     ' End If
      'shape.Background.Color = Chart.Palette(i)
      
     ' i += 1
  ' Next shape
End Sub 'Page_Load

</script>
	</head>
	<body>
		<div style="text-align:center">
			<img alt="Asia Map" src="temp/asia.png"/>
		</div>
	</body>
</html>
