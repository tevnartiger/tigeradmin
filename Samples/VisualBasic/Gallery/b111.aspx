<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	' This sample demonstrates range bars with shading effect mode Five.
	' Set the title.
	Chart.Title="My Chart"

	' Set the chart Type
	Chart.Type = ChartType.ComboHorizontal

	' Turn 3D on.
	Chart.Use3D = True
	Chart.ShadingEffectMode = ShadingEffectMode.Five


	' Set a default transparency
	Chart.DefaultElement.Transparency = 30

	' Set the x axis label
	Chart.XAxis.Label.Text="X Axis Label"

	' Set the y axis label
	Chart.YAxis.Label.Text="Y Axis Label"

	' Set the x axis scale to time.
	Chart.XAxis.Scale = Scale.Time

	' Set the directory where the images will be stored.
	Chart.TempDirectory="temp"

	' Set the chart size.
	Chart.Size = "600X500"
	' Add the random data.
	Chart.SeriesCollection.Add(getRandomData())


End Sub

Function getRandomData() As SeriesCollection
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim myR As Random = New Random()
			Dim a As Integer
		For a = 1 To 4
		Dim s As Series = New Series("Series " & a.ToString())
		Dim b As Integer 
		For b= 1 To 4
			Dim e As Element = New Element()
			e.Name = "Element " & b
			e.YDateTimeStart = New DateTime(2003,4,4).AddDays(myR.Next(50))
			e.YDateTime = e.YDateTimeStart.AddDays(myR.Next(50))
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a

	' Set Different Colors for our Series
	SC(0).DefaultElement.Color = Color.FromArgb(49,255,49)
	SC(1).DefaultElement.Color = Color.FromArgb(255,255,0)
	SC(2).DefaultElement.Color = Color.FromArgb(255,99,49)
	SC(3).DefaultElement.Color = Color.FromArgb(0,156,255)

	Return SC
End Function



</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Gallery Sample (Time Gantt Chart in 3D)</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
