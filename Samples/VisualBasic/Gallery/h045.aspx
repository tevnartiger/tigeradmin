<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	' This sample demonstrates vertical thermometer gauges using ShadingEffect.

	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.Palette = New Color(){Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)}

	' TitleBox Customization
	Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
	Chart.TitleBox.Background = New Background(Color.White,Color.LightGray,90)
	Chart.TitleBox.Label.OutlineColor = Color.LightGray
	Chart.TitleBox.CornerTopLeft = BoxCorner.Round
	Chart.TitleBox.CornerTopRight = BoxCorner.Round
	Chart.ChartArea.Background = New Background(Color.AliceBlue,Color.GhostWhite,90)


	Chart.Type = ChartType.Gauges
	Chart.DefaultSeries.GaugeType = GaugeType.Vertical
	Chart.DefaultSeries.GaugeLinearStyle = GaugeLinearStyle.Thermometer
	Chart.Size = "600x350"
	Chart.Title = ".netCHARTING Sample"
	Chart.ShadingEffectMode = ShadingEffectMode.Four

	Chart.DefaultSeries.Background.Color = Color.White

	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Help File > Getting Started > Data Tutorials
	' - DataEngine Class in the help file	
	' - Sample: features/DataEngine.aspx


	Dim mySC As SeriesCollection = getRandomData()

	' Add the random data.
	Chart.SeriesCollection.Add(mySC)


End Sub


Function getRandomData() As SeriesCollection
	Dim myR As Random = New Random()
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim a As Integer
        Dim b As Integer
	For a = 1 To 4
		Dim s As Series = New Series("Series " & a.ToString())

			Dim e As Element = New Element("Element")
			e.YValue = myR.Next(50)
			s.Elements.Add(e)

		SC.Add(s)
	Next a
	Return SC
End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
