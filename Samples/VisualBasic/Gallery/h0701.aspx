<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates digital readout gauges.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.PaletteName = Palette.DarkRainbow

		Chart.Type = ChartType.Gauges
		Chart.DefaultSeries.GaugeType = GaugeType.Bars
		Chart.DefaultSeries.GaugeBorderBox.Background.Color = Color.White
		Chart.Size = "600x350"
		Chart.Title = ".netCHARTING Sample"
		Chart.DefaultElement.SmartLabel.Type = LabelType.Digital


		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
		mySC(0).DefaultElement.SmartLabel.Font = New Font("Arial", 18)
		mySC(1).DefaultElement.SmartLabel.Font = New Font("Arial", 18,FontStyle.Italic)
		mySC(2).DefaultElement.SmartLabel.Font = New Font("Arial", 18,FontStyle.Bold)
		mySC(3).DefaultElement.SmartLabel.Font = New Font("Arial", 18, FontStyle.Bold Or FontStyle.Italic)
		mySC(0).Name = "Normal"
		mySC(1).Name = "Italic"
		mySC(2).Name = "Bold"
		mySC(3).Name = "Bold Italic"

	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 4
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 4
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(1150)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
