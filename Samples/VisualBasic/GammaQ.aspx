<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>


<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	' This sample demonstrates the Gamma Q function.

	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.Palette = New Color(){Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)}

	Chart.Type = ChartType.Combo
	Chart.Size = "600x350"
	Chart.Title = "Special Functions"


	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Help File > Getting Started > Data Tutorials
	' - DataEngine Class in the help file	
	' - Sample: features/DataEngine.aspx

	Dim mySC As SeriesCollection = getRandomData()
	Dim gammaQSC As SeriesCollection = New SeriesCollection()
	' Calculate the gammaQ function 
	Dim i As Integer
	For i = 0 To 3
		Dim gammaQ As Series = ForecastEngine.Advanced.GammaQ(mySC(i),10)
		gammaQ.Type = SeriesType.Line
		gammaQSC.Add(gammaQ)
	Next i

	' Bessel chart area
	Dim gammaChartArea As ChartArea = New ChartArea ()
	gammaChartArea.HeightPercentage = 40
	gammaChartArea.YAxis.Label.Text = "GammaQ"
	Chart.ExtraChartAreas.Add (gammaChartArea)


	' Add the random data.
	Chart.SeriesCollection.Add(mySC)
	gammaChartArea.SeriesCollection.Add(gammaQSC)

End Sub

Function getRandomData() As SeriesCollection
	Dim myR As Random = New Random(1)
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim a As Integer
	For a = 1 To 4
		Dim s As Series = New Series("Series " & a.ToString())
		Dim b As Integer
		For b = 1 To 4
			Dim e As Element = New Element()
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
	Return SC
End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
	</body>
</html>
