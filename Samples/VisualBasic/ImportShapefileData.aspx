<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.OleDb" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.Type = ChartType.Map
   Chart.Size = "1000x600"
   Chart.Title = " US states with custom sales information"
   Chart.TempDirectory = "temp"
   Chart.Mapping.DefaultShape.Label.OutlineColor = Color.White
   Chart.ChartArea.Background.Color = Color.FromArgb(220, 255, 255)
   Chart.TitleBox.Label.Color = Color.Black
   Chart.TitleBox.Background.Color = Color.LightYellow
   Chart.TitleBox.Background.GlassEffect = True
   
   ' This sample demonstrates how to integrate custom data from a database with a map layer.
   Dim layer As MapLayer = MapDataEngine.LoadLayer("../../Images/MapFiles/primusa.shp")
   layer.ImportData(CreateDataTable(), "STATE_ABBR", "Code")
   
   Dim sp As SmartPalette = New SmartPalette()
   Chart.SmartPalette =sp
   
   sp.Add("SALES", New SmartColor(Color.FromArgb(0, 250, 0), New ScaleRange(1, 1000)))
   sp.Add("SALES", New SmartColor(Color.FromArgb(0, 200, 0), New ScaleRange(1000, 10000)))
   sp.Add("SALES", New SmartColor(Color.FromArgb(0, 150, 0), New ScaleRange(10000, 100000)))
   sp.Add("SALES", New SmartColor(Color.FromArgb(0, 100, 0), New ScaleRange(100000, 1000000)))
   
   Chart.Mapping.ZoomPercentage = 120
   
   Chart.Mapping.ZoomCenterPoint = New PointF(35,- 98)
   Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   Chart.Mapping.MapLayerCollection.Add(layer)
   Chart.Mapping.DefaultShape.Label.Text = "$%Sales"
   Chart.Mapping.DefaultShape.Label.Hotspot.ToolTip = "%State_Name"
End Sub 'Page_Load
 
Private Function CreateDataTable() As DataTable
   Dim dt As New DataTable()
   Dim connString As String = "Provider=Microsoft.Jet.OLEDB.4.0;user id=admin;password=;data source=" + Server.MapPath("../../database/chartsample.mdb")
   Dim conn As New OleDbConnection(connString)
   Dim adapter As New OleDbDataAdapter()
   Dim selectQuery As String = "SELECT [Code],[Sales] FROM Locations" 
   adapter.SelectCommand = New OleDbCommand(selectQuery, conn)
   adapter.Fill(dt)
   Return dt
End Function 'CreateDataTable

</script>
	</head>
	<body>
		<div style="text-align:center">
				<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
		</div>
	</body>
</html>