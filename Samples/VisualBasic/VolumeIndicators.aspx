<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>


<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Financial Volume Indicators Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   ' This sample demonstrates the use of the Volume indicators
   
   ' First we declare a chart of type FinancialChart	
   ' The Financial Chart
   FinancialChart.Title = "Financial Chart"
   FinancialChart.TempDirectory = "temp"
   FinancialChart.Debug = True
   FinancialChart.ShadingEffect = True
   FinancialChart.LegendBox.Template = "%icon %name"
   FinancialChart.Size = "900X800"
   
   FinancialChart.XAxis.Scale = Scale.Time
   FinancialChart.XAxis.FormatString = "MMM d"
   FinancialChart.XAxis.TimeInterval = TimeInterval.Day
   ' For financial indicators the time scale is inverted (i.e. the first element of the series is the newest)
   FinancialChart.XAxis.InvertScale = True
   
   
   FinancialChart.YAxis.Label.Text = "Price (USD)"
   FinancialChart.YAxis.FormatString = "currency"
   FinancialChart.YAxis.ScaleRange.ValueLow = 15
   
   ' Here we load data samples from the FinancialCompany table from within chartsample.mdb
   Dim priceDataEngine As New DataEngine()
   priceDataEngine.ChartType = ChartType.Financial
   priceDataEngine.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   priceDataEngine.DateGrouping = TimeInterval.Day
   priceDataEngine.StartDate = New DateTime(2001, 2, 1)
   priceDataEngine.EndDate = New DateTime(2001, 9, 30)
   priceDataEngine.SqlStatement = "SELECT TransDate, HighPrice, LowPrice, OpenPrice, ClosePrice,Volume FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate  DESC "
   priceDataEngine.DataFields = "xAxis=TransDate,High=HighPrice,Low=LowPrice,Open=OpenPrice,Close=ClosePrice,Volume=Volume"
   
   Dim sc As SeriesCollection = priceDataEngine.GetSeries()
   Dim prices As Series = Nothing
   If sc.Count > 0 Then
      prices = sc(0)
   Else
      Return
   End If 
   
   prices.DefaultElement.ToolTip = "L:%Low-H:%High"
   prices.DefaultElement.SmartLabel.Font = New Font("Arial", 6)
   prices.DefaultElement.SmartLabel.Text = "O:%Open-C:%Close"
   prices.Type = SeriesTypeFinancial.CandleStick
   
   Dim cp As New CalendarPattern(TimeInterval.Day, TimeInterval.Week, "0000001")
   cp.AdjustmentUnit = TimeInterval.Day
   prices.Trim(cp, ElementValue.XDateTime)
   prices.Name = "Prices"
   FinancialChart.SeriesCollection.Add(prices)
   
   ' Create the second chart area 
   Dim volumeChartArea As New ChartArea()
   volumeChartArea.Label.Text = "Stock Volume"
   volumeChartArea.YAxis.Label.Text = "Volumes"
   volumeChartArea.Series.Name = "Stock Volume"
   volumeChartArea.HeightPercentage = 17
   volumeChartArea.Series.DefaultElement.ToolTip = "%YValue"
   FinancialChart.ExtraChartAreas.Add(volumeChartArea)
   
   ' Add a volume series to the chart area
   Dim volumeDataEngine As New DataEngine()
   volumeDataEngine.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   volumeDataEngine.DateGrouping = TimeInterval.Days
   volumeDataEngine.StartDate = New DateTime(2001, 2, 1)
   volumeDataEngine.EndDate = New DateTime(2001, 9, 30)
   volumeDataEngine.SqlStatement = "SELECT TransDate,volume FROM FinancialCompany WHERE TransDate >= #STARTDATE# AND TransDate <= #ENDDATE# ORDER BY TransDate DESC"
   volumeDataEngine.DataFields = "xAxis=TransDate,yAxis=Volume"
   
   Dim volumes As Series = volumeDataEngine.GetSeries()(0)
   volumes.Trim(cp, ElementValue.XDateTime)
   volumes.Name = "Volumes"
   volumes.Type = SeriesType.Bar
   volumeChartArea.SeriesCollection.Add(volumes)
   
   
   ' Financial Volume Indicators
   FinancialChart.DefaultSeries.DefaultElement.Marker = New ElementMarker(ElementMarkerType.None)
   FinancialChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical
   FinancialChart.DefaultSeries.Type = SeriesType.Spline
   
   ' Negative Volume Index
   Dim negativeVolume As Series = FinancialEngine.NegativeVolumeIndex(prices)
   negativeVolume.Type = SeriesType.Spline
   negativeVolume.DefaultElement.Color = Color.FromArgb(122, 255, 45)
   volumeChartArea.SeriesCollection.Add(negativeVolume)
   
   ' Positive Volume Index
   Dim positiveVolume As Series = FinancialEngine.PositiveVolumeIndex(prices)
   positiveVolume.Type = SeriesType.Spline
   positiveVolume.DefaultElement.Color = Color.FromArgb(255, 125, 45)
   volumeChartArea.SeriesCollection.Add(positiveVolume)
   
   ' Create a new char area for the indicator OnBalanceVolume
   Dim balanceChartArea As New ChartArea()
   balanceChartArea.Label.Text = "On Balance Volume"
   balanceChartArea.YAxis.Label.Text = "Volumes"
   balanceChartArea.HeightPercentage = 17
   FinancialChart.ExtraChartAreas.Add(balanceChartArea)
   
   ' On Balance Volume indicator
   Dim onBalanceVolume As Series = FinancialEngine.OnBalanceVolume(prices)
   onBalanceVolume.Type = SeriesType.Spline
   onBalanceVolume.DefaultElement.Color = Color.FromArgb(45, 125, 255)
   balanceChartArea.SeriesCollection.Add(onBalanceVolume)
End Sub 'Page_Load 
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="FinancialChart" runat="server"/>
		</div>
	</body>
</html>
