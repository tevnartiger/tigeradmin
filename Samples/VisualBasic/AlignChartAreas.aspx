<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates how to combine two charts of the same type into a single chart in order to align chart areas.

		' Individual Charts
		SetChart(Chart)
		SetChart(Chart1)
		Chart.DefaultSeries.Type = SeriesType.AreaLine
		Chart1.YAxis.SmartScaleBreak = True
		Chart1.SeriesCollection(0)(0).YValue = 5000
		Chart1.SeriesCollection(0).DefaultElement.Color = Color.FromArgb(255, 255, 0)

		' Combined Chart
		SetChart(Chart2)
		Chart2.Size = "500x500"
		Chart2.Title = ".netCHARTING Sample (Aligned)"
		Chart2.DefaultSeries.Type = SeriesType.AreaLine

		Dim ca As ChartArea = New ChartArea()
		ca.Title = ".netCHARTING Sample (Aligned)"
		ca.SeriesCollection.Add(getRandomData())
		ca.DefaultSeries.Type = SeriesType.Column
		ca.YAxis = New Axis()
		ca.YAxis.SmartScaleBreak = True
		ca.SeriesCollection(0)(0).YValue = 5000
		Chart2.ExtraChartAreas.Add(ca)
	End Sub

	Sub SetChart(ByVal c As Chart) ' Shared chart settings.
		c.Type = ChartType.Combo
		c.Size = "500x250"
		c.Title = ".netCHARTING Not Aligned"
		c.TempDirectory = "temp"
		c.Mentor = False
		c.Debug = True
		c.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		c.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		For a As Integer = 1 To 1
			Dim s As Series = New Series("Series " & a.ToString())
			For b As Integer = 1 To 4
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<table style="width: 100%">
			<tr>
				<td style="width: 50%">
					<dotnet:Chart ID="Chart" runat="server" />
					<dotnet:Chart ID="Chart1" runat="server" />
				</td>
				<td style="width: 50%">
					<dotnet:Chart ID="Chart2" runat="server" />
				</td>
			</tr>
		</table>
	</div>
</body>
</html>
