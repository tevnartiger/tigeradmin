<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   ' This sample demonstrates how Series.SplitRegroupCalculate works in stages.
   ' FullScreen
   SetChart(Chart)
   SetChart(Chart1)
   SetChart(Chart2)
   
   Chart.XAxis.Scale = Scale.Time
   Chart2.DefaultElement.DefaultSubValue.Type = SubValueType.Marker
   
   Chart.Title = "Original Data"
   Chart1.Title = "Data after the first split is applied."
   Chart2.Title = "Resulting series after Series.SplitRegroupCalculate is applied to the data from the first chart."
   Chart1.ChartArea.Label.Text = "Splitting a series by a TimeIntervalAdvanced object. The elements " + ControlChars.Lf + "were originally named after the day of the week they fall on, hence, " + ControlChars.Lf + "this x axis is a category x axis which demonstrates how these will be grouped."
   Chart2.ChartArea.Label.Text = "At this stage the above elements are consolidated into a single " + ControlChars.Lf + "element with subvalues representing the original element's values. " + ControlChars.Lf + "These elements are average values."
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   Dim splits As Series = mySC(0).SplitRegroupCalculate(TimeIntervalAdvanced.Week, TimeIntervalAdvanced.Day, Calculation.Average, "ddd", True)
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC) ' original data
   Chart1.SeriesCollection.Add(mySC(0).Split(TimeIntervalAdvanced.Week)) ' data split into weeks
   Chart2.SeriesCollection.Add(splits) ' data after SplitRegroupCalculate processing
End Sub 'Page_Load


Sub SetChart(c As Chart)
   c.Type = ChartType.Combo
   c.Size = "600x350"
   c.TempDirectory = "temp"
   c.Debug = True
   c.Title = ".netCHARTING Sample"
   c.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom
End Sub 'SetChart


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim dt As New DateTime(2006, 1, 1)
   Dim a As Integer
   For a = 1 To 1
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 199
         dt = dt.AddDays(1)
         Dim e As New Element()
         e.Name = dt.DayOfWeek.ToString()
         e.YValue = myR.Next(50)
         e.XDateTime = dt
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   Return SC
End Function 'getRandomData

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	</head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
			<dotnet:Chart id="Chart1" runat="server"/>
			<dotnet:Chart id="Chart2" runat="server"/>
		</div>
	</body>
</html>
