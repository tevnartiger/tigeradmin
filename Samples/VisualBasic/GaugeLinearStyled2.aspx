<%@ Page Language="vb" Description="dotnetCHARTING Component" %>

<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates a horizontal linear gauge using shading effect Three and customized axis line.
		Chart.Size = "220x110"

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.LegendBox.Visible = False

		Chart.Type = ChartType.Gauges
		Chart.DefaultSeries.GaugeType = GaugeType.Horizontal
		Chart.ShadingEffectMode = ShadingEffectMode.Three

		Chart.Margin = "-6"
		Chart.ChartArea.ClearColors()


		Chart.DefaultElement.Transparency = 40

		Chart.YAxis.Line.Width = 3
		Chart.YAxis.Line.EndCap = LineCap.ArrowAnchor
		Chart.YAxis.Line.AnchorCapScale = 2
		Chart.DefaultSeries.GaugeBorderBox.DefaultCorner = BoxCorner.Round
		Chart.DefaultSeries.GaugeBorderShape = GaugeBorderShape.UseBox
		Chart.DefaultSeries.GaugeBorderBox.Padding = 2
		Chart.YAxis.Label.Text = "Hits"
		Chart.YAxis.TickLabelPadding = 2
		Chart.YAxis.Orientation = dotnetCHARTING.Orientation.Top

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)

	End Sub

	Function getRandomData() As SeriesCollection
		Return New SeriesCollection(New Series("",New Element("Element 1", 8)))
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
