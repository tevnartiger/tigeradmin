Imports System
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports dotnetCHARTING

public class WebFormChart 
       Inherits System.Web.UI.Page


	protected  ChartObj As dotnetCHARTING.Chart

	 protected Overrides Sub OnInit(e As EventArgs )

			
		ChartObj.DefaultSeries.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;data source=" & Server.MapPath("../../database/chartsample.mdb")
		ChartObj.Title="Orders"
		ChartObj.TempDirectory="temp"
		ChartObj.Debug=true
		ChartObj.Type = ChartType.ComboHorizontal
		ChartObj.Transpose=True
		ChartObj.Series.StartDate= New System.DateTime(2002,3,10,0,0,0)
		ChartObj.Series.EndDate = New System.DateTime(2002,3,16,23,59,59)
		ChartObj.DateGrouping = TimeInterval.Week

		'Add a series
		ChartObj.Series.Name="Order"
		ChartObj.Series.SqlStatement= "SELECT OrderDate,1 AS q FROM Orders WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate"
		ChartObj.Series.DefaultElement.ShowValue=True
		ChartObj.SeriesCollection.Add()

	End Sub
End Class