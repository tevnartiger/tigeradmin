<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.YAxis.Maximum = 100
   Chart.YAxis.AlternateGridBackground.Color = Color.FromArgb(40, Color.White)
   
   ' This sample will demonstrate how axis markers can create gradiant zones on the chart.
   ' The axis markers have solid colors and transition colors to blend between solid markers.
   ' am1, am3, and am4 are solid color markers while the others will be gradient color transition markers.
   ' Solid Markers
   Dim am1 As New AxisMarker("Danger", New Background(Color.Red, Color.Red, 90), 85, 100)
   Dim am3 As New AxisMarker("Warning", New Background(Color.Orange, Color.Orange, 90), 50, 70)
   Dim am5 As New AxisMarker("Safe", New Background(Color.Yellow, Color.Yellow, 90), 0, 30)
   
   ' Color transition markers.
   Dim am2 As New AxisMarker("", New Background(Color.Red, Color.Orange, 90), 70, 85)
   Dim am4 As New AxisMarker("", New Background(Color.Orange, Color.Yellow, 90), 30, 50)
   
   ' Hide transition marker legend entries.
   am2.LegendEntry.Visible = False
   am4.LegendEntry.Visible = False
   
   ' Add all the markers.
   Chart.YAxis.Markers.Add(am1, am2, am3, am4, am5)
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(2)
   Dim a As Integer
   For a = 0 To 3
      Dim s As New Series()
      s.Name = "Series " +Convert.ToString(a + 1)
      Dim b As Integer
      For b = 0 To 3
         Dim e As New Element()
         e.Name = "Element " +Convert.ToString(b + 1)
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData

</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
