<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender as Object ,e as EventArgs )


	Chart.Type = ChartType.Combo 'Horizontal
	Chart.Width = new Unit(600)
	Chart.Height = new Unit(350)
	Chart.TempDirectory = "temp"
	Chart.Debug = true
	
	
	' This sample demonstrates using automatic scale breaks with numeric and time axes.
	

	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Classic samples folder
	' - Help File > Data Tutorials
	' - Sample: features/DataEngine.aspx
	
	
	' Page link related code.
	Dim page as string  = HttpContext.Current.Request.QueryString("page")
	
	If page Is Nothing Or page = "" Then
     page = "1"
    End If

	
	If( page = "1") Then ' Numeric, no break
	
		' Add the random data.
		Chart.SeriesCollection.Add(getRandomNumericData())
		Chart.Title = "No Smart Scale Break"
	
	Else If(page = "2")  Then ' Numeric, with break
	
		
		 ''Activate the automatic scale break
		Chart.YAxis.SmartScaleBreak = true
	
		' Add the random data.
		Chart.SeriesCollection.Add(getRandomNumericData())
		Chart.Title = "Smart Scale Break"
	
	Else If(page = "3")  Then ' Time, no break
	

		' Add the random data.
		Chart.SeriesCollection.Add(getRandomTimeData())
		Chart.Title = "Time Scale without Smart Scale Break"	
	
	Else if (page = "4") Then ' Time, with break
	
		' Activate the automatic scale break
		Chart.YAxis.SmartScaleBreak = true
	
		' Add the random data.
		Chart.SeriesCollection.Add(getRandomTimeData())
		Chart.Title = "Time Scale with Smart Scale Break"
	End If
	
End Sub

Function  getRandomNumericData() As SeriesCollection

	Dim SC As SeriesCollection  = new SeriesCollection()
	Dim myR AS Random  = new Random()
	Dim a,b as Integer
	Dim s as Series 
	Dim e As Element
	
	For  a = 0 To 3
	
		s = new Series()
		s.Name = "Series " & a
		For  b = 0 To 3
		
			e = new Element()
			e.Name = "Element " & b
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next 
	
	' Make one element very large
	SC(0).Elements(2).YValue = 1000

	' Set Different Colors for our Series
	SC(0).DefaultElement.Color = Color.FromArgb(49,255,49)
	SC(1).DefaultElement.Color = Color.FromArgb(255,255,0)
	SC(2).DefaultElement.Color = Color.FromArgb(255,99,49)
	SC(3).DefaultElement.Color = Color.FromArgb(0,156,255)

	return SC
End Function


	
Function  getRandomTimeData() As SeriesCollection

	Dim SC As SeriesCollection  = new SeriesCollection()
	Dim myR AS Random  = new Random()
	Dim a,b as Integer
	Dim s as Series 
	Dim e As Element
	
	Dim dt as DateTime  = new DateTime(2002,2,2)
	
	For  a = 0 To 3
	
		s = new Series()
		s.Name = "Series " & a
		For b = 0 To 3
		
			e = new Element()
			e.Name = "Element " & b
			dt = dt.AddDays(1)
			e.YDateTime = dt

			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
	
	
	' Make one element very large
	SC(0).Elements(2).YDateTime = new DateTime(2005,2,2)

	' Set Different Colors for our Series
	SC(0).DefaultElement.Color = Color.FromArgb(49,255,49)
	SC(1).DefaultElement.Color = Color.FromArgb(255,255,0)
	SC(2).DefaultElement.Color = Color.FromArgb(255,99,49)
	SC(3).DefaultElement.Color = Color.FromArgb(0,156,255)

	return SC
End Function
		</script>
	</head>
	<body>
		<div style="text-align:center">
		<a href="?page=1">Numeric Without Smart Break</a> | <a href="?page=2">Numeric With Smart Break</a> | <a href="?page=3">Time Without Smart Break</a> | <a href="?page=4">Time With Smart Break</a><br>
		</div>
		<div style="text-align:center">&nbsp;</div>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px"></dotnet:Chart></div>
	</body>
</html>
