<%@ Page Language="VB" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object,e As EventArgs)

	'set global properties
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
    Chart.Title="Sales"
    Chart.Size="800x300"
    Chart.XAxis.Label.Text="Months"
    Chart.Type = ChartType.Pies
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.DefaultSeries.StartDate= New System.DateTime(2002,2,1,12,0,0)
    Chart.DefaultSeries.EndDate = New System.DateTime(2002,6,30,11,59,59)
    Chart.DateGrouping = TimeInterval.Months
    Chart.Use3D=true

    Chart.DefaultShadow.Color = Color.Transparent

    
    'Chart.LegendType = "WithValues"
    Chart.Background = new Background("../../Images/border.gif",BackgroundMode.ImageStretch)
    Chart.ChartArea.Background = new Background(Color.FromArgb(206,206,194))
    Chart.Margin = "30"
    Chart.Height = Unit.Parse(498)  'This is adjusted up from 480 for the sample to prevent the branded footer from covering the border.
    Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
    Chart.TitleBox.ClearColors()
    Chart.ChartArea.ClearColors()

    'Add a series
    Chart.Series.SqlStatement= "SELECT OrderDate,Total, Name FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate"
    Chart.DefaultSeries.DefaultElement.ShowValue=true
	Chart.Series.SplitByLimit="3"
    Chart.SeriesCollection.Add()

End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Sales Report</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
