<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.Type = ChartType.Combo
   Chart.Size = "1200x500"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = ".netCHARTING Sample: Image Bars"
   Chart.LegendBox.Visible = False
   Chart.DefaultAxis.SpacingPercentage = 3
   
   ' This sample demonstrates how to load and set image bar template from the cache.
   ' Add the random data.
   Dim mySC As SeriesCollection = getRandomData()
   
   
   mySC(0).ImageBarTemplate = "../../images/ImageBarTemplates/soccerball"
   mySC(1).ImageBarTemplate = "../../images/ImageBarTemplates/golfball"
   mySC(2).ImageBarTemplate = "../../images/ImageBarTemplates/football"
   mySC(3).ImageBarTemplate = "../../images/ImageBarTemplates/rugbyball"
   mySC(4).ImageBarTemplate = "../../images/ImageBarTemplates/volleyball"
   mySC(5).ImageBarTemplate = "../../images/ImageBarTemplates/bowlingball"
   mySC(6).ImageBarTemplate = "../../images/ImageBarTemplates/basketball"
   mySC(7).ImageBarTemplate = "../../images/ImageBarTemplates/baseball"
   mySC(8).ImageBarTemplate = "../../images/ImageBarTemplates/tennisball"
   mySC(9).ImageBarTemplate = "../../images/ImageBarTemplates/hockeypuck"
   mySC(10).ImageBarTemplate = "../../images/ImageBarTemplates/dollarsign"
   mySC(11).ImageBarTemplate = "../../images/ImageBarTemplates/pills"
   mySC(12).ImageBarTemplate = "../../images/ImageBarTemplates/tablets"
   mySC(13).ImageBarTemplate = "../../images/ImageBarTemplates/coaxialcable"
   mySC(14).ImageBarTemplate = "../../images/ImageBarTemplates/phonecable"
   mySC(15).ImageBarTemplate = "../../images/ImageBarTemplates/ethernetcable"
   mySC(16).ImageBarTemplate = "../../images/ImageBarTemplates/wheat"
   mySC(17).ImageBarTemplate = "../../images/ImageBarTemplates/pipe"
   mySC(18).ImageBarTemplate = "../../images/ImageBarTemplates/figureblue"
   mySC(19).ImageBarTemplate = "../../images/ImageBarTemplates/figurepink"
   mySC(20).ImageBarTemplate = "../../images/ImageBarTemplates/dna"
   
   
   
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 21
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 1
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         e.YValue = myR.Next(50) + 50
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   Return SC
End Function 'getRandomData
</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server">
			</dotnet:Chart>			

		</div>
	</body>
</html>
