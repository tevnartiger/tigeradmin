<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">



Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Width = Unit.Parse(800)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "Adding ranges to time axes automatically."
   Chart.DefaultSeries.Type=SeriesType.AreaLine
   
   ' This sample will demonstrate how to use TimeLabelAutomationMode.AddRange time label 
   ' automation to add axis time ranges automatically.
   ' Setting the a time scale label mode is not necessary, simply add the time interval.
   Chart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Years)
   
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   ' Add the random data.
   Chart.SeriesCollection.Add(getRandomData())
End Sub 'Page_Load
 


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim dt As New DateTime(2004, 8, 30)
   Dim a As Integer
   For a = 0 To 0
      Dim s As New Series()
      s.Name = "Series " +Convert.ToString(a + 1)
      Dim b As Integer
      For b = 0 To 19
         Dim e As New Element()
         e.YValue = myR.Next(100)
         e.XDateTime =dt.AddDays(10)
         dt = dt.AddDays(10)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   Return SC
End Function 'getRandomData

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
