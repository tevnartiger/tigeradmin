<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Text" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Post Render Drawing Image</title>
		<script runat="server">



Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.Type = ChartType.Combo
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.DefaultSeries.Type = SeriesType.Bubble
   Chart.Title = "Label elements after the chart is generated."
   
   ' This sample shows how element can be labeled after the chart image was created.
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim s As SeriesCollection = getRandomData()
   
   ' Add the random data.
   Chart.SeriesCollection.Add(s)
   
   
   'Get a bitmap of the chart.
   Dim bmp As Bitmap = Chart.GetChartBitmap()
   
   ' Create a graphics object from the bitmap
   Dim g As Graphics = Graphics.FromImage(bmp)
   g.TextRenderingHint = TextRenderingHint.AntiAlias
   
   
   ' After the chart is generated, the element's positions are available.
   ' Lets iterate the elements we have added and draw a centered label at their positions.
   ' String format to center the labels.
   Dim sf As New StringFormat()
   sf.Alignment = StringAlignment.Center
   sf.LineAlignment = StringAlignment.Center
   
   Dim i As Integer
   For i = 0 To s.Count - 1
      Dim ib As Integer
      For ib = 0 To (s(i).Elements.Count) - 1
         Dim el As Element = s(i).Elements(ib)
         g.DrawString(el.Name, New Font("Arial", 8), Brushes.Black, el.Point, sf)
      Next ib 
   Next i
   
   ' Clean up
   g.Dispose()
   
   'Save the image using the FileManager.
   Chart.FileManager.SaveImage(bmp)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   
   Dim a As Integer
   For a = 0 To 0
      Dim s As New Series()
      s.Name = "Series " +Convert.ToString(a + 1)
      Dim b As Integer
      For b = 0 To 3
         If (True) Then
            Dim e As New Element("Element " + Convert.ToString(b+1), myR.Next(50))
            e.BubbleSize = myR.Next(50)
            s.Elements.Add(e)
         End If
      Next b
      SC.Add(s)
   Next a
   Return SC
End Function 'getRandomData


        </script>
	</head>
	<body>
		<div style="text-align:center">
           <dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px" Depth="13px" Use3D="True">
			</dotnet:Chart>
		</div>
	</body>
</html>
