<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.Type = ChartType.Map 'Horizontal;
   Chart.Size = "600x350"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = ".netCHARTING Sample"
   Chart.ChartArea.Background = New Background(Color.FromArgb(142, 195, 236), Color.FromArgb(63, 137, 200), 90)
   Chart.Mapping.DefaultShape.Background.Color = Color.LightGray
   
   ' This sample demonstrates using hotspots to display tooltip information about map shapes.
   Chart.ChartArea.Label.Text = "Hover over any state to see a tooltip."
   
   ' Add a layer	
   Chart.Mapping.MapLayerCollection.Add("../../images/MapFiles/primusa.shp")
   
   ' Set the hotspot tooltip. NOTE: State_Name is an attribute embedded in the states.shp file.
   Chart.Mapping.DefaultShape.Hotspot.ToolTip = "%State_Name"
End Sub 'Page_Load 

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	</head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server" />
		</div>
	</body>
</html>