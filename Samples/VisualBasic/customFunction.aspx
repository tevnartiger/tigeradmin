<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object,e As EventArgs )

	'set global properties
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
    Chart.Title="Sales"
    Chart.XAxis.Label.Text="Months"
    Chart.TempDirectory="temp"
    Chart.Debug=true
    
    'Using a custom function to convert yAxis values
    Chart.YAxis=Chart.YAxis.Calculate("Sales (USD) Thousands",New ChangeValueDelegate(AddressOf MyFunction))
    
    Chart.Series.StartDate= New System.DateTime(2002,1,1,12,0,0)
    Chart.Series.EndDate = New System.DateTime(2002,12,31,11,59,59)
    Chart.DateGrouping = TimeInterval.Year

       'Add a series
    Chart.Series.Name="sales"
    Chart.Series.SqlStatement= "SELECT OrderDate,Total FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate"
    Chart.SeriesCollection.Add()
    
   
End Sub
public Shared Function  MyFunction( value As String) As String

	Dim  dValue As Double = Convert.ToDouble(value)
	dValue = dValue/1000
	value = "$" & Convert.ToString(dValue) & " K"
	return value
End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Custom Function Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
