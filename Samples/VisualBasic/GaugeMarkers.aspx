<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Gauges
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   
   ' This sample will demonstrate how to use axis markers on gauge axes.
   ' First create a series. If you would like to get the data from a database you need to use
   ' the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
   Dim s As New Series("RPM", New Element("e1", 210))
   
   ' Create an axis marker.
   Dim am1 As New AxisMarker("Danger", New Background(Color.Red), 225, 275)
   
   ' Create a background
   Dim bg As New Background(Color.Orange)
   bg.HatchStyle = HatchStyle.LightUpwardDiagonal
   bg.HatchColor = Color.White
   
   ' Create another axis marker.
   Dim am2 As New AxisMarker("Warning", bg, 200, 225)
   
   ' Add the markers to the main y axis.
   Chart.YAxis.Markers.Add(am2)
   Chart.YAxis.Markers.Add(am1)
   
   
   Chart.YAxis.Interval = 25
   Chart.YAxis.Maximum = 250
   
   
   
   ' Add the random data.
   Chart.SeriesCollection.Add(s)
End Sub 'Page_Load 
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
