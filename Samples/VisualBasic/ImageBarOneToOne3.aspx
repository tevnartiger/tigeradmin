<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using the ImageBarSyncToValue feature where the image bars repeat the same number of times as represented by the element's value.

		Chart.TempDirectory = "temp"
		Chart2.TempDirectory = "temp"
		Chart.Debug = False
		Chart.Size = "600x350"
		Chart.Title = "Football Touchdowns - ImageBarSyncToValue = true"
		Chart.YAxis.Maximum = 8
		Chart.YAxis.Interval = 1
		Chart.LegendBox.Visible = False

		Chart2.Size = "600x350"
		Chart2.Title = "Football Touchdowns - ImageBarSyncToValue = false (default)"
		Chart2.YAxis.Maximum = 8
		Chart2.YAxis.Interval = 1
		Chart2.LegendBox.Visible = False

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()
		mySC(0).ImageBarTemplate = "../../images/ImageBarTemplates/football"

		Chart.DefaultSeries.ImageBarSyncToValue = True
		Chart2.DefaultSeries.ImageBarSyncToValue = False

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
		Chart2.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random()
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 1
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 6
				Dim e As Element = New Element("Team " & b.ToString())
				e.YValue = 1 + myR.Next(7)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
	<div align="center">
		<dotnet:Chart ID="Chart2" runat="server" />
	</div>
</body>
</html>
