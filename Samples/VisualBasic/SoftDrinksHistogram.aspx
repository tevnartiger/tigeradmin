<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   ' This sample demonstrates the use of statistical procedures for summarize quantitative data from
   ' within StatisticalEngine.
   ' In this example we use the RelativeTableFrequency to calculate the percentage
   ' of the types of soft drink purchases.
   ' The Histogram Chart
   HistogramChart.Title = " Frequency"
   HistogramChart.Type = ChartType.Pies
   HistogramChart.PieLabelMode = PieLabelMode.Inside
   HistogramChart.TempDirectory = "temp"
   HistogramChart.Debug = False
   HistogramChart.Size = "600x400"
   HistogramChart.LegendBox.Template = "%icon %Name"
   HistogramChart.XAxis.Scale = Scale.Normal
   HistogramChart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   HistogramChart.TitleBox.CornerTopLeft = BoxCorner.Square
   
   HistogramChart.PaletteName = Palette.Three
   
   Dim softDrinks() As String = {"Coke Classic", "Diet Coke", "Pepsi-Cola", "Diet Coke", "Coke Classic", "Coke Classic", "Dr. Pepper", "Diet Coke", "Pepsi-Cola", "Pepsi-Cola", "Coke Classic", "Dr. Pepper", "Sprite", "Coke Classic", "Coke Classic", "Sprite", "Coke Classic", "Diet Coke", "Coke Classic", "Diet Coke", "Coke Classic", "Sprite", "Pepsi-Cola", "Dr. Pepper", "Coke Classic", "Coke Classic", "Pepsi-Cola", "Coke Classic", "Sprite", "Dr. Pepper", "Pepsi-Cola", "Diet Coke", "Pepsi-Cola", "Coke Classic", "Coke Classic", "Coke Classic", "Pepsi-Cola", "Dr. Pepper", "Coke Classic", "Diet Coke", "Pepsi-Cola", "Pepsi-Cola", "Pepsi-Cola", "Pepsi-Cola", "Coke Classic", "Dr. Pepper", "Dr. Pepper", "Pepsi-Cola", "Sprite"}
   
   Dim sampledata As New Series("SoftDrinks")
   Dim i As Integer
   For i = 0 To softDrinks.Length - 1
      Dim el As New Element()
      Select Case softDrinks(i)
         Case "Coke Classic"
            el.YValue = 1
            el.Name = softDrinks(i)
         Case "Diet Coke"
            el.YValue = 2
            el.Name = softDrinks(i)
         Case "Pepsi-Cola"
            el.YValue = 3
            el.Name = softDrinks(i)
         Case "Dr. Pepper"
            el.YValue = 4
            el.Name = softDrinks(i)
         Case "Sprite"
            el.YValue = 5
            el.Name = softDrinks(i)
      End Select
      sampledata.Elements.Add(el)
   Next i
   
   ' CFrequencyTableAOL - Calculates the relative frequency table from above for a discrete data set 
   ' in accordance with the open left boundary (OLB) convention. 
   Dim relativeFreqSeries As Series = StatisticalEngine.RFrequencyTableOL("Frequency", sampledata, New Double() {1, 2, 3, 4, 5})
   relativeFreqSeries.Elements(0).Name = "Coke Classic"
   relativeFreqSeries.Elements(1).Name = "Diet Coke"
   relativeFreqSeries.Elements(2).Name = "Pepsi-Cola"
   relativeFreqSeries.Elements(3).Name = "Dr. Pepper"
   relativeFreqSeries.Elements(4).Name = "Sprite"
   relativeFreqSeries.DefaultElement.LabelTemplate = "%name" + ControlChars.Lf + "%yPercentOfTotal"
   relativeFreqSeries.DefaultElement.SmartLabel.Color = Color.White
   relativeFreqSeries.DefaultElement.SmartLabel.Font = New Font("Arial", 10)
   relativeFreqSeries.DefaultElement.ShowValue = True
   HistogramChart.SeriesCollection.Add(relativeFreqSeries)
End Sub 'Page_Load 
</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="HistogramChart" runat="server"/>
		</div>
	</body>
</html>
