<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates how to set up a timeline during which a machine is shown to be on or off.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }

		Chart.LegendBox.Visible = False
		Chart.Type = ChartType.Combo
		Chart.Size = "700x200"
		Chart.Title = "Pump Activity"
		Chart.Background.Color = Color.Transparent

		Chart.YAxis.ScaleRange = New ScaleRange(0, 1)
		Chart.YAxis.DefaultTick.Label.Font = New Font("Arial", 15, FontStyle.Bold)
		Chart.YAxis.ClearValues = True
		Chart.YAxis.ExtraTicks.Add(New AxisTick(0, "Off"), New AxisTick(1, "On"))

		Chart.XAxis.TimeScaleLabels.Mode = TimeScaleLabelMode.Smart
		Chart.XAxis.TimeScaleLabels.RangeMode = TimeScaleLabelRangeMode.Dynamic
		Chart.XAxis.TimeScaleLabels.DayFormatString = "ddd d"
		Chart.XAxis.SpacingPercentage = 0

		Chart.DefaultElement.ForceMarker = True
		Chart.DefaultElement.Marker.Size = 5
		Chart.DefaultElement.Outline.Color = Color.Green

		Chart.SmartPalette.Add("*", New SmartColor(Color.Red, New ScaleRange(0, 0)))
		Chart.SmartPalette.Add("*", New SmartColor(Color.Green, New ScaleRange(1, 1)))

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Use the getLiveData() method using the dataEngine to query a database.
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim dt As DateTime = New DateTime(2009, 1, 1)
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 1
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 99
				Dim e As Element = New Element()
				e.XDateTime = dt.AddHours(b)
				If e.XDateTime.Hour < 12 Then
					e.YValue = 0
				Else
					e.YValue = myR.Next(2)
				End If
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
