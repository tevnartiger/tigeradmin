<%@ Import Namespace="System.Drawing" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Element Annotation</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   
   
   ' This sample demonstrates how to attach an annotation to an element on the chart. 
   ' This can be any element in any chart type.
   
   ' First we get our data, if you would like to get the data from a database you need to use
   ' the data engine. See sample: features/dataEngine.aspx.
   Dim sc As SeriesCollection = getRandomData()
   
   ' Instantiate the annotation with the initial text. Because this annotation will be associated with an element
   ' we can use all the element tokens in the text, toolTip and even url of this annotation.
   Dim a As New Annotation("Element: %Name " + ControlChars.Lf + " Series: %SeriesName " + ControlChars.Lf + " Value: %YValue")
   
   ' Set the background color.
   a.Background.Color = Color.FromArgb(200, 180, 180, 220)
   
   ' Bevel the background.
   a.Background.Bevel = True
   
   ' Orient the annotation be on the top left side of our element.
   a.Orientation = dotnetCHARTING.Orientation.TopLeft
   
   ' Make all the corners round.
   a.DefaultCorner = BoxCorner.Round
   
   ' Dont dynamically size the annotation.
   a.DynamicSize = False
   
   
   ' Assign the annotation to an element in the sc collection.
   sc(0).Elements(2).Annotation = a
   
   ' Add the random data.
   Chart.SeriesCollection.Add(sc)
End Sub 'Page_Load
 


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px" Depth="13px" Use3D="True">
				
			</dotnet:Chart>
		</div>
	</body>
</html>
