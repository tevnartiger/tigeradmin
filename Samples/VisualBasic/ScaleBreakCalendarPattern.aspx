<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo
   Chart.Size = "800x350"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "Calendar Pattern Scale Breaks (Weekends)"
   
   Chart.XAxis.Line.Color = Color.Orange
   
   Chart.XAxis.TimeInterval = TimeInterval.Days
   Dim cp As CalendarPattern = CalendarPattern.Weekends
   cp.AdjustmentUnit = TimeInterval.Day
   
   Chart.XAxis.ScaleBreakStyle = ScaleBreakStyle.Gap
   Chart.XAxis.DefaultTick.Label.Text = "<%Value,ddd d yyyy>"
   Chart.XAxis.TimeScaleLabels.RangeIntervals.Add(TimeInterval.Months)
   Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Top
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   Chart.XAxis.ScaleBreakCalendarPattern = cp
   Chart.XAxis.ScaleRange = New ScaleRange(New DateTime(2005, 1, 1), New DateTime(2005, 1, 30))
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
   
   Chart.XAxis.Markers.Add(New AxisMarker("", Color.Red, New DateTime(2005, 1, 13)))
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim dt As New DateTime(2005, 1, myR.Next(1, 30))
   Dim a As Integer
   For a = 1 To 2
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 10
         Dim e As New Element()
         'e.Name = "Element " + b.ToString();
         e.YValue = myR.Next(50)
         dt = dt.AddDays(1)
         e.XDateTime =dt
         If dt.DayOfWeek <> DayOfWeek.Saturday And dt.DayOfWeek <> DayOfWeek.Sunday Then
            s.Elements.Add(e)
         End If
      Next b
      SC.Add(s)
   Next a
   Return SC
End Function 'getRandomData

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
