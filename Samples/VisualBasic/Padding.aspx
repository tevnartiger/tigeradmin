<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Pies 'Horizontal;
   Chart.Width = Unit.Parse(700)
   Chart.Height = Unit.Parse(550)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "Padding chart elements"
   
   ' This sample will demonstrate how to pad different parts of the chart.
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' 1. PADDING THE CHART IMAGE
   Chart.Margin = "50"
   
   ' 2. PADDING THE TITLE BOX
   Chart.TitleBox.Position = TitleBoxPosition.FullWithLegend
   Chart.TitleBox.Padding = 20
   
   ' 3. PADDING LEGEND BOXES IN TITLES
   ' This shows how padding of legendboxes works independently of the title boxes they are in.
   ' If a chart area is new with the legend in title and doesnt have a legend box instantiated 
   ' you must do so before you can  modify it's properties.
   Chart.LegendBox.Padding = 8
   
   ' 4. PADDING LEGEND BOXES
   ' We'll create another legend box to show the padding functionality
   Dim lb As New LegendBox()
   lb.HeaderEntry.Visible = True
   lb.HeaderEntry.Name = "New Box"
   lb.HeaderEntry.Value = ""
   lb.HeaderEntry.CustomAttributes.Clear()
   lb.HeaderEntry.CustomAttributes.Add("Icon","")
   ' Legend entries can also add padding at the top which is useful with when used as headers.
   lb.HeaderEntry.PaddingTop = 25
   lb.Padding = 10
   lb.DataSource = DataSource.FromSeriesCollection(mySC)
   ' Add the new legend box.
   Chart.ExtraLegendBoxes.Add(lb)
   
   ' 5. PADDING BETWEEN PIES AND LABELS
   ' This works also with radar and gauge charts.	
   Chart.XAxis.TickLabelPadding = 15
   
   ' 6. PADDING OF CHART BOXES
   ' This includes chart areas, legend boxes etc.
   Chart.ChartAreaSpacing = 20
   
   ' 7. PADDING BETWEEN PIES
   ' This works also with radar and gauge charts.	
   Chart.ChartArea.Padding = 50
   
   ' 8. PADDING ANNOTATIONS
   Dim an As New Annotation("Padded " + ControlChars.Lf + "annotation")
   an.Position = New Point(15, 250)
   ' Specify the annotaiton padding.
   an.Padding = 25
   ' Add the annotation.
   Chart.Annotations.Add(an)
   
   ' NOTE: If you would like to control column width please see sample features/BarFixedSize.aspx
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 0 To 3
      Dim s As New Series()
      s.Name = "Series " & a
      Dim b As Integer
      For b = 0 To 3
         Dim e As New Element()
         e.Name = "Element " & b
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
