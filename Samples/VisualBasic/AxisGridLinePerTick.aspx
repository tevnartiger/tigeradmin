<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub  Page_Load(sender as Object, e as EventArgs )


	Chart.Type = ChartType.Combo
	Chart.Width = new unit(600)
	Chart.Height = new unit(350)
	Chart.TempDirectory = "temp"
	Chart.Debug = true
	Chart.Title = "Arbitrary grid lines using axis ticks."
	
	' * INTRO *
	' This sample will demonstrate how additional grid lines on a chart area at any position using axis ticks.

	' 1. CREATE TICK MARKS AND ADD THEM TO THE AXES
	Dim at as AxisTick  = new AxisTick(17)
	at.GridLine = new Line(Color.Red,2)
	Chart.YAxis.ExtraTicks.Add(at)'
	
	Dim at2 as AxisTick  = new AxisTick(2.4)
	at2.GridLine = new Line(Color.Red,2)
	Chart.XAxis.ExtraTicks.Add(at2)
	
	' 2. ADD DATA
	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Classic samples folder
	' - Help File > Data Tutorials
	' - Sample: features/DataEngine.aspx
	' Add the random data.
	Chart.SeriesCollection.Add(getRandomData())
    
End Sub


Function  getRandomData() As SeriesCollection

	Dim SC As SeriesCollection  = new SeriesCollection()
	Dim myR AS Random  = new Random()
	Dim a,b as Integer
	Dim s as Series 
	Dim e As Element
	
	For a = 0 To 3
	
		s = new Series()
		s.Name = "Series " & a
		For b = 0 To 3
		
			e = new Element()
			'e.Name = "Element " & b
			e.XValue = b+1
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a

	' Set Different Colors for our Series
	SC(0).DefaultElement.Color = Color.FromArgb(49,255,49)
	SC(1).DefaultElement.Color = Color.FromArgb(255,255,0)
	SC(2).DefaultElement.Color = Color.FromArgb(255,99,49)
	SC(3).DefaultElement.Color = Color.FromArgb(0,156,255)

	return SC
End Function
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
