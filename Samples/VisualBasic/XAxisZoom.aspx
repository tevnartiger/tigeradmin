<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>

		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Size = "800x450"
   
   Chart.TempDirectory = "temp"
   
   ' This sample demonstrates an interactive way to zoom a section of an axis.
   Chart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Horizontal
   Chart.DefaultSeries.Type = SeriesType.Line
   Chart.DefaultElement.Marker.Visible = False
   
   
   Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom
   Chart.ChartAreaSpacing = 15
   Chart.ChartArea.Label.Text = "Click any two points on this chart."
   ' Add the random data.	
   Dim sc As SeriesCollection = getRandomData()
   Chart.SeriesCollection.Add(sc)
   
   ' Setup x axis ticks.
   Chart.XAxis.TimeScaleLabels.RangeMode = TimeScaleLabelRangeMode.Dynamic
   Chart.XAxis.TimeScaleLabels.Mode = TimeScaleLabelMode.Hidden
   Chart.XAxis.TimeScaleLabels.DayTick.Label.Text = ""
   Chart.XAxis.DefaultTick.Label.Font = New Font("Arial", 8, FontStyle.Bold)
   Chart.XAxis.Orientation = dotnetCHARTING.Orientation.Top
   ' Generate the chart.
   Dim bp As Bitmap = Nothing
   
   If Not (Page.Request.Params("x2") Is Nothing) Or Not (Page.Request.Params("x") Is Nothing) Then
      ' Only generate the chart the first time if necessary.
      bp = Chart.GetChartBitmap()
   End If
   
   ' Check if the start and end range is specified
   If Not (Page.Request.Params("x2") Is Nothing) And Not (Page.Request.Params("x") Is Nothing) Then
      
      ' Get x and y points
      Dim x As Integer = Convert.ToInt32(Page.Request.Params("x"))
      Dim y As Integer = Convert.ToInt32(Page.Request.Params("y"))
      Dim x2 As Integer = Convert.ToInt32(Page.Request.Params("x2"))
      Dim y2 As Integer = Convert.ToInt32(Page.Request.Params("y2"))
      
      ' Get the axis values at xy positions.
      Dim va As Object = Chart.XAxis.GetValueAtX((x.ToString() + "," + y.ToString()))
      Dim va2 As Object = Chart.XAxis.GetValueAtX((x2.ToString() + "," + y2.ToString()))
      
      
      If Not (va Is Nothing) And Not (va2 Is Nothing) Then
         ' If both click positions were valid:
         ' add a zoom area.
         Dim val As DateTime = Convert.ToDateTime(va)
         Dim val2 As DateTime = Convert.ToDateTime(va2)
         Chart.XAxis.Markers.Clear()
         Dim ca As ChartArea = Chart.ChartArea.GetXZoomChartArea(Chart.XAxis, New ScaleRange(val, val2), New Line(Color.LightGreen, DashStyle.Dash))
         Chart.ExtraChartAreas.Add(ca)
      End If 
   
   Else
      If Not (Page.Request.Params("y") Is Nothing) Then
         ' If only one click position is available:
         ' Get the value and draw a line marker.
         Dim x As Integer = Convert.ToInt32(Page.Request.Params("x"))
         Dim y As Integer = Convert.ToInt32(Page.Request.Params("y"))
         Dim va As Object = Chart.XAxis.GetValueAtX((x.ToString() + "," + y.ToString()))
         
         If Not (va Is Nothing) Then
            Dim val As DateTime = Convert.ToDateTime(va)
            Dim am As New AxisMarker("", Color.Red, val)
            Chart.XAxis.Markers.Add(am)
            
            ' Pass the current click position with the next click.
            imageLabel.Text += "<input type=hidden name=x2 value=" + x.ToString() + ">"
            imageLabel.Text += "<input type=hidden name=y2 value=" + y.ToString() + ">"
         End If
      End If
   End If 
   
   ' Generate the chart again.
   bp = Chart.GetChartBitmap()
   Dim fileName As String = Chart.FileManager.SaveImage(bp)
   imageLabel.Text += "<input type=image value=""submit"" border=0 src=""" + fileName + """ ISMAP>"
   
   bp.Dispose()
End Sub 'Page_Load
Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim dt As New DateTime(2006, 1, 1)
   Dim a As Integer
   For a = 0 To 0
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 0 To 2049
         Dim e As New Element()
         'e.Name = "Element " + b;
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(20) + 20
         s.Elements.Add(e)
         dt = dt.AddDays(1)
         e.XDateTime = dt
      Next b 
      SC.Add(s)
   Next a
   
   
   Return SC
End Function 'getRandomData
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	
	</head>
	<body>
<form action="XAxisZoom.aspx" method="get" >
		<asp:Label ID="imageLabel2" runat="server"/>
		<asp:Label ID="imageLabel" runat="server"/>
		</form>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px" visible="false">
			</dotnet:Chart>
		</div>
	</body>
</html>
