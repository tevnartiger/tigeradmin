<%@ Page Language="VB" debug="true" trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="dotnetCHARTING" %>

<script runat="server">
Sub Page_Load(sender As Object ,e As EventArgs )

	
	'set global properties
    Chart.Title="Average Seasons Temperature"
    Chart.ChartArea.XAxis.Label.Text ="Seasons"
    Chart.ChartArea.YAxis.NumberPercision=0
   	Chart.ChartArea.YAxis.Label.Text="Celsius"
    Chart.TempDirectory="temp"
    Chart.Debug=True
    
   
   
   Dim F As Axis
    F=Chart.YAxis.Calculate("Fahrenheit", Temperature.Celsius,Temperature.Fahrenheit)
	F.Orientation = dotnetCHARTING.Orientation.Right
	Chart.AxisCollection.Add(F)
	
	Dim K As Axis
	K=Chart.YAxis.Calculate("Kelvin", Temperature.Celsius,Temperature.Kelvin)
	K.Orientation = dotnetCHARTING.Orientation.Right
	Chart.AxisCollection.Add(K)


       
    
   'Adding series programatically
   Dim sr As Series =new Series()
  
   sr.Name="Vancouver"
   Dim el As Element  = new Element("Spring",10)
   sr.Elements.Add(el)
   el = new Element("Summer",20)
   sr.Elements.Add(el)
    el = new Element("Autumn",13)
   sr.Elements.Add(el)
   el = new Element("Winter",5)
   sr.Elements.Add(el)
  Chart.SeriesCollection.Add(sr)
   
   sr=new Series()
   sr.Name="Houston"
    el = new Element("Spring",20)
   sr.Elements.Add(el)
   el = new Element("Summer",32)
   sr.Elements.Add(el)
    el = new Element("Autumn",18)
   sr.Elements.Add(el)
   el = new Element("Winter",10)
   sr.Elements.Add(el)
   Chart.SeriesCollection.Add(sr)
 
End Sub

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Unit Conversion Sample</title></head>
<body>
<p>
 <dotnet:Chart id="Chart"  runat="server"/>
</p>
</body>
</html>
