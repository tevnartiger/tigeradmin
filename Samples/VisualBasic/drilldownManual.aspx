<%@ Page Language="VB" Debug="true" Trace="false" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object ,e As EventArgs)

	Dim de As DataEngine 
	Dim sc As SeriesCollection = Nothing
	Dim seriesObj As Series = Nothing
	
	'set global properties
    Chart.Title="Item sales"
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.DefaultSeries.DefaultElement.ToolTip="%yvalue"

    Dim curTimeInterval As dotnetCHARTING.TimeInterval  = TimeInterval.Years
 
   Dim  dateGrouping As string = HttpContext.Current.Request.QueryString("dategrouping")
   IF( (dateGrouping is Nothing) OR (dateGrouping = "") ) Then
		dateGrouping="Years"
   
   Else if(  (Not (dateGrouping Is Nothing)) AND (dateGrouping <> "") )
   
   	
   	curTimeInterval = CType([Enum].Parse(GetType(dotnetCHARTING.TimeInterval), dateGrouping, True), dotnetCHARTING.TimeInterval)

   	End If
   	Dim  startDateString As String = HttpContext.Current.Request.QueryString("startDate")
   	Dim startDate As DateTime  = new DateTime(2002,1,1,0,0,0)
   	If( (Not(startDateString is Nothing )) AND (startDateString <> "") )
   		startDate = DateTime.Parse(startDateString)
   	End If
	
	Dim endDate As DateTime = new DateTime()
	 
	Dim el As Element
	
 	Select Case curTimeInterval
 	
 		case TimeInterval.Years
 			Chart.XAxis.Label.Text="Years"
 			de = new DataEngine()
 			de.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
 			de.DateGrouping = curTimeInterval 
		    de.StartDate= New System.DateTime(2002,1,1,0,0,0)
    		de.EndDate = New System.DateTime(2002,12,31,23,59,59)
    		de.SqlStatement= "SELECT OrderDate,Sum(Quantity) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"
    		sc = de.GetSeries()
    		If( NOT(sc Is Nothing) AND (sc.Count > 0)) Then
    		
    			seriesObj = sc(0)  		
    			
    			For Each  el In seriesObj.Elements
    			
    				el.URL = "?dategrouping=quarters&startDate=" & startDate.ToString()
    				startDate = startDate.AddYears(1)
    			Next el
    		End If
    	
    	case TimeInterval.Quarters
 			Chart.XAxis.Label.Text="Quarters"
 			de = new DataEngine()
 			de.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
 			de.DateGrouping = curTimeInterval 
		    de.StartDate= New System.DateTime(2002,1,1,0,0,0)
    		de.EndDate = New System.DateTime(2002,12,31,23,59,59)
    		de.SqlStatement= "SELECT OrderDate,Sum(Quantity) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"
    		sc = de.GetSeries()
    		If( Not(sc Is Nothing) AND (sc.Count > 0) )
    		
    			seriesObj = sc(0)  		
    			For Each el In seriesObj.Elements
    			
    				el.URL = "?dategrouping=months&startDate=" & startDate.ToString()
    				startDate = startDate.AddMonths(3)
    			Next el
    		End If
    	
    	case TimeInterval.Months
 			Chart.XAxis.Label.Text="Months"
 			de = new DataEngine()
 			de.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
 			de.DateGrouping = curTimeInterval 
		    de.StartDate=startDate
		    endDate = startDate.AddMonths(3) 
		    endDate = endDate.AddSeconds(-1)
    		de.EndDate = endDate
    		de.SqlStatement= "SELECT OrderDate,Sum(Quantity) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"
    		sc = de.GetSeries()
    		If( Not(sc Is Nothing) AND (sc.Count > 0)) Then
    		
    			seriesObj = sc(0)  		
    			
    			For Each  el In seriesObj.Elements
    			
    				el.URL = "?dategrouping=days&startDate=" & startDate.ToString()
    				startDate = startDate.AddMonths(1)
    			Next El
    		End If
    	
    	case TimeInterval.Days
 			Chart.XAxis.Label.Text="Days"
 			de = new DataEngine()
 			de.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
 			de.DateGrouping = curTimeInterval 
		    de.StartDate=startDate
		    endDate = startDate.AddMonths(1) 
		    endDate = endDate.AddSeconds(-1)
    		de.EndDate = endDate
    		de.SqlStatement= "SELECT OrderDate,Sum(Quantity) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"
    		sc = de.GetSeries()
    		If( Not (sc Is Nothing) AND (sc.Count > 0))
    		
    			seriesObj = sc(0)  		
    			
    			For Each el In seriesObj.Elements
    			
    				el.URL = "?dategrouping=hours&startDate=" & startDate.ToString()
    				startDate = startDate.AddDays(1)
    			Next el
    		End If
    	
    	case TimeInterval.Hours
 			Chart.XAxis.Label.Text="hours"
 			de = new DataEngine()
 			de.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
 			de.DateGrouping = curTimeInterval 
		    de.StartDate=startDate
		    endDate = startDate.AddDays(1) 
		    endDate = endDate.AddSeconds(-1)
    		de.EndDate = endDate
    		de.SqlStatement= "SELECT OrderDate,Sum(Quantity) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"
    		sc = de.GetSeries()
    	
    End Select

    Chart.SeriesCollection.Add(sc)

   
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Drill Down Manual Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
 
</div>
</body>
</html>
