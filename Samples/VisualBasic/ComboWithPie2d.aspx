<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates generating a sum pie chart image and inserting it into the legend.

		Chart.ReloadPeriod = New TimeSpan(0, 0, 3)
		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Palette = New Color() { Color.FromArgb(49, 255, 49), Color.FromArgb(255, 255, 0), Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }

		Chart.Type = ChartType.Combo
		Chart.Size = "600x350"
		Chart.Title = ".netCHARTING Sample"
		Chart.ShadingEffectMode = ShadingEffectMode.Four
		Chart.DefaultAxis.GridLine.Color = Color.DarkGray
		Chart.YAxis.AlternateGridBackground.Color = Color.FromArgb(100, 220, 220, 220)
		Chart.ChartArea.Background.Color = Color.FromArgb(255, 200, 200, 200)


		Chart.TitleBox.Background = New Background(Color.LightGray, Color.DarkGray, 90)
		Chart.TitleBox.Label.OutlineColor = Color.LightGray
		Chart.TitleBox.CornerTopLeft = BoxCorner.Round
		Chart.TitleBox.CornerTopRight = BoxCorner.Round
		Chart.LegendBox.Background = New Background(Color.White, Color.DarkGray, 45)

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()
		Dim le As LegendEntry = New LegendEntry(" ", " ")
		le.PaddingTop = 58
		Chart.LegendBox.ExtraEntries.Add(le)

		Dim an As Annotation = New Annotation()
		an.ClearColors()
		Dim pie As System.Drawing.Image = getPieImage(mySC, Chart.Palette)
		Dim tb As TextureBrush = New TextureBrush(pie)
		tb.TranslateTransform(510, 95)
		an.Background = New Background(tb)
		an.Position = New Point(510, 95)
		an.Padding = 10
		an.Size = New Size(65, 65)
		an.Padding = 0
		an.DefaultCorner = BoxCorner.Round

		Chart.Annotations.Add(an)

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)


		pie.Dispose()
		'tb.Dispose();

	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random()
		Dim SC As SeriesCollection = New SeriesCollection()
		For a As Integer = 1 To 4
			Dim s As Series = New Series("Series " & a.ToString())
			For b As Integer = 1 To 4
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getPieImage(ByVal sc As SeriesCollection, ByVal palette As Color()) As System.Drawing.Image
		Dim c As Chart = New Chart()
		c.Type = ChartType.Pie
		c.Palette = palette
		c.Size = "77x75"
		c.Use3D = True
		c.ShadingEffectMode = ShadingEffectMode.One
		c.Margin = "-11"
		c.MarginLeft = -15
		c.Use3D = False
		c.LegendBox.Visible = False
		c.Background = New Background(Color.Transparent)

		c.ChartArea.ClearColors()
		c.SeriesCollection.Add(sc)
		Return c.GetChartBitmap()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
