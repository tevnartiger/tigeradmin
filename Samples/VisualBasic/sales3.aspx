<%@ Page Language="VB" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object ,e As EventArgs )

	'set global properties
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
    Chart.Title="Sales January vs. February"
    Chart.XAxis.Label.Text="Days"
    Chart.ShowDateInTitle=false
    Chart.YAxis.Scale = Scale.FullStacked
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.DateGrouping = TimeInterval.Month
    Chart.DefaultSeries.DefaultElement.ShowValue=true
    Chart.DefaultSeries.DefaultElement.LabelTemplate="%PercentOfGroup"
   
    'Add series 1
    Chart.Series.Name="Jan"
    Chart.Series.StartDate= New System.DateTime(2002,1,1,0,0,0)
    Chart.Series.EndDate = New System.DateTime(2002,1,31,23,59,59)
    Chart.Series.SqlStatement= "SELECT OrderDate,Total FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate"
    
    Chart.SeriesCollection.Add()
    
    'Add series 2
    Chart.Series.Name="Feb"
    Chart.Series.StartDate= New System.DateTime(2002,2,1,0,0,0)
    Chart.Series.EndDate = New System.DateTime(2002,2,28,23,59,59)
    Chart.Series.SqlStatement= "SELECT OrderDate,Total FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate"
    Chart.SeriesCollection.Add()


End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Sales Report</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
