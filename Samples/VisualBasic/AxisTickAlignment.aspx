<%@ Page Language="VB" Debug=True Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

sub Page_Load(sender as Object ,e as EventArgs )


	Chart.TempDirectory = "temp"
	Chart.Debug = true
	Chart2.TempDirectory = "temp"
	Chart2.Debug = true
	Chart3.TempDirectory = "temp"
	Chart3.Debug = true
	Chart.XAxis.SpacingPercentage = 16
	Chart2.XAxis.SpacingPercentage = 16	
	
	
	' This sample will demonstrate how using different tick alignments.
	

	' This chart doesnt center tick marks above the label. With time based x axes, this is the accepted way to draw ticks.
	Chart.XAxis.CenterTickMarks = false
	
	' To move (un-centered) labels, tick length wont work, instead padding has to be used.
	Chart.XAxis.TickLabelPadding = 5
	
	' Only the DefaultTick settings will have an affect when ticks are not centered.
	Chart.XAxis.DefaultTick.Line.Length = 30
	

	' The second chart doesnt center tick marks above the label.
	' The second chart uses a category x axis. This also is the accepted way to draw ticks.
	Chart2.XAxis.CenterTickMarks = false

	

	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Classic samples folder
	' - Help File > Data Tutorials
	' - Sample: features/DataEngine.aspx
	Chart.SeriesCollection.Add(getRandomData())
	Chart2.SeriesCollection.Add(getRandomData2())
	Chart3.SeriesCollection.Add(getRandomData3())
    
    
End Sub


Function  getRandomData() As SeriesCollection

	Dim SC As SeriesCollection  = new SeriesCollection()
	Dim myR AS Random  = new Random()
	Dim a,b as Integer
	Dim s as Series 
	Dim e As Element

	Dim dt as DateTime = new DateTime(2005,1,1)
	For  a = 0 To 0
	
		 s = new Series()
		s.Name = "Series " & a
		
		For  b = 0 To 3
		
			e = new Element()
			e.YValue = myR.Next(50)
			e.XDateTime = dt
			dt = dt.AddMonths(1)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a

	return SC
End Function


Function  getRandomData2() As SeriesCollection

	Dim SC As SeriesCollection  = new SeriesCollection()
	Dim myR AS Random  = new Random()
	Dim a,b as Integer
	Dim s as Series 
	Dim e As Element
	

	For a = 0 To 0
	
		s = new Series()
		s.Name = "Series " & a
		
		For b = 0 To 3
		
			e = new Element()
			e.Name = "Element " & b
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
	return SC
End Function


Function  getRandomData3() As SeriesCollection

	Dim SC As SeriesCollection  = new SeriesCollection()
	Dim myR AS Random  = new Random()
	Dim a,b as Integer
	Dim s as Series 
	Dim e As Element


	For a = 0 To 0
	
		s = new Series()
		s.Name = "Series " & a
		
		For b = 0 To 3
		
			e = new Element()
			e.YValue = myR.Next(50)
			e.XValue = b*2
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a

	return SC
End Function
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<dotnet:Chart id="Chart2" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
			<dotnet:Chart id="Chart3" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
