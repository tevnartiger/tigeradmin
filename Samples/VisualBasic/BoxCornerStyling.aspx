<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates a number box corner stylin options.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.Type = ChartType.Combo
		Chart.Size = "290x380"
		Chart.ChartArea.ClearColors()
		Chart.ChartArea.Line.Color = Color.Black
		Chart.Palette = New Color() { Color.Transparent }
		Chart.DefaultShadow.Color = Color.Empty
		Chart.LegendBox.Visible = False
		Chart.DefaultAxis.ClearValues = True
		Chart.DefaultElement.Outline.Color = Color.Transparent

		Dim a1 As Annotation = getAnnotation("Cut")
		Dim a2 As Annotation = getAnnotation("Rounded")
		Dim a3 As Annotation = getAnnotation("Square")
		Dim a4 As Annotation = getAnnotation("Cut 5px")
		Dim a5 As Annotation = getAnnotation("Rounded 5px")
		Dim a6 As Annotation = getAnnotation("Mixed")

		a1.Position = New Point(50, 40)
		a2.Position = New Point(50, 90)
		a3.Position = New Point(50, 140)
		a4.Position = New Point(50, 190)
		a5.Position = New Point(50, 240)
		a6.Position = New Point(50, 290)

		Chart.Annotations.Add(a1, a2, a3, a4, a5, a6)
		a1.DefaultCorner = BoxCorner.Cut
		a2.DefaultCorner = BoxCorner.Round
		a3.DefaultCorner = BoxCorner.Square
		a4.DefaultCorner = BoxCorner.Cut
		a5.DefaultCorner = BoxCorner.Round
		a6.DefaultCorner = BoxCorner.Cut
		a6.CornerTopRight = BoxCorner.Round
		a6.CornerBottomLeft = BoxCorner.Square

		a4.CornerSize = 5
		a5.CornerSize = 5

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Dim myRand As Random = New Random(1)

	Function getAnnotation(ByVal text As String) As Annotation
		Dim an As Annotation = New Annotation()
		an.Label.Text = text
		an.Label.Font = New Font("Arial", 12, FontStyle.Bold)
		an.Label.OutlineColor = Color.White
		an.Background.Color = Color.FromArgb(100, 230, 125)
		'an.Background.Color = Color.FromArgb(myRand.Next(120), myRand.Next(220), myRand.Next(255));
		an.Size = New Size(190, 40)
		Return an
	End Function

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random(1)
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 1
			Dim s As Series = New Series("Series " & a.ToString())
			For b = 1 To 1
				Dim e As Element = New Element("Element " & b.ToString())
				e.YValue = myR.Next(50)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
