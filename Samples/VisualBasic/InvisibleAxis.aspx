<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="dotnetCHARTING" %>



<script runat="server">
Function  getRandomData() As SeriesCollection

	Dim SC As   New SeriesCollection()
	Dim myR As  New Random()
	Dim a As Integer
	Dim b As Integer
	Dim s As  Series
	Dim e As Element
	For a = 1 To 5
	
		s = new Series()
		s.Name = "Series " & a
		For b = 1 To 6
		    e = new Element()
			e.Name = "E " & b
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a

	Return SC
End Function

Sub Page_Load(sender As Object,e As EventArgs)

	' Set the title.
	Chart.Title="My Chart"
	Chart.Type = ChartType.Combo
	Chart.TempDirectory="temp"

	'Set the chart size.
	Chart.Width = Unit.Parse(600)
	Chart.Height = Unit.Parse(350)
	
	' Get the data.
	Dim sc As SeriesCollection = getRandomData()
	
	'Set one of the series on a different axis
	Dim newAxis As Axis  = new Axis()
	sc(3).XAxis = newAxis
	
	'Clear method makes the original axis invisible.
	Chart.XAxis.Clear()


	' Add the random data.
	Chart.SeriesCollection.Add(sc)
    
    
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Invisible Axis Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
