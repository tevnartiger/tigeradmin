<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>JavaScript Element ToolTips</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Width = Unit.Parse(500)
   Chart.Height = Unit.Parse(250)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "Hover your mouse over a country's column"
   
   'Disable the legend box, we will use the tooltips to view information typically shown there.
   Chart.LegendBox.Visible = False
   
   'Add a shading effect to give the columns some more visual polish.
   Chart.ShadingEffect = True
   Chart.ShadingEffectMode = ShadingEffectMode.Three
   
   
   'This is the bulk of the work forthis sample, a custom HTML attribute is added
   'to render the tooltip using javascript onmouseover.
   Chart.DefaultElement.Hotspot.Attributes.Custom.Add("OnMouseOver", "this.T_WIDTH=60;return escape('%Name %YValue <img src=%flag>')")
   
   
   ' y axis label.
   Chart.YAxis.Label.Text = "GDP (Millions)"
   Chart.YAxis.SmartScaleBreak = True
   
   
   ' Add the first element.
   Chart.Series.Element.Name = "United States"
   Chart.Series.Element.YValue = 5452500
   ' Specify the image for the us flag as custom attribute for the element.
   ' This is used above in HotSpot the %flag token is replaced with this data dynamically
   Chart.Series.Element.CustomAttributes("Flag") = "../../Images/us.png"
   
   
   'Setting a series level palette name assigns a unique color to each element in a single series.
   Chart.Series.PaletteName = Palette.Two
   
   ' Add the element
   Chart.Series.Elements.Add()
   
   ' Repeat for other elements.
   Chart.Series.Element.Name = "Canada"
   Chart.Series.Element.YValue = 786052
   Chart.Series.Element.CustomAttributes("Flag") = "../../Images/ca.png"
   Chart.Series.Elements.Add()
   
   Chart.Series.Element.Name = "United Kingdom"
   Chart.Series.Element.YValue = 477338
   Chart.Series.Element.CustomAttributes("Flag") = "../../Images/uk.png"
   Chart.Series.Elements.Add()
   
   Chart.Series.Element.Name = "Mexico"
   Chart.Series.Element.YValue = 155313
   Chart.Series.Element.CustomAttributes("Flag") = "../../Images/mx.png"
   Chart.Series.Elements.Add()
   
   ' Finally we add the series.
   Chart.SeriesCollection.Add()
End Sub 'Page_Load 

		</script>
	</head>
	<body>
	<br>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
<script language="JavaScript" type="text/javascript" src="../../images/wz_tooltip.js"></script>
	</body>
</html>
