<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.DefaultSeries.DefaultElement.ShowValue = True
   Chart.DefaultSeries.DefaultElement.SmartLabel.Alignment = LabelAlignment.Center
   Chart.DefaultSeries.DefaultElement.SmartLabel.Text = "%YValue - %Name"
   Chart.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom
   
   ' This sample will demonstrate how to create a funnel chart.
   ' Setup the title.
   Chart.Title = "Funnel Chart"
   Chart.TitleBox.Position = TitleBoxPosition.Full
   Chart.TitleBox.Label.Alignment = StringAlignment.Center
   
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySeries As Series = getRandomData()(0)
   
   ' First we sort the series 
   mySeries.Sort(ElementValue.YValue, "DESC")
   
   ' Make it an area line and get rid of the element markers.
   mySeries.Type = SeriesType.AreaLine
   mySeries.DefaultElement.Marker.Type = ElementMarkerType.None
   
   ' Create funnel data
   createFunnel(mySeries)
   
   ' Five each element a new palette.	
   mySeries.PaletteName = Palette.Lavender
   
   ' Clear the background.
   Chart.YAxis.Clear()
   Chart.ChartArea.ClearColors()
   Chart.ChartArea.Background.Color = Color.FromArgb(220, 220, 220)
   Chart.YAxis.ZeroTick = Nothing
   Chart.XAxis.Clear()
   
   ' Turn off all shadows.
   Chart.DefaultShadow.Color = Color.Empty
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySeries)
End Sub 'Page_Load
 

Sub createFunnel(s As Series)
   ' This loop will set the same but negated y value start of the area line as it's value;
   Dim myElement As Element
   For Each myElement In  s.Elements
      ' Because this will double the values we divide it by two first.
      myElement.YValue /= 2
      myElement.YValueStart = - myElement.YValue
   Next myElement 
End Sub 'createFunnel


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random(1)
   Dim a As Integer
   For a = 0 To 3
      Dim s As New Series()
      s.Name = "Series " & a
      Dim b As Integer
      For b = 0 To 3
         Dim e As New Element()
         e.Name = "Element " & b
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
