<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	' This sample demonstrates getting the dynamically determined axis ranges.

	Chart.TempDirectory = "temp"
	Chart.Debug = True
	Chart.Palette = New Color(){Color.FromArgb(49,255,49),Color.FromArgb(255,255,0),Color.FromArgb(255,99,49),Color.FromArgb(0,156,255)}

	Chart.Type = ChartType.Combo
	Chart.Size = "600x350"
	Chart.Title = ".netCHARTING Sample"
	Chart.DefaultSeries.Type = SeriesType.Marker

	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Help File > Getting Started > Data Tutorials
	' - DataEngine Class in the help file	
	' - Sample: features/DataEngine.aspx

	Dim mySC As SeriesCollection = getRandomData()

	' Add the random data.
	Chart.SeriesCollection.Add(mySC)

	'Get a bitmap of the chart so the axes can be processed..
	Dim bmp As Bitmap = Chart.GetChartBitmap()

	' Get chart area rectangle
	Dim caPoints As Point() = Chart.ChartArea.GetCoordinates()
	Dim gp As GraphicsPath = New GraphicsPath()
	gp.AddPolygon(caPoints)
	Dim caRect As RectangleF = gp.GetBounds()
	gp.Dispose()

	' Get axis ranges
	Dim xLow As Double = CDbl(Chart.XAxis.GetValueAtX((CInt(Fix(caRect.Left))+1).ToString() & ",100"))
	Dim xHigh As Double = CDbl(Chart.XAxis.GetValueAtX((CInt(Fix(caRect.Right))).ToString() & ",100"))

	' Get axis ranges
	Dim yHigh As Double = CDbl(Chart.YAxis.GetValueAtY("100," & (CInt(Fix(caRect.Top))+1).ToString()))
	Dim yLow As Double = CDbl(Chart.YAxis.GetValueAtY("100," & (CInt(Fix(caRect.Bottom))).ToString()))
	label1.Text = "X Low: " & xLow.ToString() & " <br> X High: " & xHigh.ToString()
	label1.Text &= "<BR>Y Low: " & yLow.ToString() & "<br> Y High: " & yHigh.ToString()
	label1.Text &= "<BR> Rectangle: " & caRect.ToString()

	'Save the image using the FileManager.
	Chart.FileManager.SaveImage(bmp)

End Sub

Function getRandomData() As SeriesCollection
	Dim myR As Random = New Random(1)
	Dim SC As SeriesCollection = New SeriesCollection()
	        Dim a As Integer
        Dim b As Integer
	For a = 1 To 4
		Dim s As Series = New Series("Series " & a.ToString())
		For b = 1 To 4
			Dim e As Element = New Element("")
			e.XValue = myR.Next(50)
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a
	Return SC
End Function

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>.netCHARTING Sample</title></head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
		</div>
		<asp:Label ID="label1" runat=server/>
	</body>
</html>
