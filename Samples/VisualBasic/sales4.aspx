<%@ Page Language="VB" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object ,e As EventArgs )

	'set global properties
	Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
    Chart.Title="Sales"
    Chart.XAxis.Label.Text="Months"
    Chart.YAxis.Label.Text="USD "
    Chart.TempDirectory="temp"
    Chart.Debug=true
    Chart.DefaultSeries.DefaultElement.ShowValue=true
    Chart.LegendBox.Template="%icon%name"
    Chart.YAxis=Chart.YAxis.Calculate("Sales (USD) Thousands", new ChangeValueDelegate(AddressOf MyFunction))
    

   
    Chart.Series.StartDate= New System.DateTime(2002,1,1,0,0,0)
    Chart.Series.EndDate = New System.DateTime(2002,12,31,23,59,59)
    Chart.DateGrouping = TimeInterval.Year

    'Add a series
    Chart.Series.Name="sales"
    Chart.Series.SqlStatement= "SELECT OrderDate,Total FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE# ORDER BY OrderDate"
    Chart.Series.Elements.Add(Calculation.Sum,"Total")
    Chart.Series.Elements.Add(Calculation.Average,"Avg")
    Chart.Series.Elements.Add(Calculation.Minimum,"Minimum")
	Chart.Series.Elements.Add(Calculation.Maximum,"Maximum")
	Chart.Series.Elements.Add(Calculation.Median,"Med")
	Chart.Series.Elements.Add(Calculation.Mode,"Mode")

    Chart.SeriesCollection.Add()
    
   
End Sub
public Shared Function  MyFunction( value As String) As String
	Dim dValue As double
	dValue = Convert.ToDouble(value)
	dValue = dValue/1000
	return Convert.ToString(dValue)
End function

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Sales Report</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
