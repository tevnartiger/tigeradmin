<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">

Sub Page_Load(sender as Object ,e as EventArgs )


	Chart.Type = ChartType.Combo 'Horizontal
	Chart.Width = new Unit(800)
	Chart.Height = new Unit(350)
	Chart.TempDirectory = "temp"
	Chart.Debug = true
	Chart.Title = "Using a calendar pattern to trim weekends and highlight week days."
	Chart.LegendBox.Visible = false
	
	' This sample will demonstrate how a calendar pattern can be used to trim data and highlight chart area sections.
	' It shows how to
	' - Create and manipulate a calendar pattern
	' - Use it to trim elements
	' - Use it with axis markers.
	' - Setup axis intervals to match data
	
	
	' *DYNAMIC DATA NOTE* 
	' This sample uses random data to populate the chart. To populate 
	' a chart with database data see the following resources:
	' - Classic samples folder
	' - Help File > Data Tutorials
	' - Sample: features/DataEngine.aspx
	Dim sc As SeriesCollection  = getRandomData()
	
	' CREATE A PATTERN
	' We instantiate a calendar patter, and specify that day is represented by each of the 0 & 1 
	' values, week is represented by the whole string.	
	Dim cp As CalendarPattern  = new CalendarPattern(TimeInterval.Day,TimeInterval.Week,"1000001")
	' Because tick marks usually start at the beginning of the day instead of the middle an adjustment unit is specified 
	' to compensate for this offset. This will ensure your tick marks match the calendar pattern.
	cp.AdjustmentUnit = TimeInterval.Day
	
	' TRIM A SERIES
	' To trim elements with this patterm we use the Trim method of the Series object.
	sc(0).Trim(cp,ElementValue.XDateTime)	

	' INVERT THE PATTERN
	' We have trimmed out the weekends but now the pattern will be used with an axis marker to show the week days
	' not the weekends so it must be inverted.
	cp.Invert()	
	' Now the pattern will look like this: "0111110"
	
	' USE A PATTERN WITH AN AXIS MARKER
	' An axis marker is initialized.
	Dim am as AxisMarker  = new AxisMarker("",new Background(Color.FromArgb(100,Color.Orange)),0,0)
	am.Background.Color = Color.FromArgb(50,Color.Orange)
	' Our inverted calendar pattern is specified.
	am.CalendarPattern = cp
	
	am.LegendEntry = new LegendEntry("Week Days","")
	' Add it to the x axis.
	Chart.XAxis.Markers.Add(am)
		
	'  SETUP AXIS INTERVAL TO WORK WITH DATA
	' The effect we want is for the labels to be centered in the middle of each week and 
	' grid lines to be in the middle of the weekend.
	' We'll set week as the TimeInterval for the x axis.
	Chart.XAxis.TimeInterval = TimeInterval.Days
	' Next we set a start time for the interval (Middle of the week) The selected is 11/11/2004 (which is a thursday)
	Chart.XAxis.TimeIntervalAdvanced.Start = new DateTime(2004,11,11) ',22,0,0,0)


	' Add the random data.
	Chart.SeriesCollection.Add(sc)
    
    
End Sub

Function  getRandomData() As SeriesCollection

    Dim SC As SeriesCollection
	SC = new SeriesCollection()
	Dim  myR As Random = new Random()
	Dim dt as DateTime  = new DateTime(2005,1,5)
	Dim a,b As Integer
	Dim e As Element 
	Dim s As Series
	For a = 0 to 0
	
		s  = new Series()
		s.Name = "Series " & a
		
		For  b = 0 To 39
		
		    dt = dt.AddDays(1)
			e = new Element()
			'e.Name = "Element " & b
			e.XDateTime = dt
			'e.YValue = -25 + myR.Next(50)
			e.YValue = myR.Next(50)
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a

	' Set Different Colors for our Series
	SC(0).DefaultElement.Color = Color.FromArgb(49,255,49)


	return SC
End Function
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>

		</div>
	</body>
</html>
