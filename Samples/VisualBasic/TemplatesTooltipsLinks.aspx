<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 4
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   
   Return SC
End Function 'getRandomData



Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   
   '//////////////////// Using templates //////////////////////
   ' In this sample we will demonstrate how templates can be used.
   ' Lets start with the x axis tick labels. The ticks are groups of elements with the same name. 
   ' For this group we will show what percentage of the total data this group represents with the
   ' %YPercentOfTotal token. We will also show the average value of the group.
        Chart.XAxis.DefaultTick.Label.Text = "%Name (%YPercentOfTotal) Ave:%YAverage"
   
   ' Because the y axis is a value axis templates can't be used on it.
   ' Now in the legend we will add the same tokens but the values will be calculated for series instead of 
   ' element groups.
   Chart.DefaultSeries.LegendEntry.Value = "(%YPercentOfTotal) Ave:%YAverage"
   
   ' The element labels can also be modified with these templates. Lets show the value, what percent the 
   ' group the element makes up and what percent of the total the element is.
   ' First in order to show the values we have to set the ShowValue property.
   Chart.DefaultSeries.DefaultElement.ShowValue = True
   
   Chart.DefaultSeries.DefaultElement.LabelTemplate = "%YValue (G: %YPercentOfGroup) (T: %YPercentOfTotal)"
   ' OR (These properties are the same)
   Chart.DefaultSeries.DefaultElement.SmartLabel.Text = "%YValue (G: %YPercentOfGroup) (T: %YPercentOfTotal)"
   
   
   ' Even more info can be shown by using templates in tool tips. These are supported for elements and legend entries.
   ' We'll add a tool tip to the elements here.
   Chart.DefaultSeries.DefaultElement.ToolTip = "%name is " + ControlChars.Cr + "%YPercentOfSeries of %SeriesName"
   
   ' We can also add tool tips to the legend entries.
   Chart.DefaultSeries.LegendEntry.ToolTip = "- %name - " + ControlChars.Cr + "Average: %YAverage" + ControlChars.Cr + "Median: %YMedian" + ControlChars.Cr + "Mode: %YMode"
   
   
   ' Another useful template that can be set is LegendBox.Template. It will determine the layout of the legend
   ' elements.
   
   Chart.LegendBox.Template = "IconNameValue"
   
   
   '//////////////////// Embedding format string into templates //////////////////////
   ' We can embed format string into our chart by using this syntax: <(token),(FormatString)>
   ' To demonstrate we will overwrite the 1st elements template to show the value as currency.
   ' To begin we will get our data
   Dim sc As SeriesCollection = getRandomData()
   
   ' The template will look like this:
   sc(0).Elements(0).LabelTemplate = "<%YValue,Currency>"
   
   ' Just another example for the second series
   sc(1).Elements(0).LabelTemplate = "<%YValue,{0:#0.000}>"
   
   
   '//////////////////// Creating clickable charts using templates //////////////////////
   ' We don�t have any where to redirect so lets pretend site.aspx is a website that will understand 
   ' the info we're going to send
   Chart.DefaultSeries.DefaultElement.URL = "site.aspx?element=%Name"
   
   Chart.DefaultSeries.LegendEntry.URL = "site.aspx?series=%Name"
   
   ' Note: You may also use Element/LegendEntry.URLTarget = "_blank" for example to specify the link target.
   
   
   
   
   ' Add the random data.
   Chart.SeriesCollection.Add(sc)
End Sub 'Page_Load 


</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Templates Tool Tip and Links</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
