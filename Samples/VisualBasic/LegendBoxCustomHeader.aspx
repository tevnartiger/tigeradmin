<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   ' This sample will demonstrate advanced legend entry header features.
   ' *DYNAMIC DATA NOTE* 
   ' This sample uses random data to populate the chart. To populate 
   ' a chart with database data see the following resources:
   ' - Classic samples folder
   ' - Help File > Data Tutorials
   ' - Sample: features/DataEngine.aspx
   Dim mySC As SeriesCollection = getRandomData()
   
   ' Instantiate a legend entry to be our first header.	
   Dim header1 As New LegendEntry("Group A", "Value", "The Icon")
   ' Notice we used a constructor that takes a string for the icon. This can also be done by adding an attribute with 
   ' a 'icon' key' like so:
   header1.CustomAttributes.Add("Icon","The Icon")
   
   ' Now we'll instantiate our second header entry.
   Dim header2 As New LegendEntry("Group B", "Value", "The Icon")
   
   ' We'll add some additional padding at the top of each header.
   header1.PaddingTop = 5
   header2.PaddingTop = 5
   
   ' Specify a divider line color.
   header1.DividerLine.Color = Color.Black
   header2.DividerLine.Color = Color.Black
   
   ' Set a different font and color for the headers.
   header1.LabelStyle.Font = New Font("Arial", 8, FontStyle.Bold)
   header2.LabelStyle.Font = New Font("Arial", 8, FontStyle.Bold)
   header1.LabelStyle.Color = Color.DarkBlue
   header2.LabelStyle.Color = Color.DarkBlue
   
   ' This will demonstrate how a header can start a new column.
   header2.HeaderMode = LegendEntryHeaderMode.StartNewColumn
   
   ' We have our headers but since they are normal legend entries we must ensure they are placed before
   ' the series they represent. Each legend entry sort order is by default 0 so for the first entry 
   ' we set the sort order to -1. Then series mySC[0] and mySC[1] will follow.
   header1.SortOrder = - 1
   ' Now the second entry order we'll set to 1 so if follows the first group and set the sort order of the series 
   ' it represents to 2.
   header2.SortOrder = 1
   mySC(2).LegendEntry.SortOrder = 2
   mySC(3).LegendEntry.SortOrder = 2
   
   'Finally we add the header entries to our box.
   Chart.LegendBox.ExtraEntries.Add(header1)
   Chart.LegendBox.ExtraEntries.Add(header2)
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 0 To 3
      Dim s As New Series()
      s.Name = "Series " & a
      Dim b As Integer
      For b = 0 To 3
         Dim e As New Element()
         e.Name = "Element " & b
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   SC(2).DefaultElement.Color = Color.FromArgb(255, 99, 49)
   SC(3).DefaultElement.Color = Color.FromArgb(0, 156, 255)
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
