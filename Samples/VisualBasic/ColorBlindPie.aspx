<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Color Blind Pie</title>
		<script runat="server">




Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Pies 'Horizontal;
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   
   ' This sample demonstrates how to apply markers to the data when differences in color
   ' are difficult to distinguish.
   ' First we get our data, if you would like to get the data from a database you need to use
   ' the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
   Dim sc As SeriesCollection = getRandomData()
   
   ' The function below takes care of modifying the series.
   ModifySeries(sc)
   
   
   ' Add the random data.
   Chart.SeriesCollection.Add(sc)
End Sub 'Page_Load 


' Markers array
Private Shared defaultDotTypes As ElementMarkerType() =  {ElementMarkerType.Circle, ElementMarkerType.FourPointStar, ElementMarkerType.Diamond, ElementMarkerType.FivePointStar, ElementMarkerType.Square, ElementMarkerType.SevenPointStar, ElementMarkerType.Triangle, ElementMarkerType.SixPointStar}


Sub ModifySeries(sc As SeriesCollection)
   
   Dim s As Series
   For Each s In  sc
      
      ' We want the markers to always appear no matter what the chart or series type. This setting will ensure that.
      s.DefaultElement.ForceMarker = True
      ' We also want the marker to appear in the legend so we need to set the series type of the series legend entry.
      s.DefaultElement.LegendEntry.SeriesType = SeriesType.Marker
      
      ' Each element must also use this marker type and if shown in the legend it must be shown as a marker.
      Dim i As Integer
      For i = 0 To s.Elements.Count - 1
         s.Elements(i).Marker.Type = defaultDotTypes((i Mod defaultDotTypes.Length))
         s.Elements(i).LegendEntry.Marker.Type = defaultDotTypes((i Mod defaultDotTypes.Length))
      Next i
   Next s
End Sub 'ModifySeries
 


Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 1
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 4
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   
   Return SC
End Function 'getRandomData
		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
