<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

<script runat="server">

	Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
		' This sample demonstrates using the ImageBarSyncToValue feature where the image bars repeat the same number of times as represented by the element's value.

		Chart.TempDirectory = "temp"
		Chart.Debug = True
		Chart.DefaultSeries.Palette = New Color() { Color.FromArgb(255, 99, 49), Color.FromArgb(0, 156, 255) }
		Chart.LegendBox.Visible = False
		Chart.Size = "200x300"
		Chart.Type = ChartType.Gauges
		Chart.DefaultSeries.GaugeType = GaugeType.Vertical
		Chart.ShadingEffectMode = ShadingEffectMode.Three
		Chart.ChartArea.ClearColors()

		Chart.DefaultSeries.ImageBarSyncToValue = True
		Chart.DefaultSeries.GaugeBorderBox.DefaultCorner = BoxCorner.Round
		Chart.DefaultSeries.GaugeBorderShape = GaugeBorderShape.UseBox
		Chart.DefaultSeries.GaugeBorderBox.Padding = 6

		Chart.YAxis.Label.Text = "Home Run Competition"
		Chart.YAxis.TickLabelPadding = 2
		Chart.YAxis.Orientation = dotnetCHARTING.Orientation.Right
		Chart.YAxis.Maximum = 8
		Chart.YAxis.Interval = 1

		' *DYNAMIC DATA NOTE* 
		' This sample uses random data to populate the chart. To populate 
		' a chart with database data see the following resources:
		' - Help File > Getting Started > Data Tutorials
		' - DataEngine Class in the help file	
		' - Sample: features/DataEngine.aspx

		Dim mySC As SeriesCollection = getRandomData()
		mySC(0).ImageBarTemplate = "../../images/ImageBarTemplates/baseball"

		' Add the random data.
		Chart.SeriesCollection.Add(mySC)
	End Sub

	Function getRandomData() As SeriesCollection
		Dim myR As Random = New Random()
		Dim SC As SeriesCollection = New SeriesCollection()
		Dim a As Integer = 0
		Dim b As Integer = 0
		For a = 1 To 1
			Dim s As Series = New Series("Jay Vs Bill")
			For b = 1 To 2
				Dim e As Element = New Element("Team " & b.ToString())
				e.YValue = 1 + myR.Next(7)
				s.Elements.Add(e)
			Next b
			SC.Add(s)
		Next a
		Return SC
	End Function

	Function getLiveData() As SeriesCollection
		Dim de As DataEngine = New DataEngine("ConnectionString goes here")
		de.ChartObject = Chart ' Necessary to view any errors the dataEngine may throw.
		de.SqlStatement = "SELECT XAxisColumn, YAxisColumn FROM ...."
		Return de.GetSeries()
	End Function

</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>.netCHARTING Sample</title>
</head>
<body>
	<div align="center">
		<dotnet:Chart ID="Chart" runat="server" />
	</div>
</body>
</html>
