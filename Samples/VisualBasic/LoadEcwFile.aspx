<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
<script runat="server">
 ' This sample demonstrates how to load an ecw file in combination with  a shape file.
Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.Type = ChartType.Map
   Chart.Size = "1000x450"
   Chart.Title = "Satellite composite image layer"
   Chart.TempDirectory = "temp"
   Dim layer As MapLayer = MapDataEngine.LoadLayer("../../Images/MapFiles/worldmap.ecw")
   Chart.Mapping.MapLayerCollection.Add(layer)
   
   Chart2.Type = ChartType.Map
   Chart2.Size = "1000x550"
   Chart2.Title = "Loading the United States shape file"
   Chart2.TempDirectory = "temp"
   Chart2.ChartArea.Background = New Background(Color.FromArgb(142, 195, 236), Color.FromArgb(63, 137, 200), 90)
   Chart2.Mapping.ZoomPercentage = 98
   Chart2.Mapping.ZoomCenterPoint = New PointF(44,- 120)
   
   Dim layerStates2 As MapLayer = MapDataEngine.LoadLayer("../../Images/MapFiles/states.shp")
   layerStates2.DefaultShape.Label.Text = "%STATE_ABBR"
   layerStates2.DefaultShape.Label.Color = Color.Blue
   layerStates2.DefaultShape.Label.OutlineColor = Color.White
   layerStates2.DefaultShape.Label.Font = New Font("Arial", 7)
   layerStates2.DefaultShape.Background.Color = Color.PapayaWhip
   layerStates2.DefaultShape.Line.Color = Color.White
   Chart2.Mapping.MapLayerCollection.Add(layerStates2)
   
   Chart3.Type = ChartType.Map
   Chart3.Size = "1000x500"
   Chart3.Title = "Combining United states shapefile with satellite image layer"
   Chart3.TempDirectory = "temp"
   
   Dim layerJpg As MapLayer = MapDataEngine.LoadLayer("", "../../Images/MapFiles/NorthAmerica.jpg")
   Chart3.Mapping.MapLayerCollection.Add(layerJpg)
	Chart3.Mapping.Projection.Type = ProjectionType.LambertConic
	Chart3.Mapping.Projection.Parameters = "-96, 40, 32, 50"
   Chart3.Mapping.DefaultShape.Line.Color = Color.LightGray
   Dim layerStates3 As MapLayer = MapDataEngine.LoadLayer("../../Images/MapFiles/states.shp")
   layerStates3.DefaultShape.Label.Text = "%STATE_ABBR"
   layerStates3.DefaultShape.Label.Color = Color.Black
   layerStates3.DefaultShape.Label.OutlineColor = Color.White
   layerStates3.DefaultShape.Label.Font = New Font("Arial", 7)
   Chart3.Mapping.MapLayerCollection.Add(layerStates3)
End Sub 'Page_Load

</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" >
			</dotnet:Chart>
			<dotnet:Chart id="Chart2" runat="server" >
			</dotnet:Chart>
			<dotnet:Chart id="Chart3" runat="server" >
			</dotnet:Chart>

		</div>
	</body>
</html>
