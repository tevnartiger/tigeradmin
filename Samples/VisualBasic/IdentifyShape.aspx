<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING.Mapping" %>

		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   Chart.Type = ChartType.Map
   Chart.Size = "600x350"
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = ".netCHARTING Sample"
   Chart.Visible = False
   Chart.ChartArea.Label.Text = "Please click on a state to fill it with red."
   Chart.ChartArea.Background = New Background(Color.FromArgb(142, 195, 236), Color.FromArgb(63, 137, 200), 90)
   Chart.Mapping.DefaultShape.Background.Color = Color.LightGray
   
   ' This sample demonstrates getting and setting the properties of a shape on a map based on a click position.
   ' FullScreen
   Dim bp As Bitmap = Nothing
   Chart.Mapping.MapLayerCollection.Add("../../images/MapFiles/primusa.shp")
   
   If Not (Page.Request.Params("y") Is Nothing) And Not (Page.Request.Params("x") Is Nothing) Then
      bp = Chart.GetChartBitmap()
      
      ' Get x and y points
      Dim x As Integer = Convert.ToInt32(Page.Request.Params("x"))
      Dim y As Integer = Convert.ToInt32(Page.Request.Params("y"))
      
      Dim al As ArrayList = Chart.Mapping.GetShapesAtPoint(New Point(x, y))
      Dim o As Object
      For Each o In  al
         CType(o, Shape).Background.Color = Color.Red
         iLabel.Text += "<BR>State: " + CType(o, Shape)("STATE_NAME")
         iLabel.Text += "<BR>Population: " + Double.Parse(CType(o, Shape)("Population").ToString()).ToString("###,###")
      Next o
   End If
   
   ' Update the bitmap image.
   bp = Chart.GetChartBitmap()
   Dim fileName As String = Chart.FileManager.SaveImage(bp)
   imageLabel.Text += "<input type=image value=""submit"" border=0 src=""" + fileName + """ ISMAP>"
   
   bp.Dispose()
End Sub 'Page_Load

</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>	</head>
	<body>
		<div align="center">
		<form method =get >
		<asp:Label ID=imageLabel Runat=server/>
			</form>
			<dotnet:Chart id="Chart" runat="server" />
		<asp:Label ID="iLabel" Runat=server></asp:Label>
		</div>
		
	</body>
</html>
