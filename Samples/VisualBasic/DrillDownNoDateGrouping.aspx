<%@ Page Debug="true" Trace="false" Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Debug = True
   Chart.TempDirectory = "temp"
   Chart.DefaultSeries.DefaultElement.ToolTip = "Population: %yvalue"
   
   Dim connection As String = "Provider=Microsoft.Jet.OLEDB.4.0;data source=" + Server.MapPath("../../database/chartsample.mdb")
   Dim sql As String = ""
   
   ' This sample will demonstrate how a custom drill down chart can be created.
   ' The database is not provided. The structure of this database would be something like this:
   ' COUNTRIES Table:
   ' CountryID	Country		Population
   ' STATES Table:
   ' StateID		State		Population	CountryID
   ' CITIES Table
   ' CityID		City		Population	StateID
   ' First we show all the countries in the database. When a country is clicked we show the states within
   ' the country. When a state is clicked we show the cities within it.
   ' Instantiate a data engine object and set the connection string.
   Dim de As New DataEngine()
   de.ConnectionString = connection
   
   ' Instantiate a series collection for later use.
   Dim sc As New SeriesCollection()
   
   ' Test the querystring to see what level in the drill down we are at.
   ' Level 1 = Country
   ' Level 2 = State
   ' Level 3 = City
   If Request.QueryString("ddLevel") Is Nothing Or Request.QueryString("ddLevel") = "" Then
   		Chart.Title="Countries Population"
      ' If the query string is empty this is the first level.
      ' We are on the first level (country)
      de.SqlStatement = "SELECT Country, CountryID, Population FROM Countries"
      
      ' We specify the element fields where the database data will be stored.
      de.DataFields = "xAxis=Country,yAxis=Population,Url=CountryID"
      
      sc = de.GetSeries()
      ' We now have the data for countries. We will want to itterate through the elements
      ' URL values and modify them for the next drill down level.
      Dim s As Series
      For Each s In  sc
         Dim el As Element
         For Each el In  s.Elements
            el.URL = "?id=" + el.URL + "&ddLevel=2"
         Next el
      Next s
   Else
      If Request.QueryString("ddLevel") = "2" Then
      	  Chart.Title="State/Province Population"
         ' We are on the second level (state)
         ' We make the sql query using the passed id.
         de.SqlStatement = "SELECT State, StateID, Population FROM States WHERE CountryID = " + Request.QueryString("id")
         
         ' We specify the element fields where the database data will be stored.
         de.DataFields = "xAxis=State,yAxis=Population,Url=StateID"
         
         sc = de.GetSeries()
         ' We now have the data for states. We will iterate through the elements
         ' URL values and modify them for the next drill down level.
         Dim s As Series
         For Each s In  sc
            Dim el As Element
            For Each el In  s.Elements
               el.URL = "?id=" + el.URL + "&ddLevel=3"
            Next el
         Next s
      Else
         If Request.QueryString("ddLevel") = "3" Then
         	Chart.Title="Cities Population"
            ' We are on the third level (city)
            ' We make the sql query using the passed id.
            de.SqlStatement = "SELECT City, CityID, Population FROM Cities WHERE StateID = " + Request.QueryString("id")
            
            ' We specify the element fields where the database data will be stored.
            de.DataFields = "xAxis=City,yAxis=Population"
            
            ' Since this is the last level we don�t have to process the urls
            sc = de.GetSeries()
         End If
      End If
   End If
   ErrorMessage.Text = de.ErrorMessage
   
   
   
   
   ' Add the data.
   Chart.SeriesCollection.Add(sc)
End Sub 'Page_Load 

</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>DrillDown Without Date Grouping</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
 <Asp:Label id="ErrorMessage"  runat="server"/>
</div>
</body>
</html>
