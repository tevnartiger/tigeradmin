<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Logarithm Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   
   ' This sample demonstrates the use of TrendLineLogarithmic from within Forecasting 	
   'which find the best fitting logarithmical curve to sets of data
   ' The Forecast Chart
   ForecastChart.Title = "Forecast Logarithm"
   ForecastChart.TempDirectory = "temp"
   ForecastChart.Debug = True
   ForecastChart.Size = "1000x400"
   ForecastChart.LegendBox.Template = "%icon %name"
   ForecastChart.XAxis.Scale = Scale.Normal
   
   Dim slogarithm As New SeriesCollection()
   Dim sampledata1 As New Series("Sample 1")
   Dim i As Integer
   For i = 1 To 9
      Dim el As New Element()
      el.YValue = Math.Log(i) * i + 2
      el.XValue = i
      sampledata1.Elements.Add(el)
   Next i
   slogarithm.Add(sampledata1)
   slogarithm(0).Type = SeriesType.Marker
   
   ForecastChart.SeriesCollection.Add(slogarithm)
   
   
   ' Here we create a series which will hold the Y values calculated with the LogarithmFit indicator
   Dim trendLineLogarithmic As New Series()
   trendLineLogarithmic = ForecastEngine.TrendLineLogarithmic(sampledata1)
   
   'The next two lines display on to the chart the logarithmic function used
   ' to fit the curve
   trendLineLogarithmic.Elements(0).SmartLabel.Text = "Function: %Function"
   trendLineLogarithmic.Elements(0).ShowValue = True
   
   trendLineLogarithmic.DefaultElement.Color = Color.FromArgb(255, 99, 49)
   trendLineLogarithmic.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(trendLineLogarithmic)
End Sub 'Page_Load 
		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="ForecastChart" runat="server"/>
			
			
		</div>
	</body>
</html>
