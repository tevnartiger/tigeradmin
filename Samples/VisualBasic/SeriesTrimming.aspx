<%@ Page Language="VB" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>
		<script runat="server">





Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.ComboSideBySide 'Horizontal;
   Chart.Width = Unit.Parse(600)
   Chart.Height = Unit.Parse(350)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   Chart.Title = "Series.Trim() Method"
   Dim a As New Annotation()
   a.Position = New Point(300, 70)
   a.ClearColors()
   a.Label.Text = "Series 2 is trimmed to only leave elements with y values between 15 and 30"
   Chart.Annotations.Add(a)
   
   
   ' This sample will demonstrate how to trim elements from a series.
   ' First we get our data, if you would like to get the data from a database you need to use
   ' the data engine. See sample: features/dataEngine.aspx. Or the dataEngine tutorial in the help file.
   Dim mySC As SeriesCollection = getRandomData()
   
   ' mySC is a series collection that contains two series
   ' The first series will be shown in it's entirety.
   ' The second is trimmed to only leave elements with y values between 20 and 30
   mySC(1).Trim(15, 30, ElementValue.YValue)
   
   ' Add a marker to the y axis to show the range of vales not trimmed from the second series.
   Dim M As New AxisMarker()
   M.LegendEntry.Name = "Active zone"
   M.Background = New Background(Color.FromArgb(100, Color.Red))
   M.ValueHigh = 30
   M.ValueLow = 15
   Chart.YAxis.Markers.Add(M)
   
   ' Add the random data.
   Chart.SeriesCollection.Add(mySC)
End Sub 'Page_Load
 

Function getRandomData() As SeriesCollection
   Dim SC As New SeriesCollection()
   Dim myR As New Random()
   Dim a As Integer
   For a = 1 To 2
      Dim s As New Series()
      s.Name = "Series " + a.ToString()
      Dim b As Integer
      For b = 1 To 50
         Dim e As New Element()
         e.Name = "Element " + b.ToString()
         'e.YValue = -25 + myR.Next(50);
         e.YValue = myR.Next(50)
         s.Elements.Add(e)
      Next b
      SC.Add(s)
   Next a
   
   ' Set Different Colors for our Series
   SC(0).DefaultElement.Color = Color.FromArgb(49, 255, 49)
   SC(1).DefaultElement.Color = Color.FromArgb(255, 255, 0)
   Return SC
End Function 'getRandomData

		</script>
	</head>
	<body>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
