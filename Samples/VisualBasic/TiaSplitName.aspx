<%@ Page Language="vb" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<script runat="server">

Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
	' This sample demonstrates splitting a single series into multiple series based on element's name.

	SetChart(Chart)
	SetChart(Chart1)
	SetChart(Chart2)

	Chart.Title = "Original Data"
	Chart.XAxis.Scale = Scale.Time
	Chart.LegendBox.DefaultEntry.Value = "%Sum"
	Dim mySC As SeriesCollection = getRandomData()
	Chart.SeriesCollection.Add(mySC) 'Original Data

	'Group the data with year by months
	Chart1.DefaultElement.DefaultSubValue.Marker.Visible = False
	Chart1.DefaultElement.DefaultSubValue.Type = SubValueType.Marker
	Chart1.DefaultElement.DefaultSubValue.Marker.Type = ElementMarkerType.None
	Chart1.ChartArea.Label.Text = "Grouping the original series by using TimeIntervalAdvanced" & Constants.vbLf & "object. The elements are grouped with" & Constants.vbLf & "year by months."
	Dim splits As Series = mySC(0).SplitRegroupCalculate(TimeIntervalAdvanced.Year,TimeIntervalAdvanced.Month,Calculation.Sum,"MMM",True)
	Chart1.SeriesCollection.Add(splits)

	'Split the previous series based on original element's name.
	Chart2.ChartArea.Label.Text = "Splitting the previous series by the original element's name." & Constants.vbLf & "(Element subvalue's name)"

	Dim mySC2 As SeriesCollection = splits.SplitByName()
	'SplitByName with parameters.
	'SeriesCollection mySC2 = splits.SplitByName("3",LimitMode.Bottom,true,"All others");
	Chart2.DefaultElement.DefaultSubValue.Marker.Visible = False
	Chart2.DefaultElement.DefaultSubValue.Type = SubValueType.Marker
	Chart2.DefaultElement.DefaultSubValue.Marker.Type = ElementMarkerType.None
	Chart2.SeriesCollection.Add(mySC2)


End Sub
Sub SetChart(ByVal c As Chart)
	c.Type = ChartType.Combo
	c.Size = "600x350"
	c.TempDirectory = "temp"
	c.Debug = True
	c.Title = ".netCHARTING Sample"
	c.LegendBox.Orientation = dotnetCHARTING.Orientation.Bottom
End Sub


Function getRandomData() As SeriesCollection
	Dim names As String() = {"John","David", "Mary", "Carlson", "Sam", "Mark"}
	Dim SC As SeriesCollection = New SeriesCollection()
	Dim myR As Random = New Random(1)
        Dim dt As DateTime = New DateTime(2006, 1, 1)
        Dim a As Integer
        Dim b As Integer
	For a = 1 To 1
		Dim s As Series = New Series()
		s.Name = "Series " & a.ToString()
		For b = 1 To 364
			Dim e As Element = New Element()
			e.Name = names(myR.Next(0,5))
			e.YValue = myR.Next(50)
			dt = dt.AddDays(1)
			e.XDateTime = dt
			s.Elements.Add(e)
		Next b
		SC.Add(s)
	Next a

	Return SC
End Function
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Sample</title>    </head>
	<body>
		<div align="center">
			<dotnet:Chart id="Chart" runat="server"/>
			<dotnet:Chart id="Chart1" runat="server"/>
			<dotnet:Chart id="Chart2" runat="server"/>
		</div>
	</body>
</html>
