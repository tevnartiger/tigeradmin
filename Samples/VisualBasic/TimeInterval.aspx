<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component"%>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>


<script runat="server">
Sub Page_Load(sender As Object,e As EventArgs )

	'set global properties
    Chart.DefaultSeries.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")  
    Chart.Title="sales"
    Chart.XAxis.Label.Text="Days"
    Chart.TempDirectory="temp"
    Chart.Type=ChartType.Scatter
    Chart.Debug=true
    Chart.LegendBox.Template="%icon%name"
    
    'set Axis timeInterval
    Chart.XAxis.TimeInterval =TimeInterval.Days
    Chart.XAxis.Scale = Scale.Time
        
    
    'Add a series
    Chart.Series.Name="Items"
    Chart.Series.Type = SeriesType.Spline
    Chart.Series.StartDate= New System.DateTime(2002,1,1,0,0,0)
    Chart.Series.EndDate = New System.DateTime(2002,1,5,23,59,59)
    Chart.Series.SqlStatement= "SELECT OrderDate,Sum(Total) FROM Orders  WHERE OrderDate >= #STARTDATE# AND OrderDate <= #ENDDATE#  GROUP BY Orders.OrderDate ORDER BY Orders.OrderDate"
    Chart.SeriesCollection.Add()
    
       
End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Time Interval Sample</title></head>
<body>
<div style="text-align:center">
 <dotnet:Chart id="Chart"  runat="server"/>
</div>
</body>
</html>
