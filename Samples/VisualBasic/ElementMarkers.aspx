<%@ Page Language="VB"  debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Element Markers</title>
		<script runat="server">

Sub Page_Load(sender As [Object], e As EventArgs)
   
   Chart.Type = ChartType.Combo 'Horizontal;
   Chart.Width = Unit.Parse(700)
   Chart.Height = Unit.Parse(250)
   Chart.TempDirectory = "temp"
   Chart.Debug = True
   
   
   ' This sample demonstrates using image markers on elements and in the legend.
   ' Markers dont show up on columns by default therefore we force them all to show up with the force marker property.
   Chart.DefaultSeries.DefaultElement.ForceMarker = True
   
   ' The legend must display our marker icon so we set the series type to marker.
   Chart.DefaultSeries.DefaultElement.LegendEntry.SeriesType = SeriesType.Marker
   
   'Set the legend entry marker size 
 	Chart.DefaultSeries.DefaultElement.LegendEntry.Marker.Size= 15
   
   ' y axis label.
   Chart.YAxis.Label.Text = "GDP (Millions)"
   Chart.YAxis.SmartScaleBreak = true

   
   
   ' Add the first element.
   Chart.Series.Element.Name = "United States"
   Chart.Series.Element.YValue = 5452500
   ' Specify the image for the us flag.
   Chart.Series.Element.Marker.ImagePath = "../../Images/us.png"
   
   ' Add the element
   Chart.Series.Elements.Add()
   
   ' Repeat for other elements.
   Chart.Series.Element.Name = "Canada"
   Chart.Series.Element.YValue = 786052
   Chart.Series.Element.Marker.ImagePath = "../../Images/ca.png"
   Chart.Series.Elements.Add()
   
   Chart.Series.Element.Name = "United Kingdom"
   Chart.Series.Element.YValue = 477338
   Chart.Series.Element.Marker.ImagePath = "../../Images/uk.png"
   Chart.Series.Elements.Add()
   
   Chart.Series.Element.Name = "Mexico"
   Chart.Series.Element.YValue = 155313
   Chart.Series.Element.Marker.ImagePath = "../../Images/mx.png"
   Chart.Series.Elements.Add()
   
   ' In order to show each element in the legend we set the palette property for our series.
   Chart.Series.PaletteName = Palette.One
   
   ' Finally we add the series.
   Chart.SeriesCollection.Add()
End Sub 'Page_Load 

		</script>
	</head>
	<body>
	<br>
		<div style="text-align:center">
			<dotnet:Chart id="Chart" runat="server" Width="568px" Height="344px">
			</dotnet:Chart>
		</div>
	</body>
</html>
