<%@ Page Language="VB" Debug="true" Description="dotnetCHARTING Component" %>
<%@ Register TagPrefix="dotnet"  Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Drawing2D" %>
<%@ Import Namespace="dotnetCHARTING"%>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>.netCHARTING Forecasting Sample</title>
		<script runat="server">


Sub Page_Load(sender As [Object], e As EventArgs)
   ' This sample demonstrates the use of TrendLine Forecasting engine in order to
   ' find the function of best fit for the seventh degree polynom. The data used for which
   ' the polynom is fit is a set of data which represents a FX exchange rate over a given
   ' period of time. 
   ' The Forecast Chart
   ForecastChart.Title = "Exchange"
   ForecastChart.TempDirectory = "temp"
   ForecastChart.Debug = True
   ForecastChart.Size = "1000x800"
   ForecastChart.LegendBox.Template = "%icon %name"
   
   ForecastChart.YAxis.ScaleRange.ValueLow = 220
   'ForecastChart.XAxis.Scale = Scale.Normal;
   ' The Forecast data
   Dim de As New DataEngine()
   de.ConnectionString = ConfigurationSettings.AppSettings("DNCConnectionString")
   de.DateGrouping = TimeInterval.Days
   de.StartDate = New DateTime(1983, 3, 10, 0, 0, 0)
   de.EndDate = New DateTime(1984, 12, 10, 23, 59, 59)
   de.SqlStatement = "SELECT ID, Value AS q FROM Exchange WHERE Data >= #STARTDATE# AND Data <= #ENDDATE# ORDER BY Data "
   
   'Add a series
   Dim scForecast As SeriesCollection = de.GetSeries()
   ForecastChart.SeriesCollection.Add(scForecast)
   
   scForecast(0).Name = "Exchange"
   scForecast(0).Type = SeriesType.Spline
   
   ' Takes off the marker off the line and spline series.
   ForecastChart.DefaultSeries.DefaultElement.Marker = New ElementMarker(ElementMarkerType.None)
   ForecastChart.ChartAreaLayout.Mode = ChartAreaLayoutMode.Vertical
   
   ' We generate the best fitting curve of a 7th degree polynom.
   Dim trendLine As New Series()
   trendLine = ForecastEngine.TrendLinePolynomial(scForecast(0), 7)
   trendLine.Name = "7th Degree Polynomial"
   trendLine.Type = SeriesType.Spline
   ForecastChart.SeriesCollection.Add(trendLine)
End Sub 'Page_Load

		</script>
	</head>
	<body>
		<div style="text-align:center">
			
			<dotnet:Chart id="ForecastChart" runat="server"/>
			
		</div>
	</body>
</html>
